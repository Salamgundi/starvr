# README - **StarVR**#

Created by: Avery Wagar, Jeremiah Murphy

Current Version: 0.14.4 Beta

### How do I get set up? ###

- Have Unity 5.2.2 or **higher** installed

- Clone or download repo

- Open project in Unity `(May require re-import)`

- Install SteamVR plugin from asset store **Or** Install GVR SDK from [here](https://developers.google.com/vr/) (For IOS/Android)

- Load scene

- Build to selected target

**Or (if you're lazy.)**

- Download prebuilt versions from [here](https://bitbucket.org/Salamgundi/spacevrdownloads/downloads/)

### Contribution guidelines ###

-Found a bug? Let us know in the issues section on our repo

-Good at C#? **Create a branch!**

### Who do I talk to? ###
- Wanna chat? Find me on Twitter @ajmwagar