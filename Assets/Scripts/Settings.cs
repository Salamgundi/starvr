﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    public GameObject Constellations;
    public float volume;
    public static bool ConOn;
    // Use this for initialization
    void Start()
    {
        volume = 1;

    }
   public void Volume()
    {
        if (volume > 0)
        {
            volume = 0;
        }
        if (volume <= 1)
        {
            volume = 1;
        }
    }
   public void starcon()
    {
        ConOn = !ConOn;
        //Constellations.SetActive(ConOn);
    }
    void Update(){
    Constellations.SetActive(ConOn);
    }



}
