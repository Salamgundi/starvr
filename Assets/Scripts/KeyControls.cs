using UnityEngine;
using Valve.VR;
public class KeyControls : MonoBehaviour{
	public static bool DemoModeOn;
	public GameObject DemoMode;
	private SteamVR_TrackedObject trackedObj;
	public GameObject InObj;
	public GameObject InObj1;
	public GameObject InObj2;
	public GameObject InObj3;
	public bool InOn = true;
	public GameObject controller;
	// 2
	private EVRButtonId _padPutton = EVRButtonId.k_EButton_SteamVR_Touchpad;

	private SteamVR_Controller.Device Controller
	{
		get { return SteamVR_Controller.Input((int)trackedObj.index); }
	}
	void Awake()
	{
		trackedObj = controller.GetComponent<SteamVR_TrackedObject>();

	}
	void Update () {
		if (Input.GetKeyDown("q")){
			Application.Quit();
		}
		if (Controller.GetPressDown (_padPutton)) {
			InOn = !InOn;
			InObj.SetActive (InOn);
			InObj1.SetActive (InOn);
			InObj2.SetActive (InOn);
			InObj3.SetActive (InOn);
		}	
		if (Input.GetKeyDown("d")){
			DemoModeOn = !DemoModeOn;
			DemoMode.SetActive(DemoModeOn);
		}
	}
}
