﻿using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public float speed = 5f;
    public float RotationSpeed = 5f;

    void Update()
    {
        transform.localEulerAngles += new Vector3(0, Input.GetAxis("Horizontal") * RotationSpeed, 0);
        transform.localEulerAngles += new Vector3(Input.GetAxis("Vertical") * speed, 0, 0);

    }
}
