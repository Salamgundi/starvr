﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSunController : MonoBehaviour
{
    public GameObject OptOn1;
    public GameObject OptOn2;
    public GameObject OptOff1;
    public GameObject OptOff2;
    public GameObject OptOff3;
    public GameObject OnSun;
    public GameObject sun2;
    
    // Use this for initialization



    public void PlanetOn()
    {

        OptOn1.SetActive(true);
        OptOn2.SetActive(true);
        OnSun.SetActive(true);
        sun2.SetActive(false);
        OptOff1.SetActive(false);
        OptOff2.SetActive(false);
        OptOff3.SetActive(false);
    }
}
