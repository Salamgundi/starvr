﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reset : MonoBehaviour {
	//public GameObject controller;
    // Use this for initialization
    private SteamVR_TrackedObject trackedObj;
    // 2
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }
    void Update()
    {

        if (Controller.GetHairTriggerDown())
        {
            SceneManager.LoadScene(0);
        }
    }
    public void solarView()
    {
        SceneManager.LoadScene(1);
    }
}
