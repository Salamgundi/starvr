using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DemoTimer : MonoBehaviour {

	static float time = 180;
	public GameObject DemoOver;
	public GameObject StartMessage;
	public GameObject controller;
	static public bool Started;
	static public bool CoroutineOn;

	private SteamVR_TrackedObject trackedObj;
	// 2
	private SteamVR_Controller.Device Controller
	{
		get { return SteamVR_Controller.Input((int)trackedObj.index); }
	}
	void Awake()
	{
		trackedObj = controller.GetComponent<SteamVR_TrackedObject>();
	}
	void Start (){
		//StartCoroutine(ShowMessage("This is a test.", 7));
	}
	void Update() {
		if (Started) {
			time -= Time.deltaTime;
			StartMessage.SetActive(false);
		}

		if (time <= 0){
			if (!CoroutineOn){
				StartCoroutine(ShowMessage(7));
			}
		}
		if(!Started){
			StartMessage.SetActive(true);
			if (Controller.GetHairTriggerDown()){
				//if(Input.GetKeyDown("t")){
				ButtonClick();
			}
		}
	}
	void restartDemo (){
		//DemoOver.SetActive(false);
		SceneManager.LoadScene(0);
		Started = false;
		time = 180;
		CoroutineOn = false;
	}
	public void ButtonClick(){
		Started = true;

	}

	IEnumerator ShowMessage(float delay)
	{
		CoroutineOn = true;
		DemoOver.SetActive(true);
		yield return new WaitForSeconds(delay);
		DemoOver.SetActive(false);
		restartDemo();
		//Debug.Log("Coroutine Finished.");
	}
}
