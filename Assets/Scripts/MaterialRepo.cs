﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MaterialRepo : MonoBehaviour {
	public List<Material> mats;
	public static MaterialRepo inst;

	// Use this for initialization
	public void Awake () { if(inst == null){ inst = this; } }
	// Update is called once per frame        public void Update () { }

	public Material GetMat(string _name){
		if(mats != null && mats.Count > 0 && _name != null && _name != ""){
			foreach(Material mat in mats){ if(mat.name == _name){ return mat; } }
		}
		return null;
	}
	public Material GetMat(int _index){
		if(mats != null && mats.Count > 0 && _index >= 0 && _index < mats.Count){
			return mats[_index];
		}
		return null;
	}

	public Texture GetTex(string _name){
		if(mats != null && mats.Count > 0 && _name != null && _name != ""){
			foreach(Material mat in mats){ if(mat.name == _name){ return mat.mainTexture; } }
		}
		return null;
	}
	public Texture GetTex(int _index){
		if(mats != null && mats.Count > 0 && _index >= 0 && _index < mats.Count){
			return mats[_index].mainTexture;
		}
		return null;
	}
}