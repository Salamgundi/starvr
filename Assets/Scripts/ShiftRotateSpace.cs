﻿using UnityEngine;

public class ShiftRotateSpace : MonoBehaviour
{
    public float degrees = 47.6062f;
    public float rotatespeed = 0.25f;
    public bool Paused;
    public bool North;
    public GameObject Stars;
    private SteamVR_TrackedObject trackedObj;
    // 2
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    void Start()
    {
        Paused = true;
    }

    void Update()
    {

       // if (Controller.GetHairTriggerDown())
       if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
        {
            Paused =!Paused;
        }
        if (!Paused)
        {
            Stars.transform.RotateAround(transform.position, transform.up, Time.deltaTime * rotatespeed * 6.0f);
            //transform.localEulerAngles += new Vector3(0, 6.0f * rotatespeed * Time.deltaTime, 0);
        }
    }
}
