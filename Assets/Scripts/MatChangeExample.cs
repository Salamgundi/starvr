﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Renderer))]
public class MatChangeExample : MonoBehaviour {
	public int desiredMat;
	public bool useMat1;
	private Renderer _rend;
	private Material mat0;
	private Material mat1;

	// Use this for initialization
	void Start () {
		_rend = GetComponent<Renderer>();
		mat0 = _rend.material;
		mat1 = MaterialRepo.inst.GetMat( desiredMat );
	}
	// Update is called once per frame
	void Update () {
		if(useMat1 == true && _rend.material != mat1){
			_rend.material = mat1;
		}else if(useMat1 == false && _rend.material != mat0){
			_rend.material = mat0;
		}
	}
}