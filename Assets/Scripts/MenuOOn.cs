using UnityEngine;

public class MenuOOn : MonoBehaviour
{
    public bool MenuOn;
    public GameObject Menu1;
    // 1
    private SteamVR_TrackedObject trackedObj;
    // 2
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }
    void Update()
    {
			if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
			{
					MenuOn = !MenuOn;
					Menu1.SetActive(MenuOn);
					//Debug.Log("Fire!");
			}
	 }
}
