﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TxtOn : MonoBehaviour
{
    public bool TextOn;
    public GameObject Text;
    public GameObject Text0;
    public GameObject Text1;
    public GameObject Text2;
    public GameObject Text3;
    public GameObject Text4;
    public GameObject Text5;
    public GameObject Text6;
    public GameObject Text7;
    public GameObject Text8;
    private SteamVR_TrackedObject trackedObj;
    // 2
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    // Update is called once per frame
    void Update()
    {

        // if (Controller.GetHairTriggerDown())
        if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
        {
            TextOn = !TextOn;
            Text.SetActive(TextOn);
            Text0.SetActive(TextOn);
            Text1.SetActive(TextOn);
            Text2.SetActive(TextOn);
            Text3.SetActive(TextOn);
            Text4.SetActive(TextOn);
            Text5.SetActive(TextOn);
            Text6.SetActive(TextOn);
            Text7.SetActive(TextOn);
            Text8.SetActive(TextOn);
        }
    }
}
