﻿using Gvr;
using UnityEngine;

public class RotateSpace : MonoBehaviour
{
    public float degrees = 47.6062f;
    public float rotatespeed = 0.25f;
    public bool Paused;
    public bool North;

    void Start()
    {
        Paused = true;
    }

    void Update()
    {
            if (GvrViewer.Instance.Triggered)
            {
                Paused = !Paused;
                Debug.Log("Tap!");
            }
            if (!Paused)
            {

                transform.RotateAround(transform.position, transform.up, Time.deltaTime * rotatespeed * 6.0f);
            }
    }
}
