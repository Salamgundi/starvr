using UnityEngine;

public class BioOn : MonoBehaviour {
	public bool TextOn;
	public GameObject Bio;

	
	private SteamVR_TrackedObject trackedObj;
	// 2
	private SteamVR_Controller.Device Controller
	{
		get { return SteamVR_Controller.Input((int)trackedObj.index); }
	}
	void Awake()
	{
		trackedObj = GetComponent<SteamVR_TrackedObject>();
	}

	// Update is called once per frame
	void Update()
	{

		// if (Controller.GetHairTriggerDown())
		if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
		{
			TextOn = !TextOn;
			Bio.SetActive(TextOn);
		}
	}
}
