﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flarez : MonoBehaviour
{
    public GameObject rightEye;
    public GameObject leftEye;

    void Start()
    {
        Invoke("addFlarez", 1f);
    }

    public void addFlarez()
    {
        rightEye = GameObject.Find("Camera (eye) Right");
        rightEye.AddComponent<FlareLayer>();
        leftEye = GameObject.Find("Camera (eye) Left");
        leftEye.AddComponent<FlareLayer>();
    }
}