{ 
print { text " Le vol de Spoutnik 2 et de la chienne Laika \nscript de Jean-Marie Le cospérec" row -3 colunm 3 }

renderflags {set "atmospheres"}  	
renderflags {set "automag"}		
renderflags {set "cloudmaps"}		
renderflags {set "galaxies"}		
renderflags {set "nightmaps"}		
renderflags {set "planets"}		

preloadtex {object"Earth/Spoutnik-2/Spoutnik-2-Etage"}
preloadtex {object"Earth/Spoutnik-2"}
preloadtex {object"Earth/Spoutnik-2/Laika"}
wait {duration 6.0}

time { jd 2436145.60417}
 
set { name "AmbientLightLevel" value 3.6 }
labels { clear "planets|minorplanets|stars|constellations|galaxies|spacecraft" }
renderflags { clear "minorplanets|constellations|galaxies|orbits"}
renderflags { set "atmospheres|planets|moons|minorplanets|cloudmaps|ringshadows" }
unmarkall {}
setvisibilitylimit { magnitude 12.5 }
  
select { object "Earth/Spoutnik-2/Spoutnik-2-Etage" }
print { text " " row -5 }   
follow {}
print { text "Launched on November 3rd 1957 by the R-7 8K71PS launcher, Sputnik 2 is\n carrying on board the first living creature to go into space: the little dog Laika." row -3 colunm 3 }

gotolonglat { time 0 distance 30 up [-1 0 0] longitude 50 latitude 80 }
orbit { axis [ 0 -1 0 ] rate 1.5 duration 4 }
   
changedistance { duration 10.0 rate -0.14 }
center { object "Earth/Spoutnik-2/Spoutnik-2-Etage" }
  
orbit { axis [ 0 -1 0 ] rate 1 duration 14 }
  
changedistance { duration 6.0 rate -0.1 }
orbit { axis [ 0 -1 0 ] rate 5 duration 6 }
print { text " The last stage of the R7 rocket must open\n its panels after the reactor's extinction." row -3 colunm 3 }

changedistance { duration 6.0 rate -0.14 }
wait { duration 3.0 }
changedistance { duration 4.0 rate 0.12 }
print { text " On the ground, engineers find that the last stage has still not dropped." row -2 colunm 1 } 

orbit { axis [ 0 1 0 ] rate 3 duration 17 }

print { text " Laika's heart-beat is much higher than it was during his training." row -2 colunm 1 }

select { object "Earth/Spoutnik-2/Laika" }
set { orientation [ 90 0 1 0 ] }
  
goto { time 8.0 distance 0.6 } #ADDED
wait {duration 4.0}
   
changedistance { duration 3.0 rate -0.1 }
select { object "Earth/Spoutnik-2/Laika" }
center { object "Earth/Spoutnik-2/Laika" }
changedistance { duration 10.0 rate -0.12 }
center { object "Earth/Spoutnik-2/Laika" }
orbit { axis [ 0 1 0 ] rate 3 duration 9 }
  
wait { duration 3.0 }
time { jd 2436145.60868}
  
select { object "Earth/Spoutnik-2" }
follow {}
   
print { text " A few minutes late, the rocket's nose ejects.\n Then, the panels open, setting the Sputnik free." row -3 colunm 3 }

gotolonglat { time 0 distance 10 up [-1 0 0] longitude 50 latitude 80 }
orbit { axis [ 1 1 0 ] rate 2 duration 5 }
orbit { axis [ 0 1 0 ] rate 4 duration 7 }
changedistance { duration 28.0 rate -0.06 }

print { text " This delay caused a thermal regulation system malfunction." row -3 colunm 3 }
wait { duration 3 }
orbit { axis [ 1 1 0 ] rate 2 duration 3 }
print { text " The nose of the satellite is filled with\n measurement and radio instruments and antennas." row -3 colunm 3 }

changedistance { duration 10.0 rate -0.1 }
orbit { axis [ 0 1 1 ] rate 2 duration 5 }
orbit { axis [ 0 1 0 ] rate 2 duration 2 }
wait { duration 3 }
changedistance { duration 17.0 rate 0.1 }
timerate {rate 5.0}

print { text " With the help of its booster, Sputnik 2 slowly moves away." row -3 colunm 3 }
orbit { axis [ -1 0 0 ] rate 1 duration 6 }
wait { duration 14.0 }
timerate {rate 1.0}
time { jd 2436145.62771}
 
select { object "Earth/Spoutnik-2/Sp2-Etage-ecart" }
print { text " " row -5 }   
follow {}
print { text " The last stage is falling..." row -3 colunm 3 }
gotolonglat { time 0 distance 5 up [-1 1 0] longitude 80 latitude 2 }
wait { duration 5 }

changedistance { duration 0.05 rate 0.081 }
select { object "Earth/Spoutnik-2" }
  
print { text " Sputnik 2 keeps on ascending into its orbit and heads towards its apogee." row -3 column 3} 
follow {}
gotolonglat { time 0 distance 7 up [-1 0 0] longitude 60 latitude 80 }
wait { duration 5 }
orbit { axis [ 1 0 0 ] rate 4 duration 8 }

select { object "Earth/Spoutnik-2/Laika" }
center { object "Earth/Spoutnik-2/Laika" }
print { text " Laika's heart-beat is now normal, and the dog seems to appreciate the show." row -2 colunm 1 }
changedistance { duration 10.0 rate -0.11 }
wait { duration 5 }

print { text " Instruments are started and emit radio waves received by numerous stations on the ground." row -3 colunm 3 }
changedistance { duration 6.0 rate -0.05 }
follow {}
orbit { axis [ 0 -1 0 ] rate 2 duration 10 }

print { text " Sputnik 2 reaches apogee at an altitude of more than 1608\n kilometers. At this position, its speed is at its slowest." row -3 colunm 3 }
orbit { axis [ 0 -1 0 ] rate 2 duration 10 }
wait { duration 1 }
select { object "Earth" }
wait { duration 16 }
time { jd 2436145.74167}

select { object "Earth/Spoutnik-2/Laika" }
goto { time 0 distance 17 upframe "equatorial" up [ 1 0 0 ] }
lock {}
print { text " Sputnik 2 is now at its perigee, and its speed is at its fastest.\n The dog, Laika, feels much better now and has already eaten his first meal." row -3 colunm 3 }
follow {}
orbit { axis [ 0 1 1 ] rate 3 duration 24.2 }
wait { duration 4 }
select { object "Earth/Spoutnik-2/Laika" }
center { object "Earth/Spoutnik-2/Laika" }
changedistance { duration 5.0 rate 0.005 }
select { object "Earth" }
wait { duration 2 }
time { jd 2436145.74931}
 
select { object "Earth/Spoutnik-2/Laika" }
print { text " During its third orbit, it flies above Spain." row -5 colunm 1 }
gotolonglat { time 0 distance 20 up [1 0 0] longitude -50 latitude -49 }
orbit { axis [ -1 0 0 ] rate 1 duration 6 }
follow {}
  
gotolonglat { time 10 distance 4 up [-1 0 0] longitude 50 latitude 80 }
print { text " The cabin starts to heat up, since the thermal regulation system\n is not set up well. Laika begins to stress and to dehydrate." row -3 colunm 3 }
wait { duration 15.0 }
changedistance { duration 5.0 rate +0.05 }
time { jd 2436145.82431}
select { object "Earth/Spoutnik-2/Laika" }
 
gotolonglat { time 0 distance 50 up [-1 0 0] longitude 50 latitude 80 }
print { text " During Sputnik 2's fifth orbit, Laika's heart stops beating. The\n little dog could not resist the intense heat inside the cabin." row -3 colunm 3 }
orbit { axis [ 1 -1 0 ] rate 1 duration 24 }
print { text " On Earth, it is now time for deception, even if everybody knew Laika would not make it anyway." row -3 colunm 3 }

orbit { axis [ 1 -1 0 ] rate 0.5 duration 24 }
print { text " Sputnik 2 will keep on going on its mission\n and will detect radiation from Van Halen's belt." row -3 colunm 3 }
orbit { axis [ 1 -1 0 ] rate 0.5 duration 24 }

print { text " Sputnik 2 will keep on travelling during 162 days. It will be consumed during\n its entrance into the atmosphere after having completed 2570 orbits around the Earth." row -5 colunm 1 }
wait { duration 10}
wait { duration 3.0 }
}