3DS Mesh                                  11 Aug 03

Imperial TIE Interceptor     (Featured in the STAR WARS
       with pilot             trilogy by LucasFilm Ltd.)


Manufacturer: Sienar Fleet Systems (SFS)
Weapons: Four SFS L-s9.3 laser cannons
Crew: One pilot
Speed: 125 MGLT, not hyperdrive equipped.

Hello, I have attempted to design a realistic TIE interior
and pilot with fans of the Star Wars films, games, and
plastic model kits in mind. The model is original except
for parts of the pilot's body, which is converted from some
Zygote figures I purchased.

The hatches can be posed open. The "Top Hatch latch cover"
(HuH175TH)*, is designed to open before the hatch raises or
shuts then close to cover it's latch well. I guess you could
use it as a hinge if you believe the Top Hatch pivots at the
rear.

-----------------------
* HuH175TH = Hu-H-175-TH

Hu=Hull (prefix), T_=transparent as glass (opacity).
H=hatch latch cover (part name)
175=color (number (in gray scale)), Cri=crimson, DrG=dark green, etc.
TH=Top Hatch (suffix), BH=Back Hatch, US=Unsmoothed -VERY IMPORTANT!!
If the part does not have a "_US" or "US" as a suffix it means that
part is smoothed to a 45-89 degree angle. Not all parts have suffixs.
-----------------------

I regret that the eight-character-type hierarchy (parts
list) is not as clear as it could be. Some programs
truncate the instance names to eight characters. I also
do this for maximum 3ds format compatibility. I hope
the above key helps. I will try to answer your email
questions or critiques.

The instrument panels have separate back panels and front
"glass" so you may remove the gizmo's I came up with and
apply your own with textures and reflection maps.

I hope you find the model interesting. Jim


Faces: 175930, in 3.77 Mbytes.
Textures: none
Tools: SoftF/X Pro 4.1, Nugraf 2.2j, Paint Shop Pro 5.

by James R. Bassett
http://www.jrbassett.com/
jim@jrbassett.com
