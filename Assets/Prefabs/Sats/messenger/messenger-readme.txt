This file incorporates Cham's MESSENGER model (messenger2.3ds) and its associated 
textures.
Unzip this file into your extras directory, and it will create an 
add-on consisting of Cham's model, a messenger.xyz trajectory file, and an 
accompanying messenger.ssc. The trajectory was taken from JPL Horizons in 
January 2008. It begins at 2004 Aug 03 07:15, and ends at 2012 Mar 31 23:15.

Grant Hutchison, January 2008