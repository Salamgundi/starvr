This one is based on addon by FarGetaNik. Updated to higher resolution and fidelity images.

Features:
- 8k Pluto texture, 4k Charon texture
- sampled orbit for the pluto system running from 1900-2199 (ssc-orbit outside this timeframe)
  high sampling rate during New Horizons encounter
- updated physical and orbital parameters for Pluto-System
- moons Kerberos and Styx

Installation: Unzip into your Celestia/extras directory

Sources:
Pluto texture: 	NASA/JPL/New Horizons, Created by Snowfall-The-Cat (not me): http://snowfall-the-cat.deviantart.com/
Charon texture: NASA/JPL/New Horizons, http://laps.noaa.gov/cgi/albers.homepage.cgi
Orbital data: 	NASA's Horizons System, http://ssd.jpl.nasa.gov/horizons.cgi

Textures orientation preserved according to FarGetaNik's version.


TonnyBGood