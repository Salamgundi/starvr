1974-089B AKA AMSAT Oscar 7

Celestia Add-on Readme file.
=============================
Modeler: Jesse LaFleur
-----------------------------
SSC File Maker: "TERRIER" using NORAD data converted
TLE to SSC with use of Grant Hutchison's Excel converter
-----------------------------

To be used with: Celestia, the space simulator,
http://www.shatters.net/celestia

NOTE: This add-on re-packaged by Bob Hegwood on 8 Apr 2009.

FAQ
Q: Before I ask about it, how do I use it?
A: Extract the entire zip file into your extras folder.
Q: What is it?
A: This is the Amateur Satellite Oscar 7.

Q: Again, What is it?
A: An organization called AMSAT is a bunch of guys who launch sateliites
for their love of Amateur Radio. This satellite is the 7th in a series
of satellites that AMSAT has launched.

Q: What makes this one so special?
A: The fact that its still working! It was launched in 1974 with all respects
it should be dead by now and technically it was for 21 years. In 2002, for
some reason it just turned back on again, the Satellite now only works
when Sol hits its solar panels as its batteries are long dead.

Q: So why should I care?
A: Since its the oldest satellite in operation I felt that it was
special enough to make a model for it. I think it deserves some of
our respect just for lasting so long.

Q: Can you make [this satellite]
A: Maybe? I'm really busy and I'd rather not take requests
because then I'd feel obligated to make it. Besides, I'm not very good
yet and I'm sticking to more simple ones for now.

END FAQ

Please send your comments, suggestions to me, Jess LaFleur
pokechow@sympatico.ca [this email is subject to change]

=================================================================
 Pok�mon news, information, online games, helpful hints and more
-----------------------------------------------------------------
.....(-o-)   http://www.planetnintendo.com/pokechow   (-o-)......
-----------------------------------------------------------------
 Updated often, fun stuff, new stuff, great stuff, come & visit!
=================================================================
