This addon shows the Soviet's Sputnik-1, launched in 1957.
It was the first artificial Earth satellite to go to the space.
Sputnik-1 was in orbit only from 04 Oct 1957 - 04 Jan 1958, so you'll have to run time back to see it.
I made the model using an old and very crude picture of the satellite.
To use the addon, just drop this folder in your Celestia directory.
Arvydas Cetyrkovskis, 01 May 2005, Lithuania