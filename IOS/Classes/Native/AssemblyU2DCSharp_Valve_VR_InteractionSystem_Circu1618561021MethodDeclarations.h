﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0
struct U3CHapticPulsesU3Ec__Iterator0_t1618561021;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::.ctor()
extern "C"  void U3CHapticPulsesU3Ec__Iterator0__ctor_m557352852 (U3CHapticPulsesU3Ec__Iterator0_t1618561021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::MoveNext()
extern "C"  bool U3CHapticPulsesU3Ec__Iterator0_MoveNext_m3259865072 (U3CHapticPulsesU3Ec__Iterator0_t1618561021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CHapticPulsesU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3980625358 (U3CHapticPulsesU3Ec__Iterator0_t1618561021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CHapticPulsesU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m286329318 (U3CHapticPulsesU3Ec__Iterator0_t1618561021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::Dispose()
extern "C"  void U3CHapticPulsesU3Ec__Iterator0_Dispose_m2806030543 (U3CHapticPulsesU3Ec__Iterator0_t1618561021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::Reset()
extern "C"  void U3CHapticPulsesU3Ec__Iterator0_Reset_m1276531009 (U3CHapticPulsesU3Ec__Iterator0_t1618561021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
