﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_UIPointer
struct VRTK_UIPointer_t2714926455;
// VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t2013676129;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents
struct  VRTK_UIPointer_UnityEvents_t980224384  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_UIPointer VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::uip
	VRTK_UIPointer_t2714926455 * ___uip_2;
	// VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::OnUIPointerElementEnter
	UnityObjectEvent_t2013676129 * ___OnUIPointerElementEnter_3;
	// VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::OnUIPointerElementExit
	UnityObjectEvent_t2013676129 * ___OnUIPointerElementExit_4;
	// VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::OnUIPointerElementClick
	UnityObjectEvent_t2013676129 * ___OnUIPointerElementClick_5;
	// VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::OnUIPointerElementDragStart
	UnityObjectEvent_t2013676129 * ___OnUIPointerElementDragStart_6;
	// VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::OnUIPointerElementDragEnd
	UnityObjectEvent_t2013676129 * ___OnUIPointerElementDragEnd_7;

public:
	inline static int32_t get_offset_of_uip_2() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_UnityEvents_t980224384, ___uip_2)); }
	inline VRTK_UIPointer_t2714926455 * get_uip_2() const { return ___uip_2; }
	inline VRTK_UIPointer_t2714926455 ** get_address_of_uip_2() { return &___uip_2; }
	inline void set_uip_2(VRTK_UIPointer_t2714926455 * value)
	{
		___uip_2 = value;
		Il2CppCodeGenWriteBarrier(&___uip_2, value);
	}

	inline static int32_t get_offset_of_OnUIPointerElementEnter_3() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_UnityEvents_t980224384, ___OnUIPointerElementEnter_3)); }
	inline UnityObjectEvent_t2013676129 * get_OnUIPointerElementEnter_3() const { return ___OnUIPointerElementEnter_3; }
	inline UnityObjectEvent_t2013676129 ** get_address_of_OnUIPointerElementEnter_3() { return &___OnUIPointerElementEnter_3; }
	inline void set_OnUIPointerElementEnter_3(UnityObjectEvent_t2013676129 * value)
	{
		___OnUIPointerElementEnter_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnUIPointerElementEnter_3, value);
	}

	inline static int32_t get_offset_of_OnUIPointerElementExit_4() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_UnityEvents_t980224384, ___OnUIPointerElementExit_4)); }
	inline UnityObjectEvent_t2013676129 * get_OnUIPointerElementExit_4() const { return ___OnUIPointerElementExit_4; }
	inline UnityObjectEvent_t2013676129 ** get_address_of_OnUIPointerElementExit_4() { return &___OnUIPointerElementExit_4; }
	inline void set_OnUIPointerElementExit_4(UnityObjectEvent_t2013676129 * value)
	{
		___OnUIPointerElementExit_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnUIPointerElementExit_4, value);
	}

	inline static int32_t get_offset_of_OnUIPointerElementClick_5() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_UnityEvents_t980224384, ___OnUIPointerElementClick_5)); }
	inline UnityObjectEvent_t2013676129 * get_OnUIPointerElementClick_5() const { return ___OnUIPointerElementClick_5; }
	inline UnityObjectEvent_t2013676129 ** get_address_of_OnUIPointerElementClick_5() { return &___OnUIPointerElementClick_5; }
	inline void set_OnUIPointerElementClick_5(UnityObjectEvent_t2013676129 * value)
	{
		___OnUIPointerElementClick_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnUIPointerElementClick_5, value);
	}

	inline static int32_t get_offset_of_OnUIPointerElementDragStart_6() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_UnityEvents_t980224384, ___OnUIPointerElementDragStart_6)); }
	inline UnityObjectEvent_t2013676129 * get_OnUIPointerElementDragStart_6() const { return ___OnUIPointerElementDragStart_6; }
	inline UnityObjectEvent_t2013676129 ** get_address_of_OnUIPointerElementDragStart_6() { return &___OnUIPointerElementDragStart_6; }
	inline void set_OnUIPointerElementDragStart_6(UnityObjectEvent_t2013676129 * value)
	{
		___OnUIPointerElementDragStart_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnUIPointerElementDragStart_6, value);
	}

	inline static int32_t get_offset_of_OnUIPointerElementDragEnd_7() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_UnityEvents_t980224384, ___OnUIPointerElementDragEnd_7)); }
	inline UnityObjectEvent_t2013676129 * get_OnUIPointerElementDragEnd_7() const { return ___OnUIPointerElementDragEnd_7; }
	inline UnityObjectEvent_t2013676129 ** get_address_of_OnUIPointerElementDragEnd_7() { return &___OnUIPointerElementDragEnd_7; }
	inline void set_OnUIPointerElementDragEnd_7(UnityObjectEvent_t2013676129 * value)
	{
		___OnUIPointerElementDragEnd_7 = value;
		Il2CppCodeGenWriteBarrier(&___OnUIPointerElementDragEnd_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
