﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayFlags
struct _GetOverlayFlags_t2891663584;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayFlags::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayFlags__ctor_m844230455 (_GetOverlayFlags_t2891663584 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayFlags::Invoke(System.UInt64,System.UInt32&)
extern "C"  int32_t _GetOverlayFlags_Invoke_m2109145672 (_GetOverlayFlags_t2891663584 * __this, uint64_t ___ulOverlayHandle0, uint32_t* ___pFlags1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayFlags::BeginInvoke(System.UInt64,System.UInt32&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayFlags_BeginInvoke_m2616479101 (_GetOverlayFlags_t2891663584 * __this, uint64_t ___ulOverlayHandle0, uint32_t* ___pFlags1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayFlags::EndInvoke(System.UInt32&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayFlags_EndInvoke_m4245396667 (_GetOverlayFlags_t2891663584 * __this, uint32_t* ___pFlags0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
