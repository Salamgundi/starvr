﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// VRTK.VRTK_CurveGenerator/BezierControlPointMode[]
struct BezierControlPointModeU5BU5D_t2233195126;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_CurveGenerator
struct  VRTK_CurveGenerator_t3769661606  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3[] VRTK.VRTK_CurveGenerator::points
	Vector3U5BU5D_t1172311765* ___points_2;
	// UnityEngine.GameObject[] VRTK.VRTK_CurveGenerator::items
	GameObjectU5BU5D_t3057952154* ___items_3;
	// VRTK.VRTK_CurveGenerator/BezierControlPointMode[] VRTK.VRTK_CurveGenerator::modes
	BezierControlPointModeU5BU5D_t2233195126* ___modes_4;
	// System.Boolean VRTK.VRTK_CurveGenerator::loop
	bool ___loop_5;
	// System.Int32 VRTK.VRTK_CurveGenerator::frequency
	int32_t ___frequency_6;
	// System.Boolean VRTK.VRTK_CurveGenerator::customTracer
	bool ___customTracer_7;
	// System.Boolean VRTK.VRTK_CurveGenerator::rescalePointerTracer
	bool ___rescalePointerTracer_8;

public:
	inline static int32_t get_offset_of_points_2() { return static_cast<int32_t>(offsetof(VRTK_CurveGenerator_t3769661606, ___points_2)); }
	inline Vector3U5BU5D_t1172311765* get_points_2() const { return ___points_2; }
	inline Vector3U5BU5D_t1172311765** get_address_of_points_2() { return &___points_2; }
	inline void set_points_2(Vector3U5BU5D_t1172311765* value)
	{
		___points_2 = value;
		Il2CppCodeGenWriteBarrier(&___points_2, value);
	}

	inline static int32_t get_offset_of_items_3() { return static_cast<int32_t>(offsetof(VRTK_CurveGenerator_t3769661606, ___items_3)); }
	inline GameObjectU5BU5D_t3057952154* get_items_3() const { return ___items_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_items_3() { return &___items_3; }
	inline void set_items_3(GameObjectU5BU5D_t3057952154* value)
	{
		___items_3 = value;
		Il2CppCodeGenWriteBarrier(&___items_3, value);
	}

	inline static int32_t get_offset_of_modes_4() { return static_cast<int32_t>(offsetof(VRTK_CurveGenerator_t3769661606, ___modes_4)); }
	inline BezierControlPointModeU5BU5D_t2233195126* get_modes_4() const { return ___modes_4; }
	inline BezierControlPointModeU5BU5D_t2233195126** get_address_of_modes_4() { return &___modes_4; }
	inline void set_modes_4(BezierControlPointModeU5BU5D_t2233195126* value)
	{
		___modes_4 = value;
		Il2CppCodeGenWriteBarrier(&___modes_4, value);
	}

	inline static int32_t get_offset_of_loop_5() { return static_cast<int32_t>(offsetof(VRTK_CurveGenerator_t3769661606, ___loop_5)); }
	inline bool get_loop_5() const { return ___loop_5; }
	inline bool* get_address_of_loop_5() { return &___loop_5; }
	inline void set_loop_5(bool value)
	{
		___loop_5 = value;
	}

	inline static int32_t get_offset_of_frequency_6() { return static_cast<int32_t>(offsetof(VRTK_CurveGenerator_t3769661606, ___frequency_6)); }
	inline int32_t get_frequency_6() const { return ___frequency_6; }
	inline int32_t* get_address_of_frequency_6() { return &___frequency_6; }
	inline void set_frequency_6(int32_t value)
	{
		___frequency_6 = value;
	}

	inline static int32_t get_offset_of_customTracer_7() { return static_cast<int32_t>(offsetof(VRTK_CurveGenerator_t3769661606, ___customTracer_7)); }
	inline bool get_customTracer_7() const { return ___customTracer_7; }
	inline bool* get_address_of_customTracer_7() { return &___customTracer_7; }
	inline void set_customTracer_7(bool value)
	{
		___customTracer_7 = value;
	}

	inline static int32_t get_offset_of_rescalePointerTracer_8() { return static_cast<int32_t>(offsetof(VRTK_CurveGenerator_t3769661606, ___rescalePointerTracer_8)); }
	inline bool get_rescalePointerTracer_8() const { return ___rescalePointerTracer_8; }
	inline bool* get_address_of_rescalePointerTracer_8() { return &___rescalePointerTracer_8; }
	inline void set_rescalePointerTracer_8(bool value)
	{
		___rescalePointerTracer_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
