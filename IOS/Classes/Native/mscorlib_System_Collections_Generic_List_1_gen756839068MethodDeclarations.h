﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct List_1_t756839068;
// System.Collections.Generic.IEnumerable`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct IEnumerable_1_t1679844981;
// Valve.VR.InteractionSystem.Hand/AttachedObject[]
struct AttachedObjectU5BU5D_t2422108177;
// System.Collections.Generic.IEnumerator`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct IEnumerator_1_t3158209059;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct ICollection_1_t2339793241;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct ReadOnlyCollection_1_t1573503628;
// System.Predicate`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct Predicate_1_t4125655347;
// System.Comparison`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct Comparison_1_t2649456787;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1387717936.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat291568742.h"

// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::.ctor()
extern "C"  void List_1__ctor_m1739352175_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1__ctor_m1739352175(__this, method) ((  void (*) (List_1_t756839068 *, const MethodInfo*))List_1__ctor_m1739352175_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2197077375_gshared (List_1_t756839068 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m2197077375(__this, ___collection0, method) ((  void (*) (List_1_t756839068 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2197077375_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m419176393_gshared (List_1_t756839068 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m419176393(__this, ___capacity0, method) ((  void (*) (List_1_t756839068 *, int32_t, const MethodInfo*))List_1__ctor_m419176393_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m2006968123_gshared (List_1_t756839068 * __this, AttachedObjectU5BU5D_t2422108177* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m2006968123(__this, ___data0, ___size1, method) ((  void (*) (List_1_t756839068 *, AttachedObjectU5BU5D_t2422108177*, int32_t, const MethodInfo*))List_1__ctor_m2006968123_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::.cctor()
extern "C"  void List_1__cctor_m2370232983_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2370232983(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2370232983_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m249884368_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m249884368(__this, method) ((  Il2CppObject* (*) (List_1_t756839068 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m249884368_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1559022170_gshared (List_1_t756839068 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1559022170(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t756839068 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1559022170_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m216517205_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m216517205(__this, method) ((  Il2CppObject * (*) (List_1_t756839068 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m216517205_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4254368902_gshared (List_1_t756839068 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m4254368902(__this, ___item0, method) ((  int32_t (*) (List_1_t756839068 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m4254368902_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m629715140_gshared (List_1_t756839068 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m629715140(__this, ___item0, method) ((  bool (*) (List_1_t756839068 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m629715140_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m4082344796_gshared (List_1_t756839068 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m4082344796(__this, ___item0, method) ((  int32_t (*) (List_1_t756839068 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m4082344796_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m975943841_gshared (List_1_t756839068 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m975943841(__this, ___index0, ___item1, method) ((  void (*) (List_1_t756839068 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m975943841_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1660390489_gshared (List_1_t756839068 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1660390489(__this, ___item0, method) ((  void (*) (List_1_t756839068 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1660390489_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3229162429_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3229162429(__this, method) ((  bool (*) (List_1_t756839068 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3229162429_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2754390306_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2754390306(__this, method) ((  bool (*) (List_1_t756839068 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2754390306_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3590332756_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3590332756(__this, method) ((  Il2CppObject * (*) (List_1_t756839068 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3590332756_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1609347485_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1609347485(__this, method) ((  bool (*) (List_1_t756839068 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1609347485_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3062197566_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3062197566(__this, method) ((  bool (*) (List_1_t756839068 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3062197566_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m604023057_gshared (List_1_t756839068 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m604023057(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t756839068 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m604023057_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1818119624_gshared (List_1_t756839068 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1818119624(__this, ___index0, ___value1, method) ((  void (*) (List_1_t756839068 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1818119624_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Add(T)
extern "C"  void List_1_Add_m3603267939_gshared (List_1_t756839068 * __this, AttachedObject_t1387717936  ___item0, const MethodInfo* method);
#define List_1_Add_m3603267939(__this, ___item0, method) ((  void (*) (List_1_t756839068 *, AttachedObject_t1387717936 , const MethodInfo*))List_1_Add_m3603267939_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m844896312_gshared (List_1_t756839068 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m844896312(__this, ___newCount0, method) ((  void (*) (List_1_t756839068 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m844896312_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m480291545_gshared (List_1_t756839068 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m480291545(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t756839068 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m480291545_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3943848328_gshared (List_1_t756839068 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3943848328(__this, ___collection0, method) ((  void (*) (List_1_t756839068 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3943848328_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2878457288_gshared (List_1_t756839068 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2878457288(__this, ___enumerable0, method) ((  void (*) (List_1_t756839068 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2878457288_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3579323297_gshared (List_1_t756839068 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3579323297(__this, ___collection0, method) ((  void (*) (List_1_t756839068 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3579323297_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1573503628 * List_1_AsReadOnly_m1857450036_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1857450036(__this, method) ((  ReadOnlyCollection_1_t1573503628 * (*) (List_1_t756839068 *, const MethodInfo*))List_1_AsReadOnly_m1857450036_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Clear()
extern "C"  void List_1_Clear_m1819639921_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_Clear_m1819639921(__this, method) ((  void (*) (List_1_t756839068 *, const MethodInfo*))List_1_Clear_m1819639921_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Contains(T)
extern "C"  bool List_1_Contains_m2629455143_gshared (List_1_t756839068 * __this, AttachedObject_t1387717936  ___item0, const MethodInfo* method);
#define List_1_Contains_m2629455143(__this, ___item0, method) ((  bool (*) (List_1_t756839068 *, AttachedObject_t1387717936 , const MethodInfo*))List_1_Contains_m2629455143_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1950076693_gshared (List_1_t756839068 * __this, AttachedObjectU5BU5D_t2422108177* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1950076693(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t756839068 *, AttachedObjectU5BU5D_t2422108177*, int32_t, const MethodInfo*))List_1_CopyTo_m1950076693_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Find(System.Predicate`1<T>)
extern "C"  AttachedObject_t1387717936  List_1_Find_m594685183_gshared (List_1_t756839068 * __this, Predicate_1_t4125655347 * ___match0, const MethodInfo* method);
#define List_1_Find_m594685183(__this, ___match0, method) ((  AttachedObject_t1387717936  (*) (List_1_t756839068 *, Predicate_1_t4125655347 *, const MethodInfo*))List_1_Find_m594685183_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3652680290_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t4125655347 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3652680290(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t4125655347 *, const MethodInfo*))List_1_CheckMatch_m3652680290_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t756839068 * List_1_FindAll_m1121194338_gshared (List_1_t756839068 * __this, Predicate_1_t4125655347 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m1121194338(__this, ___match0, method) ((  List_1_t756839068 * (*) (List_1_t756839068 *, Predicate_1_t4125655347 *, const MethodInfo*))List_1_FindAll_m1121194338_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t756839068 * List_1_FindAllStackBits_m2352787714_gshared (List_1_t756839068 * __this, Predicate_1_t4125655347 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m2352787714(__this, ___match0, method) ((  List_1_t756839068 * (*) (List_1_t756839068 *, Predicate_1_t4125655347 *, const MethodInfo*))List_1_FindAllStackBits_m2352787714_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t756839068 * List_1_FindAllList_m2218512930_gshared (List_1_t756839068 * __this, Predicate_1_t4125655347 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m2218512930(__this, ___match0, method) ((  List_1_t756839068 * (*) (List_1_t756839068 *, Predicate_1_t4125655347 *, const MethodInfo*))List_1_FindAllList_m2218512930_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m1173170556_gshared (List_1_t756839068 * __this, Predicate_1_t4125655347 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m1173170556(__this, ___match0, method) ((  int32_t (*) (List_1_t756839068 *, Predicate_1_t4125655347 *, const MethodInfo*))List_1_FindIndex_m1173170556_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1794009671_gshared (List_1_t756839068 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t4125655347 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1794009671(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t756839068 *, int32_t, int32_t, Predicate_1_t4125655347 *, const MethodInfo*))List_1_GetIndex_m1794009671_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::GetEnumerator()
extern "C"  Enumerator_t291568742  List_1_GetEnumerator_m3765577286_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3765577286(__this, method) ((  Enumerator_t291568742  (*) (List_1_t756839068 *, const MethodInfo*))List_1_GetEnumerator_m3765577286_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3643585613_gshared (List_1_t756839068 * __this, AttachedObject_t1387717936  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3643585613(__this, ___item0, method) ((  int32_t (*) (List_1_t756839068 *, AttachedObject_t1387717936 , const MethodInfo*))List_1_IndexOf_m3643585613_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3434025142_gshared (List_1_t756839068 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3434025142(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t756839068 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3434025142_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2222848617_gshared (List_1_t756839068 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2222848617(__this, ___index0, method) ((  void (*) (List_1_t756839068 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2222848617_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m889230856_gshared (List_1_t756839068 * __this, int32_t ___index0, AttachedObject_t1387717936  ___item1, const MethodInfo* method);
#define List_1_Insert_m889230856(__this, ___index0, ___item1, method) ((  void (*) (List_1_t756839068 *, int32_t, AttachedObject_t1387717936 , const MethodInfo*))List_1_Insert_m889230856_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3304734459_gshared (List_1_t756839068 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3304734459(__this, ___collection0, method) ((  void (*) (List_1_t756839068 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3304734459_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Remove(T)
extern "C"  bool List_1_Remove_m3680873990_gshared (List_1_t756839068 * __this, AttachedObject_t1387717936  ___item0, const MethodInfo* method);
#define List_1_Remove_m3680873990(__this, ___item0, method) ((  bool (*) (List_1_t756839068 *, AttachedObject_t1387717936 , const MethodInfo*))List_1_Remove_m3680873990_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3158203970_gshared (List_1_t756839068 * __this, Predicate_1_t4125655347 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3158203970(__this, ___match0, method) ((  int32_t (*) (List_1_t756839068 *, Predicate_1_t4125655347 *, const MethodInfo*))List_1_RemoveAll_m3158203970_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m4000557985_gshared (List_1_t756839068 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m4000557985(__this, ___index0, method) ((  void (*) (List_1_t756839068 *, int32_t, const MethodInfo*))List_1_RemoveAt_m4000557985_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m3845037725_gshared (List_1_t756839068 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m3845037725(__this, ___index0, ___count1, method) ((  void (*) (List_1_t756839068 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3845037725_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Reverse()
extern "C"  void List_1_Reverse_m2339765052_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_Reverse_m2339765052(__this, method) ((  void (*) (List_1_t756839068 *, const MethodInfo*))List_1_Reverse_m2339765052_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Sort()
extern "C"  void List_1_Sort_m2830931644_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_Sort_m2830931644(__this, method) ((  void (*) (List_1_t756839068 *, const MethodInfo*))List_1_Sort_m2830931644_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1044254223_gshared (List_1_t756839068 * __this, Comparison_1_t2649456787 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1044254223(__this, ___comparison0, method) ((  void (*) (List_1_t756839068 *, Comparison_1_t2649456787 *, const MethodInfo*))List_1_Sort_m1044254223_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::ToArray()
extern "C"  AttachedObjectU5BU5D_t2422108177* List_1_ToArray_m3935884075_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_ToArray_m3935884075(__this, method) ((  AttachedObjectU5BU5D_t2422108177* (*) (List_1_t756839068 *, const MethodInfo*))List_1_ToArray_m3935884075_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::TrimExcess()
extern "C"  void List_1_TrimExcess_m4293108149_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m4293108149(__this, method) ((  void (*) (List_1_t756839068 *, const MethodInfo*))List_1_TrimExcess_m4293108149_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2542939855_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2542939855(__this, method) ((  int32_t (*) (List_1_t756839068 *, const MethodInfo*))List_1_get_Capacity_m2542939855_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m383404808_gshared (List_1_t756839068 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m383404808(__this, ___value0, method) ((  void (*) (List_1_t756839068 *, int32_t, const MethodInfo*))List_1_set_Capacity_m383404808_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::get_Count()
extern "C"  int32_t List_1_get_Count_m2569937583_gshared (List_1_t756839068 * __this, const MethodInfo* method);
#define List_1_get_Count_m2569937583(__this, method) ((  int32_t (*) (List_1_t756839068 *, const MethodInfo*))List_1_get_Count_m2569937583_gshared)(__this, method)
// T System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::get_Item(System.Int32)
extern "C"  AttachedObject_t1387717936  List_1_get_Item_m4083785192_gshared (List_1_t756839068 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m4083785192(__this, ___index0, method) ((  AttachedObject_t1387717936  (*) (List_1_t756839068 *, int32_t, const MethodInfo*))List_1_get_Item_m4083785192_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1892696789_gshared (List_1_t756839068 * __this, int32_t ___index0, AttachedObject_t1387717936  ___value1, const MethodInfo* method);
#define List_1_set_Item_m1892696789(__this, ___index0, ___value1, method) ((  void (*) (List_1_t756839068 *, int32_t, AttachedObject_t1387717936 , const MethodInfo*))List_1_set_Item_m1892696789_gshared)(__this, ___index0, ___value1, method)
