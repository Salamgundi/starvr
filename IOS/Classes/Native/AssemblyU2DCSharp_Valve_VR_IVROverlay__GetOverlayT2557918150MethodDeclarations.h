﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayTransformAbsolute
struct _GetOverlayTransformAbsolute_t2557918150;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingUniverseOrigin1464400093.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayTransformAbsolute::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayTransformAbsolute__ctor_m466079631 (_GetOverlayTransformAbsolute_t2557918150 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTransformAbsolute::Invoke(System.UInt64,Valve.VR.ETrackingUniverseOrigin&,Valve.VR.HmdMatrix34_t&)
extern "C"  int32_t _GetOverlayTransformAbsolute_Invoke_m137783593 (_GetOverlayTransformAbsolute_t2557918150 * __this, uint64_t ___ulOverlayHandle0, int32_t* ___peTrackingOrigin1, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToOverlayTransform2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayTransformAbsolute::BeginInvoke(System.UInt64,Valve.VR.ETrackingUniverseOrigin&,Valve.VR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayTransformAbsolute_BeginInvoke_m873505986 (_GetOverlayTransformAbsolute_t2557918150 * __this, uint64_t ___ulOverlayHandle0, int32_t* ___peTrackingOrigin1, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToOverlayTransform2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTransformAbsolute::EndInvoke(Valve.VR.ETrackingUniverseOrigin&,Valve.VR.HmdMatrix34_t&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayTransformAbsolute_EndInvoke_m3308652276 (_GetOverlayTransformAbsolute_t2557918150 * __this, int32_t* ___peTrackingOrigin0, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToOverlayTransform1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
