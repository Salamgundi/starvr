﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_Control/ValueChangedEvent
struct ValueChangedEvent_t4219324264;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_Control/ValueChangedEvent::.ctor()
extern "C"  void ValueChangedEvent__ctor_m1923025443 (ValueChangedEvent_t4219324264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
