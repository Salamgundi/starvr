﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_TransformFollow
struct VRTK_TransformFollow_t3532748285;
// UnityEngine.Camera
struct Camera_t189460977;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// System.Void VRTK.VRTK_TransformFollow::.ctor()
extern "C"  void VRTK_TransformFollow__ctor_m3353458671 (VRTK_TransformFollow_t3532748285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TransformFollow::OnEnable()
extern "C"  void VRTK_TransformFollow_OnEnable_m4197672875 (VRTK_TransformFollow_t3532748285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TransformFollow::OnDisable()
extern "C"  void VRTK_TransformFollow_OnDisable_m345488282 (VRTK_TransformFollow_t3532748285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TransformFollow::Update()
extern "C"  void VRTK_TransformFollow_Update_m367189140 (VRTK_TransformFollow_t3532748285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TransformFollow::LateUpdate()
extern "C"  void VRTK_TransformFollow_LateUpdate_m546395136 (VRTK_TransformFollow_t3532748285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TransformFollow::OnCamPreRender(UnityEngine.Camera)
extern "C"  void VRTK_TransformFollow_OnCamPreRender_m4014787152 (VRTK_TransformFollow_t3532748285 * __this, Camera_t189460977 * ___cam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_TransformFollow::GetPositionToFollow()
extern "C"  Vector3_t2243707580  VRTK_TransformFollow_GetPositionToFollow_m33675576 (VRTK_TransformFollow_t3532748285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TransformFollow::SetPositionOnGameObject(UnityEngine.Vector3)
extern "C"  void VRTK_TransformFollow_SetPositionOnGameObject_m398158879 (VRTK_TransformFollow_t3532748285 * __this, Vector3_t2243707580  ___newPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion VRTK.VRTK_TransformFollow::GetRotationToFollow()
extern "C"  Quaternion_t4030073918  VRTK_TransformFollow_GetRotationToFollow_m1588216399 (VRTK_TransformFollow_t3532748285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TransformFollow::SetRotationOnGameObject(UnityEngine.Quaternion)
extern "C"  void VRTK_TransformFollow_SetRotationOnGameObject_m606028722 (VRTK_TransformFollow_t3532748285 * __this, Quaternion_t4030073918  ___newRotation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
