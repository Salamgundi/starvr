﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRExtendedDisplay/_GetEyeOutputViewport
struct _GetEyeOutputViewport_t693377854;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRExtendedDisplay/_GetEyeOutputViewport::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetEyeOutputViewport__ctor_m1740074743 (_GetEyeOutputViewport_t693377854 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRExtendedDisplay/_GetEyeOutputViewport::Invoke(Valve.VR.EVREye,System.UInt32&,System.UInt32&,System.UInt32&,System.UInt32&)
extern "C"  void _GetEyeOutputViewport_Invoke_m2431447655 (_GetEyeOutputViewport_t693377854 * __this, int32_t ___eEye0, uint32_t* ___pnX1, uint32_t* ___pnY2, uint32_t* ___pnWidth3, uint32_t* ___pnHeight4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRExtendedDisplay/_GetEyeOutputViewport::BeginInvoke(Valve.VR.EVREye,System.UInt32&,System.UInt32&,System.UInt32&,System.UInt32&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetEyeOutputViewport_BeginInvoke_m2937157502 (_GetEyeOutputViewport_t693377854 * __this, int32_t ___eEye0, uint32_t* ___pnX1, uint32_t* ___pnY2, uint32_t* ___pnWidth3, uint32_t* ___pnHeight4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRExtendedDisplay/_GetEyeOutputViewport::EndInvoke(System.UInt32&,System.UInt32&,System.UInt32&,System.UInt32&,System.IAsyncResult)
extern "C"  void _GetEyeOutputViewport_EndInvoke_m2486246977 (_GetEyeOutputViewport_t693377854 * __this, uint32_t* ___pnX0, uint32_t* ___pnY1, uint32_t* ___pnWidth2, uint32_t* ___pnHeight3, Il2CppObject * ___result4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
