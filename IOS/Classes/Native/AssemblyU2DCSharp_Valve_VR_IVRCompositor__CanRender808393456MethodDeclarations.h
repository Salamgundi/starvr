﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_CanRenderScene
struct _CanRenderScene_t808393456;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_CanRenderScene::.ctor(System.Object,System.IntPtr)
extern "C"  void _CanRenderScene__ctor_m781649571 (_CanRenderScene_t808393456 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRCompositor/_CanRenderScene::Invoke()
extern "C"  bool _CanRenderScene_Invoke_m2442073119 (_CanRenderScene_t808393456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_CanRenderScene::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _CanRenderScene_BeginInvoke_m45965706 (_CanRenderScene_t808393456 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRCompositor/_CanRenderScene::EndInvoke(System.IAsyncResult)
extern "C"  bool _CanRenderScene_EndInvoke_m2556772869 (_CanRenderScene_t808393456 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
