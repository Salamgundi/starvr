﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_Knob
struct VRTK_Knob_t1401139190;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_ControlValueRa2976216666.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Knob_KnobDirection4203016241.h"

// System.Void VRTK.VRTK_Knob::.ctor()
extern "C"  void VRTK_Knob__ctor_m252517908 (VRTK_Knob_t1401139190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Knob::InitRequiredComponents()
extern "C"  void VRTK_Knob_InitRequiredComponents_m2513088805 (VRTK_Knob_t1401139190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Knob::DetectSetup()
extern "C"  bool VRTK_Knob_DetectSetup_m2603801794 (VRTK_Knob_t1401139190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_Control/ControlValueRange VRTK.VRTK_Knob::RegisterValueRange()
extern "C"  ControlValueRange_t2976216666  VRTK_Knob_RegisterValueRange_m2894618874 (VRTK_Knob_t1401139190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Knob::HandleUpdate()
extern "C"  void VRTK_Knob_HandleUpdate_m2043675305 (VRTK_Knob_t1401139190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Knob::InitKnob()
extern "C"  void VRTK_Knob_InitKnob_m20774636 (VRTK_Knob_t1401139190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_Knob/KnobDirection VRTK.VRTK_Knob::DetectDirection()
extern "C"  int32_t VRTK_Knob_DetectDirection_m1158005570 (VRTK_Knob_t1401139190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_Knob::CalculateValue()
extern "C"  float VRTK_Knob_CalculateValue_m2129167783 (VRTK_Knob_t1401139190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
