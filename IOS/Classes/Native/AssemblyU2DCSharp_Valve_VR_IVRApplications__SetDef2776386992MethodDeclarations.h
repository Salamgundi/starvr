﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_SetDefaultApplicationForMimeType
struct _SetDefaultApplicationForMimeType_t2776386992;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_SetDefaultApplicationForMimeType::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetDefaultApplicationForMimeType__ctor_m221711683 (_SetDefaultApplicationForMimeType_t2776386992 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_SetDefaultApplicationForMimeType::Invoke(System.String,System.String)
extern "C"  int32_t _SetDefaultApplicationForMimeType_Invoke_m3993462667 (_SetDefaultApplicationForMimeType_t2776386992 * __this, String_t* ___pchAppKey0, String_t* ___pchMimeType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_SetDefaultApplicationForMimeType::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetDefaultApplicationForMimeType_BeginInvoke_m2029083370 (_SetDefaultApplicationForMimeType_t2776386992 * __this, String_t* ___pchAppKey0, String_t* ___pchMimeType1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_SetDefaultApplicationForMimeType::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetDefaultApplicationForMimeType_EndInvoke_m1474267333 (_SetDefaultApplicationForMimeType_t2776386992 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
