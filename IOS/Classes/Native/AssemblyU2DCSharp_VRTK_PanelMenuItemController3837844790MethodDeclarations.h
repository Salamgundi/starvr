﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.PanelMenuItemController
struct PanelMenuItemController_t3837844790;
// VRTK.PanelMenuItemControllerEventHandler
struct PanelMenuItemControllerEventHandler_t780308162;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_PanelMenuItemControllerEvent780308162.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_VRTK_PanelMenuItemControllerEven2917504033.h"

// System.Void VRTK.PanelMenuItemController::.ctor()
extern "C"  void PanelMenuItemController__ctor_m1135063636 (PanelMenuItemController_t3837844790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::add_PanelMenuItemShowing(VRTK.PanelMenuItemControllerEventHandler)
extern "C"  void PanelMenuItemController_add_PanelMenuItemShowing_m3339108256 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventHandler_t780308162 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::remove_PanelMenuItemShowing(VRTK.PanelMenuItemControllerEventHandler)
extern "C"  void PanelMenuItemController_remove_PanelMenuItemShowing_m1465577581 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventHandler_t780308162 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::add_PanelMenuItemHiding(VRTK.PanelMenuItemControllerEventHandler)
extern "C"  void PanelMenuItemController_add_PanelMenuItemHiding_m3940003246 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventHandler_t780308162 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::remove_PanelMenuItemHiding(VRTK.PanelMenuItemControllerEventHandler)
extern "C"  void PanelMenuItemController_remove_PanelMenuItemHiding_m554355675 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventHandler_t780308162 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::add_PanelMenuItemSwipeLeft(VRTK.PanelMenuItemControllerEventHandler)
extern "C"  void PanelMenuItemController_add_PanelMenuItemSwipeLeft_m1467777128 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventHandler_t780308162 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::remove_PanelMenuItemSwipeLeft(VRTK.PanelMenuItemControllerEventHandler)
extern "C"  void PanelMenuItemController_remove_PanelMenuItemSwipeLeft_m2151762333 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventHandler_t780308162 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::add_PanelMenuItemSwipeRight(VRTK.PanelMenuItemControllerEventHandler)
extern "C"  void PanelMenuItemController_add_PanelMenuItemSwipeRight_m1675382937 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventHandler_t780308162 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::remove_PanelMenuItemSwipeRight(VRTK.PanelMenuItemControllerEventHandler)
extern "C"  void PanelMenuItemController_remove_PanelMenuItemSwipeRight_m1526877396 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventHandler_t780308162 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::add_PanelMenuItemSwipeTop(VRTK.PanelMenuItemControllerEventHandler)
extern "C"  void PanelMenuItemController_add_PanelMenuItemSwipeTop_m1191187152 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventHandler_t780308162 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::remove_PanelMenuItemSwipeTop(VRTK.PanelMenuItemControllerEventHandler)
extern "C"  void PanelMenuItemController_remove_PanelMenuItemSwipeTop_m3025352505 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventHandler_t780308162 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::add_PanelMenuItemSwipeBottom(VRTK.PanelMenuItemControllerEventHandler)
extern "C"  void PanelMenuItemController_add_PanelMenuItemSwipeBottom_m3427346078 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventHandler_t780308162 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::remove_PanelMenuItemSwipeBottom(VRTK.PanelMenuItemControllerEventHandler)
extern "C"  void PanelMenuItemController_remove_PanelMenuItemSwipeBottom_m3612841817 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventHandler_t780308162 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::add_PanelMenuItemTriggerPressed(VRTK.PanelMenuItemControllerEventHandler)
extern "C"  void PanelMenuItemController_add_PanelMenuItemTriggerPressed_m4153747211 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventHandler_t780308162 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::remove_PanelMenuItemTriggerPressed(VRTK.PanelMenuItemControllerEventHandler)
extern "C"  void PanelMenuItemController_remove_PanelMenuItemTriggerPressed_m2402304578 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventHandler_t780308162 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::Show(UnityEngine.GameObject)
extern "C"  void PanelMenuItemController_Show_m4227725453 (PanelMenuItemController_t3837844790 * __this, GameObject_t1756533147 * ___interactableObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::Hide(UnityEngine.GameObject)
extern "C"  void PanelMenuItemController_Hide_m823566268 (PanelMenuItemController_t3837844790 * __this, GameObject_t1756533147 * ___interactableObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::SwipeLeft(UnityEngine.GameObject)
extern "C"  void PanelMenuItemController_SwipeLeft_m936248685 (PanelMenuItemController_t3837844790 * __this, GameObject_t1756533147 * ___interactableObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::SwipeRight(UnityEngine.GameObject)
extern "C"  void PanelMenuItemController_SwipeRight_m2530269756 (PanelMenuItemController_t3837844790 * __this, GameObject_t1756533147 * ___interactableObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::SwipeTop(UnityEngine.GameObject)
extern "C"  void PanelMenuItemController_SwipeTop_m1922132013 (PanelMenuItemController_t3837844790 * __this, GameObject_t1756533147 * ___interactableObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::SwipeBottom(UnityEngine.GameObject)
extern "C"  void PanelMenuItemController_SwipeBottom_m1129253833 (PanelMenuItemController_t3837844790 * __this, GameObject_t1756533147 * ___interactableObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::TriggerPressed(UnityEngine.GameObject)
extern "C"  void PanelMenuItemController_TriggerPressed_m2911720998 (PanelMenuItemController_t3837844790 * __this, GameObject_t1756533147 * ___interactableObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::OnPanelMenuItemShowing(VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuItemController_OnPanelMenuItemShowing_m1496962242 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventArgs_t2917504033  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::OnPanelMenuItemHiding(VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuItemController_OnPanelMenuItemHiding_m2776364002 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventArgs_t2917504033  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::OnPanelMenuItemSwipeLeft(VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuItemController_OnPanelMenuItemSwipeLeft_m4289598042 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventArgs_t2917504033  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::OnPanelMenuItemSwipeRight(VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuItemController_OnPanelMenuItemSwipeRight_m1285667213 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventArgs_t2917504033  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::OnPanelMenuItemSwipeTop(VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuItemController_OnPanelMenuItemSwipeTop_m3739465784 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventArgs_t2917504033  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::OnPanelMenuItemSwipeBottom(VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuItemController_OnPanelMenuItemSwipeBottom_m872365972 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventArgs_t2917504033  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemController::OnPanelMenuItemTriggerPressed(VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuItemController_OnPanelMenuItemTriggerPressed_m125928915 (PanelMenuItemController_t3837844790 * __this, PanelMenuItemControllerEventArgs_t2917504033  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.PanelMenuItemControllerEventArgs VRTK.PanelMenuItemController::SetPanelMenuItemEvent(UnityEngine.GameObject)
extern "C"  PanelMenuItemControllerEventArgs_t2917504033  PanelMenuItemController_SetPanelMenuItemEvent_m1930776785 (PanelMenuItemController_t3837844790 * __this, GameObject_t1756533147 * ___interactableObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
