﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.ButtonReactor
struct ButtonReactor_t2088095572;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_Control3DEventArgs4095025701.h"

// System.Void VRTK.Examples.ButtonReactor::.ctor()
extern "C"  void ButtonReactor__ctor_m715006477 (ButtonReactor_t2088095572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.ButtonReactor::Start()
extern "C"  void ButtonReactor_Start_m3131387245 (ButtonReactor_t2088095572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.ButtonReactor::handlePush(System.Object,VRTK.Control3DEventArgs)
extern "C"  void ButtonReactor_handlePush_m2527731937 (ButtonReactor_t2088095572 * __this, Il2CppObject * ___sender0, Control3DEventArgs_t4095025701  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
