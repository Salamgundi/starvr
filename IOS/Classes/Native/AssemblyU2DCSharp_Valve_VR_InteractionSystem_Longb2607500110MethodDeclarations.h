﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Longbow
struct Longbow_t2607500110;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Valve.VR.InteractionSystem.ArrowHand
struct ArrowHand_t565341420;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_ArrowH565341420.h"

// System.Void Valve.VR.InteractionSystem.Longbow::.ctor()
extern "C"  void Longbow__ctor_m870332754 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::OnAttachedToHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void Longbow_OnAttachedToHand_m337719551 (Longbow_t2607500110 * __this, Hand_t379716353 * ___attachedHand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::Awake()
extern "C"  void Longbow_Awake_m2757506607 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::OnEnable()
extern "C"  void Longbow_OnEnable_m801560142 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::OnDisable()
extern "C"  void Longbow_OnDisable_m2690454643 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::LateUpdate()
extern "C"  void Longbow_LateUpdate_m1052482023 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::OnNewPosesApplied()
extern "C"  void Longbow_OnNewPosesApplied_m6621468 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::HandAttachedUpdate(Valve.VR.InteractionSystem.Hand)
extern "C"  void Longbow_HandAttachedUpdate_m1948742252 (Longbow_t2607500110 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::ArrowReleased()
extern "C"  void Longbow_ArrowReleased_m4097310272 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.Longbow::ResetDrawAnim()
extern "C"  Il2CppObject * Longbow_ResetDrawAnim_m2659019922 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.InteractionSystem.Longbow::GetArrowVelocity()
extern "C"  float Longbow_GetArrowVelocity_m2052236868 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::StartRotationLerp()
extern "C"  void Longbow_StartRotationLerp_m518950497 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::StartNock(Valve.VR.InteractionSystem.ArrowHand)
extern "C"  void Longbow_StartNock_m3342959368 (Longbow_t2607500110 * __this, ArrowHand_t565341420 * ___currentArrowHand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::EvaluateHandedness()
extern "C"  void Longbow_EvaluateHandedness_m2919180856 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::DoHandednessCheck()
extern "C"  void Longbow_DoHandednessCheck_m1473348648 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::ArrowInPosition()
extern "C"  void Longbow_ArrowInPosition_m2688985525 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::ReleaseNock()
extern "C"  void Longbow_ReleaseNock_m2630966734 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::ShutDown()
extern "C"  void Longbow_ShutDown_m1445027870 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::OnHandFocusLost(Valve.VR.InteractionSystem.Hand)
extern "C"  void Longbow_OnHandFocusLost_m201250944 (Longbow_t2607500110 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::OnHandFocusAcquired(Valve.VR.InteractionSystem.Hand)
extern "C"  void Longbow_OnHandFocusAcquired_m1565214458 (Longbow_t2607500110 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::OnDetachedFromHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void Longbow_OnDetachedFromHand_m1965295810 (Longbow_t2607500110 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Longbow::OnDestroy()
extern "C"  void Longbow_OnDestroy_m2357893161 (Longbow_t2607500110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
