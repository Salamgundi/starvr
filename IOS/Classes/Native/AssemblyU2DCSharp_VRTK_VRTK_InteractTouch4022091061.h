﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.ObjectInteractEventHandler
struct ObjectInteractEventHandler_t1701902511;
// System.Collections.Generic.List`1<UnityEngine.Collider>
struct List_1_t2866794480;
// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.Object
struct Object_t1021602117;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_InteractTouch
struct  VRTK_InteractTouch_t4022091061  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject VRTK.VRTK_InteractTouch::customRigidbodyObject
	GameObject_t1756533147 * ___customRigidbodyObject_2;
	// VRTK.ObjectInteractEventHandler VRTK.VRTK_InteractTouch::ControllerTouchInteractableObject
	ObjectInteractEventHandler_t1701902511 * ___ControllerTouchInteractableObject_3;
	// VRTK.ObjectInteractEventHandler VRTK.VRTK_InteractTouch::ControllerUntouchInteractableObject
	ObjectInteractEventHandler_t1701902511 * ___ControllerUntouchInteractableObject_4;
	// UnityEngine.GameObject VRTK.VRTK_InteractTouch::touchedObject
	GameObject_t1756533147 * ___touchedObject_5;
	// System.Collections.Generic.List`1<UnityEngine.Collider> VRTK.VRTK_InteractTouch::touchedObjectColliders
	List_1_t2866794480 * ___touchedObjectColliders_6;
	// System.Collections.Generic.List`1<UnityEngine.Collider> VRTK.VRTK_InteractTouch::touchedObjectActiveColliders
	List_1_t2866794480 * ___touchedObjectActiveColliders_7;
	// VRTK.VRTK_ControllerEvents VRTK.VRTK_InteractTouch::controllerEvents
	VRTK_ControllerEvents_t3225224819 * ___controllerEvents_8;
	// VRTK.VRTK_ControllerActions VRTK.VRTK_InteractTouch::controllerActions
	VRTK_ControllerActions_t3642353851 * ___controllerActions_9;
	// UnityEngine.GameObject VRTK.VRTK_InteractTouch::controllerCollisionDetector
	GameObject_t1756533147 * ___controllerCollisionDetector_10;
	// System.Boolean VRTK.VRTK_InteractTouch::triggerRumble
	bool ___triggerRumble_11;
	// System.Boolean VRTK.VRTK_InteractTouch::destroyColliderOnDisable
	bool ___destroyColliderOnDisable_12;
	// System.Boolean VRTK.VRTK_InteractTouch::triggerIsColliding
	bool ___triggerIsColliding_13;
	// System.Boolean VRTK.VRTK_InteractTouch::triggerWasColliding
	bool ___triggerWasColliding_14;
	// UnityEngine.Rigidbody VRTK.VRTK_InteractTouch::touchRigidBody
	Rigidbody_t4233889191 * ___touchRigidBody_15;
	// System.Boolean VRTK.VRTK_InteractTouch::rigidBodyForcedActive
	bool ___rigidBodyForcedActive_16;
	// UnityEngine.Object VRTK.VRTK_InteractTouch::defaultColliderPrefab
	Object_t1021602117 * ___defaultColliderPrefab_17;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_InteractTouch::originalGrabAlias
	int32_t ___originalGrabAlias_18;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_InteractTouch::originalUseAlias
	int32_t ___originalUseAlias_19;

public:
	inline static int32_t get_offset_of_customRigidbodyObject_2() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___customRigidbodyObject_2)); }
	inline GameObject_t1756533147 * get_customRigidbodyObject_2() const { return ___customRigidbodyObject_2; }
	inline GameObject_t1756533147 ** get_address_of_customRigidbodyObject_2() { return &___customRigidbodyObject_2; }
	inline void set_customRigidbodyObject_2(GameObject_t1756533147 * value)
	{
		___customRigidbodyObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___customRigidbodyObject_2, value);
	}

	inline static int32_t get_offset_of_ControllerTouchInteractableObject_3() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___ControllerTouchInteractableObject_3)); }
	inline ObjectInteractEventHandler_t1701902511 * get_ControllerTouchInteractableObject_3() const { return ___ControllerTouchInteractableObject_3; }
	inline ObjectInteractEventHandler_t1701902511 ** get_address_of_ControllerTouchInteractableObject_3() { return &___ControllerTouchInteractableObject_3; }
	inline void set_ControllerTouchInteractableObject_3(ObjectInteractEventHandler_t1701902511 * value)
	{
		___ControllerTouchInteractableObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerTouchInteractableObject_3, value);
	}

	inline static int32_t get_offset_of_ControllerUntouchInteractableObject_4() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___ControllerUntouchInteractableObject_4)); }
	inline ObjectInteractEventHandler_t1701902511 * get_ControllerUntouchInteractableObject_4() const { return ___ControllerUntouchInteractableObject_4; }
	inline ObjectInteractEventHandler_t1701902511 ** get_address_of_ControllerUntouchInteractableObject_4() { return &___ControllerUntouchInteractableObject_4; }
	inline void set_ControllerUntouchInteractableObject_4(ObjectInteractEventHandler_t1701902511 * value)
	{
		___ControllerUntouchInteractableObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerUntouchInteractableObject_4, value);
	}

	inline static int32_t get_offset_of_touchedObject_5() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___touchedObject_5)); }
	inline GameObject_t1756533147 * get_touchedObject_5() const { return ___touchedObject_5; }
	inline GameObject_t1756533147 ** get_address_of_touchedObject_5() { return &___touchedObject_5; }
	inline void set_touchedObject_5(GameObject_t1756533147 * value)
	{
		___touchedObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___touchedObject_5, value);
	}

	inline static int32_t get_offset_of_touchedObjectColliders_6() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___touchedObjectColliders_6)); }
	inline List_1_t2866794480 * get_touchedObjectColliders_6() const { return ___touchedObjectColliders_6; }
	inline List_1_t2866794480 ** get_address_of_touchedObjectColliders_6() { return &___touchedObjectColliders_6; }
	inline void set_touchedObjectColliders_6(List_1_t2866794480 * value)
	{
		___touchedObjectColliders_6 = value;
		Il2CppCodeGenWriteBarrier(&___touchedObjectColliders_6, value);
	}

	inline static int32_t get_offset_of_touchedObjectActiveColliders_7() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___touchedObjectActiveColliders_7)); }
	inline List_1_t2866794480 * get_touchedObjectActiveColliders_7() const { return ___touchedObjectActiveColliders_7; }
	inline List_1_t2866794480 ** get_address_of_touchedObjectActiveColliders_7() { return &___touchedObjectActiveColliders_7; }
	inline void set_touchedObjectActiveColliders_7(List_1_t2866794480 * value)
	{
		___touchedObjectActiveColliders_7 = value;
		Il2CppCodeGenWriteBarrier(&___touchedObjectActiveColliders_7, value);
	}

	inline static int32_t get_offset_of_controllerEvents_8() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___controllerEvents_8)); }
	inline VRTK_ControllerEvents_t3225224819 * get_controllerEvents_8() const { return ___controllerEvents_8; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_controllerEvents_8() { return &___controllerEvents_8; }
	inline void set_controllerEvents_8(VRTK_ControllerEvents_t3225224819 * value)
	{
		___controllerEvents_8 = value;
		Il2CppCodeGenWriteBarrier(&___controllerEvents_8, value);
	}

	inline static int32_t get_offset_of_controllerActions_9() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___controllerActions_9)); }
	inline VRTK_ControllerActions_t3642353851 * get_controllerActions_9() const { return ___controllerActions_9; }
	inline VRTK_ControllerActions_t3642353851 ** get_address_of_controllerActions_9() { return &___controllerActions_9; }
	inline void set_controllerActions_9(VRTK_ControllerActions_t3642353851 * value)
	{
		___controllerActions_9 = value;
		Il2CppCodeGenWriteBarrier(&___controllerActions_9, value);
	}

	inline static int32_t get_offset_of_controllerCollisionDetector_10() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___controllerCollisionDetector_10)); }
	inline GameObject_t1756533147 * get_controllerCollisionDetector_10() const { return ___controllerCollisionDetector_10; }
	inline GameObject_t1756533147 ** get_address_of_controllerCollisionDetector_10() { return &___controllerCollisionDetector_10; }
	inline void set_controllerCollisionDetector_10(GameObject_t1756533147 * value)
	{
		___controllerCollisionDetector_10 = value;
		Il2CppCodeGenWriteBarrier(&___controllerCollisionDetector_10, value);
	}

	inline static int32_t get_offset_of_triggerRumble_11() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___triggerRumble_11)); }
	inline bool get_triggerRumble_11() const { return ___triggerRumble_11; }
	inline bool* get_address_of_triggerRumble_11() { return &___triggerRumble_11; }
	inline void set_triggerRumble_11(bool value)
	{
		___triggerRumble_11 = value;
	}

	inline static int32_t get_offset_of_destroyColliderOnDisable_12() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___destroyColliderOnDisable_12)); }
	inline bool get_destroyColliderOnDisable_12() const { return ___destroyColliderOnDisable_12; }
	inline bool* get_address_of_destroyColliderOnDisable_12() { return &___destroyColliderOnDisable_12; }
	inline void set_destroyColliderOnDisable_12(bool value)
	{
		___destroyColliderOnDisable_12 = value;
	}

	inline static int32_t get_offset_of_triggerIsColliding_13() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___triggerIsColliding_13)); }
	inline bool get_triggerIsColliding_13() const { return ___triggerIsColliding_13; }
	inline bool* get_address_of_triggerIsColliding_13() { return &___triggerIsColliding_13; }
	inline void set_triggerIsColliding_13(bool value)
	{
		___triggerIsColliding_13 = value;
	}

	inline static int32_t get_offset_of_triggerWasColliding_14() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___triggerWasColliding_14)); }
	inline bool get_triggerWasColliding_14() const { return ___triggerWasColliding_14; }
	inline bool* get_address_of_triggerWasColliding_14() { return &___triggerWasColliding_14; }
	inline void set_triggerWasColliding_14(bool value)
	{
		___triggerWasColliding_14 = value;
	}

	inline static int32_t get_offset_of_touchRigidBody_15() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___touchRigidBody_15)); }
	inline Rigidbody_t4233889191 * get_touchRigidBody_15() const { return ___touchRigidBody_15; }
	inline Rigidbody_t4233889191 ** get_address_of_touchRigidBody_15() { return &___touchRigidBody_15; }
	inline void set_touchRigidBody_15(Rigidbody_t4233889191 * value)
	{
		___touchRigidBody_15 = value;
		Il2CppCodeGenWriteBarrier(&___touchRigidBody_15, value);
	}

	inline static int32_t get_offset_of_rigidBodyForcedActive_16() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___rigidBodyForcedActive_16)); }
	inline bool get_rigidBodyForcedActive_16() const { return ___rigidBodyForcedActive_16; }
	inline bool* get_address_of_rigidBodyForcedActive_16() { return &___rigidBodyForcedActive_16; }
	inline void set_rigidBodyForcedActive_16(bool value)
	{
		___rigidBodyForcedActive_16 = value;
	}

	inline static int32_t get_offset_of_defaultColliderPrefab_17() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___defaultColliderPrefab_17)); }
	inline Object_t1021602117 * get_defaultColliderPrefab_17() const { return ___defaultColliderPrefab_17; }
	inline Object_t1021602117 ** get_address_of_defaultColliderPrefab_17() { return &___defaultColliderPrefab_17; }
	inline void set_defaultColliderPrefab_17(Object_t1021602117 * value)
	{
		___defaultColliderPrefab_17 = value;
		Il2CppCodeGenWriteBarrier(&___defaultColliderPrefab_17, value);
	}

	inline static int32_t get_offset_of_originalGrabAlias_18() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___originalGrabAlias_18)); }
	inline int32_t get_originalGrabAlias_18() const { return ___originalGrabAlias_18; }
	inline int32_t* get_address_of_originalGrabAlias_18() { return &___originalGrabAlias_18; }
	inline void set_originalGrabAlias_18(int32_t value)
	{
		___originalGrabAlias_18 = value;
	}

	inline static int32_t get_offset_of_originalUseAlias_19() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_t4022091061, ___originalUseAlias_19)); }
	inline int32_t get_originalUseAlias_19() const { return ___originalUseAlias_19; }
	inline int32_t* get_address_of_originalUseAlias_19() { return &___originalUseAlias_19; }
	inline void set_originalUseAlias_19(int32_t value)
	{
		___originalUseAlias_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
