﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.LinearBlendshape
struct LinearBlendshape_t393683001;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.LinearBlendshape::.ctor()
extern "C"  void LinearBlendshape__ctor_m1112711241 (LinearBlendshape_t393683001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearBlendshape::Awake()
extern "C"  void LinearBlendshape_Awake_m3463876218 (LinearBlendshape_t393683001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearBlendshape::Update()
extern "C"  void LinearBlendshape_Update_m1363479852 (LinearBlendshape_t393683001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
