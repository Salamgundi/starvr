﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRRenderModels
struct CVRRenderModels_t2019937239;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRRenderModelError21703732.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_Valve_VR_VRControllerState_t2504874220.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_ControllerM1298199406.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_ComponentSt2032012879.h"

// System.Void Valve.VR.CVRRenderModels::.ctor(System.IntPtr)
extern "C"  void CVRRenderModels__ctor_m3076026906 (CVRRenderModels_t2019937239 * __this, IntPtr_t ___pInterface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRRenderModelError Valve.VR.CVRRenderModels::LoadRenderModel_Async(System.String,System.IntPtr&)
extern "C"  int32_t CVRRenderModels_LoadRenderModel_Async_m556554629 (CVRRenderModels_t2019937239 * __this, String_t* ___pchRenderModelName0, IntPtr_t* ___ppRenderModel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRRenderModels::FreeRenderModel(System.IntPtr)
extern "C"  void CVRRenderModels_FreeRenderModel_m4109091993 (CVRRenderModels_t2019937239 * __this, IntPtr_t ___pRenderModel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRRenderModelError Valve.VR.CVRRenderModels::LoadTexture_Async(System.Int32,System.IntPtr&)
extern "C"  int32_t CVRRenderModels_LoadTexture_Async_m418127036 (CVRRenderModels_t2019937239 * __this, int32_t ___textureId0, IntPtr_t* ___ppTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRRenderModels::FreeTexture(System.IntPtr)
extern "C"  void CVRRenderModels_FreeTexture_m2263912791 (CVRRenderModels_t2019937239 * __this, IntPtr_t ___pTexture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRRenderModelError Valve.VR.CVRRenderModels::LoadTextureD3D11_Async(System.Int32,System.IntPtr,System.IntPtr&)
extern "C"  int32_t CVRRenderModels_LoadTextureD3D11_Async_m3428365485 (CVRRenderModels_t2019937239 * __this, int32_t ___textureId0, IntPtr_t ___pD3D11Device1, IntPtr_t* ___ppD3D11Texture2D2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRRenderModelError Valve.VR.CVRRenderModels::LoadIntoTextureD3D11_Async(System.Int32,System.IntPtr)
extern "C"  int32_t CVRRenderModels_LoadIntoTextureD3D11_Async_m2173614789 (CVRRenderModels_t2019937239 * __this, int32_t ___textureId0, IntPtr_t ___pDstTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRRenderModels::FreeTextureD3D11(System.IntPtr)
extern "C"  void CVRRenderModels_FreeTextureD3D11_m986794874 (CVRRenderModels_t2019937239 * __this, IntPtr_t ___pD3D11Texture2D0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRRenderModels::GetRenderModelName(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t CVRRenderModels_GetRenderModelName_m4134079925 (CVRRenderModels_t2019937239 * __this, uint32_t ___unRenderModelIndex0, StringBuilder_t1221177846 * ___pchRenderModelName1, uint32_t ___unRenderModelNameLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRRenderModels::GetRenderModelCount()
extern "C"  uint32_t CVRRenderModels_GetRenderModelCount_m2444653849 (CVRRenderModels_t2019937239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRRenderModels::GetComponentCount(System.String)
extern "C"  uint32_t CVRRenderModels_GetComponentCount_m2002179265 (CVRRenderModels_t2019937239 * __this, String_t* ___pchRenderModelName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRRenderModels::GetComponentName(System.String,System.UInt32,System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t CVRRenderModels_GetComponentName_m2752453129 (CVRRenderModels_t2019937239 * __this, String_t* ___pchRenderModelName0, uint32_t ___unComponentIndex1, StringBuilder_t1221177846 * ___pchComponentName2, uint32_t ___unComponentNameLen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.CVRRenderModels::GetComponentButtonMask(System.String,System.String)
extern "C"  uint64_t CVRRenderModels_GetComponentButtonMask_m2275533289 (CVRRenderModels_t2019937239 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRRenderModels::GetComponentRenderModelName(System.String,System.String,System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t CVRRenderModels_GetComponentRenderModelName_m4105314842 (CVRRenderModels_t2019937239 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, StringBuilder_t1221177846 * ___pchComponentRenderModelName2, uint32_t ___unComponentRenderModelNameLen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRRenderModels::GetComponentState(System.String,System.String,Valve.VR.VRControllerState_t&,Valve.VR.RenderModel_ControllerMode_State_t&,Valve.VR.RenderModel_ComponentState_t&)
extern "C"  bool CVRRenderModels_GetComponentState_m2689897633 (CVRRenderModels_t2019937239 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, VRControllerState_t_t2504874220 * ___pControllerState2, RenderModel_ControllerMode_State_t_t1298199406 * ___pState3, RenderModel_ComponentState_t_t2032012879 * ___pComponentState4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRRenderModels::RenderModelHasComponent(System.String,System.String)
extern "C"  bool CVRRenderModels_RenderModelHasComponent_m1120878802 (CVRRenderModels_t2019937239 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRRenderModels::GetRenderModelThumbnailURL(System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRRenderModelError&)
extern "C"  uint32_t CVRRenderModels_GetRenderModelThumbnailURL_m2845239261 (CVRRenderModels_t2019937239 * __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchThumbnailURL1, uint32_t ___unThumbnailURLLen2, int32_t* ___peError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRRenderModels::GetRenderModelOriginalPath(System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRRenderModelError&)
extern "C"  uint32_t CVRRenderModels_GetRenderModelOriginalPath_m278628054 (CVRRenderModels_t2019937239 * __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchOriginalPath1, uint32_t ___unOriginalPathLen2, int32_t* ___peError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.CVRRenderModels::GetRenderModelErrorNameFromEnum(Valve.VR.EVRRenderModelError)
extern "C"  String_t* CVRRenderModels_GetRenderModelErrorNameFromEnum_m3738242548 (CVRRenderModels_t2019937239 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
