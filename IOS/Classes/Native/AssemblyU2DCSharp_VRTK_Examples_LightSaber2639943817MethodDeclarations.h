﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.LightSaber
struct LightSaber_t2639943817;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.Examples.LightSaber::.ctor()
extern "C"  void LightSaber__ctor_m855192228 (LightSaber_t2639943817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.LightSaber::StartUsing(UnityEngine.GameObject)
extern "C"  void LightSaber_StartUsing_m4060831012 (LightSaber_t2639943817 * __this, GameObject_t1756533147 * ___usingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.LightSaber::StopUsing(UnityEngine.GameObject)
extern "C"  void LightSaber_StopUsing_m859834386 (LightSaber_t2639943817 * __this, GameObject_t1756533147 * ___usingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.LightSaber::Start()
extern "C"  void LightSaber_Start_m3014474856 (LightSaber_t2639943817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.LightSaber::Update()
extern "C"  void LightSaber_Update_m376206443 (LightSaber_t2639943817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.LightSaber::SetBeamSize()
extern "C"  void LightSaber_SetBeamSize_m2150427630 (LightSaber_t2639943817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.LightSaber::PulseBeam()
extern "C"  void LightSaber_PulseBeam_m1803750766 (LightSaber_t2639943817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
