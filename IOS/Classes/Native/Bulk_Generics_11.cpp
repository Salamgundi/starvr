﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>
struct U3CSortU3Ec__Iterator21_t2488605192;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Linq.QuickSort`1<System.Object>
struct QuickSort_1_t1970792956;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Linq.SortContext`1<System.Object>
struct SortContext_1_t1798778454;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Linq.SortSequenceContext`2<System.Object,System.Object>
struct SortSequenceContext_2_t3061295073;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t643912417;
// System.String
struct String_t;
// System.Predicate`1<Gvr.Internal.EmulatorTouchEvent/Pointer>
struct Predicate_1_t1443655117;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Predicate`1<proto.PhoneEvent/Types/Type>
struct Predicate_1_t4268418272;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct Predicate_1_t2996539675;
// System.Predicate`1<System.Int32>
struct Predicate_1_t514847563;
// System.Predicate`1<System.Object>
struct Predicate_1_t1132419410;
// System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>
struct Predicate_1_t2832094954;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t4236135325;
// System.Predicate`1<System.Single>
struct Predicate_1_t519480047;
// System.Predicate`1<UnityEngine.Color32>
struct Predicate_1_t3612454929;
// System.Predicate`1<UnityEngine.CombineInstance>
struct Predicate_1_t2802532621;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t2759123787;
// System.Predicate`1<UnityEngine.Experimental.Director.Playable>
struct Predicate_1_t2110515663;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t1499606915;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t2064247989;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t3942196229;
// System.Predicate`1<UnityEngine.Vector2>
struct Predicate_1_t686677694;
// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t686677695;
// System.Predicate`1<UnityEngine.Vector4>
struct Predicate_1_t686677696;
// System.Predicate`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct Predicate_1_t4125655347;
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t4179406139;
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct StaticGetter_1_t1095697167;
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t2619124609;
// UnityEngine.Object
struct Object_t1021602117;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct CachedInvokableCall_1_t865427339;
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct CachedInvokableCall_1_t1482999186;
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t870059823;
// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct InvokableCall_1_t2019901575;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t897193173;
// UnityEngine.Events.InvokableCall`1<System.Int32>
struct InvokableCall_1_t266204305;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t3438463199;
// UnityEngine.Events.InvokableCall`1<System.Object>
struct InvokableCall_1_t883776152;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t4056035046;
// UnityEngine.Events.InvokableCall`1<System.Single>
struct InvokableCall_1_t270836789;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t3443095683;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Color>
struct InvokableCall_1_t214718932;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t3386977826;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>
struct InvokableCall_1_t438034436;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct UnityAction_1_t3610293330;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Vector3>
struct InvokableCall_1_t438034437;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector3>
struct UnityAction_1_t3610293331;
// UnityEngine.Events.InvokableCall`1<Valve.VR.VREvent_t>
struct InvokableCall_1_t1599593246;
// UnityEngine.Events.UnityAction`1<Valve.VR.VREvent_t>
struct UnityAction_1_t476884844;
// UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>
struct InvokableCall_2_t56619800;
// UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>
struct UnityAction_2_t41828916;
// UnityEngine.Events.InvokableCall`2<System.Object,System.Boolean>
struct InvokableCall_2_t640854293;
// UnityEngine.Events.UnityAction`2<System.Object,System.Boolean>
struct UnityAction_2_t626063409;
// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
struct InvokableCall_2_t3799696166;
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t3784905282;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.BodyPhysicsEventArgs>
struct InvokableCall_2_t3340378525;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.BodyPhysicsEventArgs>
struct UnityAction_2_t3325587641;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.Control3DEventArgs>
struct InvokableCall_2_t910305276;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.Control3DEventArgs>
struct UnityAction_2_t895514392;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerActionsEventArgs>
struct InvokableCall_2_t1454248347;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerActionsEventArgs>
struct UnityAction_2_t1439457463;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerInteractionEventArgs>
struct InvokableCall_2_t1397884410;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerInteractionEventArgs>
struct UnityAction_2_t1383093526;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DashTeleportEventArgs>
struct InvokableCall_2_t3307500113;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.DashTeleportEventArgs>
struct UnityAction_2_t3292709229;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DestinationMarkerEventArgs>
struct InvokableCall_2_t2962698532;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.DestinationMarkerEventArgs>
struct UnityAction_2_t2947907648;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetCollisionEventArgs>
struct InvokableCall_2_t2352620258;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetCollisionEventArgs>
struct UnityAction_2_t2337829374;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>
struct InvokableCall_2_t3763778592;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>
struct UnityAction_2_t3748987708;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetFadeEventArgs>
struct InvokableCall_2_t4002788890;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetFadeEventArgs>
struct UnityAction_2_t3987998006;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.InteractableObjectEventArgs>
struct InvokableCall_2_t1583422427;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.InteractableObjectEventArgs>
struct UnityAction_2_t1568631543;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ObjectControlEventArgs>
struct InvokableCall_2_t3569737190;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.ObjectControlEventArgs>
struct UnityAction_2_t3554946306;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ObjectInteractEventArgs>
struct InvokableCall_2_t1881538113;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.ObjectInteractEventArgs>
struct UnityAction_2_t1866747229;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.PlayerClimbEventArgs>
struct InvokableCall_2_t3647832616;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.PlayerClimbEventArgs>
struct UnityAction_2_t3633041732;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.SnapDropZoneEventArgs>
struct InvokableCall_2_t1528949645;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.SnapDropZoneEventArgs>
struct UnityAction_2_t1514158761;
// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.UIPointerEventArgs>
struct InvokableCall_2_t2282232849;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.UIPointerEventArgs>
struct UnityAction_2_t2267441965;
// UnityEngine.Events.InvokableCall`2<System.Single,System.Single>
struct InvokableCall_2_t149250066;
// UnityEngine.Events.UnityAction`2<System.Single,System.Single>
struct UnityAction_2_t134459182;
// UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>
struct InvokableCall_3_t2191335654;
// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct UnityAction_3_t3482433968;
// UnityEngine.Events.InvokableCall`3<UnityEngine.Color,System.Single,System.Boolean>
struct InvokableCall_3_t524958208;
// UnityEngine.Events.UnityAction`3<UnityEngine.Color,System.Single,System.Boolean>
struct UnityAction_3_t1816056522;
// UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
struct InvokableCall_4_t2955480072;
// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>
struct UnityAction_1_t3051495417;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_t1903595547;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct UnityAction_2_t606618774;
// UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>
struct UnityAction_4_t1666603240;
// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct UnityEvent_1_t3863924733;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t2110227463;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t2727799310;
// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_t2114859947;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Linq_QuickSort_1_U3CSortU3Ec__I2488605192.h"
#include "System_Core_System_Linq_QuickSort_1_U3CSortU3Ec__I2488605192MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "System_Core_System_Linq_SortContext_1_gen1798778454.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_UInt322149682021.h"
#include "System_Core_System_Linq_QuickSort_1_gen1970792956.h"
#include "System_Core_System_Linq_QuickSort_1_gen1970792956MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "System_Core_System_Linq_SortContext_1_gen1798778454MethodDeclarations.h"
#include "System_Core_System_Linq_SortDirection759359329.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen3061295073.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen3061295073MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2825504181.h"
#include "System_Core_System_Func_2_gen2825504181MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1693325264.h"
#include "mscorlib_System_Nullable_1_gen1693325264MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_TimeSpan3430258949MethodDeclarations.h"
#include "mscorlib_System_ValueType3507792607MethodDeclarations.h"
#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen283458390.h"
#include "mscorlib_System_Nullable_1_gen283458390MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2579219987.h"
#include "mscorlib_System_Nullable_1_gen2579219987MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen1443655117.h"
#include "mscorlib_System_Predicate_1_gen1443655117MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorTouchEvent_3000685002.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Predicate_1_gen4268418272.h"
#include "mscorlib_System_Predicate_1_gen4268418272MethodDeclarations.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Type1530480861.h"
#include "mscorlib_System_Predicate_1_gen2996539675.h"
#include "mscorlib_System_Predicate_1_gen2996539675MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_258602264.h"
#include "mscorlib_System_Predicate_1_gen514847563.h"
#include "mscorlib_System_Predicate_1_gen514847563MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen1132419410.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2832094954.h"
#include "mscorlib_System_Predicate_1_gen2832094954MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Predicate_1_gen4236135325.h"
#include "mscorlib_System_Predicate_1_gen4236135325MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Predicate_1_gen519480047.h"
#include "mscorlib_System_Predicate_1_gen519480047MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Predicate_1_gen3612454929.h"
#include "mscorlib_System_Predicate_1_gen3612454929MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "mscorlib_System_Predicate_1_gen2802532621.h"
#include "mscorlib_System_Predicate_1_gen2802532621MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CombineInstance64595210.h"
#include "mscorlib_System_Predicate_1_gen2759123787.h"
#include "mscorlib_System_Predicate_1_gen2759123787MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2110515663.h"
#include "mscorlib_System_Predicate_1_gen2110515663MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "mscorlib_System_Predicate_1_gen1499606915.h"
#include "mscorlib_System_Predicate_1_gen1499606915MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "mscorlib_System_Predicate_1_gen2064247989.h"
#include "mscorlib_System_Predicate_1_gen2064247989MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "mscorlib_System_Predicate_1_gen3942196229.h"
#include "mscorlib_System_Predicate_1_gen3942196229MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "mscorlib_System_Predicate_1_gen686677694.h"
#include "mscorlib_System_Predicate_1_gen686677694MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Predicate_1_gen686677695.h"
#include "mscorlib_System_Predicate_1_gen686677695MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Predicate_1_gen686677696.h"
#include "mscorlib_System_Predicate_1_gen686677696MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "mscorlib_System_Predicate_1_gen4125655347.h"
#include "mscorlib_System_Predicate_1_gen4125655347MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1387717936.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_g4179406139.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_g4179406139MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGett1095697167.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGett1095697167MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen3207297272.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen3207297272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall2619124609.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall2619124609MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2019901575MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2019901575.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_865427339.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_865427339MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen266204305MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen266204305.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall1482999186.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall1482999186MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen883776152MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen883776152.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_870059823.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_870059823MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen270836789MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen270836789.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen897193173.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked1625106012.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen897193173MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3438463199.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3438463199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4056035046.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4056035046MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3443095683.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3443095683MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen214718932.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen214718932MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3386977826.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3386977826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen438034436.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen438034436MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3610293330.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3610293330MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen438034437.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen438034437MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3610293331.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3610293331MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen1599593246.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen1599593246MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen476884844.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_t3405266389.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen476884844MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen56619800.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen56619800MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen41828916.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen41828916MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen640854293.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen640854293MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen626063409.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen626063409MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3799696166.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3799696166MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3784905282.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3784905282MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3340378525.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3340378525MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3325587641.h"
#include "AssemblyU2DCSharp_VRTK_BodyPhysicsEventArgs2230131654.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3325587641MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen910305276.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen910305276MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen895514392.h"
#include "AssemblyU2DCSharp_VRTK_Control3DEventArgs4095025701.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen895514392MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen1454248347.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen1454248347MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1439457463.h"
#include "AssemblyU2DCSharp_VRTK_ControllerActionsEventArgs344001476.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1439457463MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen1397884410.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen1397884410MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1383093526.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1383093526MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3307500113.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3307500113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3292709229.h"
#include "AssemblyU2DCSharp_VRTK_DashTeleportEventArgs2197253242.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3292709229MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen2962698532.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen2962698532MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen2947907648.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventArgs1852451661.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen2947907648MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen2352620258.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen2352620258MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen2337829374.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventArgs1242373387.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen2337829374MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3763778592.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3763778592MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3748987708.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetControllerAwareEvent2653531721.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3748987708MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen4002788890.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen4002788890MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3987998006.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetFadeEventArgs2892542019.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3987998006MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen1583422427.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen1583422427MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1568631543.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1568631543MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3569737190.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3569737190MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3554946306.h"
#include "AssemblyU2DCSharp_VRTK_ObjectControlEventArgs2459490319.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3554946306MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen1881538113.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen1881538113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1866747229.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventArgs771291242.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1866747229MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3647832616.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3647832616MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3633041732.h"
#include "AssemblyU2DCSharp_VRTK_PlayerClimbEventArgs2537585745.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3633041732MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen1528949645.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen1528949645MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1514158761.h"
#include "AssemblyU2DCSharp_VRTK_SnapDropZoneEventArgs418702774.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1514158761MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen2282232849.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen2282232849MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen2267441965.h"
#include "AssemblyU2DCSharp_VRTK_UIPointerEventArgs1171985978.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen2267441965MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen149250066.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen149250066MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen134459182.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen134459182MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_gen2191335654.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_gen2191335654MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen3482433968.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen3482433968MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_gen524958208.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_gen524958208MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen1816056522.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen1816056522MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_4_gen2955480072.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_4_gen2955480072MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_4_gen1666603240.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_4_gen1666603240MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3051495417.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3051495417MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1903595547.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1903595547MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen606618774.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen606618774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3863924733.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3863924733MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2110227463.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2110227463MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2727799310.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2727799310MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2114859947.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2114859947MethodDeclarations.h"

// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m457587038_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m457587038(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m457587038_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.Object>(!!0&,!!0,!!0)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m2145889806(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`1<System.Boolean>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_1_t897193173_m226081743(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_1_t897193173 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t897193173 **, UnityAction_1_t897193173 *, UnityAction_1_t897193173 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Boolean>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t3825574718_m3557881725_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t3825574718_m3557881725(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t3825574718_m3557881725_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`1<System.Int32>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_1_t3438463199_m4157363705(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_1_t3438463199 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3438463199 **, UnityAction_1_t3438463199 *, UnityAction_1_t3438463199 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Int32>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t2071877448_m4010682571_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t2071877448_m4010682571(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t2071877448_m4010682571_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`1<System.Object>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_1_t4056035046_m3323700736(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_1_t4056035046 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t4056035046 **, UnityAction_1_t4056035046 *, UnityAction_1_t4056035046 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1349548392_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1349548392(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1349548392_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`1<System.Single>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_1_t3443095683_m3615079253(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_1_t3443095683 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3443095683 **, UnityAction_1_t3443095683 *, UnityAction_1_t3443095683 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Single>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t2076509932_m3470174535_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t2076509932_m3470174535(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t2076509932_m3470174535_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`1<UnityEngine.Color>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_1_t3386977826_m3959013804(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_1_t3386977826 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3386977826 **, UnityAction_1_t3386977826 *, UnityAction_1_t3386977826 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Color>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisColor_t2020392075_m85849056_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisColor_t2020392075_m85849056(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisColor_t2020392075_m85849056_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_1_t3610293330_m1518298976(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_1_t3610293330 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3610293330 **, UnityAction_1_t3610293330 *, UnityAction_1_t3610293330 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Vector2>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t2243707579_m3249535332_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t2243707579_m3249535332(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t2243707579_m3249535332_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`1<UnityEngine.Vector3>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_1_t3610293331_m2294759493(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_1_t3610293331 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3610293331 **, UnityAction_1_t3610293331 *, UnityAction_1_t3610293331 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Vector3>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisVector3_t2243707580_m556794275_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisVector3_t2243707580_m556794275(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisVector3_t2243707580_m556794275_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`1<Valve.VR.VREvent_t>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_1_t476884844_m3800676595(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_1_t476884844 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t476884844 **, UnityAction_1_t476884844 *, UnityAction_1_t476884844 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Valve.VR.VREvent_t>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisVREvent_t_t3405266389_m18635821_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisVREvent_t_t3405266389_m18635821(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisVREvent_t_t3405266389_m18635821_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t41828916_m1084267069(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t41828916 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t41828916 **, UnityAction_2_t41828916 *, UnityAction_2_t41828916 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,System.Boolean>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t626063409_m793544546(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t626063409 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t626063409 **, UnityAction_2_t626063409 *, UnityAction_2_t626063409 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,System.Object>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t3784905282_m232124099(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t3784905282 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t3784905282 **, UnityAction_2_t3784905282 *, UnityAction_2_t3784905282 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.BodyPhysicsEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t3325587641_m295661746(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t3325587641 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t3325587641 **, UnityAction_2_t3325587641 *, UnityAction_2_t3325587641 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.BodyPhysicsEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisBodyPhysicsEventArgs_t2230131654_m953807295_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisBodyPhysicsEventArgs_t2230131654_m953807295(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisBodyPhysicsEventArgs_t2230131654_m953807295_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.Control3DEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t895514392_m186009347(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t895514392 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t895514392 **, UnityAction_2_t895514392 *, UnityAction_2_t895514392 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.Control3DEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisControl3DEventArgs_t4095025701_m2144139516_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisControl3DEventArgs_t4095025701_m2144139516(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisControl3DEventArgs_t4095025701_m2144139516_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerActionsEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t1439457463_m3041630280(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t1439457463 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t1439457463 **, UnityAction_2_t1439457463 *, UnityAction_2_t1439457463 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.ControllerActionsEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisControllerActionsEventArgs_t344001476_m1224094621_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisControllerActionsEventArgs_t344001476_m1224094621(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisControllerActionsEventArgs_t344001476_m1224094621_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerInteractionEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t1383093526_m1269146281(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t1383093526 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t1383093526 **, UnityAction_2_t1383093526 *, UnityAction_2_t1383093526 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.ControllerInteractionEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisControllerInteractionEventArgs_t287637539_m202412434_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisControllerInteractionEventArgs_t287637539_m202412434(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisControllerInteractionEventArgs_t287637539_m202412434_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.DashTeleportEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t3292709229_m1792972758(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t3292709229 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t3292709229 **, UnityAction_2_t3292709229 *, UnityAction_2_t3292709229 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.DashTeleportEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisDashTeleportEventArgs_t2197253242_m2792830099_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisDashTeleportEventArgs_t2197253242_m2792830099(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisDashTeleportEventArgs_t2197253242_m2792830099_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.DestinationMarkerEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t2947907648_m894001813(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t2947907648 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t2947907648 **, UnityAction_2_t2947907648 *, UnityAction_2_t2947907648 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.DestinationMarkerEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisDestinationMarkerEventArgs_t1852451661_m306914552_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisDestinationMarkerEventArgs_t1852451661_m306914552(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisDestinationMarkerEventArgs_t1852451661_m306914552_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetCollisionEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t2337829374_m4082282447(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t2337829374 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t2337829374 **, UnityAction_2_t2337829374 *, UnityAction_2_t2337829374 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.HeadsetCollisionEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisHeadsetCollisionEventArgs_t1242373387_m3332629616_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisHeadsetCollisionEventArgs_t1242373387_m3332629616(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisHeadsetCollisionEventArgs_t1242373387_m3332629616_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t3748987708_m3198554489(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t3748987708 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t3748987708 **, UnityAction_2_t3748987708 *, UnityAction_2_t3748987708 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.HeadsetControllerAwareEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisHeadsetControllerAwareEventArgs_t2653531721_m145694514_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisHeadsetControllerAwareEventArgs_t2653531721_m145694514(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisHeadsetControllerAwareEventArgs_t2653531721_m145694514_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetFadeEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t3987998006_m2810318355(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t3987998006 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t3987998006 **, UnityAction_2_t3987998006 *, UnityAction_2_t3987998006 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.HeadsetFadeEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisHeadsetFadeEventArgs_t2892542019_m903745134_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisHeadsetFadeEventArgs_t2892542019_m903745134(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisHeadsetFadeEventArgs_t2892542019_m903745134_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.InteractableObjectEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t1568631543_m4142570804(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t1568631543 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t1568631543 **, UnityAction_2_t1568631543 *, UnityAction_2_t1568631543 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.InteractableObjectEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisInteractableObjectEventArgs_t473175556_m3792134813_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisInteractableObjectEventArgs_t473175556_m3792134813(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisInteractableObjectEventArgs_t473175556_m3792134813_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.ObjectControlEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t3554946306_m1864899749(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t3554946306 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t3554946306 **, UnityAction_2_t3554946306 *, UnityAction_2_t3554946306 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.ObjectControlEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisObjectControlEventArgs_t2459490319_m802719782_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObjectControlEventArgs_t2459490319_m802719782(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisObjectControlEventArgs_t2459490319_m802719782_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.ObjectInteractEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t1866747229_m344140636(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t1866747229 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t1866747229 **, UnityAction_2_t1866747229 *, UnityAction_2_t1866747229 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.ObjectInteractEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisObjectInteractEventArgs_t771291242_m250022255_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObjectInteractEventArgs_t771291242_m250022255(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisObjectInteractEventArgs_t771291242_m250022255_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.PlayerClimbEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t3633041732_m3980309569(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t3633041732 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t3633041732 **, UnityAction_2_t3633041732 *, UnityAction_2_t3633041732 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.PlayerClimbEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisPlayerClimbEventArgs_t2537585745_m3115973524_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisPlayerClimbEventArgs_t2537585745_m3115973524(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisPlayerClimbEventArgs_t2537585745_m3115973524_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.SnapDropZoneEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t1514158761_m1932256042(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t1514158761 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t1514158761 **, UnityAction_2_t1514158761 *, UnityAction_2_t1514158761 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.SnapDropZoneEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisSnapDropZoneEventArgs_t418702774_m313022167_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisSnapDropZoneEventArgs_t418702774_m313022167(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisSnapDropZoneEventArgs_t418702774_m313022167_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Object,VRTK.UIPointerEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t2267441965_m2269895564(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t2267441965 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t2267441965 **, UnityAction_2_t2267441965 *, UnityAction_2_t2267441965 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<VRTK.UIPointerEventArgs>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisUIPointerEventArgs_t1171985978_m1078271303_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisUIPointerEventArgs_t1171985978_m1078271303(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisUIPointerEventArgs_t1171985978_m1078271303_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`2<System.Single,System.Single>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_2_t134459182_m216652323(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_2_t134459182 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t134459182 **, UnityAction_2_t134459182 *, UnityAction_2_t134459182 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_3_t3482433968_m192763766(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_3_t3482433968 * (*) (Il2CppObject * /* static, unused */, UnityAction_3_t3482433968 **, UnityAction_3_t3482433968 *, UnityAction_3_t3482433968 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`3<UnityEngine.Color,System.Single,System.Boolean>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_3_t1816056522_m3239023552(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_3_t1816056522 * (*) (Il2CppObject * /* static, unused */, UnityAction_3_t1816056522 **, UnityAction_3_t1816056522 *, UnityAction_3_t1816056522 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::.ctor()
extern "C"  void U3CSortU3Ec__Iterator21__ctor_m435832983_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TElement System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.Generic.IEnumerator<TElement>.get_Current()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m3627878530_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m4063555545_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m2220308456_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CSortU3Ec__Iterator21_t2488605192 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CSortU3Ec__Iterator21_t2488605192 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CSortU3Ec__Iterator21_t2488605192 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.Generic.IEnumerable<TElement>.GetEnumerator()
extern "C"  Il2CppObject* U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m2832696871_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	U3CSortU3Ec__Iterator21_t2488605192 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CSortU3Ec__Iterator21_t2488605192 * L_2 = (U3CSortU3Ec__Iterator21_t2488605192 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CSortU3Ec__Iterator21_t2488605192 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CSortU3Ec__Iterator21_t2488605192 *)L_2;
		U3CSortU3Ec__Iterator21_t2488605192 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CSortU3Ec__Iterator21_t2488605192 * L_5 = V_0;
		SortContext_1_t1798778454 * L_6 = (SortContext_1_t1798778454 *)__this->get_U3CU24U3Econtext_7();
		NullCheck(L_5);
		L_5->set_context_1(L_6);
		U3CSortU3Ec__Iterator21_t2488605192 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::MoveNext()
extern "C"  bool U3CSortU3Ec__Iterator21_MoveNext_m2095803797_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0083;
		}
	}
	{
		goto IL_00b0;
	}

IL_0021:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		SortContext_1_t1798778454 * L_3 = (SortContext_1_t1798778454 *)__this->get_context_1();
		QuickSort_1_t1970792956 * L_4 = (QuickSort_1_t1970792956 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (QuickSort_1_t1970792956 *, Il2CppObject*, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_4, (Il2CppObject*)L_2, (SortContext_1_t1798778454 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		__this->set_U3CsorterU3E__0_2(L_4);
		QuickSort_1_t1970792956 * L_5 = (QuickSort_1_t1970792956 *)__this->get_U3CsorterU3E__0_2();
		NullCheck((QuickSort_1_t1970792956 *)L_5);
		((  void (*) (QuickSort_1_t1970792956 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		__this->set_U3CiU3E__1_3(0);
		goto IL_0091;
	}

IL_004f:
	{
		QuickSort_1_t1970792956 * L_6 = (QuickSort_1_t1970792956 *)__this->get_U3CsorterU3E__0_2();
		NullCheck(L_6);
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)L_6->get_elements_0();
		QuickSort_1_t1970792956 * L_8 = (QuickSort_1_t1970792956 *)__this->get_U3CsorterU3E__0_2();
		NullCheck(L_8);
		Int32U5BU5D_t3030399641* L_9 = (Int32U5BU5D_t3030399641*)L_8->get_indexes_1();
		int32_t L_10 = (int32_t)__this->get_U3CiU3E__1_3();
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		__this->set_U24current_5(L_14);
		__this->set_U24PC_4(1);
		goto IL_00b2;
	}

IL_0083:
	{
		int32_t L_15 = (int32_t)__this->get_U3CiU3E__1_3();
		__this->set_U3CiU3E__1_3(((int32_t)((int32_t)L_15+(int32_t)1)));
	}

IL_0091:
	{
		int32_t L_16 = (int32_t)__this->get_U3CiU3E__1_3();
		QuickSort_1_t1970792956 * L_17 = (QuickSort_1_t1970792956 *)__this->get_U3CsorterU3E__0_2();
		NullCheck(L_17);
		Int32U5BU5D_t3030399641* L_18 = (Int32U5BU5D_t3030399641*)L_17->get_indexes_1();
		NullCheck(L_18);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_004f;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_00b0:
	{
		return (bool)0;
	}

IL_00b2:
	{
		return (bool)1;
	}
	// Dead block : IL_00b4: ldloc.1
}
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::Dispose()
extern "C"  void U3CSortU3Ec__Iterator21_Dispose_m2211739520_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSortU3Ec__Iterator21_Reset_m2399833958_MetadataUsageId;
extern "C"  void U3CSortU3Ec__Iterator21_Reset_m2399833958_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSortU3Ec__Iterator21_Reset_m2399833958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Linq.SortContext`1<TElement>)
extern "C"  void QuickSort_1__ctor_m1650062348_gshared (QuickSort_1_t1970792956 * __this, Il2CppObject* ___source0, SortContext_1_t1798778454 * ___context1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___source0;
		ObjectU5BU5D_t3614634134* L_1 = ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_elements_0(L_1);
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_elements_0();
		NullCheck(L_2);
		Int32U5BU5D_t3030399641* L_3 = ((  Int32U5BU5D_t3030399641* (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_indexes_1(L_3);
		SortContext_1_t1798778454 * L_4 = ___context1;
		__this->set_context_2(L_4);
		return;
	}
}
// System.Int32[] System.Linq.QuickSort`1<System.Object>::CreateIndexes(System.Int32)
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern const uint32_t QuickSort_1_CreateIndexes_m2577858579_MetadataUsageId;
extern "C"  Int32U5BU5D_t3030399641* QuickSort_1_CreateIndexes_m2577858579_gshared (Il2CppObject * __this /* static, unused */, int32_t ___length0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuickSort_1_CreateIndexes_m2577858579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t3030399641* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___length0;
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)L_0));
		V_1 = (int32_t)0;
		goto IL_0016;
	}

IL_000e:
	{
		Int32U5BU5D_t3030399641* L_1 = V_0;
		int32_t L_2 = V_1;
		int32_t L_3 = V_1;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (int32_t)L_3);
		int32_t L_4 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0016:
	{
		int32_t L_5 = V_1;
		int32_t L_6 = ___length0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_000e;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_7 = V_0;
		return L_7;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::PerformSort()
extern "C"  void QuickSort_1_PerformSort_m3295377581_gshared (QuickSort_1_t1970792956 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_elements_0();
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) > ((int32_t)1)))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}

IL_000f:
	{
		SortContext_1_t1798778454 * L_1 = (SortContext_1_t1798778454 *)__this->get_context_2();
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_elements_0();
		NullCheck((SortContext_1_t1798778454 *)L_1);
		VirtActionInvoker1< ObjectU5BU5D_t3614634134* >::Invoke(4 /* System.Void System.Linq.SortContext`1<System.Object>::Initialize(TElement[]) */, (SortContext_1_t1798778454 *)L_1, (ObjectU5BU5D_t3614634134*)L_2);
		Int32U5BU5D_t3030399641* L_3 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		NullCheck(L_3);
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)0, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Int32 System.Linq.QuickSort`1<System.Object>::CompareItems(System.Int32,System.Int32)
extern "C"  int32_t QuickSort_1_CompareItems_m2598468721_gshared (QuickSort_1_t1970792956 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method)
{
	{
		SortContext_1_t1798778454 * L_0 = (SortContext_1_t1798778454 *)__this->get_context_2();
		int32_t L_1 = ___first_index0;
		int32_t L_2 = ___second_index1;
		NullCheck((SortContext_1_t1798778454 *)L_0);
		int32_t L_3 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Linq.SortContext`1<System.Object>::Compare(System.Int32,System.Int32) */, (SortContext_1_t1798778454 *)L_0, (int32_t)L_1, (int32_t)L_2);
		return L_3;
	}
}
// System.Int32 System.Linq.QuickSort`1<System.Object>::MedianOfThree(System.Int32,System.Int32)
extern "C"  int32_t QuickSort_1_MedianOfThree_m968647497_gshared (QuickSort_1_t1970792956 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___left0;
		int32_t L_1 = ___right1;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1))/(int32_t)2));
		Int32U5BU5D_t3030399641* L_2 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		int32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		Int32U5BU5D_t3030399641* L_6 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_7 = ___left0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		int32_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_10 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_5, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_11 = ___left0;
		int32_t L_12 = V_0;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
	}

IL_002a:
	{
		Int32U5BU5D_t3030399641* L_13 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_14 = ___right1;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		int32_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		Int32U5BU5D_t3030399641* L_17 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_18 = ___left0;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		int32_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_21 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_16, (int32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_21) >= ((int32_t)0)))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_22 = ___left0;
		int32_t L_23 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_22, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
	}

IL_004e:
	{
		Int32U5BU5D_t3030399641* L_24 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_25 = ___right1;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		int32_t L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		Int32U5BU5D_t3030399641* L_28 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_29 = V_0;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		int32_t L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_32 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_27, (int32_t)L_31, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_32) >= ((int32_t)0)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_33 = V_0;
		int32_t L_34 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_33, (int32_t)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
	}

IL_0072:
	{
		int32_t L_35 = V_0;
		int32_t L_36 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_35, (int32_t)((int32_t)((int32_t)L_36-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		Int32U5BU5D_t3030399641* L_37 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_38 = ___right1;
		NullCheck(L_37);
		int32_t L_39 = ((int32_t)((int32_t)L_38-(int32_t)1));
		int32_t L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		return L_40;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::Sort(System.Int32,System.Int32)
extern "C"  void QuickSort_1_Sort_m700141710_gshared (QuickSort_1_t1970792956 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___left0;
		int32_t L_1 = ___right1;
		if ((((int32_t)((int32_t)((int32_t)L_0+(int32_t)3))) > ((int32_t)L_1)))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_2 = ___left0;
		V_0 = (int32_t)L_2;
		int32_t L_3 = ___right1;
		V_1 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		int32_t L_4 = ___left0;
		int32_t L_5 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_6 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_2 = (int32_t)L_6;
	}

IL_0018:
	{
		goto IL_001d;
	}

IL_001d:
	{
		Int32U5BU5D_t3030399641* L_7 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		V_0 = (int32_t)L_9;
		NullCheck(L_7);
		int32_t L_10 = L_9;
		int32_t L_11 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		int32_t L_12 = V_2;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_13 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_13) < ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		goto IL_003b;
	}

IL_003b:
	{
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_15 = V_1;
		int32_t L_16 = (int32_t)((int32_t)((int32_t)L_15-(int32_t)1));
		V_1 = (int32_t)L_16;
		NullCheck(L_14);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		int32_t L_19 = V_2;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_20 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_18, (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_20) > ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_21 = V_0;
		int32_t L_22 = V_1;
		if ((((int32_t)L_21) >= ((int32_t)L_22)))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_23 = V_0;
		int32_t L_24 = V_1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_23, (int32_t)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		goto IL_006d;
	}

IL_0068:
	{
		goto IL_0072;
	}

IL_006d:
	{
		goto IL_0018;
	}

IL_0072:
	{
		int32_t L_25 = V_0;
		int32_t L_26 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_25, (int32_t)((int32_t)((int32_t)L_26-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		int32_t L_27 = ___left0;
		int32_t L_28 = V_0;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_27, (int32_t)((int32_t)((int32_t)L_28-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_29 = V_0;
		int32_t L_30 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)((int32_t)((int32_t)L_29+(int32_t)1)), (int32_t)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		goto IL_009d;
	}

IL_0095:
	{
		int32_t L_31 = ___left0;
		int32_t L_32 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_31, (int32_t)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
	}

IL_009d:
	{
		return;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::InsertionSort(System.Int32,System.Int32)
extern "C"  void QuickSort_1_InsertionSort_m3575279495_gshared (QuickSort_1_t1970792956 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___left0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)1));
		goto IL_005a;
	}

IL_0009:
	{
		Int32U5BU5D_t3030399641* L_1 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = (int32_t)L_4;
		int32_t L_5 = V_0;
		V_1 = (int32_t)L_5;
		goto IL_002f;
	}

IL_0019:
	{
		Int32U5BU5D_t3030399641* L_6 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_7 = V_1;
		Int32U5BU5D_t3030399641* L_8 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_9 = V_1;
		NullCheck(L_8);
		int32_t L_10 = ((int32_t)((int32_t)L_9-(int32_t)1));
		int32_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (int32_t)L_11);
		int32_t L_12 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_12-(int32_t)1));
	}

IL_002f:
	{
		int32_t L_13 = V_1;
		int32_t L_14 = ___left0;
		if ((((int32_t)L_13) <= ((int32_t)L_14)))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_15 = V_2;
		Int32U5BU5D_t3030399641* L_16 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_17 = V_1;
		NullCheck(L_16);
		int32_t L_18 = ((int32_t)((int32_t)L_17-(int32_t)1));
		int32_t L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_20 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_15, (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_20) < ((int32_t)0)))
		{
			goto IL_0019;
		}
	}

IL_004d:
	{
		Int32U5BU5D_t3030399641* L_21 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_22 = V_1;
		int32_t L_23 = V_2;
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (int32_t)L_23);
		int32_t L_24 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_005a:
	{
		int32_t L_25 = V_0;
		int32_t L_26 = ___right1;
		if ((((int32_t)L_25) <= ((int32_t)L_26)))
		{
			goto IL_0009;
		}
	}
	{
		return;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::Swap(System.Int32,System.Int32)
extern "C"  void QuickSort_1_Swap_m1740429939_gshared (QuickSort_1_t1970792956 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_1 = ___right1;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = (int32_t)L_3;
		Int32U5BU5D_t3030399641* L_4 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_5 = ___right1;
		Int32U5BU5D_t3030399641* L_6 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_7 = ___left0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		int32_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_9);
		Int32U5BU5D_t3030399641* L_10 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_11 = ___left0;
		int32_t L_12 = V_0;
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (int32_t)L_12);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.QuickSort`1<System.Object>::Sort(System.Collections.Generic.IEnumerable`1<TElement>,System.Linq.SortContext`1<TElement>)
extern "C"  Il2CppObject* QuickSort_1_Sort_m2490553768_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, SortContext_1_t1798778454 * ___context1, const MethodInfo* method)
{
	U3CSortU3Ec__Iterator21_t2488605192 * V_0 = NULL;
	{
		U3CSortU3Ec__Iterator21_t2488605192 * L_0 = (U3CSortU3Ec__Iterator21_t2488605192 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3CSortU3Ec__Iterator21_t2488605192 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (U3CSortU3Ec__Iterator21_t2488605192 *)L_0;
		U3CSortU3Ec__Iterator21_t2488605192 * L_1 = V_0;
		Il2CppObject* L_2 = ___source0;
		NullCheck(L_1);
		L_1->set_source_0(L_2);
		U3CSortU3Ec__Iterator21_t2488605192 * L_3 = V_0;
		SortContext_1_t1798778454 * L_4 = ___context1;
		NullCheck(L_3);
		L_3->set_context_1(L_4);
		U3CSortU3Ec__Iterator21_t2488605192 * L_5 = V_0;
		Il2CppObject* L_6 = ___source0;
		NullCheck(L_5);
		L_5->set_U3CU24U3Esource_6(L_6);
		U3CSortU3Ec__Iterator21_t2488605192 * L_7 = V_0;
		SortContext_1_t1798778454 * L_8 = ___context1;
		NullCheck(L_7);
		L_7->set_U3CU24U3Econtext_7(L_8);
		U3CSortU3Ec__Iterator21_t2488605192 * L_9 = V_0;
		U3CSortU3Ec__Iterator21_t2488605192 * L_10 = (U3CSortU3Ec__Iterator21_t2488605192 *)L_9;
		NullCheck(L_10);
		L_10->set_U24PC_4(((int32_t)-2));
		return L_10;
	}
}
// System.Void System.Linq.SortContext`1<System.Object>::.ctor(System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortContext_1__ctor_m1159198615_gshared (SortContext_1_t1798778454 * __this, int32_t ___direction0, SortContext_1_t1798778454 * ___child_context1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___direction0;
		__this->set_direction_0(L_0);
		SortContext_1_t1798778454 * L_1 = ___child_context1;
		__this->set_child_context_1(L_1);
		return;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Object>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortSequenceContext_2__ctor_m2181983522_gshared (SortSequenceContext_2_t3061295073 * __this, Func_2_t2825504181 * ___selector0, Il2CppObject* ___comparer1, int32_t ___direction2, SortContext_1_t1798778454 * ___child_context3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___direction2;
		SortContext_1_t1798778454 * L_1 = ___child_context3;
		NullCheck((SortContext_1_t1798778454 *)__this);
		((  void (*) (SortContext_1_t1798778454 *, int32_t, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((SortContext_1_t1798778454 *)__this, (int32_t)L_0, (SortContext_1_t1798778454 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t2825504181 * L_2 = ___selector0;
		__this->set_selector_2(L_2);
		Il2CppObject* L_3 = ___comparer1;
		__this->set_comparer_3(L_3);
		return;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Object>::Initialize(TElement[])
extern "C"  void SortSequenceContext_2_Initialize_m2437603572_gshared (SortSequenceContext_2_t3061295073 * __this, ObjectU5BU5D_t3614634134* ___elements0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SortContext_1_t1798778454 * L_0 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		SortContext_1_t1798778454 * L_1 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		ObjectU5BU5D_t3614634134* L_2 = ___elements0;
		NullCheck((SortContext_1_t1798778454 *)L_1);
		VirtActionInvoker1< ObjectU5BU5D_t3614634134* >::Invoke(4 /* System.Void System.Linq.SortContext`1<System.Object>::Initialize(TElement[]) */, (SortContext_1_t1798778454 *)L_1, (ObjectU5BU5D_t3614634134*)L_2);
	}

IL_0017:
	{
		ObjectU5BU5D_t3614634134* L_3 = ___elements0;
		NullCheck(L_3);
		__this->set_keys_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))));
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_002c:
	{
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get_keys_4();
		int32_t L_5 = V_0;
		Func_2_t2825504181 * L_6 = (Func_2_t2825504181 *)__this->get_selector_2();
		ObjectU5BU5D_t3614634134* L_7 = ___elements0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((Func_2_t2825504181 *)L_6);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (Func_2_t2825504181 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Func_2_t2825504181 *)L_6, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_11);
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_13 = V_0;
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)__this->get_keys_4();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		return;
	}
}
// System.Int32 System.Linq.SortSequenceContext`2<System.Object,System.Object>::Compare(System.Int32,System.Int32)
extern "C"  int32_t SortSequenceContext_2_Compare_m259092091_gshared (SortSequenceContext_2_t3061295073 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B10_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_comparer_3();
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get_keys_4();
		int32_t L_2 = ___first_index0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)__this->get_keys_4();
		int32_t L_6 = ___second_index1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck((Il2CppObject*)L_0);
		int32_t L_9 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject *)L_4, (Il2CppObject *)L_8);
		V_0 = (int32_t)L_9;
		int32_t L_10 = V_0;
		if (L_10)
		{
			goto IL_005b;
		}
	}
	{
		SortContext_1_t1798778454 * L_11 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		SortContext_1_t1798778454 * L_12 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		int32_t L_13 = ___first_index0;
		int32_t L_14 = ___second_index1;
		NullCheck((SortContext_1_t1798778454 *)L_12);
		int32_t L_15 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Linq.SortContext`1<System.Object>::Compare(System.Int32,System.Int32) */, (SortContext_1_t1798778454 *)L_12, (int32_t)L_13, (int32_t)L_14);
		return L_15;
	}

IL_0043:
	{
		int32_t L_16 = (int32_t)((SortContext_1_t1798778454 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_17 = ___second_index1;
		int32_t L_18 = ___first_index0;
		G_B6_0 = ((int32_t)((int32_t)L_17-(int32_t)L_18));
		goto IL_005a;
	}

IL_0057:
	{
		int32_t L_19 = ___first_index0;
		int32_t L_20 = ___second_index1;
		G_B6_0 = ((int32_t)((int32_t)L_19-(int32_t)L_20));
	}

IL_005a:
	{
		V_0 = (int32_t)G_B6_0;
	}

IL_005b:
	{
		int32_t L_21 = (int32_t)((SortContext_1_t1798778454 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_22 = V_0;
		G_B10_0 = ((-L_22));
		goto IL_006f;
	}

IL_006e:
	{
		int32_t L_23 = V_0;
		G_B10_0 = L_23;
	}

IL_006f:
	{
		return G_B10_0;
	}
}
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C"  void Nullable_1__ctor_m796575255_gshared (Nullable_1_t1693325264 * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		TimeSpan_t3430258949  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m796575255_AdjustorThunk (Il2CppObject * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m796575255(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3663286555_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3663286555_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3663286555(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m1743067844_MetadataUsageId;
extern "C"  TimeSpan_t3430258949  Nullable_1_get_Value_m1743067844_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1743067844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		TimeSpan_t3430258949  L_2 = (TimeSpan_t3430258949 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  TimeSpan_t3430258949  Nullable_1_get_Value_m1743067844_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	TimeSpan_t3430258949  _returnValue = Nullable_1_get_Value_m1743067844(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3860982732_gshared (Nullable_1_t1693325264 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1693325264 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m1889119397((Nullable_1_t1693325264 *)__this, (Nullable_1_t1693325264 )((*(Nullable_1_t1693325264 *)((Nullable_1_t1693325264 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3860982732_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3860982732(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1889119397_gshared (Nullable_1_t1693325264 * __this, Nullable_1_t1693325264  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		TimeSpan_t3430258949 * L_3 = (TimeSpan_t3430258949 *)(&___other0)->get_address_of_value_0();
		TimeSpan_t3430258949  L_4 = (TimeSpan_t3430258949 )__this->get_value_0();
		TimeSpan_t3430258949  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = TimeSpan_Equals_m4102942751((TimeSpan_t3430258949 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1889119397_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1693325264  ___other0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1889119397(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1791015856_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		TimeSpan_t3430258949 * L_1 = (TimeSpan_t3430258949 *)__this->get_address_of_value_0();
		int32_t L_2 = TimeSpan_GetHashCode_m550404245((TimeSpan_t3430258949 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1791015856_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1791015856(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1238126148_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1238126148_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1238126148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		TimeSpan_t3430258949 * L_1 = (TimeSpan_t3430258949 *)__this->get_address_of_value_0();
		String_t* L_2 = TimeSpan_ToString_m2947282901((TimeSpan_t3430258949 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1238126148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1238126148(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<UnityEngine.Color>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3779957181_gshared (Nullable_1_t283458390 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Color_t2020392075  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3779957181_AdjustorThunk (Il2CppObject * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3779957181(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<UnityEngine.Color>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1809895772_gshared (Nullable_1_t283458390 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1809895772_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1809895772(&_thisAdjusted, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.Color>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m1839860490_MetadataUsageId;
extern "C"  Color_t2020392075  Nullable_1_get_Value_m1839860490_gshared (Nullable_1_t283458390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1839860490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Color_t2020392075  L_2 = (Color_t2020392075 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  Color_t2020392075  Nullable_1_get_Value_m1839860490_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Color_t2020392075  _returnValue = Nullable_1_get_Value_m1839860490(&_thisAdjusted, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<UnityEngine.Color>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2692902174_gshared (Nullable_1_t283458390 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t283458390 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m1377231269((Nullable_1_t283458390 *)__this, (Nullable_1_t283458390 )((*(Nullable_1_t283458390 *)((Nullable_1_t283458390 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m2692902174_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2692902174(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<UnityEngine.Color>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1377231269_gshared (Nullable_1_t283458390 * __this, Nullable_1_t283458390  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Color_t2020392075 * L_3 = (Color_t2020392075 *)(&___other0)->get_address_of_value_0();
		Color_t2020392075  L_4 = (Color_t2020392075 )__this->get_value_0();
		Color_t2020392075  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Color_Equals_m661618137((Color_t2020392075 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1377231269_AdjustorThunk (Il2CppObject * __this, Nullable_1_t283458390  ___other0, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1377231269(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<UnityEngine.Color>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m730951602_gshared (Nullable_1_t283458390 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Color_t2020392075 * L_1 = (Color_t2020392075 *)__this->get_address_of_value_0();
		int32_t L_2 = Color_GetHashCode_m3182525367((Color_t2020392075 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m730951602_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m730951602(&_thisAdjusted, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<UnityEngine.Color>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1180746670_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1180746670_gshared (Nullable_1_t283458390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1180746670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Color_t2020392075 * L_1 = (Color_t2020392075 *)__this->get_address_of_value_0();
		String_t* L_2 = Color_ToString_m4028093047((Color_t2020392075 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1180746670_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1180746670(&_thisAdjusted, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<UnityEngine.EventSystems.RaycastResult>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3769688082_gshared (Nullable_1_t2579219987 * __this, RaycastResult_t21186376  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		RaycastResult_t21186376  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3769688082_AdjustorThunk (Il2CppObject * __this, RaycastResult_t21186376  ___value0, const MethodInfo* method)
{
	Nullable_1_t2579219987  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<RaycastResult_t21186376 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3769688082(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<RaycastResult_t21186376 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<UnityEngine.EventSystems.RaycastResult>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3271298471_gshared (Nullable_1_t2579219987 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3271298471_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2579219987  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<RaycastResult_t21186376 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3271298471(&_thisAdjusted, method);
	*reinterpret_cast<RaycastResult_t21186376 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.EventSystems.RaycastResult>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m2830962711_MetadataUsageId;
extern "C"  RaycastResult_t21186376  Nullable_1_get_Value_m2830962711_gshared (Nullable_1_t2579219987 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2830962711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		RaycastResult_t21186376  L_2 = (RaycastResult_t21186376 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  RaycastResult_t21186376  Nullable_1_get_Value_m2830962711_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2579219987  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<RaycastResult_t21186376 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	RaycastResult_t21186376  _returnValue = Nullable_1_get_Value_m2830962711(&_thisAdjusted, method);
	*reinterpret_cast<RaycastResult_t21186376 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<UnityEngine.EventSystems.RaycastResult>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1931672953_gshared (Nullable_1_t2579219987 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2579219987 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m3001753250((Nullable_1_t2579219987 *)__this, (Nullable_1_t2579219987 )((*(Nullable_1_t2579219987 *)((Nullable_1_t2579219987 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1931672953_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t2579219987  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<RaycastResult_t21186376 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1931672953(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<RaycastResult_t21186376 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<UnityEngine.EventSystems.RaycastResult>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3001753250_gshared (Nullable_1_t2579219987 * __this, Nullable_1_t2579219987  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		RaycastResult_t21186376 * L_3 = (RaycastResult_t21186376 *)(&___other0)->get_address_of_value_0();
		RaycastResult_t21186376  L_4 = (RaycastResult_t21186376 )__this->get_value_0();
		RaycastResult_t21186376  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t3507792607 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m3001753250_AdjustorThunk (Il2CppObject * __this, Nullable_1_t2579219987  ___other0, const MethodInfo* method)
{
	Nullable_1_t2579219987  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<RaycastResult_t21186376 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3001753250(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<RaycastResult_t21186376 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<UnityEngine.EventSystems.RaycastResult>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1744192955_gshared (Nullable_1_t2579219987 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		RaycastResult_t21186376 * L_1 = (RaycastResult_t21186376 *)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1744192955_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2579219987  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<RaycastResult_t21186376 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1744192955(&_thisAdjusted, method);
	*reinterpret_cast<RaycastResult_t21186376 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<UnityEngine.EventSystems.RaycastResult>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3325861011_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3325861011_gshared (Nullable_1_t2579219987 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3325861011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		RaycastResult_t21186376 * L_1 = (RaycastResult_t21186376 *)__this->get_address_of_value_0();
		String_t* L_2 = RaycastResult_ToString_m2233441736((RaycastResult_t21186376 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3325861011_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2579219987  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<RaycastResult_t21186376 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3325861011(&_thisAdjusted, method);
	*reinterpret_cast<RaycastResult_t21186376 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Predicate`1<Gvr.Internal.EmulatorTouchEvent/Pointer>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2879268465_gshared (Predicate_1_t1443655117 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Gvr.Internal.EmulatorTouchEvent/Pointer>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2149240373_gshared (Predicate_1_t1443655117 * __this, Pointer_t3000685002  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2149240373((Predicate_1_t1443655117 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Pointer_t3000685002  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Pointer_t3000685002  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Gvr.Internal.EmulatorTouchEvent/Pointer>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Pointer_t3000685002_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3833021142_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3833021142_gshared (Predicate_1_t1443655117 * __this, Pointer_t3000685002  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3833021142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Pointer_t3000685002_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Gvr.Internal.EmulatorTouchEvent/Pointer>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2789001523_gshared (Predicate_1_t1443655117 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<proto.PhoneEvent/Types/Type>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2660501334_gshared (Predicate_1_t4268418272 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<proto.PhoneEvent/Types/Type>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3075390858_gshared (Predicate_1_t4268418272 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3075390858((Predicate_1_t4268418272 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<proto.PhoneEvent/Types/Type>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Type_t1530480861_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2374000957_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2374000957_gshared (Predicate_1_t4268418272 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2374000957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Type_t1530480861_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<proto.PhoneEvent/Types/Type>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2986015248_gshared (Predicate_1_t4268418272 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2898931145_gshared (Predicate_1_t2996539675 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4054137757_gshared (Predicate_1_t2996539675 * __this, KeyValuePair_2_t258602264  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m4054137757((Predicate_1_t2996539675 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t258602264  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, KeyValuePair_2_t258602264  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t258602264_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2924426472_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2924426472_gshared (Predicate_1_t2996539675 * __this, KeyValuePair_2_t258602264  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2924426472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyValuePair_2_t258602264_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2875564199_gshared (Predicate_1_t2996539675 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2826800414_gshared (Predicate_1_t514847563 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Int32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m695569038_gshared (Predicate_1_t514847563 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m695569038((Predicate_1_t514847563 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2559992383_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2559992383_gshared (Predicate_1_t514847563 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2559992383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1202813828_gshared (Predicate_1_t514847563 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2289454599_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Object>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4047721271_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m4047721271((Predicate_1_t1132419410 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3556950370_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3656575065_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1767993638_gshared (Predicate_1_t2832094954 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m527131606_gshared (Predicate_1_t2832094954 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m527131606((Predicate_1_t2832094954 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1448216027_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1448216027_gshared (Predicate_1_t2832094954 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1448216027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m215026240_gshared (Predicate_1_t2832094954 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1292402863_gshared (Predicate_1_t4236135325 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2060780095_gshared (Predicate_1_t4236135325 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2060780095((Predicate_1_t4236135325 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1856151290_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1856151290_gshared (Predicate_1_t4236135325 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1856151290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m259774785_gshared (Predicate_1_t4236135325 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1337187919_gshared (Predicate_1_t519480047 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Single>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2581621416_gshared (Predicate_1_t519480047 * __this, float ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2581621416((Predicate_1_t519480047 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, float ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, float ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3791856215_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3791856215_gshared (Predicate_1_t519480047 * __this, float ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3791856215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3081660750_gshared (Predicate_1_t519480047 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3811123782_gshared (Predicate_1_t3612454929 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Color32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m122788314_gshared (Predicate_1_t3612454929 * __this, Color32_t874517518  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m122788314((Predicate_1_t3612454929 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Color32_t874517518  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Color32_t874517518  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Color32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color32_t874517518_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2959352225_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2959352225_gshared (Predicate_1_t3612454929 * __this, Color32_t874517518  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2959352225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color32_t874517518_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Color32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m924884444_gshared (Predicate_1_t3612454929 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.CombineInstance>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m4079842978_gshared (Predicate_1_t2802532621 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.CombineInstance>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m6677534_gshared (Predicate_1_t2802532621 * __this, CombineInstance_t64595210  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m6677534((Predicate_1_t2802532621 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CombineInstance_t64595210  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CombineInstance_t64595210  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.CombineInstance>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CombineInstance_t64595210_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1962015581_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1962015581_gshared (Predicate_1_t2802532621 * __this, CombineInstance_t64595210  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1962015581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CombineInstance_t64595210_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.CombineInstance>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3251210488_gshared (Predicate_1_t2802532621 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1567825400_gshared (Predicate_1_t2759123787 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3860206640_gshared (Predicate_1_t2759123787 * __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3860206640((Predicate_1_t2759123787 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RaycastResult_t21186376_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4068629879_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4068629879_gshared (Predicate_1_t2759123787 * __this, RaycastResult_t21186376  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4068629879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RaycastResult_t21186376_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m973058386_gshared (Predicate_1_t2759123787 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Experimental.Director.Playable>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3105937642_gshared (Predicate_1_t2110515663 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Experimental.Director.Playable>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1617267354_gshared (Predicate_1_t2110515663 * __this, Playable_t3667545548  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1617267354((Predicate_1_t2110515663 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Playable_t3667545548  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Playable_t3667545548  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Experimental.Director.Playable>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Playable_t3667545548_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3423161611_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3423161611_gshared (Predicate_1_t2110515663 * __this, Playable_t3667545548  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3423161611_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Playable_t3667545548_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Experimental.Director.Playable>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2453294608_gshared (Predicate_1_t2110515663 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1020292372_gshared (Predicate_1_t1499606915 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3539717340_gshared (Predicate_1_t1499606915 * __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3539717340((Predicate_1_t1499606915 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UICharInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UICharInfo_t3056636800_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3056726495_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3056726495_gshared (Predicate_1_t1499606915 * __this, UICharInfo_t3056636800  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3056726495_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UICharInfo_t3056636800_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2354180346_gshared (Predicate_1_t1499606915 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m784266182_gshared (Predicate_1_t2064247989 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m577088274_gshared (Predicate_1_t2064247989 * __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m577088274((Predicate_1_t2064247989 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UILineInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UILineInfo_t3621277874_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2329589669_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2329589669_gshared (Predicate_1_t2064247989 * __this, UILineInfo_t3621277874  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2329589669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UILineInfo_t3621277874_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3442731496_gshared (Predicate_1_t2064247989 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UIVertex>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m549279630_gshared (Predicate_1_t3942196229 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2883675618_gshared (Predicate_1_t3942196229 * __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2883675618((Predicate_1_t3942196229 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UIVertex>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UIVertex_t1204258818_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3926587117_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3926587117_gshared (Predicate_1_t3942196229 * __this, UIVertex_t1204258818  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3926587117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UIVertex_t1204258818_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m337889472_gshared (Predicate_1_t3942196229 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2863314033_gshared (Predicate_1_t686677694 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3001657933_gshared (Predicate_1_t686677694 * __this, Vector2_t2243707579  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3001657933((Predicate_1_t686677694 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m866207434_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m866207434_gshared (Predicate_1_t686677694 * __this, Vector2_t2243707579  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m866207434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3406729927_gshared (Predicate_1_t686677694 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3243601712_gshared (Predicate_1_t686677695 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2775223656_gshared (Predicate_1_t686677695 * __this, Vector3_t2243707580  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2775223656((Predicate_1_t686677695 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1764756107_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1764756107_gshared (Predicate_1_t686677695 * __this, Vector3_t2243707580  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1764756107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1035116514_gshared (Predicate_1_t686677695 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2995226103_gshared (Predicate_1_t686677696 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2407726575_gshared (Predicate_1_t686677696 * __this, Vector4_t2243707581  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2407726575((Predicate_1_t686677696 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t2243707581  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector4_t2243707581  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2425667920_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2425667920_gshared (Predicate_1_t686677696 * __this, Vector4_t2243707581  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2425667920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2420144145_gshared (Predicate_1_t686677696 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1797124730_gshared (Predicate_1_t4125655347 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1488049071_gshared (Predicate_1_t4125655347 * __this, AttachedObject_t1387717936  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1488049071((Predicate_1_t4125655347 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, AttachedObject_t1387717936  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, AttachedObject_t1387717936  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* AttachedObject_t1387717936_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3649429664_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3649429664_gshared (Predicate_1_t4125655347 * __this, AttachedObject_t1387717936  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3649429664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AttachedObject_t1387717936_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2415822425_gshared (Predicate_1_t4125655347 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Getter_2__ctor_m653998582_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Getter_2_Invoke_m3338489829_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ____this0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Getter_2_Invoke_m3338489829((Getter_2_t4179406139 *)__this->get_prev_9(),____this0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Getter_2_BeginInvoke_m2080015031_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ____this0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____this0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Getter_2_EndInvoke_m977999903_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Reflection.MonoProperty/StaticGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void StaticGetter_1__ctor_m1290492285_gshared (StaticGetter_1_t1095697167 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * StaticGetter_1_Invoke_m1348877692_gshared (StaticGetter_1_t1095697167 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StaticGetter_1_Invoke_m1348877692((StaticGetter_1_t1095697167 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StaticGetter_1_BeginInvoke_m2732579814_gshared (StaticGetter_1_t1095697167 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * StaticGetter_1_EndInvoke_m44757160_gshared (StaticGetter_1_t1095697167 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m2563320212_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m2563320212_gshared (CachedInvokableCall_1_t2619124609 * __this, Object_t1021602117 * ___target0, MethodInfo_t * ___theFunction1, bool ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m2563320212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1021602117 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t2019901575 *)__this);
		((  void (*) (InvokableCall_1_t2019901575 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t2019901575 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		bool L_3 = ___argument2;
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m3247299909_gshared (CachedInvokableCall_1_t2619124609 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t2019901575 *)__this);
		((  void (*) (InvokableCall_1_t2019901575 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t2019901575 *)__this, (ObjectU5BU5D_t3614634134*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m127496184_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m127496184_gshared (CachedInvokableCall_1_t865427339 * __this, Object_t1021602117 * ___target0, MethodInfo_t * ___theFunction1, int32_t ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m127496184_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1021602117 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t266204305 *)__this);
		((  void (*) (InvokableCall_1_t266204305 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t266204305 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		int32_t L_3 = ___argument2;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m2815073919_gshared (CachedInvokableCall_1_t865427339 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t266204305 *)__this);
		((  void (*) (InvokableCall_1_t266204305 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t266204305 *)__this, (ObjectU5BU5D_t3614634134*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m79259589_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m79259589_gshared (CachedInvokableCall_1_t1482999186 * __this, Object_t1021602117 * ___target0, MethodInfo_t * ___theFunction1, Il2CppObject * ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m79259589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1021602117 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t883776152 *)__this);
		((  void (*) (InvokableCall_1_t883776152 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t883776152 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		Il2CppObject * L_3 = ___argument2;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m2401236944_gshared (CachedInvokableCall_1_t1482999186 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t883776152 *)__this);
		((  void (*) (InvokableCall_1_t883776152 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t883776152 *)__this, (ObjectU5BU5D_t3614634134*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m3238306320_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m3238306320_gshared (CachedInvokableCall_1_t870059823 * __this, Object_t1021602117 * ___target0, MethodInfo_t * ___theFunction1, float ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m3238306320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1021602117 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t270836789 *)__this);
		((  void (*) (InvokableCall_1_t270836789 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t270836789 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		float L_3 = ___argument2;
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m4097553971_gshared (CachedInvokableCall_1_t870059823 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t270836789 *)__this);
		((  void (*) (InvokableCall_1_t270836789 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t270836789 *)__this, (ObjectU5BU5D_t3614634134*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m874046876_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m874046876_gshared (InvokableCall_1_t2019901575 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m874046876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t2019901575 *)__this);
		((  void (*) (InvokableCall_1_t2019901575 *, UnityAction_1_t897193173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t2019901575 *)__this, (UnityAction_1_t897193173 *)((UnityAction_1_t897193173 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m2693793190_gshared (InvokableCall_1_t2019901575 * __this, UnityAction_1_t897193173 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t897193173 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t2019901575 *)__this);
		((  void (*) (InvokableCall_1_t2019901575 *, UnityAction_1_t897193173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t2019901575 *)__this, (UnityAction_1_t897193173 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m3048312905_gshared (InvokableCall_1_t2019901575 * __this, UnityAction_1_t897193173 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t897193173 * V_0 = NULL;
	UnityAction_1_t897193173 * V_1 = NULL;
	{
		UnityAction_1_t897193173 * L_0 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t897193173 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t897193173 * L_1 = V_0;
		V_1 = (UnityAction_1_t897193173 *)L_1;
		UnityAction_1_t897193173 ** L_2 = (UnityAction_1_t897193173 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t897193173 * L_3 = V_1;
		UnityAction_1_t897193173 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t897193173 * L_6 = V_0;
		UnityAction_1_t897193173 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t897193173 *>((UnityAction_1_t897193173 **)L_2, (UnityAction_1_t897193173 *)((UnityAction_1_t897193173 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t897193173 *)L_6);
		V_0 = (UnityAction_1_t897193173 *)L_7;
		UnityAction_1_t897193173 * L_8 = V_0;
		UnityAction_1_t897193173 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t897193173 *)L_8) == ((Il2CppObject*)(UnityAction_1_t897193173 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m1481038152_gshared (InvokableCall_1_t2019901575 * __this, UnityAction_1_t897193173 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t897193173 * V_0 = NULL;
	UnityAction_1_t897193173 * V_1 = NULL;
	{
		UnityAction_1_t897193173 * L_0 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t897193173 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t897193173 * L_1 = V_0;
		V_1 = (UnityAction_1_t897193173 *)L_1;
		UnityAction_1_t897193173 ** L_2 = (UnityAction_1_t897193173 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t897193173 * L_3 = V_1;
		UnityAction_1_t897193173 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t897193173 * L_6 = V_0;
		UnityAction_1_t897193173 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t897193173 *>((UnityAction_1_t897193173 **)L_2, (UnityAction_1_t897193173 *)((UnityAction_1_t897193173 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t897193173 *)L_6);
		V_0 = (UnityAction_1_t897193173 *)L_7;
		UnityAction_1_t897193173 * L_8 = V_0;
		UnityAction_1_t897193173 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t897193173 *)L_8) == ((Il2CppObject*)(UnityAction_1_t897193173 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m769918017_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m769918017_gshared (InvokableCall_1_t2019901575 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m769918017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t897193173 * L_5 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t897193173 * L_7 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t897193173 *)L_7);
		((  void (*) (UnityAction_1_t897193173 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t897193173 *)L_7, (bool)((*(bool*)((bool*)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m951110817_gshared (InvokableCall_1_t2019901575 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t897193173 * L_0 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t897193173 * L_3 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m231935020_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m231935020_gshared (InvokableCall_1_t266204305 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m231935020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t266204305 *)__this);
		((  void (*) (InvokableCall_1_t266204305 *, UnityAction_1_t3438463199 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t266204305 *)__this, (UnityAction_1_t3438463199 *)((UnityAction_1_t3438463199 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m563785030_gshared (InvokableCall_1_t266204305 * __this, UnityAction_1_t3438463199 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t3438463199 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t266204305 *)__this);
		((  void (*) (InvokableCall_1_t266204305 *, UnityAction_1_t3438463199 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t266204305 *)__this, (UnityAction_1_t3438463199 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m3068046591_gshared (InvokableCall_1_t266204305 * __this, UnityAction_1_t3438463199 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3438463199 * V_0 = NULL;
	UnityAction_1_t3438463199 * V_1 = NULL;
	{
		UnityAction_1_t3438463199 * L_0 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3438463199 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3438463199 * L_1 = V_0;
		V_1 = (UnityAction_1_t3438463199 *)L_1;
		UnityAction_1_t3438463199 ** L_2 = (UnityAction_1_t3438463199 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3438463199 * L_3 = V_1;
		UnityAction_1_t3438463199 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3438463199 * L_6 = V_0;
		UnityAction_1_t3438463199 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3438463199 *>((UnityAction_1_t3438463199 **)L_2, (UnityAction_1_t3438463199 *)((UnityAction_1_t3438463199 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3438463199 *)L_6);
		V_0 = (UnityAction_1_t3438463199 *)L_7;
		UnityAction_1_t3438463199 * L_8 = V_0;
		UnityAction_1_t3438463199 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3438463199 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3438463199 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m3070410248_gshared (InvokableCall_1_t266204305 * __this, UnityAction_1_t3438463199 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3438463199 * V_0 = NULL;
	UnityAction_1_t3438463199 * V_1 = NULL;
	{
		UnityAction_1_t3438463199 * L_0 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3438463199 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3438463199 * L_1 = V_0;
		V_1 = (UnityAction_1_t3438463199 *)L_1;
		UnityAction_1_t3438463199 ** L_2 = (UnityAction_1_t3438463199 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3438463199 * L_3 = V_1;
		UnityAction_1_t3438463199 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3438463199 * L_6 = V_0;
		UnityAction_1_t3438463199 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3438463199 *>((UnityAction_1_t3438463199 **)L_2, (UnityAction_1_t3438463199 *)((UnityAction_1_t3438463199 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3438463199 *)L_6);
		V_0 = (UnityAction_1_t3438463199 *)L_7;
		UnityAction_1_t3438463199 * L_8 = V_0;
		UnityAction_1_t3438463199 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3438463199 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3438463199 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m428957899_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m428957899_gshared (InvokableCall_1_t266204305 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m428957899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3438463199 * L_5 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3438463199 * L_7 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3438463199 *)L_7);
		((  void (*) (UnityAction_1_t3438463199 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3438463199 *)L_7, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Int32>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m2775216619_gshared (InvokableCall_1_t266204305 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t3438463199 * L_0 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t3438463199 * L_3 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m54675381_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m54675381_gshared (InvokableCall_1_t883776152 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m54675381_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t883776152 *)__this);
		((  void (*) (InvokableCall_1_t883776152 *, UnityAction_1_t4056035046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t883776152 *)__this, (UnityAction_1_t4056035046 *)((UnityAction_1_t4056035046 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m833213021_gshared (InvokableCall_1_t883776152 * __this, UnityAction_1_t4056035046 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t4056035046 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t883776152 *)__this);
		((  void (*) (InvokableCall_1_t883776152 *, UnityAction_1_t4056035046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t883776152 *)__this, (UnityAction_1_t4056035046 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m4009721884_gshared (InvokableCall_1_t883776152 * __this, UnityAction_1_t4056035046 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t4056035046 * V_0 = NULL;
	UnityAction_1_t4056035046 * V_1 = NULL;
	{
		UnityAction_1_t4056035046 * L_0 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t4056035046 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t4056035046 * L_1 = V_0;
		V_1 = (UnityAction_1_t4056035046 *)L_1;
		UnityAction_1_t4056035046 ** L_2 = (UnityAction_1_t4056035046 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t4056035046 * L_3 = V_1;
		UnityAction_1_t4056035046 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t4056035046 * L_6 = V_0;
		UnityAction_1_t4056035046 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t4056035046 *>((UnityAction_1_t4056035046 **)L_2, (UnityAction_1_t4056035046 *)((UnityAction_1_t4056035046 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t4056035046 *)L_6);
		V_0 = (UnityAction_1_t4056035046 *)L_7;
		UnityAction_1_t4056035046 * L_8 = V_0;
		UnityAction_1_t4056035046 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t4056035046 *)L_8) == ((Il2CppObject*)(UnityAction_1_t4056035046 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m527482931_gshared (InvokableCall_1_t883776152 * __this, UnityAction_1_t4056035046 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t4056035046 * V_0 = NULL;
	UnityAction_1_t4056035046 * V_1 = NULL;
	{
		UnityAction_1_t4056035046 * L_0 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t4056035046 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t4056035046 * L_1 = V_0;
		V_1 = (UnityAction_1_t4056035046 *)L_1;
		UnityAction_1_t4056035046 ** L_2 = (UnityAction_1_t4056035046 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t4056035046 * L_3 = V_1;
		UnityAction_1_t4056035046 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t4056035046 * L_6 = V_0;
		UnityAction_1_t4056035046 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t4056035046 *>((UnityAction_1_t4056035046 **)L_2, (UnityAction_1_t4056035046 *)((UnityAction_1_t4056035046 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t4056035046 *)L_6);
		V_0 = (UnityAction_1_t4056035046 *)L_7;
		UnityAction_1_t4056035046 * L_8 = V_0;
		UnityAction_1_t4056035046 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t4056035046 *)L_8) == ((Il2CppObject*)(UnityAction_1_t4056035046 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m1715547918_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m1715547918_gshared (InvokableCall_1_t883776152 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m1715547918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t4056035046 * L_5 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t4056035046 * L_7 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t4056035046 *)L_7);
		((  void (*) (UnityAction_1_t4056035046 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t4056035046 *)L_7, (Il2CppObject *)((Il2CppObject *)Castclass(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m1325295794_gshared (InvokableCall_1_t883776152 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t4056035046 * L_0 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t4056035046 * L_3 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m4078762228_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m4078762228_gshared (InvokableCall_1_t270836789 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m4078762228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t270836789 *)__this);
		((  void (*) (InvokableCall_1_t270836789 *, UnityAction_1_t3443095683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t270836789 *)__this, (UnityAction_1_t3443095683 *)((UnityAction_1_t3443095683 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m121193486_gshared (InvokableCall_1_t270836789 * __this, UnityAction_1_t3443095683 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t3443095683 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t270836789 *)__this);
		((  void (*) (InvokableCall_1_t270836789 *, UnityAction_1_t3443095683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t270836789 *)__this, (UnityAction_1_t3443095683 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m3251799843_gshared (InvokableCall_1_t270836789 * __this, UnityAction_1_t3443095683 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3443095683 * V_0 = NULL;
	UnityAction_1_t3443095683 * V_1 = NULL;
	{
		UnityAction_1_t3443095683 * L_0 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3443095683 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3443095683 * L_1 = V_0;
		V_1 = (UnityAction_1_t3443095683 *)L_1;
		UnityAction_1_t3443095683 ** L_2 = (UnityAction_1_t3443095683 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3443095683 * L_3 = V_1;
		UnityAction_1_t3443095683 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3443095683 * L_6 = V_0;
		UnityAction_1_t3443095683 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3443095683 *>((UnityAction_1_t3443095683 **)L_2, (UnityAction_1_t3443095683 *)((UnityAction_1_t3443095683 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3443095683 *)L_6);
		V_0 = (UnityAction_1_t3443095683 *)L_7;
		UnityAction_1_t3443095683 * L_8 = V_0;
		UnityAction_1_t3443095683 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3443095683 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3443095683 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m1744559252_gshared (InvokableCall_1_t270836789 * __this, UnityAction_1_t3443095683 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3443095683 * V_0 = NULL;
	UnityAction_1_t3443095683 * V_1 = NULL;
	{
		UnityAction_1_t3443095683 * L_0 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3443095683 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3443095683 * L_1 = V_0;
		V_1 = (UnityAction_1_t3443095683 *)L_1;
		UnityAction_1_t3443095683 ** L_2 = (UnityAction_1_t3443095683 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3443095683 * L_3 = V_1;
		UnityAction_1_t3443095683 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3443095683 * L_6 = V_0;
		UnityAction_1_t3443095683 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3443095683 *>((UnityAction_1_t3443095683 **)L_2, (UnityAction_1_t3443095683 *)((UnityAction_1_t3443095683 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3443095683 *)L_6);
		V_0 = (UnityAction_1_t3443095683 *)L_7;
		UnityAction_1_t3443095683 * L_8 = V_0;
		UnityAction_1_t3443095683 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3443095683 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3443095683 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m4090512311_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m4090512311_gshared (InvokableCall_1_t270836789 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m4090512311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3443095683 * L_5 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3443095683 * L_7 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3443095683 *)L_7);
		((  void (*) (UnityAction_1_t3443095683 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3443095683 *)L_7, (float)((*(float*)((float*)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Single>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m678413071_gshared (InvokableCall_1_t270836789 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t3443095683 * L_0 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t3443095683 * L_3 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m983088749_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m983088749_gshared (InvokableCall_1_t214718932 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m983088749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t214718932 *)__this);
		((  void (*) (InvokableCall_1_t214718932 *, UnityAction_1_t3386977826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t214718932 *)__this, (UnityAction_1_t3386977826 *)((UnityAction_1_t3386977826 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m3755016325_gshared (InvokableCall_1_t214718932 * __this, UnityAction_1_t3386977826 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t3386977826 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t214718932 *)__this);
		((  void (*) (InvokableCall_1_t214718932 *, UnityAction_1_t3386977826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t214718932 *)__this, (UnityAction_1_t3386977826 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m705395724_gshared (InvokableCall_1_t214718932 * __this, UnityAction_1_t3386977826 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3386977826 * V_0 = NULL;
	UnityAction_1_t3386977826 * V_1 = NULL;
	{
		UnityAction_1_t3386977826 * L_0 = (UnityAction_1_t3386977826 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3386977826 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3386977826 * L_1 = V_0;
		V_1 = (UnityAction_1_t3386977826 *)L_1;
		UnityAction_1_t3386977826 ** L_2 = (UnityAction_1_t3386977826 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3386977826 * L_3 = V_1;
		UnityAction_1_t3386977826 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3386977826 * L_6 = V_0;
		UnityAction_1_t3386977826 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3386977826 *>((UnityAction_1_t3386977826 **)L_2, (UnityAction_1_t3386977826 *)((UnityAction_1_t3386977826 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3386977826 *)L_6);
		V_0 = (UnityAction_1_t3386977826 *)L_7;
		UnityAction_1_t3386977826 * L_8 = V_0;
		UnityAction_1_t3386977826 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3386977826 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3386977826 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m3576859071_gshared (InvokableCall_1_t214718932 * __this, UnityAction_1_t3386977826 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3386977826 * V_0 = NULL;
	UnityAction_1_t3386977826 * V_1 = NULL;
	{
		UnityAction_1_t3386977826 * L_0 = (UnityAction_1_t3386977826 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3386977826 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3386977826 * L_1 = V_0;
		V_1 = (UnityAction_1_t3386977826 *)L_1;
		UnityAction_1_t3386977826 ** L_2 = (UnityAction_1_t3386977826 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3386977826 * L_3 = V_1;
		UnityAction_1_t3386977826 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3386977826 * L_6 = V_0;
		UnityAction_1_t3386977826 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3386977826 *>((UnityAction_1_t3386977826 **)L_2, (UnityAction_1_t3386977826 *)((UnityAction_1_t3386977826 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3386977826 *)L_6);
		V_0 = (UnityAction_1_t3386977826 *)L_7;
		UnityAction_1_t3386977826 * L_8 = V_0;
		UnityAction_1_t3386977826 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3386977826 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3386977826 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m2424028974_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m2424028974_gshared (InvokableCall_1_t214718932 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m2424028974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3386977826 * L_5 = (UnityAction_1_t3386977826 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3386977826 * L_7 = (UnityAction_1_t3386977826 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3386977826 *)L_7);
		((  void (*) (UnityAction_1_t3386977826 *, Color_t2020392075 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3386977826 *)L_7, (Color_t2020392075 )((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m1941574338_gshared (InvokableCall_1_t214718932 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t3386977826 * L_0 = (UnityAction_1_t3386977826 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t3386977826 * L_3 = (UnityAction_1_t3386977826 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m2837611051_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m2837611051_gshared (InvokableCall_1_t438034436 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m2837611051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t438034436 *)__this);
		((  void (*) (InvokableCall_1_t438034436 *, UnityAction_1_t3610293330 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t438034436 *)__this, (UnityAction_1_t3610293330 *)((UnityAction_1_t3610293330 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m866952903_gshared (InvokableCall_1_t438034436 * __this, UnityAction_1_t3610293330 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t3610293330 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t438034436 *)__this);
		((  void (*) (InvokableCall_1_t438034436 *, UnityAction_1_t3610293330 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t438034436 *)__this, (UnityAction_1_t3610293330 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m1013059220_gshared (InvokableCall_1_t438034436 * __this, UnityAction_1_t3610293330 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3610293330 * V_0 = NULL;
	UnityAction_1_t3610293330 * V_1 = NULL;
	{
		UnityAction_1_t3610293330 * L_0 = (UnityAction_1_t3610293330 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3610293330 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3610293330 * L_1 = V_0;
		V_1 = (UnityAction_1_t3610293330 *)L_1;
		UnityAction_1_t3610293330 ** L_2 = (UnityAction_1_t3610293330 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3610293330 * L_3 = V_1;
		UnityAction_1_t3610293330 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3610293330 * L_6 = V_0;
		UnityAction_1_t3610293330 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3610293330 *>((UnityAction_1_t3610293330 **)L_2, (UnityAction_1_t3610293330 *)((UnityAction_1_t3610293330 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3610293330 *)L_6);
		V_0 = (UnityAction_1_t3610293330 *)L_7;
		UnityAction_1_t3610293330 * L_8 = V_0;
		UnityAction_1_t3610293330 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3610293330 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3610293330 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m3619329377_gshared (InvokableCall_1_t438034436 * __this, UnityAction_1_t3610293330 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3610293330 * V_0 = NULL;
	UnityAction_1_t3610293330 * V_1 = NULL;
	{
		UnityAction_1_t3610293330 * L_0 = (UnityAction_1_t3610293330 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3610293330 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3610293330 * L_1 = V_0;
		V_1 = (UnityAction_1_t3610293330 *)L_1;
		UnityAction_1_t3610293330 ** L_2 = (UnityAction_1_t3610293330 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3610293330 * L_3 = V_1;
		UnityAction_1_t3610293330 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3610293330 * L_6 = V_0;
		UnityAction_1_t3610293330 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3610293330 *>((UnityAction_1_t3610293330 **)L_2, (UnityAction_1_t3610293330 *)((UnityAction_1_t3610293330 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3610293330 *)L_6);
		V_0 = (UnityAction_1_t3610293330 *)L_7;
		UnityAction_1_t3610293330 * L_8 = V_0;
		UnityAction_1_t3610293330 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3610293330 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3610293330 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m3239892614_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m3239892614_gshared (InvokableCall_1_t438034436 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m3239892614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3610293330 * L_5 = (UnityAction_1_t3610293330 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3610293330 * L_7 = (UnityAction_1_t3610293330 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3610293330 *)L_7);
		((  void (*) (UnityAction_1_t3610293330 *, Vector2_t2243707579 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3610293330 *)L_7, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m4182726010_gshared (InvokableCall_1_t438034436 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t3610293330 * L_0 = (UnityAction_1_t3610293330 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t3610293330 * L_3 = (UnityAction_1_t3610293330 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector3>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m2053072656_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m2053072656_gshared (InvokableCall_1_t438034437 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m2053072656_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t438034437 *)__this);
		((  void (*) (InvokableCall_1_t438034437 *, UnityAction_1_t3610293331 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t438034437 *)__this, (UnityAction_1_t3610293331 *)((UnityAction_1_t3610293331 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector3>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m4140664578_gshared (InvokableCall_1_t438034437 * __this, UnityAction_1_t3610293331 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t3610293331 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t438034437 *)__this);
		((  void (*) (InvokableCall_1_t438034437 *, UnityAction_1_t3610293331 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t438034437 *)__this, (UnityAction_1_t3610293331 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector3>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m276916015_gshared (InvokableCall_1_t438034437 * __this, UnityAction_1_t3610293331 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3610293331 * V_0 = NULL;
	UnityAction_1_t3610293331 * V_1 = NULL;
	{
		UnityAction_1_t3610293331 * L_0 = (UnityAction_1_t3610293331 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3610293331 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3610293331 * L_1 = V_0;
		V_1 = (UnityAction_1_t3610293331 *)L_1;
		UnityAction_1_t3610293331 ** L_2 = (UnityAction_1_t3610293331 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3610293331 * L_3 = V_1;
		UnityAction_1_t3610293331 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3610293331 * L_6 = V_0;
		UnityAction_1_t3610293331 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3610293331 *>((UnityAction_1_t3610293331 **)L_2, (UnityAction_1_t3610293331 *)((UnityAction_1_t3610293331 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3610293331 *)L_6);
		V_0 = (UnityAction_1_t3610293331 *)L_7;
		UnityAction_1_t3610293331 * L_8 = V_0;
		UnityAction_1_t3610293331 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3610293331 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3610293331 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector3>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m1055744316_gshared (InvokableCall_1_t438034437 * __this, UnityAction_1_t3610293331 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3610293331 * V_0 = NULL;
	UnityAction_1_t3610293331 * V_1 = NULL;
	{
		UnityAction_1_t3610293331 * L_0 = (UnityAction_1_t3610293331 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3610293331 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3610293331 * L_1 = V_0;
		V_1 = (UnityAction_1_t3610293331 *)L_1;
		UnityAction_1_t3610293331 ** L_2 = (UnityAction_1_t3610293331 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3610293331 * L_3 = V_1;
		UnityAction_1_t3610293331 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3610293331 * L_6 = V_0;
		UnityAction_1_t3610293331 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3610293331 *>((UnityAction_1_t3610293331 **)L_2, (UnityAction_1_t3610293331 *)((UnityAction_1_t3610293331 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3610293331 *)L_6);
		V_0 = (UnityAction_1_t3610293331 *)L_7;
		UnityAction_1_t3610293331 * L_8 = V_0;
		UnityAction_1_t3610293331 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3610293331 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3610293331 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector3>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m3738789835_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m3738789835_gshared (InvokableCall_1_t438034437 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m3738789835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3610293331 * L_5 = (UnityAction_1_t3610293331 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3610293331 * L_7 = (UnityAction_1_t3610293331 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3610293331 *)L_7);
		((  void (*) (UnityAction_1_t3610293331 *, Vector3_t2243707580 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3610293331 *)L_7, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Vector3>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m980157595_gshared (InvokableCall_1_t438034437 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t3610293331 * L_0 = (UnityAction_1_t3610293331 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t3610293331 * L_3 = (UnityAction_1_t3610293331 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<Valve.VR.VREvent_t>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m463912998_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m463912998_gshared (InvokableCall_1_t1599593246 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m463912998_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t1599593246 *)__this);
		((  void (*) (InvokableCall_1_t1599593246 *, UnityAction_1_t476884844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t1599593246 *)__this, (UnityAction_1_t476884844 *)((UnityAction_1_t476884844 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<Valve.VR.VREvent_t>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m2684446796_gshared (InvokableCall_1_t1599593246 * __this, UnityAction_1_t476884844 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t476884844 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t1599593246 *)__this);
		((  void (*) (InvokableCall_1_t1599593246 *, UnityAction_1_t476884844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t1599593246 *)__this, (UnityAction_1_t476884844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<Valve.VR.VREvent_t>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m2367044609_gshared (InvokableCall_1_t1599593246 * __this, UnityAction_1_t476884844 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t476884844 * V_0 = NULL;
	UnityAction_1_t476884844 * V_1 = NULL;
	{
		UnityAction_1_t476884844 * L_0 = (UnityAction_1_t476884844 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t476884844 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t476884844 * L_1 = V_0;
		V_1 = (UnityAction_1_t476884844 *)L_1;
		UnityAction_1_t476884844 ** L_2 = (UnityAction_1_t476884844 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t476884844 * L_3 = V_1;
		UnityAction_1_t476884844 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t476884844 * L_6 = V_0;
		UnityAction_1_t476884844 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t476884844 *>((UnityAction_1_t476884844 **)L_2, (UnityAction_1_t476884844 *)((UnityAction_1_t476884844 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t476884844 *)L_6);
		V_0 = (UnityAction_1_t476884844 *)L_7;
		UnityAction_1_t476884844 * L_8 = V_0;
		UnityAction_1_t476884844 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t476884844 *)L_8) == ((Il2CppObject*)(UnityAction_1_t476884844 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<Valve.VR.VREvent_t>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m3030024234_gshared (InvokableCall_1_t1599593246 * __this, UnityAction_1_t476884844 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t476884844 * V_0 = NULL;
	UnityAction_1_t476884844 * V_1 = NULL;
	{
		UnityAction_1_t476884844 * L_0 = (UnityAction_1_t476884844 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t476884844 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t476884844 * L_1 = V_0;
		V_1 = (UnityAction_1_t476884844 *)L_1;
		UnityAction_1_t476884844 ** L_2 = (UnityAction_1_t476884844 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t476884844 * L_3 = V_1;
		UnityAction_1_t476884844 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t476884844 * L_6 = V_0;
		UnityAction_1_t476884844 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t476884844 *>((UnityAction_1_t476884844 **)L_2, (UnityAction_1_t476884844 *)((UnityAction_1_t476884844 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t476884844 *)L_6);
		V_0 = (UnityAction_1_t476884844 *)L_7;
		UnityAction_1_t476884844 * L_8 = V_0;
		UnityAction_1_t476884844 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t476884844 *)L_8) == ((Il2CppObject*)(UnityAction_1_t476884844 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<Valve.VR.VREvent_t>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m4211226441_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m4211226441_gshared (InvokableCall_1_t1599593246 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m4211226441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t476884844 * L_5 = (UnityAction_1_t476884844 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t476884844 * L_7 = (UnityAction_1_t476884844 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t476884844 *)L_7);
		((  void (*) (UnityAction_1_t476884844 *, VREvent_t_t3405266389 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t476884844 *)L_7, (VREvent_t_t3405266389 )((*(VREvent_t_t3405266389 *)((VREvent_t_t3405266389 *)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<Valve.VR.VREvent_t>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m4080829761_gshared (InvokableCall_1_t1599593246 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t476884844 * L_0 = (UnityAction_1_t476884844 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t476884844 * L_3 = (UnityAction_1_t476884844 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m1363792238_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m1363792238_gshared (InvokableCall_2_t56619800 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m1363792238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t41828916 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m48765551_gshared (InvokableCall_2_t56619800 * __this, UnityAction_2_t41828916 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t41828916 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t56619800 *)__this);
		((  void (*) (InvokableCall_2_t56619800 *, UnityAction_2_t41828916 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t56619800 *)__this, (UnityAction_2_t41828916 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m4001424544_gshared (InvokableCall_2_t56619800 * __this, UnityAction_2_t41828916 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t41828916 * V_0 = NULL;
	UnityAction_2_t41828916 * V_1 = NULL;
	{
		UnityAction_2_t41828916 * L_0 = (UnityAction_2_t41828916 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t41828916 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t41828916 * L_1 = V_0;
		V_1 = (UnityAction_2_t41828916 *)L_1;
		UnityAction_2_t41828916 ** L_2 = (UnityAction_2_t41828916 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t41828916 * L_3 = V_1;
		UnityAction_2_t41828916 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t41828916 * L_6 = V_0;
		UnityAction_2_t41828916 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t41828916 *>((UnityAction_2_t41828916 **)L_2, (UnityAction_2_t41828916 *)((UnityAction_2_t41828916 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t41828916 *)L_6);
		V_0 = (UnityAction_2_t41828916 *)L_7;
		UnityAction_2_t41828916 * L_8 = V_0;
		UnityAction_2_t41828916 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t41828916 *)L_8) == ((Il2CppObject*)(UnityAction_2_t41828916 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m4131363733_gshared (InvokableCall_2_t56619800 * __this, UnityAction_2_t41828916 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t41828916 * V_0 = NULL;
	UnityAction_2_t41828916 * V_1 = NULL;
	{
		UnityAction_2_t41828916 * L_0 = (UnityAction_2_t41828916 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t41828916 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t41828916 * L_1 = V_0;
		V_1 = (UnityAction_2_t41828916 *)L_1;
		UnityAction_2_t41828916 ** L_2 = (UnityAction_2_t41828916 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t41828916 * L_3 = V_1;
		UnityAction_2_t41828916 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t41828916 * L_6 = V_0;
		UnityAction_2_t41828916 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t41828916 *>((UnityAction_2_t41828916 **)L_2, (UnityAction_2_t41828916 *)((UnityAction_2_t41828916 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t41828916 *)L_6);
		V_0 = (UnityAction_2_t41828916 *)L_7;
		UnityAction_2_t41828916 * L_8 = V_0;
		UnityAction_2_t41828916 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t41828916 *)L_8) == ((Il2CppObject*)(UnityAction_2_t41828916 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m4293234667_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m4293234667_gshared (InvokableCall_2_t56619800 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m4293234667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t41828916 * L_8 = (UnityAction_2_t41828916 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t41828916 * L_10 = (UnityAction_2_t41828916 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t41828916 *)L_10);
		((  void (*) (UnityAction_2_t41828916 *, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t41828916 *)L_10, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (bool)((*(bool*)((bool*)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m2572262419_gshared (InvokableCall_2_t56619800 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t41828916 * L_0 = (UnityAction_2_t41828916 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t41828916 * L_3 = (UnityAction_2_t41828916 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m1806504405_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m1806504405_gshared (InvokableCall_2_t640854293 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m1806504405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t626063409 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Boolean>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m2433638040_gshared (InvokableCall_2_t640854293 * __this, UnityAction_2_t626063409 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t626063409 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t640854293 *)__this);
		((  void (*) (InvokableCall_2_t640854293 *, UnityAction_2_t626063409 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t640854293 *)__this, (UnityAction_2_t626063409 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Boolean>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m22451329_gshared (InvokableCall_2_t640854293 * __this, UnityAction_2_t626063409 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t626063409 * V_0 = NULL;
	UnityAction_2_t626063409 * V_1 = NULL;
	{
		UnityAction_2_t626063409 * L_0 = (UnityAction_2_t626063409 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t626063409 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t626063409 * L_1 = V_0;
		V_1 = (UnityAction_2_t626063409 *)L_1;
		UnityAction_2_t626063409 ** L_2 = (UnityAction_2_t626063409 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t626063409 * L_3 = V_1;
		UnityAction_2_t626063409 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t626063409 * L_6 = V_0;
		UnityAction_2_t626063409 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t626063409 *>((UnityAction_2_t626063409 **)L_2, (UnityAction_2_t626063409 *)((UnityAction_2_t626063409 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t626063409 *)L_6);
		V_0 = (UnityAction_2_t626063409 *)L_7;
		UnityAction_2_t626063409 * L_8 = V_0;
		UnityAction_2_t626063409 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t626063409 *)L_8) == ((Il2CppObject*)(UnityAction_2_t626063409 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Boolean>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m1464363114_gshared (InvokableCall_2_t640854293 * __this, UnityAction_2_t626063409 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t626063409 * V_0 = NULL;
	UnityAction_2_t626063409 * V_1 = NULL;
	{
		UnityAction_2_t626063409 * L_0 = (UnityAction_2_t626063409 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t626063409 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t626063409 * L_1 = V_0;
		V_1 = (UnityAction_2_t626063409 *)L_1;
		UnityAction_2_t626063409 ** L_2 = (UnityAction_2_t626063409 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t626063409 * L_3 = V_1;
		UnityAction_2_t626063409 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t626063409 * L_6 = V_0;
		UnityAction_2_t626063409 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t626063409 *>((UnityAction_2_t626063409 **)L_2, (UnityAction_2_t626063409 *)((UnityAction_2_t626063409 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t626063409 *)L_6);
		V_0 = (UnityAction_2_t626063409 *)L_7;
		UnityAction_2_t626063409 * L_8 = V_0;
		UnityAction_2_t626063409 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t626063409 *)L_8) == ((Il2CppObject*)(UnityAction_2_t626063409 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Boolean>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m849933656_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m849933656_gshared (InvokableCall_2_t640854293 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m849933656_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t626063409 * L_8 = (UnityAction_2_t626063409 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t626063409 * L_10 = (UnityAction_2_t626063409 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t626063409 *)L_10);
		((  void (*) (UnityAction_2_t626063409 *, Il2CppObject *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t626063409 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (bool)((*(bool*)((bool*)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m1151619372_gshared (InvokableCall_2_t640854293 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t626063409 * L_0 = (UnityAction_2_t626063409 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t626063409 * L_3 = (UnityAction_2_t626063409 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m974169948_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m974169948_gshared (InvokableCall_2_t3799696166 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m974169948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t3784905282 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m1466187173_gshared (InvokableCall_2_t3799696166 * __this, UnityAction_2_t3784905282 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t3784905282 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t3799696166 *)__this);
		((  void (*) (InvokableCall_2_t3799696166 *, UnityAction_2_t3784905282 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t3799696166 *)__this, (UnityAction_2_t3784905282 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m3235681898_gshared (InvokableCall_2_t3799696166 * __this, UnityAction_2_t3784905282 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t3784905282 * V_0 = NULL;
	UnityAction_2_t3784905282 * V_1 = NULL;
	{
		UnityAction_2_t3784905282 * L_0 = (UnityAction_2_t3784905282 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t3784905282 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t3784905282 * L_1 = V_0;
		V_1 = (UnityAction_2_t3784905282 *)L_1;
		UnityAction_2_t3784905282 ** L_2 = (UnityAction_2_t3784905282 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t3784905282 * L_3 = V_1;
		UnityAction_2_t3784905282 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t3784905282 * L_6 = V_0;
		UnityAction_2_t3784905282 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t3784905282 *>((UnityAction_2_t3784905282 **)L_2, (UnityAction_2_t3784905282 *)((UnityAction_2_t3784905282 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t3784905282 *)L_6);
		V_0 = (UnityAction_2_t3784905282 *)L_7;
		UnityAction_2_t3784905282 * L_8 = V_0;
		UnityAction_2_t3784905282 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t3784905282 *)L_8) == ((Il2CppObject*)(UnityAction_2_t3784905282 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m2509334539_gshared (InvokableCall_2_t3799696166 * __this, UnityAction_2_t3784905282 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t3784905282 * V_0 = NULL;
	UnityAction_2_t3784905282 * V_1 = NULL;
	{
		UnityAction_2_t3784905282 * L_0 = (UnityAction_2_t3784905282 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t3784905282 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t3784905282 * L_1 = V_0;
		V_1 = (UnityAction_2_t3784905282 *)L_1;
		UnityAction_2_t3784905282 ** L_2 = (UnityAction_2_t3784905282 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t3784905282 * L_3 = V_1;
		UnityAction_2_t3784905282 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t3784905282 * L_6 = V_0;
		UnityAction_2_t3784905282 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t3784905282 *>((UnityAction_2_t3784905282 **)L_2, (UnityAction_2_t3784905282 *)((UnityAction_2_t3784905282 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t3784905282 *)L_6);
		V_0 = (UnityAction_2_t3784905282 *)L_7;
		UnityAction_2_t3784905282 * L_8 = V_0;
		UnityAction_2_t3784905282 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t3784905282 *)L_8) == ((Il2CppObject*)(UnityAction_2_t3784905282 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m1071013389_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m1071013389_gshared (InvokableCall_2_t3799696166 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m1071013389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t3784905282 * L_8 = (UnityAction_2_t3784905282 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t3784905282 * L_10 = (UnityAction_2_t3784905282 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t3784905282 *)L_10);
		((  void (*) (UnityAction_2_t3784905282 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t3784905282 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m1763382885_gshared (InvokableCall_2_t3799696166 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t3784905282 * L_0 = (UnityAction_2_t3784905282 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t3784905282 * L_3 = (UnityAction_2_t3784905282 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.BodyPhysicsEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m3199703015_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m3199703015_gshared (InvokableCall_2_t3340378525 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m3199703015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t3325587641 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.BodyPhysicsEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m2987865216_gshared (InvokableCall_2_t3340378525 * __this, UnityAction_2_t3325587641 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t3325587641 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t3340378525 *)__this);
		((  void (*) (InvokableCall_2_t3340378525 *, UnityAction_2_t3325587641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t3340378525 *)__this, (UnityAction_2_t3325587641 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.BodyPhysicsEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m578261047_gshared (InvokableCall_2_t3340378525 * __this, UnityAction_2_t3325587641 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t3325587641 * V_0 = NULL;
	UnityAction_2_t3325587641 * V_1 = NULL;
	{
		UnityAction_2_t3325587641 * L_0 = (UnityAction_2_t3325587641 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t3325587641 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t3325587641 * L_1 = V_0;
		V_1 = (UnityAction_2_t3325587641 *)L_1;
		UnityAction_2_t3325587641 ** L_2 = (UnityAction_2_t3325587641 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t3325587641 * L_3 = V_1;
		UnityAction_2_t3325587641 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t3325587641 * L_6 = V_0;
		UnityAction_2_t3325587641 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t3325587641 *>((UnityAction_2_t3325587641 **)L_2, (UnityAction_2_t3325587641 *)((UnityAction_2_t3325587641 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t3325587641 *)L_6);
		V_0 = (UnityAction_2_t3325587641 *)L_7;
		UnityAction_2_t3325587641 * L_8 = V_0;
		UnityAction_2_t3325587641 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t3325587641 *)L_8) == ((Il2CppObject*)(UnityAction_2_t3325587641 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.BodyPhysicsEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m3739457386_gshared (InvokableCall_2_t3340378525 * __this, UnityAction_2_t3325587641 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t3325587641 * V_0 = NULL;
	UnityAction_2_t3325587641 * V_1 = NULL;
	{
		UnityAction_2_t3325587641 * L_0 = (UnityAction_2_t3325587641 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t3325587641 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t3325587641 * L_1 = V_0;
		V_1 = (UnityAction_2_t3325587641 *)L_1;
		UnityAction_2_t3325587641 ** L_2 = (UnityAction_2_t3325587641 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t3325587641 * L_3 = V_1;
		UnityAction_2_t3325587641 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t3325587641 * L_6 = V_0;
		UnityAction_2_t3325587641 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t3325587641 *>((UnityAction_2_t3325587641 **)L_2, (UnityAction_2_t3325587641 *)((UnityAction_2_t3325587641 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t3325587641 *)L_6);
		V_0 = (UnityAction_2_t3325587641 *)L_7;
		UnityAction_2_t3325587641 * L_8 = V_0;
		UnityAction_2_t3325587641 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t3325587641 *)L_8) == ((Il2CppObject*)(UnityAction_2_t3325587641 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.BodyPhysicsEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m568552404_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m568552404_gshared (InvokableCall_2_t3340378525 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m568552404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t3325587641 * L_8 = (UnityAction_2_t3325587641 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t3325587641 * L_10 = (UnityAction_2_t3325587641 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t3325587641 *)L_10);
		((  void (*) (UnityAction_2_t3325587641 *, Il2CppObject *, BodyPhysicsEventArgs_t2230131654 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t3325587641 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (BodyPhysicsEventArgs_t2230131654 )((*(BodyPhysicsEventArgs_t2230131654 *)((BodyPhysicsEventArgs_t2230131654 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.BodyPhysicsEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m2626551512_gshared (InvokableCall_2_t3340378525 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t3325587641 * L_0 = (UnityAction_2_t3325587641 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t3325587641 * L_3 = (UnityAction_2_t3325587641 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.Control3DEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m4055870136_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m4055870136_gshared (InvokableCall_2_t910305276 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m4055870136_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t895514392 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.Control3DEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m357326609_gshared (InvokableCall_2_t910305276 * __this, UnityAction_2_t895514392 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t895514392 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t910305276 *)__this);
		((  void (*) (InvokableCall_2_t910305276 *, UnityAction_2_t895514392 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t910305276 *)__this, (UnityAction_2_t895514392 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.Control3DEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m3732820374_gshared (InvokableCall_2_t910305276 * __this, UnityAction_2_t895514392 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t895514392 * V_0 = NULL;
	UnityAction_2_t895514392 * V_1 = NULL;
	{
		UnityAction_2_t895514392 * L_0 = (UnityAction_2_t895514392 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t895514392 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t895514392 * L_1 = V_0;
		V_1 = (UnityAction_2_t895514392 *)L_1;
		UnityAction_2_t895514392 ** L_2 = (UnityAction_2_t895514392 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t895514392 * L_3 = V_1;
		UnityAction_2_t895514392 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t895514392 * L_6 = V_0;
		UnityAction_2_t895514392 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t895514392 *>((UnityAction_2_t895514392 **)L_2, (UnityAction_2_t895514392 *)((UnityAction_2_t895514392 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t895514392 *)L_6);
		V_0 = (UnityAction_2_t895514392 *)L_7;
		UnityAction_2_t895514392 * L_8 = V_0;
		UnityAction_2_t895514392 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t895514392 *)L_8) == ((Il2CppObject*)(UnityAction_2_t895514392 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.Control3DEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m3912846059_gshared (InvokableCall_2_t910305276 * __this, UnityAction_2_t895514392 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t895514392 * V_0 = NULL;
	UnityAction_2_t895514392 * V_1 = NULL;
	{
		UnityAction_2_t895514392 * L_0 = (UnityAction_2_t895514392 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t895514392 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t895514392 * L_1 = V_0;
		V_1 = (UnityAction_2_t895514392 *)L_1;
		UnityAction_2_t895514392 ** L_2 = (UnityAction_2_t895514392 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t895514392 * L_3 = V_1;
		UnityAction_2_t895514392 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t895514392 * L_6 = V_0;
		UnityAction_2_t895514392 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t895514392 *>((UnityAction_2_t895514392 **)L_2, (UnityAction_2_t895514392 *)((UnityAction_2_t895514392 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t895514392 *)L_6);
		V_0 = (UnityAction_2_t895514392 *)L_7;
		UnityAction_2_t895514392 * L_8 = V_0;
		UnityAction_2_t895514392 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t895514392 *)L_8) == ((Il2CppObject*)(UnityAction_2_t895514392 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.Control3DEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m2887987353_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m2887987353_gshared (InvokableCall_2_t910305276 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m2887987353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t895514392 * L_8 = (UnityAction_2_t895514392 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t895514392 * L_10 = (UnityAction_2_t895514392 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t895514392 *)L_10);
		((  void (*) (UnityAction_2_t895514392 *, Il2CppObject *, Control3DEventArgs_t4095025701 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t895514392 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Control3DEventArgs_t4095025701 )((*(Control3DEventArgs_t4095025701 *)((Control3DEventArgs_t4095025701 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.Control3DEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m2991183833_gshared (InvokableCall_2_t910305276 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t895514392 * L_0 = (UnityAction_2_t895514392 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t895514392 * L_3 = (UnityAction_2_t895514392 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerActionsEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m1250901645_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m1250901645_gshared (InvokableCall_2_t1454248347 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m1250901645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t1439457463 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerActionsEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m3537835750_gshared (InvokableCall_2_t1454248347 * __this, UnityAction_2_t1439457463 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t1439457463 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t1454248347 *)__this);
		((  void (*) (InvokableCall_2_t1454248347 *, UnityAction_2_t1439457463 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t1454248347 *)__this, (UnityAction_2_t1439457463 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerActionsEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m679562273_gshared (InvokableCall_2_t1454248347 * __this, UnityAction_2_t1439457463 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t1439457463 * V_0 = NULL;
	UnityAction_2_t1439457463 * V_1 = NULL;
	{
		UnityAction_2_t1439457463 * L_0 = (UnityAction_2_t1439457463 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t1439457463 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t1439457463 * L_1 = V_0;
		V_1 = (UnityAction_2_t1439457463 *)L_1;
		UnityAction_2_t1439457463 ** L_2 = (UnityAction_2_t1439457463 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t1439457463 * L_3 = V_1;
		UnityAction_2_t1439457463 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t1439457463 * L_6 = V_0;
		UnityAction_2_t1439457463 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t1439457463 *>((UnityAction_2_t1439457463 **)L_2, (UnityAction_2_t1439457463 *)((UnityAction_2_t1439457463 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t1439457463 *)L_6);
		V_0 = (UnityAction_2_t1439457463 *)L_7;
		UnityAction_2_t1439457463 * L_8 = V_0;
		UnityAction_2_t1439457463 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t1439457463 *)L_8) == ((Il2CppObject*)(UnityAction_2_t1439457463 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerActionsEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m2574429696_gshared (InvokableCall_2_t1454248347 * __this, UnityAction_2_t1439457463 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t1439457463 * V_0 = NULL;
	UnityAction_2_t1439457463 * V_1 = NULL;
	{
		UnityAction_2_t1439457463 * L_0 = (UnityAction_2_t1439457463 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t1439457463 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t1439457463 * L_1 = V_0;
		V_1 = (UnityAction_2_t1439457463 *)L_1;
		UnityAction_2_t1439457463 ** L_2 = (UnityAction_2_t1439457463 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t1439457463 * L_3 = V_1;
		UnityAction_2_t1439457463 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t1439457463 * L_6 = V_0;
		UnityAction_2_t1439457463 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t1439457463 *>((UnityAction_2_t1439457463 **)L_2, (UnityAction_2_t1439457463 *)((UnityAction_2_t1439457463 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t1439457463 *)L_6);
		V_0 = (UnityAction_2_t1439457463 *)L_7;
		UnityAction_2_t1439457463 * L_8 = V_0;
		UnityAction_2_t1439457463 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t1439457463 *)L_8) == ((Il2CppObject*)(UnityAction_2_t1439457463 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerActionsEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m686209022_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m686209022_gshared (InvokableCall_2_t1454248347 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m686209022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t1439457463 * L_8 = (UnityAction_2_t1439457463 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t1439457463 * L_10 = (UnityAction_2_t1439457463 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t1439457463 *)L_10);
		((  void (*) (UnityAction_2_t1439457463 *, Il2CppObject *, ControllerActionsEventArgs_t344001476 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t1439457463 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (ControllerActionsEventArgs_t344001476 )((*(ControllerActionsEventArgs_t344001476 *)((ControllerActionsEventArgs_t344001476 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerActionsEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m2712851434_gshared (InvokableCall_2_t1454248347 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t1439457463 * L_0 = (UnityAction_2_t1439457463 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t1439457463 * L_3 = (UnityAction_2_t1439457463 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerInteractionEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m3592476454_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m3592476454_gshared (InvokableCall_2_t1397884410 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m3592476454_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t1383093526 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerInteractionEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m635840647_gshared (InvokableCall_2_t1397884410 * __this, UnityAction_2_t1383093526 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t1383093526 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t1397884410 *)__this);
		((  void (*) (InvokableCall_2_t1397884410 *, UnityAction_2_t1383093526 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t1397884410 *)__this, (UnityAction_2_t1383093526 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerInteractionEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m1566474016_gshared (InvokableCall_2_t1397884410 * __this, UnityAction_2_t1383093526 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t1383093526 * V_0 = NULL;
	UnityAction_2_t1383093526 * V_1 = NULL;
	{
		UnityAction_2_t1383093526 * L_0 = (UnityAction_2_t1383093526 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t1383093526 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t1383093526 * L_1 = V_0;
		V_1 = (UnityAction_2_t1383093526 *)L_1;
		UnityAction_2_t1383093526 ** L_2 = (UnityAction_2_t1383093526 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t1383093526 * L_3 = V_1;
		UnityAction_2_t1383093526 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t1383093526 * L_6 = V_0;
		UnityAction_2_t1383093526 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t1383093526 *>((UnityAction_2_t1383093526 **)L_2, (UnityAction_2_t1383093526 *)((UnityAction_2_t1383093526 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t1383093526 *)L_6);
		V_0 = (UnityAction_2_t1383093526 *)L_7;
		UnityAction_2_t1383093526 * L_8 = V_0;
		UnityAction_2_t1383093526 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t1383093526 *)L_8) == ((Il2CppObject*)(UnityAction_2_t1383093526 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerInteractionEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m3555528609_gshared (InvokableCall_2_t1397884410 * __this, UnityAction_2_t1383093526 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t1383093526 * V_0 = NULL;
	UnityAction_2_t1383093526 * V_1 = NULL;
	{
		UnityAction_2_t1383093526 * L_0 = (UnityAction_2_t1383093526 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t1383093526 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t1383093526 * L_1 = V_0;
		V_1 = (UnityAction_2_t1383093526 *)L_1;
		UnityAction_2_t1383093526 ** L_2 = (UnityAction_2_t1383093526 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t1383093526 * L_3 = V_1;
		UnityAction_2_t1383093526 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t1383093526 * L_6 = V_0;
		UnityAction_2_t1383093526 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t1383093526 *>((UnityAction_2_t1383093526 **)L_2, (UnityAction_2_t1383093526 *)((UnityAction_2_t1383093526 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t1383093526 *)L_6);
		V_0 = (UnityAction_2_t1383093526 *)L_7;
		UnityAction_2_t1383093526 * L_8 = V_0;
		UnityAction_2_t1383093526 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t1383093526 *)L_8) == ((Il2CppObject*)(UnityAction_2_t1383093526 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerInteractionEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m2282007067_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m2282007067_gshared (InvokableCall_2_t1397884410 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m2282007067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t1383093526 * L_8 = (UnityAction_2_t1383093526 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t1383093526 * L_10 = (UnityAction_2_t1383093526 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t1383093526 *)L_10);
		((  void (*) (UnityAction_2_t1383093526 *, Il2CppObject *, ControllerInteractionEventArgs_t287637539 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t1383093526 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (ControllerInteractionEventArgs_t287637539 )((*(ControllerInteractionEventArgs_t287637539 *)((ControllerInteractionEventArgs_t287637539 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerInteractionEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m1124626283_gshared (InvokableCall_2_t1397884410 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t1383093526 * L_0 = (UnityAction_2_t1383093526 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t1383093526 * L_3 = (UnityAction_2_t1383093526 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DashTeleportEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m16262127_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m16262127_gshared (InvokableCall_2_t3307500113 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m16262127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t3292709229 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DashTeleportEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m3752805140_gshared (InvokableCall_2_t3307500113 * __this, UnityAction_2_t3292709229 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t3292709229 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t3307500113 *)__this);
		((  void (*) (InvokableCall_2_t3307500113 *, UnityAction_2_t3292709229 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t3307500113 *)__this, (UnityAction_2_t3292709229 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DashTeleportEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m387072543_gshared (InvokableCall_2_t3307500113 * __this, UnityAction_2_t3292709229 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t3292709229 * V_0 = NULL;
	UnityAction_2_t3292709229 * V_1 = NULL;
	{
		UnityAction_2_t3292709229 * L_0 = (UnityAction_2_t3292709229 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t3292709229 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t3292709229 * L_1 = V_0;
		V_1 = (UnityAction_2_t3292709229 *)L_1;
		UnityAction_2_t3292709229 ** L_2 = (UnityAction_2_t3292709229 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t3292709229 * L_3 = V_1;
		UnityAction_2_t3292709229 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t3292709229 * L_6 = V_0;
		UnityAction_2_t3292709229 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t3292709229 *>((UnityAction_2_t3292709229 **)L_2, (UnityAction_2_t3292709229 *)((UnityAction_2_t3292709229 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t3292709229 *)L_6);
		V_0 = (UnityAction_2_t3292709229 *)L_7;
		UnityAction_2_t3292709229 * L_8 = V_0;
		UnityAction_2_t3292709229 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t3292709229 *)L_8) == ((Il2CppObject*)(UnityAction_2_t3292709229 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DashTeleportEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m3000777586_gshared (InvokableCall_2_t3307500113 * __this, UnityAction_2_t3292709229 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t3292709229 * V_0 = NULL;
	UnityAction_2_t3292709229 * V_1 = NULL;
	{
		UnityAction_2_t3292709229 * L_0 = (UnityAction_2_t3292709229 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t3292709229 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t3292709229 * L_1 = V_0;
		V_1 = (UnityAction_2_t3292709229 *)L_1;
		UnityAction_2_t3292709229 ** L_2 = (UnityAction_2_t3292709229 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t3292709229 * L_3 = V_1;
		UnityAction_2_t3292709229 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t3292709229 * L_6 = V_0;
		UnityAction_2_t3292709229 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t3292709229 *>((UnityAction_2_t3292709229 **)L_2, (UnityAction_2_t3292709229 *)((UnityAction_2_t3292709229 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t3292709229 *)L_6);
		V_0 = (UnityAction_2_t3292709229 *)L_7;
		UnityAction_2_t3292709229 * L_8 = V_0;
		UnityAction_2_t3292709229 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t3292709229 *)L_8) == ((Il2CppObject*)(UnityAction_2_t3292709229 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DashTeleportEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m1020299440_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m1020299440_gshared (InvokableCall_2_t3307500113 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m1020299440_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t3292709229 * L_8 = (UnityAction_2_t3292709229 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t3292709229 * L_10 = (UnityAction_2_t3292709229 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t3292709229 *)L_10);
		((  void (*) (UnityAction_2_t3292709229 *, Il2CppObject *, DashTeleportEventArgs_t2197253242 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t3292709229 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (DashTeleportEventArgs_t2197253242 )((*(DashTeleportEventArgs_t2197253242 *)((DashTeleportEventArgs_t2197253242 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DashTeleportEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m3527899668_gshared (InvokableCall_2_t3307500113 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t3292709229 * L_0 = (UnityAction_2_t3292709229 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t3292709229 * L_3 = (UnityAction_2_t3292709229 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DestinationMarkerEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m4055987108_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m4055987108_gshared (InvokableCall_2_t2962698532 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m4055987108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t2947907648 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DestinationMarkerEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m2378990915_gshared (InvokableCall_2_t2962698532 * __this, UnityAction_2_t2947907648 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t2947907648 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t2962698532 *)__this);
		((  void (*) (InvokableCall_2_t2962698532 *, UnityAction_2_t2947907648 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t2962698532 *)__this, (UnityAction_2_t2947907648 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DestinationMarkerEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m2041709402_gshared (InvokableCall_2_t2962698532 * __this, UnityAction_2_t2947907648 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t2947907648 * V_0 = NULL;
	UnityAction_2_t2947907648 * V_1 = NULL;
	{
		UnityAction_2_t2947907648 * L_0 = (UnityAction_2_t2947907648 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t2947907648 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t2947907648 * L_1 = V_0;
		V_1 = (UnityAction_2_t2947907648 *)L_1;
		UnityAction_2_t2947907648 ** L_2 = (UnityAction_2_t2947907648 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t2947907648 * L_3 = V_1;
		UnityAction_2_t2947907648 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t2947907648 * L_6 = V_0;
		UnityAction_2_t2947907648 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t2947907648 *>((UnityAction_2_t2947907648 **)L_2, (UnityAction_2_t2947907648 *)((UnityAction_2_t2947907648 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t2947907648 *)L_6);
		V_0 = (UnityAction_2_t2947907648 *)L_7;
		UnityAction_2_t2947907648 * L_8 = V_0;
		UnityAction_2_t2947907648 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t2947907648 *)L_8) == ((Il2CppObject*)(UnityAction_2_t2947907648 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DestinationMarkerEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m1967689421_gshared (InvokableCall_2_t2962698532 * __this, UnityAction_2_t2947907648 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t2947907648 * V_0 = NULL;
	UnityAction_2_t2947907648 * V_1 = NULL;
	{
		UnityAction_2_t2947907648 * L_0 = (UnityAction_2_t2947907648 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t2947907648 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t2947907648 * L_1 = V_0;
		V_1 = (UnityAction_2_t2947907648 *)L_1;
		UnityAction_2_t2947907648 ** L_2 = (UnityAction_2_t2947907648 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t2947907648 * L_3 = V_1;
		UnityAction_2_t2947907648 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t2947907648 * L_6 = V_0;
		UnityAction_2_t2947907648 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t2947907648 *>((UnityAction_2_t2947907648 **)L_2, (UnityAction_2_t2947907648 *)((UnityAction_2_t2947907648 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t2947907648 *)L_6);
		V_0 = (UnityAction_2_t2947907648 *)L_7;
		UnityAction_2_t2947907648 * L_8 = V_0;
		UnityAction_2_t2947907648 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t2947907648 *)L_8) == ((Il2CppObject*)(UnityAction_2_t2947907648 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DestinationMarkerEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m3026355887_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m3026355887_gshared (InvokableCall_2_t2962698532 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m3026355887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t2947907648 * L_8 = (UnityAction_2_t2947907648 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t2947907648 * L_10 = (UnityAction_2_t2947907648 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t2947907648 *)L_10);
		((  void (*) (UnityAction_2_t2947907648 *, Il2CppObject *, DestinationMarkerEventArgs_t1852451661 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t2947907648 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (DestinationMarkerEventArgs_t1852451661 )((*(DestinationMarkerEventArgs_t1852451661 *)((DestinationMarkerEventArgs_t1852451661 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DestinationMarkerEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m1746023679_gshared (InvokableCall_2_t2962698532 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t2947907648 * L_0 = (UnityAction_2_t2947907648 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t2947907648 * L_3 = (UnityAction_2_t2947907648 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetCollisionEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m2450731302_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m2450731302_gshared (InvokableCall_2_t2352620258 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m2450731302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t2337829374 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetCollisionEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m482578081_gshared (InvokableCall_2_t2352620258 * __this, UnityAction_2_t2337829374 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t2337829374 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t2352620258 *)__this);
		((  void (*) (InvokableCall_2_t2352620258 *, UnityAction_2_t2337829374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t2352620258 *)__this, (UnityAction_2_t2337829374 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetCollisionEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m2885172656_gshared (InvokableCall_2_t2352620258 * __this, UnityAction_2_t2337829374 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t2337829374 * V_0 = NULL;
	UnityAction_2_t2337829374 * V_1 = NULL;
	{
		UnityAction_2_t2337829374 * L_0 = (UnityAction_2_t2337829374 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t2337829374 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t2337829374 * L_1 = V_0;
		V_1 = (UnityAction_2_t2337829374 *)L_1;
		UnityAction_2_t2337829374 ** L_2 = (UnityAction_2_t2337829374 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t2337829374 * L_3 = V_1;
		UnityAction_2_t2337829374 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t2337829374 * L_6 = V_0;
		UnityAction_2_t2337829374 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t2337829374 *>((UnityAction_2_t2337829374 **)L_2, (UnityAction_2_t2337829374 *)((UnityAction_2_t2337829374 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t2337829374 *)L_6);
		V_0 = (UnityAction_2_t2337829374 *)L_7;
		UnityAction_2_t2337829374 * L_8 = V_0;
		UnityAction_2_t2337829374 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t2337829374 *)L_8) == ((Il2CppObject*)(UnityAction_2_t2337829374 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetCollisionEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m3086193007_gshared (InvokableCall_2_t2352620258 * __this, UnityAction_2_t2337829374 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t2337829374 * V_0 = NULL;
	UnityAction_2_t2337829374 * V_1 = NULL;
	{
		UnityAction_2_t2337829374 * L_0 = (UnityAction_2_t2337829374 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t2337829374 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t2337829374 * L_1 = V_0;
		V_1 = (UnityAction_2_t2337829374 *)L_1;
		UnityAction_2_t2337829374 ** L_2 = (UnityAction_2_t2337829374 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t2337829374 * L_3 = V_1;
		UnityAction_2_t2337829374 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t2337829374 * L_6 = V_0;
		UnityAction_2_t2337829374 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t2337829374 *>((UnityAction_2_t2337829374 **)L_2, (UnityAction_2_t2337829374 *)((UnityAction_2_t2337829374 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t2337829374 *)L_6);
		V_0 = (UnityAction_2_t2337829374 *)L_7;
		UnityAction_2_t2337829374 * L_8 = V_0;
		UnityAction_2_t2337829374 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t2337829374 *)L_8) == ((Il2CppObject*)(UnityAction_2_t2337829374 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetCollisionEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m2919628545_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m2919628545_gshared (InvokableCall_2_t2352620258 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m2919628545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t2337829374 * L_8 = (UnityAction_2_t2337829374 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t2337829374 * L_10 = (UnityAction_2_t2337829374 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t2337829374 *)L_10);
		((  void (*) (UnityAction_2_t2337829374 *, Il2CppObject *, HeadsetCollisionEventArgs_t1242373387 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t2337829374 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (HeadsetCollisionEventArgs_t1242373387 )((*(HeadsetCollisionEventArgs_t1242373387 *)((HeadsetCollisionEventArgs_t1242373387 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetCollisionEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m1346016697_gshared (InvokableCall_2_t2352620258 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t2337829374 * L_0 = (UnityAction_2_t2337829374 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t2337829374 * L_3 = (UnityAction_2_t2337829374 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m2494562780_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m2494562780_gshared (InvokableCall_2_t3763778592 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m2494562780_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t3748987708 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m3838738679_gshared (InvokableCall_2_t3763778592 * __this, UnityAction_2_t3748987708 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t3748987708 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t3763778592 *)__this);
		((  void (*) (InvokableCall_2_t3763778592 *, UnityAction_2_t3748987708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t3763778592 *)__this, (UnityAction_2_t3748987708 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m3250696026_gshared (InvokableCall_2_t3763778592 * __this, UnityAction_2_t3748987708 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t3748987708 * V_0 = NULL;
	UnityAction_2_t3748987708 * V_1 = NULL;
	{
		UnityAction_2_t3748987708 * L_0 = (UnityAction_2_t3748987708 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t3748987708 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t3748987708 * L_1 = V_0;
		V_1 = (UnityAction_2_t3748987708 *)L_1;
		UnityAction_2_t3748987708 ** L_2 = (UnityAction_2_t3748987708 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t3748987708 * L_3 = V_1;
		UnityAction_2_t3748987708 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t3748987708 * L_6 = V_0;
		UnityAction_2_t3748987708 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t3748987708 *>((UnityAction_2_t3748987708 **)L_2, (UnityAction_2_t3748987708 *)((UnityAction_2_t3748987708 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t3748987708 *)L_6);
		V_0 = (UnityAction_2_t3748987708 *)L_7;
		UnityAction_2_t3748987708 * L_8 = V_0;
		UnityAction_2_t3748987708 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t3748987708 *)L_8) == ((Il2CppObject*)(UnityAction_2_t3748987708 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m2939077381_gshared (InvokableCall_2_t3763778592 * __this, UnityAction_2_t3748987708 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t3748987708 * V_0 = NULL;
	UnityAction_2_t3748987708 * V_1 = NULL;
	{
		UnityAction_2_t3748987708 * L_0 = (UnityAction_2_t3748987708 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t3748987708 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t3748987708 * L_1 = V_0;
		V_1 = (UnityAction_2_t3748987708 *)L_1;
		UnityAction_2_t3748987708 ** L_2 = (UnityAction_2_t3748987708 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t3748987708 * L_3 = V_1;
		UnityAction_2_t3748987708 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t3748987708 * L_6 = V_0;
		UnityAction_2_t3748987708 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t3748987708 *>((UnityAction_2_t3748987708 **)L_2, (UnityAction_2_t3748987708 *)((UnityAction_2_t3748987708 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t3748987708 *)L_6);
		V_0 = (UnityAction_2_t3748987708 *)L_7;
		UnityAction_2_t3748987708 * L_8 = V_0;
		UnityAction_2_t3748987708 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t3748987708 *)L_8) == ((Il2CppObject*)(UnityAction_2_t3748987708 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m414081419_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m414081419_gshared (InvokableCall_2_t3763778592 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m414081419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t3748987708 * L_8 = (UnityAction_2_t3748987708 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t3748987708 * L_10 = (UnityAction_2_t3748987708 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t3748987708 *)L_10);
		((  void (*) (UnityAction_2_t3748987708 *, Il2CppObject *, HeadsetControllerAwareEventArgs_t2653531721 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t3748987708 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (HeadsetControllerAwareEventArgs_t2653531721 )((*(HeadsetControllerAwareEventArgs_t2653531721 *)((HeadsetControllerAwareEventArgs_t2653531721 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m736191899_gshared (InvokableCall_2_t3763778592 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t3748987708 * L_0 = (UnityAction_2_t3748987708 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t3748987708 * L_3 = (UnityAction_2_t3748987708 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetFadeEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m807518802_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m807518802_gshared (InvokableCall_2_t4002788890 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m807518802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t3987998006 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetFadeEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m3803559441_gshared (InvokableCall_2_t4002788890 * __this, UnityAction_2_t3987998006 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t3987998006 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t4002788890 *)__this);
		((  void (*) (InvokableCall_2_t4002788890 *, UnityAction_2_t3987998006 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t4002788890 *)__this, (UnityAction_2_t3987998006 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetFadeEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m3497445164_gshared (InvokableCall_2_t4002788890 * __this, UnityAction_2_t3987998006 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t3987998006 * V_0 = NULL;
	UnityAction_2_t3987998006 * V_1 = NULL;
	{
		UnityAction_2_t3987998006 * L_0 = (UnityAction_2_t3987998006 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t3987998006 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t3987998006 * L_1 = V_0;
		V_1 = (UnityAction_2_t3987998006 *)L_1;
		UnityAction_2_t3987998006 ** L_2 = (UnityAction_2_t3987998006 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t3987998006 * L_3 = V_1;
		UnityAction_2_t3987998006 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t3987998006 * L_6 = V_0;
		UnityAction_2_t3987998006 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t3987998006 *>((UnityAction_2_t3987998006 **)L_2, (UnityAction_2_t3987998006 *)((UnityAction_2_t3987998006 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t3987998006 *)L_6);
		V_0 = (UnityAction_2_t3987998006 *)L_7;
		UnityAction_2_t3987998006 * L_8 = V_0;
		UnityAction_2_t3987998006 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t3987998006 *)L_8) == ((Il2CppObject*)(UnityAction_2_t3987998006 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetFadeEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m4092998459_gshared (InvokableCall_2_t4002788890 * __this, UnityAction_2_t3987998006 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t3987998006 * V_0 = NULL;
	UnityAction_2_t3987998006 * V_1 = NULL;
	{
		UnityAction_2_t3987998006 * L_0 = (UnityAction_2_t3987998006 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t3987998006 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t3987998006 * L_1 = V_0;
		V_1 = (UnityAction_2_t3987998006 *)L_1;
		UnityAction_2_t3987998006 ** L_2 = (UnityAction_2_t3987998006 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t3987998006 * L_3 = V_1;
		UnityAction_2_t3987998006 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t3987998006 * L_6 = V_0;
		UnityAction_2_t3987998006 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t3987998006 *>((UnityAction_2_t3987998006 **)L_2, (UnityAction_2_t3987998006 *)((UnityAction_2_t3987998006 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t3987998006 *)L_6);
		V_0 = (UnityAction_2_t3987998006 *)L_7;
		UnityAction_2_t3987998006 * L_8 = V_0;
		UnityAction_2_t3987998006 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t3987998006 *)L_8) == ((Il2CppObject*)(UnityAction_2_t3987998006 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetFadeEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m1477306801_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m1477306801_gshared (InvokableCall_2_t4002788890 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m1477306801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t3987998006 * L_8 = (UnityAction_2_t3987998006 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t3987998006 * L_10 = (UnityAction_2_t3987998006 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t3987998006 *)L_10);
		((  void (*) (UnityAction_2_t3987998006 *, Il2CppObject *, HeadsetFadeEventArgs_t2892542019 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t3987998006 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (HeadsetFadeEventArgs_t2892542019 )((*(HeadsetFadeEventArgs_t2892542019 *)((HeadsetFadeEventArgs_t2892542019 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetFadeEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m612403801_gshared (InvokableCall_2_t4002788890 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t3987998006 * L_0 = (UnityAction_2_t3987998006 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t3987998006 * L_3 = (UnityAction_2_t3987998006 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.InteractableObjectEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m2929970813_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m2929970813_gshared (InvokableCall_2_t1583422427 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m2929970813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t1568631543 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.InteractableObjectEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m4044741794_gshared (InvokableCall_2_t1583422427 * __this, UnityAction_2_t1568631543 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t1568631543 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t1583422427 *)__this);
		((  void (*) (InvokableCall_2_t1583422427 *, UnityAction_2_t1568631543 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t1583422427 *)__this, (UnityAction_2_t1568631543 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.InteractableObjectEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m951544561_gshared (InvokableCall_2_t1583422427 * __this, UnityAction_2_t1568631543 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t1568631543 * V_0 = NULL;
	UnityAction_2_t1568631543 * V_1 = NULL;
	{
		UnityAction_2_t1568631543 * L_0 = (UnityAction_2_t1568631543 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t1568631543 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t1568631543 * L_1 = V_0;
		V_1 = (UnityAction_2_t1568631543 *)L_1;
		UnityAction_2_t1568631543 ** L_2 = (UnityAction_2_t1568631543 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t1568631543 * L_3 = V_1;
		UnityAction_2_t1568631543 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t1568631543 * L_6 = V_0;
		UnityAction_2_t1568631543 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t1568631543 *>((UnityAction_2_t1568631543 **)L_2, (UnityAction_2_t1568631543 *)((UnityAction_2_t1568631543 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t1568631543 *)L_6);
		V_0 = (UnityAction_2_t1568631543 *)L_7;
		UnityAction_2_t1568631543 * L_8 = V_0;
		UnityAction_2_t1568631543 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t1568631543 *)L_8) == ((Il2CppObject*)(UnityAction_2_t1568631543 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.InteractableObjectEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m2157615652_gshared (InvokableCall_2_t1583422427 * __this, UnityAction_2_t1568631543 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t1568631543 * V_0 = NULL;
	UnityAction_2_t1568631543 * V_1 = NULL;
	{
		UnityAction_2_t1568631543 * L_0 = (UnityAction_2_t1568631543 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t1568631543 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t1568631543 * L_1 = V_0;
		V_1 = (UnityAction_2_t1568631543 *)L_1;
		UnityAction_2_t1568631543 ** L_2 = (UnityAction_2_t1568631543 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t1568631543 * L_3 = V_1;
		UnityAction_2_t1568631543 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t1568631543 * L_6 = V_0;
		UnityAction_2_t1568631543 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t1568631543 *>((UnityAction_2_t1568631543 **)L_2, (UnityAction_2_t1568631543 *)((UnityAction_2_t1568631543 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t1568631543 *)L_6);
		V_0 = (UnityAction_2_t1568631543 *)L_7;
		UnityAction_2_t1568631543 * L_8 = V_0;
		UnityAction_2_t1568631543 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t1568631543 *)L_8) == ((Il2CppObject*)(UnityAction_2_t1568631543 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.InteractableObjectEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m2098480210_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m2098480210_gshared (InvokableCall_2_t1583422427 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m2098480210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t1568631543 * L_8 = (UnityAction_2_t1568631543 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t1568631543 * L_10 = (UnityAction_2_t1568631543 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t1568631543 *)L_10);
		((  void (*) (UnityAction_2_t1568631543 *, Il2CppObject *, InteractableObjectEventArgs_t473175556 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t1568631543 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (InteractableObjectEventArgs_t473175556 )((*(InteractableObjectEventArgs_t473175556 *)((InteractableObjectEventArgs_t473175556 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.InteractableObjectEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m4083968974_gshared (InvokableCall_2_t1583422427 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t1568631543 * L_0 = (UnityAction_2_t1568631543 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t1568631543 * L_3 = (UnityAction_2_t1568631543 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ObjectControlEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m1392604122_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m1392604122_gshared (InvokableCall_2_t3569737190 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m1392604122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t3554946306 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ObjectControlEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m3795146611_gshared (InvokableCall_2_t3569737190 * __this, UnityAction_2_t3554946306 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t3554946306 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t3569737190 *)__this);
		((  void (*) (InvokableCall_2_t3569737190 *, UnityAction_2_t3554946306 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t3569737190 *)__this, (UnityAction_2_t3554946306 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ObjectControlEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m2167154932_gshared (InvokableCall_2_t3569737190 * __this, UnityAction_2_t3554946306 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t3554946306 * V_0 = NULL;
	UnityAction_2_t3554946306 * V_1 = NULL;
	{
		UnityAction_2_t3554946306 * L_0 = (UnityAction_2_t3554946306 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t3554946306 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t3554946306 * L_1 = V_0;
		V_1 = (UnityAction_2_t3554946306 *)L_1;
		UnityAction_2_t3554946306 ** L_2 = (UnityAction_2_t3554946306 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t3554946306 * L_3 = V_1;
		UnityAction_2_t3554946306 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t3554946306 * L_6 = V_0;
		UnityAction_2_t3554946306 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t3554946306 *>((UnityAction_2_t3554946306 **)L_2, (UnityAction_2_t3554946306 *)((UnityAction_2_t3554946306 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t3554946306 *)L_6);
		V_0 = (UnityAction_2_t3554946306 *)L_7;
		UnityAction_2_t3554946306 * L_8 = V_0;
		UnityAction_2_t3554946306 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t3554946306 *)L_8) == ((Il2CppObject*)(UnityAction_2_t3554946306 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ObjectControlEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m1695189133_gshared (InvokableCall_2_t3569737190 * __this, UnityAction_2_t3554946306 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t3554946306 * V_0 = NULL;
	UnityAction_2_t3554946306 * V_1 = NULL;
	{
		UnityAction_2_t3554946306 * L_0 = (UnityAction_2_t3554946306 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t3554946306 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t3554946306 * L_1 = V_0;
		V_1 = (UnityAction_2_t3554946306 *)L_1;
		UnityAction_2_t3554946306 ** L_2 = (UnityAction_2_t3554946306 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t3554946306 * L_3 = V_1;
		UnityAction_2_t3554946306 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t3554946306 * L_6 = V_0;
		UnityAction_2_t3554946306 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t3554946306 *>((UnityAction_2_t3554946306 **)L_2, (UnityAction_2_t3554946306 *)((UnityAction_2_t3554946306 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t3554946306 *)L_6);
		V_0 = (UnityAction_2_t3554946306 *)L_7;
		UnityAction_2_t3554946306 * L_8 = V_0;
		UnityAction_2_t3554946306 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t3554946306 *)L_8) == ((Il2CppObject*)(UnityAction_2_t3554946306 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ObjectControlEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m6340023_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m6340023_gshared (InvokableCall_2_t3569737190 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m6340023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t3554946306 * L_8 = (UnityAction_2_t3554946306 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t3554946306 * L_10 = (UnityAction_2_t3554946306 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t3554946306 *)L_10);
		((  void (*) (UnityAction_2_t3554946306 *, Il2CppObject *, ObjectControlEventArgs_t2459490319 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t3554946306 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (ObjectControlEventArgs_t2459490319 )((*(ObjectControlEventArgs_t2459490319 *)((ObjectControlEventArgs_t2459490319 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ObjectControlEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m1378996399_gshared (InvokableCall_2_t3569737190 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t3554946306 * L_0 = (UnityAction_2_t3554946306 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t3554946306 * L_3 = (UnityAction_2_t3554946306 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ObjectInteractEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m18933315_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m18933315_gshared (InvokableCall_2_t1881538113 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m18933315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t1866747229 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ObjectInteractEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m32818206_gshared (InvokableCall_2_t1881538113 * __this, UnityAction_2_t1866747229 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t1866747229 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t1881538113 *)__this);
		((  void (*) (InvokableCall_2_t1881538113 *, UnityAction_2_t1866747229 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t1881538113 *)__this, (UnityAction_2_t1866747229 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ObjectInteractEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m3752685555_gshared (InvokableCall_2_t1881538113 * __this, UnityAction_2_t1866747229 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t1866747229 * V_0 = NULL;
	UnityAction_2_t1866747229 * V_1 = NULL;
	{
		UnityAction_2_t1866747229 * L_0 = (UnityAction_2_t1866747229 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t1866747229 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t1866747229 * L_1 = V_0;
		V_1 = (UnityAction_2_t1866747229 *)L_1;
		UnityAction_2_t1866747229 ** L_2 = (UnityAction_2_t1866747229 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t1866747229 * L_3 = V_1;
		UnityAction_2_t1866747229 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t1866747229 * L_6 = V_0;
		UnityAction_2_t1866747229 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t1866747229 *>((UnityAction_2_t1866747229 **)L_2, (UnityAction_2_t1866747229 *)((UnityAction_2_t1866747229 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t1866747229 *)L_6);
		V_0 = (UnityAction_2_t1866747229 *)L_7;
		UnityAction_2_t1866747229 * L_8 = V_0;
		UnityAction_2_t1866747229 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t1866747229 *)L_8) == ((Il2CppObject*)(UnityAction_2_t1866747229 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ObjectInteractEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m1001512400_gshared (InvokableCall_2_t1881538113 * __this, UnityAction_2_t1866747229 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t1866747229 * V_0 = NULL;
	UnityAction_2_t1866747229 * V_1 = NULL;
	{
		UnityAction_2_t1866747229 * L_0 = (UnityAction_2_t1866747229 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t1866747229 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t1866747229 * L_1 = V_0;
		V_1 = (UnityAction_2_t1866747229 *)L_1;
		UnityAction_2_t1866747229 ** L_2 = (UnityAction_2_t1866747229 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t1866747229 * L_3 = V_1;
		UnityAction_2_t1866747229 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t1866747229 * L_6 = V_0;
		UnityAction_2_t1866747229 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t1866747229 *>((UnityAction_2_t1866747229 **)L_2, (UnityAction_2_t1866747229 *)((UnityAction_2_t1866747229 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t1866747229 *)L_6);
		V_0 = (UnityAction_2_t1866747229 *)L_7;
		UnityAction_2_t1866747229 * L_8 = V_0;
		UnityAction_2_t1866747229 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t1866747229 *)L_8) == ((Il2CppObject*)(UnityAction_2_t1866747229 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ObjectInteractEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m3558580146_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m3558580146_gshared (InvokableCall_2_t1881538113 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m3558580146_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t1866747229 * L_8 = (UnityAction_2_t1866747229 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t1866747229 * L_10 = (UnityAction_2_t1866747229 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t1866747229 *)L_10);
		((  void (*) (UnityAction_2_t1866747229 *, Il2CppObject *, ObjectInteractEventArgs_t771291242 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t1866747229 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (ObjectInteractEventArgs_t771291242 )((*(ObjectInteractEventArgs_t771291242 *)((ObjectInteractEventArgs_t771291242 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ObjectInteractEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m810415518_gshared (InvokableCall_2_t1881538113 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t1866747229 * L_0 = (UnityAction_2_t1866747229 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t1866747229 * L_3 = (UnityAction_2_t1866747229 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.PlayerClimbEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m2625014984_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m2625014984_gshared (InvokableCall_2_t3647832616 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m2625014984_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t3633041732 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.PlayerClimbEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m3415164735_gshared (InvokableCall_2_t3647832616 * __this, UnityAction_2_t3633041732 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t3633041732 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t3647832616 *)__this);
		((  void (*) (InvokableCall_2_t3647832616 *, UnityAction_2_t3633041732 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t3647832616 *)__this, (UnityAction_2_t3633041732 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.PlayerClimbEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m561449686_gshared (InvokableCall_2_t3647832616 * __this, UnityAction_2_t3633041732 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t3633041732 * V_0 = NULL;
	UnityAction_2_t3633041732 * V_1 = NULL;
	{
		UnityAction_2_t3633041732 * L_0 = (UnityAction_2_t3633041732 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t3633041732 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t3633041732 * L_1 = V_0;
		V_1 = (UnityAction_2_t3633041732 *)L_1;
		UnityAction_2_t3633041732 ** L_2 = (UnityAction_2_t3633041732 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t3633041732 * L_3 = V_1;
		UnityAction_2_t3633041732 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t3633041732 * L_6 = V_0;
		UnityAction_2_t3633041732 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t3633041732 *>((UnityAction_2_t3633041732 **)L_2, (UnityAction_2_t3633041732 *)((UnityAction_2_t3633041732 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t3633041732 *)L_6);
		V_0 = (UnityAction_2_t3633041732 *)L_7;
		UnityAction_2_t3633041732 * L_8 = V_0;
		UnityAction_2_t3633041732 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t3633041732 *)L_8) == ((Il2CppObject*)(UnityAction_2_t3633041732 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.PlayerClimbEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m598047497_gshared (InvokableCall_2_t3647832616 * __this, UnityAction_2_t3633041732 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t3633041732 * V_0 = NULL;
	UnityAction_2_t3633041732 * V_1 = NULL;
	{
		UnityAction_2_t3633041732 * L_0 = (UnityAction_2_t3633041732 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t3633041732 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t3633041732 * L_1 = V_0;
		V_1 = (UnityAction_2_t3633041732 *)L_1;
		UnityAction_2_t3633041732 ** L_2 = (UnityAction_2_t3633041732 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t3633041732 * L_3 = V_1;
		UnityAction_2_t3633041732 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t3633041732 * L_6 = V_0;
		UnityAction_2_t3633041732 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t3633041732 *>((UnityAction_2_t3633041732 **)L_2, (UnityAction_2_t3633041732 *)((UnityAction_2_t3633041732 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t3633041732 *)L_6);
		V_0 = (UnityAction_2_t3633041732 *)L_7;
		UnityAction_2_t3633041732 * L_8 = V_0;
		UnityAction_2_t3633041732 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t3633041732 *)L_8) == ((Il2CppObject*)(UnityAction_2_t3633041732 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.PlayerClimbEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m1221156515_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m1221156515_gshared (InvokableCall_2_t3647832616 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m1221156515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t3633041732 * L_8 = (UnityAction_2_t3633041732 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t3633041732 * L_10 = (UnityAction_2_t3633041732 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t3633041732 *)L_10);
		((  void (*) (UnityAction_2_t3633041732 *, Il2CppObject *, PlayerClimbEventArgs_t2537585745 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t3633041732 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (PlayerClimbEventArgs_t2537585745 )((*(PlayerClimbEventArgs_t2537585745 *)((PlayerClimbEventArgs_t2537585745 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.PlayerClimbEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m1446331987_gshared (InvokableCall_2_t3647832616 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t3633041732 * L_0 = (UnityAction_2_t3633041732 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t3633041732 * L_3 = (UnityAction_2_t3633041732 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.SnapDropZoneEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m2668677123_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m2668677123_gshared (InvokableCall_2_t1528949645 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m2668677123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t1514158761 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.SnapDropZoneEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m3910438400_gshared (InvokableCall_2_t1528949645 * __this, UnityAction_2_t1514158761 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t1514158761 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t1528949645 *)__this);
		((  void (*) (InvokableCall_2_t1528949645 *, UnityAction_2_t1514158761 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t1528949645 *)__this, (UnityAction_2_t1514158761 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.SnapDropZoneEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m3786062219_gshared (InvokableCall_2_t1528949645 * __this, UnityAction_2_t1514158761 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t1514158761 * V_0 = NULL;
	UnityAction_2_t1514158761 * V_1 = NULL;
	{
		UnityAction_2_t1514158761 * L_0 = (UnityAction_2_t1514158761 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t1514158761 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t1514158761 * L_1 = V_0;
		V_1 = (UnityAction_2_t1514158761 *)L_1;
		UnityAction_2_t1514158761 ** L_2 = (UnityAction_2_t1514158761 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t1514158761 * L_3 = V_1;
		UnityAction_2_t1514158761 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t1514158761 * L_6 = V_0;
		UnityAction_2_t1514158761 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t1514158761 *>((UnityAction_2_t1514158761 **)L_2, (UnityAction_2_t1514158761 *)((UnityAction_2_t1514158761 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t1514158761 *)L_6);
		V_0 = (UnityAction_2_t1514158761 *)L_7;
		UnityAction_2_t1514158761 * L_8 = V_0;
		UnityAction_2_t1514158761 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t1514158761 *)L_8) == ((Il2CppObject*)(UnityAction_2_t1514158761 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.SnapDropZoneEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m614995806_gshared (InvokableCall_2_t1528949645 * __this, UnityAction_2_t1514158761 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t1514158761 * V_0 = NULL;
	UnityAction_2_t1514158761 * V_1 = NULL;
	{
		UnityAction_2_t1514158761 * L_0 = (UnityAction_2_t1514158761 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t1514158761 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t1514158761 * L_1 = V_0;
		V_1 = (UnityAction_2_t1514158761 *)L_1;
		UnityAction_2_t1514158761 ** L_2 = (UnityAction_2_t1514158761 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t1514158761 * L_3 = V_1;
		UnityAction_2_t1514158761 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t1514158761 * L_6 = V_0;
		UnityAction_2_t1514158761 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t1514158761 *>((UnityAction_2_t1514158761 **)L_2, (UnityAction_2_t1514158761 *)((UnityAction_2_t1514158761 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t1514158761 *)L_6);
		V_0 = (UnityAction_2_t1514158761 *)L_7;
		UnityAction_2_t1514158761 * L_8 = V_0;
		UnityAction_2_t1514158761 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t1514158761 *)L_8) == ((Il2CppObject*)(UnityAction_2_t1514158761 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.SnapDropZoneEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m2553364292_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m2553364292_gshared (InvokableCall_2_t1528949645 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m2553364292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t1514158761 * L_8 = (UnityAction_2_t1514158761 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t1514158761 * L_10 = (UnityAction_2_t1514158761 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t1514158761 *)L_10);
		((  void (*) (UnityAction_2_t1514158761 *, Il2CppObject *, SnapDropZoneEventArgs_t418702774 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t1514158761 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (SnapDropZoneEventArgs_t418702774 )((*(SnapDropZoneEventArgs_t418702774 *)((SnapDropZoneEventArgs_t418702774 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.SnapDropZoneEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m3502364824_gshared (InvokableCall_2_t1528949645 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t1514158761 * L_0 = (UnityAction_2_t1514158761 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t1514158761 * L_3 = (UnityAction_2_t1514158761 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.UIPointerEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m2099925919_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m2099925919_gshared (InvokableCall_2_t2282232849 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m2099925919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t2267441965 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.UIPointerEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m95020894_gshared (InvokableCall_2_t2282232849 * __this, UnityAction_2_t2267441965 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t2267441965 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t2282232849 *)__this);
		((  void (*) (InvokableCall_2_t2282232849 *, UnityAction_2_t2267441965 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t2282232849 *)__this, (UnityAction_2_t2267441965 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.UIPointerEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m3071161839_gshared (InvokableCall_2_t2282232849 * __this, UnityAction_2_t2267441965 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t2267441965 * V_0 = NULL;
	UnityAction_2_t2267441965 * V_1 = NULL;
	{
		UnityAction_2_t2267441965 * L_0 = (UnityAction_2_t2267441965 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t2267441965 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t2267441965 * L_1 = V_0;
		V_1 = (UnityAction_2_t2267441965 *)L_1;
		UnityAction_2_t2267441965 ** L_2 = (UnityAction_2_t2267441965 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t2267441965 * L_3 = V_1;
		UnityAction_2_t2267441965 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t2267441965 * L_6 = V_0;
		UnityAction_2_t2267441965 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t2267441965 *>((UnityAction_2_t2267441965 **)L_2, (UnityAction_2_t2267441965 *)((UnityAction_2_t2267441965 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t2267441965 *)L_6);
		V_0 = (UnityAction_2_t2267441965 *)L_7;
		UnityAction_2_t2267441965 * L_8 = V_0;
		UnityAction_2_t2267441965 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t2267441965 *)L_8) == ((Il2CppObject*)(UnityAction_2_t2267441965 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.UIPointerEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m2611483588_gshared (InvokableCall_2_t2282232849 * __this, UnityAction_2_t2267441965 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t2267441965 * V_0 = NULL;
	UnityAction_2_t2267441965 * V_1 = NULL;
	{
		UnityAction_2_t2267441965 * L_0 = (UnityAction_2_t2267441965 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t2267441965 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t2267441965 * L_1 = V_0;
		V_1 = (UnityAction_2_t2267441965 *)L_1;
		UnityAction_2_t2267441965 ** L_2 = (UnityAction_2_t2267441965 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t2267441965 * L_3 = V_1;
		UnityAction_2_t2267441965 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t2267441965 * L_6 = V_0;
		UnityAction_2_t2267441965 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t2267441965 *>((UnityAction_2_t2267441965 **)L_2, (UnityAction_2_t2267441965 *)((UnityAction_2_t2267441965 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t2267441965 *)L_6);
		V_0 = (UnityAction_2_t2267441965 *)L_7;
		UnityAction_2_t2267441965 * L_8 = V_0;
		UnityAction_2_t2267441965 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t2267441965 *)L_8) == ((Il2CppObject*)(UnityAction_2_t2267441965 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.UIPointerEventArgs>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m943887418_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m943887418_gshared (InvokableCall_2_t2282232849 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m943887418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t2267441965 * L_8 = (UnityAction_2_t2267441965 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t2267441965 * L_10 = (UnityAction_2_t2267441965 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t2267441965 *)L_10);
		((  void (*) (UnityAction_2_t2267441965 *, Il2CppObject *, UIPointerEventArgs_t1171985978 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t2267441965 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (UIPointerEventArgs_t1171985978 )((*(UIPointerEventArgs_t1171985978 *)((UIPointerEventArgs_t1171985978 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.UIPointerEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m3020936254_gshared (InvokableCall_2_t2282232849 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t2267441965 * L_0 = (UnityAction_2_t2267441965 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t2267441965 * L_3 = (UnityAction_2_t2267441965 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Single,System.Single>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m451078716_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m451078716_gshared (InvokableCall_2_t149250066 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m451078716_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t134459182 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Single,System.Single>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m2243606533_gshared (InvokableCall_2_t149250066 * __this, UnityAction_2_t134459182 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_2_t134459182 * L_0 = ___action0;
		NullCheck((InvokableCall_2_t149250066 *)__this);
		((  void (*) (InvokableCall_2_t149250066 *, UnityAction_2_t134459182 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_2_t149250066 *)__this, (UnityAction_2_t134459182 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Single,System.Single>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m687719050_gshared (InvokableCall_2_t149250066 * __this, UnityAction_2_t134459182 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t134459182 * V_0 = NULL;
	UnityAction_2_t134459182 * V_1 = NULL;
	{
		UnityAction_2_t134459182 * L_0 = (UnityAction_2_t134459182 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t134459182 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t134459182 * L_1 = V_0;
		V_1 = (UnityAction_2_t134459182 *)L_1;
		UnityAction_2_t134459182 ** L_2 = (UnityAction_2_t134459182 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t134459182 * L_3 = V_1;
		UnityAction_2_t134459182 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t134459182 * L_6 = V_0;
		UnityAction_2_t134459182 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t134459182 *>((UnityAction_2_t134459182 **)L_2, (UnityAction_2_t134459182 *)((UnityAction_2_t134459182 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t134459182 *)L_6);
		V_0 = (UnityAction_2_t134459182 *)L_7;
		UnityAction_2_t134459182 * L_8 = V_0;
		UnityAction_2_t134459182 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t134459182 *)L_8) == ((Il2CppObject*)(UnityAction_2_t134459182 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Single,System.Single>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m4249474923_gshared (InvokableCall_2_t149250066 * __this, UnityAction_2_t134459182 * ___value0, const MethodInfo* method)
{
	UnityAction_2_t134459182 * V_0 = NULL;
	UnityAction_2_t134459182 * V_1 = NULL;
	{
		UnityAction_2_t134459182 * L_0 = (UnityAction_2_t134459182 *)__this->get_Delegate_0();
		V_0 = (UnityAction_2_t134459182 *)L_0;
	}

IL_0007:
	{
		UnityAction_2_t134459182 * L_1 = V_0;
		V_1 = (UnityAction_2_t134459182 *)L_1;
		UnityAction_2_t134459182 ** L_2 = (UnityAction_2_t134459182 **)__this->get_address_of_Delegate_0();
		UnityAction_2_t134459182 * L_3 = V_1;
		UnityAction_2_t134459182 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_2_t134459182 * L_6 = V_0;
		UnityAction_2_t134459182 * L_7 = InterlockedCompareExchangeImpl<UnityAction_2_t134459182 *>((UnityAction_2_t134459182 **)L_2, (UnityAction_2_t134459182 *)((UnityAction_2_t134459182 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_2_t134459182 *)L_6);
		V_0 = (UnityAction_2_t134459182 *)L_7;
		UnityAction_2_t134459182 * L_8 = V_0;
		UnityAction_2_t134459182 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_2_t134459182 *)L_8) == ((Il2CppObject*)(UnityAction_2_t134459182 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Single,System.Single>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m3692277805_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m3692277805_gshared (InvokableCall_2_t149250066 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m3692277805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_2_t134459182 * L_8 = (UnityAction_2_t134459182 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t134459182 * L_10 = (UnityAction_2_t134459182 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t134459182 *)L_10);
		((  void (*) (UnityAction_2_t134459182 *, float, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_2_t134459182 *)L_10, (float)((*(float*)((float*)UnBox (L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (float)((*(float*)((float*)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Single,System.Single>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m762004677_gshared (InvokableCall_2_t149250066 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t134459182 * L_0 = (UnityAction_2_t134459182 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t134459182 * L_3 = (UnityAction_2_t134459182 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_3__ctor_m3141607487_MetadataUsageId;
extern "C"  void InvokableCall_3__ctor_m3141607487_gshared (InvokableCall_3_t2191335654 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3__ctor_m3141607487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_3_t3482433968 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::.ctor(UnityEngine.Events.UnityAction`3<T1,T2,T3>)
extern "C"  void InvokableCall_3__ctor_m2259070082_gshared (InvokableCall_3_t2191335654 * __this, UnityAction_3_t3482433968 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_3_t3482433968 * L_0 = ___action0;
		NullCheck((InvokableCall_3_t2191335654 *)__this);
		((  void (*) (InvokableCall_3_t2191335654 *, UnityAction_3_t3482433968 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_3_t2191335654 *)__this, (UnityAction_3_t3482433968 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::add_Delegate(UnityEngine.Events.UnityAction`3<T1,T2,T3>)
extern "C"  void InvokableCall_3_add_Delegate_m1651912393_gshared (InvokableCall_3_t2191335654 * __this, UnityAction_3_t3482433968 * ___value0, const MethodInfo* method)
{
	UnityAction_3_t3482433968 * V_0 = NULL;
	UnityAction_3_t3482433968 * V_1 = NULL;
	{
		UnityAction_3_t3482433968 * L_0 = (UnityAction_3_t3482433968 *)__this->get_Delegate_0();
		V_0 = (UnityAction_3_t3482433968 *)L_0;
	}

IL_0007:
	{
		UnityAction_3_t3482433968 * L_1 = V_0;
		V_1 = (UnityAction_3_t3482433968 *)L_1;
		UnityAction_3_t3482433968 ** L_2 = (UnityAction_3_t3482433968 **)__this->get_address_of_Delegate_0();
		UnityAction_3_t3482433968 * L_3 = V_1;
		UnityAction_3_t3482433968 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_3_t3482433968 * L_6 = V_0;
		UnityAction_3_t3482433968 * L_7 = InterlockedCompareExchangeImpl<UnityAction_3_t3482433968 *>((UnityAction_3_t3482433968 **)L_2, (UnityAction_3_t3482433968 *)((UnityAction_3_t3482433968 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_3_t3482433968 *)L_6);
		V_0 = (UnityAction_3_t3482433968 *)L_7;
		UnityAction_3_t3482433968 * L_8 = V_0;
		UnityAction_3_t3482433968 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_3_t3482433968 *)L_8) == ((Il2CppObject*)(UnityAction_3_t3482433968 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::remove_Delegate(UnityEngine.Events.UnityAction`3<T1,T2,T3>)
extern "C"  void InvokableCall_3_remove_Delegate_m2026407704_gshared (InvokableCall_3_t2191335654 * __this, UnityAction_3_t3482433968 * ___value0, const MethodInfo* method)
{
	UnityAction_3_t3482433968 * V_0 = NULL;
	UnityAction_3_t3482433968 * V_1 = NULL;
	{
		UnityAction_3_t3482433968 * L_0 = (UnityAction_3_t3482433968 *)__this->get_Delegate_0();
		V_0 = (UnityAction_3_t3482433968 *)L_0;
	}

IL_0007:
	{
		UnityAction_3_t3482433968 * L_1 = V_0;
		V_1 = (UnityAction_3_t3482433968 *)L_1;
		UnityAction_3_t3482433968 ** L_2 = (UnityAction_3_t3482433968 **)__this->get_address_of_Delegate_0();
		UnityAction_3_t3482433968 * L_3 = V_1;
		UnityAction_3_t3482433968 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_3_t3482433968 * L_6 = V_0;
		UnityAction_3_t3482433968 * L_7 = InterlockedCompareExchangeImpl<UnityAction_3_t3482433968 *>((UnityAction_3_t3482433968 **)L_2, (UnityAction_3_t3482433968 *)((UnityAction_3_t3482433968 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_3_t3482433968 *)L_6);
		V_0 = (UnityAction_3_t3482433968 *)L_7;
		UnityAction_3_t3482433968 * L_8 = V_0;
		UnityAction_3_t3482433968 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_3_t3482433968 *)L_8) == ((Il2CppObject*)(UnityAction_3_t3482433968 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_3_Invoke_m74557124_MetadataUsageId;
extern "C"  void InvokableCall_3_Invoke_m74557124_gshared (InvokableCall_3_t2191335654 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3_Invoke_m74557124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 2;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		UnityAction_3_t3482433968 * L_11 = (UnityAction_3_t3482433968 *)__this->get_Delegate_0();
		bool L_12 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0060;
		}
	}
	{
		UnityAction_3_t3482433968 * L_13 = (UnityAction_3_t3482433968 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 0;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		ObjectU5BU5D_t3614634134* L_17 = ___args0;
		NullCheck(L_17);
		int32_t L_18 = 1;
		Il2CppObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t3614634134* L_20 = ___args0;
		NullCheck(L_20);
		int32_t L_21 = 2;
		Il2CppObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck((UnityAction_3_t3482433968 *)L_13);
		((  void (*) (UnityAction_3_t3482433968 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((UnityAction_3_t3482433968 *)L_13, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), (Il2CppObject *)((Il2CppObject *)Castclass(L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), (Il2CppObject *)((Il2CppObject *)Castclass(L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_0060:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_3_Find_m3470456112_gshared (InvokableCall_3_t2191335654 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_3_t3482433968 * L_0 = (UnityAction_3_t3482433968 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_3_t3482433968 * L_3 = (UnityAction_3_t3482433968 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<UnityEngine.Color,System.Single,System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_3__ctor_m251418621_MetadataUsageId;
extern "C"  void InvokableCall_3__ctor_m251418621_gshared (InvokableCall_3_t524958208 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3__ctor_m251418621_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_3_t1816056522 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<UnityEngine.Color,System.Single,System.Boolean>::.ctor(UnityEngine.Events.UnityAction`3<T1,T2,T3>)
extern "C"  void InvokableCall_3__ctor_m2381876632_gshared (InvokableCall_3_t524958208 * __this, UnityAction_3_t1816056522 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_3_t1816056522 * L_0 = ___action0;
		NullCheck((InvokableCall_3_t524958208 *)__this);
		((  void (*) (InvokableCall_3_t524958208 *, UnityAction_3_t1816056522 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_3_t524958208 *)__this, (UnityAction_3_t1816056522 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<UnityEngine.Color,System.Single,System.Boolean>::add_Delegate(UnityEngine.Events.UnityAction`3<T1,T2,T3>)
extern "C"  void InvokableCall_3_add_Delegate_m3487329043_gshared (InvokableCall_3_t524958208 * __this, UnityAction_3_t1816056522 * ___value0, const MethodInfo* method)
{
	UnityAction_3_t1816056522 * V_0 = NULL;
	UnityAction_3_t1816056522 * V_1 = NULL;
	{
		UnityAction_3_t1816056522 * L_0 = (UnityAction_3_t1816056522 *)__this->get_Delegate_0();
		V_0 = (UnityAction_3_t1816056522 *)L_0;
	}

IL_0007:
	{
		UnityAction_3_t1816056522 * L_1 = V_0;
		V_1 = (UnityAction_3_t1816056522 *)L_1;
		UnityAction_3_t1816056522 ** L_2 = (UnityAction_3_t1816056522 **)__this->get_address_of_Delegate_0();
		UnityAction_3_t1816056522 * L_3 = V_1;
		UnityAction_3_t1816056522 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_3_t1816056522 * L_6 = V_0;
		UnityAction_3_t1816056522 * L_7 = InterlockedCompareExchangeImpl<UnityAction_3_t1816056522 *>((UnityAction_3_t1816056522 **)L_2, (UnityAction_3_t1816056522 *)((UnityAction_3_t1816056522 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_3_t1816056522 *)L_6);
		V_0 = (UnityAction_3_t1816056522 *)L_7;
		UnityAction_3_t1816056522 * L_8 = V_0;
		UnityAction_3_t1816056522 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_3_t1816056522 *)L_8) == ((Il2CppObject*)(UnityAction_3_t1816056522 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<UnityEngine.Color,System.Single,System.Boolean>::remove_Delegate(UnityEngine.Events.UnityAction`3<T1,T2,T3>)
extern "C"  void InvokableCall_3_remove_Delegate_m3809292406_gshared (InvokableCall_3_t524958208 * __this, UnityAction_3_t1816056522 * ___value0, const MethodInfo* method)
{
	UnityAction_3_t1816056522 * V_0 = NULL;
	UnityAction_3_t1816056522 * V_1 = NULL;
	{
		UnityAction_3_t1816056522 * L_0 = (UnityAction_3_t1816056522 *)__this->get_Delegate_0();
		V_0 = (UnityAction_3_t1816056522 *)L_0;
	}

IL_0007:
	{
		UnityAction_3_t1816056522 * L_1 = V_0;
		V_1 = (UnityAction_3_t1816056522 *)L_1;
		UnityAction_3_t1816056522 ** L_2 = (UnityAction_3_t1816056522 **)__this->get_address_of_Delegate_0();
		UnityAction_3_t1816056522 * L_3 = V_1;
		UnityAction_3_t1816056522 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_3_t1816056522 * L_6 = V_0;
		UnityAction_3_t1816056522 * L_7 = InterlockedCompareExchangeImpl<UnityAction_3_t1816056522 *>((UnityAction_3_t1816056522 **)L_2, (UnityAction_3_t1816056522 *)((UnityAction_3_t1816056522 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_3_t1816056522 *)L_6);
		V_0 = (UnityAction_3_t1816056522 *)L_7;
		UnityAction_3_t1816056522 * L_8 = V_0;
		UnityAction_3_t1816056522 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_3_t1816056522 *)L_8) == ((Il2CppObject*)(UnityAction_3_t1816056522 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<UnityEngine.Color,System.Single,System.Boolean>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_3_Invoke_m1354224562_MetadataUsageId;
extern "C"  void InvokableCall_3_Invoke_m1354224562_gshared (InvokableCall_3_t524958208 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3_Invoke_m1354224562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 2;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		UnityAction_3_t1816056522 * L_11 = (UnityAction_3_t1816056522 *)__this->get_Delegate_0();
		bool L_12 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0060;
		}
	}
	{
		UnityAction_3_t1816056522 * L_13 = (UnityAction_3_t1816056522 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 0;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		ObjectU5BU5D_t3614634134* L_17 = ___args0;
		NullCheck(L_17);
		int32_t L_18 = 1;
		Il2CppObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t3614634134* L_20 = ___args0;
		NullCheck(L_20);
		int32_t L_21 = 2;
		Il2CppObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck((UnityAction_3_t1816056522 *)L_13);
		((  void (*) (UnityAction_3_t1816056522 *, Color_t2020392075 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((UnityAction_3_t1816056522 *)L_13, (Color_t2020392075 )((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox (L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))))), (float)((*(float*)((float*)UnBox (L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (bool)((*(bool*)((bool*)UnBox (L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_0060:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`3<UnityEngine.Color,System.Single,System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_3_Find_m3251042214_gshared (InvokableCall_3_t524958208 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_3_t1816056522 * L_0 = (UnityAction_3_t1816056522 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_3_t1816056522 * L_3 = (UnityAction_3_t1816056522 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_4__ctor_m1096399974_MetadataUsageId;
extern "C"  void InvokableCall_4__ctor_m1096399974_gshared (InvokableCall_4_t2955480072 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_4__ctor_m1096399974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_4_t1666603240 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_4_Invoke_m1555001411_MetadataUsageId;
extern "C"  void InvokableCall_4_Invoke_m1555001411_gshared (InvokableCall_4_t2955480072 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_4_Invoke_m1555001411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)4)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 2;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 3;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_4_t1666603240 * L_14 = (UnityAction_4_t1666603240 *)__this->get_Delegate_0();
		bool L_15 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0070;
		}
	}
	{
		UnityAction_4_t1666603240 * L_16 = (UnityAction_4_t1666603240 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_17 = ___args0;
		NullCheck(L_17);
		int32_t L_18 = 0;
		Il2CppObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t3614634134* L_20 = ___args0;
		NullCheck(L_20);
		int32_t L_21 = 1;
		Il2CppObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		ObjectU5BU5D_t3614634134* L_23 = ___args0;
		NullCheck(L_23);
		int32_t L_24 = 2;
		Il2CppObject * L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		ObjectU5BU5D_t3614634134* L_26 = ___args0;
		NullCheck(L_26);
		int32_t L_27 = 3;
		Il2CppObject * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck((UnityAction_4_t1666603240 *)L_16);
		((  void (*) (UnityAction_4_t1666603240 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((UnityAction_4_t1666603240 *)L_16, (Il2CppObject *)((Il2CppObject *)Castclass(L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), (Il2CppObject *)((Il2CppObject *)Castclass(L_25, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_0070:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_4_Find_m1467690987_gshared (InvokableCall_4_t2955480072 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_4_t1666603240 * L_0 = (UnityAction_4_t1666603240 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_4_t1666603240 * L_3 = (UnityAction_4_t1666603240 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m1968084291_gshared (UnityAction_1_t897193173 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3523417209_gshared (UnityAction_1_t897193173 * __this, bool ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m3523417209((UnityAction_1_t897193173 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Boolean>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m2512011642_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m2512011642_gshared (UnityAction_1_t897193173 * __this, bool ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m2512011642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3317901367_gshared (UnityAction_1_t897193173 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m25541871_gshared (UnityAction_1_t3438463199 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2563101999_gshared (UnityAction_1_t3438463199 * __this, int32_t ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2563101999((UnityAction_1_t3438463199 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Int32>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m530778538_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m530778538_gshared (UnityAction_1_t3438463199 * __this, int32_t ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m530778538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m1662218393_gshared (UnityAction_1_t3438463199 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2836997866_gshared (UnityAction_1_t4056035046 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m1279804060_gshared (UnityAction_1_t4056035046 * __this, Il2CppObject * ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m1279804060((UnityAction_1_t4056035046 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Object>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m3462722079_gshared (UnityAction_1_t4056035046 * __this, Il2CppObject * ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg00;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m2822290096_gshared (UnityAction_1_t4056035046 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2172708761_gshared (UnityAction_1_t3443095683 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2563206587_gshared (UnityAction_1_t3443095683 * __this, float ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2563206587((UnityAction_1_t3443095683 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Single>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m4162767106_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m4162767106_gshared (UnityAction_1_t3443095683 * __this, float ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m4162767106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3175338521_gshared (UnityAction_1_t3443095683 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m3329809356_gshared (UnityAction_1_t3386977826 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2771701188_gshared (UnityAction_1_t3386977826 * __this, Color_t2020392075  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2771701188((UnityAction_1_t3386977826 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t2020392075  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t2020392075  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Color>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m2192647899_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m2192647899_gshared (UnityAction_1_t3386977826 * __this, Color_t2020392075  ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m2192647899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t2020392075_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m2603848420_gshared (UnityAction_1_t3386977826 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2627946124_gshared (UnityAction_1_t3051495417 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3061904506_gshared (UnityAction_1_t3051495417 * __this, Scene_t1684909666  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m3061904506((UnityAction_1_t3051495417 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t1684909666  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Scene_t1684909666  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Scene_t1684909666_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m2974933271_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m2974933271_gshared (UnityAction_1_t3051495417 * __this, Scene_t1684909666  ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m2974933271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Scene_t1684909666_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3641222126_gshared (UnityAction_1_t3051495417 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m1266646666_gshared (UnityAction_1_t3610293330 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2702242020_gshared (UnityAction_1_t3610293330 * __this, Vector2_t2243707579  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2702242020((UnityAction_1_t3610293330 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m4083379797_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m4083379797_gshared (UnityAction_1_t3610293330 * __this, Vector2_t2243707579  ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m4083379797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m539982532_gshared (UnityAction_1_t3610293330 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m303438895_gshared (UnityAction_1_t3610293331 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector3>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2829170431_gshared (UnityAction_1_t3610293331 * __this, Vector3_t2243707580  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2829170431((UnityAction_1_t3610293331 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Vector3>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m2893418810_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m2893418810_gshared (UnityAction_1_t3610293331 * __this, Vector3_t2243707580  ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m2893418810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m1324198697_gshared (UnityAction_1_t3610293331 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<Valve.VR.VREvent_t>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m48331784_gshared (UnityAction_1_t476884844 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<Valve.VR.VREvent_t>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m1847946961_gshared (UnityAction_1_t476884844 * __this, VREvent_t_t3405266389  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m1847946961((UnityAction_1_t476884844 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VREvent_t_t3405266389  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, VREvent_t_t3405266389  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Valve.VR.VREvent_t>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* VREvent_t_t3405266389_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m2925219304_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m2925219304_gshared (UnityAction_1_t476884844 * __this, VREvent_t_t3405266389  ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m2925219304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(VREvent_t_t3405266389_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<Valve.VR.VREvent_t>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m1211668059_gshared (UnityAction_1_t476884844 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m3320956550_gshared (UnityAction_2_t41828916 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1847343234_gshared (UnityAction_2_t41828916 * __this, int32_t ___arg00, bool ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m1847343234((UnityAction_2_t41828916 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, bool ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, bool ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m2300242877_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m2300242877_gshared (UnityAction_2_t41828916 * __this, int32_t ___arg00, bool ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m2300242877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m1764358569_gshared (UnityAction_2_t41828916 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m2121637916_gshared (UnityAction_2_t626063409 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Boolean>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m3333140761_gshared (UnityAction_2_t626063409 * __this, Il2CppObject * ___arg00, bool ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m3333140761((UnityAction_2_t626063409 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, bool ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, bool ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,System.Boolean>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m2287629450_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m2287629450_gshared (UnityAction_2_t626063409 * __this, Il2CppObject * ___arg00, bool ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m2287629450_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m2394165802_gshared (UnityAction_2_t626063409 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m622153369_gshared (UnityAction_2_t3784905282 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1994351568_gshared (UnityAction_2_t3784905282 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m1994351568((UnityAction_2_t3784905282 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,System.Object>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m3203769083_gshared (UnityAction_2_t3784905282 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m4199296611_gshared (UnityAction_2_t3784905282 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.BodyPhysicsEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m2892912568_gshared (UnityAction_2_t3325587641 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.BodyPhysicsEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m2108171075_gshared (UnityAction_2_t3325587641 * __this, Il2CppObject * ___arg00, BodyPhysicsEventArgs_t2230131654  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m2108171075((UnityAction_2_t3325587641 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, BodyPhysicsEventArgs_t2230131654  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, BodyPhysicsEventArgs_t2230131654  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, BodyPhysicsEventArgs_t2230131654  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.BodyPhysicsEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* BodyPhysicsEventArgs_t2230131654_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m934004762_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m934004762_gshared (UnityAction_2_t3325587641 * __this, Il2CppObject * ___arg00, BodyPhysicsEventArgs_t2230131654  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m934004762_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(BodyPhysicsEventArgs_t2230131654_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.BodyPhysicsEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m3559283934_gshared (UnityAction_2_t3325587641 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.Control3DEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m1464262360_gshared (UnityAction_2_t895514392 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.Control3DEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m4247595876_gshared (UnityAction_2_t895514392 * __this, Il2CppObject * ___arg00, Control3DEventArgs_t4095025701  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m4247595876((UnityAction_2_t895514392 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Control3DEventArgs_t4095025701  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Control3DEventArgs_t4095025701  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Control3DEventArgs_t4095025701  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.Control3DEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* Control3DEventArgs_t4095025701_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m367902827_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m367902827_gshared (UnityAction_2_t895514392 * __this, Il2CppObject * ___arg00, Control3DEventArgs_t4095025701  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m367902827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(Control3DEventArgs_t4095025701_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.Control3DEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m1012309827_gshared (UnityAction_2_t895514392 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerActionsEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m3160741330_gshared (UnityAction_2_t1439457463 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerActionsEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m3715173209_gshared (UnityAction_2_t1439457463 * __this, Il2CppObject * ___arg00, ControllerActionsEventArgs_t344001476  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m3715173209((UnityAction_2_t1439457463 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, ControllerActionsEventArgs_t344001476  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, ControllerActionsEventArgs_t344001476  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ControllerActionsEventArgs_t344001476  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerActionsEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* ControllerActionsEventArgs_t344001476_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m1998387504_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m1998387504_gshared (UnityAction_2_t1439457463 * __this, Il2CppObject * ___arg00, ControllerActionsEventArgs_t344001476  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m1998387504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(ControllerActionsEventArgs_t344001476_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerActionsEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m3153296264_gshared (UnityAction_2_t1439457463 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerInteractionEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m1398844047_gshared (UnityAction_2_t1383093526 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerInteractionEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1553017970_gshared (UnityAction_2_t1383093526 * __this, Il2CppObject * ___arg00, ControllerInteractionEventArgs_t287637539  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m1553017970((UnityAction_2_t1383093526 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, ControllerInteractionEventArgs_t287637539  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, ControllerInteractionEventArgs_t287637539  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ControllerInteractionEventArgs_t287637539  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerInteractionEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* ControllerInteractionEventArgs_t287637539_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m799512457_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m799512457_gshared (UnityAction_2_t1383093526 * __this, Il2CppObject * ___arg00, ControllerInteractionEventArgs_t287637539  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m799512457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(ControllerInteractionEventArgs_t287637539_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerInteractionEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m389326813_gshared (UnityAction_2_t1383093526 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.DashTeleportEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m188569052_gshared (UnityAction_2_t3292709229 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.DashTeleportEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m665084843_gshared (UnityAction_2_t3292709229 * __this, Il2CppObject * ___arg00, DashTeleportEventArgs_t2197253242  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m665084843((UnityAction_2_t3292709229 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, DashTeleportEventArgs_t2197253242  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, DashTeleportEventArgs_t2197253242  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, DashTeleportEventArgs_t2197253242  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.DashTeleportEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* DashTeleportEventArgs_t2197253242_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m1895056870_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m1895056870_gshared (UnityAction_2_t3292709229 * __this, Il2CppObject * ___arg00, DashTeleportEventArgs_t2197253242  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m1895056870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(DashTeleportEventArgs_t2197253242_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.DashTeleportEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m802328566_gshared (UnityAction_2_t3292709229 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.DestinationMarkerEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m2766621147_gshared (UnityAction_2_t2947907648 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.DestinationMarkerEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m3994259848_gshared (UnityAction_2_t2947907648 * __this, Il2CppObject * ___arg00, DestinationMarkerEventArgs_t1852451661  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m3994259848((UnityAction_2_t2947907648 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, DestinationMarkerEventArgs_t1852451661  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, DestinationMarkerEventArgs_t1852451661  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, DestinationMarkerEventArgs_t1852451661  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.DestinationMarkerEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* DestinationMarkerEventArgs_t1852451661_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m2362485269_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m2362485269_gshared (UnityAction_2_t2947907648 * __this, Il2CppObject * ___arg00, DestinationMarkerEventArgs_t1852451661  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m2362485269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(DestinationMarkerEventArgs_t1852451661_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.DestinationMarkerEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m3914811705_gshared (UnityAction_2_t2947907648 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetCollisionEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m229790613_gshared (UnityAction_2_t2337829374 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetCollisionEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1541967082_gshared (UnityAction_2_t2337829374 * __this, Il2CppObject * ___arg00, HeadsetCollisionEventArgs_t1242373387  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m1541967082((UnityAction_2_t2337829374 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, HeadsetCollisionEventArgs_t1242373387  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, HeadsetCollisionEventArgs_t1242373387  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, HeadsetCollisionEventArgs_t1242373387  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetCollisionEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* HeadsetCollisionEventArgs_t1242373387_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m2442881263_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m2442881263_gshared (UnityAction_2_t2337829374 * __this, Il2CppObject * ___arg00, HeadsetCollisionEventArgs_t1242373387  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m2442881263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(HeadsetCollisionEventArgs_t1242373387_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetCollisionEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m2851564815_gshared (UnityAction_2_t2337829374 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m4046930207_gshared (UnityAction_2_t3748987708 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m2476246304_gshared (UnityAction_2_t3748987708 * __this, Il2CppObject * ___arg00, HeadsetControllerAwareEventArgs_t2653531721  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m2476246304((UnityAction_2_t3748987708 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, HeadsetControllerAwareEventArgs_t2653531721  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, HeadsetControllerAwareEventArgs_t2653531721  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, HeadsetControllerAwareEventArgs_t2653531721  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* HeadsetControllerAwareEventArgs_t2653531721_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m3762773945_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m3762773945_gshared (UnityAction_2_t3748987708 * __this, Il2CppObject * ___arg00, HeadsetControllerAwareEventArgs_t2653531721  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m3762773945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(HeadsetControllerAwareEventArgs_t2653531721_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m113972697_gshared (UnityAction_2_t3748987708 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetFadeEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m4034615565_gshared (UnityAction_2_t3987998006 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetFadeEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m627690614_gshared (UnityAction_2_t3987998006 * __this, Il2CppObject * ___arg00, HeadsetFadeEventArgs_t2892542019  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m627690614((UnityAction_2_t3987998006 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, HeadsetFadeEventArgs_t2892542019  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, HeadsetFadeEventArgs_t2892542019  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, HeadsetFadeEventArgs_t2892542019  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetFadeEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* HeadsetFadeEventArgs_t2892542019_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m2372199027_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m2372199027_gshared (UnityAction_2_t3987998006 * __this, Il2CppObject * ___arg00, HeadsetFadeEventArgs_t2892542019  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m2372199027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(HeadsetFadeEventArgs_t2892542019_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetFadeEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m1928413947_gshared (UnityAction_2_t3987998006 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.InteractableObjectEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m2538499918_gshared (UnityAction_2_t1568631543 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.InteractableObjectEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m3061957273_gshared (UnityAction_2_t1568631543 * __this, Il2CppObject * ___arg00, InteractableObjectEventArgs_t473175556  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m3061957273((UnityAction_2_t1568631543 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, InteractableObjectEventArgs_t473175556  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, InteractableObjectEventArgs_t473175556  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, InteractableObjectEventArgs_t473175556  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.InteractableObjectEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* InteractableObjectEventArgs_t473175556_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m2554153860_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m2554153860_gshared (UnityAction_2_t1568631543 * __this, Il2CppObject * ___arg00, InteractableObjectEventArgs_t473175556  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m2554153860_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(InteractableObjectEventArgs_t473175556_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.InteractableObjectEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m763915412_gshared (UnityAction_2_t1568631543 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.ObjectControlEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m2690657915_gshared (UnityAction_2_t3554946306 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.ObjectControlEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m811706310_gshared (UnityAction_2_t3554946306 * __this, Il2CppObject * ___arg00, ObjectControlEventArgs_t2459490319  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m811706310((UnityAction_2_t3554946306 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, ObjectControlEventArgs_t2459490319  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, ObjectControlEventArgs_t2459490319  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ObjectControlEventArgs_t2459490319  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.ObjectControlEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* ObjectControlEventArgs_t2459490319_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m525178509_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m525178509_gshared (UnityAction_2_t3554946306 * __this, Il2CppObject * ___arg00, ObjectControlEventArgs_t2459490319  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m525178509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(ObjectControlEventArgs_t2459490319_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.ObjectControlEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m3830824609_gshared (UnityAction_2_t3554946306 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.ObjectInteractEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m20579542_gshared (UnityAction_2_t1866747229 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.ObjectInteractEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m3902685671_gshared (UnityAction_2_t1866747229 * __this, Il2CppObject * ___arg00, ObjectInteractEventArgs_t771291242  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m3902685671((UnityAction_2_t1866747229 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, ObjectInteractEventArgs_t771291242  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, ObjectInteractEventArgs_t771291242  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ObjectInteractEventArgs_t771291242  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.ObjectInteractEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* ObjectInteractEventArgs_t771291242_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m661860476_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m661860476_gshared (UnityAction_2_t1866747229 * __this, Il2CppObject * ___arg00, ObjectInteractEventArgs_t771291242  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m661860476_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(ObjectInteractEventArgs_t771291242_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.ObjectInteractEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m450213180_gshared (UnityAction_2_t1866747229 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.PlayerClimbEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m3501896311_gshared (UnityAction_2_t3633041732 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.PlayerClimbEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m2693216548_gshared (UnityAction_2_t3633041732 * __this, Il2CppObject * ___arg00, PlayerClimbEventArgs_t2537585745  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m2693216548((UnityAction_2_t3633041732 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, PlayerClimbEventArgs_t2537585745  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, PlayerClimbEventArgs_t2537585745  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, PlayerClimbEventArgs_t2537585745  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.PlayerClimbEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* PlayerClimbEventArgs_t2537585745_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m1725304169_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m1725304169_gshared (UnityAction_2_t3633041732 * __this, Il2CppObject * ___arg00, PlayerClimbEventArgs_t2537585745  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m1725304169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(PlayerClimbEventArgs_t2537585745_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.PlayerClimbEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m142133677_gshared (UnityAction_2_t3633041732 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.SnapDropZoneEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m2679616560_gshared (UnityAction_2_t1514158761 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.SnapDropZoneEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m19976831_gshared (UnityAction_2_t1514158761 * __this, Il2CppObject * ___arg00, SnapDropZoneEventArgs_t418702774  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m19976831((UnityAction_2_t1514158761 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, SnapDropZoneEventArgs_t418702774  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, SnapDropZoneEventArgs_t418702774  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, SnapDropZoneEventArgs_t418702774  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.SnapDropZoneEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* SnapDropZoneEventArgs_t418702774_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m1604287186_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m1604287186_gshared (UnityAction_2_t1514158761 * __this, Il2CppObject * ___arg00, SnapDropZoneEventArgs_t418702774  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m1604287186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(SnapDropZoneEventArgs_t418702774_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.SnapDropZoneEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m1679962530_gshared (UnityAction_2_t1514158761 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.UIPointerEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m1676252150_gshared (UnityAction_2_t2267441965 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.UIPointerEventArgs>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1587873651_gshared (UnityAction_2_t2267441965 * __this, Il2CppObject * ___arg00, UIPointerEventArgs_t1171985978  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m1587873651((UnityAction_2_t2267441965 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, UIPointerEventArgs_t1171985978  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, UIPointerEventArgs_t1171985978  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UIPointerEventArgs_t1171985978  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,VRTK.UIPointerEventArgs>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* UIPointerEventArgs_t1171985978_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m503888940_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m503888940_gshared (UnityAction_2_t2267441965 * __this, Il2CppObject * ___arg00, UIPointerEventArgs_t1171985978  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m503888940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = Box(UIPointerEventArgs_t1171985978_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,VRTK.UIPointerEventArgs>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m836406840_gshared (UnityAction_2_t2267441965 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m1977252345_gshared (UnityAction_2_t134459182 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Single,System.Single>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m3374088624_gshared (UnityAction_2_t134459182 * __this, float ___arg00, float ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m3374088624((UnityAction_2_t134459182 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___arg00, float ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___arg00, float ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Single,System.Single>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m4062895899_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m4062895899_gshared (UnityAction_2_t134459182 * __this, float ___arg00, float ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m4062895899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Single,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m1634636291_gshared (UnityAction_2_t134459182 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m2684626998_gshared (UnityAction_2_t1903595547 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1528820797_gshared (UnityAction_2_t1903595547 * __this, Scene_t1684909666  ___arg00, int32_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m1528820797((UnityAction_2_t1903595547 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t1684909666  ___arg00, int32_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Scene_t1684909666  ___arg00, int32_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* Scene_t1684909666_il2cpp_TypeInfo_var;
extern Il2CppClass* LoadSceneMode_t2981886439_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m2528278652_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m2528278652_gshared (UnityAction_2_t1903595547 * __this, Scene_t1684909666  ___arg00, int32_t ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m2528278652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Scene_t1684909666_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(LoadSceneMode_t2981886439_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m1593881300_gshared (UnityAction_2_t1903595547 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m2892452633_gshared (UnityAction_2_t606618774 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m670567184_gshared (UnityAction_2_t606618774 * __this, Scene_t1684909666  ___arg00, Scene_t1684909666  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m670567184((UnityAction_2_t606618774 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t1684909666  ___arg00, Scene_t1684909666  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Scene_t1684909666  ___arg00, Scene_t1684909666  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* Scene_t1684909666_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m2733450299_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m2733450299_gshared (UnityAction_2_t606618774 * __this, Scene_t1684909666  ___arg00, Scene_t1684909666  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m2733450299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Scene_t1684909666_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(Scene_t1684909666_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m234106915_gshared (UnityAction_2_t606618774 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_3__ctor_m3783439840_gshared (UnityAction_3_t3482433968 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::Invoke(T0,T1,T2)
extern "C"  void UnityAction_3_Invoke_m1498227613_gshared (UnityAction_3_t3482433968 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_3_Invoke_m1498227613((UnityAction_3_t3482433968 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_3_BeginInvoke_m160302482_gshared (UnityAction_3_t3482433968 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	__d_args[2] = ___arg22;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_3_EndInvoke_m1279075386_gshared (UnityAction_3_t3482433968 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`3<UnityEngine.Color,System.Single,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_3__ctor_m191704265_gshared (UnityAction_3_t1816056522 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`3<UnityEngine.Color,System.Single,System.Boolean>::Invoke(T0,T1,T2)
extern "C"  void UnityAction_3_Invoke_m786686763_gshared (UnityAction_3_t1816056522 * __this, Color_t2020392075  ___arg00, float ___arg11, bool ___arg22, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_3_Invoke_m786686763((UnityAction_3_t1816056522 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t2020392075  ___arg00, float ___arg11, bool ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t2020392075  ___arg00, float ___arg11, bool ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`3<UnityEngine.Color,System.Single,System.Boolean>::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_3_BeginInvoke_m3186351552_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_3_BeginInvoke_m3186351552_gshared (UnityAction_3_t1816056522 * __this, Color_t2020392075  ___arg00, float ___arg11, bool ___arg22, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_3_BeginInvoke_m3186351552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(Color_t2020392075_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___arg11);
	__d_args[2] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg22);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.Events.UnityAction`3<UnityEngine.Color,System.Single,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_3_EndInvoke_m2026965928_gshared (UnityAction_3_t1816056522 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_4__ctor_m2053485839_gshared (UnityAction_4_t1666603240 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T0,T1,T2,T3)
extern "C"  void UnityAction_4_Invoke_m3312096275_gshared (UnityAction_4_t1666603240 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_4_Invoke_m3312096275((UnityAction_4_t1666603240 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, ___arg33, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_4_BeginInvoke_m3427746322_gshared (UnityAction_4_t1666603240 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	__d_args[2] = ___arg22;
	__d_args[3] = ___arg33;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_4_EndInvoke_m3887055469_gshared (UnityAction_4_t1666603240 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m4051141261_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m4051141261_gshared (UnityEvent_1_t3863924733 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m4051141261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m1708363187_gshared (UnityEvent_1_t3863924733 * __this, UnityAction_1_t897193173 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t897193173 * L_0 = ___call0;
		BaseInvokableCall_t2229564840 * L_1 = ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t897193173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t897193173 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_AddCall_m1842680419((UnityEventBase_t828812576 *)__this, (BaseInvokableCall_t2229564840 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m670609979_gshared (UnityEvent_1_t3863924733 * __this, UnityAction_1_t897193173 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t897193173 * L_0 = ___call0;
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t897193173 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_RemoveListener_m2645959491((UnityEventBase_t828812576 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Boolean>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m3743240374_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m3743240374_gshared (UnityEvent_1_t3863924733 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m3743240374_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Boolean>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m2856963016_gshared (UnityEvent_1_t3863924733 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t2019901575 * L_2 = (InvokableCall_1_t2019901575 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2019901575 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Boolean>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m1528404507_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t897193173 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		UnityAction_1_t897193173 * L_0 = ___action0;
		InvokableCall_1_t2019901575 * L_1 = (InvokableCall_1_t2019901575 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2019901575 *, UnityAction_1_t897193173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t897193173 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2229564840 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2229564840 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m667974834_gshared (UnityEvent_1_t3863924733 * __this, bool ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		bool L_1 = ___arg00;
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_Invoke_m2706435282((UnityEventBase_t828812576 *)__this, (ObjectU5BU5D_t3614634134*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m3244234683_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m3244234683_gshared (UnityEvent_1_t2110227463 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m3244234683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m846589010_gshared (UnityEvent_1_t2110227463 * __this, UnityAction_1_t3438463199 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3438463199 * L_0 = ___call0;
		BaseInvokableCall_t2229564840 * L_1 = ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3438463199 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t3438463199 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_AddCall_m1842680419((UnityEventBase_t828812576 *)__this, (BaseInvokableCall_t2229564840 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m2851793905_gshared (UnityEvent_1_t2110227463 * __this, UnityAction_1_t3438463199 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3438463199 * L_0 = ___call0;
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t3438463199 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_RemoveListener_m2645959491((UnityEventBase_t828812576 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Int32>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m4083384818_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m4083384818_gshared (UnityEvent_1_t2110227463 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m4083384818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Int32>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m3311025800_gshared (UnityEvent_1_t2110227463 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t266204305 * L_2 = (InvokableCall_1_t266204305 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t266204305 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Int32>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m3475403017_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t3438463199 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		UnityAction_1_t3438463199 * L_0 = ___action0;
		InvokableCall_1_t266204305 * L_1 = (InvokableCall_1_t266204305 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t266204305 *, UnityAction_1_t3438463199 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t3438463199 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2229564840 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2229564840 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m1805498302_gshared (UnityEvent_1_t2110227463 * __this, int32_t ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		int32_t L_1 = ___arg00;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_Invoke_m2706435282((UnityEventBase_t828812576 *)__this, (ObjectU5BU5D_t3614634134*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m2073978020_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m2073978020_gshared (UnityEvent_1_t2727799310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m2073978020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m22503421_gshared (UnityEvent_1_t2727799310 * __this, UnityAction_1_t4056035046 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t4056035046 * L_0 = ___call0;
		BaseInvokableCall_t2229564840 * L_1 = ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t4056035046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t4056035046 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_AddCall_m1842680419((UnityEventBase_t828812576 *)__this, (BaseInvokableCall_t2229564840 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m4278264272_gshared (UnityEvent_1_t2727799310 * __this, UnityAction_1_t4056035046 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t4056035046 * L_0 = ___call0;
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t4056035046 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_RemoveListener_m2645959491((UnityEventBase_t828812576 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Object>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m2223850067_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m2223850067_gshared (UnityEvent_1_t2727799310 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m2223850067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m669290055_gshared (UnityEvent_1_t2727799310 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t883776152 * L_2 = (InvokableCall_1_t883776152 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t883776152 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Object>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m3098147632_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t4056035046 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		UnityAction_1_t4056035046 * L_0 = ___action0;
		InvokableCall_1_t883776152 * L_1 = (InvokableCall_1_t883776152 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t883776152 *, UnityAction_1_t4056035046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t4056035046 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2229564840 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2229564840 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m838874366_gshared (UnityEvent_1_t2727799310 * __this, Il2CppObject * ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		Il2CppObject * L_1 = ___arg00;
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_Invoke_m2706435282((UnityEventBase_t828812576 *)__this, (ObjectU5BU5D_t3614634134*)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m29611311_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m29611311_gshared (UnityEvent_1_t2114859947 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m29611311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m2377847221_gshared (UnityEvent_1_t2114859947 * __this, UnityAction_1_t3443095683 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3443095683 * L_0 = ___call0;
		BaseInvokableCall_t2229564840 * L_1 = ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3443095683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t3443095683 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_AddCall_m1842680419((UnityEventBase_t828812576 *)__this, (BaseInvokableCall_t2229564840 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m2564825698_gshared (UnityEvent_1_t2114859947 * __this, UnityAction_1_t3443095683 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3443095683 * L_0 = ___call0;
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t3443095683 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_RemoveListener_m2645959491((UnityEventBase_t828812576 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Single>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m3813546_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m3813546_gshared (UnityEvent_1_t2114859947 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m3813546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Single>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m2566156550_gshared (UnityEvent_1_t2114859947 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t270836789 * L_2 = (InvokableCall_1_t270836789 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t270836789 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Single>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m4062537313_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t3443095683 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		UnityAction_1_t3443095683 * L_0 = ___action0;
		InvokableCall_1_t270836789 * L_1 = (InvokableCall_1_t270836789 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t270836789 *, UnityAction_1_t3443095683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t3443095683 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2229564840 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2229564840 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m1298892870_gshared (UnityEvent_1_t2114859947 * __this, float ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		float L_1 = ___arg00;
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_Invoke_m2706435282((UnityEventBase_t828812576 *)__this, (ObjectU5BU5D_t3614634134*)L_4, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
