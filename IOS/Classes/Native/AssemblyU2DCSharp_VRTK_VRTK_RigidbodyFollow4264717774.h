﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;

#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectFollow3175963762.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_RigidbodyFollow_Moveme4148486814.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_RigidbodyFollow
struct  VRTK_RigidbodyFollow_t4264717774  : public VRTK_ObjectFollow_t3175963762
{
public:
	// VRTK.VRTK_RigidbodyFollow/MovementOption VRTK.VRTK_RigidbodyFollow::movementOption
	int32_t ___movementOption_16;
	// UnityEngine.Rigidbody VRTK.VRTK_RigidbodyFollow::rigidbodyToFollow
	Rigidbody_t4233889191 * ___rigidbodyToFollow_17;
	// UnityEngine.Rigidbody VRTK.VRTK_RigidbodyFollow::rigidbodyToChange
	Rigidbody_t4233889191 * ___rigidbodyToChange_18;

public:
	inline static int32_t get_offset_of_movementOption_16() { return static_cast<int32_t>(offsetof(VRTK_RigidbodyFollow_t4264717774, ___movementOption_16)); }
	inline int32_t get_movementOption_16() const { return ___movementOption_16; }
	inline int32_t* get_address_of_movementOption_16() { return &___movementOption_16; }
	inline void set_movementOption_16(int32_t value)
	{
		___movementOption_16 = value;
	}

	inline static int32_t get_offset_of_rigidbodyToFollow_17() { return static_cast<int32_t>(offsetof(VRTK_RigidbodyFollow_t4264717774, ___rigidbodyToFollow_17)); }
	inline Rigidbody_t4233889191 * get_rigidbodyToFollow_17() const { return ___rigidbodyToFollow_17; }
	inline Rigidbody_t4233889191 ** get_address_of_rigidbodyToFollow_17() { return &___rigidbodyToFollow_17; }
	inline void set_rigidbodyToFollow_17(Rigidbody_t4233889191 * value)
	{
		___rigidbodyToFollow_17 = value;
		Il2CppCodeGenWriteBarrier(&___rigidbodyToFollow_17, value);
	}

	inline static int32_t get_offset_of_rigidbodyToChange_18() { return static_cast<int32_t>(offsetof(VRTK_RigidbodyFollow_t4264717774, ___rigidbodyToChange_18)); }
	inline Rigidbody_t4233889191 * get_rigidbodyToChange_18() const { return ___rigidbodyToChange_18; }
	inline Rigidbody_t4233889191 ** get_address_of_rigidbodyToChange_18() { return &___rigidbodyToChange_18; }
	inline void set_rigidbodyToChange_18(Rigidbody_t4233889191 * value)
	{
		___rigidbodyToChange_18 = value;
		Il2CppCodeGenWriteBarrier(&___rigidbodyToChange_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
