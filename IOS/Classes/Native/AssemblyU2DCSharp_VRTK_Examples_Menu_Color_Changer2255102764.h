﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Menu_Color_Changer
struct  Menu_Color_Changer_t2255102764  : public VRTK_InteractableObject_t2604188111
{
public:
	// UnityEngine.Color VRTK.Examples.Menu_Color_Changer::newMenuColor
	Color_t2020392075  ___newMenuColor_45;

public:
	inline static int32_t get_offset_of_newMenuColor_45() { return static_cast<int32_t>(offsetof(Menu_Color_Changer_t2255102764, ___newMenuColor_45)); }
	inline Color_t2020392075  get_newMenuColor_45() const { return ___newMenuColor_45; }
	inline Color_t2020392075 * get_address_of_newMenuColor_45() { return &___newMenuColor_45; }
	inline void set_newMenuColor_45(Color_t2020392075  value)
	{
		___newMenuColor_45 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
