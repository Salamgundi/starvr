﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_VRInputModule
struct VRTK_VRInputModule_t1472500726;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// VRTK.VRTK_UIPointer
struct VRTK_UIPointer_t2714926455;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer2714926455.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.VRTK_VRInputModule::.ctor()
extern "C"  void VRTK_VRInputModule__ctor_m2020183358 (VRTK_VRInputModule_t1472500726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_VRInputModule::Initialise()
extern "C"  void VRTK_VRInputModule_Initialise_m2688476575 (VRTK_VRInputModule_t1472500726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_VRInputModule::IsModuleSupported()
extern "C"  bool VRTK_VRInputModule_IsModuleSupported_m851130246 (VRTK_VRInputModule_t1472500726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_VRInputModule::Process()
extern "C"  void VRTK_VRInputModule_Process_m2081487893 (VRTK_VRInputModule_t1472500726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> VRTK.VRTK_VRInputModule::CheckRaycasts(VRTK.VRTK_UIPointer)
extern "C"  List_1_t3685274804 * VRTK_VRInputModule_CheckRaycasts_m926584408 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_VRInputModule::CheckTransformTree(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  bool VRTK_VRInputModule_CheckTransformTree_m2627734320 (VRTK_VRInputModule_t1472500726 * __this, Transform_t3275118058 * ___target0, Transform_t3275118058 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_VRInputModule::NoValidCollision(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  bool VRTK_VRInputModule_NoValidCollision_m3464415866 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_VRInputModule::IsHovering(VRTK.VRTK_UIPointer)
extern "C"  bool VRTK_VRInputModule_IsHovering_m2892594038 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_VRInputModule::ValidElement(UnityEngine.GameObject)
extern "C"  bool VRTK_VRInputModule_ValidElement_m1326706494 (VRTK_VRInputModule_t1472500726 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_VRInputModule::CheckPointerHoverClick(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  void VRTK_VRInputModule_CheckPointerHoverClick_m1182551694 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_VRInputModule::Hover(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  void VRTK_VRInputModule_Hover_m1423269845 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_VRInputModule::Click(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  void VRTK_VRInputModule_Click_m1795054129 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_VRInputModule::ClickOnUp(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  void VRTK_VRInputModule_ClickOnUp_m2325928753 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_VRInputModule::ClickOnDown(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>,System.Boolean)
extern "C"  void VRTK_VRInputModule_ClickOnDown_m903340057 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, bool ___forceClick2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_VRInputModule::IsEligibleClick(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  bool VRTK_VRInputModule_IsEligibleClick_m2949652322 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_VRInputModule::AttemptClick(VRTK.VRTK_UIPointer)
extern "C"  bool VRTK_VRInputModule_AttemptClick_m2631317559 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_VRInputModule::Drag(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  void VRTK_VRInputModule_Drag_m3103883503 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_VRInputModule::Scroll(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  void VRTK_VRInputModule_Scroll_m982370564 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
