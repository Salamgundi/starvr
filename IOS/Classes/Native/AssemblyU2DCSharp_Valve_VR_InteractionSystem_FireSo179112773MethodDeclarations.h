﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.FireSource
struct FireSource_t179112773;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void Valve.VR.InteractionSystem.FireSource::.ctor()
extern "C"  void FireSource__ctor_m1910000195 (FireSource_t179112773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.FireSource::Start()
extern "C"  void FireSource_Start_m1522264863 (FireSource_t179112773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.FireSource::Update()
extern "C"  void FireSource_Update_m2976909544 (FireSource_t179112773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.FireSource::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void FireSource_OnTriggerEnter_m1536921127 (FireSource_t179112773 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.FireSource::FireExposure()
extern "C"  void FireSource_FireExposure_m1325708240 (FireSource_t179112773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.FireSource::StartBurning()
extern "C"  void FireSource_StartBurning_m4200100418 (FireSource_t179112773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
