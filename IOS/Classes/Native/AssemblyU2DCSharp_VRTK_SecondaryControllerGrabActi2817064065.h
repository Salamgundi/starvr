﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_SecondaryControllerGrabActi4095736311.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction
struct  VRTK_AxisScaleGrabAction_t2817064065  : public VRTK_BaseGrabAction_t4095736311
{
public:
	// System.Single VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::ungrabDistance
	float ___ungrabDistance_10;
	// System.Boolean VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::lockXAxis
	bool ___lockXAxis_11;
	// System.Boolean VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::lockYAxis
	bool ___lockYAxis_12;
	// System.Boolean VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::lockZAxis
	bool ___lockZAxis_13;
	// System.Boolean VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::uniformScaling
	bool ___uniformScaling_14;
	// UnityEngine.Vector3 VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::initialScale
	Vector3_t2243707580  ___initialScale_15;
	// System.Single VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::initalLength
	float ___initalLength_16;
	// System.Single VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::initialScaleFactor
	float ___initialScaleFactor_17;

public:
	inline static int32_t get_offset_of_ungrabDistance_10() { return static_cast<int32_t>(offsetof(VRTK_AxisScaleGrabAction_t2817064065, ___ungrabDistance_10)); }
	inline float get_ungrabDistance_10() const { return ___ungrabDistance_10; }
	inline float* get_address_of_ungrabDistance_10() { return &___ungrabDistance_10; }
	inline void set_ungrabDistance_10(float value)
	{
		___ungrabDistance_10 = value;
	}

	inline static int32_t get_offset_of_lockXAxis_11() { return static_cast<int32_t>(offsetof(VRTK_AxisScaleGrabAction_t2817064065, ___lockXAxis_11)); }
	inline bool get_lockXAxis_11() const { return ___lockXAxis_11; }
	inline bool* get_address_of_lockXAxis_11() { return &___lockXAxis_11; }
	inline void set_lockXAxis_11(bool value)
	{
		___lockXAxis_11 = value;
	}

	inline static int32_t get_offset_of_lockYAxis_12() { return static_cast<int32_t>(offsetof(VRTK_AxisScaleGrabAction_t2817064065, ___lockYAxis_12)); }
	inline bool get_lockYAxis_12() const { return ___lockYAxis_12; }
	inline bool* get_address_of_lockYAxis_12() { return &___lockYAxis_12; }
	inline void set_lockYAxis_12(bool value)
	{
		___lockYAxis_12 = value;
	}

	inline static int32_t get_offset_of_lockZAxis_13() { return static_cast<int32_t>(offsetof(VRTK_AxisScaleGrabAction_t2817064065, ___lockZAxis_13)); }
	inline bool get_lockZAxis_13() const { return ___lockZAxis_13; }
	inline bool* get_address_of_lockZAxis_13() { return &___lockZAxis_13; }
	inline void set_lockZAxis_13(bool value)
	{
		___lockZAxis_13 = value;
	}

	inline static int32_t get_offset_of_uniformScaling_14() { return static_cast<int32_t>(offsetof(VRTK_AxisScaleGrabAction_t2817064065, ___uniformScaling_14)); }
	inline bool get_uniformScaling_14() const { return ___uniformScaling_14; }
	inline bool* get_address_of_uniformScaling_14() { return &___uniformScaling_14; }
	inline void set_uniformScaling_14(bool value)
	{
		___uniformScaling_14 = value;
	}

	inline static int32_t get_offset_of_initialScale_15() { return static_cast<int32_t>(offsetof(VRTK_AxisScaleGrabAction_t2817064065, ___initialScale_15)); }
	inline Vector3_t2243707580  get_initialScale_15() const { return ___initialScale_15; }
	inline Vector3_t2243707580 * get_address_of_initialScale_15() { return &___initialScale_15; }
	inline void set_initialScale_15(Vector3_t2243707580  value)
	{
		___initialScale_15 = value;
	}

	inline static int32_t get_offset_of_initalLength_16() { return static_cast<int32_t>(offsetof(VRTK_AxisScaleGrabAction_t2817064065, ___initalLength_16)); }
	inline float get_initalLength_16() const { return ___initalLength_16; }
	inline float* get_address_of_initalLength_16() { return &___initalLength_16; }
	inline void set_initalLength_16(float value)
	{
		___initalLength_16 = value;
	}

	inline static int32_t get_offset_of_initialScaleFactor_17() { return static_cast<int32_t>(offsetof(VRTK_AxisScaleGrabAction_t2817064065, ___initialScaleFactor_17)); }
	inline float get_initialScaleFactor_17() const { return ___initialScaleFactor_17; }
	inline float* get_address_of_initialScaleFactor_17() { return &___initialScaleFactor_17; }
	inline void set_initialScaleFactor_17(float value)
	{
		___initialScaleFactor_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
