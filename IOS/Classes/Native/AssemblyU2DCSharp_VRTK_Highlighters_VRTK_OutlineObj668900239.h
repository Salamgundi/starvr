﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.Material
struct Material_t193706927;

#include "AssemblyU2DCSharp_VRTK_Highlighters_VRTK_BaseHighl3110203740.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter
struct  VRTK_OutlineObjectCopyHighlighter_t668900239  : public VRTK_BaseHighlighter_t3110203740
{
public:
	// System.Single VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::thickness
	float ___thickness_5;
	// UnityEngine.GameObject[] VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::customOutlineModels
	GameObjectU5BU5D_t3057952154* ___customOutlineModels_6;
	// System.String[] VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::customOutlineModelPaths
	StringU5BU5D_t1642385972* ___customOutlineModelPaths_7;
	// System.Boolean VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::enableSubmeshHighlight
	bool ___enableSubmeshHighlight_8;
	// UnityEngine.Material VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::stencilOutline
	Material_t193706927 * ___stencilOutline_9;
	// UnityEngine.GameObject[] VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::highlightModels
	GameObjectU5BU5D_t3057952154* ___highlightModels_10;
	// System.String[] VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::copyComponents
	StringU5BU5D_t1642385972* ___copyComponents_11;

public:
	inline static int32_t get_offset_of_thickness_5() { return static_cast<int32_t>(offsetof(VRTK_OutlineObjectCopyHighlighter_t668900239, ___thickness_5)); }
	inline float get_thickness_5() const { return ___thickness_5; }
	inline float* get_address_of_thickness_5() { return &___thickness_5; }
	inline void set_thickness_5(float value)
	{
		___thickness_5 = value;
	}

	inline static int32_t get_offset_of_customOutlineModels_6() { return static_cast<int32_t>(offsetof(VRTK_OutlineObjectCopyHighlighter_t668900239, ___customOutlineModels_6)); }
	inline GameObjectU5BU5D_t3057952154* get_customOutlineModels_6() const { return ___customOutlineModels_6; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_customOutlineModels_6() { return &___customOutlineModels_6; }
	inline void set_customOutlineModels_6(GameObjectU5BU5D_t3057952154* value)
	{
		___customOutlineModels_6 = value;
		Il2CppCodeGenWriteBarrier(&___customOutlineModels_6, value);
	}

	inline static int32_t get_offset_of_customOutlineModelPaths_7() { return static_cast<int32_t>(offsetof(VRTK_OutlineObjectCopyHighlighter_t668900239, ___customOutlineModelPaths_7)); }
	inline StringU5BU5D_t1642385972* get_customOutlineModelPaths_7() const { return ___customOutlineModelPaths_7; }
	inline StringU5BU5D_t1642385972** get_address_of_customOutlineModelPaths_7() { return &___customOutlineModelPaths_7; }
	inline void set_customOutlineModelPaths_7(StringU5BU5D_t1642385972* value)
	{
		___customOutlineModelPaths_7 = value;
		Il2CppCodeGenWriteBarrier(&___customOutlineModelPaths_7, value);
	}

	inline static int32_t get_offset_of_enableSubmeshHighlight_8() { return static_cast<int32_t>(offsetof(VRTK_OutlineObjectCopyHighlighter_t668900239, ___enableSubmeshHighlight_8)); }
	inline bool get_enableSubmeshHighlight_8() const { return ___enableSubmeshHighlight_8; }
	inline bool* get_address_of_enableSubmeshHighlight_8() { return &___enableSubmeshHighlight_8; }
	inline void set_enableSubmeshHighlight_8(bool value)
	{
		___enableSubmeshHighlight_8 = value;
	}

	inline static int32_t get_offset_of_stencilOutline_9() { return static_cast<int32_t>(offsetof(VRTK_OutlineObjectCopyHighlighter_t668900239, ___stencilOutline_9)); }
	inline Material_t193706927 * get_stencilOutline_9() const { return ___stencilOutline_9; }
	inline Material_t193706927 ** get_address_of_stencilOutline_9() { return &___stencilOutline_9; }
	inline void set_stencilOutline_9(Material_t193706927 * value)
	{
		___stencilOutline_9 = value;
		Il2CppCodeGenWriteBarrier(&___stencilOutline_9, value);
	}

	inline static int32_t get_offset_of_highlightModels_10() { return static_cast<int32_t>(offsetof(VRTK_OutlineObjectCopyHighlighter_t668900239, ___highlightModels_10)); }
	inline GameObjectU5BU5D_t3057952154* get_highlightModels_10() const { return ___highlightModels_10; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_highlightModels_10() { return &___highlightModels_10; }
	inline void set_highlightModels_10(GameObjectU5BU5D_t3057952154* value)
	{
		___highlightModels_10 = value;
		Il2CppCodeGenWriteBarrier(&___highlightModels_10, value);
	}

	inline static int32_t get_offset_of_copyComponents_11() { return static_cast<int32_t>(offsetof(VRTK_OutlineObjectCopyHighlighter_t668900239, ___copyComponents_11)); }
	inline StringU5BU5D_t1642385972* get_copyComponents_11() const { return ___copyComponents_11; }
	inline StringU5BU5D_t1642385972** get_address_of_copyComponents_11() { return &___copyComponents_11; }
	inline void set_copyComponents_11(StringU5BU5D_t1642385972* value)
	{
		___copyComponents_11 = value;
		Il2CppCodeGenWriteBarrier(&___copyComponents_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
