﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_InteractGrab
struct VRTK_InteractGrab_t124353446;
// VRTK.VRTK_InteractTouch
struct VRTK_InteractTouch_t4022091061;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// VRTK.VRTK_ObjectAutoGrab
struct VRTK_ObjectAutoGrab_t2748125822;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0
struct  U3CAutoGrabU3Ec__Iterator0_t1000106702  : public Il2CppObject
{
public:
	// VRTK.VRTK_InteractGrab VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0::<controllerGrab>__0
	VRTK_InteractGrab_t124353446 * ___U3CcontrollerGrabU3E__0_0;
	// VRTK.VRTK_InteractTouch VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0::<controllerTouch>__1
	VRTK_InteractTouch_t4022091061 * ___U3CcontrollerTouchU3E__1_1;
	// System.Boolean VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0::<grabbableObjectDisableState>__2
	bool ___U3CgrabbableObjectDisableStateU3E__2_2;
	// VRTK.VRTK_InteractableObject VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0::<grabbableObject>__3
	VRTK_InteractableObject_t2604188111 * ___U3CgrabbableObjectU3E__3_3;
	// VRTK.VRTK_ObjectAutoGrab VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0::$this
	VRTK_ObjectAutoGrab_t2748125822 * ___U24this_4;
	// System.Object VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CcontrollerGrabU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAutoGrabU3Ec__Iterator0_t1000106702, ___U3CcontrollerGrabU3E__0_0)); }
	inline VRTK_InteractGrab_t124353446 * get_U3CcontrollerGrabU3E__0_0() const { return ___U3CcontrollerGrabU3E__0_0; }
	inline VRTK_InteractGrab_t124353446 ** get_address_of_U3CcontrollerGrabU3E__0_0() { return &___U3CcontrollerGrabU3E__0_0; }
	inline void set_U3CcontrollerGrabU3E__0_0(VRTK_InteractGrab_t124353446 * value)
	{
		___U3CcontrollerGrabU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcontrollerGrabU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CcontrollerTouchU3E__1_1() { return static_cast<int32_t>(offsetof(U3CAutoGrabU3Ec__Iterator0_t1000106702, ___U3CcontrollerTouchU3E__1_1)); }
	inline VRTK_InteractTouch_t4022091061 * get_U3CcontrollerTouchU3E__1_1() const { return ___U3CcontrollerTouchU3E__1_1; }
	inline VRTK_InteractTouch_t4022091061 ** get_address_of_U3CcontrollerTouchU3E__1_1() { return &___U3CcontrollerTouchU3E__1_1; }
	inline void set_U3CcontrollerTouchU3E__1_1(VRTK_InteractTouch_t4022091061 * value)
	{
		___U3CcontrollerTouchU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcontrollerTouchU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CgrabbableObjectDisableStateU3E__2_2() { return static_cast<int32_t>(offsetof(U3CAutoGrabU3Ec__Iterator0_t1000106702, ___U3CgrabbableObjectDisableStateU3E__2_2)); }
	inline bool get_U3CgrabbableObjectDisableStateU3E__2_2() const { return ___U3CgrabbableObjectDisableStateU3E__2_2; }
	inline bool* get_address_of_U3CgrabbableObjectDisableStateU3E__2_2() { return &___U3CgrabbableObjectDisableStateU3E__2_2; }
	inline void set_U3CgrabbableObjectDisableStateU3E__2_2(bool value)
	{
		___U3CgrabbableObjectDisableStateU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CgrabbableObjectU3E__3_3() { return static_cast<int32_t>(offsetof(U3CAutoGrabU3Ec__Iterator0_t1000106702, ___U3CgrabbableObjectU3E__3_3)); }
	inline VRTK_InteractableObject_t2604188111 * get_U3CgrabbableObjectU3E__3_3() const { return ___U3CgrabbableObjectU3E__3_3; }
	inline VRTK_InteractableObject_t2604188111 ** get_address_of_U3CgrabbableObjectU3E__3_3() { return &___U3CgrabbableObjectU3E__3_3; }
	inline void set_U3CgrabbableObjectU3E__3_3(VRTK_InteractableObject_t2604188111 * value)
	{
		___U3CgrabbableObjectU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CgrabbableObjectU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CAutoGrabU3Ec__Iterator0_t1000106702, ___U24this_4)); }
	inline VRTK_ObjectAutoGrab_t2748125822 * get_U24this_4() const { return ___U24this_4; }
	inline VRTK_ObjectAutoGrab_t2748125822 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(VRTK_ObjectAutoGrab_t2748125822 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CAutoGrabU3Ec__Iterator0_t1000106702, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CAutoGrabU3Ec__Iterator0_t1000106702, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CAutoGrabU3Ec__Iterator0_t1000106702, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
