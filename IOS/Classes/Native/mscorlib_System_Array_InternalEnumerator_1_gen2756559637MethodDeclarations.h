﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2756559637.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Valve_VR_VRTextureBounds_t1897807375.h"

// System.Void System.Array/InternalEnumerator`1<Valve.VR.VRTextureBounds_t>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1000591368_gshared (InternalEnumerator_1_t2756559637 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1000591368(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2756559637 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1000591368_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.VRTextureBounds_t>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4261737072_gshared (InternalEnumerator_1_t2756559637 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4261737072(__this, method) ((  void (*) (InternalEnumerator_1_t2756559637 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4261737072_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Valve.VR.VRTextureBounds_t>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4006974588_gshared (InternalEnumerator_1_t2756559637 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4006974588(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2756559637 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4006974588_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.VRTextureBounds_t>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m533051135_gshared (InternalEnumerator_1_t2756559637 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m533051135(__this, method) ((  void (*) (InternalEnumerator_1_t2756559637 *, const MethodInfo*))InternalEnumerator_1_Dispose_m533051135_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Valve.VR.VRTextureBounds_t>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1991967392_gshared (InternalEnumerator_1_t2756559637 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1991967392(__this, method) ((  bool (*) (InternalEnumerator_1_t2756559637 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1991967392_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Valve.VR.VRTextureBounds_t>::get_Current()
extern "C"  VRTextureBounds_t_t1897807375  InternalEnumerator_1_get_Current_m4139640631_gshared (InternalEnumerator_1_t2756559637 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4139640631(__this, method) ((  VRTextureBounds_t_t1897807375  (*) (InternalEnumerator_1_t2756559637 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4139640631_gshared)(__this, method)
