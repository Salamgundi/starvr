﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.VREvent_EditingCameraSurface_t
struct  VREvent_EditingCameraSurface_t_t4154643070 
{
public:
	// System.UInt64 Valve.VR.VREvent_EditingCameraSurface_t::overlayHandle
	uint64_t ___overlayHandle_0;
	// System.UInt32 Valve.VR.VREvent_EditingCameraSurface_t::nVisualMode
	uint32_t ___nVisualMode_1;

public:
	inline static int32_t get_offset_of_overlayHandle_0() { return static_cast<int32_t>(offsetof(VREvent_EditingCameraSurface_t_t4154643070, ___overlayHandle_0)); }
	inline uint64_t get_overlayHandle_0() const { return ___overlayHandle_0; }
	inline uint64_t* get_address_of_overlayHandle_0() { return &___overlayHandle_0; }
	inline void set_overlayHandle_0(uint64_t value)
	{
		___overlayHandle_0 = value;
	}

	inline static int32_t get_offset_of_nVisualMode_1() { return static_cast<int32_t>(offsetof(VREvent_EditingCameraSurface_t_t4154643070, ___nVisualMode_1)); }
	inline uint32_t get_nVisualMode_1() const { return ___nVisualMode_1; }
	inline uint32_t* get_address_of_nVisualMode_1() { return &___nVisualMode_1; }
	inline void set_nVisualMode_1(uint32_t value)
	{
		___nVisualMode_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
