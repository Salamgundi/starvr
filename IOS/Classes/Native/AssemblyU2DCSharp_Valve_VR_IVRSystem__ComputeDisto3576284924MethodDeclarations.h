﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_ComputeDistortion
struct _ComputeDistortion_t3576284924;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"
#include "AssemblyU2DCSharp_Valve_VR_DistortionCoordinates_t2253454723.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_ComputeDistortion::.ctor(System.Object,System.IntPtr)
extern "C"  void _ComputeDistortion__ctor_m2689096133 (_ComputeDistortion_t3576284924 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_ComputeDistortion::Invoke(Valve.VR.EVREye,System.Single,System.Single,Valve.VR.DistortionCoordinates_t&)
extern "C"  bool _ComputeDistortion_Invoke_m1412017482 (_ComputeDistortion_t3576284924 * __this, int32_t ___eEye0, float ___fU1, float ___fV2, DistortionCoordinates_t_t2253454723 * ___pDistortionCoordinates3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_ComputeDistortion::BeginInvoke(Valve.VR.EVREye,System.Single,System.Single,Valve.VR.DistortionCoordinates_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ComputeDistortion_BeginInvoke_m22195989 (_ComputeDistortion_t3576284924 * __this, int32_t ___eEye0, float ___fU1, float ___fV2, DistortionCoordinates_t_t2253454723 * ___pDistortionCoordinates3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_ComputeDistortion::EndInvoke(Valve.VR.DistortionCoordinates_t&,System.IAsyncResult)
extern "C"  bool _ComputeDistortion_EndInvoke_m3411213496 (_ComputeDistortion_t3576284924 * __this, DistortionCoordinates_t_t2253454723 * ___pDistortionCoordinates0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
