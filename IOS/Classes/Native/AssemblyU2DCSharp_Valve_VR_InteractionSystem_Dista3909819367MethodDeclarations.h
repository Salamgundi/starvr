﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.DistanceHaptics
struct DistanceHaptics_t3909819367;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.DistanceHaptics::.ctor()
extern "C"  void DistanceHaptics__ctor_m2663599577 (DistanceHaptics_t3909819367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.DistanceHaptics::Start()
extern "C"  Il2CppObject * DistanceHaptics_Start_m3696372451 (DistanceHaptics_t3909819367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
