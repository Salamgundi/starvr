﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_InteractHaptics
struct VRTK_InteractHaptics_t1219060492;
// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerActions3642353851.h"

// System.Void VRTK.VRTK_InteractHaptics::.ctor()
extern "C"  void VRTK_InteractHaptics__ctor_m3639163432 (VRTK_InteractHaptics_t1219060492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractHaptics::HapticsOnTouch(VRTK.VRTK_ControllerActions)
extern "C"  void VRTK_InteractHaptics_HapticsOnTouch_m1412951248 (VRTK_InteractHaptics_t1219060492 * __this, VRTK_ControllerActions_t3642353851 * ___controllerActions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractHaptics::HapticsOnGrab(VRTK.VRTK_ControllerActions)
extern "C"  void VRTK_InteractHaptics_HapticsOnGrab_m1985782947 (VRTK_InteractHaptics_t1219060492 * __this, VRTK_ControllerActions_t3642353851 * ___controllerActions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractHaptics::HapticsOnUse(VRTK.VRTK_ControllerActions)
extern "C"  void VRTK_InteractHaptics_HapticsOnUse_m2361000840 (VRTK_InteractHaptics_t1219060492 * __this, VRTK_ControllerActions_t3642353851 * ___controllerActions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractHaptics::OnEnable()
extern "C"  void VRTK_InteractHaptics_OnEnable_m1511362928 (VRTK_InteractHaptics_t1219060492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractHaptics::TriggerHapticPulse(VRTK.VRTK_ControllerActions,System.Single,System.Single,System.Single)
extern "C"  void VRTK_InteractHaptics_TriggerHapticPulse_m2630918585 (VRTK_InteractHaptics_t1219060492 * __this, VRTK_ControllerActions_t3642353851 * ___controllerActions0, float ___strength1, float ___duration2, float ___interval3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
