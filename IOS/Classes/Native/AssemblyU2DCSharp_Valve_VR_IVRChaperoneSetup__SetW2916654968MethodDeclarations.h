﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_SetWorkingCollisionBoundsTagsInfo
struct _SetWorkingCollisionBoundsTagsInfo_t2916654968;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingCollisionBoundsTagsInfo::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetWorkingCollisionBoundsTagsInfo__ctor_m2545411703 (_SetWorkingCollisionBoundsTagsInfo_t2916654968 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingCollisionBoundsTagsInfo::Invoke(System.Byte[],System.UInt32)
extern "C"  void _SetWorkingCollisionBoundsTagsInfo_Invoke_m1954917698 (_SetWorkingCollisionBoundsTagsInfo_t2916654968 * __this, ByteU5BU5D_t3397334013* ___pTagsBuffer0, uint32_t ___unTagCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_SetWorkingCollisionBoundsTagsInfo::BeginInvoke(System.Byte[],System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetWorkingCollisionBoundsTagsInfo_BeginInvoke_m1577813459 (_SetWorkingCollisionBoundsTagsInfo_t2916654968 * __this, ByteU5BU5D_t3397334013* ___pTagsBuffer0, uint32_t ___unTagCount1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingCollisionBoundsTagsInfo::EndInvoke(System.IAsyncResult)
extern "C"  void _SetWorkingCollisionBoundsTagsInfo_EndInvoke_m2684402825 (_SetWorkingCollisionBoundsTagsInfo_t2916654968 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
