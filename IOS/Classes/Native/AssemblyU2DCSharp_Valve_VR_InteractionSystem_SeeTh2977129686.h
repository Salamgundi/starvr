﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Valve.VR.InteractionSystem.Interactable
struct Interactable_t1274046986;
// UnityEngine.Renderer
struct Renderer_t257310565;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.SeeThru
struct  SeeThru_t2977129686  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material Valve.VR.InteractionSystem.SeeThru::seeThruMaterial
	Material_t193706927 * ___seeThruMaterial_2;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.SeeThru::seeThru
	GameObject_t1756533147 * ___seeThru_3;
	// Valve.VR.InteractionSystem.Interactable Valve.VR.InteractionSystem.SeeThru::interactable
	Interactable_t1274046986 * ___interactable_4;
	// UnityEngine.Renderer Valve.VR.InteractionSystem.SeeThru::sourceRenderer
	Renderer_t257310565 * ___sourceRenderer_5;
	// UnityEngine.Renderer Valve.VR.InteractionSystem.SeeThru::destRenderer
	Renderer_t257310565 * ___destRenderer_6;

public:
	inline static int32_t get_offset_of_seeThruMaterial_2() { return static_cast<int32_t>(offsetof(SeeThru_t2977129686, ___seeThruMaterial_2)); }
	inline Material_t193706927 * get_seeThruMaterial_2() const { return ___seeThruMaterial_2; }
	inline Material_t193706927 ** get_address_of_seeThruMaterial_2() { return &___seeThruMaterial_2; }
	inline void set_seeThruMaterial_2(Material_t193706927 * value)
	{
		___seeThruMaterial_2 = value;
		Il2CppCodeGenWriteBarrier(&___seeThruMaterial_2, value);
	}

	inline static int32_t get_offset_of_seeThru_3() { return static_cast<int32_t>(offsetof(SeeThru_t2977129686, ___seeThru_3)); }
	inline GameObject_t1756533147 * get_seeThru_3() const { return ___seeThru_3; }
	inline GameObject_t1756533147 ** get_address_of_seeThru_3() { return &___seeThru_3; }
	inline void set_seeThru_3(GameObject_t1756533147 * value)
	{
		___seeThru_3 = value;
		Il2CppCodeGenWriteBarrier(&___seeThru_3, value);
	}

	inline static int32_t get_offset_of_interactable_4() { return static_cast<int32_t>(offsetof(SeeThru_t2977129686, ___interactable_4)); }
	inline Interactable_t1274046986 * get_interactable_4() const { return ___interactable_4; }
	inline Interactable_t1274046986 ** get_address_of_interactable_4() { return &___interactable_4; }
	inline void set_interactable_4(Interactable_t1274046986 * value)
	{
		___interactable_4 = value;
		Il2CppCodeGenWriteBarrier(&___interactable_4, value);
	}

	inline static int32_t get_offset_of_sourceRenderer_5() { return static_cast<int32_t>(offsetof(SeeThru_t2977129686, ___sourceRenderer_5)); }
	inline Renderer_t257310565 * get_sourceRenderer_5() const { return ___sourceRenderer_5; }
	inline Renderer_t257310565 ** get_address_of_sourceRenderer_5() { return &___sourceRenderer_5; }
	inline void set_sourceRenderer_5(Renderer_t257310565 * value)
	{
		___sourceRenderer_5 = value;
		Il2CppCodeGenWriteBarrier(&___sourceRenderer_5, value);
	}

	inline static int32_t get_offset_of_destRenderer_6() { return static_cast<int32_t>(offsetof(SeeThru_t2977129686, ___destRenderer_6)); }
	inline Renderer_t257310565 * get_destRenderer_6() const { return ___destRenderer_6; }
	inline Renderer_t257310565 ** get_address_of_destRenderer_6() { return &___destRenderer_6; }
	inline void set_destRenderer_6(Renderer_t257310565 * value)
	{
		___destRenderer_6 = value;
		Il2CppCodeGenWriteBarrier(&___destRenderer_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
