﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0
struct U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::.ctor()
extern "C"  void U3CTeleportHintCoroutineU3Ec__Iterator0__ctor_m2157333379 (U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::MoveNext()
extern "C"  bool U3CTeleportHintCoroutineU3Ec__Iterator0_MoveNext_m3548691533 (U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTeleportHintCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1706394445 (U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTeleportHintCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4046691237 (U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::Dispose()
extern "C"  void U3CTeleportHintCoroutineU3Ec__Iterator0_Dispose_m2973946172 (U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::Reset()
extern "C"  void U3CTeleportHintCoroutineU3Ec__Iterator0_Reset_m2591025086 (U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
