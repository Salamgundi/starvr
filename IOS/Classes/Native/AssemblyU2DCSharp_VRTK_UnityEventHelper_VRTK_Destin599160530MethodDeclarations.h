﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t599160530;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents/UnityObjectEvent::.ctor()
extern "C"  void UnityObjectEvent__ctor_m1795952993 (UnityObjectEvent_t599160530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
