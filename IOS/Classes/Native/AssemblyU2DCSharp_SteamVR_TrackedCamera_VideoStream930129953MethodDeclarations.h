﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_TrackedCamera/VideoStreamTexture
struct VideoStreamTexture_t930129953;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_VRTextureBounds_t1897807375.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraFrameTy2003723073.h"
#include "AssemblyU2DCSharp_SteamVR_Utils_RigidTransform2602383126.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_Valve_VR_TrackedDevicePose_t1668551120.h"

// System.Void SteamVR_TrackedCamera/VideoStreamTexture::.ctor(System.UInt32,System.Boolean)
extern "C"  void VideoStreamTexture__ctor_m520957735 (VideoStreamTexture_t930129953 * __this, uint32_t ___deviceIndex0, bool ___undistorted1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_TrackedCamera/VideoStreamTexture::get_undistorted()
extern "C"  bool VideoStreamTexture_get_undistorted_m597351374 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedCamera/VideoStreamTexture::set_undistorted(System.Boolean)
extern "C"  void VideoStreamTexture_set_undistorted_m4018680063 (VideoStreamTexture_t930129953 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SteamVR_TrackedCamera/VideoStreamTexture::get_deviceIndex()
extern "C"  uint32_t VideoStreamTexture_get_deviceIndex_m9945598 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_TrackedCamera/VideoStreamTexture::get_hasCamera()
extern "C"  bool VideoStreamTexture_get_hasCamera_m2102068870 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_TrackedCamera/VideoStreamTexture::get_hasTracking()
extern "C"  bool VideoStreamTexture_get_hasTracking_m3747759564 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SteamVR_TrackedCamera/VideoStreamTexture::get_frameId()
extern "C"  uint32_t VideoStreamTexture_get_frameId_m2927675452 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.VRTextureBounds_t SteamVR_TrackedCamera/VideoStreamTexture::get_frameBounds()
extern "C"  VRTextureBounds_t_t1897807375  VideoStreamTexture_get_frameBounds_m1748960207 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedCamera/VideoStreamTexture::set_frameBounds(Valve.VR.VRTextureBounds_t)
extern "C"  void VideoStreamTexture_set_frameBounds_m333963778 (VideoStreamTexture_t930129953 * __this, VRTextureBounds_t_t1897807375  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraFrameType SteamVR_TrackedCamera/VideoStreamTexture::get_frameType()
extern "C"  int32_t VideoStreamTexture_get_frameType_m3933027992 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D SteamVR_TrackedCamera/VideoStreamTexture::get_texture()
extern "C"  Texture2D_t3542995729 * VideoStreamTexture_get_texture_m1604998143 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Utils/RigidTransform SteamVR_TrackedCamera/VideoStreamTexture::get_transform()
extern "C"  RigidTransform_t2602383126  VideoStreamTexture_get_transform_m2579138820 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SteamVR_TrackedCamera/VideoStreamTexture::get_velocity()
extern "C"  Vector3_t2243707580  VideoStreamTexture_get_velocity_m3960030802 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SteamVR_TrackedCamera/VideoStreamTexture::get_angularVelocity()
extern "C"  Vector3_t2243707580  VideoStreamTexture_get_angularVelocity_m724914328 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.TrackedDevicePose_t SteamVR_TrackedCamera/VideoStreamTexture::GetPose()
extern "C"  TrackedDevicePose_t_t1668551120  VideoStreamTexture_GetPose_m2264178292 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 SteamVR_TrackedCamera/VideoStreamTexture::Acquire()
extern "C"  uint64_t VideoStreamTexture_Acquire_m3377373236 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 SteamVR_TrackedCamera/VideoStreamTexture::Release()
extern "C"  uint64_t VideoStreamTexture_Release_m4254393925 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedCamera/VideoStreamTexture::Update()
extern "C"  void VideoStreamTexture_Update_m3755783949 (VideoStreamTexture_t930129953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
