﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.PanelMenuItemControllerEventArgs
struct PanelMenuItemControllerEventArgs_t2917504033;
struct PanelMenuItemControllerEventArgs_t2917504033_marshaled_pinvoke;
struct PanelMenuItemControllerEventArgs_t2917504033_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct PanelMenuItemControllerEventArgs_t2917504033;
struct PanelMenuItemControllerEventArgs_t2917504033_marshaled_pinvoke;

extern "C" void PanelMenuItemControllerEventArgs_t2917504033_marshal_pinvoke(const PanelMenuItemControllerEventArgs_t2917504033& unmarshaled, PanelMenuItemControllerEventArgs_t2917504033_marshaled_pinvoke& marshaled);
extern "C" void PanelMenuItemControllerEventArgs_t2917504033_marshal_pinvoke_back(const PanelMenuItemControllerEventArgs_t2917504033_marshaled_pinvoke& marshaled, PanelMenuItemControllerEventArgs_t2917504033& unmarshaled);
extern "C" void PanelMenuItemControllerEventArgs_t2917504033_marshal_pinvoke_cleanup(PanelMenuItemControllerEventArgs_t2917504033_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct PanelMenuItemControllerEventArgs_t2917504033;
struct PanelMenuItemControllerEventArgs_t2917504033_marshaled_com;

extern "C" void PanelMenuItemControllerEventArgs_t2917504033_marshal_com(const PanelMenuItemControllerEventArgs_t2917504033& unmarshaled, PanelMenuItemControllerEventArgs_t2917504033_marshaled_com& marshaled);
extern "C" void PanelMenuItemControllerEventArgs_t2917504033_marshal_com_back(const PanelMenuItemControllerEventArgs_t2917504033_marshaled_com& marshaled, PanelMenuItemControllerEventArgs_t2917504033& unmarshaled);
extern "C" void PanelMenuItemControllerEventArgs_t2917504033_marshal_com_cleanup(PanelMenuItemControllerEventArgs_t2917504033_marshaled_com& marshaled);
