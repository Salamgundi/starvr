﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_AcknowledgeQuit_UserPrompt
struct _AcknowledgeQuit_UserPrompt_t90686541;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_AcknowledgeQuit_UserPrompt::.ctor(System.Object,System.IntPtr)
extern "C"  void _AcknowledgeQuit_UserPrompt__ctor_m3212531650 (_AcknowledgeQuit_UserPrompt_t90686541 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_AcknowledgeQuit_UserPrompt::Invoke()
extern "C"  void _AcknowledgeQuit_UserPrompt_Invoke_m740905168 (_AcknowledgeQuit_UserPrompt_t90686541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_AcknowledgeQuit_UserPrompt::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _AcknowledgeQuit_UserPrompt_BeginInvoke_m559022539 (_AcknowledgeQuit_UserPrompt_t90686541 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_AcknowledgeQuit_UserPrompt::EndInvoke(System.IAsyncResult)
extern "C"  void _AcknowledgeQuit_UserPrompt_EndInvoke_m2619790188 (_AcknowledgeQuit_UserPrompt_t90686541 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
