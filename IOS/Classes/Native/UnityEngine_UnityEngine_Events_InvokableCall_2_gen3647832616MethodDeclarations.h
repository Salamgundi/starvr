﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.PlayerClimbEventArgs>
struct InvokableCall_2_t3647832616;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.PlayerClimbEventArgs>
struct UnityAction_2_t3633041732;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.PlayerClimbEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m2625014984_gshared (InvokableCall_2_t3647832616 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_2__ctor_m2625014984(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_2_t3647832616 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2__ctor_m2625014984_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.PlayerClimbEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m3415164735_gshared (InvokableCall_2_t3647832616 * __this, UnityAction_2_t3633041732 * ___action0, const MethodInfo* method);
#define InvokableCall_2__ctor_m3415164735(__this, ___action0, method) ((  void (*) (InvokableCall_2_t3647832616 *, UnityAction_2_t3633041732 *, const MethodInfo*))InvokableCall_2__ctor_m3415164735_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.PlayerClimbEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m561449686_gshared (InvokableCall_2_t3647832616 * __this, UnityAction_2_t3633041732 * ___value0, const MethodInfo* method);
#define InvokableCall_2_add_Delegate_m561449686(__this, ___value0, method) ((  void (*) (InvokableCall_2_t3647832616 *, UnityAction_2_t3633041732 *, const MethodInfo*))InvokableCall_2_add_Delegate_m561449686_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.PlayerClimbEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m598047497_gshared (InvokableCall_2_t3647832616 * __this, UnityAction_2_t3633041732 * ___value0, const MethodInfo* method);
#define InvokableCall_2_remove_Delegate_m598047497(__this, ___value0, method) ((  void (*) (InvokableCall_2_t3647832616 *, UnityAction_2_t3633041732 *, const MethodInfo*))InvokableCall_2_remove_Delegate_m598047497_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.PlayerClimbEventArgs>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m1221156515_gshared (InvokableCall_2_t3647832616 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_2_Invoke_m1221156515(__this, ___args0, method) ((  void (*) (InvokableCall_2_t3647832616 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_2_Invoke_m1221156515_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.PlayerClimbEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m1446331987_gshared (InvokableCall_2_t3647832616 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_2_Find_m1446331987(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_2_t3647832616 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2_Find_m1446331987_gshared)(__this, ___targetObj0, ___method1, method)
