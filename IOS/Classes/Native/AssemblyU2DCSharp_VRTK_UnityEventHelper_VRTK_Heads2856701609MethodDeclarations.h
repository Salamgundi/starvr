﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_HeadsetCollision_UnityEvents
struct VRTK_HeadsetCollision_UnityEvents_t2856701609;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventArgs1242373387.h"

// System.Void VRTK.UnityEventHelper.VRTK_HeadsetCollision_UnityEvents::.ctor()
extern "C"  void VRTK_HeadsetCollision_UnityEvents__ctor_m2554885694 (VRTK_HeadsetCollision_UnityEvents_t2856701609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetCollision_UnityEvents::SetHeadsetCollision()
extern "C"  void VRTK_HeadsetCollision_UnityEvents_SetHeadsetCollision_m1081291828 (VRTK_HeadsetCollision_UnityEvents_t2856701609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetCollision_UnityEvents::OnEnable()
extern "C"  void VRTK_HeadsetCollision_UnityEvents_OnEnable_m4242838778 (VRTK_HeadsetCollision_UnityEvents_t2856701609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetCollision_UnityEvents::HeadsetCollisionDetect(System.Object,VRTK.HeadsetCollisionEventArgs)
extern "C"  void VRTK_HeadsetCollision_UnityEvents_HeadsetCollisionDetect_m1135394445 (VRTK_HeadsetCollision_UnityEvents_t2856701609 * __this, Il2CppObject * ___o0, HeadsetCollisionEventArgs_t1242373387  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetCollision_UnityEvents::HeadsetCollisionEnded(System.Object,VRTK.HeadsetCollisionEventArgs)
extern "C"  void VRTK_HeadsetCollision_UnityEvents_HeadsetCollisionEnded_m1787715958 (VRTK_HeadsetCollision_UnityEvents_t2856701609 * __this, Il2CppObject * ___o0, HeadsetCollisionEventArgs_t1242373387  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetCollision_UnityEvents::OnDisable()
extern "C"  void VRTK_HeadsetCollision_UnityEvents_OnDisable_m3818767739 (VRTK_HeadsetCollision_UnityEvents_t2856701609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
