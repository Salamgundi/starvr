﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_ExternalCamera/Config
struct Config_t1802482304;
struct Config_t1802482304_marshaled_pinvoke;
struct Config_t1802482304_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct Config_t1802482304;
struct Config_t1802482304_marshaled_pinvoke;

extern "C" void Config_t1802482304_marshal_pinvoke(const Config_t1802482304& unmarshaled, Config_t1802482304_marshaled_pinvoke& marshaled);
extern "C" void Config_t1802482304_marshal_pinvoke_back(const Config_t1802482304_marshaled_pinvoke& marshaled, Config_t1802482304& unmarshaled);
extern "C" void Config_t1802482304_marshal_pinvoke_cleanup(Config_t1802482304_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Config_t1802482304;
struct Config_t1802482304_marshaled_com;

extern "C" void Config_t1802482304_marshal_com(const Config_t1802482304& unmarshaled, Config_t1802482304_marshaled_com& marshaled);
extern "C" void Config_t1802482304_marshal_com_back(const Config_t1802482304_marshaled_com& marshaled, Config_t1802482304& unmarshaled);
extern "C" void Config_t1802482304_marshal_com_cleanup(Config_t1802482304_marshaled_com& marshaled);
