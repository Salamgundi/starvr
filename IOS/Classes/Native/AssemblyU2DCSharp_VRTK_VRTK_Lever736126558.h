﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.HingeJoint
struct HingeJoint_t2745110831;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;

#include "AssemblyU2DCSharp_VRTK_VRTK_Control651619021.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Lever_LeverDirection1799098973.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Lever
struct  VRTK_Lever_t736126558  : public VRTK_Control_t651619021
{
public:
	// UnityEngine.GameObject VRTK.VRTK_Lever::connectedTo
	GameObject_t1756533147 * ___connectedTo_15;
	// VRTK.VRTK_Lever/LeverDirection VRTK.VRTK_Lever::direction
	int32_t ___direction_16;
	// System.Single VRTK.VRTK_Lever::minAngle
	float ___minAngle_17;
	// System.Single VRTK.VRTK_Lever::maxAngle
	float ___maxAngle_18;
	// System.Single VRTK.VRTK_Lever::stepSize
	float ___stepSize_19;
	// System.Single VRTK.VRTK_Lever::releasedFriction
	float ___releasedFriction_20;
	// System.Single VRTK.VRTK_Lever::grabbedFriction
	float ___grabbedFriction_21;
	// UnityEngine.HingeJoint VRTK.VRTK_Lever::leverHingeJoint
	HingeJoint_t2745110831 * ___leverHingeJoint_22;
	// System.Boolean VRTK.VRTK_Lever::leverHingeJointCreated
	bool ___leverHingeJointCreated_23;
	// UnityEngine.Rigidbody VRTK.VRTK_Lever::leverRigidbody
	Rigidbody_t4233889191 * ___leverRigidbody_24;

public:
	inline static int32_t get_offset_of_connectedTo_15() { return static_cast<int32_t>(offsetof(VRTK_Lever_t736126558, ___connectedTo_15)); }
	inline GameObject_t1756533147 * get_connectedTo_15() const { return ___connectedTo_15; }
	inline GameObject_t1756533147 ** get_address_of_connectedTo_15() { return &___connectedTo_15; }
	inline void set_connectedTo_15(GameObject_t1756533147 * value)
	{
		___connectedTo_15 = value;
		Il2CppCodeGenWriteBarrier(&___connectedTo_15, value);
	}

	inline static int32_t get_offset_of_direction_16() { return static_cast<int32_t>(offsetof(VRTK_Lever_t736126558, ___direction_16)); }
	inline int32_t get_direction_16() const { return ___direction_16; }
	inline int32_t* get_address_of_direction_16() { return &___direction_16; }
	inline void set_direction_16(int32_t value)
	{
		___direction_16 = value;
	}

	inline static int32_t get_offset_of_minAngle_17() { return static_cast<int32_t>(offsetof(VRTK_Lever_t736126558, ___minAngle_17)); }
	inline float get_minAngle_17() const { return ___minAngle_17; }
	inline float* get_address_of_minAngle_17() { return &___minAngle_17; }
	inline void set_minAngle_17(float value)
	{
		___minAngle_17 = value;
	}

	inline static int32_t get_offset_of_maxAngle_18() { return static_cast<int32_t>(offsetof(VRTK_Lever_t736126558, ___maxAngle_18)); }
	inline float get_maxAngle_18() const { return ___maxAngle_18; }
	inline float* get_address_of_maxAngle_18() { return &___maxAngle_18; }
	inline void set_maxAngle_18(float value)
	{
		___maxAngle_18 = value;
	}

	inline static int32_t get_offset_of_stepSize_19() { return static_cast<int32_t>(offsetof(VRTK_Lever_t736126558, ___stepSize_19)); }
	inline float get_stepSize_19() const { return ___stepSize_19; }
	inline float* get_address_of_stepSize_19() { return &___stepSize_19; }
	inline void set_stepSize_19(float value)
	{
		___stepSize_19 = value;
	}

	inline static int32_t get_offset_of_releasedFriction_20() { return static_cast<int32_t>(offsetof(VRTK_Lever_t736126558, ___releasedFriction_20)); }
	inline float get_releasedFriction_20() const { return ___releasedFriction_20; }
	inline float* get_address_of_releasedFriction_20() { return &___releasedFriction_20; }
	inline void set_releasedFriction_20(float value)
	{
		___releasedFriction_20 = value;
	}

	inline static int32_t get_offset_of_grabbedFriction_21() { return static_cast<int32_t>(offsetof(VRTK_Lever_t736126558, ___grabbedFriction_21)); }
	inline float get_grabbedFriction_21() const { return ___grabbedFriction_21; }
	inline float* get_address_of_grabbedFriction_21() { return &___grabbedFriction_21; }
	inline void set_grabbedFriction_21(float value)
	{
		___grabbedFriction_21 = value;
	}

	inline static int32_t get_offset_of_leverHingeJoint_22() { return static_cast<int32_t>(offsetof(VRTK_Lever_t736126558, ___leverHingeJoint_22)); }
	inline HingeJoint_t2745110831 * get_leverHingeJoint_22() const { return ___leverHingeJoint_22; }
	inline HingeJoint_t2745110831 ** get_address_of_leverHingeJoint_22() { return &___leverHingeJoint_22; }
	inline void set_leverHingeJoint_22(HingeJoint_t2745110831 * value)
	{
		___leverHingeJoint_22 = value;
		Il2CppCodeGenWriteBarrier(&___leverHingeJoint_22, value);
	}

	inline static int32_t get_offset_of_leverHingeJointCreated_23() { return static_cast<int32_t>(offsetof(VRTK_Lever_t736126558, ___leverHingeJointCreated_23)); }
	inline bool get_leverHingeJointCreated_23() const { return ___leverHingeJointCreated_23; }
	inline bool* get_address_of_leverHingeJointCreated_23() { return &___leverHingeJointCreated_23; }
	inline void set_leverHingeJointCreated_23(bool value)
	{
		___leverHingeJointCreated_23 = value;
	}

	inline static int32_t get_offset_of_leverRigidbody_24() { return static_cast<int32_t>(offsetof(VRTK_Lever_t736126558, ___leverRigidbody_24)); }
	inline Rigidbody_t4233889191 * get_leverRigidbody_24() const { return ___leverRigidbody_24; }
	inline Rigidbody_t4233889191 ** get_address_of_leverRigidbody_24() { return &___leverRigidbody_24; }
	inline void set_leverRigidbody_24(Rigidbody_t4233889191 * value)
	{
		___leverRigidbody_24 = value;
		Il2CppCodeGenWriteBarrier(&___leverRigidbody_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
