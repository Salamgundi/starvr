﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Gun
struct Gun_t3058999492;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.Examples.Gun::.ctor()
extern "C"  void Gun__ctor_m556516391 (Gun_t3058999492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Gun::StartUsing(UnityEngine.GameObject)
extern "C"  void Gun_StartUsing_m2953123577 (Gun_t3058999492 * __this, GameObject_t1756533147 * ___usingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Gun::Start()
extern "C"  void Gun_Start_m1282283251 (Gun_t3058999492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Gun::FireBullet()
extern "C"  void Gun_FireBullet_m1174377101 (Gun_t3058999492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
