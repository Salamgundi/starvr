﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR
struct SteamVR_t3920918452;
// Valve.VR.CVRSystem
struct CVRSystem_t1953699154;
// Valve.VR.CVRCompositor
struct CVRCompositor_t197946050;
// Valve.VR.CVROverlay
struct CVROverlay_t3377499315;
// Valve.VR.VRTextureBounds_t[]
struct VRTextureBounds_tU5BU5D_t650810582;
// SteamVR_Utils/RigidTransform[]
struct RigidTransformU5BU5D_t1649806291;
// System.String
struct String_t;
// Valve.VR.TrackedDevicePose_t[]
struct TrackedDevicePose_tU5BU5D_t2897272049;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRInitError1685532365.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRSystem1953699154.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRCompositor197946050.h"
#include "AssemblyU2DCSharp_Valve_VR_CVROverlay3377499315.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedDeviceProperty3226377054.h"

// System.Void SteamVR::.ctor()
extern "C"  void SteamVR__ctor_m1620583197 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR::get_active()
extern "C"  bool SteamVR_get_active_m4090918514 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR::get_enabled()
extern "C"  bool SteamVR_get_enabled_m2501210113 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::set_enabled(System.Boolean)
extern "C"  void SteamVR_set_enabled_m1511829892 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR SteamVR::get_instance()
extern "C"  SteamVR_t3920918452 * SteamVR_get_instance_m20404178 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR::get_usingNativeSupport()
extern "C"  bool SteamVR_get_usingNativeSupport_m2969787370 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR SteamVR::CreateInstance()
extern "C"  SteamVR_t3920918452 * SteamVR_CreateInstance_m2697998473 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::ReportError(Valve.VR.EVRInitError)
extern "C"  void SteamVR_ReportError_m3700741106 (Il2CppObject * __this /* static, unused */, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRSystem SteamVR::get_hmd()
extern "C"  CVRSystem_t1953699154 * SteamVR_get_hmd_m131959500 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::set_hmd(Valve.VR.CVRSystem)
extern "C"  void SteamVR_set_hmd_m1518745939 (SteamVR_t3920918452 * __this, CVRSystem_t1953699154 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRCompositor SteamVR::get_compositor()
extern "C"  CVRCompositor_t197946050 * SteamVR_get_compositor_m55341072 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::set_compositor(Valve.VR.CVRCompositor)
extern "C"  void SteamVR_set_compositor_m138999879 (SteamVR_t3920918452 * __this, CVRCompositor_t197946050 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVROverlay SteamVR::get_overlay()
extern "C"  CVROverlay_t3377499315 * SteamVR_get_overlay_m2124734854 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::set_overlay(Valve.VR.CVROverlay)
extern "C"  void SteamVR_set_overlay_m381673903 (SteamVR_t3920918452 * __this, CVROverlay_t3377499315 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR::get_initializing()
extern "C"  bool SteamVR_get_initializing_m1913496825 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::set_initializing(System.Boolean)
extern "C"  void SteamVR_set_initializing_m4197566558 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR::get_calibrating()
extern "C"  bool SteamVR_get_calibrating_m1756619568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::set_calibrating(System.Boolean)
extern "C"  void SteamVR_set_calibrating_m2668043137 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR::get_outOfRange()
extern "C"  bool SteamVR_get_outOfRange_m2247063566 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::set_outOfRange(System.Boolean)
extern "C"  void SteamVR_set_outOfRange_m1167558707 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR::get_sceneWidth()
extern "C"  float SteamVR_get_sceneWidth_m2988688892 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::set_sceneWidth(System.Single)
extern "C"  void SteamVR_set_sceneWidth_m3556692141 (SteamVR_t3920918452 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR::get_sceneHeight()
extern "C"  float SteamVR_get_sceneHeight_m1479429403 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::set_sceneHeight(System.Single)
extern "C"  void SteamVR_set_sceneHeight_m817797652 (SteamVR_t3920918452 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR::get_aspect()
extern "C"  float SteamVR_get_aspect_m2726976180 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::set_aspect(System.Single)
extern "C"  void SteamVR_set_aspect_m326828197 (SteamVR_t3920918452 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR::get_fieldOfView()
extern "C"  float SteamVR_get_fieldOfView_m1930035374 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::set_fieldOfView(System.Single)
extern "C"  void SteamVR_set_fieldOfView_m1787968203 (SteamVR_t3920918452 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 SteamVR::get_tanHalfFov()
extern "C"  Vector2_t2243707579  SteamVR_get_tanHalfFov_m1344115086 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::set_tanHalfFov(UnityEngine.Vector2)
extern "C"  void SteamVR_set_tanHalfFov_m2046547611 (SteamVR_t3920918452 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.VRTextureBounds_t[] SteamVR::get_textureBounds()
extern "C"  VRTextureBounds_tU5BU5D_t650810582* SteamVR_get_textureBounds_m3159745600 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::set_textureBounds(Valve.VR.VRTextureBounds_t[])
extern "C"  void SteamVR_set_textureBounds_m3073366225 (SteamVR_t3920918452 * __this, VRTextureBounds_tU5BU5D_t650810582* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Utils/RigidTransform[] SteamVR::get_eyes()
extern "C"  RigidTransformU5BU5D_t1649806291* SteamVR_get_eyes_m139136411 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::set_eyes(SteamVR_Utils/RigidTransform[])
extern "C"  void SteamVR_set_eyes_m549014354 (SteamVR_t3920918452 * __this, RigidTransformU5BU5D_t1649806291* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SteamVR::get_hmd_TrackingSystemName()
extern "C"  String_t* SteamVR_get_hmd_TrackingSystemName_m2013917584 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SteamVR::get_hmd_ModelNumber()
extern "C"  String_t* SteamVR_get_hmd_ModelNumber_m542309755 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SteamVR::get_hmd_SerialNumber()
extern "C"  String_t* SteamVR_get_hmd_SerialNumber_m3929602510 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR::get_hmd_SecondsFromVsyncToPhotons()
extern "C"  float SteamVR_get_hmd_SecondsFromVsyncToPhotons_m1344434982 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR::get_hmd_DisplayFrequency()
extern "C"  float SteamVR_get_hmd_DisplayFrequency_m3189131134 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SteamVR::GetTrackedDeviceString(System.UInt32)
extern "C"  String_t* SteamVR_GetTrackedDeviceString_m1302267149 (SteamVR_t3920918452 * __this, uint32_t ___deviceId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SteamVR::GetStringProperty(Valve.VR.ETrackedDeviceProperty)
extern "C"  String_t* SteamVR_GetStringProperty_m723617206 (SteamVR_t3920918452 * __this, int32_t ___prop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR::GetFloatProperty(Valve.VR.ETrackedDeviceProperty)
extern "C"  float SteamVR_GetFloatProperty_m2511700414 (SteamVR_t3920918452 * __this, int32_t ___prop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::OnInitializing(System.Boolean)
extern "C"  void SteamVR_OnInitializing_m932725298 (SteamVR_t3920918452 * __this, bool ___initializing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::OnCalibrating(System.Boolean)
extern "C"  void SteamVR_OnCalibrating_m2707364645 (SteamVR_t3920918452 * __this, bool ___calibrating0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::OnOutOfRange(System.Boolean)
extern "C"  void SteamVR_OnOutOfRange_m3794268895 (SteamVR_t3920918452 * __this, bool ___outOfRange0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::OnDeviceConnected(System.Int32,System.Boolean)
extern "C"  void SteamVR_OnDeviceConnected_m2576397333 (SteamVR_t3920918452 * __this, int32_t ___i0, bool ___connected1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::OnNewPoses(Valve.VR.TrackedDevicePose_t[])
extern "C"  void SteamVR_OnNewPoses_m1235909614 (SteamVR_t3920918452 * __this, TrackedDevicePose_tU5BU5D_t2897272049* ___poses0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::Finalize()
extern "C"  void SteamVR_Finalize_m4066662651 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::Dispose()
extern "C"  void SteamVR_Dispose_m818977494 (SteamVR_t3920918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::Dispose(System.Boolean)
extern "C"  void SteamVR_Dispose_m146158105 (SteamVR_t3920918452 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::SafeDispose()
extern "C"  void SteamVR_SafeDispose_m3496091611 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR::.cctor()
extern "C"  void SteamVR__cctor_m2461830542 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
