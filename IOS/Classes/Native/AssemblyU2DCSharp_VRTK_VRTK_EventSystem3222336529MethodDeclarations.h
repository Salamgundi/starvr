﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_EventSystem
struct VRTK_EventSystem_t3222336529;
// VRTK.VRTK_VRInputModule
struct VRTK_VRInputModule_t1472500726;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_VRInputModule1472500726.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"

// System.Void VRTK.VRTK_EventSystem::.ctor()
extern "C"  void VRTK_EventSystem__ctor_m1297374403 (VRTK_EventSystem_t3222336529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_EventSystem::RegisterVRInputModule(VRTK.VRTK_VRInputModule)
extern "C"  void VRTK_EventSystem_RegisterVRInputModule_m2456534659 (VRTK_EventSystem_t3222336529 * __this, VRTK_VRInputModule_t1472500726 * ___vrInputModule0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_EventSystem::UnregisterVRInputModule(VRTK.VRTK_VRInputModule)
extern "C"  void VRTK_EventSystem_UnregisterVRInputModule_m3327919882 (VRTK_EventSystem_t3222336529 * __this, VRTK_VRInputModule_t1472500726 * ___vrInputModule0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_EventSystem::OnEnable()
extern "C"  void VRTK_EventSystem_OnEnable_m1034780223 (VRTK_EventSystem_t3222336529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_EventSystem::OnDisable()
extern "C"  void VRTK_EventSystem_OnDisable_m2177127622 (VRTK_EventSystem_t3222336529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_EventSystem::Update()
extern "C"  void VRTK_EventSystem_Update_m3160977256 (VRTK_EventSystem_t3222336529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_EventSystem::OnApplicationFocus(System.Boolean)
extern "C"  void VRTK_EventSystem_OnApplicationFocus_m2676263359 (VRTK_EventSystem_t3222336529 * __this, bool ___hasFocus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_EventSystem::CopyValuesFrom(UnityEngine.EventSystems.EventSystem,UnityEngine.EventSystems.EventSystem)
extern "C"  void VRTK_EventSystem_CopyValuesFrom_m8174072 (Il2CppObject * __this /* static, unused */, EventSystem_t3466835263 * ___fromEventSystem0, EventSystem_t3466835263 * ___toEventSystem1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_EventSystem::SetEventSystemOfBaseInputModulesTo(UnityEngine.EventSystems.EventSystem)
extern "C"  void VRTK_EventSystem_SetEventSystemOfBaseInputModulesTo_m1303901930 (Il2CppObject * __this /* static, unused */, EventSystem_t3466835263 * ___eventSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_EventSystem::.cctor()
extern "C"  void VRTK_EventSystem__cctor_m1533773832 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
