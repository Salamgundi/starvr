﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_CompositorGoToBack
struct _CompositorGoToBack_t2760466547;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_CompositorGoToBack::.ctor(System.Object,System.IntPtr)
extern "C"  void _CompositorGoToBack__ctor_m2737103930 (_CompositorGoToBack_t2760466547 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_CompositorGoToBack::Invoke()
extern "C"  void _CompositorGoToBack_Invoke_m2784642240 (_CompositorGoToBack_t2760466547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_CompositorGoToBack::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _CompositorGoToBack_BeginInvoke_m2266833021 (_CompositorGoToBack_t2760466547 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_CompositorGoToBack::EndInvoke(System.IAsyncResult)
extern "C"  void _CompositorGoToBack_EndInvoke_m191624604 (_CompositorGoToBack_t2760466547 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
