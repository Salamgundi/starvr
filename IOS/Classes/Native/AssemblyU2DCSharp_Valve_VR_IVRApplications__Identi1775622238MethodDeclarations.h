﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_IdentifyApplication
struct _IdentifyApplication_t1775622238;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_IdentifyApplication::.ctor(System.Object,System.IntPtr)
extern "C"  void _IdentifyApplication__ctor_m783587869 (_IdentifyApplication_t1775622238 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_IdentifyApplication::Invoke(System.UInt32,System.String)
extern "C"  int32_t _IdentifyApplication_Invoke_m1843853891 (_IdentifyApplication_t1775622238 * __this, uint32_t ___unProcessId0, String_t* ___pchAppKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_IdentifyApplication::BeginInvoke(System.UInt32,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _IdentifyApplication_BeginInvoke_m2529881044 (_IdentifyApplication_t1775622238 * __this, uint32_t ___unProcessId0, String_t* ___pchAppKey1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_IdentifyApplication::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _IdentifyApplication_EndInvoke_m3215669723 (_IdentifyApplication_t1775622238 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
