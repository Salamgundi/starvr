﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnalyzeStars
struct  AnalyzeStars_t3158799765  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.ParticleSystem AnalyzeStars::particleSystem
	ParticleSystem_t3394631041 * ___particleSystem_2;
	// System.Int32 AnalyzeStars::maxParticles
	int32_t ___maxParticles_3;
	// UnityEngine.TextAsset AnalyzeStars::starCSV
	TextAsset_t3973159845 * ___starCSV_4;

public:
	inline static int32_t get_offset_of_particleSystem_2() { return static_cast<int32_t>(offsetof(AnalyzeStars_t3158799765, ___particleSystem_2)); }
	inline ParticleSystem_t3394631041 * get_particleSystem_2() const { return ___particleSystem_2; }
	inline ParticleSystem_t3394631041 ** get_address_of_particleSystem_2() { return &___particleSystem_2; }
	inline void set_particleSystem_2(ParticleSystem_t3394631041 * value)
	{
		___particleSystem_2 = value;
		Il2CppCodeGenWriteBarrier(&___particleSystem_2, value);
	}

	inline static int32_t get_offset_of_maxParticles_3() { return static_cast<int32_t>(offsetof(AnalyzeStars_t3158799765, ___maxParticles_3)); }
	inline int32_t get_maxParticles_3() const { return ___maxParticles_3; }
	inline int32_t* get_address_of_maxParticles_3() { return &___maxParticles_3; }
	inline void set_maxParticles_3(int32_t value)
	{
		___maxParticles_3 = value;
	}

	inline static int32_t get_offset_of_starCSV_4() { return static_cast<int32_t>(offsetof(AnalyzeStars_t3158799765, ___starCSV_4)); }
	inline TextAsset_t3973159845 * get_starCSV_4() const { return ___starCSV_4; }
	inline TextAsset_t3973159845 ** get_address_of_starCSV_4() { return &___starCSV_4; }
	inline void set_starCSV_4(TextAsset_t3973159845 * value)
	{
		___starCSV_4 = value;
		Il2CppCodeGenWriteBarrier(&___starCSV_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
