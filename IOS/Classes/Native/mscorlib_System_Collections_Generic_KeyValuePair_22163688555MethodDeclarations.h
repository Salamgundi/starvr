﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23104887502MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3489903754(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2163688555 *, int32_t, VRTK_SDKDetails_t1748250348 *, const MethodInfo*))KeyValuePair_2__ctor_m3340313999_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::get_Key()
#define KeyValuePair_2_get_Key_m1907287332(__this, method) ((  int32_t (*) (KeyValuePair_2_t2163688555 *, const MethodInfo*))KeyValuePair_2_get_Key_m1301319541_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2235199555(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2163688555 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m264665148_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::get_Value()
#define KeyValuePair_2_get_Value_m507435236(__this, method) ((  VRTK_SDKDetails_t1748250348 * (*) (KeyValuePair_2_t2163688555 *, const MethodInfo*))KeyValuePair_2_get_Value_m1120936565_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2430516083(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2163688555 *, VRTK_SDKDetails_t1748250348 *, const MethodInfo*))KeyValuePair_2_set_Value_m3158972364_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::ToString()
#define KeyValuePair_2_ToString_m1766535445(__this, method) ((  String_t* (*) (KeyValuePair_2_t2163688555 *, const MethodInfo*))KeyValuePair_2_ToString_m1271884226_gshared)(__this, method)
