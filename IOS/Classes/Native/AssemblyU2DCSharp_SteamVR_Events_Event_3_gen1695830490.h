﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen3149477088.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SteamVR_Events/Event`3<System.Object,System.Object,System.Object>
struct  Event_3_t1695830490  : public UnityEvent_3_t3149477088
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
