﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_HeightAdjustTeleport
struct VRTK_HeightAdjustTeleport_t2696079433;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void VRTK.VRTK_HeightAdjustTeleport::.ctor()
extern "C"  void VRTK_HeightAdjustTeleport__ctor_m1092601913 (VRTK_HeightAdjustTeleport_t2696079433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeightAdjustTeleport::OnEnable()
extern "C"  void VRTK_HeightAdjustTeleport_OnEnable_m3928852753 (VRTK_HeightAdjustTeleport_t2696079433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeightAdjustTeleport::OnDisable()
extern "C"  void VRTK_HeightAdjustTeleport_OnDisable_m1960340966 (VRTK_HeightAdjustTeleport_t2696079433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_HeightAdjustTeleport::GetNewPosition(UnityEngine.Vector3,UnityEngine.Transform,System.Boolean)
extern "C"  Vector3_t2243707580  VRTK_HeightAdjustTeleport_GetNewPosition_m1708122885 (VRTK_HeightAdjustTeleport_t2696079433 * __this, Vector3_t2243707580  ___tipPosition0, Transform_t3275118058 * ___target1, bool ___returnOriginalPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_HeightAdjustTeleport::GetTeleportY(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  float VRTK_HeightAdjustTeleport_GetTeleportY_m128457339 (VRTK_HeightAdjustTeleport_t2696079433 * __this, Transform_t3275118058 * ___target0, Vector3_t2243707580  ___tipPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
