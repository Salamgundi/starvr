﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_ControllerSim
struct SDK_ControllerSim_t66347054;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.SDK_ControllerSim::.ctor()
extern "C"  void SDK_ControllerSim__ctor_m2799168242 (SDK_ControllerSim_t66347054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_ControllerSim::get_Selected()
extern "C"  bool SDK_ControllerSim_get_Selected_m2763637030 (SDK_ControllerSim_t66347054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_ControllerSim::set_Selected(System.Boolean)
extern "C"  void SDK_ControllerSim_set_Selected_m3487301761 (SDK_ControllerSim_t66347054 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.SDK_ControllerSim::GetVelocity()
extern "C"  Vector3_t2243707580  SDK_ControllerSim_GetVelocity_m3406920293 (SDK_ControllerSim_t66347054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.SDK_ControllerSim::GetAngularVelocity()
extern "C"  Vector3_t2243707580  SDK_ControllerSim_GetAngularVelocity_m1263178319 (SDK_ControllerSim_t66347054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_ControllerSim::Awake()
extern "C"  void SDK_ControllerSim_Awake_m916423191 (SDK_ControllerSim_t66347054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_ControllerSim::Update()
extern "C"  void SDK_ControllerSim_Update_m2355212771 (SDK_ControllerSim_t66347054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
