﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_IsMirrorWindowVisible
struct _IsMirrorWindowVisible_t410903031;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_IsMirrorWindowVisible::.ctor(System.Object,System.IntPtr)
extern "C"  void _IsMirrorWindowVisible__ctor_m4166017668 (_IsMirrorWindowVisible_t410903031 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRCompositor/_IsMirrorWindowVisible::Invoke()
extern "C"  bool _IsMirrorWindowVisible_Invoke_m4090586820 (_IsMirrorWindowVisible_t410903031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_IsMirrorWindowVisible::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _IsMirrorWindowVisible_BeginInvoke_m3384776313 (_IsMirrorWindowVisible_t410903031 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRCompositor/_IsMirrorWindowVisible::EndInvoke(System.IAsyncResult)
extern "C"  bool _IsMirrorWindowVisible_EndInvoke_m1530024216 (_IsMirrorWindowVisible_t410903031 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
