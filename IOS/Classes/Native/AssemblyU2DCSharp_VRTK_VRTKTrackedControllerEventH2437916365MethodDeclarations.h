﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTKTrackedControllerEventHandler
struct VRTKTrackedControllerEventHandler_t2437916365;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_VRTKTrackedControllerEventA2407378264.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.VRTKTrackedControllerEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void VRTKTrackedControllerEventHandler__ctor_m4210029403 (VRTKTrackedControllerEventHandler_t2437916365 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTKTrackedControllerEventHandler::Invoke(System.Object,VRTK.VRTKTrackedControllerEventArgs)
extern "C"  void VRTKTrackedControllerEventHandler_Invoke_m4037681358 (VRTKTrackedControllerEventHandler_t2437916365 * __this, Il2CppObject * ___sender0, VRTKTrackedControllerEventArgs_t2407378264  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.VRTKTrackedControllerEventHandler::BeginInvoke(System.Object,VRTK.VRTKTrackedControllerEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * VRTKTrackedControllerEventHandler_BeginInvoke_m3154246081 (VRTKTrackedControllerEventHandler_t2437916365 * __this, Il2CppObject * ___sender0, VRTKTrackedControllerEventArgs_t2407378264  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTKTrackedControllerEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void VRTKTrackedControllerEventHandler_EndInvoke_m3508965689 (VRTKTrackedControllerEventHandler_t2437916365 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
