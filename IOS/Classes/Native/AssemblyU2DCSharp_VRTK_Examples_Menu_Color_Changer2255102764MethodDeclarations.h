﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Menu_Color_Changer
struct Menu_Color_Changer_t2255102764;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.Examples.Menu_Color_Changer::.ctor()
extern "C"  void Menu_Color_Changer__ctor_m2138104097 (Menu_Color_Changer_t2255102764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Menu_Color_Changer::StartUsing(UnityEngine.GameObject)
extern "C"  void Menu_Color_Changer_StartUsing_m1849563523 (Menu_Color_Changer_t2255102764 * __this, GameObject_t1756533147 * ___usingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Menu_Color_Changer::OnEnable()
extern "C"  void Menu_Color_Changer_OnEnable_m3995391721 (Menu_Color_Changer_t2255102764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Menu_Color_Changer::ResetMenuItems()
extern "C"  void Menu_Color_Changer_ResetMenuItems_m3813752633 (Menu_Color_Changer_t2255102764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
