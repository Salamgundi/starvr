﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PagedScrollRect
struct PagedScrollRect_t3048021378;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void PagedScrollRect::.ctor()
extern "C"  void PagedScrollRect__ctor_m1951253373 (PagedScrollRect_t3048021378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PagedScrollRect::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void PagedScrollRect_OnPointerEnter_m1951426943 (PagedScrollRect_t3048021378 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PagedScrollRect::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void PagedScrollRect_OnPointerExit_m506837327 (PagedScrollRect_t3048021378 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
