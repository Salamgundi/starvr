﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3457420366MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1392629595(__this, ___l0, method) ((  void (*) (Enumerator_t1485172204 *, List_1_t1950442530 *, const MethodInfo*))Enumerator__ctor_m3360409919_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1790882167(__this, method) ((  void (*) (Enumerator_t1485172204 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1421715187_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3368364359(__this, method) ((  Il2CppObject * (*) (Enumerator_t1485172204 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3461842571_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::Dispose()
#define Enumerator_Dispose_m3575188306(__this, method) ((  void (*) (Enumerator_t1485172204 *, const MethodInfo*))Enumerator_Dispose_m28399880_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::VerifyState()
#define Enumerator_VerifyState_m1684308017(__this, method) ((  void (*) (Enumerator_t1485172204 *, const MethodInfo*))Enumerator_VerifyState_m699500485_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::MoveNext()
#define Enumerator_MoveNext_m2148943544(__this, method) ((  bool (*) (Enumerator_t1485172204 *, const MethodInfo*))Enumerator_MoveNext_m1551428375_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::get_Current()
#define Enumerator_get_Current_m909880774(__this, method) ((  KeyValuePair_2_t2581321398  (*) (Enumerator_t1485172204 *, const MethodInfo*))Enumerator_get_Current_m1781954980_gshared)(__this, method)
