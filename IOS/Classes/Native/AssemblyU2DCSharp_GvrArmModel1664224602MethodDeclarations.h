﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrArmModel
struct GvrArmModel_t1664224602;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrArmModel::.ctor()
extern "C"  void GvrArmModel__ctor_m1809788965 (GvrArmModel_t1664224602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
