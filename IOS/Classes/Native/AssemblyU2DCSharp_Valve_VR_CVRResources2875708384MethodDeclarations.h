﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRResources
struct CVRResources_t2875708384;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Valve.VR.CVRResources::.ctor(System.IntPtr)
extern "C"  void CVRResources__ctor_m3421553599 (CVRResources_t2875708384 * __this, IntPtr_t ___pInterface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRResources::LoadSharedResource(System.String,System.String,System.UInt32)
extern "C"  uint32_t CVRResources_LoadSharedResource_m3407533735 (CVRResources_t2875708384 * __this, String_t* ___pchResourceName0, String_t* ___pchBuffer1, uint32_t ___unBufferLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRResources::GetResourceFullPath(System.String,System.String,System.String,System.UInt32)
extern "C"  uint32_t CVRResources_GetResourceFullPath_m3437523814 (CVRResources_t2875708384 * __this, String_t* ___pchResourceName0, String_t* ___pchResourceTypeDirectory1, String_t* ___pchPathBuffer2, uint32_t ___unBufferLen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
