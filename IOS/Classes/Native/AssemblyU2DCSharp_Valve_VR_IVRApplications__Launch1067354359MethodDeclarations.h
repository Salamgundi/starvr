﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_LaunchInternalProcess
struct _LaunchInternalProcess_t1067354359;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_LaunchInternalProcess::.ctor(System.Object,System.IntPtr)
extern "C"  void _LaunchInternalProcess__ctor_m732541354 (_LaunchInternalProcess_t1067354359 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_LaunchInternalProcess::Invoke(System.String,System.String,System.String)
extern "C"  int32_t _LaunchInternalProcess_Invoke_m2746823590 (_LaunchInternalProcess_t1067354359 * __this, String_t* ___pchBinaryPath0, String_t* ___pchArguments1, String_t* ___pchWorkingDirectory2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_LaunchInternalProcess::BeginInvoke(System.String,System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _LaunchInternalProcess_BeginInvoke_m1136181899 (_LaunchInternalProcess_t1067354359 * __this, String_t* ___pchBinaryPath0, String_t* ___pchArguments1, String_t* ___pchWorkingDirectory2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_LaunchInternalProcess::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _LaunchInternalProcess_EndInvoke_m1696494400 (_LaunchInternalProcess_t1067354359 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
