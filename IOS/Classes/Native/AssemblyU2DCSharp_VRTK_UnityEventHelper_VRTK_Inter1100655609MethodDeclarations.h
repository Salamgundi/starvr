﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_InteractGrab_UnityEvents
struct VRTK_InteractGrab_UnityEvents_t1100655609;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventArgs771291242.h"

// System.Void VRTK.UnityEventHelper.VRTK_InteractGrab_UnityEvents::.ctor()
extern "C"  void VRTK_InteractGrab_UnityEvents__ctor_m585221404 (VRTK_InteractGrab_UnityEvents_t1100655609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractGrab_UnityEvents::SetInteractGrab()
extern "C"  void VRTK_InteractGrab_UnityEvents_SetInteractGrab_m1336184276 (VRTK_InteractGrab_UnityEvents_t1100655609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractGrab_UnityEvents::OnEnable()
extern "C"  void VRTK_InteractGrab_UnityEvents_OnEnable_m4029950824 (VRTK_InteractGrab_UnityEvents_t1100655609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractGrab_UnityEvents::ControllerGrabInteractableObject(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_InteractGrab_UnityEvents_ControllerGrabInteractableObject_m2069376044 (VRTK_InteractGrab_UnityEvents_t1100655609 * __this, Il2CppObject * ___o0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractGrab_UnityEvents::ControllerUngrabInteractableObject(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_InteractGrab_UnityEvents_ControllerUngrabInteractableObject_m3668198465 (VRTK_InteractGrab_UnityEvents_t1100655609 * __this, Il2CppObject * ___o0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractGrab_UnityEvents::OnDisable()
extern "C"  void VRTK_InteractGrab_UnityEvents_OnDisable_m836023103 (VRTK_InteractGrab_UnityEvents_t1100655609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
