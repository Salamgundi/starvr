﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents
struct VRTK_UIPointer_UnityEvents_t980224384;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_UIPointerEventArgs1171985978.h"

// System.Void VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::.ctor()
extern "C"  void VRTK_UIPointer_UnityEvents__ctor_m2584603349 (VRTK_UIPointer_UnityEvents_t980224384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::SetUIPointer()
extern "C"  void VRTK_UIPointer_UnityEvents_SetUIPointer_m3388109546 (VRTK_UIPointer_UnityEvents_t980224384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::OnEnable()
extern "C"  void VRTK_UIPointer_UnityEvents_OnEnable_m1391363893 (VRTK_UIPointer_UnityEvents_t980224384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::UIPointerElementEnter(System.Object,VRTK.UIPointerEventArgs)
extern "C"  void VRTK_UIPointer_UnityEvents_UIPointerElementEnter_m844599979 (VRTK_UIPointer_UnityEvents_t980224384 * __this, Il2CppObject * ___o0, UIPointerEventArgs_t1171985978  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::UIPointerElementExit(System.Object,VRTK.UIPointerEventArgs)
extern "C"  void VRTK_UIPointer_UnityEvents_UIPointerElementExit_m3423339663 (VRTK_UIPointer_UnityEvents_t980224384 * __this, Il2CppObject * ___o0, UIPointerEventArgs_t1171985978  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::UIPointerElementClick(System.Object,VRTK.UIPointerEventArgs)
extern "C"  void VRTK_UIPointer_UnityEvents_UIPointerElementClick_m1032986169 (VRTK_UIPointer_UnityEvents_t980224384 * __this, Il2CppObject * ___o0, UIPointerEventArgs_t1171985978  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::UIPointerElementDragStart(System.Object,VRTK.UIPointerEventArgs)
extern "C"  void VRTK_UIPointer_UnityEvents_UIPointerElementDragStart_m2986377523 (VRTK_UIPointer_UnityEvents_t980224384 * __this, Il2CppObject * ___o0, UIPointerEventArgs_t1171985978  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::UIPointerElementDragEnd(System.Object,VRTK.UIPointerEventArgs)
extern "C"  void VRTK_UIPointer_UnityEvents_UIPointerElementDragEnd_m2827593304 (VRTK_UIPointer_UnityEvents_t980224384 * __this, Il2CppObject * ___o0, UIPointerEventArgs_t1171985978  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents::OnDisable()
extern "C"  void VRTK_UIPointer_UnityEvents_OnDisable_m18946314 (VRTK_UIPointer_UnityEvents_t980224384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
