﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ScreenFade
struct VRTK_ScreenFade_t2079719832;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void VRTK.VRTK_ScreenFade::.ctor()
extern "C"  void VRTK_ScreenFade__ctor_m41033330 (VRTK_ScreenFade_t2079719832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ScreenFade::Start(UnityEngine.Color,System.Single)
extern "C"  void VRTK_ScreenFade_Start_m3402784275 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___newColor0, float ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ScreenFade::StartFade(UnityEngine.Color,System.Single)
extern "C"  void VRTK_ScreenFade_StartFade_m918667397 (VRTK_ScreenFade_t2079719832 * __this, Color_t2020392075  ___newColor0, float ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ScreenFade::Awake()
extern "C"  void VRTK_ScreenFade_Awake_m2445994845 (VRTK_ScreenFade_t2079719832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ScreenFade::OnPostRender()
extern "C"  void VRTK_ScreenFade_OnPostRender_m3307520427 (VRTK_ScreenFade_t2079719832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
