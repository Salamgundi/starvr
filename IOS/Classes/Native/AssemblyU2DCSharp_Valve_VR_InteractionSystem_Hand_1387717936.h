﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.Hand/AttachedObject
struct  AttachedObject_t1387717936 
{
public:
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Hand/AttachedObject::attachedObject
	GameObject_t1756533147 * ___attachedObject_0;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Hand/AttachedObject::originalParent
	GameObject_t1756533147 * ___originalParent_1;
	// System.Boolean Valve.VR.InteractionSystem.Hand/AttachedObject::isParentedToHand
	bool ___isParentedToHand_2;

public:
	inline static int32_t get_offset_of_attachedObject_0() { return static_cast<int32_t>(offsetof(AttachedObject_t1387717936, ___attachedObject_0)); }
	inline GameObject_t1756533147 * get_attachedObject_0() const { return ___attachedObject_0; }
	inline GameObject_t1756533147 ** get_address_of_attachedObject_0() { return &___attachedObject_0; }
	inline void set_attachedObject_0(GameObject_t1756533147 * value)
	{
		___attachedObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___attachedObject_0, value);
	}

	inline static int32_t get_offset_of_originalParent_1() { return static_cast<int32_t>(offsetof(AttachedObject_t1387717936, ___originalParent_1)); }
	inline GameObject_t1756533147 * get_originalParent_1() const { return ___originalParent_1; }
	inline GameObject_t1756533147 ** get_address_of_originalParent_1() { return &___originalParent_1; }
	inline void set_originalParent_1(GameObject_t1756533147 * value)
	{
		___originalParent_1 = value;
		Il2CppCodeGenWriteBarrier(&___originalParent_1, value);
	}

	inline static int32_t get_offset_of_isParentedToHand_2() { return static_cast<int32_t>(offsetof(AttachedObject_t1387717936, ___isParentedToHand_2)); }
	inline bool get_isParentedToHand_2() const { return ___isParentedToHand_2; }
	inline bool* get_address_of_isParentedToHand_2() { return &___isParentedToHand_2; }
	inline void set_isParentedToHand_2(bool value)
	{
		___isParentedToHand_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Valve.VR.InteractionSystem.Hand/AttachedObject
struct AttachedObject_t1387717936_marshaled_pinvoke
{
	GameObject_t1756533147 * ___attachedObject_0;
	GameObject_t1756533147 * ___originalParent_1;
	int32_t ___isParentedToHand_2;
};
// Native definition for COM marshalling of Valve.VR.InteractionSystem.Hand/AttachedObject
struct AttachedObject_t1387717936_marshaled_com
{
	GameObject_t1756533147 * ___attachedObject_0;
	GameObject_t1756533147 * ___originalParent_1;
	int32_t ___isParentedToHand_2;
};
