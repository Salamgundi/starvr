﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_ReleaseMirrorTextureD3D11
struct _ReleaseMirrorTextureD3D11_t2100129624;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_ReleaseMirrorTextureD3D11::.ctor(System.Object,System.IntPtr)
extern "C"  void _ReleaseMirrorTextureD3D11__ctor_m35173435 (_ReleaseMirrorTextureD3D11_t2100129624 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_ReleaseMirrorTextureD3D11::Invoke(System.IntPtr)
extern "C"  void _ReleaseMirrorTextureD3D11_Invoke_m1720246033 (_ReleaseMirrorTextureD3D11_t2100129624 * __this, IntPtr_t ___pD3D11ShaderResourceView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_ReleaseMirrorTextureD3D11::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ReleaseMirrorTextureD3D11_BeginInvoke_m2966221514 (_ReleaseMirrorTextureD3D11_t2100129624 * __this, IntPtr_t ___pD3D11ShaderResourceView0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_ReleaseMirrorTextureD3D11::EndInvoke(System.IAsyncResult)
extern "C"  void _ReleaseMirrorTextureD3D11_EndInvoke_m2195958321 (_ReleaseMirrorTextureD3D11_t2100129624 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
