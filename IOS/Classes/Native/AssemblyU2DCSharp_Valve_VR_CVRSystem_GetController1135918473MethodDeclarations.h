﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRSystem/GetControllerStateUnion
struct GetControllerStateUnion_t1135918473;
struct GetControllerStateUnion_t1135918473_marshaled_pinvoke;
struct GetControllerStateUnion_t1135918473_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct GetControllerStateUnion_t1135918473;
struct GetControllerStateUnion_t1135918473_marshaled_pinvoke;

extern "C" void GetControllerStateUnion_t1135918473_marshal_pinvoke(const GetControllerStateUnion_t1135918473& unmarshaled, GetControllerStateUnion_t1135918473_marshaled_pinvoke& marshaled);
extern "C" void GetControllerStateUnion_t1135918473_marshal_pinvoke_back(const GetControllerStateUnion_t1135918473_marshaled_pinvoke& marshaled, GetControllerStateUnion_t1135918473& unmarshaled);
extern "C" void GetControllerStateUnion_t1135918473_marshal_pinvoke_cleanup(GetControllerStateUnion_t1135918473_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct GetControllerStateUnion_t1135918473;
struct GetControllerStateUnion_t1135918473_marshaled_com;

extern "C" void GetControllerStateUnion_t1135918473_marshal_com(const GetControllerStateUnion_t1135918473& unmarshaled, GetControllerStateUnion_t1135918473_marshaled_com& marshaled);
extern "C" void GetControllerStateUnion_t1135918473_marshal_com_back(const GetControllerStateUnion_t1135918473_marshaled_com& marshaled, GetControllerStateUnion_t1135918473& unmarshaled);
extern "C" void GetControllerStateUnion_t1135918473_marshal_com_cleanup(GetControllerStateUnion_t1135918473_marshaled_com& marshaled);
