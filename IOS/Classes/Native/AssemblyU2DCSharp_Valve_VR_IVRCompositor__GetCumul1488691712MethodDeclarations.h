﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_GetCumulativeStats
struct _GetCumulativeStats_t1488691712;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_Compositor_CumulativeSta450065686.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_GetCumulativeStats::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetCumulativeStats__ctor_m2390892569 (_GetCumulativeStats_t1488691712 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_GetCumulativeStats::Invoke(Valve.VR.Compositor_CumulativeStats&,System.UInt32)
extern "C"  void _GetCumulativeStats_Invoke_m3705063085 (_GetCumulativeStats_t1488691712 * __this, Compositor_CumulativeStats_t450065686 * ___pStats0, uint32_t ___nStatsSizeInBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_GetCumulativeStats::BeginInvoke(Valve.VR.Compositor_CumulativeStats&,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetCumulativeStats_BeginInvoke_m1839076308 (_GetCumulativeStats_t1488691712 * __this, Compositor_CumulativeStats_t450065686 * ___pStats0, uint32_t ___nStatsSizeInBytes1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_GetCumulativeStats::EndInvoke(Valve.VR.Compositor_CumulativeStats&,System.IAsyncResult)
extern "C"  void _GetCumulativeStats_EndInvoke_m31222759 (_GetCumulativeStats_t1488691712 * __this, Compositor_CumulativeStats_t450065686 * ___pStats0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
