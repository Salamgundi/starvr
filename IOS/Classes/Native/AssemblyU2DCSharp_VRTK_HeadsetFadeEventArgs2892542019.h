﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.HeadsetFadeEventArgs
struct  HeadsetFadeEventArgs_t2892542019 
{
public:
	// System.Single VRTK.HeadsetFadeEventArgs::timeTillComplete
	float ___timeTillComplete_0;
	// UnityEngine.Transform VRTK.HeadsetFadeEventArgs::currentTransform
	Transform_t3275118058 * ___currentTransform_1;

public:
	inline static int32_t get_offset_of_timeTillComplete_0() { return static_cast<int32_t>(offsetof(HeadsetFadeEventArgs_t2892542019, ___timeTillComplete_0)); }
	inline float get_timeTillComplete_0() const { return ___timeTillComplete_0; }
	inline float* get_address_of_timeTillComplete_0() { return &___timeTillComplete_0; }
	inline void set_timeTillComplete_0(float value)
	{
		___timeTillComplete_0 = value;
	}

	inline static int32_t get_offset_of_currentTransform_1() { return static_cast<int32_t>(offsetof(HeadsetFadeEventArgs_t2892542019, ___currentTransform_1)); }
	inline Transform_t3275118058 * get_currentTransform_1() const { return ___currentTransform_1; }
	inline Transform_t3275118058 ** get_address_of_currentTransform_1() { return &___currentTransform_1; }
	inline void set_currentTransform_1(Transform_t3275118058 * value)
	{
		___currentTransform_1 = value;
		Il2CppCodeGenWriteBarrier(&___currentTransform_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VRTK.HeadsetFadeEventArgs
struct HeadsetFadeEventArgs_t2892542019_marshaled_pinvoke
{
	float ___timeTillComplete_0;
	Transform_t3275118058 * ___currentTransform_1;
};
// Native definition for COM marshalling of VRTK.HeadsetFadeEventArgs
struct HeadsetFadeEventArgs_t2892542019_marshaled_com
{
	float ___timeTillComplete_0;
	Transform_t3275118058 * ___currentTransform_1;
};
