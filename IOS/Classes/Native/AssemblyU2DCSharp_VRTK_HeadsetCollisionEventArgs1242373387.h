﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.HeadsetCollisionEventArgs
struct  HeadsetCollisionEventArgs_t1242373387 
{
public:
	// UnityEngine.Collider VRTK.HeadsetCollisionEventArgs::collider
	Collider_t3497673348 * ___collider_0;
	// UnityEngine.Transform VRTK.HeadsetCollisionEventArgs::currentTransform
	Transform_t3275118058 * ___currentTransform_1;

public:
	inline static int32_t get_offset_of_collider_0() { return static_cast<int32_t>(offsetof(HeadsetCollisionEventArgs_t1242373387, ___collider_0)); }
	inline Collider_t3497673348 * get_collider_0() const { return ___collider_0; }
	inline Collider_t3497673348 ** get_address_of_collider_0() { return &___collider_0; }
	inline void set_collider_0(Collider_t3497673348 * value)
	{
		___collider_0 = value;
		Il2CppCodeGenWriteBarrier(&___collider_0, value);
	}

	inline static int32_t get_offset_of_currentTransform_1() { return static_cast<int32_t>(offsetof(HeadsetCollisionEventArgs_t1242373387, ___currentTransform_1)); }
	inline Transform_t3275118058 * get_currentTransform_1() const { return ___currentTransform_1; }
	inline Transform_t3275118058 ** get_address_of_currentTransform_1() { return &___currentTransform_1; }
	inline void set_currentTransform_1(Transform_t3275118058 * value)
	{
		___currentTransform_1 = value;
		Il2CppCodeGenWriteBarrier(&___currentTransform_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VRTK.HeadsetCollisionEventArgs
struct HeadsetCollisionEventArgs_t1242373387_marshaled_pinvoke
{
	Collider_t3497673348 * ___collider_0;
	Transform_t3275118058 * ___currentTransform_1;
};
// Native definition for COM marshalling of VRTK.HeadsetCollisionEventArgs
struct HeadsetCollisionEventArgs_t1242373387_marshaled_com
{
	Collider_t3497673348 * ___collider_0;
	Transform_t3275118058 * ___currentTransform_1;
};
