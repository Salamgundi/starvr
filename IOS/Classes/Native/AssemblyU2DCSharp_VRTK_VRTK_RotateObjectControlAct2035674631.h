﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_VRTK_BaseObjectControlActio3375001897.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_RotateObjectControlAction
struct  VRTK_RotateObjectControlAction_t2035674631  : public VRTK_BaseObjectControlAction_t3375001897
{
public:
	// System.Single VRTK.VRTK_RotateObjectControlAction::maximumRotationSpeed
	float ___maximumRotationSpeed_10;
	// System.Single VRTK.VRTK_RotateObjectControlAction::rotationMultiplier
	float ___rotationMultiplier_11;

public:
	inline static int32_t get_offset_of_maximumRotationSpeed_10() { return static_cast<int32_t>(offsetof(VRTK_RotateObjectControlAction_t2035674631, ___maximumRotationSpeed_10)); }
	inline float get_maximumRotationSpeed_10() const { return ___maximumRotationSpeed_10; }
	inline float* get_address_of_maximumRotationSpeed_10() { return &___maximumRotationSpeed_10; }
	inline void set_maximumRotationSpeed_10(float value)
	{
		___maximumRotationSpeed_10 = value;
	}

	inline static int32_t get_offset_of_rotationMultiplier_11() { return static_cast<int32_t>(offsetof(VRTK_RotateObjectControlAction_t2035674631, ___rotationMultiplier_11)); }
	inline float get_rotationMultiplier_11() const { return ___rotationMultiplier_11; }
	inline float* get_address_of_rotationMultiplier_11() { return &___rotationMultiplier_11; }
	inline void set_rotationMultiplier_11(float value)
	{
		___rotationMultiplier_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
