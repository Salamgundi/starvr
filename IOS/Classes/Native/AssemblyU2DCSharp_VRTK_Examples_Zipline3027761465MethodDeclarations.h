﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Zipline
struct Zipline_t3027761465;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"

// System.Void VRTK.Examples.Zipline::.ctor()
extern "C"  void Zipline__ctor_m1143820936 (Zipline_t3027761465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Zipline::OnInteractableObjectGrabbed(VRTK.InteractableObjectEventArgs)
extern "C"  void Zipline_OnInteractableObjectGrabbed_m3452333386 (Zipline_t3027761465 * __this, InteractableObjectEventArgs_t473175556  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Zipline::Awake()
extern "C"  void Zipline_Awake_m2849282599 (Zipline_t3027761465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Zipline::Update()
extern "C"  void Zipline_Update_m2704580579 (Zipline_t3027761465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
