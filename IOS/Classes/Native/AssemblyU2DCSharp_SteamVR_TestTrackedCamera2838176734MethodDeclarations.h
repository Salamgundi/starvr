﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_TestTrackedCamera
struct SteamVR_TestTrackedCamera_t2838176734;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_TestTrackedCamera::.ctor()
extern "C"  void SteamVR_TestTrackedCamera__ctor_m2115725695 (SteamVR_TestTrackedCamera_t2838176734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TestTrackedCamera::OnEnable()
extern "C"  void SteamVR_TestTrackedCamera_OnEnable_m1667019267 (SteamVR_TestTrackedCamera_t2838176734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TestTrackedCamera::OnDisable()
extern "C"  void SteamVR_TestTrackedCamera_OnDisable_m1895403770 (SteamVR_TestTrackedCamera_t2838176734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TestTrackedCamera::Update()
extern "C"  void SteamVR_TestTrackedCamera_Update_m2836563212 (SteamVR_TestTrackedCamera_t2838176734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
