﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_LockGLSharedTextureForAccess
struct _LockGLSharedTextureForAccess_t2180462479;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_LockGLSharedTextureForAccess::.ctor(System.Object,System.IntPtr)
extern "C"  void _LockGLSharedTextureForAccess__ctor_m1609714086 (_LockGLSharedTextureForAccess_t2180462479 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_LockGLSharedTextureForAccess::Invoke(System.IntPtr)
extern "C"  void _LockGLSharedTextureForAccess_Invoke_m2535687770 (_LockGLSharedTextureForAccess_t2180462479 * __this, IntPtr_t ___glSharedTextureHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_LockGLSharedTextureForAccess::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _LockGLSharedTextureForAccess_BeginInvoke_m691660307 (_LockGLSharedTextureForAccess_t2180462479 * __this, IntPtr_t ___glSharedTextureHandle0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_LockGLSharedTextureForAccess::EndInvoke(System.IAsyncResult)
extern "C"  void _LockGLSharedTextureForAccess_EndInvoke_m1306834528 (_LockGLSharedTextureForAccess_t2180462479 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
