﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_BodyPhysics
struct VRTK_BodyPhysics_t3414085265;
// VRTK.BodyPhysicsEventHandler
struct BodyPhysicsEventHandler_t3963963105;
// UnityEngine.Collision
struct Collision_t2876846408;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_BodyPhysicsEventHandler3963963105.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "AssemblyU2DCSharp_VRTK_BodyPhysicsEventArgs2230131654.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventArgs1852451661.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventArgs771291242.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"

// System.Void VRTK.VRTK_BodyPhysics::.ctor()
extern "C"  void VRTK_BodyPhysics__ctor_m3880102873 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::add_StartFalling(VRTK.BodyPhysicsEventHandler)
extern "C"  void VRTK_BodyPhysics_add_StartFalling_m531703056 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventHandler_t3963963105 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::remove_StartFalling(VRTK.BodyPhysicsEventHandler)
extern "C"  void VRTK_BodyPhysics_remove_StartFalling_m1449688141 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventHandler_t3963963105 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::add_StopFalling(VRTK.BodyPhysicsEventHandler)
extern "C"  void VRTK_BodyPhysics_add_StopFalling_m3819674712 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventHandler_t3963963105 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::remove_StopFalling(VRTK.BodyPhysicsEventHandler)
extern "C"  void VRTK_BodyPhysics_remove_StopFalling_m2014851729 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventHandler_t3963963105 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::add_StartMoving(VRTK.BodyPhysicsEventHandler)
extern "C"  void VRTK_BodyPhysics_add_StartMoving_m834676893 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventHandler_t3963963105 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::remove_StartMoving(VRTK.BodyPhysicsEventHandler)
extern "C"  void VRTK_BodyPhysics_remove_StartMoving_m3911756774 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventHandler_t3963963105 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::add_StopMoving(VRTK.BodyPhysicsEventHandler)
extern "C"  void VRTK_BodyPhysics_add_StopMoving_m3086017793 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventHandler_t3963963105 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::remove_StopMoving(VRTK.BodyPhysicsEventHandler)
extern "C"  void VRTK_BodyPhysics_remove_StopMoving_m4228590356 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventHandler_t3963963105 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::add_StartColliding(VRTK.BodyPhysicsEventHandler)
extern "C"  void VRTK_BodyPhysics_add_StartColliding_m888888706 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventHandler_t3963963105 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::remove_StartColliding(VRTK.BodyPhysicsEventHandler)
extern "C"  void VRTK_BodyPhysics_remove_StartColliding_m2106930307 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventHandler_t3963963105 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::add_StopColliding(VRTK.BodyPhysicsEventHandler)
extern "C"  void VRTK_BodyPhysics_add_StopColliding_m2594028154 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventHandler_t3963963105 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::remove_StopColliding(VRTK.BodyPhysicsEventHandler)
extern "C"  void VRTK_BodyPhysics_remove_StopColliding_m3212466555 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventHandler_t3963963105 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BodyPhysics::ArePhysicsEnabled()
extern "C"  bool VRTK_BodyPhysics_ArePhysicsEnabled_m765128513 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::ApplyBodyVelocity(UnityEngine.Vector3,System.Boolean,System.Boolean)
extern "C"  void VRTK_BodyPhysics_ApplyBodyVelocity_m3722539479 (VRTK_BodyPhysics_t3414085265 * __this, Vector3_t2243707580  ___velocity0, bool ___forcePhysicsOn1, bool ___applyMomentum2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::ToggleOnGround(System.Boolean)
extern "C"  void VRTK_BodyPhysics_ToggleOnGround_m3653489970 (VRTK_BodyPhysics_t3414085265 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::TogglePreventSnapToFloor(System.Boolean)
extern "C"  void VRTK_BodyPhysics_TogglePreventSnapToFloor_m1421424805 (VRTK_BodyPhysics_t3414085265 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BodyPhysics::IsFalling()
extern "C"  bool VRTK_BodyPhysics_IsFalling_m3925295988 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BodyPhysics::IsMoving()
extern "C"  bool VRTK_BodyPhysics_IsMoving_m1222407635 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BodyPhysics::IsLeaning()
extern "C"  bool VRTK_BodyPhysics_IsLeaning_m1822301361 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BodyPhysics::OnGround()
extern "C"  bool VRTK_BodyPhysics_OnGround_m1270734477 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnEnable()
extern "C"  void VRTK_BodyPhysics_OnEnable_m1737467425 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnDisable()
extern "C"  void VRTK_BodyPhysics_OnDisable_m3837375922 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::FixedUpdate()
extern "C"  void VRTK_BodyPhysics_FixedUpdate_m400159134 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void VRTK_BodyPhysics_OnCollisionEnter_m1247842559 (VRTK_BodyPhysics_t3414085265 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void VRTK_BodyPhysics_OnTriggerEnter_m3252052461 (VRTK_BodyPhysics_t3414085265 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnCollisionExit(UnityEngine.Collision)
extern "C"  void VRTK_BodyPhysics_OnCollisionExit_m2210569139 (VRTK_BodyPhysics_t3414085265 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnTriggerExit(UnityEngine.Collider)
extern "C"  void VRTK_BodyPhysics_OnTriggerExit_m1992332245 (VRTK_BodyPhysics_t3414085265 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnDrawGizmos()
extern "C"  void VRTK_BodyPhysics_OnDrawGizmos_m405530589 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnStartFalling(VRTK.BodyPhysicsEventArgs)
extern "C"  void VRTK_BodyPhysics_OnStartFalling_m1921147378 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventArgs_t2230131654  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnStopFalling(VRTK.BodyPhysicsEventArgs)
extern "C"  void VRTK_BodyPhysics_OnStopFalling_m3571176384 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventArgs_t2230131654  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnStartMoving(VRTK.BodyPhysicsEventArgs)
extern "C"  void VRTK_BodyPhysics_OnStartMoving_m52267697 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventArgs_t2230131654  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnStopMoving(VRTK.BodyPhysicsEventArgs)
extern "C"  void VRTK_BodyPhysics_OnStopMoving_m637563749 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventArgs_t2230131654  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnStartColliding(VRTK.BodyPhysicsEventArgs)
extern "C"  void VRTK_BodyPhysics_OnStartColliding_m1257755852 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventArgs_t2230131654  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnStopColliding(VRTK.BodyPhysicsEventArgs)
extern "C"  void VRTK_BodyPhysics_OnStopColliding_m1937502406 (VRTK_BodyPhysics_t3414085265 * __this, BodyPhysicsEventArgs_t2230131654  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.BodyPhysicsEventArgs VRTK.VRTK_BodyPhysics::SetBodyPhysicsEvent(UnityEngine.GameObject)
extern "C"  BodyPhysicsEventArgs_t2230131654  VRTK_BodyPhysics_SetBodyPhysicsEvent_m1432674472 (VRTK_BodyPhysics_t3414085265 * __this, GameObject_t1756533147 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::CalculateVelocity()
extern "C"  void VRTK_BodyPhysics_CalculateVelocity_m2126666362 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::TogglePhysics(System.Boolean)
extern "C"  void VRTK_BodyPhysics_TogglePhysics_m796943131 (VRTK_BodyPhysics_t3414085265 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::CheckBodyCollisionsSetting()
extern "C"  void VRTK_BodyPhysics_CheckBodyCollisionsSetting_m2834398358 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::CheckFalling()
extern "C"  void VRTK_BodyPhysics_CheckFalling_m1552989478 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::SetCurrentStandingPosition()
extern "C"  void VRTK_BodyPhysics_SetCurrentStandingPosition_m2924938539 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::SetIsMoving(UnityEngine.Vector2)
extern "C"  void VRTK_BodyPhysics_SetIsMoving_m2654567525 (VRTK_BodyPhysics_t3414085265 * __this, Vector2_t2243707579  ___currentHeadsetPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::CheckLean()
extern "C"  void VRTK_BodyPhysics_CheckLean_m2752561313 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::UpdateStandingPosition(UnityEngine.Vector2)
extern "C"  void VRTK_BodyPhysics_UpdateStandingPosition_m4186275121 (VRTK_BodyPhysics_t3414085265 * __this, Vector2_t2243707579  ___currentHeadsetPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::CheckHeadsetMovement()
extern "C"  void VRTK_BodyPhysics_CheckHeadsetMovement_m4201769758 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::MovementChanged(System.Boolean)
extern "C"  void VRTK_BodyPhysics_MovementChanged_m3012234367 (VRTK_BodyPhysics_t3414085265 * __this, bool ___movementState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::EnableDropToFloor()
extern "C"  void VRTK_BodyPhysics_EnableDropToFloor_m1982707970 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::DisableDropToFloor()
extern "C"  void VRTK_BodyPhysics_DisableDropToFloor_m2511853483 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::Teleporter_Teleported(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_BodyPhysics_Teleporter_Teleported_m3995443840 (VRTK_BodyPhysics_t3414085265 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::EnableBodyPhysics()
extern "C"  void VRTK_BodyPhysics_EnableBodyPhysics_m1227775095 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::DisableBodyPhysics()
extern "C"  void VRTK_BodyPhysics_DisableBodyPhysics_m2920151676 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::CreateCollider()
extern "C"  void VRTK_BodyPhysics_CreateCollider_m1808968579 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::DestroyCollider()
extern "C"  void VRTK_BodyPhysics_DestroyCollider_m2041307661 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::UpdateCollider()
extern "C"  void VRTK_BodyPhysics_UpdateCollider_m124054052 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::InitControllerListeners(UnityEngine.GameObject,System.Boolean)
extern "C"  void VRTK_BodyPhysics_InitControllerListeners_m392892341 (VRTK_BodyPhysics_t3414085265 * __this, GameObject_t1756533147 * ___mappedController0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_BodyPhysics::RestoreCollisions(UnityEngine.GameObject)
extern "C"  Il2CppObject * VRTK_BodyPhysics_RestoreCollisions_m2963610696 (VRTK_BodyPhysics_t3414085265 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::IgnoreCollisions(UnityEngine.Collider[],System.Boolean)
extern "C"  void VRTK_BodyPhysics_IgnoreCollisions_m174739370 (VRTK_BodyPhysics_t3414085265 * __this, ColliderU5BU5D_t462843629* ___colliders0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnGrabObject(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_BodyPhysics_OnGrabObject_m1477491088 (VRTK_BodyPhysics_t3414085265 * __this, Il2CppObject * ___sender0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::OnUngrabObject(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_BodyPhysics_OnUngrabObject_m2825846807 (VRTK_BodyPhysics_t3414085265 * __this, Il2CppObject * ___sender0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BodyPhysics::FloorIsGrabbedObject(UnityEngine.RaycastHit)
extern "C"  bool VRTK_BodyPhysics_FloorIsGrabbedObject_m1732015614 (VRTK_BodyPhysics_t3414085265 * __this, RaycastHit_t87180320  ___collidedObj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BodyPhysics::FloorHeightChanged(System.Single)
extern "C"  bool VRTK_BodyPhysics_FloorHeightChanged_m3976822717 (VRTK_BodyPhysics_t3414085265 * __this, float ___currentY0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BodyPhysics::ValidDrop(System.Boolean,UnityEngine.RaycastHit,System.Single)
extern "C"  bool VRTK_BodyPhysics_ValidDrop_m1901891809 (VRTK_BodyPhysics_t3414085265 * __this, bool ___rayHit0, RaycastHit_t87180320  ___rayCollidedWith1, float ___floorY2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_BodyPhysics::ControllerHeightCheck(UnityEngine.GameObject)
extern "C"  float VRTK_BodyPhysics_ControllerHeightCheck_m506161004 (VRTK_BodyPhysics_t3414085265 * __this, GameObject_t1756533147 * ___controllerObj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BodyPhysics::ControllersStillOverPreviousFloor()
extern "C"  bool VRTK_BodyPhysics_ControllersStillOverPreviousFloor_m3153144779 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::SnapToNearestFloor()
extern "C"  void VRTK_BodyPhysics_SnapToNearestFloor_m3444335804 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BodyPhysics::PreventFall(System.Single)
extern "C"  bool VRTK_BodyPhysics_PreventFall_m1208358849 (VRTK_BodyPhysics_t3414085265 * __this, float ___hitFloorY0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::HandleFall(System.Single,UnityEngine.RaycastHit)
extern "C"  void VRTK_BodyPhysics_HandleFall_m2738522510 (VRTK_BodyPhysics_t3414085265 * __this, float ___hitFloorY0, RaycastHit_t87180320  ___rayCollidedWith1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::StartFall(UnityEngine.GameObject)
extern "C"  void VRTK_BodyPhysics_StartFall_m3195170162 (VRTK_BodyPhysics_t3414085265 * __this, GameObject_t1756533147 * ___targetFloor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::StopFall()
extern "C"  void VRTK_BodyPhysics_StopFall_m266485372 (VRTK_BodyPhysics_t3414085265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::GravityFall(UnityEngine.RaycastHit)
extern "C"  void VRTK_BodyPhysics_GravityFall_m2393860497 (VRTK_BodyPhysics_t3414085265 * __this, RaycastHit_t87180320  ___rayCollidedWith0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics::TeleportFall(System.Single,UnityEngine.RaycastHit)
extern "C"  void VRTK_BodyPhysics_TeleportFall_m519122019 (VRTK_BodyPhysics_t3414085265 * __this, float ___floorY0, RaycastHit_t87180320  ___rayCollidedWith1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
