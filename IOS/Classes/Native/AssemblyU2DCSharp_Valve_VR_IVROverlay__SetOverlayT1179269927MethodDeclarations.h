﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetOverlayTextureBounds
struct _SetOverlayTextureBounds_t1179269927;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_VRTextureBounds_t1897807375.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetOverlayTextureBounds::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayTextureBounds__ctor_m427715900 (_SetOverlayTextureBounds_t1179269927 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTextureBounds::Invoke(System.UInt64,Valve.VR.VRTextureBounds_t&)
extern "C"  int32_t _SetOverlayTextureBounds_Invoke_m1166394270 (_SetOverlayTextureBounds_t1179269927 * __this, uint64_t ___ulOverlayHandle0, VRTextureBounds_t_t1897807375 * ___pOverlayTextureBounds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayTextureBounds::BeginInvoke(System.UInt64,Valve.VR.VRTextureBounds_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetOverlayTextureBounds_BeginInvoke_m2978399441 (_SetOverlayTextureBounds_t1179269927 * __this, uint64_t ___ulOverlayHandle0, VRTextureBounds_t_t1897807375 * ___pOverlayTextureBounds1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTextureBounds::EndInvoke(Valve.VR.VRTextureBounds_t&,System.IAsyncResult)
extern "C"  int32_t _SetOverlayTextureBounds_EndInvoke_m2056867507 (_SetOverlayTextureBounds_t1179269927 * __this, VRTextureBounds_t_t1897807375 * ___pOverlayTextureBounds0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
