﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Action`1<Valve.VR.VREvent_t>
struct Action_1_t568677678;
// SteamVR_Events/Event`1<Valve.VR.VREvent_t>
struct Event_1_t1285721510;
// UnityEngine.Events.UnityAction`1<Valve.VR.VREvent_t>
struct UnityAction_1_t476884844;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Events/Action`1<Valve.VR.VREvent_t>::.ctor(SteamVR_Events/Event`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Action_1__ctor_m2897621874_gshared (Action_1_t568677678 * __this, Event_1_t1285721510 * ____event0, UnityAction_1_t476884844 * ___action1, const MethodInfo* method);
#define Action_1__ctor_m2897621874(__this, ____event0, ___action1, method) ((  void (*) (Action_1_t568677678 *, Event_1_t1285721510 *, UnityAction_1_t476884844 *, const MethodInfo*))Action_1__ctor_m2897621874_gshared)(__this, ____event0, ___action1, method)
// System.Void SteamVR_Events/Action`1<Valve.VR.VREvent_t>::Enable(System.Boolean)
extern "C"  void Action_1_Enable_m1016492056_gshared (Action_1_t568677678 * __this, bool ___enabled0, const MethodInfo* method);
#define Action_1_Enable_m1016492056(__this, ___enabled0, method) ((  void (*) (Action_1_t568677678 *, bool, const MethodInfo*))Action_1_Enable_m1016492056_gshared)(__this, ___enabled0, method)
