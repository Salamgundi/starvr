﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_Wheel
struct VRTK_Wheel_t803354859;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_ControlValueRa2976216666.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"

// System.Void VRTK.VRTK_Wheel::.ctor()
extern "C"  void VRTK_Wheel__ctor_m4178971337 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Wheel::InitRequiredComponents()
extern "C"  void VRTK_Wheel_InitRequiredComponents_m2622498188 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Wheel::DetectSetup()
extern "C"  bool VRTK_Wheel_DetectSetup_m2996764025 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_Control/ControlValueRange VRTK.VRTK_Wheel::RegisterValueRange()
extern "C"  ControlValueRange_t2976216666  VRTK_Wheel_RegisterValueRange_m860455153 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Wheel::HandleUpdate()
extern "C"  void VRTK_Wheel_HandleUpdate_m1696673920 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Wheel::InitWheel()
extern "C"  void VRTK_Wheel_InitWheel_m1410538514 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Wheel::SetupRigidbody()
extern "C"  void VRTK_Wheel_SetupRigidbody_m3453275419 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Wheel::SetupHinge()
extern "C"  void VRTK_Wheel_SetupHinge_m2643596911 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Wheel::SetupHingeRestrictions()
extern "C"  void VRTK_Wheel_SetupHingeRestrictions_m305252104 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Wheel::ConfigureHingeSpring()
extern "C"  void VRTK_Wheel_ConfigureHingeSpring_m567323129 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Wheel::SetupInteractableObject()
extern "C"  void VRTK_Wheel_SetupInteractableObject_m3175052259 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Wheel::WheelInteractableObjectGrabbed(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_Wheel_WheelInteractableObjectGrabbed_m2047642123 (VRTK_Wheel_t803354859 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Wheel::WheelInteractableObjectUngrabbed(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_Wheel_WheelInteractableObjectUngrabbed_m162986388 (VRTK_Wheel_t803354859 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Wheel::CalculateValue()
extern "C"  void VRTK_Wheel_CalculateValue_m79252598 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
