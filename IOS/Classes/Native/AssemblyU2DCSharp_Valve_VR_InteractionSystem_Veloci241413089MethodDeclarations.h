﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0
struct U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::.ctor()
extern "C"  void U3CEstimateVelocityCoroutineU3Ec__Iterator0__ctor_m1559768318 (U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::MoveNext()
extern "C"  bool U3CEstimateVelocityCoroutineU3Ec__Iterator0_MoveNext_m1263936782 (U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEstimateVelocityCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2002617514 (U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEstimateVelocityCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3515570082 (U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::Dispose()
extern "C"  void U3CEstimateVelocityCoroutineU3Ec__Iterator0_Dispose_m737795059 (U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::Reset()
extern "C"  void U3CEstimateVelocityCoroutineU3Ec__Iterator0_Reset_m119314205 (U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
