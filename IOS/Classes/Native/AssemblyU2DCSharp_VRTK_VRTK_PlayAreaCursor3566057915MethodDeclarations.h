﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_PlayAreaCursor
struct VRTK_PlayAreaCursor_t3566057915;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.VRTK_PlayAreaCursor::.ctor()
extern "C"  void VRTK_PlayAreaCursor__ctor_m2680246139 (VRTK_PlayAreaCursor_t3566057915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_PlayAreaCursor::HasCollided()
extern "C"  bool VRTK_PlayAreaCursor_HasCollided_m2589292473 (VRTK_PlayAreaCursor_t3566057915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCursor::SetHeadsetPositionCompensation(System.Boolean)
extern "C"  void VRTK_PlayAreaCursor_SetHeadsetPositionCompensation_m2392409005 (VRTK_PlayAreaCursor_t3566057915 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCursor::SetPlayAreaCursorCollision(System.Boolean)
extern "C"  void VRTK_PlayAreaCursor_SetPlayAreaCursorCollision_m1926964129 (VRTK_PlayAreaCursor_t3566057915 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCursor::SetMaterialColor(UnityEngine.Color)
extern "C"  void VRTK_PlayAreaCursor_SetMaterialColor_m2494915709 (VRTK_PlayAreaCursor_t3566057915 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCursor::SetPlayAreaCursorTransform(UnityEngine.Vector3)
extern "C"  void VRTK_PlayAreaCursor_SetPlayAreaCursorTransform_m1312461305 (VRTK_PlayAreaCursor_t3566057915 * __this, Vector3_t2243707580  ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCursor::ToggleState(System.Boolean)
extern "C"  void VRTK_PlayAreaCursor_ToggleState_m505427369 (VRTK_PlayAreaCursor_t3566057915 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_PlayAreaCursor::IsActive()
extern "C"  bool VRTK_PlayAreaCursor_IsActive_m2231024317 (VRTK_PlayAreaCursor_t3566057915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_PlayAreaCursor::GetPlayAreaContainer()
extern "C"  GameObject_t1756533147 * VRTK_PlayAreaCursor_GetPlayAreaContainer_m3967850528 (VRTK_PlayAreaCursor_t3566057915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCursor::ToggleVisibility(System.Boolean)
extern "C"  void VRTK_PlayAreaCursor_ToggleVisibility_m4142660754 (VRTK_PlayAreaCursor_t3566057915 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCursor::OnEnable()
extern "C"  void VRTK_PlayAreaCursor_OnEnable_m3266051887 (VRTK_PlayAreaCursor_t3566057915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCursor::OnDisable()
extern "C"  void VRTK_PlayAreaCursor_OnDisable_m3789511060 (VRTK_PlayAreaCursor_t3566057915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCursor::Update()
extern "C"  void VRTK_PlayAreaCursor_Update_m3096212578 (VRTK_PlayAreaCursor_t3566057915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCursor::DrawPlayAreaCursorBoundary(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,UnityEngine.Vector3)
extern "C"  void VRTK_PlayAreaCursor_DrawPlayAreaCursorBoundary_m4158944807 (VRTK_PlayAreaCursor_t3566057915 * __this, int32_t ___index0, float ___left1, float ___right2, float ___top3, float ___bottom4, float ___thickness5, Vector3_t2243707580  ___localPosition6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCursor::InitPlayAreaCursor()
extern "C"  void VRTK_PlayAreaCursor_InitPlayAreaCursor_m3494112236 (VRTK_PlayAreaCursor_t3566057915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCursor::CreateCursorCollider(UnityEngine.GameObject)
extern "C"  void VRTK_PlayAreaCursor_CreateCursorCollider_m3634328815 (VRTK_PlayAreaCursor_t3566057915 * __this, GameObject_t1756533147 * ___cursor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCursor::UpdateCollider()
extern "C"  void VRTK_PlayAreaCursor_UpdateCollider_m2675295342 (VRTK_PlayAreaCursor_t3566057915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
