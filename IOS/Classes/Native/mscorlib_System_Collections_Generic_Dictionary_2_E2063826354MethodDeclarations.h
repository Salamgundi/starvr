﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Material[]>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2767908955(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2063826354 *, Dictionary_2_t743801652 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Material[]>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1574500122(__this, method) ((  Il2CppObject * (*) (Enumerator_t2063826354 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Material[]>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m69966756(__this, method) ((  void (*) (Enumerator_t2063826354 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Material[]>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3142147173(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2063826354 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Material[]>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3927951296(__this, method) ((  Il2CppObject * (*) (Enumerator_t2063826354 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Material[]>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m49484314(__this, method) ((  Il2CppObject * (*) (Enumerator_t2063826354 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Material[]>::MoveNext()
#define Enumerator_MoveNext_m3307145820(__this, method) ((  bool (*) (Enumerator_t2063826354 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Material[]>::get_Current()
#define Enumerator_get_Current_m3041614012(__this, method) ((  KeyValuePair_2_t2796114170  (*) (Enumerator_t2063826354 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Material[]>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3913571951(__this, method) ((  String_t* (*) (Enumerator_t2063826354 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Material[]>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2407716615(__this, method) ((  MaterialU5BU5D_t3123989686* (*) (Enumerator_t2063826354 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Material[]>::Reset()
#define Enumerator_Reset_m691190781(__this, method) ((  void (*) (Enumerator_t2063826354 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Material[]>::VerifyState()
#define Enumerator_VerifyState_m3579764080(__this, method) ((  void (*) (Enumerator_t2063826354 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Material[]>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2574112564(__this, method) ((  void (*) (Enumerator_t2063826354 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Material[]>::Dispose()
#define Enumerator_Dispose_m3827127391(__this, method) ((  void (*) (Enumerator_t2063826354 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
