﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraMove
struct CameraMove_t818163774;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraMove::.ctor()
extern "C"  void CameraMove__ctor_m3283250391 (CameraMove_t818163774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMove::Update()
extern "C"  void CameraMove_Update_m2835649416 (CameraMove_t818163774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
