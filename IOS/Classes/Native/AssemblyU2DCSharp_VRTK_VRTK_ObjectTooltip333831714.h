﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ObjectTooltip
struct  VRTK_ObjectTooltip_t333831714  : public MonoBehaviour_t1158329972
{
public:
	// System.String VRTK.VRTK_ObjectTooltip::displayText
	String_t* ___displayText_2;
	// System.Int32 VRTK.VRTK_ObjectTooltip::fontSize
	int32_t ___fontSize_3;
	// UnityEngine.Vector2 VRTK.VRTK_ObjectTooltip::containerSize
	Vector2_t2243707579  ___containerSize_4;
	// UnityEngine.Transform VRTK.VRTK_ObjectTooltip::drawLineFrom
	Transform_t3275118058 * ___drawLineFrom_5;
	// UnityEngine.Transform VRTK.VRTK_ObjectTooltip::drawLineTo
	Transform_t3275118058 * ___drawLineTo_6;
	// System.Single VRTK.VRTK_ObjectTooltip::lineWidth
	float ___lineWidth_7;
	// UnityEngine.Color VRTK.VRTK_ObjectTooltip::fontColor
	Color_t2020392075  ___fontColor_8;
	// UnityEngine.Color VRTK.VRTK_ObjectTooltip::containerColor
	Color_t2020392075  ___containerColor_9;
	// UnityEngine.Color VRTK.VRTK_ObjectTooltip::lineColor
	Color_t2020392075  ___lineColor_10;
	// UnityEngine.LineRenderer VRTK.VRTK_ObjectTooltip::line
	LineRenderer_t849157671 * ___line_11;

public:
	inline static int32_t get_offset_of_displayText_2() { return static_cast<int32_t>(offsetof(VRTK_ObjectTooltip_t333831714, ___displayText_2)); }
	inline String_t* get_displayText_2() const { return ___displayText_2; }
	inline String_t** get_address_of_displayText_2() { return &___displayText_2; }
	inline void set_displayText_2(String_t* value)
	{
		___displayText_2 = value;
		Il2CppCodeGenWriteBarrier(&___displayText_2, value);
	}

	inline static int32_t get_offset_of_fontSize_3() { return static_cast<int32_t>(offsetof(VRTK_ObjectTooltip_t333831714, ___fontSize_3)); }
	inline int32_t get_fontSize_3() const { return ___fontSize_3; }
	inline int32_t* get_address_of_fontSize_3() { return &___fontSize_3; }
	inline void set_fontSize_3(int32_t value)
	{
		___fontSize_3 = value;
	}

	inline static int32_t get_offset_of_containerSize_4() { return static_cast<int32_t>(offsetof(VRTK_ObjectTooltip_t333831714, ___containerSize_4)); }
	inline Vector2_t2243707579  get_containerSize_4() const { return ___containerSize_4; }
	inline Vector2_t2243707579 * get_address_of_containerSize_4() { return &___containerSize_4; }
	inline void set_containerSize_4(Vector2_t2243707579  value)
	{
		___containerSize_4 = value;
	}

	inline static int32_t get_offset_of_drawLineFrom_5() { return static_cast<int32_t>(offsetof(VRTK_ObjectTooltip_t333831714, ___drawLineFrom_5)); }
	inline Transform_t3275118058 * get_drawLineFrom_5() const { return ___drawLineFrom_5; }
	inline Transform_t3275118058 ** get_address_of_drawLineFrom_5() { return &___drawLineFrom_5; }
	inline void set_drawLineFrom_5(Transform_t3275118058 * value)
	{
		___drawLineFrom_5 = value;
		Il2CppCodeGenWriteBarrier(&___drawLineFrom_5, value);
	}

	inline static int32_t get_offset_of_drawLineTo_6() { return static_cast<int32_t>(offsetof(VRTK_ObjectTooltip_t333831714, ___drawLineTo_6)); }
	inline Transform_t3275118058 * get_drawLineTo_6() const { return ___drawLineTo_6; }
	inline Transform_t3275118058 ** get_address_of_drawLineTo_6() { return &___drawLineTo_6; }
	inline void set_drawLineTo_6(Transform_t3275118058 * value)
	{
		___drawLineTo_6 = value;
		Il2CppCodeGenWriteBarrier(&___drawLineTo_6, value);
	}

	inline static int32_t get_offset_of_lineWidth_7() { return static_cast<int32_t>(offsetof(VRTK_ObjectTooltip_t333831714, ___lineWidth_7)); }
	inline float get_lineWidth_7() const { return ___lineWidth_7; }
	inline float* get_address_of_lineWidth_7() { return &___lineWidth_7; }
	inline void set_lineWidth_7(float value)
	{
		___lineWidth_7 = value;
	}

	inline static int32_t get_offset_of_fontColor_8() { return static_cast<int32_t>(offsetof(VRTK_ObjectTooltip_t333831714, ___fontColor_8)); }
	inline Color_t2020392075  get_fontColor_8() const { return ___fontColor_8; }
	inline Color_t2020392075 * get_address_of_fontColor_8() { return &___fontColor_8; }
	inline void set_fontColor_8(Color_t2020392075  value)
	{
		___fontColor_8 = value;
	}

	inline static int32_t get_offset_of_containerColor_9() { return static_cast<int32_t>(offsetof(VRTK_ObjectTooltip_t333831714, ___containerColor_9)); }
	inline Color_t2020392075  get_containerColor_9() const { return ___containerColor_9; }
	inline Color_t2020392075 * get_address_of_containerColor_9() { return &___containerColor_9; }
	inline void set_containerColor_9(Color_t2020392075  value)
	{
		___containerColor_9 = value;
	}

	inline static int32_t get_offset_of_lineColor_10() { return static_cast<int32_t>(offsetof(VRTK_ObjectTooltip_t333831714, ___lineColor_10)); }
	inline Color_t2020392075  get_lineColor_10() const { return ___lineColor_10; }
	inline Color_t2020392075 * get_address_of_lineColor_10() { return &___lineColor_10; }
	inline void set_lineColor_10(Color_t2020392075  value)
	{
		___lineColor_10 = value;
	}

	inline static int32_t get_offset_of_line_11() { return static_cast<int32_t>(offsetof(VRTK_ObjectTooltip_t333831714, ___line_11)); }
	inline LineRenderer_t849157671 * get_line_11() const { return ___line_11; }
	inline LineRenderer_t849157671 ** get_address_of_line_11() { return &___line_11; }
	inline void set_line_11(LineRenderer_t849157671 * value)
	{
		___line_11 = value;
		Il2CppCodeGenWriteBarrier(&___line_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
