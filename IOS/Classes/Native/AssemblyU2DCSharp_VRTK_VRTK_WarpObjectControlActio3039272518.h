﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_VRTK_VRTK_BaseObjectControlActio3375001897.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_WarpObjectControlAction
struct  VRTK_WarpObjectControlAction_t3039272518  : public VRTK_BaseObjectControlAction_t3375001897
{
public:
	// System.Single VRTK.VRTK_WarpObjectControlAction::warpDistance
	float ___warpDistance_10;
	// System.Single VRTK.VRTK_WarpObjectControlAction::warpMultiplier
	float ___warpMultiplier_11;
	// System.Single VRTK.VRTK_WarpObjectControlAction::warpDelay
	float ___warpDelay_12;
	// System.Single VRTK.VRTK_WarpObjectControlAction::floorHeightTolerance
	float ___floorHeightTolerance_13;
	// System.Single VRTK.VRTK_WarpObjectControlAction::blinkTransitionSpeed
	float ___blinkTransitionSpeed_14;
	// System.Single VRTK.VRTK_WarpObjectControlAction::warpDelayTimer
	float ___warpDelayTimer_15;
	// UnityEngine.Transform VRTK.VRTK_WarpObjectControlAction::headset
	Transform_t3275118058 * ___headset_16;

public:
	inline static int32_t get_offset_of_warpDistance_10() { return static_cast<int32_t>(offsetof(VRTK_WarpObjectControlAction_t3039272518, ___warpDistance_10)); }
	inline float get_warpDistance_10() const { return ___warpDistance_10; }
	inline float* get_address_of_warpDistance_10() { return &___warpDistance_10; }
	inline void set_warpDistance_10(float value)
	{
		___warpDistance_10 = value;
	}

	inline static int32_t get_offset_of_warpMultiplier_11() { return static_cast<int32_t>(offsetof(VRTK_WarpObjectControlAction_t3039272518, ___warpMultiplier_11)); }
	inline float get_warpMultiplier_11() const { return ___warpMultiplier_11; }
	inline float* get_address_of_warpMultiplier_11() { return &___warpMultiplier_11; }
	inline void set_warpMultiplier_11(float value)
	{
		___warpMultiplier_11 = value;
	}

	inline static int32_t get_offset_of_warpDelay_12() { return static_cast<int32_t>(offsetof(VRTK_WarpObjectControlAction_t3039272518, ___warpDelay_12)); }
	inline float get_warpDelay_12() const { return ___warpDelay_12; }
	inline float* get_address_of_warpDelay_12() { return &___warpDelay_12; }
	inline void set_warpDelay_12(float value)
	{
		___warpDelay_12 = value;
	}

	inline static int32_t get_offset_of_floorHeightTolerance_13() { return static_cast<int32_t>(offsetof(VRTK_WarpObjectControlAction_t3039272518, ___floorHeightTolerance_13)); }
	inline float get_floorHeightTolerance_13() const { return ___floorHeightTolerance_13; }
	inline float* get_address_of_floorHeightTolerance_13() { return &___floorHeightTolerance_13; }
	inline void set_floorHeightTolerance_13(float value)
	{
		___floorHeightTolerance_13 = value;
	}

	inline static int32_t get_offset_of_blinkTransitionSpeed_14() { return static_cast<int32_t>(offsetof(VRTK_WarpObjectControlAction_t3039272518, ___blinkTransitionSpeed_14)); }
	inline float get_blinkTransitionSpeed_14() const { return ___blinkTransitionSpeed_14; }
	inline float* get_address_of_blinkTransitionSpeed_14() { return &___blinkTransitionSpeed_14; }
	inline void set_blinkTransitionSpeed_14(float value)
	{
		___blinkTransitionSpeed_14 = value;
	}

	inline static int32_t get_offset_of_warpDelayTimer_15() { return static_cast<int32_t>(offsetof(VRTK_WarpObjectControlAction_t3039272518, ___warpDelayTimer_15)); }
	inline float get_warpDelayTimer_15() const { return ___warpDelayTimer_15; }
	inline float* get_address_of_warpDelayTimer_15() { return &___warpDelayTimer_15; }
	inline void set_warpDelayTimer_15(float value)
	{
		___warpDelayTimer_15 = value;
	}

	inline static int32_t get_offset_of_headset_16() { return static_cast<int32_t>(offsetof(VRTK_WarpObjectControlAction_t3039272518, ___headset_16)); }
	inline Transform_t3275118058 * get_headset_16() const { return ___headset_16; }
	inline Transform_t3275118058 ** get_address_of_headset_16() { return &___headset_16; }
	inline void set_headset_16(Transform_t3275118058 * value)
	{
		___headset_16 = value;
		Il2CppCodeGenWriteBarrier(&___headset_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
