﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.IntersectionMaskRectangle_t
struct  IntersectionMaskRectangle_t_t2372341025 
{
public:
	// System.Single Valve.VR.IntersectionMaskRectangle_t::m_flTopLeftX
	float ___m_flTopLeftX_0;
	// System.Single Valve.VR.IntersectionMaskRectangle_t::m_flTopLeftY
	float ___m_flTopLeftY_1;
	// System.Single Valve.VR.IntersectionMaskRectangle_t::m_flWidth
	float ___m_flWidth_2;
	// System.Single Valve.VR.IntersectionMaskRectangle_t::m_flHeight
	float ___m_flHeight_3;

public:
	inline static int32_t get_offset_of_m_flTopLeftX_0() { return static_cast<int32_t>(offsetof(IntersectionMaskRectangle_t_t2372341025, ___m_flTopLeftX_0)); }
	inline float get_m_flTopLeftX_0() const { return ___m_flTopLeftX_0; }
	inline float* get_address_of_m_flTopLeftX_0() { return &___m_flTopLeftX_0; }
	inline void set_m_flTopLeftX_0(float value)
	{
		___m_flTopLeftX_0 = value;
	}

	inline static int32_t get_offset_of_m_flTopLeftY_1() { return static_cast<int32_t>(offsetof(IntersectionMaskRectangle_t_t2372341025, ___m_flTopLeftY_1)); }
	inline float get_m_flTopLeftY_1() const { return ___m_flTopLeftY_1; }
	inline float* get_address_of_m_flTopLeftY_1() { return &___m_flTopLeftY_1; }
	inline void set_m_flTopLeftY_1(float value)
	{
		___m_flTopLeftY_1 = value;
	}

	inline static int32_t get_offset_of_m_flWidth_2() { return static_cast<int32_t>(offsetof(IntersectionMaskRectangle_t_t2372341025, ___m_flWidth_2)); }
	inline float get_m_flWidth_2() const { return ___m_flWidth_2; }
	inline float* get_address_of_m_flWidth_2() { return &___m_flWidth_2; }
	inline void set_m_flWidth_2(float value)
	{
		___m_flWidth_2 = value;
	}

	inline static int32_t get_offset_of_m_flHeight_3() { return static_cast<int32_t>(offsetof(IntersectionMaskRectangle_t_t2372341025, ___m_flHeight_3)); }
	inline float get_m_flHeight_3() const { return ___m_flHeight_3; }
	inline float* get_address_of_m_flHeight_3() { return &___m_flHeight_3; }
	inline void set_m_flHeight_3(float value)
	{
		___m_flHeight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
