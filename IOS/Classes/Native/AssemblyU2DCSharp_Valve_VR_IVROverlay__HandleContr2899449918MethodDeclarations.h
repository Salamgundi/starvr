﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_HandleControllerOverlayInteractionAsMouse
struct _HandleControllerOverlayInteractionAsMouse_t2899449918;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_HandleControllerOverlayInteractionAsMouse::.ctor(System.Object,System.IntPtr)
extern "C"  void _HandleControllerOverlayInteractionAsMouse__ctor_m2602837093 (_HandleControllerOverlayInteractionAsMouse_t2899449918 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVROverlay/_HandleControllerOverlayInteractionAsMouse::Invoke(System.UInt64,System.UInt32)
extern "C"  bool _HandleControllerOverlayInteractionAsMouse_Invoke_m3488173890 (_HandleControllerOverlayInteractionAsMouse_t2899449918 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unControllerDeviceIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_HandleControllerOverlayInteractionAsMouse::BeginInvoke(System.UInt64,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _HandleControllerOverlayInteractionAsMouse_BeginInvoke_m1836753161 (_HandleControllerOverlayInteractionAsMouse_t2899449918 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unControllerDeviceIndex1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVROverlay/_HandleControllerOverlayInteractionAsMouse::EndInvoke(System.IAsyncResult)
extern "C"  bool _HandleControllerOverlayInteractionAsMouse_EndInvoke_m2773289083 (_HandleControllerOverlayInteractionAsMouse_t2899449918 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
