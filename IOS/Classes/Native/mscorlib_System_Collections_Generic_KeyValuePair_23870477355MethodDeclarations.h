﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23720882578MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Transform,System.Single>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m363603831(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3870477355 *, Transform_t3275118058 *, float, const MethodInfo*))KeyValuePair_2__ctor_m3796742776_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Transform,System.Single>::get_Key()
#define KeyValuePair_2_get_Key_m2569135101(__this, method) ((  Transform_t3275118058 * (*) (KeyValuePair_2_t3870477355 *, const MethodInfo*))KeyValuePair_2_get_Key_m1314509062_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Transform,System.Single>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3419402928(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3870477355 *, Transform_t3275118058 *, const MethodInfo*))KeyValuePair_2_set_Key_m3897037655_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Transform,System.Single>::get_Value()
#define KeyValuePair_2_get_Value_m2548256797(__this, method) ((  float (*) (KeyValuePair_2_t3870477355 *, const MethodInfo*))KeyValuePair_2_get_Value_m253835334_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Transform,System.Single>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2308175688(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3870477355 *, float, const MethodInfo*))KeyValuePair_2_set_Value_m2368197927_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Transform,System.Single>::ToString()
#define KeyValuePair_2_ToString_m947986544(__this, method) ((  String_t* (*) (KeyValuePair_2_t3870477355 *, const MethodInfo*))KeyValuePair_2_ToString_m495261565_gshared)(__this, method)
