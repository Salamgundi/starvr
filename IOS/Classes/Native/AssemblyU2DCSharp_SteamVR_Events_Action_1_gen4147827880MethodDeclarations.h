﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Action`1<System.Object>
struct Action_1_t4147827880;
// SteamVR_Events/Event`1<System.Object>
struct Event_1_t569904416;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t4056035046;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Events/Action`1<System.Object>::.ctor(SteamVR_Events/Event`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Action_1__ctor_m3060490555_gshared (Action_1_t4147827880 * __this, Event_1_t569904416 * ____event0, UnityAction_1_t4056035046 * ___action1, const MethodInfo* method);
#define Action_1__ctor_m3060490555(__this, ____event0, ___action1, method) ((  void (*) (Action_1_t4147827880 *, Event_1_t569904416 *, UnityAction_1_t4056035046 *, const MethodInfo*))Action_1__ctor_m3060490555_gshared)(__this, ____event0, ___action1, method)
// System.Void SteamVR_Events/Action`1<System.Object>::Enable(System.Boolean)
extern "C"  void Action_1_Enable_m3823038047_gshared (Action_1_t4147827880 * __this, bool ___enabled0, const MethodInfo* method);
#define Action_1_Enable_m3823038047(__this, ___enabled0, method) ((  void (*) (Action_1_t4147827880 *, bool, const MethodInfo*))Action_1_Enable_m3823038047_gshared)(__this, ___enabled0, method)
