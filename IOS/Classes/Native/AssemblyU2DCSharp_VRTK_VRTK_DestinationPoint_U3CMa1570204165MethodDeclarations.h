﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_DestinationPoint/<ManageDestinationMarkersAtEndOfFrame>c__Iterator0
struct U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_t1570204165;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_DestinationPoint/<ManageDestinationMarkersAtEndOfFrame>c__Iterator0::.ctor()
extern "C"  void U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0__ctor_m2477119300 (U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_t1570204165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_DestinationPoint/<ManageDestinationMarkersAtEndOfFrame>c__Iterator0::MoveNext()
extern "C"  bool U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_MoveNext_m3071934048 (U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_t1570204165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_DestinationPoint/<ManageDestinationMarkersAtEndOfFrame>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m731386422 (U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_t1570204165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_DestinationPoint/<ManageDestinationMarkersAtEndOfFrame>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1706549662 (U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_t1570204165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint/<ManageDestinationMarkersAtEndOfFrame>c__Iterator0::Dispose()
extern "C"  void U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_Dispose_m1872403983 (U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_t1570204165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint/<ManageDestinationMarkersAtEndOfFrame>c__Iterator0::Reset()
extern "C"  void U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_Reset_m1322366449 (U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_t1570204165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
