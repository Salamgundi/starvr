﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DashTeleportEventArgs>
struct UnityEvent_2_t879939851;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.DashTeleportEventArgs>
struct UnityAction_2_t3292709229;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_VRTK_DashTeleportEventArgs2197253242.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DashTeleportEventArgs>::.ctor()
extern "C"  void UnityEvent_2__ctor_m1384120221_gshared (UnityEvent_2_t879939851 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m1384120221(__this, method) ((  void (*) (UnityEvent_2_t879939851 *, const MethodInfo*))UnityEvent_2__ctor_m1384120221_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DashTeleportEventArgs>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m3094692197_gshared (UnityEvent_2_t879939851 * __this, UnityAction_2_t3292709229 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m3094692197(__this, ___call0, method) ((  void (*) (UnityEvent_2_t879939851 *, UnityAction_2_t3292709229 *, const MethodInfo*))UnityEvent_2_AddListener_m3094692197_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DashTeleportEventArgs>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m903236222_gshared (UnityEvent_2_t879939851 * __this, UnityAction_2_t3292709229 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m903236222(__this, ___call0, method) ((  void (*) (UnityEvent_2_t879939851 *, UnityAction_2_t3292709229 *, const MethodInfo*))UnityEvent_2_RemoveListener_m903236222_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DashTeleportEventArgs>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m3318963229_gshared (UnityEvent_2_t879939851 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m3318963229(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t879939851 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m3318963229_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DashTeleportEventArgs>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m570777701_gshared (UnityEvent_2_t879939851 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m570777701(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t879939851 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m570777701_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DashTeleportEventArgs>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m179335196_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t3292709229 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m179335196(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t3292709229 *, const MethodInfo*))UnityEvent_2_GetDelegate_m179335196_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DashTeleportEventArgs>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m3018722264_gshared (UnityEvent_2_t879939851 * __this, Il2CppObject * ___arg00, DashTeleportEventArgs_t2197253242  ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m3018722264(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t879939851 *, Il2CppObject *, DashTeleportEventArgs_t2197253242 , const MethodInfo*))UnityEvent_2_Invoke_m3018722264_gshared)(__this, ___arg00, ___arg11, method)
