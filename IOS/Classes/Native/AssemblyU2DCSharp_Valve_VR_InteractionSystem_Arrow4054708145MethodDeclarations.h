﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ArrowHand/<ArrowReleaseHaptics>c__Iterator0
struct U3CArrowReleaseHapticsU3Ec__Iterator0_t4054708145;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.ArrowHand/<ArrowReleaseHaptics>c__Iterator0::.ctor()
extern "C"  void U3CArrowReleaseHapticsU3Ec__Iterator0__ctor_m3511232228 (U3CArrowReleaseHapticsU3Ec__Iterator0_t4054708145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.ArrowHand/<ArrowReleaseHaptics>c__Iterator0::MoveNext()
extern "C"  bool U3CArrowReleaseHapticsU3Ec__Iterator0_MoveNext_m892984536 (U3CArrowReleaseHapticsU3Ec__Iterator0_t4054708145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.ArrowHand/<ArrowReleaseHaptics>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CArrowReleaseHapticsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4056009412 (U3CArrowReleaseHapticsU3Ec__Iterator0_t4054708145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.ArrowHand/<ArrowReleaseHaptics>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CArrowReleaseHapticsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m600744972 (U3CArrowReleaseHapticsU3Ec__Iterator0_t4054708145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArrowHand/<ArrowReleaseHaptics>c__Iterator0::Dispose()
extern "C"  void U3CArrowReleaseHapticsU3Ec__Iterator0_Dispose_m2719693411 (U3CArrowReleaseHapticsU3Ec__Iterator0_t4054708145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArrowHand/<ArrowReleaseHaptics>c__Iterator0::Reset()
extern "C"  void U3CArrowReleaseHapticsU3Ec__Iterator0_Reset_m904939469 (U3CArrowReleaseHapticsU3Ec__Iterator0_t4054708145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
