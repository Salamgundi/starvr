﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen535138270.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents/UnityObjectEvent
struct  UnityObjectEvent_t4156405881  : public UnityEvent_2_t535138270
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
