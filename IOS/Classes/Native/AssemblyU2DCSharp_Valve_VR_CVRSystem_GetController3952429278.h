﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.IVRSystem/_GetControllerStateWithPose
struct _GetControllerStateWithPose_t4079915850;
// Valve.VR.CVRSystem/_GetControllerStateWithPosePacked
struct _GetControllerStateWithPosePacked_t15306592;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.CVRSystem/GetControllerStateWithPoseUnion
struct  GetControllerStateWithPoseUnion_t3952429278 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// Valve.VR.IVRSystem/_GetControllerStateWithPose Valve.VR.CVRSystem/GetControllerStateWithPoseUnion::pGetControllerStateWithPose
			_GetControllerStateWithPose_t4079915850 * ___pGetControllerStateWithPose_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			_GetControllerStateWithPose_t4079915850 * ___pGetControllerStateWithPose_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// Valve.VR.CVRSystem/_GetControllerStateWithPosePacked Valve.VR.CVRSystem/GetControllerStateWithPoseUnion::pGetControllerStateWithPosePacked
			_GetControllerStateWithPosePacked_t15306592 * ___pGetControllerStateWithPosePacked_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			_GetControllerStateWithPosePacked_t15306592 * ___pGetControllerStateWithPosePacked_1_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_pGetControllerStateWithPose_0() { return static_cast<int32_t>(offsetof(GetControllerStateWithPoseUnion_t3952429278, ___pGetControllerStateWithPose_0)); }
	inline _GetControllerStateWithPose_t4079915850 * get_pGetControllerStateWithPose_0() const { return ___pGetControllerStateWithPose_0; }
	inline _GetControllerStateWithPose_t4079915850 ** get_address_of_pGetControllerStateWithPose_0() { return &___pGetControllerStateWithPose_0; }
	inline void set_pGetControllerStateWithPose_0(_GetControllerStateWithPose_t4079915850 * value)
	{
		___pGetControllerStateWithPose_0 = value;
		Il2CppCodeGenWriteBarrier(&___pGetControllerStateWithPose_0, value);
	}

	inline static int32_t get_offset_of_pGetControllerStateWithPosePacked_1() { return static_cast<int32_t>(offsetof(GetControllerStateWithPoseUnion_t3952429278, ___pGetControllerStateWithPosePacked_1)); }
	inline _GetControllerStateWithPosePacked_t15306592 * get_pGetControllerStateWithPosePacked_1() const { return ___pGetControllerStateWithPosePacked_1; }
	inline _GetControllerStateWithPosePacked_t15306592 ** get_address_of_pGetControllerStateWithPosePacked_1() { return &___pGetControllerStateWithPosePacked_1; }
	inline void set_pGetControllerStateWithPosePacked_1(_GetControllerStateWithPosePacked_t15306592 * value)
	{
		___pGetControllerStateWithPosePacked_1 = value;
		Il2CppCodeGenWriteBarrier(&___pGetControllerStateWithPosePacked_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Valve.VR.CVRSystem/GetControllerStateWithPoseUnion
struct GetControllerStateWithPoseUnion_t3952429278_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			Il2CppMethodPointer ___pGetControllerStateWithPose_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppMethodPointer ___pGetControllerStateWithPose_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Il2CppMethodPointer ___pGetControllerStateWithPosePacked_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppMethodPointer ___pGetControllerStateWithPosePacked_1_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of Valve.VR.CVRSystem/GetControllerStateWithPoseUnion
struct GetControllerStateWithPoseUnion_t3952429278_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			Il2CppMethodPointer ___pGetControllerStateWithPose_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppMethodPointer ___pGetControllerStateWithPose_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Il2CppMethodPointer ___pGetControllerStateWithPosePacked_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppMethodPointer ___pGetControllerStateWithPosePacked_1_forAlignmentOnly;
		};
	};
};
