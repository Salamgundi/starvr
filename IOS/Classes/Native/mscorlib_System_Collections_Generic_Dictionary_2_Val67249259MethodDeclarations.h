﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2650563081MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2233340255(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t67249259 *, Dictionary_2_t1364189416 *, const MethodInfo*))ValueCollection__ctor_m3284155060_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1926024397(__this, ___item0, method) ((  void (*) (ValueCollection_t67249259 *, ButtonHintInfo_t106135473 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1980768562_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m414357898(__this, method) ((  void (*) (ValueCollection_t67249259 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1741175791_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3231756851(__this, ___item0, method) ((  bool (*) (ValueCollection_t67249259 *, ButtonHintInfo_t106135473 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2187356890_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4215652872(__this, ___item0, method) ((  bool (*) (ValueCollection_t67249259 *, ButtonHintInfo_t106135473 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1161991615_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4089193180(__this, method) ((  Il2CppObject* (*) (ValueCollection_t67249259 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m969365381_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1925696082(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t67249259 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3149111765_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1091675053(__this, method) ((  Il2CppObject * (*) (ValueCollection_t67249259 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1646348086_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1030919492(__this, method) ((  bool (*) (ValueCollection_t67249259 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3606844835_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1646496034(__this, method) ((  bool (*) (ValueCollection_t67249259 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m999719041_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3570697052(__this, method) ((  Il2CppObject * (*) (ValueCollection_t67249259 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1142149105_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m736705314(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t67249259 *, ButtonHintInfoU5BU5D_t1326857836*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3746321839_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1491613815(__this, method) ((  Enumerator_t3050722180  (*) (ValueCollection_t67249259 *, const MethodInfo*))ValueCollection_GetEnumerator_m1206592724_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::get_Count()
#define ValueCollection_get_Count_m1428919550(__this, method) ((  int32_t (*) (ValueCollection_t67249259 *, const MethodInfo*))ValueCollection_get_Count_m1017264669_gshared)(__this, method)
