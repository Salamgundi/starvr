﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0
struct U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::.ctor()
extern "C"  void U3CUpdateTransformDimensionsU3Ec__Iterator0__ctor_m2150590239 (U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::MoveNext()
extern "C"  bool U3CUpdateTransformDimensionsU3Ec__Iterator0_MoveNext_m40448733 (U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUpdateTransformDimensionsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4242580529 (U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUpdateTransformDimensionsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3157981097 (U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::Dispose()
extern "C"  void U3CUpdateTransformDimensionsU3Ec__Iterator0_Dispose_m2235264360 (U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::Reset()
extern "C"  void U3CUpdateTransformDimensionsU3Ec__Iterator0_Reset_m2980108126 (U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
