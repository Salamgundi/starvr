﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerInteractionEventArgs>
struct InvokableCall_2_t1397884410;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerInteractionEventArgs>
struct UnityAction_2_t1383093526;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerInteractionEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m3592476454_gshared (InvokableCall_2_t1397884410 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_2__ctor_m3592476454(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_2_t1397884410 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2__ctor_m3592476454_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerInteractionEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m635840647_gshared (InvokableCall_2_t1397884410 * __this, UnityAction_2_t1383093526 * ___action0, const MethodInfo* method);
#define InvokableCall_2__ctor_m635840647(__this, ___action0, method) ((  void (*) (InvokableCall_2_t1397884410 *, UnityAction_2_t1383093526 *, const MethodInfo*))InvokableCall_2__ctor_m635840647_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerInteractionEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m1566474016_gshared (InvokableCall_2_t1397884410 * __this, UnityAction_2_t1383093526 * ___value0, const MethodInfo* method);
#define InvokableCall_2_add_Delegate_m1566474016(__this, ___value0, method) ((  void (*) (InvokableCall_2_t1397884410 *, UnityAction_2_t1383093526 *, const MethodInfo*))InvokableCall_2_add_Delegate_m1566474016_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerInteractionEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m3555528609_gshared (InvokableCall_2_t1397884410 * __this, UnityAction_2_t1383093526 * ___value0, const MethodInfo* method);
#define InvokableCall_2_remove_Delegate_m3555528609(__this, ___value0, method) ((  void (*) (InvokableCall_2_t1397884410 *, UnityAction_2_t1383093526 *, const MethodInfo*))InvokableCall_2_remove_Delegate_m3555528609_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerInteractionEventArgs>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m2282007067_gshared (InvokableCall_2_t1397884410 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_2_Invoke_m2282007067(__this, ___args0, method) ((  void (*) (InvokableCall_2_t1397884410 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_2_Invoke_m2282007067_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerInteractionEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m1124626283_gshared (InvokableCall_2_t1397884410 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_2_Find_m1124626283(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_2_t1397884410 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2_Find_m1124626283_gshared)(__this, ___targetObj0, ___method1, method)
