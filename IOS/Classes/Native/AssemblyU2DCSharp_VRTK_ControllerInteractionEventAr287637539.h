﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.ControllerInteractionEventArgs
struct  ControllerInteractionEventArgs_t287637539 
{
public:
	// System.UInt32 VRTK.ControllerInteractionEventArgs::controllerIndex
	uint32_t ___controllerIndex_0;
	// System.Single VRTK.ControllerInteractionEventArgs::buttonPressure
	float ___buttonPressure_1;
	// UnityEngine.Vector2 VRTK.ControllerInteractionEventArgs::touchpadAxis
	Vector2_t2243707579  ___touchpadAxis_2;
	// System.Single VRTK.ControllerInteractionEventArgs::touchpadAngle
	float ___touchpadAngle_3;

public:
	inline static int32_t get_offset_of_controllerIndex_0() { return static_cast<int32_t>(offsetof(ControllerInteractionEventArgs_t287637539, ___controllerIndex_0)); }
	inline uint32_t get_controllerIndex_0() const { return ___controllerIndex_0; }
	inline uint32_t* get_address_of_controllerIndex_0() { return &___controllerIndex_0; }
	inline void set_controllerIndex_0(uint32_t value)
	{
		___controllerIndex_0 = value;
	}

	inline static int32_t get_offset_of_buttonPressure_1() { return static_cast<int32_t>(offsetof(ControllerInteractionEventArgs_t287637539, ___buttonPressure_1)); }
	inline float get_buttonPressure_1() const { return ___buttonPressure_1; }
	inline float* get_address_of_buttonPressure_1() { return &___buttonPressure_1; }
	inline void set_buttonPressure_1(float value)
	{
		___buttonPressure_1 = value;
	}

	inline static int32_t get_offset_of_touchpadAxis_2() { return static_cast<int32_t>(offsetof(ControllerInteractionEventArgs_t287637539, ___touchpadAxis_2)); }
	inline Vector2_t2243707579  get_touchpadAxis_2() const { return ___touchpadAxis_2; }
	inline Vector2_t2243707579 * get_address_of_touchpadAxis_2() { return &___touchpadAxis_2; }
	inline void set_touchpadAxis_2(Vector2_t2243707579  value)
	{
		___touchpadAxis_2 = value;
	}

	inline static int32_t get_offset_of_touchpadAngle_3() { return static_cast<int32_t>(offsetof(ControllerInteractionEventArgs_t287637539, ___touchpadAngle_3)); }
	inline float get_touchpadAngle_3() const { return ___touchpadAngle_3; }
	inline float* get_address_of_touchpadAngle_3() { return &___touchpadAngle_3; }
	inline void set_touchpadAngle_3(float value)
	{
		___touchpadAngle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
