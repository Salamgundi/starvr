﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_SetSkyboxOverride
struct _SetSkyboxOverride_t2590693784;
// System.Object
struct Il2CppObject;
// Valve.VR.Texture_t[]
struct Texture_tU5BU5D_t3142294487;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRCompositorError3948578210.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_SetSkyboxOverride::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetSkyboxOverride__ctor_m1304892293 (_SetSkyboxOverride_t2590693784 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.IVRCompositor/_SetSkyboxOverride::Invoke(Valve.VR.Texture_t[],System.UInt32)
extern "C"  int32_t _SetSkyboxOverride_Invoke_m753399040 (_SetSkyboxOverride_t2590693784 * __this, Texture_tU5BU5D_t3142294487* ___pTextures0, uint32_t ___unTextureCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_SetSkyboxOverride::BeginInvoke(Valve.VR.Texture_t[],System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetSkyboxOverride_BeginInvoke_m2386420818 (_SetSkyboxOverride_t2590693784 * __this, Texture_tU5BU5D_t3142294487* ___pTextures0, uint32_t ___unTextureCount1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.IVRCompositor/_SetSkyboxOverride::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetSkyboxOverride_EndInvoke_m2527182342 (_SetSkyboxOverride_t2590693784 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
