﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.UIPointerEventHandler
struct UIPointerEventHandler_t988663103;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// VRTK.VRTK_VRInputModule
struct VRTK_VRInputModule_t1472500726;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer_ActivationMe3611442703.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer_ClickMethods1099240745.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_UIPointer
struct  VRTK_UIPointer_t2714926455  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_UIPointer::activationButton
	int32_t ___activationButton_2;
	// VRTK.VRTK_UIPointer/ActivationMethods VRTK.VRTK_UIPointer::activationMode
	int32_t ___activationMode_3;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_UIPointer::selectionButton
	int32_t ___selectionButton_4;
	// VRTK.VRTK_UIPointer/ClickMethods VRTK.VRTK_UIPointer::clickMethod
	int32_t ___clickMethod_5;
	// System.Boolean VRTK.VRTK_UIPointer::attemptClickOnDeactivate
	bool ___attemptClickOnDeactivate_6;
	// System.Single VRTK.VRTK_UIPointer::clickAfterHoverDuration
	float ___clickAfterHoverDuration_7;
	// VRTK.VRTK_ControllerEvents VRTK.VRTK_UIPointer::controller
	VRTK_ControllerEvents_t3225224819 * ___controller_8;
	// UnityEngine.Transform VRTK.VRTK_UIPointer::pointerOriginTransform
	Transform_t3275118058 * ___pointerOriginTransform_9;
	// UnityEngine.EventSystems.PointerEventData VRTK.VRTK_UIPointer::pointerEventData
	PointerEventData_t1599784723 * ___pointerEventData_10;
	// UnityEngine.GameObject VRTK.VRTK_UIPointer::hoveringElement
	GameObject_t1756533147 * ___hoveringElement_11;
	// UnityEngine.GameObject VRTK.VRTK_UIPointer::controllerRenderModel
	GameObject_t1756533147 * ___controllerRenderModel_12;
	// System.Single VRTK.VRTK_UIPointer::hoverDurationTimer
	float ___hoverDurationTimer_13;
	// System.Boolean VRTK.VRTK_UIPointer::canClickOnHover
	bool ___canClickOnHover_14;
	// UnityEngine.GameObject VRTK.VRTK_UIPointer::autoActivatingCanvas
	GameObject_t1756533147 * ___autoActivatingCanvas_15;
	// System.Boolean VRTK.VRTK_UIPointer::collisionClick
	bool ___collisionClick_16;
	// VRTK.UIPointerEventHandler VRTK.VRTK_UIPointer::UIPointerElementEnter
	UIPointerEventHandler_t988663103 * ___UIPointerElementEnter_17;
	// VRTK.UIPointerEventHandler VRTK.VRTK_UIPointer::UIPointerElementExit
	UIPointerEventHandler_t988663103 * ___UIPointerElementExit_18;
	// VRTK.UIPointerEventHandler VRTK.VRTK_UIPointer::UIPointerElementClick
	UIPointerEventHandler_t988663103 * ___UIPointerElementClick_19;
	// VRTK.UIPointerEventHandler VRTK.VRTK_UIPointer::UIPointerElementDragStart
	UIPointerEventHandler_t988663103 * ___UIPointerElementDragStart_20;
	// VRTK.UIPointerEventHandler VRTK.VRTK_UIPointer::UIPointerElementDragEnd
	UIPointerEventHandler_t988663103 * ___UIPointerElementDragEnd_21;
	// System.Boolean VRTK.VRTK_UIPointer::pointerClicked
	bool ___pointerClicked_22;
	// System.Boolean VRTK.VRTK_UIPointer::beamEnabledState
	bool ___beamEnabledState_23;
	// System.Boolean VRTK.VRTK_UIPointer::lastPointerPressState
	bool ___lastPointerPressState_24;
	// System.Boolean VRTK.VRTK_UIPointer::lastPointerClickState
	bool ___lastPointerClickState_25;
	// UnityEngine.GameObject VRTK.VRTK_UIPointer::currentTarget
	GameObject_t1756533147 * ___currentTarget_26;
	// UnityEngine.EventSystems.EventSystem VRTK.VRTK_UIPointer::cachedEventSystem
	EventSystem_t3466835263 * ___cachedEventSystem_27;
	// VRTK.VRTK_VRInputModule VRTK.VRTK_UIPointer::cachedVRInputModule
	VRTK_VRInputModule_t1472500726 * ___cachedVRInputModule_28;

public:
	inline static int32_t get_offset_of_activationButton_2() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___activationButton_2)); }
	inline int32_t get_activationButton_2() const { return ___activationButton_2; }
	inline int32_t* get_address_of_activationButton_2() { return &___activationButton_2; }
	inline void set_activationButton_2(int32_t value)
	{
		___activationButton_2 = value;
	}

	inline static int32_t get_offset_of_activationMode_3() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___activationMode_3)); }
	inline int32_t get_activationMode_3() const { return ___activationMode_3; }
	inline int32_t* get_address_of_activationMode_3() { return &___activationMode_3; }
	inline void set_activationMode_3(int32_t value)
	{
		___activationMode_3 = value;
	}

	inline static int32_t get_offset_of_selectionButton_4() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___selectionButton_4)); }
	inline int32_t get_selectionButton_4() const { return ___selectionButton_4; }
	inline int32_t* get_address_of_selectionButton_4() { return &___selectionButton_4; }
	inline void set_selectionButton_4(int32_t value)
	{
		___selectionButton_4 = value;
	}

	inline static int32_t get_offset_of_clickMethod_5() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___clickMethod_5)); }
	inline int32_t get_clickMethod_5() const { return ___clickMethod_5; }
	inline int32_t* get_address_of_clickMethod_5() { return &___clickMethod_5; }
	inline void set_clickMethod_5(int32_t value)
	{
		___clickMethod_5 = value;
	}

	inline static int32_t get_offset_of_attemptClickOnDeactivate_6() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___attemptClickOnDeactivate_6)); }
	inline bool get_attemptClickOnDeactivate_6() const { return ___attemptClickOnDeactivate_6; }
	inline bool* get_address_of_attemptClickOnDeactivate_6() { return &___attemptClickOnDeactivate_6; }
	inline void set_attemptClickOnDeactivate_6(bool value)
	{
		___attemptClickOnDeactivate_6 = value;
	}

	inline static int32_t get_offset_of_clickAfterHoverDuration_7() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___clickAfterHoverDuration_7)); }
	inline float get_clickAfterHoverDuration_7() const { return ___clickAfterHoverDuration_7; }
	inline float* get_address_of_clickAfterHoverDuration_7() { return &___clickAfterHoverDuration_7; }
	inline void set_clickAfterHoverDuration_7(float value)
	{
		___clickAfterHoverDuration_7 = value;
	}

	inline static int32_t get_offset_of_controller_8() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___controller_8)); }
	inline VRTK_ControllerEvents_t3225224819 * get_controller_8() const { return ___controller_8; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_controller_8() { return &___controller_8; }
	inline void set_controller_8(VRTK_ControllerEvents_t3225224819 * value)
	{
		___controller_8 = value;
		Il2CppCodeGenWriteBarrier(&___controller_8, value);
	}

	inline static int32_t get_offset_of_pointerOriginTransform_9() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___pointerOriginTransform_9)); }
	inline Transform_t3275118058 * get_pointerOriginTransform_9() const { return ___pointerOriginTransform_9; }
	inline Transform_t3275118058 ** get_address_of_pointerOriginTransform_9() { return &___pointerOriginTransform_9; }
	inline void set_pointerOriginTransform_9(Transform_t3275118058 * value)
	{
		___pointerOriginTransform_9 = value;
		Il2CppCodeGenWriteBarrier(&___pointerOriginTransform_9, value);
	}

	inline static int32_t get_offset_of_pointerEventData_10() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___pointerEventData_10)); }
	inline PointerEventData_t1599784723 * get_pointerEventData_10() const { return ___pointerEventData_10; }
	inline PointerEventData_t1599784723 ** get_address_of_pointerEventData_10() { return &___pointerEventData_10; }
	inline void set_pointerEventData_10(PointerEventData_t1599784723 * value)
	{
		___pointerEventData_10 = value;
		Il2CppCodeGenWriteBarrier(&___pointerEventData_10, value);
	}

	inline static int32_t get_offset_of_hoveringElement_11() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___hoveringElement_11)); }
	inline GameObject_t1756533147 * get_hoveringElement_11() const { return ___hoveringElement_11; }
	inline GameObject_t1756533147 ** get_address_of_hoveringElement_11() { return &___hoveringElement_11; }
	inline void set_hoveringElement_11(GameObject_t1756533147 * value)
	{
		___hoveringElement_11 = value;
		Il2CppCodeGenWriteBarrier(&___hoveringElement_11, value);
	}

	inline static int32_t get_offset_of_controllerRenderModel_12() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___controllerRenderModel_12)); }
	inline GameObject_t1756533147 * get_controllerRenderModel_12() const { return ___controllerRenderModel_12; }
	inline GameObject_t1756533147 ** get_address_of_controllerRenderModel_12() { return &___controllerRenderModel_12; }
	inline void set_controllerRenderModel_12(GameObject_t1756533147 * value)
	{
		___controllerRenderModel_12 = value;
		Il2CppCodeGenWriteBarrier(&___controllerRenderModel_12, value);
	}

	inline static int32_t get_offset_of_hoverDurationTimer_13() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___hoverDurationTimer_13)); }
	inline float get_hoverDurationTimer_13() const { return ___hoverDurationTimer_13; }
	inline float* get_address_of_hoverDurationTimer_13() { return &___hoverDurationTimer_13; }
	inline void set_hoverDurationTimer_13(float value)
	{
		___hoverDurationTimer_13 = value;
	}

	inline static int32_t get_offset_of_canClickOnHover_14() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___canClickOnHover_14)); }
	inline bool get_canClickOnHover_14() const { return ___canClickOnHover_14; }
	inline bool* get_address_of_canClickOnHover_14() { return &___canClickOnHover_14; }
	inline void set_canClickOnHover_14(bool value)
	{
		___canClickOnHover_14 = value;
	}

	inline static int32_t get_offset_of_autoActivatingCanvas_15() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___autoActivatingCanvas_15)); }
	inline GameObject_t1756533147 * get_autoActivatingCanvas_15() const { return ___autoActivatingCanvas_15; }
	inline GameObject_t1756533147 ** get_address_of_autoActivatingCanvas_15() { return &___autoActivatingCanvas_15; }
	inline void set_autoActivatingCanvas_15(GameObject_t1756533147 * value)
	{
		___autoActivatingCanvas_15 = value;
		Il2CppCodeGenWriteBarrier(&___autoActivatingCanvas_15, value);
	}

	inline static int32_t get_offset_of_collisionClick_16() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___collisionClick_16)); }
	inline bool get_collisionClick_16() const { return ___collisionClick_16; }
	inline bool* get_address_of_collisionClick_16() { return &___collisionClick_16; }
	inline void set_collisionClick_16(bool value)
	{
		___collisionClick_16 = value;
	}

	inline static int32_t get_offset_of_UIPointerElementEnter_17() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___UIPointerElementEnter_17)); }
	inline UIPointerEventHandler_t988663103 * get_UIPointerElementEnter_17() const { return ___UIPointerElementEnter_17; }
	inline UIPointerEventHandler_t988663103 ** get_address_of_UIPointerElementEnter_17() { return &___UIPointerElementEnter_17; }
	inline void set_UIPointerElementEnter_17(UIPointerEventHandler_t988663103 * value)
	{
		___UIPointerElementEnter_17 = value;
		Il2CppCodeGenWriteBarrier(&___UIPointerElementEnter_17, value);
	}

	inline static int32_t get_offset_of_UIPointerElementExit_18() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___UIPointerElementExit_18)); }
	inline UIPointerEventHandler_t988663103 * get_UIPointerElementExit_18() const { return ___UIPointerElementExit_18; }
	inline UIPointerEventHandler_t988663103 ** get_address_of_UIPointerElementExit_18() { return &___UIPointerElementExit_18; }
	inline void set_UIPointerElementExit_18(UIPointerEventHandler_t988663103 * value)
	{
		___UIPointerElementExit_18 = value;
		Il2CppCodeGenWriteBarrier(&___UIPointerElementExit_18, value);
	}

	inline static int32_t get_offset_of_UIPointerElementClick_19() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___UIPointerElementClick_19)); }
	inline UIPointerEventHandler_t988663103 * get_UIPointerElementClick_19() const { return ___UIPointerElementClick_19; }
	inline UIPointerEventHandler_t988663103 ** get_address_of_UIPointerElementClick_19() { return &___UIPointerElementClick_19; }
	inline void set_UIPointerElementClick_19(UIPointerEventHandler_t988663103 * value)
	{
		___UIPointerElementClick_19 = value;
		Il2CppCodeGenWriteBarrier(&___UIPointerElementClick_19, value);
	}

	inline static int32_t get_offset_of_UIPointerElementDragStart_20() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___UIPointerElementDragStart_20)); }
	inline UIPointerEventHandler_t988663103 * get_UIPointerElementDragStart_20() const { return ___UIPointerElementDragStart_20; }
	inline UIPointerEventHandler_t988663103 ** get_address_of_UIPointerElementDragStart_20() { return &___UIPointerElementDragStart_20; }
	inline void set_UIPointerElementDragStart_20(UIPointerEventHandler_t988663103 * value)
	{
		___UIPointerElementDragStart_20 = value;
		Il2CppCodeGenWriteBarrier(&___UIPointerElementDragStart_20, value);
	}

	inline static int32_t get_offset_of_UIPointerElementDragEnd_21() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___UIPointerElementDragEnd_21)); }
	inline UIPointerEventHandler_t988663103 * get_UIPointerElementDragEnd_21() const { return ___UIPointerElementDragEnd_21; }
	inline UIPointerEventHandler_t988663103 ** get_address_of_UIPointerElementDragEnd_21() { return &___UIPointerElementDragEnd_21; }
	inline void set_UIPointerElementDragEnd_21(UIPointerEventHandler_t988663103 * value)
	{
		___UIPointerElementDragEnd_21 = value;
		Il2CppCodeGenWriteBarrier(&___UIPointerElementDragEnd_21, value);
	}

	inline static int32_t get_offset_of_pointerClicked_22() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___pointerClicked_22)); }
	inline bool get_pointerClicked_22() const { return ___pointerClicked_22; }
	inline bool* get_address_of_pointerClicked_22() { return &___pointerClicked_22; }
	inline void set_pointerClicked_22(bool value)
	{
		___pointerClicked_22 = value;
	}

	inline static int32_t get_offset_of_beamEnabledState_23() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___beamEnabledState_23)); }
	inline bool get_beamEnabledState_23() const { return ___beamEnabledState_23; }
	inline bool* get_address_of_beamEnabledState_23() { return &___beamEnabledState_23; }
	inline void set_beamEnabledState_23(bool value)
	{
		___beamEnabledState_23 = value;
	}

	inline static int32_t get_offset_of_lastPointerPressState_24() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___lastPointerPressState_24)); }
	inline bool get_lastPointerPressState_24() const { return ___lastPointerPressState_24; }
	inline bool* get_address_of_lastPointerPressState_24() { return &___lastPointerPressState_24; }
	inline void set_lastPointerPressState_24(bool value)
	{
		___lastPointerPressState_24 = value;
	}

	inline static int32_t get_offset_of_lastPointerClickState_25() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___lastPointerClickState_25)); }
	inline bool get_lastPointerClickState_25() const { return ___lastPointerClickState_25; }
	inline bool* get_address_of_lastPointerClickState_25() { return &___lastPointerClickState_25; }
	inline void set_lastPointerClickState_25(bool value)
	{
		___lastPointerClickState_25 = value;
	}

	inline static int32_t get_offset_of_currentTarget_26() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___currentTarget_26)); }
	inline GameObject_t1756533147 * get_currentTarget_26() const { return ___currentTarget_26; }
	inline GameObject_t1756533147 ** get_address_of_currentTarget_26() { return &___currentTarget_26; }
	inline void set_currentTarget_26(GameObject_t1756533147 * value)
	{
		___currentTarget_26 = value;
		Il2CppCodeGenWriteBarrier(&___currentTarget_26, value);
	}

	inline static int32_t get_offset_of_cachedEventSystem_27() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___cachedEventSystem_27)); }
	inline EventSystem_t3466835263 * get_cachedEventSystem_27() const { return ___cachedEventSystem_27; }
	inline EventSystem_t3466835263 ** get_address_of_cachedEventSystem_27() { return &___cachedEventSystem_27; }
	inline void set_cachedEventSystem_27(EventSystem_t3466835263 * value)
	{
		___cachedEventSystem_27 = value;
		Il2CppCodeGenWriteBarrier(&___cachedEventSystem_27, value);
	}

	inline static int32_t get_offset_of_cachedVRInputModule_28() { return static_cast<int32_t>(offsetof(VRTK_UIPointer_t2714926455, ___cachedVRInputModule_28)); }
	inline VRTK_VRInputModule_t1472500726 * get_cachedVRInputModule_28() const { return ___cachedVRInputModule_28; }
	inline VRTK_VRInputModule_t1472500726 ** get_address_of_cachedVRInputModule_28() { return &___cachedVRInputModule_28; }
	inline void set_cachedVRInputModule_28(VRTK_VRInputModule_t1472500726 * value)
	{
		___cachedVRInputModule_28 = value;
		Il2CppCodeGenWriteBarrier(&___cachedVRInputModule_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
