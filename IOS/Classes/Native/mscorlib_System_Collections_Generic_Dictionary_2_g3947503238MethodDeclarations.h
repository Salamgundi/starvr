﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>
struct Dictionary_2_t3947503238;
// System.Collections.Generic.IEqualityComparer`1<Valve.VR.EVRButtonId>
struct IEqualityComparer_1_t3573745486;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>[]
struct KeyValuePair_2U5BU5D_t4023991557;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>>
struct IEnumerator_1_t3475339583;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>
struct ValueCollection_t2650563081;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21704848460.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRButtonId66145412.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En972560644.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m1286095967_gshared (Dictionary_2_t3947503238 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1286095967(__this, method) ((  void (*) (Dictionary_2_t3947503238 *, const MethodInfo*))Dictionary_2__ctor_m1286095967_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1823091778_gshared (Dictionary_2_t3947503238 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1823091778(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3947503238 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1823091778_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m562912162_gshared (Dictionary_2_t3947503238 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m562912162(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3947503238 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m562912162_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m3690141588_gshared (Dictionary_2_t3947503238 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m3690141588(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3947503238 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m3690141588_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m198087563_gshared (Dictionary_2_t3947503238 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m198087563(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3947503238 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m198087563_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m997103232_gshared (Dictionary_2_t3947503238 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m997103232(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3947503238 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m997103232_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m2469536369_gshared (Dictionary_2_t3947503238 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m2469536369(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3947503238 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2469536369_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m418195236_gshared (Dictionary_2_t3947503238 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m418195236(__this, ___key0, method) ((  void (*) (Dictionary_2_t3947503238 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m418195236_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m636468395_gshared (Dictionary_2_t3947503238 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m636468395(__this, method) ((  bool (*) (Dictionary_2_t3947503238 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m636468395_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3782074543_gshared (Dictionary_2_t3947503238 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3782074543(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3947503238 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3782074543_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2708174521_gshared (Dictionary_2_t3947503238 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2708174521(__this, method) ((  bool (*) (Dictionary_2_t3947503238 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2708174521_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1480046902_gshared (Dictionary_2_t3947503238 * __this, KeyValuePair_2_t1704848460  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1480046902(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3947503238 *, KeyValuePair_2_t1704848460 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1480046902_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m185902386_gshared (Dictionary_2_t3947503238 * __this, KeyValuePair_2_t1704848460  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m185902386(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3947503238 *, KeyValuePair_2_t1704848460 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m185902386_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4078990706_gshared (Dictionary_2_t3947503238 * __this, KeyValuePair_2U5BU5D_t4023991557* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4078990706(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3947503238 *, KeyValuePair_2U5BU5D_t4023991557*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4078990706_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m980359839_gshared (Dictionary_2_t3947503238 * __this, KeyValuePair_2_t1704848460  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m980359839(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3947503238 *, KeyValuePair_2_t1704848460 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m980359839_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1550097683_gshared (Dictionary_2_t3947503238 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1550097683(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3947503238 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1550097683_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1619596364_gshared (Dictionary_2_t3947503238 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1619596364(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3947503238 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1619596364_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m611581641_gshared (Dictionary_2_t3947503238 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m611581641(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3947503238 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m611581641_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1904745622_gshared (Dictionary_2_t3947503238 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1904745622(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3947503238 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1904745622_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2275193491_gshared (Dictionary_2_t3947503238 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2275193491(__this, method) ((  int32_t (*) (Dictionary_2_t3947503238 *, const MethodInfo*))Dictionary_2_get_Count_m2275193491_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m252799234_gshared (Dictionary_2_t3947503238 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m252799234(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3947503238 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m252799234_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m2256415207_gshared (Dictionary_2_t3947503238 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m2256415207(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3947503238 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m2256415207_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2343338959_gshared (Dictionary_2_t3947503238 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2343338959(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3947503238 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2343338959_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m4132604134_gshared (Dictionary_2_t3947503238 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m4132604134(__this, ___size0, method) ((  void (*) (Dictionary_2_t3947503238 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m4132604134_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3120157996_gshared (Dictionary_2_t3947503238 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3120157996(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3947503238 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3120157996_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1704848460  Dictionary_2_make_pair_m1551931734_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1551931734(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1704848460  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m1551931734_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m1274556520_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1274556520(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m1274556520_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1578521787_gshared (Dictionary_2_t3947503238 * __this, KeyValuePair_2U5BU5D_t4023991557* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1578521787(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3947503238 *, KeyValuePair_2U5BU5D_t4023991557*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1578521787_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m2455928609_gshared (Dictionary_2_t3947503238 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m2455928609(__this, method) ((  void (*) (Dictionary_2_t3947503238 *, const MethodInfo*))Dictionary_2_Resize_m2455928609_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1260293366_gshared (Dictionary_2_t3947503238 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1260293366(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3947503238 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m1260293366_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2993884854_gshared (Dictionary_2_t3947503238 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2993884854(__this, method) ((  void (*) (Dictionary_2_t3947503238 *, const MethodInfo*))Dictionary_2_Clear_m2993884854_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2467580546_gshared (Dictionary_2_t3947503238 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2467580546(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3947503238 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2467580546_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1806080970_gshared (Dictionary_2_t3947503238 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1806080970(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3947503238 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m1806080970_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m4001917175_gshared (Dictionary_2_t3947503238 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m4001917175(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3947503238 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m4001917175_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2143783311_gshared (Dictionary_2_t3947503238 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2143783311(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3947503238 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2143783311_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m933765762_gshared (Dictionary_2_t3947503238 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m933765762(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3947503238 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m933765762_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2305544871_gshared (Dictionary_2_t3947503238 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2305544871(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3947503238 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m2305544871_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::get_Values()
extern "C"  ValueCollection_t2650563081 * Dictionary_2_get_Values_m4135797354_gshared (Dictionary_2_t3947503238 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m4135797354(__this, method) ((  ValueCollection_t2650563081 * (*) (Dictionary_2_t3947503238 *, const MethodInfo*))Dictionary_2_get_Values_m4135797354_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m3888699241_gshared (Dictionary_2_t3947503238 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3888699241(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3947503238 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3888699241_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1035498505_gshared (Dictionary_2_t3947503238 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1035498505(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t3947503238 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1035498505_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2991871191_gshared (Dictionary_2_t3947503238 * __this, KeyValuePair_2_t1704848460  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2991871191(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3947503238 *, KeyValuePair_2_t1704848460 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2991871191_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::GetEnumerator()
extern "C"  Enumerator_t972560644  Dictionary_2_GetEnumerator_m4064032532_gshared (Dictionary_2_t3947503238 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m4064032532(__this, method) ((  Enumerator_t972560644  (*) (Dictionary_2_t3947503238 *, const MethodInfo*))Dictionary_2_GetEnumerator_m4064032532_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m4084034777_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m4084034777(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m4084034777_gshared)(__this /* static, unused */, ___key0, ___value1, method)
