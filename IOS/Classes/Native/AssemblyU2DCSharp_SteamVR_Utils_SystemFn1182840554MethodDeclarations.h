﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Utils/SystemFn
struct SystemFn_t1182840554;
// System.Object
struct Il2CppObject;
// Valve.VR.CVRSystem
struct CVRSystem_t1953699154;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRSystem1953699154.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void SteamVR_Utils/SystemFn::.ctor(System.Object,System.IntPtr)
extern "C"  void SystemFn__ctor_m2986527939 (SystemFn_t1182840554 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SteamVR_Utils/SystemFn::Invoke(Valve.VR.CVRSystem,System.Object[])
extern "C"  Il2CppObject * SystemFn_Invoke_m909308102 (SystemFn_t1182840554 * __this, CVRSystem_t1953699154 * ___system0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult SteamVR_Utils/SystemFn::BeginInvoke(Valve.VR.CVRSystem,System.Object[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SystemFn_BeginInvoke_m2335054318 (SystemFn_t1182840554 * __this, CVRSystem_t1953699154 * ___system0, ObjectU5BU5D_t3614634134* ___args1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SteamVR_Utils/SystemFn::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * SystemFn_EndInvoke_m4032352922 (SystemFn_t1182840554 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
