﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_ClearSkyboxOverride
struct _ClearSkyboxOverride_t745528287;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_ClearSkyboxOverride::.ctor(System.Object,System.IntPtr)
extern "C"  void _ClearSkyboxOverride__ctor_m3149925340 (_ClearSkyboxOverride_t745528287 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_ClearSkyboxOverride::Invoke()
extern "C"  void _ClearSkyboxOverride_Invoke_m845027622 (_ClearSkyboxOverride_t745528287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_ClearSkyboxOverride::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ClearSkyboxOverride_BeginInvoke_m3078278737 (_ClearSkyboxOverride_t745528287 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_ClearSkyboxOverride::EndInvoke(System.IAsyncResult)
extern "C"  void _ClearSkyboxOverride_EndInvoke_m686576234 (_ClearSkyboxOverride_t745528287 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
