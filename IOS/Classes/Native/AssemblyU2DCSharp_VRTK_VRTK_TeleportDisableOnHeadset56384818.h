﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_BasicTeleport
struct VRTK_BasicTeleport_t3532761337;
// VRTK.VRTK_HeadsetCollision
struct VRTK_HeadsetCollision_t2015187094;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_TeleportDisableOnHeadsetCollision
struct  VRTK_TeleportDisableOnHeadsetCollision_t56384818  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_BasicTeleport VRTK.VRTK_TeleportDisableOnHeadsetCollision::basicTeleport
	VRTK_BasicTeleport_t3532761337 * ___basicTeleport_2;
	// VRTK.VRTK_HeadsetCollision VRTK.VRTK_TeleportDisableOnHeadsetCollision::headsetCollision
	VRTK_HeadsetCollision_t2015187094 * ___headsetCollision_3;

public:
	inline static int32_t get_offset_of_basicTeleport_2() { return static_cast<int32_t>(offsetof(VRTK_TeleportDisableOnHeadsetCollision_t56384818, ___basicTeleport_2)); }
	inline VRTK_BasicTeleport_t3532761337 * get_basicTeleport_2() const { return ___basicTeleport_2; }
	inline VRTK_BasicTeleport_t3532761337 ** get_address_of_basicTeleport_2() { return &___basicTeleport_2; }
	inline void set_basicTeleport_2(VRTK_BasicTeleport_t3532761337 * value)
	{
		___basicTeleport_2 = value;
		Il2CppCodeGenWriteBarrier(&___basicTeleport_2, value);
	}

	inline static int32_t get_offset_of_headsetCollision_3() { return static_cast<int32_t>(offsetof(VRTK_TeleportDisableOnHeadsetCollision_t56384818, ___headsetCollision_3)); }
	inline VRTK_HeadsetCollision_t2015187094 * get_headsetCollision_3() const { return ___headsetCollision_3; }
	inline VRTK_HeadsetCollision_t2015187094 ** get_address_of_headsetCollision_3() { return &___headsetCollision_3; }
	inline void set_headsetCollision_3(VRTK_HeadsetCollision_t2015187094 * value)
	{
		___headsetCollision_3 = value;
		Il2CppCodeGenWriteBarrier(&___headsetCollision_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
