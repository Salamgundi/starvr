﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_ForceReconnectProcess
struct _ForceReconnectProcess_t3057442189;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_ForceReconnectProcess::.ctor(System.Object,System.IntPtr)
extern "C"  void _ForceReconnectProcess__ctor_m1135829802 (_ForceReconnectProcess_t3057442189 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_ForceReconnectProcess::Invoke()
extern "C"  void _ForceReconnectProcess_Invoke_m3634485360 (_ForceReconnectProcess_t3057442189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_ForceReconnectProcess::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ForceReconnectProcess_BeginInvoke_m1118120835 (_ForceReconnectProcess_t3057442189 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_ForceReconnectProcess::EndInvoke(System.IAsyncResult)
extern "C"  void _ForceReconnectProcess_EndInvoke_m4020160864 (_ForceReconnectProcess_t3057442189 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
