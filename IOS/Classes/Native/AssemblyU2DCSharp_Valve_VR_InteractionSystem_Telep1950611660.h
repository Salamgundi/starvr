﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;

#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Telep1112706968.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.TeleportArea
struct  TeleportArea_t1950611660  : public TeleportMarkerBase_t1112706968
{
public:
	// UnityEngine.Bounds Valve.VR.InteractionSystem.TeleportArea::<meshBounds>k__BackingField
	Bounds_t3033363703  ___U3CmeshBoundsU3Ek__BackingField_4;
	// UnityEngine.MeshRenderer Valve.VR.InteractionSystem.TeleportArea::areaMesh
	MeshRenderer_t1268241104 * ___areaMesh_5;
	// System.Int32 Valve.VR.InteractionSystem.TeleportArea::tintColorId
	int32_t ___tintColorId_6;
	// UnityEngine.Color Valve.VR.InteractionSystem.TeleportArea::visibleTintColor
	Color_t2020392075  ___visibleTintColor_7;
	// UnityEngine.Color Valve.VR.InteractionSystem.TeleportArea::highlightedTintColor
	Color_t2020392075  ___highlightedTintColor_8;
	// UnityEngine.Color Valve.VR.InteractionSystem.TeleportArea::lockedTintColor
	Color_t2020392075  ___lockedTintColor_9;
	// System.Boolean Valve.VR.InteractionSystem.TeleportArea::highlighted
	bool ___highlighted_10;

public:
	inline static int32_t get_offset_of_U3CmeshBoundsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TeleportArea_t1950611660, ___U3CmeshBoundsU3Ek__BackingField_4)); }
	inline Bounds_t3033363703  get_U3CmeshBoundsU3Ek__BackingField_4() const { return ___U3CmeshBoundsU3Ek__BackingField_4; }
	inline Bounds_t3033363703 * get_address_of_U3CmeshBoundsU3Ek__BackingField_4() { return &___U3CmeshBoundsU3Ek__BackingField_4; }
	inline void set_U3CmeshBoundsU3Ek__BackingField_4(Bounds_t3033363703  value)
	{
		___U3CmeshBoundsU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_areaMesh_5() { return static_cast<int32_t>(offsetof(TeleportArea_t1950611660, ___areaMesh_5)); }
	inline MeshRenderer_t1268241104 * get_areaMesh_5() const { return ___areaMesh_5; }
	inline MeshRenderer_t1268241104 ** get_address_of_areaMesh_5() { return &___areaMesh_5; }
	inline void set_areaMesh_5(MeshRenderer_t1268241104 * value)
	{
		___areaMesh_5 = value;
		Il2CppCodeGenWriteBarrier(&___areaMesh_5, value);
	}

	inline static int32_t get_offset_of_tintColorId_6() { return static_cast<int32_t>(offsetof(TeleportArea_t1950611660, ___tintColorId_6)); }
	inline int32_t get_tintColorId_6() const { return ___tintColorId_6; }
	inline int32_t* get_address_of_tintColorId_6() { return &___tintColorId_6; }
	inline void set_tintColorId_6(int32_t value)
	{
		___tintColorId_6 = value;
	}

	inline static int32_t get_offset_of_visibleTintColor_7() { return static_cast<int32_t>(offsetof(TeleportArea_t1950611660, ___visibleTintColor_7)); }
	inline Color_t2020392075  get_visibleTintColor_7() const { return ___visibleTintColor_7; }
	inline Color_t2020392075 * get_address_of_visibleTintColor_7() { return &___visibleTintColor_7; }
	inline void set_visibleTintColor_7(Color_t2020392075  value)
	{
		___visibleTintColor_7 = value;
	}

	inline static int32_t get_offset_of_highlightedTintColor_8() { return static_cast<int32_t>(offsetof(TeleportArea_t1950611660, ___highlightedTintColor_8)); }
	inline Color_t2020392075  get_highlightedTintColor_8() const { return ___highlightedTintColor_8; }
	inline Color_t2020392075 * get_address_of_highlightedTintColor_8() { return &___highlightedTintColor_8; }
	inline void set_highlightedTintColor_8(Color_t2020392075  value)
	{
		___highlightedTintColor_8 = value;
	}

	inline static int32_t get_offset_of_lockedTintColor_9() { return static_cast<int32_t>(offsetof(TeleportArea_t1950611660, ___lockedTintColor_9)); }
	inline Color_t2020392075  get_lockedTintColor_9() const { return ___lockedTintColor_9; }
	inline Color_t2020392075 * get_address_of_lockedTintColor_9() { return &___lockedTintColor_9; }
	inline void set_lockedTintColor_9(Color_t2020392075  value)
	{
		___lockedTintColor_9 = value;
	}

	inline static int32_t get_offset_of_highlighted_10() { return static_cast<int32_t>(offsetof(TeleportArea_t1950611660, ___highlighted_10)); }
	inline bool get_highlighted_10() const { return ___highlighted_10; }
	inline bool* get_address_of_highlighted_10() { return &___highlighted_10; }
	inline void set_highlighted_10(bool value)
	{
		___highlighted_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
