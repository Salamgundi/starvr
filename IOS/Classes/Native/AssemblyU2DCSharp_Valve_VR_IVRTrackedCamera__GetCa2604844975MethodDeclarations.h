﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRTrackedCamera/_GetCameraIntrinsics
struct _GetCameraIntrinsics_t2604844975;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraError3529690400.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraFrameTy2003723073.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdVector2_t2255225135.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRTrackedCamera/_GetCameraIntrinsics::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetCameraIntrinsics__ctor_m3405752822 (_GetCameraIntrinsics_t2604844975 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_GetCameraIntrinsics::Invoke(System.UInt32,Valve.VR.EVRTrackedCameraFrameType,Valve.VR.HmdVector2_t&,Valve.VR.HmdVector2_t&)
extern "C"  int32_t _GetCameraIntrinsics_Invoke_m1921988032 (_GetCameraIntrinsics_t2604844975 * __this, uint32_t ___nDeviceIndex0, int32_t ___eFrameType1, HmdVector2_t_t2255225135 * ___pFocalLength2, HmdVector2_t_t2255225135 * ___pCenter3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRTrackedCamera/_GetCameraIntrinsics::BeginInvoke(System.UInt32,Valve.VR.EVRTrackedCameraFrameType,Valve.VR.HmdVector2_t&,Valve.VR.HmdVector2_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetCameraIntrinsics_BeginInvoke_m3103554326 (_GetCameraIntrinsics_t2604844975 * __this, uint32_t ___nDeviceIndex0, int32_t ___eFrameType1, HmdVector2_t_t2255225135 * ___pFocalLength2, HmdVector2_t_t2255225135 * ___pCenter3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_GetCameraIntrinsics::EndInvoke(Valve.VR.HmdVector2_t&,Valve.VR.HmdVector2_t&,System.IAsyncResult)
extern "C"  int32_t _GetCameraIntrinsics_EndInvoke_m1411941285 (_GetCameraIntrinsics_t2604844975 * __this, HmdVector2_t_t2255225135 * ___pFocalLength0, HmdVector2_t_t2255225135 * ___pCenter1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
