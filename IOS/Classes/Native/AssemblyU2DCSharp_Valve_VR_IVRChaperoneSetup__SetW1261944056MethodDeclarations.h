﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_SetWorkingSeatedZeroPoseToRawTrackingPose
struct _SetWorkingSeatedZeroPoseToRawTrackingPose_t1261944056;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingSeatedZeroPoseToRawTrackingPose::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetWorkingSeatedZeroPoseToRawTrackingPose__ctor_m2585886983 (_SetWorkingSeatedZeroPoseToRawTrackingPose_t1261944056 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingSeatedZeroPoseToRawTrackingPose::Invoke(Valve.VR.HmdMatrix34_t&)
extern "C"  void _SetWorkingSeatedZeroPoseToRawTrackingPose_Invoke_m3466533019 (_SetWorkingSeatedZeroPoseToRawTrackingPose_t1261944056 * __this, HmdMatrix34_t_t664273062 * ___pMatSeatedZeroPoseToRawTrackingPose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_SetWorkingSeatedZeroPoseToRawTrackingPose::BeginInvoke(Valve.VR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetWorkingSeatedZeroPoseToRawTrackingPose_BeginInvoke_m3812662002 (_SetWorkingSeatedZeroPoseToRawTrackingPose_t1261944056 * __this, HmdMatrix34_t_t664273062 * ___pMatSeatedZeroPoseToRawTrackingPose0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingSeatedZeroPoseToRawTrackingPose::EndInvoke(Valve.VR.HmdMatrix34_t&,System.IAsyncResult)
extern "C"  void _SetWorkingSeatedZeroPoseToRawTrackingPose_EndInvoke_m984362745 (_SetWorkingSeatedZeroPoseToRawTrackingPose_t1261944056 * __this, HmdMatrix34_t_t664273062 * ___pMatSeatedZeroPoseToRawTrackingPose0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
