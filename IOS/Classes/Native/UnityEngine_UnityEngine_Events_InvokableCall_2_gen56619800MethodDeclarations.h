﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>
struct InvokableCall_2_t56619800;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>
struct UnityAction_2_t41828916;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m1363792238_gshared (InvokableCall_2_t56619800 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_2__ctor_m1363792238(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_2_t56619800 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2__ctor_m1363792238_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m48765551_gshared (InvokableCall_2_t56619800 * __this, UnityAction_2_t41828916 * ___action0, const MethodInfo* method);
#define InvokableCall_2__ctor_m48765551(__this, ___action0, method) ((  void (*) (InvokableCall_2_t56619800 *, UnityAction_2_t41828916 *, const MethodInfo*))InvokableCall_2__ctor_m48765551_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m4001424544_gshared (InvokableCall_2_t56619800 * __this, UnityAction_2_t41828916 * ___value0, const MethodInfo* method);
#define InvokableCall_2_add_Delegate_m4001424544(__this, ___value0, method) ((  void (*) (InvokableCall_2_t56619800 *, UnityAction_2_t41828916 *, const MethodInfo*))InvokableCall_2_add_Delegate_m4001424544_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m4131363733_gshared (InvokableCall_2_t56619800 * __this, UnityAction_2_t41828916 * ___value0, const MethodInfo* method);
#define InvokableCall_2_remove_Delegate_m4131363733(__this, ___value0, method) ((  void (*) (InvokableCall_2_t56619800 *, UnityAction_2_t41828916 *, const MethodInfo*))InvokableCall_2_remove_Delegate_m4131363733_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m4293234667_gshared (InvokableCall_2_t56619800 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_2_Invoke_m4293234667(__this, ___args0, method) ((  void (*) (InvokableCall_2_t56619800 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_2_Invoke_m4293234667_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m2572262419_gshared (InvokableCall_2_t56619800 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_2_Find_m2572262419(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_2_t56619800 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2_Find_m2572262419_gshared)(__this, ___targetObj0, ___method1, method)
