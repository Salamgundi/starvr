﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_GetFrameTimings
struct _GetFrameTimings_t711480574;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_Compositor_FrameTiming2839634313.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_GetFrameTimings::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetFrameTimings__ctor_m2097377115 (_GetFrameTimings_t711480574 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRCompositor/_GetFrameTimings::Invoke(Valve.VR.Compositor_FrameTiming&,System.UInt32)
extern "C"  uint32_t _GetFrameTimings_Invoke_m3458388469 (_GetFrameTimings_t711480574 * __this, Compositor_FrameTiming_t2839634313 * ___pTiming0, uint32_t ___nFrames1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_GetFrameTimings::BeginInvoke(Valve.VR.Compositor_FrameTiming&,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetFrameTimings_BeginInvoke_m1826304929 (_GetFrameTimings_t711480574 * __this, Compositor_FrameTiming_t2839634313 * ___pTiming0, uint32_t ___nFrames1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRCompositor/_GetFrameTimings::EndInvoke(Valve.VR.Compositor_FrameTiming&,System.IAsyncResult)
extern "C"  uint32_t _GetFrameTimings_EndInvoke_m3232491479 (_GetFrameTimings_t711480574 * __this, Compositor_FrameTiming_t2839634313 * ___pTiming0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
