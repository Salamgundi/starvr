﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetOverlayAutoCurveDistanceRangeInMeters
struct _SetOverlayAutoCurveDistanceRangeInMeters_t1475163830;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetOverlayAutoCurveDistanceRangeInMeters::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayAutoCurveDistanceRangeInMeters__ctor_m3478312943 (_SetOverlayAutoCurveDistanceRangeInMeters_t1475163830 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayAutoCurveDistanceRangeInMeters::Invoke(System.UInt64,System.Single,System.Single)
extern "C"  int32_t _SetOverlayAutoCurveDistanceRangeInMeters_Invoke_m3194103798 (_SetOverlayAutoCurveDistanceRangeInMeters_t1475163830 * __this, uint64_t ___ulOverlayHandle0, float ___fMinDistanceInMeters1, float ___fMaxDistanceInMeters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayAutoCurveDistanceRangeInMeters::BeginInvoke(System.UInt64,System.Single,System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetOverlayAutoCurveDistanceRangeInMeters_BeginInvoke_m3702547235 (_SetOverlayAutoCurveDistanceRangeInMeters_t1475163830 * __this, uint64_t ___ulOverlayHandle0, float ___fMinDistanceInMeters1, float ___fMaxDistanceInMeters2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayAutoCurveDistanceRangeInMeters::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayAutoCurveDistanceRangeInMeters_EndInvoke_m3217418169 (_SetOverlayAutoCurveDistanceRangeInMeters_t1475163830 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
