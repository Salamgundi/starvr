﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_DestinationPoint
struct VRTK_DestinationPoint_t1547370418;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventArgs1852451661.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_DestinationPoint/<DoDestinationMarkerSetAtEndOfFrame>c__Iterator1
struct  U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915  : public Il2CppObject
{
public:
	// VRTK.DestinationMarkerEventArgs VRTK.VRTK_DestinationPoint/<DoDestinationMarkerSetAtEndOfFrame>c__Iterator1::e
	DestinationMarkerEventArgs_t1852451661  ___e_0;
	// VRTK.VRTK_DestinationPoint VRTK.VRTK_DestinationPoint/<DoDestinationMarkerSetAtEndOfFrame>c__Iterator1::$this
	VRTK_DestinationPoint_t1547370418 * ___U24this_1;
	// System.Object VRTK.VRTK_DestinationPoint/<DoDestinationMarkerSetAtEndOfFrame>c__Iterator1::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean VRTK.VRTK_DestinationPoint/<DoDestinationMarkerSetAtEndOfFrame>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 VRTK.VRTK_DestinationPoint/<DoDestinationMarkerSetAtEndOfFrame>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_e_0() { return static_cast<int32_t>(offsetof(U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915, ___e_0)); }
	inline DestinationMarkerEventArgs_t1852451661  get_e_0() const { return ___e_0; }
	inline DestinationMarkerEventArgs_t1852451661 * get_address_of_e_0() { return &___e_0; }
	inline void set_e_0(DestinationMarkerEventArgs_t1852451661  value)
	{
		___e_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915, ___U24this_1)); }
	inline VRTK_DestinationPoint_t1547370418 * get_U24this_1() const { return ___U24this_1; }
	inline VRTK_DestinationPoint_t1547370418 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(VRTK_DestinationPoint_t1547370418 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
