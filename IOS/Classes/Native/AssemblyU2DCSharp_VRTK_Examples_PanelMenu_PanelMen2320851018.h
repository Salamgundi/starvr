﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Slider
struct Slider_t297367283;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.PanelMenu.PanelMenuUISlider
struct  PanelMenuUISlider_t2320851018  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Slider VRTK.Examples.PanelMenu.PanelMenuUISlider::slider
	Slider_t297367283 * ___slider_2;

public:
	inline static int32_t get_offset_of_slider_2() { return static_cast<int32_t>(offsetof(PanelMenuUISlider_t2320851018, ___slider_2)); }
	inline Slider_t297367283 * get_slider_2() const { return ___slider_2; }
	inline Slider_t297367283 ** get_address_of_slider_2() { return &___slider_2; }
	inline void set_slider_2(Slider_t297367283 * value)
	{
		___slider_2 = value;
		Il2CppCodeGenWriteBarrier(&___slider_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
