﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_PlayAreaCursor
struct VRTK_PlayAreaCursor_t3566057915;
// VRTK.VRTK_PolicyList
struct VRTK_PolicyList_t2965133344;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_PlayAreaCollider
struct  VRTK_PlayAreaCollider_t1466370881  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_PlayAreaCursor VRTK.VRTK_PlayAreaCollider::parent
	VRTK_PlayAreaCursor_t3566057915 * ___parent_2;
	// VRTK.VRTK_PolicyList VRTK.VRTK_PlayAreaCollider::targetListPolicy
	VRTK_PolicyList_t2965133344 * ___targetListPolicy_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCollider_t1466370881, ___parent_2)); }
	inline VRTK_PlayAreaCursor_t3566057915 * get_parent_2() const { return ___parent_2; }
	inline VRTK_PlayAreaCursor_t3566057915 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(VRTK_PlayAreaCursor_t3566057915 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_targetListPolicy_3() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCollider_t1466370881, ___targetListPolicy_3)); }
	inline VRTK_PolicyList_t2965133344 * get_targetListPolicy_3() const { return ___targetListPolicy_3; }
	inline VRTK_PolicyList_t2965133344 ** get_address_of_targetListPolicy_3() { return &___targetListPolicy_3; }
	inline void set_targetListPolicy_3(VRTK_PolicyList_t2965133344 * value)
	{
		___targetListPolicy_3 = value;
		Il2CppCodeGenWriteBarrier(&___targetListPolicy_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
