﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Controller
struct SteamVR_Controller_t4041762851;
// SteamVR_Controller/Device
struct Device_t2885069456;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SteamVR_Controller_DeviceRelatio2314381548.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedDeviceClass2121051631.h"

// System.Void SteamVR_Controller::.ctor()
extern "C"  void SteamVR_Controller__ctor_m4224566588 (SteamVR_Controller_t4041762851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Controller/Device SteamVR_Controller::Input(System.Int32)
extern "C"  Device_t2885069456 * SteamVR_Controller_Input_m3558509414 (Il2CppObject * __this /* static, unused */, int32_t ___deviceIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Controller::Update()
extern "C"  void SteamVR_Controller_Update_m1638132643 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SteamVR_Controller::GetDeviceIndex(SteamVR_Controller/DeviceRelation,Valve.VR.ETrackedDeviceClass,System.Int32)
extern "C"  int32_t SteamVR_Controller_GetDeviceIndex_m3541510044 (Il2CppObject * __this /* static, unused */, int32_t ___relation0, int32_t ___deviceClass1, int32_t ___relativeTo2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
