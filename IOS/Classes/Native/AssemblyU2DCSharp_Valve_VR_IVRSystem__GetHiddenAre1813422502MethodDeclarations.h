﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetHiddenAreaMesh
struct _GetHiddenAreaMesh_t1813422502;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HiddenAreaMesh_t3319190843.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"
#include "AssemblyU2DCSharp_Valve_VR_EHiddenAreaMeshType3068936429.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetHiddenAreaMesh::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetHiddenAreaMesh__ctor_m3597632979 (_GetHiddenAreaMesh_t1813422502 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HiddenAreaMesh_t Valve.VR.IVRSystem/_GetHiddenAreaMesh::Invoke(Valve.VR.EVREye,Valve.VR.EHiddenAreaMeshType)
extern "C"  HiddenAreaMesh_t_t3319190843  _GetHiddenAreaMesh_Invoke_m3075037462 (_GetHiddenAreaMesh_t1813422502 * __this, int32_t ___eEye0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetHiddenAreaMesh::BeginInvoke(Valve.VR.EVREye,Valve.VR.EHiddenAreaMeshType,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetHiddenAreaMesh_BeginInvoke_m404340791 (_GetHiddenAreaMesh_t1813422502 * __this, int32_t ___eEye0, int32_t ___type1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HiddenAreaMesh_t Valve.VR.IVRSystem/_GetHiddenAreaMesh::EndInvoke(System.IAsyncResult)
extern "C"  HiddenAreaMesh_t_t3319190843  _GetHiddenAreaMesh_EndInvoke_m179802351 (_GetHiddenAreaMesh_t1813422502 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
