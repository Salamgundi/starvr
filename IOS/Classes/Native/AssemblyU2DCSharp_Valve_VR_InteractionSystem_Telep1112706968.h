﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.TeleportMarkerBase
struct  TeleportMarkerBase_t1112706968  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Valve.VR.InteractionSystem.TeleportMarkerBase::locked
	bool ___locked_2;
	// System.Boolean Valve.VR.InteractionSystem.TeleportMarkerBase::markerActive
	bool ___markerActive_3;

public:
	inline static int32_t get_offset_of_locked_2() { return static_cast<int32_t>(offsetof(TeleportMarkerBase_t1112706968, ___locked_2)); }
	inline bool get_locked_2() const { return ___locked_2; }
	inline bool* get_address_of_locked_2() { return &___locked_2; }
	inline void set_locked_2(bool value)
	{
		___locked_2 = value;
	}

	inline static int32_t get_offset_of_markerActive_3() { return static_cast<int32_t>(offsetof(TeleportMarkerBase_t1112706968, ___markerActive_3)); }
	inline bool get_markerActive_3() const { return ___markerActive_3; }
	inline bool* get_address_of_markerActive_3() { return &___markerActive_3; }
	inline void set_markerActive_3(bool value)
	{
		___markerActive_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
