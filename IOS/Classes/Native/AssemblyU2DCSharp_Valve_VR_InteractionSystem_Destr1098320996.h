﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.DestroyOnParticleSystemDeath
struct  DestroyOnParticleSystemDeath_t1098320996  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.ParticleSystem Valve.VR.InteractionSystem.DestroyOnParticleSystemDeath::particles
	ParticleSystem_t3394631041 * ___particles_2;

public:
	inline static int32_t get_offset_of_particles_2() { return static_cast<int32_t>(offsetof(DestroyOnParticleSystemDeath_t1098320996, ___particles_2)); }
	inline ParticleSystem_t3394631041 * get_particles_2() const { return ___particles_2; }
	inline ParticleSystem_t3394631041 ** get_address_of_particles_2() { return &___particles_2; }
	inline void set_particles_2(ParticleSystem_t3394631041 * value)
	{
		___particles_2 = value;
		Il2CppCodeGenWriteBarrier(&___particles_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
