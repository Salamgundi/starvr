﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_WarpObjectControlAction
struct VRTK_WarpObjectControlAction_t3039272518;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_WarpObjectControlAction::.ctor()
extern "C"  void VRTK_WarpObjectControlAction__ctor_m2400935438 (VRTK_WarpObjectControlAction_t3039272518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_WarpObjectControlAction::Process(UnityEngine.GameObject,UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Single,System.Boolean,System.Boolean)
extern "C"  void VRTK_WarpObjectControlAction_Process_m1965104587 (VRTK_WarpObjectControlAction_t3039272518 * __this, GameObject_t1756533147 * ___controlledGameObject0, Transform_t3275118058 * ___directionDevice1, Vector3_t2243707580  ___axisDirection2, float ___axis3, float ___deadzone4, bool ___currentlyFalling5, bool ___modifierActive6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_WarpObjectControlAction::OnEnable()
extern "C"  void VRTK_WarpObjectControlAction_OnEnable_m2196046506 (VRTK_WarpObjectControlAction_t3039272518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_WarpObjectControlAction::Warp(UnityEngine.GameObject,UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  void VRTK_WarpObjectControlAction_Warp_m3281243986 (VRTK_WarpObjectControlAction_t3039272518 * __this, GameObject_t1756533147 * ___controlledGameObject0, Transform_t3275118058 * ___directionDevice1, Vector3_t2243707580  ___axisDirection2, float ___axis3, bool ___modifierActive4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
