﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.SDK_ControllerHapticModifiers
struct  SDK_ControllerHapticModifiers_t3871094838  : public Il2CppObject
{
public:
	// System.Single VRTK.SDK_ControllerHapticModifiers::durationModifier
	float ___durationModifier_0;
	// System.Single VRTK.SDK_ControllerHapticModifiers::intervalModifier
	float ___intervalModifier_1;

public:
	inline static int32_t get_offset_of_durationModifier_0() { return static_cast<int32_t>(offsetof(SDK_ControllerHapticModifiers_t3871094838, ___durationModifier_0)); }
	inline float get_durationModifier_0() const { return ___durationModifier_0; }
	inline float* get_address_of_durationModifier_0() { return &___durationModifier_0; }
	inline void set_durationModifier_0(float value)
	{
		___durationModifier_0 = value;
	}

	inline static int32_t get_offset_of_intervalModifier_1() { return static_cast<int32_t>(offsetof(SDK_ControllerHapticModifiers_t3871094838, ___intervalModifier_1)); }
	inline float get_intervalModifier_1() const { return ___intervalModifier_1; }
	inline float* get_address_of_intervalModifier_1() { return &___intervalModifier_1; }
	inline void set_intervalModifier_1(float value)
	{
		___intervalModifier_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
