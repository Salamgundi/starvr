﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_Control_UnityEvents
struct VRTK_Control_UnityEvents_t692650506;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_Control3DEventArgs4095025701.h"

// System.Void VRTK.UnityEventHelper.VRTK_Control_UnityEvents::.ctor()
extern "C"  void VRTK_Control_UnityEvents__ctor_m2082383265 (VRTK_Control_UnityEvents_t692650506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_Control_UnityEvents::SetControl3D()
extern "C"  void VRTK_Control_UnityEvents_SetControl3D_m1751854191 (VRTK_Control_UnityEvents_t692650506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_Control_UnityEvents::OnEnable()
extern "C"  void VRTK_Control_UnityEvents_OnEnable_m2804834881 (VRTK_Control_UnityEvents_t692650506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_Control_UnityEvents::ValueChanged(System.Object,VRTK.Control3DEventArgs)
extern "C"  void VRTK_Control_UnityEvents_ValueChanged_m2161766810 (VRTK_Control_UnityEvents_t692650506 * __this, Il2CppObject * ___o0, Control3DEventArgs_t4095025701  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_Control_UnityEvents::OnDisable()
extern "C"  void VRTK_Control_UnityEvents_OnDisable_m1346410420 (VRTK_Control_UnityEvents_t692650506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
