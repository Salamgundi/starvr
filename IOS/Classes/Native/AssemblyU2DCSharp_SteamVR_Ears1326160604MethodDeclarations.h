﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Ears
struct SteamVR_Ears_t1326160604;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Ears::.ctor()
extern "C"  void SteamVR_Ears__ctor_m3617964867 (SteamVR_Ears_t1326160604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Ears::OnNewPosesApplied()
extern "C"  void SteamVR_Ears_OnNewPosesApplied_m972909789 (SteamVR_Ears_t1326160604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Ears::OnEnable()
extern "C"  void SteamVR_Ears_OnEnable_m3023960903 (SteamVR_Ears_t1326160604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Ears::OnDisable()
extern "C"  void SteamVR_Ears_OnDisable_m1801647196 (SteamVR_Ears_t1326160604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
