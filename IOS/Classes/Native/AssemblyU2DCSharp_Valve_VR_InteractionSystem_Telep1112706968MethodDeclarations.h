﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.TeleportMarkerBase
struct TeleportMarkerBase_t1112706968;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Valve.VR.InteractionSystem.TeleportMarkerBase::.ctor()
extern "C"  void TeleportMarkerBase__ctor_m115346386 (TeleportMarkerBase_t1112706968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.TeleportMarkerBase::get_showReticle()
extern "C"  bool TeleportMarkerBase_get_showReticle_m4078543352 (TeleportMarkerBase_t1112706968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportMarkerBase::SetLocked(System.Boolean)
extern "C"  void TeleportMarkerBase_SetLocked_m409877099 (TeleportMarkerBase_t1112706968 * __this, bool ___locked0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportMarkerBase::TeleportPlayer(UnityEngine.Vector3)
extern "C"  void TeleportMarkerBase_TeleportPlayer_m1685649791 (TeleportMarkerBase_t1112706968 * __this, Vector3_t2243707580  ___pointedAtPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
