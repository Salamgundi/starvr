﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3461135388.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_SteamVR_Utils_RigidTransform2602383126.h"

// System.Void System.Array/InternalEnumerator`1<SteamVR_Utils/RigidTransform>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m162628759_gshared (InternalEnumerator_1_t3461135388 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m162628759(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3461135388 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m162628759_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SteamVR_Utils/RigidTransform>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m71863143_gshared (InternalEnumerator_1_t3461135388 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m71863143(__this, method) ((  void (*) (InternalEnumerator_1_t3461135388 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m71863143_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SteamVR_Utils/RigidTransform>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1961150267_gshared (InternalEnumerator_1_t3461135388 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1961150267(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3461135388 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1961150267_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SteamVR_Utils/RigidTransform>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m678020422_gshared (InternalEnumerator_1_t3461135388 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m678020422(__this, method) ((  void (*) (InternalEnumerator_1_t3461135388 *, const MethodInfo*))InternalEnumerator_1_Dispose_m678020422_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SteamVR_Utils/RigidTransform>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2215102579_gshared (InternalEnumerator_1_t3461135388 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2215102579(__this, method) ((  bool (*) (InternalEnumerator_1_t3461135388 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2215102579_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SteamVR_Utils/RigidTransform>::get_Current()
extern "C"  RigidTransform_t2602383126  InternalEnumerator_1_get_Current_m1620449408_gshared (InternalEnumerator_1_t3461135388 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1620449408(__this, method) ((  RigidTransform_t2602383126  (*) (InternalEnumerator_1_t3461135388 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1620449408_gshared)(__this, method)
