﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_CommitWorkingCopy
struct _CommitWorkingCopy_t206435796;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EChaperoneConfigFile30305944.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_CommitWorkingCopy::.ctor(System.Object,System.IntPtr)
extern "C"  void _CommitWorkingCopy__ctor_m3567475901 (_CommitWorkingCopy_t206435796 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_CommitWorkingCopy::Invoke(Valve.VR.EChaperoneConfigFile)
extern "C"  bool _CommitWorkingCopy_Invoke_m1166193145 (_CommitWorkingCopy_t206435796 * __this, int32_t ___configFile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_CommitWorkingCopy::BeginInvoke(Valve.VR.EChaperoneConfigFile,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _CommitWorkingCopy_BeginInvoke_m1196525242 (_CommitWorkingCopy_t206435796 * __this, int32_t ___configFile0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_CommitWorkingCopy::EndInvoke(System.IAsyncResult)
extern "C"  bool _CommitWorkingCopy_EndInvoke_m1374379863 (_CommitWorkingCopy_t206435796 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
