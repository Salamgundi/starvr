﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach
struct  VRTK_BaseGrabAttach_t3487134318  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::precisionGrab
	bool ___precisionGrab_2;
	// UnityEngine.Transform VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::rightSnapHandle
	Transform_t3275118058 * ___rightSnapHandle_3;
	// UnityEngine.Transform VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::leftSnapHandle
	Transform_t3275118058 * ___leftSnapHandle_4;
	// System.Boolean VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::throwVelocityWithAttachDistance
	bool ___throwVelocityWithAttachDistance_5;
	// System.Single VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::throwMultiplier
	float ___throwMultiplier_6;
	// System.Single VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::onGrabCollisionDelay
	float ___onGrabCollisionDelay_7;
	// System.Boolean VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::tracked
	bool ___tracked_8;
	// System.Boolean VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::climbable
	bool ___climbable_9;
	// System.Boolean VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::kinematic
	bool ___kinematic_10;
	// UnityEngine.GameObject VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::grabbedObject
	GameObject_t1756533147 * ___grabbedObject_11;
	// UnityEngine.Rigidbody VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::grabbedObjectRigidBody
	Rigidbody_t4233889191 * ___grabbedObjectRigidBody_12;
	// VRTK.VRTK_InteractableObject VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::grabbedObjectScript
	VRTK_InteractableObject_t2604188111 * ___grabbedObjectScript_13;
	// UnityEngine.Transform VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::trackPoint
	Transform_t3275118058 * ___trackPoint_14;
	// UnityEngine.Transform VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::grabbedSnapHandle
	Transform_t3275118058 * ___grabbedSnapHandle_15;
	// UnityEngine.Transform VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::initialAttachPoint
	Transform_t3275118058 * ___initialAttachPoint_16;
	// UnityEngine.Rigidbody VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::controllerAttachPoint
	Rigidbody_t4233889191 * ___controllerAttachPoint_17;

public:
	inline static int32_t get_offset_of_precisionGrab_2() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___precisionGrab_2)); }
	inline bool get_precisionGrab_2() const { return ___precisionGrab_2; }
	inline bool* get_address_of_precisionGrab_2() { return &___precisionGrab_2; }
	inline void set_precisionGrab_2(bool value)
	{
		___precisionGrab_2 = value;
	}

	inline static int32_t get_offset_of_rightSnapHandle_3() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___rightSnapHandle_3)); }
	inline Transform_t3275118058 * get_rightSnapHandle_3() const { return ___rightSnapHandle_3; }
	inline Transform_t3275118058 ** get_address_of_rightSnapHandle_3() { return &___rightSnapHandle_3; }
	inline void set_rightSnapHandle_3(Transform_t3275118058 * value)
	{
		___rightSnapHandle_3 = value;
		Il2CppCodeGenWriteBarrier(&___rightSnapHandle_3, value);
	}

	inline static int32_t get_offset_of_leftSnapHandle_4() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___leftSnapHandle_4)); }
	inline Transform_t3275118058 * get_leftSnapHandle_4() const { return ___leftSnapHandle_4; }
	inline Transform_t3275118058 ** get_address_of_leftSnapHandle_4() { return &___leftSnapHandle_4; }
	inline void set_leftSnapHandle_4(Transform_t3275118058 * value)
	{
		___leftSnapHandle_4 = value;
		Il2CppCodeGenWriteBarrier(&___leftSnapHandle_4, value);
	}

	inline static int32_t get_offset_of_throwVelocityWithAttachDistance_5() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___throwVelocityWithAttachDistance_5)); }
	inline bool get_throwVelocityWithAttachDistance_5() const { return ___throwVelocityWithAttachDistance_5; }
	inline bool* get_address_of_throwVelocityWithAttachDistance_5() { return &___throwVelocityWithAttachDistance_5; }
	inline void set_throwVelocityWithAttachDistance_5(bool value)
	{
		___throwVelocityWithAttachDistance_5 = value;
	}

	inline static int32_t get_offset_of_throwMultiplier_6() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___throwMultiplier_6)); }
	inline float get_throwMultiplier_6() const { return ___throwMultiplier_6; }
	inline float* get_address_of_throwMultiplier_6() { return &___throwMultiplier_6; }
	inline void set_throwMultiplier_6(float value)
	{
		___throwMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_onGrabCollisionDelay_7() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___onGrabCollisionDelay_7)); }
	inline float get_onGrabCollisionDelay_7() const { return ___onGrabCollisionDelay_7; }
	inline float* get_address_of_onGrabCollisionDelay_7() { return &___onGrabCollisionDelay_7; }
	inline void set_onGrabCollisionDelay_7(float value)
	{
		___onGrabCollisionDelay_7 = value;
	}

	inline static int32_t get_offset_of_tracked_8() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___tracked_8)); }
	inline bool get_tracked_8() const { return ___tracked_8; }
	inline bool* get_address_of_tracked_8() { return &___tracked_8; }
	inline void set_tracked_8(bool value)
	{
		___tracked_8 = value;
	}

	inline static int32_t get_offset_of_climbable_9() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___climbable_9)); }
	inline bool get_climbable_9() const { return ___climbable_9; }
	inline bool* get_address_of_climbable_9() { return &___climbable_9; }
	inline void set_climbable_9(bool value)
	{
		___climbable_9 = value;
	}

	inline static int32_t get_offset_of_kinematic_10() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___kinematic_10)); }
	inline bool get_kinematic_10() const { return ___kinematic_10; }
	inline bool* get_address_of_kinematic_10() { return &___kinematic_10; }
	inline void set_kinematic_10(bool value)
	{
		___kinematic_10 = value;
	}

	inline static int32_t get_offset_of_grabbedObject_11() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___grabbedObject_11)); }
	inline GameObject_t1756533147 * get_grabbedObject_11() const { return ___grabbedObject_11; }
	inline GameObject_t1756533147 ** get_address_of_grabbedObject_11() { return &___grabbedObject_11; }
	inline void set_grabbedObject_11(GameObject_t1756533147 * value)
	{
		___grabbedObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___grabbedObject_11, value);
	}

	inline static int32_t get_offset_of_grabbedObjectRigidBody_12() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___grabbedObjectRigidBody_12)); }
	inline Rigidbody_t4233889191 * get_grabbedObjectRigidBody_12() const { return ___grabbedObjectRigidBody_12; }
	inline Rigidbody_t4233889191 ** get_address_of_grabbedObjectRigidBody_12() { return &___grabbedObjectRigidBody_12; }
	inline void set_grabbedObjectRigidBody_12(Rigidbody_t4233889191 * value)
	{
		___grabbedObjectRigidBody_12 = value;
		Il2CppCodeGenWriteBarrier(&___grabbedObjectRigidBody_12, value);
	}

	inline static int32_t get_offset_of_grabbedObjectScript_13() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___grabbedObjectScript_13)); }
	inline VRTK_InteractableObject_t2604188111 * get_grabbedObjectScript_13() const { return ___grabbedObjectScript_13; }
	inline VRTK_InteractableObject_t2604188111 ** get_address_of_grabbedObjectScript_13() { return &___grabbedObjectScript_13; }
	inline void set_grabbedObjectScript_13(VRTK_InteractableObject_t2604188111 * value)
	{
		___grabbedObjectScript_13 = value;
		Il2CppCodeGenWriteBarrier(&___grabbedObjectScript_13, value);
	}

	inline static int32_t get_offset_of_trackPoint_14() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___trackPoint_14)); }
	inline Transform_t3275118058 * get_trackPoint_14() const { return ___trackPoint_14; }
	inline Transform_t3275118058 ** get_address_of_trackPoint_14() { return &___trackPoint_14; }
	inline void set_trackPoint_14(Transform_t3275118058 * value)
	{
		___trackPoint_14 = value;
		Il2CppCodeGenWriteBarrier(&___trackPoint_14, value);
	}

	inline static int32_t get_offset_of_grabbedSnapHandle_15() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___grabbedSnapHandle_15)); }
	inline Transform_t3275118058 * get_grabbedSnapHandle_15() const { return ___grabbedSnapHandle_15; }
	inline Transform_t3275118058 ** get_address_of_grabbedSnapHandle_15() { return &___grabbedSnapHandle_15; }
	inline void set_grabbedSnapHandle_15(Transform_t3275118058 * value)
	{
		___grabbedSnapHandle_15 = value;
		Il2CppCodeGenWriteBarrier(&___grabbedSnapHandle_15, value);
	}

	inline static int32_t get_offset_of_initialAttachPoint_16() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___initialAttachPoint_16)); }
	inline Transform_t3275118058 * get_initialAttachPoint_16() const { return ___initialAttachPoint_16; }
	inline Transform_t3275118058 ** get_address_of_initialAttachPoint_16() { return &___initialAttachPoint_16; }
	inline void set_initialAttachPoint_16(Transform_t3275118058 * value)
	{
		___initialAttachPoint_16 = value;
		Il2CppCodeGenWriteBarrier(&___initialAttachPoint_16, value);
	}

	inline static int32_t get_offset_of_controllerAttachPoint_17() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAttach_t3487134318, ___controllerAttachPoint_17)); }
	inline Rigidbody_t4233889191 * get_controllerAttachPoint_17() const { return ___controllerAttachPoint_17; }
	inline Rigidbody_t4233889191 ** get_address_of_controllerAttachPoint_17() { return &___controllerAttachPoint_17; }
	inline void set_controllerAttachPoint_17(Rigidbody_t4233889191 * value)
	{
		___controllerAttachPoint_17 = value;
		Il2CppCodeGenWriteBarrier(&___controllerAttachPoint_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
