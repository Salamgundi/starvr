﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach
struct VRTK_BaseJointGrabAttach_t1229653768;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach::.ctor()
extern "C"  void VRTK_BaseJointGrabAttach__ctor_m4256144894 (VRTK_BaseJointGrabAttach_t1229653768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach::ValidGrab(UnityEngine.Rigidbody)
extern "C"  bool VRTK_BaseJointGrabAttach_ValidGrab_m3323298500 (VRTK_BaseJointGrabAttach_t1229653768 * __this, Rigidbody_t4233889191 * ___checkAttachPoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach::StartGrab(UnityEngine.GameObject,UnityEngine.GameObject,UnityEngine.Rigidbody)
extern "C"  bool VRTK_BaseJointGrabAttach_StartGrab_m2764348458 (VRTK_BaseJointGrabAttach_t1229653768 * __this, GameObject_t1756533147 * ___grabbingObject0, GameObject_t1756533147 * ___givenGrabbedObject1, Rigidbody_t4233889191 * ___givenControllerAttachPoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach::StopGrab(System.Boolean)
extern "C"  void VRTK_BaseJointGrabAttach_StopGrab_m88451727 (VRTK_BaseJointGrabAttach_t1229653768 * __this, bool ___applyGrabbingObjectVelocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach::Initialise()
extern "C"  void VRTK_BaseJointGrabAttach_Initialise_m194708055 (VRTK_BaseJointGrabAttach_t1229653768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach::ReleaseFromController(System.Boolean)
extern "C"  Rigidbody_t4233889191 * VRTK_BaseJointGrabAttach_ReleaseFromController_m485095443 (VRTK_BaseJointGrabAttach_t1229653768 * __this, bool ___applyGrabbingObjectVelocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach::OnJointBreak(System.Single)
extern "C"  void VRTK_BaseJointGrabAttach_OnJointBreak_m3725421683 (VRTK_BaseJointGrabAttach_t1229653768 * __this, float ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach::CreateJoint(UnityEngine.GameObject)
extern "C"  void VRTK_BaseJointGrabAttach_CreateJoint_m974512232 (VRTK_BaseJointGrabAttach_t1229653768 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach::DestroyJoint(System.Boolean,System.Boolean)
extern "C"  void VRTK_BaseJointGrabAttach_DestroyJoint_m2806471522 (VRTK_BaseJointGrabAttach_t1229653768 * __this, bool ___withDestroyImmediate0, bool ___applyGrabbingObjectVelocity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach::SetSnappedObjectPosition(UnityEngine.GameObject)
extern "C"  void VRTK_BaseJointGrabAttach_SetSnappedObjectPosition_m2274558685 (VRTK_BaseJointGrabAttach_t1229653768 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach::SnapObjectToGrabToController(UnityEngine.GameObject)
extern "C"  void VRTK_BaseJointGrabAttach_SnapObjectToGrabToController_m872219257 (VRTK_BaseJointGrabAttach_t1229653768 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
