﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_GetLiveSeatedZeroPoseToRawTrackingPose
struct _GetLiveSeatedZeroPoseToRawTrackingPose_t1524066641;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_GetLiveSeatedZeroPoseToRawTrackingPose::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetLiveSeatedZeroPoseToRawTrackingPose__ctor_m1080796148 (_GetLiveSeatedZeroPoseToRawTrackingPose_t1524066641 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetLiveSeatedZeroPoseToRawTrackingPose::Invoke(Valve.VR.HmdMatrix34_t&)
extern "C"  bool _GetLiveSeatedZeroPoseToRawTrackingPose_Invoke_m2923829630 (_GetLiveSeatedZeroPoseToRawTrackingPose_t1524066641 * __this, HmdMatrix34_t_t664273062 * ___pmatSeatedZeroPoseToRawTrackingPose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_GetLiveSeatedZeroPoseToRawTrackingPose::BeginInvoke(Valve.VR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetLiveSeatedZeroPoseToRawTrackingPose_BeginInvoke_m1568217071 (_GetLiveSeatedZeroPoseToRawTrackingPose_t1524066641 * __this, HmdMatrix34_t_t664273062 * ___pmatSeatedZeroPoseToRawTrackingPose0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetLiveSeatedZeroPoseToRawTrackingPose::EndInvoke(Valve.VR.HmdMatrix34_t&,System.IAsyncResult)
extern "C"  bool _GetLiveSeatedZeroPoseToRawTrackingPose_EndInvoke_m271738580 (_GetLiveSeatedZeroPoseToRawTrackingPose_t1524066641 * __this, HmdMatrix34_t_t664273062 * ___pmatSeatedZeroPoseToRawTrackingPose0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
