﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_1_gen569904416MethodDeclarations.h"

// System.Void SteamVR_Events/Event`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::.ctor()
#define Event_1__ctor_m2660236022(__this, method) ((  void (*) (Event_1_t3288129385 *, const MethodInfo*))Event_1__ctor_m28252523_gshared)(__this, method)
// System.Void SteamVR_Events/Event`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::Listen(UnityEngine.Events.UnityAction`1<T>)
#define Event_1_Listen_m3194407871(__this, ___action0, method) ((  void (*) (Event_1_t3288129385 *, UnityAction_1_t2479292719 *, const MethodInfo*))Event_1_Listen_m1961284276_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::Remove(UnityEngine.Events.UnityAction`1<T>)
#define Event_1_Remove_m1445766104(__this, ___action0, method) ((  void (*) (Event_1_t3288129385 *, UnityAction_1_t2479292719 *, const MethodInfo*))Event_1_Remove_m1960904225_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::Send(T)
#define Event_1_Send_m284515992(__this, ___arg00, method) ((  void (*) (Event_1_t3288129385 *, TeleportMarkerBase_t1112706968 *, const MethodInfo*))Event_1_Send_m2836426603_gshared)(__this, ___arg00, method)
