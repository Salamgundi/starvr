﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.PanelMenu.PanelMenuDemoSphere
struct PanelMenuDemoSphere_t1711579793;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.Examples.PanelMenu.PanelMenuDemoSphere::.ctor()
extern "C"  void PanelMenuDemoSphere__ctor_m4080746931 (PanelMenuDemoSphere_t1711579793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.PanelMenu.PanelMenuDemoSphere::UpdateSliderValue(System.Single)
extern "C"  void PanelMenuDemoSphere_UpdateSliderValue_m2969686481 (PanelMenuDemoSphere_t1711579793 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
