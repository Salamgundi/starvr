﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetDeviceToAbsoluteTrackingPose
struct _GetDeviceToAbsoluteTrackingPose_t1432625068;
// System.Object
struct Il2CppObject;
// Valve.VR.TrackedDevicePose_t[]
struct TrackedDevicePose_tU5BU5D_t2897272049;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingUniverseOrigin1464400093.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetDeviceToAbsoluteTrackingPose::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetDeviceToAbsoluteTrackingPose__ctor_m1617064779 (_GetDeviceToAbsoluteTrackingPose_t1432625068 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_GetDeviceToAbsoluteTrackingPose::Invoke(Valve.VR.ETrackingUniverseOrigin,System.Single,Valve.VR.TrackedDevicePose_t[],System.UInt32)
extern "C"  void _GetDeviceToAbsoluteTrackingPose_Invoke_m3408239547 (_GetDeviceToAbsoluteTrackingPose_t1432625068 * __this, int32_t ___eOrigin0, float ___fPredictedSecondsToPhotonsFromNow1, TrackedDevicePose_tU5BU5D_t2897272049* ___pTrackedDevicePoseArray2, uint32_t ___unTrackedDevicePoseArrayCount3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetDeviceToAbsoluteTrackingPose::BeginInvoke(Valve.VR.ETrackingUniverseOrigin,System.Single,Valve.VR.TrackedDevicePose_t[],System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetDeviceToAbsoluteTrackingPose_BeginInvoke_m3795648090 (_GetDeviceToAbsoluteTrackingPose_t1432625068 * __this, int32_t ___eOrigin0, float ___fPredictedSecondsToPhotonsFromNow1, TrackedDevicePose_tU5BU5D_t2897272049* ___pTrackedDevicePoseArray2, uint32_t ___unTrackedDevicePoseArrayCount3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_GetDeviceToAbsoluteTrackingPose::EndInvoke(System.IAsyncResult)
extern "C"  void _GetDeviceToAbsoluteTrackingPose_EndInvoke_m3064134549 (_GetDeviceToAbsoluteTrackingPose_t1432625068 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
