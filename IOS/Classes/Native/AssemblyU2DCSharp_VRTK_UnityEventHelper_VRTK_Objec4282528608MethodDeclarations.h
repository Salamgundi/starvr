﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t4282528608;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents/UnityObjectEvent::.ctor()
extern "C"  void UnityObjectEvent__ctor_m4164473841 (UnityObjectEvent_t4282528608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
