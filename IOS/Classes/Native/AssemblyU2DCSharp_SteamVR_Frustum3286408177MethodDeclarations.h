﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Frustum
struct SteamVR_Frustum_t3286408177;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Frustum::.ctor()
extern "C"  void SteamVR_Frustum__ctor_m3226904876 (SteamVR_Frustum_t3286408177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Frustum::UpdateModel()
extern "C"  void SteamVR_Frustum_UpdateModel_m418477062 (SteamVR_Frustum_t3286408177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Frustum::OnDeviceConnected(System.Int32,System.Boolean)
extern "C"  void SteamVR_Frustum_OnDeviceConnected_m3488487414 (SteamVR_Frustum_t3286408177 * __this, int32_t ___i0, bool ___connected1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Frustum::OnEnable()
extern "C"  void SteamVR_Frustum_OnEnable_m2178177804 (SteamVR_Frustum_t3286408177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Frustum::OnDisable()
extern "C"  void SteamVR_Frustum_OnDisable_m821561757 (SteamVR_Frustum_t3286408177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
