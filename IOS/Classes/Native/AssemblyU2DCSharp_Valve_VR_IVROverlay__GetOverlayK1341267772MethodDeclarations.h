﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayKey
struct _GetOverlayKey_t1341267772;
// System.Object
struct Il2CppObject;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayKey::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayKey__ctor_m3281608815 (_GetOverlayKey_t1341267772 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVROverlay/_GetOverlayKey::Invoke(System.UInt64,System.Text.StringBuilder,System.UInt32,Valve.VR.EVROverlayError&)
extern "C"  uint32_t _GetOverlayKey_Invoke_m4080167232 (_GetOverlayKey_t1341267772 * __this, uint64_t ___ulOverlayHandle0, StringBuilder_t1221177846 * ___pchValue1, uint32_t ___unBufferSize2, int32_t* ___pError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayKey::BeginInvoke(System.UInt64,System.Text.StringBuilder,System.UInt32,Valve.VR.EVROverlayError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayKey_BeginInvoke_m2376808886 (_GetOverlayKey_t1341267772 * __this, uint64_t ___ulOverlayHandle0, StringBuilder_t1221177846 * ___pchValue1, uint32_t ___unBufferSize2, int32_t* ___pError3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVROverlay/_GetOverlayKey::EndInvoke(Valve.VR.EVROverlayError&,System.IAsyncResult)
extern "C"  uint32_t _GetOverlayKey_EndInvoke_m2551808127 (_GetOverlayKey_t1341267772 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
