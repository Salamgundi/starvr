﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen865599137.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREventType6846875.h"

// System.Void System.Array/InternalEnumerator`1<Valve.VR.EVREventType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1342052172_gshared (InternalEnumerator_1_t865599137 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1342052172(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t865599137 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1342052172_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.EVREventType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3964452764_gshared (InternalEnumerator_1_t865599137 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3964452764(__this, method) ((  void (*) (InternalEnumerator_1_t865599137 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3964452764_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Valve.VR.EVREventType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m456168282_gshared (InternalEnumerator_1_t865599137 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m456168282(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t865599137 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m456168282_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.EVREventType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m569371783_gshared (InternalEnumerator_1_t865599137 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m569371783(__this, method) ((  void (*) (InternalEnumerator_1_t865599137 *, const MethodInfo*))InternalEnumerator_1_Dispose_m569371783_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Valve.VR.EVREventType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3956717700_gshared (InternalEnumerator_1_t865599137 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3956717700(__this, method) ((  bool (*) (InternalEnumerator_1_t865599137 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3956717700_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Valve.VR.EVREventType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m668670275_gshared (InternalEnumerator_1_t865599137 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m668670275(__this, method) ((  int32_t (*) (InternalEnumerator_1_t865599137 *, const MethodInfo*))InternalEnumerator_1_get_Current_m668670275_gshared)(__this, method)
