﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Util/<WrapCoroutine>c__Iterator0
struct U3CWrapCoroutineU3Ec__Iterator0_t4183039461;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.Util/<WrapCoroutine>c__Iterator0::.ctor()
extern "C"  void U3CWrapCoroutineU3Ec__Iterator0__ctor_m1030865626 (U3CWrapCoroutineU3Ec__Iterator0_t4183039461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Util/<WrapCoroutine>c__Iterator0::MoveNext()
extern "C"  bool U3CWrapCoroutineU3Ec__Iterator0_MoveNext_m4149582662 (U3CWrapCoroutineU3Ec__Iterator0_t4183039461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.Util/<WrapCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWrapCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1151046688 (U3CWrapCoroutineU3Ec__Iterator0_t4183039461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.Util/<WrapCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWrapCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1248670728 (U3CWrapCoroutineU3Ec__Iterator0_t4183039461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Util/<WrapCoroutine>c__Iterator0::Dispose()
extern "C"  void U3CWrapCoroutineU3Ec__Iterator0_Dispose_m2865911895 (U3CWrapCoroutineU3Ec__Iterator0_t4183039461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Util/<WrapCoroutine>c__Iterator0::Reset()
extern "C"  void U3CWrapCoroutineU3Ec__Iterator0_Reset_m1498365989 (U3CWrapCoroutineU3Ec__Iterator0_t4183039461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
