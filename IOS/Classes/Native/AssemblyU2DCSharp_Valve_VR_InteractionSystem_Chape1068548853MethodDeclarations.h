﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ChaperoneInfo
struct ChaperoneInfo_t1068548853;
// SteamVR_Events/Action
struct Action_t1836998693;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"

// System.Void Valve.VR.InteractionSystem.ChaperoneInfo::.ctor()
extern "C"  void ChaperoneInfo__ctor_m523571777 (ChaperoneInfo_t1068548853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.ChaperoneInfo::get_initialized()
extern "C"  bool ChaperoneInfo_get_initialized_m2980756654 (ChaperoneInfo_t1068548853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ChaperoneInfo::set_initialized(System.Boolean)
extern "C"  void ChaperoneInfo_set_initialized_m1915092831 (ChaperoneInfo_t1068548853 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.InteractionSystem.ChaperoneInfo::get_playAreaSizeX()
extern "C"  float ChaperoneInfo_get_playAreaSizeX_m2548356546 (ChaperoneInfo_t1068548853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ChaperoneInfo::set_playAreaSizeX(System.Single)
extern "C"  void ChaperoneInfo_set_playAreaSizeX_m105678135 (ChaperoneInfo_t1068548853 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.InteractionSystem.ChaperoneInfo::get_playAreaSizeZ()
extern "C"  float ChaperoneInfo_get_playAreaSizeZ_m2548356480 (ChaperoneInfo_t1068548853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ChaperoneInfo::set_playAreaSizeZ(System.Single)
extern "C"  void ChaperoneInfo_set_playAreaSizeZ_m3429850357 (ChaperoneInfo_t1068548853 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.ChaperoneInfo::get_roomscale()
extern "C"  bool ChaperoneInfo_get_roomscale_m1035664319 (ChaperoneInfo_t1068548853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ChaperoneInfo::set_roomscale(System.Boolean)
extern "C"  void ChaperoneInfo_set_roomscale_m1970209884 (ChaperoneInfo_t1068548853 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action Valve.VR.InteractionSystem.ChaperoneInfo::InitializedAction(UnityEngine.Events.UnityAction)
extern "C"  Action_t1836998693 * ChaperoneInfo_InitializedAction_m2370837664 (Il2CppObject * __this /* static, unused */, UnityAction_t4025899511 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.InteractionSystem.ChaperoneInfo Valve.VR.InteractionSystem.ChaperoneInfo::get_instance()
extern "C"  ChaperoneInfo_t1068548853 * ChaperoneInfo_get_instance_m3378780446 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.ChaperoneInfo::Start()
extern "C"  Il2CppObject * ChaperoneInfo_Start_m371384047 (ChaperoneInfo_t1068548853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ChaperoneInfo::.cctor()
extern "C"  void ChaperoneInfo__cctor_m289789700 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
