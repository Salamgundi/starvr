﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.CVRChaperone
struct CVRChaperone_t441701222;
// Valve.VR.InteractionSystem.ChaperoneInfo
struct ChaperoneInfo_t1068548853;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.ChaperoneInfo/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3843791664  : public Il2CppObject
{
public:
	// Valve.VR.CVRChaperone Valve.VR.InteractionSystem.ChaperoneInfo/<Start>c__Iterator0::<chaperone>__0
	CVRChaperone_t441701222 * ___U3CchaperoneU3E__0_0;
	// System.Single Valve.VR.InteractionSystem.ChaperoneInfo/<Start>c__Iterator0::<px>__1
	float ___U3CpxU3E__1_1;
	// System.Single Valve.VR.InteractionSystem.ChaperoneInfo/<Start>c__Iterator0::<pz>__2
	float ___U3CpzU3E__2_2;
	// Valve.VR.InteractionSystem.ChaperoneInfo Valve.VR.InteractionSystem.ChaperoneInfo/<Start>c__Iterator0::$this
	ChaperoneInfo_t1068548853 * ___U24this_3;
	// System.Object Valve.VR.InteractionSystem.ChaperoneInfo/<Start>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean Valve.VR.InteractionSystem.ChaperoneInfo/<Start>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Valve.VR.InteractionSystem.ChaperoneInfo/<Start>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CchaperoneU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3843791664, ___U3CchaperoneU3E__0_0)); }
	inline CVRChaperone_t441701222 * get_U3CchaperoneU3E__0_0() const { return ___U3CchaperoneU3E__0_0; }
	inline CVRChaperone_t441701222 ** get_address_of_U3CchaperoneU3E__0_0() { return &___U3CchaperoneU3E__0_0; }
	inline void set_U3CchaperoneU3E__0_0(CVRChaperone_t441701222 * value)
	{
		___U3CchaperoneU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchaperoneU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CpxU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3843791664, ___U3CpxU3E__1_1)); }
	inline float get_U3CpxU3E__1_1() const { return ___U3CpxU3E__1_1; }
	inline float* get_address_of_U3CpxU3E__1_1() { return &___U3CpxU3E__1_1; }
	inline void set_U3CpxU3E__1_1(float value)
	{
		___U3CpxU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CpzU3E__2_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3843791664, ___U3CpzU3E__2_2)); }
	inline float get_U3CpzU3E__2_2() const { return ___U3CpzU3E__2_2; }
	inline float* get_address_of_U3CpzU3E__2_2() { return &___U3CpzU3E__2_2; }
	inline void set_U3CpzU3E__2_2(float value)
	{
		___U3CpzU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3843791664, ___U24this_3)); }
	inline ChaperoneInfo_t1068548853 * get_U24this_3() const { return ___U24this_3; }
	inline ChaperoneInfo_t1068548853 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ChaperoneInfo_t1068548853 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3843791664, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3843791664, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3843791664, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
