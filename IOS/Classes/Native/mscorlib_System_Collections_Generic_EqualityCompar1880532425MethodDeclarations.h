﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct DefaultComparer_t1880532425;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_258602264.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::.ctor()
extern "C"  void DefaultComparer__ctor_m2973309532_gshared (DefaultComparer_t1880532425 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2973309532(__this, method) ((  void (*) (DefaultComparer_t1880532425 *, const MethodInfo*))DefaultComparer__ctor_m2973309532_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m630804995_gshared (DefaultComparer_t1880532425 * __this, KeyValuePair_2_t258602264  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m630804995(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1880532425 *, KeyValuePair_2_t258602264 , const MethodInfo*))DefaultComparer_GetHashCode_m630804995_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4225610563_gshared (DefaultComparer_t1880532425 * __this, KeyValuePair_2_t258602264  ___x0, KeyValuePair_2_t258602264  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m4225610563(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1880532425 *, KeyValuePair_2_t258602264 , KeyValuePair_2_t258602264 , const MethodInfo*))DefaultComparer_Equals_m4225610563_gshared)(__this, ___x0, ___y1, method)
