﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayName
struct _GetOverlayName_t221395366;
// System.Object
struct Il2CppObject;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayName::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayName__ctor_m2069618847 (_GetOverlayName_t221395366 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVROverlay/_GetOverlayName::Invoke(System.UInt64,System.Text.StringBuilder,System.UInt32,Valve.VR.EVROverlayError&)
extern "C"  uint32_t _GetOverlayName_Invoke_m3733961544 (_GetOverlayName_t221395366 * __this, uint64_t ___ulOverlayHandle0, StringBuilder_t1221177846 * ___pchValue1, uint32_t ___unBufferSize2, int32_t* ___pError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayName::BeginInvoke(System.UInt64,System.Text.StringBuilder,System.UInt32,Valve.VR.EVROverlayError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayName_BeginInvoke_m1777353508 (_GetOverlayName_t221395366 * __this, uint64_t ___ulOverlayHandle0, StringBuilder_t1221177846 * ___pchValue1, uint32_t ___unBufferSize2, int32_t* ___pError3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVROverlay/_GetOverlayName::EndInvoke(Valve.VR.EVROverlayError&,System.IAsyncResult)
extern "C"  uint32_t _GetOverlayName_EndInvoke_m1308951003 (_GetOverlayName_t221395366 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
