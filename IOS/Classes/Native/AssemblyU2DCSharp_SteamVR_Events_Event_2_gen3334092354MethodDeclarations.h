﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Event`2<System.Object,System.Object>
struct Event_2_t3334092354;
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t3784905282;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void SteamVR_Events/Event`2<System.Object,System.Object>::.ctor()
extern "C"  void Event_2__ctor_m3642705320_gshared (Event_2_t3334092354 * __this, const MethodInfo* method);
#define Event_2__ctor_m3642705320(__this, method) ((  void (*) (Event_2_t3334092354 *, const MethodInfo*))Event_2__ctor_m3642705320_gshared)(__this, method)
// System.Void SteamVR_Events/Event`2<System.Object,System.Object>::Listen(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Event_2_Listen_m2446347801_gshared (Event_2_t3334092354 * __this, UnityAction_2_t3784905282 * ___action0, const MethodInfo* method);
#define Event_2_Listen_m2446347801(__this, ___action0, method) ((  void (*) (Event_2_t3334092354 *, UnityAction_2_t3784905282 *, const MethodInfo*))Event_2_Listen_m2446347801_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`2<System.Object,System.Object>::Remove(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Event_2_Remove_m197184152_gshared (Event_2_t3334092354 * __this, UnityAction_2_t3784905282 * ___action0, const MethodInfo* method);
#define Event_2_Remove_m197184152(__this, ___action0, method) ((  void (*) (Event_2_t3334092354 *, UnityAction_2_t3784905282 *, const MethodInfo*))Event_2_Remove_m197184152_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`2<System.Object,System.Object>::Send(T0,T1)
extern "C"  void Event_2_Send_m2307785703_gshared (Event_2_t3334092354 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
#define Event_2_Send_m2307785703(__this, ___arg00, ___arg11, method) ((  void (*) (Event_2_t3334092354 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Event_2_Send_m2307785703_gshared)(__this, ___arg00, ___arg11, method)
