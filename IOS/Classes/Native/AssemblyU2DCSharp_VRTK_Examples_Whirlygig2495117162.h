﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Whirlygig
struct  Whirlygig_t2495117162  : public VRTK_InteractableObject_t2604188111
{
public:
	// System.Single VRTK.Examples.Whirlygig::spinSpeed
	float ___spinSpeed_45;
	// UnityEngine.Transform VRTK.Examples.Whirlygig::rotator
	Transform_t3275118058 * ___rotator_46;

public:
	inline static int32_t get_offset_of_spinSpeed_45() { return static_cast<int32_t>(offsetof(Whirlygig_t2495117162, ___spinSpeed_45)); }
	inline float get_spinSpeed_45() const { return ___spinSpeed_45; }
	inline float* get_address_of_spinSpeed_45() { return &___spinSpeed_45; }
	inline void set_spinSpeed_45(float value)
	{
		___spinSpeed_45 = value;
	}

	inline static int32_t get_offset_of_rotator_46() { return static_cast<int32_t>(offsetof(Whirlygig_t2495117162, ___rotator_46)); }
	inline Transform_t3275118058 * get_rotator_46() const { return ___rotator_46; }
	inline Transform_t3275118058 ** get_address_of_rotator_46() { return &___rotator_46; }
	inline void set_rotator_46(Transform_t3275118058 * value)
	{
		___rotator_46 = value;
		Il2CppCodeGenWriteBarrier(&___rotator_46, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
