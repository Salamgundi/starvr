﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_GetLastPoseForTrackedDeviceIndex
struct _GetLastPoseForTrackedDeviceIndex_t3158332482;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRCompositorError3948578210.h"
#include "AssemblyU2DCSharp_Valve_VR_TrackedDevicePose_t1668551120.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_GetLastPoseForTrackedDeviceIndex::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetLastPoseForTrackedDeviceIndex__ctor_m1864826521 (_GetLastPoseForTrackedDeviceIndex_t3158332482 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.IVRCompositor/_GetLastPoseForTrackedDeviceIndex::Invoke(System.UInt32,Valve.VR.TrackedDevicePose_t&,Valve.VR.TrackedDevicePose_t&)
extern "C"  int32_t _GetLastPoseForTrackedDeviceIndex_Invoke_m4168243680 (_GetLastPoseForTrackedDeviceIndex_t3158332482 * __this, uint32_t ___unDeviceIndex0, TrackedDevicePose_t_t1668551120 * ___pOutputPose1, TrackedDevicePose_t_t1668551120 * ___pOutputGamePose2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_GetLastPoseForTrackedDeviceIndex::BeginInvoke(System.UInt32,Valve.VR.TrackedDevicePose_t&,Valve.VR.TrackedDevicePose_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetLastPoseForTrackedDeviceIndex_BeginInvoke_m605658032 (_GetLastPoseForTrackedDeviceIndex_t3158332482 * __this, uint32_t ___unDeviceIndex0, TrackedDevicePose_t_t1668551120 * ___pOutputPose1, TrackedDevicePose_t_t1668551120 * ___pOutputGamePose2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.IVRCompositor/_GetLastPoseForTrackedDeviceIndex::EndInvoke(Valve.VR.TrackedDevicePose_t&,Valve.VR.TrackedDevicePose_t&,System.IAsyncResult)
extern "C"  int32_t _GetLastPoseForTrackedDeviceIndex_EndInvoke_m1271562638 (_GetLastPoseForTrackedDeviceIndex_t3158332482 * __this, TrackedDevicePose_t_t1668551120 * ___pOutputPose0, TrackedDevicePose_t_t1668551120 * ___pOutputGamePose1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
