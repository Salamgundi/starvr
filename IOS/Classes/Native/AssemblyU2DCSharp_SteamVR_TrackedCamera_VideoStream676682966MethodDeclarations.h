﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_TrackedCamera/VideoStream
struct VideoStream_t676682966;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_TrackedCamera/VideoStream::.ctor(System.UInt32)
extern "C"  void VideoStream__ctor_m2470850995 (VideoStream_t676682966 * __this, uint32_t ___deviceIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SteamVR_TrackedCamera/VideoStream::get_deviceIndex()
extern "C"  uint32_t VideoStream_get_deviceIndex_m2328551707 (VideoStream_t676682966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedCamera/VideoStream::set_deviceIndex(System.UInt32)
extern "C"  void VideoStream_set_deviceIndex_m4229787498 (VideoStream_t676682966 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 SteamVR_TrackedCamera/VideoStream::get_handle()
extern "C"  uint64_t VideoStream_get_handle_m1814001726 (VideoStream_t676682966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_TrackedCamera/VideoStream::get_hasCamera()
extern "C"  bool VideoStream_get_hasCamera_m471126169 (VideoStream_t676682966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 SteamVR_TrackedCamera/VideoStream::Acquire()
extern "C"  uint64_t VideoStream_Acquire_m358936815 (VideoStream_t676682966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 SteamVR_TrackedCamera/VideoStream::Release()
extern "C"  uint64_t VideoStream_Release_m3937791808 (VideoStream_t676682966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
