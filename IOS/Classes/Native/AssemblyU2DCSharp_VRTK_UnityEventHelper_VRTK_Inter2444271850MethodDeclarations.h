﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_InteractTouch_UnityEvents
struct VRTK_InteractTouch_UnityEvents_t2444271850;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventArgs771291242.h"

// System.Void VRTK.UnityEventHelper.VRTK_InteractTouch_UnityEvents::.ctor()
extern "C"  void VRTK_InteractTouch_UnityEvents__ctor_m4277180917 (VRTK_InteractTouch_UnityEvents_t2444271850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractTouch_UnityEvents::SetInteractTouch()
extern "C"  void VRTK_InteractTouch_UnityEvents_SetInteractTouch_m97912938 (VRTK_InteractTouch_UnityEvents_t2444271850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractTouch_UnityEvents::OnEnable()
extern "C"  void VRTK_InteractTouch_UnityEvents_OnEnable_m3747275253 (VRTK_InteractTouch_UnityEvents_t2444271850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractTouch_UnityEvents::ControllerTouchInteractableObject(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_InteractTouch_UnityEvents_ControllerTouchInteractableObject_m4082482132 (VRTK_InteractTouch_UnityEvents_t2444271850 * __this, Il2CppObject * ___o0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractTouch_UnityEvents::ControllerUntouchInteractableObject(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_InteractTouch_UnityEvents_ControllerUntouchInteractableObject_m2308062021 (VRTK_InteractTouch_UnityEvents_t2444271850 * __this, Il2CppObject * ___o0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractTouch_UnityEvents::OnDisable()
extern "C"  void VRTK_InteractTouch_UnityEvents_OnDisable_m3579235200 (VRTK_InteractTouch_UnityEvents_t2444271850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
