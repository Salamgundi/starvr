﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.AutoRotation
struct  AutoRotation_t3877681447  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VRTK.Examples.AutoRotation::degPerSec
	float ___degPerSec_2;
	// UnityEngine.Vector3 VRTK.Examples.AutoRotation::rotAxis
	Vector3_t2243707580  ___rotAxis_3;

public:
	inline static int32_t get_offset_of_degPerSec_2() { return static_cast<int32_t>(offsetof(AutoRotation_t3877681447, ___degPerSec_2)); }
	inline float get_degPerSec_2() const { return ___degPerSec_2; }
	inline float* get_address_of_degPerSec_2() { return &___degPerSec_2; }
	inline void set_degPerSec_2(float value)
	{
		___degPerSec_2 = value;
	}

	inline static int32_t get_offset_of_rotAxis_3() { return static_cast<int32_t>(offsetof(AutoRotation_t3877681447, ___rotAxis_3)); }
	inline Vector3_t2243707580  get_rotAxis_3() const { return ___rotAxis_3; }
	inline Vector3_t2243707580 * get_address_of_rotAxis_3() { return &___rotAxis_3; }
	inline void set_rotAxis_3(Vector3_t2243707580  value)
	{
		___rotAxis_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
