﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SteamVR_Events/Event
struct Event_t1855872343;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;

#include "AssemblyU2DCSharp_SteamVR_Events_Action1836998693.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SteamVR_Events/ActionNoArgs
struct  ActionNoArgs_t953081287  : public Action_t1836998693
{
public:
	// SteamVR_Events/Event SteamVR_Events/ActionNoArgs::_event
	Event_t1855872343 * ____event_0;
	// UnityEngine.Events.UnityAction SteamVR_Events/ActionNoArgs::action
	UnityAction_t4025899511 * ___action_1;

public:
	inline static int32_t get_offset_of__event_0() { return static_cast<int32_t>(offsetof(ActionNoArgs_t953081287, ____event_0)); }
	inline Event_t1855872343 * get__event_0() const { return ____event_0; }
	inline Event_t1855872343 ** get_address_of__event_0() { return &____event_0; }
	inline void set__event_0(Event_t1855872343 * value)
	{
		____event_0 = value;
		Il2CppCodeGenWriteBarrier(&____event_0, value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(ActionNoArgs_t953081287, ___action_1)); }
	inline UnityAction_t4025899511 * get_action_1() const { return ___action_1; }
	inline UnityAction_t4025899511 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(UnityAction_t4025899511 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
