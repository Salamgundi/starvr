﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_GetWorkingStandingZeroPoseToRawTrackingPose
struct _GetWorkingStandingZeroPoseToRawTrackingPose_t2605213384;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_GetWorkingStandingZeroPoseToRawTrackingPose::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetWorkingStandingZeroPoseToRawTrackingPose__ctor_m1571046433 (_GetWorkingStandingZeroPoseToRawTrackingPose_t2605213384 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetWorkingStandingZeroPoseToRawTrackingPose::Invoke(Valve.VR.HmdMatrix34_t&)
extern "C"  bool _GetWorkingStandingZeroPoseToRawTrackingPose_Invoke_m3727056141 (_GetWorkingStandingZeroPoseToRawTrackingPose_t2605213384 * __this, HmdMatrix34_t_t664273062 * ___pmatStandingZeroPoseToRawTrackingPose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_GetWorkingStandingZeroPoseToRawTrackingPose::BeginInvoke(Valve.VR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetWorkingStandingZeroPoseToRawTrackingPose_BeginInvoke_m2534876758 (_GetWorkingStandingZeroPoseToRawTrackingPose_t2605213384 * __this, HmdMatrix34_t_t664273062 * ___pmatStandingZeroPoseToRawTrackingPose0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetWorkingStandingZeroPoseToRawTrackingPose::EndInvoke(Valve.VR.HmdMatrix34_t&,System.IAsyncResult)
extern "C"  bool _GetWorkingStandingZeroPoseToRawTrackingPose_EndInvoke_m1186166515 (_GetWorkingStandingZeroPoseToRawTrackingPose_t2605213384 * __this, HmdMatrix34_t_t664273062 * ___pmatStandingZeroPoseToRawTrackingPose0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
