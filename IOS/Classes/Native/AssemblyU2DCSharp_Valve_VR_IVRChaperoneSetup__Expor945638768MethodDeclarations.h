﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_ExportLiveToBuffer
struct _ExportLiveToBuffer_t945638768;
// System.Object
struct Il2CppObject;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_ExportLiveToBuffer::.ctor(System.Object,System.IntPtr)
extern "C"  void _ExportLiveToBuffer__ctor_m1632317561 (_ExportLiveToBuffer_t945638768 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_ExportLiveToBuffer::Invoke(System.Text.StringBuilder,System.UInt32&)
extern "C"  bool _ExportLiveToBuffer_Invoke_m1511854819 (_ExportLiveToBuffer_t945638768 * __this, StringBuilder_t1221177846 * ___pBuffer0, uint32_t* ___pnBufferLength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_ExportLiveToBuffer::BeginInvoke(System.Text.StringBuilder,System.UInt32&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ExportLiveToBuffer_BeginInvoke_m4209750970 (_ExportLiveToBuffer_t945638768 * __this, StringBuilder_t1221177846 * ___pBuffer0, uint32_t* ___pnBufferLength1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_ExportLiveToBuffer::EndInvoke(System.UInt32&,System.IAsyncResult)
extern "C"  bool _ExportLiveToBuffer_EndInvoke_m1647124725 (_ExportLiveToBuffer_t945638768 * __this, uint32_t* ___pnBufferLength0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
