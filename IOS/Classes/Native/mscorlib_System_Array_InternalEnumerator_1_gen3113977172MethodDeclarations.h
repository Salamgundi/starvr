﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3113977172.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdVector3_t2255224910.h"

// System.Void System.Array/InternalEnumerator`1<Valve.VR.HmdVector3_t>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3635424575_gshared (InternalEnumerator_1_t3113977172 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3635424575(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3113977172 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3635424575_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.HmdVector3_t>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m881861559_gshared (InternalEnumerator_1_t3113977172 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m881861559(__this, method) ((  void (*) (InternalEnumerator_1_t3113977172 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m881861559_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Valve.VR.HmdVector3_t>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m739130759_gshared (InternalEnumerator_1_t3113977172 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m739130759(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3113977172 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m739130759_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.HmdVector3_t>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2288127970_gshared (InternalEnumerator_1_t3113977172 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2288127970(__this, method) ((  void (*) (InternalEnumerator_1_t3113977172 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2288127970_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Valve.VR.HmdVector3_t>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3551099091_gshared (InternalEnumerator_1_t3113977172 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3551099091(__this, method) ((  bool (*) (InternalEnumerator_1_t3113977172 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3551099091_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Valve.VR.HmdVector3_t>::get_Current()
extern "C"  HmdVector3_t_t2255224910  InternalEnumerator_1_get_Current_m2739417534_gshared (InternalEnumerator_1_t3113977172 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2739417534(__this, method) ((  HmdVector3_t_t2255224910  (*) (InternalEnumerator_1_t3113977172 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2739417534_gshared)(__this, method)
