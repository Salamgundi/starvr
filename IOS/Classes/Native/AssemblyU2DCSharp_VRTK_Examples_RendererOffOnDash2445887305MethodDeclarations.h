﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.RendererOffOnDash
struct RendererOffOnDash_t2445887305;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_DashTeleportEventArgs2197253242.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.Examples.RendererOffOnDash::.ctor()
extern "C"  void RendererOffOnDash__ctor_m2240652400 (RendererOffOnDash_t2445887305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RendererOffOnDash::OnEnable()
extern "C"  void RendererOffOnDash_OnEnable_m869640876 (RendererOffOnDash_t2445887305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RendererOffOnDash::OnDisable()
extern "C"  void RendererOffOnDash_OnDisable_m3029106939 (RendererOffOnDash_t2445887305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RendererOffOnDash::RendererOff(System.Object,VRTK.DashTeleportEventArgs)
extern "C"  void RendererOffOnDash_RendererOff_m3600160191 (RendererOffOnDash_t2445887305 * __this, Il2CppObject * ___sender0, DashTeleportEventArgs_t2197253242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RendererOffOnDash::RendererOn(System.Object,VRTK.DashTeleportEventArgs)
extern "C"  void RendererOffOnDash_RendererOn_m3531074497 (RendererOffOnDash_t2445887305 * __this, Il2CppObject * ___sender0, DashTeleportEventArgs_t2197253242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RendererOffOnDash::SwitchRenderer(UnityEngine.GameObject,System.Boolean)
extern "C"  void RendererOffOnDash_SwitchRenderer_m598264462 (RendererOffOnDash_t2445887305 * __this, GameObject_t1756533147 * ___go0, bool ___enable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
