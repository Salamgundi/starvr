﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.VelocityEstimator
struct  VelocityEstimator_t1153298725  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Valve.VR.InteractionSystem.VelocityEstimator::velocityAverageFrames
	int32_t ___velocityAverageFrames_2;
	// System.Int32 Valve.VR.InteractionSystem.VelocityEstimator::angularVelocityAverageFrames
	int32_t ___angularVelocityAverageFrames_3;
	// System.Boolean Valve.VR.InteractionSystem.VelocityEstimator::estimateOnAwake
	bool ___estimateOnAwake_4;
	// UnityEngine.Coroutine Valve.VR.InteractionSystem.VelocityEstimator::routine
	Coroutine_t2299508840 * ___routine_5;
	// System.Int32 Valve.VR.InteractionSystem.VelocityEstimator::sampleCount
	int32_t ___sampleCount_6;
	// UnityEngine.Vector3[] Valve.VR.InteractionSystem.VelocityEstimator::velocitySamples
	Vector3U5BU5D_t1172311765* ___velocitySamples_7;
	// UnityEngine.Vector3[] Valve.VR.InteractionSystem.VelocityEstimator::angularVelocitySamples
	Vector3U5BU5D_t1172311765* ___angularVelocitySamples_8;

public:
	inline static int32_t get_offset_of_velocityAverageFrames_2() { return static_cast<int32_t>(offsetof(VelocityEstimator_t1153298725, ___velocityAverageFrames_2)); }
	inline int32_t get_velocityAverageFrames_2() const { return ___velocityAverageFrames_2; }
	inline int32_t* get_address_of_velocityAverageFrames_2() { return &___velocityAverageFrames_2; }
	inline void set_velocityAverageFrames_2(int32_t value)
	{
		___velocityAverageFrames_2 = value;
	}

	inline static int32_t get_offset_of_angularVelocityAverageFrames_3() { return static_cast<int32_t>(offsetof(VelocityEstimator_t1153298725, ___angularVelocityAverageFrames_3)); }
	inline int32_t get_angularVelocityAverageFrames_3() const { return ___angularVelocityAverageFrames_3; }
	inline int32_t* get_address_of_angularVelocityAverageFrames_3() { return &___angularVelocityAverageFrames_3; }
	inline void set_angularVelocityAverageFrames_3(int32_t value)
	{
		___angularVelocityAverageFrames_3 = value;
	}

	inline static int32_t get_offset_of_estimateOnAwake_4() { return static_cast<int32_t>(offsetof(VelocityEstimator_t1153298725, ___estimateOnAwake_4)); }
	inline bool get_estimateOnAwake_4() const { return ___estimateOnAwake_4; }
	inline bool* get_address_of_estimateOnAwake_4() { return &___estimateOnAwake_4; }
	inline void set_estimateOnAwake_4(bool value)
	{
		___estimateOnAwake_4 = value;
	}

	inline static int32_t get_offset_of_routine_5() { return static_cast<int32_t>(offsetof(VelocityEstimator_t1153298725, ___routine_5)); }
	inline Coroutine_t2299508840 * get_routine_5() const { return ___routine_5; }
	inline Coroutine_t2299508840 ** get_address_of_routine_5() { return &___routine_5; }
	inline void set_routine_5(Coroutine_t2299508840 * value)
	{
		___routine_5 = value;
		Il2CppCodeGenWriteBarrier(&___routine_5, value);
	}

	inline static int32_t get_offset_of_sampleCount_6() { return static_cast<int32_t>(offsetof(VelocityEstimator_t1153298725, ___sampleCount_6)); }
	inline int32_t get_sampleCount_6() const { return ___sampleCount_6; }
	inline int32_t* get_address_of_sampleCount_6() { return &___sampleCount_6; }
	inline void set_sampleCount_6(int32_t value)
	{
		___sampleCount_6 = value;
	}

	inline static int32_t get_offset_of_velocitySamples_7() { return static_cast<int32_t>(offsetof(VelocityEstimator_t1153298725, ___velocitySamples_7)); }
	inline Vector3U5BU5D_t1172311765* get_velocitySamples_7() const { return ___velocitySamples_7; }
	inline Vector3U5BU5D_t1172311765** get_address_of_velocitySamples_7() { return &___velocitySamples_7; }
	inline void set_velocitySamples_7(Vector3U5BU5D_t1172311765* value)
	{
		___velocitySamples_7 = value;
		Il2CppCodeGenWriteBarrier(&___velocitySamples_7, value);
	}

	inline static int32_t get_offset_of_angularVelocitySamples_8() { return static_cast<int32_t>(offsetof(VelocityEstimator_t1153298725, ___angularVelocitySamples_8)); }
	inline Vector3U5BU5D_t1172311765* get_angularVelocitySamples_8() const { return ___angularVelocitySamples_8; }
	inline Vector3U5BU5D_t1172311765** get_address_of_angularVelocitySamples_8() { return &___angularVelocitySamples_8; }
	inline void set_angularVelocitySamples_8(Vector3U5BU5D_t1172311765* value)
	{
		___angularVelocitySamples_8 = value;
		Il2CppCodeGenWriteBarrier(&___angularVelocitySamples_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
