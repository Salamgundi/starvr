﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.UI_Interactions
struct UI_Interactions_t2908460892;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"

// System.Void VRTK.Examples.UI_Interactions::.ctor()
extern "C"  void UI_Interactions__ctor_m2492758397 (UI_Interactions_t2908460892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.UI_Interactions::Button_Red()
extern "C"  void UI_Interactions_Button_Red_m1770165267 (UI_Interactions_t2908460892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.UI_Interactions::Button_Pink()
extern "C"  void UI_Interactions_Button_Pink_m3009451580 (UI_Interactions_t2908460892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.UI_Interactions::Toggle(System.Boolean)
extern "C"  void UI_Interactions_Toggle_m1546314612 (UI_Interactions_t2908460892 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.UI_Interactions::Dropdown(System.Int32)
extern "C"  void UI_Interactions_Dropdown_m3913230899 (UI_Interactions_t2908460892 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.UI_Interactions::SetDropText(UnityEngine.EventSystems.BaseEventData)
extern "C"  void UI_Interactions_SetDropText_m1051260033 (UI_Interactions_t2908460892 * __this, BaseEventData_t2681005625 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.UI_Interactions::CreateCanvas()
extern "C"  void UI_Interactions_CreateCanvas_m2799857253 (UI_Interactions_t2908460892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
