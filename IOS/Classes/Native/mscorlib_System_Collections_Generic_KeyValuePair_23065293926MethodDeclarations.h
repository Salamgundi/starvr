﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23065293926.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2032267518_gshared (KeyValuePair_2_t3065293926 * __this, int32_t ___key0, Color_t2020392075  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2032267518(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3065293926 *, int32_t, Color_t2020392075 , const MethodInfo*))KeyValuePair_2__ctor_m2032267518_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1052904296_gshared (KeyValuePair_2_t3065293926 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1052904296(__this, method) ((  int32_t (*) (KeyValuePair_2_t3065293926 *, const MethodInfo*))KeyValuePair_2_get_Key_m1052904296_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m810339705_gshared (KeyValuePair_2_t3065293926 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m810339705(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3065293926 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m810339705_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>::get_Value()
extern "C"  Color_t2020392075  KeyValuePair_2_get_Value_m1273110824_gshared (KeyValuePair_2_t3065293926 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1273110824(__this, method) ((  Color_t2020392075  (*) (KeyValuePair_2_t3065293926 *, const MethodInfo*))KeyValuePair_2_get_Value_m1273110824_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3342348593_gshared (KeyValuePair_2_t3065293926 * __this, Color_t2020392075  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3342348593(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3065293926 *, Color_t2020392075 , const MethodInfo*))KeyValuePair_2_set_Value_m3342348593_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m379889987_gshared (KeyValuePair_2_t3065293926 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m379889987(__this, method) ((  String_t* (*) (KeyValuePair_2_t3065293926 *, const MethodInfo*))KeyValuePair_2_ToString_m379889987_gshared)(__this, method)
