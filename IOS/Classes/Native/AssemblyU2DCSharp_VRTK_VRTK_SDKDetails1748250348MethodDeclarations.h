﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_SDKDetails
struct VRTK_SDKDetails_t1748250348;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void VRTK.VRTK_SDKDetails::.ctor(System.String,System.String,System.String)
extern "C"  void VRTK_SDKDetails__ctor_m2164058908 (VRTK_SDKDetails_t1748250348 * __this, String_t* ___givenDefineSymbol0, String_t* ___givenPrettyName1, String_t* ___givenCheckType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
