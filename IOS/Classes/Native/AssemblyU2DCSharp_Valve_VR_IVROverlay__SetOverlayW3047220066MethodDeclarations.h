﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetOverlayWidthInMeters
struct _SetOverlayWidthInMeters_t3047220066;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetOverlayWidthInMeters::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayWidthInMeters__ctor_m3877495301 (_SetOverlayWidthInMeters_t3047220066 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayWidthInMeters::Invoke(System.UInt64,System.Single)
extern "C"  int32_t _SetOverlayWidthInMeters_Invoke_m2146702471 (_SetOverlayWidthInMeters_t3047220066 * __this, uint64_t ___ulOverlayHandle0, float ___fWidthInMeters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayWidthInMeters::BeginInvoke(System.UInt64,System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetOverlayWidthInMeters_BeginInvoke_m1494965178 (_SetOverlayWidthInMeters_t3047220066 * __this, uint64_t ___ulOverlayHandle0, float ___fWidthInMeters1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayWidthInMeters::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayWidthInMeters_EndInvoke_m2619954543 (_SetOverlayWidthInMeters_t3047220066 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
