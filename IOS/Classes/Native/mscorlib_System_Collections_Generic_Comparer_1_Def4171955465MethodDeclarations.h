﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct DefaultComparer_t4171955465;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1387717936.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Valve.VR.InteractionSystem.Hand/AttachedObject>::.ctor()
extern "C"  void DefaultComparer__ctor_m3087352132_gshared (DefaultComparer_t4171955465 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3087352132(__this, method) ((  void (*) (DefaultComparer_t4171955465 *, const MethodInfo*))DefaultComparer__ctor_m3087352132_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Valve.VR.InteractionSystem.Hand/AttachedObject>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m4192095683_gshared (DefaultComparer_t4171955465 * __this, AttachedObject_t1387717936  ___x0, AttachedObject_t1387717936  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m4192095683(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t4171955465 *, AttachedObject_t1387717936 , AttachedObject_t1387717936 , const MethodInfo*))DefaultComparer_Compare_m4192095683_gshared)(__this, ___x0, ___y1, method)
