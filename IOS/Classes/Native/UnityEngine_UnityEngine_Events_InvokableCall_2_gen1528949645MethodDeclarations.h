﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.SnapDropZoneEventArgs>
struct InvokableCall_2_t1528949645;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.SnapDropZoneEventArgs>
struct UnityAction_2_t1514158761;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.SnapDropZoneEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m2668677123_gshared (InvokableCall_2_t1528949645 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_2__ctor_m2668677123(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_2_t1528949645 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2__ctor_m2668677123_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.SnapDropZoneEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m3910438400_gshared (InvokableCall_2_t1528949645 * __this, UnityAction_2_t1514158761 * ___action0, const MethodInfo* method);
#define InvokableCall_2__ctor_m3910438400(__this, ___action0, method) ((  void (*) (InvokableCall_2_t1528949645 *, UnityAction_2_t1514158761 *, const MethodInfo*))InvokableCall_2__ctor_m3910438400_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.SnapDropZoneEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m3786062219_gshared (InvokableCall_2_t1528949645 * __this, UnityAction_2_t1514158761 * ___value0, const MethodInfo* method);
#define InvokableCall_2_add_Delegate_m3786062219(__this, ___value0, method) ((  void (*) (InvokableCall_2_t1528949645 *, UnityAction_2_t1514158761 *, const MethodInfo*))InvokableCall_2_add_Delegate_m3786062219_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.SnapDropZoneEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m614995806_gshared (InvokableCall_2_t1528949645 * __this, UnityAction_2_t1514158761 * ___value0, const MethodInfo* method);
#define InvokableCall_2_remove_Delegate_m614995806(__this, ___value0, method) ((  void (*) (InvokableCall_2_t1528949645 *, UnityAction_2_t1514158761 *, const MethodInfo*))InvokableCall_2_remove_Delegate_m614995806_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.SnapDropZoneEventArgs>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m2553364292_gshared (InvokableCall_2_t1528949645 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_2_Invoke_m2553364292(__this, ___args0, method) ((  void (*) (InvokableCall_2_t1528949645 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_2_Invoke_m2553364292_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.SnapDropZoneEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m3502364824_gshared (InvokableCall_2_t1528949645 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_2_Find_m3502364824(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_2_t1528949645 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2_Find_m3502364824_gshared)(__this, ___targetObj0, ___method1, method)
