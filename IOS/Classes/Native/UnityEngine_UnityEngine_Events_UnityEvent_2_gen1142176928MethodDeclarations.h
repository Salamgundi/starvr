﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectControlEventArgs>
struct UnityEvent_2_t1142176928;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.ObjectControlEventArgs>
struct UnityAction_2_t3554946306;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_VRTK_ObjectControlEventArgs2459490319.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectControlEventArgs>::.ctor()
extern "C"  void UnityEvent_2__ctor_m2465717130_gshared (UnityEvent_2_t1142176928 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m2465717130(__this, method) ((  void (*) (UnityEvent_2_t1142176928 *, const MethodInfo*))UnityEvent_2__ctor_m2465717130_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectControlEventArgs>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m3952616804_gshared (UnityEvent_2_t1142176928 * __this, UnityAction_2_t3554946306 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m3952616804(__this, ___call0, method) ((  void (*) (UnityEvent_2_t1142176928 *, UnityAction_2_t3554946306 *, const MethodInfo*))UnityEvent_2_AddListener_m3952616804_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectControlEventArgs>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m102021659_gshared (UnityEvent_2_t1142176928 * __this, UnityAction_2_t3554946306 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m102021659(__this, ___call0, method) ((  void (*) (UnityEvent_2_t1142176928 *, UnityAction_2_t3554946306 *, const MethodInfo*))UnityEvent_2_RemoveListener_m102021659_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectControlEventArgs>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m3533120048_gshared (UnityEvent_2_t1142176928 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m3533120048(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t1142176928 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m3533120048_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectControlEventArgs>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m1589749500_gshared (UnityEvent_2_t1142176928 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m1589749500(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t1142176928 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m1589749500_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectControlEventArgs>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m2066497623_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t3554946306 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m2066497623(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t3554946306 *, const MethodInfo*))UnityEvent_2_GetDelegate_m2066497623_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectControlEventArgs>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m889465793_gshared (UnityEvent_2_t1142176928 * __this, Il2CppObject * ___arg00, ObjectControlEventArgs_t2459490319  ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m889465793(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t1142176928 *, Il2CppObject *, ObjectControlEventArgs_t2459490319 , const MethodInfo*))UnityEvent_2_Invoke_m889465793_gshared)(__this, ___arg00, ___arg11, method)
