﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ControllerTooltips
struct VRTK_ControllerTooltips_t3537184660;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerTooltips_Too2877834378.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerActionsEventArgs344001476.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetControllerAwareEvent2653531721.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "AssemblyU2DCSharp_VRTK_SDK_BaseController_Controlle447683143.h"

// System.Void VRTK.VRTK_ControllerTooltips::.ctor()
extern "C"  void VRTK_ControllerTooltips__ctor_m3674791448 (VRTK_ControllerTooltips_t3537184660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerTooltips::ResetTooltip()
extern "C"  void VRTK_ControllerTooltips_ResetTooltip_m3092911912 (VRTK_ControllerTooltips_t3537184660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerTooltips::UpdateText(VRTK.VRTK_ControllerTooltips/TooltipButtons,System.String)
extern "C"  void VRTK_ControllerTooltips_UpdateText_m3090611192 (VRTK_ControllerTooltips_t3537184660 * __this, int32_t ___element0, String_t* ___newText1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerTooltips::ToggleTips(System.Boolean,VRTK.VRTK_ControllerTooltips/TooltipButtons)
extern "C"  void VRTK_ControllerTooltips_ToggleTips_m480851963 (VRTK_ControllerTooltips_t3537184660 * __this, bool ___state0, int32_t ___element1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerTooltips::Awake()
extern "C"  void VRTK_ControllerTooltips_Awake_m1267698925 (VRTK_ControllerTooltips_t3537184660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerTooltips::OnEnable()
extern "C"  void VRTK_ControllerTooltips_OnEnable_m1991811400 (VRTK_ControllerTooltips_t3537184660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerTooltips::OnDisable()
extern "C"  void VRTK_ControllerTooltips_OnDisable_m3024080941 (VRTK_ControllerTooltips_t3537184660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerTooltips::Update()
extern "C"  void VRTK_ControllerTooltips_Update_m17488361 (VRTK_ControllerTooltips_t3537184660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerTooltips::DoControllerVisible(System.Object,VRTK.ControllerActionsEventArgs)
extern "C"  void VRTK_ControllerTooltips_DoControllerVisible_m2744710332 (VRTK_ControllerTooltips_t3537184660 * __this, Il2CppObject * ___sender0, ControllerActionsEventArgs_t344001476  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerTooltips::DoControllerInvisible(System.Object,VRTK.ControllerActionsEventArgs)
extern "C"  void VRTK_ControllerTooltips_DoControllerInvisible_m1735990643 (VRTK_ControllerTooltips_t3537184660 * __this, Il2CppObject * ___sender0, ControllerActionsEventArgs_t344001476  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerTooltips::DoGlanceEnterController(System.Object,VRTK.HeadsetControllerAwareEventArgs)
extern "C"  void VRTK_ControllerTooltips_DoGlanceEnterController_m3689505727 (VRTK_ControllerTooltips_t3537184660 * __this, Il2CppObject * ___sender0, HeadsetControllerAwareEventArgs_t2653531721  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerTooltips::DoGlanceExitController(System.Object,VRTK.HeadsetControllerAwareEventArgs)
extern "C"  void VRTK_ControllerTooltips_DoGlanceExitController_m930942839 (VRTK_ControllerTooltips_t3537184660 * __this, Il2CppObject * ___sender0, HeadsetControllerAwareEventArgs_t2653531721  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerTooltips::InitialiseTips()
extern "C"  void VRTK_ControllerTooltips_InitialiseTips_m416544399 (VRTK_ControllerTooltips_t3537184660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_ControllerTooltips::TipsInitialised()
extern "C"  bool VRTK_ControllerTooltips_TipsInitialised_m1975845423 (VRTK_ControllerTooltips_t3537184660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_ControllerTooltips::GetTransform(UnityEngine.Transform,VRTK.SDK_BaseController/ControllerElements)
extern "C"  Transform_t3275118058 * VRTK_ControllerTooltips_GetTransform_m391292856 (VRTK_ControllerTooltips_t3537184660 * __this, Transform_t3275118058 * ___setTransform0, int32_t ___findElement1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
