﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.CustomEvents/UnityEventHand
struct UnityEventHand_t749092352;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.CustomEvents/UnityEventHand::.ctor()
extern "C"  void UnityEventHand__ctor_m3854945237 (UnityEventHand_t749092352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
