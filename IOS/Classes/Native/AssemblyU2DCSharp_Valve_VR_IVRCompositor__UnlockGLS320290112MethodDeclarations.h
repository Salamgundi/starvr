﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_UnlockGLSharedTextureForAccess
struct _UnlockGLSharedTextureForAccess_t320290112;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_UnlockGLSharedTextureForAccess::.ctor(System.Object,System.IntPtr)
extern "C"  void _UnlockGLSharedTextureForAccess__ctor_m4124647781 (_UnlockGLSharedTextureForAccess_t320290112 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_UnlockGLSharedTextureForAccess::Invoke(System.IntPtr)
extern "C"  void _UnlockGLSharedTextureForAccess_Invoke_m3053049967 (_UnlockGLSharedTextureForAccess_t320290112 * __this, IntPtr_t ___glSharedTextureHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_UnlockGLSharedTextureForAccess::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _UnlockGLSharedTextureForAccess_BeginInvoke_m467237154 (_UnlockGLSharedTextureForAccess_t320290112 * __this, IntPtr_t ___glSharedTextureHandle0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_UnlockGLSharedTextureForAccess::EndInvoke(System.IAsyncResult)
extern "C"  void _UnlockGLSharedTextureForAccess_EndInvoke_m3696885743 (_UnlockGLSharedTextureForAccess_t320290112 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
