﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetMatrix34TrackedDeviceProperty
struct _GetMatrix34TrackedDeviceProperty_t3426445457;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedDeviceProperty3226377054.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedPropertyError3340022390.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetMatrix34TrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetMatrix34TrackedDeviceProperty__ctor_m2418638436 (_GetMatrix34TrackedDeviceProperty_t3426445457 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix34_t Valve.VR.IVRSystem/_GetMatrix34TrackedDeviceProperty::Invoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&)
extern "C"  HmdMatrix34_t_t664273062  _GetMatrix34TrackedDeviceProperty_Invoke_m1622936551 (_GetMatrix34TrackedDeviceProperty_t3426445457 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetMatrix34TrackedDeviceProperty::BeginInvoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetMatrix34TrackedDeviceProperty_BeginInvoke_m3901349401 (_GetMatrix34TrackedDeviceProperty_t3426445457 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix34_t Valve.VR.IVRSystem/_GetMatrix34TrackedDeviceProperty::EndInvoke(Valve.VR.ETrackedPropertyError&,System.IAsyncResult)
extern "C"  HmdMatrix34_t_t664273062  _GetMatrix34TrackedDeviceProperty_EndInvoke_m2319733641 (_GetMatrix34TrackedDeviceProperty_t3426445457 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
