﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen4149639883.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_UIPointer_UnityEvents/UnityObjectEvent
struct  UnityObjectEvent_t2013676129  : public UnityEvent_2_t4149639883
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
