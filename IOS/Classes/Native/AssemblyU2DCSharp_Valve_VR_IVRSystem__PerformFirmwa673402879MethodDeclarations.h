﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_PerformFirmwareUpdate
struct _PerformFirmwareUpdate_t673402879;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRFirmwareError2321703066.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_PerformFirmwareUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void _PerformFirmwareUpdate__ctor_m2481185390 (_PerformFirmwareUpdate_t673402879 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRFirmwareError Valve.VR.IVRSystem/_PerformFirmwareUpdate::Invoke(System.UInt32)
extern "C"  int32_t _PerformFirmwareUpdate_Invoke_m262581537 (_PerformFirmwareUpdate_t673402879 * __this, uint32_t ___unDeviceIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_PerformFirmwareUpdate::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _PerformFirmwareUpdate_BeginInvoke_m751292041 (_PerformFirmwareUpdate_t673402879 * __this, uint32_t ___unDeviceIndex0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRFirmwareError Valve.VR.IVRSystem/_PerformFirmwareUpdate::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _PerformFirmwareUpdate_EndInvoke_m2241895231 (_PerformFirmwareUpdate_t673402879 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
