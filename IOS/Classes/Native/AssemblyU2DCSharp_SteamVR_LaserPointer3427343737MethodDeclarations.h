﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_LaserPointer
struct SteamVR_LaserPointer_t3427343737;
// PointerEventHandler
struct PointerEventHandler_t583817773;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PointerEventHandler583817773.h"
#include "AssemblyU2DCSharp_PointerEventArgs4020535570.h"

// System.Void SteamVR_LaserPointer::.ctor()
extern "C"  void SteamVR_LaserPointer__ctor_m3839163260 (SteamVR_LaserPointer_t3427343737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LaserPointer::add_PointerIn(PointerEventHandler)
extern "C"  void SteamVR_LaserPointer_add_PointerIn_m1017480877 (SteamVR_LaserPointer_t3427343737 * __this, PointerEventHandler_t583817773 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LaserPointer::remove_PointerIn(PointerEventHandler)
extern "C"  void SteamVR_LaserPointer_remove_PointerIn_m1492307788 (SteamVR_LaserPointer_t3427343737 * __this, PointerEventHandler_t583817773 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LaserPointer::add_PointerOut(PointerEventHandler)
extern "C"  void SteamVR_LaserPointer_add_PointerOut_m1626753758 (SteamVR_LaserPointer_t3427343737 * __this, PointerEventHandler_t583817773 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LaserPointer::remove_PointerOut(PointerEventHandler)
extern "C"  void SteamVR_LaserPointer_remove_PointerOut_m857313855 (SteamVR_LaserPointer_t3427343737 * __this, PointerEventHandler_t583817773 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LaserPointer::Start()
extern "C"  void SteamVR_LaserPointer_Start_m3882750116 (SteamVR_LaserPointer_t3427343737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LaserPointer::OnPointerIn(PointerEventArgs)
extern "C"  void SteamVR_LaserPointer_OnPointerIn_m4141232961 (SteamVR_LaserPointer_t3427343737 * __this, PointerEventArgs_t4020535570  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LaserPointer::OnPointerOut(PointerEventArgs)
extern "C"  void SteamVR_LaserPointer_OnPointerOut_m3691735370 (SteamVR_LaserPointer_t3427343737 * __this, PointerEventArgs_t4020535570  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LaserPointer::Update()
extern "C"  void SteamVR_LaserPointer_Update_m3088751461 (SteamVR_LaserPointer_t3427343737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
