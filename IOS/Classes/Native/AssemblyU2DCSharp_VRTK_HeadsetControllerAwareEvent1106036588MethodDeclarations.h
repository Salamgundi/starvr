﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.HeadsetControllerAwareEventHandler
struct HeadsetControllerAwareEventHandler_t1106036588;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetControllerAwareEvent2653531721.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.HeadsetControllerAwareEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void HeadsetControllerAwareEventHandler__ctor_m506264312 (HeadsetControllerAwareEventHandler_t1106036588 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.HeadsetControllerAwareEventHandler::Invoke(System.Object,VRTK.HeadsetControllerAwareEventArgs)
extern "C"  void HeadsetControllerAwareEventHandler_Invoke_m3271404950 (HeadsetControllerAwareEventHandler_t1106036588 * __this, Il2CppObject * ___sender0, HeadsetControllerAwareEventArgs_t2653531721  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.HeadsetControllerAwareEventHandler::BeginInvoke(System.Object,VRTK.HeadsetControllerAwareEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * HeadsetControllerAwareEventHandler_BeginInvoke_m2359860389 (HeadsetControllerAwareEventHandler_t1106036588 * __this, Il2CppObject * ___sender0, HeadsetControllerAwareEventArgs_t2653531721  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.HeadsetControllerAwareEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void HeadsetControllerAwareEventHandler_EndInvoke_m2062878358 (HeadsetControllerAwareEventHandler_t1106036588 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
