﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.DistanceHaptics
struct  DistanceHaptics_t3909819367  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Valve.VR.InteractionSystem.DistanceHaptics::firstTransform
	Transform_t3275118058 * ___firstTransform_2;
	// UnityEngine.Transform Valve.VR.InteractionSystem.DistanceHaptics::secondTransform
	Transform_t3275118058 * ___secondTransform_3;
	// UnityEngine.AnimationCurve Valve.VR.InteractionSystem.DistanceHaptics::distanceIntensityCurve
	AnimationCurve_t3306541151 * ___distanceIntensityCurve_4;
	// UnityEngine.AnimationCurve Valve.VR.InteractionSystem.DistanceHaptics::pulseIntervalCurve
	AnimationCurve_t3306541151 * ___pulseIntervalCurve_5;

public:
	inline static int32_t get_offset_of_firstTransform_2() { return static_cast<int32_t>(offsetof(DistanceHaptics_t3909819367, ___firstTransform_2)); }
	inline Transform_t3275118058 * get_firstTransform_2() const { return ___firstTransform_2; }
	inline Transform_t3275118058 ** get_address_of_firstTransform_2() { return &___firstTransform_2; }
	inline void set_firstTransform_2(Transform_t3275118058 * value)
	{
		___firstTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&___firstTransform_2, value);
	}

	inline static int32_t get_offset_of_secondTransform_3() { return static_cast<int32_t>(offsetof(DistanceHaptics_t3909819367, ___secondTransform_3)); }
	inline Transform_t3275118058 * get_secondTransform_3() const { return ___secondTransform_3; }
	inline Transform_t3275118058 ** get_address_of_secondTransform_3() { return &___secondTransform_3; }
	inline void set_secondTransform_3(Transform_t3275118058 * value)
	{
		___secondTransform_3 = value;
		Il2CppCodeGenWriteBarrier(&___secondTransform_3, value);
	}

	inline static int32_t get_offset_of_distanceIntensityCurve_4() { return static_cast<int32_t>(offsetof(DistanceHaptics_t3909819367, ___distanceIntensityCurve_4)); }
	inline AnimationCurve_t3306541151 * get_distanceIntensityCurve_4() const { return ___distanceIntensityCurve_4; }
	inline AnimationCurve_t3306541151 ** get_address_of_distanceIntensityCurve_4() { return &___distanceIntensityCurve_4; }
	inline void set_distanceIntensityCurve_4(AnimationCurve_t3306541151 * value)
	{
		___distanceIntensityCurve_4 = value;
		Il2CppCodeGenWriteBarrier(&___distanceIntensityCurve_4, value);
	}

	inline static int32_t get_offset_of_pulseIntervalCurve_5() { return static_cast<int32_t>(offsetof(DistanceHaptics_t3909819367, ___pulseIntervalCurve_5)); }
	inline AnimationCurve_t3306541151 * get_pulseIntervalCurve_5() const { return ___pulseIntervalCurve_5; }
	inline AnimationCurve_t3306541151 ** get_address_of_pulseIntervalCurve_5() { return &___pulseIntervalCurve_5; }
	inline void set_pulseIntervalCurve_5(AnimationCurve_t3306541151 * value)
	{
		___pulseIntervalCurve_5 = value;
		Il2CppCodeGenWriteBarrier(&___pulseIntervalCurve_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
