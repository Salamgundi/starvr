﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.AutoRotation
struct AutoRotation_t3877681447;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.Examples.AutoRotation::.ctor()
extern "C"  void AutoRotation__ctor_m262712028 (AutoRotation_t3877681447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.AutoRotation::Start()
extern "C"  void AutoRotation_Start_m2567995684 (AutoRotation_t3877681447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.AutoRotation::Update()
extern "C"  void AutoRotation_Update_m53102381 (AutoRotation_t3877681447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
