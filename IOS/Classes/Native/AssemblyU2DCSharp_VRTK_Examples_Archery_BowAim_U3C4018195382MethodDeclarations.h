﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Archery.BowAim/<GetBaseRotation>c__Iterator0
struct U3CGetBaseRotationU3Ec__Iterator0_t4018195382;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.Examples.Archery.BowAim/<GetBaseRotation>c__Iterator0::.ctor()
extern "C"  void U3CGetBaseRotationU3Ec__Iterator0__ctor_m403259083 (U3CGetBaseRotationU3Ec__Iterator0_t4018195382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.Examples.Archery.BowAim/<GetBaseRotation>c__Iterator0::MoveNext()
extern "C"  bool U3CGetBaseRotationU3Ec__Iterator0_MoveNext_m3506931553 (U3CGetBaseRotationU3Ec__Iterator0_t4018195382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.Examples.Archery.BowAim/<GetBaseRotation>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetBaseRotationU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2884640157 (U3CGetBaseRotationU3Ec__Iterator0_t4018195382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.Examples.Archery.BowAim/<GetBaseRotation>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetBaseRotationU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1904129621 (U3CGetBaseRotationU3Ec__Iterator0_t4018195382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.BowAim/<GetBaseRotation>c__Iterator0::Dispose()
extern "C"  void U3CGetBaseRotationU3Ec__Iterator0_Dispose_m1370222332 (U3CGetBaseRotationU3Ec__Iterator0_t4018195382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.BowAim/<GetBaseRotation>c__Iterator0::Reset()
extern "C"  void U3CGetBaseRotationU3Ec__Iterator0_Reset_m2364241650 (U3CGetBaseRotationU3Ec__Iterator0_t4018195382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
