﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetEyeToHeadTransform
struct _GetEyeToHeadTransform_t3057184772;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetEyeToHeadTransform::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetEyeToHeadTransform__ctor_m1490155399 (_GetEyeToHeadTransform_t3057184772 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix34_t Valve.VR.IVRSystem/_GetEyeToHeadTransform::Invoke(Valve.VR.EVREye)
extern "C"  HmdMatrix34_t_t664273062  _GetEyeToHeadTransform_Invoke_m2328941924 (_GetEyeToHeadTransform_t3057184772 * __this, int32_t ___eEye0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetEyeToHeadTransform::BeginInvoke(Valve.VR.EVREye,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetEyeToHeadTransform_BeginInvoke_m148652392 (_GetEyeToHeadTransform_t3057184772 * __this, int32_t ___eEye0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix34_t Valve.VR.IVRSystem/_GetEyeToHeadTransform::EndInvoke(System.IAsyncResult)
extern "C"  HmdMatrix34_t_t664273062  _GetEyeToHeadTransform_EndInvoke_m2450405904 (_GetEyeToHeadTransform_t3057184772 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
