﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_PlayerObject
struct VRTK_PlayerObject_t502441292;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PlayerObject_ObjectTyp2764484032.h"

// System.Void VRTK.VRTK_PlayerObject::.ctor()
extern "C"  void VRTK_PlayerObject__ctor_m1361006270 (VRTK_PlayerObject_t502441292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerObject::SetPlayerObject(UnityEngine.GameObject,VRTK.VRTK_PlayerObject/ObjectTypes)
extern "C"  void VRTK_PlayerObject_SetPlayerObject_m1121178556 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___obj0, int32_t ___objType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_PlayerObject::IsPlayerObject(UnityEngine.GameObject,VRTK.VRTK_PlayerObject/ObjectTypes)
extern "C"  bool VRTK_PlayerObject_IsPlayerObject_m1369301804 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___obj0, int32_t ___ofType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
