﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_TouchpadMovement
struct VRTK_TouchpadMovement_t1979813355;
// VRTK.TouchpadMovementAxisEventHandler
struct TouchpadMovementAxisEventHandler_t2208405138;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_TouchpadMovementAxisEventHa2208405138.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_AxisM2300405083.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_AxisM1724819570.h"

// System.Void VRTK.VRTK_TouchpadMovement::.ctor()
extern "C"  void VRTK_TouchpadMovement__ctor_m1906722849 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::add_AxisMovement(VRTK.TouchpadMovementAxisEventHandler)
extern "C"  void VRTK_TouchpadMovement_add_AxisMovement_m2558945748 (VRTK_TouchpadMovement_t1979813355 * __this, TouchpadMovementAxisEventHandler_t2208405138 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::remove_AxisMovement(VRTK.TouchpadMovementAxisEventHandler)
extern "C"  void VRTK_TouchpadMovement_remove_AxisMovement_m1781378661 (VRTK_TouchpadMovement_t1979813355 * __this, TouchpadMovementAxisEventHandler_t2208405138 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::Awake()
extern "C"  void VRTK_TouchpadMovement_Awake_m2540975428 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::OnEnable()
extern "C"  void VRTK_TouchpadMovement_OnEnable_m1783118689 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::Start()
extern "C"  void VRTK_TouchpadMovement_Start_m3065344833 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::OnDisable()
extern "C"  void VRTK_TouchpadMovement_OnDisable_m608677076 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::Update()
extern "C"  void VRTK_TouchpadMovement_Update_m1620871106 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::FixedUpdate()
extern "C"  void VRTK_TouchpadMovement_FixedUpdate_m694020628 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::HandleFalling()
extern "C"  void VRTK_TouchpadMovement_HandleFalling_m3684340994 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::CheckControllerState(UnityEngine.GameObject,System.Boolean,System.Boolean&,System.Boolean&)
extern "C"  void VRTK_TouchpadMovement_CheckControllerState_m2599412199 (VRTK_TouchpadMovement_t1979813355 * __this, GameObject_t1756533147 * ___controller0, bool ___controllerState1, bool* ___subscribedState2, bool* ___previousState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::DoTouchpadAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_TouchpadMovement_DoTouchpadAxisChanged_m314873841 (VRTK_TouchpadMovement_t1979813355 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::DoTouchpadTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_TouchpadMovement_DoTouchpadTouchEnd_m3376986714 (VRTK_TouchpadMovement_t1979813355 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::OnAxisMovement(VRTK.VRTK_TouchpadMovement/AxisMovementType,VRTK.VRTK_TouchpadMovement/AxisMovementDirection)
extern "C"  void VRTK_TouchpadMovement_OnAxisMovement_m3391609629 (VRTK_TouchpadMovement_t1979813355 * __this, int32_t ___givenMovementType0, int32_t ___givenDirection1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::CalculateSpeed(System.Boolean,System.Single&,System.Single)
extern "C"  void VRTK_TouchpadMovement_CalculateSpeed_m746457475 (VRTK_TouchpadMovement_t1979813355 * __this, bool ___horizontal0, float* ___speed1, float ___inputValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::Decelerate(System.Single&)
extern "C"  void VRTK_TouchpadMovement_Decelerate_m2395966906 (VRTK_TouchpadMovement_t1979813355 * __this, float* ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::Move()
extern "C"  void VRTK_TouchpadMovement_Move_m3310755870 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::Warp(System.Boolean,System.Boolean)
extern "C"  void VRTK_TouchpadMovement_Warp_m2450696817 (VRTK_TouchpadMovement_t1979813355 * __this, bool ___blink0, bool ___horizontal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::RotateAroundPlayer(System.Single)
extern "C"  void VRTK_TouchpadMovement_RotateAroundPlayer_m2493629917 (VRTK_TouchpadMovement_t1979813355 * __this, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::Rotate()
extern "C"  void VRTK_TouchpadMovement_Rotate_m4105360080 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::SnapRotate(System.Boolean,System.Boolean)
extern "C"  void VRTK_TouchpadMovement_SnapRotate_m1907291896 (VRTK_TouchpadMovement_t1979813355 * __this, bool ___blink0, bool ___flipDirection1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::ReleaseBlink()
extern "C"  void VRTK_TouchpadMovement_ReleaseBlink_m3016794946 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::SetControllerListeners(UnityEngine.GameObject,System.Boolean,System.Boolean&,System.Boolean)
extern "C"  void VRTK_TouchpadMovement_SetControllerListeners_m814129575 (VRTK_TouchpadMovement_t1979813355 * __this, GameObject_t1756533147 * ___controller0, bool ___controllerState1, bool* ___subscribedState2, bool ___forceDisabled3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadMovement::ToggleControllerListeners(UnityEngine.GameObject,System.Boolean,System.Boolean&)
extern "C"  void VRTK_TouchpadMovement_ToggleControllerListeners_m1383735902 (VRTK_TouchpadMovement_t1979813355 * __this, GameObject_t1756533147 * ___controller0, bool ___toggle1, bool* ___subscribed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
