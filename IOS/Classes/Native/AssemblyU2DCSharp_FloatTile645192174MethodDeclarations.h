﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FloatTile
struct FloatTile_t645192174;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void FloatTile::.ctor()
extern "C"  void FloatTile__ctor_m967607759 (FloatTile_t645192174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTile::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void FloatTile_OnPointerEnter_m1843985537 (FloatTile_t645192174 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTile::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void FloatTile_OnPointerExit_m3847484989 (FloatTile_t645192174 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTile::OnGvrPointerHover(UnityEngine.EventSystems.PointerEventData)
extern "C"  void FloatTile_OnGvrPointerHover_m4020376646 (FloatTile_t645192174 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
