﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_AdaptiveQuality/ShaderPropertyIDs
struct  ShaderPropertyIDs_t445255555  : public Il2CppObject
{
public:

public:
};

struct ShaderPropertyIDs_t445255555_StaticFields
{
public:
	// System.Int32 VRTK.VRTK_AdaptiveQuality/ShaderPropertyIDs::RenderScaleLevelsCount
	int32_t ___RenderScaleLevelsCount_0;
	// System.Int32 VRTK.VRTK_AdaptiveQuality/ShaderPropertyIDs::DefaultRenderViewportScaleLevel
	int32_t ___DefaultRenderViewportScaleLevel_1;
	// System.Int32 VRTK.VRTK_AdaptiveQuality/ShaderPropertyIDs::CurrentRenderViewportScaleLevel
	int32_t ___CurrentRenderViewportScaleLevel_2;
	// System.Int32 VRTK.VRTK_AdaptiveQuality/ShaderPropertyIDs::CurrentRenderScaleLevel
	int32_t ___CurrentRenderScaleLevel_3;
	// System.Int32 VRTK.VRTK_AdaptiveQuality/ShaderPropertyIDs::LastFrameIsInBudget
	int32_t ___LastFrameIsInBudget_4;

public:
	inline static int32_t get_offset_of_RenderScaleLevelsCount_0() { return static_cast<int32_t>(offsetof(ShaderPropertyIDs_t445255555_StaticFields, ___RenderScaleLevelsCount_0)); }
	inline int32_t get_RenderScaleLevelsCount_0() const { return ___RenderScaleLevelsCount_0; }
	inline int32_t* get_address_of_RenderScaleLevelsCount_0() { return &___RenderScaleLevelsCount_0; }
	inline void set_RenderScaleLevelsCount_0(int32_t value)
	{
		___RenderScaleLevelsCount_0 = value;
	}

	inline static int32_t get_offset_of_DefaultRenderViewportScaleLevel_1() { return static_cast<int32_t>(offsetof(ShaderPropertyIDs_t445255555_StaticFields, ___DefaultRenderViewportScaleLevel_1)); }
	inline int32_t get_DefaultRenderViewportScaleLevel_1() const { return ___DefaultRenderViewportScaleLevel_1; }
	inline int32_t* get_address_of_DefaultRenderViewportScaleLevel_1() { return &___DefaultRenderViewportScaleLevel_1; }
	inline void set_DefaultRenderViewportScaleLevel_1(int32_t value)
	{
		___DefaultRenderViewportScaleLevel_1 = value;
	}

	inline static int32_t get_offset_of_CurrentRenderViewportScaleLevel_2() { return static_cast<int32_t>(offsetof(ShaderPropertyIDs_t445255555_StaticFields, ___CurrentRenderViewportScaleLevel_2)); }
	inline int32_t get_CurrentRenderViewportScaleLevel_2() const { return ___CurrentRenderViewportScaleLevel_2; }
	inline int32_t* get_address_of_CurrentRenderViewportScaleLevel_2() { return &___CurrentRenderViewportScaleLevel_2; }
	inline void set_CurrentRenderViewportScaleLevel_2(int32_t value)
	{
		___CurrentRenderViewportScaleLevel_2 = value;
	}

	inline static int32_t get_offset_of_CurrentRenderScaleLevel_3() { return static_cast<int32_t>(offsetof(ShaderPropertyIDs_t445255555_StaticFields, ___CurrentRenderScaleLevel_3)); }
	inline int32_t get_CurrentRenderScaleLevel_3() const { return ___CurrentRenderScaleLevel_3; }
	inline int32_t* get_address_of_CurrentRenderScaleLevel_3() { return &___CurrentRenderScaleLevel_3; }
	inline void set_CurrentRenderScaleLevel_3(int32_t value)
	{
		___CurrentRenderScaleLevel_3 = value;
	}

	inline static int32_t get_offset_of_LastFrameIsInBudget_4() { return static_cast<int32_t>(offsetof(ShaderPropertyIDs_t445255555_StaticFields, ___LastFrameIsInBudget_4)); }
	inline int32_t get_LastFrameIsInBudget_4() const { return ___LastFrameIsInBudget_4; }
	inline int32_t* get_address_of_LastFrameIsInBudget_4() { return &___LastFrameIsInBudget_4; }
	inline void set_LastFrameIsInBudget_4(int32_t value)
	{
		___LastFrameIsInBudget_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
