﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Interactable/OnAttachedToHandDelegate
struct OnAttachedToHandDelegate_t3380341848;
// System.Object
struct Il2CppObject;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.InteractionSystem.Interactable/OnAttachedToHandDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void OnAttachedToHandDelegate__ctor_m2839264765 (OnAttachedToHandDelegate_t3380341848 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Interactable/OnAttachedToHandDelegate::Invoke(Valve.VR.InteractionSystem.Hand)
extern "C"  void OnAttachedToHandDelegate_Invoke_m4008701971 (OnAttachedToHandDelegate_t3380341848 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.InteractionSystem.Interactable/OnAttachedToHandDelegate::BeginInvoke(Valve.VR.InteractionSystem.Hand,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnAttachedToHandDelegate_BeginInvoke_m2992068256 (OnAttachedToHandDelegate_t3380341848 * __this, Hand_t379716353 * ___hand0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Interactable/OnAttachedToHandDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void OnAttachedToHandDelegate_EndInvoke_m2412353815 (OnAttachedToHandDelegate_t3380341848 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
