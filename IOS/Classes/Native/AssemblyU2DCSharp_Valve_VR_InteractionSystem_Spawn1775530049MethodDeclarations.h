﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.SpawnAndAttachToHand
struct SpawnAndAttachToHand_t1775530049;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"

// System.Void Valve.VR.InteractionSystem.SpawnAndAttachToHand::.ctor()
extern "C"  void SpawnAndAttachToHand__ctor_m1568787133 (SpawnAndAttachToHand_t1775530049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SpawnAndAttachToHand::SpawnAndAttach(Valve.VR.InteractionSystem.Hand)
extern "C"  void SpawnAndAttachToHand_SpawnAndAttach_m803784656 (SpawnAndAttachToHand_t1775530049 * __this, Hand_t379716353 * ___passedInhand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
