﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Openable_Door
struct  Openable_Door_t677159015  : public VRTK_InteractableObject_t2604188111
{
public:
	// System.Boolean VRTK.Examples.Openable_Door::flipped
	bool ___flipped_45;
	// System.Boolean VRTK.Examples.Openable_Door::rotated
	bool ___rotated_46;
	// System.Single VRTK.Examples.Openable_Door::sideFlip
	float ___sideFlip_47;
	// System.Single VRTK.Examples.Openable_Door::side
	float ___side_48;
	// System.Single VRTK.Examples.Openable_Door::smooth
	float ___smooth_49;
	// System.Single VRTK.Examples.Openable_Door::doorOpenAngle
	float ___doorOpenAngle_50;
	// System.Boolean VRTK.Examples.Openable_Door::open
	bool ___open_51;
	// UnityEngine.Vector3 VRTK.Examples.Openable_Door::defaultRotation
	Vector3_t2243707580  ___defaultRotation_52;
	// UnityEngine.Vector3 VRTK.Examples.Openable_Door::openRotation
	Vector3_t2243707580  ___openRotation_53;

public:
	inline static int32_t get_offset_of_flipped_45() { return static_cast<int32_t>(offsetof(Openable_Door_t677159015, ___flipped_45)); }
	inline bool get_flipped_45() const { return ___flipped_45; }
	inline bool* get_address_of_flipped_45() { return &___flipped_45; }
	inline void set_flipped_45(bool value)
	{
		___flipped_45 = value;
	}

	inline static int32_t get_offset_of_rotated_46() { return static_cast<int32_t>(offsetof(Openable_Door_t677159015, ___rotated_46)); }
	inline bool get_rotated_46() const { return ___rotated_46; }
	inline bool* get_address_of_rotated_46() { return &___rotated_46; }
	inline void set_rotated_46(bool value)
	{
		___rotated_46 = value;
	}

	inline static int32_t get_offset_of_sideFlip_47() { return static_cast<int32_t>(offsetof(Openable_Door_t677159015, ___sideFlip_47)); }
	inline float get_sideFlip_47() const { return ___sideFlip_47; }
	inline float* get_address_of_sideFlip_47() { return &___sideFlip_47; }
	inline void set_sideFlip_47(float value)
	{
		___sideFlip_47 = value;
	}

	inline static int32_t get_offset_of_side_48() { return static_cast<int32_t>(offsetof(Openable_Door_t677159015, ___side_48)); }
	inline float get_side_48() const { return ___side_48; }
	inline float* get_address_of_side_48() { return &___side_48; }
	inline void set_side_48(float value)
	{
		___side_48 = value;
	}

	inline static int32_t get_offset_of_smooth_49() { return static_cast<int32_t>(offsetof(Openable_Door_t677159015, ___smooth_49)); }
	inline float get_smooth_49() const { return ___smooth_49; }
	inline float* get_address_of_smooth_49() { return &___smooth_49; }
	inline void set_smooth_49(float value)
	{
		___smooth_49 = value;
	}

	inline static int32_t get_offset_of_doorOpenAngle_50() { return static_cast<int32_t>(offsetof(Openable_Door_t677159015, ___doorOpenAngle_50)); }
	inline float get_doorOpenAngle_50() const { return ___doorOpenAngle_50; }
	inline float* get_address_of_doorOpenAngle_50() { return &___doorOpenAngle_50; }
	inline void set_doorOpenAngle_50(float value)
	{
		___doorOpenAngle_50 = value;
	}

	inline static int32_t get_offset_of_open_51() { return static_cast<int32_t>(offsetof(Openable_Door_t677159015, ___open_51)); }
	inline bool get_open_51() const { return ___open_51; }
	inline bool* get_address_of_open_51() { return &___open_51; }
	inline void set_open_51(bool value)
	{
		___open_51 = value;
	}

	inline static int32_t get_offset_of_defaultRotation_52() { return static_cast<int32_t>(offsetof(Openable_Door_t677159015, ___defaultRotation_52)); }
	inline Vector3_t2243707580  get_defaultRotation_52() const { return ___defaultRotation_52; }
	inline Vector3_t2243707580 * get_address_of_defaultRotation_52() { return &___defaultRotation_52; }
	inline void set_defaultRotation_52(Vector3_t2243707580  value)
	{
		___defaultRotation_52 = value;
	}

	inline static int32_t get_offset_of_openRotation_53() { return static_cast<int32_t>(offsetof(Openable_Door_t677159015, ___openRotation_53)); }
	inline Vector3_t2243707580  get_openRotation_53() const { return ___openRotation_53; }
	inline Vector3_t2243707580 * get_address_of_openRotation_53() { return &___openRotation_53; }
	inline void set_openRotation_53(Vector3_t2243707580  value)
	{
		___openRotation_53 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
