﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_StraightPointerRenderer
struct VRTK_StraightPointerRenderer_t3315603018;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"

// System.Void VRTK.VRTK_StraightPointerRenderer::.ctor()
extern "C"  void VRTK_StraightPointerRenderer__ctor_m4288843134 (VRTK_StraightPointerRenderer_t3315603018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_StraightPointerRenderer::UpdateRenderer()
extern "C"  void VRTK_StraightPointerRenderer_UpdateRenderer_m1894105254 (VRTK_StraightPointerRenderer_t3315603018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_StraightPointerRenderer::ToggleRenderer(System.Boolean,System.Boolean)
extern "C"  void VRTK_StraightPointerRenderer_ToggleRenderer_m1489533413 (VRTK_StraightPointerRenderer_t3315603018 * __this, bool ___pointerState0, bool ___actualState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_StraightPointerRenderer::CreatePointerObjects()
extern "C"  void VRTK_StraightPointerRenderer_CreatePointerObjects_m2815054919 (VRTK_StraightPointerRenderer_t3315603018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_StraightPointerRenderer::DestroyPointerObjects()
extern "C"  void VRTK_StraightPointerRenderer_DestroyPointerObjects_m2169076845 (VRTK_StraightPointerRenderer_t3315603018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_StraightPointerRenderer::ChangeMaterial(UnityEngine.Color)
extern "C"  void VRTK_StraightPointerRenderer_ChangeMaterial_m3253377681 (VRTK_StraightPointerRenderer_t3315603018 * __this, Color_t2020392075  ___givenColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_StraightPointerRenderer::UpdateObjectInteractor()
extern "C"  void VRTK_StraightPointerRenderer_UpdateObjectInteractor_m4207350697 (VRTK_StraightPointerRenderer_t3315603018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_StraightPointerRenderer::CreateTracer()
extern "C"  void VRTK_StraightPointerRenderer_CreateTracer_m4086473113 (VRTK_StraightPointerRenderer_t3315603018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_StraightPointerRenderer::CreateCursor()
extern "C"  void VRTK_StraightPointerRenderer_CreateCursor_m2259241854 (VRTK_StraightPointerRenderer_t3315603018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_StraightPointerRenderer::CheckRayMiss(System.Boolean,UnityEngine.RaycastHit)
extern "C"  void VRTK_StraightPointerRenderer_CheckRayMiss_m1208533176 (VRTK_StraightPointerRenderer_t3315603018 * __this, bool ___rayHit0, RaycastHit_t87180320  ___pointerCollidedWith1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_StraightPointerRenderer::CheckRayHit(System.Boolean,UnityEngine.RaycastHit)
extern "C"  void VRTK_StraightPointerRenderer_CheckRayHit_m1500941533 (VRTK_StraightPointerRenderer_t3315603018 * __this, bool ___rayHit0, RaycastHit_t87180320  ___pointerCollidedWith1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_StraightPointerRenderer::CastRayForward()
extern "C"  float VRTK_StraightPointerRenderer_CastRayForward_m2812550752 (VRTK_StraightPointerRenderer_t3315603018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_StraightPointerRenderer::SetPointerAppearance(System.Single)
extern "C"  void VRTK_StraightPointerRenderer_SetPointerAppearance_m1146799952 (VRTK_StraightPointerRenderer_t3315603018 * __this, float ___tracerLength0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
