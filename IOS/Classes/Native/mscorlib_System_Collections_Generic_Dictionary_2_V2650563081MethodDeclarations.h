﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>
struct ValueCollection_t2650563081;
// System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>
struct Dictionary_2_t3947503238;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1339068706.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3284155060_gshared (ValueCollection_t2650563081 * __this, Dictionary_2_t3947503238 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m3284155060(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2650563081 *, Dictionary_2_t3947503238 *, const MethodInfo*))ValueCollection__ctor_m3284155060_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1980768562_gshared (ValueCollection_t2650563081 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1980768562(__this, ___item0, method) ((  void (*) (ValueCollection_t2650563081 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1980768562_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1741175791_gshared (ValueCollection_t2650563081 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1741175791(__this, method) ((  void (*) (ValueCollection_t2650563081 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1741175791_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2187356890_gshared (ValueCollection_t2650563081 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2187356890(__this, ___item0, method) ((  bool (*) (ValueCollection_t2650563081 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2187356890_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1161991615_gshared (ValueCollection_t2650563081 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1161991615(__this, ___item0, method) ((  bool (*) (ValueCollection_t2650563081 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1161991615_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m969365381_gshared (ValueCollection_t2650563081 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m969365381(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2650563081 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m969365381_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3149111765_gshared (ValueCollection_t2650563081 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3149111765(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2650563081 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3149111765_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1646348086_gshared (ValueCollection_t2650563081 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1646348086(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2650563081 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1646348086_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3606844835_gshared (ValueCollection_t2650563081 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3606844835(__this, method) ((  bool (*) (ValueCollection_t2650563081 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3606844835_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m999719041_gshared (ValueCollection_t2650563081 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m999719041(__this, method) ((  bool (*) (ValueCollection_t2650563081 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m999719041_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1142149105_gshared (ValueCollection_t2650563081 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1142149105(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2650563081 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1142149105_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3746321839_gshared (ValueCollection_t2650563081 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3746321839(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2650563081 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3746321839_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1339068706  ValueCollection_GetEnumerator_m1206592724_gshared (ValueCollection_t2650563081 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1206592724(__this, method) ((  Enumerator_t1339068706  (*) (ValueCollection_t2650563081 *, const MethodInfo*))ValueCollection_GetEnumerator_m1206592724_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVRButtonId,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1017264669_gshared (ValueCollection_t2650563081 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1017264669(__this, method) ((  int32_t (*) (ValueCollection_t2650563081 *, const MethodInfo*))ValueCollection_get_Count_m1017264669_gshared)(__this, method)
