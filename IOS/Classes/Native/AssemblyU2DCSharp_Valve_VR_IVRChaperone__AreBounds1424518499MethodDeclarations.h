﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperone/_AreBoundsVisible
struct _AreBoundsVisible_t1424518499;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperone/_AreBoundsVisible::.ctor(System.Object,System.IntPtr)
extern "C"  void _AreBoundsVisible__ctor_m515403392 (_AreBoundsVisible_t1424518499 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperone/_AreBoundsVisible::Invoke()
extern "C"  bool _AreBoundsVisible_Invoke_m2726265744 (_AreBoundsVisible_t1424518499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperone/_AreBoundsVisible::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _AreBoundsVisible_BeginInvoke_m1087013549 (_AreBoundsVisible_t1424518499 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperone/_AreBoundsVisible::EndInvoke(System.IAsyncResult)
extern "C"  bool _AreBoundsVisible_EndInvoke_m4010557932 (_AreBoundsVisible_t1424518499 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
