﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.RadialButtonIcon
struct RadialButtonIcon_t700423124;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.RadialButtonIcon::.ctor()
extern "C"  void RadialButtonIcon__ctor_m3087344578 (RadialButtonIcon_t700423124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
