﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ArrowheadRotation
struct ArrowheadRotation_t4253007731;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.ArrowheadRotation::.ctor()
extern "C"  void ArrowheadRotation__ctor_m1749915203 (ArrowheadRotation_t4253007731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArrowheadRotation::Start()
extern "C"  void ArrowheadRotation_Start_m232811207 (ArrowheadRotation_t4253007731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
