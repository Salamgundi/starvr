﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayTexelAspect
struct _GetOverlayTexelAspect_t1835744573;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayTexelAspect::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayTexelAspect__ctor_m2056036498 (_GetOverlayTexelAspect_t1835744573 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTexelAspect::Invoke(System.UInt64,System.Single&)
extern "C"  int32_t _GetOverlayTexelAspect_Invoke_m630228854 (_GetOverlayTexelAspect_t1835744573 * __this, uint64_t ___ulOverlayHandle0, float* ___pfTexelAspect1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayTexelAspect::BeginInvoke(System.UInt64,System.Single&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayTexelAspect_BeginInvoke_m2888330703 (_GetOverlayTexelAspect_t1835744573 * __this, uint64_t ___ulOverlayHandle0, float* ___pfTexelAspect1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTexelAspect::EndInvoke(System.Single&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayTexelAspect_EndInvoke_m2033724953 (_GetOverlayTexelAspect_t1835744573 * __this, float* ___pfTexelAspect0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
