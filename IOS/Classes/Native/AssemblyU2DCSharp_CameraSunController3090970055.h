﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSunController
struct  CameraSunController_t3090970055  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject CameraSunController::OptOn1
	GameObject_t1756533147 * ___OptOn1_2;
	// UnityEngine.GameObject CameraSunController::OptOn2
	GameObject_t1756533147 * ___OptOn2_3;
	// UnityEngine.GameObject CameraSunController::OptOff1
	GameObject_t1756533147 * ___OptOff1_4;
	// UnityEngine.GameObject CameraSunController::OptOff2
	GameObject_t1756533147 * ___OptOff2_5;
	// UnityEngine.GameObject CameraSunController::OptOff3
	GameObject_t1756533147 * ___OptOff3_6;
	// UnityEngine.GameObject CameraSunController::OnSun
	GameObject_t1756533147 * ___OnSun_7;
	// UnityEngine.GameObject CameraSunController::sun2
	GameObject_t1756533147 * ___sun2_8;
	// UnityEngine.GameObject CameraSunController::sun3
	GameObject_t1756533147 * ___sun3_9;
	// UnityEngine.GameObject CameraSunController::sun4
	GameObject_t1756533147 * ___sun4_10;
	// UnityEngine.GameObject CameraSunController::sun5
	GameObject_t1756533147 * ___sun5_11;
	// UnityEngine.GameObject CameraSunController::sun6
	GameObject_t1756533147 * ___sun6_12;
	// UnityEngine.GameObject CameraSunController::sun7
	GameObject_t1756533147 * ___sun7_13;
	// UnityEngine.GameObject CameraSunController::sun8
	GameObject_t1756533147 * ___sun8_14;
	// UnityEngine.GameObject CameraSunController::sun9
	GameObject_t1756533147 * ___sun9_15;
	// UnityEngine.GameObject CameraSunController::sun10
	GameObject_t1756533147 * ___sun10_16;

public:
	inline static int32_t get_offset_of_OptOn1_2() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___OptOn1_2)); }
	inline GameObject_t1756533147 * get_OptOn1_2() const { return ___OptOn1_2; }
	inline GameObject_t1756533147 ** get_address_of_OptOn1_2() { return &___OptOn1_2; }
	inline void set_OptOn1_2(GameObject_t1756533147 * value)
	{
		___OptOn1_2 = value;
		Il2CppCodeGenWriteBarrier(&___OptOn1_2, value);
	}

	inline static int32_t get_offset_of_OptOn2_3() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___OptOn2_3)); }
	inline GameObject_t1756533147 * get_OptOn2_3() const { return ___OptOn2_3; }
	inline GameObject_t1756533147 ** get_address_of_OptOn2_3() { return &___OptOn2_3; }
	inline void set_OptOn2_3(GameObject_t1756533147 * value)
	{
		___OptOn2_3 = value;
		Il2CppCodeGenWriteBarrier(&___OptOn2_3, value);
	}

	inline static int32_t get_offset_of_OptOff1_4() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___OptOff1_4)); }
	inline GameObject_t1756533147 * get_OptOff1_4() const { return ___OptOff1_4; }
	inline GameObject_t1756533147 ** get_address_of_OptOff1_4() { return &___OptOff1_4; }
	inline void set_OptOff1_4(GameObject_t1756533147 * value)
	{
		___OptOff1_4 = value;
		Il2CppCodeGenWriteBarrier(&___OptOff1_4, value);
	}

	inline static int32_t get_offset_of_OptOff2_5() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___OptOff2_5)); }
	inline GameObject_t1756533147 * get_OptOff2_5() const { return ___OptOff2_5; }
	inline GameObject_t1756533147 ** get_address_of_OptOff2_5() { return &___OptOff2_5; }
	inline void set_OptOff2_5(GameObject_t1756533147 * value)
	{
		___OptOff2_5 = value;
		Il2CppCodeGenWriteBarrier(&___OptOff2_5, value);
	}

	inline static int32_t get_offset_of_OptOff3_6() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___OptOff3_6)); }
	inline GameObject_t1756533147 * get_OptOff3_6() const { return ___OptOff3_6; }
	inline GameObject_t1756533147 ** get_address_of_OptOff3_6() { return &___OptOff3_6; }
	inline void set_OptOff3_6(GameObject_t1756533147 * value)
	{
		___OptOff3_6 = value;
		Il2CppCodeGenWriteBarrier(&___OptOff3_6, value);
	}

	inline static int32_t get_offset_of_OnSun_7() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___OnSun_7)); }
	inline GameObject_t1756533147 * get_OnSun_7() const { return ___OnSun_7; }
	inline GameObject_t1756533147 ** get_address_of_OnSun_7() { return &___OnSun_7; }
	inline void set_OnSun_7(GameObject_t1756533147 * value)
	{
		___OnSun_7 = value;
		Il2CppCodeGenWriteBarrier(&___OnSun_7, value);
	}

	inline static int32_t get_offset_of_sun2_8() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___sun2_8)); }
	inline GameObject_t1756533147 * get_sun2_8() const { return ___sun2_8; }
	inline GameObject_t1756533147 ** get_address_of_sun2_8() { return &___sun2_8; }
	inline void set_sun2_8(GameObject_t1756533147 * value)
	{
		___sun2_8 = value;
		Il2CppCodeGenWriteBarrier(&___sun2_8, value);
	}

	inline static int32_t get_offset_of_sun3_9() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___sun3_9)); }
	inline GameObject_t1756533147 * get_sun3_9() const { return ___sun3_9; }
	inline GameObject_t1756533147 ** get_address_of_sun3_9() { return &___sun3_9; }
	inline void set_sun3_9(GameObject_t1756533147 * value)
	{
		___sun3_9 = value;
		Il2CppCodeGenWriteBarrier(&___sun3_9, value);
	}

	inline static int32_t get_offset_of_sun4_10() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___sun4_10)); }
	inline GameObject_t1756533147 * get_sun4_10() const { return ___sun4_10; }
	inline GameObject_t1756533147 ** get_address_of_sun4_10() { return &___sun4_10; }
	inline void set_sun4_10(GameObject_t1756533147 * value)
	{
		___sun4_10 = value;
		Il2CppCodeGenWriteBarrier(&___sun4_10, value);
	}

	inline static int32_t get_offset_of_sun5_11() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___sun5_11)); }
	inline GameObject_t1756533147 * get_sun5_11() const { return ___sun5_11; }
	inline GameObject_t1756533147 ** get_address_of_sun5_11() { return &___sun5_11; }
	inline void set_sun5_11(GameObject_t1756533147 * value)
	{
		___sun5_11 = value;
		Il2CppCodeGenWriteBarrier(&___sun5_11, value);
	}

	inline static int32_t get_offset_of_sun6_12() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___sun6_12)); }
	inline GameObject_t1756533147 * get_sun6_12() const { return ___sun6_12; }
	inline GameObject_t1756533147 ** get_address_of_sun6_12() { return &___sun6_12; }
	inline void set_sun6_12(GameObject_t1756533147 * value)
	{
		___sun6_12 = value;
		Il2CppCodeGenWriteBarrier(&___sun6_12, value);
	}

	inline static int32_t get_offset_of_sun7_13() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___sun7_13)); }
	inline GameObject_t1756533147 * get_sun7_13() const { return ___sun7_13; }
	inline GameObject_t1756533147 ** get_address_of_sun7_13() { return &___sun7_13; }
	inline void set_sun7_13(GameObject_t1756533147 * value)
	{
		___sun7_13 = value;
		Il2CppCodeGenWriteBarrier(&___sun7_13, value);
	}

	inline static int32_t get_offset_of_sun8_14() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___sun8_14)); }
	inline GameObject_t1756533147 * get_sun8_14() const { return ___sun8_14; }
	inline GameObject_t1756533147 ** get_address_of_sun8_14() { return &___sun8_14; }
	inline void set_sun8_14(GameObject_t1756533147 * value)
	{
		___sun8_14 = value;
		Il2CppCodeGenWriteBarrier(&___sun8_14, value);
	}

	inline static int32_t get_offset_of_sun9_15() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___sun9_15)); }
	inline GameObject_t1756533147 * get_sun9_15() const { return ___sun9_15; }
	inline GameObject_t1756533147 ** get_address_of_sun9_15() { return &___sun9_15; }
	inline void set_sun9_15(GameObject_t1756533147 * value)
	{
		___sun9_15 = value;
		Il2CppCodeGenWriteBarrier(&___sun9_15, value);
	}

	inline static int32_t get_offset_of_sun10_16() { return static_cast<int32_t>(offsetof(CameraSunController_t3090970055, ___sun10_16)); }
	inline GameObject_t1756533147 * get_sun10_16() const { return ___sun10_16; }
	inline GameObject_t1756533147 ** get_address_of_sun10_16() { return &___sun10_16; }
	inline void set_sun10_16(GameObject_t1756533147 * value)
	{
		___sun10_16 = value;
		Il2CppCodeGenWriteBarrier(&___sun10_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
