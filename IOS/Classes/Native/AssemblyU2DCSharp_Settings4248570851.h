﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Settings
struct  Settings_t4248570851  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Settings::Constellations
	GameObject_t1756533147 * ___Constellations_2;
	// System.Single Settings::volume
	float ___volume_3;
	// System.Boolean Settings::ConOn
	bool ___ConOn_4;

public:
	inline static int32_t get_offset_of_Constellations_2() { return static_cast<int32_t>(offsetof(Settings_t4248570851, ___Constellations_2)); }
	inline GameObject_t1756533147 * get_Constellations_2() const { return ___Constellations_2; }
	inline GameObject_t1756533147 ** get_address_of_Constellations_2() { return &___Constellations_2; }
	inline void set_Constellations_2(GameObject_t1756533147 * value)
	{
		___Constellations_2 = value;
		Il2CppCodeGenWriteBarrier(&___Constellations_2, value);
	}

	inline static int32_t get_offset_of_volume_3() { return static_cast<int32_t>(offsetof(Settings_t4248570851, ___volume_3)); }
	inline float get_volume_3() const { return ___volume_3; }
	inline float* get_address_of_volume_3() { return &___volume_3; }
	inline void set_volume_3(float value)
	{
		___volume_3 = value;
	}

	inline static int32_t get_offset_of_ConOn_4() { return static_cast<int32_t>(offsetof(Settings_t4248570851, ___ConOn_4)); }
	inline bool get_ConOn_4() const { return ___ConOn_4; }
	inline bool* get_address_of_ConOn_4() { return &___ConOn_4; }
	inline void set_ConOn_4(bool value)
	{
		___ConOn_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
