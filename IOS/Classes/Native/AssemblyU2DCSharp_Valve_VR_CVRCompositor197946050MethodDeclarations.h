﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRCompositor
struct CVRCompositor_t197946050;
// Valve.VR.TrackedDevicePose_t[]
struct TrackedDevicePose_tU5BU5D_t2897272049;
// Valve.VR.Texture_t[]
struct Texture_tU5BU5D_t3142294487;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingUniverseOrigin1464400093.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRCompositorError3948578210.h"
#include "AssemblyU2DCSharp_Valve_VR_TrackedDevicePose_t1668551120.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"
#include "AssemblyU2DCSharp_Valve_VR_Texture_t3277130850.h"
#include "AssemblyU2DCSharp_Valve_VR_VRTextureBounds_t1897807375.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRSubmitFlags2736259668.h"
#include "AssemblyU2DCSharp_Valve_VR_Compositor_FrameTiming2839634313.h"
#include "AssemblyU2DCSharp_Valve_VR_Compositor_CumulativeSta450065686.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdColor_t1780554589.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"

// System.Void Valve.VR.CVRCompositor::.ctor(System.IntPtr)
extern "C"  void CVRCompositor__ctor_m837364259 (CVRCompositor_t197946050 * __this, IntPtr_t ___pInterface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::SetTrackingSpace(Valve.VR.ETrackingUniverseOrigin)
extern "C"  void CVRCompositor_SetTrackingSpace_m944595227 (CVRCompositor_t197946050 * __this, int32_t ___eOrigin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.ETrackingUniverseOrigin Valve.VR.CVRCompositor::GetTrackingSpace()
extern "C"  int32_t CVRCompositor_GetTrackingSpace_m2911781788 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.CVRCompositor::WaitGetPoses(Valve.VR.TrackedDevicePose_t[],Valve.VR.TrackedDevicePose_t[])
extern "C"  int32_t CVRCompositor_WaitGetPoses_m1011843025 (CVRCompositor_t197946050 * __this, TrackedDevicePose_tU5BU5D_t2897272049* ___pRenderPoseArray0, TrackedDevicePose_tU5BU5D_t2897272049* ___pGamePoseArray1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.CVRCompositor::GetLastPoses(Valve.VR.TrackedDevicePose_t[],Valve.VR.TrackedDevicePose_t[])
extern "C"  int32_t CVRCompositor_GetLastPoses_m1260604006 (CVRCompositor_t197946050 * __this, TrackedDevicePose_tU5BU5D_t2897272049* ___pRenderPoseArray0, TrackedDevicePose_tU5BU5D_t2897272049* ___pGamePoseArray1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.CVRCompositor::GetLastPoseForTrackedDeviceIndex(System.UInt32,Valve.VR.TrackedDevicePose_t&,Valve.VR.TrackedDevicePose_t&)
extern "C"  int32_t CVRCompositor_GetLastPoseForTrackedDeviceIndex_m3240668192 (CVRCompositor_t197946050 * __this, uint32_t ___unDeviceIndex0, TrackedDevicePose_t_t1668551120 * ___pOutputPose1, TrackedDevicePose_t_t1668551120 * ___pOutputGamePose2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.CVRCompositor::Submit(Valve.VR.EVREye,Valve.VR.Texture_t&,Valve.VR.VRTextureBounds_t&,Valve.VR.EVRSubmitFlags)
extern "C"  int32_t CVRCompositor_Submit_m3760151261 (CVRCompositor_t197946050 * __this, int32_t ___eEye0, Texture_t_t3277130850 * ___pTexture1, VRTextureBounds_t_t1897807375 * ___pBounds2, int32_t ___nSubmitFlags3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::ClearLastSubmittedFrame()
extern "C"  void CVRCompositor_ClearLastSubmittedFrame_m1228113794 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::PostPresentHandoff()
extern "C"  void CVRCompositor_PostPresentHandoff_m1100632456 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRCompositor::GetFrameTiming(Valve.VR.Compositor_FrameTiming&,System.UInt32)
extern "C"  bool CVRCompositor_GetFrameTiming_m99094131 (CVRCompositor_t197946050 * __this, Compositor_FrameTiming_t2839634313 * ___pTiming0, uint32_t ___unFramesAgo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRCompositor::GetFrameTimings(Valve.VR.Compositor_FrameTiming&,System.UInt32)
extern "C"  uint32_t CVRCompositor_GetFrameTimings_m1517206991 (CVRCompositor_t197946050 * __this, Compositor_FrameTiming_t2839634313 * ___pTiming0, uint32_t ___nFrames1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.CVRCompositor::GetFrameTimeRemaining()
extern "C"  float CVRCompositor_GetFrameTimeRemaining_m3347569673 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::GetCumulativeStats(Valve.VR.Compositor_CumulativeStats&,System.UInt32)
extern "C"  void CVRCompositor_GetCumulativeStats_m156890435 (CVRCompositor_t197946050 * __this, Compositor_CumulativeStats_t450065686 * ___pStats0, uint32_t ___nStatsSizeInBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::FadeToColor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C"  void CVRCompositor_FadeToColor_m341369179 (CVRCompositor_t197946050 * __this, float ___fSeconds0, float ___fRed1, float ___fGreen2, float ___fBlue3, float ___fAlpha4, bool ___bBackground5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdColor_t Valve.VR.CVRCompositor::GetCurrentFadeColor(System.Boolean)
extern "C"  HmdColor_t_t1780554589  CVRCompositor_GetCurrentFadeColor_m3229640144 (CVRCompositor_t197946050 * __this, bool ___bBackground0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::FadeGrid(System.Single,System.Boolean)
extern "C"  void CVRCompositor_FadeGrid_m2002200473 (CVRCompositor_t197946050 * __this, float ___fSeconds0, bool ___bFadeIn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.CVRCompositor::GetCurrentGridAlpha()
extern "C"  float CVRCompositor_GetCurrentGridAlpha_m790409240 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.CVRCompositor::SetSkyboxOverride(Valve.VR.Texture_t[])
extern "C"  int32_t CVRCompositor_SetSkyboxOverride_m1129036764 (CVRCompositor_t197946050 * __this, Texture_tU5BU5D_t3142294487* ___pTextures0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::ClearSkyboxOverride()
extern "C"  void CVRCompositor_ClearSkyboxOverride_m2875147730 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::CompositorBringToFront()
extern "C"  void CVRCompositor_CompositorBringToFront_m3550090872 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::CompositorGoToBack()
extern "C"  void CVRCompositor_CompositorGoToBack_m4015594160 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::CompositorQuit()
extern "C"  void CVRCompositor_CompositorQuit_m1577551569 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRCompositor::IsFullscreen()
extern "C"  bool CVRCompositor_IsFullscreen_m2163982150 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRCompositor::GetCurrentSceneFocusProcess()
extern "C"  uint32_t CVRCompositor_GetCurrentSceneFocusProcess_m1681794348 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRCompositor::GetLastFrameRenderer()
extern "C"  uint32_t CVRCompositor_GetLastFrameRenderer_m30874282 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRCompositor::CanRenderScene()
extern "C"  bool CVRCompositor_CanRenderScene_m635356961 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::ShowMirrorWindow()
extern "C"  void CVRCompositor_ShowMirrorWindow_m472918043 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::HideMirrorWindow()
extern "C"  void CVRCompositor_HideMirrorWindow_m3717119978 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRCompositor::IsMirrorWindowVisible()
extern "C"  bool CVRCompositor_IsMirrorWindowVisible_m295342592 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::CompositorDumpImages()
extern "C"  void CVRCompositor_CompositorDumpImages_m6227924 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRCompositor::ShouldAppRenderWithLowResources()
extern "C"  bool CVRCompositor_ShouldAppRenderWithLowResources_m2522030402 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::ForceInterleavedReprojectionOn(System.Boolean)
extern "C"  void CVRCompositor_ForceInterleavedReprojectionOn_m1924933791 (CVRCompositor_t197946050 * __this, bool ___bOverride0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::ForceReconnectProcess()
extern "C"  void CVRCompositor_ForceReconnectProcess_m3038092896 (CVRCompositor_t197946050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::SuspendRendering(System.Boolean)
extern "C"  void CVRCompositor_SuspendRendering_m1533528808 (CVRCompositor_t197946050 * __this, bool ___bSuspend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.CVRCompositor::GetMirrorTextureD3D11(Valve.VR.EVREye,System.IntPtr,System.IntPtr&)
extern "C"  int32_t CVRCompositor_GetMirrorTextureD3D11_m2633802975 (CVRCompositor_t197946050 * __this, int32_t ___eEye0, IntPtr_t ___pD3D11DeviceOrResource1, IntPtr_t* ___ppD3D11ShaderResourceView2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::ReleaseMirrorTextureD3D11(System.IntPtr)
extern "C"  void CVRCompositor_ReleaseMirrorTextureD3D11_m2426011799 (CVRCompositor_t197946050 * __this, IntPtr_t ___pD3D11ShaderResourceView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.CVRCompositor::GetMirrorTextureGL(Valve.VR.EVREye,System.UInt32&,System.IntPtr)
extern "C"  int32_t CVRCompositor_GetMirrorTextureGL_m1130105549 (CVRCompositor_t197946050 * __this, int32_t ___eEye0, uint32_t* ___pglTextureId1, IntPtr_t ___pglSharedTextureHandle2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRCompositor::ReleaseSharedGLTexture(System.UInt32,System.IntPtr)
extern "C"  bool CVRCompositor_ReleaseSharedGLTexture_m4080639923 (CVRCompositor_t197946050 * __this, uint32_t ___glTextureId0, IntPtr_t ___glSharedTextureHandle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::LockGLSharedTextureForAccess(System.IntPtr)
extern "C"  void CVRCompositor_LockGLSharedTextureForAccess_m1395481434 (CVRCompositor_t197946050 * __this, IntPtr_t ___glSharedTextureHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRCompositor::UnlockGLSharedTextureForAccess(System.IntPtr)
extern "C"  void CVRCompositor_UnlockGLSharedTextureForAccess_m3646655761 (CVRCompositor_t197946050 * __this, IntPtr_t ___glSharedTextureHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRCompositor::GetVulkanInstanceExtensionsRequired(System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t CVRCompositor_GetVulkanInstanceExtensionsRequired_m136717449 (CVRCompositor_t197946050 * __this, StringBuilder_t1221177846 * ___pchValue0, uint32_t ___unBufferSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRCompositor::GetVulkanDeviceExtensionsRequired(System.IntPtr,System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t CVRCompositor_GetVulkanDeviceExtensionsRequired_m3340680038 (CVRCompositor_t197946050 * __this, IntPtr_t ___pPhysicalDevice0, StringBuilder_t1221177846 * ___pchValue1, uint32_t ___unBufferSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
