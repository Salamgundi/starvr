﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3963639764.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23104887502.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2770994422_gshared (InternalEnumerator_1_t3963639764 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2770994422(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3963639764 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2770994422_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1056733090_gshared (InternalEnumerator_1_t3963639764 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1056733090(__this, method) ((  void (*) (InternalEnumerator_1_t3963639764 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1056733090_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2023646268_gshared (InternalEnumerator_1_t3963639764 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2023646268(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3963639764 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2023646268_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3249713521_gshared (InternalEnumerator_1_t3963639764 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3249713521(__this, method) ((  void (*) (InternalEnumerator_1_t3963639764 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3249713521_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3360821254_gshared (InternalEnumerator_1_t3963639764 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3360821254(__this, method) ((  bool (*) (InternalEnumerator_1_t3963639764 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3360821254_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t3104887502  InternalEnumerator_1_get_Current_m4277271057_gshared (InternalEnumerator_1_t3963639764 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4277271057(__this, method) ((  KeyValuePair_2_t3104887502  (*) (InternalEnumerator_1_t3963639764 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4277271057_gshared)(__this, method)
