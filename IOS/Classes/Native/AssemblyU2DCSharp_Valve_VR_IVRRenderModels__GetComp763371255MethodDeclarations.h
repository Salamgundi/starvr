﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_GetComponentCount
struct _GetComponentCount_t763371255;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_GetComponentCount::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetComponentCount__ctor_m4292303028 (_GetComponentCount_t763371255 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRRenderModels/_GetComponentCount::Invoke(System.String)
extern "C"  uint32_t _GetComponentCount_Invoke_m2742369791 (_GetComponentCount_t763371255 * __this, String_t* ___pchRenderModelName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetComponentCount::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetComponentCount_BeginInvoke_m152053003 (_GetComponentCount_t763371255 * __this, String_t* ___pchRenderModelName0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRRenderModels/_GetComponentCount::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetComponentCount_EndInvoke_m3953108151 (_GetComponentCount_t763371255 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
