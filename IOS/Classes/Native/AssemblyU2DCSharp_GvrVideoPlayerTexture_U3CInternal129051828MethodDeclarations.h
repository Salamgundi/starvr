﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey2
struct U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey2::.ctor()
extern "C"  void U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2__ctor_m4150867243 (U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey2::<>m__0()
extern "C"  void U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_U3CU3Em__0_m3900346486 (U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
