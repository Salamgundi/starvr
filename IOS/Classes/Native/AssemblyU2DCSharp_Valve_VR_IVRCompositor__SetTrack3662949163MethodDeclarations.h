﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_SetTrackingSpace
struct _SetTrackingSpace_t3662949163;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingUniverseOrigin1464400093.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_SetTrackingSpace::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetTrackingSpace__ctor_m2084596174 (_SetTrackingSpace_t3662949163 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_SetTrackingSpace::Invoke(Valve.VR.ETrackingUniverseOrigin)
extern "C"  void _SetTrackingSpace_Invoke_m3731131925 (_SetTrackingSpace_t3662949163 * __this, int32_t ___eOrigin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_SetTrackingSpace::BeginInvoke(Valve.VR.ETrackingUniverseOrigin,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetTrackingSpace_BeginInvoke_m1796365910 (_SetTrackingSpace_t3662949163 * __this, int32_t ___eOrigin0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_SetTrackingSpace::EndInvoke(System.IAsyncResult)
extern "C"  void _SetTrackingSpace_EndInvoke_m2737042724 (_SetTrackingSpace_t3662949163 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
