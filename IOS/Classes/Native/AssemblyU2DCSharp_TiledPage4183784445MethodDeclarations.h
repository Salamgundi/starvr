﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TiledPage
struct TiledPage_t4183784445;

#include "codegen/il2cpp-codegen.h"

// System.Void TiledPage::.ctor()
extern "C"  void TiledPage__ctor_m855545878 (TiledPage_t4183784445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
