﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.GrabAttachMechanics.VRTK_ChildOfControllerGrabAttach
struct VRTK_ChildOfControllerGrabAttach_t1018385220;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"

// System.Void VRTK.GrabAttachMechanics.VRTK_ChildOfControllerGrabAttach::.ctor()
extern "C"  void VRTK_ChildOfControllerGrabAttach__ctor_m1176193066 (VRTK_ChildOfControllerGrabAttach_t1018385220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.GrabAttachMechanics.VRTK_ChildOfControllerGrabAttach::StartGrab(UnityEngine.GameObject,UnityEngine.GameObject,UnityEngine.Rigidbody)
extern "C"  bool VRTK_ChildOfControllerGrabAttach_StartGrab_m3235356518 (VRTK_ChildOfControllerGrabAttach_t1018385220 * __this, GameObject_t1756533147 * ___grabbingObject0, GameObject_t1756533147 * ___givenGrabbedObject1, Rigidbody_t4233889191 * ___givenControllerAttachPoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_ChildOfControllerGrabAttach::StopGrab(System.Boolean)
extern "C"  void VRTK_ChildOfControllerGrabAttach_StopGrab_m3362072219 (VRTK_ChildOfControllerGrabAttach_t1018385220 * __this, bool ___applyGrabbingObjectVelocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_ChildOfControllerGrabAttach::Initialise()
extern "C"  void VRTK_ChildOfControllerGrabAttach_Initialise_m1503320115 (VRTK_ChildOfControllerGrabAttach_t1018385220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_ChildOfControllerGrabAttach::SetSnappedObjectPosition(UnityEngine.GameObject)
extern "C"  void VRTK_ChildOfControllerGrabAttach_SetSnappedObjectPosition_m3642365313 (VRTK_ChildOfControllerGrabAttach_t1018385220 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_ChildOfControllerGrabAttach::SnapObjectToGrabToController(UnityEngine.GameObject)
extern "C"  void VRTK_ChildOfControllerGrabAttach_SnapObjectToGrabToController_m852062309 (VRTK_ChildOfControllerGrabAttach_t1018385220 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
