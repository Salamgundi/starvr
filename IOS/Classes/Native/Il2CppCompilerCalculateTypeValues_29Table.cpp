﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_TeleportEventHandler2417981165.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BasicTeleport3532761337.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BasicTeleport_U3CInitL2156630853.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ButtonControl2487860925.h"
#include "AssemblyU2DCSharp_VRTK_DashTeleportEventArgs2197253242.h"
#include "AssemblyU2DCSharp_VRTK_DashTeleportEventHandler1179650517.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DashTeleport1206199485.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DashTeleport_U3ClerpTo2809459013.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_HeightAdjustTeleport2696079433.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_MoveInPlace3384869737.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_MoveInPlace_ControlOpt4045712330.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_MoveInPlace_Directiona2902995460.h"
#include "AssemblyU2DCSharp_VRTK_ObjectControlEventArgs2459490319.h"
#include "AssemblyU2DCSharp_VRTK_ObjectControlEventHandler1756898952.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectControl724022372.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectControl_Direction626792020.h"
#include "AssemblyU2DCSharp_VRTK_PlayerClimbEventArgs2537585745.h"
#include "AssemblyU2DCSharp_VRTK_PlayerClimbEventHandler2826600604.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PlayerClimb322130288.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_RoomExtender3041247552.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_RoomExtender_MovementF2403078603.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TeleportDisableOnContr3503067553.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TeleportDisableOnContro513607455.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TeleportDisableOnHeadset56384818.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TeleportDisableOnHeads3548001882.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadControl3191058221.h"
#include "AssemblyU2DCSharp_VRTK_TouchpadMovementAxisEventAr1237298133.h"
#include "AssemblyU2DCSharp_VRTK_TouchpadMovementAxisEventHa2208405138.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement1979813355.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_Verti2238062081.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_Horiz3153229385.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_AxisM2300405083.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_AxisM1724819570.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadWalking2507848959.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BasePointerRenderer1270536273.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BasePointerRenderer_Vis517960985.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BasePointerRenderer_Po2399923839.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BezierPointerRenderer3371893943.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_StraightPointerRendere3315603018.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BasePointer3466805540.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BasePointer_pointerVisi986354463.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BasePointer_PointerOri2805273516.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BezierPointer2668864506.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventArgs1852451661.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventHandl1148830384.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DestinationMarker667613644.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PlayAreaCursor3566057915.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PlayAreaCollider1466370881.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Pointer2647108841.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SimplePointer1980002747.h"
#include "AssemblyU2DCSharp_VRTK_BodyPhysicsEventArgs2230131654.h"
#include "AssemblyU2DCSharp_VRTK_BodyPhysicsEventHandler3963963105.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BodyPhysics3414085265.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BodyPhysics_FallingRes3718949972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BodyPhysics_U3CRestore1574376249.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventArgs1242373387.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventHandle3119610762.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_HeadsetCollision2015187094.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_HeadsetCollider4272516266.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_HeadsetCollisionFade3608385924.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetControllerAwareEvent2653531721.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetControllerAwareEvent1106036588.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_HeadsetControllerAware1678000416.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetFadeEventArgs2892542019.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetFadeEventHandler2012619754.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_HeadsetFade3539061086.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_HipTracking3141756816.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PositionRewind3080560492.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UICanvas1283311654.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointerAutoActivator1282719345.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIDraggableItem2269178406.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIDropZone1661569883.h"
#include "AssemblyU2DCSharp_VRTK_UIPointerEventArgs1171985978.h"
#include "AssemblyU2DCSharp_VRTK_UIPointerEventHandler988663103.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer2714926455.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer_ActivationMe3611442703.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer_ClickMethods1099240745.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer_U3CWaitForPo2911062424.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectFollow3175963762.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_RigidbodyFollow4264717774.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_RigidbodyFollow_Moveme4148486814.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TransformFollow3532748285.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TransformFollow_Follow1516682274.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_BasicT776645422.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Basic4156405881.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_BodyP2330929566.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_BodyP1260615097.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Butto4285437623.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Butto2046806484.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Contro692650506.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Contr3966949053.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Contr3920462976.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Contr3606750807.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Contr1877972230.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Contr2200943093.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_DashT1858955576.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_DashT1688415125.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Desti2475004431.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Destin599160530.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Heads2856701609.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (TeleportEventHandler_t2417981165), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (VRTK_BasicTeleport_t3532761337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2901[16] = 
{
	VRTK_BasicTeleport_t3532761337::get_offset_of_blinkTransitionSpeed_2(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_distanceBlinkDelay_3(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_headsetPositionCompensation_4(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_targetListPolicy_5(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_navMeshLimitDistance_6(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_Teleporting_7(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_Teleported_8(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_headset_9(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_playArea_10(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_adjustYForTerrain_11(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_enableTeleport_12(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_blinkPause_13(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_fadeInTime_14(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_maxBlinkTransitionSpeed_15(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_maxBlinkDistance_16(),
	VRTK_BasicTeleport_t3532761337::get_offset_of_initaliseListeners_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (U3CInitListenersAtEndOfFrameU3Ec__Iterator0_t2156630853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2902[4] = 
{
	U3CInitListenersAtEndOfFrameU3Ec__Iterator0_t2156630853::get_offset_of_U24this_0(),
	U3CInitListenersAtEndOfFrameU3Ec__Iterator0_t2156630853::get_offset_of_U24current_1(),
	U3CInitListenersAtEndOfFrameU3Ec__Iterator0_t2156630853::get_offset_of_U24disposing_2(),
	U3CInitListenersAtEndOfFrameU3Ec__Iterator0_t2156630853::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (VRTK_ButtonControl_t2487860925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2903[13] = 
{
	VRTK_ButtonControl_t2487860925::get_offset_of_forwardButton_22(),
	VRTK_ButtonControl_t2487860925::get_offset_of_backwardButton_23(),
	VRTK_ButtonControl_t2487860925::get_offset_of_leftButton_24(),
	VRTK_ButtonControl_t2487860925::get_offset_of_rightButton_25(),
	VRTK_ButtonControl_t2487860925::get_offset_of_forwardPressed_26(),
	VRTK_ButtonControl_t2487860925::get_offset_of_backwardPressed_27(),
	VRTK_ButtonControl_t2487860925::get_offset_of_leftPressed_28(),
	VRTK_ButtonControl_t2487860925::get_offset_of_rightPressed_29(),
	VRTK_ButtonControl_t2487860925::get_offset_of_subscribedForwardButton_30(),
	VRTK_ButtonControl_t2487860925::get_offset_of_subscribedBackwardButton_31(),
	VRTK_ButtonControl_t2487860925::get_offset_of_subscribedLeftButton_32(),
	VRTK_ButtonControl_t2487860925::get_offset_of_subscribedRightButton_33(),
	VRTK_ButtonControl_t2487860925::get_offset_of_axisDeadzone_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (DashTeleportEventArgs_t2197253242)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2904[1] = 
{
	DashTeleportEventArgs_t2197253242::get_offset_of_hits_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (DashTeleportEventHandler_t1179650517), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (VRTK_DashTeleport_t1206199485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2906[9] = 
{
	VRTK_DashTeleport_t1206199485::get_offset_of_normalLerpTime_19(),
	VRTK_DashTeleport_t1206199485::get_offset_of_minSpeedMps_20(),
	VRTK_DashTeleport_t1206199485::get_offset_of_capsuleTopOffset_21(),
	VRTK_DashTeleport_t1206199485::get_offset_of_capsuleBottomOffset_22(),
	VRTK_DashTeleport_t1206199485::get_offset_of_capsuleRadius_23(),
	VRTK_DashTeleport_t1206199485::get_offset_of_WillDashThruObjects_24(),
	VRTK_DashTeleport_t1206199485::get_offset_of_DashedThruObjects_25(),
	VRTK_DashTeleport_t1206199485::get_offset_of_minDistanceForNormalLerp_26(),
	VRTK_DashTeleport_t1206199485::get_offset_of_lerpTime_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (U3ClerpToPositionU3Ec__Iterator0_t2809459013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2907[21] = 
{
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U3CgameObjectInTheWayU3E__0_0(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U3CeyeCameraPositionU3E__1_1(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U3CeyeCameraPositionOnGroundU3E__2_2(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U3CeyeCameraRelativeToRigU3E__3_3(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_targetPosition_4(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U3CtargetEyeCameraPositionU3E__4_5(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U3CdirectionU3E__5_6(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U3CbottomPointU3E__6_7(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U3CtopPointU3E__7_8(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U3CmaxDistanceU3E__8_9(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U3CallHitsU3E__9_10(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U24locvar0_11(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U24locvar1_12(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_target_13(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U3CstartPositionU3E__A_14(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U3CelapsedTimeU3E__B_15(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U3CtU3E__C_16(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U24this_17(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U24current_18(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U24disposing_19(),
	U3ClerpToPositionU3Ec__Iterator0_t2809459013::get_offset_of_U24PC_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (VRTK_HeightAdjustTeleport_t2696079433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2908[1] = 
{
	VRTK_HeightAdjustTeleport_t2696079433::get_offset_of_layersToIgnore_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (VRTK_MoveInPlace_t3384869737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2909[30] = 
{
	VRTK_MoveInPlace_t3384869737::get_offset_of_leftController_2(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_rightController_3(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_engageButton_4(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_controlOptions_5(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_speedScale_6(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_maxSpeed_7(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_deceleration_8(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_fallingDeceleration_9(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_directionMethod_10(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_smartDecoupleThreshold_11(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_sensitivity_12(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_playArea_13(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_controllerLeftHand_14(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_controllerRightHand_15(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_headset_16(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_leftSubscribed_17(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_rightSubscribed_18(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_previousLeftControllerState_19(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_previousRightControllerState_20(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_previousEngageButton_21(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_bodyPhysics_22(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_currentlyFalling_23(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_averagePeriod_24(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_trackedObjects_25(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_movementList_26(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_previousYPositions_27(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_initalGaze_28(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_currentSpeed_29(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_direction_30(),
	VRTK_MoveInPlace_t3384869737::get_offset_of_active_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (ControlOptions_t4045712330)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2910[4] = 
{
	ControlOptions_t4045712330::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (DirectionalMethod_t2902995460)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2911[5] = 
{
	DirectionalMethod_t2902995460::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (ObjectControlEventArgs_t2459490319)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2912[7] = 
{
	ObjectControlEventArgs_t2459490319::get_offset_of_controlledGameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectControlEventArgs_t2459490319::get_offset_of_directionDevice_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectControlEventArgs_t2459490319::get_offset_of_axisDirection_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectControlEventArgs_t2459490319::get_offset_of_axis_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectControlEventArgs_t2459490319::get_offset_of_deadzone_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectControlEventArgs_t2459490319::get_offset_of_currentlyFalling_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectControlEventArgs_t2459490319::get_offset_of_modifierActive_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (ObjectControlEventHandler_t1756898952), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (VRTK_ObjectControl_t724022372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2914[20] = 
{
	VRTK_ObjectControl_t724022372::get_offset_of_controller_2(),
	VRTK_ObjectControl_t724022372::get_offset_of_deviceForDirection_3(),
	VRTK_ObjectControl_t724022372::get_offset_of_disableOtherControlsOnActive_4(),
	VRTK_ObjectControl_t724022372::get_offset_of_affectOnFalling_5(),
	VRTK_ObjectControl_t724022372::get_offset_of_controlOverrideObject_6(),
	VRTK_ObjectControl_t724022372::get_offset_of_XAxisChanged_7(),
	VRTK_ObjectControl_t724022372::get_offset_of_YAxisChanged_8(),
	VRTK_ObjectControl_t724022372::get_offset_of_controllerEvents_9(),
	VRTK_ObjectControl_t724022372::get_offset_of_bodyPhysics_10(),
	VRTK_ObjectControl_t724022372::get_offset_of_otherObjectControl_11(),
	VRTK_ObjectControl_t724022372::get_offset_of_controlledGameObject_12(),
	VRTK_ObjectControl_t724022372::get_offset_of_setControlOverrideObject_13(),
	VRTK_ObjectControl_t724022372::get_offset_of_directionDevice_14(),
	VRTK_ObjectControl_t724022372::get_offset_of_previousDeviceForDirection_15(),
	VRTK_ObjectControl_t724022372::get_offset_of_currentAxis_16(),
	VRTK_ObjectControl_t724022372::get_offset_of_storedAxis_17(),
	VRTK_ObjectControl_t724022372::get_offset_of_currentlyFalling_18(),
	VRTK_ObjectControl_t724022372::get_offset_of_modifierActive_19(),
	VRTK_ObjectControl_t724022372::get_offset_of_controlledGameObjectPreviousY_20(),
	VRTK_ObjectControl_t724022372::get_offset_of_controlledGameObjectPreviousYOffset_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (DirectionDevices_t626792020)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2915[5] = 
{
	DirectionDevices_t626792020::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (PlayerClimbEventArgs_t2537585745)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2916[2] = 
{
	PlayerClimbEventArgs_t2537585745::get_offset_of_controllerIndex_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PlayerClimbEventArgs_t2537585745::get_offset_of_target_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (PlayerClimbEventHandler_t2826600604), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (VRTK_PlayerClimb_t322130288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2918[13] = 
{
	VRTK_PlayerClimb_t322130288::get_offset_of_usePlayerScale_2(),
	VRTK_PlayerClimb_t322130288::get_offset_of_PlayerClimbStarted_3(),
	VRTK_PlayerClimb_t322130288::get_offset_of_PlayerClimbEnded_4(),
	VRTK_PlayerClimb_t322130288::get_offset_of_playArea_5(),
	VRTK_PlayerClimb_t322130288::get_offset_of_startControllerScaledLocalPosition_6(),
	VRTK_PlayerClimb_t322130288::get_offset_of_startGrabPointLocalPosition_7(),
	VRTK_PlayerClimb_t322130288::get_offset_of_startPlayAreaWorldOffset_8(),
	VRTK_PlayerClimb_t322130288::get_offset_of_grabbingController_9(),
	VRTK_PlayerClimb_t322130288::get_offset_of_climbingObject_10(),
	VRTK_PlayerClimb_t322130288::get_offset_of_climbingObjectLastRotation_11(),
	VRTK_PlayerClimb_t322130288::get_offset_of_bodyPhysics_12(),
	VRTK_PlayerClimb_t322130288::get_offset_of_isClimbing_13(),
	VRTK_PlayerClimb_t322130288::get_offset_of_useGrabbedObjectRotation_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (VRTK_RoomExtender_t3041247552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[12] = 
{
	VRTK_RoomExtender_t3041247552::get_offset_of_movementFunction_2(),
	VRTK_RoomExtender_t3041247552::get_offset_of_additionalMovementEnabled_3(),
	VRTK_RoomExtender_t3041247552::get_offset_of_additionalMovementEnabledOnButtonPress_4(),
	VRTK_RoomExtender_t3041247552::get_offset_of_additionalMovementMultiplier_5(),
	VRTK_RoomExtender_t3041247552::get_offset_of_headZoneRadius_6(),
	VRTK_RoomExtender_t3041247552::get_offset_of_debugTransform_7(),
	VRTK_RoomExtender_t3041247552::get_offset_of_relativeMovementOfCameraRig_8(),
	VRTK_RoomExtender_t3041247552::get_offset_of_movementTransform_9(),
	VRTK_RoomExtender_t3041247552::get_offset_of_playArea_10(),
	VRTK_RoomExtender_t3041247552::get_offset_of_headCirclePosition_11(),
	VRTK_RoomExtender_t3041247552::get_offset_of_lastPosition_12(),
	VRTK_RoomExtender_t3041247552::get_offset_of_lastMovement_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (MovementFunction_t2403078603)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2920[3] = 
{
	MovementFunction_t2403078603::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (VRTK_TeleportDisableOnControllerObscured_t3503067553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2921[2] = 
{
	VRTK_TeleportDisableOnControllerObscured_t3503067553::get_offset_of_basicTeleport_2(),
	VRTK_TeleportDisableOnControllerObscured_t3503067553::get_offset_of_headset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (U3CEnableAtEndOfFrameU3Ec__Iterator0_t513607455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2922[4] = 
{
	U3CEnableAtEndOfFrameU3Ec__Iterator0_t513607455::get_offset_of_U24this_0(),
	U3CEnableAtEndOfFrameU3Ec__Iterator0_t513607455::get_offset_of_U24current_1(),
	U3CEnableAtEndOfFrameU3Ec__Iterator0_t513607455::get_offset_of_U24disposing_2(),
	U3CEnableAtEndOfFrameU3Ec__Iterator0_t513607455::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (VRTK_TeleportDisableOnHeadsetCollision_t56384818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2923[2] = 
{
	VRTK_TeleportDisableOnHeadsetCollision_t56384818::get_offset_of_basicTeleport_2(),
	VRTK_TeleportDisableOnHeadsetCollision_t56384818::get_offset_of_headsetCollision_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2924[4] = 
{
	U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882::get_offset_of_U24this_0(),
	U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882::get_offset_of_U24current_1(),
	U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882::get_offset_of_U24disposing_2(),
	U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (VRTK_TouchpadControl_t3191058221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2925[5] = 
{
	VRTK_TouchpadControl_t3191058221::get_offset_of_primaryActivationButton_22(),
	VRTK_TouchpadControl_t3191058221::get_offset_of_actionModifierButton_23(),
	VRTK_TouchpadControl_t3191058221::get_offset_of_axisDeadzone_24(),
	VRTK_TouchpadControl_t3191058221::get_offset_of_touchpadFirstChange_25(),
	VRTK_TouchpadControl_t3191058221::get_offset_of_otherTouchpadControlEnabledState_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (TouchpadMovementAxisEventArgs_t1237298133)+ sizeof (Il2CppObject), sizeof(TouchpadMovementAxisEventArgs_t1237298133 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2926[2] = 
{
	TouchpadMovementAxisEventArgs_t1237298133::get_offset_of_movementType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchpadMovementAxisEventArgs_t1237298133::get_offset_of_direction_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (TouchpadMovementAxisEventHandler_t2208405138), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (VRTK_TouchpadMovement_t1979813355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2928[47] = 
{
	VRTK_TouchpadMovement_t1979813355::get_offset_of_AxisMovement_2(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_leftController_3(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_rightController_4(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_moveOnButtonPress_5(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_movementMultiplierButton_6(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_verticalAxisMovement_7(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_verticalDeadzone_8(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_verticalMultiplier_9(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_deviceForDirection_10(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_flipDirectionEnabled_11(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_flipDeadzone_12(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_flipDelay_13(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_flipBlink_14(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_horizontalAxisMovement_15(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_horizontalDeadzone_16(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_horizontalMultiplier_17(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_snapRotateDelay_18(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_snapRotateAngle_19(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_rotateMaxSpeed_20(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_blinkDurationMultiplier_21(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_slideMaxSpeed_22(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_slideDeceleration_23(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_warpDelay_24(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_warpRange_25(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_warpMaxAltitudeChange_26(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_controllerLeftHand_27(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_controllerRightHand_28(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_playArea_29(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_touchAxis_30(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_movementSpeed_31(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_strafeSpeed_32(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_blinkFadeInTime_33(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_lastWarp_34(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_lastFlip_35(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_lastSnapRotate_36(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_multiplyMovement_37(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_bodyCollider_38(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_headset_39(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_leftSubscribed_40(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_rightSubscribed_41(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_touchpadAxisChanged_42(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_touchpadUntouched_43(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_controllerEvents_44(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_bodyPhysics_45(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_wasFalling_46(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_previousLeftControllerState_47(),
	VRTK_TouchpadMovement_t1979813355::get_offset_of_previousRightControllerState_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (VerticalAxisMovement_t2238062081)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2929[5] = 
{
	VerticalAxisMovement_t2238062081::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (HorizontalAxisMovement_t3153229385)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2930[8] = 
{
	HorizontalAxisMovement_t3153229385::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (AxisMovementType_t2300405083)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2931[4] = 
{
	AxisMovementType_t2300405083::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (AxisMovementDirection_t1724819570)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2932[6] = 
{
	AxisMovementDirection_t1724819570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (VRTK_TouchpadWalking_t2507848959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2933[24] = 
{
	VRTK_TouchpadWalking_t2507848959::get_offset_of_leftController_2(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_rightController_3(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_maxWalkSpeed_4(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_deceleration_5(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_moveOnButtonPress_6(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_deviceForDirection_7(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_speedMultiplierButton_8(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_speedMultiplier_9(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_controllerLeftHand_10(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_controllerRightHand_11(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_playArea_12(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_touchAxis_13(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_movementSpeed_14(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_strafeSpeed_15(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_leftSubscribed_16(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_rightSubscribed_17(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_touchpadAxisChanged_18(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_touchpadUntouched_19(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_multiplySpeed_20(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_controllerEvents_21(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_bodyPhysics_22(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_wasFalling_23(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_previousLeftControllerState_24(),
	VRTK_TouchpadWalking_t2507848959::get_offset_of_previousRightControllerState_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (VRTK_BasePointerRenderer_t1270536273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2934[26] = 
{
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_playareaCursor_2(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_layersToIgnore_3(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_pointerOriginSmoothingSettings_4(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_validCollisionColor_5(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_invalidCollisionColor_6(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_tracerVisibility_7(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_cursorVisibility_8(),
	0,
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_controllingPointer_10(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_destinationHit_11(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_defaultMaterial_12(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_currentColor_13(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_invalidListPolicy_14(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_navMeshCheckDistance_15(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_headsetPositionCompensation_16(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_objectInteractor_17(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_objectInteractorAttachPoint_18(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_pointerOriginTransformFollowGameObject_19(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_pointerOriginTransformFollow_20(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_controllerGrabScript_21(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_savedAttachPoint_22(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_attachedToInteractorAttachPoint_23(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_savedBeamLength_24(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_makeRendererVisible_25(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_tracerVisible_26(),
	VRTK_BasePointerRenderer_t1270536273::get_offset_of_cursorVisible_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (VisibilityStates_t517960985)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2935[4] = 
{
	VisibilityStates_t517960985::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (PointerOriginSmoothingSettings_t2399923839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2936[4] = 
{
	PointerOriginSmoothingSettings_t2399923839::get_offset_of_smoothsPosition_0(),
	PointerOriginSmoothingSettings_t2399923839::get_offset_of_maxAllowedPerFrameDistanceDifference_1(),
	PointerOriginSmoothingSettings_t2399923839::get_offset_of_smoothsRotation_2(),
	PointerOriginSmoothingSettings_t2399923839::get_offset_of_maxAllowedPerFrameAngleDifference_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (VRTK_BezierPointerRenderer_t3371893943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2937[18] = 
{
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_maximumLength_28(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_tracerDensity_29(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_cursorRadius_30(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_heightLimitAngle_31(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_curveOffset_32(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_rescaleTracer_33(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_cursorMatchTargetRotation_34(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_collisionCheckFrequency_35(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_customTracer_36(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_customCursor_37(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_validLocationObject_38(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_invalidLocationObject_39(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_actualTracer_40(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_actualContainer_41(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_actualCursor_42(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_actualValidLocationObject_43(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_actualInvalidLocationObject_44(),
	VRTK_BezierPointerRenderer_t3371893943::get_offset_of_fixedForwardBeamForward_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (VRTK_StraightPointerRenderer_t3315603018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2938[11] = 
{
	VRTK_StraightPointerRenderer_t3315603018::get_offset_of_maximumLength_28(),
	VRTK_StraightPointerRenderer_t3315603018::get_offset_of_scaleFactor_29(),
	VRTK_StraightPointerRenderer_t3315603018::get_offset_of_cursorScaleMultiplier_30(),
	VRTK_StraightPointerRenderer_t3315603018::get_offset_of_cursorMatchTargetRotation_31(),
	VRTK_StraightPointerRenderer_t3315603018::get_offset_of_cursorDistanceRescale_32(),
	VRTK_StraightPointerRenderer_t3315603018::get_offset_of_customTracer_33(),
	VRTK_StraightPointerRenderer_t3315603018::get_offset_of_customCursor_34(),
	VRTK_StraightPointerRenderer_t3315603018::get_offset_of_actualContainer_35(),
	VRTK_StraightPointerRenderer_t3315603018::get_offset_of_actualTracer_36(),
	VRTK_StraightPointerRenderer_t3315603018::get_offset_of_actualCursor_37(),
	VRTK_StraightPointerRenderer_t3315603018::get_offset_of_cursorOriginalScale_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (VRTK_BasePointer_t3466805540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2939[32] = 
{
	VRTK_BasePointer_t3466805540::get_offset_of_controller_9(),
	VRTK_BasePointer_t3466805540::get_offset_of_pointerOriginTransform_10(),
	VRTK_BasePointer_t3466805540::get_offset_of_pointerOriginSmoothingSettings_11(),
	VRTK_BasePointer_t3466805540::get_offset_of_pointerMaterial_12(),
	VRTK_BasePointer_t3466805540::get_offset_of_pointerHitColor_13(),
	VRTK_BasePointer_t3466805540::get_offset_of_pointerMissColor_14(),
	VRTK_BasePointer_t3466805540::get_offset_of_holdButtonToActivate_15(),
	VRTK_BasePointer_t3466805540::get_offset_of_interactWithObjects_16(),
	VRTK_BasePointer_t3466805540::get_offset_of_grabToPointerTip_17(),
	VRTK_BasePointer_t3466805540::get_offset_of_activateDelay_18(),
	VRTK_BasePointer_t3466805540::get_offset_of_pointerVisibility_19(),
	VRTK_BasePointer_t3466805540::get_offset_of_layersToIgnore_20(),
	VRTK_BasePointer_t3466805540::get_offset_of_destinationPosition_21(),
	VRTK_BasePointer_t3466805540::get_offset_of_pointerContactDistance_22(),
	VRTK_BasePointer_t3466805540::get_offset_of_pointerContactTarget_23(),
	VRTK_BasePointer_t3466805540::get_offset_of_pointerContactRaycastHit_24(),
	VRTK_BasePointer_t3466805540::get_offset_of_controllerIndex_25(),
	VRTK_BasePointer_t3466805540::get_offset_of_playAreaCursor_26(),
	VRTK_BasePointer_t3466805540::get_offset_of_currentPointerColor_27(),
	VRTK_BasePointer_t3466805540::get_offset_of_objectInteractor_28(),
	VRTK_BasePointer_t3466805540::get_offset_of_objectInteractorAttachPoint_29(),
	VRTK_BasePointer_t3466805540::get_offset_of_isActive_30(),
	VRTK_BasePointer_t3466805540::get_offset_of_destinationSetActive_31(),
	VRTK_BasePointer_t3466805540::get_offset_of_activateDelayTimer_32(),
	VRTK_BasePointer_t3466805540::get_offset_of_beamEnabledState_33(),
	VRTK_BasePointer_t3466805540::get_offset_of_interactableObject_34(),
	VRTK_BasePointer_t3466805540::get_offset_of_savedAttachPoint_35(),
	VRTK_BasePointer_t3466805540::get_offset_of_attachedToInteractorAttachPoint_36(),
	VRTK_BasePointer_t3466805540::get_offset_of_savedBeamLength_37(),
	VRTK_BasePointer_t3466805540::get_offset_of_controllerGrabScript_38(),
	VRTK_BasePointer_t3466805540::get_offset_of_pointerOriginTransformFollowGameObject_39(),
	VRTK_BasePointer_t3466805540::get_offset_of_pointerOriginTransformFollow_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (pointerVisibilityStates_t986354463)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2940[4] = 
{
	pointerVisibilityStates_t986354463::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (PointerOriginSmoothingSettings_t2805273516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2941[4] = 
{
	PointerOriginSmoothingSettings_t2805273516::get_offset_of_smoothsPosition_0(),
	PointerOriginSmoothingSettings_t2805273516::get_offset_of_maxAllowedPerFrameDistanceDifference_1(),
	PointerOriginSmoothingSettings_t2805273516::get_offset_of_smoothsRotation_2(),
	PointerOriginSmoothingSettings_t2805273516::get_offset_of_maxAllowedPerFrameAngleDifference_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (VRTK_BezierPointer_t2668864506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2942[20] = 
{
	VRTK_BezierPointer_t2668864506::get_offset_of_pointerLength_41(),
	VRTK_BezierPointer_t2668864506::get_offset_of_pointerDensity_42(),
	VRTK_BezierPointer_t2668864506::get_offset_of_collisionCheckFrequency_43(),
	VRTK_BezierPointer_t2668864506::get_offset_of_beamCurveOffset_44(),
	VRTK_BezierPointer_t2668864506::get_offset_of_beamHeightLimitAngle_45(),
	VRTK_BezierPointer_t2668864506::get_offset_of_rescalePointerTracer_46(),
	VRTK_BezierPointer_t2668864506::get_offset_of_showPointerCursor_47(),
	VRTK_BezierPointer_t2668864506::get_offset_of_pointerCursorRadius_48(),
	VRTK_BezierPointer_t2668864506::get_offset_of_pointerCursorMatchTargetRotation_49(),
	VRTK_BezierPointer_t2668864506::get_offset_of_customPointerTracer_50(),
	VRTK_BezierPointer_t2668864506::get_offset_of_customPointerCursor_51(),
	VRTK_BezierPointer_t2668864506::get_offset_of_validTeleportLocationObject_52(),
	VRTK_BezierPointer_t2668864506::get_offset_of_pointerCursor_53(),
	VRTK_BezierPointer_t2668864506::get_offset_of_curvedBeamContainer_54(),
	VRTK_BezierPointer_t2668864506::get_offset_of_curvedBeam_55(),
	VRTK_BezierPointer_t2668864506::get_offset_of_validTeleportLocationInstance_56(),
	VRTK_BezierPointer_t2668864506::get_offset_of_beamActive_57(),
	VRTK_BezierPointer_t2668864506::get_offset_of_fixedForwardBeamForward_58(),
	VRTK_BezierPointer_t2668864506::get_offset_of_contactNormal_59(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (DestinationMarkerEventArgs_t1852451661)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2943[7] = 
{
	DestinationMarkerEventArgs_t1852451661::get_offset_of_distance_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DestinationMarkerEventArgs_t1852451661::get_offset_of_target_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DestinationMarkerEventArgs_t1852451661::get_offset_of_raycastHit_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DestinationMarkerEventArgs_t1852451661::get_offset_of_destinationPosition_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DestinationMarkerEventArgs_t1852451661::get_offset_of_forceDestinationPosition_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DestinationMarkerEventArgs_t1852451661::get_offset_of_enableTeleport_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DestinationMarkerEventArgs_t1852451661::get_offset_of_controllerIndex_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (DestinationMarkerEventHandler_t1148830384), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (VRTK_DestinationMarker_t667613644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2945[7] = 
{
	VRTK_DestinationMarker_t667613644::get_offset_of_enableTeleport_2(),
	VRTK_DestinationMarker_t667613644::get_offset_of_DestinationMarkerEnter_3(),
	VRTK_DestinationMarker_t667613644::get_offset_of_DestinationMarkerExit_4(),
	VRTK_DestinationMarker_t667613644::get_offset_of_DestinationMarkerSet_5(),
	VRTK_DestinationMarker_t667613644::get_offset_of_invalidListPolicy_6(),
	VRTK_DestinationMarker_t667613644::get_offset_of_navMeshCheckDistance_7(),
	VRTK_DestinationMarker_t667613644::get_offset_of_headsetPositionCompensation_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (VRTK_PlayAreaCursor_t3566057915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2946[13] = 
{
	VRTK_PlayAreaCursor_t3566057915::get_offset_of_playAreaCursorDimensions_2(),
	VRTK_PlayAreaCursor_t3566057915::get_offset_of_handlePlayAreaCursorCollisions_3(),
	VRTK_PlayAreaCursor_t3566057915::get_offset_of_headsetOutOfBoundsIsCollision_4(),
	VRTK_PlayAreaCursor_t3566057915::get_offset_of_targetListPolicy_5(),
	VRTK_PlayAreaCursor_t3566057915::get_offset_of_headsetPositionCompensation_6(),
	VRTK_PlayAreaCursor_t3566057915::get_offset_of_playAreaCursorCollided_7(),
	VRTK_PlayAreaCursor_t3566057915::get_offset_of_headsetOutOfBounds_8(),
	VRTK_PlayAreaCursor_t3566057915::get_offset_of_playArea_9(),
	VRTK_PlayAreaCursor_t3566057915::get_offset_of_playAreaCursor_10(),
	VRTK_PlayAreaCursor_t3566057915::get_offset_of_playAreaCursorBoundaries_11(),
	VRTK_PlayAreaCursor_t3566057915::get_offset_of_playAreaCursorCollider_12(),
	VRTK_PlayAreaCursor_t3566057915::get_offset_of_headset_13(),
	VRTK_PlayAreaCursor_t3566057915::get_offset_of_boundaryRenderers_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (VRTK_PlayAreaCollider_t1466370881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2947[2] = 
{
	VRTK_PlayAreaCollider_t1466370881::get_offset_of_parent_2(),
	VRTK_PlayAreaCollider_t1466370881::get_offset_of_targetListPolicy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (VRTK_Pointer_t2647108841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2948[19] = 
{
	VRTK_Pointer_t2647108841::get_offset_of_pointerRenderer_9(),
	VRTK_Pointer_t2647108841::get_offset_of_activationButton_10(),
	VRTK_Pointer_t2647108841::get_offset_of_holdButtonToActivate_11(),
	VRTK_Pointer_t2647108841::get_offset_of_activationDelay_12(),
	VRTK_Pointer_t2647108841::get_offset_of_selectionButton_13(),
	VRTK_Pointer_t2647108841::get_offset_of_selectOnPress_14(),
	VRTK_Pointer_t2647108841::get_offset_of_interactWithObjects_15(),
	VRTK_Pointer_t2647108841::get_offset_of_grabToPointerTip_16(),
	VRTK_Pointer_t2647108841::get_offset_of_controller_17(),
	VRTK_Pointer_t2647108841::get_offset_of_customOrigin_18(),
	VRTK_Pointer_t2647108841::get_offset_of_subscribedActivationButton_19(),
	VRTK_Pointer_t2647108841::get_offset_of_subscribedSelectionButton_20(),
	VRTK_Pointer_t2647108841::get_offset_of_currentSelectOnPress_21(),
	VRTK_Pointer_t2647108841::get_offset_of_activateDelayTimer_22(),
	VRTK_Pointer_t2647108841::get_offset_of_currentActivationState_23(),
	VRTK_Pointer_t2647108841::get_offset_of_willDeactivate_24(),
	VRTK_Pointer_t2647108841::get_offset_of_wasActivated_25(),
	VRTK_Pointer_t2647108841::get_offset_of_controllerIndex_26(),
	VRTK_Pointer_t2647108841::get_offset_of_pointerInteractableObject_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (VRTK_SimplePointer_t1980002747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2949[14] = 
{
	VRTK_SimplePointer_t1980002747::get_offset_of_pointerThickness_41(),
	VRTK_SimplePointer_t1980002747::get_offset_of_pointerLength_42(),
	VRTK_SimplePointer_t1980002747::get_offset_of_showPointerTip_43(),
	VRTK_SimplePointer_t1980002747::get_offset_of_customPointerCursor_44(),
	VRTK_SimplePointer_t1980002747::get_offset_of_pointerCursorMatchTargetNormal_45(),
	VRTK_SimplePointer_t1980002747::get_offset_of_pointerCursorRescaledAlongDistance_46(),
	VRTK_SimplePointer_t1980002747::get_offset_of_pointerHolder_47(),
	VRTK_SimplePointer_t1980002747::get_offset_of_pointerBeam_48(),
	VRTK_SimplePointer_t1980002747::get_offset_of_pointerTip_49(),
	VRTK_SimplePointer_t1980002747::get_offset_of_pointerTipScale_50(),
	VRTK_SimplePointer_t1980002747::get_offset_of_pointerCursorOriginalScale_51(),
	VRTK_SimplePointer_t1980002747::get_offset_of_activeEnabled_52(),
	VRTK_SimplePointer_t1980002747::get_offset_of_storedBeamState_53(),
	VRTK_SimplePointer_t1980002747::get_offset_of_storedTipState_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (BodyPhysicsEventArgs_t2230131654)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2950[1] = 
{
	BodyPhysicsEventArgs_t2230131654::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (BodyPhysicsEventHandler_t3963963105), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (VRTK_BodyPhysics_t3414085265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2952[45] = 
{
	VRTK_BodyPhysics_t3414085265::get_offset_of_enableBodyCollisions_9(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_ignoreGrabbedCollisions_10(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_headsetYOffset_11(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_movementThreshold_12(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_standingHistorySamples_13(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_leanYThreshold_14(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_layersToIgnore_15(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_fallRestriction_16(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_gravityFallYThreshold_17(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_blinkYThreshold_18(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_floorHeightTolerance_19(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_StartFalling_20(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_StopFalling_21(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_StartMoving_22(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_StopMoving_23(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_StartColliding_24(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_StopColliding_25(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_playArea_26(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_headset_27(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_bodyRigidbody_28(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_bodyCollider_29(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_currentBodyCollisionsSetting_30(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_currentCollidingObject_31(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_currentValidFloorObject_32(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_teleporter_33(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_lastFrameFloorY_34(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_hitFloorYDelta_35(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_initialFloorDrop_36(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_resetPhysicsAfterTeleport_37(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_storedCurrentPhysics_38(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_retogglePhysicsOnCanFall_39(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_storedRetogglePhysics_40(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_lastPlayAreaPosition_41(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_currentStandingPosition_42(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_standingPositionHistory_43(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_playAreaHeightAdjustment_44(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_isFalling_45(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_isMoving_46(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_isLeaning_47(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_onGround_48(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_preventSnapToFloor_49(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_generateCollider_50(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_generateRigidbody_51(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_playAreaVelocity_52(),
	VRTK_BodyPhysics_t3414085265::get_offset_of_drawDebugGizmo_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (FallingRestrictors_t3718949972)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2953[6] = 
{
	FallingRestrictors_t3718949972::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (U3CRestoreCollisionsU3Ec__Iterator0_t1574376249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2954[5] = 
{
	U3CRestoreCollisionsU3Ec__Iterator0_t1574376249::get_offset_of_obj_0(),
	U3CRestoreCollisionsU3Ec__Iterator0_t1574376249::get_offset_of_U24this_1(),
	U3CRestoreCollisionsU3Ec__Iterator0_t1574376249::get_offset_of_U24current_2(),
	U3CRestoreCollisionsU3Ec__Iterator0_t1574376249::get_offset_of_U24disposing_3(),
	U3CRestoreCollisionsU3Ec__Iterator0_t1574376249::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (HeadsetCollisionEventArgs_t1242373387)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2955[2] = 
{
	HeadsetCollisionEventArgs_t1242373387::get_offset_of_collider_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HeadsetCollisionEventArgs_t1242373387::get_offset_of_currentTransform_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (HeadsetCollisionEventHandler_t3119610762), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (VRTK_HeadsetCollision_t2015187094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2957[11] = 
{
	VRTK_HeadsetCollision_t2015187094::get_offset_of_colliderRadius_2(),
	VRTK_HeadsetCollision_t2015187094::get_offset_of_targetListPolicy_3(),
	VRTK_HeadsetCollision_t2015187094::get_offset_of_HeadsetCollisionDetect_4(),
	VRTK_HeadsetCollision_t2015187094::get_offset_of_HeadsetCollisionEnded_5(),
	VRTK_HeadsetCollision_t2015187094::get_offset_of_headsetColliding_6(),
	VRTK_HeadsetCollision_t2015187094::get_offset_of_collidingWith_7(),
	VRTK_HeadsetCollision_t2015187094::get_offset_of_headset_8(),
	VRTK_HeadsetCollision_t2015187094::get_offset_of_headsetColliderScript_9(),
	VRTK_HeadsetCollision_t2015187094::get_offset_of_headsetColliderContainer_10(),
	VRTK_HeadsetCollision_t2015187094::get_offset_of_generateCollider_11(),
	VRTK_HeadsetCollision_t2015187094::get_offset_of_generateRigidbody_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (VRTK_HeadsetCollider_t4272516266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2958[2] = 
{
	VRTK_HeadsetCollider_t4272516266::get_offset_of_parent_2(),
	VRTK_HeadsetCollider_t4272516266::get_offset_of_targetListPolicy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (VRTK_HeadsetCollisionFade_t3608385924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2959[4] = 
{
	VRTK_HeadsetCollisionFade_t3608385924::get_offset_of_blinkTransitionSpeed_2(),
	VRTK_HeadsetCollisionFade_t3608385924::get_offset_of_fadeColor_3(),
	VRTK_HeadsetCollisionFade_t3608385924::get_offset_of_headsetCollision_4(),
	VRTK_HeadsetCollisionFade_t3608385924::get_offset_of_headsetFade_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (HeadsetControllerAwareEventArgs_t2653531721)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2960[2] = 
{
	HeadsetControllerAwareEventArgs_t2653531721::get_offset_of_raycastHit_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HeadsetControllerAwareEventArgs_t2653531721::get_offset_of_controllerIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (HeadsetControllerAwareEventHandler_t1106036588), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (VRTK_HeadsetControllerAware_t1678000416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2962[20] = 
{
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_trackLeftController_2(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_trackRightController_3(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_controllerGlanceRadius_4(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_customRightControllerOrigin_5(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_customLeftControllerOrigin_6(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_ControllerObscured_7(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_ControllerUnobscured_8(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_ControllerGlanceEnter_9(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_ControllerGlanceExit_10(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_leftController_11(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_rightController_12(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_headset_13(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_leftControllerObscured_14(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_rightControllerObscured_15(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_leftControllerLastState_16(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_rightControllerLastState_17(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_leftControllerGlance_18(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_rightControllerGlance_19(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_leftControllerGlanceLastState_20(),
	VRTK_HeadsetControllerAware_t1678000416::get_offset_of_rightControllerGlanceLastState_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (HeadsetFadeEventArgs_t2892542019)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2963[2] = 
{
	HeadsetFadeEventArgs_t2892542019::get_offset_of_timeTillComplete_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HeadsetFadeEventArgs_t2892542019::get_offset_of_currentTransform_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (HeadsetFadeEventHandler_t2012619754), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (VRTK_HeadsetFade_t3539061086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2965[7] = 
{
	VRTK_HeadsetFade_t3539061086::get_offset_of_HeadsetFadeStart_2(),
	VRTK_HeadsetFade_t3539061086::get_offset_of_HeadsetFadeComplete_3(),
	VRTK_HeadsetFade_t3539061086::get_offset_of_HeadsetUnfadeStart_4(),
	VRTK_HeadsetFade_t3539061086::get_offset_of_HeadsetUnfadeComplete_5(),
	VRTK_HeadsetFade_t3539061086::get_offset_of_headset_6(),
	VRTK_HeadsetFade_t3539061086::get_offset_of_isTransitioning_7(),
	VRTK_HeadsetFade_t3539061086::get_offset_of_isFaded_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (VRTK_HipTracking_t3141756816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2966[4] = 
{
	VRTK_HipTracking_t3141756816::get_offset_of_HeadOffset_2(),
	VRTK_HipTracking_t3141756816::get_offset_of_headOverride_3(),
	VRTK_HipTracking_t3141756816::get_offset_of_ReferenceUp_4(),
	VRTK_HipTracking_t3141756816::get_offset_of_playerHead_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (VRTK_PositionRewind_t3080560492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2967[15] = 
{
	VRTK_PositionRewind_t3080560492::get_offset_of_rewindDelay_2(),
	VRTK_PositionRewind_t3080560492::get_offset_of_pushbackDistance_3(),
	VRTK_PositionRewind_t3080560492::get_offset_of_crouchThreshold_4(),
	VRTK_PositionRewind_t3080560492::get_offset_of_headset_5(),
	VRTK_PositionRewind_t3080560492::get_offset_of_playArea_6(),
	VRTK_PositionRewind_t3080560492::get_offset_of_playareaRigidbody_7(),
	VRTK_PositionRewind_t3080560492::get_offset_of_headsetCollision_8(),
	VRTK_PositionRewind_t3080560492::get_offset_of_lastGoodStandingPosition_9(),
	VRTK_PositionRewind_t3080560492::get_offset_of_lastGoodHeadsetPosition_10(),
	VRTK_PositionRewind_t3080560492::get_offset_of_highestHeadsetY_11(),
	VRTK_PositionRewind_t3080560492::get_offset_of_lastPlayAreaY_12(),
	VRTK_PositionRewind_t3080560492::get_offset_of_lastGoodPositionSet_13(),
	VRTK_PositionRewind_t3080560492::get_offset_of_hasCollided_14(),
	VRTK_PositionRewind_t3080560492::get_offset_of_isColliding_15(),
	VRTK_PositionRewind_t3080560492::get_offset_of_collideTimer_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (VRTK_UICanvas_t1283311654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2968[6] = 
{
	VRTK_UICanvas_t1283311654::get_offset_of_clickOnPointerCollision_2(),
	VRTK_UICanvas_t1283311654::get_offset_of_autoActivateWithinDistance_3(),
	VRTK_UICanvas_t1283311654::get_offset_of_canvasBoxCollider_4(),
	VRTK_UICanvas_t1283311654::get_offset_of_canvasRigidBody_5(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (VRTK_UIPointerAutoActivator_t1282719345), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (VRTK_UIDraggableItem_t2269178406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2970[11] = 
{
	VRTK_UIDraggableItem_t2269178406::get_offset_of_restrictToDropZone_2(),
	VRTK_UIDraggableItem_t2269178406::get_offset_of_restrictToOriginalCanvas_3(),
	VRTK_UIDraggableItem_t2269178406::get_offset_of_forwardOffset_4(),
	VRTK_UIDraggableItem_t2269178406::get_offset_of_validDropZone_5(),
	VRTK_UIDraggableItem_t2269178406::get_offset_of_dragTransform_6(),
	VRTK_UIDraggableItem_t2269178406::get_offset_of_startPosition_7(),
	VRTK_UIDraggableItem_t2269178406::get_offset_of_startRotation_8(),
	VRTK_UIDraggableItem_t2269178406::get_offset_of_startDropZone_9(),
	VRTK_UIDraggableItem_t2269178406::get_offset_of_startParent_10(),
	VRTK_UIDraggableItem_t2269178406::get_offset_of_startCanvas_11(),
	VRTK_UIDraggableItem_t2269178406::get_offset_of_canvasGroup_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (VRTK_UIDropZone_t1661569883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2971[1] = 
{
	VRTK_UIDropZone_t1661569883::get_offset_of_droppableItem_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (UIPointerEventArgs_t1171985978)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2972[4] = 
{
	UIPointerEventArgs_t1171985978::get_offset_of_controllerIndex_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIPointerEventArgs_t1171985978::get_offset_of_isActive_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIPointerEventArgs_t1171985978::get_offset_of_currentTarget_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIPointerEventArgs_t1171985978::get_offset_of_previousTarget_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (UIPointerEventHandler_t988663103), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (VRTK_UIPointer_t2714926455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2974[27] = 
{
	VRTK_UIPointer_t2714926455::get_offset_of_activationButton_2(),
	VRTK_UIPointer_t2714926455::get_offset_of_activationMode_3(),
	VRTK_UIPointer_t2714926455::get_offset_of_selectionButton_4(),
	VRTK_UIPointer_t2714926455::get_offset_of_clickMethod_5(),
	VRTK_UIPointer_t2714926455::get_offset_of_attemptClickOnDeactivate_6(),
	VRTK_UIPointer_t2714926455::get_offset_of_clickAfterHoverDuration_7(),
	VRTK_UIPointer_t2714926455::get_offset_of_controller_8(),
	VRTK_UIPointer_t2714926455::get_offset_of_pointerOriginTransform_9(),
	VRTK_UIPointer_t2714926455::get_offset_of_pointerEventData_10(),
	VRTK_UIPointer_t2714926455::get_offset_of_hoveringElement_11(),
	VRTK_UIPointer_t2714926455::get_offset_of_controllerRenderModel_12(),
	VRTK_UIPointer_t2714926455::get_offset_of_hoverDurationTimer_13(),
	VRTK_UIPointer_t2714926455::get_offset_of_canClickOnHover_14(),
	VRTK_UIPointer_t2714926455::get_offset_of_autoActivatingCanvas_15(),
	VRTK_UIPointer_t2714926455::get_offset_of_collisionClick_16(),
	VRTK_UIPointer_t2714926455::get_offset_of_UIPointerElementEnter_17(),
	VRTK_UIPointer_t2714926455::get_offset_of_UIPointerElementExit_18(),
	VRTK_UIPointer_t2714926455::get_offset_of_UIPointerElementClick_19(),
	VRTK_UIPointer_t2714926455::get_offset_of_UIPointerElementDragStart_20(),
	VRTK_UIPointer_t2714926455::get_offset_of_UIPointerElementDragEnd_21(),
	VRTK_UIPointer_t2714926455::get_offset_of_pointerClicked_22(),
	VRTK_UIPointer_t2714926455::get_offset_of_beamEnabledState_23(),
	VRTK_UIPointer_t2714926455::get_offset_of_lastPointerPressState_24(),
	VRTK_UIPointer_t2714926455::get_offset_of_lastPointerClickState_25(),
	VRTK_UIPointer_t2714926455::get_offset_of_currentTarget_26(),
	VRTK_UIPointer_t2714926455::get_offset_of_cachedEventSystem_27(),
	VRTK_UIPointer_t2714926455::get_offset_of_cachedVRInputModule_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (ActivationMethods_t3611442703)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2975[4] = 
{
	ActivationMethods_t3611442703::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (ClickMethods_t1099240745)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2976[3] = 
{
	ClickMethods_t1099240745::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { sizeof (U3CWaitForPointerIdU3Ec__Iterator0_t2911062424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2977[5] = 
{
	U3CWaitForPointerIdU3Ec__Iterator0_t2911062424::get_offset_of_U3CindexU3E__0_0(),
	U3CWaitForPointerIdU3Ec__Iterator0_t2911062424::get_offset_of_U24this_1(),
	U3CWaitForPointerIdU3Ec__Iterator0_t2911062424::get_offset_of_U24current_2(),
	U3CWaitForPointerIdU3Ec__Iterator0_t2911062424::get_offset_of_U24disposing_3(),
	U3CWaitForPointerIdU3Ec__Iterator0_t2911062424::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (VRTK_ObjectFollow_t3175963762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2978[14] = 
{
	VRTK_ObjectFollow_t3175963762::get_offset_of_gameObjectToFollow_2(),
	VRTK_ObjectFollow_t3175963762::get_offset_of_gameObjectToChange_3(),
	VRTK_ObjectFollow_t3175963762::get_offset_of_followsPosition_4(),
	VRTK_ObjectFollow_t3175963762::get_offset_of_smoothsPosition_5(),
	VRTK_ObjectFollow_t3175963762::get_offset_of_maxAllowedPerFrameDistanceDifference_6(),
	VRTK_ObjectFollow_t3175963762::get_offset_of_U3CtargetPositionU3Ek__BackingField_7(),
	VRTK_ObjectFollow_t3175963762::get_offset_of_followsRotation_8(),
	VRTK_ObjectFollow_t3175963762::get_offset_of_smoothsRotation_9(),
	VRTK_ObjectFollow_t3175963762::get_offset_of_maxAllowedPerFrameAngleDifference_10(),
	VRTK_ObjectFollow_t3175963762::get_offset_of_U3CtargetRotationU3Ek__BackingField_11(),
	VRTK_ObjectFollow_t3175963762::get_offset_of_followsScale_12(),
	VRTK_ObjectFollow_t3175963762::get_offset_of_smoothsScale_13(),
	VRTK_ObjectFollow_t3175963762::get_offset_of_maxAllowedPerFrameSizeDifference_14(),
	VRTK_ObjectFollow_t3175963762::get_offset_of_U3CtargetScaleU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (VRTK_RigidbodyFollow_t4264717774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2979[3] = 
{
	VRTK_RigidbodyFollow_t4264717774::get_offset_of_movementOption_16(),
	VRTK_RigidbodyFollow_t4264717774::get_offset_of_rigidbodyToFollow_17(),
	VRTK_RigidbodyFollow_t4264717774::get_offset_of_rigidbodyToChange_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (MovementOption_t4148486814)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2980[4] = 
{
	MovementOption_t4148486814::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (VRTK_TransformFollow_t3532748285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2981[4] = 
{
	VRTK_TransformFollow_t3532748285::get_offset_of_moment_16(),
	VRTK_TransformFollow_t3532748285::get_offset_of_transformToFollow_17(),
	VRTK_TransformFollow_t3532748285::get_offset_of_transformToChange_18(),
	VRTK_TransformFollow_t3532748285::get_offset_of_isListeningToOnPreRender_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (FollowMoment_t1516682274)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2982[4] = 
{
	FollowMoment_t1516682274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (VRTK_BasicTeleport_UnityEvents_t776645422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2983[3] = 
{
	VRTK_BasicTeleport_UnityEvents_t776645422::get_offset_of_bt_2(),
	VRTK_BasicTeleport_UnityEvents_t776645422::get_offset_of_OnTeleporting_3(),
	VRTK_BasicTeleport_UnityEvents_t776645422::get_offset_of_OnTeleported_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (UnityObjectEvent_t4156405881), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (VRTK_BodyPhysics_UnityEvents_t2330929566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2985[7] = 
{
	VRTK_BodyPhysics_UnityEvents_t2330929566::get_offset_of_bp_2(),
	VRTK_BodyPhysics_UnityEvents_t2330929566::get_offset_of_OnStartFalling_3(),
	VRTK_BodyPhysics_UnityEvents_t2330929566::get_offset_of_OnStopFalling_4(),
	VRTK_BodyPhysics_UnityEvents_t2330929566::get_offset_of_OnStartMoving_5(),
	VRTK_BodyPhysics_UnityEvents_t2330929566::get_offset_of_OnStopMoving_6(),
	VRTK_BodyPhysics_UnityEvents_t2330929566::get_offset_of_OnStartColliding_7(),
	VRTK_BodyPhysics_UnityEvents_t2330929566::get_offset_of_OnStopColliding_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (UnityObjectEvent_t1260615097), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (VRTK_Button_UnityEvents_t4285437623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2987[2] = 
{
	VRTK_Button_UnityEvents_t4285437623::get_offset_of_b3d_2(),
	VRTK_Button_UnityEvents_t4285437623::get_offset_of_OnPushed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (UnityObjectEvent_t2046806484), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (VRTK_Control_UnityEvents_t692650506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2989[2] = 
{
	VRTK_Control_UnityEvents_t692650506::get_offset_of_c3d_2(),
	VRTK_Control_UnityEvents_t692650506::get_offset_of_OnValueChanged_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (UnityObjectEvent_t3966949053), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (VRTK_ControllerActions_UnityEvents_t3920462976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2991[3] = 
{
	VRTK_ControllerActions_UnityEvents_t3920462976::get_offset_of_ca_2(),
	VRTK_ControllerActions_UnityEvents_t3920462976::get_offset_of_OnControllerModelVisible_3(),
	VRTK_ControllerActions_UnityEvents_t3920462976::get_offset_of_OnControllerModelInvisible_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (UnityObjectEvent_t3606750807), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { sizeof (VRTK_ControllerEvents_UnityEvents_t1877972230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2993[48] = 
{
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_ce_2(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnTriggerPressed_3(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnTriggerReleased_4(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnTriggerTouchStart_5(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnTriggerTouchEnd_6(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnTriggerHairlineStart_7(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnTriggerHairlineEnd_8(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnTriggerClicked_9(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnTriggerUnclicked_10(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnTriggerAxisChanged_11(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnGripPressed_12(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnGripReleased_13(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnGripTouchStart_14(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnGripTouchEnd_15(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnGripHairlineStart_16(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnGripHairlineEnd_17(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnGripClicked_18(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnGripUnclicked_19(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnGripAxisChanged_20(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnTouchpadPressed_21(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnTouchpadReleased_22(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnTouchpadTouchStart_23(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnTouchpadTouchEnd_24(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnTouchpadAxisChanged_25(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnButtonOnePressed_26(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnButtonOneReleased_27(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnButtonOneTouchStart_28(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnButtonOneTouchEnd_29(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnButtonTwoPressed_30(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnButtonTwoReleased_31(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnButtonTwoTouchStart_32(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnButtonTwoTouchEnd_33(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnStartMenuPressed_34(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnStartMenuReleased_35(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnAliasPointerOn_36(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnAliasPointerOff_37(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnAliasPointerSet_38(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnAliasGrabOn_39(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnAliasGrabOff_40(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnAliasUseOn_41(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnAliasUseOff_42(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnAliasUIClickOn_43(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnAliasUIClickOff_44(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnAliasMenuOn_45(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnAliasMenuOff_46(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnControllerEnabled_47(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnControllerDisabled_48(),
	VRTK_ControllerEvents_UnityEvents_t1877972230::get_offset_of_OnControllerIndexChanged_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (UnityObjectEvent_t2200943093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (VRTK_DashTeleport_UnityEvents_t1858955576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2995[3] = 
{
	VRTK_DashTeleport_UnityEvents_t1858955576::get_offset_of_dt_2(),
	VRTK_DashTeleport_UnityEvents_t1858955576::get_offset_of_OnWillDashThruObjects_3(),
	VRTK_DashTeleport_UnityEvents_t1858955576::get_offset_of_OnDashedThruObjects_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (UnityObjectEvent_t1688415125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (VRTK_DestinationMarker_UnityEvents_t2475004431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2997[4] = 
{
	VRTK_DestinationMarker_UnityEvents_t2475004431::get_offset_of_dm_2(),
	VRTK_DestinationMarker_UnityEvents_t2475004431::get_offset_of_OnDestinationMarkerEnter_3(),
	VRTK_DestinationMarker_UnityEvents_t2475004431::get_offset_of_OnDestinationMarkerExit_4(),
	VRTK_DestinationMarker_UnityEvents_t2475004431::get_offset_of_OnDestinationMarkerSet_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (UnityObjectEvent_t599160530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (VRTK_HeadsetCollision_UnityEvents_t2856701609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2999[3] = 
{
	VRTK_HeadsetCollision_UnityEvents_t2856701609::get_offset_of_hc_2(),
	VRTK_HeadsetCollision_UnityEvents_t2856701609::get_offset_of_OnHeadsetCollisionDetect_3(),
	VRTK_HeadsetCollision_UnityEvents_t2856701609::get_offset_of_OnHeadsetCollisionEnded_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
