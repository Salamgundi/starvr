﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.GrabAttachMechanics.VRTK_SpringJointGrabAttach
struct VRTK_SpringJointGrabAttach_t1884620216;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.GrabAttachMechanics.VRTK_SpringJointGrabAttach::.ctor()
extern "C"  void VRTK_SpringJointGrabAttach__ctor_m670742698 (VRTK_SpringJointGrabAttach_t1884620216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_SpringJointGrabAttach::CreateJoint(UnityEngine.GameObject)
extern "C"  void VRTK_SpringJointGrabAttach_CreateJoint_m836789336 (VRTK_SpringJointGrabAttach_t1884620216 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
