﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach
struct VRTK_TrackObjectGrabAttach_t1133162717;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach::.ctor()
extern "C"  void VRTK_TrackObjectGrabAttach__ctor_m3024572383 (VRTK_TrackObjectGrabAttach_t1133162717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach::StopGrab(System.Boolean)
extern "C"  void VRTK_TrackObjectGrabAttach_StopGrab_m3767472304 (VRTK_TrackObjectGrabAttach_t1133162717 * __this, bool ___applyGrabbingObjectVelocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach::CreateTrackPoint(UnityEngine.Transform,UnityEngine.GameObject,UnityEngine.GameObject,System.Boolean&)
extern "C"  Transform_t3275118058 * VRTK_TrackObjectGrabAttach_CreateTrackPoint_m1747724486 (VRTK_TrackObjectGrabAttach_t1133162717 * __this, Transform_t3275118058 * ___controllerPoint0, GameObject_t1756533147 * ___currentGrabbedObject1, GameObject_t1756533147 * ___currentGrabbingObject2, bool* ___customTrackPoint3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach::ProcessUpdate()
extern "C"  void VRTK_TrackObjectGrabAttach_ProcessUpdate_m3024910169 (VRTK_TrackObjectGrabAttach_t1133162717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach::ProcessFixedUpdate()
extern "C"  void VRTK_TrackObjectGrabAttach_ProcessFixedUpdate_m1074983035 (VRTK_TrackObjectGrabAttach_t1133162717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach::Initialise()
extern "C"  void VRTK_TrackObjectGrabAttach_Initialise_m1871374680 (VRTK_TrackObjectGrabAttach_t1133162717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach::SetTrackPointOrientation(UnityEngine.Transform&,UnityEngine.Transform,UnityEngine.Transform)
extern "C"  void VRTK_TrackObjectGrabAttach_SetTrackPointOrientation_m91753053 (VRTK_TrackObjectGrabAttach_t1133162717 * __this, Transform_t3275118058 ** ___trackPoint0, Transform_t3275118058 * ___currentGrabbedObject1, Transform_t3275118058 * ___controllerPoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
