﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRExtendedDisplay/_GetWindowBounds
struct _GetWindowBounds_t2171929041;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRExtendedDisplay/_GetWindowBounds::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetWindowBounds__ctor_m2347673800 (_GetWindowBounds_t2171929041 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRExtendedDisplay/_GetWindowBounds::Invoke(System.Int32&,System.Int32&,System.UInt32&,System.UInt32&)
extern "C"  void _GetWindowBounds_Invoke_m563877460 (_GetWindowBounds_t2171929041 * __this, int32_t* ___pnX0, int32_t* ___pnY1, uint32_t* ___pnWidth2, uint32_t* ___pnHeight3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRExtendedDisplay/_GetWindowBounds::BeginInvoke(System.Int32&,System.Int32&,System.UInt32&,System.UInt32&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetWindowBounds_BeginInvoke_m3535285031 (_GetWindowBounds_t2171929041 * __this, int32_t* ___pnX0, int32_t* ___pnY1, uint32_t* ___pnWidth2, uint32_t* ___pnHeight3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRExtendedDisplay/_GetWindowBounds::EndInvoke(System.Int32&,System.Int32&,System.UInt32&,System.UInt32&,System.IAsyncResult)
extern "C"  void _GetWindowBounds_EndInvoke_m3163324262 (_GetWindowBounds_t2171929041 * __this, int32_t* ___pnX0, int32_t* ___pnY1, uint32_t* ___pnWidth2, uint32_t* ___pnHeight3, Il2CppObject * ___result4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
