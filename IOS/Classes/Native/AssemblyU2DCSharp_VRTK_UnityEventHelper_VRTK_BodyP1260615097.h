﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen912818263.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents/UnityObjectEvent
struct  UnityObjectEvent_t1260615097  : public UnityEvent_2_t912818263
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
