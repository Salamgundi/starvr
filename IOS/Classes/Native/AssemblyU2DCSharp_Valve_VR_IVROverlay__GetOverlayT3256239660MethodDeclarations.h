﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayTransformTrackedDeviceComponent
struct _GetOverlayTransformTrackedDeviceComponent_t3256239660;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayTransformTrackedDeviceComponent::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayTransformTrackedDeviceComponent__ctor_m2315080437 (_GetOverlayTransformTrackedDeviceComponent_t3256239660 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTransformTrackedDeviceComponent::Invoke(System.UInt64,System.UInt32&,System.String,System.UInt32)
extern "C"  int32_t _GetOverlayTransformTrackedDeviceComponent_Invoke_m2277321912 (_GetOverlayTransformTrackedDeviceComponent_t3256239660 * __this, uint64_t ___ulOverlayHandle0, uint32_t* ___punDeviceIndex1, String_t* ___pchComponentName2, uint32_t ___unComponentNameSize3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayTransformTrackedDeviceComponent::BeginInvoke(System.UInt64,System.UInt32&,System.String,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayTransformTrackedDeviceComponent_BeginInvoke_m2550072101 (_GetOverlayTransformTrackedDeviceComponent_t3256239660 * __this, uint64_t ___ulOverlayHandle0, uint32_t* ___punDeviceIndex1, String_t* ___pchComponentName2, uint32_t ___unComponentNameSize3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTransformTrackedDeviceComponent::EndInvoke(System.UInt32&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayTransformTrackedDeviceComponent_EndInvoke_m1165018309 (_GetOverlayTransformTrackedDeviceComponent_t3256239660 * __this, uint32_t* ___punDeviceIndex0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
