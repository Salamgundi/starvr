﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.GrabAttachMechanics.VRTK_FixedJointGrabAttach
struct VRTK_FixedJointGrabAttach_t3523806925;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.GrabAttachMechanics.VRTK_FixedJointGrabAttach::.ctor()
extern "C"  void VRTK_FixedJointGrabAttach__ctor_m3731100281 (VRTK_FixedJointGrabAttach_t3523806925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_FixedJointGrabAttach::CreateJoint(UnityEngine.GameObject)
extern "C"  void VRTK_FixedJointGrabAttach_CreateJoint_m445169143 (VRTK_FixedJointGrabAttach_t3523806925 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
