﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_HeadsetFade
struct VRTK_HeadsetFade_t3539061086;
// VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t515017992;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents
struct  VRTK_HeadsetFade_UnityEvents_t2247800009  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_HeadsetFade VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents::hf
	VRTK_HeadsetFade_t3539061086 * ___hf_2;
	// VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents::OnHeadsetFadeStart
	UnityObjectEvent_t515017992 * ___OnHeadsetFadeStart_3;
	// VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents::OnHeadsetFadeComplete
	UnityObjectEvent_t515017992 * ___OnHeadsetFadeComplete_4;
	// VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents::OnHeadsetUnfadeStart
	UnityObjectEvent_t515017992 * ___OnHeadsetUnfadeStart_5;
	// VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents::OnHeadsetUnfadeComplete
	UnityObjectEvent_t515017992 * ___OnHeadsetUnfadeComplete_6;

public:
	inline static int32_t get_offset_of_hf_2() { return static_cast<int32_t>(offsetof(VRTK_HeadsetFade_UnityEvents_t2247800009, ___hf_2)); }
	inline VRTK_HeadsetFade_t3539061086 * get_hf_2() const { return ___hf_2; }
	inline VRTK_HeadsetFade_t3539061086 ** get_address_of_hf_2() { return &___hf_2; }
	inline void set_hf_2(VRTK_HeadsetFade_t3539061086 * value)
	{
		___hf_2 = value;
		Il2CppCodeGenWriteBarrier(&___hf_2, value);
	}

	inline static int32_t get_offset_of_OnHeadsetFadeStart_3() { return static_cast<int32_t>(offsetof(VRTK_HeadsetFade_UnityEvents_t2247800009, ___OnHeadsetFadeStart_3)); }
	inline UnityObjectEvent_t515017992 * get_OnHeadsetFadeStart_3() const { return ___OnHeadsetFadeStart_3; }
	inline UnityObjectEvent_t515017992 ** get_address_of_OnHeadsetFadeStart_3() { return &___OnHeadsetFadeStart_3; }
	inline void set_OnHeadsetFadeStart_3(UnityObjectEvent_t515017992 * value)
	{
		___OnHeadsetFadeStart_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnHeadsetFadeStart_3, value);
	}

	inline static int32_t get_offset_of_OnHeadsetFadeComplete_4() { return static_cast<int32_t>(offsetof(VRTK_HeadsetFade_UnityEvents_t2247800009, ___OnHeadsetFadeComplete_4)); }
	inline UnityObjectEvent_t515017992 * get_OnHeadsetFadeComplete_4() const { return ___OnHeadsetFadeComplete_4; }
	inline UnityObjectEvent_t515017992 ** get_address_of_OnHeadsetFadeComplete_4() { return &___OnHeadsetFadeComplete_4; }
	inline void set_OnHeadsetFadeComplete_4(UnityObjectEvent_t515017992 * value)
	{
		___OnHeadsetFadeComplete_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnHeadsetFadeComplete_4, value);
	}

	inline static int32_t get_offset_of_OnHeadsetUnfadeStart_5() { return static_cast<int32_t>(offsetof(VRTK_HeadsetFade_UnityEvents_t2247800009, ___OnHeadsetUnfadeStart_5)); }
	inline UnityObjectEvent_t515017992 * get_OnHeadsetUnfadeStart_5() const { return ___OnHeadsetUnfadeStart_5; }
	inline UnityObjectEvent_t515017992 ** get_address_of_OnHeadsetUnfadeStart_5() { return &___OnHeadsetUnfadeStart_5; }
	inline void set_OnHeadsetUnfadeStart_5(UnityObjectEvent_t515017992 * value)
	{
		___OnHeadsetUnfadeStart_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnHeadsetUnfadeStart_5, value);
	}

	inline static int32_t get_offset_of_OnHeadsetUnfadeComplete_6() { return static_cast<int32_t>(offsetof(VRTK_HeadsetFade_UnityEvents_t2247800009, ___OnHeadsetUnfadeComplete_6)); }
	inline UnityObjectEvent_t515017992 * get_OnHeadsetUnfadeComplete_6() const { return ___OnHeadsetUnfadeComplete_6; }
	inline UnityObjectEvent_t515017992 ** get_address_of_OnHeadsetUnfadeComplete_6() { return &___OnHeadsetUnfadeComplete_6; }
	inline void set_OnHeadsetUnfadeComplete_6(UnityObjectEvent_t515017992 * value)
	{
		___OnHeadsetUnfadeComplete_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnHeadsetUnfadeComplete_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
