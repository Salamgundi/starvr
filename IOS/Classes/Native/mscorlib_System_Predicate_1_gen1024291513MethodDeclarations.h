﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen2996539675MethodDeclarations.h"

// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m350650525(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t1024291513 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2898931145_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::Invoke(T)
#define Predicate_1_Invoke_m2670178553(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1024291513 *, KeyValuePair_2_t2581321398 , const MethodInfo*))Predicate_1_Invoke_m4054137757_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m1784441106(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t1024291513 *, KeyValuePair_2_t2581321398 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2924426472_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m1224953763(__this, ___result0, method) ((  bool (*) (Predicate_1_t1024291513 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m2875564199_gshared)(__this, ___result0, method)
