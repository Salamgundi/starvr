﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_PlayAreaCollider
struct VRTK_PlayAreaCollider_t1466370881;
// VRTK.VRTK_PlayAreaCursor
struct VRTK_PlayAreaCursor_t3566057915;
// VRTK.VRTK_PolicyList
struct VRTK_PolicyList_t2965133344;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PlayAreaCursor3566057915.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PolicyList2965133344.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void VRTK.VRTK_PlayAreaCollider::.ctor()
extern "C"  void VRTK_PlayAreaCollider__ctor_m2799180715 (VRTK_PlayAreaCollider_t1466370881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCollider::SetParent(VRTK.VRTK_PlayAreaCursor)
extern "C"  void VRTK_PlayAreaCollider_SetParent_m887004661 (VRTK_PlayAreaCollider_t1466370881 * __this, VRTK_PlayAreaCursor_t3566057915 * ___setParent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCollider::SetIgnoreTarget(VRTK.VRTK_PolicyList)
extern "C"  void VRTK_PlayAreaCollider_SetIgnoreTarget_m1212939355 (VRTK_PlayAreaCollider_t1466370881 * __this, VRTK_PolicyList_t2965133344 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCollider::OnDisable()
extern "C"  void VRTK_PlayAreaCollider_OnDisable_m437995242 (VRTK_PlayAreaCollider_t1466370881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCollider::OnTriggerStay(UnityEngine.Collider)
extern "C"  void VRTK_PlayAreaCollider_OnTriggerStay_m3872086690 (VRTK_PlayAreaCollider_t1466370881 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayAreaCollider::OnTriggerExit(UnityEngine.Collider)
extern "C"  void VRTK_PlayAreaCollider_OnTriggerExit_m3280993851 (VRTK_PlayAreaCollider_t1466370881 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_PlayAreaCollider::ValidTarget(UnityEngine.Collider)
extern "C"  bool VRTK_PlayAreaCollider_ValidTarget_m1880053877 (VRTK_PlayAreaCollider_t1466370881 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
