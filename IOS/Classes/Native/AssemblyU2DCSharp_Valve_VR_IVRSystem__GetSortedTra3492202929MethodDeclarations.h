﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetSortedTrackedDeviceIndicesOfClass
struct _GetSortedTrackedDeviceIndicesOfClass_t3492202929;
// System.Object
struct Il2CppObject;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedDeviceClass2121051631.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetSortedTrackedDeviceIndicesOfClass::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetSortedTrackedDeviceIndicesOfClass__ctor_m2939470824 (_GetSortedTrackedDeviceIndicesOfClass_t3492202929 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRSystem/_GetSortedTrackedDeviceIndicesOfClass::Invoke(Valve.VR.ETrackedDeviceClass,System.UInt32[],System.UInt32,System.UInt32)
extern "C"  uint32_t _GetSortedTrackedDeviceIndicesOfClass_Invoke_m84543012 (_GetSortedTrackedDeviceIndicesOfClass_t3492202929 * __this, int32_t ___eTrackedDeviceClass0, UInt32U5BU5D_t59386216* ___punTrackedDeviceIndexArray1, uint32_t ___unTrackedDeviceIndexArrayCount2, uint32_t ___unRelativeToTrackedDeviceIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetSortedTrackedDeviceIndicesOfClass::BeginInvoke(Valve.VR.ETrackedDeviceClass,System.UInt32[],System.UInt32,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetSortedTrackedDeviceIndicesOfClass_BeginInvoke_m467037688 (_GetSortedTrackedDeviceIndicesOfClass_t3492202929 * __this, int32_t ___eTrackedDeviceClass0, UInt32U5BU5D_t59386216* ___punTrackedDeviceIndexArray1, uint32_t ___unTrackedDeviceIndexArrayCount2, uint32_t ___unRelativeToTrackedDeviceIndex3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRSystem/_GetSortedTrackedDeviceIndicesOfClass::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetSortedTrackedDeviceIndicesOfClass_EndInvoke_m3026927929 (_GetSortedTrackedDeviceIndicesOfClass_t3492202929 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
