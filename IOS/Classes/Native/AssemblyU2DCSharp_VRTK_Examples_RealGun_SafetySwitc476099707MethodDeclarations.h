﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.RealGun_SafetySwitch
struct RealGun_SafetySwitch_t476099707;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.Examples.RealGun_SafetySwitch::.ctor()
extern "C"  void RealGun_SafetySwitch__ctor_m186016900 (RealGun_SafetySwitch_t476099707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun_SafetySwitch::StartUsing(UnityEngine.GameObject)
extern "C"  void RealGun_SafetySwitch_StartUsing_m817953868 (RealGun_SafetySwitch_t476099707 * __this, GameObject_t1756533147 * ___currentUsingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun_SafetySwitch::Start()
extern "C"  void RealGun_SafetySwitch_Start_m303650852 (RealGun_SafetySwitch_t476099707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun_SafetySwitch::Update()
extern "C"  void RealGun_SafetySwitch_Update_m3288091909 (RealGun_SafetySwitch_t476099707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun_SafetySwitch::SetSafety(System.Boolean)
extern "C"  void RealGun_SafetySwitch_SetSafety_m4286476581 (RealGun_SafetySwitch_t476099707 * __this, bool ___safety0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
