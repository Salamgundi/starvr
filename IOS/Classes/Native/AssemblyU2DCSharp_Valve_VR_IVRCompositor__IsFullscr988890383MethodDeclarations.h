﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_IsFullscreen
struct _IsFullscreen_t988890383;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_IsFullscreen::.ctor(System.Object,System.IntPtr)
extern "C"  void _IsFullscreen__ctor_m2303590306 (_IsFullscreen_t988890383 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRCompositor/_IsFullscreen::Invoke()
extern "C"  bool _IsFullscreen_Invoke_m1188878114 (_IsFullscreen_t988890383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_IsFullscreen::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _IsFullscreen_BeginInvoke_m2498905665 (_IsFullscreen_t988890383 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRCompositor/_IsFullscreen::EndInvoke(System.IAsyncResult)
extern "C"  bool _IsFullscreen_EndInvoke_m4031731970 (_IsFullscreen_t988890383 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
