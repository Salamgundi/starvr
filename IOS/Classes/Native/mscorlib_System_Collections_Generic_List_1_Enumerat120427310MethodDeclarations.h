﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Valve.VR.InteractionSystem.SpawnRenderModel>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2530786926(__this, ___l0, method) ((  void (*) (Enumerator_t120427310 *, List_1_t585697636 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Valve.VR.InteractionSystem.SpawnRenderModel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m839534044(__this, method) ((  void (*) (Enumerator_t120427310 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Valve.VR.InteractionSystem.SpawnRenderModel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1134031212(__this, method) ((  Il2CppObject * (*) (Enumerator_t120427310 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Valve.VR.InteractionSystem.SpawnRenderModel>::Dispose()
#define Enumerator_Dispose_m3372903297(__this, method) ((  void (*) (Enumerator_t120427310 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Valve.VR.InteractionSystem.SpawnRenderModel>::VerifyState()
#define Enumerator_VerifyState_m576535728(__this, method) ((  void (*) (Enumerator_t120427310 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Valve.VR.InteractionSystem.SpawnRenderModel>::MoveNext()
#define Enumerator_MoveNext_m681729744(__this, method) ((  bool (*) (Enumerator_t120427310 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Valve.VR.InteractionSystem.SpawnRenderModel>::get_Current()
#define Enumerator_get_Current_m2155805361(__this, method) ((  SpawnRenderModel_t1216576504 * (*) (Enumerator_t120427310 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
