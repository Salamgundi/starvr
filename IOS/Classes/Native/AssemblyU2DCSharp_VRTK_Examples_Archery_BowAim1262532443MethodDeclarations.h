﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Archery.BowAim
struct BowAim_t1262532443;
// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"

// System.Void VRTK.Examples.Archery.BowAim::.ctor()
extern "C"  void BowAim__ctor_m4280451284 (BowAim_t1262532443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_ControllerEvents VRTK.Examples.Archery.BowAim::GetPullHand()
extern "C"  VRTK_ControllerEvents_t3225224819 * BowAim_GetPullHand_m4040838717 (BowAim_t1262532443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.Examples.Archery.BowAim::IsHeld()
extern "C"  bool BowAim_IsHeld_m441981071 (BowAim_t1262532443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.Examples.Archery.BowAim::HasArrow()
extern "C"  bool BowAim_HasArrow_m4251180015 (BowAim_t1262532443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.BowAim::SetArrow(UnityEngine.GameObject)
extern "C"  void BowAim_SetArrow_m437194367 (BowAim_t1262532443 * __this, GameObject_t1756533147 * ___arrow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.BowAim::Start()
extern "C"  void BowAim_Start_m1802611316 (BowAim_t1262532443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.BowAim::DoObjectGrab(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void BowAim_DoObjectGrab_m1736708109 (BowAim_t1262532443 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.Examples.Archery.BowAim::GetBaseRotation()
extern "C"  Il2CppObject * BowAim_GetBaseRotation_m3017829869 (BowAim_t1262532443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.BowAim::Update()
extern "C"  void BowAim_Update_m837937845 (BowAim_t1262532443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.BowAim::Release()
extern "C"  void BowAim_Release_m1576537873 (BowAim_t1262532443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.BowAim::ReleaseArrow()
extern "C"  void BowAim_ReleaseArrow_m38213604 (BowAim_t1262532443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.BowAim::AimArrow()
extern "C"  void BowAim_AimArrow_m3385505724 (BowAim_t1262532443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.BowAim::AimBow()
extern "C"  void BowAim_AimBow_m2781883391 (BowAim_t1262532443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.BowAim::PullString()
extern "C"  void BowAim_PullString_m2481497716 (BowAim_t1262532443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
