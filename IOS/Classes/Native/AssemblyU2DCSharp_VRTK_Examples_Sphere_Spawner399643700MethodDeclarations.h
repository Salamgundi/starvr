﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Sphere_Spawner
struct Sphere_Spawner_t399643700;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"

// System.Void VRTK.Examples.Sphere_Spawner::.ctor()
extern "C"  void Sphere_Spawner__ctor_m2856501729 (Sphere_Spawner_t399643700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Sphere_Spawner::Start()
extern "C"  void Sphere_Spawner_Start_m300779761 (Sphere_Spawner_t399643700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Sphere_Spawner::DoTriggerPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void Sphere_Spawner_DoTriggerPressed_m3874542542 (Sphere_Spawner_t399643700 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Sphere_Spawner::DoTouchpadPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void Sphere_Spawner_DoTouchpadPressed_m2544443372 (Sphere_Spawner_t399643700 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Sphere_Spawner::CreateSphere()
extern "C"  void Sphere_Spawner_CreateSphere_m795641152 (Sphere_Spawner_t399643700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
