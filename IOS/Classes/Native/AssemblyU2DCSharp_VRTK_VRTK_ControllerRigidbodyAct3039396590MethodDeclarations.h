﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ControllerRigidbodyActivator
struct VRTK_ControllerRigidbodyActivator_t3039396590;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void VRTK.VRTK_ControllerRigidbodyActivator::.ctor()
extern "C"  void VRTK_ControllerRigidbodyActivator__ctor_m1040369578 (VRTK_ControllerRigidbodyActivator_t3039396590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerRigidbodyActivator::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void VRTK_ControllerRigidbodyActivator_OnTriggerEnter_m279601606 (VRTK_ControllerRigidbodyActivator_t3039396590 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerRigidbodyActivator::OnTriggerExit(UnityEngine.Collider)
extern "C"  void VRTK_ControllerRigidbodyActivator_OnTriggerExit_m1232627052 (VRTK_ControllerRigidbodyActivator_t3039396590 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerRigidbodyActivator::ToggleRigidbody(UnityEngine.Collider,System.Boolean)
extern "C"  void VRTK_ControllerRigidbodyActivator_ToggleRigidbody_m1429319535 (VRTK_ControllerRigidbodyActivator_t3039396590 * __this, Collider_t3497673348 * ___collider0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
