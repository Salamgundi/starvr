﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_FramesPerSecondViewer
struct  VRTK_FramesPerSecondViewer_t2787247731  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.VRTK_FramesPerSecondViewer::displayFPS
	bool ___displayFPS_2;
	// System.Int32 VRTK.VRTK_FramesPerSecondViewer::targetFPS
	int32_t ___targetFPS_3;
	// System.Int32 VRTK.VRTK_FramesPerSecondViewer::fontSize
	int32_t ___fontSize_4;
	// UnityEngine.Vector3 VRTK.VRTK_FramesPerSecondViewer::position
	Vector3_t2243707580  ___position_5;
	// UnityEngine.Color VRTK.VRTK_FramesPerSecondViewer::goodColor
	Color_t2020392075  ___goodColor_6;
	// UnityEngine.Color VRTK.VRTK_FramesPerSecondViewer::warnColor
	Color_t2020392075  ___warnColor_7;
	// UnityEngine.Color VRTK.VRTK_FramesPerSecondViewer::badColor
	Color_t2020392075  ___badColor_8;
	// System.Int32 VRTK.VRTK_FramesPerSecondViewer::framesCount
	int32_t ___framesCount_10;
	// System.Single VRTK.VRTK_FramesPerSecondViewer::framesTime
	float ___framesTime_11;
	// UnityEngine.UI.Text VRTK.VRTK_FramesPerSecondViewer::text
	Text_t356221433 * ___text_12;

public:
	inline static int32_t get_offset_of_displayFPS_2() { return static_cast<int32_t>(offsetof(VRTK_FramesPerSecondViewer_t2787247731, ___displayFPS_2)); }
	inline bool get_displayFPS_2() const { return ___displayFPS_2; }
	inline bool* get_address_of_displayFPS_2() { return &___displayFPS_2; }
	inline void set_displayFPS_2(bool value)
	{
		___displayFPS_2 = value;
	}

	inline static int32_t get_offset_of_targetFPS_3() { return static_cast<int32_t>(offsetof(VRTK_FramesPerSecondViewer_t2787247731, ___targetFPS_3)); }
	inline int32_t get_targetFPS_3() const { return ___targetFPS_3; }
	inline int32_t* get_address_of_targetFPS_3() { return &___targetFPS_3; }
	inline void set_targetFPS_3(int32_t value)
	{
		___targetFPS_3 = value;
	}

	inline static int32_t get_offset_of_fontSize_4() { return static_cast<int32_t>(offsetof(VRTK_FramesPerSecondViewer_t2787247731, ___fontSize_4)); }
	inline int32_t get_fontSize_4() const { return ___fontSize_4; }
	inline int32_t* get_address_of_fontSize_4() { return &___fontSize_4; }
	inline void set_fontSize_4(int32_t value)
	{
		___fontSize_4 = value;
	}

	inline static int32_t get_offset_of_position_5() { return static_cast<int32_t>(offsetof(VRTK_FramesPerSecondViewer_t2787247731, ___position_5)); }
	inline Vector3_t2243707580  get_position_5() const { return ___position_5; }
	inline Vector3_t2243707580 * get_address_of_position_5() { return &___position_5; }
	inline void set_position_5(Vector3_t2243707580  value)
	{
		___position_5 = value;
	}

	inline static int32_t get_offset_of_goodColor_6() { return static_cast<int32_t>(offsetof(VRTK_FramesPerSecondViewer_t2787247731, ___goodColor_6)); }
	inline Color_t2020392075  get_goodColor_6() const { return ___goodColor_6; }
	inline Color_t2020392075 * get_address_of_goodColor_6() { return &___goodColor_6; }
	inline void set_goodColor_6(Color_t2020392075  value)
	{
		___goodColor_6 = value;
	}

	inline static int32_t get_offset_of_warnColor_7() { return static_cast<int32_t>(offsetof(VRTK_FramesPerSecondViewer_t2787247731, ___warnColor_7)); }
	inline Color_t2020392075  get_warnColor_7() const { return ___warnColor_7; }
	inline Color_t2020392075 * get_address_of_warnColor_7() { return &___warnColor_7; }
	inline void set_warnColor_7(Color_t2020392075  value)
	{
		___warnColor_7 = value;
	}

	inline static int32_t get_offset_of_badColor_8() { return static_cast<int32_t>(offsetof(VRTK_FramesPerSecondViewer_t2787247731, ___badColor_8)); }
	inline Color_t2020392075  get_badColor_8() const { return ___badColor_8; }
	inline Color_t2020392075 * get_address_of_badColor_8() { return &___badColor_8; }
	inline void set_badColor_8(Color_t2020392075  value)
	{
		___badColor_8 = value;
	}

	inline static int32_t get_offset_of_framesCount_10() { return static_cast<int32_t>(offsetof(VRTK_FramesPerSecondViewer_t2787247731, ___framesCount_10)); }
	inline int32_t get_framesCount_10() const { return ___framesCount_10; }
	inline int32_t* get_address_of_framesCount_10() { return &___framesCount_10; }
	inline void set_framesCount_10(int32_t value)
	{
		___framesCount_10 = value;
	}

	inline static int32_t get_offset_of_framesTime_11() { return static_cast<int32_t>(offsetof(VRTK_FramesPerSecondViewer_t2787247731, ___framesTime_11)); }
	inline float get_framesTime_11() const { return ___framesTime_11; }
	inline float* get_address_of_framesTime_11() { return &___framesTime_11; }
	inline void set_framesTime_11(float value)
	{
		___framesTime_11 = value;
	}

	inline static int32_t get_offset_of_text_12() { return static_cast<int32_t>(offsetof(VRTK_FramesPerSecondViewer_t2787247731, ___text_12)); }
	inline Text_t356221433 * get_text_12() const { return ___text_12; }
	inline Text_t356221433 ** get_address_of_text_12() { return &___text_12; }
	inline void set_text_12(Text_t356221433 * value)
	{
		___text_12 = value;
		Il2CppCodeGenWriteBarrier(&___text_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
