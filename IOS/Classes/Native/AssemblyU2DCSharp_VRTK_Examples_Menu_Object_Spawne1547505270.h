﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Menu_Object_Spawne2203986029.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Menu_Object_Spawner
struct  Menu_Object_Spawner_t1547505270  : public VRTK_InteractableObject_t2604188111
{
public:
	// VRTK.Examples.Menu_Object_Spawner/PrimitiveTypes VRTK.Examples.Menu_Object_Spawner::shape
	int32_t ___shape_45;
	// UnityEngine.Color VRTK.Examples.Menu_Object_Spawner::selectedColor
	Color_t2020392075  ___selectedColor_46;

public:
	inline static int32_t get_offset_of_shape_45() { return static_cast<int32_t>(offsetof(Menu_Object_Spawner_t1547505270, ___shape_45)); }
	inline int32_t get_shape_45() const { return ___shape_45; }
	inline int32_t* get_address_of_shape_45() { return &___shape_45; }
	inline void set_shape_45(int32_t value)
	{
		___shape_45 = value;
	}

	inline static int32_t get_offset_of_selectedColor_46() { return static_cast<int32_t>(offsetof(Menu_Object_Spawner_t1547505270, ___selectedColor_46)); }
	inline Color_t2020392075  get_selectedColor_46() const { return ___selectedColor_46; }
	inline Color_t2020392075 * get_address_of_selectedColor_46() { return &___selectedColor_46; }
	inline void set_selectedColor_46(Color_t2020392075  value)
	{
		___selectedColor_46 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
