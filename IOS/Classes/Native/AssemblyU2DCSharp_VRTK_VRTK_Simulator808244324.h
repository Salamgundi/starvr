﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_Simulator/Keys
struct Keys_t2631472146;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Simulator
struct  VRTK_Simulator_t808244324  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_Simulator/Keys VRTK.VRTK_Simulator::keys
	Keys_t2631472146 * ___keys_2;
	// System.Boolean VRTK.VRTK_Simulator::onlyInEditor
	bool ___onlyInEditor_3;
	// System.Single VRTK.VRTK_Simulator::stepSize
	float ___stepSize_4;
	// UnityEngine.Transform VRTK.VRTK_Simulator::camStart
	Transform_t3275118058 * ___camStart_5;
	// UnityEngine.Transform VRTK.VRTK_Simulator::headset
	Transform_t3275118058 * ___headset_6;
	// UnityEngine.Transform VRTK.VRTK_Simulator::playArea
	Transform_t3275118058 * ___playArea_7;
	// UnityEngine.Vector3 VRTK.VRTK_Simulator::initialPosition
	Vector3_t2243707580  ___initialPosition_8;
	// UnityEngine.Quaternion VRTK.VRTK_Simulator::initialRotation
	Quaternion_t4030073918  ___initialRotation_9;

public:
	inline static int32_t get_offset_of_keys_2() { return static_cast<int32_t>(offsetof(VRTK_Simulator_t808244324, ___keys_2)); }
	inline Keys_t2631472146 * get_keys_2() const { return ___keys_2; }
	inline Keys_t2631472146 ** get_address_of_keys_2() { return &___keys_2; }
	inline void set_keys_2(Keys_t2631472146 * value)
	{
		___keys_2 = value;
		Il2CppCodeGenWriteBarrier(&___keys_2, value);
	}

	inline static int32_t get_offset_of_onlyInEditor_3() { return static_cast<int32_t>(offsetof(VRTK_Simulator_t808244324, ___onlyInEditor_3)); }
	inline bool get_onlyInEditor_3() const { return ___onlyInEditor_3; }
	inline bool* get_address_of_onlyInEditor_3() { return &___onlyInEditor_3; }
	inline void set_onlyInEditor_3(bool value)
	{
		___onlyInEditor_3 = value;
	}

	inline static int32_t get_offset_of_stepSize_4() { return static_cast<int32_t>(offsetof(VRTK_Simulator_t808244324, ___stepSize_4)); }
	inline float get_stepSize_4() const { return ___stepSize_4; }
	inline float* get_address_of_stepSize_4() { return &___stepSize_4; }
	inline void set_stepSize_4(float value)
	{
		___stepSize_4 = value;
	}

	inline static int32_t get_offset_of_camStart_5() { return static_cast<int32_t>(offsetof(VRTK_Simulator_t808244324, ___camStart_5)); }
	inline Transform_t3275118058 * get_camStart_5() const { return ___camStart_5; }
	inline Transform_t3275118058 ** get_address_of_camStart_5() { return &___camStart_5; }
	inline void set_camStart_5(Transform_t3275118058 * value)
	{
		___camStart_5 = value;
		Il2CppCodeGenWriteBarrier(&___camStart_5, value);
	}

	inline static int32_t get_offset_of_headset_6() { return static_cast<int32_t>(offsetof(VRTK_Simulator_t808244324, ___headset_6)); }
	inline Transform_t3275118058 * get_headset_6() const { return ___headset_6; }
	inline Transform_t3275118058 ** get_address_of_headset_6() { return &___headset_6; }
	inline void set_headset_6(Transform_t3275118058 * value)
	{
		___headset_6 = value;
		Il2CppCodeGenWriteBarrier(&___headset_6, value);
	}

	inline static int32_t get_offset_of_playArea_7() { return static_cast<int32_t>(offsetof(VRTK_Simulator_t808244324, ___playArea_7)); }
	inline Transform_t3275118058 * get_playArea_7() const { return ___playArea_7; }
	inline Transform_t3275118058 ** get_address_of_playArea_7() { return &___playArea_7; }
	inline void set_playArea_7(Transform_t3275118058 * value)
	{
		___playArea_7 = value;
		Il2CppCodeGenWriteBarrier(&___playArea_7, value);
	}

	inline static int32_t get_offset_of_initialPosition_8() { return static_cast<int32_t>(offsetof(VRTK_Simulator_t808244324, ___initialPosition_8)); }
	inline Vector3_t2243707580  get_initialPosition_8() const { return ___initialPosition_8; }
	inline Vector3_t2243707580 * get_address_of_initialPosition_8() { return &___initialPosition_8; }
	inline void set_initialPosition_8(Vector3_t2243707580  value)
	{
		___initialPosition_8 = value;
	}

	inline static int32_t get_offset_of_initialRotation_9() { return static_cast<int32_t>(offsetof(VRTK_Simulator_t808244324, ___initialRotation_9)); }
	inline Quaternion_t4030073918  get_initialRotation_9() const { return ___initialRotation_9; }
	inline Quaternion_t4030073918 * get_address_of_initialRotation_9() { return &___initialRotation_9; }
	inline void set_initialRotation_9(Quaternion_t4030073918  value)
	{
		___initialRotation_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
