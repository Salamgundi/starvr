﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// Valve.VR.InteractionSystem.Player
struct Player_t4256718089;
// Valve.VR.InteractionSystem.TeleportArc
struct TeleportArc_t2123987351;
// Valve.VR.InteractionSystem.TeleportMarkerBase[]
struct TeleportMarkerBaseU5BU5D_t3589211913;
// Valve.VR.InteractionSystem.TeleportMarkerBase
struct TeleportMarkerBase_t1112706968;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// Valve.VR.InteractionSystem.Interactable
struct Interactable_t1274046986;
// Valve.VR.InteractionSystem.AllowTeleportWhileAttachedToHand
struct AllowTeleportWhileAttachedToHand_t416616227;
// SteamVR_Events/Action
struct Action_t1836998693;
// SteamVR_Events/Event`1<System.Single>
struct Event_1_t4251932349;
// SteamVR_Events/Event`1<Valve.VR.InteractionSystem.TeleportMarkerBase>
struct Event_1_t3288129385;
// Valve.VR.InteractionSystem.Teleport
struct Teleport_t865564691;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.Teleport
struct  Teleport_t865564691  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.LayerMask Valve.VR.InteractionSystem.Teleport::traceLayerMask
	LayerMask_t3188175821  ___traceLayerMask_2;
	// UnityEngine.LayerMask Valve.VR.InteractionSystem.Teleport::floorFixupTraceLayerMask
	LayerMask_t3188175821  ___floorFixupTraceLayerMask_3;
	// System.Single Valve.VR.InteractionSystem.Teleport::floorFixupMaximumTraceDistance
	float ___floorFixupMaximumTraceDistance_4;
	// UnityEngine.Material Valve.VR.InteractionSystem.Teleport::areaVisibleMaterial
	Material_t193706927 * ___areaVisibleMaterial_5;
	// UnityEngine.Material Valve.VR.InteractionSystem.Teleport::areaLockedMaterial
	Material_t193706927 * ___areaLockedMaterial_6;
	// UnityEngine.Material Valve.VR.InteractionSystem.Teleport::areaHighlightedMaterial
	Material_t193706927 * ___areaHighlightedMaterial_7;
	// UnityEngine.Material Valve.VR.InteractionSystem.Teleport::pointVisibleMaterial
	Material_t193706927 * ___pointVisibleMaterial_8;
	// UnityEngine.Material Valve.VR.InteractionSystem.Teleport::pointLockedMaterial
	Material_t193706927 * ___pointLockedMaterial_9;
	// UnityEngine.Material Valve.VR.InteractionSystem.Teleport::pointHighlightedMaterial
	Material_t193706927 * ___pointHighlightedMaterial_10;
	// UnityEngine.Transform Valve.VR.InteractionSystem.Teleport::destinationReticleTransform
	Transform_t3275118058 * ___destinationReticleTransform_11;
	// UnityEngine.Transform Valve.VR.InteractionSystem.Teleport::invalidReticleTransform
	Transform_t3275118058 * ___invalidReticleTransform_12;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Teleport::playAreaPreviewCorner
	GameObject_t1756533147 * ___playAreaPreviewCorner_13;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Teleport::playAreaPreviewSide
	GameObject_t1756533147 * ___playAreaPreviewSide_14;
	// UnityEngine.Color Valve.VR.InteractionSystem.Teleport::pointerValidColor
	Color_t2020392075  ___pointerValidColor_15;
	// UnityEngine.Color Valve.VR.InteractionSystem.Teleport::pointerInvalidColor
	Color_t2020392075  ___pointerInvalidColor_16;
	// UnityEngine.Color Valve.VR.InteractionSystem.Teleport::pointerLockedColor
	Color_t2020392075  ___pointerLockedColor_17;
	// System.Boolean Valve.VR.InteractionSystem.Teleport::showPlayAreaMarker
	bool ___showPlayAreaMarker_18;
	// System.Single Valve.VR.InteractionSystem.Teleport::teleportFadeTime
	float ___teleportFadeTime_19;
	// System.Single Valve.VR.InteractionSystem.Teleport::meshFadeTime
	float ___meshFadeTime_20;
	// System.Single Valve.VR.InteractionSystem.Teleport::arcDistance
	float ___arcDistance_21;
	// UnityEngine.Transform Valve.VR.InteractionSystem.Teleport::onActivateObjectTransform
	Transform_t3275118058 * ___onActivateObjectTransform_22;
	// UnityEngine.Transform Valve.VR.InteractionSystem.Teleport::onDeactivateObjectTransform
	Transform_t3275118058 * ___onDeactivateObjectTransform_23;
	// System.Single Valve.VR.InteractionSystem.Teleport::activateObjectTime
	float ___activateObjectTime_24;
	// System.Single Valve.VR.InteractionSystem.Teleport::deactivateObjectTime
	float ___deactivateObjectTime_25;
	// UnityEngine.AudioSource Valve.VR.InteractionSystem.Teleport::pointerAudioSource
	AudioSource_t1135106623 * ___pointerAudioSource_26;
	// UnityEngine.AudioSource Valve.VR.InteractionSystem.Teleport::loopingAudioSource
	AudioSource_t1135106623 * ___loopingAudioSource_27;
	// UnityEngine.AudioSource Valve.VR.InteractionSystem.Teleport::headAudioSource
	AudioSource_t1135106623 * ___headAudioSource_28;
	// UnityEngine.AudioSource Valve.VR.InteractionSystem.Teleport::reticleAudioSource
	AudioSource_t1135106623 * ___reticleAudioSource_29;
	// UnityEngine.AudioClip Valve.VR.InteractionSystem.Teleport::teleportSound
	AudioClip_t1932558630 * ___teleportSound_30;
	// UnityEngine.AudioClip Valve.VR.InteractionSystem.Teleport::pointerStartSound
	AudioClip_t1932558630 * ___pointerStartSound_31;
	// UnityEngine.AudioClip Valve.VR.InteractionSystem.Teleport::pointerLoopSound
	AudioClip_t1932558630 * ___pointerLoopSound_32;
	// UnityEngine.AudioClip Valve.VR.InteractionSystem.Teleport::pointerStopSound
	AudioClip_t1932558630 * ___pointerStopSound_33;
	// UnityEngine.AudioClip Valve.VR.InteractionSystem.Teleport::goodHighlightSound
	AudioClip_t1932558630 * ___goodHighlightSound_34;
	// UnityEngine.AudioClip Valve.VR.InteractionSystem.Teleport::badHighlightSound
	AudioClip_t1932558630 * ___badHighlightSound_35;
	// System.Boolean Valve.VR.InteractionSystem.Teleport::debugFloor
	bool ___debugFloor_36;
	// System.Boolean Valve.VR.InteractionSystem.Teleport::showOffsetReticle
	bool ___showOffsetReticle_37;
	// UnityEngine.Transform Valve.VR.InteractionSystem.Teleport::offsetReticleTransform
	Transform_t3275118058 * ___offsetReticleTransform_38;
	// UnityEngine.MeshRenderer Valve.VR.InteractionSystem.Teleport::floorDebugSphere
	MeshRenderer_t1268241104 * ___floorDebugSphere_39;
	// UnityEngine.LineRenderer Valve.VR.InteractionSystem.Teleport::floorDebugLine
	LineRenderer_t849157671 * ___floorDebugLine_40;
	// UnityEngine.LineRenderer Valve.VR.InteractionSystem.Teleport::pointerLineRenderer
	LineRenderer_t849157671 * ___pointerLineRenderer_41;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Teleport::teleportPointerObject
	GameObject_t1756533147 * ___teleportPointerObject_42;
	// UnityEngine.Transform Valve.VR.InteractionSystem.Teleport::pointerStartTransform
	Transform_t3275118058 * ___pointerStartTransform_43;
	// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.Teleport::pointerHand
	Hand_t379716353 * ___pointerHand_44;
	// Valve.VR.InteractionSystem.Player Valve.VR.InteractionSystem.Teleport::player
	Player_t4256718089 * ___player_45;
	// Valve.VR.InteractionSystem.TeleportArc Valve.VR.InteractionSystem.Teleport::teleportArc
	TeleportArc_t2123987351 * ___teleportArc_46;
	// System.Boolean Valve.VR.InteractionSystem.Teleport::visible
	bool ___visible_47;
	// Valve.VR.InteractionSystem.TeleportMarkerBase[] Valve.VR.InteractionSystem.Teleport::teleportMarkers
	TeleportMarkerBaseU5BU5D_t3589211913* ___teleportMarkers_48;
	// Valve.VR.InteractionSystem.TeleportMarkerBase Valve.VR.InteractionSystem.Teleport::pointedAtTeleportMarker
	TeleportMarkerBase_t1112706968 * ___pointedAtTeleportMarker_49;
	// Valve.VR.InteractionSystem.TeleportMarkerBase Valve.VR.InteractionSystem.Teleport::teleportingToMarker
	TeleportMarkerBase_t1112706968 * ___teleportingToMarker_50;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.Teleport::pointedAtPosition
	Vector3_t2243707580  ___pointedAtPosition_51;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.Teleport::prevPointedAtPosition
	Vector3_t2243707580  ___prevPointedAtPosition_52;
	// System.Boolean Valve.VR.InteractionSystem.Teleport::teleporting
	bool ___teleporting_53;
	// System.Single Valve.VR.InteractionSystem.Teleport::currentFadeTime
	float ___currentFadeTime_54;
	// System.Single Valve.VR.InteractionSystem.Teleport::meshAlphaPercent
	float ___meshAlphaPercent_55;
	// System.Single Valve.VR.InteractionSystem.Teleport::pointerShowStartTime
	float ___pointerShowStartTime_56;
	// System.Single Valve.VR.InteractionSystem.Teleport::pointerHideStartTime
	float ___pointerHideStartTime_57;
	// System.Boolean Valve.VR.InteractionSystem.Teleport::meshFading
	bool ___meshFading_58;
	// System.Single Valve.VR.InteractionSystem.Teleport::fullTintAlpha
	float ___fullTintAlpha_59;
	// System.Single Valve.VR.InteractionSystem.Teleport::invalidReticleMinScale
	float ___invalidReticleMinScale_60;
	// System.Single Valve.VR.InteractionSystem.Teleport::invalidReticleMaxScale
	float ___invalidReticleMaxScale_61;
	// System.Single Valve.VR.InteractionSystem.Teleport::invalidReticleMinScaleDistance
	float ___invalidReticleMinScaleDistance_62;
	// System.Single Valve.VR.InteractionSystem.Teleport::invalidReticleMaxScaleDistance
	float ___invalidReticleMaxScaleDistance_63;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.Teleport::invalidReticleScale
	Vector3_t2243707580  ___invalidReticleScale_64;
	// UnityEngine.Quaternion Valve.VR.InteractionSystem.Teleport::invalidReticleTargetRotation
	Quaternion_t4030073918  ___invalidReticleTargetRotation_65;
	// UnityEngine.Transform Valve.VR.InteractionSystem.Teleport::playAreaPreviewTransform
	Transform_t3275118058 * ___playAreaPreviewTransform_66;
	// UnityEngine.Transform[] Valve.VR.InteractionSystem.Teleport::playAreaPreviewCorners
	TransformU5BU5D_t3764228911* ___playAreaPreviewCorners_67;
	// UnityEngine.Transform[] Valve.VR.InteractionSystem.Teleport::playAreaPreviewSides
	TransformU5BU5D_t3764228911* ___playAreaPreviewSides_68;
	// System.Single Valve.VR.InteractionSystem.Teleport::loopingAudioMaxVolume
	float ___loopingAudioMaxVolume_69;
	// UnityEngine.Coroutine Valve.VR.InteractionSystem.Teleport::hintCoroutine
	Coroutine_t2299508840 * ___hintCoroutine_70;
	// System.Boolean Valve.VR.InteractionSystem.Teleport::originalHoverLockState
	bool ___originalHoverLockState_71;
	// Valve.VR.InteractionSystem.Interactable Valve.VR.InteractionSystem.Teleport::originalHoveringInteractable
	Interactable_t1274046986 * ___originalHoveringInteractable_72;
	// Valve.VR.InteractionSystem.AllowTeleportWhileAttachedToHand Valve.VR.InteractionSystem.Teleport::allowTeleportWhileAttached
	AllowTeleportWhileAttachedToHand_t416616227 * ___allowTeleportWhileAttached_73;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.Teleport::startingFeetOffset
	Vector3_t2243707580  ___startingFeetOffset_74;
	// System.Boolean Valve.VR.InteractionSystem.Teleport::movedFeetFarEnough
	bool ___movedFeetFarEnough_75;
	// SteamVR_Events/Action Valve.VR.InteractionSystem.Teleport::chaperoneInfoInitializedAction
	Action_t1836998693 * ___chaperoneInfoInitializedAction_76;

public:
	inline static int32_t get_offset_of_traceLayerMask_2() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___traceLayerMask_2)); }
	inline LayerMask_t3188175821  get_traceLayerMask_2() const { return ___traceLayerMask_2; }
	inline LayerMask_t3188175821 * get_address_of_traceLayerMask_2() { return &___traceLayerMask_2; }
	inline void set_traceLayerMask_2(LayerMask_t3188175821  value)
	{
		___traceLayerMask_2 = value;
	}

	inline static int32_t get_offset_of_floorFixupTraceLayerMask_3() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___floorFixupTraceLayerMask_3)); }
	inline LayerMask_t3188175821  get_floorFixupTraceLayerMask_3() const { return ___floorFixupTraceLayerMask_3; }
	inline LayerMask_t3188175821 * get_address_of_floorFixupTraceLayerMask_3() { return &___floorFixupTraceLayerMask_3; }
	inline void set_floorFixupTraceLayerMask_3(LayerMask_t3188175821  value)
	{
		___floorFixupTraceLayerMask_3 = value;
	}

	inline static int32_t get_offset_of_floorFixupMaximumTraceDistance_4() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___floorFixupMaximumTraceDistance_4)); }
	inline float get_floorFixupMaximumTraceDistance_4() const { return ___floorFixupMaximumTraceDistance_4; }
	inline float* get_address_of_floorFixupMaximumTraceDistance_4() { return &___floorFixupMaximumTraceDistance_4; }
	inline void set_floorFixupMaximumTraceDistance_4(float value)
	{
		___floorFixupMaximumTraceDistance_4 = value;
	}

	inline static int32_t get_offset_of_areaVisibleMaterial_5() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___areaVisibleMaterial_5)); }
	inline Material_t193706927 * get_areaVisibleMaterial_5() const { return ___areaVisibleMaterial_5; }
	inline Material_t193706927 ** get_address_of_areaVisibleMaterial_5() { return &___areaVisibleMaterial_5; }
	inline void set_areaVisibleMaterial_5(Material_t193706927 * value)
	{
		___areaVisibleMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___areaVisibleMaterial_5, value);
	}

	inline static int32_t get_offset_of_areaLockedMaterial_6() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___areaLockedMaterial_6)); }
	inline Material_t193706927 * get_areaLockedMaterial_6() const { return ___areaLockedMaterial_6; }
	inline Material_t193706927 ** get_address_of_areaLockedMaterial_6() { return &___areaLockedMaterial_6; }
	inline void set_areaLockedMaterial_6(Material_t193706927 * value)
	{
		___areaLockedMaterial_6 = value;
		Il2CppCodeGenWriteBarrier(&___areaLockedMaterial_6, value);
	}

	inline static int32_t get_offset_of_areaHighlightedMaterial_7() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___areaHighlightedMaterial_7)); }
	inline Material_t193706927 * get_areaHighlightedMaterial_7() const { return ___areaHighlightedMaterial_7; }
	inline Material_t193706927 ** get_address_of_areaHighlightedMaterial_7() { return &___areaHighlightedMaterial_7; }
	inline void set_areaHighlightedMaterial_7(Material_t193706927 * value)
	{
		___areaHighlightedMaterial_7 = value;
		Il2CppCodeGenWriteBarrier(&___areaHighlightedMaterial_7, value);
	}

	inline static int32_t get_offset_of_pointVisibleMaterial_8() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointVisibleMaterial_8)); }
	inline Material_t193706927 * get_pointVisibleMaterial_8() const { return ___pointVisibleMaterial_8; }
	inline Material_t193706927 ** get_address_of_pointVisibleMaterial_8() { return &___pointVisibleMaterial_8; }
	inline void set_pointVisibleMaterial_8(Material_t193706927 * value)
	{
		___pointVisibleMaterial_8 = value;
		Il2CppCodeGenWriteBarrier(&___pointVisibleMaterial_8, value);
	}

	inline static int32_t get_offset_of_pointLockedMaterial_9() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointLockedMaterial_9)); }
	inline Material_t193706927 * get_pointLockedMaterial_9() const { return ___pointLockedMaterial_9; }
	inline Material_t193706927 ** get_address_of_pointLockedMaterial_9() { return &___pointLockedMaterial_9; }
	inline void set_pointLockedMaterial_9(Material_t193706927 * value)
	{
		___pointLockedMaterial_9 = value;
		Il2CppCodeGenWriteBarrier(&___pointLockedMaterial_9, value);
	}

	inline static int32_t get_offset_of_pointHighlightedMaterial_10() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointHighlightedMaterial_10)); }
	inline Material_t193706927 * get_pointHighlightedMaterial_10() const { return ___pointHighlightedMaterial_10; }
	inline Material_t193706927 ** get_address_of_pointHighlightedMaterial_10() { return &___pointHighlightedMaterial_10; }
	inline void set_pointHighlightedMaterial_10(Material_t193706927 * value)
	{
		___pointHighlightedMaterial_10 = value;
		Il2CppCodeGenWriteBarrier(&___pointHighlightedMaterial_10, value);
	}

	inline static int32_t get_offset_of_destinationReticleTransform_11() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___destinationReticleTransform_11)); }
	inline Transform_t3275118058 * get_destinationReticleTransform_11() const { return ___destinationReticleTransform_11; }
	inline Transform_t3275118058 ** get_address_of_destinationReticleTransform_11() { return &___destinationReticleTransform_11; }
	inline void set_destinationReticleTransform_11(Transform_t3275118058 * value)
	{
		___destinationReticleTransform_11 = value;
		Il2CppCodeGenWriteBarrier(&___destinationReticleTransform_11, value);
	}

	inline static int32_t get_offset_of_invalidReticleTransform_12() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___invalidReticleTransform_12)); }
	inline Transform_t3275118058 * get_invalidReticleTransform_12() const { return ___invalidReticleTransform_12; }
	inline Transform_t3275118058 ** get_address_of_invalidReticleTransform_12() { return &___invalidReticleTransform_12; }
	inline void set_invalidReticleTransform_12(Transform_t3275118058 * value)
	{
		___invalidReticleTransform_12 = value;
		Il2CppCodeGenWriteBarrier(&___invalidReticleTransform_12, value);
	}

	inline static int32_t get_offset_of_playAreaPreviewCorner_13() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___playAreaPreviewCorner_13)); }
	inline GameObject_t1756533147 * get_playAreaPreviewCorner_13() const { return ___playAreaPreviewCorner_13; }
	inline GameObject_t1756533147 ** get_address_of_playAreaPreviewCorner_13() { return &___playAreaPreviewCorner_13; }
	inline void set_playAreaPreviewCorner_13(GameObject_t1756533147 * value)
	{
		___playAreaPreviewCorner_13 = value;
		Il2CppCodeGenWriteBarrier(&___playAreaPreviewCorner_13, value);
	}

	inline static int32_t get_offset_of_playAreaPreviewSide_14() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___playAreaPreviewSide_14)); }
	inline GameObject_t1756533147 * get_playAreaPreviewSide_14() const { return ___playAreaPreviewSide_14; }
	inline GameObject_t1756533147 ** get_address_of_playAreaPreviewSide_14() { return &___playAreaPreviewSide_14; }
	inline void set_playAreaPreviewSide_14(GameObject_t1756533147 * value)
	{
		___playAreaPreviewSide_14 = value;
		Il2CppCodeGenWriteBarrier(&___playAreaPreviewSide_14, value);
	}

	inline static int32_t get_offset_of_pointerValidColor_15() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointerValidColor_15)); }
	inline Color_t2020392075  get_pointerValidColor_15() const { return ___pointerValidColor_15; }
	inline Color_t2020392075 * get_address_of_pointerValidColor_15() { return &___pointerValidColor_15; }
	inline void set_pointerValidColor_15(Color_t2020392075  value)
	{
		___pointerValidColor_15 = value;
	}

	inline static int32_t get_offset_of_pointerInvalidColor_16() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointerInvalidColor_16)); }
	inline Color_t2020392075  get_pointerInvalidColor_16() const { return ___pointerInvalidColor_16; }
	inline Color_t2020392075 * get_address_of_pointerInvalidColor_16() { return &___pointerInvalidColor_16; }
	inline void set_pointerInvalidColor_16(Color_t2020392075  value)
	{
		___pointerInvalidColor_16 = value;
	}

	inline static int32_t get_offset_of_pointerLockedColor_17() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointerLockedColor_17)); }
	inline Color_t2020392075  get_pointerLockedColor_17() const { return ___pointerLockedColor_17; }
	inline Color_t2020392075 * get_address_of_pointerLockedColor_17() { return &___pointerLockedColor_17; }
	inline void set_pointerLockedColor_17(Color_t2020392075  value)
	{
		___pointerLockedColor_17 = value;
	}

	inline static int32_t get_offset_of_showPlayAreaMarker_18() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___showPlayAreaMarker_18)); }
	inline bool get_showPlayAreaMarker_18() const { return ___showPlayAreaMarker_18; }
	inline bool* get_address_of_showPlayAreaMarker_18() { return &___showPlayAreaMarker_18; }
	inline void set_showPlayAreaMarker_18(bool value)
	{
		___showPlayAreaMarker_18 = value;
	}

	inline static int32_t get_offset_of_teleportFadeTime_19() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___teleportFadeTime_19)); }
	inline float get_teleportFadeTime_19() const { return ___teleportFadeTime_19; }
	inline float* get_address_of_teleportFadeTime_19() { return &___teleportFadeTime_19; }
	inline void set_teleportFadeTime_19(float value)
	{
		___teleportFadeTime_19 = value;
	}

	inline static int32_t get_offset_of_meshFadeTime_20() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___meshFadeTime_20)); }
	inline float get_meshFadeTime_20() const { return ___meshFadeTime_20; }
	inline float* get_address_of_meshFadeTime_20() { return &___meshFadeTime_20; }
	inline void set_meshFadeTime_20(float value)
	{
		___meshFadeTime_20 = value;
	}

	inline static int32_t get_offset_of_arcDistance_21() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___arcDistance_21)); }
	inline float get_arcDistance_21() const { return ___arcDistance_21; }
	inline float* get_address_of_arcDistance_21() { return &___arcDistance_21; }
	inline void set_arcDistance_21(float value)
	{
		___arcDistance_21 = value;
	}

	inline static int32_t get_offset_of_onActivateObjectTransform_22() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___onActivateObjectTransform_22)); }
	inline Transform_t3275118058 * get_onActivateObjectTransform_22() const { return ___onActivateObjectTransform_22; }
	inline Transform_t3275118058 ** get_address_of_onActivateObjectTransform_22() { return &___onActivateObjectTransform_22; }
	inline void set_onActivateObjectTransform_22(Transform_t3275118058 * value)
	{
		___onActivateObjectTransform_22 = value;
		Il2CppCodeGenWriteBarrier(&___onActivateObjectTransform_22, value);
	}

	inline static int32_t get_offset_of_onDeactivateObjectTransform_23() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___onDeactivateObjectTransform_23)); }
	inline Transform_t3275118058 * get_onDeactivateObjectTransform_23() const { return ___onDeactivateObjectTransform_23; }
	inline Transform_t3275118058 ** get_address_of_onDeactivateObjectTransform_23() { return &___onDeactivateObjectTransform_23; }
	inline void set_onDeactivateObjectTransform_23(Transform_t3275118058 * value)
	{
		___onDeactivateObjectTransform_23 = value;
		Il2CppCodeGenWriteBarrier(&___onDeactivateObjectTransform_23, value);
	}

	inline static int32_t get_offset_of_activateObjectTime_24() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___activateObjectTime_24)); }
	inline float get_activateObjectTime_24() const { return ___activateObjectTime_24; }
	inline float* get_address_of_activateObjectTime_24() { return &___activateObjectTime_24; }
	inline void set_activateObjectTime_24(float value)
	{
		___activateObjectTime_24 = value;
	}

	inline static int32_t get_offset_of_deactivateObjectTime_25() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___deactivateObjectTime_25)); }
	inline float get_deactivateObjectTime_25() const { return ___deactivateObjectTime_25; }
	inline float* get_address_of_deactivateObjectTime_25() { return &___deactivateObjectTime_25; }
	inline void set_deactivateObjectTime_25(float value)
	{
		___deactivateObjectTime_25 = value;
	}

	inline static int32_t get_offset_of_pointerAudioSource_26() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointerAudioSource_26)); }
	inline AudioSource_t1135106623 * get_pointerAudioSource_26() const { return ___pointerAudioSource_26; }
	inline AudioSource_t1135106623 ** get_address_of_pointerAudioSource_26() { return &___pointerAudioSource_26; }
	inline void set_pointerAudioSource_26(AudioSource_t1135106623 * value)
	{
		___pointerAudioSource_26 = value;
		Il2CppCodeGenWriteBarrier(&___pointerAudioSource_26, value);
	}

	inline static int32_t get_offset_of_loopingAudioSource_27() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___loopingAudioSource_27)); }
	inline AudioSource_t1135106623 * get_loopingAudioSource_27() const { return ___loopingAudioSource_27; }
	inline AudioSource_t1135106623 ** get_address_of_loopingAudioSource_27() { return &___loopingAudioSource_27; }
	inline void set_loopingAudioSource_27(AudioSource_t1135106623 * value)
	{
		___loopingAudioSource_27 = value;
		Il2CppCodeGenWriteBarrier(&___loopingAudioSource_27, value);
	}

	inline static int32_t get_offset_of_headAudioSource_28() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___headAudioSource_28)); }
	inline AudioSource_t1135106623 * get_headAudioSource_28() const { return ___headAudioSource_28; }
	inline AudioSource_t1135106623 ** get_address_of_headAudioSource_28() { return &___headAudioSource_28; }
	inline void set_headAudioSource_28(AudioSource_t1135106623 * value)
	{
		___headAudioSource_28 = value;
		Il2CppCodeGenWriteBarrier(&___headAudioSource_28, value);
	}

	inline static int32_t get_offset_of_reticleAudioSource_29() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___reticleAudioSource_29)); }
	inline AudioSource_t1135106623 * get_reticleAudioSource_29() const { return ___reticleAudioSource_29; }
	inline AudioSource_t1135106623 ** get_address_of_reticleAudioSource_29() { return &___reticleAudioSource_29; }
	inline void set_reticleAudioSource_29(AudioSource_t1135106623 * value)
	{
		___reticleAudioSource_29 = value;
		Il2CppCodeGenWriteBarrier(&___reticleAudioSource_29, value);
	}

	inline static int32_t get_offset_of_teleportSound_30() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___teleportSound_30)); }
	inline AudioClip_t1932558630 * get_teleportSound_30() const { return ___teleportSound_30; }
	inline AudioClip_t1932558630 ** get_address_of_teleportSound_30() { return &___teleportSound_30; }
	inline void set_teleportSound_30(AudioClip_t1932558630 * value)
	{
		___teleportSound_30 = value;
		Il2CppCodeGenWriteBarrier(&___teleportSound_30, value);
	}

	inline static int32_t get_offset_of_pointerStartSound_31() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointerStartSound_31)); }
	inline AudioClip_t1932558630 * get_pointerStartSound_31() const { return ___pointerStartSound_31; }
	inline AudioClip_t1932558630 ** get_address_of_pointerStartSound_31() { return &___pointerStartSound_31; }
	inline void set_pointerStartSound_31(AudioClip_t1932558630 * value)
	{
		___pointerStartSound_31 = value;
		Il2CppCodeGenWriteBarrier(&___pointerStartSound_31, value);
	}

	inline static int32_t get_offset_of_pointerLoopSound_32() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointerLoopSound_32)); }
	inline AudioClip_t1932558630 * get_pointerLoopSound_32() const { return ___pointerLoopSound_32; }
	inline AudioClip_t1932558630 ** get_address_of_pointerLoopSound_32() { return &___pointerLoopSound_32; }
	inline void set_pointerLoopSound_32(AudioClip_t1932558630 * value)
	{
		___pointerLoopSound_32 = value;
		Il2CppCodeGenWriteBarrier(&___pointerLoopSound_32, value);
	}

	inline static int32_t get_offset_of_pointerStopSound_33() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointerStopSound_33)); }
	inline AudioClip_t1932558630 * get_pointerStopSound_33() const { return ___pointerStopSound_33; }
	inline AudioClip_t1932558630 ** get_address_of_pointerStopSound_33() { return &___pointerStopSound_33; }
	inline void set_pointerStopSound_33(AudioClip_t1932558630 * value)
	{
		___pointerStopSound_33 = value;
		Il2CppCodeGenWriteBarrier(&___pointerStopSound_33, value);
	}

	inline static int32_t get_offset_of_goodHighlightSound_34() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___goodHighlightSound_34)); }
	inline AudioClip_t1932558630 * get_goodHighlightSound_34() const { return ___goodHighlightSound_34; }
	inline AudioClip_t1932558630 ** get_address_of_goodHighlightSound_34() { return &___goodHighlightSound_34; }
	inline void set_goodHighlightSound_34(AudioClip_t1932558630 * value)
	{
		___goodHighlightSound_34 = value;
		Il2CppCodeGenWriteBarrier(&___goodHighlightSound_34, value);
	}

	inline static int32_t get_offset_of_badHighlightSound_35() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___badHighlightSound_35)); }
	inline AudioClip_t1932558630 * get_badHighlightSound_35() const { return ___badHighlightSound_35; }
	inline AudioClip_t1932558630 ** get_address_of_badHighlightSound_35() { return &___badHighlightSound_35; }
	inline void set_badHighlightSound_35(AudioClip_t1932558630 * value)
	{
		___badHighlightSound_35 = value;
		Il2CppCodeGenWriteBarrier(&___badHighlightSound_35, value);
	}

	inline static int32_t get_offset_of_debugFloor_36() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___debugFloor_36)); }
	inline bool get_debugFloor_36() const { return ___debugFloor_36; }
	inline bool* get_address_of_debugFloor_36() { return &___debugFloor_36; }
	inline void set_debugFloor_36(bool value)
	{
		___debugFloor_36 = value;
	}

	inline static int32_t get_offset_of_showOffsetReticle_37() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___showOffsetReticle_37)); }
	inline bool get_showOffsetReticle_37() const { return ___showOffsetReticle_37; }
	inline bool* get_address_of_showOffsetReticle_37() { return &___showOffsetReticle_37; }
	inline void set_showOffsetReticle_37(bool value)
	{
		___showOffsetReticle_37 = value;
	}

	inline static int32_t get_offset_of_offsetReticleTransform_38() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___offsetReticleTransform_38)); }
	inline Transform_t3275118058 * get_offsetReticleTransform_38() const { return ___offsetReticleTransform_38; }
	inline Transform_t3275118058 ** get_address_of_offsetReticleTransform_38() { return &___offsetReticleTransform_38; }
	inline void set_offsetReticleTransform_38(Transform_t3275118058 * value)
	{
		___offsetReticleTransform_38 = value;
		Il2CppCodeGenWriteBarrier(&___offsetReticleTransform_38, value);
	}

	inline static int32_t get_offset_of_floorDebugSphere_39() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___floorDebugSphere_39)); }
	inline MeshRenderer_t1268241104 * get_floorDebugSphere_39() const { return ___floorDebugSphere_39; }
	inline MeshRenderer_t1268241104 ** get_address_of_floorDebugSphere_39() { return &___floorDebugSphere_39; }
	inline void set_floorDebugSphere_39(MeshRenderer_t1268241104 * value)
	{
		___floorDebugSphere_39 = value;
		Il2CppCodeGenWriteBarrier(&___floorDebugSphere_39, value);
	}

	inline static int32_t get_offset_of_floorDebugLine_40() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___floorDebugLine_40)); }
	inline LineRenderer_t849157671 * get_floorDebugLine_40() const { return ___floorDebugLine_40; }
	inline LineRenderer_t849157671 ** get_address_of_floorDebugLine_40() { return &___floorDebugLine_40; }
	inline void set_floorDebugLine_40(LineRenderer_t849157671 * value)
	{
		___floorDebugLine_40 = value;
		Il2CppCodeGenWriteBarrier(&___floorDebugLine_40, value);
	}

	inline static int32_t get_offset_of_pointerLineRenderer_41() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointerLineRenderer_41)); }
	inline LineRenderer_t849157671 * get_pointerLineRenderer_41() const { return ___pointerLineRenderer_41; }
	inline LineRenderer_t849157671 ** get_address_of_pointerLineRenderer_41() { return &___pointerLineRenderer_41; }
	inline void set_pointerLineRenderer_41(LineRenderer_t849157671 * value)
	{
		___pointerLineRenderer_41 = value;
		Il2CppCodeGenWriteBarrier(&___pointerLineRenderer_41, value);
	}

	inline static int32_t get_offset_of_teleportPointerObject_42() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___teleportPointerObject_42)); }
	inline GameObject_t1756533147 * get_teleportPointerObject_42() const { return ___teleportPointerObject_42; }
	inline GameObject_t1756533147 ** get_address_of_teleportPointerObject_42() { return &___teleportPointerObject_42; }
	inline void set_teleportPointerObject_42(GameObject_t1756533147 * value)
	{
		___teleportPointerObject_42 = value;
		Il2CppCodeGenWriteBarrier(&___teleportPointerObject_42, value);
	}

	inline static int32_t get_offset_of_pointerStartTransform_43() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointerStartTransform_43)); }
	inline Transform_t3275118058 * get_pointerStartTransform_43() const { return ___pointerStartTransform_43; }
	inline Transform_t3275118058 ** get_address_of_pointerStartTransform_43() { return &___pointerStartTransform_43; }
	inline void set_pointerStartTransform_43(Transform_t3275118058 * value)
	{
		___pointerStartTransform_43 = value;
		Il2CppCodeGenWriteBarrier(&___pointerStartTransform_43, value);
	}

	inline static int32_t get_offset_of_pointerHand_44() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointerHand_44)); }
	inline Hand_t379716353 * get_pointerHand_44() const { return ___pointerHand_44; }
	inline Hand_t379716353 ** get_address_of_pointerHand_44() { return &___pointerHand_44; }
	inline void set_pointerHand_44(Hand_t379716353 * value)
	{
		___pointerHand_44 = value;
		Il2CppCodeGenWriteBarrier(&___pointerHand_44, value);
	}

	inline static int32_t get_offset_of_player_45() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___player_45)); }
	inline Player_t4256718089 * get_player_45() const { return ___player_45; }
	inline Player_t4256718089 ** get_address_of_player_45() { return &___player_45; }
	inline void set_player_45(Player_t4256718089 * value)
	{
		___player_45 = value;
		Il2CppCodeGenWriteBarrier(&___player_45, value);
	}

	inline static int32_t get_offset_of_teleportArc_46() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___teleportArc_46)); }
	inline TeleportArc_t2123987351 * get_teleportArc_46() const { return ___teleportArc_46; }
	inline TeleportArc_t2123987351 ** get_address_of_teleportArc_46() { return &___teleportArc_46; }
	inline void set_teleportArc_46(TeleportArc_t2123987351 * value)
	{
		___teleportArc_46 = value;
		Il2CppCodeGenWriteBarrier(&___teleportArc_46, value);
	}

	inline static int32_t get_offset_of_visible_47() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___visible_47)); }
	inline bool get_visible_47() const { return ___visible_47; }
	inline bool* get_address_of_visible_47() { return &___visible_47; }
	inline void set_visible_47(bool value)
	{
		___visible_47 = value;
	}

	inline static int32_t get_offset_of_teleportMarkers_48() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___teleportMarkers_48)); }
	inline TeleportMarkerBaseU5BU5D_t3589211913* get_teleportMarkers_48() const { return ___teleportMarkers_48; }
	inline TeleportMarkerBaseU5BU5D_t3589211913** get_address_of_teleportMarkers_48() { return &___teleportMarkers_48; }
	inline void set_teleportMarkers_48(TeleportMarkerBaseU5BU5D_t3589211913* value)
	{
		___teleportMarkers_48 = value;
		Il2CppCodeGenWriteBarrier(&___teleportMarkers_48, value);
	}

	inline static int32_t get_offset_of_pointedAtTeleportMarker_49() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointedAtTeleportMarker_49)); }
	inline TeleportMarkerBase_t1112706968 * get_pointedAtTeleportMarker_49() const { return ___pointedAtTeleportMarker_49; }
	inline TeleportMarkerBase_t1112706968 ** get_address_of_pointedAtTeleportMarker_49() { return &___pointedAtTeleportMarker_49; }
	inline void set_pointedAtTeleportMarker_49(TeleportMarkerBase_t1112706968 * value)
	{
		___pointedAtTeleportMarker_49 = value;
		Il2CppCodeGenWriteBarrier(&___pointedAtTeleportMarker_49, value);
	}

	inline static int32_t get_offset_of_teleportingToMarker_50() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___teleportingToMarker_50)); }
	inline TeleportMarkerBase_t1112706968 * get_teleportingToMarker_50() const { return ___teleportingToMarker_50; }
	inline TeleportMarkerBase_t1112706968 ** get_address_of_teleportingToMarker_50() { return &___teleportingToMarker_50; }
	inline void set_teleportingToMarker_50(TeleportMarkerBase_t1112706968 * value)
	{
		___teleportingToMarker_50 = value;
		Il2CppCodeGenWriteBarrier(&___teleportingToMarker_50, value);
	}

	inline static int32_t get_offset_of_pointedAtPosition_51() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointedAtPosition_51)); }
	inline Vector3_t2243707580  get_pointedAtPosition_51() const { return ___pointedAtPosition_51; }
	inline Vector3_t2243707580 * get_address_of_pointedAtPosition_51() { return &___pointedAtPosition_51; }
	inline void set_pointedAtPosition_51(Vector3_t2243707580  value)
	{
		___pointedAtPosition_51 = value;
	}

	inline static int32_t get_offset_of_prevPointedAtPosition_52() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___prevPointedAtPosition_52)); }
	inline Vector3_t2243707580  get_prevPointedAtPosition_52() const { return ___prevPointedAtPosition_52; }
	inline Vector3_t2243707580 * get_address_of_prevPointedAtPosition_52() { return &___prevPointedAtPosition_52; }
	inline void set_prevPointedAtPosition_52(Vector3_t2243707580  value)
	{
		___prevPointedAtPosition_52 = value;
	}

	inline static int32_t get_offset_of_teleporting_53() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___teleporting_53)); }
	inline bool get_teleporting_53() const { return ___teleporting_53; }
	inline bool* get_address_of_teleporting_53() { return &___teleporting_53; }
	inline void set_teleporting_53(bool value)
	{
		___teleporting_53 = value;
	}

	inline static int32_t get_offset_of_currentFadeTime_54() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___currentFadeTime_54)); }
	inline float get_currentFadeTime_54() const { return ___currentFadeTime_54; }
	inline float* get_address_of_currentFadeTime_54() { return &___currentFadeTime_54; }
	inline void set_currentFadeTime_54(float value)
	{
		___currentFadeTime_54 = value;
	}

	inline static int32_t get_offset_of_meshAlphaPercent_55() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___meshAlphaPercent_55)); }
	inline float get_meshAlphaPercent_55() const { return ___meshAlphaPercent_55; }
	inline float* get_address_of_meshAlphaPercent_55() { return &___meshAlphaPercent_55; }
	inline void set_meshAlphaPercent_55(float value)
	{
		___meshAlphaPercent_55 = value;
	}

	inline static int32_t get_offset_of_pointerShowStartTime_56() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointerShowStartTime_56)); }
	inline float get_pointerShowStartTime_56() const { return ___pointerShowStartTime_56; }
	inline float* get_address_of_pointerShowStartTime_56() { return &___pointerShowStartTime_56; }
	inline void set_pointerShowStartTime_56(float value)
	{
		___pointerShowStartTime_56 = value;
	}

	inline static int32_t get_offset_of_pointerHideStartTime_57() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___pointerHideStartTime_57)); }
	inline float get_pointerHideStartTime_57() const { return ___pointerHideStartTime_57; }
	inline float* get_address_of_pointerHideStartTime_57() { return &___pointerHideStartTime_57; }
	inline void set_pointerHideStartTime_57(float value)
	{
		___pointerHideStartTime_57 = value;
	}

	inline static int32_t get_offset_of_meshFading_58() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___meshFading_58)); }
	inline bool get_meshFading_58() const { return ___meshFading_58; }
	inline bool* get_address_of_meshFading_58() { return &___meshFading_58; }
	inline void set_meshFading_58(bool value)
	{
		___meshFading_58 = value;
	}

	inline static int32_t get_offset_of_fullTintAlpha_59() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___fullTintAlpha_59)); }
	inline float get_fullTintAlpha_59() const { return ___fullTintAlpha_59; }
	inline float* get_address_of_fullTintAlpha_59() { return &___fullTintAlpha_59; }
	inline void set_fullTintAlpha_59(float value)
	{
		___fullTintAlpha_59 = value;
	}

	inline static int32_t get_offset_of_invalidReticleMinScale_60() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___invalidReticleMinScale_60)); }
	inline float get_invalidReticleMinScale_60() const { return ___invalidReticleMinScale_60; }
	inline float* get_address_of_invalidReticleMinScale_60() { return &___invalidReticleMinScale_60; }
	inline void set_invalidReticleMinScale_60(float value)
	{
		___invalidReticleMinScale_60 = value;
	}

	inline static int32_t get_offset_of_invalidReticleMaxScale_61() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___invalidReticleMaxScale_61)); }
	inline float get_invalidReticleMaxScale_61() const { return ___invalidReticleMaxScale_61; }
	inline float* get_address_of_invalidReticleMaxScale_61() { return &___invalidReticleMaxScale_61; }
	inline void set_invalidReticleMaxScale_61(float value)
	{
		___invalidReticleMaxScale_61 = value;
	}

	inline static int32_t get_offset_of_invalidReticleMinScaleDistance_62() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___invalidReticleMinScaleDistance_62)); }
	inline float get_invalidReticleMinScaleDistance_62() const { return ___invalidReticleMinScaleDistance_62; }
	inline float* get_address_of_invalidReticleMinScaleDistance_62() { return &___invalidReticleMinScaleDistance_62; }
	inline void set_invalidReticleMinScaleDistance_62(float value)
	{
		___invalidReticleMinScaleDistance_62 = value;
	}

	inline static int32_t get_offset_of_invalidReticleMaxScaleDistance_63() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___invalidReticleMaxScaleDistance_63)); }
	inline float get_invalidReticleMaxScaleDistance_63() const { return ___invalidReticleMaxScaleDistance_63; }
	inline float* get_address_of_invalidReticleMaxScaleDistance_63() { return &___invalidReticleMaxScaleDistance_63; }
	inline void set_invalidReticleMaxScaleDistance_63(float value)
	{
		___invalidReticleMaxScaleDistance_63 = value;
	}

	inline static int32_t get_offset_of_invalidReticleScale_64() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___invalidReticleScale_64)); }
	inline Vector3_t2243707580  get_invalidReticleScale_64() const { return ___invalidReticleScale_64; }
	inline Vector3_t2243707580 * get_address_of_invalidReticleScale_64() { return &___invalidReticleScale_64; }
	inline void set_invalidReticleScale_64(Vector3_t2243707580  value)
	{
		___invalidReticleScale_64 = value;
	}

	inline static int32_t get_offset_of_invalidReticleTargetRotation_65() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___invalidReticleTargetRotation_65)); }
	inline Quaternion_t4030073918  get_invalidReticleTargetRotation_65() const { return ___invalidReticleTargetRotation_65; }
	inline Quaternion_t4030073918 * get_address_of_invalidReticleTargetRotation_65() { return &___invalidReticleTargetRotation_65; }
	inline void set_invalidReticleTargetRotation_65(Quaternion_t4030073918  value)
	{
		___invalidReticleTargetRotation_65 = value;
	}

	inline static int32_t get_offset_of_playAreaPreviewTransform_66() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___playAreaPreviewTransform_66)); }
	inline Transform_t3275118058 * get_playAreaPreviewTransform_66() const { return ___playAreaPreviewTransform_66; }
	inline Transform_t3275118058 ** get_address_of_playAreaPreviewTransform_66() { return &___playAreaPreviewTransform_66; }
	inline void set_playAreaPreviewTransform_66(Transform_t3275118058 * value)
	{
		___playAreaPreviewTransform_66 = value;
		Il2CppCodeGenWriteBarrier(&___playAreaPreviewTransform_66, value);
	}

	inline static int32_t get_offset_of_playAreaPreviewCorners_67() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___playAreaPreviewCorners_67)); }
	inline TransformU5BU5D_t3764228911* get_playAreaPreviewCorners_67() const { return ___playAreaPreviewCorners_67; }
	inline TransformU5BU5D_t3764228911** get_address_of_playAreaPreviewCorners_67() { return &___playAreaPreviewCorners_67; }
	inline void set_playAreaPreviewCorners_67(TransformU5BU5D_t3764228911* value)
	{
		___playAreaPreviewCorners_67 = value;
		Il2CppCodeGenWriteBarrier(&___playAreaPreviewCorners_67, value);
	}

	inline static int32_t get_offset_of_playAreaPreviewSides_68() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___playAreaPreviewSides_68)); }
	inline TransformU5BU5D_t3764228911* get_playAreaPreviewSides_68() const { return ___playAreaPreviewSides_68; }
	inline TransformU5BU5D_t3764228911** get_address_of_playAreaPreviewSides_68() { return &___playAreaPreviewSides_68; }
	inline void set_playAreaPreviewSides_68(TransformU5BU5D_t3764228911* value)
	{
		___playAreaPreviewSides_68 = value;
		Il2CppCodeGenWriteBarrier(&___playAreaPreviewSides_68, value);
	}

	inline static int32_t get_offset_of_loopingAudioMaxVolume_69() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___loopingAudioMaxVolume_69)); }
	inline float get_loopingAudioMaxVolume_69() const { return ___loopingAudioMaxVolume_69; }
	inline float* get_address_of_loopingAudioMaxVolume_69() { return &___loopingAudioMaxVolume_69; }
	inline void set_loopingAudioMaxVolume_69(float value)
	{
		___loopingAudioMaxVolume_69 = value;
	}

	inline static int32_t get_offset_of_hintCoroutine_70() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___hintCoroutine_70)); }
	inline Coroutine_t2299508840 * get_hintCoroutine_70() const { return ___hintCoroutine_70; }
	inline Coroutine_t2299508840 ** get_address_of_hintCoroutine_70() { return &___hintCoroutine_70; }
	inline void set_hintCoroutine_70(Coroutine_t2299508840 * value)
	{
		___hintCoroutine_70 = value;
		Il2CppCodeGenWriteBarrier(&___hintCoroutine_70, value);
	}

	inline static int32_t get_offset_of_originalHoverLockState_71() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___originalHoverLockState_71)); }
	inline bool get_originalHoverLockState_71() const { return ___originalHoverLockState_71; }
	inline bool* get_address_of_originalHoverLockState_71() { return &___originalHoverLockState_71; }
	inline void set_originalHoverLockState_71(bool value)
	{
		___originalHoverLockState_71 = value;
	}

	inline static int32_t get_offset_of_originalHoveringInteractable_72() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___originalHoveringInteractable_72)); }
	inline Interactable_t1274046986 * get_originalHoveringInteractable_72() const { return ___originalHoveringInteractable_72; }
	inline Interactable_t1274046986 ** get_address_of_originalHoveringInteractable_72() { return &___originalHoveringInteractable_72; }
	inline void set_originalHoveringInteractable_72(Interactable_t1274046986 * value)
	{
		___originalHoveringInteractable_72 = value;
		Il2CppCodeGenWriteBarrier(&___originalHoveringInteractable_72, value);
	}

	inline static int32_t get_offset_of_allowTeleportWhileAttached_73() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___allowTeleportWhileAttached_73)); }
	inline AllowTeleportWhileAttachedToHand_t416616227 * get_allowTeleportWhileAttached_73() const { return ___allowTeleportWhileAttached_73; }
	inline AllowTeleportWhileAttachedToHand_t416616227 ** get_address_of_allowTeleportWhileAttached_73() { return &___allowTeleportWhileAttached_73; }
	inline void set_allowTeleportWhileAttached_73(AllowTeleportWhileAttachedToHand_t416616227 * value)
	{
		___allowTeleportWhileAttached_73 = value;
		Il2CppCodeGenWriteBarrier(&___allowTeleportWhileAttached_73, value);
	}

	inline static int32_t get_offset_of_startingFeetOffset_74() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___startingFeetOffset_74)); }
	inline Vector3_t2243707580  get_startingFeetOffset_74() const { return ___startingFeetOffset_74; }
	inline Vector3_t2243707580 * get_address_of_startingFeetOffset_74() { return &___startingFeetOffset_74; }
	inline void set_startingFeetOffset_74(Vector3_t2243707580  value)
	{
		___startingFeetOffset_74 = value;
	}

	inline static int32_t get_offset_of_movedFeetFarEnough_75() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___movedFeetFarEnough_75)); }
	inline bool get_movedFeetFarEnough_75() const { return ___movedFeetFarEnough_75; }
	inline bool* get_address_of_movedFeetFarEnough_75() { return &___movedFeetFarEnough_75; }
	inline void set_movedFeetFarEnough_75(bool value)
	{
		___movedFeetFarEnough_75 = value;
	}

	inline static int32_t get_offset_of_chaperoneInfoInitializedAction_76() { return static_cast<int32_t>(offsetof(Teleport_t865564691, ___chaperoneInfoInitializedAction_76)); }
	inline Action_t1836998693 * get_chaperoneInfoInitializedAction_76() const { return ___chaperoneInfoInitializedAction_76; }
	inline Action_t1836998693 ** get_address_of_chaperoneInfoInitializedAction_76() { return &___chaperoneInfoInitializedAction_76; }
	inline void set_chaperoneInfoInitializedAction_76(Action_t1836998693 * value)
	{
		___chaperoneInfoInitializedAction_76 = value;
		Il2CppCodeGenWriteBarrier(&___chaperoneInfoInitializedAction_76, value);
	}
};

struct Teleport_t865564691_StaticFields
{
public:
	// SteamVR_Events/Event`1<System.Single> Valve.VR.InteractionSystem.Teleport::ChangeScene
	Event_1_t4251932349 * ___ChangeScene_77;
	// SteamVR_Events/Event`1<Valve.VR.InteractionSystem.TeleportMarkerBase> Valve.VR.InteractionSystem.Teleport::Player
	Event_1_t3288129385 * ___Player_78;
	// SteamVR_Events/Event`1<Valve.VR.InteractionSystem.TeleportMarkerBase> Valve.VR.InteractionSystem.Teleport::PlayerPre
	Event_1_t3288129385 * ___PlayerPre_79;
	// Valve.VR.InteractionSystem.Teleport Valve.VR.InteractionSystem.Teleport::_instance
	Teleport_t865564691 * ____instance_80;

public:
	inline static int32_t get_offset_of_ChangeScene_77() { return static_cast<int32_t>(offsetof(Teleport_t865564691_StaticFields, ___ChangeScene_77)); }
	inline Event_1_t4251932349 * get_ChangeScene_77() const { return ___ChangeScene_77; }
	inline Event_1_t4251932349 ** get_address_of_ChangeScene_77() { return &___ChangeScene_77; }
	inline void set_ChangeScene_77(Event_1_t4251932349 * value)
	{
		___ChangeScene_77 = value;
		Il2CppCodeGenWriteBarrier(&___ChangeScene_77, value);
	}

	inline static int32_t get_offset_of_Player_78() { return static_cast<int32_t>(offsetof(Teleport_t865564691_StaticFields, ___Player_78)); }
	inline Event_1_t3288129385 * get_Player_78() const { return ___Player_78; }
	inline Event_1_t3288129385 ** get_address_of_Player_78() { return &___Player_78; }
	inline void set_Player_78(Event_1_t3288129385 * value)
	{
		___Player_78 = value;
		Il2CppCodeGenWriteBarrier(&___Player_78, value);
	}

	inline static int32_t get_offset_of_PlayerPre_79() { return static_cast<int32_t>(offsetof(Teleport_t865564691_StaticFields, ___PlayerPre_79)); }
	inline Event_1_t3288129385 * get_PlayerPre_79() const { return ___PlayerPre_79; }
	inline Event_1_t3288129385 ** get_address_of_PlayerPre_79() { return &___PlayerPre_79; }
	inline void set_PlayerPre_79(Event_1_t3288129385 * value)
	{
		___PlayerPre_79 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerPre_79, value);
	}

	inline static int32_t get_offset_of__instance_80() { return static_cast<int32_t>(offsetof(Teleport_t865564691_StaticFields, ____instance_80)); }
	inline Teleport_t865564691 * get__instance_80() const { return ____instance_80; }
	inline Teleport_t865564691 ** get_address_of__instance_80() { return &____instance_80; }
	inline void set__instance_80(Teleport_t865564691 * value)
	{
		____instance_80 = value;
		Il2CppCodeGenWriteBarrier(&____instance_80, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
