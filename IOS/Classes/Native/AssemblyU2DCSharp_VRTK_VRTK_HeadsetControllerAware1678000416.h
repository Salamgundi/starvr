﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.HeadsetControllerAwareEventHandler
struct HeadsetControllerAwareEventHandler_t1106036588;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_HeadsetControllerAware
struct  VRTK_HeadsetControllerAware_t1678000416  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.VRTK_HeadsetControllerAware::trackLeftController
	bool ___trackLeftController_2;
	// System.Boolean VRTK.VRTK_HeadsetControllerAware::trackRightController
	bool ___trackRightController_3;
	// System.Single VRTK.VRTK_HeadsetControllerAware::controllerGlanceRadius
	float ___controllerGlanceRadius_4;
	// UnityEngine.Transform VRTK.VRTK_HeadsetControllerAware::customRightControllerOrigin
	Transform_t3275118058 * ___customRightControllerOrigin_5;
	// UnityEngine.Transform VRTK.VRTK_HeadsetControllerAware::customLeftControllerOrigin
	Transform_t3275118058 * ___customLeftControllerOrigin_6;
	// VRTK.HeadsetControllerAwareEventHandler VRTK.VRTK_HeadsetControllerAware::ControllerObscured
	HeadsetControllerAwareEventHandler_t1106036588 * ___ControllerObscured_7;
	// VRTK.HeadsetControllerAwareEventHandler VRTK.VRTK_HeadsetControllerAware::ControllerUnobscured
	HeadsetControllerAwareEventHandler_t1106036588 * ___ControllerUnobscured_8;
	// VRTK.HeadsetControllerAwareEventHandler VRTK.VRTK_HeadsetControllerAware::ControllerGlanceEnter
	HeadsetControllerAwareEventHandler_t1106036588 * ___ControllerGlanceEnter_9;
	// VRTK.HeadsetControllerAwareEventHandler VRTK.VRTK_HeadsetControllerAware::ControllerGlanceExit
	HeadsetControllerAwareEventHandler_t1106036588 * ___ControllerGlanceExit_10;
	// UnityEngine.GameObject VRTK.VRTK_HeadsetControllerAware::leftController
	GameObject_t1756533147 * ___leftController_11;
	// UnityEngine.GameObject VRTK.VRTK_HeadsetControllerAware::rightController
	GameObject_t1756533147 * ___rightController_12;
	// UnityEngine.Transform VRTK.VRTK_HeadsetControllerAware::headset
	Transform_t3275118058 * ___headset_13;
	// System.Boolean VRTK.VRTK_HeadsetControllerAware::leftControllerObscured
	bool ___leftControllerObscured_14;
	// System.Boolean VRTK.VRTK_HeadsetControllerAware::rightControllerObscured
	bool ___rightControllerObscured_15;
	// System.Boolean VRTK.VRTK_HeadsetControllerAware::leftControllerLastState
	bool ___leftControllerLastState_16;
	// System.Boolean VRTK.VRTK_HeadsetControllerAware::rightControllerLastState
	bool ___rightControllerLastState_17;
	// System.Boolean VRTK.VRTK_HeadsetControllerAware::leftControllerGlance
	bool ___leftControllerGlance_18;
	// System.Boolean VRTK.VRTK_HeadsetControllerAware::rightControllerGlance
	bool ___rightControllerGlance_19;
	// System.Boolean VRTK.VRTK_HeadsetControllerAware::leftControllerGlanceLastState
	bool ___leftControllerGlanceLastState_20;
	// System.Boolean VRTK.VRTK_HeadsetControllerAware::rightControllerGlanceLastState
	bool ___rightControllerGlanceLastState_21;

public:
	inline static int32_t get_offset_of_trackLeftController_2() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___trackLeftController_2)); }
	inline bool get_trackLeftController_2() const { return ___trackLeftController_2; }
	inline bool* get_address_of_trackLeftController_2() { return &___trackLeftController_2; }
	inline void set_trackLeftController_2(bool value)
	{
		___trackLeftController_2 = value;
	}

	inline static int32_t get_offset_of_trackRightController_3() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___trackRightController_3)); }
	inline bool get_trackRightController_3() const { return ___trackRightController_3; }
	inline bool* get_address_of_trackRightController_3() { return &___trackRightController_3; }
	inline void set_trackRightController_3(bool value)
	{
		___trackRightController_3 = value;
	}

	inline static int32_t get_offset_of_controllerGlanceRadius_4() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___controllerGlanceRadius_4)); }
	inline float get_controllerGlanceRadius_4() const { return ___controllerGlanceRadius_4; }
	inline float* get_address_of_controllerGlanceRadius_4() { return &___controllerGlanceRadius_4; }
	inline void set_controllerGlanceRadius_4(float value)
	{
		___controllerGlanceRadius_4 = value;
	}

	inline static int32_t get_offset_of_customRightControllerOrigin_5() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___customRightControllerOrigin_5)); }
	inline Transform_t3275118058 * get_customRightControllerOrigin_5() const { return ___customRightControllerOrigin_5; }
	inline Transform_t3275118058 ** get_address_of_customRightControllerOrigin_5() { return &___customRightControllerOrigin_5; }
	inline void set_customRightControllerOrigin_5(Transform_t3275118058 * value)
	{
		___customRightControllerOrigin_5 = value;
		Il2CppCodeGenWriteBarrier(&___customRightControllerOrigin_5, value);
	}

	inline static int32_t get_offset_of_customLeftControllerOrigin_6() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___customLeftControllerOrigin_6)); }
	inline Transform_t3275118058 * get_customLeftControllerOrigin_6() const { return ___customLeftControllerOrigin_6; }
	inline Transform_t3275118058 ** get_address_of_customLeftControllerOrigin_6() { return &___customLeftControllerOrigin_6; }
	inline void set_customLeftControllerOrigin_6(Transform_t3275118058 * value)
	{
		___customLeftControllerOrigin_6 = value;
		Il2CppCodeGenWriteBarrier(&___customLeftControllerOrigin_6, value);
	}

	inline static int32_t get_offset_of_ControllerObscured_7() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___ControllerObscured_7)); }
	inline HeadsetControllerAwareEventHandler_t1106036588 * get_ControllerObscured_7() const { return ___ControllerObscured_7; }
	inline HeadsetControllerAwareEventHandler_t1106036588 ** get_address_of_ControllerObscured_7() { return &___ControllerObscured_7; }
	inline void set_ControllerObscured_7(HeadsetControllerAwareEventHandler_t1106036588 * value)
	{
		___ControllerObscured_7 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerObscured_7, value);
	}

	inline static int32_t get_offset_of_ControllerUnobscured_8() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___ControllerUnobscured_8)); }
	inline HeadsetControllerAwareEventHandler_t1106036588 * get_ControllerUnobscured_8() const { return ___ControllerUnobscured_8; }
	inline HeadsetControllerAwareEventHandler_t1106036588 ** get_address_of_ControllerUnobscured_8() { return &___ControllerUnobscured_8; }
	inline void set_ControllerUnobscured_8(HeadsetControllerAwareEventHandler_t1106036588 * value)
	{
		___ControllerUnobscured_8 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerUnobscured_8, value);
	}

	inline static int32_t get_offset_of_ControllerGlanceEnter_9() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___ControllerGlanceEnter_9)); }
	inline HeadsetControllerAwareEventHandler_t1106036588 * get_ControllerGlanceEnter_9() const { return ___ControllerGlanceEnter_9; }
	inline HeadsetControllerAwareEventHandler_t1106036588 ** get_address_of_ControllerGlanceEnter_9() { return &___ControllerGlanceEnter_9; }
	inline void set_ControllerGlanceEnter_9(HeadsetControllerAwareEventHandler_t1106036588 * value)
	{
		___ControllerGlanceEnter_9 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerGlanceEnter_9, value);
	}

	inline static int32_t get_offset_of_ControllerGlanceExit_10() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___ControllerGlanceExit_10)); }
	inline HeadsetControllerAwareEventHandler_t1106036588 * get_ControllerGlanceExit_10() const { return ___ControllerGlanceExit_10; }
	inline HeadsetControllerAwareEventHandler_t1106036588 ** get_address_of_ControllerGlanceExit_10() { return &___ControllerGlanceExit_10; }
	inline void set_ControllerGlanceExit_10(HeadsetControllerAwareEventHandler_t1106036588 * value)
	{
		___ControllerGlanceExit_10 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerGlanceExit_10, value);
	}

	inline static int32_t get_offset_of_leftController_11() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___leftController_11)); }
	inline GameObject_t1756533147 * get_leftController_11() const { return ___leftController_11; }
	inline GameObject_t1756533147 ** get_address_of_leftController_11() { return &___leftController_11; }
	inline void set_leftController_11(GameObject_t1756533147 * value)
	{
		___leftController_11 = value;
		Il2CppCodeGenWriteBarrier(&___leftController_11, value);
	}

	inline static int32_t get_offset_of_rightController_12() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___rightController_12)); }
	inline GameObject_t1756533147 * get_rightController_12() const { return ___rightController_12; }
	inline GameObject_t1756533147 ** get_address_of_rightController_12() { return &___rightController_12; }
	inline void set_rightController_12(GameObject_t1756533147 * value)
	{
		___rightController_12 = value;
		Il2CppCodeGenWriteBarrier(&___rightController_12, value);
	}

	inline static int32_t get_offset_of_headset_13() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___headset_13)); }
	inline Transform_t3275118058 * get_headset_13() const { return ___headset_13; }
	inline Transform_t3275118058 ** get_address_of_headset_13() { return &___headset_13; }
	inline void set_headset_13(Transform_t3275118058 * value)
	{
		___headset_13 = value;
		Il2CppCodeGenWriteBarrier(&___headset_13, value);
	}

	inline static int32_t get_offset_of_leftControllerObscured_14() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___leftControllerObscured_14)); }
	inline bool get_leftControllerObscured_14() const { return ___leftControllerObscured_14; }
	inline bool* get_address_of_leftControllerObscured_14() { return &___leftControllerObscured_14; }
	inline void set_leftControllerObscured_14(bool value)
	{
		___leftControllerObscured_14 = value;
	}

	inline static int32_t get_offset_of_rightControllerObscured_15() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___rightControllerObscured_15)); }
	inline bool get_rightControllerObscured_15() const { return ___rightControllerObscured_15; }
	inline bool* get_address_of_rightControllerObscured_15() { return &___rightControllerObscured_15; }
	inline void set_rightControllerObscured_15(bool value)
	{
		___rightControllerObscured_15 = value;
	}

	inline static int32_t get_offset_of_leftControllerLastState_16() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___leftControllerLastState_16)); }
	inline bool get_leftControllerLastState_16() const { return ___leftControllerLastState_16; }
	inline bool* get_address_of_leftControllerLastState_16() { return &___leftControllerLastState_16; }
	inline void set_leftControllerLastState_16(bool value)
	{
		___leftControllerLastState_16 = value;
	}

	inline static int32_t get_offset_of_rightControllerLastState_17() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___rightControllerLastState_17)); }
	inline bool get_rightControllerLastState_17() const { return ___rightControllerLastState_17; }
	inline bool* get_address_of_rightControllerLastState_17() { return &___rightControllerLastState_17; }
	inline void set_rightControllerLastState_17(bool value)
	{
		___rightControllerLastState_17 = value;
	}

	inline static int32_t get_offset_of_leftControllerGlance_18() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___leftControllerGlance_18)); }
	inline bool get_leftControllerGlance_18() const { return ___leftControllerGlance_18; }
	inline bool* get_address_of_leftControllerGlance_18() { return &___leftControllerGlance_18; }
	inline void set_leftControllerGlance_18(bool value)
	{
		___leftControllerGlance_18 = value;
	}

	inline static int32_t get_offset_of_rightControllerGlance_19() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___rightControllerGlance_19)); }
	inline bool get_rightControllerGlance_19() const { return ___rightControllerGlance_19; }
	inline bool* get_address_of_rightControllerGlance_19() { return &___rightControllerGlance_19; }
	inline void set_rightControllerGlance_19(bool value)
	{
		___rightControllerGlance_19 = value;
	}

	inline static int32_t get_offset_of_leftControllerGlanceLastState_20() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___leftControllerGlanceLastState_20)); }
	inline bool get_leftControllerGlanceLastState_20() const { return ___leftControllerGlanceLastState_20; }
	inline bool* get_address_of_leftControllerGlanceLastState_20() { return &___leftControllerGlanceLastState_20; }
	inline void set_leftControllerGlanceLastState_20(bool value)
	{
		___leftControllerGlanceLastState_20 = value;
	}

	inline static int32_t get_offset_of_rightControllerGlanceLastState_21() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_t1678000416, ___rightControllerGlanceLastState_21)); }
	inline bool get_rightControllerGlanceLastState_21() const { return ___rightControllerGlanceLastState_21; }
	inline bool* get_address_of_rightControllerGlanceLastState_21() { return &___rightControllerGlanceLastState_21; }
	inline void set_rightControllerGlanceLastState_21(bool value)
	{
		___rightControllerGlanceLastState_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
