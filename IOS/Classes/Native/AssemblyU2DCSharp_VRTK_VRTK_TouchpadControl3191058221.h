﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectControl724022372.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_TouchpadControl
struct  VRTK_TouchpadControl_t3191058221  : public VRTK_ObjectControl_t724022372
{
public:
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_TouchpadControl::primaryActivationButton
	int32_t ___primaryActivationButton_22;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_TouchpadControl::actionModifierButton
	int32_t ___actionModifierButton_23;
	// UnityEngine.Vector2 VRTK.VRTK_TouchpadControl::axisDeadzone
	Vector2_t2243707579  ___axisDeadzone_24;
	// System.Boolean VRTK.VRTK_TouchpadControl::touchpadFirstChange
	bool ___touchpadFirstChange_25;
	// System.Boolean VRTK.VRTK_TouchpadControl::otherTouchpadControlEnabledState
	bool ___otherTouchpadControlEnabledState_26;

public:
	inline static int32_t get_offset_of_primaryActivationButton_22() { return static_cast<int32_t>(offsetof(VRTK_TouchpadControl_t3191058221, ___primaryActivationButton_22)); }
	inline int32_t get_primaryActivationButton_22() const { return ___primaryActivationButton_22; }
	inline int32_t* get_address_of_primaryActivationButton_22() { return &___primaryActivationButton_22; }
	inline void set_primaryActivationButton_22(int32_t value)
	{
		___primaryActivationButton_22 = value;
	}

	inline static int32_t get_offset_of_actionModifierButton_23() { return static_cast<int32_t>(offsetof(VRTK_TouchpadControl_t3191058221, ___actionModifierButton_23)); }
	inline int32_t get_actionModifierButton_23() const { return ___actionModifierButton_23; }
	inline int32_t* get_address_of_actionModifierButton_23() { return &___actionModifierButton_23; }
	inline void set_actionModifierButton_23(int32_t value)
	{
		___actionModifierButton_23 = value;
	}

	inline static int32_t get_offset_of_axisDeadzone_24() { return static_cast<int32_t>(offsetof(VRTK_TouchpadControl_t3191058221, ___axisDeadzone_24)); }
	inline Vector2_t2243707579  get_axisDeadzone_24() const { return ___axisDeadzone_24; }
	inline Vector2_t2243707579 * get_address_of_axisDeadzone_24() { return &___axisDeadzone_24; }
	inline void set_axisDeadzone_24(Vector2_t2243707579  value)
	{
		___axisDeadzone_24 = value;
	}

	inline static int32_t get_offset_of_touchpadFirstChange_25() { return static_cast<int32_t>(offsetof(VRTK_TouchpadControl_t3191058221, ___touchpadFirstChange_25)); }
	inline bool get_touchpadFirstChange_25() const { return ___touchpadFirstChange_25; }
	inline bool* get_address_of_touchpadFirstChange_25() { return &___touchpadFirstChange_25; }
	inline void set_touchpadFirstChange_25(bool value)
	{
		___touchpadFirstChange_25 = value;
	}

	inline static int32_t get_offset_of_otherTouchpadControlEnabledState_26() { return static_cast<int32_t>(offsetof(VRTK_TouchpadControl_t3191058221, ___otherTouchpadControlEnabledState_26)); }
	inline bool get_otherTouchpadControlEnabledState_26() const { return ___otherTouchpadControlEnabledState_26; }
	inline bool* get_address_of_otherTouchpadControlEnabledState_26() { return &___otherTouchpadControlEnabledState_26; }
	inline void set_otherTouchpadControlEnabledState_26(bool value)
	{
		___otherTouchpadControlEnabledState_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
