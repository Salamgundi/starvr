template <typename T> void RegisterClass();
template <typename T> void RegisterStrippedTypeInfo(int, const char*, const char*);

void InvokeRegisterStaticallyLinkedModuleClasses()
{
	// Do nothing (we're in stripping mode)
}

void RegisterStaticallyLinkedModulesGranular()
{
	void RegisterModule_AI();
	RegisterModule_AI();

	void RegisterModule_Animation();
	RegisterModule_Animation();

	void RegisterModule_Audio();
	RegisterModule_Audio();

	void RegisterModule_CloudWebServices();
	RegisterModule_CloudWebServices();

	void RegisterModule_ParticleSystem();
	RegisterModule_ParticleSystem();

	void RegisterModule_Physics();
	RegisterModule_Physics();

	void RegisterModule_Physics2D();
	RegisterModule_Physics2D();

	void RegisterModule_Terrain();
	RegisterModule_Terrain();

	void RegisterModule_TerrainPhysics();
	RegisterModule_TerrainPhysics();

	void RegisterModule_TextRendering();
	RegisterModule_TextRendering();

	void RegisterModule_UI();
	RegisterModule_UI();

	void RegisterModule_UnityConnect();
	RegisterModule_UnityConnect();

	void RegisterModule_IMGUI();
	RegisterModule_IMGUI();

	void RegisterModule_VR();
	RegisterModule_VR();

	void RegisterModule_UnityWebRequest();
	RegisterModule_UnityWebRequest();

	void RegisterModule_JSONSerialize();
	RegisterModule_JSONSerialize();

}

class EditorExtension; template <> void RegisterClass<EditorExtension>();
namespace Unity { class Component; } template <> void RegisterClass<Unity::Component>();
class Behaviour; template <> void RegisterClass<Behaviour>();
class Animation; template <> void RegisterClass<Animation>();
class AudioBehaviour; template <> void RegisterClass<AudioBehaviour>();
class AudioListener; template <> void RegisterClass<AudioListener>();
class AudioSource; template <> void RegisterClass<AudioSource>();
class AudioFilter; 
class AudioChorusFilter; 
class AudioDistortionFilter; 
class AudioEchoFilter; 
class AudioHighPassFilter; 
class AudioLowPassFilter; 
class AudioReverbFilter; 
class AudioReverbZone; 
class Camera; template <> void RegisterClass<Camera>();
namespace UI { class Canvas; } template <> void RegisterClass<UI::Canvas>();
namespace UI { class CanvasGroup; } template <> void RegisterClass<UI::CanvasGroup>();
namespace Unity { class Cloth; } 
class Collider2D; template <> void RegisterClass<Collider2D>();
class BoxCollider2D; 
class CapsuleCollider2D; 
class CircleCollider2D; 
class EdgeCollider2D; 
class PolygonCollider2D; 
class ConstantForce; template <> void RegisterClass<ConstantForce>();
class DirectorPlayer; template <> void RegisterClass<DirectorPlayer>();
class Animator; template <> void RegisterClass<Animator>();
class Effector2D; 
class AreaEffector2D; 
class BuoyancyEffector2D; 
class PlatformEffector2D; 
class PointEffector2D; 
class SurfaceEffector2D; 
class FlareLayer; template <> void RegisterClass<FlareLayer>();
class GUIElement; template <> void RegisterClass<GUIElement>();
namespace TextRenderingPrivate { class GUIText; } template <> void RegisterClass<TextRenderingPrivate::GUIText>();
class GUITexture; 
class GUILayer; template <> void RegisterClass<GUILayer>();
class Halo; 
class HaloLayer; 
class Joint2D; 
class AnchoredJoint2D; 
class DistanceJoint2D; 
class FixedJoint2D; 
class FrictionJoint2D; 
class HingeJoint2D; 
class SliderJoint2D; 
class SpringJoint2D; 
class WheelJoint2D; 
class RelativeJoint2D; 
class TargetJoint2D; 
class LensFlare; 
class Light; template <> void RegisterClass<Light>();
class LightProbeGroup; 
class LightProbeProxyVolume; 
class MonoBehaviour; template <> void RegisterClass<MonoBehaviour>();
class NavMeshAgent; 
class NavMeshObstacle; 
class NetworkView; template <> void RegisterClass<NetworkView>();
class OffMeshLink; 
class PhysicsUpdateBehaviour2D; 
class ConstantForce2D; 
class Projector; 
class ReflectionProbe; 
class Skybox; template <> void RegisterClass<Skybox>();
class Terrain; template <> void RegisterClass<Terrain>();
class WindZone; 
namespace UI { class CanvasRenderer; } template <> void RegisterClass<UI::CanvasRenderer>();
class Collider; template <> void RegisterClass<Collider>();
class BoxCollider; template <> void RegisterClass<BoxCollider>();
class CapsuleCollider; template <> void RegisterClass<CapsuleCollider>();
class CharacterController; 
class MeshCollider; template <> void RegisterClass<MeshCollider>();
class SphereCollider; template <> void RegisterClass<SphereCollider>();
class TerrainCollider; template <> void RegisterClass<TerrainCollider>();
class WheelCollider; 
namespace Unity { class Joint; } template <> void RegisterClass<Unity::Joint>();
namespace Unity { class CharacterJoint; } 
namespace Unity { class ConfigurableJoint; } template <> void RegisterClass<Unity::ConfigurableJoint>();
namespace Unity { class FixedJoint; } template <> void RegisterClass<Unity::FixedJoint>();
namespace Unity { class HingeJoint; } template <> void RegisterClass<Unity::HingeJoint>();
namespace Unity { class SpringJoint; } template <> void RegisterClass<Unity::SpringJoint>();
class LODGroup; 
class MeshFilter; template <> void RegisterClass<MeshFilter>();
class OcclusionArea; 
class OcclusionPortal; 
class ParticleAnimator; 
class ParticleEmitter; 
class EllipsoidParticleEmitter; 
class MeshParticleEmitter; 
class ParticleSystem; template <> void RegisterClass<ParticleSystem>();
class Renderer; template <> void RegisterClass<Renderer>();
class BillboardRenderer; 
class LineRenderer; template <> void RegisterClass<LineRenderer>();
class MeshRenderer; template <> void RegisterClass<MeshRenderer>();
class ParticleRenderer; 
class ParticleSystemRenderer; template <> void RegisterClass<ParticleSystemRenderer>();
class SkinnedMeshRenderer; template <> void RegisterClass<SkinnedMeshRenderer>();
class SpriteRenderer; template <> void RegisterClass<SpriteRenderer>();
class TrailRenderer; 
class Rigidbody; template <> void RegisterClass<Rigidbody>();
class Rigidbody2D; 
namespace TextRenderingPrivate { class TextMesh; } template <> void RegisterClass<TextRenderingPrivate::TextMesh>();
class Transform; template <> void RegisterClass<Transform>();
namespace UI { class RectTransform; } template <> void RegisterClass<UI::RectTransform>();
class Tree; 
class WorldAnchor; 
class WorldParticleCollider; 
class GameObject; template <> void RegisterClass<GameObject>();
class NamedObject; template <> void RegisterClass<NamedObject>();
class AssetBundle; 
class AssetBundleManifest; 
class AudioMixer; template <> void RegisterClass<AudioMixer>();
class AudioMixerController; 
class AudioMixerGroup; template <> void RegisterClass<AudioMixerGroup>();
class AudioMixerGroupController; 
class AudioMixerSnapshot; template <> void RegisterClass<AudioMixerSnapshot>();
class AudioMixerSnapshotController; 
class Avatar; 
class BillboardAsset; 
class ComputeShader; 
class Flare; template <> void RegisterClass<Flare>();
namespace TextRendering { class Font; } template <> void RegisterClass<TextRendering::Font>();
class LightProbes; template <> void RegisterClass<LightProbes>();
class Material; template <> void RegisterClass<Material>();
class ProceduralMaterial; 
class Mesh; template <> void RegisterClass<Mesh>();
class Motion; template <> void RegisterClass<Motion>();
class AnimationClip; template <> void RegisterClass<AnimationClip>();
class NavMeshData; template <> void RegisterClass<NavMeshData>();
class OcclusionCullingData; 
class PhysicMaterial; template <> void RegisterClass<PhysicMaterial>();
class PhysicsMaterial2D; 
class PreloadData; template <> void RegisterClass<PreloadData>();
class RuntimeAnimatorController; template <> void RegisterClass<RuntimeAnimatorController>();
class AnimatorController; template <> void RegisterClass<AnimatorController>();
class AnimatorOverrideController; 
class SampleClip; template <> void RegisterClass<SampleClip>();
class AudioClip; template <> void RegisterClass<AudioClip>();
class Shader; template <> void RegisterClass<Shader>();
class ShaderVariantCollection; 
class SpeedTreeWindAsset; 
class Sprite; template <> void RegisterClass<Sprite>();
class SubstanceArchive; 
class TerrainData; template <> void RegisterClass<TerrainData>();
class TextAsset; template <> void RegisterClass<TextAsset>();
class CGProgram; 
class MonoScript; template <> void RegisterClass<MonoScript>();
class Texture; template <> void RegisterClass<Texture>();
class BaseVideoTexture; 
class MovieTexture; 
class CubemapArray; 
class ProceduralTexture; 
class RenderTexture; template <> void RegisterClass<RenderTexture>();
class SparseTexture; 
class Texture2D; template <> void RegisterClass<Texture2D>();
class Cubemap; template <> void RegisterClass<Cubemap>();
class Texture2DArray; template <> void RegisterClass<Texture2DArray>();
class Texture3D; template <> void RegisterClass<Texture3D>();
class WebCamTexture; 
class GameManager; template <> void RegisterClass<GameManager>();
class GlobalGameManager; template <> void RegisterClass<GlobalGameManager>();
class AudioManager; template <> void RegisterClass<AudioManager>();
class BuildSettings; template <> void RegisterClass<BuildSettings>();
class CloudWebServicesManager; template <> void RegisterClass<CloudWebServicesManager>();
class CrashReportManager; 
class DelayedCallManager; template <> void RegisterClass<DelayedCallManager>();
class GraphicsSettings; template <> void RegisterClass<GraphicsSettings>();
class InputManager; template <> void RegisterClass<InputManager>();
class MasterServerInterface; template <> void RegisterClass<MasterServerInterface>();
class MonoManager; template <> void RegisterClass<MonoManager>();
class NavMeshProjectSettings; template <> void RegisterClass<NavMeshProjectSettings>();
class NetworkManager; template <> void RegisterClass<NetworkManager>();
class Physics2DSettings; template <> void RegisterClass<Physics2DSettings>();
class PhysicsManager; template <> void RegisterClass<PhysicsManager>();
class PlayerSettings; template <> void RegisterClass<PlayerSettings>();
class QualitySettings; template <> void RegisterClass<QualitySettings>();
class ResourceManager; template <> void RegisterClass<ResourceManager>();
class RuntimeInitializeOnLoadManager; template <> void RegisterClass<RuntimeInitializeOnLoadManager>();
class ScriptMapper; template <> void RegisterClass<ScriptMapper>();
class TagManager; template <> void RegisterClass<TagManager>();
class TimeManager; template <> void RegisterClass<TimeManager>();
class UnityAdsManager; 
class UnityAnalyticsManager; 
class UnityConnectSettings; template <> void RegisterClass<UnityConnectSettings>();
class LevelGameManager; template <> void RegisterClass<LevelGameManager>();
class LightmapSettings; template <> void RegisterClass<LightmapSettings>();
class NavMeshSettings; template <> void RegisterClass<NavMeshSettings>();
class OcclusionCullingSettings; 
class RenderSettings; template <> void RegisterClass<RenderSettings>();
class NScreenBridge; 

void RegisterAllClasses()
{
void RegisterBuiltinTypes();
RegisterBuiltinTypes();
	//Total: 103 non stripped classes
	//0. Behaviour
	RegisterClass<Behaviour>();
	//1. Unity::Component
	RegisterClass<Unity::Component>();
	//2. EditorExtension
	RegisterClass<EditorExtension>();
	//3. Camera
	RegisterClass<Camera>();
	//4. GameObject
	RegisterClass<GameObject>();
	//5. RenderSettings
	RegisterClass<RenderSettings>();
	//6. LevelGameManager
	RegisterClass<LevelGameManager>();
	//7. GameManager
	RegisterClass<GameManager>();
	//8. QualitySettings
	RegisterClass<QualitySettings>();
	//9. GlobalGameManager
	RegisterClass<GlobalGameManager>();
	//10. MeshFilter
	RegisterClass<MeshFilter>();
	//11. SkinnedMeshRenderer
	RegisterClass<SkinnedMeshRenderer>();
	//12. Renderer
	RegisterClass<Renderer>();
	//13. Skybox
	RegisterClass<Skybox>();
	//14. LineRenderer
	RegisterClass<LineRenderer>();
	//15. GUILayer
	RegisterClass<GUILayer>();
	//16. Light
	RegisterClass<Light>();
	//17. Mesh
	RegisterClass<Mesh>();
	//18. NamedObject
	RegisterClass<NamedObject>();
	//19. MonoBehaviour
	RegisterClass<MonoBehaviour>();
	//20. NetworkView
	RegisterClass<NetworkView>();
	//21. UI::RectTransform
	RegisterClass<UI::RectTransform>();
	//22. Transform
	RegisterClass<Transform>();
	//23. Shader
	RegisterClass<Shader>();
	//24. Material
	RegisterClass<Material>();
	//25. Sprite
	RegisterClass<Sprite>();
	//26. TextAsset
	RegisterClass<TextAsset>();
	//27. Texture
	RegisterClass<Texture>();
	//28. Texture2D
	RegisterClass<Texture2D>();
	//29. Texture3D
	RegisterClass<Texture3D>();
	//30. RenderTexture
	RegisterClass<RenderTexture>();
	//31. ParticleSystem
	RegisterClass<ParticleSystem>();
	//32. Rigidbody
	RegisterClass<Rigidbody>();
	//33. Unity::Joint
	RegisterClass<Unity::Joint>();
	//34. Unity::HingeJoint
	RegisterClass<Unity::HingeJoint>();
	//35. Unity::SpringJoint
	RegisterClass<Unity::SpringJoint>();
	//36. Unity::ConfigurableJoint
	RegisterClass<Unity::ConfigurableJoint>();
	//37. ConstantForce
	RegisterClass<ConstantForce>();
	//38. Collider
	RegisterClass<Collider>();
	//39. BoxCollider
	RegisterClass<BoxCollider>();
	//40. SphereCollider
	RegisterClass<SphereCollider>();
	//41. CapsuleCollider
	RegisterClass<CapsuleCollider>();
	//42. AudioClip
	RegisterClass<AudioClip>();
	//43. SampleClip
	RegisterClass<SampleClip>();
	//44. AudioSource
	RegisterClass<AudioSource>();
	//45. AudioBehaviour
	RegisterClass<AudioBehaviour>();
	//46. AudioMixer
	RegisterClass<AudioMixer>();
	//47. Animation
	RegisterClass<Animation>();
	//48. Animator
	RegisterClass<Animator>();
	//49. DirectorPlayer
	RegisterClass<DirectorPlayer>();
	//50. Terrain
	RegisterClass<Terrain>();
	//51. TextRenderingPrivate::GUIText
	RegisterClass<TextRenderingPrivate::GUIText>();
	//52. GUIElement
	RegisterClass<GUIElement>();
	//53. TextRenderingPrivate::TextMesh
	RegisterClass<TextRenderingPrivate::TextMesh>();
	//54. TextRendering::Font
	RegisterClass<TextRendering::Font>();
	//55. UI::Canvas
	RegisterClass<UI::Canvas>();
	//56. UI::CanvasGroup
	RegisterClass<UI::CanvasGroup>();
	//57. UI::CanvasRenderer
	RegisterClass<UI::CanvasRenderer>();
	//58. FlareLayer
	RegisterClass<FlareLayer>();
	//59. AudioMixerGroup
	RegisterClass<AudioMixerGroup>();
	//60. Collider2D
	RegisterClass<Collider2D>();
	//61. MeshRenderer
	RegisterClass<MeshRenderer>();
	//62. TerrainCollider
	RegisterClass<TerrainCollider>();
	//63. Unity::FixedJoint
	RegisterClass<Unity::FixedJoint>();
	//64. AnimationClip
	RegisterClass<AnimationClip>();
	//65. Motion
	RegisterClass<Motion>();
	//66. PhysicMaterial
	RegisterClass<PhysicMaterial>();
	//67. AudioListener
	RegisterClass<AudioListener>();
	//68. MeshCollider
	RegisterClass<MeshCollider>();
	//69. SpriteRenderer
	RegisterClass<SpriteRenderer>();
	//70. RuntimeAnimatorController
	RegisterClass<RuntimeAnimatorController>();
	//71. PreloadData
	RegisterClass<PreloadData>();
	//72. Cubemap
	RegisterClass<Cubemap>();
	//73. Texture2DArray
	RegisterClass<Texture2DArray>();
	//74. TimeManager
	RegisterClass<TimeManager>();
	//75. AudioManager
	RegisterClass<AudioManager>();
	//76. InputManager
	RegisterClass<InputManager>();
	//77. Physics2DSettings
	RegisterClass<Physics2DSettings>();
	//78. GraphicsSettings
	RegisterClass<GraphicsSettings>();
	//79. PhysicsManager
	RegisterClass<PhysicsManager>();
	//80. TagManager
	RegisterClass<TagManager>();
	//81. ScriptMapper
	RegisterClass<ScriptMapper>();
	//82. DelayedCallManager
	RegisterClass<DelayedCallManager>();
	//83. MonoScript
	RegisterClass<MonoScript>();
	//84. MonoManager
	RegisterClass<MonoManager>();
	//85. NavMeshProjectSettings
	RegisterClass<NavMeshProjectSettings>();
	//86. PlayerSettings
	RegisterClass<PlayerSettings>();
	//87. BuildSettings
	RegisterClass<BuildSettings>();
	//88. ResourceManager
	RegisterClass<ResourceManager>();
	//89. NetworkManager
	RegisterClass<NetworkManager>();
	//90. MasterServerInterface
	RegisterClass<MasterServerInterface>();
	//91. RuntimeInitializeOnLoadManager
	RegisterClass<RuntimeInitializeOnLoadManager>();
	//92. CloudWebServicesManager
	RegisterClass<CloudWebServicesManager>();
	//93. UnityConnectSettings
	RegisterClass<UnityConnectSettings>();
	//94. AnimatorController
	RegisterClass<AnimatorController>();
	//95. Flare
	RegisterClass<Flare>();
	//96. TerrainData
	RegisterClass<TerrainData>();
	//97. LightmapSettings
	RegisterClass<LightmapSettings>();
	//98. ParticleSystemRenderer
	RegisterClass<ParticleSystemRenderer>();
	//99. NavMeshData
	RegisterClass<NavMeshData>();
	//100. LightProbes
	RegisterClass<LightProbes>();
	//101. AudioMixerSnapshot
	RegisterClass<AudioMixerSnapshot>();
	//102. NavMeshSettings
	RegisterClass<NavMeshSettings>();

}
