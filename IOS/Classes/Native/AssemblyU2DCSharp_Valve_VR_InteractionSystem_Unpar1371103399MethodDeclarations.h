﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Unparent
struct Unparent_t1371103399;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.Unparent::.ctor()
extern "C"  void Unparent__ctor_m3593280353 (Unparent_t1371103399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Unparent::Start()
extern "C"  void Unparent_Start_m36020161 (Unparent_t1371103399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Unparent::Update()
extern "C"  void Unparent_Update_m1070915206 (Unparent_t1371103399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Valve.VR.InteractionSystem.Unparent::GetOldParent()
extern "C"  Transform_t3275118058 * Unparent_GetOldParent_m2950010608 (Unparent_t1371103399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
