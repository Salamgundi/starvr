﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Controller_Menu
struct  Controller_Menu_t102574584  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject VRTK.Examples.Controller_Menu::menuObject
	GameObject_t1756533147 * ___menuObject_2;
	// UnityEngine.GameObject VRTK.Examples.Controller_Menu::clonedMenuObject
	GameObject_t1756533147 * ___clonedMenuObject_3;
	// System.Boolean VRTK.Examples.Controller_Menu::menuInit
	bool ___menuInit_4;
	// System.Boolean VRTK.Examples.Controller_Menu::menuActive
	bool ___menuActive_5;

public:
	inline static int32_t get_offset_of_menuObject_2() { return static_cast<int32_t>(offsetof(Controller_Menu_t102574584, ___menuObject_2)); }
	inline GameObject_t1756533147 * get_menuObject_2() const { return ___menuObject_2; }
	inline GameObject_t1756533147 ** get_address_of_menuObject_2() { return &___menuObject_2; }
	inline void set_menuObject_2(GameObject_t1756533147 * value)
	{
		___menuObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___menuObject_2, value);
	}

	inline static int32_t get_offset_of_clonedMenuObject_3() { return static_cast<int32_t>(offsetof(Controller_Menu_t102574584, ___clonedMenuObject_3)); }
	inline GameObject_t1756533147 * get_clonedMenuObject_3() const { return ___clonedMenuObject_3; }
	inline GameObject_t1756533147 ** get_address_of_clonedMenuObject_3() { return &___clonedMenuObject_3; }
	inline void set_clonedMenuObject_3(GameObject_t1756533147 * value)
	{
		___clonedMenuObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___clonedMenuObject_3, value);
	}

	inline static int32_t get_offset_of_menuInit_4() { return static_cast<int32_t>(offsetof(Controller_Menu_t102574584, ___menuInit_4)); }
	inline bool get_menuInit_4() const { return ___menuInit_4; }
	inline bool* get_address_of_menuInit_4() { return &___menuInit_4; }
	inline void set_menuInit_4(bool value)
	{
		___menuInit_4 = value;
	}

	inline static int32_t get_offset_of_menuActive_5() { return static_cast<int32_t>(offsetof(Controller_Menu_t102574584, ___menuActive_5)); }
	inline bool get_menuActive_5() const { return ___menuActive_5; }
	inline bool* get_address_of_menuActive_5() { return &___menuActive_5; }
	inline void set_menuActive_5(bool value)
	{
		___menuActive_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
