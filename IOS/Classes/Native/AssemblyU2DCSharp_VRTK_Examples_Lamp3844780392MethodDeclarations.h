﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Lamp
struct Lamp_t3844780392;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.Examples.Lamp::.ctor()
extern "C"  void Lamp__ctor_m4251342927 (Lamp_t3844780392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Lamp::Grabbed(UnityEngine.GameObject)
extern "C"  void Lamp_Grabbed_m2613392522 (Lamp_t3844780392 * __this, GameObject_t1756533147 * ___grabbingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Lamp::Ungrabbed(UnityEngine.GameObject)
extern "C"  void Lamp_Ungrabbed_m570174437 (Lamp_t3844780392 * __this, GameObject_t1756533147 * ___previousGrabbingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Lamp::ToggleKinematics(System.Boolean)
extern "C"  void Lamp_ToggleKinematics_m2796152054 (Lamp_t3844780392 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
