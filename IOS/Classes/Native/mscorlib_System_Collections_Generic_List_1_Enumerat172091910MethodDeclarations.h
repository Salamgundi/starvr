﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshRenderer>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1802877084(__this, ___l0, method) ((  void (*) (Enumerator_t172091910 *, List_1_t637362236 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshRenderer>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m32066702(__this, method) ((  void (*) (Enumerator_t172091910 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshRenderer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2983385320(__this, method) ((  Il2CppObject * (*) (Enumerator_t172091910 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshRenderer>::Dispose()
#define Enumerator_Dispose_m1568559619(__this, method) ((  void (*) (Enumerator_t172091910 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshRenderer>::VerifyState()
#define Enumerator_VerifyState_m2514175234(__this, method) ((  void (*) (Enumerator_t172091910 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshRenderer>::MoveNext()
#define Enumerator_MoveNext_m3118496929(__this, method) ((  bool (*) (Enumerator_t172091910 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshRenderer>::get_Current()
#define Enumerator_get_Current_m2220553813(__this, method) ((  MeshRenderer_t1268241104 * (*) (Enumerator_t172091910 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
