﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_CreateDashboardOverlay
struct _CreateDashboardOverlay_t637199691;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_CreateDashboardOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _CreateDashboardOverlay__ctor_m111531704 (_CreateDashboardOverlay_t637199691 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_CreateDashboardOverlay::Invoke(System.String,System.String,System.UInt64&,System.UInt64&)
extern "C"  int32_t _CreateDashboardOverlay_Invoke_m2373863772 (_CreateDashboardOverlay_t637199691 * __this, String_t* ___pchOverlayKey0, String_t* ___pchOverlayFriendlyName1, uint64_t* ___pMainHandle2, uint64_t* ___pThumbnailHandle3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_CreateDashboardOverlay::BeginInvoke(System.String,System.String,System.UInt64&,System.UInt64&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _CreateDashboardOverlay_BeginInvoke_m107664817 (_CreateDashboardOverlay_t637199691 * __this, String_t* ___pchOverlayKey0, String_t* ___pchOverlayFriendlyName1, uint64_t* ___pMainHandle2, uint64_t* ___pThumbnailHandle3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_CreateDashboardOverlay::EndInvoke(System.UInt64&,System.UInt64&,System.IAsyncResult)
extern "C"  int32_t _CreateDashboardOverlay_EndInvoke_m1949197174 (_CreateDashboardOverlay_t637199691 * __this, uint64_t* ___pMainHandle0, uint64_t* ___pThumbnailHandle1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
