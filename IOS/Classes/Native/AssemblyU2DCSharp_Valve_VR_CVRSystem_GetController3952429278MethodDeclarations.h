﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRSystem/GetControllerStateWithPoseUnion
struct GetControllerStateWithPoseUnion_t3952429278;
struct GetControllerStateWithPoseUnion_t3952429278_marshaled_pinvoke;
struct GetControllerStateWithPoseUnion_t3952429278_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct GetControllerStateWithPoseUnion_t3952429278;
struct GetControllerStateWithPoseUnion_t3952429278_marshaled_pinvoke;

extern "C" void GetControllerStateWithPoseUnion_t3952429278_marshal_pinvoke(const GetControllerStateWithPoseUnion_t3952429278& unmarshaled, GetControllerStateWithPoseUnion_t3952429278_marshaled_pinvoke& marshaled);
extern "C" void GetControllerStateWithPoseUnion_t3952429278_marshal_pinvoke_back(const GetControllerStateWithPoseUnion_t3952429278_marshaled_pinvoke& marshaled, GetControllerStateWithPoseUnion_t3952429278& unmarshaled);
extern "C" void GetControllerStateWithPoseUnion_t3952429278_marshal_pinvoke_cleanup(GetControllerStateWithPoseUnion_t3952429278_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct GetControllerStateWithPoseUnion_t3952429278;
struct GetControllerStateWithPoseUnion_t3952429278_marshaled_com;

extern "C" void GetControllerStateWithPoseUnion_t3952429278_marshal_com(const GetControllerStateWithPoseUnion_t3952429278& unmarshaled, GetControllerStateWithPoseUnion_t3952429278_marshaled_com& marshaled);
extern "C" void GetControllerStateWithPoseUnion_t3952429278_marshal_com_back(const GetControllerStateWithPoseUnion_t3952429278_marshaled_com& marshaled, GetControllerStateWithPoseUnion_t3952429278& unmarshaled);
extern "C" void GetControllerStateWithPoseUnion_t3952429278_marshal_com_cleanup(GetControllerStateWithPoseUnion_t3952429278_marshaled_com& marshaled);
