﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetHighQualityOverlay
struct _SetHighQualityOverlay_t4225758140;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetHighQualityOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetHighQualityOverlay__ctor_m17114191 (_SetHighQualityOverlay_t4225758140 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetHighQualityOverlay::Invoke(System.UInt64)
extern "C"  int32_t _SetHighQualityOverlay_Invoke_m462705292 (_SetHighQualityOverlay_t4225758140 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetHighQualityOverlay::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetHighQualityOverlay_BeginInvoke_m1445686107 (_SetHighQualityOverlay_t4225758140 * __this, uint64_t ___ulOverlayHandle0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetHighQualityOverlay::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetHighQualityOverlay_EndInvoke_m2661878317 (_SetHighQualityOverlay_t4225758140 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
