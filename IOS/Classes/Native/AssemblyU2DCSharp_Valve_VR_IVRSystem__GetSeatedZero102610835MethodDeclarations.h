﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetSeatedZeroPoseToStandingAbsoluteTrackingPose
struct _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetSeatedZeroPoseToStandingAbsoluteTrackingPose::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetSeatedZeroPoseToStandingAbsoluteTrackingPose__ctor_m3085694044 (_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix34_t Valve.VR.IVRSystem/_GetSeatedZeroPoseToStandingAbsoluteTrackingPose::Invoke()
extern "C"  HmdMatrix34_t_t664273062  _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_Invoke_m2248037343 (_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetSeatedZeroPoseToStandingAbsoluteTrackingPose::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_BeginInvoke_m401331261 (_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix34_t Valve.VR.IVRSystem/_GetSeatedZeroPoseToStandingAbsoluteTrackingPose::EndInvoke(System.IAsyncResult)
extern "C"  HmdMatrix34_t_t664273062  _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_EndInvoke_m3761222813 (_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
