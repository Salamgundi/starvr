﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/Burst[]
struct BurstU5BU5D_t3091130216;
// UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_t2748003162;
struct EmissionModule_t2748003162_marshaled_pinvoke;
struct EmissionModule_t2748003162_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_EmissionMod2748003162.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void EmissionModule__ctor_m1076689768 (EmissionModule_t2748003162 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetBursts(UnityEngine.ParticleSystem/Burst[],System.Int32)
extern "C"  void EmissionModule_SetBursts_m1290967351 (EmissionModule_t2748003162 * __this, BurstU5BU5D_t3091130216* ___bursts0, int32_t ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetBursts(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/Burst[],System.Int32)
extern "C"  void EmissionModule_SetBursts_m3041577753 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, BurstU5BU5D_t3091130216* ___bursts1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct EmissionModule_t2748003162;
struct EmissionModule_t2748003162_marshaled_pinvoke;

extern "C" void EmissionModule_t2748003162_marshal_pinvoke(const EmissionModule_t2748003162& unmarshaled, EmissionModule_t2748003162_marshaled_pinvoke& marshaled);
extern "C" void EmissionModule_t2748003162_marshal_pinvoke_back(const EmissionModule_t2748003162_marshaled_pinvoke& marshaled, EmissionModule_t2748003162& unmarshaled);
extern "C" void EmissionModule_t2748003162_marshal_pinvoke_cleanup(EmissionModule_t2748003162_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct EmissionModule_t2748003162;
struct EmissionModule_t2748003162_marshaled_com;

extern "C" void EmissionModule_t2748003162_marshal_com(const EmissionModule_t2748003162& unmarshaled, EmissionModule_t2748003162_marshaled_com& marshaled);
extern "C" void EmissionModule_t2748003162_marshal_com_back(const EmissionModule_t2748003162_marshaled_com& marshaled, EmissionModule_t2748003162& unmarshaled);
extern "C" void EmissionModule_t2748003162_marshal_com_cleanup(EmissionModule_t2748003162_marshaled_com& marshaled);
