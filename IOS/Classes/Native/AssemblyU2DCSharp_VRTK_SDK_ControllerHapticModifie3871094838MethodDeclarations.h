﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_ControllerHapticModifiers
struct SDK_ControllerHapticModifiers_t3871094838;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_ControllerHapticModifiers::.ctor()
extern "C"  void SDK_ControllerHapticModifiers__ctor_m406516918 (SDK_ControllerHapticModifiers_t3871094838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
