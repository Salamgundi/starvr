﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SteamVR_TrackedObject
struct SteamVR_TrackedObject_t2338458854;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reset
struct  Reset_t866863539  : public MonoBehaviour_t1158329972
{
public:
	// SteamVR_TrackedObject Reset::trackedObj
	SteamVR_TrackedObject_t2338458854 * ___trackedObj_2;

public:
	inline static int32_t get_offset_of_trackedObj_2() { return static_cast<int32_t>(offsetof(Reset_t866863539, ___trackedObj_2)); }
	inline SteamVR_TrackedObject_t2338458854 * get_trackedObj_2() const { return ___trackedObj_2; }
	inline SteamVR_TrackedObject_t2338458854 ** get_address_of_trackedObj_2() { return &___trackedObj_2; }
	inline void set_trackedObj_2(SteamVR_TrackedObject_t2338458854 * value)
	{
		___trackedObj_2 = value;
		Il2CppCodeGenWriteBarrier(&___trackedObj_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
