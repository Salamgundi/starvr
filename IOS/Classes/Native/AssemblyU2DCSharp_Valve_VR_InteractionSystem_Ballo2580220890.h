﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t1854387467;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.BalloonColliders
struct  BalloonColliders_t2580220890  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] Valve.VR.InteractionSystem.BalloonColliders::colliders
	GameObjectU5BU5D_t3057952154* ___colliders_2;
	// UnityEngine.Vector3[] Valve.VR.InteractionSystem.BalloonColliders::colliderLocalPositions
	Vector3U5BU5D_t1172311765* ___colliderLocalPositions_3;
	// UnityEngine.Quaternion[] Valve.VR.InteractionSystem.BalloonColliders::colliderLocalRotations
	QuaternionU5BU5D_t1854387467* ___colliderLocalRotations_4;
	// UnityEngine.Rigidbody Valve.VR.InteractionSystem.BalloonColliders::rb
	Rigidbody_t4233889191 * ___rb_5;

public:
	inline static int32_t get_offset_of_colliders_2() { return static_cast<int32_t>(offsetof(BalloonColliders_t2580220890, ___colliders_2)); }
	inline GameObjectU5BU5D_t3057952154* get_colliders_2() const { return ___colliders_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_colliders_2() { return &___colliders_2; }
	inline void set_colliders_2(GameObjectU5BU5D_t3057952154* value)
	{
		___colliders_2 = value;
		Il2CppCodeGenWriteBarrier(&___colliders_2, value);
	}

	inline static int32_t get_offset_of_colliderLocalPositions_3() { return static_cast<int32_t>(offsetof(BalloonColliders_t2580220890, ___colliderLocalPositions_3)); }
	inline Vector3U5BU5D_t1172311765* get_colliderLocalPositions_3() const { return ___colliderLocalPositions_3; }
	inline Vector3U5BU5D_t1172311765** get_address_of_colliderLocalPositions_3() { return &___colliderLocalPositions_3; }
	inline void set_colliderLocalPositions_3(Vector3U5BU5D_t1172311765* value)
	{
		___colliderLocalPositions_3 = value;
		Il2CppCodeGenWriteBarrier(&___colliderLocalPositions_3, value);
	}

	inline static int32_t get_offset_of_colliderLocalRotations_4() { return static_cast<int32_t>(offsetof(BalloonColliders_t2580220890, ___colliderLocalRotations_4)); }
	inline QuaternionU5BU5D_t1854387467* get_colliderLocalRotations_4() const { return ___colliderLocalRotations_4; }
	inline QuaternionU5BU5D_t1854387467** get_address_of_colliderLocalRotations_4() { return &___colliderLocalRotations_4; }
	inline void set_colliderLocalRotations_4(QuaternionU5BU5D_t1854387467* value)
	{
		___colliderLocalRotations_4 = value;
		Il2CppCodeGenWriteBarrier(&___colliderLocalRotations_4, value);
	}

	inline static int32_t get_offset_of_rb_5() { return static_cast<int32_t>(offsetof(BalloonColliders_t2580220890, ___rb_5)); }
	inline Rigidbody_t4233889191 * get_rb_5() const { return ___rb_5; }
	inline Rigidbody_t4233889191 ** get_address_of_rb_5() { return &___rb_5; }
	inline void set_rb_5(Rigidbody_t4233889191 * value)
	{
		___rb_5 = value;
		Il2CppCodeGenWriteBarrier(&___rb_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
