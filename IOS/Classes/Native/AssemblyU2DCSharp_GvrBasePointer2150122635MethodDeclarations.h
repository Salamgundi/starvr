﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrBasePointer
struct GvrBasePointer_t2150122635;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void GvrBasePointer::.ctor()
extern "C"  void GvrBasePointer__ctor_m677865000 (GvrBasePointer_t2150122635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GvrBasePointer::get_ShouldUseExitRadiusForRaycast()
extern "C"  bool GvrBasePointer_get_ShouldUseExitRadiusForRaycast_m3705361285 (GvrBasePointer_t2150122635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrBasePointer::set_ShouldUseExitRadiusForRaycast(System.Boolean)
extern "C"  void GvrBasePointer_set_ShouldUseExitRadiusForRaycast_m1138403000 (GvrBasePointer_t2150122635 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform GvrBasePointer::get_PointerTransform()
extern "C"  Transform_t3275118058 * GvrBasePointer_get_PointerTransform_m3494232690 (GvrBasePointer_t2150122635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrBasePointer::set_PointerTransform(UnityEngine.Transform)
extern "C"  void GvrBasePointer_set_PointerTransform_m2270092423 (GvrBasePointer_t2150122635 * __this, Transform_t3275118058 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrBasePointer::OnStart()
extern "C"  void GvrBasePointer_OnStart_m2563285875 (GvrBasePointer_t2150122635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
