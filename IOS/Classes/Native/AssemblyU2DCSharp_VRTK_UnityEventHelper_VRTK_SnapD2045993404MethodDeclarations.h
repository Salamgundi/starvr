﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents
struct VRTK_SnapDropZone_UnityEvents_t2045993404;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_SnapDropZoneEventArgs418702774.h"

// System.Void VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents::.ctor()
extern "C"  void VRTK_SnapDropZone_UnityEvents__ctor_m3286003053 (VRTK_SnapDropZone_UnityEvents_t2045993404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents::SetSnapDropZone()
extern "C"  void VRTK_SnapDropZone_UnityEvents_SetSnapDropZone_m1546831974 (VRTK_SnapDropZone_UnityEvents_t2045993404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents::OnEnable()
extern "C"  void VRTK_SnapDropZone_UnityEvents_OnEnable_m891636813 (VRTK_SnapDropZone_UnityEvents_t2045993404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents::ObjectEnteredSnapDropZone(System.Object,VRTK.SnapDropZoneEventArgs)
extern "C"  void VRTK_SnapDropZone_UnityEvents_ObjectEnteredSnapDropZone_m1391970449 (VRTK_SnapDropZone_UnityEvents_t2045993404 * __this, Il2CppObject * ___o0, SnapDropZoneEventArgs_t418702774  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents::ObjectExitedSnapDropZone(System.Object,VRTK.SnapDropZoneEventArgs)
extern "C"  void VRTK_SnapDropZone_UnityEvents_ObjectExitedSnapDropZone_m764640209 (VRTK_SnapDropZone_UnityEvents_t2045993404 * __this, Il2CppObject * ___o0, SnapDropZoneEventArgs_t418702774  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents::ObjectSnappedToDropZone(System.Object,VRTK.SnapDropZoneEventArgs)
extern "C"  void VRTK_SnapDropZone_UnityEvents_ObjectSnappedToDropZone_m3883596852 (VRTK_SnapDropZone_UnityEvents_t2045993404 * __this, Il2CppObject * ___o0, SnapDropZoneEventArgs_t418702774  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents::ObjectUnsnappedFromDropZone(System.Object,VRTK.SnapDropZoneEventArgs)
extern "C"  void VRTK_SnapDropZone_UnityEvents_ObjectUnsnappedFromDropZone_m2911089704 (VRTK_SnapDropZone_UnityEvents_t2045993404 * __this, Il2CppObject * ___o0, SnapDropZoneEventArgs_t418702774  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents::OnDisable()
extern "C"  void VRTK_SnapDropZone_UnityEvents_OnDisable_m2150232734 (VRTK_SnapDropZone_UnityEvents_t2045993404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
