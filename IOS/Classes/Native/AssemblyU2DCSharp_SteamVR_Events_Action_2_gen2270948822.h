﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SteamVR_Events/Event`2<System.Int32,System.Boolean>
struct Event_2_t3885983284;
// UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>
struct UnityAction_2_t41828916;

#include "AssemblyU2DCSharp_SteamVR_Events_Action1836998693.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SteamVR_Events/Action`2<System.Int32,System.Boolean>
struct  Action_2_t2270948822  : public Action_t1836998693
{
public:
	// SteamVR_Events/Event`2<T0,T1> SteamVR_Events/Action`2::_event
	Event_2_t3885983284 * ____event_0;
	// UnityEngine.Events.UnityAction`2<T0,T1> SteamVR_Events/Action`2::action
	UnityAction_2_t41828916 * ___action_1;

public:
	inline static int32_t get_offset_of__event_0() { return static_cast<int32_t>(offsetof(Action_2_t2270948822, ____event_0)); }
	inline Event_2_t3885983284 * get__event_0() const { return ____event_0; }
	inline Event_2_t3885983284 ** get_address_of__event_0() { return &____event_0; }
	inline void set__event_0(Event_2_t3885983284 * value)
	{
		____event_0 = value;
		Il2CppCodeGenWriteBarrier(&____event_0, value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(Action_2_t2270948822, ___action_1)); }
	inline UnityAction_2_t41828916 * get_action_1() const { return ___action_1; }
	inline UnityAction_2_t41828916 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(UnityAction_2_t41828916 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
