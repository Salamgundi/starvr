﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_GazeTracker
struct SteamVR_GazeTracker_t1880012212;
// GazeEventHandler
struct GazeEventHandler_t1044944507;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GazeEventHandler1044944507.h"
#include "AssemblyU2DCSharp_GazeEventArgs2196141074.h"

// System.Void SteamVR_GazeTracker::.ctor()
extern "C"  void SteamVR_GazeTracker__ctor_m778466827 (SteamVR_GazeTracker_t1880012212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_GazeTracker::add_GazeOn(GazeEventHandler)
extern "C"  void SteamVR_GazeTracker_add_GazeOn_m1391893272 (SteamVR_GazeTracker_t1880012212 * __this, GazeEventHandler_t1044944507 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_GazeTracker::remove_GazeOn(GazeEventHandler)
extern "C"  void SteamVR_GazeTracker_remove_GazeOn_m3155257119 (SteamVR_GazeTracker_t1880012212 * __this, GazeEventHandler_t1044944507 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_GazeTracker::add_GazeOff(GazeEventHandler)
extern "C"  void SteamVR_GazeTracker_add_GazeOff_m1597148042 (SteamVR_GazeTracker_t1880012212 * __this, GazeEventHandler_t1044944507 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_GazeTracker::remove_GazeOff(GazeEventHandler)
extern "C"  void SteamVR_GazeTracker_remove_GazeOff_m1768487897 (SteamVR_GazeTracker_t1880012212 * __this, GazeEventHandler_t1044944507 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_GazeTracker::Start()
extern "C"  void SteamVR_GazeTracker_Start_m581331343 (SteamVR_GazeTracker_t1880012212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_GazeTracker::OnGazeOn(GazeEventArgs)
extern "C"  void SteamVR_GazeTracker_OnGazeOn_m2964915438 (SteamVR_GazeTracker_t1880012212 * __this, GazeEventArgs_t2196141074  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_GazeTracker::OnGazeOff(GazeEventArgs)
extern "C"  void SteamVR_GazeTracker_OnGazeOff_m3742406610 (SteamVR_GazeTracker_t1880012212 * __this, GazeEventArgs_t2196141074  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_GazeTracker::Update()
extern "C"  void SteamVR_GazeTracker_Update_m1955123222 (SteamVR_GazeTracker_t1880012212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
