﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<Valve.VR.EVREventType>
struct EqualityComparer_1_t2875449442;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Valve.VR.EVREventType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3957896036_gshared (EqualityComparer_1_t2875449442 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m3957896036(__this, method) ((  void (*) (EqualityComparer_1_t2875449442 *, const MethodInfo*))EqualityComparer_1__ctor_m3957896036_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Valve.VR.EVREventType>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3465077807_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3465077807(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3465077807_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Valve.VR.EVREventType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m540050915_gshared (EqualityComparer_1_t2875449442 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m540050915(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t2875449442 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m540050915_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Valve.VR.EVREventType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m4274816429_gshared (EqualityComparer_1_t2875449442 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m4274816429(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t2875449442 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m4274816429_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Valve.VR.EVREventType>::get_Default()
extern "C"  EqualityComparer_1_t2875449442 * EqualityComparer_1_get_Default_m1392557992_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m1392557992(__this /* static, unused */, method) ((  EqualityComparer_1_t2875449442 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m1392557992_gshared)(__this /* static, unused */, method)
