﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.Examples.Archery.BowAnimation
struct BowAnimation_t4022033600;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.Examples.Archery.BowHandle
struct BowHandle_t1058184516;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Archery.BowAim
struct  BowAim_t1262532443  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VRTK.Examples.Archery.BowAim::powerMultiplier
	float ___powerMultiplier_2;
	// System.Single VRTK.Examples.Archery.BowAim::pullMultiplier
	float ___pullMultiplier_3;
	// System.Single VRTK.Examples.Archery.BowAim::pullOffset
	float ___pullOffset_4;
	// System.Single VRTK.Examples.Archery.BowAim::maxPullDistance
	float ___maxPullDistance_5;
	// System.Single VRTK.Examples.Archery.BowAim::bowVibration
	float ___bowVibration_6;
	// System.Single VRTK.Examples.Archery.BowAim::stringVibration
	float ___stringVibration_7;
	// VRTK.Examples.Archery.BowAnimation VRTK.Examples.Archery.BowAim::bowAnimation
	BowAnimation_t4022033600 * ___bowAnimation_8;
	// UnityEngine.GameObject VRTK.Examples.Archery.BowAim::currentArrow
	GameObject_t1756533147 * ___currentArrow_9;
	// VRTK.Examples.Archery.BowHandle VRTK.Examples.Archery.BowAim::handle
	BowHandle_t1058184516 * ___handle_10;
	// VRTK.VRTK_InteractableObject VRTK.Examples.Archery.BowAim::interact
	VRTK_InteractableObject_t2604188111 * ___interact_11;
	// VRTK.VRTK_ControllerEvents VRTK.Examples.Archery.BowAim::holdControl
	VRTK_ControllerEvents_t3225224819 * ___holdControl_12;
	// VRTK.VRTK_ControllerEvents VRTK.Examples.Archery.BowAim::stringControl
	VRTK_ControllerEvents_t3225224819 * ___stringControl_13;
	// VRTK.VRTK_ControllerActions VRTK.Examples.Archery.BowAim::stringActions
	VRTK_ControllerActions_t3642353851 * ___stringActions_14;
	// VRTK.VRTK_ControllerActions VRTK.Examples.Archery.BowAim::holdActions
	VRTK_ControllerActions_t3642353851 * ___holdActions_15;
	// UnityEngine.Quaternion VRTK.Examples.Archery.BowAim::releaseRotation
	Quaternion_t4030073918  ___releaseRotation_16;
	// UnityEngine.Quaternion VRTK.Examples.Archery.BowAim::baseRotation
	Quaternion_t4030073918  ___baseRotation_17;
	// System.Boolean VRTK.Examples.Archery.BowAim::fired
	bool ___fired_18;
	// System.Single VRTK.Examples.Archery.BowAim::fireOffset
	float ___fireOffset_19;
	// System.Single VRTK.Examples.Archery.BowAim::currentPull
	float ___currentPull_20;
	// System.Single VRTK.Examples.Archery.BowAim::previousPull
	float ___previousPull_21;

public:
	inline static int32_t get_offset_of_powerMultiplier_2() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___powerMultiplier_2)); }
	inline float get_powerMultiplier_2() const { return ___powerMultiplier_2; }
	inline float* get_address_of_powerMultiplier_2() { return &___powerMultiplier_2; }
	inline void set_powerMultiplier_2(float value)
	{
		___powerMultiplier_2 = value;
	}

	inline static int32_t get_offset_of_pullMultiplier_3() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___pullMultiplier_3)); }
	inline float get_pullMultiplier_3() const { return ___pullMultiplier_3; }
	inline float* get_address_of_pullMultiplier_3() { return &___pullMultiplier_3; }
	inline void set_pullMultiplier_3(float value)
	{
		___pullMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_pullOffset_4() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___pullOffset_4)); }
	inline float get_pullOffset_4() const { return ___pullOffset_4; }
	inline float* get_address_of_pullOffset_4() { return &___pullOffset_4; }
	inline void set_pullOffset_4(float value)
	{
		___pullOffset_4 = value;
	}

	inline static int32_t get_offset_of_maxPullDistance_5() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___maxPullDistance_5)); }
	inline float get_maxPullDistance_5() const { return ___maxPullDistance_5; }
	inline float* get_address_of_maxPullDistance_5() { return &___maxPullDistance_5; }
	inline void set_maxPullDistance_5(float value)
	{
		___maxPullDistance_5 = value;
	}

	inline static int32_t get_offset_of_bowVibration_6() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___bowVibration_6)); }
	inline float get_bowVibration_6() const { return ___bowVibration_6; }
	inline float* get_address_of_bowVibration_6() { return &___bowVibration_6; }
	inline void set_bowVibration_6(float value)
	{
		___bowVibration_6 = value;
	}

	inline static int32_t get_offset_of_stringVibration_7() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___stringVibration_7)); }
	inline float get_stringVibration_7() const { return ___stringVibration_7; }
	inline float* get_address_of_stringVibration_7() { return &___stringVibration_7; }
	inline void set_stringVibration_7(float value)
	{
		___stringVibration_7 = value;
	}

	inline static int32_t get_offset_of_bowAnimation_8() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___bowAnimation_8)); }
	inline BowAnimation_t4022033600 * get_bowAnimation_8() const { return ___bowAnimation_8; }
	inline BowAnimation_t4022033600 ** get_address_of_bowAnimation_8() { return &___bowAnimation_8; }
	inline void set_bowAnimation_8(BowAnimation_t4022033600 * value)
	{
		___bowAnimation_8 = value;
		Il2CppCodeGenWriteBarrier(&___bowAnimation_8, value);
	}

	inline static int32_t get_offset_of_currentArrow_9() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___currentArrow_9)); }
	inline GameObject_t1756533147 * get_currentArrow_9() const { return ___currentArrow_9; }
	inline GameObject_t1756533147 ** get_address_of_currentArrow_9() { return &___currentArrow_9; }
	inline void set_currentArrow_9(GameObject_t1756533147 * value)
	{
		___currentArrow_9 = value;
		Il2CppCodeGenWriteBarrier(&___currentArrow_9, value);
	}

	inline static int32_t get_offset_of_handle_10() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___handle_10)); }
	inline BowHandle_t1058184516 * get_handle_10() const { return ___handle_10; }
	inline BowHandle_t1058184516 ** get_address_of_handle_10() { return &___handle_10; }
	inline void set_handle_10(BowHandle_t1058184516 * value)
	{
		___handle_10 = value;
		Il2CppCodeGenWriteBarrier(&___handle_10, value);
	}

	inline static int32_t get_offset_of_interact_11() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___interact_11)); }
	inline VRTK_InteractableObject_t2604188111 * get_interact_11() const { return ___interact_11; }
	inline VRTK_InteractableObject_t2604188111 ** get_address_of_interact_11() { return &___interact_11; }
	inline void set_interact_11(VRTK_InteractableObject_t2604188111 * value)
	{
		___interact_11 = value;
		Il2CppCodeGenWriteBarrier(&___interact_11, value);
	}

	inline static int32_t get_offset_of_holdControl_12() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___holdControl_12)); }
	inline VRTK_ControllerEvents_t3225224819 * get_holdControl_12() const { return ___holdControl_12; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_holdControl_12() { return &___holdControl_12; }
	inline void set_holdControl_12(VRTK_ControllerEvents_t3225224819 * value)
	{
		___holdControl_12 = value;
		Il2CppCodeGenWriteBarrier(&___holdControl_12, value);
	}

	inline static int32_t get_offset_of_stringControl_13() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___stringControl_13)); }
	inline VRTK_ControllerEvents_t3225224819 * get_stringControl_13() const { return ___stringControl_13; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_stringControl_13() { return &___stringControl_13; }
	inline void set_stringControl_13(VRTK_ControllerEvents_t3225224819 * value)
	{
		___stringControl_13 = value;
		Il2CppCodeGenWriteBarrier(&___stringControl_13, value);
	}

	inline static int32_t get_offset_of_stringActions_14() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___stringActions_14)); }
	inline VRTK_ControllerActions_t3642353851 * get_stringActions_14() const { return ___stringActions_14; }
	inline VRTK_ControllerActions_t3642353851 ** get_address_of_stringActions_14() { return &___stringActions_14; }
	inline void set_stringActions_14(VRTK_ControllerActions_t3642353851 * value)
	{
		___stringActions_14 = value;
		Il2CppCodeGenWriteBarrier(&___stringActions_14, value);
	}

	inline static int32_t get_offset_of_holdActions_15() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___holdActions_15)); }
	inline VRTK_ControllerActions_t3642353851 * get_holdActions_15() const { return ___holdActions_15; }
	inline VRTK_ControllerActions_t3642353851 ** get_address_of_holdActions_15() { return &___holdActions_15; }
	inline void set_holdActions_15(VRTK_ControllerActions_t3642353851 * value)
	{
		___holdActions_15 = value;
		Il2CppCodeGenWriteBarrier(&___holdActions_15, value);
	}

	inline static int32_t get_offset_of_releaseRotation_16() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___releaseRotation_16)); }
	inline Quaternion_t4030073918  get_releaseRotation_16() const { return ___releaseRotation_16; }
	inline Quaternion_t4030073918 * get_address_of_releaseRotation_16() { return &___releaseRotation_16; }
	inline void set_releaseRotation_16(Quaternion_t4030073918  value)
	{
		___releaseRotation_16 = value;
	}

	inline static int32_t get_offset_of_baseRotation_17() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___baseRotation_17)); }
	inline Quaternion_t4030073918  get_baseRotation_17() const { return ___baseRotation_17; }
	inline Quaternion_t4030073918 * get_address_of_baseRotation_17() { return &___baseRotation_17; }
	inline void set_baseRotation_17(Quaternion_t4030073918  value)
	{
		___baseRotation_17 = value;
	}

	inline static int32_t get_offset_of_fired_18() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___fired_18)); }
	inline bool get_fired_18() const { return ___fired_18; }
	inline bool* get_address_of_fired_18() { return &___fired_18; }
	inline void set_fired_18(bool value)
	{
		___fired_18 = value;
	}

	inline static int32_t get_offset_of_fireOffset_19() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___fireOffset_19)); }
	inline float get_fireOffset_19() const { return ___fireOffset_19; }
	inline float* get_address_of_fireOffset_19() { return &___fireOffset_19; }
	inline void set_fireOffset_19(float value)
	{
		___fireOffset_19 = value;
	}

	inline static int32_t get_offset_of_currentPull_20() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___currentPull_20)); }
	inline float get_currentPull_20() const { return ___currentPull_20; }
	inline float* get_address_of_currentPull_20() { return &___currentPull_20; }
	inline void set_currentPull_20(float value)
	{
		___currentPull_20 = value;
	}

	inline static int32_t get_offset_of_previousPull_21() { return static_cast<int32_t>(offsetof(BowAim_t1262532443, ___previousPull_21)); }
	inline float get_previousPull_21() const { return ___previousPull_21; }
	inline float* get_address_of_previousPull_21() { return &___previousPull_21; }
	inline void set_previousPull_21(float value)
	{
		___previousPull_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
