﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_UIPointer/<WaitForPointerId>c__Iterator0
struct U3CWaitForPointerIdU3Ec__Iterator0_t2911062424;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_UIPointer/<WaitForPointerId>c__Iterator0::.ctor()
extern "C"  void U3CWaitForPointerIdU3Ec__Iterator0__ctor_m1634011407 (U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_UIPointer/<WaitForPointerId>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitForPointerIdU3Ec__Iterator0_MoveNext_m3214135897 (U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_UIPointer/<WaitForPointerId>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForPointerIdU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1264197277 (U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_UIPointer/<WaitForPointerId>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForPointerIdU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1953379669 (U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer/<WaitForPointerId>c__Iterator0::Dispose()
extern "C"  void U3CWaitForPointerIdU3Ec__Iterator0_Dispose_m1763776926 (U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer/<WaitForPointerId>c__Iterator0::Reset()
extern "C"  void U3CWaitForPointerIdU3Ec__Iterator0_Reset_m841313512 (U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
