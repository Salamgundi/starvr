﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2197888944.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SDKManager_SupportedSD1339136682.h"

// System.Void System.Array/InternalEnumerator`1<VRTK.VRTK_SDKManager/SupportedSDKs>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3311511119_gshared (InternalEnumerator_1_t2197888944 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3311511119(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2197888944 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3311511119_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<VRTK.VRTK_SDKManager/SupportedSDKs>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2109874287_gshared (InternalEnumerator_1_t2197888944 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2109874287(__this, method) ((  void (*) (InternalEnumerator_1_t2197888944 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2109874287_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<VRTK.VRTK_SDKManager/SupportedSDKs>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3917030771_gshared (InternalEnumerator_1_t2197888944 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3917030771(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2197888944 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3917030771_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<VRTK.VRTK_SDKManager/SupportedSDKs>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3023185086_gshared (InternalEnumerator_1_t2197888944 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3023185086(__this, method) ((  void (*) (InternalEnumerator_1_t2197888944 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3023185086_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<VRTK.VRTK_SDKManager/SupportedSDKs>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m298410075_gshared (InternalEnumerator_1_t2197888944 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m298410075(__this, method) ((  bool (*) (InternalEnumerator_1_t2197888944 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m298410075_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<VRTK.VRTK_SDKManager/SupportedSDKs>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1411318776_gshared (InternalEnumerator_1_t2197888944 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1411318776(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2197888944 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1411318776_gshared)(__this, method)
