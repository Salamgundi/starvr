﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Stats
struct SteamVR_Stats_t2562702356;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Stats::.ctor()
extern "C"  void SteamVR_Stats__ctor_m3180156331 (SteamVR_Stats_t2562702356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Stats::Awake()
extern "C"  void SteamVR_Stats_Awake_m69959828 (SteamVR_Stats_t2562702356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Stats::Update()
extern "C"  void SteamVR_Stats_Update_m20554646 (SteamVR_Stats_t2562702356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
