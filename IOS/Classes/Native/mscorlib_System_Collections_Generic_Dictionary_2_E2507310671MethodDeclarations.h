﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1316079175(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2507310671 *, Dictionary_2_t1187285969 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3561589304(__this, method) ((  Il2CppObject * (*) (Enumerator_t2507310671 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1443917886(__this, method) ((  void (*) (Enumerator_t2507310671 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2500488329(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2507310671 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1542623270(__this, method) ((  Il2CppObject * (*) (Enumerator_t2507310671 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2201557356(__this, method) ((  Il2CppObject * (*) (Enumerator_t2507310671 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::MoveNext()
#define Enumerator_MoveNext_m508785830(__this, method) ((  bool (*) (Enumerator_t2507310671 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::get_Current()
#define Enumerator_get_Current_m1365938374(__this, method) ((  KeyValuePair_2_t3239598487  (*) (Enumerator_t2507310671 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3106575619(__this, method) ((  Transform_t3275118058 * (*) (Enumerator_t2507310671 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3214394411(__this, method) ((  List_1_t1445631064 * (*) (Enumerator_t2507310671 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::Reset()
#define Enumerator_Reset_m3625565361(__this, method) ((  void (*) (Enumerator_t2507310671 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::VerifyState()
#define Enumerator_VerifyState_m4138043998(__this, method) ((  void (*) (Enumerator_t2507310671 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2530656270(__this, method) ((  void (*) (Enumerator_t2507310671 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::Dispose()
#define Enumerator_Dispose_m2642338579(__this, method) ((  void (*) (Enumerator_t2507310671 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
