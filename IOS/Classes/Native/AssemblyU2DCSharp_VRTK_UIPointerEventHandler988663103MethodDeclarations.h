﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UIPointerEventHandler
struct UIPointerEventHandler_t988663103;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_UIPointerEventArgs1171985978.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.UIPointerEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UIPointerEventHandler__ctor_m669826349 (UIPointerEventHandler_t988663103 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UIPointerEventHandler::Invoke(System.Object,VRTK.UIPointerEventArgs)
extern "C"  void UIPointerEventHandler_Invoke_m2700959014 (UIPointerEventHandler_t988663103 * __this, Il2CppObject * ___sender0, UIPointerEventArgs_t1171985978  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.UIPointerEventHandler::BeginInvoke(System.Object,VRTK.UIPointerEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UIPointerEventHandler_BeginInvoke_m3375410497 (UIPointerEventHandler_t988663103 * __this, Il2CppObject * ___sender0, UIPointerEventArgs_t1171985978  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UIPointerEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UIPointerEventHandler_EndInvoke_m1604293479 (UIPointerEventHandler_t988663103 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
