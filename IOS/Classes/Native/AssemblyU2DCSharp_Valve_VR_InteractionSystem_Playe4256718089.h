﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// Valve.VR.InteractionSystem.Hand[]
struct HandU5BU5D_t3984143324;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Valve.VR.InteractionSystem.Player
struct Player_t4256718089;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.Player
struct  Player_t4256718089  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Valve.VR.InteractionSystem.Player::trackingOriginTransform
	Transform_t3275118058 * ___trackingOriginTransform_2;
	// UnityEngine.Transform[] Valve.VR.InteractionSystem.Player::hmdTransforms
	TransformU5BU5D_t3764228911* ___hmdTransforms_3;
	// Valve.VR.InteractionSystem.Hand[] Valve.VR.InteractionSystem.Player::hands
	HandU5BU5D_t3984143324* ___hands_4;
	// UnityEngine.Collider Valve.VR.InteractionSystem.Player::headCollider
	Collider_t3497673348 * ___headCollider_5;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Player::rigSteamVR
	GameObject_t1756533147 * ___rigSteamVR_6;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Player::rig2DFallback
	GameObject_t1756533147 * ___rig2DFallback_7;
	// UnityEngine.Transform Valve.VR.InteractionSystem.Player::audioListener
	Transform_t3275118058 * ___audioListener_8;
	// System.Boolean Valve.VR.InteractionSystem.Player::allowToggleTo2D
	bool ___allowToggleTo2D_9;

public:
	inline static int32_t get_offset_of_trackingOriginTransform_2() { return static_cast<int32_t>(offsetof(Player_t4256718089, ___trackingOriginTransform_2)); }
	inline Transform_t3275118058 * get_trackingOriginTransform_2() const { return ___trackingOriginTransform_2; }
	inline Transform_t3275118058 ** get_address_of_trackingOriginTransform_2() { return &___trackingOriginTransform_2; }
	inline void set_trackingOriginTransform_2(Transform_t3275118058 * value)
	{
		___trackingOriginTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&___trackingOriginTransform_2, value);
	}

	inline static int32_t get_offset_of_hmdTransforms_3() { return static_cast<int32_t>(offsetof(Player_t4256718089, ___hmdTransforms_3)); }
	inline TransformU5BU5D_t3764228911* get_hmdTransforms_3() const { return ___hmdTransforms_3; }
	inline TransformU5BU5D_t3764228911** get_address_of_hmdTransforms_3() { return &___hmdTransforms_3; }
	inline void set_hmdTransforms_3(TransformU5BU5D_t3764228911* value)
	{
		___hmdTransforms_3 = value;
		Il2CppCodeGenWriteBarrier(&___hmdTransforms_3, value);
	}

	inline static int32_t get_offset_of_hands_4() { return static_cast<int32_t>(offsetof(Player_t4256718089, ___hands_4)); }
	inline HandU5BU5D_t3984143324* get_hands_4() const { return ___hands_4; }
	inline HandU5BU5D_t3984143324** get_address_of_hands_4() { return &___hands_4; }
	inline void set_hands_4(HandU5BU5D_t3984143324* value)
	{
		___hands_4 = value;
		Il2CppCodeGenWriteBarrier(&___hands_4, value);
	}

	inline static int32_t get_offset_of_headCollider_5() { return static_cast<int32_t>(offsetof(Player_t4256718089, ___headCollider_5)); }
	inline Collider_t3497673348 * get_headCollider_5() const { return ___headCollider_5; }
	inline Collider_t3497673348 ** get_address_of_headCollider_5() { return &___headCollider_5; }
	inline void set_headCollider_5(Collider_t3497673348 * value)
	{
		___headCollider_5 = value;
		Il2CppCodeGenWriteBarrier(&___headCollider_5, value);
	}

	inline static int32_t get_offset_of_rigSteamVR_6() { return static_cast<int32_t>(offsetof(Player_t4256718089, ___rigSteamVR_6)); }
	inline GameObject_t1756533147 * get_rigSteamVR_6() const { return ___rigSteamVR_6; }
	inline GameObject_t1756533147 ** get_address_of_rigSteamVR_6() { return &___rigSteamVR_6; }
	inline void set_rigSteamVR_6(GameObject_t1756533147 * value)
	{
		___rigSteamVR_6 = value;
		Il2CppCodeGenWriteBarrier(&___rigSteamVR_6, value);
	}

	inline static int32_t get_offset_of_rig2DFallback_7() { return static_cast<int32_t>(offsetof(Player_t4256718089, ___rig2DFallback_7)); }
	inline GameObject_t1756533147 * get_rig2DFallback_7() const { return ___rig2DFallback_7; }
	inline GameObject_t1756533147 ** get_address_of_rig2DFallback_7() { return &___rig2DFallback_7; }
	inline void set_rig2DFallback_7(GameObject_t1756533147 * value)
	{
		___rig2DFallback_7 = value;
		Il2CppCodeGenWriteBarrier(&___rig2DFallback_7, value);
	}

	inline static int32_t get_offset_of_audioListener_8() { return static_cast<int32_t>(offsetof(Player_t4256718089, ___audioListener_8)); }
	inline Transform_t3275118058 * get_audioListener_8() const { return ___audioListener_8; }
	inline Transform_t3275118058 ** get_address_of_audioListener_8() { return &___audioListener_8; }
	inline void set_audioListener_8(Transform_t3275118058 * value)
	{
		___audioListener_8 = value;
		Il2CppCodeGenWriteBarrier(&___audioListener_8, value);
	}

	inline static int32_t get_offset_of_allowToggleTo2D_9() { return static_cast<int32_t>(offsetof(Player_t4256718089, ___allowToggleTo2D_9)); }
	inline bool get_allowToggleTo2D_9() const { return ___allowToggleTo2D_9; }
	inline bool* get_address_of_allowToggleTo2D_9() { return &___allowToggleTo2D_9; }
	inline void set_allowToggleTo2D_9(bool value)
	{
		___allowToggleTo2D_9 = value;
	}
};

struct Player_t4256718089_StaticFields
{
public:
	// Valve.VR.InteractionSystem.Player Valve.VR.InteractionSystem.Player::_instance
	Player_t4256718089 * ____instance_10;

public:
	inline static int32_t get_offset_of__instance_10() { return static_cast<int32_t>(offsetof(Player_t4256718089_StaticFields, ____instance_10)); }
	inline Player_t4256718089 * get__instance_10() const { return ____instance_10; }
	inline Player_t4256718089 ** get_address_of__instance_10() { return &____instance_10; }
	inline void set__instance_10(Player_t4256718089 * value)
	{
		____instance_10 = value;
		Il2CppCodeGenWriteBarrier(&____instance_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
