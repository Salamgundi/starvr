﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_FramesPerSecondViewer
struct VRTK_FramesPerSecondViewer_t2787247731;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_FramesPerSecondViewer::.ctor()
extern "C"  void VRTK_FramesPerSecondViewer__ctor_m578177689 (VRTK_FramesPerSecondViewer_t2787247731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_FramesPerSecondViewer::Start()
extern "C"  void VRTK_FramesPerSecondViewer_Start_m1500064641 (VRTK_FramesPerSecondViewer_t2787247731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_FramesPerSecondViewer::Update()
extern "C"  void VRTK_FramesPerSecondViewer_Update_m1155598630 (VRTK_FramesPerSecondViewer_t2787247731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
