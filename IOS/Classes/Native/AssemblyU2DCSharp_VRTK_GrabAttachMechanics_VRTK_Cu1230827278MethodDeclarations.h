﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.GrabAttachMechanics.VRTK_CustomJointGrabAttach
struct VRTK_CustomJointGrabAttach_t1230827278;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.GrabAttachMechanics.VRTK_CustomJointGrabAttach::.ctor()
extern "C"  void VRTK_CustomJointGrabAttach__ctor_m1648248832 (VRTK_CustomJointGrabAttach_t1230827278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_CustomJointGrabAttach::Initialise()
extern "C"  void VRTK_CustomJointGrabAttach_Initialise_m2913167901 (VRTK_CustomJointGrabAttach_t1230827278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_CustomJointGrabAttach::CreateJoint(UnityEngine.GameObject)
extern "C"  void VRTK_CustomJointGrabAttach_CreateJoint_m1840928290 (VRTK_CustomJointGrabAttach_t1230827278 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_CustomJointGrabAttach::DestroyJoint(System.Boolean,System.Boolean)
extern "C"  void VRTK_CustomJointGrabAttach_DestroyJoint_m2657577616 (VRTK_CustomJointGrabAttach_t1230827278 * __this, bool ___withDestroyImmediate0, bool ___applyGrabbingObjectVelocity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_CustomJointGrabAttach::CopyCustomJoint()
extern "C"  void VRTK_CustomJointGrabAttach_CopyCustomJoint_m138701614 (VRTK_CustomJointGrabAttach_t1230827278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
