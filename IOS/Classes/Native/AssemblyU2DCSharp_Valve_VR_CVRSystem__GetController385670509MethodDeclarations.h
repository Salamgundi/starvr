﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRSystem/_GetControllerStatePacked
struct _GetControllerStatePacked_t385670509;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_VRControllerState_t_Pac1296713633.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.CVRSystem/_GetControllerStatePacked::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetControllerStatePacked__ctor_m2193543824 (_GetControllerStatePacked_t385670509 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSystem/_GetControllerStatePacked::Invoke(System.UInt32,Valve.VR.VRControllerState_t_Packed&,System.UInt32)
extern "C"  bool _GetControllerStatePacked_Invoke_m1641169905 (_GetControllerStatePacked_t385670509 * __this, uint32_t ___unControllerDeviceIndex0, VRControllerState_t_Packed_t1296713633 * ___pControllerState1, uint32_t ___unControllerStateSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.CVRSystem/_GetControllerStatePacked::BeginInvoke(System.UInt32,Valve.VR.VRControllerState_t_Packed&,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetControllerStatePacked_BeginInvoke_m3580240796 (_GetControllerStatePacked_t385670509 * __this, uint32_t ___unControllerDeviceIndex0, VRControllerState_t_Packed_t1296713633 * ___pControllerState1, uint32_t ___unControllerStateSize2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSystem/_GetControllerStatePacked::EndInvoke(Valve.VR.VRControllerState_t_Packed&,System.IAsyncResult)
extern "C"  bool _GetControllerStatePacked_EndInvoke_m4037832079 (_GetControllerStatePacked_t385670509 * __this, VRControllerState_t_Packed_t1296713633 * ___pControllerState0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
