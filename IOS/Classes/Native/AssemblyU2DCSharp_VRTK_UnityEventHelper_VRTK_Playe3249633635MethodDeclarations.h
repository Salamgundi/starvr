﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_PlayerClimb_UnityEvents
struct VRTK_PlayerClimb_UnityEvents_t3249633635;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_PlayerClimbEventArgs2537585745.h"

// System.Void VRTK.UnityEventHelper.VRTK_PlayerClimb_UnityEvents::.ctor()
extern "C"  void VRTK_PlayerClimb_UnityEvents__ctor_m3653682822 (VRTK_PlayerClimb_UnityEvents_t3249633635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_PlayerClimb_UnityEvents::SetPlayerClimb()
extern "C"  void VRTK_PlayerClimb_UnityEvents_SetPlayerClimb_m2286306730 (VRTK_PlayerClimb_UnityEvents_t3249633635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_PlayerClimb_UnityEvents::OnEnable()
extern "C"  void VRTK_PlayerClimb_UnityEvents_OnEnable_m1905295982 (VRTK_PlayerClimb_UnityEvents_t3249633635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_PlayerClimb_UnityEvents::PlayerClimbStarted(System.Object,VRTK.PlayerClimbEventArgs)
extern "C"  void VRTK_PlayerClimb_UnityEvents_PlayerClimbStarted_m2482019133 (VRTK_PlayerClimb_UnityEvents_t3249633635 * __this, Il2CppObject * ___o0, PlayerClimbEventArgs_t2537585745  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_PlayerClimb_UnityEvents::PlayerClimbEnded(System.Object,VRTK.PlayerClimbEventArgs)
extern "C"  void VRTK_PlayerClimb_UnityEvents_PlayerClimbEnded_m4073561060 (VRTK_PlayerClimb_UnityEvents_t3249633635 * __this, Il2CppObject * ___o0, PlayerClimbEventArgs_t2537585745  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_PlayerClimb_UnityEvents::OnDisable()
extern "C"  void VRTK_PlayerClimb_UnityEvents_OnDisable_m3680269609 (VRTK_PlayerClimb_UnityEvents_t3249633635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
