﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.FireExtinguisher_Base
struct FireExtinguisher_Base_t3980302883;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.Examples.FireExtinguisher_Base::.ctor()
extern "C"  void FireExtinguisher_Base__ctor_m4088006536 (FireExtinguisher_Base_t3980302883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.FireExtinguisher_Base::StartUsing(UnityEngine.GameObject)
extern "C"  void FireExtinguisher_Base_StartUsing_m188159544 (FireExtinguisher_Base_t3980302883 * __this, GameObject_t1756533147 * ___usingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.FireExtinguisher_Base::StopUsing(UnityEngine.GameObject)
extern "C"  void FireExtinguisher_Base_StopUsing_m2034402970 (FireExtinguisher_Base_t3980302883 * __this, GameObject_t1756533147 * ___previousUsingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.FireExtinguisher_Base::Update()
extern "C"  void FireExtinguisher_Base_Update_m3756372325 (FireExtinguisher_Base_t3980302883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.FireExtinguisher_Base::Spray(System.Single)
extern "C"  void FireExtinguisher_Base_Spray_m2012552266 (FireExtinguisher_Base_t3980302883 * __this, float ___power0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.FireExtinguisher_Base::SetHandleFrame(System.Single)
extern "C"  void FireExtinguisher_Base_SetHandleFrame_m396787214 (FireExtinguisher_Base_t3980302883 * __this, float ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
