﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_Button/ButtonEvents
struct ButtonEvents_t99329243;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_Button/ButtonEvents::.ctor()
extern "C"  void ButtonEvents__ctor_m3398785998 (ButtonEvents_t99329243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
