﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_BezierPointer
struct VRTK_BezierPointer_t2668864506;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_BezierPointer::.ctor()
extern "C"  void VRTK_BezierPointer__ctor_m678993446 (VRTK_BezierPointer_t2668864506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointer::OnEnable()
extern "C"  void VRTK_BezierPointer_OnEnable_m747126922 (VRTK_BezierPointer_t2668864506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointer::OnDisable()
extern "C"  void VRTK_BezierPointer_OnDisable_m2174892867 (VRTK_BezierPointer_t2668864506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointer::Update()
extern "C"  void VRTK_BezierPointer_Update_m2688380675 (VRTK_BezierPointer_t2668864506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointer::InitPointer()
extern "C"  void VRTK_BezierPointer_InitPointer_m1083639823 (VRTK_BezierPointer_t2668864506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointer::SetPointerMaterial(UnityEngine.Color)
extern "C"  void VRTK_BezierPointer_SetPointerMaterial_m3432558784 (VRTK_BezierPointer_t2668864506 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointer::TogglePointer(System.Boolean)
extern "C"  void VRTK_BezierPointer_TogglePointer_m3055662222 (VRTK_BezierPointer_t2668864506 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointer::DisablePointerBeam(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_BezierPointer_DisablePointerBeam_m1597401226 (VRTK_BezierPointer_t2668864506 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointer::ToggleBezierBeam(System.Boolean)
extern "C"  void VRTK_BezierPointer_ToggleBezierBeam_m1717029279 (VRTK_BezierPointer_t2668864506 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_BezierPointer::CreateCursor()
extern "C"  GameObject_t1756533147 * VRTK_BezierPointer_CreateCursor_m858646163 (VRTK_BezierPointer_t2668864506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointer::TogglePointerCursor(System.Boolean)
extern "C"  void VRTK_BezierPointer_TogglePointerCursor_m1055710164 (VRTK_BezierPointer_t2668864506 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_BezierPointer::ProjectForwardBeam()
extern "C"  Vector3_t2243707580  VRTK_BezierPointer_ProjectForwardBeam_m3704246309 (VRTK_BezierPointer_t2668864506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_BezierPointer::ProjectDownBeam(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  VRTK_BezierPointer_ProjectDownBeam_m288215045 (VRTK_BezierPointer_t2668864506 * __this, Vector3_t2243707580  ___jointPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointer::SetPointerCursor(UnityEngine.Vector3)
extern "C"  void VRTK_BezierPointer_SetPointerCursor_m2177740396 (VRTK_BezierPointer_t2668864506 * __this, Vector3_t2243707580  ___cursorPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointer::AdjustForEarlyCollisions(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void VRTK_BezierPointer_AdjustForEarlyCollisions_m4265993142 (VRTK_BezierPointer_t2668864506 * __this, Vector3_t2243707580  ___jointPosition0, Vector3_t2243707580  ___downPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointer::DisplayCurvedBeam(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void VRTK_BezierPointer_DisplayCurvedBeam_m3676326080 (VRTK_BezierPointer_t2668864506 * __this, Vector3_t2243707580  ___jointPosition0, Vector3_t2243707580  ___downPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
