﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Joint
struct Joint_t454317436;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Ba1229653768.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.GrabAttachMechanics.VRTK_CustomJointGrabAttach
struct  VRTK_CustomJointGrabAttach_t1230827278  : public VRTK_BaseJointGrabAttach_t1229653768
{
public:
	// UnityEngine.Joint VRTK.GrabAttachMechanics.VRTK_CustomJointGrabAttach::customJoint
	Joint_t454317436 * ___customJoint_21;
	// UnityEngine.GameObject VRTK.GrabAttachMechanics.VRTK_CustomJointGrabAttach::jointHolder
	GameObject_t1756533147 * ___jointHolder_22;

public:
	inline static int32_t get_offset_of_customJoint_21() { return static_cast<int32_t>(offsetof(VRTK_CustomJointGrabAttach_t1230827278, ___customJoint_21)); }
	inline Joint_t454317436 * get_customJoint_21() const { return ___customJoint_21; }
	inline Joint_t454317436 ** get_address_of_customJoint_21() { return &___customJoint_21; }
	inline void set_customJoint_21(Joint_t454317436 * value)
	{
		___customJoint_21 = value;
		Il2CppCodeGenWriteBarrier(&___customJoint_21, value);
	}

	inline static int32_t get_offset_of_jointHolder_22() { return static_cast<int32_t>(offsetof(VRTK_CustomJointGrabAttach_t1230827278, ___jointHolder_22)); }
	inline GameObject_t1756533147 * get_jointHolder_22() const { return ___jointHolder_22; }
	inline GameObject_t1756533147 ** get_address_of_jointHolder_22() { return &___jointHolder_22; }
	inline void set_jointHolder_22(GameObject_t1756533147 * value)
	{
		___jointHolder_22 = value;
		Il2CppCodeGenWriteBarrier(&___jointHolder_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
