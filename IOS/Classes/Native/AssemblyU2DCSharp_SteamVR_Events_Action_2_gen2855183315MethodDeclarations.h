﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Action`2<System.Object,System.Boolean>
struct Action_2_t2855183315;
// SteamVR_Events/Event`2<System.Object,System.Boolean>
struct Event_2_t175250481;
// UnityEngine.Events.UnityAction`2<System.Object,System.Boolean>
struct UnityAction_2_t626063409;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Events/Action`2<System.Object,System.Boolean>::.ctor(SteamVR_Events/Event`2<T0,T1>,UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Action_2__ctor_m980819815_gshared (Action_2_t2855183315 * __this, Event_2_t175250481 * ____event0, UnityAction_2_t626063409 * ___action1, const MethodInfo* method);
#define Action_2__ctor_m980819815(__this, ____event0, ___action1, method) ((  void (*) (Action_2_t2855183315 *, Event_2_t175250481 *, UnityAction_2_t626063409 *, const MethodInfo*))Action_2__ctor_m980819815_gshared)(__this, ____event0, ___action1, method)
// System.Void SteamVR_Events/Action`2<System.Object,System.Boolean>::Enable(System.Boolean)
extern "C"  void Action_2_Enable_m822486595_gshared (Action_2_t2855183315 * __this, bool ___enabled0, const MethodInfo* method);
#define Action_2_Enable_m822486595(__this, ___enabled0, method) ((  void (*) (Action_2_t2855183315 *, bool, const MethodInfo*))Action_2_Enable_m822486595_gshared)(__this, ___enabled0, method)
