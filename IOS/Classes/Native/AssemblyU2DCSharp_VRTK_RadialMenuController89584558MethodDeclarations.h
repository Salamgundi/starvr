﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.RadialMenuController
struct RadialMenuController_t89584558;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"

// System.Void VRTK.RadialMenuController::.ctor()
extern "C"  void RadialMenuController__ctor_m486343900 (RadialMenuController_t89584558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::Awake()
extern "C"  void RadialMenuController_Awake_m1783408131 (RadialMenuController_t89584558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::Initialize()
extern "C"  void RadialMenuController_Initialize_m2763090048 (RadialMenuController_t89584558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::OnEnable()
extern "C"  void RadialMenuController_OnEnable_m1954466312 (RadialMenuController_t89584558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::OnDisable()
extern "C"  void RadialMenuController_OnDisable_m3527303375 (RadialMenuController_t89584558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::DoClickButton(System.Object)
extern "C"  void RadialMenuController_DoClickButton_m427274759 (RadialMenuController_t89584558 * __this, Il2CppObject * ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::DoUnClickButton(System.Object)
extern "C"  void RadialMenuController_DoUnClickButton_m3122148946 (RadialMenuController_t89584558 * __this, Il2CppObject * ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::DoShowMenu(System.Single,System.Object)
extern "C"  void RadialMenuController_DoShowMenu_m3673569920 (RadialMenuController_t89584558 * __this, float ___initialAngle0, Il2CppObject * ___sender1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::DoHideMenu(System.Boolean,System.Object)
extern "C"  void RadialMenuController_DoHideMenu_m1949007691 (RadialMenuController_t89584558 * __this, bool ___force0, Il2CppObject * ___sender1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::DoChangeAngle(System.Single,System.Object)
extern "C"  void RadialMenuController_DoChangeAngle_m3880219221 (RadialMenuController_t89584558 * __this, float ___angle0, Il2CppObject * ___sender1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::AttemptHapticPulse(System.Single)
extern "C"  void RadialMenuController_AttemptHapticPulse_m1570058342 (RadialMenuController_t89584558 * __this, float ___strength0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::DoTouchpadClicked(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void RadialMenuController_DoTouchpadClicked_m3300647770 (RadialMenuController_t89584558 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::DoTouchpadUnclicked(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void RadialMenuController_DoTouchpadUnclicked_m44910847 (RadialMenuController_t89584558 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::DoTouchpadTouched(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void RadialMenuController_DoTouchpadTouched_m82975171 (RadialMenuController_t89584558 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::DoTouchpadUntouched(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void RadialMenuController_DoTouchpadUntouched_m605157604 (RadialMenuController_t89584558 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenuController::DoTouchpadAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void RadialMenuController_DoTouchpadAxisChanged_m1637709272 (RadialMenuController_t89584558 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.RadialMenuController::CalculateAngle(VRTK.ControllerInteractionEventArgs)
extern "C"  float RadialMenuController_CalculateAngle_m1552052809 (RadialMenuController_t89584558 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
