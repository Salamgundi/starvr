﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t3353154814;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3066241505 (U3CStartU3Ec__Iterator0_t3353154814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m2433547487 (U3CStartU3Ec__Iterator0_t3353154814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1409399143 (U3CStartU3Ec__Iterator0_t3353154814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1638854751 (U3CStartU3Ec__Iterator0_t3353154814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2402415680 (U3CStartU3Ec__Iterator0_t3353154814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m2237235674 (U3CStartU3Ec__Iterator0_t3353154814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
