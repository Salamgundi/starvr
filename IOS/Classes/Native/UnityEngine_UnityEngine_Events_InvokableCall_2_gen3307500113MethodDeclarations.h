﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DashTeleportEventArgs>
struct InvokableCall_2_t3307500113;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.DashTeleportEventArgs>
struct UnityAction_2_t3292709229;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DashTeleportEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m16262127_gshared (InvokableCall_2_t3307500113 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_2__ctor_m16262127(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_2_t3307500113 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2__ctor_m16262127_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DashTeleportEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m3752805140_gshared (InvokableCall_2_t3307500113 * __this, UnityAction_2_t3292709229 * ___action0, const MethodInfo* method);
#define InvokableCall_2__ctor_m3752805140(__this, ___action0, method) ((  void (*) (InvokableCall_2_t3307500113 *, UnityAction_2_t3292709229 *, const MethodInfo*))InvokableCall_2__ctor_m3752805140_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DashTeleportEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m387072543_gshared (InvokableCall_2_t3307500113 * __this, UnityAction_2_t3292709229 * ___value0, const MethodInfo* method);
#define InvokableCall_2_add_Delegate_m387072543(__this, ___value0, method) ((  void (*) (InvokableCall_2_t3307500113 *, UnityAction_2_t3292709229 *, const MethodInfo*))InvokableCall_2_add_Delegate_m387072543_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DashTeleportEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m3000777586_gshared (InvokableCall_2_t3307500113 * __this, UnityAction_2_t3292709229 * ___value0, const MethodInfo* method);
#define InvokableCall_2_remove_Delegate_m3000777586(__this, ___value0, method) ((  void (*) (InvokableCall_2_t3307500113 *, UnityAction_2_t3292709229 *, const MethodInfo*))InvokableCall_2_remove_Delegate_m3000777586_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DashTeleportEventArgs>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m1020299440_gshared (InvokableCall_2_t3307500113 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_2_Invoke_m1020299440(__this, ___args0, method) ((  void (*) (InvokableCall_2_t3307500113 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_2_Invoke_m1020299440_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.DashTeleportEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m3527899668_gshared (InvokableCall_2_t3307500113 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_2_Find_m3527899668(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_2_t3307500113 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2_Find_m3527899668_gshared)(__this, ___targetObj0, ___method1, method)
