﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JumpToPage
struct JumpToPage_t3783692930;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void JumpToPage::.ctor()
extern "C"  void JumpToPage__ctor_m1108927741 (JumpToPage_t3783692930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpToPage::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JumpToPage_OnPointerEnter_m2699662627 (JumpToPage_t3783692930 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpToPage::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JumpToPage_OnPointerExit_m2178241635 (JumpToPage_t3783692930 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpToPage::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JumpToPage_OnPointerClick_m3917868085 (JumpToPage_t3783692930 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
