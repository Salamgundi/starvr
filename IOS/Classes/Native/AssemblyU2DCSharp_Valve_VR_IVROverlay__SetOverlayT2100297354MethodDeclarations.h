﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetOverlayTransformAbsolute
struct _SetOverlayTransformAbsolute_t2100297354;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingUniverseOrigin1464400093.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetOverlayTransformAbsolute::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayTransformAbsolute__ctor_m50906963 (_SetOverlayTransformAbsolute_t2100297354 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTransformAbsolute::Invoke(System.UInt64,Valve.VR.ETrackingUniverseOrigin,Valve.VR.HmdMatrix34_t&)
extern "C"  int32_t _SetOverlayTransformAbsolute_Invoke_m2113521179 (_SetOverlayTransformAbsolute_t2100297354 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTrackingOrigin1, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToOverlayTransform2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayTransformAbsolute::BeginInvoke(System.UInt64,Valve.VR.ETrackingUniverseOrigin,Valve.VR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetOverlayTransformAbsolute_BeginInvoke_m3734430934 (_SetOverlayTransformAbsolute_t2100297354 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTrackingOrigin1, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToOverlayTransform2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTransformAbsolute::EndInvoke(Valve.VR.HmdMatrix34_t&,System.IAsyncResult)
extern "C"  int32_t _SetOverlayTransformAbsolute_EndInvoke_m401476197 (_SetOverlayTransformAbsolute_t2100297354 * __this, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToOverlayTransform0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
