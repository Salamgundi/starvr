﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.SDK_BaseHeadset
struct  SDK_BaseHeadset_t3046873914  : public ScriptableObject_t1975622470
{
public:
	// UnityEngine.Transform VRTK.SDK_BaseHeadset::cachedHeadset
	Transform_t3275118058 * ___cachedHeadset_2;
	// UnityEngine.Transform VRTK.SDK_BaseHeadset::cachedHeadsetCamera
	Transform_t3275118058 * ___cachedHeadsetCamera_3;

public:
	inline static int32_t get_offset_of_cachedHeadset_2() { return static_cast<int32_t>(offsetof(SDK_BaseHeadset_t3046873914, ___cachedHeadset_2)); }
	inline Transform_t3275118058 * get_cachedHeadset_2() const { return ___cachedHeadset_2; }
	inline Transform_t3275118058 ** get_address_of_cachedHeadset_2() { return &___cachedHeadset_2; }
	inline void set_cachedHeadset_2(Transform_t3275118058 * value)
	{
		___cachedHeadset_2 = value;
		Il2CppCodeGenWriteBarrier(&___cachedHeadset_2, value);
	}

	inline static int32_t get_offset_of_cachedHeadsetCamera_3() { return static_cast<int32_t>(offsetof(SDK_BaseHeadset_t3046873914, ___cachedHeadsetCamera_3)); }
	inline Transform_t3275118058 * get_cachedHeadsetCamera_3() const { return ___cachedHeadsetCamera_3; }
	inline Transform_t3275118058 ** get_address_of_cachedHeadsetCamera_3() { return &___cachedHeadsetCamera_3; }
	inline void set_cachedHeadsetCamera_3(Transform_t3275118058 * value)
	{
		___cachedHeadsetCamera_3 = value;
		Il2CppCodeGenWriteBarrier(&___cachedHeadsetCamera_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
