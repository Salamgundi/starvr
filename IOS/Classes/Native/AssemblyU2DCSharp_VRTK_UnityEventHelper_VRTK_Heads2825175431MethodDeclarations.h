﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents
struct VRTK_HeadsetControllerAware_UnityEvents_t2825175431;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetControllerAwareEvent2653531721.h"

// System.Void VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents::.ctor()
extern "C"  void VRTK_HeadsetControllerAware_UnityEvents__ctor_m551225504 (VRTK_HeadsetControllerAware_UnityEvents_t2825175431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents::SetHeadsetControllerAware()
extern "C"  void VRTK_HeadsetControllerAware_UnityEvents_SetHeadsetControllerAware_m2414910432 (VRTK_HeadsetControllerAware_UnityEvents_t2825175431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents::OnEnable()
extern "C"  void VRTK_HeadsetControllerAware_UnityEvents_OnEnable_m154004760 (VRTK_HeadsetControllerAware_UnityEvents_t2825175431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents::ControllerObscured(System.Object,VRTK.HeadsetControllerAwareEventArgs)
extern "C"  void VRTK_HeadsetControllerAware_UnityEvents_ControllerObscured_m3189086169 (VRTK_HeadsetControllerAware_UnityEvents_t2825175431 * __this, Il2CppObject * ___o0, HeadsetControllerAwareEventArgs_t2653531721  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents::ControllerUnobscured(System.Object,VRTK.HeadsetControllerAwareEventArgs)
extern "C"  void VRTK_HeadsetControllerAware_UnityEvents_ControllerUnobscured_m497265348 (VRTK_HeadsetControllerAware_UnityEvents_t2825175431 * __this, Il2CppObject * ___o0, HeadsetControllerAwareEventArgs_t2653531721  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents::ControllerGlanceEnter(System.Object,VRTK.HeadsetControllerAwareEventArgs)
extern "C"  void VRTK_HeadsetControllerAware_UnityEvents_ControllerGlanceEnter_m1296865198 (VRTK_HeadsetControllerAware_UnityEvents_t2825175431 * __this, Il2CppObject * ___o0, HeadsetControllerAwareEventArgs_t2653531721  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents::ControllerGlanceExit(System.Object,VRTK.HeadsetControllerAwareEventArgs)
extern "C"  void VRTK_HeadsetControllerAware_UnityEvents_ControllerGlanceExit_m280693446 (VRTK_HeadsetControllerAware_UnityEvents_t2825175431 * __this, Il2CppObject * ___o0, HeadsetControllerAwareEventArgs_t2653531721  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents::OnDisable()
extern "C"  void VRTK_HeadsetControllerAware_UnityEvents_OnDisable_m1412871417 (VRTK_HeadsetControllerAware_UnityEvents_t2825175431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
