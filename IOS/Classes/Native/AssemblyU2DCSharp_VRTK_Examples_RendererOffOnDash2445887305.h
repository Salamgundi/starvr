﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<VRTK.VRTK_DashTeleport>
struct List_1_t575320617;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.RendererOffOnDash
struct  RendererOffOnDash_t2445887305  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.Examples.RendererOffOnDash::wasSwitchedOff
	bool ___wasSwitchedOff_2;
	// System.Collections.Generic.List`1<VRTK.VRTK_DashTeleport> VRTK.Examples.RendererOffOnDash::dashTeleporters
	List_1_t575320617 * ___dashTeleporters_3;

public:
	inline static int32_t get_offset_of_wasSwitchedOff_2() { return static_cast<int32_t>(offsetof(RendererOffOnDash_t2445887305, ___wasSwitchedOff_2)); }
	inline bool get_wasSwitchedOff_2() const { return ___wasSwitchedOff_2; }
	inline bool* get_address_of_wasSwitchedOff_2() { return &___wasSwitchedOff_2; }
	inline void set_wasSwitchedOff_2(bool value)
	{
		___wasSwitchedOff_2 = value;
	}

	inline static int32_t get_offset_of_dashTeleporters_3() { return static_cast<int32_t>(offsetof(RendererOffOnDash_t2445887305, ___dashTeleporters_3)); }
	inline List_1_t575320617 * get_dashTeleporters_3() const { return ___dashTeleporters_3; }
	inline List_1_t575320617 ** get_address_of_dashTeleporters_3() { return &___dashTeleporters_3; }
	inline void set_dashTeleporters_3(List_1_t575320617 * value)
	{
		___dashTeleporters_3 = value;
		Il2CppCodeGenWriteBarrier(&___dashTeleporters_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
