﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_SetWorkingCollisionBoundsInfo
struct _SetWorkingCollisionBoundsInfo_t3486256295;
// System.Object
struct Il2CppObject;
// Valve.VR.HmdQuad_t[]
struct HmdQuad_tU5BU5D_t16941492;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingCollisionBoundsInfo::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetWorkingCollisionBoundsInfo__ctor_m2602829668 (_SetWorkingCollisionBoundsInfo_t3486256295 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingCollisionBoundsInfo::Invoke(Valve.VR.HmdQuad_t[],System.UInt32)
extern "C"  void _SetWorkingCollisionBoundsInfo_Invoke_m1737851547 (_SetWorkingCollisionBoundsInfo_t3486256295 * __this, HmdQuad_tU5BU5D_t16941492* ___pQuadsBuffer0, uint32_t ___unQuadsCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_SetWorkingCollisionBoundsInfo::BeginInvoke(Valve.VR.HmdQuad_t[],System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetWorkingCollisionBoundsInfo_BeginInvoke_m3729621058 (_SetWorkingCollisionBoundsInfo_t3486256295 * __this, HmdQuad_tU5BU5D_t16941492* ___pQuadsBuffer0, uint32_t ___unQuadsCount1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingCollisionBoundsInfo::EndInvoke(System.IAsyncResult)
extern "C"  void _SetWorkingCollisionBoundsInfo_EndInvoke_m836416866 (_SetWorkingCollisionBoundsInfo_t3486256295 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
