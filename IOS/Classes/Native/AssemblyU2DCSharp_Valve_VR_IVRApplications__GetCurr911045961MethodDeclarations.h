﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetCurrentSceneProcessId
struct _GetCurrentSceneProcessId_t911045961;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetCurrentSceneProcessId::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetCurrentSceneProcessId__ctor_m2593155616 (_GetCurrentSceneProcessId_t911045961 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRApplications/_GetCurrentSceneProcessId::Invoke()
extern "C"  uint32_t _GetCurrentSceneProcessId_Invoke_m2272024855 (_GetCurrentSceneProcessId_t911045961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetCurrentSceneProcessId::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetCurrentSceneProcessId_BeginInvoke_m2765976103 (_GetCurrentSceneProcessId_t911045961 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRApplications/_GetCurrentSceneProcessId::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetCurrentSceneProcessId_EndInvoke_m3715101089 (_GetCurrentSceneProcessId_t911045961 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
