﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.PanelMenu.PanelMenuUISlider
struct PanelMenuUISlider_t2320851018;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_PanelMenuItemControllerEven2917504033.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.Examples.PanelMenu.PanelMenuUISlider::.ctor()
extern "C"  void PanelMenuUISlider__ctor_m3764904012 (PanelMenuUISlider_t2320851018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.PanelMenu.PanelMenuUISlider::Start()
extern "C"  void PanelMenuUISlider_Start_m2471147908 (PanelMenuUISlider_t2320851018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.PanelMenu.PanelMenuUISlider::OnPanelMenuItemSwipeLeft(System.Object,VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuUISlider_OnPanelMenuItemSwipeLeft_m2396041044 (PanelMenuUISlider_t2320851018 * __this, Il2CppObject * ___sender0, PanelMenuItemControllerEventArgs_t2917504033  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.PanelMenu.PanelMenuUISlider::OnPanelMenuItemSwipeRight(System.Object,VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuUISlider_OnPanelMenuItemSwipeRight_m1213731025 (PanelMenuUISlider_t2320851018 * __this, Il2CppObject * ___sender0, PanelMenuItemControllerEventArgs_t2917504033  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.PanelMenu.PanelMenuUISlider::SendMessageToInteractableObject(UnityEngine.GameObject)
extern "C"  void PanelMenuUISlider_SendMessageToInteractableObject_m650333489 (PanelMenuUISlider_t2320851018 * __this, GameObject_t1756533147 * ___interactableObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
