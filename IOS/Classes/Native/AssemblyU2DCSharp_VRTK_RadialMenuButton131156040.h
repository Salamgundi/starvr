﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.RadialMenuButton
struct  RadialMenuButton_t131156040  : public Il2CppObject
{
public:
	// UnityEngine.Sprite VRTK.RadialMenuButton::ButtonIcon
	Sprite_t309593783 * ___ButtonIcon_0;
	// UnityEngine.Events.UnityEvent VRTK.RadialMenuButton::OnClick
	UnityEvent_t408735097 * ___OnClick_1;
	// UnityEngine.Events.UnityEvent VRTK.RadialMenuButton::OnHold
	UnityEvent_t408735097 * ___OnHold_2;
	// UnityEngine.Events.UnityEvent VRTK.RadialMenuButton::OnHoverEnter
	UnityEvent_t408735097 * ___OnHoverEnter_3;
	// UnityEngine.Events.UnityEvent VRTK.RadialMenuButton::OnHoverExit
	UnityEvent_t408735097 * ___OnHoverExit_4;

public:
	inline static int32_t get_offset_of_ButtonIcon_0() { return static_cast<int32_t>(offsetof(RadialMenuButton_t131156040, ___ButtonIcon_0)); }
	inline Sprite_t309593783 * get_ButtonIcon_0() const { return ___ButtonIcon_0; }
	inline Sprite_t309593783 ** get_address_of_ButtonIcon_0() { return &___ButtonIcon_0; }
	inline void set_ButtonIcon_0(Sprite_t309593783 * value)
	{
		___ButtonIcon_0 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonIcon_0, value);
	}

	inline static int32_t get_offset_of_OnClick_1() { return static_cast<int32_t>(offsetof(RadialMenuButton_t131156040, ___OnClick_1)); }
	inline UnityEvent_t408735097 * get_OnClick_1() const { return ___OnClick_1; }
	inline UnityEvent_t408735097 ** get_address_of_OnClick_1() { return &___OnClick_1; }
	inline void set_OnClick_1(UnityEvent_t408735097 * value)
	{
		___OnClick_1 = value;
		Il2CppCodeGenWriteBarrier(&___OnClick_1, value);
	}

	inline static int32_t get_offset_of_OnHold_2() { return static_cast<int32_t>(offsetof(RadialMenuButton_t131156040, ___OnHold_2)); }
	inline UnityEvent_t408735097 * get_OnHold_2() const { return ___OnHold_2; }
	inline UnityEvent_t408735097 ** get_address_of_OnHold_2() { return &___OnHold_2; }
	inline void set_OnHold_2(UnityEvent_t408735097 * value)
	{
		___OnHold_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnHold_2, value);
	}

	inline static int32_t get_offset_of_OnHoverEnter_3() { return static_cast<int32_t>(offsetof(RadialMenuButton_t131156040, ___OnHoverEnter_3)); }
	inline UnityEvent_t408735097 * get_OnHoverEnter_3() const { return ___OnHoverEnter_3; }
	inline UnityEvent_t408735097 ** get_address_of_OnHoverEnter_3() { return &___OnHoverEnter_3; }
	inline void set_OnHoverEnter_3(UnityEvent_t408735097 * value)
	{
		___OnHoverEnter_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnHoverEnter_3, value);
	}

	inline static int32_t get_offset_of_OnHoverExit_4() { return static_cast<int32_t>(offsetof(RadialMenuButton_t131156040, ___OnHoverExit_4)); }
	inline UnityEvent_t408735097 * get_OnHoverExit_4() const { return ___OnHoverExit_4; }
	inline UnityEvent_t408735097 ** get_address_of_OnHoverExit_4() { return &___OnHoverExit_4; }
	inline void set_OnHoverExit_4(UnityEvent_t408735097 * value)
	{
		___OnHoverExit_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnHoverExit_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
