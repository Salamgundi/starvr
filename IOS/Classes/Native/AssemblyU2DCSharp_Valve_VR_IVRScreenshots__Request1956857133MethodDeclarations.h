﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRScreenshots/_RequestScreenshot
struct _RequestScreenshot_t1956857133;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotError1400268927.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotType611740195.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRScreenshots/_RequestScreenshot::.ctor(System.Object,System.IntPtr)
extern "C"  void _RequestScreenshot__ctor_m2733240852 (_RequestScreenshot_t1956857133 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_RequestScreenshot::Invoke(System.UInt32&,Valve.VR.EVRScreenshotType,System.String,System.String)
extern "C"  int32_t _RequestScreenshot_Invoke_m1504245885 (_RequestScreenshot_t1956857133 * __this, uint32_t* ___pOutScreenshotHandle0, int32_t ___type1, String_t* ___pchPreviewFilename2, String_t* ___pchVRFilename3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRScreenshots/_RequestScreenshot::BeginInvoke(System.UInt32&,Valve.VR.EVRScreenshotType,System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _RequestScreenshot_BeginInvoke_m1291758310 (_RequestScreenshot_t1956857133 * __this, uint32_t* ___pOutScreenshotHandle0, int32_t ___type1, String_t* ___pchPreviewFilename2, String_t* ___pchVRFilename3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_RequestScreenshot::EndInvoke(System.UInt32&,System.IAsyncResult)
extern "C"  int32_t _RequestScreenshot_EndInvoke_m2318681602 (_RequestScreenshot_t1956857133 * __this, uint32_t* ___pOutScreenshotHandle0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
