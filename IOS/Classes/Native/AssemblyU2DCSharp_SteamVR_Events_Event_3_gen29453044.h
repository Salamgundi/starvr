﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen1483099642.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SteamVR_Events/Event`3<UnityEngine.Color,System.Single,System.Boolean>
struct  Event_3_t29453044  : public UnityEvent_3_t1483099642
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
