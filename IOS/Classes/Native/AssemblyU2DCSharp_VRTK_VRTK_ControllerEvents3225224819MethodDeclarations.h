﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// VRTK.ControllerInteractionEventHandler
struct ControllerInteractionEventHandler_t343979916;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventHa343979916.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_VRTKTrackedControllerEventA2407378264.h"

// System.Void VRTK.VRTK_ControllerEvents::.ctor()
extern "C"  void VRTK_ControllerEvents__ctor_m1416054565 (VRTK_ControllerEvents_t3225224819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_TriggerPressed(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_TriggerPressed_m3350143972 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_TriggerPressed(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_TriggerPressed_m3046440801 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_TriggerReleased(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_TriggerReleased_m3231694913 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_TriggerReleased(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_TriggerReleased_m134672058 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_TriggerTouchStart(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_TriggerTouchStart_m341082181 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_TriggerTouchStart(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_TriggerTouchStart_m786776752 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_TriggerTouchEnd(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_TriggerTouchEnd_m249965266 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_TriggerTouchEnd(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_TriggerTouchEnd_m3869899789 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_TriggerHairlineStart(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_TriggerHairlineStart_m187668652 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_TriggerHairlineStart(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_TriggerHairlineStart_m1313120715 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_TriggerHairlineEnd(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_TriggerHairlineEnd_m3042033527 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_TriggerHairlineEnd(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_TriggerHairlineEnd_m869876160 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_TriggerClicked(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_TriggerClicked_m4054561407 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_TriggerClicked(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_TriggerClicked_m2014258294 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_TriggerUnclicked(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_TriggerUnclicked_m1769818634 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_TriggerUnclicked(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_TriggerUnclicked_m3301451599 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_TriggerAxisChanged(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_TriggerAxisChanged_m1620402793 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_TriggerAxisChanged(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_TriggerAxisChanged_m3658170564 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_GripPressed(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_GripPressed_m1566747152 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_GripPressed(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_GripPressed_m3695193465 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_GripReleased(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_GripReleased_m1165937929 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_GripReleased(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_GripReleased_m1632369596 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_GripTouchStart(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_GripTouchStart_m494490717 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_GripTouchStart(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_GripTouchStart_m1157723150 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_GripTouchEnd(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_GripTouchEnd_m2267796566 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_GripTouchEnd(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_GripTouchEnd_m238300077 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_GripHairlineStart(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_GripHairlineStart_m2888571896 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_GripHairlineStart(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_GripHairlineStart_m2673866535 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_GripHairlineEnd(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_GripHairlineEnd_m2443594215 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_GripHairlineEnd(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_GripHairlineEnd_m3428781230 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_GripClicked(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_GripClicked_m337946103 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_GripClicked(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_GripClicked_m3041108536 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_GripUnclicked(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_GripUnclicked_m3192893790 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_GripUnclicked(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_GripUnclicked_m2634804499 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_GripAxisChanged(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_GripAxisChanged_m676928425 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_GripAxisChanged(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_GripAxisChanged_m1478686578 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_TouchpadPressed(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_TouchpadPressed_m2918324572 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_TouchpadPressed(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_TouchpadPressed_m2688965897 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_TouchpadReleased(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_TouchpadReleased_m788405889 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_TouchpadReleased(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_TouchpadReleased_m3468282560 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_TouchpadTouchStart(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_TouchpadTouchStart_m2004867901 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_TouchpadTouchStart(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_TouchpadTouchStart_m2973985338 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_TouchpadTouchEnd(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_TouchpadTouchEnd_m4159394722 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_TouchpadTouchEnd(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_TouchpadTouchEnd_m1310523989 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_TouchpadAxisChanged(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_TouchpadAxisChanged_m3280812681 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_TouchpadAxisChanged(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_TouchpadAxisChanged_m2513257630 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_ButtonOneTouchStart(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_ButtonOneTouchStart_m2957898963 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_ButtonOneTouchStart(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_ButtonOneTouchStart_m3478045144 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_ButtonOneTouchEnd(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_ButtonOneTouchEnd_m3200515934 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_ButtonOneTouchEnd(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_ButtonOneTouchEnd_m3954551315 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_ButtonOnePressed(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_ButtonOnePressed_m1342669304 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_ButtonOnePressed(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_ButtonOnePressed_m3881387963 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_ButtonOneReleased(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_ButtonOneReleased_m3419523127 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_ButtonOneReleased(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_ButtonOneReleased_m4067148862 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_ButtonTwoTouchStart(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_ButtonTwoTouchStart_m1731878391 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_ButtonTwoTouchStart(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_ButtonTwoTouchStart_m2710625494 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_ButtonTwoTouchEnd(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_ButtonTwoTouchEnd_m4270642672 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_ButtonTwoTouchEnd(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_ButtonTwoTouchEnd_m677335511 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_ButtonTwoPressed(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_ButtonTwoPressed_m480784438 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_ButtonTwoPressed(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_ButtonTwoPressed_m1413184039 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_ButtonTwoReleased(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_ButtonTwoReleased_m2506495651 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_ButtonTwoReleased(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_ButtonTwoReleased_m1815905072 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_StartMenuPressed(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_StartMenuPressed_m2024320809 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_StartMenuPressed(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_StartMenuPressed_m4004201080 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_StartMenuReleased(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_StartMenuReleased_m646893094 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_StartMenuReleased(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_StartMenuReleased_m3449797025 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_AliasPointerOn(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_AliasPointerOn_m2282926346 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_AliasPointerOn(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_AliasPointerOn_m294857207 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_AliasPointerOff(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_AliasPointerOff_m2248934096 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_AliasPointerOff(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_AliasPointerOff_m1232411629 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_AliasPointerSet(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_AliasPointerSet_m3906565799 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_AliasPointerSet(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_AliasPointerSet_m16188262 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_AliasGrabOn(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_AliasGrabOn_m2311283857 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_AliasGrabOn(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_AliasGrabOn_m403268926 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_AliasGrabOff(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_AliasGrabOff_m2719527059 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_AliasGrabOff(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_AliasGrabOff_m1316403116 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_AliasUseOn(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_AliasUseOn_m139473816 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_AliasUseOn(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_AliasUseOn_m1225248827 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_AliasUseOff(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_AliasUseOff_m2957470898 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_AliasUseOff(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_AliasUseOff_m3703992997 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_AliasMenuOn(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_AliasMenuOn_m2488590552 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_AliasMenuOn(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_AliasMenuOn_m3990053571 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_AliasMenuOff(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_AliasMenuOff_m4069745402 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_AliasMenuOff(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_AliasMenuOff_m1948970897 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_AliasUIClickOn(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_AliasUIClickOn_m3374814457 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_AliasUIClickOn(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_AliasUIClickOn_m2740789896 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_AliasUIClickOff(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_AliasUIClickOff_m2882585819 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_AliasUIClickOff(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_AliasUIClickOff_m2436092426 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_ControllerEnabled(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_ControllerEnabled_m36486785 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_ControllerEnabled(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_ControllerEnabled_m1919413348 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_ControllerDisabled(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_ControllerDisabled_m1734740454 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_ControllerDisabled(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_ControllerDisabled_m305314583 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::add_ControllerIndexChanged(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_add_ControllerIndexChanged_m1916801958 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::remove_ControllerIndexChanged(VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_remove_ControllerIndexChanged_m3544724939 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventHandler_t343979916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnTriggerPressed(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnTriggerPressed_m2965138846 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnTriggerReleased(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnTriggerReleased_m2030305037 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnTriggerTouchStart(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnTriggerTouchStart_m1145478737 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnTriggerTouchEnd(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnTriggerTouchEnd_m2174461562 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnTriggerHairlineStart(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnTriggerHairlineStart_m3505181826 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnTriggerHairlineEnd(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnTriggerHairlineEnd_m2071046187 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnTriggerClicked(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnTriggerClicked_m4209870387 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnTriggerUnclicked(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnTriggerUnclicked_m3353287556 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnTriggerAxisChanged(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnTriggerAxisChanged_m574297413 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnGripPressed(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnGripPressed_m1616838616 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnGripReleased(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnGripReleased_m1754288005 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnGripTouchStart(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnGripTouchStart_m2130301425 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnGripTouchEnd(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnGripTouchEnd_m2688483116 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnGripHairlineStart(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnGripHairlineStart_m1250276100 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnGripHairlineEnd(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnGripHairlineEnd_m1428455055 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnGripClicked(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnGripClicked_m904533359 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnGripUnclicked(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnGripUnclicked_m4249375370 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnGripAxisChanged(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnGripAxisChanged_m2139116725 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnTouchpadPressed(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnTouchpadPressed_m1081317972 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnTouchpadReleased(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnTouchpadReleased_m14643301 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnTouchpadTouchStart(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnTouchpadTouchStart_m929261545 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnTouchpadTouchEnd(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnTouchpadTouchEnd_m3452866576 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnTouchpadAxisChanged(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnTouchpadAxisChanged_m2236695869 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnButtonOneTouchStart(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnButtonOneTouchStart_m3653177563 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnButtonOneTouchEnd(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnButtonOneTouchEnd_m2216037066 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnButtonOnePressed(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnButtonOnePressed_m1916115662 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnButtonOneReleased(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnButtonOneReleased_m2997182847 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnButtonTwoTouchStart(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnButtonTwoTouchStart_m778766367 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnButtonTwoTouchEnd(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnButtonTwoTouchEnd_m3382120956 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnButtonTwoPressed(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnButtonTwoPressed_m2446866608 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnButtonTwoReleased(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnButtonTwoReleased_m958334923 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnStartMenuPressed(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnStartMenuPressed_m1175126141 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnStartMenuReleased(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnStartMenuReleased_m3157690670 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnAliasPointerOn(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnAliasPointerOn_m3794261092 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnAliasPointerOff(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnAliasPointerOff_m1632796104 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnAliasPointerSet(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnAliasPointerSet_m4215127567 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnAliasGrabOn(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnAliasGrabOn_m641809221 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnAliasGrabOff(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnAliasGrabOff_m1577142063 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnAliasUseOn(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnAliasUseOn_m24093198 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnAliasUseOff(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnAliasUseOff_m183122074 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnAliasUIClickOn(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnAliasUIClickOn_m3851969485 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnAliasUIClickOff(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnAliasUIClickOff_m1389829315 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnAliasMenuOn(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnAliasMenuOn_m425335724 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnAliasMenuOff(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnAliasMenuOff_m1828930376 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnControllerEnabled(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnControllerEnabled_m3164330445 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnControllerDisabled(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnControllerDisabled_m3170019296 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnControllerIndexChanged(VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_OnControllerIndexChanged_m1914628360 (VRTK_ControllerEvents_t3225224819 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_ControllerEvents::GetVelocity()
extern "C"  Vector3_t2243707580  VRTK_ControllerEvents_GetVelocity_m2922886522 (VRTK_ControllerEvents_t3225224819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_ControllerEvents::GetAngularVelocity()
extern "C"  Vector3_t2243707580  VRTK_ControllerEvents_GetAngularVelocity_m3342794332 (VRTK_ControllerEvents_t3225224819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 VRTK.VRTK_ControllerEvents::GetTouchpadAxis()
extern "C"  Vector2_t2243707579  VRTK_ControllerEvents_GetTouchpadAxis_m84603443 (VRTK_ControllerEvents_t3225224819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_ControllerEvents::GetTouchpadAxisAngle()
extern "C"  float VRTK_ControllerEvents_GetTouchpadAxisAngle_m84128183 (VRTK_ControllerEvents_t3225224819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_ControllerEvents::GetTriggerAxis()
extern "C"  float VRTK_ControllerEvents_GetTriggerAxis_m1305465280 (VRTK_ControllerEvents_t3225224819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_ControllerEvents::GetGripAxis()
extern "C"  float VRTK_ControllerEvents_GetGripAxis_m3880657916 (VRTK_ControllerEvents_t3225224819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_ControllerEvents::GetHairTriggerDelta()
extern "C"  float VRTK_ControllerEvents_GetHairTriggerDelta_m1881286271 (VRTK_ControllerEvents_t3225224819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_ControllerEvents::GetHairGripDelta()
extern "C"  float VRTK_ControllerEvents_GetHairGripDelta_m657368847 (VRTK_ControllerEvents_t3225224819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_ControllerEvents::AnyButtonPressed()
extern "C"  bool VRTK_ControllerEvents_AnyButtonPressed_m2759378727 (VRTK_ControllerEvents_t3225224819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_ControllerEvents::IsButtonPressed(VRTK.VRTK_ControllerEvents/ButtonAlias)
extern "C"  bool VRTK_ControllerEvents_IsButtonPressed_m407246062 (VRTK_ControllerEvents_t3225224819 * __this, int32_t ___button0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::SubscribeToButtonAliasEvent(VRTK.VRTK_ControllerEvents/ButtonAlias,System.Boolean,VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_SubscribeToButtonAliasEvent_m3516497283 (VRTK_ControllerEvents_t3225224819 * __this, int32_t ___givenButton0, bool ___startEvent1, ControllerInteractionEventHandler_t343979916 * ___callbackMethod2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::UnsubscribeToButtonAliasEvent(VRTK.VRTK_ControllerEvents/ButtonAlias,System.Boolean,VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_UnsubscribeToButtonAliasEvent_m3645672408 (VRTK_ControllerEvents_t3225224819 * __this, int32_t ___givenButton0, bool ___startEvent1, ControllerInteractionEventHandler_t343979916 * ___callbackMethod2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnEnable()
extern "C"  void VRTK_ControllerEvents_OnEnable_m1620943741 (VRTK_ControllerEvents_t3225224819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::OnDisable()
extern "C"  void VRTK_ControllerEvents_OnDisable_m1735913936 (VRTK_ControllerEvents_t3225224819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::Update()
extern "C"  void VRTK_ControllerEvents_Update_m4268025990 (VRTK_ControllerEvents_t3225224819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::ButtonAliasEventSubscription(System.Boolean,VRTK.VRTK_ControllerEvents/ButtonAlias,System.Boolean,VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ControllerEvents_ButtonAliasEventSubscription_m3034853300 (VRTK_ControllerEvents_t3225224819 * __this, bool ___subscribe0, int32_t ___givenButton1, bool ___startEvent2, ControllerInteractionEventHandler_t343979916 * ___callbackMethod3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.ControllerInteractionEventArgs VRTK.VRTK_ControllerEvents::SetButtonEvent(System.Boolean&,System.Boolean,System.Single)
extern "C"  ControllerInteractionEventArgs_t287637539  VRTK_ControllerEvents_SetButtonEvent_m2635691401 (VRTK_ControllerEvents_t3225224819 * __this, bool* ___buttonBool0, bool ___value1, float ___buttonPressure2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::TrackedControllerEnabled(System.Object,VRTK.VRTKTrackedControllerEventArgs)
extern "C"  void VRTK_ControllerEvents_TrackedControllerEnabled_m1317122995 (VRTK_ControllerEvents_t3225224819 * __this, Il2CppObject * ___sender0, VRTKTrackedControllerEventArgs_t2407378264  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::TrackedControllerDisabled(System.Object,VRTK.VRTKTrackedControllerEventArgs)
extern "C"  void VRTK_ControllerEvents_TrackedControllerDisabled_m3986327638 (VRTK_ControllerEvents_t3225224819 * __this, Il2CppObject * ___sender0, VRTKTrackedControllerEventArgs_t2407378264  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::TrackedControllerIndexChanged(System.Object,VRTK.VRTKTrackedControllerEventArgs)
extern "C"  void VRTK_ControllerEvents_TrackedControllerIndexChanged_m854151514 (VRTK_ControllerEvents_t3225224819 * __this, Il2CppObject * ___sender0, VRTKTrackedControllerEventArgs_t2407378264  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_ControllerEvents::CalculateTouchpadAxisAngle(UnityEngine.Vector2)
extern "C"  float VRTK_ControllerEvents_CalculateTouchpadAxisAngle_m3278491213 (VRTK_ControllerEvents_t3225224819 * __this, Vector2_t2243707579  ___axis0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::EmitAlias(VRTK.VRTK_ControllerEvents/ButtonAlias,System.Boolean,System.Single,System.Boolean&)
extern "C"  void VRTK_ControllerEvents_EmitAlias_m1346030644 (VRTK_ControllerEvents_t3225224819 * __this, int32_t ___type0, bool ___touchDown1, float ___buttonPressure2, bool* ___buttonBool3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_ControllerEvents::Vector2ShallowEquals(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool VRTK_ControllerEvents_Vector2ShallowEquals_m228767767 (VRTK_ControllerEvents_t3225224819 * __this, Vector2_t2243707579  ___vectorA0, Vector2_t2243707579  ___vectorB1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerEvents::DisableEvents()
extern "C"  void VRTK_ControllerEvents_DisableEvents_m4146803410 (VRTK_ControllerEvents_t3225224819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
