﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Action`1<System.Single>
struct Action_1_t3534888517;
// SteamVR_Events/Event`1<System.Single>
struct Event_1_t4251932349;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t3443095683;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Events/Action`1<System.Single>::.ctor(SteamVR_Events/Event`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Action_1__ctor_m2638676342_gshared (Action_1_t3534888517 * __this, Event_1_t4251932349 * ____event0, UnityAction_1_t3443095683 * ___action1, const MethodInfo* method);
#define Action_1__ctor_m2638676342(__this, ____event0, ___action1, method) ((  void (*) (Action_1_t3534888517 *, Event_1_t4251932349 *, UnityAction_1_t3443095683 *, const MethodInfo*))Action_1__ctor_m2638676342_gshared)(__this, ____event0, ___action1, method)
// System.Void SteamVR_Events/Action`1<System.Single>::Enable(System.Boolean)
extern "C"  void Action_1_Enable_m3309652672_gshared (Action_1_t3534888517 * __this, bool ___enabled0, const MethodInfo* method);
#define Action_1_Enable_m3309652672(__this, ___enabled0, method) ((  void (*) (Action_1_t3534888517 *, bool, const MethodInfo*))Action_1_Enable_m3309652672_gshared)(__this, ___enabled0, method)
