﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Controller_Hand
struct Controller_Hand_t3658229830;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"

// System.Void VRTK.Examples.Controller_Hand::.ctor()
extern "C"  void Controller_Hand__ctor_m3276087023 (Controller_Hand_t3658229830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Controller_Hand::Start()
extern "C"  void Controller_Hand_Start_m280140363 (Controller_Hand_t3658229830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Controller_Hand::InversePosition(UnityEngine.Transform)
extern "C"  void Controller_Hand_InversePosition_m2325043197 (Controller_Hand_t3658229830 * __this, Transform_t3275118058 * ___givenTransform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Controller_Hand::DoGrabOn(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void Controller_Hand_DoGrabOn_m1562948133 (Controller_Hand_t3658229830 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Controller_Hand::DoGrabOff(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void Controller_Hand_DoGrabOff_m892677859 (Controller_Hand_t3658229830 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Controller_Hand::DoUseOn(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void Controller_Hand_DoUseOn_m1308866418 (Controller_Hand_t3658229830 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Controller_Hand::DoUseOff(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void Controller_Hand_DoUseOff_m1259237518 (Controller_Hand_t3658229830 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Controller_Hand::Update()
extern "C"  void Controller_Hand_Update_m4259124866 (Controller_Hand_t3658229830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
