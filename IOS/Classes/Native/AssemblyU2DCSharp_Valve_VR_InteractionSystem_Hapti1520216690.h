﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.LinearMapping
struct LinearMapping_t810676855;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.HapticRack
struct  HapticRack_t1520216690  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.LinearMapping Valve.VR.InteractionSystem.HapticRack::linearMapping
	LinearMapping_t810676855 * ___linearMapping_2;
	// System.Int32 Valve.VR.InteractionSystem.HapticRack::teethCount
	int32_t ___teethCount_3;
	// System.Int32 Valve.VR.InteractionSystem.HapticRack::minimumPulseDuration
	int32_t ___minimumPulseDuration_4;
	// System.Int32 Valve.VR.InteractionSystem.HapticRack::maximumPulseDuration
	int32_t ___maximumPulseDuration_5;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.HapticRack::onPulse
	UnityEvent_t408735097 * ___onPulse_6;
	// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.HapticRack::hand
	Hand_t379716353 * ___hand_7;
	// System.Int32 Valve.VR.InteractionSystem.HapticRack::previousToothIndex
	int32_t ___previousToothIndex_8;

public:
	inline static int32_t get_offset_of_linearMapping_2() { return static_cast<int32_t>(offsetof(HapticRack_t1520216690, ___linearMapping_2)); }
	inline LinearMapping_t810676855 * get_linearMapping_2() const { return ___linearMapping_2; }
	inline LinearMapping_t810676855 ** get_address_of_linearMapping_2() { return &___linearMapping_2; }
	inline void set_linearMapping_2(LinearMapping_t810676855 * value)
	{
		___linearMapping_2 = value;
		Il2CppCodeGenWriteBarrier(&___linearMapping_2, value);
	}

	inline static int32_t get_offset_of_teethCount_3() { return static_cast<int32_t>(offsetof(HapticRack_t1520216690, ___teethCount_3)); }
	inline int32_t get_teethCount_3() const { return ___teethCount_3; }
	inline int32_t* get_address_of_teethCount_3() { return &___teethCount_3; }
	inline void set_teethCount_3(int32_t value)
	{
		___teethCount_3 = value;
	}

	inline static int32_t get_offset_of_minimumPulseDuration_4() { return static_cast<int32_t>(offsetof(HapticRack_t1520216690, ___minimumPulseDuration_4)); }
	inline int32_t get_minimumPulseDuration_4() const { return ___minimumPulseDuration_4; }
	inline int32_t* get_address_of_minimumPulseDuration_4() { return &___minimumPulseDuration_4; }
	inline void set_minimumPulseDuration_4(int32_t value)
	{
		___minimumPulseDuration_4 = value;
	}

	inline static int32_t get_offset_of_maximumPulseDuration_5() { return static_cast<int32_t>(offsetof(HapticRack_t1520216690, ___maximumPulseDuration_5)); }
	inline int32_t get_maximumPulseDuration_5() const { return ___maximumPulseDuration_5; }
	inline int32_t* get_address_of_maximumPulseDuration_5() { return &___maximumPulseDuration_5; }
	inline void set_maximumPulseDuration_5(int32_t value)
	{
		___maximumPulseDuration_5 = value;
	}

	inline static int32_t get_offset_of_onPulse_6() { return static_cast<int32_t>(offsetof(HapticRack_t1520216690, ___onPulse_6)); }
	inline UnityEvent_t408735097 * get_onPulse_6() const { return ___onPulse_6; }
	inline UnityEvent_t408735097 ** get_address_of_onPulse_6() { return &___onPulse_6; }
	inline void set_onPulse_6(UnityEvent_t408735097 * value)
	{
		___onPulse_6 = value;
		Il2CppCodeGenWriteBarrier(&___onPulse_6, value);
	}

	inline static int32_t get_offset_of_hand_7() { return static_cast<int32_t>(offsetof(HapticRack_t1520216690, ___hand_7)); }
	inline Hand_t379716353 * get_hand_7() const { return ___hand_7; }
	inline Hand_t379716353 ** get_address_of_hand_7() { return &___hand_7; }
	inline void set_hand_7(Hand_t379716353 * value)
	{
		___hand_7 = value;
		Il2CppCodeGenWriteBarrier(&___hand_7, value);
	}

	inline static int32_t get_offset_of_previousToothIndex_8() { return static_cast<int32_t>(offsetof(HapticRack_t1520216690, ___previousToothIndex_8)); }
	inline int32_t get_previousToothIndex_8() const { return ___previousToothIndex_8; }
	inline int32_t* get_address_of_previousToothIndex_8() { return &___previousToothIndex_8; }
	inline void set_previousToothIndex_8(int32_t value)
	{
		___previousToothIndex_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
