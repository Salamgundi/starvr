﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetDefaultApplicationForMimeType
struct _GetDefaultApplicationForMimeType_t1319680284;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetDefaultApplicationForMimeType::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetDefaultApplicationForMimeType__ctor_m3802075343 (_GetDefaultApplicationForMimeType_t1319680284 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRApplications/_GetDefaultApplicationForMimeType::Invoke(System.String,System.String,System.UInt32)
extern "C"  bool _GetDefaultApplicationForMimeType_Invoke_m3296238731 (_GetDefaultApplicationForMimeType_t1319680284 * __this, String_t* ___pchMimeType0, String_t* ___pchAppKeyBuffer1, uint32_t ___unAppKeyBufferLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetDefaultApplicationForMimeType::BeginInvoke(System.String,System.String,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetDefaultApplicationForMimeType_BeginInvoke_m245680678 (_GetDefaultApplicationForMimeType_t1319680284 * __this, String_t* ___pchMimeType0, String_t* ___pchAppKeyBuffer1, uint32_t ___unAppKeyBufferLen2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRApplications/_GetDefaultApplicationForMimeType::EndInvoke(System.IAsyncResult)
extern "C"  bool _GetDefaultApplicationForMimeType_EndInvoke_m707879113 (_GetDefaultApplicationForMimeType_t1319680284 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
