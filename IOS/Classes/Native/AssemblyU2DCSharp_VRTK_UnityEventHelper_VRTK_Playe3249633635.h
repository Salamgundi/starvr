﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_PlayerClimb
struct VRTK_PlayerClimb_t322130288;
// VRTK.UnityEventHelper.VRTK_PlayerClimb_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t1820762702;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_PlayerClimb_UnityEvents
struct  VRTK_PlayerClimb_UnityEvents_t3249633635  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_PlayerClimb VRTK.UnityEventHelper.VRTK_PlayerClimb_UnityEvents::pc
	VRTK_PlayerClimb_t322130288 * ___pc_2;
	// VRTK.UnityEventHelper.VRTK_PlayerClimb_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_PlayerClimb_UnityEvents::OnPlayerClimbStarted
	UnityObjectEvent_t1820762702 * ___OnPlayerClimbStarted_3;
	// VRTK.UnityEventHelper.VRTK_PlayerClimb_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_PlayerClimb_UnityEvents::OnPlayerClimbEnded
	UnityObjectEvent_t1820762702 * ___OnPlayerClimbEnded_4;

public:
	inline static int32_t get_offset_of_pc_2() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_UnityEvents_t3249633635, ___pc_2)); }
	inline VRTK_PlayerClimb_t322130288 * get_pc_2() const { return ___pc_2; }
	inline VRTK_PlayerClimb_t322130288 ** get_address_of_pc_2() { return &___pc_2; }
	inline void set_pc_2(VRTK_PlayerClimb_t322130288 * value)
	{
		___pc_2 = value;
		Il2CppCodeGenWriteBarrier(&___pc_2, value);
	}

	inline static int32_t get_offset_of_OnPlayerClimbStarted_3() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_UnityEvents_t3249633635, ___OnPlayerClimbStarted_3)); }
	inline UnityObjectEvent_t1820762702 * get_OnPlayerClimbStarted_3() const { return ___OnPlayerClimbStarted_3; }
	inline UnityObjectEvent_t1820762702 ** get_address_of_OnPlayerClimbStarted_3() { return &___OnPlayerClimbStarted_3; }
	inline void set_OnPlayerClimbStarted_3(UnityObjectEvent_t1820762702 * value)
	{
		___OnPlayerClimbStarted_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnPlayerClimbStarted_3, value);
	}

	inline static int32_t get_offset_of_OnPlayerClimbEnded_4() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_UnityEvents_t3249633635, ___OnPlayerClimbEnded_4)); }
	inline UnityObjectEvent_t1820762702 * get_OnPlayerClimbEnded_4() const { return ___OnPlayerClimbEnded_4; }
	inline UnityObjectEvent_t1820762702 ** get_address_of_OnPlayerClimbEnded_4() { return &___OnPlayerClimbEnded_4; }
	inline void set_OnPlayerClimbEnded_4(UnityObjectEvent_t1820762702 * value)
	{
		___OnPlayerClimbEnded_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnPlayerClimbEnded_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
