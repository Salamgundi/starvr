﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.HingeJoint
struct HingeJoint_t2745110831;

#include "AssemblyU2DCSharp_VRTK_VRTK_Control651619021.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Wheel_GrabTypes504515700.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Wheel
struct  VRTK_Wheel_t803354859  : public VRTK_Control_t651619021
{
public:
	// UnityEngine.GameObject VRTK.VRTK_Wheel::connectedTo
	GameObject_t1756533147 * ___connectedTo_15;
	// VRTK.VRTK_Wheel/GrabTypes VRTK.VRTK_Wheel::grabType
	int32_t ___grabType_16;
	// System.Single VRTK.VRTK_Wheel::detatchDistance
	float ___detatchDistance_17;
	// System.Single VRTK.VRTK_Wheel::minimumValue
	float ___minimumValue_18;
	// System.Single VRTK.VRTK_Wheel::maximumValue
	float ___maximumValue_19;
	// System.Single VRTK.VRTK_Wheel::stepSize
	float ___stepSize_20;
	// System.Boolean VRTK.VRTK_Wheel::snapToStep
	bool ___snapToStep_21;
	// System.Single VRTK.VRTK_Wheel::grabbedFriction
	float ___grabbedFriction_22;
	// System.Single VRTK.VRTK_Wheel::releasedFriction
	float ___releasedFriction_23;
	// System.Single VRTK.VRTK_Wheel::maxAngle
	float ___maxAngle_24;
	// System.Boolean VRTK.VRTK_Wheel::lockAtLimits
	bool ___lockAtLimits_25;
	// System.Single VRTK.VRTK_Wheel::angularVelocityLimit
	float ___angularVelocityLimit_26;
	// System.Single VRTK.VRTK_Wheel::springStrengthValue
	float ___springStrengthValue_27;
	// System.Single VRTK.VRTK_Wheel::springDamperValue
	float ___springDamperValue_28;
	// UnityEngine.Quaternion VRTK.VRTK_Wheel::initialLocalRotation
	Quaternion_t4030073918  ___initialLocalRotation_29;
	// UnityEngine.Rigidbody VRTK.VRTK_Wheel::wheelRigidbody
	Rigidbody_t4233889191 * ___wheelRigidbody_30;
	// UnityEngine.HingeJoint VRTK.VRTK_Wheel::wheelHinge
	HingeJoint_t2745110831 * ___wheelHinge_31;
	// System.Boolean VRTK.VRTK_Wheel::wheelHingeCreated
	bool ___wheelHingeCreated_32;
	// System.Boolean VRTK.VRTK_Wheel::initialValueCalculated
	bool ___initialValueCalculated_33;
	// System.Single VRTK.VRTK_Wheel::springAngle
	float ___springAngle_34;

public:
	inline static int32_t get_offset_of_connectedTo_15() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___connectedTo_15)); }
	inline GameObject_t1756533147 * get_connectedTo_15() const { return ___connectedTo_15; }
	inline GameObject_t1756533147 ** get_address_of_connectedTo_15() { return &___connectedTo_15; }
	inline void set_connectedTo_15(GameObject_t1756533147 * value)
	{
		___connectedTo_15 = value;
		Il2CppCodeGenWriteBarrier(&___connectedTo_15, value);
	}

	inline static int32_t get_offset_of_grabType_16() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___grabType_16)); }
	inline int32_t get_grabType_16() const { return ___grabType_16; }
	inline int32_t* get_address_of_grabType_16() { return &___grabType_16; }
	inline void set_grabType_16(int32_t value)
	{
		___grabType_16 = value;
	}

	inline static int32_t get_offset_of_detatchDistance_17() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___detatchDistance_17)); }
	inline float get_detatchDistance_17() const { return ___detatchDistance_17; }
	inline float* get_address_of_detatchDistance_17() { return &___detatchDistance_17; }
	inline void set_detatchDistance_17(float value)
	{
		___detatchDistance_17 = value;
	}

	inline static int32_t get_offset_of_minimumValue_18() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___minimumValue_18)); }
	inline float get_minimumValue_18() const { return ___minimumValue_18; }
	inline float* get_address_of_minimumValue_18() { return &___minimumValue_18; }
	inline void set_minimumValue_18(float value)
	{
		___minimumValue_18 = value;
	}

	inline static int32_t get_offset_of_maximumValue_19() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___maximumValue_19)); }
	inline float get_maximumValue_19() const { return ___maximumValue_19; }
	inline float* get_address_of_maximumValue_19() { return &___maximumValue_19; }
	inline void set_maximumValue_19(float value)
	{
		___maximumValue_19 = value;
	}

	inline static int32_t get_offset_of_stepSize_20() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___stepSize_20)); }
	inline float get_stepSize_20() const { return ___stepSize_20; }
	inline float* get_address_of_stepSize_20() { return &___stepSize_20; }
	inline void set_stepSize_20(float value)
	{
		___stepSize_20 = value;
	}

	inline static int32_t get_offset_of_snapToStep_21() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___snapToStep_21)); }
	inline bool get_snapToStep_21() const { return ___snapToStep_21; }
	inline bool* get_address_of_snapToStep_21() { return &___snapToStep_21; }
	inline void set_snapToStep_21(bool value)
	{
		___snapToStep_21 = value;
	}

	inline static int32_t get_offset_of_grabbedFriction_22() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___grabbedFriction_22)); }
	inline float get_grabbedFriction_22() const { return ___grabbedFriction_22; }
	inline float* get_address_of_grabbedFriction_22() { return &___grabbedFriction_22; }
	inline void set_grabbedFriction_22(float value)
	{
		___grabbedFriction_22 = value;
	}

	inline static int32_t get_offset_of_releasedFriction_23() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___releasedFriction_23)); }
	inline float get_releasedFriction_23() const { return ___releasedFriction_23; }
	inline float* get_address_of_releasedFriction_23() { return &___releasedFriction_23; }
	inline void set_releasedFriction_23(float value)
	{
		___releasedFriction_23 = value;
	}

	inline static int32_t get_offset_of_maxAngle_24() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___maxAngle_24)); }
	inline float get_maxAngle_24() const { return ___maxAngle_24; }
	inline float* get_address_of_maxAngle_24() { return &___maxAngle_24; }
	inline void set_maxAngle_24(float value)
	{
		___maxAngle_24 = value;
	}

	inline static int32_t get_offset_of_lockAtLimits_25() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___lockAtLimits_25)); }
	inline bool get_lockAtLimits_25() const { return ___lockAtLimits_25; }
	inline bool* get_address_of_lockAtLimits_25() { return &___lockAtLimits_25; }
	inline void set_lockAtLimits_25(bool value)
	{
		___lockAtLimits_25 = value;
	}

	inline static int32_t get_offset_of_angularVelocityLimit_26() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___angularVelocityLimit_26)); }
	inline float get_angularVelocityLimit_26() const { return ___angularVelocityLimit_26; }
	inline float* get_address_of_angularVelocityLimit_26() { return &___angularVelocityLimit_26; }
	inline void set_angularVelocityLimit_26(float value)
	{
		___angularVelocityLimit_26 = value;
	}

	inline static int32_t get_offset_of_springStrengthValue_27() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___springStrengthValue_27)); }
	inline float get_springStrengthValue_27() const { return ___springStrengthValue_27; }
	inline float* get_address_of_springStrengthValue_27() { return &___springStrengthValue_27; }
	inline void set_springStrengthValue_27(float value)
	{
		___springStrengthValue_27 = value;
	}

	inline static int32_t get_offset_of_springDamperValue_28() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___springDamperValue_28)); }
	inline float get_springDamperValue_28() const { return ___springDamperValue_28; }
	inline float* get_address_of_springDamperValue_28() { return &___springDamperValue_28; }
	inline void set_springDamperValue_28(float value)
	{
		___springDamperValue_28 = value;
	}

	inline static int32_t get_offset_of_initialLocalRotation_29() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___initialLocalRotation_29)); }
	inline Quaternion_t4030073918  get_initialLocalRotation_29() const { return ___initialLocalRotation_29; }
	inline Quaternion_t4030073918 * get_address_of_initialLocalRotation_29() { return &___initialLocalRotation_29; }
	inline void set_initialLocalRotation_29(Quaternion_t4030073918  value)
	{
		___initialLocalRotation_29 = value;
	}

	inline static int32_t get_offset_of_wheelRigidbody_30() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___wheelRigidbody_30)); }
	inline Rigidbody_t4233889191 * get_wheelRigidbody_30() const { return ___wheelRigidbody_30; }
	inline Rigidbody_t4233889191 ** get_address_of_wheelRigidbody_30() { return &___wheelRigidbody_30; }
	inline void set_wheelRigidbody_30(Rigidbody_t4233889191 * value)
	{
		___wheelRigidbody_30 = value;
		Il2CppCodeGenWriteBarrier(&___wheelRigidbody_30, value);
	}

	inline static int32_t get_offset_of_wheelHinge_31() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___wheelHinge_31)); }
	inline HingeJoint_t2745110831 * get_wheelHinge_31() const { return ___wheelHinge_31; }
	inline HingeJoint_t2745110831 ** get_address_of_wheelHinge_31() { return &___wheelHinge_31; }
	inline void set_wheelHinge_31(HingeJoint_t2745110831 * value)
	{
		___wheelHinge_31 = value;
		Il2CppCodeGenWriteBarrier(&___wheelHinge_31, value);
	}

	inline static int32_t get_offset_of_wheelHingeCreated_32() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___wheelHingeCreated_32)); }
	inline bool get_wheelHingeCreated_32() const { return ___wheelHingeCreated_32; }
	inline bool* get_address_of_wheelHingeCreated_32() { return &___wheelHingeCreated_32; }
	inline void set_wheelHingeCreated_32(bool value)
	{
		___wheelHingeCreated_32 = value;
	}

	inline static int32_t get_offset_of_initialValueCalculated_33() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___initialValueCalculated_33)); }
	inline bool get_initialValueCalculated_33() const { return ___initialValueCalculated_33; }
	inline bool* get_address_of_initialValueCalculated_33() { return &___initialValueCalculated_33; }
	inline void set_initialValueCalculated_33(bool value)
	{
		___initialValueCalculated_33 = value;
	}

	inline static int32_t get_offset_of_springAngle_34() { return static_cast<int32_t>(offsetof(VRTK_Wheel_t803354859, ___springAngle_34)); }
	inline float get_springAngle_34() const { return ___springAngle_34; }
	inline float* get_address_of_springAngle_34() { return &___springAngle_34; }
	inline void set_springAngle_34(float value)
	{
		___springAngle_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
