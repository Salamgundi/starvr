﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.MaterialPropertyBlock>
struct Dictionary_2_t923460923;

#include "AssemblyU2DCSharp_VRTK_Highlighters_VRTK_MaterialC2305160438.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter
struct  VRTK_MaterialPropertyBlockColorSwapHighlighter_t2327852568  : public VRTK_MaterialColorSwapHighlighter_t2305160438
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.MaterialPropertyBlock> VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter::originalMaterialPropertyBlocks
	Dictionary_2_t923460923 * ___originalMaterialPropertyBlocks_11;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.MaterialPropertyBlock> VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter::highlightMaterialPropertyBlocks
	Dictionary_2_t923460923 * ___highlightMaterialPropertyBlocks_12;

public:
	inline static int32_t get_offset_of_originalMaterialPropertyBlocks_11() { return static_cast<int32_t>(offsetof(VRTK_MaterialPropertyBlockColorSwapHighlighter_t2327852568, ___originalMaterialPropertyBlocks_11)); }
	inline Dictionary_2_t923460923 * get_originalMaterialPropertyBlocks_11() const { return ___originalMaterialPropertyBlocks_11; }
	inline Dictionary_2_t923460923 ** get_address_of_originalMaterialPropertyBlocks_11() { return &___originalMaterialPropertyBlocks_11; }
	inline void set_originalMaterialPropertyBlocks_11(Dictionary_2_t923460923 * value)
	{
		___originalMaterialPropertyBlocks_11 = value;
		Il2CppCodeGenWriteBarrier(&___originalMaterialPropertyBlocks_11, value);
	}

	inline static int32_t get_offset_of_highlightMaterialPropertyBlocks_12() { return static_cast<int32_t>(offsetof(VRTK_MaterialPropertyBlockColorSwapHighlighter_t2327852568, ___highlightMaterialPropertyBlocks_12)); }
	inline Dictionary_2_t923460923 * get_highlightMaterialPropertyBlocks_12() const { return ___highlightMaterialPropertyBlocks_12; }
	inline Dictionary_2_t923460923 ** get_address_of_highlightMaterialPropertyBlocks_12() { return &___highlightMaterialPropertyBlocks_12; }
	inline void set_highlightMaterialPropertyBlocks_12(Dictionary_2_t923460923 * value)
	{
		___highlightMaterialPropertyBlocks_12 = value;
		Il2CppCodeGenWriteBarrier(&___highlightMaterialPropertyBlocks_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
