﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRScreenshots
struct CVRScreenshots_t3241040508;
// System.String
struct String_t;
// Valve.VR.EVRScreenshotType[]
struct EVRScreenshotTypeU5BU5D_t2594501106;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotError1400268927.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotType611740195.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotPropertyFile29427162.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"

// System.Void Valve.VR.CVRScreenshots::.ctor(System.IntPtr)
extern "C"  void CVRScreenshots__ctor_m3873632701 (CVRScreenshots_t3241040508 * __this, IntPtr_t ___pInterface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotError Valve.VR.CVRScreenshots::RequestScreenshot(System.UInt32&,Valve.VR.EVRScreenshotType,System.String,System.String)
extern "C"  int32_t CVRScreenshots_RequestScreenshot_m423790327 (CVRScreenshots_t3241040508 * __this, uint32_t* ___pOutScreenshotHandle0, int32_t ___type1, String_t* ___pchPreviewFilename2, String_t* ___pchVRFilename3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotError Valve.VR.CVRScreenshots::HookScreenshot(Valve.VR.EVRScreenshotType[])
extern "C"  int32_t CVRScreenshots_HookScreenshot_m2096721201 (CVRScreenshots_t3241040508 * __this, EVRScreenshotTypeU5BU5D_t2594501106* ___pSupportedTypes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotType Valve.VR.CVRScreenshots::GetScreenshotPropertyType(System.UInt32,Valve.VR.EVRScreenshotError&)
extern "C"  int32_t CVRScreenshots_GetScreenshotPropertyType_m2406802155 (CVRScreenshots_t3241040508 * __this, uint32_t ___screenshotHandle0, int32_t* ___pError1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRScreenshots::GetScreenshotPropertyFilename(System.UInt32,Valve.VR.EVRScreenshotPropertyFilenames,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRScreenshotError&)
extern "C"  uint32_t CVRScreenshots_GetScreenshotPropertyFilename_m858491191 (CVRScreenshots_t3241040508 * __this, uint32_t ___screenshotHandle0, int32_t ___filenameType1, StringBuilder_t1221177846 * ___pchFilename2, uint32_t ___cchFilename3, int32_t* ___pError4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotError Valve.VR.CVRScreenshots::UpdateScreenshotProgress(System.UInt32,System.Single)
extern "C"  int32_t CVRScreenshots_UpdateScreenshotProgress_m93420444 (CVRScreenshots_t3241040508 * __this, uint32_t ___screenshotHandle0, float ___flProgress1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotError Valve.VR.CVRScreenshots::TakeStereoScreenshot(System.UInt32&,System.String,System.String)
extern "C"  int32_t CVRScreenshots_TakeStereoScreenshot_m4138772900 (CVRScreenshots_t3241040508 * __this, uint32_t* ___pOutScreenshotHandle0, String_t* ___pchPreviewFilename1, String_t* ___pchVRFilename2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotError Valve.VR.CVRScreenshots::SubmitScreenshot(System.UInt32,Valve.VR.EVRScreenshotType,System.String,System.String)
extern "C"  int32_t CVRScreenshots_SubmitScreenshot_m2866395502 (CVRScreenshots_t3241040508 * __this, uint32_t ___screenshotHandle0, int32_t ___type1, String_t* ___pchSourcePreviewFilename2, String_t* ___pchSourceVRFilename3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
