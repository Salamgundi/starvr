﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.DebugUI
struct DebugUI_t3938194035;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.DebugUI::.ctor()
extern "C"  void DebugUI__ctor_m2821858155 (DebugUI_t3938194035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.InteractionSystem.DebugUI Valve.VR.InteractionSystem.DebugUI::get_instance()
extern "C"  DebugUI_t3938194035 * DebugUI_get_instance_m946106162 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.DebugUI::Start()
extern "C"  void DebugUI_Start_m187184135 (DebugUI_t3938194035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.DebugUI::OnGUI()
extern "C"  void DebugUI_OnGUI_m1582676821 (DebugUI_t3938194035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
