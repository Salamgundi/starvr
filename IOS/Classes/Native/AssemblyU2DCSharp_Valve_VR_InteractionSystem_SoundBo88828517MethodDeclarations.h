﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.SoundBowClick
struct SoundBowClick_t88828517;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.SoundBowClick::.ctor()
extern "C"  void SoundBowClick__ctor_m3237563441 (SoundBowClick_t88828517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SoundBowClick::Awake()
extern "C"  void SoundBowClick_Awake_m3864981526 (SoundBowClick_t88828517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SoundBowClick::PlayBowTensionClicks(System.Single)
extern "C"  void SoundBowClick_PlayBowTensionClicks_m3189958167 (SoundBowClick_t88828517 * __this, float ___normalizedTension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
