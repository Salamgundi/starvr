﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_SharedMethods
struct VRTK_SharedMethods_t185961009;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Component
struct Component_t3819376471;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void VRTK.VRTK_SharedMethods::.ctor()
extern "C"  void VRTK_SharedMethods__ctor_m419920687 (VRTK_SharedMethods_t185961009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds VRTK.VRTK_SharedMethods::GetBounds(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform)
extern "C"  Bounds_t3033363703  VRTK_SharedMethods_GetBounds_m1245784250 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___transform0, Transform_t3275118058 * ___excludeRotation1, Transform_t3275118058 * ___excludeTransform2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SharedMethods::IsLowest(System.Single,System.Single[])
extern "C"  bool VRTK_SharedMethods_IsLowest_m3021866377 (Il2CppObject * __this /* static, unused */, float ___value0, SingleU5BU5D_t577127397* ___others1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_SharedMethods::AddCameraFade()
extern "C"  Transform_t3275118058 * VRTK_SharedMethods_AddCameraFade_m577229013 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SharedMethods::CreateColliders(UnityEngine.GameObject)
extern "C"  void VRTK_SharedMethods_CreateColliders_m621209738 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component VRTK.VRTK_SharedMethods::CloneComponent(UnityEngine.Component,UnityEngine.GameObject,System.Boolean)
extern "C"  Component_t3819376471 * VRTK_SharedMethods_CloneComponent_m2258737719 (Il2CppObject * __this /* static, unused */, Component_t3819376471 * ___source0, GameObject_t1756533147 * ___destination1, bool ___copyProperties2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color VRTK.VRTK_SharedMethods::ColorDarken(UnityEngine.Color,System.Single)
extern "C"  Color_t2020392075  VRTK_SharedMethods_ColorDarken_m3872862231 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, float ___percent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SharedMethods::IsEditTime()
extern "C"  bool VRTK_SharedMethods_IsEditTime_m4195920538 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_SharedMethods::ColorPercent(System.Single,System.Single)
extern "C"  float VRTK_SharedMethods_ColorPercent_m2625907195 (Il2CppObject * __this /* static, unused */, float ___value0, float ___percent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
