﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Event`3<System.Object,System.Object,System.Object>
struct Event_3_t1695830490;
// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct UnityAction_3_t3482433968;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void SteamVR_Events/Event`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Event_3__ctor_m3485785953_gshared (Event_3_t1695830490 * __this, const MethodInfo* method);
#define Event_3__ctor_m3485785953(__this, method) ((  void (*) (Event_3_t1695830490 *, const MethodInfo*))Event_3__ctor_m3485785953_gshared)(__this, method)
// System.Void SteamVR_Events/Event`3<System.Object,System.Object,System.Object>::Listen(UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void Event_3_Listen_m733424601_gshared (Event_3_t1695830490 * __this, UnityAction_3_t3482433968 * ___action0, const MethodInfo* method);
#define Event_3_Listen_m733424601(__this, ___action0, method) ((  void (*) (Event_3_t1695830490 *, UnityAction_3_t3482433968 *, const MethodInfo*))Event_3_Listen_m733424601_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`3<System.Object,System.Object,System.Object>::Remove(UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void Event_3_Remove_m3453444002_gshared (Event_3_t1695830490 * __this, UnityAction_3_t3482433968 * ___action0, const MethodInfo* method);
#define Event_3_Remove_m3453444002(__this, ___action0, method) ((  void (*) (Event_3_t1695830490 *, UnityAction_3_t3482433968 *, const MethodInfo*))Event_3_Remove_m3453444002_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`3<System.Object,System.Object,System.Object>::Send(T0,T1,T2)
extern "C"  void Event_3_Send_m109483862_gshared (Event_3_t1695830490 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
#define Event_3_Send_m109483862(__this, ___arg00, ___arg11, ___arg22, method) ((  void (*) (Event_3_t1695830490 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Event_3_Send_m109483862_gshared)(__this, ___arg00, ___arg11, ___arg22, method)
