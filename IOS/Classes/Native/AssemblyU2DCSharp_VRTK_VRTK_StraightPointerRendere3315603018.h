﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_VRTK_VRTK_BasePointerRenderer1270536273.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_StraightPointerRenderer
struct  VRTK_StraightPointerRenderer_t3315603018  : public VRTK_BasePointerRenderer_t1270536273
{
public:
	// System.Single VRTK.VRTK_StraightPointerRenderer::maximumLength
	float ___maximumLength_28;
	// System.Single VRTK.VRTK_StraightPointerRenderer::scaleFactor
	float ___scaleFactor_29;
	// System.Single VRTK.VRTK_StraightPointerRenderer::cursorScaleMultiplier
	float ___cursorScaleMultiplier_30;
	// System.Boolean VRTK.VRTK_StraightPointerRenderer::cursorMatchTargetRotation
	bool ___cursorMatchTargetRotation_31;
	// System.Boolean VRTK.VRTK_StraightPointerRenderer::cursorDistanceRescale
	bool ___cursorDistanceRescale_32;
	// UnityEngine.GameObject VRTK.VRTK_StraightPointerRenderer::customTracer
	GameObject_t1756533147 * ___customTracer_33;
	// UnityEngine.GameObject VRTK.VRTK_StraightPointerRenderer::customCursor
	GameObject_t1756533147 * ___customCursor_34;
	// UnityEngine.GameObject VRTK.VRTK_StraightPointerRenderer::actualContainer
	GameObject_t1756533147 * ___actualContainer_35;
	// UnityEngine.GameObject VRTK.VRTK_StraightPointerRenderer::actualTracer
	GameObject_t1756533147 * ___actualTracer_36;
	// UnityEngine.GameObject VRTK.VRTK_StraightPointerRenderer::actualCursor
	GameObject_t1756533147 * ___actualCursor_37;
	// UnityEngine.Vector3 VRTK.VRTK_StraightPointerRenderer::cursorOriginalScale
	Vector3_t2243707580  ___cursorOriginalScale_38;

public:
	inline static int32_t get_offset_of_maximumLength_28() { return static_cast<int32_t>(offsetof(VRTK_StraightPointerRenderer_t3315603018, ___maximumLength_28)); }
	inline float get_maximumLength_28() const { return ___maximumLength_28; }
	inline float* get_address_of_maximumLength_28() { return &___maximumLength_28; }
	inline void set_maximumLength_28(float value)
	{
		___maximumLength_28 = value;
	}

	inline static int32_t get_offset_of_scaleFactor_29() { return static_cast<int32_t>(offsetof(VRTK_StraightPointerRenderer_t3315603018, ___scaleFactor_29)); }
	inline float get_scaleFactor_29() const { return ___scaleFactor_29; }
	inline float* get_address_of_scaleFactor_29() { return &___scaleFactor_29; }
	inline void set_scaleFactor_29(float value)
	{
		___scaleFactor_29 = value;
	}

	inline static int32_t get_offset_of_cursorScaleMultiplier_30() { return static_cast<int32_t>(offsetof(VRTK_StraightPointerRenderer_t3315603018, ___cursorScaleMultiplier_30)); }
	inline float get_cursorScaleMultiplier_30() const { return ___cursorScaleMultiplier_30; }
	inline float* get_address_of_cursorScaleMultiplier_30() { return &___cursorScaleMultiplier_30; }
	inline void set_cursorScaleMultiplier_30(float value)
	{
		___cursorScaleMultiplier_30 = value;
	}

	inline static int32_t get_offset_of_cursorMatchTargetRotation_31() { return static_cast<int32_t>(offsetof(VRTK_StraightPointerRenderer_t3315603018, ___cursorMatchTargetRotation_31)); }
	inline bool get_cursorMatchTargetRotation_31() const { return ___cursorMatchTargetRotation_31; }
	inline bool* get_address_of_cursorMatchTargetRotation_31() { return &___cursorMatchTargetRotation_31; }
	inline void set_cursorMatchTargetRotation_31(bool value)
	{
		___cursorMatchTargetRotation_31 = value;
	}

	inline static int32_t get_offset_of_cursorDistanceRescale_32() { return static_cast<int32_t>(offsetof(VRTK_StraightPointerRenderer_t3315603018, ___cursorDistanceRescale_32)); }
	inline bool get_cursorDistanceRescale_32() const { return ___cursorDistanceRescale_32; }
	inline bool* get_address_of_cursorDistanceRescale_32() { return &___cursorDistanceRescale_32; }
	inline void set_cursorDistanceRescale_32(bool value)
	{
		___cursorDistanceRescale_32 = value;
	}

	inline static int32_t get_offset_of_customTracer_33() { return static_cast<int32_t>(offsetof(VRTK_StraightPointerRenderer_t3315603018, ___customTracer_33)); }
	inline GameObject_t1756533147 * get_customTracer_33() const { return ___customTracer_33; }
	inline GameObject_t1756533147 ** get_address_of_customTracer_33() { return &___customTracer_33; }
	inline void set_customTracer_33(GameObject_t1756533147 * value)
	{
		___customTracer_33 = value;
		Il2CppCodeGenWriteBarrier(&___customTracer_33, value);
	}

	inline static int32_t get_offset_of_customCursor_34() { return static_cast<int32_t>(offsetof(VRTK_StraightPointerRenderer_t3315603018, ___customCursor_34)); }
	inline GameObject_t1756533147 * get_customCursor_34() const { return ___customCursor_34; }
	inline GameObject_t1756533147 ** get_address_of_customCursor_34() { return &___customCursor_34; }
	inline void set_customCursor_34(GameObject_t1756533147 * value)
	{
		___customCursor_34 = value;
		Il2CppCodeGenWriteBarrier(&___customCursor_34, value);
	}

	inline static int32_t get_offset_of_actualContainer_35() { return static_cast<int32_t>(offsetof(VRTK_StraightPointerRenderer_t3315603018, ___actualContainer_35)); }
	inline GameObject_t1756533147 * get_actualContainer_35() const { return ___actualContainer_35; }
	inline GameObject_t1756533147 ** get_address_of_actualContainer_35() { return &___actualContainer_35; }
	inline void set_actualContainer_35(GameObject_t1756533147 * value)
	{
		___actualContainer_35 = value;
		Il2CppCodeGenWriteBarrier(&___actualContainer_35, value);
	}

	inline static int32_t get_offset_of_actualTracer_36() { return static_cast<int32_t>(offsetof(VRTK_StraightPointerRenderer_t3315603018, ___actualTracer_36)); }
	inline GameObject_t1756533147 * get_actualTracer_36() const { return ___actualTracer_36; }
	inline GameObject_t1756533147 ** get_address_of_actualTracer_36() { return &___actualTracer_36; }
	inline void set_actualTracer_36(GameObject_t1756533147 * value)
	{
		___actualTracer_36 = value;
		Il2CppCodeGenWriteBarrier(&___actualTracer_36, value);
	}

	inline static int32_t get_offset_of_actualCursor_37() { return static_cast<int32_t>(offsetof(VRTK_StraightPointerRenderer_t3315603018, ___actualCursor_37)); }
	inline GameObject_t1756533147 * get_actualCursor_37() const { return ___actualCursor_37; }
	inline GameObject_t1756533147 ** get_address_of_actualCursor_37() { return &___actualCursor_37; }
	inline void set_actualCursor_37(GameObject_t1756533147 * value)
	{
		___actualCursor_37 = value;
		Il2CppCodeGenWriteBarrier(&___actualCursor_37, value);
	}

	inline static int32_t get_offset_of_cursorOriginalScale_38() { return static_cast<int32_t>(offsetof(VRTK_StraightPointerRenderer_t3315603018, ___cursorOriginalScale_38)); }
	inline Vector3_t2243707580  get_cursorOriginalScale_38() const { return ___cursorOriginalScale_38; }
	inline Vector3_t2243707580 * get_address_of_cursorOriginalScale_38() { return &___cursorOriginalScale_38; }
	inline void set_cursorOriginalScale_38(Vector3_t2243707580  value)
	{
		___cursorOriginalScale_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
