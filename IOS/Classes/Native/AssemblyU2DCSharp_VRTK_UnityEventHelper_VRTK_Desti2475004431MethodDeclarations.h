﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents
struct VRTK_DestinationMarker_UnityEvents_t2475004431;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventArgs1852451661.h"

// System.Void VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents::.ctor()
extern "C"  void VRTK_DestinationMarker_UnityEvents__ctor_m3586807474 (VRTK_DestinationMarker_UnityEvents_t2475004431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents::SetDestinationMarker()
extern "C"  void VRTK_DestinationMarker_UnityEvents_SetDestinationMarker_m1622320522 (VRTK_DestinationMarker_UnityEvents_t2475004431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents::OnEnable()
extern "C"  void VRTK_DestinationMarker_UnityEvents_OnEnable_m4280306138 (VRTK_DestinationMarker_UnityEvents_t2475004431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents::DestinationMarkerEnter(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_DestinationMarker_UnityEvents_DestinationMarkerEnter_m2018683534 (VRTK_DestinationMarker_UnityEvents_t2475004431 * __this, Il2CppObject * ___o0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents::DestinationMarkerExit(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_DestinationMarker_UnityEvents_DestinationMarkerExit_m3647396262 (VRTK_DestinationMarker_UnityEvents_t2475004431 * __this, Il2CppObject * ___o0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents::DestinationMarkerSet(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_DestinationMarker_UnityEvents_DestinationMarkerSet_m244466548 (VRTK_DestinationMarker_UnityEvents_t2475004431 * __this, Il2CppObject * ___o0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents::OnDisable()
extern "C"  void VRTK_DestinationMarker_UnityEvents_OnDisable_m1395063133 (VRTK_DestinationMarker_UnityEvents_t2475004431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
