﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ControllerRigidbodyActivator
struct  VRTK_ControllerRigidbodyActivator_t3039396590  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.VRTK_ControllerRigidbodyActivator::isEnabled
	bool ___isEnabled_2;

public:
	inline static int32_t get_offset_of_isEnabled_2() { return static_cast<int32_t>(offsetof(VRTK_ControllerRigidbodyActivator_t3039396590, ___isEnabled_2)); }
	inline bool get_isEnabled_2() const { return ___isEnabled_2; }
	inline bool* get_address_of_isEnabled_2() { return &___isEnabled_2; }
	inline void set_isEnabled_2(bool value)
	{
		___isEnabled_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
