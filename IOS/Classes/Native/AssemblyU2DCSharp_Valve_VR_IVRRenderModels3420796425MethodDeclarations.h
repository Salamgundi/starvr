﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels
struct IVRRenderModels_t3420796425;
struct IVRRenderModels_t3420796425_marshaled_pinvoke;
struct IVRRenderModels_t3420796425_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IVRRenderModels_t3420796425;
struct IVRRenderModels_t3420796425_marshaled_pinvoke;

extern "C" void IVRRenderModels_t3420796425_marshal_pinvoke(const IVRRenderModels_t3420796425& unmarshaled, IVRRenderModels_t3420796425_marshaled_pinvoke& marshaled);
extern "C" void IVRRenderModels_t3420796425_marshal_pinvoke_back(const IVRRenderModels_t3420796425_marshaled_pinvoke& marshaled, IVRRenderModels_t3420796425& unmarshaled);
extern "C" void IVRRenderModels_t3420796425_marshal_pinvoke_cleanup(IVRRenderModels_t3420796425_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IVRRenderModels_t3420796425;
struct IVRRenderModels_t3420796425_marshaled_com;

extern "C" void IVRRenderModels_t3420796425_marshal_com(const IVRRenderModels_t3420796425& unmarshaled, IVRRenderModels_t3420796425_marshaled_com& marshaled);
extern "C" void IVRRenderModels_t3420796425_marshal_com_back(const IVRRenderModels_t3420796425_marshaled_com& marshaled, IVRRenderModels_t3420796425& unmarshaled);
extern "C" void IVRRenderModels_t3420796425_marshal_com_cleanup(IVRRenderModels_t3420796425_marshaled_com& marshaled);
