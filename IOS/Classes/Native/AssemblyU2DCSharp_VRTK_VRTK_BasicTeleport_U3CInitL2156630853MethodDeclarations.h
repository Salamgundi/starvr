﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_BasicTeleport/<InitListenersAtEndOfFrame>c__Iterator0
struct U3CInitListenersAtEndOfFrameU3Ec__Iterator0_t2156630853;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_BasicTeleport/<InitListenersAtEndOfFrame>c__Iterator0::.ctor()
extern "C"  void U3CInitListenersAtEndOfFrameU3Ec__Iterator0__ctor_m3060730540 (U3CInitListenersAtEndOfFrameU3Ec__Iterator0_t2156630853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BasicTeleport/<InitListenersAtEndOfFrame>c__Iterator0::MoveNext()
extern "C"  bool U3CInitListenersAtEndOfFrameU3Ec__Iterator0_MoveNext_m3528010392 (U3CInitListenersAtEndOfFrameU3Ec__Iterator0_t2156630853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_BasicTeleport/<InitListenersAtEndOfFrame>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CInitListenersAtEndOfFrameU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1286783158 (U3CInitListenersAtEndOfFrameU3Ec__Iterator0_t2156630853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_BasicTeleport/<InitListenersAtEndOfFrame>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CInitListenersAtEndOfFrameU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3313347214 (U3CInitListenersAtEndOfFrameU3Ec__Iterator0_t2156630853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport/<InitListenersAtEndOfFrame>c__Iterator0::Dispose()
extern "C"  void U3CInitListenersAtEndOfFrameU3Ec__Iterator0_Dispose_m3372603783 (U3CInitListenersAtEndOfFrameU3Ec__Iterator0_t2156630853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport/<InitListenersAtEndOfFrame>c__Iterator0::Reset()
extern "C"  void U3CInitListenersAtEndOfFrameU3Ec__Iterator0_Reset_m2748526569 (U3CInitListenersAtEndOfFrameU3Ec__Iterator0_t2156630853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
