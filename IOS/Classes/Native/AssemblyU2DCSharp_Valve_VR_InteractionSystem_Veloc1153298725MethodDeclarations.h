﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.VelocityEstimator
struct VelocityEstimator_t1153298725;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Valve.VR.InteractionSystem.VelocityEstimator::.ctor()
extern "C"  void VelocityEstimator__ctor_m2995421979 (VelocityEstimator_t1153298725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.VelocityEstimator::BeginEstimatingVelocity()
extern "C"  void VelocityEstimator_BeginEstimatingVelocity_m105247914 (VelocityEstimator_t1153298725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.VelocityEstimator::FinishEstimatingVelocity()
extern "C"  void VelocityEstimator_FinishEstimatingVelocity_m576864688 (VelocityEstimator_t1153298725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Valve.VR.InteractionSystem.VelocityEstimator::GetVelocityEstimate()
extern "C"  Vector3_t2243707580  VelocityEstimator_GetVelocityEstimate_m2277847650 (VelocityEstimator_t1153298725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Valve.VR.InteractionSystem.VelocityEstimator::GetAngularVelocityEstimate()
extern "C"  Vector3_t2243707580  VelocityEstimator_GetAngularVelocityEstimate_m1715720228 (VelocityEstimator_t1153298725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Valve.VR.InteractionSystem.VelocityEstimator::GetAccelerationEstimate()
extern "C"  Vector3_t2243707580  VelocityEstimator_GetAccelerationEstimate_m1708524431 (VelocityEstimator_t1153298725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.VelocityEstimator::Awake()
extern "C"  void VelocityEstimator_Awake_m583808614 (VelocityEstimator_t1153298725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.VelocityEstimator::EstimateVelocityCoroutine()
extern "C"  Il2CppObject * VelocityEstimator_EstimateVelocityCoroutine_m1365399524 (VelocityEstimator_t1153298725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
