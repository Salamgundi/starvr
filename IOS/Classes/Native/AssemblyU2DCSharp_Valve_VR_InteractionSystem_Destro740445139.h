﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.DestroyOnTriggerEnter
struct  DestroyOnTriggerEnter_t740445139  : public MonoBehaviour_t1158329972
{
public:
	// System.String Valve.VR.InteractionSystem.DestroyOnTriggerEnter::tagFilter
	String_t* ___tagFilter_2;
	// System.Boolean Valve.VR.InteractionSystem.DestroyOnTriggerEnter::useTag
	bool ___useTag_3;

public:
	inline static int32_t get_offset_of_tagFilter_2() { return static_cast<int32_t>(offsetof(DestroyOnTriggerEnter_t740445139, ___tagFilter_2)); }
	inline String_t* get_tagFilter_2() const { return ___tagFilter_2; }
	inline String_t** get_address_of_tagFilter_2() { return &___tagFilter_2; }
	inline void set_tagFilter_2(String_t* value)
	{
		___tagFilter_2 = value;
		Il2CppCodeGenWriteBarrier(&___tagFilter_2, value);
	}

	inline static int32_t get_offset_of_useTag_3() { return static_cast<int32_t>(offsetof(DestroyOnTriggerEnter_t740445139, ___useTag_3)); }
	inline bool get_useTag_3() const { return ___useTag_3; }
	inline bool* get_address_of_useTag_3() { return &___useTag_3; }
	inline void set_useTag_3(bool value)
	{
		___useTag_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
