﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ObjectAutoGrab
struct VRTK_ObjectAutoGrab_t2748125822;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_ObjectAutoGrab::.ctor()
extern "C"  void VRTK_ObjectAutoGrab__ctor_m232511994 (VRTK_ObjectAutoGrab_t2748125822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectAutoGrab::ClearPreviousClone()
extern "C"  void VRTK_ObjectAutoGrab_ClearPreviousClone_m1259291837 (VRTK_ObjectAutoGrab_t2748125822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectAutoGrab::OnEnable()
extern "C"  void VRTK_ObjectAutoGrab_OnEnable_m3578201158 (VRTK_ObjectAutoGrab_t2748125822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_ObjectAutoGrab::AutoGrab()
extern "C"  Il2CppObject * VRTK_ObjectAutoGrab_AutoGrab_m3586873377 (VRTK_ObjectAutoGrab_t2748125822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
