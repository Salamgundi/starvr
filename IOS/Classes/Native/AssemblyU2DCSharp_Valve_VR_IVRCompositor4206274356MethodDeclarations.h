﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor
struct IVRCompositor_t4206274356;
struct IVRCompositor_t4206274356_marshaled_pinvoke;
struct IVRCompositor_t4206274356_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IVRCompositor_t4206274356;
struct IVRCompositor_t4206274356_marshaled_pinvoke;

extern "C" void IVRCompositor_t4206274356_marshal_pinvoke(const IVRCompositor_t4206274356& unmarshaled, IVRCompositor_t4206274356_marshaled_pinvoke& marshaled);
extern "C" void IVRCompositor_t4206274356_marshal_pinvoke_back(const IVRCompositor_t4206274356_marshaled_pinvoke& marshaled, IVRCompositor_t4206274356& unmarshaled);
extern "C" void IVRCompositor_t4206274356_marshal_pinvoke_cleanup(IVRCompositor_t4206274356_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IVRCompositor_t4206274356;
struct IVRCompositor_t4206274356_marshaled_com;

extern "C" void IVRCompositor_t4206274356_marshal_com(const IVRCompositor_t4206274356& unmarshaled, IVRCompositor_t4206274356_marshaled_com& marshaled);
extern "C" void IVRCompositor_t4206274356_marshal_com_back(const IVRCompositor_t4206274356_marshaled_com& marshaled, IVRCompositor_t4206274356& unmarshaled);
extern "C" void IVRCompositor_t4206274356_marshal_com_cleanup(IVRCompositor_t4206274356_marshaled_com& marshaled);
