﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_GetWorkingSeatedZeroPoseToRawTrackingPose
struct _GetWorkingSeatedZeroPoseToRawTrackingPose_t459969004;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_GetWorkingSeatedZeroPoseToRawTrackingPose::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetWorkingSeatedZeroPoseToRawTrackingPose__ctor_m100214779 (_GetWorkingSeatedZeroPoseToRawTrackingPose_t459969004 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetWorkingSeatedZeroPoseToRawTrackingPose::Invoke(Valve.VR.HmdMatrix34_t&)
extern "C"  bool _GetWorkingSeatedZeroPoseToRawTrackingPose_Invoke_m2644456383 (_GetWorkingSeatedZeroPoseToRawTrackingPose_t459969004 * __this, HmdMatrix34_t_t664273062 * ___pmatSeatedZeroPoseToRawTrackingPose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_GetWorkingSeatedZeroPoseToRawTrackingPose::BeginInvoke(Valve.VR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetWorkingSeatedZeroPoseToRawTrackingPose_BeginInvoke_m2347620574 (_GetWorkingSeatedZeroPoseToRawTrackingPose_t459969004 * __this, HmdMatrix34_t_t664273062 * ___pmatSeatedZeroPoseToRawTrackingPose0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetWorkingSeatedZeroPoseToRawTrackingPose::EndInvoke(Valve.VR.HmdMatrix34_t&,System.IAsyncResult)
extern "C"  bool _GetWorkingSeatedZeroPoseToRawTrackingPose_EndInvoke_m1626973001 (_GetWorkingSeatedZeroPoseToRawTrackingPose_t459969004 * __this, HmdMatrix34_t_t664273062 * ___pmatSeatedZeroPoseToRawTrackingPose0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
