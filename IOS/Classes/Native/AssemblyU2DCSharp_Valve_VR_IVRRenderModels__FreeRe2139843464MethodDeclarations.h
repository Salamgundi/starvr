﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_FreeRenderModel
struct _FreeRenderModel_t2139843464;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_FreeRenderModel::.ctor(System.Object,System.IntPtr)
extern "C"  void _FreeRenderModel__ctor_m3343122049 (_FreeRenderModel_t2139843464 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRRenderModels/_FreeRenderModel::Invoke(System.IntPtr)
extern "C"  void _FreeRenderModel_Invoke_m1132817099 (_FreeRenderModel_t2139843464 * __this, IntPtr_t ___pRenderModel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_FreeRenderModel::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _FreeRenderModel_BeginInvoke_m1880110598 (_FreeRenderModel_t2139843464 * __this, IntPtr_t ___pRenderModel0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRRenderModels/_FreeRenderModel::EndInvoke(System.IAsyncResult)
extern "C"  void _FreeRenderModel_EndInvoke_m1525062483 (_FreeRenderModel_t2139843464 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
