﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetApplicationKeyByProcessId
struct _GetApplicationKeyByProcessId_t114599352;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetApplicationKeyByProcessId::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetApplicationKeyByProcessId__ctor_m1526684017 (_GetApplicationKeyByProcessId_t114599352 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_GetApplicationKeyByProcessId::Invoke(System.UInt32,System.String,System.UInt32)
extern "C"  int32_t _GetApplicationKeyByProcessId_Invoke_m2870761795 (_GetApplicationKeyByProcessId_t114599352 * __this, uint32_t ___unProcessId0, String_t* ___pchAppKeyBuffer1, uint32_t ___unAppKeyBufferLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetApplicationKeyByProcessId::BeginInvoke(System.UInt32,System.String,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetApplicationKeyByProcessId_BeginInvoke_m3113081442 (_GetApplicationKeyByProcessId_t114599352 * __this, uint32_t ___unProcessId0, String_t* ___pchAppKeyBuffer1, uint32_t ___unAppKeyBufferLen2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_GetApplicationKeyByProcessId::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _GetApplicationKeyByProcessId_EndInvoke_m1736764339 (_GetApplicationKeyByProcessId_t114599352 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
