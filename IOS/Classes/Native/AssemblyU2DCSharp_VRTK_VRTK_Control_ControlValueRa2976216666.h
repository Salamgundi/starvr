﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Control/ControlValueRange
struct  ControlValueRange_t2976216666 
{
public:
	// System.Single VRTK.VRTK_Control/ControlValueRange::controlMin
	float ___controlMin_0;
	// System.Single VRTK.VRTK_Control/ControlValueRange::controlMax
	float ___controlMax_1;

public:
	inline static int32_t get_offset_of_controlMin_0() { return static_cast<int32_t>(offsetof(ControlValueRange_t2976216666, ___controlMin_0)); }
	inline float get_controlMin_0() const { return ___controlMin_0; }
	inline float* get_address_of_controlMin_0() { return &___controlMin_0; }
	inline void set_controlMin_0(float value)
	{
		___controlMin_0 = value;
	}

	inline static int32_t get_offset_of_controlMax_1() { return static_cast<int32_t>(offsetof(ControlValueRange_t2976216666, ___controlMax_1)); }
	inline float get_controlMax_1() const { return ___controlMax_1; }
	inline float* get_address_of_controlMax_1() { return &___controlMax_1; }
	inline void set_controlMax_1(float value)
	{
		___controlMax_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
