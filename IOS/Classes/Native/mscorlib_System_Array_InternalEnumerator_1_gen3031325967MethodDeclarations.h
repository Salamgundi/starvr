﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3031325967.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdQuad_t2172573705.h"

// System.Void System.Array/InternalEnumerator`1<Valve.VR.HmdQuad_t>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1066660024_gshared (InternalEnumerator_1_t3031325967 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1066660024(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3031325967 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1066660024_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.HmdQuad_t>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3075129588_gshared (InternalEnumerator_1_t3031325967 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3075129588(__this, method) ((  void (*) (InternalEnumerator_1_t3031325967 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3075129588_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Valve.VR.HmdQuad_t>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2812089204_gshared (InternalEnumerator_1_t3031325967 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2812089204(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3031325967 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2812089204_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.HmdQuad_t>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m54296465_gshared (InternalEnumerator_1_t3031325967 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m54296465(__this, method) ((  void (*) (InternalEnumerator_1_t3031325967 *, const MethodInfo*))InternalEnumerator_1_Dispose_m54296465_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Valve.VR.HmdQuad_t>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1318341960_gshared (InternalEnumerator_1_t3031325967 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1318341960(__this, method) ((  bool (*) (InternalEnumerator_1_t3031325967 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1318341960_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Valve.VR.HmdQuad_t>::get_Current()
extern "C"  HmdQuad_t_t2172573705  InternalEnumerator_1_get_Current_m1467193529_gshared (InternalEnumerator_1_t3031325967 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1467193529(__this, method) ((  HmdQuad_t_t2172573705  (*) (InternalEnumerator_1_t3031325967 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1467193529_gshared)(__this, method)
