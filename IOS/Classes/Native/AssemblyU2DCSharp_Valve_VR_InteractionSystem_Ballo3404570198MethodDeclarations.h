﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.BalloonHapticBump
struct BalloonHapticBump_t3404570198;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void Valve.VR.InteractionSystem.BalloonHapticBump::.ctor()
extern "C"  void BalloonHapticBump__ctor_m947868416 (BalloonHapticBump_t3404570198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.BalloonHapticBump::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void BalloonHapticBump_OnCollisionEnter_m3718885966 (BalloonHapticBump_t3404570198 * __this, Collision_t2876846408 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
