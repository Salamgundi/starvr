﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.ObjectControlEventArgs
struct  ObjectControlEventArgs_t2459490319 
{
public:
	// UnityEngine.GameObject VRTK.ObjectControlEventArgs::controlledGameObject
	GameObject_t1756533147 * ___controlledGameObject_0;
	// UnityEngine.Transform VRTK.ObjectControlEventArgs::directionDevice
	Transform_t3275118058 * ___directionDevice_1;
	// UnityEngine.Vector3 VRTK.ObjectControlEventArgs::axisDirection
	Vector3_t2243707580  ___axisDirection_2;
	// System.Single VRTK.ObjectControlEventArgs::axis
	float ___axis_3;
	// System.Single VRTK.ObjectControlEventArgs::deadzone
	float ___deadzone_4;
	// System.Boolean VRTK.ObjectControlEventArgs::currentlyFalling
	bool ___currentlyFalling_5;
	// System.Boolean VRTK.ObjectControlEventArgs::modifierActive
	bool ___modifierActive_6;

public:
	inline static int32_t get_offset_of_controlledGameObject_0() { return static_cast<int32_t>(offsetof(ObjectControlEventArgs_t2459490319, ___controlledGameObject_0)); }
	inline GameObject_t1756533147 * get_controlledGameObject_0() const { return ___controlledGameObject_0; }
	inline GameObject_t1756533147 ** get_address_of_controlledGameObject_0() { return &___controlledGameObject_0; }
	inline void set_controlledGameObject_0(GameObject_t1756533147 * value)
	{
		___controlledGameObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___controlledGameObject_0, value);
	}

	inline static int32_t get_offset_of_directionDevice_1() { return static_cast<int32_t>(offsetof(ObjectControlEventArgs_t2459490319, ___directionDevice_1)); }
	inline Transform_t3275118058 * get_directionDevice_1() const { return ___directionDevice_1; }
	inline Transform_t3275118058 ** get_address_of_directionDevice_1() { return &___directionDevice_1; }
	inline void set_directionDevice_1(Transform_t3275118058 * value)
	{
		___directionDevice_1 = value;
		Il2CppCodeGenWriteBarrier(&___directionDevice_1, value);
	}

	inline static int32_t get_offset_of_axisDirection_2() { return static_cast<int32_t>(offsetof(ObjectControlEventArgs_t2459490319, ___axisDirection_2)); }
	inline Vector3_t2243707580  get_axisDirection_2() const { return ___axisDirection_2; }
	inline Vector3_t2243707580 * get_address_of_axisDirection_2() { return &___axisDirection_2; }
	inline void set_axisDirection_2(Vector3_t2243707580  value)
	{
		___axisDirection_2 = value;
	}

	inline static int32_t get_offset_of_axis_3() { return static_cast<int32_t>(offsetof(ObjectControlEventArgs_t2459490319, ___axis_3)); }
	inline float get_axis_3() const { return ___axis_3; }
	inline float* get_address_of_axis_3() { return &___axis_3; }
	inline void set_axis_3(float value)
	{
		___axis_3 = value;
	}

	inline static int32_t get_offset_of_deadzone_4() { return static_cast<int32_t>(offsetof(ObjectControlEventArgs_t2459490319, ___deadzone_4)); }
	inline float get_deadzone_4() const { return ___deadzone_4; }
	inline float* get_address_of_deadzone_4() { return &___deadzone_4; }
	inline void set_deadzone_4(float value)
	{
		___deadzone_4 = value;
	}

	inline static int32_t get_offset_of_currentlyFalling_5() { return static_cast<int32_t>(offsetof(ObjectControlEventArgs_t2459490319, ___currentlyFalling_5)); }
	inline bool get_currentlyFalling_5() const { return ___currentlyFalling_5; }
	inline bool* get_address_of_currentlyFalling_5() { return &___currentlyFalling_5; }
	inline void set_currentlyFalling_5(bool value)
	{
		___currentlyFalling_5 = value;
	}

	inline static int32_t get_offset_of_modifierActive_6() { return static_cast<int32_t>(offsetof(ObjectControlEventArgs_t2459490319, ___modifierActive_6)); }
	inline bool get_modifierActive_6() const { return ___modifierActive_6; }
	inline bool* get_address_of_modifierActive_6() { return &___modifierActive_6; }
	inline void set_modifierActive_6(bool value)
	{
		___modifierActive_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VRTK.ObjectControlEventArgs
struct ObjectControlEventArgs_t2459490319_marshaled_pinvoke
{
	GameObject_t1756533147 * ___controlledGameObject_0;
	Transform_t3275118058 * ___directionDevice_1;
	Vector3_t2243707580  ___axisDirection_2;
	float ___axis_3;
	float ___deadzone_4;
	int32_t ___currentlyFalling_5;
	int32_t ___modifierActive_6;
};
// Native definition for COM marshalling of VRTK.ObjectControlEventArgs
struct ObjectControlEventArgs_t2459490319_marshaled_com
{
	GameObject_t1756533147 * ___controlledGameObject_0;
	Transform_t3275118058 * ___directionDevice_1;
	Vector3_t2243707580  ___axisDirection_2;
	float ___axis_3;
	float ___deadzone_4;
	int32_t ___currentlyFalling_5;
	int32_t ___modifierActive_6;
};
