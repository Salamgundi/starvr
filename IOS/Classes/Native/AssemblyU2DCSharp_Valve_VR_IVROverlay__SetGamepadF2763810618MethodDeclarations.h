﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetGamepadFocusOverlay
struct _SetGamepadFocusOverlay_t2763810618;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetGamepadFocusOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetGamepadFocusOverlay__ctor_m2182576563 (_SetGamepadFocusOverlay_t2763810618 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetGamepadFocusOverlay::Invoke(System.UInt64)
extern "C"  int32_t _SetGamepadFocusOverlay_Invoke_m2476495002 (_SetGamepadFocusOverlay_t2763810618 * __this, uint64_t ___ulNewFocusOverlay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetGamepadFocusOverlay::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetGamepadFocusOverlay_BeginInvoke_m2873341503 (_SetGamepadFocusOverlay_t2763810618 * __this, uint64_t ___ulNewFocusOverlay0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetGamepadFocusOverlay::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetGamepadFocusOverlay_EndInvoke_m1475485797 (_SetGamepadFocusOverlay_t2763810618 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
