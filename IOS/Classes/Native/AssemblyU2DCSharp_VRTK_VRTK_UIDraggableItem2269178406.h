﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_UIDraggableItem
struct  VRTK_UIDraggableItem_t2269178406  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.VRTK_UIDraggableItem::restrictToDropZone
	bool ___restrictToDropZone_2;
	// System.Boolean VRTK.VRTK_UIDraggableItem::restrictToOriginalCanvas
	bool ___restrictToOriginalCanvas_3;
	// System.Single VRTK.VRTK_UIDraggableItem::forwardOffset
	float ___forwardOffset_4;
	// UnityEngine.GameObject VRTK.VRTK_UIDraggableItem::validDropZone
	GameObject_t1756533147 * ___validDropZone_5;
	// UnityEngine.RectTransform VRTK.VRTK_UIDraggableItem::dragTransform
	RectTransform_t3349966182 * ___dragTransform_6;
	// UnityEngine.Vector3 VRTK.VRTK_UIDraggableItem::startPosition
	Vector3_t2243707580  ___startPosition_7;
	// UnityEngine.Quaternion VRTK.VRTK_UIDraggableItem::startRotation
	Quaternion_t4030073918  ___startRotation_8;
	// UnityEngine.GameObject VRTK.VRTK_UIDraggableItem::startDropZone
	GameObject_t1756533147 * ___startDropZone_9;
	// UnityEngine.Transform VRTK.VRTK_UIDraggableItem::startParent
	Transform_t3275118058 * ___startParent_10;
	// UnityEngine.Canvas VRTK.VRTK_UIDraggableItem::startCanvas
	Canvas_t209405766 * ___startCanvas_11;
	// UnityEngine.CanvasGroup VRTK.VRTK_UIDraggableItem::canvasGroup
	CanvasGroup_t3296560743 * ___canvasGroup_12;

public:
	inline static int32_t get_offset_of_restrictToDropZone_2() { return static_cast<int32_t>(offsetof(VRTK_UIDraggableItem_t2269178406, ___restrictToDropZone_2)); }
	inline bool get_restrictToDropZone_2() const { return ___restrictToDropZone_2; }
	inline bool* get_address_of_restrictToDropZone_2() { return &___restrictToDropZone_2; }
	inline void set_restrictToDropZone_2(bool value)
	{
		___restrictToDropZone_2 = value;
	}

	inline static int32_t get_offset_of_restrictToOriginalCanvas_3() { return static_cast<int32_t>(offsetof(VRTK_UIDraggableItem_t2269178406, ___restrictToOriginalCanvas_3)); }
	inline bool get_restrictToOriginalCanvas_3() const { return ___restrictToOriginalCanvas_3; }
	inline bool* get_address_of_restrictToOriginalCanvas_3() { return &___restrictToOriginalCanvas_3; }
	inline void set_restrictToOriginalCanvas_3(bool value)
	{
		___restrictToOriginalCanvas_3 = value;
	}

	inline static int32_t get_offset_of_forwardOffset_4() { return static_cast<int32_t>(offsetof(VRTK_UIDraggableItem_t2269178406, ___forwardOffset_4)); }
	inline float get_forwardOffset_4() const { return ___forwardOffset_4; }
	inline float* get_address_of_forwardOffset_4() { return &___forwardOffset_4; }
	inline void set_forwardOffset_4(float value)
	{
		___forwardOffset_4 = value;
	}

	inline static int32_t get_offset_of_validDropZone_5() { return static_cast<int32_t>(offsetof(VRTK_UIDraggableItem_t2269178406, ___validDropZone_5)); }
	inline GameObject_t1756533147 * get_validDropZone_5() const { return ___validDropZone_5; }
	inline GameObject_t1756533147 ** get_address_of_validDropZone_5() { return &___validDropZone_5; }
	inline void set_validDropZone_5(GameObject_t1756533147 * value)
	{
		___validDropZone_5 = value;
		Il2CppCodeGenWriteBarrier(&___validDropZone_5, value);
	}

	inline static int32_t get_offset_of_dragTransform_6() { return static_cast<int32_t>(offsetof(VRTK_UIDraggableItem_t2269178406, ___dragTransform_6)); }
	inline RectTransform_t3349966182 * get_dragTransform_6() const { return ___dragTransform_6; }
	inline RectTransform_t3349966182 ** get_address_of_dragTransform_6() { return &___dragTransform_6; }
	inline void set_dragTransform_6(RectTransform_t3349966182 * value)
	{
		___dragTransform_6 = value;
		Il2CppCodeGenWriteBarrier(&___dragTransform_6, value);
	}

	inline static int32_t get_offset_of_startPosition_7() { return static_cast<int32_t>(offsetof(VRTK_UIDraggableItem_t2269178406, ___startPosition_7)); }
	inline Vector3_t2243707580  get_startPosition_7() const { return ___startPosition_7; }
	inline Vector3_t2243707580 * get_address_of_startPosition_7() { return &___startPosition_7; }
	inline void set_startPosition_7(Vector3_t2243707580  value)
	{
		___startPosition_7 = value;
	}

	inline static int32_t get_offset_of_startRotation_8() { return static_cast<int32_t>(offsetof(VRTK_UIDraggableItem_t2269178406, ___startRotation_8)); }
	inline Quaternion_t4030073918  get_startRotation_8() const { return ___startRotation_8; }
	inline Quaternion_t4030073918 * get_address_of_startRotation_8() { return &___startRotation_8; }
	inline void set_startRotation_8(Quaternion_t4030073918  value)
	{
		___startRotation_8 = value;
	}

	inline static int32_t get_offset_of_startDropZone_9() { return static_cast<int32_t>(offsetof(VRTK_UIDraggableItem_t2269178406, ___startDropZone_9)); }
	inline GameObject_t1756533147 * get_startDropZone_9() const { return ___startDropZone_9; }
	inline GameObject_t1756533147 ** get_address_of_startDropZone_9() { return &___startDropZone_9; }
	inline void set_startDropZone_9(GameObject_t1756533147 * value)
	{
		___startDropZone_9 = value;
		Il2CppCodeGenWriteBarrier(&___startDropZone_9, value);
	}

	inline static int32_t get_offset_of_startParent_10() { return static_cast<int32_t>(offsetof(VRTK_UIDraggableItem_t2269178406, ___startParent_10)); }
	inline Transform_t3275118058 * get_startParent_10() const { return ___startParent_10; }
	inline Transform_t3275118058 ** get_address_of_startParent_10() { return &___startParent_10; }
	inline void set_startParent_10(Transform_t3275118058 * value)
	{
		___startParent_10 = value;
		Il2CppCodeGenWriteBarrier(&___startParent_10, value);
	}

	inline static int32_t get_offset_of_startCanvas_11() { return static_cast<int32_t>(offsetof(VRTK_UIDraggableItem_t2269178406, ___startCanvas_11)); }
	inline Canvas_t209405766 * get_startCanvas_11() const { return ___startCanvas_11; }
	inline Canvas_t209405766 ** get_address_of_startCanvas_11() { return &___startCanvas_11; }
	inline void set_startCanvas_11(Canvas_t209405766 * value)
	{
		___startCanvas_11 = value;
		Il2CppCodeGenWriteBarrier(&___startCanvas_11, value);
	}

	inline static int32_t get_offset_of_canvasGroup_12() { return static_cast<int32_t>(offsetof(VRTK_UIDraggableItem_t2269178406, ___canvasGroup_12)); }
	inline CanvasGroup_t3296560743 * get_canvasGroup_12() const { return ___canvasGroup_12; }
	inline CanvasGroup_t3296560743 ** get_address_of_canvasGroup_12() { return &___canvasGroup_12; }
	inline void set_canvasGroup_12(CanvasGroup_t3296560743 * value)
	{
		___canvasGroup_12 = value;
		Il2CppCodeGenWriteBarrier(&___canvasGroup_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
