﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.PropertyAttribute
struct PropertyAttribute_t2606999759;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m3663555848 (PropertyAttribute_t2606999759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyAttribute::set_order(System.Int32)
extern "C"  void PropertyAttribute_set_order_m2838113430 (PropertyAttribute_t2606999759 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
