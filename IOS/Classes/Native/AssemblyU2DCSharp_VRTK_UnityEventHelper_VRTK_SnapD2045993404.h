﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_SnapDropZone
struct VRTK_SnapDropZone_t1948041105;
// VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t447141393;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents
struct  VRTK_SnapDropZone_UnityEvents_t2045993404  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_SnapDropZone VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents::sdz
	VRTK_SnapDropZone_t1948041105 * ___sdz_2;
	// VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents::OnObjectEnteredSnapDropZone
	UnityObjectEvent_t447141393 * ___OnObjectEnteredSnapDropZone_3;
	// VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents::OnObjectExitedSnapDropZone
	UnityObjectEvent_t447141393 * ___OnObjectExitedSnapDropZone_4;
	// VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents::OnObjectSnappedToDropZone
	UnityObjectEvent_t447141393 * ___OnObjectSnappedToDropZone_5;
	// VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents::OnObjectUnsnappedFromDropZone
	UnityObjectEvent_t447141393 * ___OnObjectUnsnappedFromDropZone_6;

public:
	inline static int32_t get_offset_of_sdz_2() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_UnityEvents_t2045993404, ___sdz_2)); }
	inline VRTK_SnapDropZone_t1948041105 * get_sdz_2() const { return ___sdz_2; }
	inline VRTK_SnapDropZone_t1948041105 ** get_address_of_sdz_2() { return &___sdz_2; }
	inline void set_sdz_2(VRTK_SnapDropZone_t1948041105 * value)
	{
		___sdz_2 = value;
		Il2CppCodeGenWriteBarrier(&___sdz_2, value);
	}

	inline static int32_t get_offset_of_OnObjectEnteredSnapDropZone_3() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_UnityEvents_t2045993404, ___OnObjectEnteredSnapDropZone_3)); }
	inline UnityObjectEvent_t447141393 * get_OnObjectEnteredSnapDropZone_3() const { return ___OnObjectEnteredSnapDropZone_3; }
	inline UnityObjectEvent_t447141393 ** get_address_of_OnObjectEnteredSnapDropZone_3() { return &___OnObjectEnteredSnapDropZone_3; }
	inline void set_OnObjectEnteredSnapDropZone_3(UnityObjectEvent_t447141393 * value)
	{
		___OnObjectEnteredSnapDropZone_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnObjectEnteredSnapDropZone_3, value);
	}

	inline static int32_t get_offset_of_OnObjectExitedSnapDropZone_4() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_UnityEvents_t2045993404, ___OnObjectExitedSnapDropZone_4)); }
	inline UnityObjectEvent_t447141393 * get_OnObjectExitedSnapDropZone_4() const { return ___OnObjectExitedSnapDropZone_4; }
	inline UnityObjectEvent_t447141393 ** get_address_of_OnObjectExitedSnapDropZone_4() { return &___OnObjectExitedSnapDropZone_4; }
	inline void set_OnObjectExitedSnapDropZone_4(UnityObjectEvent_t447141393 * value)
	{
		___OnObjectExitedSnapDropZone_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnObjectExitedSnapDropZone_4, value);
	}

	inline static int32_t get_offset_of_OnObjectSnappedToDropZone_5() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_UnityEvents_t2045993404, ___OnObjectSnappedToDropZone_5)); }
	inline UnityObjectEvent_t447141393 * get_OnObjectSnappedToDropZone_5() const { return ___OnObjectSnappedToDropZone_5; }
	inline UnityObjectEvent_t447141393 ** get_address_of_OnObjectSnappedToDropZone_5() { return &___OnObjectSnappedToDropZone_5; }
	inline void set_OnObjectSnappedToDropZone_5(UnityObjectEvent_t447141393 * value)
	{
		___OnObjectSnappedToDropZone_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnObjectSnappedToDropZone_5, value);
	}

	inline static int32_t get_offset_of_OnObjectUnsnappedFromDropZone_6() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_UnityEvents_t2045993404, ___OnObjectUnsnappedFromDropZone_6)); }
	inline UnityObjectEvent_t447141393 * get_OnObjectUnsnappedFromDropZone_6() const { return ___OnObjectUnsnappedFromDropZone_6; }
	inline UnityObjectEvent_t447141393 ** get_address_of_OnObjectUnsnappedFromDropZone_6() { return &___OnObjectUnsnappedFromDropZone_6; }
	inline void set_OnObjectUnsnappedFromDropZone_6(UnityObjectEvent_t447141393 * value)
	{
		___OnObjectUnsnappedFromDropZone_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnObjectUnsnappedFromDropZone_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
