﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Utilities.SceneChanger
struct SceneChanger_t1690824060;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.Examples.Utilities.SceneChanger::.ctor()
extern "C"  void SceneChanger__ctor_m3288491041 (SceneChanger_t1690824060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Utilities.SceneChanger::Awake()
extern "C"  void SceneChanger_Awake_m2661072828 (SceneChanger_t1690824060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.Examples.Utilities.SceneChanger::ForwardPressed()
extern "C"  bool SceneChanger_ForwardPressed_m2536753052 (SceneChanger_t1690824060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.Examples.Utilities.SceneChanger::BackPressed()
extern "C"  bool SceneChanger_BackPressed_m198885422 (SceneChanger_t1690824060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Utilities.SceneChanger::ResetPress()
extern "C"  void SceneChanger_ResetPress_m3331439167 (SceneChanger_t1690824060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Utilities.SceneChanger::Update()
extern "C"  void SceneChanger_Update_m26815370 (SceneChanger_t1690824060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
