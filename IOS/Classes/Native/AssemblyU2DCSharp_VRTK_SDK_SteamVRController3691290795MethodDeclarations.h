﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_SteamVRController
struct SDK_SteamVRController_t3691290795;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_SteamVRController::.ctor()
extern "C"  void SDK_SteamVRController__ctor_m3196720307 (SDK_SteamVRController_t3691290795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
