﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_SetApplicationAutoLaunch
struct _SetApplicationAutoLaunch_t1014802842;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_SetApplicationAutoLaunch::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetApplicationAutoLaunch__ctor_m205427469 (_SetApplicationAutoLaunch_t1014802842 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_SetApplicationAutoLaunch::Invoke(System.String,System.Boolean)
extern "C"  int32_t _SetApplicationAutoLaunch_Invoke_m3574528488 (_SetApplicationAutoLaunch_t1014802842 * __this, String_t* ___pchAppKey0, bool ___bAutoLaunch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_SetApplicationAutoLaunch::BeginInvoke(System.String,System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetApplicationAutoLaunch_BeginInvoke_m3888769223 (_SetApplicationAutoLaunch_t1014802842 * __this, String_t* ___pchAppKey0, bool ___bAutoLaunch1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_SetApplicationAutoLaunch::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetApplicationAutoLaunch_EndInvoke_m3057194739 (_SetApplicationAutoLaunch_t1014802842 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
