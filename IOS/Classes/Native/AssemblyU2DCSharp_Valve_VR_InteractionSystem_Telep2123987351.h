﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.LineRenderer[]
struct LineRendererU5BU5D_t1871935838;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.TeleportArc
struct  TeleportArc_t2123987351  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Valve.VR.InteractionSystem.TeleportArc::segmentCount
	int32_t ___segmentCount_2;
	// System.Single Valve.VR.InteractionSystem.TeleportArc::thickness
	float ___thickness_3;
	// System.Single Valve.VR.InteractionSystem.TeleportArc::arcDuration
	float ___arcDuration_4;
	// System.Single Valve.VR.InteractionSystem.TeleportArc::segmentBreak
	float ___segmentBreak_5;
	// System.Single Valve.VR.InteractionSystem.TeleportArc::arcSpeed
	float ___arcSpeed_6;
	// UnityEngine.Material Valve.VR.InteractionSystem.TeleportArc::material
	Material_t193706927 * ___material_7;
	// System.Int32 Valve.VR.InteractionSystem.TeleportArc::traceLayerMask
	int32_t ___traceLayerMask_8;
	// UnityEngine.LineRenderer[] Valve.VR.InteractionSystem.TeleportArc::lineRenderers
	LineRendererU5BU5D_t1871935838* ___lineRenderers_9;
	// System.Single Valve.VR.InteractionSystem.TeleportArc::arcTimeOffset
	float ___arcTimeOffset_10;
	// System.Single Valve.VR.InteractionSystem.TeleportArc::prevThickness
	float ___prevThickness_11;
	// System.Int32 Valve.VR.InteractionSystem.TeleportArc::prevSegmentCount
	int32_t ___prevSegmentCount_12;
	// System.Boolean Valve.VR.InteractionSystem.TeleportArc::showArc
	bool ___showArc_13;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.TeleportArc::startPos
	Vector3_t2243707580  ___startPos_14;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.TeleportArc::projectileVelocity
	Vector3_t2243707580  ___projectileVelocity_15;
	// System.Boolean Valve.VR.InteractionSystem.TeleportArc::useGravity
	bool ___useGravity_16;
	// UnityEngine.Transform Valve.VR.InteractionSystem.TeleportArc::arcObjectsTransfrom
	Transform_t3275118058 * ___arcObjectsTransfrom_17;
	// System.Boolean Valve.VR.InteractionSystem.TeleportArc::arcInvalid
	bool ___arcInvalid_18;

public:
	inline static int32_t get_offset_of_segmentCount_2() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___segmentCount_2)); }
	inline int32_t get_segmentCount_2() const { return ___segmentCount_2; }
	inline int32_t* get_address_of_segmentCount_2() { return &___segmentCount_2; }
	inline void set_segmentCount_2(int32_t value)
	{
		___segmentCount_2 = value;
	}

	inline static int32_t get_offset_of_thickness_3() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___thickness_3)); }
	inline float get_thickness_3() const { return ___thickness_3; }
	inline float* get_address_of_thickness_3() { return &___thickness_3; }
	inline void set_thickness_3(float value)
	{
		___thickness_3 = value;
	}

	inline static int32_t get_offset_of_arcDuration_4() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___arcDuration_4)); }
	inline float get_arcDuration_4() const { return ___arcDuration_4; }
	inline float* get_address_of_arcDuration_4() { return &___arcDuration_4; }
	inline void set_arcDuration_4(float value)
	{
		___arcDuration_4 = value;
	}

	inline static int32_t get_offset_of_segmentBreak_5() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___segmentBreak_5)); }
	inline float get_segmentBreak_5() const { return ___segmentBreak_5; }
	inline float* get_address_of_segmentBreak_5() { return &___segmentBreak_5; }
	inline void set_segmentBreak_5(float value)
	{
		___segmentBreak_5 = value;
	}

	inline static int32_t get_offset_of_arcSpeed_6() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___arcSpeed_6)); }
	inline float get_arcSpeed_6() const { return ___arcSpeed_6; }
	inline float* get_address_of_arcSpeed_6() { return &___arcSpeed_6; }
	inline void set_arcSpeed_6(float value)
	{
		___arcSpeed_6 = value;
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___material_7)); }
	inline Material_t193706927 * get_material_7() const { return ___material_7; }
	inline Material_t193706927 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_t193706927 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier(&___material_7, value);
	}

	inline static int32_t get_offset_of_traceLayerMask_8() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___traceLayerMask_8)); }
	inline int32_t get_traceLayerMask_8() const { return ___traceLayerMask_8; }
	inline int32_t* get_address_of_traceLayerMask_8() { return &___traceLayerMask_8; }
	inline void set_traceLayerMask_8(int32_t value)
	{
		___traceLayerMask_8 = value;
	}

	inline static int32_t get_offset_of_lineRenderers_9() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___lineRenderers_9)); }
	inline LineRendererU5BU5D_t1871935838* get_lineRenderers_9() const { return ___lineRenderers_9; }
	inline LineRendererU5BU5D_t1871935838** get_address_of_lineRenderers_9() { return &___lineRenderers_9; }
	inline void set_lineRenderers_9(LineRendererU5BU5D_t1871935838* value)
	{
		___lineRenderers_9 = value;
		Il2CppCodeGenWriteBarrier(&___lineRenderers_9, value);
	}

	inline static int32_t get_offset_of_arcTimeOffset_10() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___arcTimeOffset_10)); }
	inline float get_arcTimeOffset_10() const { return ___arcTimeOffset_10; }
	inline float* get_address_of_arcTimeOffset_10() { return &___arcTimeOffset_10; }
	inline void set_arcTimeOffset_10(float value)
	{
		___arcTimeOffset_10 = value;
	}

	inline static int32_t get_offset_of_prevThickness_11() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___prevThickness_11)); }
	inline float get_prevThickness_11() const { return ___prevThickness_11; }
	inline float* get_address_of_prevThickness_11() { return &___prevThickness_11; }
	inline void set_prevThickness_11(float value)
	{
		___prevThickness_11 = value;
	}

	inline static int32_t get_offset_of_prevSegmentCount_12() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___prevSegmentCount_12)); }
	inline int32_t get_prevSegmentCount_12() const { return ___prevSegmentCount_12; }
	inline int32_t* get_address_of_prevSegmentCount_12() { return &___prevSegmentCount_12; }
	inline void set_prevSegmentCount_12(int32_t value)
	{
		___prevSegmentCount_12 = value;
	}

	inline static int32_t get_offset_of_showArc_13() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___showArc_13)); }
	inline bool get_showArc_13() const { return ___showArc_13; }
	inline bool* get_address_of_showArc_13() { return &___showArc_13; }
	inline void set_showArc_13(bool value)
	{
		___showArc_13 = value;
	}

	inline static int32_t get_offset_of_startPos_14() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___startPos_14)); }
	inline Vector3_t2243707580  get_startPos_14() const { return ___startPos_14; }
	inline Vector3_t2243707580 * get_address_of_startPos_14() { return &___startPos_14; }
	inline void set_startPos_14(Vector3_t2243707580  value)
	{
		___startPos_14 = value;
	}

	inline static int32_t get_offset_of_projectileVelocity_15() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___projectileVelocity_15)); }
	inline Vector3_t2243707580  get_projectileVelocity_15() const { return ___projectileVelocity_15; }
	inline Vector3_t2243707580 * get_address_of_projectileVelocity_15() { return &___projectileVelocity_15; }
	inline void set_projectileVelocity_15(Vector3_t2243707580  value)
	{
		___projectileVelocity_15 = value;
	}

	inline static int32_t get_offset_of_useGravity_16() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___useGravity_16)); }
	inline bool get_useGravity_16() const { return ___useGravity_16; }
	inline bool* get_address_of_useGravity_16() { return &___useGravity_16; }
	inline void set_useGravity_16(bool value)
	{
		___useGravity_16 = value;
	}

	inline static int32_t get_offset_of_arcObjectsTransfrom_17() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___arcObjectsTransfrom_17)); }
	inline Transform_t3275118058 * get_arcObjectsTransfrom_17() const { return ___arcObjectsTransfrom_17; }
	inline Transform_t3275118058 ** get_address_of_arcObjectsTransfrom_17() { return &___arcObjectsTransfrom_17; }
	inline void set_arcObjectsTransfrom_17(Transform_t3275118058 * value)
	{
		___arcObjectsTransfrom_17 = value;
		Il2CppCodeGenWriteBarrier(&___arcObjectsTransfrom_17, value);
	}

	inline static int32_t get_offset_of_arcInvalid_18() { return static_cast<int32_t>(offsetof(TeleportArc_t2123987351, ___arcInvalid_18)); }
	inline bool get_arcInvalid_18() const { return ___arcInvalid_18; }
	inline bool* get_address_of_arcInvalid_18() { return &___arcInvalid_18; }
	inline void set_arcInvalid_18(bool value)
	{
		___arcInvalid_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
