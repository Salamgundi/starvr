﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRApplications
struct CVRApplications_t1900926488;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// Valve.VR.AppOverrideKeys_t[]
struct AppOverrideKeys_tU5BU5D_t3538561671;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationProperty1959780520.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationTransitio3895609521.h"

// System.Void Valve.VR.CVRApplications::.ctor(System.IntPtr)
extern "C"  void CVRApplications__ctor_m3615588523 (CVRApplications_t1900926488 * __this, IntPtr_t ___pInterface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.CVRApplications::AddApplicationManifest(System.String,System.Boolean)
extern "C"  int32_t CVRApplications_AddApplicationManifest_m804480956 (CVRApplications_t1900926488 * __this, String_t* ___pchApplicationManifestFullPath0, bool ___bTemporary1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.CVRApplications::RemoveApplicationManifest(System.String)
extern "C"  int32_t CVRApplications_RemoveApplicationManifest_m3766635630 (CVRApplications_t1900926488 * __this, String_t* ___pchApplicationManifestFullPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRApplications::IsApplicationInstalled(System.String)
extern "C"  bool CVRApplications_IsApplicationInstalled_m1833982659 (CVRApplications_t1900926488 * __this, String_t* ___pchAppKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRApplications::GetApplicationCount()
extern "C"  uint32_t CVRApplications_GetApplicationCount_m3955173953 (CVRApplications_t1900926488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.CVRApplications::GetApplicationKeyByIndex(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern "C"  int32_t CVRApplications_GetApplicationKeyByIndex_m223228867 (CVRApplications_t1900926488 * __this, uint32_t ___unApplicationIndex0, StringBuilder_t1221177846 * ___pchAppKeyBuffer1, uint32_t ___unAppKeyBufferLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.CVRApplications::GetApplicationKeyByProcessId(System.UInt32,System.String,System.UInt32)
extern "C"  int32_t CVRApplications_GetApplicationKeyByProcessId_m19509541 (CVRApplications_t1900926488 * __this, uint32_t ___unProcessId0, String_t* ___pchAppKeyBuffer1, uint32_t ___unAppKeyBufferLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.CVRApplications::LaunchApplication(System.String)
extern "C"  int32_t CVRApplications_LaunchApplication_m130859438 (CVRApplications_t1900926488 * __this, String_t* ___pchAppKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.CVRApplications::LaunchTemplateApplication(System.String,System.String,Valve.VR.AppOverrideKeys_t[])
extern "C"  int32_t CVRApplications_LaunchTemplateApplication_m842628428 (CVRApplications_t1900926488 * __this, String_t* ___pchTemplateAppKey0, String_t* ___pchNewAppKey1, AppOverrideKeys_tU5BU5D_t3538561671* ___pKeys2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.CVRApplications::LaunchApplicationFromMimeType(System.String,System.String)
extern "C"  int32_t CVRApplications_LaunchApplicationFromMimeType_m4194815298 (CVRApplications_t1900926488 * __this, String_t* ___pchMimeType0, String_t* ___pchArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.CVRApplications::LaunchDashboardOverlay(System.String)
extern "C"  int32_t CVRApplications_LaunchDashboardOverlay_m865418434 (CVRApplications_t1900926488 * __this, String_t* ___pchAppKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRApplications::CancelApplicationLaunch(System.String)
extern "C"  bool CVRApplications_CancelApplicationLaunch_m2827274476 (CVRApplications_t1900926488 * __this, String_t* ___pchAppKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.CVRApplications::IdentifyApplication(System.UInt32,System.String)
extern "C"  int32_t CVRApplications_IdentifyApplication_m1202881493 (CVRApplications_t1900926488 * __this, uint32_t ___unProcessId0, String_t* ___pchAppKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRApplications::GetApplicationProcessId(System.String)
extern "C"  uint32_t CVRApplications_GetApplicationProcessId_m1652300460 (CVRApplications_t1900926488 * __this, String_t* ___pchAppKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.CVRApplications::GetApplicationsErrorNameFromEnum(Valve.VR.EVRApplicationError)
extern "C"  String_t* CVRApplications_GetApplicationsErrorNameFromEnum_m2109200624 (CVRApplications_t1900926488 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRApplications::GetApplicationPropertyString(System.String,Valve.VR.EVRApplicationProperty,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRApplicationError&)
extern "C"  uint32_t CVRApplications_GetApplicationPropertyString_m4027315363 (CVRApplications_t1900926488 * __this, String_t* ___pchAppKey0, int32_t ___eProperty1, StringBuilder_t1221177846 * ___pchPropertyValueBuffer2, uint32_t ___unPropertyValueBufferLen3, int32_t* ___peError4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRApplications::GetApplicationPropertyBool(System.String,Valve.VR.EVRApplicationProperty,Valve.VR.EVRApplicationError&)
extern "C"  bool CVRApplications_GetApplicationPropertyBool_m246700291 (CVRApplications_t1900926488 * __this, String_t* ___pchAppKey0, int32_t ___eProperty1, int32_t* ___peError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.CVRApplications::GetApplicationPropertyUint64(System.String,Valve.VR.EVRApplicationProperty,Valve.VR.EVRApplicationError&)
extern "C"  uint64_t CVRApplications_GetApplicationPropertyUint64_m2693501725 (CVRApplications_t1900926488 * __this, String_t* ___pchAppKey0, int32_t ___eProperty1, int32_t* ___peError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.CVRApplications::SetApplicationAutoLaunch(System.String,System.Boolean)
extern "C"  int32_t CVRApplications_SetApplicationAutoLaunch_m103796892 (CVRApplications_t1900926488 * __this, String_t* ___pchAppKey0, bool ___bAutoLaunch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRApplications::GetApplicationAutoLaunch(System.String)
extern "C"  bool CVRApplications_GetApplicationAutoLaunch_m1009941637 (CVRApplications_t1900926488 * __this, String_t* ___pchAppKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.CVRApplications::SetDefaultApplicationForMimeType(System.String,System.String)
extern "C"  int32_t CVRApplications_SetDefaultApplicationForMimeType_m3725380209 (CVRApplications_t1900926488 * __this, String_t* ___pchAppKey0, String_t* ___pchMimeType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRApplications::GetDefaultApplicationForMimeType(System.String,System.String,System.UInt32)
extern "C"  bool CVRApplications_GetDefaultApplicationForMimeType_m1906849285 (CVRApplications_t1900926488 * __this, String_t* ___pchMimeType0, String_t* ___pchAppKeyBuffer1, uint32_t ___unAppKeyBufferLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRApplications::GetApplicationSupportedMimeTypes(System.String,System.String,System.UInt32)
extern "C"  bool CVRApplications_GetApplicationSupportedMimeTypes_m2872171734 (CVRApplications_t1900926488 * __this, String_t* ___pchAppKey0, String_t* ___pchMimeTypesBuffer1, uint32_t ___unMimeTypesBuffer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRApplications::GetApplicationsThatSupportMimeType(System.String,System.String,System.UInt32)
extern "C"  uint32_t CVRApplications_GetApplicationsThatSupportMimeType_m1827738519 (CVRApplications_t1900926488 * __this, String_t* ___pchMimeType0, String_t* ___pchAppKeysThatSupportBuffer1, uint32_t ___unAppKeysThatSupportBuffer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRApplications::GetApplicationLaunchArguments(System.UInt32,System.String,System.UInt32)
extern "C"  uint32_t CVRApplications_GetApplicationLaunchArguments_m3970627421 (CVRApplications_t1900926488 * __this, uint32_t ___unHandle0, String_t* ___pchArgs1, uint32_t ___unArgs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.CVRApplications::GetStartingApplication(System.String,System.UInt32)
extern "C"  int32_t CVRApplications_GetStartingApplication_m3023646343 (CVRApplications_t1900926488 * __this, String_t* ___pchAppKeyBuffer0, uint32_t ___unAppKeyBufferLen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationTransitionState Valve.VR.CVRApplications::GetTransitionState()
extern "C"  int32_t CVRApplications_GetTransitionState_m1631651367 (CVRApplications_t1900926488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.CVRApplications::PerformApplicationPrelaunchCheck(System.String)
extern "C"  int32_t CVRApplications_PerformApplicationPrelaunchCheck_m1660167810 (CVRApplications_t1900926488 * __this, String_t* ___pchAppKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.CVRApplications::GetApplicationsTransitionStateNameFromEnum(Valve.VR.EVRApplicationTransitionState)
extern "C"  String_t* CVRApplications_GetApplicationsTransitionStateNameFromEnum_m3950000112 (CVRApplications_t1900926488 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRApplications::IsQuitUserPromptRequested()
extern "C"  bool CVRApplications_IsQuitUserPromptRequested_m1347343487 (CVRApplications_t1900926488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.CVRApplications::LaunchInternalProcess(System.String,System.String,System.String)
extern "C"  int32_t CVRApplications_LaunchInternalProcess_m2528947070 (CVRApplications_t1900926488 * __this, String_t* ___pchBinaryPath0, String_t* ___pchArguments1, String_t* ___pchWorkingDirectory2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRApplications::GetCurrentSceneProcessId()
extern "C"  uint32_t CVRApplications_GetCurrentSceneProcessId_m4191298917 (CVRApplications_t1900926488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
