﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>
struct Dictionary_2_t1012981408;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ConsoleViewer
struct  VRTK_ConsoleViewer_t2446975743  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 VRTK.VRTK_ConsoleViewer::fontSize
	int32_t ___fontSize_2;
	// UnityEngine.Color VRTK.VRTK_ConsoleViewer::infoMessage
	Color_t2020392075  ___infoMessage_3;
	// UnityEngine.Color VRTK.VRTK_ConsoleViewer::assertMessage
	Color_t2020392075  ___assertMessage_4;
	// UnityEngine.Color VRTK.VRTK_ConsoleViewer::warningMessage
	Color_t2020392075  ___warningMessage_5;
	// UnityEngine.Color VRTK.VRTK_ConsoleViewer::errorMessage
	Color_t2020392075  ___errorMessage_6;
	// UnityEngine.Color VRTK.VRTK_ConsoleViewer::exceptionMessage
	Color_t2020392075  ___exceptionMessage_7;
	// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color> VRTK.VRTK_ConsoleViewer::logTypeColors
	Dictionary_2_t1012981408 * ___logTypeColors_8;
	// UnityEngine.UI.ScrollRect VRTK.VRTK_ConsoleViewer::scrollWindow
	ScrollRect_t1199013257 * ___scrollWindow_9;
	// UnityEngine.RectTransform VRTK.VRTK_ConsoleViewer::consoleRect
	RectTransform_t3349966182 * ___consoleRect_10;
	// UnityEngine.UI.Text VRTK.VRTK_ConsoleViewer::consoleOutput
	Text_t356221433 * ___consoleOutput_11;
	// System.Int32 VRTK.VRTK_ConsoleViewer::lineBuffer
	int32_t ___lineBuffer_13;
	// System.Int32 VRTK.VRTK_ConsoleViewer::currentBuffer
	int32_t ___currentBuffer_14;
	// System.String VRTK.VRTK_ConsoleViewer::lastMessage
	String_t* ___lastMessage_15;
	// System.Boolean VRTK.VRTK_ConsoleViewer::collapseLog
	bool ___collapseLog_16;

public:
	inline static int32_t get_offset_of_fontSize_2() { return static_cast<int32_t>(offsetof(VRTK_ConsoleViewer_t2446975743, ___fontSize_2)); }
	inline int32_t get_fontSize_2() const { return ___fontSize_2; }
	inline int32_t* get_address_of_fontSize_2() { return &___fontSize_2; }
	inline void set_fontSize_2(int32_t value)
	{
		___fontSize_2 = value;
	}

	inline static int32_t get_offset_of_infoMessage_3() { return static_cast<int32_t>(offsetof(VRTK_ConsoleViewer_t2446975743, ___infoMessage_3)); }
	inline Color_t2020392075  get_infoMessage_3() const { return ___infoMessage_3; }
	inline Color_t2020392075 * get_address_of_infoMessage_3() { return &___infoMessage_3; }
	inline void set_infoMessage_3(Color_t2020392075  value)
	{
		___infoMessage_3 = value;
	}

	inline static int32_t get_offset_of_assertMessage_4() { return static_cast<int32_t>(offsetof(VRTK_ConsoleViewer_t2446975743, ___assertMessage_4)); }
	inline Color_t2020392075  get_assertMessage_4() const { return ___assertMessage_4; }
	inline Color_t2020392075 * get_address_of_assertMessage_4() { return &___assertMessage_4; }
	inline void set_assertMessage_4(Color_t2020392075  value)
	{
		___assertMessage_4 = value;
	}

	inline static int32_t get_offset_of_warningMessage_5() { return static_cast<int32_t>(offsetof(VRTK_ConsoleViewer_t2446975743, ___warningMessage_5)); }
	inline Color_t2020392075  get_warningMessage_5() const { return ___warningMessage_5; }
	inline Color_t2020392075 * get_address_of_warningMessage_5() { return &___warningMessage_5; }
	inline void set_warningMessage_5(Color_t2020392075  value)
	{
		___warningMessage_5 = value;
	}

	inline static int32_t get_offset_of_errorMessage_6() { return static_cast<int32_t>(offsetof(VRTK_ConsoleViewer_t2446975743, ___errorMessage_6)); }
	inline Color_t2020392075  get_errorMessage_6() const { return ___errorMessage_6; }
	inline Color_t2020392075 * get_address_of_errorMessage_6() { return &___errorMessage_6; }
	inline void set_errorMessage_6(Color_t2020392075  value)
	{
		___errorMessage_6 = value;
	}

	inline static int32_t get_offset_of_exceptionMessage_7() { return static_cast<int32_t>(offsetof(VRTK_ConsoleViewer_t2446975743, ___exceptionMessage_7)); }
	inline Color_t2020392075  get_exceptionMessage_7() const { return ___exceptionMessage_7; }
	inline Color_t2020392075 * get_address_of_exceptionMessage_7() { return &___exceptionMessage_7; }
	inline void set_exceptionMessage_7(Color_t2020392075  value)
	{
		___exceptionMessage_7 = value;
	}

	inline static int32_t get_offset_of_logTypeColors_8() { return static_cast<int32_t>(offsetof(VRTK_ConsoleViewer_t2446975743, ___logTypeColors_8)); }
	inline Dictionary_2_t1012981408 * get_logTypeColors_8() const { return ___logTypeColors_8; }
	inline Dictionary_2_t1012981408 ** get_address_of_logTypeColors_8() { return &___logTypeColors_8; }
	inline void set_logTypeColors_8(Dictionary_2_t1012981408 * value)
	{
		___logTypeColors_8 = value;
		Il2CppCodeGenWriteBarrier(&___logTypeColors_8, value);
	}

	inline static int32_t get_offset_of_scrollWindow_9() { return static_cast<int32_t>(offsetof(VRTK_ConsoleViewer_t2446975743, ___scrollWindow_9)); }
	inline ScrollRect_t1199013257 * get_scrollWindow_9() const { return ___scrollWindow_9; }
	inline ScrollRect_t1199013257 ** get_address_of_scrollWindow_9() { return &___scrollWindow_9; }
	inline void set_scrollWindow_9(ScrollRect_t1199013257 * value)
	{
		___scrollWindow_9 = value;
		Il2CppCodeGenWriteBarrier(&___scrollWindow_9, value);
	}

	inline static int32_t get_offset_of_consoleRect_10() { return static_cast<int32_t>(offsetof(VRTK_ConsoleViewer_t2446975743, ___consoleRect_10)); }
	inline RectTransform_t3349966182 * get_consoleRect_10() const { return ___consoleRect_10; }
	inline RectTransform_t3349966182 ** get_address_of_consoleRect_10() { return &___consoleRect_10; }
	inline void set_consoleRect_10(RectTransform_t3349966182 * value)
	{
		___consoleRect_10 = value;
		Il2CppCodeGenWriteBarrier(&___consoleRect_10, value);
	}

	inline static int32_t get_offset_of_consoleOutput_11() { return static_cast<int32_t>(offsetof(VRTK_ConsoleViewer_t2446975743, ___consoleOutput_11)); }
	inline Text_t356221433 * get_consoleOutput_11() const { return ___consoleOutput_11; }
	inline Text_t356221433 ** get_address_of_consoleOutput_11() { return &___consoleOutput_11; }
	inline void set_consoleOutput_11(Text_t356221433 * value)
	{
		___consoleOutput_11 = value;
		Il2CppCodeGenWriteBarrier(&___consoleOutput_11, value);
	}

	inline static int32_t get_offset_of_lineBuffer_13() { return static_cast<int32_t>(offsetof(VRTK_ConsoleViewer_t2446975743, ___lineBuffer_13)); }
	inline int32_t get_lineBuffer_13() const { return ___lineBuffer_13; }
	inline int32_t* get_address_of_lineBuffer_13() { return &___lineBuffer_13; }
	inline void set_lineBuffer_13(int32_t value)
	{
		___lineBuffer_13 = value;
	}

	inline static int32_t get_offset_of_currentBuffer_14() { return static_cast<int32_t>(offsetof(VRTK_ConsoleViewer_t2446975743, ___currentBuffer_14)); }
	inline int32_t get_currentBuffer_14() const { return ___currentBuffer_14; }
	inline int32_t* get_address_of_currentBuffer_14() { return &___currentBuffer_14; }
	inline void set_currentBuffer_14(int32_t value)
	{
		___currentBuffer_14 = value;
	}

	inline static int32_t get_offset_of_lastMessage_15() { return static_cast<int32_t>(offsetof(VRTK_ConsoleViewer_t2446975743, ___lastMessage_15)); }
	inline String_t* get_lastMessage_15() const { return ___lastMessage_15; }
	inline String_t** get_address_of_lastMessage_15() { return &___lastMessage_15; }
	inline void set_lastMessage_15(String_t* value)
	{
		___lastMessage_15 = value;
		Il2CppCodeGenWriteBarrier(&___lastMessage_15, value);
	}

	inline static int32_t get_offset_of_collapseLog_16() { return static_cast<int32_t>(offsetof(VRTK_ConsoleViewer_t2446975743, ___collapseLog_16)); }
	inline bool get_collapseLog_16() const { return ___collapseLog_16; }
	inline bool* get_address_of_collapseLog_16() { return &___collapseLog_16; }
	inline void set_collapseLog_16(bool value)
	{
		___collapseLog_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
