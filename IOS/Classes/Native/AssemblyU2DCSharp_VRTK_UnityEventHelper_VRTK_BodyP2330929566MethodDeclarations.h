﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents
struct VRTK_BodyPhysics_UnityEvents_t2330929566;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_BodyPhysicsEventArgs2230131654.h"

// System.Void VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::.ctor()
extern "C"  void VRTK_BodyPhysics_UnityEvents__ctor_m1253779073 (VRTK_BodyPhysics_UnityEvents_t2330929566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::SetBodyPhysics()
extern "C"  void VRTK_BodyPhysics_UnityEvents_SetBodyPhysics_m649486378 (VRTK_BodyPhysics_UnityEvents_t2330929566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::OnEnable()
extern "C"  void VRTK_BodyPhysics_UnityEvents_OnEnable_m263169257 (VRTK_BodyPhysics_UnityEvents_t2330929566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::StartFalling(System.Object,VRTK.BodyPhysicsEventArgs)
extern "C"  void VRTK_BodyPhysics_UnityEvents_StartFalling_m908008497 (VRTK_BodyPhysics_UnityEvents_t2330929566 * __this, Il2CppObject * ___o0, BodyPhysicsEventArgs_t2230131654  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::StopFalling(System.Object,VRTK.BodyPhysicsEventArgs)
extern "C"  void VRTK_BodyPhysics_UnityEvents_StopFalling_m25010769 (VRTK_BodyPhysics_UnityEvents_t2330929566 * __this, Il2CppObject * ___o0, BodyPhysicsEventArgs_t2230131654  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::StartMoving(System.Object,VRTK.BodyPhysicsEventArgs)
extern "C"  void VRTK_BodyPhysics_UnityEvents_StartMoving_m3940898518 (VRTK_BodyPhysics_UnityEvents_t2330929566 * __this, Il2CppObject * ___o0, BodyPhysicsEventArgs_t2230131654  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::StopMoving(System.Object,VRTK.BodyPhysicsEventArgs)
extern "C"  void VRTK_BodyPhysics_UnityEvents_StopMoving_m1558264718 (VRTK_BodyPhysics_UnityEvents_t2330929566 * __this, Il2CppObject * ___o0, BodyPhysicsEventArgs_t2230131654  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::StartColliding(System.Object,VRTK.BodyPhysicsEventArgs)
extern "C"  void VRTK_BodyPhysics_UnityEvents_StartColliding_m2440460127 (VRTK_BodyPhysics_UnityEvents_t2330929566 * __this, Il2CppObject * ___o0, BodyPhysicsEventArgs_t2230131654  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::StopColliding(System.Object,VRTK.BodyPhysicsEventArgs)
extern "C"  void VRTK_BodyPhysics_UnityEvents_StopColliding_m1678068347 (VRTK_BodyPhysics_UnityEvents_t2330929566 * __this, Il2CppObject * ___o0, BodyPhysicsEventArgs_t2230131654  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::OnDisable()
extern "C"  void VRTK_BodyPhysics_UnityEvents_OnDisable_m941904164 (VRTK_BodyPhysics_UnityEvents_t2330929566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
