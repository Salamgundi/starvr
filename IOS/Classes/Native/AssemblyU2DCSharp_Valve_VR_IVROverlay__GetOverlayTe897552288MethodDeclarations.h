﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayTexture
struct _GetOverlayTexture_t897552288;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_ETextureType992125572.h"
#include "AssemblyU2DCSharp_Valve_VR_EColorSpace2848861630.h"
#include "AssemblyU2DCSharp_Valve_VR_VRTextureBounds_t1897807375.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayTexture::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayTexture__ctor_m2922697447 (_GetOverlayTexture_t897552288 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTexture::Invoke(System.UInt64,System.IntPtr&,System.IntPtr,System.UInt32&,System.UInt32&,System.UInt32&,Valve.VR.ETextureType&,Valve.VR.EColorSpace&,Valve.VR.VRTextureBounds_t&)
extern "C"  int32_t _GetOverlayTexture_Invoke_m3699648145 (_GetOverlayTexture_t897552288 * __this, uint64_t ___ulOverlayHandle0, IntPtr_t* ___pNativeTextureHandle1, IntPtr_t ___pNativeTextureRef2, uint32_t* ___pWidth3, uint32_t* ___pHeight4, uint32_t* ___pNativeFormat5, int32_t* ___pAPIType6, int32_t* ___pColorSpace7, VRTextureBounds_t_t1897807375 * ___pTextureBounds8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayTexture::BeginInvoke(System.UInt64,System.IntPtr&,System.IntPtr,System.UInt32&,System.UInt32&,System.UInt32&,Valve.VR.ETextureType&,Valve.VR.EColorSpace&,Valve.VR.VRTextureBounds_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayTexture_BeginInvoke_m672475608 (_GetOverlayTexture_t897552288 * __this, uint64_t ___ulOverlayHandle0, IntPtr_t* ___pNativeTextureHandle1, IntPtr_t ___pNativeTextureRef2, uint32_t* ___pWidth3, uint32_t* ___pHeight4, uint32_t* ___pNativeFormat5, int32_t* ___pAPIType6, int32_t* ___pColorSpace7, VRTextureBounds_t_t1897807375 * ___pTextureBounds8, AsyncCallback_t163412349 * ___callback9, Il2CppObject * ___object10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTexture::EndInvoke(System.IntPtr&,System.UInt32&,System.UInt32&,System.UInt32&,Valve.VR.ETextureType&,Valve.VR.EColorSpace&,Valve.VR.VRTextureBounds_t&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayTexture_EndInvoke_m2412684408 (_GetOverlayTexture_t897552288 * __this, IntPtr_t* ___pNativeTextureHandle0, uint32_t* ___pWidth1, uint32_t* ___pHeight2, uint32_t* ___pNativeFormat3, int32_t* ___pAPIType4, int32_t* ___pColorSpace5, VRTextureBounds_t_t1897807375 * ___pTextureBounds6, Il2CppObject * ___result7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
