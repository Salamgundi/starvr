﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.DontDestroyOnLoad
struct DontDestroyOnLoad_t814827202;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.DontDestroyOnLoad::.ctor()
extern "C"  void DontDestroyOnLoad__ctor_m2124025030 (DontDestroyOnLoad_t814827202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.DontDestroyOnLoad::Awake()
extern "C"  void DontDestroyOnLoad_Awake_m1496537187 (DontDestroyOnLoad_t814827202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
