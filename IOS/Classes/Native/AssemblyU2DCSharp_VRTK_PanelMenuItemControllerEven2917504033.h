﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.PanelMenuItemControllerEventArgs
struct  PanelMenuItemControllerEventArgs_t2917504033 
{
public:
	// UnityEngine.GameObject VRTK.PanelMenuItemControllerEventArgs::interactableObject
	GameObject_t1756533147 * ___interactableObject_0;

public:
	inline static int32_t get_offset_of_interactableObject_0() { return static_cast<int32_t>(offsetof(PanelMenuItemControllerEventArgs_t2917504033, ___interactableObject_0)); }
	inline GameObject_t1756533147 * get_interactableObject_0() const { return ___interactableObject_0; }
	inline GameObject_t1756533147 ** get_address_of_interactableObject_0() { return &___interactableObject_0; }
	inline void set_interactableObject_0(GameObject_t1756533147 * value)
	{
		___interactableObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___interactableObject_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VRTK.PanelMenuItemControllerEventArgs
struct PanelMenuItemControllerEventArgs_t2917504033_marshaled_pinvoke
{
	GameObject_t1756533147 * ___interactableObject_0;
};
// Native definition for COM marshalling of VRTK.PanelMenuItemControllerEventArgs
struct PanelMenuItemControllerEventArgs_t2917504033_marshaled_com
{
	GameObject_t1756533147 * ___interactableObject_0;
};
