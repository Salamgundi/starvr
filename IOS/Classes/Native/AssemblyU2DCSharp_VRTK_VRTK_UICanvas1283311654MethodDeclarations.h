﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_UICanvas
struct VRTK_UICanvas_t1283311654;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Canvas
struct Canvas_t209405766;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void VRTK.VRTK_UICanvas::.ctor()
extern "C"  void VRTK_UICanvas__ctor_m1974588954 (VRTK_UICanvas_t1283311654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UICanvas::OnEnable()
extern "C"  void VRTK_UICanvas_OnEnable_m3066735918 (VRTK_UICanvas_t1283311654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UICanvas::OnDisable()
extern "C"  void VRTK_UICanvas_OnDisable_m1138892475 (VRTK_UICanvas_t1283311654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UICanvas::OnDestroy()
extern "C"  void VRTK_UICanvas_OnDestroy_m4209568341 (VRTK_UICanvas_t1283311654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UICanvas::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void VRTK_UICanvas_OnTriggerEnter_m2083917790 (VRTK_UICanvas_t1283311654 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UICanvas::OnTriggerExit(UnityEngine.Collider)
extern "C"  void VRTK_UICanvas_OnTriggerExit_m2747870036 (VRTK_UICanvas_t1283311654 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UICanvas::SetupCanvas()
extern "C"  void VRTK_UICanvas_SetupCanvas_m1915469409 (VRTK_UICanvas_t1283311654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UICanvas::CreateDraggablePanel(UnityEngine.Canvas,UnityEngine.Vector2)
extern "C"  void VRTK_UICanvas_CreateDraggablePanel_m3027513116 (VRTK_UICanvas_t1283311654 * __this, Canvas_t209405766 * ___canvas0, Vector2_t2243707579  ___canvasSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UICanvas::CreateActivator(UnityEngine.Canvas,UnityEngine.Vector2)
extern "C"  void VRTK_UICanvas_CreateActivator_m820516210 (VRTK_UICanvas_t1283311654 * __this, Canvas_t209405766 * ___canvas0, Vector2_t2243707579  ___canvasSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UICanvas::RemoveCanvas()
extern "C"  void VRTK_UICanvas_RemoveCanvas_m3047433396 (VRTK_UICanvas_t1283311654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
