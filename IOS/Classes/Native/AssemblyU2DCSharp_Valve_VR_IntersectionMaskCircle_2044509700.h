﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.IntersectionMaskCircle_t
struct  IntersectionMaskCircle_t_t2044509700 
{
public:
	// System.Single Valve.VR.IntersectionMaskCircle_t::m_flCenterX
	float ___m_flCenterX_0;
	// System.Single Valve.VR.IntersectionMaskCircle_t::m_flCenterY
	float ___m_flCenterY_1;
	// System.Single Valve.VR.IntersectionMaskCircle_t::m_flRadius
	float ___m_flRadius_2;

public:
	inline static int32_t get_offset_of_m_flCenterX_0() { return static_cast<int32_t>(offsetof(IntersectionMaskCircle_t_t2044509700, ___m_flCenterX_0)); }
	inline float get_m_flCenterX_0() const { return ___m_flCenterX_0; }
	inline float* get_address_of_m_flCenterX_0() { return &___m_flCenterX_0; }
	inline void set_m_flCenterX_0(float value)
	{
		___m_flCenterX_0 = value;
	}

	inline static int32_t get_offset_of_m_flCenterY_1() { return static_cast<int32_t>(offsetof(IntersectionMaskCircle_t_t2044509700, ___m_flCenterY_1)); }
	inline float get_m_flCenterY_1() const { return ___m_flCenterY_1; }
	inline float* get_address_of_m_flCenterY_1() { return &___m_flCenterY_1; }
	inline void set_m_flCenterY_1(float value)
	{
		___m_flCenterY_1 = value;
	}

	inline static int32_t get_offset_of_m_flRadius_2() { return static_cast<int32_t>(offsetof(IntersectionMaskCircle_t_t2044509700, ___m_flRadius_2)); }
	inline float get_m_flRadius_2() const { return ___m_flRadius_2; }
	inline float* get_address_of_m_flRadius_2() { return &___m_flRadius_2; }
	inline void set_m_flRadius_2(float value)
	{
		___m_flRadius_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
