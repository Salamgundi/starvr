﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.IgnoreTeleportTrace
struct IgnoreTeleportTrace_t333906550;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.IgnoreTeleportTrace::.ctor()
extern "C"  void IgnoreTeleportTrace__ctor_m2847025834 (IgnoreTeleportTrace_t333906550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
