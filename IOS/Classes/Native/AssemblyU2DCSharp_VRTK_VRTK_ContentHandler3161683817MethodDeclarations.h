﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ContentHandler
struct VRTK_ContentHandler_t3161683817;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void VRTK.VRTK_ContentHandler::.ctor()
extern "C"  void VRTK_ContentHandler__ctor_m1855542007 (VRTK_ContentHandler_t3161683817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ContentHandler::Start()
extern "C"  void VRTK_ContentHandler_Start_m783370003 (VRTK_ContentHandler_t3161683817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ContentHandler::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void VRTK_ContentHandler_OnCollisionEnter_m3933990949 (VRTK_ContentHandler_t3161683817 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
