﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetStartingApplication
struct _GetStartingApplication_t4154880362;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetStartingApplication::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetStartingApplication__ctor_m3509480255 (_GetStartingApplication_t4154880362 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_GetStartingApplication::Invoke(System.String,System.UInt32)
extern "C"  int32_t _GetStartingApplication_Invoke_m2572095029 (_GetStartingApplication_t4154880362 * __this, String_t* ___pchAppKeyBuffer0, uint32_t ___unAppKeyBufferLen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetStartingApplication::BeginInvoke(System.String,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetStartingApplication_BeginInvoke_m694575348 (_GetStartingApplication_t4154880362 * __this, String_t* ___pchAppKeyBuffer0, uint32_t ___unAppKeyBufferLen1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_GetStartingApplication::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _GetStartingApplication_EndInvoke_m1044694633 (_GetStartingApplication_t4154880362 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
