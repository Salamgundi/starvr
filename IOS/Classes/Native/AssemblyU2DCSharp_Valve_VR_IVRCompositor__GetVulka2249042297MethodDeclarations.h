﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_GetVulkanInstanceExtensionsRequired
struct _GetVulkanInstanceExtensionsRequired_t2249042297;
// System.Object
struct Il2CppObject;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_GetVulkanInstanceExtensionsRequired::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetVulkanInstanceExtensionsRequired__ctor_m2663160876 (_GetVulkanInstanceExtensionsRequired_t2249042297 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRCompositor/_GetVulkanInstanceExtensionsRequired::Invoke(System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t _GetVulkanInstanceExtensionsRequired_Invoke_m3953292051 (_GetVulkanInstanceExtensionsRequired_t2249042297 * __this, StringBuilder_t1221177846 * ___pchValue0, uint32_t ___unBufferSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_GetVulkanInstanceExtensionsRequired::BeginInvoke(System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetVulkanInstanceExtensionsRequired_BeginInvoke_m3512824359 (_GetVulkanInstanceExtensionsRequired_t2249042297 * __this, StringBuilder_t1221177846 * ___pchValue0, uint32_t ___unBufferSize1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRCompositor/_GetVulkanInstanceExtensionsRequired::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetVulkanInstanceExtensionsRequired_EndInvoke_m1532810937 (_GetVulkanInstanceExtensionsRequired_t2249042297 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
