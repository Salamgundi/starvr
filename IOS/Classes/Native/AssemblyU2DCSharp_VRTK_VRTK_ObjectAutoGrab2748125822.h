﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ObjectAutoGrab
struct  VRTK_ObjectAutoGrab_t2748125822  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_InteractableObject VRTK.VRTK_ObjectAutoGrab::objectToGrab
	VRTK_InteractableObject_t2604188111 * ___objectToGrab_2;
	// System.Boolean VRTK.VRTK_ObjectAutoGrab::objectIsPrefab
	bool ___objectIsPrefab_3;
	// System.Boolean VRTK.VRTK_ObjectAutoGrab::cloneGrabbedObject
	bool ___cloneGrabbedObject_4;
	// System.Boolean VRTK.VRTK_ObjectAutoGrab::alwaysCloneOnEnable
	bool ___alwaysCloneOnEnable_5;
	// VRTK.VRTK_InteractableObject VRTK.VRTK_ObjectAutoGrab::previousClonedObject
	VRTK_InteractableObject_t2604188111 * ___previousClonedObject_6;

public:
	inline static int32_t get_offset_of_objectToGrab_2() { return static_cast<int32_t>(offsetof(VRTK_ObjectAutoGrab_t2748125822, ___objectToGrab_2)); }
	inline VRTK_InteractableObject_t2604188111 * get_objectToGrab_2() const { return ___objectToGrab_2; }
	inline VRTK_InteractableObject_t2604188111 ** get_address_of_objectToGrab_2() { return &___objectToGrab_2; }
	inline void set_objectToGrab_2(VRTK_InteractableObject_t2604188111 * value)
	{
		___objectToGrab_2 = value;
		Il2CppCodeGenWriteBarrier(&___objectToGrab_2, value);
	}

	inline static int32_t get_offset_of_objectIsPrefab_3() { return static_cast<int32_t>(offsetof(VRTK_ObjectAutoGrab_t2748125822, ___objectIsPrefab_3)); }
	inline bool get_objectIsPrefab_3() const { return ___objectIsPrefab_3; }
	inline bool* get_address_of_objectIsPrefab_3() { return &___objectIsPrefab_3; }
	inline void set_objectIsPrefab_3(bool value)
	{
		___objectIsPrefab_3 = value;
	}

	inline static int32_t get_offset_of_cloneGrabbedObject_4() { return static_cast<int32_t>(offsetof(VRTK_ObjectAutoGrab_t2748125822, ___cloneGrabbedObject_4)); }
	inline bool get_cloneGrabbedObject_4() const { return ___cloneGrabbedObject_4; }
	inline bool* get_address_of_cloneGrabbedObject_4() { return &___cloneGrabbedObject_4; }
	inline void set_cloneGrabbedObject_4(bool value)
	{
		___cloneGrabbedObject_4 = value;
	}

	inline static int32_t get_offset_of_alwaysCloneOnEnable_5() { return static_cast<int32_t>(offsetof(VRTK_ObjectAutoGrab_t2748125822, ___alwaysCloneOnEnable_5)); }
	inline bool get_alwaysCloneOnEnable_5() const { return ___alwaysCloneOnEnable_5; }
	inline bool* get_address_of_alwaysCloneOnEnable_5() { return &___alwaysCloneOnEnable_5; }
	inline void set_alwaysCloneOnEnable_5(bool value)
	{
		___alwaysCloneOnEnable_5 = value;
	}

	inline static int32_t get_offset_of_previousClonedObject_6() { return static_cast<int32_t>(offsetof(VRTK_ObjectAutoGrab_t2748125822, ___previousClonedObject_6)); }
	inline VRTK_InteractableObject_t2604188111 * get_previousClonedObject_6() const { return ___previousClonedObject_6; }
	inline VRTK_InteractableObject_t2604188111 ** get_address_of_previousClonedObject_6() { return &___previousClonedObject_6; }
	inline void set_previousClonedObject_6(VRTK_InteractableObject_t2604188111 * value)
	{
		___previousClonedObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___previousClonedObject_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
