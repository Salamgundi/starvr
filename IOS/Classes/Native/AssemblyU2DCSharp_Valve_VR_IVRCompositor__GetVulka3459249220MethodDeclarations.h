﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_GetVulkanDeviceExtensionsRequired
struct _GetVulkanDeviceExtensionsRequired_t3459249220;
// System.Object
struct Il2CppObject;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_GetVulkanDeviceExtensionsRequired::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetVulkanDeviceExtensionsRequired__ctor_m1735679089 (_GetVulkanDeviceExtensionsRequired_t3459249220 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRCompositor/_GetVulkanDeviceExtensionsRequired::Invoke(System.IntPtr,System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t _GetVulkanDeviceExtensionsRequired_Invoke_m17227686 (_GetVulkanDeviceExtensionsRequired_t3459249220 * __this, IntPtr_t ___pPhysicalDevice0, StringBuilder_t1221177846 * ___pchValue1, uint32_t ___unBufferSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_GetVulkanDeviceExtensionsRequired::BeginInvoke(System.IntPtr,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetVulkanDeviceExtensionsRequired_BeginInvoke_m2481228390 (_GetVulkanDeviceExtensionsRequired_t3459249220 * __this, IntPtr_t ___pPhysicalDevice0, StringBuilder_t1221177846 * ___pchValue1, uint32_t ___unBufferSize2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRCompositor/_GetVulkanDeviceExtensionsRequired::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetVulkanDeviceExtensionsRequired_EndInvoke_m4289721956 (_GetVulkanDeviceExtensionsRequired_t3459249220 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
