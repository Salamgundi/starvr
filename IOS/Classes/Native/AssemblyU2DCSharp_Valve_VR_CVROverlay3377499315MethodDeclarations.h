﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVROverlay
struct CVROverlay_t3377499315;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayFlags2344570851.h"
#include "AssemblyU2DCSharp_Valve_VR_EColorSpace2848861630.h"
#include "AssemblyU2DCSharp_Valve_VR_VRTextureBounds_t1897807375.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayTransformType3148689642.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingUniverseOrigin1464400093.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdVector2_t2255225135.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_t3405266389.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayInputMethod3830649193.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayIntersectionPa3201480230.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayIntersectionRe2886517940.h"
#include "AssemblyU2DCSharp_Valve_VR_EOverlayDirection2759670492.h"
#include "AssemblyU2DCSharp_Valve_VR_Texture_t3277130850.h"
#include "AssemblyU2DCSharp_Valve_VR_ETextureType992125572.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdRect2_t1656020282.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayIntersectionMa4278514679.h"
#include "AssemblyU2DCSharp_Valve_VR_VRMessageOverlayResponse875548136.h"

// System.Void Valve.VR.CVROverlay::.ctor(System.IntPtr)
extern "C"  void CVROverlay__ctor_m2295060894 (CVROverlay_t3377499315 * __this, IntPtr_t ___pInterface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::FindOverlay(System.String,System.UInt64&)
extern "C"  int32_t CVROverlay_FindOverlay_m2398268486 (CVROverlay_t3377499315 * __this, String_t* ___pchOverlayKey0, uint64_t* ___pOverlayHandle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::CreateOverlay(System.String,System.String,System.UInt64&)
extern "C"  int32_t CVROverlay_CreateOverlay_m1074945733 (CVROverlay_t3377499315 * __this, String_t* ___pchOverlayKey0, String_t* ___pchOverlayFriendlyName1, uint64_t* ___pOverlayHandle2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::DestroyOverlay(System.UInt64)
extern "C"  int32_t CVROverlay_DestroyOverlay_m2233828117 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetHighQualityOverlay(System.UInt64)
extern "C"  int32_t CVROverlay_SetHighQualityOverlay_m3415532148 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.CVROverlay::GetHighQualityOverlay()
extern "C"  uint64_t CVROverlay_GetHighQualityOverlay_m3864504737 (CVROverlay_t3377499315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVROverlay::GetOverlayKey(System.UInt64,System.Text.StringBuilder,System.UInt32,Valve.VR.EVROverlayError&)
extern "C"  uint32_t CVROverlay_GetOverlayKey_m4184667652 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, StringBuilder_t1221177846 * ___pchValue1, uint32_t ___unBufferSize2, int32_t* ___pError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVROverlay::GetOverlayName(System.UInt64,System.Text.StringBuilder,System.UInt32,Valve.VR.EVROverlayError&)
extern "C"  uint32_t CVROverlay_GetOverlayName_m955059704 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, StringBuilder_t1221177846 * ___pchValue1, uint32_t ___unBufferSize2, int32_t* ___pError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayImageData(System.UInt64,System.IntPtr,System.UInt32,System.UInt32&,System.UInt32&)
extern "C"  int32_t CVROverlay_GetOverlayImageData_m1532208006 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, IntPtr_t ___pvBuffer1, uint32_t ___unBufferSize2, uint32_t* ___punWidth3, uint32_t* ___punHeight4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.CVROverlay::GetOverlayErrorNameFromEnum(Valve.VR.EVROverlayError)
extern "C"  String_t* CVROverlay_GetOverlayErrorNameFromEnum_m3647688272 (CVROverlay_t3377499315 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayRenderingPid(System.UInt64,System.UInt32)
extern "C"  int32_t CVROverlay_SetOverlayRenderingPid_m1876879368 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unPID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVROverlay::GetOverlayRenderingPid(System.UInt64)
extern "C"  uint32_t CVROverlay_GetOverlayRenderingPid_m3797816937 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayFlag(System.UInt64,Valve.VR.VROverlayFlags,System.Boolean)
extern "C"  int32_t CVROverlay_SetOverlayFlag_m260228263 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eOverlayFlag1, bool ___bEnabled2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayFlag(System.UInt64,Valve.VR.VROverlayFlags,System.Boolean&)
extern "C"  int32_t CVROverlay_GetOverlayFlag_m757452065 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eOverlayFlag1, bool* ___pbEnabled2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayColor(System.UInt64,System.Single,System.Single,System.Single)
extern "C"  int32_t CVROverlay_SetOverlayColor_m1078350277 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, float ___fRed1, float ___fGreen2, float ___fBlue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayColor(System.UInt64,System.Single&,System.Single&,System.Single&)
extern "C"  int32_t CVROverlay_GetOverlayColor_m2303989763 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, float* ___pfRed1, float* ___pfGreen2, float* ___pfBlue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayAlpha(System.UInt64,System.Single)
extern "C"  int32_t CVROverlay_SetOverlayAlpha_m2757722286 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, float ___fAlpha1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayAlpha(System.UInt64,System.Single&)
extern "C"  int32_t CVROverlay_GetOverlayAlpha_m3734606302 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, float* ___pfAlpha1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayTexelAspect(System.UInt64,System.Single)
extern "C"  int32_t CVROverlay_SetOverlayTexelAspect_m500936118 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, float ___fTexelAspect1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayTexelAspect(System.UInt64,System.Single&)
extern "C"  int32_t CVROverlay_GetOverlayTexelAspect_m630052466 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, float* ___pfTexelAspect1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlaySortOrder(System.UInt64,System.UInt32)
extern "C"  int32_t CVROverlay_SetOverlaySortOrder_m698615037 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unSortOrder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlaySortOrder(System.UInt64,System.UInt32&)
extern "C"  int32_t CVROverlay_GetOverlaySortOrder_m3764722223 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, uint32_t* ___punSortOrder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayWidthInMeters(System.UInt64,System.Single)
extern "C"  int32_t CVROverlay_SetOverlayWidthInMeters_m1614745937 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, float ___fWidthInMeters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayWidthInMeters(System.UInt64,System.Single&)
extern "C"  int32_t CVROverlay_GetOverlayWidthInMeters_m2215517783 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, float* ___pfWidthInMeters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayAutoCurveDistanceRangeInMeters(System.UInt64,System.Single,System.Single)
extern "C"  int32_t CVROverlay_SetOverlayAutoCurveDistanceRangeInMeters_m3737848510 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, float ___fMinDistanceInMeters1, float ___fMaxDistanceInMeters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayAutoCurveDistanceRangeInMeters(System.UInt64,System.Single&,System.Single&)
extern "C"  int32_t CVROverlay_GetOverlayAutoCurveDistanceRangeInMeters_m1389943040 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, float* ___pfMinDistanceInMeters1, float* ___pfMaxDistanceInMeters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayTextureColorSpace(System.UInt64,Valve.VR.EColorSpace)
extern "C"  int32_t CVROverlay_SetOverlayTextureColorSpace_m3634789147 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTextureColorSpace1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayTextureColorSpace(System.UInt64,Valve.VR.EColorSpace&)
extern "C"  int32_t CVROverlay_GetOverlayTextureColorSpace_m2720097193 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, int32_t* ___peTextureColorSpace1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayTextureBounds(System.UInt64,Valve.VR.VRTextureBounds_t&)
extern "C"  int32_t CVROverlay_SetOverlayTextureBounds_m3853579714 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, VRTextureBounds_t_t1897807375 * ___pOverlayTextureBounds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayTextureBounds(System.UInt64,Valve.VR.VRTextureBounds_t&)
extern "C"  int32_t CVROverlay_GetOverlayTextureBounds_m4214665646 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, VRTextureBounds_t_t1897807375 * ___pOverlayTextureBounds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayTransformType(System.UInt64,Valve.VR.VROverlayTransformType&)
extern "C"  int32_t CVROverlay_GetOverlayTransformType_m549200489 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, int32_t* ___peTransformType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayTransformAbsolute(System.UInt64,Valve.VR.ETrackingUniverseOrigin,Valve.VR.HmdMatrix34_t&)
extern "C"  int32_t CVROverlay_SetOverlayTransformAbsolute_m2302074181 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTrackingOrigin1, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToOverlayTransform2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayTransformAbsolute(System.UInt64,Valve.VR.ETrackingUniverseOrigin&,Valve.VR.HmdMatrix34_t&)
extern "C"  int32_t CVROverlay_GetOverlayTransformAbsolute_m4032768651 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, int32_t* ___peTrackingOrigin1, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToOverlayTransform2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayTransformTrackedDeviceRelative(System.UInt64,System.UInt32,Valve.VR.HmdMatrix34_t&)
extern "C"  int32_t CVROverlay_SetOverlayTransformTrackedDeviceRelative_m2883814967 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unTrackedDevice1, HmdMatrix34_t_t664273062 * ___pmatTrackedDeviceToOverlayTransform2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayTransformTrackedDeviceRelative(System.UInt64,System.UInt32&,Valve.VR.HmdMatrix34_t&)
extern "C"  int32_t CVROverlay_GetOverlayTransformTrackedDeviceRelative_m3599833829 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, uint32_t* ___punTrackedDevice1, HmdMatrix34_t_t664273062 * ___pmatTrackedDeviceToOverlayTransform2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayTransformTrackedDeviceComponent(System.UInt64,System.UInt32,System.String)
extern "C"  int32_t CVROverlay_SetOverlayTransformTrackedDeviceComponent_m2126181688 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unDeviceIndex1, String_t* ___pchComponentName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayTransformTrackedDeviceComponent(System.UInt64,System.UInt32&,System.String,System.UInt32)
extern "C"  int32_t CVROverlay_GetOverlayTransformTrackedDeviceComponent_m3775764400 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, uint32_t* ___punDeviceIndex1, String_t* ___pchComponentName2, uint32_t ___unComponentNameSize3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::ShowOverlay(System.UInt64)
extern "C"  int32_t CVROverlay_ShowOverlay_m1193818624 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::HideOverlay(System.UInt64)
extern "C"  int32_t CVROverlay_HideOverlay_m2183284859 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVROverlay::IsOverlayVisible(System.UInt64)
extern "C"  bool CVROverlay_IsOverlayVisible_m2865600979 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetTransformForOverlayCoordinates(System.UInt64,Valve.VR.ETrackingUniverseOrigin,Valve.VR.HmdVector2_t,Valve.VR.HmdMatrix34_t&)
extern "C"  int32_t CVROverlay_GetTransformForOverlayCoordinates_m1370213837 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTrackingOrigin1, HmdVector2_t_t2255225135  ___coordinatesInOverlay2, HmdMatrix34_t_t664273062 * ___pmatTransform3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVROverlay::PollNextOverlayEvent(System.UInt64,Valve.VR.VREvent_t&,System.UInt32)
extern "C"  bool CVROverlay_PollNextOverlayEvent_m3510766484 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, VREvent_t_t3405266389 * ___pEvent1, uint32_t ___uncbVREvent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayInputMethod(System.UInt64,Valve.VR.VROverlayInputMethod&)
extern "C"  int32_t CVROverlay_GetOverlayInputMethod_m3944219209 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, int32_t* ___peInputMethod1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayInputMethod(System.UInt64,Valve.VR.VROverlayInputMethod)
extern "C"  int32_t CVROverlay_SetOverlayInputMethod_m1630548899 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eInputMethod1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayMouseScale(System.UInt64,Valve.VR.HmdVector2_t&)
extern "C"  int32_t CVROverlay_GetOverlayMouseScale_m1691363935 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, HmdVector2_t_t2255225135 * ___pvecMouseScale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayMouseScale(System.UInt64,Valve.VR.HmdVector2_t&)
extern "C"  int32_t CVROverlay_SetOverlayMouseScale_m2769828171 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, HmdVector2_t_t2255225135 * ___pvecMouseScale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVROverlay::ComputeOverlayIntersection(System.UInt64,Valve.VR.VROverlayIntersectionParams_t&,Valve.VR.VROverlayIntersectionResults_t&)
extern "C"  bool CVROverlay_ComputeOverlayIntersection_m1215373613 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, VROverlayIntersectionParams_t_t3201480230 * ___pParams1, VROverlayIntersectionResults_t_t2886517940 * ___pResults2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVROverlay::HandleControllerOverlayInteractionAsMouse(System.UInt64,System.UInt32)
extern "C"  bool CVROverlay_HandleControllerOverlayInteractionAsMouse_m2823594662 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unControllerDeviceIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVROverlay::IsHoverTargetOverlay(System.UInt64)
extern "C"  bool CVROverlay_IsHoverTargetOverlay_m1626251904 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.CVROverlay::GetGamepadFocusOverlay()
extern "C"  uint64_t CVROverlay_GetGamepadFocusOverlay_m2037884125 (CVROverlay_t3377499315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetGamepadFocusOverlay(System.UInt64)
extern "C"  int32_t CVROverlay_SetGamepadFocusOverlay_m2491511154 (CVROverlay_t3377499315 * __this, uint64_t ___ulNewFocusOverlay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayNeighbor(Valve.VR.EOverlayDirection,System.UInt64,System.UInt64)
extern "C"  int32_t CVROverlay_SetOverlayNeighbor_m1728609766 (CVROverlay_t3377499315 * __this, int32_t ___eDirection0, uint64_t ___ulFrom1, uint64_t ___ulTo2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::MoveGamepadFocusToNeighbor(Valve.VR.EOverlayDirection,System.UInt64)
extern "C"  int32_t CVROverlay_MoveGamepadFocusToNeighbor_m2712769218 (CVROverlay_t3377499315 * __this, int32_t ___eDirection0, uint64_t ___ulFrom1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayTexture(System.UInt64,Valve.VR.Texture_t&)
extern "C"  int32_t CVROverlay_SetOverlayTexture_m3441699676 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, Texture_t_t3277130850 * ___pTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::ClearOverlayTexture(System.UInt64)
extern "C"  int32_t CVROverlay_ClearOverlayTexture_m1160914853 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayRaw(System.UInt64,System.IntPtr,System.UInt32,System.UInt32,System.UInt32)
extern "C"  int32_t CVROverlay_SetOverlayRaw_m2675965019 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, IntPtr_t ___pvBuffer1, uint32_t ___unWidth2, uint32_t ___unHeight3, uint32_t ___unDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayFromFile(System.UInt64,System.String)
extern "C"  int32_t CVROverlay_SetOverlayFromFile_m2590275999 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, String_t* ___pchFilePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayTexture(System.UInt64,System.IntPtr&,System.IntPtr,System.UInt32&,System.UInt32&,System.UInt32&,Valve.VR.ETextureType&,Valve.VR.EColorSpace&,Valve.VR.VRTextureBounds_t&)
extern "C"  int32_t CVROverlay_GetOverlayTexture_m1474844255 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, IntPtr_t* ___pNativeTextureHandle1, IntPtr_t ___pNativeTextureRef2, uint32_t* ___pWidth3, uint32_t* ___pHeight4, uint32_t* ___pNativeFormat5, int32_t* ___pAPIType6, int32_t* ___pColorSpace7, VRTextureBounds_t_t1897807375 * ___pTextureBounds8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::ReleaseNativeOverlayHandle(System.UInt64,System.IntPtr)
extern "C"  int32_t CVROverlay_ReleaseNativeOverlayHandle_m2142965211 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, IntPtr_t ___pNativeTextureHandle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayTextureSize(System.UInt64,System.UInt32&,System.UInt32&)
extern "C"  int32_t CVROverlay_GetOverlayTextureSize_m125447643 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, uint32_t* ___pWidth1, uint32_t* ___pHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::CreateDashboardOverlay(System.String,System.String,System.UInt64&,System.UInt64&)
extern "C"  int32_t CVROverlay_CreateDashboardOverlay_m154829652 (CVROverlay_t3377499315 * __this, String_t* ___pchOverlayKey0, String_t* ___pchOverlayFriendlyName1, uint64_t* ___pMainHandle2, uint64_t* ___pThumbnailHandle3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVROverlay::IsDashboardVisible()
extern "C"  bool CVROverlay_IsDashboardVisible_m2666726650 (CVROverlay_t3377499315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVROverlay::IsActiveDashboardOverlay(System.UInt64)
extern "C"  bool CVROverlay_IsActiveDashboardOverlay_m1273813285 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetDashboardOverlaySceneProcess(System.UInt64,System.UInt32)
extern "C"  int32_t CVROverlay_SetDashboardOverlaySceneProcess_m47160042 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unProcessId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetDashboardOverlaySceneProcess(System.UInt64,System.UInt32&)
extern "C"  int32_t CVROverlay_GetDashboardOverlaySceneProcess_m1446854814 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, uint32_t* ___punProcessId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVROverlay::ShowDashboard(System.String)
extern "C"  void CVROverlay_ShowDashboard_m1229589271 (CVROverlay_t3377499315 * __this, String_t* ___pchOverlayToShow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVROverlay::GetPrimaryDashboardDevice()
extern "C"  uint32_t CVROverlay_GetPrimaryDashboardDevice_m2982404915 (CVROverlay_t3377499315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::ShowKeyboard(System.Int32,System.Int32,System.String,System.UInt32,System.String,System.Boolean,System.UInt64)
extern "C"  int32_t CVROverlay_ShowKeyboard_m675186710 (CVROverlay_t3377499315 * __this, int32_t ___eInputMode0, int32_t ___eLineInputMode1, String_t* ___pchDescription2, uint32_t ___unCharMax3, String_t* ___pchExistingText4, bool ___bUseMinimalMode5, uint64_t ___uUserValue6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::ShowKeyboardForOverlay(System.UInt64,System.Int32,System.Int32,System.String,System.UInt32,System.String,System.Boolean,System.UInt64)
extern "C"  int32_t CVROverlay_ShowKeyboardForOverlay_m3432079040 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eInputMode1, int32_t ___eLineInputMode2, String_t* ___pchDescription3, uint32_t ___unCharMax4, String_t* ___pchExistingText5, bool ___bUseMinimalMode6, uint64_t ___uUserValue7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVROverlay::GetKeyboardText(System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t CVROverlay_GetKeyboardText_m158134631 (CVROverlay_t3377499315 * __this, StringBuilder_t1221177846 * ___pchText0, uint32_t ___cchText1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVROverlay::HideKeyboard()
extern "C"  void CVROverlay_HideKeyboard_m1123812691 (CVROverlay_t3377499315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVROverlay::SetKeyboardTransformAbsolute(Valve.VR.ETrackingUniverseOrigin,Valve.VR.HmdMatrix34_t&)
extern "C"  void CVROverlay_SetKeyboardTransformAbsolute_m3592843785 (CVROverlay_t3377499315 * __this, int32_t ___eTrackingOrigin0, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToKeyboardTransform1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVROverlay::SetKeyboardPositionForOverlay(System.UInt64,Valve.VR.HmdRect2_t)
extern "C"  void CVROverlay_SetKeyboardPositionForOverlay_m3719609024 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, HmdRect2_t_t1656020282  ___avoidRect1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::SetOverlayIntersectionMask(System.UInt64,Valve.VR.VROverlayIntersectionMaskPrimitive_t&,System.UInt32,System.UInt32)
extern "C"  int32_t CVROverlay_SetOverlayIntersectionMask_m4067518263 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, VROverlayIntersectionMaskPrimitive_t_t4278514679 * ___pMaskPrimitives1, uint32_t ___unNumMaskPrimitives2, uint32_t ___unPrimitiveSize3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.CVROverlay::GetOverlayFlags(System.UInt64,System.UInt32&)
extern "C"  int32_t CVROverlay_GetOverlayFlags_m4191384132 (CVROverlay_t3377499315 * __this, uint64_t ___ulOverlayHandle0, uint32_t* ___pFlags1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.VRMessageOverlayResponse Valve.VR.CVROverlay::ShowMessageOverlay(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  int32_t CVROverlay_ShowMessageOverlay_m737269673 (CVROverlay_t3377499315 * __this, String_t* ___pchText0, String_t* ___pchCaption1, String_t* ___pchButton0Text2, String_t* ___pchButton1Text3, String_t* ___pchButton2Text4, String_t* ___pchButton3Text5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
