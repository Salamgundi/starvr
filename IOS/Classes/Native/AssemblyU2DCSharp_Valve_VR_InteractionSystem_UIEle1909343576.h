﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.CustomEvents/UnityEventHand
struct UnityEventHand_t749092352;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.UIElement
struct  UIElement_t1909343576  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.CustomEvents/UnityEventHand Valve.VR.InteractionSystem.UIElement::onHandClick
	UnityEventHand_t749092352 * ___onHandClick_2;
	// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.UIElement::currentHand
	Hand_t379716353 * ___currentHand_3;

public:
	inline static int32_t get_offset_of_onHandClick_2() { return static_cast<int32_t>(offsetof(UIElement_t1909343576, ___onHandClick_2)); }
	inline UnityEventHand_t749092352 * get_onHandClick_2() const { return ___onHandClick_2; }
	inline UnityEventHand_t749092352 ** get_address_of_onHandClick_2() { return &___onHandClick_2; }
	inline void set_onHandClick_2(UnityEventHand_t749092352 * value)
	{
		___onHandClick_2 = value;
		Il2CppCodeGenWriteBarrier(&___onHandClick_2, value);
	}

	inline static int32_t get_offset_of_currentHand_3() { return static_cast<int32_t>(offsetof(UIElement_t1909343576, ___currentHand_3)); }
	inline Hand_t379716353 * get_currentHand_3() const { return ___currentHand_3; }
	inline Hand_t379716353 ** get_address_of_currentHand_3() { return &___currentHand_3; }
	inline void set_currentHand_3(Hand_t379716353 * value)
	{
		___currentHand_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentHand_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
