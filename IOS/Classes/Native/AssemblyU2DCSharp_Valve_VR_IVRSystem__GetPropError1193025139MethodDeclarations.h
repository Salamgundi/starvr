﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetPropErrorNameFromEnum
struct _GetPropErrorNameFromEnum_t1193025139;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedPropertyError3340022390.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetPropErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetPropErrorNameFromEnum__ctor_m2861715900 (_GetPropErrorNameFromEnum_t1193025139 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRSystem/_GetPropErrorNameFromEnum::Invoke(Valve.VR.ETrackedPropertyError)
extern "C"  IntPtr_t _GetPropErrorNameFromEnum_Invoke_m969431979 (_GetPropErrorNameFromEnum_t1193025139 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetPropErrorNameFromEnum::BeginInvoke(Valve.VR.ETrackedPropertyError,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetPropErrorNameFromEnum_BeginInvoke_m3519723757 (_GetPropErrorNameFromEnum_t1193025139 * __this, int32_t ___error0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRSystem/_GetPropErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetPropErrorNameFromEnum_EndInvoke_m2540180665 (_GetPropErrorNameFromEnum_t1193025139 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
