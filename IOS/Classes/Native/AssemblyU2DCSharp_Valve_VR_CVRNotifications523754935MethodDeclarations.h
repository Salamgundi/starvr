﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRNotifications
struct CVRNotifications_t523754935;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRNotificationError1058814108.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRNotificationType408717238.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRNotificationStyle1858720717.h"
#include "AssemblyU2DCSharp_Valve_VR_NotificationBitmap_t1973232283.h"

// System.Void Valve.VR.CVRNotifications::.ctor(System.IntPtr)
extern "C"  void CVRNotifications__ctor_m2153725106 (CVRNotifications_t523754935 * __this, IntPtr_t ___pInterface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRNotificationError Valve.VR.CVRNotifications::CreateNotification(System.UInt64,System.UInt64,Valve.VR.EVRNotificationType,System.String,Valve.VR.EVRNotificationStyle,Valve.VR.NotificationBitmap_t&,System.UInt32&)
extern "C"  int32_t CVRNotifications_CreateNotification_m88295618 (CVRNotifications_t523754935 * __this, uint64_t ___ulOverlayHandle0, uint64_t ___ulUserValue1, int32_t ___type2, String_t* ___pchText3, int32_t ___style4, NotificationBitmap_t_t1973232283 * ___pImage5, uint32_t* ___pNotificationId6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRNotificationError Valve.VR.CVRNotifications::RemoveNotification(System.UInt32)
extern "C"  int32_t CVRNotifications_RemoveNotification_m1170040396 (CVRNotifications_t523754935 * __this, uint32_t ___notificationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
