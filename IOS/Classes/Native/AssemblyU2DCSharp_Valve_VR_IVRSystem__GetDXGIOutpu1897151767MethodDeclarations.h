﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetDXGIOutputInfo
struct _GetDXGIOutputInfo_t1897151767;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetDXGIOutputInfo::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetDXGIOutputInfo__ctor_m1025261082 (_GetDXGIOutputInfo_t1897151767 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_GetDXGIOutputInfo::Invoke(System.Int32&)
extern "C"  void _GetDXGIOutputInfo_Invoke_m2895185309 (_GetDXGIOutputInfo_t1897151767 * __this, int32_t* ___pnAdapterIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetDXGIOutputInfo::BeginInvoke(System.Int32&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetDXGIOutputInfo_BeginInvoke_m2857825104 (_GetDXGIOutputInfo_t1897151767 * __this, int32_t* ___pnAdapterIndex0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_GetDXGIOutputInfo::EndInvoke(System.Int32&,System.IAsyncResult)
extern "C"  void _GetDXGIOutputInfo_EndInvoke_m782386979 (_GetDXGIOutputInfo_t1897151767 * __this, int32_t* ___pnAdapterIndex0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
