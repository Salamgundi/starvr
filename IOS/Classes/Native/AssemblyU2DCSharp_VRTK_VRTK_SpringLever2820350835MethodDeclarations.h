﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_SpringLever
struct VRTK_SpringLever_t2820350835;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"

// System.Void VRTK.VRTK_SpringLever::.ctor()
extern "C"  void VRTK_SpringLever__ctor_m595608247 (VRTK_SpringLever_t2820350835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SpringLever::InitRequiredComponents()
extern "C"  void VRTK_SpringLever_InitRequiredComponents_m2152332916 (VRTK_SpringLever_t2820350835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SpringLever::HandleUpdate()
extern "C"  void VRTK_SpringLever_HandleUpdate_m1637800712 (VRTK_SpringLever_t2820350835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SpringLever::InteractableObjectGrabbed(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_SpringLever_InteractableObjectGrabbed_m1685237120 (VRTK_SpringLever_t2820350835 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SpringLever::InteractableObjectUngrabbed(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_SpringLever_InteractableObjectUngrabbed_m1795997577 (VRTK_SpringLever_t2820350835 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_SpringLever::GetSpringTarget(System.Boolean)
extern "C"  float VRTK_SpringLever_GetSpringTarget_m61822408 (VRTK_SpringLever_t2820350835 * __this, bool ___towardZero0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SpringLever::ApplySpringForce()
extern "C"  void VRTK_SpringLever_ApplySpringForce_m2514926295 (VRTK_SpringLever_t2820350835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
