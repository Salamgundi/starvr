﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.InteractableObjectEventArgs
struct InteractableObjectEventArgs_t473175556;
struct InteractableObjectEventArgs_t473175556_marshaled_pinvoke;
struct InteractableObjectEventArgs_t473175556_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct InteractableObjectEventArgs_t473175556;
struct InteractableObjectEventArgs_t473175556_marshaled_pinvoke;

extern "C" void InteractableObjectEventArgs_t473175556_marshal_pinvoke(const InteractableObjectEventArgs_t473175556& unmarshaled, InteractableObjectEventArgs_t473175556_marshaled_pinvoke& marshaled);
extern "C" void InteractableObjectEventArgs_t473175556_marshal_pinvoke_back(const InteractableObjectEventArgs_t473175556_marshaled_pinvoke& marshaled, InteractableObjectEventArgs_t473175556& unmarshaled);
extern "C" void InteractableObjectEventArgs_t473175556_marshal_pinvoke_cleanup(InteractableObjectEventArgs_t473175556_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct InteractableObjectEventArgs_t473175556;
struct InteractableObjectEventArgs_t473175556_marshaled_com;

extern "C" void InteractableObjectEventArgs_t473175556_marshal_com(const InteractableObjectEventArgs_t473175556& unmarshaled, InteractableObjectEventArgs_t473175556_marshaled_com& marshaled);
extern "C" void InteractableObjectEventArgs_t473175556_marshal_com_back(const InteractableObjectEventArgs_t473175556_marshaled_com& marshaled, InteractableObjectEventArgs_t473175556& unmarshaled);
extern "C" void InteractableObjectEventArgs_t473175556_marshal_com_cleanup(InteractableObjectEventArgs_t473175556_marshaled_com& marshaled);
