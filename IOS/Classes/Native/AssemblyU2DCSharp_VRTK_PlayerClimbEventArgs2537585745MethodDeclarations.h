﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.PlayerClimbEventArgs
struct PlayerClimbEventArgs_t2537585745;
struct PlayerClimbEventArgs_t2537585745_marshaled_pinvoke;
struct PlayerClimbEventArgs_t2537585745_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct PlayerClimbEventArgs_t2537585745;
struct PlayerClimbEventArgs_t2537585745_marshaled_pinvoke;

extern "C" void PlayerClimbEventArgs_t2537585745_marshal_pinvoke(const PlayerClimbEventArgs_t2537585745& unmarshaled, PlayerClimbEventArgs_t2537585745_marshaled_pinvoke& marshaled);
extern "C" void PlayerClimbEventArgs_t2537585745_marshal_pinvoke_back(const PlayerClimbEventArgs_t2537585745_marshaled_pinvoke& marshaled, PlayerClimbEventArgs_t2537585745& unmarshaled);
extern "C" void PlayerClimbEventArgs_t2537585745_marshal_pinvoke_cleanup(PlayerClimbEventArgs_t2537585745_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct PlayerClimbEventArgs_t2537585745;
struct PlayerClimbEventArgs_t2537585745_marshaled_com;

extern "C" void PlayerClimbEventArgs_t2537585745_marshal_com(const PlayerClimbEventArgs_t2537585745& unmarshaled, PlayerClimbEventArgs_t2537585745_marshaled_com& marshaled);
extern "C" void PlayerClimbEventArgs_t2537585745_marshal_com_back(const PlayerClimbEventArgs_t2537585745_marshaled_com& marshaled, PlayerClimbEventArgs_t2537585745& unmarshaled);
extern "C" void PlayerClimbEventArgs_t2537585745_marshal_com_cleanup(PlayerClimbEventArgs_t2537585745_marshaled_com& marshaled);
