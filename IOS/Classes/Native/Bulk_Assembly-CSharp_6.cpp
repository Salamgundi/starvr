﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Valve.VR.IVROverlay/_SetOverlayAutoCurveDistanceRangeInMeters
struct _SetOverlayAutoCurveDistanceRangeInMeters_t1475163830;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// Valve.VR.IVROverlay/_SetOverlayColor
struct _SetOverlayColor_t2804573176;
// Valve.VR.IVROverlay/_SetOverlayFlag
struct _SetOverlayFlag_t1900166033;
// Valve.VR.IVROverlay/_SetOverlayFromFile
struct _SetOverlayFromFile_t598184189;
// System.String
struct String_t;
// Valve.VR.IVROverlay/_SetOverlayInputMethod
struct _SetOverlayInputMethod_t3575042602;
// Valve.VR.IVROverlay/_SetOverlayIntersectionMask
struct _SetOverlayIntersectionMask_t3952572284;
// Valve.VR.IVROverlay/_SetOverlayMouseScale
struct _SetOverlayMouseScale_t2624726138;
// Valve.VR.IVROverlay/_SetOverlayNeighbor
struct _SetOverlayNeighbor_t1117963895;
// Valve.VR.IVROverlay/_SetOverlayRaw
struct _SetOverlayRaw_t3268606321;
// Valve.VR.IVROverlay/_SetOverlayRenderingPid
struct _SetOverlayRenderingPid_t1970553664;
// Valve.VR.IVROverlay/_SetOverlaySortOrder
struct _SetOverlaySortOrder_t3099711365;
// Valve.VR.IVROverlay/_SetOverlayTexelAspect
struct _SetOverlayTexelAspect_t460846625;
// Valve.VR.IVROverlay/_SetOverlayTexture
struct _SetOverlayTexture_t2238656700;
// Valve.VR.IVROverlay/_SetOverlayTextureBounds
struct _SetOverlayTextureBounds_t1179269927;
// Valve.VR.IVROverlay/_SetOverlayTextureColorSpace
struct _SetOverlayTextureColorSpace_t1399555963;
// Valve.VR.IVROverlay/_SetOverlayTransformAbsolute
struct _SetOverlayTransformAbsolute_t2100297354;
// Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceComponent
struct _SetOverlayTransformTrackedDeviceComponent_t1749665136;
// Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceRelative
struct _SetOverlayTransformTrackedDeviceRelative_t579082695;
// Valve.VR.IVROverlay/_SetOverlayWidthInMeters
struct _SetOverlayWidthInMeters_t3047220066;
// Valve.VR.IVROverlay/_ShowDashboard
struct _ShowDashboard_t4127025320;
// Valve.VR.IVROverlay/_ShowKeyboard
struct _ShowKeyboard_t3095606223;
// Valve.VR.IVROverlay/_ShowKeyboardForOverlay
struct _ShowKeyboardForOverlay_t3006565844;
// Valve.VR.IVROverlay/_ShowMessageOverlay
struct _ShowMessageOverlay_t3284759035;
// Valve.VR.IVROverlay/_ShowOverlay
struct _ShowOverlay_t733914692;
// Valve.VR.IVRRenderModels/_FreeRenderModel
struct _FreeRenderModel_t2139843464;
// Valve.VR.IVRRenderModels/_FreeTexture
struct _FreeTexture_t4051202214;
// Valve.VR.IVRRenderModels/_FreeTextureD3D11
struct _FreeTextureD3D11_t2385986941;
// Valve.VR.IVRRenderModels/_GetComponentButtonMask
struct _GetComponentButtonMask_t1474657094;
// Valve.VR.IVRRenderModels/_GetComponentCount
struct _GetComponentCount_t763371255;
// Valve.VR.IVRRenderModels/_GetComponentName
struct _GetComponentName_t3462998887;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// Valve.VR.IVRRenderModels/_GetComponentRenderModelName
struct _GetComponentRenderModelName_t2860930600;
// Valve.VR.IVRRenderModels/_GetComponentState
struct _GetComponentState_t742926735;
// Valve.VR.IVRRenderModels/_GetRenderModelCount
struct _GetRenderModelCount_t3364784497;
// Valve.VR.IVRRenderModels/_GetRenderModelErrorNameFromEnum
struct _GetRenderModelErrorNameFromEnum_t298277168;
// Valve.VR.IVRRenderModels/_GetRenderModelName
struct _GetRenderModelName_t4149685257;
// Valve.VR.IVRRenderModels/_GetRenderModelOriginalPath
struct _GetRenderModelOriginalPath_t4216085620;
// Valve.VR.IVRRenderModels/_GetRenderModelThumbnailURL
struct _GetRenderModelThumbnailURL_t3954674309;
// Valve.VR.IVRRenderModels/_LoadIntoTextureD3D11_Async
struct _LoadIntoTextureD3D11_Async_t3518850916;
// Valve.VR.IVRRenderModels/_LoadRenderModel_Async
struct _LoadRenderModel_Async_t3622270247;
// Valve.VR.IVRRenderModels/_LoadTexture_Async
struct _LoadTexture_Async_t1786536393;
// Valve.VR.IVRRenderModels/_LoadTextureD3D11_Async
struct _LoadTextureD3D11_Async_t2681806282;
// Valve.VR.IVRRenderModels/_RenderModelHasComponent
struct _RenderModelHasComponent_t1969881317;
// Valve.VR.IVRResources/_GetResourceFullPath
struct _GetResourceFullPath_t915790394;
// Valve.VR.IVRResources/_LoadSharedResource
struct _LoadSharedResource_t4289483331;
// Valve.VR.IVRScreenshots/_GetScreenshotPropertyFilename
struct _GetScreenshotPropertyFilename_t1122176780;
// Valve.VR.IVRScreenshots/_GetScreenshotPropertyType
struct _GetScreenshotPropertyType_t3028991757;
// Valve.VR.IVRScreenshots/_HookScreenshot
struct _HookScreenshot_t2804207343;
// Valve.VR.EVRScreenshotType[]
struct EVRScreenshotTypeU5BU5D_t2594501106;
// Valve.VR.IVRScreenshots/_RequestScreenshot
struct _RequestScreenshot_t1956857133;
// Valve.VR.IVRScreenshots/_SubmitScreenshot
struct _SubmitScreenshot_t3156929320;
// Valve.VR.IVRScreenshots/_TakeStereoScreenshot
struct _TakeStereoScreenshot_t3387995749;
// Valve.VR.IVRScreenshots/_UpdateScreenshotProgress
struct _UpdateScreenshotProgress_t2161609358;
// Valve.VR.IVRSettings/_GetBool
struct _GetBool_t1034551410;
// Valve.VR.IVRSettings/_GetFloat
struct _GetFloat_t3725422668;
// Valve.VR.IVRSettings/_GetInt32
struct _GetInt32_t3538756002;
// Valve.VR.IVRSettings/_GetSettingsErrorNameFromEnum
struct _GetSettingsErrorNameFromEnum_t293614055;
// Valve.VR.IVRSettings/_GetString
struct _GetString_t1816180801;
// Valve.VR.IVRSettings/_RemoveKeyInSection
struct _RemoveKeyInSection_t1836506429;
// Valve.VR.IVRSettings/_RemoveSection
struct _RemoveSection_t1493649321;
// Valve.VR.IVRSettings/_SetBool
struct _SetBool_t1033875974;
// Valve.VR.IVRSettings/_SetFloat
struct _SetFloat_t3724747224;
// Valve.VR.IVRSettings/_SetInt32
struct _SetInt32_t3538080526;
// Valve.VR.IVRSettings/_SetString
struct _SetString_t1793856309;
// Valve.VR.IVRSettings/_Sync
struct _Sync_t2978470277;
// Valve.VR.IVRSystem/_AcknowledgeQuit_Exiting
struct _AcknowledgeQuit_Exiting_t1109677234;
// Valve.VR.IVRSystem/_AcknowledgeQuit_UserPrompt
struct _AcknowledgeQuit_UserPrompt_t90686541;
// Valve.VR.IVRSystem/_ApplyTransform
struct _ApplyTransform_t1439808290;
// Valve.VR.IVRSystem/_CaptureInputFocus
struct _CaptureInputFocus_t2994096092;
// Valve.VR.IVRSystem/_ComputeDistortion
struct _ComputeDistortion_t3576284924;
// Valve.VR.IVRSystem/_DriverDebugRequest
struct _DriverDebugRequest_t4049208724;
// Valve.VR.IVRSystem/_GetBoolTrackedDeviceProperty
struct _GetBoolTrackedDeviceProperty_t2236257287;
// Valve.VR.IVRSystem/_GetButtonIdNameFromEnum
struct _GetButtonIdNameFromEnum_t195009473;
// Valve.VR.IVRSystem/_GetControllerAxisTypeNameFromEnum
struct _GetControllerAxisTypeNameFromEnum_t3568402941;
// Valve.VR.IVRSystem/_GetControllerRoleForTrackedDeviceIndex
struct _GetControllerRoleForTrackedDeviceIndex_t1728202579;
// Valve.VR.IVRSystem/_GetControllerState
struct _GetControllerState_t3891090487;
// Valve.VR.IVRSystem/_GetControllerStateWithPose
struct _GetControllerStateWithPose_t4079915850;
// Valve.VR.IVRSystem/_GetD3D9AdapterIndex
struct _GetD3D9AdapterIndex_t4234979703;
// Valve.VR.IVRSystem/_GetDeviceToAbsoluteTrackingPose
struct _GetDeviceToAbsoluteTrackingPose_t1432625068;
// Valve.VR.TrackedDevicePose_t[]
struct TrackedDevicePose_tU5BU5D_t2897272049;
// Valve.VR.IVRSystem/_GetDXGIOutputInfo
struct _GetDXGIOutputInfo_t1897151767;
// Valve.VR.IVRSystem/_GetEventTypeNameFromEnum
struct _GetEventTypeNameFromEnum_t1950138544;
// Valve.VR.IVRSystem/_GetEyeToHeadTransform
struct _GetEyeToHeadTransform_t3057184772;
// Valve.VR.IVRSystem/_GetFloatTrackedDeviceProperty
struct _GetFloatTrackedDeviceProperty_t1406950913;
// Valve.VR.IVRSystem/_GetHiddenAreaMesh
struct _GetHiddenAreaMesh_t1813422502;
// Valve.VR.IVRSystem/_GetInt32TrackedDeviceProperty
struct _GetInt32TrackedDeviceProperty_t2396289227;
// Valve.VR.IVRSystem/_GetMatrix34TrackedDeviceProperty
struct _GetMatrix34TrackedDeviceProperty_t3426445457;
// Valve.VR.IVRSystem/_GetProjectionMatrix
struct _GetProjectionMatrix_t2621141914;
// Valve.VR.IVRSystem/_GetProjectionRaw
struct _GetProjectionRaw_t3426995441;
// Valve.VR.IVRSystem/_GetPropErrorNameFromEnum
struct _GetPropErrorNameFromEnum_t1193025139;
// Valve.VR.IVRSystem/_GetRawZeroPoseToStandingAbsoluteTrackingPose
struct _GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273;
// Valve.VR.IVRSystem/_GetRecommendedRenderTargetSize
struct _GetRecommendedRenderTargetSize_t4195542627;
// Valve.VR.IVRSystem/_GetSeatedZeroPoseToStandingAbsoluteTrackingPose
struct _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835;
// Valve.VR.IVRSystem/_GetSortedTrackedDeviceIndicesOfClass
struct _GetSortedTrackedDeviceIndicesOfClass_t3492202929;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// Valve.VR.IVRSystem/_GetStringTrackedDeviceProperty
struct _GetStringTrackedDeviceProperty_t87797800;
// Valve.VR.IVRSystem/_GetTimeSinceLastVsync
struct _GetTimeSinceLastVsync_t1215702688;
// Valve.VR.IVRSystem/_GetTrackedDeviceActivityLevel
struct _GetTrackedDeviceActivityLevel_t212130385;
// Valve.VR.IVRSystem/_GetTrackedDeviceClass
struct _GetTrackedDeviceClass_t1455580370;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayA1475163830.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayA1475163830MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Void1841601450.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayC2804573176.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayC2804573176MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayF1900166033.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayF1900166033MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayFlags2344570851.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayFr598184189.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayFr598184189MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayI3575042602.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayI3575042602MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayInputMethod3830649193.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayI3952572284.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayI3952572284MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayIntersectionMa4278514679.h"
#include "mscorlib_System_UInt322149682021.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayM2624726138.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayM2624726138MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdVector2_t2255225135.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayN1117963895.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayN1117963895MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_EOverlayDirection2759670492.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayR3268606321.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayR3268606321MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayR1970553664.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayR1970553664MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayS3099711365.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayS3099711365MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayTe460846625.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayTe460846625MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT2238656700.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT2238656700MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_Texture_t3277130850.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT1179269927.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT1179269927MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_VRTextureBounds_t1897807375.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT1399555963.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT1399555963MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_EColorSpace2848861630.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT2100297354.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT2100297354MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingUniverseOrigin1464400093.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT1749665136.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT1749665136MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayTr579082695.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayTr579082695MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayW3047220066.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayW3047220066MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowDashboa4127025320.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowDashboa4127025320MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowKeyboar3095606223.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowKeyboar3095606223MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowKeyboar3006565844.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowKeyboar3006565844MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowMessage3284759035.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowMessage3284759035MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_VRMessageOverlayResponse875548136.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowOverlay733914692.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowOverlay733914692MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels3420796425.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels3420796425MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__LoadRe3622270247.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__FreeRe2139843464.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__LoadTe1786536393.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__FreeTe4051202214.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__LoadTe2681806282.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__LoadIn3518850916.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__FreeTe2385986941.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRen4149685257.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRen3364784497.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetComp763371255.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetCom3462998887.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetCom1474657094.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetCom2860930600.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetComp742926735.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__Render1969881317.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRen3954674309.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRen4216085620.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRend298277168.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__FreeRe2139843464MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__FreeTe4051202214MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__FreeTe2385986941MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetCom1474657094MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetComp763371255MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetCom3462998887MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetCom2860930600MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetComp742926735MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_VRControllerState_t2504874220.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_ControllerM1298199406.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_ComponentSt2032012879.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRen3364784497MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRend298277168MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRRenderModelError21703732.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRen4149685257MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRen4216085620MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRen3954674309MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__LoadIn3518850916MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__LoadRe3622270247MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__LoadTe1786536393MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__LoadTe2681806282MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__Render1969881317MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRResources1092978558.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRResources1092978558MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRResources__LoadShare4289483331.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRResources__GetResourc915790394.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRResources__GetResourc915790394MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRResources__LoadShare4289483331MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots1006836234.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots1006836234MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__Request1956857133.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__HookScr2804207343.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__GetScre3028991757.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__GetScre1122176780.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__UpdateS2161609358.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__TakeSte3387995749.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__SubmitS3156929320.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__GetScre1122176780MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotPropertyFile29427162.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotError1400268927.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__GetScre3028991757MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotType611740195.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__HookScr2804207343MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__Request1956857133MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__SubmitS3156929320MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__TakeSte3387995749MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__UpdateS2161609358MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings254931744.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings254931744MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetSettings293614055.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__Sync2978470277.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__SetBool1033875974.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__SetInt323538080526.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__SetFloat3724747224.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__SetString1793856309.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetBool1034551410.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetInt323538756002.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetFloat3725422668.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetString1816180801.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__RemoveSect1493649321.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__RemoveKeyI1836506429.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetBool1034551410MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRSettingsError4124928198.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetFloat3725422668MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetInt323538756002MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetSettings293614055MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetString1816180801MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__RemoveKeyI1836506429MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__RemoveSect1493649321MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__SetBool1033875974MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__SetFloat3724747224MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__SetInt323538080526MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__SetString1793856309MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__Sync2978470277MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem3365196000.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem3365196000MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetRecommend4195542627.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetProjectio2621141914.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetProjectio3426995441.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__ComputeDisto3576284924.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetEyeToHead3057184772.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetTimeSince1215702688.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetD3D9Adapt4234979703.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetDXGIOutpu1897151767.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__IsDisplayOnD2551312917.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__SetDisplayVi3986281708.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetDeviceToA1432625068.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__ResetSeatedZ3471614486.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetSeatedZero102610835.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetRawZeroPo1986385273.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetSortedTra3492202929.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetTrackedDev212130385.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__ApplyTransfo1439808290.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetTrackedDe3232960147.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetControlle1728202579.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetTrackedDe1455580370.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__IsTrackedDevi459208129.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetBoolTrack2236257287.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetFloatTrac1406950913.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetInt32Trac2396289227.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetUint64Trac537540785.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetMatrix34T3426445457.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetStringTrack87797800.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetPropError1193025139.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__PollNextEven3908295690.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__PollNextEven2759121141.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetEventType1950138544.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetHiddenAre1813422502.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetControlle3891090487.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetControlle4079915850.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__TriggerHaptic158863722.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetButtonIdNa195009473.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetControlle3568402941.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__CaptureInput2994096092.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__ReleaseInputF580725753.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__IsInputFocusCa84136089.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__DriverDebugR4049208724.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__PerformFirmwa673402879.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__AcknowledgeQ1109677234.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__AcknowledgeQui90686541.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__AcknowledgeQ1109677234MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__AcknowledgeQui90686541MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__ApplyTransfo1439808290MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_TrackedDevicePose_t1668551120.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__CaptureInput2994096092MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__ComputeDisto3576284924MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"
#include "AssemblyU2DCSharp_Valve_VR_DistortionCoordinates_t2253454723.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__DriverDebugR4049208724MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetBoolTrack2236257287MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedDeviceProperty3226377054.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedPropertyError3340022390.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetButtonIdNa195009473MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRButtonId66145412.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetControlle3568402941MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRControllerAxisType1358176136.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetControlle1728202579MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedControllerRole361251409.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetControlle3891090487MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetControlle4079915850MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetD3D9Adapt4234979703MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetDeviceToA1432625068MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetDXGIOutpu1897151767MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetEventType1950138544MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREventType6846875.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetEyeToHead3057184772MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetFloatTrac1406950913MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetHiddenAre1813422502MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_HiddenAreaMesh_t3319190843.h"
#include "AssemblyU2DCSharp_Valve_VR_EHiddenAreaMeshType3068936429.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetInt32Trac2396289227MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetMatrix34T3426445457MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetProjectio2621141914MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix44_t664273159.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetProjectio3426995441MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetPropError1193025139MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetRawZeroPo1986385273MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetRecommend4195542627MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetSeatedZero102610835MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetSortedTra3492202929MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedDeviceClass2121051631.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetStringTrack87797800MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetTimeSince1215702688MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetTrackedDev212130385MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_EDeviceActivityLevel886867856.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetTrackedDe1455580370MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Valve.VR.IVROverlay/_SetOverlayAutoCurveDistanceRangeInMeters::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayAutoCurveDistanceRangeInMeters__ctor_m3478312943 (_SetOverlayAutoCurveDistanceRangeInMeters_t1475163830 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayAutoCurveDistanceRangeInMeters::Invoke(System.UInt64,System.Single,System.Single)
extern "C"  int32_t _SetOverlayAutoCurveDistanceRangeInMeters_Invoke_m3194103798 (_SetOverlayAutoCurveDistanceRangeInMeters_t1475163830 * __this, uint64_t ___ulOverlayHandle0, float ___fMinDistanceInMeters1, float ___fMaxDistanceInMeters2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayAutoCurveDistanceRangeInMeters_Invoke_m3194103798((_SetOverlayAutoCurveDistanceRangeInMeters_t1475163830 *)__this->get_prev_9(),___ulOverlayHandle0, ___fMinDistanceInMeters1, ___fMaxDistanceInMeters2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, float ___fMinDistanceInMeters1, float ___fMaxDistanceInMeters2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___fMinDistanceInMeters1, ___fMaxDistanceInMeters2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, float ___fMinDistanceInMeters1, float ___fMaxDistanceInMeters2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___fMinDistanceInMeters1, ___fMaxDistanceInMeters2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayAutoCurveDistanceRangeInMeters_t1475163830 (_SetOverlayAutoCurveDistanceRangeInMeters_t1475163830 * __this, uint64_t ___ulOverlayHandle0, float ___fMinDistanceInMeters1, float ___fMaxDistanceInMeters2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, float, float);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___fMinDistanceInMeters1, ___fMaxDistanceInMeters2);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayAutoCurveDistanceRangeInMeters::BeginInvoke(System.UInt64,System.Single,System.Single,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayAutoCurveDistanceRangeInMeters_BeginInvoke_m3702547235_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayAutoCurveDistanceRangeInMeters_BeginInvoke_m3702547235 (_SetOverlayAutoCurveDistanceRangeInMeters_t1475163830 * __this, uint64_t ___ulOverlayHandle0, float ___fMinDistanceInMeters1, float ___fMaxDistanceInMeters2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayAutoCurveDistanceRangeInMeters_BeginInvoke_m3702547235_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___fMinDistanceInMeters1);
	__d_args[2] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___fMaxDistanceInMeters2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayAutoCurveDistanceRangeInMeters::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayAutoCurveDistanceRangeInMeters_EndInvoke_m3217418169 (_SetOverlayAutoCurveDistanceRangeInMeters_t1475163830 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayColor::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayColor__ctor_m4115211265 (_SetOverlayColor_t2804573176 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayColor::Invoke(System.UInt64,System.Single,System.Single,System.Single)
extern "C"  int32_t _SetOverlayColor_Invoke_m2702138511 (_SetOverlayColor_t2804573176 * __this, uint64_t ___ulOverlayHandle0, float ___fRed1, float ___fGreen2, float ___fBlue3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayColor_Invoke_m2702138511((_SetOverlayColor_t2804573176 *)__this->get_prev_9(),___ulOverlayHandle0, ___fRed1, ___fGreen2, ___fBlue3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, float ___fRed1, float ___fGreen2, float ___fBlue3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___fRed1, ___fGreen2, ___fBlue3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, float ___fRed1, float ___fGreen2, float ___fBlue3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___fRed1, ___fGreen2, ___fBlue3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayColor_t2804573176 (_SetOverlayColor_t2804573176 * __this, uint64_t ___ulOverlayHandle0, float ___fRed1, float ___fGreen2, float ___fBlue3, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, float, float, float);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___fRed1, ___fGreen2, ___fBlue3);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayColor::BeginInvoke(System.UInt64,System.Single,System.Single,System.Single,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayColor_BeginInvoke_m743038156_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayColor_BeginInvoke_m743038156 (_SetOverlayColor_t2804573176 * __this, uint64_t ___ulOverlayHandle0, float ___fRed1, float ___fGreen2, float ___fBlue3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayColor_BeginInvoke_m743038156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___fRed1);
	__d_args[2] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___fGreen2);
	__d_args[3] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___fBlue3);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayColor::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayColor_EndInvoke_m917296431 (_SetOverlayColor_t2804573176 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayFlag::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayFlag__ctor_m468538538 (_SetOverlayFlag_t1900166033 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayFlag::Invoke(System.UInt64,Valve.VR.VROverlayFlags,System.Boolean)
extern "C"  int32_t _SetOverlayFlag_Invoke_m4124422209 (_SetOverlayFlag_t1900166033 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eOverlayFlag1, bool ___bEnabled2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayFlag_Invoke_m4124422209((_SetOverlayFlag_t1900166033 *)__this->get_prev_9(),___ulOverlayHandle0, ___eOverlayFlag1, ___bEnabled2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, int32_t ___eOverlayFlag1, bool ___bEnabled2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___eOverlayFlag1, ___bEnabled2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, int32_t ___eOverlayFlag1, bool ___bEnabled2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___eOverlayFlag1, ___bEnabled2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayFlag_t1900166033 (_SetOverlayFlag_t1900166033 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eOverlayFlag1, bool ___bEnabled2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, int32_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___eOverlayFlag1, static_cast<int32_t>(___bEnabled2));

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayFlag::BeginInvoke(System.UInt64,Valve.VR.VROverlayFlags,System.Boolean,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* VROverlayFlags_t2344570851_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayFlag_BeginInvoke_m3663599310_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayFlag_BeginInvoke_m3663599310 (_SetOverlayFlag_t1900166033 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eOverlayFlag1, bool ___bEnabled2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayFlag_BeginInvoke_m3663599310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(VROverlayFlags_t2344570851_il2cpp_TypeInfo_var, &___eOverlayFlag1);
	__d_args[2] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___bEnabled2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayFlag::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayFlag_EndInvoke_m2307678206 (_SetOverlayFlag_t1900166033 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayFromFile::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayFromFile__ctor_m2969859798 (_SetOverlayFromFile_t598184189 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayFromFile::Invoke(System.UInt64,System.String)
extern "C"  int32_t _SetOverlayFromFile_Invoke_m973623825 (_SetOverlayFromFile_t598184189 * __this, uint64_t ___ulOverlayHandle0, String_t* ___pchFilePath1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayFromFile_Invoke_m973623825((_SetOverlayFromFile_t598184189 *)__this->get_prev_9(),___ulOverlayHandle0, ___pchFilePath1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, String_t* ___pchFilePath1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___pchFilePath1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, String_t* ___pchFilePath1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___pchFilePath1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayFromFile_t598184189 (_SetOverlayFromFile_t598184189 * __this, uint64_t ___ulOverlayHandle0, String_t* ___pchFilePath1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchFilePath1' to native representation
	char* ____pchFilePath1_marshaled = NULL;
	____pchFilePath1_marshaled = il2cpp_codegen_marshal_string(___pchFilePath1);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ____pchFilePath1_marshaled);

	// Marshaling cleanup of parameter '___pchFilePath1' native representation
	il2cpp_codegen_marshal_free(____pchFilePath1_marshaled);
	____pchFilePath1_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayFromFile::BeginInvoke(System.UInt64,System.String,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayFromFile_BeginInvoke_m3405235600_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayFromFile_BeginInvoke_m3405235600 (_SetOverlayFromFile_t598184189 * __this, uint64_t ___ulOverlayHandle0, String_t* ___pchFilePath1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayFromFile_BeginInvoke_m3405235600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = ___pchFilePath1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayFromFile::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayFromFile_EndInvoke_m2872400866 (_SetOverlayFromFile_t598184189 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayInputMethod::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayInputMethod__ctor_m3092122113 (_SetOverlayInputMethod_t3575042602 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayInputMethod::Invoke(System.UInt64,Valve.VR.VROverlayInputMethod)
extern "C"  int32_t _SetOverlayInputMethod_Invoke_m8169801 (_SetOverlayInputMethod_t3575042602 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eInputMethod1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayInputMethod_Invoke_m8169801((_SetOverlayInputMethod_t3575042602 *)__this->get_prev_9(),___ulOverlayHandle0, ___eInputMethod1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, int32_t ___eInputMethod1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___eInputMethod1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, int32_t ___eInputMethod1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___eInputMethod1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayInputMethod_t3575042602 (_SetOverlayInputMethod_t3575042602 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eInputMethod1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___eInputMethod1);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayInputMethod::BeginInvoke(System.UInt64,Valve.VR.VROverlayInputMethod,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* VROverlayInputMethod_t3830649193_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayInputMethod_BeginInvoke_m605303188_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayInputMethod_BeginInvoke_m605303188 (_SetOverlayInputMethod_t3575042602 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eInputMethod1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayInputMethod_BeginInvoke_m605303188_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(VROverlayInputMethod_t3830649193_il2cpp_TypeInfo_var, &___eInputMethod1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayInputMethod::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayInputMethod_EndInvoke_m1225885463 (_SetOverlayInputMethod_t3575042602 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayIntersectionMask::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayIntersectionMask__ctor_m2962422373 (_SetOverlayIntersectionMask_t3952572284 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayIntersectionMask::Invoke(System.UInt64,Valve.VR.VROverlayIntersectionMaskPrimitive_t&,System.UInt32,System.UInt32)
extern "C"  int32_t _SetOverlayIntersectionMask_Invoke_m1851553101 (_SetOverlayIntersectionMask_t3952572284 * __this, uint64_t ___ulOverlayHandle0, VROverlayIntersectionMaskPrimitive_t_t4278514679 * ___pMaskPrimitives1, uint32_t ___unNumMaskPrimitives2, uint32_t ___unPrimitiveSize3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayIntersectionMask_Invoke_m1851553101((_SetOverlayIntersectionMask_t3952572284 *)__this->get_prev_9(),___ulOverlayHandle0, ___pMaskPrimitives1, ___unNumMaskPrimitives2, ___unPrimitiveSize3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, VROverlayIntersectionMaskPrimitive_t_t4278514679 * ___pMaskPrimitives1, uint32_t ___unNumMaskPrimitives2, uint32_t ___unPrimitiveSize3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___pMaskPrimitives1, ___unNumMaskPrimitives2, ___unPrimitiveSize3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, VROverlayIntersectionMaskPrimitive_t_t4278514679 * ___pMaskPrimitives1, uint32_t ___unNumMaskPrimitives2, uint32_t ___unPrimitiveSize3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___pMaskPrimitives1, ___unNumMaskPrimitives2, ___unPrimitiveSize3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayIntersectionMask_t3952572284 (_SetOverlayIntersectionMask_t3952572284 * __this, uint64_t ___ulOverlayHandle0, VROverlayIntersectionMaskPrimitive_t_t4278514679 * ___pMaskPrimitives1, uint32_t ___unNumMaskPrimitives2, uint32_t ___unPrimitiveSize3, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, VROverlayIntersectionMaskPrimitive_t_t4278514679 *, uint32_t, uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___pMaskPrimitives1, ___unNumMaskPrimitives2, ___unPrimitiveSize3);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayIntersectionMask::BeginInvoke(System.UInt64,Valve.VR.VROverlayIntersectionMaskPrimitive_t&,System.UInt32,System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* VROverlayIntersectionMaskPrimitive_t_t4278514679_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayIntersectionMask_BeginInvoke_m350419626_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayIntersectionMask_BeginInvoke_m350419626 (_SetOverlayIntersectionMask_t3952572284 * __this, uint64_t ___ulOverlayHandle0, VROverlayIntersectionMaskPrimitive_t_t4278514679 * ___pMaskPrimitives1, uint32_t ___unNumMaskPrimitives2, uint32_t ___unPrimitiveSize3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayIntersectionMask_BeginInvoke_m350419626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(VROverlayIntersectionMaskPrimitive_t_t4278514679_il2cpp_TypeInfo_var, &(*___pMaskPrimitives1));
	__d_args[2] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unNumMaskPrimitives2);
	__d_args[3] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unPrimitiveSize3);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayIntersectionMask::EndInvoke(Valve.VR.VROverlayIntersectionMaskPrimitive_t&,System.IAsyncResult)
extern "C"  int32_t _SetOverlayIntersectionMask_EndInvoke_m2642320422 (_SetOverlayIntersectionMask_t3952572284 * __this, VROverlayIntersectionMaskPrimitive_t_t4278514679 * ___pMaskPrimitives0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pMaskPrimitives0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayMouseScale::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayMouseScale__ctor_m460284393 (_SetOverlayMouseScale_t2624726138 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayMouseScale::Invoke(System.UInt64,Valve.VR.HmdVector2_t&)
extern "C"  int32_t _SetOverlayMouseScale_Invoke_m978528017 (_SetOverlayMouseScale_t2624726138 * __this, uint64_t ___ulOverlayHandle0, HmdVector2_t_t2255225135 * ___pvecMouseScale1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayMouseScale_Invoke_m978528017((_SetOverlayMouseScale_t2624726138 *)__this->get_prev_9(),___ulOverlayHandle0, ___pvecMouseScale1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, HmdVector2_t_t2255225135 * ___pvecMouseScale1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___pvecMouseScale1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, HmdVector2_t_t2255225135 * ___pvecMouseScale1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___pvecMouseScale1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayMouseScale_t2624726138 (_SetOverlayMouseScale_t2624726138 * __this, uint64_t ___ulOverlayHandle0, HmdVector2_t_t2255225135 * ___pvecMouseScale1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, HmdVector2_t_t2255225135 *);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___pvecMouseScale1);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayMouseScale::BeginInvoke(System.UInt64,Valve.VR.HmdVector2_t&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* HmdVector2_t_t2255225135_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayMouseScale_BeginInvoke_m1678467926_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayMouseScale_BeginInvoke_m1678467926 (_SetOverlayMouseScale_t2624726138 * __this, uint64_t ___ulOverlayHandle0, HmdVector2_t_t2255225135 * ___pvecMouseScale1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayMouseScale_BeginInvoke_m1678467926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(HmdVector2_t_t2255225135_il2cpp_TypeInfo_var, &(*___pvecMouseScale1));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayMouseScale::EndInvoke(Valve.VR.HmdVector2_t&,System.IAsyncResult)
extern "C"  int32_t _SetOverlayMouseScale_EndInvoke_m902859026 (_SetOverlayMouseScale_t2624726138 * __this, HmdVector2_t_t2255225135 * ___pvecMouseScale0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pvecMouseScale0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayNeighbor::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayNeighbor__ctor_m91799642 (_SetOverlayNeighbor_t1117963895 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayNeighbor::Invoke(Valve.VR.EOverlayDirection,System.UInt64,System.UInt64)
extern "C"  int32_t _SetOverlayNeighbor_Invoke_m201989114 (_SetOverlayNeighbor_t1117963895 * __this, int32_t ___eDirection0, uint64_t ___ulFrom1, uint64_t ___ulTo2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayNeighbor_Invoke_m201989114((_SetOverlayNeighbor_t1117963895 *)__this->get_prev_9(),___eDirection0, ___ulFrom1, ___ulTo2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___eDirection0, uint64_t ___ulFrom1, uint64_t ___ulTo2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eDirection0, ___ulFrom1, ___ulTo2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___eDirection0, uint64_t ___ulFrom1, uint64_t ___ulTo2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eDirection0, ___ulFrom1, ___ulTo2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayNeighbor_t1117963895 (_SetOverlayNeighbor_t1117963895 * __this, int32_t ___eDirection0, uint64_t ___ulFrom1, uint64_t ___ulTo2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(int32_t, uint64_t, uint64_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___eDirection0, ___ulFrom1, ___ulTo2);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayNeighbor::BeginInvoke(Valve.VR.EOverlayDirection,System.UInt64,System.UInt64,System.AsyncCallback,System.Object)
extern Il2CppClass* EOverlayDirection_t2759670492_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayNeighbor_BeginInvoke_m2559932015_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayNeighbor_BeginInvoke_m2559932015 (_SetOverlayNeighbor_t1117963895 * __this, int32_t ___eDirection0, uint64_t ___ulFrom1, uint64_t ___ulTo2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayNeighbor_BeginInvoke_m2559932015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(EOverlayDirection_t2759670492_il2cpp_TypeInfo_var, &___eDirection0);
	__d_args[1] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulFrom1);
	__d_args[2] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulTo2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayNeighbor::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayNeighbor_EndInvoke_m1118765798 (_SetOverlayNeighbor_t1117963895 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayRaw::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayRaw__ctor_m1104188152 (_SetOverlayRaw_t3268606321 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayRaw::Invoke(System.UInt64,System.IntPtr,System.UInt32,System.UInt32,System.UInt32)
extern "C"  int32_t _SetOverlayRaw_Invoke_m3271751677 (_SetOverlayRaw_t3268606321 * __this, uint64_t ___ulOverlayHandle0, IntPtr_t ___pvBuffer1, uint32_t ___unWidth2, uint32_t ___unHeight3, uint32_t ___unDepth4, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayRaw_Invoke_m3271751677((_SetOverlayRaw_t3268606321 *)__this->get_prev_9(),___ulOverlayHandle0, ___pvBuffer1, ___unWidth2, ___unHeight3, ___unDepth4, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, IntPtr_t ___pvBuffer1, uint32_t ___unWidth2, uint32_t ___unHeight3, uint32_t ___unDepth4, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___pvBuffer1, ___unWidth2, ___unHeight3, ___unDepth4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, IntPtr_t ___pvBuffer1, uint32_t ___unWidth2, uint32_t ___unHeight3, uint32_t ___unDepth4, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___pvBuffer1, ___unWidth2, ___unHeight3, ___unDepth4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayRaw_t3268606321 (_SetOverlayRaw_t3268606321 * __this, uint64_t ___ulOverlayHandle0, IntPtr_t ___pvBuffer1, uint32_t ___unWidth2, uint32_t ___unHeight3, uint32_t ___unDepth4, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, intptr_t, uint32_t, uint32_t, uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, reinterpret_cast<intptr_t>((___pvBuffer1).get_m_value_0()), ___unWidth2, ___unHeight3, ___unDepth4);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayRaw::BeginInvoke(System.UInt64,System.IntPtr,System.UInt32,System.UInt32,System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayRaw_BeginInvoke_m2364514622_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayRaw_BeginInvoke_m2364514622 (_SetOverlayRaw_t3268606321 * __this, uint64_t ___ulOverlayHandle0, IntPtr_t ___pvBuffer1, uint32_t ___unWidth2, uint32_t ___unHeight3, uint32_t ___unDepth4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayRaw_BeginInvoke_m2364514622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___pvBuffer1);
	__d_args[2] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unWidth2);
	__d_args[3] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unHeight3);
	__d_args[4] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unDepth4);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback5, (Il2CppObject*)___object6);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayRaw::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayRaw_EndInvoke_m3519291388 (_SetOverlayRaw_t3268606321 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayRenderingPid::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayRenderingPid__ctor_m1493727551 (_SetOverlayRenderingPid_t1970553664 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayRenderingPid::Invoke(System.UInt64,System.UInt32)
extern "C"  int32_t _SetOverlayRenderingPid_Invoke_m2792708852 (_SetOverlayRenderingPid_t1970553664 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unPID1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayRenderingPid_Invoke_m2792708852((_SetOverlayRenderingPid_t1970553664 *)__this->get_prev_9(),___ulOverlayHandle0, ___unPID1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, uint32_t ___unPID1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___unPID1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, uint32_t ___unPID1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___unPID1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayRenderingPid_t1970553664 (_SetOverlayRenderingPid_t1970553664 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unPID1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___unPID1);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayRenderingPid::BeginInvoke(System.UInt64,System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayRenderingPid_BeginInvoke_m1782692179_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayRenderingPid_BeginInvoke_m1782692179 (_SetOverlayRenderingPid_t1970553664 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unPID1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayRenderingPid_BeginInvoke_m1782692179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unPID1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayRenderingPid::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayRenderingPid_EndInvoke_m70980545 (_SetOverlayRenderingPid_t1970553664 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlaySortOrder::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlaySortOrder__ctor_m226450654 (_SetOverlaySortOrder_t3099711365 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlaySortOrder::Invoke(System.UInt64,System.UInt32)
extern "C"  int32_t _SetOverlaySortOrder_Invoke_m2974214863 (_SetOverlaySortOrder_t3099711365 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unSortOrder1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlaySortOrder_Invoke_m2974214863((_SetOverlaySortOrder_t3099711365 *)__this->get_prev_9(),___ulOverlayHandle0, ___unSortOrder1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, uint32_t ___unSortOrder1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___unSortOrder1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, uint32_t ___unSortOrder1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___unSortOrder1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlaySortOrder_t3099711365 (_SetOverlaySortOrder_t3099711365 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unSortOrder1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___unSortOrder1);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlaySortOrder::BeginInvoke(System.UInt64,System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlaySortOrder_BeginInvoke_m3898373280_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlaySortOrder_BeginInvoke_m3898373280 (_SetOverlaySortOrder_t3099711365 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unSortOrder1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlaySortOrder_BeginInvoke_m3898373280_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unSortOrder1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlaySortOrder::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlaySortOrder_EndInvoke_m2112615110 (_SetOverlaySortOrder_t3099711365 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayTexelAspect::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayTexelAspect__ctor_m7922550 (_SetOverlayTexelAspect_t460846625 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTexelAspect::Invoke(System.UInt64,System.Single)
extern "C"  int32_t _SetOverlayTexelAspect_Invoke_m1243437074 (_SetOverlayTexelAspect_t460846625 * __this, uint64_t ___ulOverlayHandle0, float ___fTexelAspect1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayTexelAspect_Invoke_m1243437074((_SetOverlayTexelAspect_t460846625 *)__this->get_prev_9(),___ulOverlayHandle0, ___fTexelAspect1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, float ___fTexelAspect1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___fTexelAspect1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, float ___fTexelAspect1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___fTexelAspect1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayTexelAspect_t460846625 (_SetOverlayTexelAspect_t460846625 * __this, uint64_t ___ulOverlayHandle0, float ___fTexelAspect1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, float);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___fTexelAspect1);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayTexelAspect::BeginInvoke(System.UInt64,System.Single,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayTexelAspect_BeginInvoke_m1444875393_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayTexelAspect_BeginInvoke_m1444875393 (_SetOverlayTexelAspect_t460846625 * __this, uint64_t ___ulOverlayHandle0, float ___fTexelAspect1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayTexelAspect_BeginInvoke_m1444875393_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___fTexelAspect1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTexelAspect::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayTexelAspect_EndInvoke_m4291229802 (_SetOverlayTexelAspect_t460846625 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayTexture::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayTexture__ctor_m2069476867 (_SetOverlayTexture_t2238656700 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTexture::Invoke(System.UInt64,Valve.VR.Texture_t&)
extern "C"  int32_t _SetOverlayTexture_Invoke_m1888514032 (_SetOverlayTexture_t2238656700 * __this, uint64_t ___ulOverlayHandle0, Texture_t_t3277130850 * ___pTexture1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayTexture_Invoke_m1888514032((_SetOverlayTexture_t2238656700 *)__this->get_prev_9(),___ulOverlayHandle0, ___pTexture1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, Texture_t_t3277130850 * ___pTexture1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___pTexture1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, Texture_t_t3277130850 * ___pTexture1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___pTexture1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayTexture_t2238656700 (_SetOverlayTexture_t2238656700 * __this, uint64_t ___ulOverlayHandle0, Texture_t_t3277130850 * ___pTexture1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, Texture_t_t3277130850 *);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___pTexture1);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayTexture::BeginInvoke(System.UInt64,Valve.VR.Texture_t&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture_t_t3277130850_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayTexture_BeginInvoke_m2576935409_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayTexture_BeginInvoke_m2576935409 (_SetOverlayTexture_t2238656700 * __this, uint64_t ___ulOverlayHandle0, Texture_t_t3277130850 * ___pTexture1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayTexture_BeginInvoke_m2576935409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(Texture_t_t3277130850_il2cpp_TypeInfo_var, &(*___pTexture1));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTexture::EndInvoke(Valve.VR.Texture_t&,System.IAsyncResult)
extern "C"  int32_t _SetOverlayTexture_EndInvoke_m4001221015 (_SetOverlayTexture_t2238656700 * __this, Texture_t_t3277130850 * ___pTexture0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pTexture0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayTextureBounds::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayTextureBounds__ctor_m427715900 (_SetOverlayTextureBounds_t1179269927 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTextureBounds::Invoke(System.UInt64,Valve.VR.VRTextureBounds_t&)
extern "C"  int32_t _SetOverlayTextureBounds_Invoke_m1166394270 (_SetOverlayTextureBounds_t1179269927 * __this, uint64_t ___ulOverlayHandle0, VRTextureBounds_t_t1897807375 * ___pOverlayTextureBounds1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayTextureBounds_Invoke_m1166394270((_SetOverlayTextureBounds_t1179269927 *)__this->get_prev_9(),___ulOverlayHandle0, ___pOverlayTextureBounds1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, VRTextureBounds_t_t1897807375 * ___pOverlayTextureBounds1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___pOverlayTextureBounds1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, VRTextureBounds_t_t1897807375 * ___pOverlayTextureBounds1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___pOverlayTextureBounds1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayTextureBounds_t1179269927 (_SetOverlayTextureBounds_t1179269927 * __this, uint64_t ___ulOverlayHandle0, VRTextureBounds_t_t1897807375 * ___pOverlayTextureBounds1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, VRTextureBounds_t_t1897807375 *);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___pOverlayTextureBounds1);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayTextureBounds::BeginInvoke(System.UInt64,Valve.VR.VRTextureBounds_t&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* VRTextureBounds_t_t1897807375_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayTextureBounds_BeginInvoke_m2978399441_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayTextureBounds_BeginInvoke_m2978399441 (_SetOverlayTextureBounds_t1179269927 * __this, uint64_t ___ulOverlayHandle0, VRTextureBounds_t_t1897807375 * ___pOverlayTextureBounds1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayTextureBounds_BeginInvoke_m2978399441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(VRTextureBounds_t_t1897807375_il2cpp_TypeInfo_var, &(*___pOverlayTextureBounds1));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTextureBounds::EndInvoke(Valve.VR.VRTextureBounds_t&,System.IAsyncResult)
extern "C"  int32_t _SetOverlayTextureBounds_EndInvoke_m2056867507 (_SetOverlayTextureBounds_t1179269927 * __this, VRTextureBounds_t_t1897807375 * ___pOverlayTextureBounds0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pOverlayTextureBounds0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayTextureColorSpace::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayTextureColorSpace__ctor_m1758240510 (_SetOverlayTextureColorSpace_t1399555963 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTextureColorSpace::Invoke(System.UInt64,Valve.VR.EColorSpace)
extern "C"  int32_t _SetOverlayTextureColorSpace_Invoke_m3985814997 (_SetOverlayTextureColorSpace_t1399555963 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTextureColorSpace1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayTextureColorSpace_Invoke_m3985814997((_SetOverlayTextureColorSpace_t1399555963 *)__this->get_prev_9(),___ulOverlayHandle0, ___eTextureColorSpace1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, int32_t ___eTextureColorSpace1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___eTextureColorSpace1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, int32_t ___eTextureColorSpace1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___eTextureColorSpace1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayTextureColorSpace_t1399555963 (_SetOverlayTextureColorSpace_t1399555963 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTextureColorSpace1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___eTextureColorSpace1);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayTextureColorSpace::BeginInvoke(System.UInt64,Valve.VR.EColorSpace,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* EColorSpace_t2848861630_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayTextureColorSpace_BeginInvoke_m424434592_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayTextureColorSpace_BeginInvoke_m424434592 (_SetOverlayTextureColorSpace_t1399555963 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTextureColorSpace1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayTextureColorSpace_BeginInvoke_m424434592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(EColorSpace_t2848861630_il2cpp_TypeInfo_var, &___eTextureColorSpace1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTextureColorSpace::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayTextureColorSpace_EndInvoke_m4018109338 (_SetOverlayTextureColorSpace_t1399555963 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayTransformAbsolute::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayTransformAbsolute__ctor_m50906963 (_SetOverlayTransformAbsolute_t2100297354 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTransformAbsolute::Invoke(System.UInt64,Valve.VR.ETrackingUniverseOrigin,Valve.VR.HmdMatrix34_t&)
extern "C"  int32_t _SetOverlayTransformAbsolute_Invoke_m2113521179 (_SetOverlayTransformAbsolute_t2100297354 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTrackingOrigin1, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToOverlayTransform2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayTransformAbsolute_Invoke_m2113521179((_SetOverlayTransformAbsolute_t2100297354 *)__this->get_prev_9(),___ulOverlayHandle0, ___eTrackingOrigin1, ___pmatTrackingOriginToOverlayTransform2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, int32_t ___eTrackingOrigin1, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToOverlayTransform2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___eTrackingOrigin1, ___pmatTrackingOriginToOverlayTransform2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, int32_t ___eTrackingOrigin1, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToOverlayTransform2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___eTrackingOrigin1, ___pmatTrackingOriginToOverlayTransform2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayTransformAbsolute_t2100297354 (_SetOverlayTransformAbsolute_t2100297354 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTrackingOrigin1, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToOverlayTransform2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, int32_t, HmdMatrix34_t_t664273062 *);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___eTrackingOrigin1, ___pmatTrackingOriginToOverlayTransform2);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayTransformAbsolute::BeginInvoke(System.UInt64,Valve.VR.ETrackingUniverseOrigin,Valve.VR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* ETrackingUniverseOrigin_t1464400093_il2cpp_TypeInfo_var;
extern Il2CppClass* HmdMatrix34_t_t664273062_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayTransformAbsolute_BeginInvoke_m3734430934_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayTransformAbsolute_BeginInvoke_m3734430934 (_SetOverlayTransformAbsolute_t2100297354 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTrackingOrigin1, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToOverlayTransform2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayTransformAbsolute_BeginInvoke_m3734430934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(ETrackingUniverseOrigin_t1464400093_il2cpp_TypeInfo_var, &___eTrackingOrigin1);
	__d_args[2] = Box(HmdMatrix34_t_t664273062_il2cpp_TypeInfo_var, &(*___pmatTrackingOriginToOverlayTransform2));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTransformAbsolute::EndInvoke(Valve.VR.HmdMatrix34_t&,System.IAsyncResult)
extern "C"  int32_t _SetOverlayTransformAbsolute_EndInvoke_m401476197 (_SetOverlayTransformAbsolute_t2100297354 * __this, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToOverlayTransform0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pmatTrackingOriginToOverlayTransform0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceComponent::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayTransformTrackedDeviceComponent__ctor_m404352569 (_SetOverlayTransformTrackedDeviceComponent_t1749665136 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceComponent::Invoke(System.UInt64,System.UInt32,System.String)
extern "C"  int32_t _SetOverlayTransformTrackedDeviceComponent_Invoke_m2742715876 (_SetOverlayTransformTrackedDeviceComponent_t1749665136 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unDeviceIndex1, String_t* ___pchComponentName2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayTransformTrackedDeviceComponent_Invoke_m2742715876((_SetOverlayTransformTrackedDeviceComponent_t1749665136 *)__this->get_prev_9(),___ulOverlayHandle0, ___unDeviceIndex1, ___pchComponentName2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, uint32_t ___unDeviceIndex1, String_t* ___pchComponentName2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___unDeviceIndex1, ___pchComponentName2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, uint32_t ___unDeviceIndex1, String_t* ___pchComponentName2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___unDeviceIndex1, ___pchComponentName2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayTransformTrackedDeviceComponent_t1749665136 (_SetOverlayTransformTrackedDeviceComponent_t1749665136 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unDeviceIndex1, String_t* ___pchComponentName2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, uint32_t, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchComponentName2' to native representation
	char* ____pchComponentName2_marshaled = NULL;
	____pchComponentName2_marshaled = il2cpp_codegen_marshal_string(___pchComponentName2);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___unDeviceIndex1, ____pchComponentName2_marshaled);

	// Marshaling cleanup of parameter '___pchComponentName2' native representation
	il2cpp_codegen_marshal_free(____pchComponentName2_marshaled);
	____pchComponentName2_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceComponent::BeginInvoke(System.UInt64,System.UInt32,System.String,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayTransformTrackedDeviceComponent_BeginInvoke_m1125179223_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayTransformTrackedDeviceComponent_BeginInvoke_m1125179223 (_SetOverlayTransformTrackedDeviceComponent_t1749665136 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unDeviceIndex1, String_t* ___pchComponentName2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayTransformTrackedDeviceComponent_BeginInvoke_m1125179223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unDeviceIndex1);
	__d_args[2] = ___pchComponentName2;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceComponent::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayTransformTrackedDeviceComponent_EndInvoke_m3757268983 (_SetOverlayTransformTrackedDeviceComponent_t1749665136 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceRelative::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayTransformTrackedDeviceRelative__ctor_m163753444 (_SetOverlayTransformTrackedDeviceRelative_t579082695 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceRelative::Invoke(System.UInt64,System.UInt32,Valve.VR.HmdMatrix34_t&)
extern "C"  int32_t _SetOverlayTransformTrackedDeviceRelative_Invoke_m2348853633 (_SetOverlayTransformTrackedDeviceRelative_t579082695 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unTrackedDevice1, HmdMatrix34_t_t664273062 * ___pmatTrackedDeviceToOverlayTransform2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayTransformTrackedDeviceRelative_Invoke_m2348853633((_SetOverlayTransformTrackedDeviceRelative_t579082695 *)__this->get_prev_9(),___ulOverlayHandle0, ___unTrackedDevice1, ___pmatTrackedDeviceToOverlayTransform2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, uint32_t ___unTrackedDevice1, HmdMatrix34_t_t664273062 * ___pmatTrackedDeviceToOverlayTransform2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___unTrackedDevice1, ___pmatTrackedDeviceToOverlayTransform2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, uint32_t ___unTrackedDevice1, HmdMatrix34_t_t664273062 * ___pmatTrackedDeviceToOverlayTransform2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___unTrackedDevice1, ___pmatTrackedDeviceToOverlayTransform2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayTransformTrackedDeviceRelative_t579082695 (_SetOverlayTransformTrackedDeviceRelative_t579082695 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unTrackedDevice1, HmdMatrix34_t_t664273062 * ___pmatTrackedDeviceToOverlayTransform2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, uint32_t, HmdMatrix34_t_t664273062 *);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___unTrackedDevice1, ___pmatTrackedDeviceToOverlayTransform2);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceRelative::BeginInvoke(System.UInt64,System.UInt32,Valve.VR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* HmdMatrix34_t_t664273062_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayTransformTrackedDeviceRelative_BeginInvoke_m4142423774_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayTransformTrackedDeviceRelative_BeginInvoke_m4142423774 (_SetOverlayTransformTrackedDeviceRelative_t579082695 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unTrackedDevice1, HmdMatrix34_t_t664273062 * ___pmatTrackedDeviceToOverlayTransform2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayTransformTrackedDeviceRelative_BeginInvoke_m4142423774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unTrackedDevice1);
	__d_args[2] = Box(HmdMatrix34_t_t664273062_il2cpp_TypeInfo_var, &(*___pmatTrackedDeviceToOverlayTransform2));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceRelative::EndInvoke(Valve.VR.HmdMatrix34_t&,System.IAsyncResult)
extern "C"  int32_t _SetOverlayTransformTrackedDeviceRelative_EndInvoke_m178959756 (_SetOverlayTransformTrackedDeviceRelative_t579082695 * __this, HmdMatrix34_t_t664273062 * ___pmatTrackedDeviceToOverlayTransform0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pmatTrackedDeviceToOverlayTransform0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_SetOverlayWidthInMeters::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayWidthInMeters__ctor_m3877495301 (_SetOverlayWidthInMeters_t3047220066 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayWidthInMeters::Invoke(System.UInt64,System.Single)
extern "C"  int32_t _SetOverlayWidthInMeters_Invoke_m2146702471 (_SetOverlayWidthInMeters_t3047220066 * __this, uint64_t ___ulOverlayHandle0, float ___fWidthInMeters1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetOverlayWidthInMeters_Invoke_m2146702471((_SetOverlayWidthInMeters_t3047220066 *)__this->get_prev_9(),___ulOverlayHandle0, ___fWidthInMeters1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, float ___fWidthInMeters1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___fWidthInMeters1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, float ___fWidthInMeters1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___fWidthInMeters1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SetOverlayWidthInMeters_t3047220066 (_SetOverlayWidthInMeters_t3047220066 * __this, uint64_t ___ulOverlayHandle0, float ___fWidthInMeters1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, float);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___fWidthInMeters1);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayWidthInMeters::BeginInvoke(System.UInt64,System.Single,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t _SetOverlayWidthInMeters_BeginInvoke_m1494965178_MetadataUsageId;
extern "C"  Il2CppObject * _SetOverlayWidthInMeters_BeginInvoke_m1494965178 (_SetOverlayWidthInMeters_t3047220066 * __this, uint64_t ___ulOverlayHandle0, float ___fWidthInMeters1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetOverlayWidthInMeters_BeginInvoke_m1494965178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___fWidthInMeters1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayWidthInMeters::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayWidthInMeters_EndInvoke_m2619954543 (_SetOverlayWidthInMeters_t3047220066 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_ShowDashboard::.ctor(System.Object,System.IntPtr)
extern "C"  void _ShowDashboard__ctor_m3814115003 (_ShowDashboard_t4127025320 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVROverlay/_ShowDashboard::Invoke(System.String)
extern "C"  void _ShowDashboard_Invoke_m862074589 (_ShowDashboard_t4127025320 * __this, String_t* ___pchOverlayToShow0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_ShowDashboard_Invoke_m862074589((_ShowDashboard_t4127025320 *)__this->get_prev_9(),___pchOverlayToShow0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchOverlayToShow0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchOverlayToShow0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___pchOverlayToShow0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchOverlayToShow0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pchOverlayToShow0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__ShowDashboard_t4127025320 (_ShowDashboard_t4127025320 * __this, String_t* ___pchOverlayToShow0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchOverlayToShow0' to native representation
	char* ____pchOverlayToShow0_marshaled = NULL;
	____pchOverlayToShow0_marshaled = il2cpp_codegen_marshal_string(___pchOverlayToShow0);

	// Native function invocation
	il2cppPInvokeFunc(____pchOverlayToShow0_marshaled);

	// Marshaling cleanup of parameter '___pchOverlayToShow0' native representation
	il2cpp_codegen_marshal_free(____pchOverlayToShow0_marshaled);
	____pchOverlayToShow0_marshaled = NULL;

}
// System.IAsyncResult Valve.VR.IVROverlay/_ShowDashboard::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ShowDashboard_BeginInvoke_m1839620774 (_ShowDashboard_t4127025320 * __this, String_t* ___pchOverlayToShow0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___pchOverlayToShow0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Valve.VR.IVROverlay/_ShowDashboard::EndInvoke(System.IAsyncResult)
extern "C"  void _ShowDashboard_EndInvoke_m2941218449 (_ShowDashboard_t4127025320 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Valve.VR.IVROverlay/_ShowKeyboard::.ctor(System.Object,System.IntPtr)
extern "C"  void _ShowKeyboard__ctor_m320904348 (_ShowKeyboard_t3095606223 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_ShowKeyboard::Invoke(System.Int32,System.Int32,System.String,System.UInt32,System.String,System.Boolean,System.UInt64)
extern "C"  int32_t _ShowKeyboard_Invoke_m2347904594 (_ShowKeyboard_t3095606223 * __this, int32_t ___eInputMode0, int32_t ___eLineInputMode1, String_t* ___pchDescription2, uint32_t ___unCharMax3, String_t* ___pchExistingText4, bool ___bUseMinimalMode5, uint64_t ___uUserValue6, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_ShowKeyboard_Invoke_m2347904594((_ShowKeyboard_t3095606223 *)__this->get_prev_9(),___eInputMode0, ___eLineInputMode1, ___pchDescription2, ___unCharMax3, ___pchExistingText4, ___bUseMinimalMode5, ___uUserValue6, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___eInputMode0, int32_t ___eLineInputMode1, String_t* ___pchDescription2, uint32_t ___unCharMax3, String_t* ___pchExistingText4, bool ___bUseMinimalMode5, uint64_t ___uUserValue6, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eInputMode0, ___eLineInputMode1, ___pchDescription2, ___unCharMax3, ___pchExistingText4, ___bUseMinimalMode5, ___uUserValue6,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___eInputMode0, int32_t ___eLineInputMode1, String_t* ___pchDescription2, uint32_t ___unCharMax3, String_t* ___pchExistingText4, bool ___bUseMinimalMode5, uint64_t ___uUserValue6, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eInputMode0, ___eLineInputMode1, ___pchDescription2, ___unCharMax3, ___pchExistingText4, ___bUseMinimalMode5, ___uUserValue6,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__ShowKeyboard_t3095606223 (_ShowKeyboard_t3095606223 * __this, int32_t ___eInputMode0, int32_t ___eLineInputMode1, String_t* ___pchDescription2, uint32_t ___unCharMax3, String_t* ___pchExistingText4, bool ___bUseMinimalMode5, uint64_t ___uUserValue6, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(int32_t, int32_t, char*, uint32_t, char*, int32_t, uint64_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchDescription2' to native representation
	char* ____pchDescription2_marshaled = NULL;
	____pchDescription2_marshaled = il2cpp_codegen_marshal_string(___pchDescription2);

	// Marshaling of parameter '___pchExistingText4' to native representation
	char* ____pchExistingText4_marshaled = NULL;
	____pchExistingText4_marshaled = il2cpp_codegen_marshal_string(___pchExistingText4);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___eInputMode0, ___eLineInputMode1, ____pchDescription2_marshaled, ___unCharMax3, ____pchExistingText4_marshaled, static_cast<int32_t>(___bUseMinimalMode5), ___uUserValue6);

	// Marshaling cleanup of parameter '___pchDescription2' native representation
	il2cpp_codegen_marshal_free(____pchDescription2_marshaled);
	____pchDescription2_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchExistingText4' native representation
	il2cpp_codegen_marshal_free(____pchExistingText4_marshaled);
	____pchExistingText4_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_ShowKeyboard::BeginInvoke(System.Int32,System.Int32,System.String,System.UInt32,System.String,System.Boolean,System.UInt64,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern const uint32_t _ShowKeyboard_BeginInvoke_m2918508965_MetadataUsageId;
extern "C"  Il2CppObject * _ShowKeyboard_BeginInvoke_m2918508965 (_ShowKeyboard_t3095606223 * __this, int32_t ___eInputMode0, int32_t ___eLineInputMode1, String_t* ___pchDescription2, uint32_t ___unCharMax3, String_t* ___pchExistingText4, bool ___bUseMinimalMode5, uint64_t ___uUserValue6, AsyncCallback_t163412349 * ___callback7, Il2CppObject * ___object8, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_ShowKeyboard_BeginInvoke_m2918508965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[8] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___eInputMode0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___eLineInputMode1);
	__d_args[2] = ___pchDescription2;
	__d_args[3] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unCharMax3);
	__d_args[4] = ___pchExistingText4;
	__d_args[5] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___bUseMinimalMode5);
	__d_args[6] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___uUserValue6);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback7, (Il2CppObject*)___object8);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_ShowKeyboard::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _ShowKeyboard_EndInvoke_m3390091732 (_ShowKeyboard_t3095606223 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_ShowKeyboardForOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _ShowKeyboardForOverlay__ctor_m3804754231 (_ShowKeyboardForOverlay_t3006565844 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_ShowKeyboardForOverlay::Invoke(System.UInt64,System.Int32,System.Int32,System.String,System.UInt32,System.String,System.Boolean,System.UInt64)
extern "C"  int32_t _ShowKeyboardForOverlay_Invoke_m2067399400 (_ShowKeyboardForOverlay_t3006565844 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eInputMode1, int32_t ___eLineInputMode2, String_t* ___pchDescription3, uint32_t ___unCharMax4, String_t* ___pchExistingText5, bool ___bUseMinimalMode6, uint64_t ___uUserValue7, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_ShowKeyboardForOverlay_Invoke_m2067399400((_ShowKeyboardForOverlay_t3006565844 *)__this->get_prev_9(),___ulOverlayHandle0, ___eInputMode1, ___eLineInputMode2, ___pchDescription3, ___unCharMax4, ___pchExistingText5, ___bUseMinimalMode6, ___uUserValue7, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, int32_t ___eInputMode1, int32_t ___eLineInputMode2, String_t* ___pchDescription3, uint32_t ___unCharMax4, String_t* ___pchExistingText5, bool ___bUseMinimalMode6, uint64_t ___uUserValue7, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0, ___eInputMode1, ___eLineInputMode2, ___pchDescription3, ___unCharMax4, ___pchExistingText5, ___bUseMinimalMode6, ___uUserValue7,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, int32_t ___eInputMode1, int32_t ___eLineInputMode2, String_t* ___pchDescription3, uint32_t ___unCharMax4, String_t* ___pchExistingText5, bool ___bUseMinimalMode6, uint64_t ___uUserValue7, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0, ___eInputMode1, ___eLineInputMode2, ___pchDescription3, ___unCharMax4, ___pchExistingText5, ___bUseMinimalMode6, ___uUserValue7,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__ShowKeyboardForOverlay_t3006565844 (_ShowKeyboardForOverlay_t3006565844 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eInputMode1, int32_t ___eLineInputMode2, String_t* ___pchDescription3, uint32_t ___unCharMax4, String_t* ___pchExistingText5, bool ___bUseMinimalMode6, uint64_t ___uUserValue7, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t, int32_t, int32_t, char*, uint32_t, char*, int32_t, uint64_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchDescription3' to native representation
	char* ____pchDescription3_marshaled = NULL;
	____pchDescription3_marshaled = il2cpp_codegen_marshal_string(___pchDescription3);

	// Marshaling of parameter '___pchExistingText5' to native representation
	char* ____pchExistingText5_marshaled = NULL;
	____pchExistingText5_marshaled = il2cpp_codegen_marshal_string(___pchExistingText5);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0, ___eInputMode1, ___eLineInputMode2, ____pchDescription3_marshaled, ___unCharMax4, ____pchExistingText5_marshaled, static_cast<int32_t>(___bUseMinimalMode6), ___uUserValue7);

	// Marshaling cleanup of parameter '___pchDescription3' native representation
	il2cpp_codegen_marshal_free(____pchDescription3_marshaled);
	____pchDescription3_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchExistingText5' native representation
	il2cpp_codegen_marshal_free(____pchExistingText5_marshaled);
	____pchExistingText5_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_ShowKeyboardForOverlay::BeginInvoke(System.UInt64,System.Int32,System.Int32,System.String,System.UInt32,System.String,System.Boolean,System.UInt64,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t _ShowKeyboardForOverlay_BeginInvoke_m2148736307_MetadataUsageId;
extern "C"  Il2CppObject * _ShowKeyboardForOverlay_BeginInvoke_m2148736307 (_ShowKeyboardForOverlay_t3006565844 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eInputMode1, int32_t ___eLineInputMode2, String_t* ___pchDescription3, uint32_t ___unCharMax4, String_t* ___pchExistingText5, bool ___bUseMinimalMode6, uint64_t ___uUserValue7, AsyncCallback_t163412349 * ___callback8, Il2CppObject * ___object9, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_ShowKeyboardForOverlay_BeginInvoke_m2148736307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[9] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___eInputMode1);
	__d_args[2] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___eLineInputMode2);
	__d_args[3] = ___pchDescription3;
	__d_args[4] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unCharMax4);
	__d_args[5] = ___pchExistingText5;
	__d_args[6] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___bUseMinimalMode6);
	__d_args[7] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___uUserValue7);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback8, (Il2CppObject*)___object9);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_ShowKeyboardForOverlay::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _ShowKeyboardForOverlay_EndInvoke_m37445941 (_ShowKeyboardForOverlay_t3006565844 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_ShowMessageOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _ShowMessageOverlay__ctor_m547989352 (_ShowMessageOverlay_t3284759035 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.VRMessageOverlayResponse Valve.VR.IVROverlay/_ShowMessageOverlay::Invoke(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  int32_t _ShowMessageOverlay_Invoke_m1793096887 (_ShowMessageOverlay_t3284759035 * __this, String_t* ___pchText0, String_t* ___pchCaption1, String_t* ___pchButton0Text2, String_t* ___pchButton1Text3, String_t* ___pchButton2Text4, String_t* ___pchButton3Text5, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_ShowMessageOverlay_Invoke_m1793096887((_ShowMessageOverlay_t3284759035 *)__this->get_prev_9(),___pchText0, ___pchCaption1, ___pchButton0Text2, ___pchButton1Text3, ___pchButton2Text4, ___pchButton3Text5, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchText0, String_t* ___pchCaption1, String_t* ___pchButton0Text2, String_t* ___pchButton1Text3, String_t* ___pchButton2Text4, String_t* ___pchButton3Text5, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchText0, ___pchCaption1, ___pchButton0Text2, ___pchButton1Text3, ___pchButton2Text4, ___pchButton3Text5,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, String_t* ___pchText0, String_t* ___pchCaption1, String_t* ___pchButton0Text2, String_t* ___pchButton1Text3, String_t* ___pchButton2Text4, String_t* ___pchButton3Text5, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchText0, ___pchCaption1, ___pchButton0Text2, ___pchButton1Text3, ___pchButton2Text4, ___pchButton3Text5,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, String_t* ___pchCaption1, String_t* ___pchButton0Text2, String_t* ___pchButton1Text3, String_t* ___pchButton2Text4, String_t* ___pchButton3Text5, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchText0, ___pchCaption1, ___pchButton0Text2, ___pchButton1Text3, ___pchButton2Text4, ___pchButton3Text5,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__ShowMessageOverlay_t3284759035 (_ShowMessageOverlay_t3284759035 * __this, String_t* ___pchText0, String_t* ___pchCaption1, String_t* ___pchButton0Text2, String_t* ___pchButton1Text3, String_t* ___pchButton2Text4, String_t* ___pchButton3Text5, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(char*, char*, char*, char*, char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchText0' to native representation
	char* ____pchText0_marshaled = NULL;
	____pchText0_marshaled = il2cpp_codegen_marshal_string(___pchText0);

	// Marshaling of parameter '___pchCaption1' to native representation
	char* ____pchCaption1_marshaled = NULL;
	____pchCaption1_marshaled = il2cpp_codegen_marshal_string(___pchCaption1);

	// Marshaling of parameter '___pchButton0Text2' to native representation
	char* ____pchButton0Text2_marshaled = NULL;
	____pchButton0Text2_marshaled = il2cpp_codegen_marshal_string(___pchButton0Text2);

	// Marshaling of parameter '___pchButton1Text3' to native representation
	char* ____pchButton1Text3_marshaled = NULL;
	____pchButton1Text3_marshaled = il2cpp_codegen_marshal_string(___pchButton1Text3);

	// Marshaling of parameter '___pchButton2Text4' to native representation
	char* ____pchButton2Text4_marshaled = NULL;
	____pchButton2Text4_marshaled = il2cpp_codegen_marshal_string(___pchButton2Text4);

	// Marshaling of parameter '___pchButton3Text5' to native representation
	char* ____pchButton3Text5_marshaled = NULL;
	____pchButton3Text5_marshaled = il2cpp_codegen_marshal_string(___pchButton3Text5);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____pchText0_marshaled, ____pchCaption1_marshaled, ____pchButton0Text2_marshaled, ____pchButton1Text3_marshaled, ____pchButton2Text4_marshaled, ____pchButton3Text5_marshaled);

	// Marshaling cleanup of parameter '___pchText0' native representation
	il2cpp_codegen_marshal_free(____pchText0_marshaled);
	____pchText0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchCaption1' native representation
	il2cpp_codegen_marshal_free(____pchCaption1_marshaled);
	____pchCaption1_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchButton0Text2' native representation
	il2cpp_codegen_marshal_free(____pchButton0Text2_marshaled);
	____pchButton0Text2_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchButton1Text3' native representation
	il2cpp_codegen_marshal_free(____pchButton1Text3_marshaled);
	____pchButton1Text3_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchButton2Text4' native representation
	il2cpp_codegen_marshal_free(____pchButton2Text4_marshaled);
	____pchButton2Text4_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchButton3Text5' native representation
	il2cpp_codegen_marshal_free(____pchButton3Text5_marshaled);
	____pchButton3Text5_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_ShowMessageOverlay::BeginInvoke(System.String,System.String,System.String,System.String,System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ShowMessageOverlay_BeginInvoke_m2757357013 (_ShowMessageOverlay_t3284759035 * __this, String_t* ___pchText0, String_t* ___pchCaption1, String_t* ___pchButton0Text2, String_t* ___pchButton1Text3, String_t* ___pchButton2Text4, String_t* ___pchButton3Text5, AsyncCallback_t163412349 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method)
{
	void *__d_args[7] = {0};
	__d_args[0] = ___pchText0;
	__d_args[1] = ___pchCaption1;
	__d_args[2] = ___pchButton0Text2;
	__d_args[3] = ___pchButton1Text3;
	__d_args[4] = ___pchButton2Text4;
	__d_args[5] = ___pchButton3Text5;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback6, (Il2CppObject*)___object7);
}
// Valve.VR.VRMessageOverlayResponse Valve.VR.IVROverlay/_ShowMessageOverlay::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _ShowMessageOverlay_EndInvoke_m1502031633 (_ShowMessageOverlay_t3284759035 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVROverlay/_ShowOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _ShowOverlay__ctor_m1335864653 (_ShowOverlay_t733914692 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_ShowOverlay::Invoke(System.UInt64)
extern "C"  int32_t _ShowOverlay_Invoke_m2200725924 (_ShowOverlay_t733914692 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_ShowOverlay_Invoke_m2200725924((_ShowOverlay_t733914692 *)__this->get_prev_9(),___ulOverlayHandle0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ulOverlayHandle0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ulOverlayHandle0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__ShowOverlay_t733914692 (_ShowOverlay_t733914692 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint64_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___ulOverlayHandle0);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVROverlay/_ShowOverlay::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern const uint32_t _ShowOverlay_BeginInvoke_m3995783825_MetadataUsageId;
extern "C"  Il2CppObject * _ShowOverlay_BeginInvoke_m3995783825 (_ShowOverlay_t733914692 * __this, uint64_t ___ulOverlayHandle0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_ShowOverlay_BeginInvoke_m3995783825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___ulOverlayHandle0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_ShowOverlay::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _ShowOverlay_EndInvoke_m4102959827 (_ShowOverlay_t733914692 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Conversion methods for marshalling of: Valve.VR.IVRRenderModels
extern "C" void IVRRenderModels_t3420796425_marshal_pinvoke(const IVRRenderModels_t3420796425& unmarshaled, IVRRenderModels_t3420796425_marshaled_pinvoke& marshaled)
{
	marshaled.___LoadRenderModel_Async_0 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_LoadRenderModel_Async_0()));
	marshaled.___FreeRenderModel_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_FreeRenderModel_1()));
	marshaled.___LoadTexture_Async_2 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_LoadTexture_Async_2()));
	marshaled.___FreeTexture_3 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_FreeTexture_3()));
	marshaled.___LoadTextureD3D11_Async_4 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_LoadTextureD3D11_Async_4()));
	marshaled.___LoadIntoTextureD3D11_Async_5 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_LoadIntoTextureD3D11_Async_5()));
	marshaled.___FreeTextureD3D11_6 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_FreeTextureD3D11_6()));
	marshaled.___GetRenderModelName_7 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetRenderModelName_7()));
	marshaled.___GetRenderModelCount_8 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetRenderModelCount_8()));
	marshaled.___GetComponentCount_9 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetComponentCount_9()));
	marshaled.___GetComponentName_10 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetComponentName_10()));
	marshaled.___GetComponentButtonMask_11 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetComponentButtonMask_11()));
	marshaled.___GetComponentRenderModelName_12 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetComponentRenderModelName_12()));
	marshaled.___GetComponentState_13 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetComponentState_13()));
	marshaled.___RenderModelHasComponent_14 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_RenderModelHasComponent_14()));
	marshaled.___GetRenderModelThumbnailURL_15 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetRenderModelThumbnailURL_15()));
	marshaled.___GetRenderModelOriginalPath_16 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetRenderModelOriginalPath_16()));
	marshaled.___GetRenderModelErrorNameFromEnum_17 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetRenderModelErrorNameFromEnum_17()));
}
extern Il2CppClass* _LoadRenderModel_Async_t3622270247_il2cpp_TypeInfo_var;
extern Il2CppClass* _FreeRenderModel_t2139843464_il2cpp_TypeInfo_var;
extern Il2CppClass* _LoadTexture_Async_t1786536393_il2cpp_TypeInfo_var;
extern Il2CppClass* _FreeTexture_t4051202214_il2cpp_TypeInfo_var;
extern Il2CppClass* _LoadTextureD3D11_Async_t2681806282_il2cpp_TypeInfo_var;
extern Il2CppClass* _LoadIntoTextureD3D11_Async_t3518850916_il2cpp_TypeInfo_var;
extern Il2CppClass* _FreeTextureD3D11_t2385986941_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetRenderModelName_t4149685257_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetRenderModelCount_t3364784497_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetComponentCount_t763371255_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetComponentName_t3462998887_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetComponentButtonMask_t1474657094_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetComponentRenderModelName_t2860930600_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetComponentState_t742926735_il2cpp_TypeInfo_var;
extern Il2CppClass* _RenderModelHasComponent_t1969881317_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetRenderModelThumbnailURL_t3954674309_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetRenderModelOriginalPath_t4216085620_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetRenderModelErrorNameFromEnum_t298277168_il2cpp_TypeInfo_var;
extern const uint32_t IVRRenderModels_t3420796425_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void IVRRenderModels_t3420796425_marshal_pinvoke_back(const IVRRenderModels_t3420796425_marshaled_pinvoke& marshaled, IVRRenderModels_t3420796425& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVRRenderModels_t3420796425_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_LoadRenderModel_Async_0(il2cpp_codegen_marshal_function_ptr_to_delegate<_LoadRenderModel_Async_t3622270247>(marshaled.___LoadRenderModel_Async_0, _LoadRenderModel_Async_t3622270247_il2cpp_TypeInfo_var));
	unmarshaled.set_FreeRenderModel_1(il2cpp_codegen_marshal_function_ptr_to_delegate<_FreeRenderModel_t2139843464>(marshaled.___FreeRenderModel_1, _FreeRenderModel_t2139843464_il2cpp_TypeInfo_var));
	unmarshaled.set_LoadTexture_Async_2(il2cpp_codegen_marshal_function_ptr_to_delegate<_LoadTexture_Async_t1786536393>(marshaled.___LoadTexture_Async_2, _LoadTexture_Async_t1786536393_il2cpp_TypeInfo_var));
	unmarshaled.set_FreeTexture_3(il2cpp_codegen_marshal_function_ptr_to_delegate<_FreeTexture_t4051202214>(marshaled.___FreeTexture_3, _FreeTexture_t4051202214_il2cpp_TypeInfo_var));
	unmarshaled.set_LoadTextureD3D11_Async_4(il2cpp_codegen_marshal_function_ptr_to_delegate<_LoadTextureD3D11_Async_t2681806282>(marshaled.___LoadTextureD3D11_Async_4, _LoadTextureD3D11_Async_t2681806282_il2cpp_TypeInfo_var));
	unmarshaled.set_LoadIntoTextureD3D11_Async_5(il2cpp_codegen_marshal_function_ptr_to_delegate<_LoadIntoTextureD3D11_Async_t3518850916>(marshaled.___LoadIntoTextureD3D11_Async_5, _LoadIntoTextureD3D11_Async_t3518850916_il2cpp_TypeInfo_var));
	unmarshaled.set_FreeTextureD3D11_6(il2cpp_codegen_marshal_function_ptr_to_delegate<_FreeTextureD3D11_t2385986941>(marshaled.___FreeTextureD3D11_6, _FreeTextureD3D11_t2385986941_il2cpp_TypeInfo_var));
	unmarshaled.set_GetRenderModelName_7(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetRenderModelName_t4149685257>(marshaled.___GetRenderModelName_7, _GetRenderModelName_t4149685257_il2cpp_TypeInfo_var));
	unmarshaled.set_GetRenderModelCount_8(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetRenderModelCount_t3364784497>(marshaled.___GetRenderModelCount_8, _GetRenderModelCount_t3364784497_il2cpp_TypeInfo_var));
	unmarshaled.set_GetComponentCount_9(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetComponentCount_t763371255>(marshaled.___GetComponentCount_9, _GetComponentCount_t763371255_il2cpp_TypeInfo_var));
	unmarshaled.set_GetComponentName_10(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetComponentName_t3462998887>(marshaled.___GetComponentName_10, _GetComponentName_t3462998887_il2cpp_TypeInfo_var));
	unmarshaled.set_GetComponentButtonMask_11(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetComponentButtonMask_t1474657094>(marshaled.___GetComponentButtonMask_11, _GetComponentButtonMask_t1474657094_il2cpp_TypeInfo_var));
	unmarshaled.set_GetComponentRenderModelName_12(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetComponentRenderModelName_t2860930600>(marshaled.___GetComponentRenderModelName_12, _GetComponentRenderModelName_t2860930600_il2cpp_TypeInfo_var));
	unmarshaled.set_GetComponentState_13(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetComponentState_t742926735>(marshaled.___GetComponentState_13, _GetComponentState_t742926735_il2cpp_TypeInfo_var));
	unmarshaled.set_RenderModelHasComponent_14(il2cpp_codegen_marshal_function_ptr_to_delegate<_RenderModelHasComponent_t1969881317>(marshaled.___RenderModelHasComponent_14, _RenderModelHasComponent_t1969881317_il2cpp_TypeInfo_var));
	unmarshaled.set_GetRenderModelThumbnailURL_15(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetRenderModelThumbnailURL_t3954674309>(marshaled.___GetRenderModelThumbnailURL_15, _GetRenderModelThumbnailURL_t3954674309_il2cpp_TypeInfo_var));
	unmarshaled.set_GetRenderModelOriginalPath_16(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetRenderModelOriginalPath_t4216085620>(marshaled.___GetRenderModelOriginalPath_16, _GetRenderModelOriginalPath_t4216085620_il2cpp_TypeInfo_var));
	unmarshaled.set_GetRenderModelErrorNameFromEnum_17(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetRenderModelErrorNameFromEnum_t298277168>(marshaled.___GetRenderModelErrorNameFromEnum_17, _GetRenderModelErrorNameFromEnum_t298277168_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: Valve.VR.IVRRenderModels
extern "C" void IVRRenderModels_t3420796425_marshal_pinvoke_cleanup(IVRRenderModels_t3420796425_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Valve.VR.IVRRenderModels
extern "C" void IVRRenderModels_t3420796425_marshal_com(const IVRRenderModels_t3420796425& unmarshaled, IVRRenderModels_t3420796425_marshaled_com& marshaled)
{
	marshaled.___LoadRenderModel_Async_0 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_LoadRenderModel_Async_0()));
	marshaled.___FreeRenderModel_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_FreeRenderModel_1()));
	marshaled.___LoadTexture_Async_2 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_LoadTexture_Async_2()));
	marshaled.___FreeTexture_3 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_FreeTexture_3()));
	marshaled.___LoadTextureD3D11_Async_4 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_LoadTextureD3D11_Async_4()));
	marshaled.___LoadIntoTextureD3D11_Async_5 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_LoadIntoTextureD3D11_Async_5()));
	marshaled.___FreeTextureD3D11_6 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_FreeTextureD3D11_6()));
	marshaled.___GetRenderModelName_7 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetRenderModelName_7()));
	marshaled.___GetRenderModelCount_8 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetRenderModelCount_8()));
	marshaled.___GetComponentCount_9 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetComponentCount_9()));
	marshaled.___GetComponentName_10 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetComponentName_10()));
	marshaled.___GetComponentButtonMask_11 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetComponentButtonMask_11()));
	marshaled.___GetComponentRenderModelName_12 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetComponentRenderModelName_12()));
	marshaled.___GetComponentState_13 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetComponentState_13()));
	marshaled.___RenderModelHasComponent_14 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_RenderModelHasComponent_14()));
	marshaled.___GetRenderModelThumbnailURL_15 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetRenderModelThumbnailURL_15()));
	marshaled.___GetRenderModelOriginalPath_16 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetRenderModelOriginalPath_16()));
	marshaled.___GetRenderModelErrorNameFromEnum_17 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetRenderModelErrorNameFromEnum_17()));
}
extern Il2CppClass* _LoadRenderModel_Async_t3622270247_il2cpp_TypeInfo_var;
extern Il2CppClass* _FreeRenderModel_t2139843464_il2cpp_TypeInfo_var;
extern Il2CppClass* _LoadTexture_Async_t1786536393_il2cpp_TypeInfo_var;
extern Il2CppClass* _FreeTexture_t4051202214_il2cpp_TypeInfo_var;
extern Il2CppClass* _LoadTextureD3D11_Async_t2681806282_il2cpp_TypeInfo_var;
extern Il2CppClass* _LoadIntoTextureD3D11_Async_t3518850916_il2cpp_TypeInfo_var;
extern Il2CppClass* _FreeTextureD3D11_t2385986941_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetRenderModelName_t4149685257_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetRenderModelCount_t3364784497_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetComponentCount_t763371255_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetComponentName_t3462998887_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetComponentButtonMask_t1474657094_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetComponentRenderModelName_t2860930600_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetComponentState_t742926735_il2cpp_TypeInfo_var;
extern Il2CppClass* _RenderModelHasComponent_t1969881317_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetRenderModelThumbnailURL_t3954674309_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetRenderModelOriginalPath_t4216085620_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetRenderModelErrorNameFromEnum_t298277168_il2cpp_TypeInfo_var;
extern const uint32_t IVRRenderModels_t3420796425_com_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void IVRRenderModels_t3420796425_marshal_com_back(const IVRRenderModels_t3420796425_marshaled_com& marshaled, IVRRenderModels_t3420796425& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVRRenderModels_t3420796425_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_LoadRenderModel_Async_0(il2cpp_codegen_marshal_function_ptr_to_delegate<_LoadRenderModel_Async_t3622270247>(marshaled.___LoadRenderModel_Async_0, _LoadRenderModel_Async_t3622270247_il2cpp_TypeInfo_var));
	unmarshaled.set_FreeRenderModel_1(il2cpp_codegen_marshal_function_ptr_to_delegate<_FreeRenderModel_t2139843464>(marshaled.___FreeRenderModel_1, _FreeRenderModel_t2139843464_il2cpp_TypeInfo_var));
	unmarshaled.set_LoadTexture_Async_2(il2cpp_codegen_marshal_function_ptr_to_delegate<_LoadTexture_Async_t1786536393>(marshaled.___LoadTexture_Async_2, _LoadTexture_Async_t1786536393_il2cpp_TypeInfo_var));
	unmarshaled.set_FreeTexture_3(il2cpp_codegen_marshal_function_ptr_to_delegate<_FreeTexture_t4051202214>(marshaled.___FreeTexture_3, _FreeTexture_t4051202214_il2cpp_TypeInfo_var));
	unmarshaled.set_LoadTextureD3D11_Async_4(il2cpp_codegen_marshal_function_ptr_to_delegate<_LoadTextureD3D11_Async_t2681806282>(marshaled.___LoadTextureD3D11_Async_4, _LoadTextureD3D11_Async_t2681806282_il2cpp_TypeInfo_var));
	unmarshaled.set_LoadIntoTextureD3D11_Async_5(il2cpp_codegen_marshal_function_ptr_to_delegate<_LoadIntoTextureD3D11_Async_t3518850916>(marshaled.___LoadIntoTextureD3D11_Async_5, _LoadIntoTextureD3D11_Async_t3518850916_il2cpp_TypeInfo_var));
	unmarshaled.set_FreeTextureD3D11_6(il2cpp_codegen_marshal_function_ptr_to_delegate<_FreeTextureD3D11_t2385986941>(marshaled.___FreeTextureD3D11_6, _FreeTextureD3D11_t2385986941_il2cpp_TypeInfo_var));
	unmarshaled.set_GetRenderModelName_7(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetRenderModelName_t4149685257>(marshaled.___GetRenderModelName_7, _GetRenderModelName_t4149685257_il2cpp_TypeInfo_var));
	unmarshaled.set_GetRenderModelCount_8(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetRenderModelCount_t3364784497>(marshaled.___GetRenderModelCount_8, _GetRenderModelCount_t3364784497_il2cpp_TypeInfo_var));
	unmarshaled.set_GetComponentCount_9(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetComponentCount_t763371255>(marshaled.___GetComponentCount_9, _GetComponentCount_t763371255_il2cpp_TypeInfo_var));
	unmarshaled.set_GetComponentName_10(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetComponentName_t3462998887>(marshaled.___GetComponentName_10, _GetComponentName_t3462998887_il2cpp_TypeInfo_var));
	unmarshaled.set_GetComponentButtonMask_11(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetComponentButtonMask_t1474657094>(marshaled.___GetComponentButtonMask_11, _GetComponentButtonMask_t1474657094_il2cpp_TypeInfo_var));
	unmarshaled.set_GetComponentRenderModelName_12(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetComponentRenderModelName_t2860930600>(marshaled.___GetComponentRenderModelName_12, _GetComponentRenderModelName_t2860930600_il2cpp_TypeInfo_var));
	unmarshaled.set_GetComponentState_13(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetComponentState_t742926735>(marshaled.___GetComponentState_13, _GetComponentState_t742926735_il2cpp_TypeInfo_var));
	unmarshaled.set_RenderModelHasComponent_14(il2cpp_codegen_marshal_function_ptr_to_delegate<_RenderModelHasComponent_t1969881317>(marshaled.___RenderModelHasComponent_14, _RenderModelHasComponent_t1969881317_il2cpp_TypeInfo_var));
	unmarshaled.set_GetRenderModelThumbnailURL_15(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetRenderModelThumbnailURL_t3954674309>(marshaled.___GetRenderModelThumbnailURL_15, _GetRenderModelThumbnailURL_t3954674309_il2cpp_TypeInfo_var));
	unmarshaled.set_GetRenderModelOriginalPath_16(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetRenderModelOriginalPath_t4216085620>(marshaled.___GetRenderModelOriginalPath_16, _GetRenderModelOriginalPath_t4216085620_il2cpp_TypeInfo_var));
	unmarshaled.set_GetRenderModelErrorNameFromEnum_17(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetRenderModelErrorNameFromEnum_t298277168>(marshaled.___GetRenderModelErrorNameFromEnum_17, _GetRenderModelErrorNameFromEnum_t298277168_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: Valve.VR.IVRRenderModels
extern "C" void IVRRenderModels_t3420796425_marshal_com_cleanup(IVRRenderModels_t3420796425_marshaled_com& marshaled)
{
}
// System.Void Valve.VR.IVRRenderModels/_FreeRenderModel::.ctor(System.Object,System.IntPtr)
extern "C"  void _FreeRenderModel__ctor_m3343122049 (_FreeRenderModel_t2139843464 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRRenderModels/_FreeRenderModel::Invoke(System.IntPtr)
extern "C"  void _FreeRenderModel_Invoke_m1132817099 (_FreeRenderModel_t2139843464 * __this, IntPtr_t ___pRenderModel0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_FreeRenderModel_Invoke_m1132817099((_FreeRenderModel_t2139843464 *)__this->get_prev_9(),___pRenderModel0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___pRenderModel0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pRenderModel0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___pRenderModel0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pRenderModel0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__FreeRenderModel_t2139843464 (_FreeRenderModel_t2139843464 * __this, IntPtr_t ___pRenderModel0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___pRenderModel0).get_m_value_0()));

}
// System.IAsyncResult Valve.VR.IVRRenderModels/_FreeRenderModel::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t _FreeRenderModel_BeginInvoke_m1880110598_MetadataUsageId;
extern "C"  Il2CppObject * _FreeRenderModel_BeginInvoke_m1880110598 (_FreeRenderModel_t2139843464 * __this, IntPtr_t ___pRenderModel0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_FreeRenderModel_BeginInvoke_m1880110598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___pRenderModel0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Valve.VR.IVRRenderModels/_FreeRenderModel::EndInvoke(System.IAsyncResult)
extern "C"  void _FreeRenderModel_EndInvoke_m1525062483 (_FreeRenderModel_t2139843464 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Valve.VR.IVRRenderModels/_FreeTexture::.ctor(System.Object,System.IntPtr)
extern "C"  void _FreeTexture__ctor_m542112547 (_FreeTexture_t4051202214 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRRenderModels/_FreeTexture::Invoke(System.IntPtr)
extern "C"  void _FreeTexture_Invoke_m28621065 (_FreeTexture_t4051202214 * __this, IntPtr_t ___pTexture0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_FreeTexture_Invoke_m28621065((_FreeTexture_t4051202214 *)__this->get_prev_9(),___pTexture0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___pTexture0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pTexture0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___pTexture0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pTexture0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__FreeTexture_t4051202214 (_FreeTexture_t4051202214 * __this, IntPtr_t ___pTexture0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___pTexture0).get_m_value_0()));

}
// System.IAsyncResult Valve.VR.IVRRenderModels/_FreeTexture::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t _FreeTexture_BeginInvoke_m3398534740_MetadataUsageId;
extern "C"  Il2CppObject * _FreeTexture_BeginInvoke_m3398534740 (_FreeTexture_t4051202214 * __this, IntPtr_t ___pTexture0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_FreeTexture_BeginInvoke_m3398534740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___pTexture0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Valve.VR.IVRRenderModels/_FreeTexture::EndInvoke(System.IAsyncResult)
extern "C"  void _FreeTexture_EndInvoke_m2226226337 (_FreeTexture_t4051202214 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Valve.VR.IVRRenderModels/_FreeTextureD3D11::.ctor(System.Object,System.IntPtr)
extern "C"  void _FreeTextureD3D11__ctor_m916203686 (_FreeTextureD3D11_t2385986941 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRRenderModels/_FreeTextureD3D11::Invoke(System.IntPtr)
extern "C"  void _FreeTextureD3D11_Invoke_m3182118330 (_FreeTextureD3D11_t2385986941 * __this, IntPtr_t ___pD3D11Texture2D0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_FreeTextureD3D11_Invoke_m3182118330((_FreeTextureD3D11_t2385986941 *)__this->get_prev_9(),___pD3D11Texture2D0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___pD3D11Texture2D0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pD3D11Texture2D0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___pD3D11Texture2D0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pD3D11Texture2D0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__FreeTextureD3D11_t2385986941 (_FreeTextureD3D11_t2385986941 * __this, IntPtr_t ___pD3D11Texture2D0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___pD3D11Texture2D0).get_m_value_0()));

}
// System.IAsyncResult Valve.VR.IVRRenderModels/_FreeTextureD3D11::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t _FreeTextureD3D11_BeginInvoke_m3366071529_MetadataUsageId;
extern "C"  Il2CppObject * _FreeTextureD3D11_BeginInvoke_m3366071529 (_FreeTextureD3D11_t2385986941 * __this, IntPtr_t ___pD3D11Texture2D0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_FreeTextureD3D11_BeginInvoke_m3366071529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___pD3D11Texture2D0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Valve.VR.IVRRenderModels/_FreeTextureD3D11::EndInvoke(System.IAsyncResult)
extern "C"  void _FreeTextureD3D11_EndInvoke_m2630650268 (_FreeTextureD3D11_t2385986941 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Valve.VR.IVRRenderModels/_GetComponentButtonMask::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetComponentButtonMask__ctor_m1294274575 (_GetComponentButtonMask_t1474657094 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UInt64 Valve.VR.IVRRenderModels/_GetComponentButtonMask::Invoke(System.String,System.String)
extern "C"  uint64_t _GetComponentButtonMask_Invoke_m2851395467 (_GetComponentButtonMask_t1474657094 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetComponentButtonMask_Invoke_m2851395467((_GetComponentButtonMask_t1474657094 *)__this->get_prev_9(),___pchRenderModelName0, ___pchComponentName1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint64_t (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchRenderModelName0, ___pchComponentName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef uint64_t (*FunctionPointerType) (void* __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchRenderModelName0, ___pchComponentName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint64_t (*FunctionPointerType) (void* __this, String_t* ___pchComponentName1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchRenderModelName0, ___pchComponentName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  uint64_t DelegatePInvokeWrapper__GetComponentButtonMask_t1474657094 (_GetComponentButtonMask_t1474657094 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, const MethodInfo* method)
{
	typedef uint64_t (STDCALL *PInvokeFunc)(char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchRenderModelName0' to native representation
	char* ____pchRenderModelName0_marshaled = NULL;
	____pchRenderModelName0_marshaled = il2cpp_codegen_marshal_string(___pchRenderModelName0);

	// Marshaling of parameter '___pchComponentName1' to native representation
	char* ____pchComponentName1_marshaled = NULL;
	____pchComponentName1_marshaled = il2cpp_codegen_marshal_string(___pchComponentName1);

	// Native function invocation
	uint64_t returnValue = il2cppPInvokeFunc(____pchRenderModelName0_marshaled, ____pchComponentName1_marshaled);

	// Marshaling cleanup of parameter '___pchRenderModelName0' native representation
	il2cpp_codegen_marshal_free(____pchRenderModelName0_marshaled);
	____pchRenderModelName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchComponentName1' native representation
	il2cpp_codegen_marshal_free(____pchComponentName1_marshaled);
	____pchComponentName1_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetComponentButtonMask::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetComponentButtonMask_BeginInvoke_m4122093192 (_GetComponentButtonMask_t1474657094 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___pchRenderModelName0;
	__d_args[1] = ___pchComponentName1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.UInt64 Valve.VR.IVRRenderModels/_GetComponentButtonMask::EndInvoke(System.IAsyncResult)
extern "C"  uint64_t _GetComponentButtonMask_EndInvoke_m313207949 (_GetComponentButtonMask_t1474657094 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRRenderModels/_GetComponentCount::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetComponentCount__ctor_m4292303028 (_GetComponentCount_t763371255 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UInt32 Valve.VR.IVRRenderModels/_GetComponentCount::Invoke(System.String)
extern "C"  uint32_t _GetComponentCount_Invoke_m2742369791 (_GetComponentCount_t763371255 * __this, String_t* ___pchRenderModelName0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetComponentCount_Invoke_m2742369791((_GetComponentCount_t763371255 *)__this->get_prev_9(),___pchRenderModelName0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchRenderModelName0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchRenderModelName0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, String_t* ___pchRenderModelName0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchRenderModelName0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchRenderModelName0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  uint32_t DelegatePInvokeWrapper__GetComponentCount_t763371255 (_GetComponentCount_t763371255 * __this, String_t* ___pchRenderModelName0, const MethodInfo* method)
{
	typedef uint32_t (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchRenderModelName0' to native representation
	char* ____pchRenderModelName0_marshaled = NULL;
	____pchRenderModelName0_marshaled = il2cpp_codegen_marshal_string(___pchRenderModelName0);

	// Native function invocation
	uint32_t returnValue = il2cppPInvokeFunc(____pchRenderModelName0_marshaled);

	// Marshaling cleanup of parameter '___pchRenderModelName0' native representation
	il2cpp_codegen_marshal_free(____pchRenderModelName0_marshaled);
	____pchRenderModelName0_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetComponentCount::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetComponentCount_BeginInvoke_m152053003 (_GetComponentCount_t763371255 * __this, String_t* ___pchRenderModelName0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___pchRenderModelName0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.UInt32 Valve.VR.IVRRenderModels/_GetComponentCount::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetComponentCount_EndInvoke_m3953108151 (_GetComponentCount_t763371255 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRRenderModels/_GetComponentName::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetComponentName__ctor_m3486105392 (_GetComponentName_t3462998887 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UInt32 Valve.VR.IVRRenderModels/_GetComponentName::Invoke(System.String,System.UInt32,System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t _GetComponentName_Invoke_m2092916931 (_GetComponentName_t3462998887 * __this, String_t* ___pchRenderModelName0, uint32_t ___unComponentIndex1, StringBuilder_t1221177846 * ___pchComponentName2, uint32_t ___unComponentNameLen3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetComponentName_Invoke_m2092916931((_GetComponentName_t3462998887 *)__this->get_prev_9(),___pchRenderModelName0, ___unComponentIndex1, ___pchComponentName2, ___unComponentNameLen3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchRenderModelName0, uint32_t ___unComponentIndex1, StringBuilder_t1221177846 * ___pchComponentName2, uint32_t ___unComponentNameLen3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchRenderModelName0, ___unComponentIndex1, ___pchComponentName2, ___unComponentNameLen3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, String_t* ___pchRenderModelName0, uint32_t ___unComponentIndex1, StringBuilder_t1221177846 * ___pchComponentName2, uint32_t ___unComponentNameLen3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchRenderModelName0, ___unComponentIndex1, ___pchComponentName2, ___unComponentNameLen3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, uint32_t ___unComponentIndex1, StringBuilder_t1221177846 * ___pchComponentName2, uint32_t ___unComponentNameLen3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchRenderModelName0, ___unComponentIndex1, ___pchComponentName2, ___unComponentNameLen3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  uint32_t DelegatePInvokeWrapper__GetComponentName_t3462998887 (_GetComponentName_t3462998887 * __this, String_t* ___pchRenderModelName0, uint32_t ___unComponentIndex1, StringBuilder_t1221177846 * ___pchComponentName2, uint32_t ___unComponentNameLen3, const MethodInfo* method)
{
	typedef uint32_t (STDCALL *PInvokeFunc)(char*, uint32_t, char*, uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchRenderModelName0' to native representation
	char* ____pchRenderModelName0_marshaled = NULL;
	____pchRenderModelName0_marshaled = il2cpp_codegen_marshal_string(___pchRenderModelName0);

	// Marshaling of parameter '___pchComponentName2' to native representation
	char* ____pchComponentName2_marshaled = NULL;
	____pchComponentName2_marshaled = il2cpp_codegen_marshal_string_builder(___pchComponentName2);

	// Native function invocation
	uint32_t returnValue = il2cppPInvokeFunc(____pchRenderModelName0_marshaled, ___unComponentIndex1, ____pchComponentName2_marshaled, ___unComponentNameLen3);

	// Marshaling cleanup of parameter '___pchRenderModelName0' native representation
	il2cpp_codegen_marshal_free(____pchRenderModelName0_marshaled);
	____pchRenderModelName0_marshaled = NULL;

	// Marshaling of parameter '___pchComponentName2' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___pchComponentName2, ____pchComponentName2_marshaled);

	// Marshaling cleanup of parameter '___pchComponentName2' native representation
	il2cpp_codegen_marshal_free(____pchComponentName2_marshaled);
	____pchComponentName2_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetComponentName::BeginInvoke(System.String,System.UInt32,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _GetComponentName_BeginInvoke_m1213578803_MetadataUsageId;
extern "C"  Il2CppObject * _GetComponentName_BeginInvoke_m1213578803 (_GetComponentName_t3462998887 * __this, String_t* ___pchRenderModelName0, uint32_t ___unComponentIndex1, StringBuilder_t1221177846 * ___pchComponentName2, uint32_t ___unComponentNameLen3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetComponentName_BeginInvoke_m1213578803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___pchRenderModelName0;
	__d_args[1] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unComponentIndex1);
	__d_args[2] = ___pchComponentName2;
	__d_args[3] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unComponentNameLen3);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.UInt32 Valve.VR.IVRRenderModels/_GetComponentName::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetComponentName_EndInvoke_m2526683259 (_GetComponentName_t3462998887 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRRenderModels/_GetComponentRenderModelName::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetComponentRenderModelName__ctor_m1767760407 (_GetComponentRenderModelName_t2860930600 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UInt32 Valve.VR.IVRRenderModels/_GetComponentRenderModelName::Invoke(System.String,System.String,System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t _GetComponentRenderModelName_Invoke_m1715179606 (_GetComponentRenderModelName_t2860930600 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, StringBuilder_t1221177846 * ___pchComponentRenderModelName2, uint32_t ___unComponentRenderModelNameLen3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetComponentRenderModelName_Invoke_m1715179606((_GetComponentRenderModelName_t2860930600 *)__this->get_prev_9(),___pchRenderModelName0, ___pchComponentName1, ___pchComponentRenderModelName2, ___unComponentRenderModelNameLen3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, StringBuilder_t1221177846 * ___pchComponentRenderModelName2, uint32_t ___unComponentRenderModelNameLen3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchRenderModelName0, ___pchComponentName1, ___pchComponentRenderModelName2, ___unComponentRenderModelNameLen3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, StringBuilder_t1221177846 * ___pchComponentRenderModelName2, uint32_t ___unComponentRenderModelNameLen3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchRenderModelName0, ___pchComponentName1, ___pchComponentRenderModelName2, ___unComponentRenderModelNameLen3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, String_t* ___pchComponentName1, StringBuilder_t1221177846 * ___pchComponentRenderModelName2, uint32_t ___unComponentRenderModelNameLen3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchRenderModelName0, ___pchComponentName1, ___pchComponentRenderModelName2, ___unComponentRenderModelNameLen3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  uint32_t DelegatePInvokeWrapper__GetComponentRenderModelName_t2860930600 (_GetComponentRenderModelName_t2860930600 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, StringBuilder_t1221177846 * ___pchComponentRenderModelName2, uint32_t ___unComponentRenderModelNameLen3, const MethodInfo* method)
{
	typedef uint32_t (STDCALL *PInvokeFunc)(char*, char*, char*, uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchRenderModelName0' to native representation
	char* ____pchRenderModelName0_marshaled = NULL;
	____pchRenderModelName0_marshaled = il2cpp_codegen_marshal_string(___pchRenderModelName0);

	// Marshaling of parameter '___pchComponentName1' to native representation
	char* ____pchComponentName1_marshaled = NULL;
	____pchComponentName1_marshaled = il2cpp_codegen_marshal_string(___pchComponentName1);

	// Marshaling of parameter '___pchComponentRenderModelName2' to native representation
	char* ____pchComponentRenderModelName2_marshaled = NULL;
	____pchComponentRenderModelName2_marshaled = il2cpp_codegen_marshal_string_builder(___pchComponentRenderModelName2);

	// Native function invocation
	uint32_t returnValue = il2cppPInvokeFunc(____pchRenderModelName0_marshaled, ____pchComponentName1_marshaled, ____pchComponentRenderModelName2_marshaled, ___unComponentRenderModelNameLen3);

	// Marshaling cleanup of parameter '___pchRenderModelName0' native representation
	il2cpp_codegen_marshal_free(____pchRenderModelName0_marshaled);
	____pchRenderModelName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchComponentName1' native representation
	il2cpp_codegen_marshal_free(____pchComponentName1_marshaled);
	____pchComponentName1_marshaled = NULL;

	// Marshaling of parameter '___pchComponentRenderModelName2' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___pchComponentRenderModelName2, ____pchComponentRenderModelName2_marshaled);

	// Marshaling cleanup of parameter '___pchComponentRenderModelName2' native representation
	il2cpp_codegen_marshal_free(____pchComponentRenderModelName2_marshaled);
	____pchComponentRenderModelName2_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetComponentRenderModelName::BeginInvoke(System.String,System.String,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _GetComponentRenderModelName_BeginInvoke_m3352067234_MetadataUsageId;
extern "C"  Il2CppObject * _GetComponentRenderModelName_BeginInvoke_m3352067234 (_GetComponentRenderModelName_t2860930600 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, StringBuilder_t1221177846 * ___pchComponentRenderModelName2, uint32_t ___unComponentRenderModelNameLen3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetComponentRenderModelName_BeginInvoke_m3352067234_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___pchRenderModelName0;
	__d_args[1] = ___pchComponentName1;
	__d_args[2] = ___pchComponentRenderModelName2;
	__d_args[3] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unComponentRenderModelNameLen3);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.UInt32 Valve.VR.IVRRenderModels/_GetComponentRenderModelName::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetComponentRenderModelName_EndInvoke_m78849076 (_GetComponentRenderModelName_t2860930600 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRRenderModels/_GetComponentState::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetComponentState__ctor_m3689130818 (_GetComponentState_t742926735 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean Valve.VR.IVRRenderModels/_GetComponentState::Invoke(System.String,System.String,Valve.VR.VRControllerState_t&,Valve.VR.RenderModel_ControllerMode_State_t&,Valve.VR.RenderModel_ComponentState_t&)
extern "C"  bool _GetComponentState_Invoke_m3625501015 (_GetComponentState_t742926735 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, VRControllerState_t_t2504874220 * ___pControllerState2, RenderModel_ControllerMode_State_t_t1298199406 * ___pState3, RenderModel_ComponentState_t_t2032012879 * ___pComponentState4, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetComponentState_Invoke_m3625501015((_GetComponentState_t742926735 *)__this->get_prev_9(),___pchRenderModelName0, ___pchComponentName1, ___pControllerState2, ___pState3, ___pComponentState4, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, VRControllerState_t_t2504874220 * ___pControllerState2, RenderModel_ControllerMode_State_t_t1298199406 * ___pState3, RenderModel_ComponentState_t_t2032012879 * ___pComponentState4, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchRenderModelName0, ___pchComponentName1, ___pControllerState2, ___pState3, ___pComponentState4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, VRControllerState_t_t2504874220 * ___pControllerState2, RenderModel_ControllerMode_State_t_t1298199406 * ___pState3, RenderModel_ComponentState_t_t2032012879 * ___pComponentState4, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchRenderModelName0, ___pchComponentName1, ___pControllerState2, ___pState3, ___pComponentState4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, String_t* ___pchComponentName1, VRControllerState_t_t2504874220 * ___pControllerState2, RenderModel_ControllerMode_State_t_t1298199406 * ___pState3, RenderModel_ComponentState_t_t2032012879 * ___pComponentState4, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchRenderModelName0, ___pchComponentName1, ___pControllerState2, ___pState3, ___pComponentState4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  bool DelegatePInvokeWrapper__GetComponentState_t742926735 (_GetComponentState_t742926735 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, VRControllerState_t_t2504874220 * ___pControllerState2, RenderModel_ControllerMode_State_t_t1298199406 * ___pState3, RenderModel_ComponentState_t_t2032012879 * ___pComponentState4, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(char*, char*, VRControllerState_t_t2504874220 *, RenderModel_ControllerMode_State_t_t1298199406 *, RenderModel_ComponentState_t_t2032012879 *);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchRenderModelName0' to native representation
	char* ____pchRenderModelName0_marshaled = NULL;
	____pchRenderModelName0_marshaled = il2cpp_codegen_marshal_string(___pchRenderModelName0);

	// Marshaling of parameter '___pchComponentName1' to native representation
	char* ____pchComponentName1_marshaled = NULL;
	____pchComponentName1_marshaled = il2cpp_codegen_marshal_string(___pchComponentName1);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____pchRenderModelName0_marshaled, ____pchComponentName1_marshaled, ___pControllerState2, ___pState3, ___pComponentState4);

	// Marshaling cleanup of parameter '___pchRenderModelName0' native representation
	il2cpp_codegen_marshal_free(____pchRenderModelName0_marshaled);
	____pchRenderModelName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchComponentName1' native representation
	il2cpp_codegen_marshal_free(____pchComponentName1_marshaled);
	____pchComponentName1_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetComponentState::BeginInvoke(System.String,System.String,Valve.VR.VRControllerState_t&,Valve.VR.RenderModel_ControllerMode_State_t&,Valve.VR.RenderModel_ComponentState_t&,System.AsyncCallback,System.Object)
extern Il2CppClass* VRControllerState_t_t2504874220_il2cpp_TypeInfo_var;
extern Il2CppClass* RenderModel_ControllerMode_State_t_t1298199406_il2cpp_TypeInfo_var;
extern Il2CppClass* RenderModel_ComponentState_t_t2032012879_il2cpp_TypeInfo_var;
extern const uint32_t _GetComponentState_BeginInvoke_m3312649562_MetadataUsageId;
extern "C"  Il2CppObject * _GetComponentState_BeginInvoke_m3312649562 (_GetComponentState_t742926735 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, VRControllerState_t_t2504874220 * ___pControllerState2, RenderModel_ControllerMode_State_t_t1298199406 * ___pState3, RenderModel_ComponentState_t_t2032012879 * ___pComponentState4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetComponentState_BeginInvoke_m3312649562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = ___pchRenderModelName0;
	__d_args[1] = ___pchComponentName1;
	__d_args[2] = Box(VRControllerState_t_t2504874220_il2cpp_TypeInfo_var, &(*___pControllerState2));
	__d_args[3] = Box(RenderModel_ControllerMode_State_t_t1298199406_il2cpp_TypeInfo_var, &(*___pState3));
	__d_args[4] = Box(RenderModel_ComponentState_t_t2032012879_il2cpp_TypeInfo_var, &(*___pComponentState4));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback5, (Il2CppObject*)___object6);
}
// System.Boolean Valve.VR.IVRRenderModels/_GetComponentState::EndInvoke(Valve.VR.VRControllerState_t&,Valve.VR.RenderModel_ControllerMode_State_t&,Valve.VR.RenderModel_ComponentState_t&,System.IAsyncResult)
extern "C"  bool _GetComponentState_EndInvoke_m206294501 (_GetComponentState_t742926735 * __this, VRControllerState_t_t2504874220 * ___pControllerState0, RenderModel_ControllerMode_State_t_t1298199406 * ___pState1, RenderModel_ComponentState_t_t2032012879 * ___pComponentState2, Il2CppObject * ___result3, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pControllerState0,
	___pState1,
	___pComponentState2,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result3, ___out_args);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRRenderModels/_GetRenderModelCount::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetRenderModelCount__ctor_m3843656478 (_GetRenderModelCount_t3364784497 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelCount::Invoke()
extern "C"  uint32_t _GetRenderModelCount_Invoke_m2026972611 (_GetRenderModelCount_t3364784497 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetRenderModelCount_Invoke_m2026972611((_GetRenderModelCount_t3364784497 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  uint32_t DelegatePInvokeWrapper__GetRenderModelCount_t3364784497 (_GetRenderModelCount_t3364784497 * __this, const MethodInfo* method)
{
	typedef uint32_t (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	uint32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetRenderModelCount::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetRenderModelCount_BeginInvoke_m2693889567 (_GetRenderModelCount_t3364784497 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelCount::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetRenderModelCount_EndInvoke_m2333466433 (_GetRenderModelCount_t3364784497 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRRenderModels/_GetRenderModelErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetRenderModelErrorNameFromEnum__ctor_m3211935049 (_GetRenderModelErrorNameFromEnum_t298277168 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.IntPtr Valve.VR.IVRRenderModels/_GetRenderModelErrorNameFromEnum::Invoke(Valve.VR.EVRRenderModelError)
extern "C"  IntPtr_t _GetRenderModelErrorNameFromEnum_Invoke_m3481395996 (_GetRenderModelErrorNameFromEnum_t298277168 * __this, int32_t ___error0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetRenderModelErrorNameFromEnum_Invoke_m3481395996((_GetRenderModelErrorNameFromEnum_t298277168 *)__this->get_prev_9(),___error0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef IntPtr_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___error0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___error0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef IntPtr_t (*FunctionPointerType) (void* __this, int32_t ___error0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___error0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  IntPtr_t DelegatePInvokeWrapper__GetRenderModelErrorNameFromEnum_t298277168 (_GetRenderModelErrorNameFromEnum_t298277168 * __this, int32_t ___error0, const MethodInfo* method)
{
	typedef intptr_t (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc(___error0);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetRenderModelErrorNameFromEnum::BeginInvoke(Valve.VR.EVRRenderModelError,System.AsyncCallback,System.Object)
extern Il2CppClass* EVRRenderModelError_t21703732_il2cpp_TypeInfo_var;
extern const uint32_t _GetRenderModelErrorNameFromEnum_BeginInvoke_m3692303504_MetadataUsageId;
extern "C"  Il2CppObject * _GetRenderModelErrorNameFromEnum_BeginInvoke_m3692303504 (_GetRenderModelErrorNameFromEnum_t298277168 * __this, int32_t ___error0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetRenderModelErrorNameFromEnum_BeginInvoke_m3692303504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(EVRRenderModelError_t21703732_il2cpp_TypeInfo_var, &___error0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.IntPtr Valve.VR.IVRRenderModels/_GetRenderModelErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetRenderModelErrorNameFromEnum_EndInvoke_m1717712356 (_GetRenderModelErrorNameFromEnum_t298277168 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(IntPtr_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRRenderModels/_GetRenderModelName::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetRenderModelName__ctor_m1619642450 (_GetRenderModelName_t4149685257 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelName::Invoke(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t _GetRenderModelName_Invoke_m858619975 (_GetRenderModelName_t4149685257 * __this, uint32_t ___unRenderModelIndex0, StringBuilder_t1221177846 * ___pchRenderModelName1, uint32_t ___unRenderModelNameLen2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetRenderModelName_Invoke_m858619975((_GetRenderModelName_t4149685257 *)__this->get_prev_9(),___unRenderModelIndex0, ___pchRenderModelName1, ___unRenderModelNameLen2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___unRenderModelIndex0, StringBuilder_t1221177846 * ___pchRenderModelName1, uint32_t ___unRenderModelNameLen2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___unRenderModelIndex0, ___pchRenderModelName1, ___unRenderModelNameLen2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, uint32_t ___unRenderModelIndex0, StringBuilder_t1221177846 * ___pchRenderModelName1, uint32_t ___unRenderModelNameLen2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___unRenderModelIndex0, ___pchRenderModelName1, ___unRenderModelNameLen2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  uint32_t DelegatePInvokeWrapper__GetRenderModelName_t4149685257 (_GetRenderModelName_t4149685257 * __this, uint32_t ___unRenderModelIndex0, StringBuilder_t1221177846 * ___pchRenderModelName1, uint32_t ___unRenderModelNameLen2, const MethodInfo* method)
{
	typedef uint32_t (STDCALL *PInvokeFunc)(uint32_t, char*, uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchRenderModelName1' to native representation
	char* ____pchRenderModelName1_marshaled = NULL;
	____pchRenderModelName1_marshaled = il2cpp_codegen_marshal_string_builder(___pchRenderModelName1);

	// Native function invocation
	uint32_t returnValue = il2cppPInvokeFunc(___unRenderModelIndex0, ____pchRenderModelName1_marshaled, ___unRenderModelNameLen2);

	// Marshaling of parameter '___pchRenderModelName1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___pchRenderModelName1, ____pchRenderModelName1_marshaled);

	// Marshaling cleanup of parameter '___pchRenderModelName1' native representation
	il2cpp_codegen_marshal_free(____pchRenderModelName1_marshaled);
	____pchRenderModelName1_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetRenderModelName::BeginInvoke(System.UInt32,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _GetRenderModelName_BeginInvoke_m2011707583_MetadataUsageId;
extern "C"  Il2CppObject * _GetRenderModelName_BeginInvoke_m2011707583 (_GetRenderModelName_t4149685257 * __this, uint32_t ___unRenderModelIndex0, StringBuilder_t1221177846 * ___pchRenderModelName1, uint32_t ___unRenderModelNameLen2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetRenderModelName_BeginInvoke_m2011707583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unRenderModelIndex0);
	__d_args[1] = ___pchRenderModelName1;
	__d_args[2] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unRenderModelNameLen2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelName::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetRenderModelName_EndInvoke_m475684905 (_GetRenderModelName_t4149685257 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRRenderModels/_GetRenderModelOriginalPath::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetRenderModelOriginalPath__ctor_m4287276727 (_GetRenderModelOriginalPath_t4216085620 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelOriginalPath::Invoke(System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRRenderModelError&)
extern "C"  uint32_t _GetRenderModelOriginalPath_Invoke_m218742038 (_GetRenderModelOriginalPath_t4216085620 * __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchOriginalPath1, uint32_t ___unOriginalPathLen2, int32_t* ___peError3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetRenderModelOriginalPath_Invoke_m218742038((_GetRenderModelOriginalPath_t4216085620 *)__this->get_prev_9(),___pchRenderModelName0, ___pchOriginalPath1, ___unOriginalPathLen2, ___peError3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchOriginalPath1, uint32_t ___unOriginalPathLen2, int32_t* ___peError3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchRenderModelName0, ___pchOriginalPath1, ___unOriginalPathLen2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchOriginalPath1, uint32_t ___unOriginalPathLen2, int32_t* ___peError3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchRenderModelName0, ___pchOriginalPath1, ___unOriginalPathLen2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, StringBuilder_t1221177846 * ___pchOriginalPath1, uint32_t ___unOriginalPathLen2, int32_t* ___peError3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchRenderModelName0, ___pchOriginalPath1, ___unOriginalPathLen2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  uint32_t DelegatePInvokeWrapper__GetRenderModelOriginalPath_t4216085620 (_GetRenderModelOriginalPath_t4216085620 * __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchOriginalPath1, uint32_t ___unOriginalPathLen2, int32_t* ___peError3, const MethodInfo* method)
{
	typedef uint32_t (STDCALL *PInvokeFunc)(char*, char*, uint32_t, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchRenderModelName0' to native representation
	char* ____pchRenderModelName0_marshaled = NULL;
	____pchRenderModelName0_marshaled = il2cpp_codegen_marshal_string(___pchRenderModelName0);

	// Marshaling of parameter '___pchOriginalPath1' to native representation
	char* ____pchOriginalPath1_marshaled = NULL;
	____pchOriginalPath1_marshaled = il2cpp_codegen_marshal_string_builder(___pchOriginalPath1);

	// Native function invocation
	uint32_t returnValue = il2cppPInvokeFunc(____pchRenderModelName0_marshaled, ____pchOriginalPath1_marshaled, ___unOriginalPathLen2, ___peError3);

	// Marshaling cleanup of parameter '___pchRenderModelName0' native representation
	il2cpp_codegen_marshal_free(____pchRenderModelName0_marshaled);
	____pchRenderModelName0_marshaled = NULL;

	// Marshaling of parameter '___pchOriginalPath1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___pchOriginalPath1, ____pchOriginalPath1_marshaled);

	// Marshaling cleanup of parameter '___pchOriginalPath1' native representation
	il2cpp_codegen_marshal_free(____pchOriginalPath1_marshaled);
	____pchOriginalPath1_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetRenderModelOriginalPath::BeginInvoke(System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRRenderModelError&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* EVRRenderModelError_t21703732_il2cpp_TypeInfo_var;
extern const uint32_t _GetRenderModelOriginalPath_BeginInvoke_m3084562884_MetadataUsageId;
extern "C"  Il2CppObject * _GetRenderModelOriginalPath_BeginInvoke_m3084562884 (_GetRenderModelOriginalPath_t4216085620 * __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchOriginalPath1, uint32_t ___unOriginalPathLen2, int32_t* ___peError3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetRenderModelOriginalPath_BeginInvoke_m3084562884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___pchRenderModelName0;
	__d_args[1] = ___pchOriginalPath1;
	__d_args[2] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unOriginalPathLen2);
	__d_args[3] = Box(EVRRenderModelError_t21703732_il2cpp_TypeInfo_var, &(*___peError3));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelOriginalPath::EndInvoke(Valve.VR.EVRRenderModelError&,System.IAsyncResult)
extern "C"  uint32_t _GetRenderModelOriginalPath_EndInvoke_m215431012 (_GetRenderModelOriginalPath_t4216085620 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___peError0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRRenderModels/_GetRenderModelThumbnailURL::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetRenderModelThumbnailURL__ctor_m4077953000 (_GetRenderModelThumbnailURL_t3954674309 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelThumbnailURL::Invoke(System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRRenderModelError&)
extern "C"  uint32_t _GetRenderModelThumbnailURL_Invoke_m560263279 (_GetRenderModelThumbnailURL_t3954674309 * __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchThumbnailURL1, uint32_t ___unThumbnailURLLen2, int32_t* ___peError3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetRenderModelThumbnailURL_Invoke_m560263279((_GetRenderModelThumbnailURL_t3954674309 *)__this->get_prev_9(),___pchRenderModelName0, ___pchThumbnailURL1, ___unThumbnailURLLen2, ___peError3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchThumbnailURL1, uint32_t ___unThumbnailURLLen2, int32_t* ___peError3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchRenderModelName0, ___pchThumbnailURL1, ___unThumbnailURLLen2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchThumbnailURL1, uint32_t ___unThumbnailURLLen2, int32_t* ___peError3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchRenderModelName0, ___pchThumbnailURL1, ___unThumbnailURLLen2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, StringBuilder_t1221177846 * ___pchThumbnailURL1, uint32_t ___unThumbnailURLLen2, int32_t* ___peError3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchRenderModelName0, ___pchThumbnailURL1, ___unThumbnailURLLen2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  uint32_t DelegatePInvokeWrapper__GetRenderModelThumbnailURL_t3954674309 (_GetRenderModelThumbnailURL_t3954674309 * __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchThumbnailURL1, uint32_t ___unThumbnailURLLen2, int32_t* ___peError3, const MethodInfo* method)
{
	typedef uint32_t (STDCALL *PInvokeFunc)(char*, char*, uint32_t, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchRenderModelName0' to native representation
	char* ____pchRenderModelName0_marshaled = NULL;
	____pchRenderModelName0_marshaled = il2cpp_codegen_marshal_string(___pchRenderModelName0);

	// Marshaling of parameter '___pchThumbnailURL1' to native representation
	char* ____pchThumbnailURL1_marshaled = NULL;
	____pchThumbnailURL1_marshaled = il2cpp_codegen_marshal_string_builder(___pchThumbnailURL1);

	// Native function invocation
	uint32_t returnValue = il2cppPInvokeFunc(____pchRenderModelName0_marshaled, ____pchThumbnailURL1_marshaled, ___unThumbnailURLLen2, ___peError3);

	// Marshaling cleanup of parameter '___pchRenderModelName0' native representation
	il2cpp_codegen_marshal_free(____pchRenderModelName0_marshaled);
	____pchRenderModelName0_marshaled = NULL;

	// Marshaling of parameter '___pchThumbnailURL1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___pchThumbnailURL1, ____pchThumbnailURL1_marshaled);

	// Marshaling cleanup of parameter '___pchThumbnailURL1' native representation
	il2cpp_codegen_marshal_free(____pchThumbnailURL1_marshaled);
	____pchThumbnailURL1_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetRenderModelThumbnailURL::BeginInvoke(System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRRenderModelError&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* EVRRenderModelError_t21703732_il2cpp_TypeInfo_var;
extern const uint32_t _GetRenderModelThumbnailURL_BeginInvoke_m4092992911_MetadataUsageId;
extern "C"  Il2CppObject * _GetRenderModelThumbnailURL_BeginInvoke_m4092992911 (_GetRenderModelThumbnailURL_t3954674309 * __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchThumbnailURL1, uint32_t ___unThumbnailURLLen2, int32_t* ___peError3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetRenderModelThumbnailURL_BeginInvoke_m4092992911_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___pchRenderModelName0;
	__d_args[1] = ___pchThumbnailURL1;
	__d_args[2] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unThumbnailURLLen2);
	__d_args[3] = Box(EVRRenderModelError_t21703732_il2cpp_TypeInfo_var, &(*___peError3));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelThumbnailURL::EndInvoke(Valve.VR.EVRRenderModelError&,System.IAsyncResult)
extern "C"  uint32_t _GetRenderModelThumbnailURL_EndInvoke_m1079584463 (_GetRenderModelThumbnailURL_t3954674309 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___peError0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRRenderModels/_LoadIntoTextureD3D11_Async::.ctor(System.Object,System.IntPtr)
extern "C"  void _LoadIntoTextureD3D11_Async__ctor_m2703113879 (_LoadIntoTextureD3D11_Async_t3518850916 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVRRenderModelError Valve.VR.IVRRenderModels/_LoadIntoTextureD3D11_Async::Invoke(System.Int32,System.IntPtr)
extern "C"  int32_t _LoadIntoTextureD3D11_Async_Invoke_m3511542015 (_LoadIntoTextureD3D11_Async_t3518850916 * __this, int32_t ___textureId0, IntPtr_t ___pDstTexture1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_LoadIntoTextureD3D11_Async_Invoke_m3511542015((_LoadIntoTextureD3D11_Async_t3518850916 *)__this->get_prev_9(),___textureId0, ___pDstTexture1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___textureId0, IntPtr_t ___pDstTexture1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___textureId0, ___pDstTexture1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___textureId0, IntPtr_t ___pDstTexture1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___textureId0, ___pDstTexture1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__LoadIntoTextureD3D11_Async_t3518850916 (_LoadIntoTextureD3D11_Async_t3518850916 * __this, int32_t ___textureId0, IntPtr_t ___pDstTexture1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(int32_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___textureId0, reinterpret_cast<intptr_t>((___pDstTexture1).get_m_value_0()));

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_LoadIntoTextureD3D11_Async::BeginInvoke(System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t _LoadIntoTextureD3D11_Async_BeginInvoke_m1346441687_MetadataUsageId;
extern "C"  Il2CppObject * _LoadIntoTextureD3D11_Async_BeginInvoke_m1346441687 (_LoadIntoTextureD3D11_Async_t3518850916 * __this, int32_t ___textureId0, IntPtr_t ___pDstTexture1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_LoadIntoTextureD3D11_Async_BeginInvoke_m1346441687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___textureId0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___pDstTexture1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVRRenderModelError Valve.VR.IVRRenderModels/_LoadIntoTextureD3D11_Async::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _LoadIntoTextureD3D11_Async_EndInvoke_m2327368908 (_LoadIntoTextureD3D11_Async_t3518850916 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRRenderModels/_LoadRenderModel_Async::.ctor(System.Object,System.IntPtr)
extern "C"  void _LoadRenderModel_Async__ctor_m1496280292 (_LoadRenderModel_Async_t3622270247 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVRRenderModelError Valve.VR.IVRRenderModels/_LoadRenderModel_Async::Invoke(System.String,System.IntPtr&)
extern "C"  int32_t _LoadRenderModel_Async_Invoke_m2277295415 (_LoadRenderModel_Async_t3622270247 * __this, String_t* ___pchRenderModelName0, IntPtr_t* ___ppRenderModel1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_LoadRenderModel_Async_Invoke_m2277295415((_LoadRenderModel_Async_t3622270247 *)__this->get_prev_9(),___pchRenderModelName0, ___ppRenderModel1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchRenderModelName0, IntPtr_t* ___ppRenderModel1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchRenderModelName0, ___ppRenderModel1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, String_t* ___pchRenderModelName0, IntPtr_t* ___ppRenderModel1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchRenderModelName0, ___ppRenderModel1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, IntPtr_t* ___ppRenderModel1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchRenderModelName0, ___ppRenderModel1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__LoadRenderModel_Async_t3622270247 (_LoadRenderModel_Async_t3622270247 * __this, String_t* ___pchRenderModelName0, IntPtr_t* ___ppRenderModel1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(char*, intptr_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchRenderModelName0' to native representation
	char* ____pchRenderModelName0_marshaled = NULL;
	____pchRenderModelName0_marshaled = il2cpp_codegen_marshal_string(___pchRenderModelName0);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____pchRenderModelName0_marshaled, reinterpret_cast<intptr_t*>(___ppRenderModel1));

	// Marshaling cleanup of parameter '___pchRenderModelName0' native representation
	il2cpp_codegen_marshal_free(____pchRenderModelName0_marshaled);
	____pchRenderModelName0_marshaled = NULL;

	// Marshaling of parameter '___ppRenderModel1' back from native representation
	___ppRenderModel1 = reinterpret_cast<IntPtr_t*>(reinterpret_cast<intptr_t*>(___ppRenderModel1));

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_LoadRenderModel_Async::BeginInvoke(System.String,System.IntPtr&,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t _LoadRenderModel_Async_BeginInvoke_m1525822855_MetadataUsageId;
extern "C"  Il2CppObject * _LoadRenderModel_Async_BeginInvoke_m1525822855 (_LoadRenderModel_Async_t3622270247 * __this, String_t* ___pchRenderModelName0, IntPtr_t* ___ppRenderModel1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_LoadRenderModel_Async_BeginInvoke_m1525822855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___pchRenderModelName0;
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &(*___ppRenderModel1));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVRRenderModelError Valve.VR.IVRRenderModels/_LoadRenderModel_Async::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern "C"  int32_t _LoadRenderModel_Async_EndInvoke_m2925842115 (_LoadRenderModel_Async_t3622270247 * __this, IntPtr_t* ___ppRenderModel0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___ppRenderModel0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRRenderModels/_LoadTexture_Async::.ctor(System.Object,System.IntPtr)
extern "C"  void _LoadTexture_Async__ctor_m2664168578 (_LoadTexture_Async_t1786536393 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVRRenderModelError Valve.VR.IVRRenderModels/_LoadTexture_Async::Invoke(System.Int32,System.IntPtr&)
extern "C"  int32_t _LoadTexture_Async_Invoke_m1545540020 (_LoadTexture_Async_t1786536393 * __this, int32_t ___textureId0, IntPtr_t* ___ppTexture1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_LoadTexture_Async_Invoke_m1545540020((_LoadTexture_Async_t1786536393 *)__this->get_prev_9(),___textureId0, ___ppTexture1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___textureId0, IntPtr_t* ___ppTexture1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___textureId0, ___ppTexture1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___textureId0, IntPtr_t* ___ppTexture1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___textureId0, ___ppTexture1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__LoadTexture_Async_t1786536393 (_LoadTexture_Async_t1786536393 * __this, int32_t ___textureId0, IntPtr_t* ___ppTexture1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(int32_t, intptr_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___textureId0, reinterpret_cast<intptr_t*>(___ppTexture1));

	// Marshaling of parameter '___ppTexture1' back from native representation
	___ppTexture1 = reinterpret_cast<IntPtr_t*>(reinterpret_cast<intptr_t*>(___ppTexture1));

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_LoadTexture_Async::BeginInvoke(System.Int32,System.IntPtr&,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t _LoadTexture_Async_BeginInvoke_m224759628_MetadataUsageId;
extern "C"  Il2CppObject * _LoadTexture_Async_BeginInvoke_m224759628 (_LoadTexture_Async_t1786536393 * __this, int32_t ___textureId0, IntPtr_t* ___ppTexture1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_LoadTexture_Async_BeginInvoke_m224759628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___textureId0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &(*___ppTexture1));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVRRenderModelError Valve.VR.IVRRenderModels/_LoadTexture_Async::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern "C"  int32_t _LoadTexture_Async_EndInvoke_m2404127357 (_LoadTexture_Async_t1786536393 * __this, IntPtr_t* ___ppTexture0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___ppTexture0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRRenderModels/_LoadTextureD3D11_Async::.ctor(System.Object,System.IntPtr)
extern "C"  void _LoadTextureD3D11_Async__ctor_m2012365743 (_LoadTextureD3D11_Async_t2681806282 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVRRenderModelError Valve.VR.IVRRenderModels/_LoadTextureD3D11_Async::Invoke(System.Int32,System.IntPtr,System.IntPtr&)
extern "C"  int32_t _LoadTextureD3D11_Async_Invoke_m285591107 (_LoadTextureD3D11_Async_t2681806282 * __this, int32_t ___textureId0, IntPtr_t ___pD3D11Device1, IntPtr_t* ___ppD3D11Texture2D2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_LoadTextureD3D11_Async_Invoke_m285591107((_LoadTextureD3D11_Async_t2681806282 *)__this->get_prev_9(),___textureId0, ___pD3D11Device1, ___ppD3D11Texture2D2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___textureId0, IntPtr_t ___pD3D11Device1, IntPtr_t* ___ppD3D11Texture2D2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___textureId0, ___pD3D11Device1, ___ppD3D11Texture2D2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___textureId0, IntPtr_t ___pD3D11Device1, IntPtr_t* ___ppD3D11Texture2D2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___textureId0, ___pD3D11Device1, ___ppD3D11Texture2D2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__LoadTextureD3D11_Async_t2681806282 (_LoadTextureD3D11_Async_t2681806282 * __this, int32_t ___textureId0, IntPtr_t ___pD3D11Device1, IntPtr_t* ___ppD3D11Texture2D2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(int32_t, intptr_t, intptr_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___textureId0, reinterpret_cast<intptr_t>((___pD3D11Device1).get_m_value_0()), reinterpret_cast<intptr_t*>(___ppD3D11Texture2D2));

	// Marshaling of parameter '___ppD3D11Texture2D2' back from native representation
	___ppD3D11Texture2D2 = reinterpret_cast<IntPtr_t*>(reinterpret_cast<intptr_t*>(___ppD3D11Texture2D2));

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_LoadTextureD3D11_Async::BeginInvoke(System.Int32,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t _LoadTextureD3D11_Async_BeginInvoke_m766226979_MetadataUsageId;
extern "C"  Il2CppObject * _LoadTextureD3D11_Async_BeginInvoke_m766226979 (_LoadTextureD3D11_Async_t2681806282 * __this, int32_t ___textureId0, IntPtr_t ___pD3D11Device1, IntPtr_t* ___ppD3D11Texture2D2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_LoadTextureD3D11_Async_BeginInvoke_m766226979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___textureId0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___pD3D11Device1);
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &(*___ppD3D11Texture2D2));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// Valve.VR.EVRRenderModelError Valve.VR.IVRRenderModels/_LoadTextureD3D11_Async::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern "C"  int32_t _LoadTextureD3D11_Async_EndInvoke_m1875304892 (_LoadTextureD3D11_Async_t2681806282 * __this, IntPtr_t* ___ppD3D11Texture2D0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___ppD3D11Texture2D0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRRenderModels/_RenderModelHasComponent::.ctor(System.Object,System.IntPtr)
extern "C"  void _RenderModelHasComponent__ctor_m3003626504 (_RenderModelHasComponent_t1969881317 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean Valve.VR.IVRRenderModels/_RenderModelHasComponent::Invoke(System.String,System.String)
extern "C"  bool _RenderModelHasComponent_Invoke_m1753530378 (_RenderModelHasComponent_t1969881317 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_RenderModelHasComponent_Invoke_m1753530378((_RenderModelHasComponent_t1969881317 *)__this->get_prev_9(),___pchRenderModelName0, ___pchComponentName1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchRenderModelName0, ___pchComponentName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchRenderModelName0, ___pchComponentName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, String_t* ___pchComponentName1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchRenderModelName0, ___pchComponentName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  bool DelegatePInvokeWrapper__RenderModelHasComponent_t1969881317 (_RenderModelHasComponent_t1969881317 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchRenderModelName0' to native representation
	char* ____pchRenderModelName0_marshaled = NULL;
	____pchRenderModelName0_marshaled = il2cpp_codegen_marshal_string(___pchRenderModelName0);

	// Marshaling of parameter '___pchComponentName1' to native representation
	char* ____pchComponentName1_marshaled = NULL;
	____pchComponentName1_marshaled = il2cpp_codegen_marshal_string(___pchComponentName1);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____pchRenderModelName0_marshaled, ____pchComponentName1_marshaled);

	// Marshaling cleanup of parameter '___pchRenderModelName0' native representation
	il2cpp_codegen_marshal_free(____pchRenderModelName0_marshaled);
	____pchRenderModelName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchComponentName1' native representation
	il2cpp_codegen_marshal_free(____pchComponentName1_marshaled);
	____pchComponentName1_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
// System.IAsyncResult Valve.VR.IVRRenderModels/_RenderModelHasComponent::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _RenderModelHasComponent_BeginInvoke_m911620243 (_RenderModelHasComponent_t1969881317 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___pchRenderModelName0;
	__d_args[1] = ___pchComponentName1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Boolean Valve.VR.IVRRenderModels/_RenderModelHasComponent::EndInvoke(System.IAsyncResult)
extern "C"  bool _RenderModelHasComponent_EndInvoke_m500587676 (_RenderModelHasComponent_t1969881317 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Conversion methods for marshalling of: Valve.VR.IVRResources
extern "C" void IVRResources_t1092978558_marshal_pinvoke(const IVRResources_t1092978558& unmarshaled, IVRResources_t1092978558_marshaled_pinvoke& marshaled)
{
	marshaled.___LoadSharedResource_0 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_LoadSharedResource_0()));
	marshaled.___GetResourceFullPath_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetResourceFullPath_1()));
}
extern Il2CppClass* _LoadSharedResource_t4289483331_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetResourceFullPath_t915790394_il2cpp_TypeInfo_var;
extern const uint32_t IVRResources_t1092978558_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void IVRResources_t1092978558_marshal_pinvoke_back(const IVRResources_t1092978558_marshaled_pinvoke& marshaled, IVRResources_t1092978558& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVRResources_t1092978558_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_LoadSharedResource_0(il2cpp_codegen_marshal_function_ptr_to_delegate<_LoadSharedResource_t4289483331>(marshaled.___LoadSharedResource_0, _LoadSharedResource_t4289483331_il2cpp_TypeInfo_var));
	unmarshaled.set_GetResourceFullPath_1(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetResourceFullPath_t915790394>(marshaled.___GetResourceFullPath_1, _GetResourceFullPath_t915790394_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: Valve.VR.IVRResources
extern "C" void IVRResources_t1092978558_marshal_pinvoke_cleanup(IVRResources_t1092978558_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Valve.VR.IVRResources
extern "C" void IVRResources_t1092978558_marshal_com(const IVRResources_t1092978558& unmarshaled, IVRResources_t1092978558_marshaled_com& marshaled)
{
	marshaled.___LoadSharedResource_0 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_LoadSharedResource_0()));
	marshaled.___GetResourceFullPath_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetResourceFullPath_1()));
}
extern Il2CppClass* _LoadSharedResource_t4289483331_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetResourceFullPath_t915790394_il2cpp_TypeInfo_var;
extern const uint32_t IVRResources_t1092978558_com_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void IVRResources_t1092978558_marshal_com_back(const IVRResources_t1092978558_marshaled_com& marshaled, IVRResources_t1092978558& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVRResources_t1092978558_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_LoadSharedResource_0(il2cpp_codegen_marshal_function_ptr_to_delegate<_LoadSharedResource_t4289483331>(marshaled.___LoadSharedResource_0, _LoadSharedResource_t4289483331_il2cpp_TypeInfo_var));
	unmarshaled.set_GetResourceFullPath_1(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetResourceFullPath_t915790394>(marshaled.___GetResourceFullPath_1, _GetResourceFullPath_t915790394_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: Valve.VR.IVRResources
extern "C" void IVRResources_t1092978558_marshal_com_cleanup(IVRResources_t1092978558_marshaled_com& marshaled)
{
}
// System.Void Valve.VR.IVRResources/_GetResourceFullPath::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetResourceFullPath__ctor_m2248991025 (_GetResourceFullPath_t915790394 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UInt32 Valve.VR.IVRResources/_GetResourceFullPath::Invoke(System.String,System.String,System.String,System.UInt32)
extern "C"  uint32_t _GetResourceFullPath_Invoke_m3238287078 (_GetResourceFullPath_t915790394 * __this, String_t* ___pchResourceName0, String_t* ___pchResourceTypeDirectory1, String_t* ___pchPathBuffer2, uint32_t ___unBufferLen3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetResourceFullPath_Invoke_m3238287078((_GetResourceFullPath_t915790394 *)__this->get_prev_9(),___pchResourceName0, ___pchResourceTypeDirectory1, ___pchPathBuffer2, ___unBufferLen3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchResourceName0, String_t* ___pchResourceTypeDirectory1, String_t* ___pchPathBuffer2, uint32_t ___unBufferLen3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchResourceName0, ___pchResourceTypeDirectory1, ___pchPathBuffer2, ___unBufferLen3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, String_t* ___pchResourceName0, String_t* ___pchResourceTypeDirectory1, String_t* ___pchPathBuffer2, uint32_t ___unBufferLen3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchResourceName0, ___pchResourceTypeDirectory1, ___pchPathBuffer2, ___unBufferLen3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, String_t* ___pchResourceTypeDirectory1, String_t* ___pchPathBuffer2, uint32_t ___unBufferLen3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchResourceName0, ___pchResourceTypeDirectory1, ___pchPathBuffer2, ___unBufferLen3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  uint32_t DelegatePInvokeWrapper__GetResourceFullPath_t915790394 (_GetResourceFullPath_t915790394 * __this, String_t* ___pchResourceName0, String_t* ___pchResourceTypeDirectory1, String_t* ___pchPathBuffer2, uint32_t ___unBufferLen3, const MethodInfo* method)
{
	typedef uint32_t (STDCALL *PInvokeFunc)(char*, char*, char*, uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchResourceName0' to native representation
	char* ____pchResourceName0_marshaled = NULL;
	____pchResourceName0_marshaled = il2cpp_codegen_marshal_string(___pchResourceName0);

	// Marshaling of parameter '___pchResourceTypeDirectory1' to native representation
	char* ____pchResourceTypeDirectory1_marshaled = NULL;
	____pchResourceTypeDirectory1_marshaled = il2cpp_codegen_marshal_string(___pchResourceTypeDirectory1);

	// Marshaling of parameter '___pchPathBuffer2' to native representation
	char* ____pchPathBuffer2_marshaled = NULL;
	____pchPathBuffer2_marshaled = il2cpp_codegen_marshal_string(___pchPathBuffer2);

	// Native function invocation
	uint32_t returnValue = il2cppPInvokeFunc(____pchResourceName0_marshaled, ____pchResourceTypeDirectory1_marshaled, ____pchPathBuffer2_marshaled, ___unBufferLen3);

	// Marshaling cleanup of parameter '___pchResourceName0' native representation
	il2cpp_codegen_marshal_free(____pchResourceName0_marshaled);
	____pchResourceName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchResourceTypeDirectory1' native representation
	il2cpp_codegen_marshal_free(____pchResourceTypeDirectory1_marshaled);
	____pchResourceTypeDirectory1_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchPathBuffer2' native representation
	il2cpp_codegen_marshal_free(____pchPathBuffer2_marshaled);
	____pchPathBuffer2_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRResources/_GetResourceFullPath::BeginInvoke(System.String,System.String,System.String,System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _GetResourceFullPath_BeginInvoke_m3374072904_MetadataUsageId;
extern "C"  Il2CppObject * _GetResourceFullPath_BeginInvoke_m3374072904 (_GetResourceFullPath_t915790394 * __this, String_t* ___pchResourceName0, String_t* ___pchResourceTypeDirectory1, String_t* ___pchPathBuffer2, uint32_t ___unBufferLen3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetResourceFullPath_BeginInvoke_m3374072904_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___pchResourceName0;
	__d_args[1] = ___pchResourceTypeDirectory1;
	__d_args[2] = ___pchPathBuffer2;
	__d_args[3] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unBufferLen3);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.UInt32 Valve.VR.IVRResources/_GetResourceFullPath::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetResourceFullPath_EndInvoke_m1714703720 (_GetResourceFullPath_t915790394 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRResources/_LoadSharedResource::.ctor(System.Object,System.IntPtr)
extern "C"  void _LoadSharedResource__ctor_m2685106812 (_LoadSharedResource_t4289483331 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UInt32 Valve.VR.IVRResources/_LoadSharedResource::Invoke(System.String,System.String,System.UInt32)
extern "C"  uint32_t _LoadSharedResource_Invoke_m810773033 (_LoadSharedResource_t4289483331 * __this, String_t* ___pchResourceName0, String_t* ___pchBuffer1, uint32_t ___unBufferLen2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_LoadSharedResource_Invoke_m810773033((_LoadSharedResource_t4289483331 *)__this->get_prev_9(),___pchResourceName0, ___pchBuffer1, ___unBufferLen2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchResourceName0, String_t* ___pchBuffer1, uint32_t ___unBufferLen2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchResourceName0, ___pchBuffer1, ___unBufferLen2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, String_t* ___pchResourceName0, String_t* ___pchBuffer1, uint32_t ___unBufferLen2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchResourceName0, ___pchBuffer1, ___unBufferLen2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, String_t* ___pchBuffer1, uint32_t ___unBufferLen2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchResourceName0, ___pchBuffer1, ___unBufferLen2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  uint32_t DelegatePInvokeWrapper__LoadSharedResource_t4289483331 (_LoadSharedResource_t4289483331 * __this, String_t* ___pchResourceName0, String_t* ___pchBuffer1, uint32_t ___unBufferLen2, const MethodInfo* method)
{
	typedef uint32_t (STDCALL *PInvokeFunc)(char*, char*, uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchResourceName0' to native representation
	char* ____pchResourceName0_marshaled = NULL;
	____pchResourceName0_marshaled = il2cpp_codegen_marshal_string(___pchResourceName0);

	// Marshaling of parameter '___pchBuffer1' to native representation
	char* ____pchBuffer1_marshaled = NULL;
	____pchBuffer1_marshaled = il2cpp_codegen_marshal_string(___pchBuffer1);

	// Native function invocation
	uint32_t returnValue = il2cppPInvokeFunc(____pchResourceName0_marshaled, ____pchBuffer1_marshaled, ___unBufferLen2);

	// Marshaling cleanup of parameter '___pchResourceName0' native representation
	il2cpp_codegen_marshal_free(____pchResourceName0_marshaled);
	____pchResourceName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchBuffer1' native representation
	il2cpp_codegen_marshal_free(____pchBuffer1_marshaled);
	____pchBuffer1_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRResources/_LoadSharedResource::BeginInvoke(System.String,System.String,System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _LoadSharedResource_BeginInvoke_m2740774717_MetadataUsageId;
extern "C"  Il2CppObject * _LoadSharedResource_BeginInvoke_m2740774717 (_LoadSharedResource_t4289483331 * __this, String_t* ___pchResourceName0, String_t* ___pchBuffer1, uint32_t ___unBufferLen2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_LoadSharedResource_BeginInvoke_m2740774717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___pchResourceName0;
	__d_args[1] = ___pchBuffer1;
	__d_args[2] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unBufferLen2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.UInt32 Valve.VR.IVRResources/_LoadSharedResource::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _LoadSharedResource_EndInvoke_m1272028175 (_LoadSharedResource_t4289483331 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Conversion methods for marshalling of: Valve.VR.IVRScreenshots
extern "C" void IVRScreenshots_t1006836234_marshal_pinvoke(const IVRScreenshots_t1006836234& unmarshaled, IVRScreenshots_t1006836234_marshaled_pinvoke& marshaled)
{
	marshaled.___RequestScreenshot_0 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_RequestScreenshot_0()));
	marshaled.___HookScreenshot_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_HookScreenshot_1()));
	marshaled.___GetScreenshotPropertyType_2 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetScreenshotPropertyType_2()));
	marshaled.___GetScreenshotPropertyFilename_3 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetScreenshotPropertyFilename_3()));
	marshaled.___UpdateScreenshotProgress_4 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_UpdateScreenshotProgress_4()));
	marshaled.___TakeStereoScreenshot_5 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_TakeStereoScreenshot_5()));
	marshaled.___SubmitScreenshot_6 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_SubmitScreenshot_6()));
}
extern Il2CppClass* _RequestScreenshot_t1956857133_il2cpp_TypeInfo_var;
extern Il2CppClass* _HookScreenshot_t2804207343_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetScreenshotPropertyType_t3028991757_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetScreenshotPropertyFilename_t1122176780_il2cpp_TypeInfo_var;
extern Il2CppClass* _UpdateScreenshotProgress_t2161609358_il2cpp_TypeInfo_var;
extern Il2CppClass* _TakeStereoScreenshot_t3387995749_il2cpp_TypeInfo_var;
extern Il2CppClass* _SubmitScreenshot_t3156929320_il2cpp_TypeInfo_var;
extern const uint32_t IVRScreenshots_t1006836234_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void IVRScreenshots_t1006836234_marshal_pinvoke_back(const IVRScreenshots_t1006836234_marshaled_pinvoke& marshaled, IVRScreenshots_t1006836234& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVRScreenshots_t1006836234_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_RequestScreenshot_0(il2cpp_codegen_marshal_function_ptr_to_delegate<_RequestScreenshot_t1956857133>(marshaled.___RequestScreenshot_0, _RequestScreenshot_t1956857133_il2cpp_TypeInfo_var));
	unmarshaled.set_HookScreenshot_1(il2cpp_codegen_marshal_function_ptr_to_delegate<_HookScreenshot_t2804207343>(marshaled.___HookScreenshot_1, _HookScreenshot_t2804207343_il2cpp_TypeInfo_var));
	unmarshaled.set_GetScreenshotPropertyType_2(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetScreenshotPropertyType_t3028991757>(marshaled.___GetScreenshotPropertyType_2, _GetScreenshotPropertyType_t3028991757_il2cpp_TypeInfo_var));
	unmarshaled.set_GetScreenshotPropertyFilename_3(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetScreenshotPropertyFilename_t1122176780>(marshaled.___GetScreenshotPropertyFilename_3, _GetScreenshotPropertyFilename_t1122176780_il2cpp_TypeInfo_var));
	unmarshaled.set_UpdateScreenshotProgress_4(il2cpp_codegen_marshal_function_ptr_to_delegate<_UpdateScreenshotProgress_t2161609358>(marshaled.___UpdateScreenshotProgress_4, _UpdateScreenshotProgress_t2161609358_il2cpp_TypeInfo_var));
	unmarshaled.set_TakeStereoScreenshot_5(il2cpp_codegen_marshal_function_ptr_to_delegate<_TakeStereoScreenshot_t3387995749>(marshaled.___TakeStereoScreenshot_5, _TakeStereoScreenshot_t3387995749_il2cpp_TypeInfo_var));
	unmarshaled.set_SubmitScreenshot_6(il2cpp_codegen_marshal_function_ptr_to_delegate<_SubmitScreenshot_t3156929320>(marshaled.___SubmitScreenshot_6, _SubmitScreenshot_t3156929320_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: Valve.VR.IVRScreenshots
extern "C" void IVRScreenshots_t1006836234_marshal_pinvoke_cleanup(IVRScreenshots_t1006836234_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Valve.VR.IVRScreenshots
extern "C" void IVRScreenshots_t1006836234_marshal_com(const IVRScreenshots_t1006836234& unmarshaled, IVRScreenshots_t1006836234_marshaled_com& marshaled)
{
	marshaled.___RequestScreenshot_0 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_RequestScreenshot_0()));
	marshaled.___HookScreenshot_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_HookScreenshot_1()));
	marshaled.___GetScreenshotPropertyType_2 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetScreenshotPropertyType_2()));
	marshaled.___GetScreenshotPropertyFilename_3 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetScreenshotPropertyFilename_3()));
	marshaled.___UpdateScreenshotProgress_4 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_UpdateScreenshotProgress_4()));
	marshaled.___TakeStereoScreenshot_5 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_TakeStereoScreenshot_5()));
	marshaled.___SubmitScreenshot_6 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_SubmitScreenshot_6()));
}
extern Il2CppClass* _RequestScreenshot_t1956857133_il2cpp_TypeInfo_var;
extern Il2CppClass* _HookScreenshot_t2804207343_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetScreenshotPropertyType_t3028991757_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetScreenshotPropertyFilename_t1122176780_il2cpp_TypeInfo_var;
extern Il2CppClass* _UpdateScreenshotProgress_t2161609358_il2cpp_TypeInfo_var;
extern Il2CppClass* _TakeStereoScreenshot_t3387995749_il2cpp_TypeInfo_var;
extern Il2CppClass* _SubmitScreenshot_t3156929320_il2cpp_TypeInfo_var;
extern const uint32_t IVRScreenshots_t1006836234_com_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void IVRScreenshots_t1006836234_marshal_com_back(const IVRScreenshots_t1006836234_marshaled_com& marshaled, IVRScreenshots_t1006836234& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVRScreenshots_t1006836234_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_RequestScreenshot_0(il2cpp_codegen_marshal_function_ptr_to_delegate<_RequestScreenshot_t1956857133>(marshaled.___RequestScreenshot_0, _RequestScreenshot_t1956857133_il2cpp_TypeInfo_var));
	unmarshaled.set_HookScreenshot_1(il2cpp_codegen_marshal_function_ptr_to_delegate<_HookScreenshot_t2804207343>(marshaled.___HookScreenshot_1, _HookScreenshot_t2804207343_il2cpp_TypeInfo_var));
	unmarshaled.set_GetScreenshotPropertyType_2(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetScreenshotPropertyType_t3028991757>(marshaled.___GetScreenshotPropertyType_2, _GetScreenshotPropertyType_t3028991757_il2cpp_TypeInfo_var));
	unmarshaled.set_GetScreenshotPropertyFilename_3(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetScreenshotPropertyFilename_t1122176780>(marshaled.___GetScreenshotPropertyFilename_3, _GetScreenshotPropertyFilename_t1122176780_il2cpp_TypeInfo_var));
	unmarshaled.set_UpdateScreenshotProgress_4(il2cpp_codegen_marshal_function_ptr_to_delegate<_UpdateScreenshotProgress_t2161609358>(marshaled.___UpdateScreenshotProgress_4, _UpdateScreenshotProgress_t2161609358_il2cpp_TypeInfo_var));
	unmarshaled.set_TakeStereoScreenshot_5(il2cpp_codegen_marshal_function_ptr_to_delegate<_TakeStereoScreenshot_t3387995749>(marshaled.___TakeStereoScreenshot_5, _TakeStereoScreenshot_t3387995749_il2cpp_TypeInfo_var));
	unmarshaled.set_SubmitScreenshot_6(il2cpp_codegen_marshal_function_ptr_to_delegate<_SubmitScreenshot_t3156929320>(marshaled.___SubmitScreenshot_6, _SubmitScreenshot_t3156929320_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: Valve.VR.IVRScreenshots
extern "C" void IVRScreenshots_t1006836234_marshal_com_cleanup(IVRScreenshots_t1006836234_marshaled_com& marshaled)
{
}
// System.Void Valve.VR.IVRScreenshots/_GetScreenshotPropertyFilename::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetScreenshotPropertyFilename__ctor_m1609003829 (_GetScreenshotPropertyFilename_t1122176780 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UInt32 Valve.VR.IVRScreenshots/_GetScreenshotPropertyFilename::Invoke(System.UInt32,Valve.VR.EVRScreenshotPropertyFilenames,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRScreenshotError&)
extern "C"  uint32_t _GetScreenshotPropertyFilename_Invoke_m3641906837 (_GetScreenshotPropertyFilename_t1122176780 * __this, uint32_t ___screenshotHandle0, int32_t ___filenameType1, StringBuilder_t1221177846 * ___pchFilename2, uint32_t ___cchFilename3, int32_t* ___pError4, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetScreenshotPropertyFilename_Invoke_m3641906837((_GetScreenshotPropertyFilename_t1122176780 *)__this->get_prev_9(),___screenshotHandle0, ___filenameType1, ___pchFilename2, ___cchFilename3, ___pError4, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___screenshotHandle0, int32_t ___filenameType1, StringBuilder_t1221177846 * ___pchFilename2, uint32_t ___cchFilename3, int32_t* ___pError4, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___screenshotHandle0, ___filenameType1, ___pchFilename2, ___cchFilename3, ___pError4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, uint32_t ___screenshotHandle0, int32_t ___filenameType1, StringBuilder_t1221177846 * ___pchFilename2, uint32_t ___cchFilename3, int32_t* ___pError4, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___screenshotHandle0, ___filenameType1, ___pchFilename2, ___cchFilename3, ___pError4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  uint32_t DelegatePInvokeWrapper__GetScreenshotPropertyFilename_t1122176780 (_GetScreenshotPropertyFilename_t1122176780 * __this, uint32_t ___screenshotHandle0, int32_t ___filenameType1, StringBuilder_t1221177846 * ___pchFilename2, uint32_t ___cchFilename3, int32_t* ___pError4, const MethodInfo* method)
{
	typedef uint32_t (STDCALL *PInvokeFunc)(uint32_t, int32_t, char*, uint32_t, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchFilename2' to native representation
	char* ____pchFilename2_marshaled = NULL;
	____pchFilename2_marshaled = il2cpp_codegen_marshal_string_builder(___pchFilename2);

	// Native function invocation
	uint32_t returnValue = il2cppPInvokeFunc(___screenshotHandle0, ___filenameType1, ____pchFilename2_marshaled, ___cchFilename3, ___pError4);

	// Marshaling of parameter '___pchFilename2' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___pchFilename2, ____pchFilename2_marshaled);

	// Marshaling cleanup of parameter '___pchFilename2' native representation
	il2cpp_codegen_marshal_free(____pchFilename2_marshaled);
	____pchFilename2_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRScreenshots/_GetScreenshotPropertyFilename::BeginInvoke(System.UInt32,Valve.VR.EVRScreenshotPropertyFilenames,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRScreenshotError&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* EVRScreenshotPropertyFilenames_t29427162_il2cpp_TypeInfo_var;
extern Il2CppClass* EVRScreenshotError_t1400268927_il2cpp_TypeInfo_var;
extern const uint32_t _GetScreenshotPropertyFilename_BeginInvoke_m2793546033_MetadataUsageId;
extern "C"  Il2CppObject * _GetScreenshotPropertyFilename_BeginInvoke_m2793546033 (_GetScreenshotPropertyFilename_t1122176780 * __this, uint32_t ___screenshotHandle0, int32_t ___filenameType1, StringBuilder_t1221177846 * ___pchFilename2, uint32_t ___cchFilename3, int32_t* ___pError4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetScreenshotPropertyFilename_BeginInvoke_m2793546033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___screenshotHandle0);
	__d_args[1] = Box(EVRScreenshotPropertyFilenames_t29427162_il2cpp_TypeInfo_var, &___filenameType1);
	__d_args[2] = ___pchFilename2;
	__d_args[3] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___cchFilename3);
	__d_args[4] = Box(EVRScreenshotError_t1400268927_il2cpp_TypeInfo_var, &(*___pError4));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback5, (Il2CppObject*)___object6);
}
// System.UInt32 Valve.VR.IVRScreenshots/_GetScreenshotPropertyFilename::EndInvoke(Valve.VR.EVRScreenshotError&,System.IAsyncResult)
extern "C"  uint32_t _GetScreenshotPropertyFilename_EndInvoke_m872640387 (_GetScreenshotPropertyFilename_t1122176780 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pError0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRScreenshots/_GetScreenshotPropertyType::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetScreenshotPropertyType__ctor_m1910313476 (_GetScreenshotPropertyType_t3028991757 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVRScreenshotType Valve.VR.IVRScreenshots/_GetScreenshotPropertyType::Invoke(System.UInt32,Valve.VR.EVRScreenshotError&)
extern "C"  int32_t _GetScreenshotPropertyType_Invoke_m1382846401 (_GetScreenshotPropertyType_t3028991757 * __this, uint32_t ___screenshotHandle0, int32_t* ___pError1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetScreenshotPropertyType_Invoke_m1382846401((_GetScreenshotPropertyType_t3028991757 *)__this->get_prev_9(),___screenshotHandle0, ___pError1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___screenshotHandle0, int32_t* ___pError1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___screenshotHandle0, ___pError1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint32_t ___screenshotHandle0, int32_t* ___pError1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___screenshotHandle0, ___pError1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__GetScreenshotPropertyType_t3028991757 (_GetScreenshotPropertyType_t3028991757 * __this, uint32_t ___screenshotHandle0, int32_t* ___pError1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint32_t, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___screenshotHandle0, ___pError1);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRScreenshots/_GetScreenshotPropertyType::BeginInvoke(System.UInt32,Valve.VR.EVRScreenshotError&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* EVRScreenshotError_t1400268927_il2cpp_TypeInfo_var;
extern const uint32_t _GetScreenshotPropertyType_BeginInvoke_m4002242868_MetadataUsageId;
extern "C"  Il2CppObject * _GetScreenshotPropertyType_BeginInvoke_m4002242868 (_GetScreenshotPropertyType_t3028991757 * __this, uint32_t ___screenshotHandle0, int32_t* ___pError1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetScreenshotPropertyType_BeginInvoke_m4002242868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___screenshotHandle0);
	__d_args[1] = Box(EVRScreenshotError_t1400268927_il2cpp_TypeInfo_var, &(*___pError1));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVRScreenshotType Valve.VR.IVRScreenshots/_GetScreenshotPropertyType::EndInvoke(Valve.VR.EVRScreenshotError&,System.IAsyncResult)
extern "C"  int32_t _GetScreenshotPropertyType_EndInvoke_m1385256291 (_GetScreenshotPropertyType_t3028991757 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pError0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRScreenshots/_HookScreenshot::.ctor(System.Object,System.IntPtr)
extern "C"  void _HookScreenshot__ctor_m3444396258 (_HookScreenshot_t2804207343 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_HookScreenshot::Invoke(Valve.VR.EVRScreenshotType[],System.Int32)
extern "C"  int32_t _HookScreenshot_Invoke_m625201236 (_HookScreenshot_t2804207343 * __this, EVRScreenshotTypeU5BU5D_t2594501106* ___pSupportedTypes0, int32_t ___numTypes1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_HookScreenshot_Invoke_m625201236((_HookScreenshot_t2804207343 *)__this->get_prev_9(),___pSupportedTypes0, ___numTypes1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, EVRScreenshotTypeU5BU5D_t2594501106* ___pSupportedTypes0, int32_t ___numTypes1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pSupportedTypes0, ___numTypes1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, EVRScreenshotTypeU5BU5D_t2594501106* ___pSupportedTypes0, int32_t ___numTypes1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pSupportedTypes0, ___numTypes1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___numTypes1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pSupportedTypes0, ___numTypes1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__HookScreenshot_t2804207343 (_HookScreenshot_t2804207343 * __this, EVRScreenshotTypeU5BU5D_t2594501106* ___pSupportedTypes0, int32_t ___numTypes1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(int32_t*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pSupportedTypes0' to native representation
	int32_t* ____pSupportedTypes0_marshaled = NULL;
	if (___pSupportedTypes0 != NULL)
	{
		____pSupportedTypes0_marshaled = reinterpret_cast<int32_t*>((___pSupportedTypes0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____pSupportedTypes0_marshaled, ___numTypes1);

	// Marshaling of parameter '___pSupportedTypes0' back from native representation
	if (____pSupportedTypes0_marshaled != NULL)
	{
		int32_t ____pSupportedTypes0_Length = (___pSupportedTypes0)->max_length;
		for (int32_t i = 0; i < ____pSupportedTypes0_Length; i++)
		{
			(___pSupportedTypes0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____pSupportedTypes0_marshaled)[i]);
		}
	}

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRScreenshots/_HookScreenshot::BeginInvoke(Valve.VR.EVRScreenshotType[],System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t _HookScreenshot_BeginInvoke_m2347167407_MetadataUsageId;
extern "C"  Il2CppObject * _HookScreenshot_BeginInvoke_m2347167407 (_HookScreenshot_t2804207343 * __this, EVRScreenshotTypeU5BU5D_t2594501106* ___pSupportedTypes0, int32_t ___numTypes1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_HookScreenshot_BeginInvoke_m2347167407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___pSupportedTypes0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___numTypes1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_HookScreenshot::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _HookScreenshot_EndInvoke_m1843050030 (_HookScreenshot_t2804207343 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRScreenshots/_RequestScreenshot::.ctor(System.Object,System.IntPtr)
extern "C"  void _RequestScreenshot__ctor_m2733240852 (_RequestScreenshot_t1956857133 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_RequestScreenshot::Invoke(System.UInt32&,Valve.VR.EVRScreenshotType,System.String,System.String)
extern "C"  int32_t _RequestScreenshot_Invoke_m1504245885 (_RequestScreenshot_t1956857133 * __this, uint32_t* ___pOutScreenshotHandle0, int32_t ___type1, String_t* ___pchPreviewFilename2, String_t* ___pchVRFilename3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_RequestScreenshot_Invoke_m1504245885((_RequestScreenshot_t1956857133 *)__this->get_prev_9(),___pOutScreenshotHandle0, ___type1, ___pchPreviewFilename2, ___pchVRFilename3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t* ___pOutScreenshotHandle0, int32_t ___type1, String_t* ___pchPreviewFilename2, String_t* ___pchVRFilename3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pOutScreenshotHandle0, ___type1, ___pchPreviewFilename2, ___pchVRFilename3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint32_t* ___pOutScreenshotHandle0, int32_t ___type1, String_t* ___pchPreviewFilename2, String_t* ___pchVRFilename3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pOutScreenshotHandle0, ___type1, ___pchPreviewFilename2, ___pchVRFilename3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__RequestScreenshot_t1956857133 (_RequestScreenshot_t1956857133 * __this, uint32_t* ___pOutScreenshotHandle0, int32_t ___type1, String_t* ___pchPreviewFilename2, String_t* ___pchVRFilename3, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint32_t*, int32_t, char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchPreviewFilename2' to native representation
	char* ____pchPreviewFilename2_marshaled = NULL;
	____pchPreviewFilename2_marshaled = il2cpp_codegen_marshal_string(___pchPreviewFilename2);

	// Marshaling of parameter '___pchVRFilename3' to native representation
	char* ____pchVRFilename3_marshaled = NULL;
	____pchVRFilename3_marshaled = il2cpp_codegen_marshal_string(___pchVRFilename3);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___pOutScreenshotHandle0, ___type1, ____pchPreviewFilename2_marshaled, ____pchVRFilename3_marshaled);

	// Marshaling cleanup of parameter '___pchPreviewFilename2' native representation
	il2cpp_codegen_marshal_free(____pchPreviewFilename2_marshaled);
	____pchPreviewFilename2_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchVRFilename3' native representation
	il2cpp_codegen_marshal_free(____pchVRFilename3_marshaled);
	____pchVRFilename3_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRScreenshots/_RequestScreenshot::BeginInvoke(System.UInt32&,Valve.VR.EVRScreenshotType,System.String,System.String,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* EVRScreenshotType_t611740195_il2cpp_TypeInfo_var;
extern const uint32_t _RequestScreenshot_BeginInvoke_m1291758310_MetadataUsageId;
extern "C"  Il2CppObject * _RequestScreenshot_BeginInvoke_m1291758310 (_RequestScreenshot_t1956857133 * __this, uint32_t* ___pOutScreenshotHandle0, int32_t ___type1, String_t* ___pchPreviewFilename2, String_t* ___pchVRFilename3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_RequestScreenshot_BeginInvoke_m1291758310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &(*___pOutScreenshotHandle0));
	__d_args[1] = Box(EVRScreenshotType_t611740195_il2cpp_TypeInfo_var, &___type1);
	__d_args[2] = ___pchPreviewFilename2;
	__d_args[3] = ___pchVRFilename3;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_RequestScreenshot::EndInvoke(System.UInt32&,System.IAsyncResult)
extern "C"  int32_t _RequestScreenshot_EndInvoke_m2318681602 (_RequestScreenshot_t1956857133 * __this, uint32_t* ___pOutScreenshotHandle0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pOutScreenshotHandle0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRScreenshots/_SubmitScreenshot::.ctor(System.Object,System.IntPtr)
extern "C"  void _SubmitScreenshot__ctor_m1729493409 (_SubmitScreenshot_t3156929320 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_SubmitScreenshot::Invoke(System.UInt32,Valve.VR.EVRScreenshotType,System.String,System.String)
extern "C"  int32_t _SubmitScreenshot_Invoke_m26953202 (_SubmitScreenshot_t3156929320 * __this, uint32_t ___screenshotHandle0, int32_t ___type1, String_t* ___pchSourcePreviewFilename2, String_t* ___pchSourceVRFilename3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SubmitScreenshot_Invoke_m26953202((_SubmitScreenshot_t3156929320 *)__this->get_prev_9(),___screenshotHandle0, ___type1, ___pchSourcePreviewFilename2, ___pchSourceVRFilename3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___screenshotHandle0, int32_t ___type1, String_t* ___pchSourcePreviewFilename2, String_t* ___pchSourceVRFilename3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___screenshotHandle0, ___type1, ___pchSourcePreviewFilename2, ___pchSourceVRFilename3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint32_t ___screenshotHandle0, int32_t ___type1, String_t* ___pchSourcePreviewFilename2, String_t* ___pchSourceVRFilename3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___screenshotHandle0, ___type1, ___pchSourcePreviewFilename2, ___pchSourceVRFilename3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__SubmitScreenshot_t3156929320 (_SubmitScreenshot_t3156929320 * __this, uint32_t ___screenshotHandle0, int32_t ___type1, String_t* ___pchSourcePreviewFilename2, String_t* ___pchSourceVRFilename3, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint32_t, int32_t, char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchSourcePreviewFilename2' to native representation
	char* ____pchSourcePreviewFilename2_marshaled = NULL;
	____pchSourcePreviewFilename2_marshaled = il2cpp_codegen_marshal_string(___pchSourcePreviewFilename2);

	// Marshaling of parameter '___pchSourceVRFilename3' to native representation
	char* ____pchSourceVRFilename3_marshaled = NULL;
	____pchSourceVRFilename3_marshaled = il2cpp_codegen_marshal_string(___pchSourceVRFilename3);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___screenshotHandle0, ___type1, ____pchSourcePreviewFilename2_marshaled, ____pchSourceVRFilename3_marshaled);

	// Marshaling cleanup of parameter '___pchSourcePreviewFilename2' native representation
	il2cpp_codegen_marshal_free(____pchSourcePreviewFilename2_marshaled);
	____pchSourcePreviewFilename2_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchSourceVRFilename3' native representation
	il2cpp_codegen_marshal_free(____pchSourceVRFilename3_marshaled);
	____pchSourceVRFilename3_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRScreenshots/_SubmitScreenshot::BeginInvoke(System.UInt32,Valve.VR.EVRScreenshotType,System.String,System.String,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* EVRScreenshotType_t611740195_il2cpp_TypeInfo_var;
extern const uint32_t _SubmitScreenshot_BeginInvoke_m2995271979_MetadataUsageId;
extern "C"  Il2CppObject * _SubmitScreenshot_BeginInvoke_m2995271979 (_SubmitScreenshot_t3156929320 * __this, uint32_t ___screenshotHandle0, int32_t ___type1, String_t* ___pchSourcePreviewFilename2, String_t* ___pchSourceVRFilename3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SubmitScreenshot_BeginInvoke_m2995271979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___screenshotHandle0);
	__d_args[1] = Box(EVRScreenshotType_t611740195_il2cpp_TypeInfo_var, &___type1);
	__d_args[2] = ___pchSourcePreviewFilename2;
	__d_args[3] = ___pchSourceVRFilename3;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_SubmitScreenshot::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SubmitScreenshot_EndInvoke_m3607649749 (_SubmitScreenshot_t3156929320 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRScreenshots/_TakeStereoScreenshot::.ctor(System.Object,System.IntPtr)
extern "C"  void _TakeStereoScreenshot__ctor_m4113251518 (_TakeStereoScreenshot_t3387995749 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_TakeStereoScreenshot::Invoke(System.UInt32&,System.String,System.String)
extern "C"  int32_t _TakeStereoScreenshot_Invoke_m2925078552 (_TakeStereoScreenshot_t3387995749 * __this, uint32_t* ___pOutScreenshotHandle0, String_t* ___pchPreviewFilename1, String_t* ___pchVRFilename2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_TakeStereoScreenshot_Invoke_m2925078552((_TakeStereoScreenshot_t3387995749 *)__this->get_prev_9(),___pOutScreenshotHandle0, ___pchPreviewFilename1, ___pchVRFilename2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t* ___pOutScreenshotHandle0, String_t* ___pchPreviewFilename1, String_t* ___pchVRFilename2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pOutScreenshotHandle0, ___pchPreviewFilename1, ___pchVRFilename2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint32_t* ___pOutScreenshotHandle0, String_t* ___pchPreviewFilename1, String_t* ___pchVRFilename2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pOutScreenshotHandle0, ___pchPreviewFilename1, ___pchVRFilename2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__TakeStereoScreenshot_t3387995749 (_TakeStereoScreenshot_t3387995749 * __this, uint32_t* ___pOutScreenshotHandle0, String_t* ___pchPreviewFilename1, String_t* ___pchVRFilename2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint32_t*, char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchPreviewFilename1' to native representation
	char* ____pchPreviewFilename1_marshaled = NULL;
	____pchPreviewFilename1_marshaled = il2cpp_codegen_marshal_string(___pchPreviewFilename1);

	// Marshaling of parameter '___pchVRFilename2' to native representation
	char* ____pchVRFilename2_marshaled = NULL;
	____pchVRFilename2_marshaled = il2cpp_codegen_marshal_string(___pchVRFilename2);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___pOutScreenshotHandle0, ____pchPreviewFilename1_marshaled, ____pchVRFilename2_marshaled);

	// Marshaling cleanup of parameter '___pchPreviewFilename1' native representation
	il2cpp_codegen_marshal_free(____pchPreviewFilename1_marshaled);
	____pchPreviewFilename1_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchVRFilename2' native representation
	il2cpp_codegen_marshal_free(____pchVRFilename2_marshaled);
	____pchVRFilename2_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRScreenshots/_TakeStereoScreenshot::BeginInvoke(System.UInt32&,System.String,System.String,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _TakeStereoScreenshot_BeginInvoke_m3970592741_MetadataUsageId;
extern "C"  Il2CppObject * _TakeStereoScreenshot_BeginInvoke_m3970592741 (_TakeStereoScreenshot_t3387995749 * __this, uint32_t* ___pOutScreenshotHandle0, String_t* ___pchPreviewFilename1, String_t* ___pchVRFilename2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_TakeStereoScreenshot_BeginInvoke_m3970592741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &(*___pOutScreenshotHandle0));
	__d_args[1] = ___pchPreviewFilename1;
	__d_args[2] = ___pchVRFilename2;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_TakeStereoScreenshot::EndInvoke(System.UInt32&,System.IAsyncResult)
extern "C"  int32_t _TakeStereoScreenshot_EndInvoke_m3288943832 (_TakeStereoScreenshot_t3387995749 * __this, uint32_t* ___pOutScreenshotHandle0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pOutScreenshotHandle0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRScreenshots/_UpdateScreenshotProgress::.ctor(System.Object,System.IntPtr)
extern "C"  void _UpdateScreenshotProgress__ctor_m1711528145 (_UpdateScreenshotProgress_t2161609358 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_UpdateScreenshotProgress::Invoke(System.UInt32,System.Single)
extern "C"  int32_t _UpdateScreenshotProgress_Invoke_m851287384 (_UpdateScreenshotProgress_t2161609358 * __this, uint32_t ___screenshotHandle0, float ___flProgress1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_UpdateScreenshotProgress_Invoke_m851287384((_UpdateScreenshotProgress_t2161609358 *)__this->get_prev_9(),___screenshotHandle0, ___flProgress1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___screenshotHandle0, float ___flProgress1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___screenshotHandle0, ___flProgress1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint32_t ___screenshotHandle0, float ___flProgress1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___screenshotHandle0, ___flProgress1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__UpdateScreenshotProgress_t2161609358 (_UpdateScreenshotProgress_t2161609358 * __this, uint32_t ___screenshotHandle0, float ___flProgress1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint32_t, float);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___screenshotHandle0, ___flProgress1);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRScreenshots/_UpdateScreenshotProgress::BeginInvoke(System.UInt32,System.Single,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t _UpdateScreenshotProgress_BeginInvoke_m694852103_MetadataUsageId;
extern "C"  Il2CppObject * _UpdateScreenshotProgress_BeginInvoke_m694852103 (_UpdateScreenshotProgress_t2161609358 * __this, uint32_t ___screenshotHandle0, float ___flProgress1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_UpdateScreenshotProgress_BeginInvoke_m694852103_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___screenshotHandle0);
	__d_args[1] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___flProgress1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_UpdateScreenshotProgress::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _UpdateScreenshotProgress_EndInvoke_m3851553093 (_UpdateScreenshotProgress_t2161609358 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Conversion methods for marshalling of: Valve.VR.IVRSettings
extern "C" void IVRSettings_t254931744_marshal_pinvoke(const IVRSettings_t254931744& unmarshaled, IVRSettings_t254931744_marshaled_pinvoke& marshaled)
{
	marshaled.___GetSettingsErrorNameFromEnum_0 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetSettingsErrorNameFromEnum_0()));
	marshaled.___Sync_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_Sync_1()));
	marshaled.___SetBool_2 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_SetBool_2()));
	marshaled.___SetInt32_3 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_SetInt32_3()));
	marshaled.___SetFloat_4 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_SetFloat_4()));
	marshaled.___SetString_5 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_SetString_5()));
	marshaled.___GetBool_6 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetBool_6()));
	marshaled.___GetInt32_7 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetInt32_7()));
	marshaled.___GetFloat_8 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetFloat_8()));
	marshaled.___GetString_9 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetString_9()));
	marshaled.___RemoveSection_10 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_RemoveSection_10()));
	marshaled.___RemoveKeyInSection_11 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_RemoveKeyInSection_11()));
}
extern Il2CppClass* _GetSettingsErrorNameFromEnum_t293614055_il2cpp_TypeInfo_var;
extern Il2CppClass* _Sync_t2978470277_il2cpp_TypeInfo_var;
extern Il2CppClass* _SetBool_t1033875974_il2cpp_TypeInfo_var;
extern Il2CppClass* _SetInt32_t3538080526_il2cpp_TypeInfo_var;
extern Il2CppClass* _SetFloat_t3724747224_il2cpp_TypeInfo_var;
extern Il2CppClass* _SetString_t1793856309_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetBool_t1034551410_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetInt32_t3538756002_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetFloat_t3725422668_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetString_t1816180801_il2cpp_TypeInfo_var;
extern Il2CppClass* _RemoveSection_t1493649321_il2cpp_TypeInfo_var;
extern Il2CppClass* _RemoveKeyInSection_t1836506429_il2cpp_TypeInfo_var;
extern const uint32_t IVRSettings_t254931744_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void IVRSettings_t254931744_marshal_pinvoke_back(const IVRSettings_t254931744_marshaled_pinvoke& marshaled, IVRSettings_t254931744& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVRSettings_t254931744_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_GetSettingsErrorNameFromEnum_0(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetSettingsErrorNameFromEnum_t293614055>(marshaled.___GetSettingsErrorNameFromEnum_0, _GetSettingsErrorNameFromEnum_t293614055_il2cpp_TypeInfo_var));
	unmarshaled.set_Sync_1(il2cpp_codegen_marshal_function_ptr_to_delegate<_Sync_t2978470277>(marshaled.___Sync_1, _Sync_t2978470277_il2cpp_TypeInfo_var));
	unmarshaled.set_SetBool_2(il2cpp_codegen_marshal_function_ptr_to_delegate<_SetBool_t1033875974>(marshaled.___SetBool_2, _SetBool_t1033875974_il2cpp_TypeInfo_var));
	unmarshaled.set_SetInt32_3(il2cpp_codegen_marshal_function_ptr_to_delegate<_SetInt32_t3538080526>(marshaled.___SetInt32_3, _SetInt32_t3538080526_il2cpp_TypeInfo_var));
	unmarshaled.set_SetFloat_4(il2cpp_codegen_marshal_function_ptr_to_delegate<_SetFloat_t3724747224>(marshaled.___SetFloat_4, _SetFloat_t3724747224_il2cpp_TypeInfo_var));
	unmarshaled.set_SetString_5(il2cpp_codegen_marshal_function_ptr_to_delegate<_SetString_t1793856309>(marshaled.___SetString_5, _SetString_t1793856309_il2cpp_TypeInfo_var));
	unmarshaled.set_GetBool_6(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetBool_t1034551410>(marshaled.___GetBool_6, _GetBool_t1034551410_il2cpp_TypeInfo_var));
	unmarshaled.set_GetInt32_7(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetInt32_t3538756002>(marshaled.___GetInt32_7, _GetInt32_t3538756002_il2cpp_TypeInfo_var));
	unmarshaled.set_GetFloat_8(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetFloat_t3725422668>(marshaled.___GetFloat_8, _GetFloat_t3725422668_il2cpp_TypeInfo_var));
	unmarshaled.set_GetString_9(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetString_t1816180801>(marshaled.___GetString_9, _GetString_t1816180801_il2cpp_TypeInfo_var));
	unmarshaled.set_RemoveSection_10(il2cpp_codegen_marshal_function_ptr_to_delegate<_RemoveSection_t1493649321>(marshaled.___RemoveSection_10, _RemoveSection_t1493649321_il2cpp_TypeInfo_var));
	unmarshaled.set_RemoveKeyInSection_11(il2cpp_codegen_marshal_function_ptr_to_delegate<_RemoveKeyInSection_t1836506429>(marshaled.___RemoveKeyInSection_11, _RemoveKeyInSection_t1836506429_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: Valve.VR.IVRSettings
extern "C" void IVRSettings_t254931744_marshal_pinvoke_cleanup(IVRSettings_t254931744_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Valve.VR.IVRSettings
extern "C" void IVRSettings_t254931744_marshal_com(const IVRSettings_t254931744& unmarshaled, IVRSettings_t254931744_marshaled_com& marshaled)
{
	marshaled.___GetSettingsErrorNameFromEnum_0 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetSettingsErrorNameFromEnum_0()));
	marshaled.___Sync_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_Sync_1()));
	marshaled.___SetBool_2 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_SetBool_2()));
	marshaled.___SetInt32_3 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_SetInt32_3()));
	marshaled.___SetFloat_4 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_SetFloat_4()));
	marshaled.___SetString_5 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_SetString_5()));
	marshaled.___GetBool_6 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetBool_6()));
	marshaled.___GetInt32_7 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetInt32_7()));
	marshaled.___GetFloat_8 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetFloat_8()));
	marshaled.___GetString_9 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetString_9()));
	marshaled.___RemoveSection_10 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_RemoveSection_10()));
	marshaled.___RemoveKeyInSection_11 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_RemoveKeyInSection_11()));
}
extern Il2CppClass* _GetSettingsErrorNameFromEnum_t293614055_il2cpp_TypeInfo_var;
extern Il2CppClass* _Sync_t2978470277_il2cpp_TypeInfo_var;
extern Il2CppClass* _SetBool_t1033875974_il2cpp_TypeInfo_var;
extern Il2CppClass* _SetInt32_t3538080526_il2cpp_TypeInfo_var;
extern Il2CppClass* _SetFloat_t3724747224_il2cpp_TypeInfo_var;
extern Il2CppClass* _SetString_t1793856309_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetBool_t1034551410_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetInt32_t3538756002_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetFloat_t3725422668_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetString_t1816180801_il2cpp_TypeInfo_var;
extern Il2CppClass* _RemoveSection_t1493649321_il2cpp_TypeInfo_var;
extern Il2CppClass* _RemoveKeyInSection_t1836506429_il2cpp_TypeInfo_var;
extern const uint32_t IVRSettings_t254931744_com_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void IVRSettings_t254931744_marshal_com_back(const IVRSettings_t254931744_marshaled_com& marshaled, IVRSettings_t254931744& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVRSettings_t254931744_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_GetSettingsErrorNameFromEnum_0(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetSettingsErrorNameFromEnum_t293614055>(marshaled.___GetSettingsErrorNameFromEnum_0, _GetSettingsErrorNameFromEnum_t293614055_il2cpp_TypeInfo_var));
	unmarshaled.set_Sync_1(il2cpp_codegen_marshal_function_ptr_to_delegate<_Sync_t2978470277>(marshaled.___Sync_1, _Sync_t2978470277_il2cpp_TypeInfo_var));
	unmarshaled.set_SetBool_2(il2cpp_codegen_marshal_function_ptr_to_delegate<_SetBool_t1033875974>(marshaled.___SetBool_2, _SetBool_t1033875974_il2cpp_TypeInfo_var));
	unmarshaled.set_SetInt32_3(il2cpp_codegen_marshal_function_ptr_to_delegate<_SetInt32_t3538080526>(marshaled.___SetInt32_3, _SetInt32_t3538080526_il2cpp_TypeInfo_var));
	unmarshaled.set_SetFloat_4(il2cpp_codegen_marshal_function_ptr_to_delegate<_SetFloat_t3724747224>(marshaled.___SetFloat_4, _SetFloat_t3724747224_il2cpp_TypeInfo_var));
	unmarshaled.set_SetString_5(il2cpp_codegen_marshal_function_ptr_to_delegate<_SetString_t1793856309>(marshaled.___SetString_5, _SetString_t1793856309_il2cpp_TypeInfo_var));
	unmarshaled.set_GetBool_6(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetBool_t1034551410>(marshaled.___GetBool_6, _GetBool_t1034551410_il2cpp_TypeInfo_var));
	unmarshaled.set_GetInt32_7(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetInt32_t3538756002>(marshaled.___GetInt32_7, _GetInt32_t3538756002_il2cpp_TypeInfo_var));
	unmarshaled.set_GetFloat_8(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetFloat_t3725422668>(marshaled.___GetFloat_8, _GetFloat_t3725422668_il2cpp_TypeInfo_var));
	unmarshaled.set_GetString_9(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetString_t1816180801>(marshaled.___GetString_9, _GetString_t1816180801_il2cpp_TypeInfo_var));
	unmarshaled.set_RemoveSection_10(il2cpp_codegen_marshal_function_ptr_to_delegate<_RemoveSection_t1493649321>(marshaled.___RemoveSection_10, _RemoveSection_t1493649321_il2cpp_TypeInfo_var));
	unmarshaled.set_RemoveKeyInSection_11(il2cpp_codegen_marshal_function_ptr_to_delegate<_RemoveKeyInSection_t1836506429>(marshaled.___RemoveKeyInSection_11, _RemoveKeyInSection_t1836506429_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: Valve.VR.IVRSettings
extern "C" void IVRSettings_t254931744_marshal_com_cleanup(IVRSettings_t254931744_marshaled_com& marshaled)
{
}
// System.Void Valve.VR.IVRSettings/_GetBool::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetBool__ctor_m317966315 (_GetBool_t1034551410 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean Valve.VR.IVRSettings/_GetBool::Invoke(System.String,System.String,Valve.VR.EVRSettingsError&)
extern "C"  bool _GetBool_Invoke_m2509938721 (_GetBool_t1034551410 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetBool_Invoke_m2509938721((_GetBool_t1034551410 *)__this->get_prev_9(),___pchSection0, ___pchSettingsKey1, ___peError2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___peError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___peError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchSection0, ___pchSettingsKey1, ___peError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  bool DelegatePInvokeWrapper__GetBool_t1034551410 (_GetBool_t1034551410 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(char*, char*, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchSection0' to native representation
	char* ____pchSection0_marshaled = NULL;
	____pchSection0_marshaled = il2cpp_codegen_marshal_string(___pchSection0);

	// Marshaling of parameter '___pchSettingsKey1' to native representation
	char* ____pchSettingsKey1_marshaled = NULL;
	____pchSettingsKey1_marshaled = il2cpp_codegen_marshal_string(___pchSettingsKey1);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____pchSection0_marshaled, ____pchSettingsKey1_marshaled, ___peError2);

	// Marshaling cleanup of parameter '___pchSection0' native representation
	il2cpp_codegen_marshal_free(____pchSection0_marshaled);
	____pchSection0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchSettingsKey1' native representation
	il2cpp_codegen_marshal_free(____pchSettingsKey1_marshaled);
	____pchSettingsKey1_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
// System.IAsyncResult Valve.VR.IVRSettings/_GetBool::BeginInvoke(System.String,System.String,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern Il2CppClass* EVRSettingsError_t4124928198_il2cpp_TypeInfo_var;
extern const uint32_t _GetBool_BeginInvoke_m2680744014_MetadataUsageId;
extern "C"  Il2CppObject * _GetBool_BeginInvoke_m2680744014 (_GetBool_t1034551410 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetBool_BeginInvoke_m2680744014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___pchSection0;
	__d_args[1] = ___pchSettingsKey1;
	__d_args[2] = Box(EVRSettingsError_t4124928198_il2cpp_TypeInfo_var, &(*___peError2));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Boolean Valve.VR.IVRSettings/_GetBool::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  bool _GetBool_EndInvoke_m1343717103 (_GetBool_t1034551410 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___peError0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSettings/_GetFloat::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetFloat__ctor_m511259393 (_GetFloat_t3725422668 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Single Valve.VR.IVRSettings/_GetFloat::Invoke(System.String,System.String,Valve.VR.EVRSettingsError&)
extern "C"  float _GetFloat_Invoke_m2540038257 (_GetFloat_t3725422668 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetFloat_Invoke_m2540038257((_GetFloat_t3725422668 *)__this->get_prev_9(),___pchSection0, ___pchSettingsKey1, ___peError2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___peError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___peError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchSection0, ___pchSettingsKey1, ___peError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  float DelegatePInvokeWrapper__GetFloat_t3725422668 (_GetFloat_t3725422668 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method)
{
	typedef float (STDCALL *PInvokeFunc)(char*, char*, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchSection0' to native representation
	char* ____pchSection0_marshaled = NULL;
	____pchSection0_marshaled = il2cpp_codegen_marshal_string(___pchSection0);

	// Marshaling of parameter '___pchSettingsKey1' to native representation
	char* ____pchSettingsKey1_marshaled = NULL;
	____pchSettingsKey1_marshaled = il2cpp_codegen_marshal_string(___pchSettingsKey1);

	// Native function invocation
	float returnValue = il2cppPInvokeFunc(____pchSection0_marshaled, ____pchSettingsKey1_marshaled, ___peError2);

	// Marshaling cleanup of parameter '___pchSection0' native representation
	il2cpp_codegen_marshal_free(____pchSection0_marshaled);
	____pchSection0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchSettingsKey1' native representation
	il2cpp_codegen_marshal_free(____pchSettingsKey1_marshaled);
	____pchSettingsKey1_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSettings/_GetFloat::BeginInvoke(System.String,System.String,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern Il2CppClass* EVRSettingsError_t4124928198_il2cpp_TypeInfo_var;
extern const uint32_t _GetFloat_BeginInvoke_m1143133072_MetadataUsageId;
extern "C"  Il2CppObject * _GetFloat_BeginInvoke_m1143133072 (_GetFloat_t3725422668 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetFloat_BeginInvoke_m1143133072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___pchSection0;
	__d_args[1] = ___pchSettingsKey1;
	__d_args[2] = Box(EVRSettingsError_t4124928198_il2cpp_TypeInfo_var, &(*___peError2));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Single Valve.VR.IVRSettings/_GetFloat::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  float _GetFloat_EndInvoke_m379496283 (_GetFloat_t3725422668 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___peError0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSettings/_GetInt32::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetInt32__ctor_m116697035 (_GetInt32_t3538756002 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 Valve.VR.IVRSettings/_GetInt32::Invoke(System.String,System.String,Valve.VR.EVRSettingsError&)
extern "C"  int32_t _GetInt32_Invoke_m582897879 (_GetInt32_t3538756002 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetInt32_Invoke_m582897879((_GetInt32_t3538756002 *)__this->get_prev_9(),___pchSection0, ___pchSettingsKey1, ___peError2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___peError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___peError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___pchSection0, ___pchSettingsKey1, ___peError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__GetInt32_t3538756002 (_GetInt32_t3538756002 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(char*, char*, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchSection0' to native representation
	char* ____pchSection0_marshaled = NULL;
	____pchSection0_marshaled = il2cpp_codegen_marshal_string(___pchSection0);

	// Marshaling of parameter '___pchSettingsKey1' to native representation
	char* ____pchSettingsKey1_marshaled = NULL;
	____pchSettingsKey1_marshaled = il2cpp_codegen_marshal_string(___pchSettingsKey1);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____pchSection0_marshaled, ____pchSettingsKey1_marshaled, ___peError2);

	// Marshaling cleanup of parameter '___pchSection0' native representation
	il2cpp_codegen_marshal_free(____pchSection0_marshaled);
	____pchSection0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchSettingsKey1' native representation
	il2cpp_codegen_marshal_free(____pchSettingsKey1_marshaled);
	____pchSettingsKey1_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSettings/_GetInt32::BeginInvoke(System.String,System.String,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern Il2CppClass* EVRSettingsError_t4124928198_il2cpp_TypeInfo_var;
extern const uint32_t _GetInt32_BeginInvoke_m776285754_MetadataUsageId;
extern "C"  Il2CppObject * _GetInt32_BeginInvoke_m776285754 (_GetInt32_t3538756002 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetInt32_BeginInvoke_m776285754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___pchSection0;
	__d_args[1] = ___pchSettingsKey1;
	__d_args[2] = Box(EVRSettingsError_t4124928198_il2cpp_TypeInfo_var, &(*___peError2));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Int32 Valve.VR.IVRSettings/_GetInt32::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  int32_t _GetInt32_EndInvoke_m722728173 (_GetInt32_t3538756002 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___peError0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSettings/_GetSettingsErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetSettingsErrorNameFromEnum__ctor_m4089028380 (_GetSettingsErrorNameFromEnum_t293614055 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.IntPtr Valve.VR.IVRSettings/_GetSettingsErrorNameFromEnum::Invoke(Valve.VR.EVRSettingsError)
extern "C"  IntPtr_t _GetSettingsErrorNameFromEnum_Invoke_m1498703935 (_GetSettingsErrorNameFromEnum_t293614055 * __this, int32_t ___eError0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetSettingsErrorNameFromEnum_Invoke_m1498703935((_GetSettingsErrorNameFromEnum_t293614055 *)__this->get_prev_9(),___eError0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef IntPtr_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___eError0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eError0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef IntPtr_t (*FunctionPointerType) (void* __this, int32_t ___eError0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eError0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  IntPtr_t DelegatePInvokeWrapper__GetSettingsErrorNameFromEnum_t293614055 (_GetSettingsErrorNameFromEnum_t293614055 * __this, int32_t ___eError0, const MethodInfo* method)
{
	typedef intptr_t (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc(___eError0);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.IAsyncResult Valve.VR.IVRSettings/_GetSettingsErrorNameFromEnum::BeginInvoke(Valve.VR.EVRSettingsError,System.AsyncCallback,System.Object)
extern Il2CppClass* EVRSettingsError_t4124928198_il2cpp_TypeInfo_var;
extern const uint32_t _GetSettingsErrorNameFromEnum_BeginInvoke_m796991969_MetadataUsageId;
extern "C"  Il2CppObject * _GetSettingsErrorNameFromEnum_BeginInvoke_m796991969 (_GetSettingsErrorNameFromEnum_t293614055 * __this, int32_t ___eError0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetSettingsErrorNameFromEnum_BeginInvoke_m796991969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(EVRSettingsError_t4124928198_il2cpp_TypeInfo_var, &___eError0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.IntPtr Valve.VR.IVRSettings/_GetSettingsErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetSettingsErrorNameFromEnum_EndInvoke_m2302931389 (_GetSettingsErrorNameFromEnum_t293614055 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(IntPtr_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSettings/_GetString::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetString__ctor_m3751110094 (_GetString_t1816180801 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRSettings/_GetString::Invoke(System.String,System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRSettingsError&)
extern "C"  void _GetString_Invoke_m707189712 (_GetString_t1816180801 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unValueLen3, int32_t* ___peError4, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetString_Invoke_m707189712((_GetString_t1816180801 *)__this->get_prev_9(),___pchSection0, ___pchSettingsKey1, ___pchValue2, ___unValueLen3, ___peError4, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unValueLen3, int32_t* ___peError4, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___pchValue2, ___unValueLen3, ___peError4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unValueLen3, int32_t* ___peError4, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___pchValue2, ___unValueLen3, ___peError4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___pchSettingsKey1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unValueLen3, int32_t* ___peError4, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pchSection0, ___pchSettingsKey1, ___pchValue2, ___unValueLen3, ___peError4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__GetString_t1816180801 (_GetString_t1816180801 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unValueLen3, int32_t* ___peError4, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, char*, uint32_t, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchSection0' to native representation
	char* ____pchSection0_marshaled = NULL;
	____pchSection0_marshaled = il2cpp_codegen_marshal_string(___pchSection0);

	// Marshaling of parameter '___pchSettingsKey1' to native representation
	char* ____pchSettingsKey1_marshaled = NULL;
	____pchSettingsKey1_marshaled = il2cpp_codegen_marshal_string(___pchSettingsKey1);

	// Marshaling of parameter '___pchValue2' to native representation
	char* ____pchValue2_marshaled = NULL;
	____pchValue2_marshaled = il2cpp_codegen_marshal_string_builder(___pchValue2);

	// Native function invocation
	il2cppPInvokeFunc(____pchSection0_marshaled, ____pchSettingsKey1_marshaled, ____pchValue2_marshaled, ___unValueLen3, ___peError4);

	// Marshaling cleanup of parameter '___pchSection0' native representation
	il2cpp_codegen_marshal_free(____pchSection0_marshaled);
	____pchSection0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchSettingsKey1' native representation
	il2cpp_codegen_marshal_free(____pchSettingsKey1_marshaled);
	____pchSettingsKey1_marshaled = NULL;

	// Marshaling of parameter '___pchValue2' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___pchValue2, ____pchValue2_marshaled);

	// Marshaling cleanup of parameter '___pchValue2' native representation
	il2cpp_codegen_marshal_free(____pchValue2_marshaled);
	____pchValue2_marshaled = NULL;

}
// System.IAsyncResult Valve.VR.IVRSettings/_GetString::BeginInvoke(System.String,System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* EVRSettingsError_t4124928198_il2cpp_TypeInfo_var;
extern const uint32_t _GetString_BeginInvoke_m3142172733_MetadataUsageId;
extern "C"  Il2CppObject * _GetString_BeginInvoke_m3142172733 (_GetString_t1816180801 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unValueLen3, int32_t* ___peError4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetString_BeginInvoke_m3142172733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = ___pchSection0;
	__d_args[1] = ___pchSettingsKey1;
	__d_args[2] = ___pchValue2;
	__d_args[3] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unValueLen3);
	__d_args[4] = Box(EVRSettingsError_t4124928198_il2cpp_TypeInfo_var, &(*___peError4));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback5, (Il2CppObject*)___object6);
}
// System.Void Valve.VR.IVRSettings/_GetString::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  void _GetString_EndInvoke_m145041772 (_GetString_t1816180801 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___peError0,
	};
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
}
// System.Void Valve.VR.IVRSettings/_RemoveKeyInSection::.ctor(System.Object,System.IntPtr)
extern "C"  void _RemoveKeyInSection__ctor_m3387857040 (_RemoveKeyInSection_t1836506429 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRSettings/_RemoveKeyInSection::Invoke(System.String,System.String,Valve.VR.EVRSettingsError&)
extern "C"  void _RemoveKeyInSection_Invoke_m548050310 (_RemoveKeyInSection_t1836506429 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_RemoveKeyInSection_Invoke_m548050310((_RemoveKeyInSection_t1836506429 *)__this->get_prev_9(),___pchSection0, ___pchSettingsKey1, ___peError2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___peError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___peError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pchSection0, ___pchSettingsKey1, ___peError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__RemoveKeyInSection_t1836506429 (_RemoveKeyInSection_t1836506429 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchSection0' to native representation
	char* ____pchSection0_marshaled = NULL;
	____pchSection0_marshaled = il2cpp_codegen_marshal_string(___pchSection0);

	// Marshaling of parameter '___pchSettingsKey1' to native representation
	char* ____pchSettingsKey1_marshaled = NULL;
	____pchSettingsKey1_marshaled = il2cpp_codegen_marshal_string(___pchSettingsKey1);

	// Native function invocation
	il2cppPInvokeFunc(____pchSection0_marshaled, ____pchSettingsKey1_marshaled, ___peError2);

	// Marshaling cleanup of parameter '___pchSection0' native representation
	il2cpp_codegen_marshal_free(____pchSection0_marshaled);
	____pchSection0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchSettingsKey1' native representation
	il2cpp_codegen_marshal_free(____pchSettingsKey1_marshaled);
	____pchSettingsKey1_marshaled = NULL;

}
// System.IAsyncResult Valve.VR.IVRSettings/_RemoveKeyInSection::BeginInvoke(System.String,System.String,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern Il2CppClass* EVRSettingsError_t4124928198_il2cpp_TypeInfo_var;
extern const uint32_t _RemoveKeyInSection_BeginInvoke_m2025829649_MetadataUsageId;
extern "C"  Il2CppObject * _RemoveKeyInSection_BeginInvoke_m2025829649 (_RemoveKeyInSection_t1836506429 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_RemoveKeyInSection_BeginInvoke_m2025829649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___pchSection0;
	__d_args[1] = ___pchSettingsKey1;
	__d_args[2] = Box(EVRSettingsError_t4124928198_il2cpp_TypeInfo_var, &(*___peError2));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void Valve.VR.IVRSettings/_RemoveKeyInSection::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  void _RemoveKeyInSection_EndInvoke_m4281034706 (_RemoveKeyInSection_t1836506429 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___peError0,
	};
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
}
// System.Void Valve.VR.IVRSettings/_RemoveSection::.ctor(System.Object,System.IntPtr)
extern "C"  void _RemoveSection__ctor_m1143728008 (_RemoveSection_t1493649321 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRSettings/_RemoveSection::Invoke(System.String,Valve.VR.EVRSettingsError&)
extern "C"  void _RemoveSection_Invoke_m2825581582 (_RemoveSection_t1493649321 * __this, String_t* ___pchSection0, int32_t* ___peError1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_RemoveSection_Invoke_m2825581582((_RemoveSection_t1493649321 *)__this->get_prev_9(),___pchSection0, ___peError1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchSection0, int32_t* ___peError1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchSection0, ___peError1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___pchSection0, int32_t* ___peError1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchSection0, ___peError1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t* ___peError1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pchSection0, ___peError1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__RemoveSection_t1493649321 (_RemoveSection_t1493649321 * __this, String_t* ___pchSection0, int32_t* ___peError1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchSection0' to native representation
	char* ____pchSection0_marshaled = NULL;
	____pchSection0_marshaled = il2cpp_codegen_marshal_string(___pchSection0);

	// Native function invocation
	il2cppPInvokeFunc(____pchSection0_marshaled, ___peError1);

	// Marshaling cleanup of parameter '___pchSection0' native representation
	il2cpp_codegen_marshal_free(____pchSection0_marshaled);
	____pchSection0_marshaled = NULL;

}
// System.IAsyncResult Valve.VR.IVRSettings/_RemoveSection::BeginInvoke(System.String,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern Il2CppClass* EVRSettingsError_t4124928198_il2cpp_TypeInfo_var;
extern const uint32_t _RemoveSection_BeginInvoke_m205494627_MetadataUsageId;
extern "C"  Il2CppObject * _RemoveSection_BeginInvoke_m205494627 (_RemoveSection_t1493649321 * __this, String_t* ___pchSection0, int32_t* ___peError1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_RemoveSection_BeginInvoke_m205494627_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___pchSection0;
	__d_args[1] = Box(EVRSettingsError_t4124928198_il2cpp_TypeInfo_var, &(*___peError1));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void Valve.VR.IVRSettings/_RemoveSection::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  void _RemoveSection_EndInvoke_m723905034 (_RemoveSection_t1493649321 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___peError0,
	};
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
}
// System.Void Valve.VR.IVRSettings/_SetBool::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetBool__ctor_m3189774975 (_SetBool_t1033875974 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRSettings/_SetBool::Invoke(System.String,System.String,System.Boolean,Valve.VR.EVRSettingsError&)
extern "C"  void _SetBool_Invoke_m1522142478 (_SetBool_t1033875974 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, bool ___bValue2, int32_t* ___peError3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetBool_Invoke_m1522142478((_SetBool_t1033875974 *)__this->get_prev_9(),___pchSection0, ___pchSettingsKey1, ___bValue2, ___peError3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, bool ___bValue2, int32_t* ___peError3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___bValue2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, bool ___bValue2, int32_t* ___peError3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___bValue2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___pchSettingsKey1, bool ___bValue2, int32_t* ___peError3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pchSection0, ___pchSettingsKey1, ___bValue2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__SetBool_t1033875974 (_SetBool_t1033875974 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, bool ___bValue2, int32_t* ___peError3, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, int32_t, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchSection0' to native representation
	char* ____pchSection0_marshaled = NULL;
	____pchSection0_marshaled = il2cpp_codegen_marshal_string(___pchSection0);

	// Marshaling of parameter '___pchSettingsKey1' to native representation
	char* ____pchSettingsKey1_marshaled = NULL;
	____pchSettingsKey1_marshaled = il2cpp_codegen_marshal_string(___pchSettingsKey1);

	// Native function invocation
	il2cppPInvokeFunc(____pchSection0_marshaled, ____pchSettingsKey1_marshaled, static_cast<int32_t>(___bValue2), ___peError3);

	// Marshaling cleanup of parameter '___pchSection0' native representation
	il2cpp_codegen_marshal_free(____pchSection0_marshaled);
	____pchSection0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchSettingsKey1' native representation
	il2cpp_codegen_marshal_free(____pchSettingsKey1_marshaled);
	____pchSettingsKey1_marshaled = NULL;

}
// System.IAsyncResult Valve.VR.IVRSettings/_SetBool::BeginInvoke(System.String,System.String,System.Boolean,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* EVRSettingsError_t4124928198_il2cpp_TypeInfo_var;
extern const uint32_t _SetBool_BeginInvoke_m1156548325_MetadataUsageId;
extern "C"  Il2CppObject * _SetBool_BeginInvoke_m1156548325 (_SetBool_t1033875974 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, bool ___bValue2, int32_t* ___peError3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetBool_BeginInvoke_m1156548325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___pchSection0;
	__d_args[1] = ___pchSettingsKey1;
	__d_args[2] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___bValue2);
	__d_args[3] = Box(EVRSettingsError_t4124928198_il2cpp_TypeInfo_var, &(*___peError3));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void Valve.VR.IVRSettings/_SetBool::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  void _SetBool_EndInvoke_m3371597907 (_SetBool_t1033875974 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___peError0,
	};
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
}
// System.Void Valve.VR.IVRSettings/_SetFloat::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetFloat__ctor_m3962828909 (_SetFloat_t3724747224 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRSettings/_SetFloat::Invoke(System.String,System.String,System.Single,Valve.VR.EVRSettingsError&)
extern "C"  void _SetFloat_Invoke_m699020166 (_SetFloat_t3724747224 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, float ___flValue2, int32_t* ___peError3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetFloat_Invoke_m699020166((_SetFloat_t3724747224 *)__this->get_prev_9(),___pchSection0, ___pchSettingsKey1, ___flValue2, ___peError3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, float ___flValue2, int32_t* ___peError3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___flValue2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, float ___flValue2, int32_t* ___peError3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___flValue2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___pchSettingsKey1, float ___flValue2, int32_t* ___peError3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pchSection0, ___pchSettingsKey1, ___flValue2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__SetFloat_t3724747224 (_SetFloat_t3724747224 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, float ___flValue2, int32_t* ___peError3, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, float, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchSection0' to native representation
	char* ____pchSection0_marshaled = NULL;
	____pchSection0_marshaled = il2cpp_codegen_marshal_string(___pchSection0);

	// Marshaling of parameter '___pchSettingsKey1' to native representation
	char* ____pchSettingsKey1_marshaled = NULL;
	____pchSettingsKey1_marshaled = il2cpp_codegen_marshal_string(___pchSettingsKey1);

	// Native function invocation
	il2cppPInvokeFunc(____pchSection0_marshaled, ____pchSettingsKey1_marshaled, ___flValue2, ___peError3);

	// Marshaling cleanup of parameter '___pchSection0' native representation
	il2cpp_codegen_marshal_free(____pchSection0_marshaled);
	____pchSection0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchSettingsKey1' native representation
	il2cpp_codegen_marshal_free(____pchSettingsKey1_marshaled);
	____pchSettingsKey1_marshaled = NULL;

}
// System.IAsyncResult Valve.VR.IVRSettings/_SetFloat::BeginInvoke(System.String,System.String,System.Single,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* EVRSettingsError_t4124928198_il2cpp_TypeInfo_var;
extern const uint32_t _SetFloat_BeginInvoke_m559217701_MetadataUsageId;
extern "C"  Il2CppObject * _SetFloat_BeginInvoke_m559217701 (_SetFloat_t3724747224 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, float ___flValue2, int32_t* ___peError3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetFloat_BeginInvoke_m559217701_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___pchSection0;
	__d_args[1] = ___pchSettingsKey1;
	__d_args[2] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___flValue2);
	__d_args[3] = Box(EVRSettingsError_t4124928198_il2cpp_TypeInfo_var, &(*___peError3));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void Valve.VR.IVRSettings/_SetFloat::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  void _SetFloat_EndInvoke_m3662833325 (_SetFloat_t3724747224 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___peError0,
	};
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
}
// System.Void Valve.VR.IVRSettings/_SetInt32::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetInt32__ctor_m90039639 (_SetInt32_t3538080526 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRSettings/_SetInt32::Invoke(System.String,System.String,System.Int32,Valve.VR.EVRSettingsError&)
extern "C"  void _SetInt32_Invoke_m1833108778 (_SetInt32_t3538080526 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t ___nValue2, int32_t* ___peError3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetInt32_Invoke_m1833108778((_SetInt32_t3538080526 *)__this->get_prev_9(),___pchSection0, ___pchSettingsKey1, ___nValue2, ___peError3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t ___nValue2, int32_t* ___peError3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___nValue2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t ___nValue2, int32_t* ___peError3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___nValue2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___pchSettingsKey1, int32_t ___nValue2, int32_t* ___peError3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pchSection0, ___pchSettingsKey1, ___nValue2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__SetInt32_t3538080526 (_SetInt32_t3538080526 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t ___nValue2, int32_t* ___peError3, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, int32_t, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchSection0' to native representation
	char* ____pchSection0_marshaled = NULL;
	____pchSection0_marshaled = il2cpp_codegen_marshal_string(___pchSection0);

	// Marshaling of parameter '___pchSettingsKey1' to native representation
	char* ____pchSettingsKey1_marshaled = NULL;
	____pchSettingsKey1_marshaled = il2cpp_codegen_marshal_string(___pchSettingsKey1);

	// Native function invocation
	il2cppPInvokeFunc(____pchSection0_marshaled, ____pchSettingsKey1_marshaled, ___nValue2, ___peError3);

	// Marshaling cleanup of parameter '___pchSection0' native representation
	il2cpp_codegen_marshal_free(____pchSection0_marshaled);
	____pchSection0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchSettingsKey1' native representation
	il2cpp_codegen_marshal_free(____pchSettingsKey1_marshaled);
	____pchSettingsKey1_marshaled = NULL;

}
// System.IAsyncResult Valve.VR.IVRSettings/_SetInt32::BeginInvoke(System.String,System.String,System.Int32,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* EVRSettingsError_t4124928198_il2cpp_TypeInfo_var;
extern const uint32_t _SetInt32_BeginInvoke_m3820684659_MetadataUsageId;
extern "C"  Il2CppObject * _SetInt32_BeginInvoke_m3820684659 (_SetInt32_t3538080526 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t ___nValue2, int32_t* ___peError3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetInt32_BeginInvoke_m3820684659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___pchSection0;
	__d_args[1] = ___pchSettingsKey1;
	__d_args[2] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___nValue2);
	__d_args[3] = Box(EVRSettingsError_t4124928198_il2cpp_TypeInfo_var, &(*___peError3));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void Valve.VR.IVRSettings/_SetInt32::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  void _SetInt32_EndInvoke_m3072726115 (_SetInt32_t3538080526 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___peError0,
	};
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
}
// System.Void Valve.VR.IVRSettings/_SetString::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetString__ctor_m1567557314 (_SetString_t1793856309 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRSettings/_SetString::Invoke(System.String,System.String,System.String,Valve.VR.EVRSettingsError&)
extern "C"  void _SetString_Invoke_m108682660 (_SetString_t1793856309 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, String_t* ___pchValue2, int32_t* ___peError3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_SetString_Invoke_m108682660((_SetString_t1793856309 *)__this->get_prev_9(),___pchSection0, ___pchSettingsKey1, ___pchValue2, ___peError3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, String_t* ___pchValue2, int32_t* ___peError3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___pchValue2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, String_t* ___pchValue2, int32_t* ___peError3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pchSection0, ___pchSettingsKey1, ___pchValue2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___pchSettingsKey1, String_t* ___pchValue2, int32_t* ___peError3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pchSection0, ___pchSettingsKey1, ___pchValue2, ___peError3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__SetString_t1793856309 (_SetString_t1793856309 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, String_t* ___pchValue2, int32_t* ___peError3, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, char*, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchSection0' to native representation
	char* ____pchSection0_marshaled = NULL;
	____pchSection0_marshaled = il2cpp_codegen_marshal_string(___pchSection0);

	// Marshaling of parameter '___pchSettingsKey1' to native representation
	char* ____pchSettingsKey1_marshaled = NULL;
	____pchSettingsKey1_marshaled = il2cpp_codegen_marshal_string(___pchSettingsKey1);

	// Marshaling of parameter '___pchValue2' to native representation
	char* ____pchValue2_marshaled = NULL;
	____pchValue2_marshaled = il2cpp_codegen_marshal_string(___pchValue2);

	// Native function invocation
	il2cppPInvokeFunc(____pchSection0_marshaled, ____pchSettingsKey1_marshaled, ____pchValue2_marshaled, ___peError3);

	// Marshaling cleanup of parameter '___pchSection0' native representation
	il2cpp_codegen_marshal_free(____pchSection0_marshaled);
	____pchSection0_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchSettingsKey1' native representation
	il2cpp_codegen_marshal_free(____pchSettingsKey1_marshaled);
	____pchSettingsKey1_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchValue2' native representation
	il2cpp_codegen_marshal_free(____pchValue2_marshaled);
	____pchValue2_marshaled = NULL;

}
// System.IAsyncResult Valve.VR.IVRSettings/_SetString::BeginInvoke(System.String,System.String,System.String,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern Il2CppClass* EVRSettingsError_t4124928198_il2cpp_TypeInfo_var;
extern const uint32_t _SetString_BeginInvoke_m2035818935_MetadataUsageId;
extern "C"  Il2CppObject * _SetString_BeginInvoke_m2035818935 (_SetString_t1793856309 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, String_t* ___pchValue2, int32_t* ___peError3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SetString_BeginInvoke_m2035818935_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___pchSection0;
	__d_args[1] = ___pchSettingsKey1;
	__d_args[2] = ___pchValue2;
	__d_args[3] = Box(EVRSettingsError_t4124928198_il2cpp_TypeInfo_var, &(*___peError3));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void Valve.VR.IVRSettings/_SetString::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  void _SetString_EndInvoke_m2697589472 (_SetString_t1793856309 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___peError0,
	};
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
}
// System.Void Valve.VR.IVRSettings/_Sync::.ctor(System.Object,System.IntPtr)
extern "C"  void _Sync__ctor_m3527021068 (_Sync_t2978470277 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean Valve.VR.IVRSettings/_Sync::Invoke(System.Boolean,Valve.VR.EVRSettingsError&)
extern "C"  bool _Sync_Invoke_m4190606357 (_Sync_t2978470277 * __this, bool ___bForce0, int32_t* ___peError1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_Sync_Invoke_m4190606357((_Sync_t2978470277 *)__this->get_prev_9(),___bForce0, ___peError1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___bForce0, int32_t* ___peError1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___bForce0, ___peError1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, bool ___bForce0, int32_t* ___peError1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___bForce0, ___peError1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  bool DelegatePInvokeWrapper__Sync_t2978470277 (_Sync_t2978470277 * __this, bool ___bForce0, int32_t* ___peError1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(int32_t, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(static_cast<int32_t>(___bForce0), ___peError1);

	return static_cast<bool>(returnValue);
}
// System.IAsyncResult Valve.VR.IVRSettings/_Sync::BeginInvoke(System.Boolean,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* EVRSettingsError_t4124928198_il2cpp_TypeInfo_var;
extern const uint32_t _Sync_BeginInvoke_m1842647256_MetadataUsageId;
extern "C"  Il2CppObject * _Sync_BeginInvoke_m1842647256 (_Sync_t2978470277 * __this, bool ___bForce0, int32_t* ___peError1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_Sync_BeginInvoke_m1842647256_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___bForce0);
	__d_args[1] = Box(EVRSettingsError_t4124928198_il2cpp_TypeInfo_var, &(*___peError1));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Boolean Valve.VR.IVRSettings/_Sync::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  bool _Sync_EndInvoke_m2579371820 (_Sync_t2978470277 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___peError0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Conversion methods for marshalling of: Valve.VR.IVRSystem
extern "C" void IVRSystem_t3365196000_marshal_pinvoke(const IVRSystem_t3365196000& unmarshaled, IVRSystem_t3365196000_marshaled_pinvoke& marshaled)
{
	marshaled.___GetRecommendedRenderTargetSize_0 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetRecommendedRenderTargetSize_0()));
	marshaled.___GetProjectionMatrix_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetProjectionMatrix_1()));
	marshaled.___GetProjectionRaw_2 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetProjectionRaw_2()));
	marshaled.___ComputeDistortion_3 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_ComputeDistortion_3()));
	marshaled.___GetEyeToHeadTransform_4 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetEyeToHeadTransform_4()));
	marshaled.___GetTimeSinceLastVsync_5 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetTimeSinceLastVsync_5()));
	marshaled.___GetD3D9AdapterIndex_6 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetD3D9AdapterIndex_6()));
	marshaled.___GetDXGIOutputInfo_7 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetDXGIOutputInfo_7()));
	marshaled.___IsDisplayOnDesktop_8 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_IsDisplayOnDesktop_8()));
	marshaled.___SetDisplayVisibility_9 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_SetDisplayVisibility_9()));
	marshaled.___GetDeviceToAbsoluteTrackingPose_10 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetDeviceToAbsoluteTrackingPose_10()));
	marshaled.___ResetSeatedZeroPose_11 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_ResetSeatedZeroPose_11()));
	marshaled.___GetSeatedZeroPoseToStandingAbsoluteTrackingPose_12 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_12()));
	marshaled.___GetRawZeroPoseToStandingAbsoluteTrackingPose_13 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetRawZeroPoseToStandingAbsoluteTrackingPose_13()));
	marshaled.___GetSortedTrackedDeviceIndicesOfClass_14 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetSortedTrackedDeviceIndicesOfClass_14()));
	marshaled.___GetTrackedDeviceActivityLevel_15 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetTrackedDeviceActivityLevel_15()));
	marshaled.___ApplyTransform_16 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_ApplyTransform_16()));
	marshaled.___GetTrackedDeviceIndexForControllerRole_17 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetTrackedDeviceIndexForControllerRole_17()));
	marshaled.___GetControllerRoleForTrackedDeviceIndex_18 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetControllerRoleForTrackedDeviceIndex_18()));
	marshaled.___GetTrackedDeviceClass_19 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetTrackedDeviceClass_19()));
	marshaled.___IsTrackedDeviceConnected_20 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_IsTrackedDeviceConnected_20()));
	marshaled.___GetBoolTrackedDeviceProperty_21 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetBoolTrackedDeviceProperty_21()));
	marshaled.___GetFloatTrackedDeviceProperty_22 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetFloatTrackedDeviceProperty_22()));
	marshaled.___GetInt32TrackedDeviceProperty_23 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetInt32TrackedDeviceProperty_23()));
	marshaled.___GetUint64TrackedDeviceProperty_24 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetUint64TrackedDeviceProperty_24()));
	marshaled.___GetMatrix34TrackedDeviceProperty_25 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetMatrix34TrackedDeviceProperty_25()));
	marshaled.___GetStringTrackedDeviceProperty_26 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetStringTrackedDeviceProperty_26()));
	marshaled.___GetPropErrorNameFromEnum_27 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetPropErrorNameFromEnum_27()));
	marshaled.___PollNextEvent_28 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_PollNextEvent_28()));
	marshaled.___PollNextEventWithPose_29 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_PollNextEventWithPose_29()));
	marshaled.___GetEventTypeNameFromEnum_30 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetEventTypeNameFromEnum_30()));
	marshaled.___GetHiddenAreaMesh_31 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetHiddenAreaMesh_31()));
	marshaled.___GetControllerState_32 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetControllerState_32()));
	marshaled.___GetControllerStateWithPose_33 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetControllerStateWithPose_33()));
	marshaled.___TriggerHapticPulse_34 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_TriggerHapticPulse_34()));
	marshaled.___GetButtonIdNameFromEnum_35 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetButtonIdNameFromEnum_35()));
	marshaled.___GetControllerAxisTypeNameFromEnum_36 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetControllerAxisTypeNameFromEnum_36()));
	marshaled.___CaptureInputFocus_37 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_CaptureInputFocus_37()));
	marshaled.___ReleaseInputFocus_38 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_ReleaseInputFocus_38()));
	marshaled.___IsInputFocusCapturedByAnotherProcess_39 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_IsInputFocusCapturedByAnotherProcess_39()));
	marshaled.___DriverDebugRequest_40 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_DriverDebugRequest_40()));
	marshaled.___PerformFirmwareUpdate_41 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_PerformFirmwareUpdate_41()));
	marshaled.___AcknowledgeQuit_Exiting_42 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_AcknowledgeQuit_Exiting_42()));
	marshaled.___AcknowledgeQuit_UserPrompt_43 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_AcknowledgeQuit_UserPrompt_43()));
}
extern Il2CppClass* _GetRecommendedRenderTargetSize_t4195542627_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetProjectionMatrix_t2621141914_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetProjectionRaw_t3426995441_il2cpp_TypeInfo_var;
extern Il2CppClass* _ComputeDistortion_t3576284924_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetEyeToHeadTransform_t3057184772_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetTimeSinceLastVsync_t1215702688_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetD3D9AdapterIndex_t4234979703_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetDXGIOutputInfo_t1897151767_il2cpp_TypeInfo_var;
extern Il2CppClass* _IsDisplayOnDesktop_t2551312917_il2cpp_TypeInfo_var;
extern Il2CppClass* _SetDisplayVisibility_t3986281708_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetDeviceToAbsoluteTrackingPose_t1432625068_il2cpp_TypeInfo_var;
extern Il2CppClass* _ResetSeatedZeroPose_t3471614486_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetSortedTrackedDeviceIndicesOfClass_t3492202929_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetTrackedDeviceActivityLevel_t212130385_il2cpp_TypeInfo_var;
extern Il2CppClass* _ApplyTransform_t1439808290_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetTrackedDeviceIndexForControllerRole_t3232960147_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetControllerRoleForTrackedDeviceIndex_t1728202579_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetTrackedDeviceClass_t1455580370_il2cpp_TypeInfo_var;
extern Il2CppClass* _IsTrackedDeviceConnected_t459208129_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetBoolTrackedDeviceProperty_t2236257287_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetFloatTrackedDeviceProperty_t1406950913_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetInt32TrackedDeviceProperty_t2396289227_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetUint64TrackedDeviceProperty_t537540785_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetMatrix34TrackedDeviceProperty_t3426445457_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetStringTrackedDeviceProperty_t87797800_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetPropErrorNameFromEnum_t1193025139_il2cpp_TypeInfo_var;
extern Il2CppClass* _PollNextEvent_t3908295690_il2cpp_TypeInfo_var;
extern Il2CppClass* _PollNextEventWithPose_t2759121141_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetEventTypeNameFromEnum_t1950138544_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetHiddenAreaMesh_t1813422502_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetControllerState_t3891090487_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetControllerStateWithPose_t4079915850_il2cpp_TypeInfo_var;
extern Il2CppClass* _TriggerHapticPulse_t158863722_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetButtonIdNameFromEnum_t195009473_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetControllerAxisTypeNameFromEnum_t3568402941_il2cpp_TypeInfo_var;
extern Il2CppClass* _CaptureInputFocus_t2994096092_il2cpp_TypeInfo_var;
extern Il2CppClass* _ReleaseInputFocus_t580725753_il2cpp_TypeInfo_var;
extern Il2CppClass* _IsInputFocusCapturedByAnotherProcess_t84136089_il2cpp_TypeInfo_var;
extern Il2CppClass* _DriverDebugRequest_t4049208724_il2cpp_TypeInfo_var;
extern Il2CppClass* _PerformFirmwareUpdate_t673402879_il2cpp_TypeInfo_var;
extern Il2CppClass* _AcknowledgeQuit_Exiting_t1109677234_il2cpp_TypeInfo_var;
extern Il2CppClass* _AcknowledgeQuit_UserPrompt_t90686541_il2cpp_TypeInfo_var;
extern const uint32_t IVRSystem_t3365196000_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void IVRSystem_t3365196000_marshal_pinvoke_back(const IVRSystem_t3365196000_marshaled_pinvoke& marshaled, IVRSystem_t3365196000& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVRSystem_t3365196000_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_GetRecommendedRenderTargetSize_0(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetRecommendedRenderTargetSize_t4195542627>(marshaled.___GetRecommendedRenderTargetSize_0, _GetRecommendedRenderTargetSize_t4195542627_il2cpp_TypeInfo_var));
	unmarshaled.set_GetProjectionMatrix_1(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetProjectionMatrix_t2621141914>(marshaled.___GetProjectionMatrix_1, _GetProjectionMatrix_t2621141914_il2cpp_TypeInfo_var));
	unmarshaled.set_GetProjectionRaw_2(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetProjectionRaw_t3426995441>(marshaled.___GetProjectionRaw_2, _GetProjectionRaw_t3426995441_il2cpp_TypeInfo_var));
	unmarshaled.set_ComputeDistortion_3(il2cpp_codegen_marshal_function_ptr_to_delegate<_ComputeDistortion_t3576284924>(marshaled.___ComputeDistortion_3, _ComputeDistortion_t3576284924_il2cpp_TypeInfo_var));
	unmarshaled.set_GetEyeToHeadTransform_4(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetEyeToHeadTransform_t3057184772>(marshaled.___GetEyeToHeadTransform_4, _GetEyeToHeadTransform_t3057184772_il2cpp_TypeInfo_var));
	unmarshaled.set_GetTimeSinceLastVsync_5(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetTimeSinceLastVsync_t1215702688>(marshaled.___GetTimeSinceLastVsync_5, _GetTimeSinceLastVsync_t1215702688_il2cpp_TypeInfo_var));
	unmarshaled.set_GetD3D9AdapterIndex_6(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetD3D9AdapterIndex_t4234979703>(marshaled.___GetD3D9AdapterIndex_6, _GetD3D9AdapterIndex_t4234979703_il2cpp_TypeInfo_var));
	unmarshaled.set_GetDXGIOutputInfo_7(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetDXGIOutputInfo_t1897151767>(marshaled.___GetDXGIOutputInfo_7, _GetDXGIOutputInfo_t1897151767_il2cpp_TypeInfo_var));
	unmarshaled.set_IsDisplayOnDesktop_8(il2cpp_codegen_marshal_function_ptr_to_delegate<_IsDisplayOnDesktop_t2551312917>(marshaled.___IsDisplayOnDesktop_8, _IsDisplayOnDesktop_t2551312917_il2cpp_TypeInfo_var));
	unmarshaled.set_SetDisplayVisibility_9(il2cpp_codegen_marshal_function_ptr_to_delegate<_SetDisplayVisibility_t3986281708>(marshaled.___SetDisplayVisibility_9, _SetDisplayVisibility_t3986281708_il2cpp_TypeInfo_var));
	unmarshaled.set_GetDeviceToAbsoluteTrackingPose_10(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetDeviceToAbsoluteTrackingPose_t1432625068>(marshaled.___GetDeviceToAbsoluteTrackingPose_10, _GetDeviceToAbsoluteTrackingPose_t1432625068_il2cpp_TypeInfo_var));
	unmarshaled.set_ResetSeatedZeroPose_11(il2cpp_codegen_marshal_function_ptr_to_delegate<_ResetSeatedZeroPose_t3471614486>(marshaled.___ResetSeatedZeroPose_11, _ResetSeatedZeroPose_t3471614486_il2cpp_TypeInfo_var));
	unmarshaled.set_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_12(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835>(marshaled.___GetSeatedZeroPoseToStandingAbsoluteTrackingPose_12, _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835_il2cpp_TypeInfo_var));
	unmarshaled.set_GetRawZeroPoseToStandingAbsoluteTrackingPose_13(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273>(marshaled.___GetRawZeroPoseToStandingAbsoluteTrackingPose_13, _GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273_il2cpp_TypeInfo_var));
	unmarshaled.set_GetSortedTrackedDeviceIndicesOfClass_14(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetSortedTrackedDeviceIndicesOfClass_t3492202929>(marshaled.___GetSortedTrackedDeviceIndicesOfClass_14, _GetSortedTrackedDeviceIndicesOfClass_t3492202929_il2cpp_TypeInfo_var));
	unmarshaled.set_GetTrackedDeviceActivityLevel_15(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetTrackedDeviceActivityLevel_t212130385>(marshaled.___GetTrackedDeviceActivityLevel_15, _GetTrackedDeviceActivityLevel_t212130385_il2cpp_TypeInfo_var));
	unmarshaled.set_ApplyTransform_16(il2cpp_codegen_marshal_function_ptr_to_delegate<_ApplyTransform_t1439808290>(marshaled.___ApplyTransform_16, _ApplyTransform_t1439808290_il2cpp_TypeInfo_var));
	unmarshaled.set_GetTrackedDeviceIndexForControllerRole_17(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetTrackedDeviceIndexForControllerRole_t3232960147>(marshaled.___GetTrackedDeviceIndexForControllerRole_17, _GetTrackedDeviceIndexForControllerRole_t3232960147_il2cpp_TypeInfo_var));
	unmarshaled.set_GetControllerRoleForTrackedDeviceIndex_18(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetControllerRoleForTrackedDeviceIndex_t1728202579>(marshaled.___GetControllerRoleForTrackedDeviceIndex_18, _GetControllerRoleForTrackedDeviceIndex_t1728202579_il2cpp_TypeInfo_var));
	unmarshaled.set_GetTrackedDeviceClass_19(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetTrackedDeviceClass_t1455580370>(marshaled.___GetTrackedDeviceClass_19, _GetTrackedDeviceClass_t1455580370_il2cpp_TypeInfo_var));
	unmarshaled.set_IsTrackedDeviceConnected_20(il2cpp_codegen_marshal_function_ptr_to_delegate<_IsTrackedDeviceConnected_t459208129>(marshaled.___IsTrackedDeviceConnected_20, _IsTrackedDeviceConnected_t459208129_il2cpp_TypeInfo_var));
	unmarshaled.set_GetBoolTrackedDeviceProperty_21(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetBoolTrackedDeviceProperty_t2236257287>(marshaled.___GetBoolTrackedDeviceProperty_21, _GetBoolTrackedDeviceProperty_t2236257287_il2cpp_TypeInfo_var));
	unmarshaled.set_GetFloatTrackedDeviceProperty_22(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetFloatTrackedDeviceProperty_t1406950913>(marshaled.___GetFloatTrackedDeviceProperty_22, _GetFloatTrackedDeviceProperty_t1406950913_il2cpp_TypeInfo_var));
	unmarshaled.set_GetInt32TrackedDeviceProperty_23(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetInt32TrackedDeviceProperty_t2396289227>(marshaled.___GetInt32TrackedDeviceProperty_23, _GetInt32TrackedDeviceProperty_t2396289227_il2cpp_TypeInfo_var));
	unmarshaled.set_GetUint64TrackedDeviceProperty_24(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetUint64TrackedDeviceProperty_t537540785>(marshaled.___GetUint64TrackedDeviceProperty_24, _GetUint64TrackedDeviceProperty_t537540785_il2cpp_TypeInfo_var));
	unmarshaled.set_GetMatrix34TrackedDeviceProperty_25(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetMatrix34TrackedDeviceProperty_t3426445457>(marshaled.___GetMatrix34TrackedDeviceProperty_25, _GetMatrix34TrackedDeviceProperty_t3426445457_il2cpp_TypeInfo_var));
	unmarshaled.set_GetStringTrackedDeviceProperty_26(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetStringTrackedDeviceProperty_t87797800>(marshaled.___GetStringTrackedDeviceProperty_26, _GetStringTrackedDeviceProperty_t87797800_il2cpp_TypeInfo_var));
	unmarshaled.set_GetPropErrorNameFromEnum_27(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetPropErrorNameFromEnum_t1193025139>(marshaled.___GetPropErrorNameFromEnum_27, _GetPropErrorNameFromEnum_t1193025139_il2cpp_TypeInfo_var));
	unmarshaled.set_PollNextEvent_28(il2cpp_codegen_marshal_function_ptr_to_delegate<_PollNextEvent_t3908295690>(marshaled.___PollNextEvent_28, _PollNextEvent_t3908295690_il2cpp_TypeInfo_var));
	unmarshaled.set_PollNextEventWithPose_29(il2cpp_codegen_marshal_function_ptr_to_delegate<_PollNextEventWithPose_t2759121141>(marshaled.___PollNextEventWithPose_29, _PollNextEventWithPose_t2759121141_il2cpp_TypeInfo_var));
	unmarshaled.set_GetEventTypeNameFromEnum_30(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetEventTypeNameFromEnum_t1950138544>(marshaled.___GetEventTypeNameFromEnum_30, _GetEventTypeNameFromEnum_t1950138544_il2cpp_TypeInfo_var));
	unmarshaled.set_GetHiddenAreaMesh_31(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetHiddenAreaMesh_t1813422502>(marshaled.___GetHiddenAreaMesh_31, _GetHiddenAreaMesh_t1813422502_il2cpp_TypeInfo_var));
	unmarshaled.set_GetControllerState_32(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetControllerState_t3891090487>(marshaled.___GetControllerState_32, _GetControllerState_t3891090487_il2cpp_TypeInfo_var));
	unmarshaled.set_GetControllerStateWithPose_33(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetControllerStateWithPose_t4079915850>(marshaled.___GetControllerStateWithPose_33, _GetControllerStateWithPose_t4079915850_il2cpp_TypeInfo_var));
	unmarshaled.set_TriggerHapticPulse_34(il2cpp_codegen_marshal_function_ptr_to_delegate<_TriggerHapticPulse_t158863722>(marshaled.___TriggerHapticPulse_34, _TriggerHapticPulse_t158863722_il2cpp_TypeInfo_var));
	unmarshaled.set_GetButtonIdNameFromEnum_35(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetButtonIdNameFromEnum_t195009473>(marshaled.___GetButtonIdNameFromEnum_35, _GetButtonIdNameFromEnum_t195009473_il2cpp_TypeInfo_var));
	unmarshaled.set_GetControllerAxisTypeNameFromEnum_36(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetControllerAxisTypeNameFromEnum_t3568402941>(marshaled.___GetControllerAxisTypeNameFromEnum_36, _GetControllerAxisTypeNameFromEnum_t3568402941_il2cpp_TypeInfo_var));
	unmarshaled.set_CaptureInputFocus_37(il2cpp_codegen_marshal_function_ptr_to_delegate<_CaptureInputFocus_t2994096092>(marshaled.___CaptureInputFocus_37, _CaptureInputFocus_t2994096092_il2cpp_TypeInfo_var));
	unmarshaled.set_ReleaseInputFocus_38(il2cpp_codegen_marshal_function_ptr_to_delegate<_ReleaseInputFocus_t580725753>(marshaled.___ReleaseInputFocus_38, _ReleaseInputFocus_t580725753_il2cpp_TypeInfo_var));
	unmarshaled.set_IsInputFocusCapturedByAnotherProcess_39(il2cpp_codegen_marshal_function_ptr_to_delegate<_IsInputFocusCapturedByAnotherProcess_t84136089>(marshaled.___IsInputFocusCapturedByAnotherProcess_39, _IsInputFocusCapturedByAnotherProcess_t84136089_il2cpp_TypeInfo_var));
	unmarshaled.set_DriverDebugRequest_40(il2cpp_codegen_marshal_function_ptr_to_delegate<_DriverDebugRequest_t4049208724>(marshaled.___DriverDebugRequest_40, _DriverDebugRequest_t4049208724_il2cpp_TypeInfo_var));
	unmarshaled.set_PerformFirmwareUpdate_41(il2cpp_codegen_marshal_function_ptr_to_delegate<_PerformFirmwareUpdate_t673402879>(marshaled.___PerformFirmwareUpdate_41, _PerformFirmwareUpdate_t673402879_il2cpp_TypeInfo_var));
	unmarshaled.set_AcknowledgeQuit_Exiting_42(il2cpp_codegen_marshal_function_ptr_to_delegate<_AcknowledgeQuit_Exiting_t1109677234>(marshaled.___AcknowledgeQuit_Exiting_42, _AcknowledgeQuit_Exiting_t1109677234_il2cpp_TypeInfo_var));
	unmarshaled.set_AcknowledgeQuit_UserPrompt_43(il2cpp_codegen_marshal_function_ptr_to_delegate<_AcknowledgeQuit_UserPrompt_t90686541>(marshaled.___AcknowledgeQuit_UserPrompt_43, _AcknowledgeQuit_UserPrompt_t90686541_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: Valve.VR.IVRSystem
extern "C" void IVRSystem_t3365196000_marshal_pinvoke_cleanup(IVRSystem_t3365196000_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Valve.VR.IVRSystem
extern "C" void IVRSystem_t3365196000_marshal_com(const IVRSystem_t3365196000& unmarshaled, IVRSystem_t3365196000_marshaled_com& marshaled)
{
	marshaled.___GetRecommendedRenderTargetSize_0 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetRecommendedRenderTargetSize_0()));
	marshaled.___GetProjectionMatrix_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetProjectionMatrix_1()));
	marshaled.___GetProjectionRaw_2 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetProjectionRaw_2()));
	marshaled.___ComputeDistortion_3 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_ComputeDistortion_3()));
	marshaled.___GetEyeToHeadTransform_4 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetEyeToHeadTransform_4()));
	marshaled.___GetTimeSinceLastVsync_5 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetTimeSinceLastVsync_5()));
	marshaled.___GetD3D9AdapterIndex_6 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetD3D9AdapterIndex_6()));
	marshaled.___GetDXGIOutputInfo_7 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetDXGIOutputInfo_7()));
	marshaled.___IsDisplayOnDesktop_8 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_IsDisplayOnDesktop_8()));
	marshaled.___SetDisplayVisibility_9 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_SetDisplayVisibility_9()));
	marshaled.___GetDeviceToAbsoluteTrackingPose_10 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetDeviceToAbsoluteTrackingPose_10()));
	marshaled.___ResetSeatedZeroPose_11 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_ResetSeatedZeroPose_11()));
	marshaled.___GetSeatedZeroPoseToStandingAbsoluteTrackingPose_12 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_12()));
	marshaled.___GetRawZeroPoseToStandingAbsoluteTrackingPose_13 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetRawZeroPoseToStandingAbsoluteTrackingPose_13()));
	marshaled.___GetSortedTrackedDeviceIndicesOfClass_14 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetSortedTrackedDeviceIndicesOfClass_14()));
	marshaled.___GetTrackedDeviceActivityLevel_15 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetTrackedDeviceActivityLevel_15()));
	marshaled.___ApplyTransform_16 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_ApplyTransform_16()));
	marshaled.___GetTrackedDeviceIndexForControllerRole_17 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetTrackedDeviceIndexForControllerRole_17()));
	marshaled.___GetControllerRoleForTrackedDeviceIndex_18 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetControllerRoleForTrackedDeviceIndex_18()));
	marshaled.___GetTrackedDeviceClass_19 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetTrackedDeviceClass_19()));
	marshaled.___IsTrackedDeviceConnected_20 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_IsTrackedDeviceConnected_20()));
	marshaled.___GetBoolTrackedDeviceProperty_21 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetBoolTrackedDeviceProperty_21()));
	marshaled.___GetFloatTrackedDeviceProperty_22 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetFloatTrackedDeviceProperty_22()));
	marshaled.___GetInt32TrackedDeviceProperty_23 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetInt32TrackedDeviceProperty_23()));
	marshaled.___GetUint64TrackedDeviceProperty_24 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetUint64TrackedDeviceProperty_24()));
	marshaled.___GetMatrix34TrackedDeviceProperty_25 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetMatrix34TrackedDeviceProperty_25()));
	marshaled.___GetStringTrackedDeviceProperty_26 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetStringTrackedDeviceProperty_26()));
	marshaled.___GetPropErrorNameFromEnum_27 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetPropErrorNameFromEnum_27()));
	marshaled.___PollNextEvent_28 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_PollNextEvent_28()));
	marshaled.___PollNextEventWithPose_29 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_PollNextEventWithPose_29()));
	marshaled.___GetEventTypeNameFromEnum_30 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetEventTypeNameFromEnum_30()));
	marshaled.___GetHiddenAreaMesh_31 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetHiddenAreaMesh_31()));
	marshaled.___GetControllerState_32 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetControllerState_32()));
	marshaled.___GetControllerStateWithPose_33 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetControllerStateWithPose_33()));
	marshaled.___TriggerHapticPulse_34 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_TriggerHapticPulse_34()));
	marshaled.___GetButtonIdNameFromEnum_35 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetButtonIdNameFromEnum_35()));
	marshaled.___GetControllerAxisTypeNameFromEnum_36 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_GetControllerAxisTypeNameFromEnum_36()));
	marshaled.___CaptureInputFocus_37 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_CaptureInputFocus_37()));
	marshaled.___ReleaseInputFocus_38 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_ReleaseInputFocus_38()));
	marshaled.___IsInputFocusCapturedByAnotherProcess_39 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_IsInputFocusCapturedByAnotherProcess_39()));
	marshaled.___DriverDebugRequest_40 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_DriverDebugRequest_40()));
	marshaled.___PerformFirmwareUpdate_41 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_PerformFirmwareUpdate_41()));
	marshaled.___AcknowledgeQuit_Exiting_42 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_AcknowledgeQuit_Exiting_42()));
	marshaled.___AcknowledgeQuit_UserPrompt_43 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_AcknowledgeQuit_UserPrompt_43()));
}
extern Il2CppClass* _GetRecommendedRenderTargetSize_t4195542627_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetProjectionMatrix_t2621141914_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetProjectionRaw_t3426995441_il2cpp_TypeInfo_var;
extern Il2CppClass* _ComputeDistortion_t3576284924_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetEyeToHeadTransform_t3057184772_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetTimeSinceLastVsync_t1215702688_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetD3D9AdapterIndex_t4234979703_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetDXGIOutputInfo_t1897151767_il2cpp_TypeInfo_var;
extern Il2CppClass* _IsDisplayOnDesktop_t2551312917_il2cpp_TypeInfo_var;
extern Il2CppClass* _SetDisplayVisibility_t3986281708_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetDeviceToAbsoluteTrackingPose_t1432625068_il2cpp_TypeInfo_var;
extern Il2CppClass* _ResetSeatedZeroPose_t3471614486_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetSortedTrackedDeviceIndicesOfClass_t3492202929_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetTrackedDeviceActivityLevel_t212130385_il2cpp_TypeInfo_var;
extern Il2CppClass* _ApplyTransform_t1439808290_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetTrackedDeviceIndexForControllerRole_t3232960147_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetControllerRoleForTrackedDeviceIndex_t1728202579_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetTrackedDeviceClass_t1455580370_il2cpp_TypeInfo_var;
extern Il2CppClass* _IsTrackedDeviceConnected_t459208129_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetBoolTrackedDeviceProperty_t2236257287_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetFloatTrackedDeviceProperty_t1406950913_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetInt32TrackedDeviceProperty_t2396289227_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetUint64TrackedDeviceProperty_t537540785_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetMatrix34TrackedDeviceProperty_t3426445457_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetStringTrackedDeviceProperty_t87797800_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetPropErrorNameFromEnum_t1193025139_il2cpp_TypeInfo_var;
extern Il2CppClass* _PollNextEvent_t3908295690_il2cpp_TypeInfo_var;
extern Il2CppClass* _PollNextEventWithPose_t2759121141_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetEventTypeNameFromEnum_t1950138544_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetHiddenAreaMesh_t1813422502_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetControllerState_t3891090487_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetControllerStateWithPose_t4079915850_il2cpp_TypeInfo_var;
extern Il2CppClass* _TriggerHapticPulse_t158863722_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetButtonIdNameFromEnum_t195009473_il2cpp_TypeInfo_var;
extern Il2CppClass* _GetControllerAxisTypeNameFromEnum_t3568402941_il2cpp_TypeInfo_var;
extern Il2CppClass* _CaptureInputFocus_t2994096092_il2cpp_TypeInfo_var;
extern Il2CppClass* _ReleaseInputFocus_t580725753_il2cpp_TypeInfo_var;
extern Il2CppClass* _IsInputFocusCapturedByAnotherProcess_t84136089_il2cpp_TypeInfo_var;
extern Il2CppClass* _DriverDebugRequest_t4049208724_il2cpp_TypeInfo_var;
extern Il2CppClass* _PerformFirmwareUpdate_t673402879_il2cpp_TypeInfo_var;
extern Il2CppClass* _AcknowledgeQuit_Exiting_t1109677234_il2cpp_TypeInfo_var;
extern Il2CppClass* _AcknowledgeQuit_UserPrompt_t90686541_il2cpp_TypeInfo_var;
extern const uint32_t IVRSystem_t3365196000_com_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void IVRSystem_t3365196000_marshal_com_back(const IVRSystem_t3365196000_marshaled_com& marshaled, IVRSystem_t3365196000& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVRSystem_t3365196000_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_GetRecommendedRenderTargetSize_0(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetRecommendedRenderTargetSize_t4195542627>(marshaled.___GetRecommendedRenderTargetSize_0, _GetRecommendedRenderTargetSize_t4195542627_il2cpp_TypeInfo_var));
	unmarshaled.set_GetProjectionMatrix_1(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetProjectionMatrix_t2621141914>(marshaled.___GetProjectionMatrix_1, _GetProjectionMatrix_t2621141914_il2cpp_TypeInfo_var));
	unmarshaled.set_GetProjectionRaw_2(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetProjectionRaw_t3426995441>(marshaled.___GetProjectionRaw_2, _GetProjectionRaw_t3426995441_il2cpp_TypeInfo_var));
	unmarshaled.set_ComputeDistortion_3(il2cpp_codegen_marshal_function_ptr_to_delegate<_ComputeDistortion_t3576284924>(marshaled.___ComputeDistortion_3, _ComputeDistortion_t3576284924_il2cpp_TypeInfo_var));
	unmarshaled.set_GetEyeToHeadTransform_4(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetEyeToHeadTransform_t3057184772>(marshaled.___GetEyeToHeadTransform_4, _GetEyeToHeadTransform_t3057184772_il2cpp_TypeInfo_var));
	unmarshaled.set_GetTimeSinceLastVsync_5(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetTimeSinceLastVsync_t1215702688>(marshaled.___GetTimeSinceLastVsync_5, _GetTimeSinceLastVsync_t1215702688_il2cpp_TypeInfo_var));
	unmarshaled.set_GetD3D9AdapterIndex_6(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetD3D9AdapterIndex_t4234979703>(marshaled.___GetD3D9AdapterIndex_6, _GetD3D9AdapterIndex_t4234979703_il2cpp_TypeInfo_var));
	unmarshaled.set_GetDXGIOutputInfo_7(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetDXGIOutputInfo_t1897151767>(marshaled.___GetDXGIOutputInfo_7, _GetDXGIOutputInfo_t1897151767_il2cpp_TypeInfo_var));
	unmarshaled.set_IsDisplayOnDesktop_8(il2cpp_codegen_marshal_function_ptr_to_delegate<_IsDisplayOnDesktop_t2551312917>(marshaled.___IsDisplayOnDesktop_8, _IsDisplayOnDesktop_t2551312917_il2cpp_TypeInfo_var));
	unmarshaled.set_SetDisplayVisibility_9(il2cpp_codegen_marshal_function_ptr_to_delegate<_SetDisplayVisibility_t3986281708>(marshaled.___SetDisplayVisibility_9, _SetDisplayVisibility_t3986281708_il2cpp_TypeInfo_var));
	unmarshaled.set_GetDeviceToAbsoluteTrackingPose_10(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetDeviceToAbsoluteTrackingPose_t1432625068>(marshaled.___GetDeviceToAbsoluteTrackingPose_10, _GetDeviceToAbsoluteTrackingPose_t1432625068_il2cpp_TypeInfo_var));
	unmarshaled.set_ResetSeatedZeroPose_11(il2cpp_codegen_marshal_function_ptr_to_delegate<_ResetSeatedZeroPose_t3471614486>(marshaled.___ResetSeatedZeroPose_11, _ResetSeatedZeroPose_t3471614486_il2cpp_TypeInfo_var));
	unmarshaled.set_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_12(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835>(marshaled.___GetSeatedZeroPoseToStandingAbsoluteTrackingPose_12, _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835_il2cpp_TypeInfo_var));
	unmarshaled.set_GetRawZeroPoseToStandingAbsoluteTrackingPose_13(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273>(marshaled.___GetRawZeroPoseToStandingAbsoluteTrackingPose_13, _GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273_il2cpp_TypeInfo_var));
	unmarshaled.set_GetSortedTrackedDeviceIndicesOfClass_14(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetSortedTrackedDeviceIndicesOfClass_t3492202929>(marshaled.___GetSortedTrackedDeviceIndicesOfClass_14, _GetSortedTrackedDeviceIndicesOfClass_t3492202929_il2cpp_TypeInfo_var));
	unmarshaled.set_GetTrackedDeviceActivityLevel_15(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetTrackedDeviceActivityLevel_t212130385>(marshaled.___GetTrackedDeviceActivityLevel_15, _GetTrackedDeviceActivityLevel_t212130385_il2cpp_TypeInfo_var));
	unmarshaled.set_ApplyTransform_16(il2cpp_codegen_marshal_function_ptr_to_delegate<_ApplyTransform_t1439808290>(marshaled.___ApplyTransform_16, _ApplyTransform_t1439808290_il2cpp_TypeInfo_var));
	unmarshaled.set_GetTrackedDeviceIndexForControllerRole_17(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetTrackedDeviceIndexForControllerRole_t3232960147>(marshaled.___GetTrackedDeviceIndexForControllerRole_17, _GetTrackedDeviceIndexForControllerRole_t3232960147_il2cpp_TypeInfo_var));
	unmarshaled.set_GetControllerRoleForTrackedDeviceIndex_18(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetControllerRoleForTrackedDeviceIndex_t1728202579>(marshaled.___GetControllerRoleForTrackedDeviceIndex_18, _GetControllerRoleForTrackedDeviceIndex_t1728202579_il2cpp_TypeInfo_var));
	unmarshaled.set_GetTrackedDeviceClass_19(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetTrackedDeviceClass_t1455580370>(marshaled.___GetTrackedDeviceClass_19, _GetTrackedDeviceClass_t1455580370_il2cpp_TypeInfo_var));
	unmarshaled.set_IsTrackedDeviceConnected_20(il2cpp_codegen_marshal_function_ptr_to_delegate<_IsTrackedDeviceConnected_t459208129>(marshaled.___IsTrackedDeviceConnected_20, _IsTrackedDeviceConnected_t459208129_il2cpp_TypeInfo_var));
	unmarshaled.set_GetBoolTrackedDeviceProperty_21(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetBoolTrackedDeviceProperty_t2236257287>(marshaled.___GetBoolTrackedDeviceProperty_21, _GetBoolTrackedDeviceProperty_t2236257287_il2cpp_TypeInfo_var));
	unmarshaled.set_GetFloatTrackedDeviceProperty_22(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetFloatTrackedDeviceProperty_t1406950913>(marshaled.___GetFloatTrackedDeviceProperty_22, _GetFloatTrackedDeviceProperty_t1406950913_il2cpp_TypeInfo_var));
	unmarshaled.set_GetInt32TrackedDeviceProperty_23(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetInt32TrackedDeviceProperty_t2396289227>(marshaled.___GetInt32TrackedDeviceProperty_23, _GetInt32TrackedDeviceProperty_t2396289227_il2cpp_TypeInfo_var));
	unmarshaled.set_GetUint64TrackedDeviceProperty_24(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetUint64TrackedDeviceProperty_t537540785>(marshaled.___GetUint64TrackedDeviceProperty_24, _GetUint64TrackedDeviceProperty_t537540785_il2cpp_TypeInfo_var));
	unmarshaled.set_GetMatrix34TrackedDeviceProperty_25(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetMatrix34TrackedDeviceProperty_t3426445457>(marshaled.___GetMatrix34TrackedDeviceProperty_25, _GetMatrix34TrackedDeviceProperty_t3426445457_il2cpp_TypeInfo_var));
	unmarshaled.set_GetStringTrackedDeviceProperty_26(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetStringTrackedDeviceProperty_t87797800>(marshaled.___GetStringTrackedDeviceProperty_26, _GetStringTrackedDeviceProperty_t87797800_il2cpp_TypeInfo_var));
	unmarshaled.set_GetPropErrorNameFromEnum_27(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetPropErrorNameFromEnum_t1193025139>(marshaled.___GetPropErrorNameFromEnum_27, _GetPropErrorNameFromEnum_t1193025139_il2cpp_TypeInfo_var));
	unmarshaled.set_PollNextEvent_28(il2cpp_codegen_marshal_function_ptr_to_delegate<_PollNextEvent_t3908295690>(marshaled.___PollNextEvent_28, _PollNextEvent_t3908295690_il2cpp_TypeInfo_var));
	unmarshaled.set_PollNextEventWithPose_29(il2cpp_codegen_marshal_function_ptr_to_delegate<_PollNextEventWithPose_t2759121141>(marshaled.___PollNextEventWithPose_29, _PollNextEventWithPose_t2759121141_il2cpp_TypeInfo_var));
	unmarshaled.set_GetEventTypeNameFromEnum_30(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetEventTypeNameFromEnum_t1950138544>(marshaled.___GetEventTypeNameFromEnum_30, _GetEventTypeNameFromEnum_t1950138544_il2cpp_TypeInfo_var));
	unmarshaled.set_GetHiddenAreaMesh_31(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetHiddenAreaMesh_t1813422502>(marshaled.___GetHiddenAreaMesh_31, _GetHiddenAreaMesh_t1813422502_il2cpp_TypeInfo_var));
	unmarshaled.set_GetControllerState_32(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetControllerState_t3891090487>(marshaled.___GetControllerState_32, _GetControllerState_t3891090487_il2cpp_TypeInfo_var));
	unmarshaled.set_GetControllerStateWithPose_33(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetControllerStateWithPose_t4079915850>(marshaled.___GetControllerStateWithPose_33, _GetControllerStateWithPose_t4079915850_il2cpp_TypeInfo_var));
	unmarshaled.set_TriggerHapticPulse_34(il2cpp_codegen_marshal_function_ptr_to_delegate<_TriggerHapticPulse_t158863722>(marshaled.___TriggerHapticPulse_34, _TriggerHapticPulse_t158863722_il2cpp_TypeInfo_var));
	unmarshaled.set_GetButtonIdNameFromEnum_35(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetButtonIdNameFromEnum_t195009473>(marshaled.___GetButtonIdNameFromEnum_35, _GetButtonIdNameFromEnum_t195009473_il2cpp_TypeInfo_var));
	unmarshaled.set_GetControllerAxisTypeNameFromEnum_36(il2cpp_codegen_marshal_function_ptr_to_delegate<_GetControllerAxisTypeNameFromEnum_t3568402941>(marshaled.___GetControllerAxisTypeNameFromEnum_36, _GetControllerAxisTypeNameFromEnum_t3568402941_il2cpp_TypeInfo_var));
	unmarshaled.set_CaptureInputFocus_37(il2cpp_codegen_marshal_function_ptr_to_delegate<_CaptureInputFocus_t2994096092>(marshaled.___CaptureInputFocus_37, _CaptureInputFocus_t2994096092_il2cpp_TypeInfo_var));
	unmarshaled.set_ReleaseInputFocus_38(il2cpp_codegen_marshal_function_ptr_to_delegate<_ReleaseInputFocus_t580725753>(marshaled.___ReleaseInputFocus_38, _ReleaseInputFocus_t580725753_il2cpp_TypeInfo_var));
	unmarshaled.set_IsInputFocusCapturedByAnotherProcess_39(il2cpp_codegen_marshal_function_ptr_to_delegate<_IsInputFocusCapturedByAnotherProcess_t84136089>(marshaled.___IsInputFocusCapturedByAnotherProcess_39, _IsInputFocusCapturedByAnotherProcess_t84136089_il2cpp_TypeInfo_var));
	unmarshaled.set_DriverDebugRequest_40(il2cpp_codegen_marshal_function_ptr_to_delegate<_DriverDebugRequest_t4049208724>(marshaled.___DriverDebugRequest_40, _DriverDebugRequest_t4049208724_il2cpp_TypeInfo_var));
	unmarshaled.set_PerformFirmwareUpdate_41(il2cpp_codegen_marshal_function_ptr_to_delegate<_PerformFirmwareUpdate_t673402879>(marshaled.___PerformFirmwareUpdate_41, _PerformFirmwareUpdate_t673402879_il2cpp_TypeInfo_var));
	unmarshaled.set_AcknowledgeQuit_Exiting_42(il2cpp_codegen_marshal_function_ptr_to_delegate<_AcknowledgeQuit_Exiting_t1109677234>(marshaled.___AcknowledgeQuit_Exiting_42, _AcknowledgeQuit_Exiting_t1109677234_il2cpp_TypeInfo_var));
	unmarshaled.set_AcknowledgeQuit_UserPrompt_43(il2cpp_codegen_marshal_function_ptr_to_delegate<_AcknowledgeQuit_UserPrompt_t90686541>(marshaled.___AcknowledgeQuit_UserPrompt_43, _AcknowledgeQuit_UserPrompt_t90686541_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: Valve.VR.IVRSystem
extern "C" void IVRSystem_t3365196000_marshal_com_cleanup(IVRSystem_t3365196000_marshaled_com& marshaled)
{
}
// System.Void Valve.VR.IVRSystem/_AcknowledgeQuit_Exiting::.ctor(System.Object,System.IntPtr)
extern "C"  void _AcknowledgeQuit_Exiting__ctor_m4195251985 (_AcknowledgeQuit_Exiting_t1109677234 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRSystem/_AcknowledgeQuit_Exiting::Invoke()
extern "C"  void _AcknowledgeQuit_Exiting_Invoke_m833227865 (_AcknowledgeQuit_Exiting_t1109677234 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_AcknowledgeQuit_Exiting_Invoke_m833227865((_AcknowledgeQuit_Exiting_t1109677234 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__AcknowledgeQuit_Exiting_t1109677234 (_AcknowledgeQuit_Exiting_t1109677234 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult Valve.VR.IVRSystem/_AcknowledgeQuit_Exiting::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _AcknowledgeQuit_Exiting_BeginInvoke_m930824168 (_AcknowledgeQuit_Exiting_t1109677234 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void Valve.VR.IVRSystem/_AcknowledgeQuit_Exiting::EndInvoke(System.IAsyncResult)
extern "C"  void _AcknowledgeQuit_Exiting_EndInvoke_m3947172267 (_AcknowledgeQuit_Exiting_t1109677234 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Valve.VR.IVRSystem/_AcknowledgeQuit_UserPrompt::.ctor(System.Object,System.IntPtr)
extern "C"  void _AcknowledgeQuit_UserPrompt__ctor_m3212531650 (_AcknowledgeQuit_UserPrompt_t90686541 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRSystem/_AcknowledgeQuit_UserPrompt::Invoke()
extern "C"  void _AcknowledgeQuit_UserPrompt_Invoke_m740905168 (_AcknowledgeQuit_UserPrompt_t90686541 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_AcknowledgeQuit_UserPrompt_Invoke_m740905168((_AcknowledgeQuit_UserPrompt_t90686541 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__AcknowledgeQuit_UserPrompt_t90686541 (_AcknowledgeQuit_UserPrompt_t90686541 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult Valve.VR.IVRSystem/_AcknowledgeQuit_UserPrompt::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _AcknowledgeQuit_UserPrompt_BeginInvoke_m559022539 (_AcknowledgeQuit_UserPrompt_t90686541 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void Valve.VR.IVRSystem/_AcknowledgeQuit_UserPrompt::EndInvoke(System.IAsyncResult)
extern "C"  void _AcknowledgeQuit_UserPrompt_EndInvoke_m2619790188 (_AcknowledgeQuit_UserPrompt_t90686541 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Valve.VR.IVRSystem/_ApplyTransform::.ctor(System.Object,System.IntPtr)
extern "C"  void _ApplyTransform__ctor_m543625527 (_ApplyTransform_t1439808290 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRSystem/_ApplyTransform::Invoke(Valve.VR.TrackedDevicePose_t&,Valve.VR.TrackedDevicePose_t&,Valve.VR.HmdMatrix34_t&)
extern "C"  void _ApplyTransform_Invoke_m1851493499 (_ApplyTransform_t1439808290 * __this, TrackedDevicePose_t_t1668551120 * ___pOutputPose0, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose1, HmdMatrix34_t_t664273062 * ___pTransform2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_ApplyTransform_Invoke_m1851493499((_ApplyTransform_t1439808290 *)__this->get_prev_9(),___pOutputPose0, ___pTrackedDevicePose1, ___pTransform2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, TrackedDevicePose_t_t1668551120 * ___pOutputPose0, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose1, HmdMatrix34_t_t664273062 * ___pTransform2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pOutputPose0, ___pTrackedDevicePose1, ___pTransform2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, TrackedDevicePose_t_t1668551120 * ___pOutputPose0, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose1, HmdMatrix34_t_t664273062 * ___pTransform2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pOutputPose0, ___pTrackedDevicePose1, ___pTransform2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__ApplyTransform_t1439808290 (_ApplyTransform_t1439808290 * __this, TrackedDevicePose_t_t1668551120 * ___pOutputPose0, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose1, HmdMatrix34_t_t664273062 * ___pTransform2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(TrackedDevicePose_t_t1668551120 *, TrackedDevicePose_t_t1668551120 *, HmdMatrix34_t_t664273062 *);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___pOutputPose0, ___pTrackedDevicePose1, ___pTransform2);

}
// System.IAsyncResult Valve.VR.IVRSystem/_ApplyTransform::BeginInvoke(Valve.VR.TrackedDevicePose_t&,Valve.VR.TrackedDevicePose_t&,Valve.VR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern Il2CppClass* TrackedDevicePose_t_t1668551120_il2cpp_TypeInfo_var;
extern Il2CppClass* HmdMatrix34_t_t664273062_il2cpp_TypeInfo_var;
extern const uint32_t _ApplyTransform_BeginInvoke_m896920956_MetadataUsageId;
extern "C"  Il2CppObject * _ApplyTransform_BeginInvoke_m896920956 (_ApplyTransform_t1439808290 * __this, TrackedDevicePose_t_t1668551120 * ___pOutputPose0, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose1, HmdMatrix34_t_t664273062 * ___pTransform2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_ApplyTransform_BeginInvoke_m896920956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(TrackedDevicePose_t_t1668551120_il2cpp_TypeInfo_var, &(*___pOutputPose0));
	__d_args[1] = Box(TrackedDevicePose_t_t1668551120_il2cpp_TypeInfo_var, &(*___pTrackedDevicePose1));
	__d_args[2] = Box(HmdMatrix34_t_t664273062_il2cpp_TypeInfo_var, &(*___pTransform2));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void Valve.VR.IVRSystem/_ApplyTransform::EndInvoke(Valve.VR.TrackedDevicePose_t&,Valve.VR.TrackedDevicePose_t&,Valve.VR.HmdMatrix34_t&,System.IAsyncResult)
extern "C"  void _ApplyTransform_EndInvoke_m3222636061 (_ApplyTransform_t1439808290 * __this, TrackedDevicePose_t_t1668551120 * ___pOutputPose0, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose1, HmdMatrix34_t_t664273062 * ___pTransform2, Il2CppObject * ___result3, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pOutputPose0,
	___pTrackedDevicePose1,
	___pTransform2,
	};
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result3, ___out_args);
}
// System.Void Valve.VR.IVRSystem/_CaptureInputFocus::.ctor(System.Object,System.IntPtr)
extern "C"  void _CaptureInputFocus__ctor_m951123125 (_CaptureInputFocus_t2994096092 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean Valve.VR.IVRSystem/_CaptureInputFocus::Invoke()
extern "C"  bool _CaptureInputFocus_Invoke_m498423369 (_CaptureInputFocus_t2994096092 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_CaptureInputFocus_Invoke_m498423369((_CaptureInputFocus_t2994096092 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  bool DelegatePInvokeWrapper__CaptureInputFocus_t2994096092 (_CaptureInputFocus_t2994096092 * __this, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return static_cast<bool>(returnValue);
}
// System.IAsyncResult Valve.VR.IVRSystem/_CaptureInputFocus::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _CaptureInputFocus_BeginInvoke_m3273249746 (_CaptureInputFocus_t2994096092 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Boolean Valve.VR.IVRSystem/_CaptureInputFocus::EndInvoke(System.IAsyncResult)
extern "C"  bool _CaptureInputFocus_EndInvoke_m2449820687 (_CaptureInputFocus_t2994096092 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_ComputeDistortion::.ctor(System.Object,System.IntPtr)
extern "C"  void _ComputeDistortion__ctor_m2689096133 (_ComputeDistortion_t3576284924 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean Valve.VR.IVRSystem/_ComputeDistortion::Invoke(Valve.VR.EVREye,System.Single,System.Single,Valve.VR.DistortionCoordinates_t&)
extern "C"  bool _ComputeDistortion_Invoke_m1412017482 (_ComputeDistortion_t3576284924 * __this, int32_t ___eEye0, float ___fU1, float ___fV2, DistortionCoordinates_t_t2253454723 * ___pDistortionCoordinates3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_ComputeDistortion_Invoke_m1412017482((_ComputeDistortion_t3576284924 *)__this->get_prev_9(),___eEye0, ___fU1, ___fV2, ___pDistortionCoordinates3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___eEye0, float ___fU1, float ___fV2, DistortionCoordinates_t_t2253454723 * ___pDistortionCoordinates3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eEye0, ___fU1, ___fV2, ___pDistortionCoordinates3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___eEye0, float ___fU1, float ___fV2, DistortionCoordinates_t_t2253454723 * ___pDistortionCoordinates3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eEye0, ___fU1, ___fV2, ___pDistortionCoordinates3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  bool DelegatePInvokeWrapper__ComputeDistortion_t3576284924 (_ComputeDistortion_t3576284924 * __this, int32_t ___eEye0, float ___fU1, float ___fV2, DistortionCoordinates_t_t2253454723 * ___pDistortionCoordinates3, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(int32_t, float, float, DistortionCoordinates_t_t2253454723 *);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___eEye0, ___fU1, ___fV2, ___pDistortionCoordinates3);

	return static_cast<bool>(returnValue);
}
// System.IAsyncResult Valve.VR.IVRSystem/_ComputeDistortion::BeginInvoke(Valve.VR.EVREye,System.Single,System.Single,Valve.VR.DistortionCoordinates_t&,System.AsyncCallback,System.Object)
extern Il2CppClass* EVREye_t3088716538_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* DistortionCoordinates_t_t2253454723_il2cpp_TypeInfo_var;
extern const uint32_t _ComputeDistortion_BeginInvoke_m22195989_MetadataUsageId;
extern "C"  Il2CppObject * _ComputeDistortion_BeginInvoke_m22195989 (_ComputeDistortion_t3576284924 * __this, int32_t ___eEye0, float ___fU1, float ___fV2, DistortionCoordinates_t_t2253454723 * ___pDistortionCoordinates3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_ComputeDistortion_BeginInvoke_m22195989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(EVREye_t3088716538_il2cpp_TypeInfo_var, &___eEye0);
	__d_args[1] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___fU1);
	__d_args[2] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___fV2);
	__d_args[3] = Box(DistortionCoordinates_t_t2253454723_il2cpp_TypeInfo_var, &(*___pDistortionCoordinates3));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Boolean Valve.VR.IVRSystem/_ComputeDistortion::EndInvoke(Valve.VR.DistortionCoordinates_t&,System.IAsyncResult)
extern "C"  bool _ComputeDistortion_EndInvoke_m3411213496 (_ComputeDistortion_t3576284924 * __this, DistortionCoordinates_t_t2253454723 * ___pDistortionCoordinates0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pDistortionCoordinates0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_DriverDebugRequest::.ctor(System.Object,System.IntPtr)
extern "C"  void _DriverDebugRequest__ctor_m1763855545 (_DriverDebugRequest_t4049208724 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UInt32 Valve.VR.IVRSystem/_DriverDebugRequest::Invoke(System.UInt32,System.String,System.String,System.UInt32)
extern "C"  uint32_t _DriverDebugRequest_Invoke_m1499682732 (_DriverDebugRequest_t4049208724 * __this, uint32_t ___unDeviceIndex0, String_t* ___pchRequest1, String_t* ___pchResponseBuffer2, uint32_t ___unResponseBufferSize3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_DriverDebugRequest_Invoke_m1499682732((_DriverDebugRequest_t4049208724 *)__this->get_prev_9(),___unDeviceIndex0, ___pchRequest1, ___pchResponseBuffer2, ___unResponseBufferSize3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___unDeviceIndex0, String_t* ___pchRequest1, String_t* ___pchResponseBuffer2, uint32_t ___unResponseBufferSize3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___unDeviceIndex0, ___pchRequest1, ___pchResponseBuffer2, ___unResponseBufferSize3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, uint32_t ___unDeviceIndex0, String_t* ___pchRequest1, String_t* ___pchResponseBuffer2, uint32_t ___unResponseBufferSize3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___unDeviceIndex0, ___pchRequest1, ___pchResponseBuffer2, ___unResponseBufferSize3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  uint32_t DelegatePInvokeWrapper__DriverDebugRequest_t4049208724 (_DriverDebugRequest_t4049208724 * __this, uint32_t ___unDeviceIndex0, String_t* ___pchRequest1, String_t* ___pchResponseBuffer2, uint32_t ___unResponseBufferSize3, const MethodInfo* method)
{
	typedef uint32_t (STDCALL *PInvokeFunc)(uint32_t, char*, char*, uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchRequest1' to native representation
	char* ____pchRequest1_marshaled = NULL;
	____pchRequest1_marshaled = il2cpp_codegen_marshal_string(___pchRequest1);

	// Marshaling of parameter '___pchResponseBuffer2' to native representation
	char* ____pchResponseBuffer2_marshaled = NULL;
	____pchResponseBuffer2_marshaled = il2cpp_codegen_marshal_string(___pchResponseBuffer2);

	// Native function invocation
	uint32_t returnValue = il2cppPInvokeFunc(___unDeviceIndex0, ____pchRequest1_marshaled, ____pchResponseBuffer2_marshaled, ___unResponseBufferSize3);

	// Marshaling cleanup of parameter '___pchRequest1' native representation
	il2cpp_codegen_marshal_free(____pchRequest1_marshaled);
	____pchRequest1_marshaled = NULL;

	// Marshaling cleanup of parameter '___pchResponseBuffer2' native representation
	il2cpp_codegen_marshal_free(____pchResponseBuffer2_marshaled);
	____pchResponseBuffer2_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_DriverDebugRequest::BeginInvoke(System.UInt32,System.String,System.String,System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _DriverDebugRequest_BeginInvoke_m1239116954_MetadataUsageId;
extern "C"  Il2CppObject * _DriverDebugRequest_BeginInvoke_m1239116954 (_DriverDebugRequest_t4049208724 * __this, uint32_t ___unDeviceIndex0, String_t* ___pchRequest1, String_t* ___pchResponseBuffer2, uint32_t ___unResponseBufferSize3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_DriverDebugRequest_BeginInvoke_m1239116954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unDeviceIndex0);
	__d_args[1] = ___pchRequest1;
	__d_args[2] = ___pchResponseBuffer2;
	__d_args[3] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unResponseBufferSize3);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.UInt32 Valve.VR.IVRSystem/_DriverDebugRequest::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _DriverDebugRequest_EndInvoke_m1383865042 (_DriverDebugRequest_t4049208724 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetBoolTrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetBoolTrackedDeviceProperty__ctor_m105252126 (_GetBoolTrackedDeviceProperty_t2236257287 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean Valve.VR.IVRSystem/_GetBoolTrackedDeviceProperty::Invoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&)
extern "C"  bool _GetBoolTrackedDeviceProperty_Invoke_m728451402 (_GetBoolTrackedDeviceProperty_t2236257287 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetBoolTrackedDeviceProperty_Invoke_m728451402((_GetBoolTrackedDeviceProperty_t2236257287 *)__this->get_prev_9(),___unDeviceIndex0, ___prop1, ___pError2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___unDeviceIndex0, ___prop1, ___pError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___unDeviceIndex0, ___prop1, ___pError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  bool DelegatePInvokeWrapper__GetBoolTrackedDeviceProperty_t2236257287 (_GetBoolTrackedDeviceProperty_t2236257287 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint32_t, int32_t, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___unDeviceIndex0, ___prop1, ___pError2);

	return static_cast<bool>(returnValue);
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetBoolTrackedDeviceProperty::BeginInvoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* ETrackedDeviceProperty_t3226377054_il2cpp_TypeInfo_var;
extern Il2CppClass* ETrackedPropertyError_t3340022390_il2cpp_TypeInfo_var;
extern const uint32_t _GetBoolTrackedDeviceProperty_BeginInvoke_m2470156651_MetadataUsageId;
extern "C"  Il2CppObject * _GetBoolTrackedDeviceProperty_BeginInvoke_m2470156651 (_GetBoolTrackedDeviceProperty_t2236257287 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetBoolTrackedDeviceProperty_BeginInvoke_m2470156651_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unDeviceIndex0);
	__d_args[1] = Box(ETrackedDeviceProperty_t3226377054_il2cpp_TypeInfo_var, &___prop1);
	__d_args[2] = Box(ETrackedPropertyError_t3340022390_il2cpp_TypeInfo_var, &(*___pError2));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Boolean Valve.VR.IVRSystem/_GetBoolTrackedDeviceProperty::EndInvoke(Valve.VR.ETrackedPropertyError&,System.IAsyncResult)
extern "C"  bool _GetBoolTrackedDeviceProperty_EndInvoke_m3471448870 (_GetBoolTrackedDeviceProperty_t2236257287 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pError0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetButtonIdNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetButtonIdNameFromEnum__ctor_m1685853460 (_GetButtonIdNameFromEnum_t195009473 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.IntPtr Valve.VR.IVRSystem/_GetButtonIdNameFromEnum::Invoke(Valve.VR.EVRButtonId)
extern "C"  IntPtr_t _GetButtonIdNameFromEnum_Invoke_m1474086367 (_GetButtonIdNameFromEnum_t195009473 * __this, int32_t ___eButtonId0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetButtonIdNameFromEnum_Invoke_m1474086367((_GetButtonIdNameFromEnum_t195009473 *)__this->get_prev_9(),___eButtonId0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef IntPtr_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___eButtonId0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eButtonId0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef IntPtr_t (*FunctionPointerType) (void* __this, int32_t ___eButtonId0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eButtonId0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  IntPtr_t DelegatePInvokeWrapper__GetButtonIdNameFromEnum_t195009473 (_GetButtonIdNameFromEnum_t195009473 * __this, int32_t ___eButtonId0, const MethodInfo* method)
{
	typedef intptr_t (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc(___eButtonId0);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetButtonIdNameFromEnum::BeginInvoke(Valve.VR.EVRButtonId,System.AsyncCallback,System.Object)
extern Il2CppClass* EVRButtonId_t66145412_il2cpp_TypeInfo_var;
extern const uint32_t _GetButtonIdNameFromEnum_BeginInvoke_m3543697797_MetadataUsageId;
extern "C"  Il2CppObject * _GetButtonIdNameFromEnum_BeginInvoke_m3543697797 (_GetButtonIdNameFromEnum_t195009473 * __this, int32_t ___eButtonId0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetButtonIdNameFromEnum_BeginInvoke_m3543697797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(EVRButtonId_t66145412_il2cpp_TypeInfo_var, &___eButtonId0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.IntPtr Valve.VR.IVRSystem/_GetButtonIdNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetButtonIdNameFromEnum_EndInvoke_m3510842715 (_GetButtonIdNameFromEnum_t195009473 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(IntPtr_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetControllerAxisTypeNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetControllerAxisTypeNameFromEnum__ctor_m2699871724 (_GetControllerAxisTypeNameFromEnum_t3568402941 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.IntPtr Valve.VR.IVRSystem/_GetControllerAxisTypeNameFromEnum::Invoke(Valve.VR.EVRControllerAxisType)
extern "C"  IntPtr_t _GetControllerAxisTypeNameFromEnum_Invoke_m1738538447 (_GetControllerAxisTypeNameFromEnum_t3568402941 * __this, int32_t ___eAxisType0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetControllerAxisTypeNameFromEnum_Invoke_m1738538447((_GetControllerAxisTypeNameFromEnum_t3568402941 *)__this->get_prev_9(),___eAxisType0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef IntPtr_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___eAxisType0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eAxisType0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef IntPtr_t (*FunctionPointerType) (void* __this, int32_t ___eAxisType0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eAxisType0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  IntPtr_t DelegatePInvokeWrapper__GetControllerAxisTypeNameFromEnum_t3568402941 (_GetControllerAxisTypeNameFromEnum_t3568402941 * __this, int32_t ___eAxisType0, const MethodInfo* method)
{
	typedef intptr_t (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc(___eAxisType0);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetControllerAxisTypeNameFromEnum::BeginInvoke(Valve.VR.EVRControllerAxisType,System.AsyncCallback,System.Object)
extern Il2CppClass* EVRControllerAxisType_t1358176136_il2cpp_TypeInfo_var;
extern const uint32_t _GetControllerAxisTypeNameFromEnum_BeginInvoke_m3232829541_MetadataUsageId;
extern "C"  Il2CppObject * _GetControllerAxisTypeNameFromEnum_BeginInvoke_m3232829541 (_GetControllerAxisTypeNameFromEnum_t3568402941 * __this, int32_t ___eAxisType0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetControllerAxisTypeNameFromEnum_BeginInvoke_m3232829541_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(EVRControllerAxisType_t1358176136_il2cpp_TypeInfo_var, &___eAxisType0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.IntPtr Valve.VR.IVRSystem/_GetControllerAxisTypeNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetControllerAxisTypeNameFromEnum_EndInvoke_m3480416983 (_GetControllerAxisTypeNameFromEnum_t3568402941 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(IntPtr_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetControllerRoleForTrackedDeviceIndex::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetControllerRoleForTrackedDeviceIndex__ctor_m1818992616 (_GetControllerRoleForTrackedDeviceIndex_t1728202579 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.ETrackedControllerRole Valve.VR.IVRSystem/_GetControllerRoleForTrackedDeviceIndex::Invoke(System.UInt32)
extern "C"  int32_t _GetControllerRoleForTrackedDeviceIndex_Invoke_m4099929182 (_GetControllerRoleForTrackedDeviceIndex_t1728202579 * __this, uint32_t ___unDeviceIndex0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetControllerRoleForTrackedDeviceIndex_Invoke_m4099929182((_GetControllerRoleForTrackedDeviceIndex_t1728202579 *)__this->get_prev_9(),___unDeviceIndex0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___unDeviceIndex0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___unDeviceIndex0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint32_t ___unDeviceIndex0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___unDeviceIndex0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__GetControllerRoleForTrackedDeviceIndex_t1728202579 (_GetControllerRoleForTrackedDeviceIndex_t1728202579 * __this, uint32_t ___unDeviceIndex0, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___unDeviceIndex0);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetControllerRoleForTrackedDeviceIndex::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _GetControllerRoleForTrackedDeviceIndex_BeginInvoke_m976477837_MetadataUsageId;
extern "C"  Il2CppObject * _GetControllerRoleForTrackedDeviceIndex_BeginInvoke_m976477837 (_GetControllerRoleForTrackedDeviceIndex_t1728202579 * __this, uint32_t ___unDeviceIndex0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetControllerRoleForTrackedDeviceIndex_BeginInvoke_m976477837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unDeviceIndex0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// Valve.VR.ETrackedControllerRole Valve.VR.IVRSystem/_GetControllerRoleForTrackedDeviceIndex::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _GetControllerRoleForTrackedDeviceIndex_EndInvoke_m4028217496 (_GetControllerRoleForTrackedDeviceIndex_t1728202579 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetControllerState::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetControllerState__ctor_m1598880442 (_GetControllerState_t3891090487 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean Valve.VR.IVRSystem/_GetControllerState::Invoke(System.UInt32,Valve.VR.VRControllerState_t&,System.UInt32)
extern "C"  bool _GetControllerState_Invoke_m3024192214 (_GetControllerState_t3891090487 * __this, uint32_t ___unControllerDeviceIndex0, VRControllerState_t_t2504874220 * ___pControllerState1, uint32_t ___unControllerStateSize2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetControllerState_Invoke_m3024192214((_GetControllerState_t3891090487 *)__this->get_prev_9(),___unControllerDeviceIndex0, ___pControllerState1, ___unControllerStateSize2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___unControllerDeviceIndex0, VRControllerState_t_t2504874220 * ___pControllerState1, uint32_t ___unControllerStateSize2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___unControllerDeviceIndex0, ___pControllerState1, ___unControllerStateSize2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, uint32_t ___unControllerDeviceIndex0, VRControllerState_t_t2504874220 * ___pControllerState1, uint32_t ___unControllerStateSize2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___unControllerDeviceIndex0, ___pControllerState1, ___unControllerStateSize2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  bool DelegatePInvokeWrapper__GetControllerState_t3891090487 (_GetControllerState_t3891090487 * __this, uint32_t ___unControllerDeviceIndex0, VRControllerState_t_t2504874220 * ___pControllerState1, uint32_t ___unControllerStateSize2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint32_t, VRControllerState_t_t2504874220 *, uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___unControllerDeviceIndex0, ___pControllerState1, ___unControllerStateSize2);

	return static_cast<bool>(returnValue);
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetControllerState::BeginInvoke(System.UInt32,Valve.VR.VRControllerState_t&,System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* VRControllerState_t_t2504874220_il2cpp_TypeInfo_var;
extern const uint32_t _GetControllerState_BeginInvoke_m1607478547_MetadataUsageId;
extern "C"  Il2CppObject * _GetControllerState_BeginInvoke_m1607478547 (_GetControllerState_t3891090487 * __this, uint32_t ___unControllerDeviceIndex0, VRControllerState_t_t2504874220 * ___pControllerState1, uint32_t ___unControllerStateSize2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetControllerState_BeginInvoke_m1607478547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unControllerDeviceIndex0);
	__d_args[1] = Box(VRControllerState_t_t2504874220_il2cpp_TypeInfo_var, &(*___pControllerState1));
	__d_args[2] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unControllerStateSize2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Boolean Valve.VR.IVRSystem/_GetControllerState::EndInvoke(Valve.VR.VRControllerState_t&,System.IAsyncResult)
extern "C"  bool _GetControllerState_EndInvoke_m2488851528 (_GetControllerState_t3891090487 * __this, VRControllerState_t_t2504874220 * ___pControllerState0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pControllerState0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetControllerStateWithPose::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetControllerStateWithPose__ctor_m978017505 (_GetControllerStateWithPose_t4079915850 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean Valve.VR.IVRSystem/_GetControllerStateWithPose::Invoke(Valve.VR.ETrackingUniverseOrigin,System.UInt32,Valve.VR.VRControllerState_t&,System.UInt32,Valve.VR.TrackedDevicePose_t&)
extern "C"  bool _GetControllerStateWithPose_Invoke_m3300024348 (_GetControllerStateWithPose_t4079915850 * __this, int32_t ___eOrigin0, uint32_t ___unControllerDeviceIndex1, VRControllerState_t_t2504874220 * ___pControllerState2, uint32_t ___unControllerStateSize3, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose4, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetControllerStateWithPose_Invoke_m3300024348((_GetControllerStateWithPose_t4079915850 *)__this->get_prev_9(),___eOrigin0, ___unControllerDeviceIndex1, ___pControllerState2, ___unControllerStateSize3, ___pTrackedDevicePose4, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___eOrigin0, uint32_t ___unControllerDeviceIndex1, VRControllerState_t_t2504874220 * ___pControllerState2, uint32_t ___unControllerStateSize3, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose4, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eOrigin0, ___unControllerDeviceIndex1, ___pControllerState2, ___unControllerStateSize3, ___pTrackedDevicePose4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___eOrigin0, uint32_t ___unControllerDeviceIndex1, VRControllerState_t_t2504874220 * ___pControllerState2, uint32_t ___unControllerStateSize3, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose4, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eOrigin0, ___unControllerDeviceIndex1, ___pControllerState2, ___unControllerStateSize3, ___pTrackedDevicePose4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  bool DelegatePInvokeWrapper__GetControllerStateWithPose_t4079915850 (_GetControllerStateWithPose_t4079915850 * __this, int32_t ___eOrigin0, uint32_t ___unControllerDeviceIndex1, VRControllerState_t_t2504874220 * ___pControllerState2, uint32_t ___unControllerStateSize3, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose4, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(int32_t, uint32_t, VRControllerState_t_t2504874220 *, uint32_t, TrackedDevicePose_t_t1668551120 *);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___eOrigin0, ___unControllerDeviceIndex1, ___pControllerState2, ___unControllerStateSize3, ___pTrackedDevicePose4);

	return static_cast<bool>(returnValue);
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetControllerStateWithPose::BeginInvoke(Valve.VR.ETrackingUniverseOrigin,System.UInt32,Valve.VR.VRControllerState_t&,System.UInt32,Valve.VR.TrackedDevicePose_t&,System.AsyncCallback,System.Object)
extern Il2CppClass* ETrackingUniverseOrigin_t1464400093_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* VRControllerState_t_t2504874220_il2cpp_TypeInfo_var;
extern Il2CppClass* TrackedDevicePose_t_t1668551120_il2cpp_TypeInfo_var;
extern const uint32_t _GetControllerStateWithPose_BeginInvoke_m1372453569_MetadataUsageId;
extern "C"  Il2CppObject * _GetControllerStateWithPose_BeginInvoke_m1372453569 (_GetControllerStateWithPose_t4079915850 * __this, int32_t ___eOrigin0, uint32_t ___unControllerDeviceIndex1, VRControllerState_t_t2504874220 * ___pControllerState2, uint32_t ___unControllerStateSize3, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetControllerStateWithPose_BeginInvoke_m1372453569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = Box(ETrackingUniverseOrigin_t1464400093_il2cpp_TypeInfo_var, &___eOrigin0);
	__d_args[1] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unControllerDeviceIndex1);
	__d_args[2] = Box(VRControllerState_t_t2504874220_il2cpp_TypeInfo_var, &(*___pControllerState2));
	__d_args[3] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unControllerStateSize3);
	__d_args[4] = Box(TrackedDevicePose_t_t1668551120_il2cpp_TypeInfo_var, &(*___pTrackedDevicePose4));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback5, (Il2CppObject*)___object6);
}
// System.Boolean Valve.VR.IVRSystem/_GetControllerStateWithPose::EndInvoke(Valve.VR.VRControllerState_t&,Valve.VR.TrackedDevicePose_t&,System.IAsyncResult)
extern "C"  bool _GetControllerStateWithPose_EndInvoke_m451703135 (_GetControllerStateWithPose_t4079915850 * __this, VRControllerState_t_t2504874220 * ___pControllerState0, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose1, Il2CppObject * ___result2, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pControllerState0,
	___pTrackedDevicePose1,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result2, ___out_args);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetD3D9AdapterIndex::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetD3D9AdapterIndex__ctor_m2022820928 (_GetD3D9AdapterIndex_t4234979703 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 Valve.VR.IVRSystem/_GetD3D9AdapterIndex::Invoke()
extern "C"  int32_t _GetD3D9AdapterIndex_Invoke_m2465082528 (_GetD3D9AdapterIndex_t4234979703 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetD3D9AdapterIndex_Invoke_m2465082528((_GetD3D9AdapterIndex_t4234979703 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__GetD3D9AdapterIndex_t4234979703 (_GetD3D9AdapterIndex_t4234979703 * __this, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetD3D9AdapterIndex::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetD3D9AdapterIndex_BeginInvoke_m1655616089 (_GetD3D9AdapterIndex_t4234979703 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Int32 Valve.VR.IVRSystem/_GetD3D9AdapterIndex::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _GetD3D9AdapterIndex_EndInvoke_m3586611964 (_GetD3D9AdapterIndex_t4234979703 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetDeviceToAbsoluteTrackingPose::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetDeviceToAbsoluteTrackingPose__ctor_m1617064779 (_GetDeviceToAbsoluteTrackingPose_t1432625068 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRSystem/_GetDeviceToAbsoluteTrackingPose::Invoke(Valve.VR.ETrackingUniverseOrigin,System.Single,Valve.VR.TrackedDevicePose_t[],System.UInt32)
extern "C"  void _GetDeviceToAbsoluteTrackingPose_Invoke_m3408239547 (_GetDeviceToAbsoluteTrackingPose_t1432625068 * __this, int32_t ___eOrigin0, float ___fPredictedSecondsToPhotonsFromNow1, TrackedDevicePose_tU5BU5D_t2897272049* ___pTrackedDevicePoseArray2, uint32_t ___unTrackedDevicePoseArrayCount3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetDeviceToAbsoluteTrackingPose_Invoke_m3408239547((_GetDeviceToAbsoluteTrackingPose_t1432625068 *)__this->get_prev_9(),___eOrigin0, ___fPredictedSecondsToPhotonsFromNow1, ___pTrackedDevicePoseArray2, ___unTrackedDevicePoseArrayCount3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___eOrigin0, float ___fPredictedSecondsToPhotonsFromNow1, TrackedDevicePose_tU5BU5D_t2897272049* ___pTrackedDevicePoseArray2, uint32_t ___unTrackedDevicePoseArrayCount3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eOrigin0, ___fPredictedSecondsToPhotonsFromNow1, ___pTrackedDevicePoseArray2, ___unTrackedDevicePoseArrayCount3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___eOrigin0, float ___fPredictedSecondsToPhotonsFromNow1, TrackedDevicePose_tU5BU5D_t2897272049* ___pTrackedDevicePoseArray2, uint32_t ___unTrackedDevicePoseArrayCount3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eOrigin0, ___fPredictedSecondsToPhotonsFromNow1, ___pTrackedDevicePoseArray2, ___unTrackedDevicePoseArrayCount3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__GetDeviceToAbsoluteTrackingPose_t1432625068 (_GetDeviceToAbsoluteTrackingPose_t1432625068 * __this, int32_t ___eOrigin0, float ___fPredictedSecondsToPhotonsFromNow1, TrackedDevicePose_tU5BU5D_t2897272049* ___pTrackedDevicePoseArray2, uint32_t ___unTrackedDevicePoseArrayCount3, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, float, TrackedDevicePose_t_t1668551120 *, uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pTrackedDevicePoseArray2' to native representation
	TrackedDevicePose_t_t1668551120 * ____pTrackedDevicePoseArray2_marshaled = NULL;
	if (___pTrackedDevicePoseArray2 != NULL)
	{
		____pTrackedDevicePoseArray2_marshaled = reinterpret_cast<TrackedDevicePose_t_t1668551120 *>((___pTrackedDevicePoseArray2)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	il2cppPInvokeFunc(___eOrigin0, ___fPredictedSecondsToPhotonsFromNow1, ____pTrackedDevicePoseArray2_marshaled, ___unTrackedDevicePoseArrayCount3);

	// Marshaling of parameter '___pTrackedDevicePoseArray2' back from native representation
	if (____pTrackedDevicePoseArray2_marshaled != NULL)
	{
		int32_t ____pTrackedDevicePoseArray2_Length = (___pTrackedDevicePoseArray2)->max_length;
		for (int32_t i = 0; i < ____pTrackedDevicePoseArray2_Length; i++)
		{
			(___pTrackedDevicePoseArray2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____pTrackedDevicePoseArray2_marshaled)[i]);
		}
	}

}
// System.IAsyncResult Valve.VR.IVRSystem/_GetDeviceToAbsoluteTrackingPose::BeginInvoke(Valve.VR.ETrackingUniverseOrigin,System.Single,Valve.VR.TrackedDevicePose_t[],System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* ETrackingUniverseOrigin_t1464400093_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _GetDeviceToAbsoluteTrackingPose_BeginInvoke_m3795648090_MetadataUsageId;
extern "C"  Il2CppObject * _GetDeviceToAbsoluteTrackingPose_BeginInvoke_m3795648090 (_GetDeviceToAbsoluteTrackingPose_t1432625068 * __this, int32_t ___eOrigin0, float ___fPredictedSecondsToPhotonsFromNow1, TrackedDevicePose_tU5BU5D_t2897272049* ___pTrackedDevicePoseArray2, uint32_t ___unTrackedDevicePoseArrayCount3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetDeviceToAbsoluteTrackingPose_BeginInvoke_m3795648090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(ETrackingUniverseOrigin_t1464400093_il2cpp_TypeInfo_var, &___eOrigin0);
	__d_args[1] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___fPredictedSecondsToPhotonsFromNow1);
	__d_args[2] = ___pTrackedDevicePoseArray2;
	__d_args[3] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unTrackedDevicePoseArrayCount3);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void Valve.VR.IVRSystem/_GetDeviceToAbsoluteTrackingPose::EndInvoke(System.IAsyncResult)
extern "C"  void _GetDeviceToAbsoluteTrackingPose_EndInvoke_m3064134549 (_GetDeviceToAbsoluteTrackingPose_t1432625068 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Valve.VR.IVRSystem/_GetDXGIOutputInfo::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetDXGIOutputInfo__ctor_m1025261082 (_GetDXGIOutputInfo_t1897151767 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRSystem/_GetDXGIOutputInfo::Invoke(System.Int32&)
extern "C"  void _GetDXGIOutputInfo_Invoke_m2895185309 (_GetDXGIOutputInfo_t1897151767 * __this, int32_t* ___pnAdapterIndex0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetDXGIOutputInfo_Invoke_m2895185309((_GetDXGIOutputInfo_t1897151767 *)__this->get_prev_9(),___pnAdapterIndex0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t* ___pnAdapterIndex0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pnAdapterIndex0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t* ___pnAdapterIndex0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pnAdapterIndex0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__GetDXGIOutputInfo_t1897151767 (_GetDXGIOutputInfo_t1897151767 * __this, int32_t* ___pnAdapterIndex0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___pnAdapterIndex0);

}
// System.IAsyncResult Valve.VR.IVRSystem/_GetDXGIOutputInfo::BeginInvoke(System.Int32&,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t _GetDXGIOutputInfo_BeginInvoke_m2857825104_MetadataUsageId;
extern "C"  Il2CppObject * _GetDXGIOutputInfo_BeginInvoke_m2857825104 (_GetDXGIOutputInfo_t1897151767 * __this, int32_t* ___pnAdapterIndex0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetDXGIOutputInfo_BeginInvoke_m2857825104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &(*___pnAdapterIndex0));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Valve.VR.IVRSystem/_GetDXGIOutputInfo::EndInvoke(System.Int32&,System.IAsyncResult)
extern "C"  void _GetDXGIOutputInfo_EndInvoke_m782386979 (_GetDXGIOutputInfo_t1897151767 * __this, int32_t* ___pnAdapterIndex0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pnAdapterIndex0,
	};
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
}
// System.Void Valve.VR.IVRSystem/_GetEventTypeNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetEventTypeNameFromEnum__ctor_m219289187 (_GetEventTypeNameFromEnum_t1950138544 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.IntPtr Valve.VR.IVRSystem/_GetEventTypeNameFromEnum::Invoke(Valve.VR.EVREventType)
extern "C"  IntPtr_t _GetEventTypeNameFromEnum_Invoke_m2044523695 (_GetEventTypeNameFromEnum_t1950138544 * __this, int32_t ___eType0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetEventTypeNameFromEnum_Invoke_m2044523695((_GetEventTypeNameFromEnum_t1950138544 *)__this->get_prev_9(),___eType0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef IntPtr_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___eType0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eType0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef IntPtr_t (*FunctionPointerType) (void* __this, int32_t ___eType0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eType0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  IntPtr_t DelegatePInvokeWrapper__GetEventTypeNameFromEnum_t1950138544 (_GetEventTypeNameFromEnum_t1950138544 * __this, int32_t ___eType0, const MethodInfo* method)
{
	typedef intptr_t (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc(___eType0);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetEventTypeNameFromEnum::BeginInvoke(Valve.VR.EVREventType,System.AsyncCallback,System.Object)
extern Il2CppClass* EVREventType_t6846875_il2cpp_TypeInfo_var;
extern const uint32_t _GetEventTypeNameFromEnum_BeginInvoke_m297886973_MetadataUsageId;
extern "C"  Il2CppObject * _GetEventTypeNameFromEnum_BeginInvoke_m297886973 (_GetEventTypeNameFromEnum_t1950138544 * __this, int32_t ___eType0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetEventTypeNameFromEnum_BeginInvoke_m297886973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(EVREventType_t6846875_il2cpp_TypeInfo_var, &___eType0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.IntPtr Valve.VR.IVRSystem/_GetEventTypeNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetEventTypeNameFromEnum_EndInvoke_m3635183318 (_GetEventTypeNameFromEnum_t1950138544 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(IntPtr_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetEyeToHeadTransform::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetEyeToHeadTransform__ctor_m1490155399 (_GetEyeToHeadTransform_t3057184772 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.HmdMatrix34_t Valve.VR.IVRSystem/_GetEyeToHeadTransform::Invoke(Valve.VR.EVREye)
extern "C"  HmdMatrix34_t_t664273062  _GetEyeToHeadTransform_Invoke_m2328941924 (_GetEyeToHeadTransform_t3057184772 * __this, int32_t ___eEye0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetEyeToHeadTransform_Invoke_m2328941924((_GetEyeToHeadTransform_t3057184772 *)__this->get_prev_9(),___eEye0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef HmdMatrix34_t_t664273062  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___eEye0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eEye0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef HmdMatrix34_t_t664273062  (*FunctionPointerType) (void* __this, int32_t ___eEye0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eEye0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  HmdMatrix34_t_t664273062  DelegatePInvokeWrapper__GetEyeToHeadTransform_t3057184772 (_GetEyeToHeadTransform_t3057184772 * __this, int32_t ___eEye0, const MethodInfo* method)
{
	typedef HmdMatrix34_t_t664273062  (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	HmdMatrix34_t_t664273062  returnValue = il2cppPInvokeFunc(___eEye0);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetEyeToHeadTransform::BeginInvoke(Valve.VR.EVREye,System.AsyncCallback,System.Object)
extern Il2CppClass* EVREye_t3088716538_il2cpp_TypeInfo_var;
extern const uint32_t _GetEyeToHeadTransform_BeginInvoke_m148652392_MetadataUsageId;
extern "C"  Il2CppObject * _GetEyeToHeadTransform_BeginInvoke_m148652392 (_GetEyeToHeadTransform_t3057184772 * __this, int32_t ___eEye0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetEyeToHeadTransform_BeginInvoke_m148652392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(EVREye_t3088716538_il2cpp_TypeInfo_var, &___eEye0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// Valve.VR.HmdMatrix34_t Valve.VR.IVRSystem/_GetEyeToHeadTransform::EndInvoke(System.IAsyncResult)
extern "C"  HmdMatrix34_t_t664273062  _GetEyeToHeadTransform_EndInvoke_m2450405904 (_GetEyeToHeadTransform_t3057184772 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(HmdMatrix34_t_t664273062 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetFloatTrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetFloatTrackedDeviceProperty__ctor_m2313141476 (_GetFloatTrackedDeviceProperty_t1406950913 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Single Valve.VR.IVRSystem/_GetFloatTrackedDeviceProperty::Invoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&)
extern "C"  float _GetFloatTrackedDeviceProperty_Invoke_m1416208608 (_GetFloatTrackedDeviceProperty_t1406950913 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetFloatTrackedDeviceProperty_Invoke_m1416208608((_GetFloatTrackedDeviceProperty_t1406950913 *)__this->get_prev_9(),___unDeviceIndex0, ___prop1, ___pError2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___unDeviceIndex0, ___prop1, ___pError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___unDeviceIndex0, ___prop1, ___pError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  float DelegatePInvokeWrapper__GetFloatTrackedDeviceProperty_t1406950913 (_GetFloatTrackedDeviceProperty_t1406950913 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method)
{
	typedef float (STDCALL *PInvokeFunc)(uint32_t, int32_t, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	float returnValue = il2cppPInvokeFunc(___unDeviceIndex0, ___prop1, ___pError2);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetFloatTrackedDeviceProperty::BeginInvoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* ETrackedDeviceProperty_t3226377054_il2cpp_TypeInfo_var;
extern Il2CppClass* ETrackedPropertyError_t3340022390_il2cpp_TypeInfo_var;
extern const uint32_t _GetFloatTrackedDeviceProperty_BeginInvoke_m3951474841_MetadataUsageId;
extern "C"  Il2CppObject * _GetFloatTrackedDeviceProperty_BeginInvoke_m3951474841 (_GetFloatTrackedDeviceProperty_t1406950913 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetFloatTrackedDeviceProperty_BeginInvoke_m3951474841_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unDeviceIndex0);
	__d_args[1] = Box(ETrackedDeviceProperty_t3226377054_il2cpp_TypeInfo_var, &___prop1);
	__d_args[2] = Box(ETrackedPropertyError_t3340022390_il2cpp_TypeInfo_var, &(*___pError2));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Single Valve.VR.IVRSystem/_GetFloatTrackedDeviceProperty::EndInvoke(Valve.VR.ETrackedPropertyError&,System.IAsyncResult)
extern "C"  float _GetFloatTrackedDeviceProperty_EndInvoke_m637675364 (_GetFloatTrackedDeviceProperty_t1406950913 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pError0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetHiddenAreaMesh::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetHiddenAreaMesh__ctor_m3597632979 (_GetHiddenAreaMesh_t1813422502 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.HiddenAreaMesh_t Valve.VR.IVRSystem/_GetHiddenAreaMesh::Invoke(Valve.VR.EVREye,Valve.VR.EHiddenAreaMeshType)
extern "C"  HiddenAreaMesh_t_t3319190843  _GetHiddenAreaMesh_Invoke_m3075037462 (_GetHiddenAreaMesh_t1813422502 * __this, int32_t ___eEye0, int32_t ___type1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetHiddenAreaMesh_Invoke_m3075037462((_GetHiddenAreaMesh_t1813422502 *)__this->get_prev_9(),___eEye0, ___type1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef HiddenAreaMesh_t_t3319190843  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___eEye0, int32_t ___type1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eEye0, ___type1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef HiddenAreaMesh_t_t3319190843  (*FunctionPointerType) (void* __this, int32_t ___eEye0, int32_t ___type1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eEye0, ___type1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  HiddenAreaMesh_t_t3319190843  DelegatePInvokeWrapper__GetHiddenAreaMesh_t1813422502 (_GetHiddenAreaMesh_t1813422502 * __this, int32_t ___eEye0, int32_t ___type1, const MethodInfo* method)
{
	typedef HiddenAreaMesh_t_t3319190843  (STDCALL *PInvokeFunc)(int32_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	HiddenAreaMesh_t_t3319190843  returnValue = il2cppPInvokeFunc(___eEye0, ___type1);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetHiddenAreaMesh::BeginInvoke(Valve.VR.EVREye,Valve.VR.EHiddenAreaMeshType,System.AsyncCallback,System.Object)
extern Il2CppClass* EVREye_t3088716538_il2cpp_TypeInfo_var;
extern Il2CppClass* EHiddenAreaMeshType_t3068936429_il2cpp_TypeInfo_var;
extern const uint32_t _GetHiddenAreaMesh_BeginInvoke_m404340791_MetadataUsageId;
extern "C"  Il2CppObject * _GetHiddenAreaMesh_BeginInvoke_m404340791 (_GetHiddenAreaMesh_t1813422502 * __this, int32_t ___eEye0, int32_t ___type1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetHiddenAreaMesh_BeginInvoke_m404340791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(EVREye_t3088716538_il2cpp_TypeInfo_var, &___eEye0);
	__d_args[1] = Box(EHiddenAreaMeshType_t3068936429_il2cpp_TypeInfo_var, &___type1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// Valve.VR.HiddenAreaMesh_t Valve.VR.IVRSystem/_GetHiddenAreaMesh::EndInvoke(System.IAsyncResult)
extern "C"  HiddenAreaMesh_t_t3319190843  _GetHiddenAreaMesh_EndInvoke_m179802351 (_GetHiddenAreaMesh_t1813422502 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(HiddenAreaMesh_t_t3319190843 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetInt32TrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetInt32TrackedDeviceProperty__ctor_m3778535034 (_GetInt32TrackedDeviceProperty_t2396289227 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 Valve.VR.IVRSystem/_GetInt32TrackedDeviceProperty::Invoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&)
extern "C"  int32_t _GetInt32TrackedDeviceProperty_Invoke_m1360536014 (_GetInt32TrackedDeviceProperty_t2396289227 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetInt32TrackedDeviceProperty_Invoke_m1360536014((_GetInt32TrackedDeviceProperty_t2396289227 *)__this->get_prev_9(),___unDeviceIndex0, ___prop1, ___pError2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___unDeviceIndex0, ___prop1, ___pError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___unDeviceIndex0, ___prop1, ___pError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__GetInt32TrackedDeviceProperty_t2396289227 (_GetInt32TrackedDeviceProperty_t2396289227 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint32_t, int32_t, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___unDeviceIndex0, ___prop1, ___pError2);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetInt32TrackedDeviceProperty::BeginInvoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* ETrackedDeviceProperty_t3226377054_il2cpp_TypeInfo_var;
extern Il2CppClass* ETrackedPropertyError_t3340022390_il2cpp_TypeInfo_var;
extern const uint32_t _GetInt32TrackedDeviceProperty_BeginInvoke_m2934564355_MetadataUsageId;
extern "C"  Il2CppObject * _GetInt32TrackedDeviceProperty_BeginInvoke_m2934564355 (_GetInt32TrackedDeviceProperty_t2396289227 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetInt32TrackedDeviceProperty_BeginInvoke_m2934564355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unDeviceIndex0);
	__d_args[1] = Box(ETrackedDeviceProperty_t3226377054_il2cpp_TypeInfo_var, &___prop1);
	__d_args[2] = Box(ETrackedPropertyError_t3340022390_il2cpp_TypeInfo_var, &(*___pError2));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Int32 Valve.VR.IVRSystem/_GetInt32TrackedDeviceProperty::EndInvoke(Valve.VR.ETrackedPropertyError&,System.IAsyncResult)
extern "C"  int32_t _GetInt32TrackedDeviceProperty_EndInvoke_m213712922 (_GetInt32TrackedDeviceProperty_t2396289227 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pError0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetMatrix34TrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetMatrix34TrackedDeviceProperty__ctor_m2418638436 (_GetMatrix34TrackedDeviceProperty_t3426445457 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.HmdMatrix34_t Valve.VR.IVRSystem/_GetMatrix34TrackedDeviceProperty::Invoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&)
extern "C"  HmdMatrix34_t_t664273062  _GetMatrix34TrackedDeviceProperty_Invoke_m1622936551 (_GetMatrix34TrackedDeviceProperty_t3426445457 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetMatrix34TrackedDeviceProperty_Invoke_m1622936551((_GetMatrix34TrackedDeviceProperty_t3426445457 *)__this->get_prev_9(),___unDeviceIndex0, ___prop1, ___pError2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef HmdMatrix34_t_t664273062  (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___unDeviceIndex0, ___prop1, ___pError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef HmdMatrix34_t_t664273062  (*FunctionPointerType) (void* __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___unDeviceIndex0, ___prop1, ___pError2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  HmdMatrix34_t_t664273062  DelegatePInvokeWrapper__GetMatrix34TrackedDeviceProperty_t3426445457 (_GetMatrix34TrackedDeviceProperty_t3426445457 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method)
{
	typedef HmdMatrix34_t_t664273062  (STDCALL *PInvokeFunc)(uint32_t, int32_t, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	HmdMatrix34_t_t664273062  returnValue = il2cppPInvokeFunc(___unDeviceIndex0, ___prop1, ___pError2);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetMatrix34TrackedDeviceProperty::BeginInvoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* ETrackedDeviceProperty_t3226377054_il2cpp_TypeInfo_var;
extern Il2CppClass* ETrackedPropertyError_t3340022390_il2cpp_TypeInfo_var;
extern const uint32_t _GetMatrix34TrackedDeviceProperty_BeginInvoke_m3901349401_MetadataUsageId;
extern "C"  Il2CppObject * _GetMatrix34TrackedDeviceProperty_BeginInvoke_m3901349401 (_GetMatrix34TrackedDeviceProperty_t3426445457 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetMatrix34TrackedDeviceProperty_BeginInvoke_m3901349401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unDeviceIndex0);
	__d_args[1] = Box(ETrackedDeviceProperty_t3226377054_il2cpp_TypeInfo_var, &___prop1);
	__d_args[2] = Box(ETrackedPropertyError_t3340022390_il2cpp_TypeInfo_var, &(*___pError2));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// Valve.VR.HmdMatrix34_t Valve.VR.IVRSystem/_GetMatrix34TrackedDeviceProperty::EndInvoke(Valve.VR.ETrackedPropertyError&,System.IAsyncResult)
extern "C"  HmdMatrix34_t_t664273062  _GetMatrix34TrackedDeviceProperty_EndInvoke_m2319733641 (_GetMatrix34TrackedDeviceProperty_t3426445457 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pError0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(HmdMatrix34_t_t664273062 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetProjectionMatrix::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetProjectionMatrix__ctor_m1740759091 (_GetProjectionMatrix_t2621141914 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.HmdMatrix44_t Valve.VR.IVRSystem/_GetProjectionMatrix::Invoke(Valve.VR.EVREye,System.Single,System.Single)
extern "C"  HmdMatrix44_t_t664273159  _GetProjectionMatrix_Invoke_m3714650273 (_GetProjectionMatrix_t2621141914 * __this, int32_t ___eEye0, float ___fNearZ1, float ___fFarZ2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetProjectionMatrix_Invoke_m3714650273((_GetProjectionMatrix_t2621141914 *)__this->get_prev_9(),___eEye0, ___fNearZ1, ___fFarZ2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef HmdMatrix44_t_t664273159  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___eEye0, float ___fNearZ1, float ___fFarZ2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eEye0, ___fNearZ1, ___fFarZ2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef HmdMatrix44_t_t664273159  (*FunctionPointerType) (void* __this, int32_t ___eEye0, float ___fNearZ1, float ___fFarZ2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eEye0, ___fNearZ1, ___fFarZ2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  HmdMatrix44_t_t664273159  DelegatePInvokeWrapper__GetProjectionMatrix_t2621141914 (_GetProjectionMatrix_t2621141914 * __this, int32_t ___eEye0, float ___fNearZ1, float ___fFarZ2, const MethodInfo* method)
{
	typedef HmdMatrix44_t_t664273159  (STDCALL *PInvokeFunc)(int32_t, float, float);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	HmdMatrix44_t_t664273159  returnValue = il2cppPInvokeFunc(___eEye0, ___fNearZ1, ___fFarZ2);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetProjectionMatrix::BeginInvoke(Valve.VR.EVREye,System.Single,System.Single,System.AsyncCallback,System.Object)
extern Il2CppClass* EVREye_t3088716538_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t _GetProjectionMatrix_BeginInvoke_m359430866_MetadataUsageId;
extern "C"  Il2CppObject * _GetProjectionMatrix_BeginInvoke_m359430866 (_GetProjectionMatrix_t2621141914 * __this, int32_t ___eEye0, float ___fNearZ1, float ___fFarZ2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetProjectionMatrix_BeginInvoke_m359430866_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(EVREye_t3088716538_il2cpp_TypeInfo_var, &___eEye0);
	__d_args[1] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___fNearZ1);
	__d_args[2] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___fFarZ2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// Valve.VR.HmdMatrix44_t Valve.VR.IVRSystem/_GetProjectionMatrix::EndInvoke(System.IAsyncResult)
extern "C"  HmdMatrix44_t_t664273159  _GetProjectionMatrix_EndInvoke_m722289619 (_GetProjectionMatrix_t2621141914 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(HmdMatrix44_t_t664273159 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetProjectionRaw::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetProjectionRaw__ctor_m1598285270 (_GetProjectionRaw_t3426995441 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRSystem/_GetProjectionRaw::Invoke(Valve.VR.EVREye,System.Single&,System.Single&,System.Single&,System.Single&)
extern "C"  void _GetProjectionRaw_Invoke_m4200820360 (_GetProjectionRaw_t3426995441 * __this, int32_t ___eEye0, float* ___pfLeft1, float* ___pfRight2, float* ___pfTop3, float* ___pfBottom4, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetProjectionRaw_Invoke_m4200820360((_GetProjectionRaw_t3426995441 *)__this->get_prev_9(),___eEye0, ___pfLeft1, ___pfRight2, ___pfTop3, ___pfBottom4, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___eEye0, float* ___pfLeft1, float* ___pfRight2, float* ___pfTop3, float* ___pfBottom4, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eEye0, ___pfLeft1, ___pfRight2, ___pfTop3, ___pfBottom4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___eEye0, float* ___pfLeft1, float* ___pfRight2, float* ___pfTop3, float* ___pfBottom4, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eEye0, ___pfLeft1, ___pfRight2, ___pfTop3, ___pfBottom4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__GetProjectionRaw_t3426995441 (_GetProjectionRaw_t3426995441 * __this, int32_t ___eEye0, float* ___pfLeft1, float* ___pfRight2, float* ___pfTop3, float* ___pfBottom4, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, float*, float*, float*, float*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___eEye0, ___pfLeft1, ___pfRight2, ___pfTop3, ___pfBottom4);

}
// System.IAsyncResult Valve.VR.IVRSystem/_GetProjectionRaw::BeginInvoke(Valve.VR.EVREye,System.Single&,System.Single&,System.Single&,System.Single&,System.AsyncCallback,System.Object)
extern Il2CppClass* EVREye_t3088716538_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t _GetProjectionRaw_BeginInvoke_m1246333403_MetadataUsageId;
extern "C"  Il2CppObject * _GetProjectionRaw_BeginInvoke_m1246333403 (_GetProjectionRaw_t3426995441 * __this, int32_t ___eEye0, float* ___pfLeft1, float* ___pfRight2, float* ___pfTop3, float* ___pfBottom4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetProjectionRaw_BeginInvoke_m1246333403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = Box(EVREye_t3088716538_il2cpp_TypeInfo_var, &___eEye0);
	__d_args[1] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &(*___pfLeft1));
	__d_args[2] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &(*___pfRight2));
	__d_args[3] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &(*___pfTop3));
	__d_args[4] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &(*___pfBottom4));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback5, (Il2CppObject*)___object6);
}
// System.Void Valve.VR.IVRSystem/_GetProjectionRaw::EndInvoke(System.Single&,System.Single&,System.Single&,System.Single&,System.IAsyncResult)
extern "C"  void _GetProjectionRaw_EndInvoke_m1852423016 (_GetProjectionRaw_t3426995441 * __this, float* ___pfLeft0, float* ___pfRight1, float* ___pfTop2, float* ___pfBottom3, Il2CppObject * ___result4, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pfLeft0,
	___pfRight1,
	___pfTop2,
	___pfBottom3,
	};
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result4, ___out_args);
}
// System.Void Valve.VR.IVRSystem/_GetPropErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetPropErrorNameFromEnum__ctor_m2861715900 (_GetPropErrorNameFromEnum_t1193025139 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.IntPtr Valve.VR.IVRSystem/_GetPropErrorNameFromEnum::Invoke(Valve.VR.ETrackedPropertyError)
extern "C"  IntPtr_t _GetPropErrorNameFromEnum_Invoke_m969431979 (_GetPropErrorNameFromEnum_t1193025139 * __this, int32_t ___error0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetPropErrorNameFromEnum_Invoke_m969431979((_GetPropErrorNameFromEnum_t1193025139 *)__this->get_prev_9(),___error0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef IntPtr_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___error0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___error0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef IntPtr_t (*FunctionPointerType) (void* __this, int32_t ___error0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___error0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  IntPtr_t DelegatePInvokeWrapper__GetPropErrorNameFromEnum_t1193025139 (_GetPropErrorNameFromEnum_t1193025139 * __this, int32_t ___error0, const MethodInfo* method)
{
	typedef intptr_t (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc(___error0);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetPropErrorNameFromEnum::BeginInvoke(Valve.VR.ETrackedPropertyError,System.AsyncCallback,System.Object)
extern Il2CppClass* ETrackedPropertyError_t3340022390_il2cpp_TypeInfo_var;
extern const uint32_t _GetPropErrorNameFromEnum_BeginInvoke_m3519723757_MetadataUsageId;
extern "C"  Il2CppObject * _GetPropErrorNameFromEnum_BeginInvoke_m3519723757 (_GetPropErrorNameFromEnum_t1193025139 * __this, int32_t ___error0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetPropErrorNameFromEnum_BeginInvoke_m3519723757_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ETrackedPropertyError_t3340022390_il2cpp_TypeInfo_var, &___error0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.IntPtr Valve.VR.IVRSystem/_GetPropErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetPropErrorNameFromEnum_EndInvoke_m2540180665 (_GetPropErrorNameFromEnum_t1193025139 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(IntPtr_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetRawZeroPoseToStandingAbsoluteTrackingPose::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetRawZeroPoseToStandingAbsoluteTrackingPose__ctor_m1449908402 (_GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.HmdMatrix34_t Valve.VR.IVRSystem/_GetRawZeroPoseToStandingAbsoluteTrackingPose::Invoke()
extern "C"  HmdMatrix34_t_t664273062  _GetRawZeroPoseToStandingAbsoluteTrackingPose_Invoke_m3003872993 (_GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetRawZeroPoseToStandingAbsoluteTrackingPose_Invoke_m3003872993((_GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef HmdMatrix34_t_t664273062  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef HmdMatrix34_t_t664273062  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  HmdMatrix34_t_t664273062  DelegatePInvokeWrapper__GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273 (_GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273 * __this, const MethodInfo* method)
{
	typedef HmdMatrix34_t_t664273062  (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	HmdMatrix34_t_t664273062  returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetRawZeroPoseToStandingAbsoluteTrackingPose::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetRawZeroPoseToStandingAbsoluteTrackingPose_BeginInvoke_m666182783 (_GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// Valve.VR.HmdMatrix34_t Valve.VR.IVRSystem/_GetRawZeroPoseToStandingAbsoluteTrackingPose::EndInvoke(System.IAsyncResult)
extern "C"  HmdMatrix34_t_t664273062  _GetRawZeroPoseToStandingAbsoluteTrackingPose_EndInvoke_m293114191 (_GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(HmdMatrix34_t_t664273062 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetRecommendedRenderTargetSize::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetRecommendedRenderTargetSize__ctor_m2113485512 (_GetRecommendedRenderTargetSize_t4195542627 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Valve.VR.IVRSystem/_GetRecommendedRenderTargetSize::Invoke(System.UInt32&,System.UInt32&)
extern "C"  void _GetRecommendedRenderTargetSize_Invoke_m4106040300 (_GetRecommendedRenderTargetSize_t4195542627 * __this, uint32_t* ___pnWidth0, uint32_t* ___pnHeight1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetRecommendedRenderTargetSize_Invoke_m4106040300((_GetRecommendedRenderTargetSize_t4195542627 *)__this->get_prev_9(),___pnWidth0, ___pnHeight1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t* ___pnWidth0, uint32_t* ___pnHeight1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pnWidth0, ___pnHeight1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint32_t* ___pnWidth0, uint32_t* ___pnHeight1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pnWidth0, ___pnHeight1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper__GetRecommendedRenderTargetSize_t4195542627 (_GetRecommendedRenderTargetSize_t4195542627 * __this, uint32_t* ___pnWidth0, uint32_t* ___pnHeight1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(uint32_t*, uint32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___pnWidth0, ___pnHeight1);

}
// System.IAsyncResult Valve.VR.IVRSystem/_GetRecommendedRenderTargetSize::BeginInvoke(System.UInt32&,System.UInt32&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _GetRecommendedRenderTargetSize_BeginInvoke_m894391113_MetadataUsageId;
extern "C"  Il2CppObject * _GetRecommendedRenderTargetSize_BeginInvoke_m894391113 (_GetRecommendedRenderTargetSize_t4195542627 * __this, uint32_t* ___pnWidth0, uint32_t* ___pnHeight1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetRecommendedRenderTargetSize_BeginInvoke_m894391113_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &(*___pnWidth0));
	__d_args[1] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &(*___pnHeight1));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void Valve.VR.IVRSystem/_GetRecommendedRenderTargetSize::EndInvoke(System.UInt32&,System.UInt32&,System.IAsyncResult)
extern "C"  void _GetRecommendedRenderTargetSize_EndInvoke_m2432261258 (_GetRecommendedRenderTargetSize_t4195542627 * __this, uint32_t* ___pnWidth0, uint32_t* ___pnHeight1, Il2CppObject * ___result2, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pnWidth0,
	___pnHeight1,
	};
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result2, ___out_args);
}
// System.Void Valve.VR.IVRSystem/_GetSeatedZeroPoseToStandingAbsoluteTrackingPose::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetSeatedZeroPoseToStandingAbsoluteTrackingPose__ctor_m3085694044 (_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.HmdMatrix34_t Valve.VR.IVRSystem/_GetSeatedZeroPoseToStandingAbsoluteTrackingPose::Invoke()
extern "C"  HmdMatrix34_t_t664273062  _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_Invoke_m2248037343 (_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_Invoke_m2248037343((_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef HmdMatrix34_t_t664273062  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef HmdMatrix34_t_t664273062  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  HmdMatrix34_t_t664273062  DelegatePInvokeWrapper__GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835 (_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835 * __this, const MethodInfo* method)
{
	typedef HmdMatrix34_t_t664273062  (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	HmdMatrix34_t_t664273062  returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetSeatedZeroPoseToStandingAbsoluteTrackingPose::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_BeginInvoke_m401331261 (_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// Valve.VR.HmdMatrix34_t Valve.VR.IVRSystem/_GetSeatedZeroPoseToStandingAbsoluteTrackingPose::EndInvoke(System.IAsyncResult)
extern "C"  HmdMatrix34_t_t664273062  _GetSeatedZeroPoseToStandingAbsoluteTrackingPose_EndInvoke_m3761222813 (_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(HmdMatrix34_t_t664273062 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetSortedTrackedDeviceIndicesOfClass::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetSortedTrackedDeviceIndicesOfClass__ctor_m2939470824 (_GetSortedTrackedDeviceIndicesOfClass_t3492202929 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UInt32 Valve.VR.IVRSystem/_GetSortedTrackedDeviceIndicesOfClass::Invoke(Valve.VR.ETrackedDeviceClass,System.UInt32[],System.UInt32,System.UInt32)
extern "C"  uint32_t _GetSortedTrackedDeviceIndicesOfClass_Invoke_m84543012 (_GetSortedTrackedDeviceIndicesOfClass_t3492202929 * __this, int32_t ___eTrackedDeviceClass0, UInt32U5BU5D_t59386216* ___punTrackedDeviceIndexArray1, uint32_t ___unTrackedDeviceIndexArrayCount2, uint32_t ___unRelativeToTrackedDeviceIndex3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetSortedTrackedDeviceIndicesOfClass_Invoke_m84543012((_GetSortedTrackedDeviceIndicesOfClass_t3492202929 *)__this->get_prev_9(),___eTrackedDeviceClass0, ___punTrackedDeviceIndexArray1, ___unTrackedDeviceIndexArrayCount2, ___unRelativeToTrackedDeviceIndex3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___eTrackedDeviceClass0, UInt32U5BU5D_t59386216* ___punTrackedDeviceIndexArray1, uint32_t ___unTrackedDeviceIndexArrayCount2, uint32_t ___unRelativeToTrackedDeviceIndex3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eTrackedDeviceClass0, ___punTrackedDeviceIndexArray1, ___unTrackedDeviceIndexArrayCount2, ___unRelativeToTrackedDeviceIndex3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, int32_t ___eTrackedDeviceClass0, UInt32U5BU5D_t59386216* ___punTrackedDeviceIndexArray1, uint32_t ___unTrackedDeviceIndexArrayCount2, uint32_t ___unRelativeToTrackedDeviceIndex3, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eTrackedDeviceClass0, ___punTrackedDeviceIndexArray1, ___unTrackedDeviceIndexArrayCount2, ___unRelativeToTrackedDeviceIndex3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  uint32_t DelegatePInvokeWrapper__GetSortedTrackedDeviceIndicesOfClass_t3492202929 (_GetSortedTrackedDeviceIndicesOfClass_t3492202929 * __this, int32_t ___eTrackedDeviceClass0, UInt32U5BU5D_t59386216* ___punTrackedDeviceIndexArray1, uint32_t ___unTrackedDeviceIndexArrayCount2, uint32_t ___unRelativeToTrackedDeviceIndex3, const MethodInfo* method)
{
	typedef uint32_t (STDCALL *PInvokeFunc)(int32_t, uint32_t*, uint32_t, uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___punTrackedDeviceIndexArray1' to native representation
	uint32_t* ____punTrackedDeviceIndexArray1_marshaled = NULL;
	if (___punTrackedDeviceIndexArray1 != NULL)
	{
		____punTrackedDeviceIndexArray1_marshaled = reinterpret_cast<uint32_t*>((___punTrackedDeviceIndexArray1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	uint32_t returnValue = il2cppPInvokeFunc(___eTrackedDeviceClass0, ____punTrackedDeviceIndexArray1_marshaled, ___unTrackedDeviceIndexArrayCount2, ___unRelativeToTrackedDeviceIndex3);

	// Marshaling of parameter '___punTrackedDeviceIndexArray1' back from native representation
	if (____punTrackedDeviceIndexArray1_marshaled != NULL)
	{
		int32_t ____punTrackedDeviceIndexArray1_Length = (___punTrackedDeviceIndexArray1)->max_length;
		for (int32_t i = 0; i < ____punTrackedDeviceIndexArray1_Length; i++)
		{
			(___punTrackedDeviceIndexArray1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____punTrackedDeviceIndexArray1_marshaled)[i]);
		}
	}

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetSortedTrackedDeviceIndicesOfClass::BeginInvoke(Valve.VR.ETrackedDeviceClass,System.UInt32[],System.UInt32,System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* ETrackedDeviceClass_t2121051631_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _GetSortedTrackedDeviceIndicesOfClass_BeginInvoke_m467037688_MetadataUsageId;
extern "C"  Il2CppObject * _GetSortedTrackedDeviceIndicesOfClass_BeginInvoke_m467037688 (_GetSortedTrackedDeviceIndicesOfClass_t3492202929 * __this, int32_t ___eTrackedDeviceClass0, UInt32U5BU5D_t59386216* ___punTrackedDeviceIndexArray1, uint32_t ___unTrackedDeviceIndexArrayCount2, uint32_t ___unRelativeToTrackedDeviceIndex3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetSortedTrackedDeviceIndicesOfClass_BeginInvoke_m467037688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(ETrackedDeviceClass_t2121051631_il2cpp_TypeInfo_var, &___eTrackedDeviceClass0);
	__d_args[1] = ___punTrackedDeviceIndexArray1;
	__d_args[2] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unTrackedDeviceIndexArrayCount2);
	__d_args[3] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unRelativeToTrackedDeviceIndex3);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.UInt32 Valve.VR.IVRSystem/_GetSortedTrackedDeviceIndicesOfClass::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetSortedTrackedDeviceIndicesOfClass_EndInvoke_m3026927929 (_GetSortedTrackedDeviceIndicesOfClass_t3492202929 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetStringTrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetStringTrackedDeviceProperty__ctor_m500825725 (_GetStringTrackedDeviceProperty_t87797800 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UInt32 Valve.VR.IVRSystem/_GetStringTrackedDeviceProperty::Invoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,System.Text.StringBuilder,System.UInt32,Valve.VR.ETrackedPropertyError&)
extern "C"  uint32_t _GetStringTrackedDeviceProperty_Invoke_m2122365806 (_GetStringTrackedDeviceProperty_t87797800 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unBufferSize3, int32_t* ___pError4, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetStringTrackedDeviceProperty_Invoke_m2122365806((_GetStringTrackedDeviceProperty_t87797800 *)__this->get_prev_9(),___unDeviceIndex0, ___prop1, ___pchValue2, ___unBufferSize3, ___pError4, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unBufferSize3, int32_t* ___pError4, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___unDeviceIndex0, ___prop1, ___pchValue2, ___unBufferSize3, ___pError4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unBufferSize3, int32_t* ___pError4, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___unDeviceIndex0, ___prop1, ___pchValue2, ___unBufferSize3, ___pError4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  uint32_t DelegatePInvokeWrapper__GetStringTrackedDeviceProperty_t87797800 (_GetStringTrackedDeviceProperty_t87797800 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unBufferSize3, int32_t* ___pError4, const MethodInfo* method)
{
	typedef uint32_t (STDCALL *PInvokeFunc)(uint32_t, int32_t, char*, uint32_t, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___pchValue2' to native representation
	char* ____pchValue2_marshaled = NULL;
	____pchValue2_marshaled = il2cpp_codegen_marshal_string_builder(___pchValue2);

	// Native function invocation
	uint32_t returnValue = il2cppPInvokeFunc(___unDeviceIndex0, ___prop1, ____pchValue2_marshaled, ___unBufferSize3, ___pError4);

	// Marshaling of parameter '___pchValue2' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___pchValue2, ____pchValue2_marshaled);

	// Marshaling cleanup of parameter '___pchValue2' native representation
	il2cpp_codegen_marshal_free(____pchValue2_marshaled);
	____pchValue2_marshaled = NULL;

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetStringTrackedDeviceProperty::BeginInvoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,System.Text.StringBuilder,System.UInt32,Valve.VR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* ETrackedDeviceProperty_t3226377054_il2cpp_TypeInfo_var;
extern Il2CppClass* ETrackedPropertyError_t3340022390_il2cpp_TypeInfo_var;
extern const uint32_t _GetStringTrackedDeviceProperty_BeginInvoke_m3045677472_MetadataUsageId;
extern "C"  Il2CppObject * _GetStringTrackedDeviceProperty_BeginInvoke_m3045677472 (_GetStringTrackedDeviceProperty_t87797800 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unBufferSize3, int32_t* ___pError4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetStringTrackedDeviceProperty_BeginInvoke_m3045677472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unDeviceIndex0);
	__d_args[1] = Box(ETrackedDeviceProperty_t3226377054_il2cpp_TypeInfo_var, &___prop1);
	__d_args[2] = ___pchValue2;
	__d_args[3] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unBufferSize3);
	__d_args[4] = Box(ETrackedPropertyError_t3340022390_il2cpp_TypeInfo_var, &(*___pError4));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback5, (Il2CppObject*)___object6);
}
// System.UInt32 Valve.VR.IVRSystem/_GetStringTrackedDeviceProperty::EndInvoke(Valve.VR.ETrackedPropertyError&,System.IAsyncResult)
extern "C"  uint32_t _GetStringTrackedDeviceProperty_EndInvoke_m1782671678 (_GetStringTrackedDeviceProperty_t87797800 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pError0,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetTimeSinceLastVsync::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetTimeSinceLastVsync__ctor_m514120617 (_GetTimeSinceLastVsync_t1215702688 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean Valve.VR.IVRSystem/_GetTimeSinceLastVsync::Invoke(System.Single&,System.UInt64&)
extern "C"  bool _GetTimeSinceLastVsync_Invoke_m149520711 (_GetTimeSinceLastVsync_t1215702688 * __this, float* ___pfSecondsSinceLastVsync0, uint64_t* ___pulFrameCounter1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetTimeSinceLastVsync_Invoke_m149520711((_GetTimeSinceLastVsync_t1215702688 *)__this->get_prev_9(),___pfSecondsSinceLastVsync0, ___pulFrameCounter1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, float* ___pfSecondsSinceLastVsync0, uint64_t* ___pulFrameCounter1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pfSecondsSinceLastVsync0, ___pulFrameCounter1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, float* ___pfSecondsSinceLastVsync0, uint64_t* ___pulFrameCounter1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pfSecondsSinceLastVsync0, ___pulFrameCounter1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  bool DelegatePInvokeWrapper__GetTimeSinceLastVsync_t1215702688 (_GetTimeSinceLastVsync_t1215702688 * __this, float* ___pfSecondsSinceLastVsync0, uint64_t* ___pulFrameCounter1, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(float*, uint64_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___pfSecondsSinceLastVsync0, ___pulFrameCounter1);

	return static_cast<bool>(returnValue);
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetTimeSinceLastVsync::BeginInvoke(System.Single&,System.UInt64&,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern const uint32_t _GetTimeSinceLastVsync_BeginInvoke_m2443950666_MetadataUsageId;
extern "C"  Il2CppObject * _GetTimeSinceLastVsync_BeginInvoke_m2443950666 (_GetTimeSinceLastVsync_t1215702688 * __this, float* ___pfSecondsSinceLastVsync0, uint64_t* ___pulFrameCounter1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetTimeSinceLastVsync_BeginInvoke_m2443950666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &(*___pfSecondsSinceLastVsync0));
	__d_args[1] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &(*___pulFrameCounter1));
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Boolean Valve.VR.IVRSystem/_GetTimeSinceLastVsync::EndInvoke(System.Single&,System.UInt64&,System.IAsyncResult)
extern "C"  bool _GetTimeSinceLastVsync_EndInvoke_m3244267461 (_GetTimeSinceLastVsync_t1215702688 * __this, float* ___pfSecondsSinceLastVsync0, uint64_t* ___pulFrameCounter1, Il2CppObject * ___result2, const MethodInfo* method)
{
	void* ___out_args[] = {
	___pfSecondsSinceLastVsync0,
	___pulFrameCounter1,
	};
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result2, ___out_args);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetTrackedDeviceActivityLevel::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetTrackedDeviceActivityLevel__ctor_m2869817562 (_GetTrackedDeviceActivityLevel_t212130385 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.EDeviceActivityLevel Valve.VR.IVRSystem/_GetTrackedDeviceActivityLevel::Invoke(System.UInt32)
extern "C"  int32_t _GetTrackedDeviceActivityLevel_Invoke_m1284377401 (_GetTrackedDeviceActivityLevel_t212130385 * __this, uint32_t ___unDeviceId0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetTrackedDeviceActivityLevel_Invoke_m1284377401((_GetTrackedDeviceActivityLevel_t212130385 *)__this->get_prev_9(),___unDeviceId0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___unDeviceId0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___unDeviceId0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint32_t ___unDeviceId0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___unDeviceId0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__GetTrackedDeviceActivityLevel_t212130385 (_GetTrackedDeviceActivityLevel_t212130385 * __this, uint32_t ___unDeviceId0, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___unDeviceId0);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetTrackedDeviceActivityLevel::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _GetTrackedDeviceActivityLevel_BeginInvoke_m111391887_MetadataUsageId;
extern "C"  Il2CppObject * _GetTrackedDeviceActivityLevel_BeginInvoke_m111391887 (_GetTrackedDeviceActivityLevel_t212130385 * __this, uint32_t ___unDeviceId0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetTrackedDeviceActivityLevel_BeginInvoke_m111391887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unDeviceId0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// Valve.VR.EDeviceActivityLevel Valve.VR.IVRSystem/_GetTrackedDeviceActivityLevel::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _GetTrackedDeviceActivityLevel_EndInvoke_m293185487 (_GetTrackedDeviceActivityLevel_t212130385 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void Valve.VR.IVRSystem/_GetTrackedDeviceClass::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetTrackedDeviceClass__ctor_m1939001105 (_GetTrackedDeviceClass_t1455580370 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Valve.VR.ETrackedDeviceClass Valve.VR.IVRSystem/_GetTrackedDeviceClass::Invoke(System.UInt32)
extern "C"  int32_t _GetTrackedDeviceClass_Invoke_m3026082667 (_GetTrackedDeviceClass_t1455580370 * __this, uint32_t ___unDeviceIndex0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		_GetTrackedDeviceClass_Invoke_m3026082667((_GetTrackedDeviceClass_t1455580370 *)__this->get_prev_9(),___unDeviceIndex0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___unDeviceIndex0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___unDeviceIndex0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, uint32_t ___unDeviceIndex0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___unDeviceIndex0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper__GetTrackedDeviceClass_t1455580370 (_GetTrackedDeviceClass_t1455580370 * __this, uint32_t ___unDeviceIndex0, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___unDeviceIndex0);

	return returnValue;
}
// System.IAsyncResult Valve.VR.IVRSystem/_GetTrackedDeviceClass::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t _GetTrackedDeviceClass_BeginInvoke_m2261278416_MetadataUsageId;
extern "C"  Il2CppObject * _GetTrackedDeviceClass_BeginInvoke_m2261278416 (_GetTrackedDeviceClass_t1455580370 * __this, uint32_t ___unDeviceIndex0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_GetTrackedDeviceClass_BeginInvoke_m2261278416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___unDeviceIndex0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// Valve.VR.ETrackedDeviceClass Valve.VR.IVRSystem/_GetTrackedDeviceClass::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _GetTrackedDeviceClass_EndInvoke_m3498798641 (_GetTrackedDeviceClass_t1455580370 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
