﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_GetFrameTiming
struct _GetFrameTiming_t1729400753;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_Compositor_FrameTiming2839634313.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_GetFrameTiming::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetFrameTiming__ctor_m1103026376 (_GetFrameTiming_t1729400753 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRCompositor/_GetFrameTiming::Invoke(Valve.VR.Compositor_FrameTiming&,System.UInt32)
extern "C"  bool _GetFrameTiming_Invoke_m2707021913 (_GetFrameTiming_t1729400753 * __this, Compositor_FrameTiming_t2839634313 * ___pTiming0, uint32_t ___unFramesAgo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_GetFrameTiming::BeginInvoke(Valve.VR.Compositor_FrameTiming&,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetFrameTiming_BeginInvoke_m1203696878 (_GetFrameTiming_t1729400753 * __this, Compositor_FrameTiming_t2839634313 * ___pTiming0, uint32_t ___unFramesAgo1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRCompositor/_GetFrameTiming::EndInvoke(Valve.VR.Compositor_FrameTiming&,System.IAsyncResult)
extern "C"  bool _GetFrameTiming_EndInvoke_m3983238027 (_GetFrameTiming_t1729400753 * __this, Compositor_FrameTiming_t2839634313 * ___pTiming0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
