﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.InteractableButtonEvents
struct  InteractableButtonEvents_t1568724519  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.InteractableButtonEvents::onTriggerDown
	UnityEvent_t408735097 * ___onTriggerDown_2;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.InteractableButtonEvents::onTriggerUp
	UnityEvent_t408735097 * ___onTriggerUp_3;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.InteractableButtonEvents::onGripDown
	UnityEvent_t408735097 * ___onGripDown_4;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.InteractableButtonEvents::onGripUp
	UnityEvent_t408735097 * ___onGripUp_5;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.InteractableButtonEvents::onTouchpadDown
	UnityEvent_t408735097 * ___onTouchpadDown_6;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.InteractableButtonEvents::onTouchpadUp
	UnityEvent_t408735097 * ___onTouchpadUp_7;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.InteractableButtonEvents::onTouchpadTouch
	UnityEvent_t408735097 * ___onTouchpadTouch_8;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.InteractableButtonEvents::onTouchpadRelease
	UnityEvent_t408735097 * ___onTouchpadRelease_9;

public:
	inline static int32_t get_offset_of_onTriggerDown_2() { return static_cast<int32_t>(offsetof(InteractableButtonEvents_t1568724519, ___onTriggerDown_2)); }
	inline UnityEvent_t408735097 * get_onTriggerDown_2() const { return ___onTriggerDown_2; }
	inline UnityEvent_t408735097 ** get_address_of_onTriggerDown_2() { return &___onTriggerDown_2; }
	inline void set_onTriggerDown_2(UnityEvent_t408735097 * value)
	{
		___onTriggerDown_2 = value;
		Il2CppCodeGenWriteBarrier(&___onTriggerDown_2, value);
	}

	inline static int32_t get_offset_of_onTriggerUp_3() { return static_cast<int32_t>(offsetof(InteractableButtonEvents_t1568724519, ___onTriggerUp_3)); }
	inline UnityEvent_t408735097 * get_onTriggerUp_3() const { return ___onTriggerUp_3; }
	inline UnityEvent_t408735097 ** get_address_of_onTriggerUp_3() { return &___onTriggerUp_3; }
	inline void set_onTriggerUp_3(UnityEvent_t408735097 * value)
	{
		___onTriggerUp_3 = value;
		Il2CppCodeGenWriteBarrier(&___onTriggerUp_3, value);
	}

	inline static int32_t get_offset_of_onGripDown_4() { return static_cast<int32_t>(offsetof(InteractableButtonEvents_t1568724519, ___onGripDown_4)); }
	inline UnityEvent_t408735097 * get_onGripDown_4() const { return ___onGripDown_4; }
	inline UnityEvent_t408735097 ** get_address_of_onGripDown_4() { return &___onGripDown_4; }
	inline void set_onGripDown_4(UnityEvent_t408735097 * value)
	{
		___onGripDown_4 = value;
		Il2CppCodeGenWriteBarrier(&___onGripDown_4, value);
	}

	inline static int32_t get_offset_of_onGripUp_5() { return static_cast<int32_t>(offsetof(InteractableButtonEvents_t1568724519, ___onGripUp_5)); }
	inline UnityEvent_t408735097 * get_onGripUp_5() const { return ___onGripUp_5; }
	inline UnityEvent_t408735097 ** get_address_of_onGripUp_5() { return &___onGripUp_5; }
	inline void set_onGripUp_5(UnityEvent_t408735097 * value)
	{
		___onGripUp_5 = value;
		Il2CppCodeGenWriteBarrier(&___onGripUp_5, value);
	}

	inline static int32_t get_offset_of_onTouchpadDown_6() { return static_cast<int32_t>(offsetof(InteractableButtonEvents_t1568724519, ___onTouchpadDown_6)); }
	inline UnityEvent_t408735097 * get_onTouchpadDown_6() const { return ___onTouchpadDown_6; }
	inline UnityEvent_t408735097 ** get_address_of_onTouchpadDown_6() { return &___onTouchpadDown_6; }
	inline void set_onTouchpadDown_6(UnityEvent_t408735097 * value)
	{
		___onTouchpadDown_6 = value;
		Il2CppCodeGenWriteBarrier(&___onTouchpadDown_6, value);
	}

	inline static int32_t get_offset_of_onTouchpadUp_7() { return static_cast<int32_t>(offsetof(InteractableButtonEvents_t1568724519, ___onTouchpadUp_7)); }
	inline UnityEvent_t408735097 * get_onTouchpadUp_7() const { return ___onTouchpadUp_7; }
	inline UnityEvent_t408735097 ** get_address_of_onTouchpadUp_7() { return &___onTouchpadUp_7; }
	inline void set_onTouchpadUp_7(UnityEvent_t408735097 * value)
	{
		___onTouchpadUp_7 = value;
		Il2CppCodeGenWriteBarrier(&___onTouchpadUp_7, value);
	}

	inline static int32_t get_offset_of_onTouchpadTouch_8() { return static_cast<int32_t>(offsetof(InteractableButtonEvents_t1568724519, ___onTouchpadTouch_8)); }
	inline UnityEvent_t408735097 * get_onTouchpadTouch_8() const { return ___onTouchpadTouch_8; }
	inline UnityEvent_t408735097 ** get_address_of_onTouchpadTouch_8() { return &___onTouchpadTouch_8; }
	inline void set_onTouchpadTouch_8(UnityEvent_t408735097 * value)
	{
		___onTouchpadTouch_8 = value;
		Il2CppCodeGenWriteBarrier(&___onTouchpadTouch_8, value);
	}

	inline static int32_t get_offset_of_onTouchpadRelease_9() { return static_cast<int32_t>(offsetof(InteractableButtonEvents_t1568724519, ___onTouchpadRelease_9)); }
	inline UnityEvent_t408735097 * get_onTouchpadRelease_9() const { return ___onTouchpadRelease_9; }
	inline UnityEvent_t408735097 ** get_address_of_onTouchpadRelease_9() { return &___onTouchpadRelease_9; }
	inline void set_onTouchpadRelease_9(UnityEvent_t408735097 * value)
	{
		___onTouchpadRelease_9 = value;
		Il2CppCodeGenWriteBarrier(&___onTouchpadRelease_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
