﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DestinationMarkerEventArgs>
struct UnityEvent_2_t535138270;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.DestinationMarkerEventArgs>
struct UnityAction_2_t2947907648;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventArgs1852451661.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DestinationMarkerEventArgs>::.ctor()
extern "C"  void UnityEvent_2__ctor_m3146912912_gshared (UnityEvent_2_t535138270 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m3146912912(__this, method) ((  void (*) (UnityEvent_2_t535138270 *, const MethodInfo*))UnityEvent_2__ctor_m3146912912_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DestinationMarkerEventArgs>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m1794707726_gshared (UnityEvent_2_t535138270 * __this, UnityAction_2_t2947907648 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m1794707726(__this, ___call0, method) ((  void (*) (UnityEvent_2_t535138270 *, UnityAction_2_t2947907648 *, const MethodInfo*))UnityEvent_2_AddListener_m1794707726_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DestinationMarkerEventArgs>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m151120715_gshared (UnityEvent_2_t535138270 * __this, UnityAction_2_t2947907648 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m151120715(__this, ___call0, method) ((  void (*) (UnityEvent_2_t535138270 *, UnityAction_2_t2947907648 *, const MethodInfo*))UnityEvent_2_RemoveListener_m151120715_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DestinationMarkerEventArgs>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m4068334722_gshared (UnityEvent_2_t535138270 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m4068334722(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t535138270 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m4068334722_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DestinationMarkerEventArgs>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m3021528934_gshared (UnityEvent_2_t535138270 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m3021528934(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t535138270 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m3021528934_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DestinationMarkerEventArgs>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m3952412479_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t2947907648 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m3952412479(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t2947907648 *, const MethodInfo*))UnityEvent_2_GetDelegate_m3952412479_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.DestinationMarkerEventArgs>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m3588472641_gshared (UnityEvent_2_t535138270 * __this, Il2CppObject * ___arg00, DestinationMarkerEventArgs_t1852451661  ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m3588472641(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t535138270 *, Il2CppObject *, DestinationMarkerEventArgs_t1852451661 , const MethodInfo*))UnityEvent_2_Invoke_m3588472641_gshared)(__this, ___arg00, ___arg11, method)
