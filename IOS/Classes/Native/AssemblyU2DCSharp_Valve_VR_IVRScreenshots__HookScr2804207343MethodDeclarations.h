﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRScreenshots/_HookScreenshot
struct _HookScreenshot_t2804207343;
// System.Object
struct Il2CppObject;
// Valve.VR.EVRScreenshotType[]
struct EVRScreenshotTypeU5BU5D_t2594501106;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotError1400268927.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRScreenshots/_HookScreenshot::.ctor(System.Object,System.IntPtr)
extern "C"  void _HookScreenshot__ctor_m3444396258 (_HookScreenshot_t2804207343 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_HookScreenshot::Invoke(Valve.VR.EVRScreenshotType[],System.Int32)
extern "C"  int32_t _HookScreenshot_Invoke_m625201236 (_HookScreenshot_t2804207343 * __this, EVRScreenshotTypeU5BU5D_t2594501106* ___pSupportedTypes0, int32_t ___numTypes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRScreenshots/_HookScreenshot::BeginInvoke(Valve.VR.EVRScreenshotType[],System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _HookScreenshot_BeginInvoke_m2347167407 (_HookScreenshot_t2804207343 * __this, EVRScreenshotTypeU5BU5D_t2594501106* ___pSupportedTypes0, int32_t ___numTypes1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_HookScreenshot::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _HookScreenshot_EndInvoke_m1843050030 (_HookScreenshot_t2804207343 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
