﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/ActionNoArgs
struct ActionNoArgs_t953081287;
// SteamVR_Events/Event
struct Event_t1855872343;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event1855872343.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"

// System.Void SteamVR_Events/ActionNoArgs::.ctor(SteamVR_Events/Event,UnityEngine.Events.UnityAction)
extern "C"  void ActionNoArgs__ctor_m3607943440 (ActionNoArgs_t953081287 * __this, Event_t1855872343 * ____event0, UnityAction_t4025899511 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Events/ActionNoArgs::Enable(System.Boolean)
extern "C"  void ActionNoArgs_Enable_m2539320268 (ActionNoArgs_t953081287 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
