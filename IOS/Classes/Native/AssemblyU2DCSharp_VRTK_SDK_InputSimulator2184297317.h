﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.SDK_ControllerSim
struct SDK_ControllerSim_t66347054;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.SDK_InputSimulator
struct  SDK_InputSimulator_t2184297317  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.SDK_InputSimulator::hideHandsAtSwitch
	bool ___hideHandsAtSwitch_2;
	// System.Boolean VRTK.SDK_InputSimulator::resetHandsAtSwitch
	bool ___resetHandsAtSwitch_3;
	// System.Single VRTK.SDK_InputSimulator::handMoveMultiplier
	float ___handMoveMultiplier_4;
	// System.Single VRTK.SDK_InputSimulator::handRotationMultiplier
	float ___handRotationMultiplier_5;
	// System.Single VRTK.SDK_InputSimulator::playerMoveMultiplier
	float ___playerMoveMultiplier_6;
	// System.Single VRTK.SDK_InputSimulator::playerRotationMultiplier
	float ___playerRotationMultiplier_7;
	// UnityEngine.KeyCode VRTK.SDK_InputSimulator::changeHands
	int32_t ___changeHands_8;
	// UnityEngine.KeyCode VRTK.SDK_InputSimulator::handsOnOff
	int32_t ___handsOnOff_9;
	// UnityEngine.KeyCode VRTK.SDK_InputSimulator::rotationPosition
	int32_t ___rotationPosition_10;
	// UnityEngine.KeyCode VRTK.SDK_InputSimulator::changeAxis
	int32_t ___changeAxis_11;
	// UnityEngine.KeyCode VRTK.SDK_InputSimulator::triggerAlias
	int32_t ___triggerAlias_12;
	// UnityEngine.KeyCode VRTK.SDK_InputSimulator::gripAlias
	int32_t ___gripAlias_13;
	// UnityEngine.KeyCode VRTK.SDK_InputSimulator::touchpadAlias
	int32_t ___touchpadAlias_14;
	// UnityEngine.KeyCode VRTK.SDK_InputSimulator::buttonOneAlias
	int32_t ___buttonOneAlias_15;
	// UnityEngine.KeyCode VRTK.SDK_InputSimulator::buttonTwoAlias
	int32_t ___buttonTwoAlias_16;
	// UnityEngine.KeyCode VRTK.SDK_InputSimulator::startMenuAlias
	int32_t ___startMenuAlias_17;
	// UnityEngine.KeyCode VRTK.SDK_InputSimulator::touchModifier
	int32_t ___touchModifier_18;
	// UnityEngine.KeyCode VRTK.SDK_InputSimulator::hairTouchModifier
	int32_t ___hairTouchModifier_19;
	// System.Boolean VRTK.SDK_InputSimulator::isHand
	bool ___isHand_20;
	// UnityEngine.Transform VRTK.SDK_InputSimulator::rightHand
	Transform_t3275118058 * ___rightHand_21;
	// UnityEngine.Transform VRTK.SDK_InputSimulator::leftHand
	Transform_t3275118058 * ___leftHand_22;
	// UnityEngine.Transform VRTK.SDK_InputSimulator::currentHand
	Transform_t3275118058 * ___currentHand_23;
	// UnityEngine.Vector3 VRTK.SDK_InputSimulator::oldPos
	Vector3_t2243707580  ___oldPos_24;
	// UnityEngine.Transform VRTK.SDK_InputSimulator::myCamera
	Transform_t3275118058 * ___myCamera_25;
	// VRTK.SDK_ControllerSim VRTK.SDK_InputSimulator::rightController
	SDK_ControllerSim_t66347054 * ___rightController_26;
	// VRTK.SDK_ControllerSim VRTK.SDK_InputSimulator::leftController
	SDK_ControllerSim_t66347054 * ___leftController_27;

public:
	inline static int32_t get_offset_of_hideHandsAtSwitch_2() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___hideHandsAtSwitch_2)); }
	inline bool get_hideHandsAtSwitch_2() const { return ___hideHandsAtSwitch_2; }
	inline bool* get_address_of_hideHandsAtSwitch_2() { return &___hideHandsAtSwitch_2; }
	inline void set_hideHandsAtSwitch_2(bool value)
	{
		___hideHandsAtSwitch_2 = value;
	}

	inline static int32_t get_offset_of_resetHandsAtSwitch_3() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___resetHandsAtSwitch_3)); }
	inline bool get_resetHandsAtSwitch_3() const { return ___resetHandsAtSwitch_3; }
	inline bool* get_address_of_resetHandsAtSwitch_3() { return &___resetHandsAtSwitch_3; }
	inline void set_resetHandsAtSwitch_3(bool value)
	{
		___resetHandsAtSwitch_3 = value;
	}

	inline static int32_t get_offset_of_handMoveMultiplier_4() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___handMoveMultiplier_4)); }
	inline float get_handMoveMultiplier_4() const { return ___handMoveMultiplier_4; }
	inline float* get_address_of_handMoveMultiplier_4() { return &___handMoveMultiplier_4; }
	inline void set_handMoveMultiplier_4(float value)
	{
		___handMoveMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_handRotationMultiplier_5() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___handRotationMultiplier_5)); }
	inline float get_handRotationMultiplier_5() const { return ___handRotationMultiplier_5; }
	inline float* get_address_of_handRotationMultiplier_5() { return &___handRotationMultiplier_5; }
	inline void set_handRotationMultiplier_5(float value)
	{
		___handRotationMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_playerMoveMultiplier_6() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___playerMoveMultiplier_6)); }
	inline float get_playerMoveMultiplier_6() const { return ___playerMoveMultiplier_6; }
	inline float* get_address_of_playerMoveMultiplier_6() { return &___playerMoveMultiplier_6; }
	inline void set_playerMoveMultiplier_6(float value)
	{
		___playerMoveMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_playerRotationMultiplier_7() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___playerRotationMultiplier_7)); }
	inline float get_playerRotationMultiplier_7() const { return ___playerRotationMultiplier_7; }
	inline float* get_address_of_playerRotationMultiplier_7() { return &___playerRotationMultiplier_7; }
	inline void set_playerRotationMultiplier_7(float value)
	{
		___playerRotationMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_changeHands_8() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___changeHands_8)); }
	inline int32_t get_changeHands_8() const { return ___changeHands_8; }
	inline int32_t* get_address_of_changeHands_8() { return &___changeHands_8; }
	inline void set_changeHands_8(int32_t value)
	{
		___changeHands_8 = value;
	}

	inline static int32_t get_offset_of_handsOnOff_9() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___handsOnOff_9)); }
	inline int32_t get_handsOnOff_9() const { return ___handsOnOff_9; }
	inline int32_t* get_address_of_handsOnOff_9() { return &___handsOnOff_9; }
	inline void set_handsOnOff_9(int32_t value)
	{
		___handsOnOff_9 = value;
	}

	inline static int32_t get_offset_of_rotationPosition_10() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___rotationPosition_10)); }
	inline int32_t get_rotationPosition_10() const { return ___rotationPosition_10; }
	inline int32_t* get_address_of_rotationPosition_10() { return &___rotationPosition_10; }
	inline void set_rotationPosition_10(int32_t value)
	{
		___rotationPosition_10 = value;
	}

	inline static int32_t get_offset_of_changeAxis_11() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___changeAxis_11)); }
	inline int32_t get_changeAxis_11() const { return ___changeAxis_11; }
	inline int32_t* get_address_of_changeAxis_11() { return &___changeAxis_11; }
	inline void set_changeAxis_11(int32_t value)
	{
		___changeAxis_11 = value;
	}

	inline static int32_t get_offset_of_triggerAlias_12() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___triggerAlias_12)); }
	inline int32_t get_triggerAlias_12() const { return ___triggerAlias_12; }
	inline int32_t* get_address_of_triggerAlias_12() { return &___triggerAlias_12; }
	inline void set_triggerAlias_12(int32_t value)
	{
		___triggerAlias_12 = value;
	}

	inline static int32_t get_offset_of_gripAlias_13() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___gripAlias_13)); }
	inline int32_t get_gripAlias_13() const { return ___gripAlias_13; }
	inline int32_t* get_address_of_gripAlias_13() { return &___gripAlias_13; }
	inline void set_gripAlias_13(int32_t value)
	{
		___gripAlias_13 = value;
	}

	inline static int32_t get_offset_of_touchpadAlias_14() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___touchpadAlias_14)); }
	inline int32_t get_touchpadAlias_14() const { return ___touchpadAlias_14; }
	inline int32_t* get_address_of_touchpadAlias_14() { return &___touchpadAlias_14; }
	inline void set_touchpadAlias_14(int32_t value)
	{
		___touchpadAlias_14 = value;
	}

	inline static int32_t get_offset_of_buttonOneAlias_15() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___buttonOneAlias_15)); }
	inline int32_t get_buttonOneAlias_15() const { return ___buttonOneAlias_15; }
	inline int32_t* get_address_of_buttonOneAlias_15() { return &___buttonOneAlias_15; }
	inline void set_buttonOneAlias_15(int32_t value)
	{
		___buttonOneAlias_15 = value;
	}

	inline static int32_t get_offset_of_buttonTwoAlias_16() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___buttonTwoAlias_16)); }
	inline int32_t get_buttonTwoAlias_16() const { return ___buttonTwoAlias_16; }
	inline int32_t* get_address_of_buttonTwoAlias_16() { return &___buttonTwoAlias_16; }
	inline void set_buttonTwoAlias_16(int32_t value)
	{
		___buttonTwoAlias_16 = value;
	}

	inline static int32_t get_offset_of_startMenuAlias_17() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___startMenuAlias_17)); }
	inline int32_t get_startMenuAlias_17() const { return ___startMenuAlias_17; }
	inline int32_t* get_address_of_startMenuAlias_17() { return &___startMenuAlias_17; }
	inline void set_startMenuAlias_17(int32_t value)
	{
		___startMenuAlias_17 = value;
	}

	inline static int32_t get_offset_of_touchModifier_18() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___touchModifier_18)); }
	inline int32_t get_touchModifier_18() const { return ___touchModifier_18; }
	inline int32_t* get_address_of_touchModifier_18() { return &___touchModifier_18; }
	inline void set_touchModifier_18(int32_t value)
	{
		___touchModifier_18 = value;
	}

	inline static int32_t get_offset_of_hairTouchModifier_19() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___hairTouchModifier_19)); }
	inline int32_t get_hairTouchModifier_19() const { return ___hairTouchModifier_19; }
	inline int32_t* get_address_of_hairTouchModifier_19() { return &___hairTouchModifier_19; }
	inline void set_hairTouchModifier_19(int32_t value)
	{
		___hairTouchModifier_19 = value;
	}

	inline static int32_t get_offset_of_isHand_20() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___isHand_20)); }
	inline bool get_isHand_20() const { return ___isHand_20; }
	inline bool* get_address_of_isHand_20() { return &___isHand_20; }
	inline void set_isHand_20(bool value)
	{
		___isHand_20 = value;
	}

	inline static int32_t get_offset_of_rightHand_21() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___rightHand_21)); }
	inline Transform_t3275118058 * get_rightHand_21() const { return ___rightHand_21; }
	inline Transform_t3275118058 ** get_address_of_rightHand_21() { return &___rightHand_21; }
	inline void set_rightHand_21(Transform_t3275118058 * value)
	{
		___rightHand_21 = value;
		Il2CppCodeGenWriteBarrier(&___rightHand_21, value);
	}

	inline static int32_t get_offset_of_leftHand_22() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___leftHand_22)); }
	inline Transform_t3275118058 * get_leftHand_22() const { return ___leftHand_22; }
	inline Transform_t3275118058 ** get_address_of_leftHand_22() { return &___leftHand_22; }
	inline void set_leftHand_22(Transform_t3275118058 * value)
	{
		___leftHand_22 = value;
		Il2CppCodeGenWriteBarrier(&___leftHand_22, value);
	}

	inline static int32_t get_offset_of_currentHand_23() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___currentHand_23)); }
	inline Transform_t3275118058 * get_currentHand_23() const { return ___currentHand_23; }
	inline Transform_t3275118058 ** get_address_of_currentHand_23() { return &___currentHand_23; }
	inline void set_currentHand_23(Transform_t3275118058 * value)
	{
		___currentHand_23 = value;
		Il2CppCodeGenWriteBarrier(&___currentHand_23, value);
	}

	inline static int32_t get_offset_of_oldPos_24() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___oldPos_24)); }
	inline Vector3_t2243707580  get_oldPos_24() const { return ___oldPos_24; }
	inline Vector3_t2243707580 * get_address_of_oldPos_24() { return &___oldPos_24; }
	inline void set_oldPos_24(Vector3_t2243707580  value)
	{
		___oldPos_24 = value;
	}

	inline static int32_t get_offset_of_myCamera_25() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___myCamera_25)); }
	inline Transform_t3275118058 * get_myCamera_25() const { return ___myCamera_25; }
	inline Transform_t3275118058 ** get_address_of_myCamera_25() { return &___myCamera_25; }
	inline void set_myCamera_25(Transform_t3275118058 * value)
	{
		___myCamera_25 = value;
		Il2CppCodeGenWriteBarrier(&___myCamera_25, value);
	}

	inline static int32_t get_offset_of_rightController_26() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___rightController_26)); }
	inline SDK_ControllerSim_t66347054 * get_rightController_26() const { return ___rightController_26; }
	inline SDK_ControllerSim_t66347054 ** get_address_of_rightController_26() { return &___rightController_26; }
	inline void set_rightController_26(SDK_ControllerSim_t66347054 * value)
	{
		___rightController_26 = value;
		Il2CppCodeGenWriteBarrier(&___rightController_26, value);
	}

	inline static int32_t get_offset_of_leftController_27() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317, ___leftController_27)); }
	inline SDK_ControllerSim_t66347054 * get_leftController_27() const { return ___leftController_27; }
	inline SDK_ControllerSim_t66347054 ** get_address_of_leftController_27() { return &___leftController_27; }
	inline void set_leftController_27(SDK_ControllerSim_t66347054 * value)
	{
		___leftController_27 = value;
		Il2CppCodeGenWriteBarrier(&___leftController_27, value);
	}
};

struct SDK_InputSimulator_t2184297317_StaticFields
{
public:
	// UnityEngine.GameObject VRTK.SDK_InputSimulator::cachedCameraRig
	GameObject_t1756533147 * ___cachedCameraRig_28;
	// System.Boolean VRTK.SDK_InputSimulator::destroyed
	bool ___destroyed_29;

public:
	inline static int32_t get_offset_of_cachedCameraRig_28() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317_StaticFields, ___cachedCameraRig_28)); }
	inline GameObject_t1756533147 * get_cachedCameraRig_28() const { return ___cachedCameraRig_28; }
	inline GameObject_t1756533147 ** get_address_of_cachedCameraRig_28() { return &___cachedCameraRig_28; }
	inline void set_cachedCameraRig_28(GameObject_t1756533147 * value)
	{
		___cachedCameraRig_28 = value;
		Il2CppCodeGenWriteBarrier(&___cachedCameraRig_28, value);
	}

	inline static int32_t get_offset_of_destroyed_29() { return static_cast<int32_t>(offsetof(SDK_InputSimulator_t2184297317_StaticFields, ___destroyed_29)); }
	inline bool get_destroyed_29() const { return ___destroyed_29; }
	inline bool* get_address_of_destroyed_29() { return &___destroyed_29; }
	inline void set_destroyed_29(bool value)
	{
		___destroyed_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
