﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va371629903MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m244531082(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t521224680 *, Dictionary_2_t1818164837 *, const MethodInfo*))ValueCollection__ctor_m3515137709_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Single>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m513702096(__this, ___item0, method) ((  void (*) (ValueCollection_t521224680 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3922752079_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Single>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4003644877(__this, method) ((  void (*) (ValueCollection_t521224680 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2934356600_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Single>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3895566460(__this, ___item0, method) ((  bool (*) (ValueCollection_t521224680 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2885363365_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Single>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2820551377(__this, ___item0, method) ((  bool (*) (ValueCollection_t521224680 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2972460002_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Single>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2573001067(__this, method) ((  Il2CppObject* (*) (ValueCollection_t521224680 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1108020688_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3829865567(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t521224680 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m4129844936_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Single>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m263792148(__this, method) ((  Il2CppObject * (*) (ValueCollection_t521224680 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1607842279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Single>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1757564293(__this, method) ((  bool (*) (ValueCollection_t521224680 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m443612254_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Single>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2180079295(__this, method) ((  bool (*) (ValueCollection_t521224680 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4287515044_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Single>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1692868655(__this, method) ((  Il2CppObject * (*) (ValueCollection_t521224680 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3331633640_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Single>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m776835417(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t521224680 *, SingleU5BU5D_t577127397*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2913520516_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Single>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3354254868(__this, method) ((  Enumerator_t3504697601  (*) (ValueCollection_t521224680 *, const MethodInfo*))ValueCollection_GetEnumerator_m1902112745_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Single>::get_Count()
#define ValueCollection_get_Count_m1938087655(__this, method) ((  int32_t (*) (ValueCollection_t521224680 *, const MethodInfo*))ValueCollection_get_Count_m4265976900_gshared)(__this, method)
