﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`1<Valve.VR.VREvent_t>
struct UnityEvent_1_t3443616404;
// UnityEngine.Events.UnityAction`1<Valve.VR.VREvent_t>
struct UnityAction_1_t476884844;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_t3405266389.h"

// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.VREvent_t>::.ctor()
extern "C"  void UnityEvent_1__ctor_m2619795_gshared (UnityEvent_1_t3443616404 * __this, const MethodInfo* method);
#define UnityEvent_1__ctor_m2619795(__this, method) ((  void (*) (UnityEvent_1_t3443616404 *, const MethodInfo*))UnityEvent_1__ctor_m2619795_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.VREvent_t>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m169900092_gshared (UnityEvent_1_t3443616404 * __this, UnityAction_1_t476884844 * ___call0, const MethodInfo* method);
#define UnityEvent_1_AddListener_m169900092(__this, ___call0, method) ((  void (*) (UnityEvent_1_t3443616404 *, UnityAction_1_t476884844 *, const MethodInfo*))UnityEvent_1_AddListener_m169900092_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.VREvent_t>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m1907584807_gshared (UnityEvent_1_t3443616404 * __this, UnityAction_1_t476884844 * ___call0, const MethodInfo* method);
#define UnityEvent_1_RemoveListener_m1907584807(__this, ___call0, method) ((  void (*) (UnityEvent_1_t3443616404 *, UnityAction_1_t476884844 *, const MethodInfo*))UnityEvent_1_RemoveListener_m1907584807_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<Valve.VR.VREvent_t>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m3527399892_gshared (UnityEvent_1_t3443616404 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_1_FindMethod_Impl_m3527399892(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_1_t3443616404 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m3527399892_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<Valve.VR.VREvent_t>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m263372522_gshared (UnityEvent_1_t3443616404 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m263372522(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_1_t3443616404 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m263372522_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<Valve.VR.VREvent_t>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m3018741991_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t476884844 * ___action0, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m3018741991(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t476884844 *, const MethodInfo*))UnityEvent_1_GetDelegate_m3018741991_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.VREvent_t>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m4092148559_gshared (UnityEvent_1_t3443616404 * __this, VREvent_t_t3405266389  ___arg00, const MethodInfo* method);
#define UnityEvent_1_Invoke_m4092148559(__this, ___arg00, method) ((  void (*) (UnityEvent_1_t3443616404 *, VREvent_t_t3405266389 , const MethodInfo*))UnityEvent_1_Invoke_m4092148559_gshared)(__this, ___arg00, method)
