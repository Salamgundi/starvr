﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SteamVR_TrackedObject
struct SteamVR_TrackedObject_t2338458854;
// Valve.VR.InteractionSystem.DistanceHaptics
struct DistanceHaptics_t3909819367;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3353154814  : public Il2CppObject
{
public:
	// System.Single Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0::<distance>__0
	float ___U3CdistanceU3E__0_0;
	// SteamVR_TrackedObject Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0::<trackedObject>__1
	SteamVR_TrackedObject_t2338458854 * ___U3CtrackedObjectU3E__1_1;
	// System.Single Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0::<nextPulse>__2
	float ___U3CnextPulseU3E__2_2;
	// Valve.VR.InteractionSystem.DistanceHaptics Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0::$this
	DistanceHaptics_t3909819367 * ___U24this_3;
	// System.Object Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Valve.VR.InteractionSystem.DistanceHaptics/<Start>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CdistanceU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3353154814, ___U3CdistanceU3E__0_0)); }
	inline float get_U3CdistanceU3E__0_0() const { return ___U3CdistanceU3E__0_0; }
	inline float* get_address_of_U3CdistanceU3E__0_0() { return &___U3CdistanceU3E__0_0; }
	inline void set_U3CdistanceU3E__0_0(float value)
	{
		___U3CdistanceU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CtrackedObjectU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3353154814, ___U3CtrackedObjectU3E__1_1)); }
	inline SteamVR_TrackedObject_t2338458854 * get_U3CtrackedObjectU3E__1_1() const { return ___U3CtrackedObjectU3E__1_1; }
	inline SteamVR_TrackedObject_t2338458854 ** get_address_of_U3CtrackedObjectU3E__1_1() { return &___U3CtrackedObjectU3E__1_1; }
	inline void set_U3CtrackedObjectU3E__1_1(SteamVR_TrackedObject_t2338458854 * value)
	{
		___U3CtrackedObjectU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtrackedObjectU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CnextPulseU3E__2_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3353154814, ___U3CnextPulseU3E__2_2)); }
	inline float get_U3CnextPulseU3E__2_2() const { return ___U3CnextPulseU3E__2_2; }
	inline float* get_address_of_U3CnextPulseU3E__2_2() { return &___U3CnextPulseU3E__2_2; }
	inline void set_U3CnextPulseU3E__2_2(float value)
	{
		___U3CnextPulseU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3353154814, ___U24this_3)); }
	inline DistanceHaptics_t3909819367 * get_U24this_3() const { return ___U24this_3; }
	inline DistanceHaptics_t3909819367 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(DistanceHaptics_t3909819367 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3353154814, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3353154814, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3353154814, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
