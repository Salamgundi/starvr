﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_ComputeOverlayIntersection
struct _ComputeOverlayIntersection_t1141587151;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayIntersectionPa3201480230.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayIntersectionRe2886517940.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_ComputeOverlayIntersection::.ctor(System.Object,System.IntPtr)
extern "C"  void _ComputeOverlayIntersection__ctor_m1204846786 (_ComputeOverlayIntersection_t1141587151 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVROverlay/_ComputeOverlayIntersection::Invoke(System.UInt64,Valve.VR.VROverlayIntersectionParams_t&,Valve.VR.VROverlayIntersectionResults_t&)
extern "C"  bool _ComputeOverlayIntersection_Invoke_m1691643387 (_ComputeOverlayIntersection_t1141587151 * __this, uint64_t ___ulOverlayHandle0, VROverlayIntersectionParams_t_t3201480230 * ___pParams1, VROverlayIntersectionResults_t_t2886517940 * ___pResults2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_ComputeOverlayIntersection::BeginInvoke(System.UInt64,Valve.VR.VROverlayIntersectionParams_t&,Valve.VR.VROverlayIntersectionResults_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ComputeOverlayIntersection_BeginInvoke_m1868790552 (_ComputeOverlayIntersection_t1141587151 * __this, uint64_t ___ulOverlayHandle0, VROverlayIntersectionParams_t_t3201480230 * ___pParams1, VROverlayIntersectionResults_t_t2886517940 * ___pResults2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVROverlay/_ComputeOverlayIntersection::EndInvoke(Valve.VR.VROverlayIntersectionParams_t&,Valve.VR.VROverlayIntersectionResults_t&,System.IAsyncResult)
extern "C"  bool _ComputeOverlayIntersection_EndInvoke_m2935285268 (_ComputeOverlayIntersection_t1141587151 * __this, VROverlayIntersectionParams_t_t3201480230 * ___pParams0, VROverlayIntersectionResults_t_t2886517940 * ___pResults1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
