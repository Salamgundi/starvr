﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Controller_Menu
struct Controller_Menu_t102574584;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"

// System.Void VRTK.Examples.Controller_Menu::.ctor()
extern "C"  void Controller_Menu__ctor_m3226309939 (Controller_Menu_t102574584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Controller_Menu::Start()
extern "C"  void Controller_Menu_Start_m4088091815 (Controller_Menu_t102574584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Controller_Menu::InitMenu()
extern "C"  void Controller_Menu_InitMenu_m3267418136 (Controller_Menu_t102574584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Controller_Menu::DoMenuOn(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void Controller_Menu_DoMenuOn_m1493008890 (Controller_Menu_t102574584 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Controller_Menu::DoMenuOff(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void Controller_Menu_DoMenuOff_m443888022 (Controller_Menu_t102574584 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Controller_Menu::Update()
extern "C"  void Controller_Menu_Update_m104645152 (Controller_Menu_t102574584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
