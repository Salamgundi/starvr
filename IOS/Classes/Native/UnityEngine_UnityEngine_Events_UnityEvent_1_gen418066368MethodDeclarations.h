﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2727799310MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.InteractionSystem.Hand>::.ctor()
#define UnityEvent_1__ctor_m2333700691(__this, method) ((  void (*) (UnityEvent_1_t418066368 *, const MethodInfo*))UnityEvent_1__ctor_m2073978020_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.InteractionSystem.Hand>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_AddListener_m1632402397(__this, ___call0, method) ((  void (*) (UnityEvent_1_t418066368 *, UnityAction_1_t1746302104 *, const MethodInfo*))UnityEvent_1_AddListener_m22503421_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.InteractionSystem.Hand>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_RemoveListener_m3242177008(__this, ___call0, method) ((  void (*) (UnityEvent_1_t418066368 *, UnityAction_1_t1746302104 *, const MethodInfo*))UnityEvent_1_RemoveListener_m4278264272_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<Valve.VR.InteractionSystem.Hand>::FindMethod_Impl(System.String,System.Object)
#define UnityEvent_1_FindMethod_Impl_m1575421235(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_1_t418066368 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m2223850067_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<Valve.VR.InteractionSystem.Hand>::GetDelegate(System.Object,System.Reflection.MethodInfo)
#define UnityEvent_1_GetDelegate_m2563194439(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_1_t418066368 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m669290055_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<Valve.VR.InteractionSystem.Hand>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_GetDelegate_m1434421040(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t1746302104 *, const MethodInfo*))UnityEvent_1_GetDelegate_m3098147632_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.InteractionSystem.Hand>::Invoke(T0)
#define UnityEvent_1_Invoke_m2006130120(__this, ___arg00, method) ((  void (*) (UnityEvent_1_t418066368 *, Hand_t379716353 *, const MethodInfo*))UnityEvent_1_Invoke_m838874366_gshared)(__this, ___arg00, method)
