﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSettings/_RemoveSection
struct _RemoveSection_t1493649321;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRSettingsError4124928198.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSettings/_RemoveSection::.ctor(System.Object,System.IntPtr)
extern "C"  void _RemoveSection__ctor_m1143728008 (_RemoveSection_t1493649321 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSettings/_RemoveSection::Invoke(System.String,Valve.VR.EVRSettingsError&)
extern "C"  void _RemoveSection_Invoke_m2825581582 (_RemoveSection_t1493649321 * __this, String_t* ___pchSection0, int32_t* ___peError1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSettings/_RemoveSection::BeginInvoke(System.String,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _RemoveSection_BeginInvoke_m205494627 (_RemoveSection_t1493649321 * __this, String_t* ___pchSection0, int32_t* ___peError1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSettings/_RemoveSection::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  void _RemoveSection_EndInvoke_m723905034 (_RemoveSection_t1493649321 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
