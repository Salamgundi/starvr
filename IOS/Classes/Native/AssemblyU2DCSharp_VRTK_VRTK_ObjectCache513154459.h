﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<VRTK.VRTK_BasicTeleport>
struct List_1_t2901882469;
// System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>
struct List_1_t36734776;
// VRTK.VRTK_HeadsetCollision
struct VRTK_HeadsetCollision_t2015187094;
// VRTK.VRTK_HeadsetControllerAware
struct VRTK_HeadsetControllerAware_t1678000416;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ObjectCache
struct  VRTK_ObjectCache_t513154459  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct VRTK_ObjectCache_t513154459_StaticFields
{
public:
	// System.Collections.Generic.List`1<VRTK.VRTK_BasicTeleport> VRTK.VRTK_ObjectCache::registeredTeleporters
	List_1_t2901882469 * ___registeredTeleporters_2;
	// System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker> VRTK.VRTK_ObjectCache::registeredDestinationMarkers
	List_1_t36734776 * ___registeredDestinationMarkers_3;
	// VRTK.VRTK_HeadsetCollision VRTK.VRTK_ObjectCache::registeredHeadsetCollider
	VRTK_HeadsetCollision_t2015187094 * ___registeredHeadsetCollider_4;
	// VRTK.VRTK_HeadsetControllerAware VRTK.VRTK_ObjectCache::registeredHeadsetControllerAwareness
	VRTK_HeadsetControllerAware_t1678000416 * ___registeredHeadsetControllerAwareness_5;

public:
	inline static int32_t get_offset_of_registeredTeleporters_2() { return static_cast<int32_t>(offsetof(VRTK_ObjectCache_t513154459_StaticFields, ___registeredTeleporters_2)); }
	inline List_1_t2901882469 * get_registeredTeleporters_2() const { return ___registeredTeleporters_2; }
	inline List_1_t2901882469 ** get_address_of_registeredTeleporters_2() { return &___registeredTeleporters_2; }
	inline void set_registeredTeleporters_2(List_1_t2901882469 * value)
	{
		___registeredTeleporters_2 = value;
		Il2CppCodeGenWriteBarrier(&___registeredTeleporters_2, value);
	}

	inline static int32_t get_offset_of_registeredDestinationMarkers_3() { return static_cast<int32_t>(offsetof(VRTK_ObjectCache_t513154459_StaticFields, ___registeredDestinationMarkers_3)); }
	inline List_1_t36734776 * get_registeredDestinationMarkers_3() const { return ___registeredDestinationMarkers_3; }
	inline List_1_t36734776 ** get_address_of_registeredDestinationMarkers_3() { return &___registeredDestinationMarkers_3; }
	inline void set_registeredDestinationMarkers_3(List_1_t36734776 * value)
	{
		___registeredDestinationMarkers_3 = value;
		Il2CppCodeGenWriteBarrier(&___registeredDestinationMarkers_3, value);
	}

	inline static int32_t get_offset_of_registeredHeadsetCollider_4() { return static_cast<int32_t>(offsetof(VRTK_ObjectCache_t513154459_StaticFields, ___registeredHeadsetCollider_4)); }
	inline VRTK_HeadsetCollision_t2015187094 * get_registeredHeadsetCollider_4() const { return ___registeredHeadsetCollider_4; }
	inline VRTK_HeadsetCollision_t2015187094 ** get_address_of_registeredHeadsetCollider_4() { return &___registeredHeadsetCollider_4; }
	inline void set_registeredHeadsetCollider_4(VRTK_HeadsetCollision_t2015187094 * value)
	{
		___registeredHeadsetCollider_4 = value;
		Il2CppCodeGenWriteBarrier(&___registeredHeadsetCollider_4, value);
	}

	inline static int32_t get_offset_of_registeredHeadsetControllerAwareness_5() { return static_cast<int32_t>(offsetof(VRTK_ObjectCache_t513154459_StaticFields, ___registeredHeadsetControllerAwareness_5)); }
	inline VRTK_HeadsetControllerAware_t1678000416 * get_registeredHeadsetControllerAwareness_5() const { return ___registeredHeadsetControllerAwareness_5; }
	inline VRTK_HeadsetControllerAware_t1678000416 ** get_address_of_registeredHeadsetControllerAwareness_5() { return &___registeredHeadsetControllerAwareness_5; }
	inline void set_registeredHeadsetControllerAwareness_5(VRTK_HeadsetControllerAware_t1678000416 * value)
	{
		___registeredHeadsetControllerAwareness_5 = value;
		Il2CppCodeGenWriteBarrier(&___registeredHeadsetControllerAwareness_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
