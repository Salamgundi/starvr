﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Action
struct Action_t3226471752;
// UnityEngine.Collider
struct Collider_t3497673348;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.AI.NavMeshPath
struct NavMeshPath_t1197654427;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "System_Core_System_Action3226471752.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Decimal724701077.h"
#include "UnityEngine_UnityEngine_AI_NavMeshPath1197654427.h"

// System.Single Valve.VR.InteractionSystem.Util::RemapNumber(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  float Util_RemapNumber_m463636465 (Il2CppObject * __this /* static, unused */, float ___num0, float ___low11, float ___high12, float ___low23, float ___high24, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.InteractionSystem.Util::RemapNumberClamped(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  float Util_RemapNumberClamped_m1437443635 (Il2CppObject * __this /* static, unused */, float ___num0, float ___low11, float ___high12, float ___low23, float ___high24, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.InteractionSystem.Util::Approach(System.Single,System.Single,System.Single)
extern "C"  float Util_Approach_m2417520403 (Il2CppObject * __this /* static, unused */, float ___target0, float ___value1, float ___speed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Valve.VR.InteractionSystem.Util::BezierInterpolate3(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Util_BezierInterpolate3_m963296171 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___p00, Vector3_t2243707580  ___c01, Vector3_t2243707580  ___p12, float ___t3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Valve.VR.InteractionSystem.Util::BezierInterpolate4(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Util_BezierInterpolate4_m3526004449 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___p00, Vector3_t2243707580  ___c01, Vector3_t2243707580  ___c12, Vector3_t2243707580  ___p13, float ___t4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Valve.VR.InteractionSystem.Util::Vector3FromString(System.String)
extern "C"  Vector3_t2243707580  Util_Vector3FromString_m2887836487 (Il2CppObject * __this /* static, unused */, String_t* ___szString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Valve.VR.InteractionSystem.Util::Vector2FromString(System.String)
extern "C"  Vector2_t2243707579  Util_Vector2FromString_m2930797195 (Il2CppObject * __this /* static, unused */, String_t* ___szString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.InteractionSystem.Util::Normalize(System.Single,System.Single,System.Single)
extern "C"  float Util_Normalize_m4066675932 (Il2CppObject * __this /* static, unused */, float ___value0, float ___min1, float ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Valve.VR.InteractionSystem.Util::Vector2AsVector3(UnityEngine.Vector2)
extern "C"  Vector3_t2243707580  Util_Vector2AsVector3_m613173635 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Valve.VR.InteractionSystem.Util::Vector3AsVector2(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  Util_Vector3AsVector2_m1296865951 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.InteractionSystem.Util::AngleOf(UnityEngine.Vector2)
extern "C"  float Util_AngleOf_m2331229422 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.InteractionSystem.Util::YawOf(UnityEngine.Vector3)
extern "C"  float Util_YawOf_m2755390251 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Valve.VR.InteractionSystem.Util::RandomWithLookback(System.Int32,System.Int32,System.Collections.Generic.List`1<System.Int32>,System.Int32)
extern "C"  int32_t Util_RandomWithLookback_m354184235 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, List_1_t1440998580 * ___history2, int32_t ___historyCount3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Valve.VR.InteractionSystem.Util::FindChild(UnityEngine.Transform,System.String)
extern "C"  Transform_t3275118058 * Util_FindChild_m3228025980 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___parent0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Util::SwitchLayerRecursively(UnityEngine.Transform,System.Int32,System.Int32)
extern "C"  void Util_SwitchLayerRecursively_m2738977455 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___transform0, int32_t ___fromLayer1, int32_t ___toLayer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Util::DrawCross(UnityEngine.Vector3,UnityEngine.Color,System.Single)
extern "C"  void Util_DrawCross_m1688035040 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Color_t2020392075  ___crossColor1, float ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Util::ResetTransform(UnityEngine.Transform,System.Boolean)
extern "C"  void Util_ResetTransform_m3259657643 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___t0, bool ___resetScale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Valve.VR.InteractionSystem.Util::ClosestPointOnLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Util_ClosestPointOnLine_m1164264911 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___vA0, Vector3_t2243707580  ___vB1, Vector3_t2243707580  ___vPoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Util::AfterTimer(UnityEngine.GameObject,System.Single,System.Action,System.Boolean)
extern "C"  void Util_AfterTimer_m3783952826 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, float ____time1, Action_t3226471752 * ___callback2, bool ___trigger_if_destroyed_early3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Util::SendPhysicsMessage(UnityEngine.Collider,System.String,UnityEngine.SendMessageOptions)
extern "C"  void Util_SendPhysicsMessage_m2010460585 (Il2CppObject * __this /* static, unused */, Collider_t3497673348 * ___collider0, String_t* ___message1, int32_t ___sendMessageOptions2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Util::SendPhysicsMessage(UnityEngine.Collider,System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Util_SendPhysicsMessage_m3174745243 (Il2CppObject * __this /* static, unused */, Collider_t3497673348 * ___collider0, String_t* ___message1, Il2CppObject * ___arg2, int32_t ___sendMessageOptions3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Util::IgnoreCollisions(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  void Util_IgnoreCollisions_m173461051 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___goA0, GameObject_t1756533147 * ___goB1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.Util::WrapCoroutine(System.Collections.IEnumerator,System.Action)
extern "C"  Il2CppObject * Util_WrapCoroutine_m1466674482 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___coroutine0, Action_t3226471752 * ___onCoroutineFinished1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Valve.VR.InteractionSystem.Util::ColorWithAlpha(UnityEngine.Color,System.Single)
extern "C"  Color_t2020392075  Util_ColorWithAlpha_m1769690563 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, float ___alpha1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Util::Quit()
extern "C"  void Util_Quit_m1066334463 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal Valve.VR.InteractionSystem.Util::FloatToDecimal(System.Single,System.Int32)
extern "C"  Decimal_t724701077  Util_FloatToDecimal_m595791229 (Il2CppObject * __this /* static, unused */, float ___value0, int32_t ___decimalPlaces1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.InteractionSystem.Util::FixupNewlines(System.String)
extern "C"  String_t* Util_FixupNewlines_m2150689822 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.InteractionSystem.Util::PathLength(UnityEngine.AI.NavMeshPath)
extern "C"  float Util_PathLength_m3510788723 (Il2CppObject * __this /* static, unused */, NavMeshPath_t1197654427 * ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Util::HasCommandLineArgument(System.String)
extern "C"  bool Util_HasCommandLineArgument_m367439780 (Il2CppObject * __this /* static, unused */, String_t* ___argumentName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Valve.VR.InteractionSystem.Util::GetCommandLineArgValue(System.String,System.Int32)
extern "C"  int32_t Util_GetCommandLineArgValue_m3970367707 (Il2CppObject * __this /* static, unused */, String_t* ___argumentName0, int32_t ___nDefaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.InteractionSystem.Util::GetCommandLineArgValue(System.String,System.Single)
extern "C"  float Util_GetCommandLineArgValue_m3566018003 (Il2CppObject * __this /* static, unused */, String_t* ___argumentName0, float ___flDefaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Util::SetActive(UnityEngine.GameObject,System.Boolean)
extern "C"  void Util_SetActive_m317108123 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___gameObject0, bool ___active1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.InteractionSystem.Util::CombinePaths(System.String[])
extern "C"  String_t* Util_CombinePaths_m2531197134 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___paths0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
