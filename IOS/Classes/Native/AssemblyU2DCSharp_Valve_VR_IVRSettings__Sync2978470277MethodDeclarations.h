﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSettings/_Sync
struct _Sync_t2978470277;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRSettingsError4124928198.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSettings/_Sync::.ctor(System.Object,System.IntPtr)
extern "C"  void _Sync__ctor_m3527021068 (_Sync_t2978470277 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSettings/_Sync::Invoke(System.Boolean,Valve.VR.EVRSettingsError&)
extern "C"  bool _Sync_Invoke_m4190606357 (_Sync_t2978470277 * __this, bool ___bForce0, int32_t* ___peError1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSettings/_Sync::BeginInvoke(System.Boolean,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _Sync_BeginInvoke_m1842647256 (_Sync_t2978470277 * __this, bool ___bForce0, int32_t* ___peError1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSettings/_Sync::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  bool _Sync_EndInvoke_m2579371820 (_Sync_t2978470277 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
