﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_AdaptiveQuality/Timing
struct Timing_t3878883323;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_AdaptiveQuality/Timing::.ctor()
extern "C"  void Timing__ctor_m3512227182 (Timing_t3878883323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality/Timing::SaveCurrentFrameTiming()
extern "C"  void Timing_SaveCurrentFrameTiming_m2538047811 (Timing_t3878883323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_AdaptiveQuality/Timing::GetFrameTiming(System.Int32)
extern "C"  float Timing_GetFrameTiming_m426042620 (Timing_t3878883323 * __this, int32_t ___framesAgo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_AdaptiveQuality/Timing::WasFrameTimingBad(System.Int32,System.Single,System.Int32,System.Int32)
extern "C"  bool Timing_WasFrameTimingBad_m283239053 (Timing_t3878883323 * __this, int32_t ___framesAgo0, float ___thresholdInMilliseconds1, int32_t ___lastChangeFrameCount2, int32_t ___changeFrameCost3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_AdaptiveQuality/Timing::WasFrameTimingGood(System.Int32,System.Single,System.Int32,System.Int32)
extern "C"  bool Timing_WasFrameTimingGood_m358427969 (Timing_t3878883323 * __this, int32_t ___framesAgo0, float ___thresholdInMilliseconds1, int32_t ___lastChangeFrameCount2, int32_t ___changeFrameCost3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_AdaptiveQuality/Timing::WillFrameTimingBeBad(System.Single,System.Single,System.Int32,System.Int32)
extern "C"  bool Timing_WillFrameTimingBeBad_m2502402369 (Timing_t3878883323 * __this, float ___extrapolationThresholdInMilliseconds0, float ___thresholdInMilliseconds1, int32_t ___lastChangeFrameCount2, int32_t ___changeFrameCost3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_AdaptiveQuality/Timing::AreFramesAvailable(System.Int32,System.Int32,System.Int32)
extern "C"  bool Timing_AreFramesAvailable_m2244984482 (Il2CppObject * __this /* static, unused */, int32_t ___framesAgo0, int32_t ___lastChangeFrameCount1, int32_t ___changeFrameCost2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
