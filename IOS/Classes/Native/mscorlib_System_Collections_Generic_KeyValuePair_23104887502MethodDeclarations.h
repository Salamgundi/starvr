﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23104887502.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SDKManager_SupportedSD1339136682.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3340313999_gshared (KeyValuePair_2_t3104887502 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3340313999(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3104887502 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3340313999_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1301319541_gshared (KeyValuePair_2_t3104887502 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1301319541(__this, method) ((  int32_t (*) (KeyValuePair_2_t3104887502 *, const MethodInfo*))KeyValuePair_2_get_Key_m1301319541_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m264665148_gshared (KeyValuePair_2_t3104887502 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m264665148(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3104887502 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m264665148_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1120936565_gshared (KeyValuePair_2_t3104887502 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1120936565(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3104887502 *, const MethodInfo*))KeyValuePair_2_get_Value_m1120936565_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3158972364_gshared (KeyValuePair_2_t3104887502 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3158972364(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3104887502 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m3158972364_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1271884226_gshared (KeyValuePair_2_t3104887502 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1271884226(__this, method) ((  String_t* (*) (KeyValuePair_2_t3104887502 *, const MethodInfo*))KeyValuePair_2_ToString_m1271884226_gshared)(__this, method)
