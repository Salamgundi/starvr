﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2397995702MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m368329930(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t994267917 *, Dictionary_2_t2291208074 *, const MethodInfo*))ValueCollection__ctor_m2564002873_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1993117248(__this, ___item0, method) ((  void (*) (ValueCollection_t994267917 *, Event_1_t1285721510 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m817171771_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3193665407(__this, method) ((  void (*) (ValueCollection_t994267917 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2138940134_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m955021052(__this, ___item0, method) ((  bool (*) (ValueCollection_t994267917 *, Event_1_t1285721510 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m132305249_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2183251431(__this, ___item0, method) ((  bool (*) (ValueCollection_t994267917 *, Event_1_t1285721510 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2885333860_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m545752413(__this, method) ((  Il2CppObject* (*) (ValueCollection_t994267917 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m167065842_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3023276253(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t994267917 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m4026706282_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2136733580(__this, method) ((  Il2CppObject * (*) (ValueCollection_t994267917 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4273237419_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2417203259(__this, method) ((  bool (*) (ValueCollection_t994267917 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2030574432_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m937980297(__this, method) ((  bool (*) (ValueCollection_t994267917 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2420584342_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2866148609(__this, method) ((  Il2CppObject * (*) (ValueCollection_t994267917 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2275224122_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3224003959(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t994267917 *, Event_1U5BU5D_t3905829123*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m793632866_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1030342258(__this, method) ((  Enumerator_t3977740838  (*) (ValueCollection_t994267917 *, const MethodInfo*))ValueCollection_GetEnumerator_m1801917245_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::get_Count()
#define ValueCollection_get_Count_m1848441029(__this, method) ((  int32_t (*) (ValueCollection_t994267917 *, const MethodInfo*))ValueCollection_get_Count_m1258902038_gshared)(__this, method)
