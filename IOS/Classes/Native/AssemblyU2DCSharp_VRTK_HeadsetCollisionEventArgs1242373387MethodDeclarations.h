﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.HeadsetCollisionEventArgs
struct HeadsetCollisionEventArgs_t1242373387;
struct HeadsetCollisionEventArgs_t1242373387_marshaled_pinvoke;
struct HeadsetCollisionEventArgs_t1242373387_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct HeadsetCollisionEventArgs_t1242373387;
struct HeadsetCollisionEventArgs_t1242373387_marshaled_pinvoke;

extern "C" void HeadsetCollisionEventArgs_t1242373387_marshal_pinvoke(const HeadsetCollisionEventArgs_t1242373387& unmarshaled, HeadsetCollisionEventArgs_t1242373387_marshaled_pinvoke& marshaled);
extern "C" void HeadsetCollisionEventArgs_t1242373387_marshal_pinvoke_back(const HeadsetCollisionEventArgs_t1242373387_marshaled_pinvoke& marshaled, HeadsetCollisionEventArgs_t1242373387& unmarshaled);
extern "C" void HeadsetCollisionEventArgs_t1242373387_marshal_pinvoke_cleanup(HeadsetCollisionEventArgs_t1242373387_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct HeadsetCollisionEventArgs_t1242373387;
struct HeadsetCollisionEventArgs_t1242373387_marshaled_com;

extern "C" void HeadsetCollisionEventArgs_t1242373387_marshal_com(const HeadsetCollisionEventArgs_t1242373387& unmarshaled, HeadsetCollisionEventArgs_t1242373387_marshaled_com& marshaled);
extern "C" void HeadsetCollisionEventArgs_t1242373387_marshal_com_back(const HeadsetCollisionEventArgs_t1242373387_marshaled_com& marshaled, HeadsetCollisionEventArgs_t1242373387& unmarshaled);
extern "C" void HeadsetCollisionEventArgs_t1242373387_marshal_com_cleanup(HeadsetCollisionEventArgs_t1242373387_marshaled_com& marshaled);
