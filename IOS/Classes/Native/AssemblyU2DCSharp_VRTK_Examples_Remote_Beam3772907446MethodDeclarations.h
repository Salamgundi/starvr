﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Remote_Beam
struct Remote_Beam_t3772907446;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void VRTK.Examples.Remote_Beam::.ctor()
extern "C"  void Remote_Beam__ctor_m3071327903 (Remote_Beam_t3772907446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Remote_Beam::SetTouchAxis(UnityEngine.Vector2)
extern "C"  void Remote_Beam_SetTouchAxis_m2441049339 (Remote_Beam_t3772907446 * __this, Vector2_t2243707579  ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Remote_Beam::FixedUpdate()
extern "C"  void Remote_Beam_FixedUpdate_m4252145444 (Remote_Beam_t3772907446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
