﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureGL
struct _GetVideoStreamTextureGL_t1897576655;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraError3529690400.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraFrameTy2003723073.h"
#include "AssemblyU2DCSharp_Valve_VR_CameraVideoStreamFrameHe968213647.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureGL::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetVideoStreamTextureGL__ctor_m2926114760 (_GetVideoStreamTextureGL_t1897576655 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureGL::Invoke(System.UInt64,Valve.VR.EVRTrackedCameraFrameType,System.UInt32&,Valve.VR.CameraVideoStreamFrameHeader_t&,System.UInt32)
extern "C"  int32_t _GetVideoStreamTextureGL_Invoke_m3911710892 (_GetVideoStreamTextureGL_t1897576655 * __this, uint64_t ___hTrackedCamera0, int32_t ___eFrameType1, uint32_t* ___pglTextureId2, CameraVideoStreamFrameHeader_t_t968213647 * ___pFrameHeader3, uint32_t ___nFrameHeaderSize4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureGL::BeginInvoke(System.UInt64,Valve.VR.EVRTrackedCameraFrameType,System.UInt32&,Valve.VR.CameraVideoStreamFrameHeader_t&,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetVideoStreamTextureGL_BeginInvoke_m1271953430 (_GetVideoStreamTextureGL_t1897576655 * __this, uint64_t ___hTrackedCamera0, int32_t ___eFrameType1, uint32_t* ___pglTextureId2, CameraVideoStreamFrameHeader_t_t968213647 * ___pFrameHeader3, uint32_t ___nFrameHeaderSize4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureGL::EndInvoke(System.UInt32&,Valve.VR.CameraVideoStreamFrameHeader_t&,System.IAsyncResult)
extern "C"  int32_t _GetVideoStreamTextureGL_EndInvoke_m3318453002 (_GetVideoStreamTextureGL_t1897576655 * __this, uint32_t* ___pglTextureId0, CameraVideoStreamFrameHeader_t_t968213647 * ___pFrameHeader1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
