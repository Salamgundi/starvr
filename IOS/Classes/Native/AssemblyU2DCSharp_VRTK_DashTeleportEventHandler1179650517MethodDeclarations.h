﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.DashTeleportEventHandler
struct DashTeleportEventHandler_t1179650517;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_DashTeleportEventArgs2197253242.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.DashTeleportEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void DashTeleportEventHandler__ctor_m4160651057 (DashTeleportEventHandler_t1179650517 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.DashTeleportEventHandler::Invoke(System.Object,VRTK.DashTeleportEventArgs)
extern "C"  void DashTeleportEventHandler_Invoke_m27167478 (DashTeleportEventHandler_t1179650517 * __this, Il2CppObject * ___sender0, DashTeleportEventArgs_t2197253242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.DashTeleportEventHandler::BeginInvoke(System.Object,VRTK.DashTeleportEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DashTeleportEventHandler_BeginInvoke_m1992568945 (DashTeleportEventHandler_t1179650517 * __this, Il2CppObject * ___sender0, DashTeleportEventArgs_t2197253242  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.DashTeleportEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void DashTeleportEventHandler_EndInvoke_m2521631023 (DashTeleportEventHandler_t1179650517 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
