﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.InteractableObjectEventArgs
struct  InteractableObjectEventArgs_t473175556 
{
public:
	// UnityEngine.GameObject VRTK.InteractableObjectEventArgs::interactingObject
	GameObject_t1756533147 * ___interactingObject_0;

public:
	inline static int32_t get_offset_of_interactingObject_0() { return static_cast<int32_t>(offsetof(InteractableObjectEventArgs_t473175556, ___interactingObject_0)); }
	inline GameObject_t1756533147 * get_interactingObject_0() const { return ___interactingObject_0; }
	inline GameObject_t1756533147 ** get_address_of_interactingObject_0() { return &___interactingObject_0; }
	inline void set_interactingObject_0(GameObject_t1756533147 * value)
	{
		___interactingObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___interactingObject_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VRTK.InteractableObjectEventArgs
struct InteractableObjectEventArgs_t473175556_marshaled_pinvoke
{
	GameObject_t1756533147 * ___interactingObject_0;
};
// Native definition for COM marshalling of VRTK.InteractableObjectEventArgs
struct InteractableObjectEventArgs_t473175556_marshaled_com
{
	GameObject_t1756533147 * ___interactingObject_0;
};
