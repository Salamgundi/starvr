﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_AdaptiveQuality
struct VRTK_AdaptiveQuality_t2723434039;
// System.String
struct String_t;
// UnityEngine.Camera
struct Camera_t189460977;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"

// System.Void VRTK.VRTK_AdaptiveQuality::.ctor()
extern "C"  void VRTK_AdaptiveQuality__ctor_m954231029 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_AdaptiveQuality::get_CurrentRenderScale()
extern "C"  float VRTK_AdaptiveQuality_get_CurrentRenderScale_m2387354237 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 VRTK.VRTK_AdaptiveQuality::get_defaultRenderTargetResolution()
extern "C"  Vector2_t2243707579  VRTK_AdaptiveQuality_get_defaultRenderTargetResolution_m4195761073 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 VRTK.VRTK_AdaptiveQuality::get_currentRenderTargetResolution()
extern "C"  Vector2_t2243707579  VRTK_AdaptiveQuality_get_currentRenderTargetResolution_m143056159 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 VRTK.VRTK_AdaptiveQuality::RenderTargetResolutionForRenderScale(System.Single)
extern "C"  Vector2_t2243707579  VRTK_AdaptiveQuality_RenderTargetResolutionForRenderScale_m1468761491 (Il2CppObject * __this /* static, unused */, float ___renderScale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_AdaptiveQuality::BiggestAllowedMaximumRenderScale()
extern "C"  float VRTK_AdaptiveQuality_BiggestAllowedMaximumRenderScale_m129504990 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VRTK.VRTK_AdaptiveQuality::ToString()
extern "C"  String_t* VRTK_AdaptiveQuality_ToString_m1650238808 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality::OnEnable()
extern "C"  void VRTK_AdaptiveQuality_OnEnable_m3639978853 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality::OnDisable()
extern "C"  void VRTK_AdaptiveQuality_OnDisable_m2932601348 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality::OnValidate()
extern "C"  void VRTK_AdaptiveQuality_OnValidate_m3127534400 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality::Update()
extern "C"  void VRTK_AdaptiveQuality_Update_m120129130 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality::LateUpdate()
extern "C"  void VRTK_AdaptiveQuality_LateUpdate_m928676486 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality::OnCameraPreCull(UnityEngine.Camera)
extern "C"  void VRTK_AdaptiveQuality_OnCameraPreCull_m3997100136 (VRTK_AdaptiveQuality_t2723434039 * __this, Camera_t189460977 * ___camera0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality::HandleCommandLineArguments()
extern "C"  void VRTK_AdaptiveQuality_HandleCommandLineArguments_m1286548548 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality::HandleKeyPresses()
extern "C"  void VRTK_AdaptiveQuality_HandleKeyPresses_m1487528001 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality::UpdateMSAALevel()
extern "C"  void VRTK_AdaptiveQuality_UpdateMSAALevel_m3732941624 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality::UpdateRenderScaleLevels()
extern "C"  void VRTK_AdaptiveQuality_UpdateRenderScaleLevels_m961346551 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality::UpdateRenderScale()
extern "C"  void VRTK_AdaptiveQuality_UpdateRenderScale_m875666302 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality::SetRenderScale(System.Single,System.Single)
extern "C"  void VRTK_AdaptiveQuality_SetRenderScale_m282541437 (Il2CppObject * __this /* static, unused */, float ___renderScale0, float ___renderViewportScale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VRTK.VRTK_AdaptiveQuality::ClampRenderScaleLevel(System.Int32)
extern "C"  int32_t VRTK_AdaptiveQuality_ClampRenderScaleLevel_m416802313 (VRTK_AdaptiveQuality_t2723434039 * __this, int32_t ___renderScaleLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality::CreateOrDestroyDebugVisualization()
extern "C"  void VRTK_AdaptiveQuality_CreateOrDestroyDebugVisualization_m3447022069 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_AdaptiveQuality::UpdateDebugVisualization()
extern "C"  void VRTK_AdaptiveQuality_UpdateDebugVisualization_m3032460499 (VRTK_AdaptiveQuality_t2723434039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_AdaptiveQuality::<UpdateRenderScaleLevels>m__0(System.Single)
extern "C"  bool VRTK_AdaptiveQuality_U3CUpdateRenderScaleLevelsU3Em__0_m2621177617 (Il2CppObject * __this /* static, unused */, float ___renderScale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
