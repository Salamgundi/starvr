﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TiltWindow
struct TiltWindow_t1839185375;

#include "codegen/il2cpp-codegen.h"

// System.Void TiltWindow::.ctor()
extern "C"  void TiltWindow__ctor_m3071582230 (TiltWindow_t1839185375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TiltWindow::Start()
extern "C"  void TiltWindow_Start_m3112429194 (TiltWindow_t1839185375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TiltWindow::Update()
extern "C"  void TiltWindow_Update_m460467911 (TiltWindow_t1839185375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
