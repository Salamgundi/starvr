﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Controller/ButtonMask
struct ButtonMask_t2972238878;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Controller/ButtonMask::.ctor()
extern "C"  void ButtonMask__ctor_m4241999575 (ButtonMask_t2972238878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
