﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_SphericalProjection
struct SteamVR_SphericalProjection_t2273435385;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void SteamVR_SphericalProjection::.ctor()
extern "C"  void SteamVR_SphericalProjection__ctor_m1794318020 (SteamVR_SphericalProjection_t2273435385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_SphericalProjection::Set(UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  void SteamVR_SphericalProjection_Set_m1120678311 (SteamVR_SphericalProjection_t2273435385 * __this, Vector3_t2243707580  ___N0, float ___phi01, float ___phi12, float ___theta03, float ___theta14, Vector3_t2243707580  ___uAxis5, Vector3_t2243707580  ___uOrigin6, float ___uScale7, Vector3_t2243707580  ___vAxis8, Vector3_t2243707580  ___vOrigin9, float ___vScale10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_SphericalProjection::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void SteamVR_SphericalProjection_OnRenderImage_m980496348 (SteamVR_SphericalProjection_t2273435385 * __this, RenderTexture_t2666733923 * ___src0, RenderTexture_t2666733923 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
