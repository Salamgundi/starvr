﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollDetailTexture
struct ScrollDetailTexture_t275486717;

#include "codegen/il2cpp-codegen.h"

// System.Void ScrollDetailTexture::.ctor()
extern "C"  void ScrollDetailTexture__ctor_m3530921942 (ScrollDetailTexture_t275486717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollDetailTexture::OnEnable()
extern "C"  void ScrollDetailTexture_OnEnable_m3107209742 (ScrollDetailTexture_t275486717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollDetailTexture::OnDisable()
extern "C"  void ScrollDetailTexture_OnDisable_m33699557 (ScrollDetailTexture_t275486717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollDetailTexture::Update()
extern "C"  void ScrollDetailTexture_Update_m316319017 (ScrollDetailTexture_t275486717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
