﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ConfigurableJoint
struct ConfigurableJoint_t454307495;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_ConfigurableJointMotion1185819153.h"
#include "UnityEngine_UnityEngine_SoftJointLimit3286660309.h"
#include "UnityEngine_UnityEngine_JointDrive1422794032.h"

// UnityEngine.Vector3 UnityEngine.ConfigurableJoint::get_secondaryAxis()
extern "C"  Vector3_t2243707580  ConfigurableJoint_get_secondaryAxis_m438299350 (ConfigurableJoint_t454307495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_secondaryAxis(UnityEngine.Vector3&)
extern "C"  void ConfigurableJoint_INTERNAL_get_secondaryAxis_m1743458491 (ConfigurableJoint_t454307495 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_xMotion(UnityEngine.ConfigurableJointMotion)
extern "C"  void ConfigurableJoint_set_xMotion_m3237366215 (ConfigurableJoint_t454307495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_yMotion(UnityEngine.ConfigurableJointMotion)
extern "C"  void ConfigurableJoint_set_yMotion_m4075131532 (ConfigurableJoint_t454307495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_zMotion(UnityEngine.ConfigurableJointMotion)
extern "C"  void ConfigurableJoint_set_zMotion_m3709400637 (ConfigurableJoint_t454307495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_angularXMotion(UnityEngine.ConfigurableJointMotion)
extern "C"  void ConfigurableJoint_set_angularXMotion_m1092292817 (ConfigurableJoint_t454307495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_angularYMotion(UnityEngine.ConfigurableJointMotion)
extern "C"  void ConfigurableJoint_set_angularYMotion_m4203819186 (ConfigurableJoint_t454307495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_angularZMotion(UnityEngine.ConfigurableJointMotion)
extern "C"  void ConfigurableJoint_set_angularZMotion_m52260755 (ConfigurableJoint_t454307495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimit UnityEngine.ConfigurableJoint::get_linearLimit()
extern "C"  SoftJointLimit_t3286660309  ConfigurableJoint_get_linearLimit_m551314346 (ConfigurableJoint_t454307495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_linearLimit(UnityEngine.SoftJointLimit)
extern "C"  void ConfigurableJoint_set_linearLimit_m3193623083 (ConfigurableJoint_t454307495 * __this, SoftJointLimit_t3286660309  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_linearLimit(UnityEngine.SoftJointLimit&)
extern "C"  void ConfigurableJoint_INTERNAL_get_linearLimit_m2623413637 (ConfigurableJoint_t454307495 * __this, SoftJointLimit_t3286660309 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_linearLimit(UnityEngine.SoftJointLimit&)
extern "C"  void ConfigurableJoint_INTERNAL_set_linearLimit_m2095997433 (ConfigurableJoint_t454307495 * __this, SoftJointLimit_t3286660309 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_targetPosition(UnityEngine.Vector3)
extern "C"  void ConfigurableJoint_set_targetPosition_m1134424870 (ConfigurableJoint_t454307495 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_targetPosition(UnityEngine.Vector3&)
extern "C"  void ConfigurableJoint_INTERNAL_set_targetPosition_m3595655658 (ConfigurableJoint_t454307495 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_targetVelocity(UnityEngine.Vector3)
extern "C"  void ConfigurableJoint_set_targetVelocity_m3588510434 (ConfigurableJoint_t454307495 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_targetVelocity(UnityEngine.Vector3&)
extern "C"  void ConfigurableJoint_INTERNAL_set_targetVelocity_m1908257962 (ConfigurableJoint_t454307495 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_xDrive(UnityEngine.JointDrive)
extern "C"  void ConfigurableJoint_set_xDrive_m3121621322 (ConfigurableJoint_t454307495 * __this, JointDrive_t1422794032  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_xDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_set_xDrive_m2627694390 (ConfigurableJoint_t454307495 * __this, JointDrive_t1422794032 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_yDrive(UnityEngine.JointDrive)
extern "C"  void ConfigurableJoint_set_yDrive_m974083301 (ConfigurableJoint_t454307495 * __this, JointDrive_t1422794032  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_yDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_set_yDrive_m886684887 (ConfigurableJoint_t454307495 * __this, JointDrive_t1422794032 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_zDrive(UnityEngine.JointDrive)
extern "C"  void ConfigurableJoint_set_zDrive_m4037895552 (ConfigurableJoint_t454307495 * __this, JointDrive_t1422794032  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_zDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_set_zDrive_m3997083380 (ConfigurableJoint_t454307495 * __this, JointDrive_t1422794032 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_configuredInWorldSpace(System.Boolean)
extern "C"  void ConfigurableJoint_set_configuredInWorldSpace_m501518735 (ConfigurableJoint_t454307495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
