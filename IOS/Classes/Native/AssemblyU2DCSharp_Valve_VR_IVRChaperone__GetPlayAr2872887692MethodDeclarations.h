﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperone/_GetPlayAreaSize
struct _GetPlayAreaSize_t2872887692;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperone/_GetPlayAreaSize::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetPlayAreaSize__ctor_m1520669215 (_GetPlayAreaSize_t2872887692 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperone/_GetPlayAreaSize::Invoke(System.Single&,System.Single&)
extern "C"  bool _GetPlayAreaSize_Invoke_m1246174215 (_GetPlayAreaSize_t2872887692 * __this, float* ___pSizeX0, float* ___pSizeZ1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperone/_GetPlayAreaSize::BeginInvoke(System.Single&,System.Single&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetPlayAreaSize_BeginInvoke_m3481769140 (_GetPlayAreaSize_t2872887692 * __this, float* ___pSizeX0, float* ___pSizeZ1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperone/_GetPlayAreaSize::EndInvoke(System.Single&,System.Single&,System.IAsyncResult)
extern "C"  bool _GetPlayAreaSize_EndInvoke_m3776019705 (_GetPlayAreaSize_t2872887692 * __this, float* ___pSizeX0, float* ___pSizeZ1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
