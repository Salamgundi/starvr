﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_FallbackBoundaries
struct SDK_FallbackBoundaries_t1566319879;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.SDK_FallbackBoundaries::.ctor()
extern "C"  void SDK_FallbackBoundaries__ctor_m3838208163 (SDK_FallbackBoundaries_t1566319879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_FallbackBoundaries::InitBoundaries()
extern "C"  void SDK_FallbackBoundaries_InitBoundaries_m281152479 (SDK_FallbackBoundaries_t1566319879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.SDK_FallbackBoundaries::GetPlayArea()
extern "C"  Transform_t3275118058 * SDK_FallbackBoundaries_GetPlayArea_m1117764606 (SDK_FallbackBoundaries_t1566319879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] VRTK.SDK_FallbackBoundaries::GetPlayAreaVertices(UnityEngine.GameObject)
extern "C"  Vector3U5BU5D_t1172311765* SDK_FallbackBoundaries_GetPlayAreaVertices_m3246200563 (SDK_FallbackBoundaries_t1566319879 * __this, GameObject_t1756533147 * ___playArea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.SDK_FallbackBoundaries::GetPlayAreaBorderThickness(UnityEngine.GameObject)
extern "C"  float SDK_FallbackBoundaries_GetPlayAreaBorderThickness_m1399232330 (SDK_FallbackBoundaries_t1566319879 * __this, GameObject_t1756533147 * ___playArea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackBoundaries::IsPlayAreaSizeCalibrated(UnityEngine.GameObject)
extern "C"  bool SDK_FallbackBoundaries_IsPlayAreaSizeCalibrated_m414568590 (SDK_FallbackBoundaries_t1566319879 * __this, GameObject_t1756533147 * ___playArea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_FallbackBoundaries::Awake()
extern "C"  void SDK_FallbackBoundaries_Awake_m1526430732 (SDK_FallbackBoundaries_t1566319879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
