﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.RenderModel_TextureMap_t_Packed
struct RenderModel_TextureMap_t_Packed_t1702369923;
struct RenderModel_TextureMap_t_Packed_t1702369923_marshaled_pinvoke;
struct RenderModel_TextureMap_t_Packed_t1702369923_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_TextureMap_1702369923.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_TextureMap_1828165156.h"

// System.Void Valve.VR.RenderModel_TextureMap_t_Packed::Unpack(Valve.VR.RenderModel_TextureMap_t&)
extern "C"  void RenderModel_TextureMap_t_Packed_Unpack_m1174237282 (RenderModel_TextureMap_t_Packed_t1702369923 * __this, RenderModel_TextureMap_t_t1828165156 * ___unpacked0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct RenderModel_TextureMap_t_Packed_t1702369923;
struct RenderModel_TextureMap_t_Packed_t1702369923_marshaled_pinvoke;

extern "C" void RenderModel_TextureMap_t_Packed_t1702369923_marshal_pinvoke(const RenderModel_TextureMap_t_Packed_t1702369923& unmarshaled, RenderModel_TextureMap_t_Packed_t1702369923_marshaled_pinvoke& marshaled);
extern "C" void RenderModel_TextureMap_t_Packed_t1702369923_marshal_pinvoke_back(const RenderModel_TextureMap_t_Packed_t1702369923_marshaled_pinvoke& marshaled, RenderModel_TextureMap_t_Packed_t1702369923& unmarshaled);
extern "C" void RenderModel_TextureMap_t_Packed_t1702369923_marshal_pinvoke_cleanup(RenderModel_TextureMap_t_Packed_t1702369923_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct RenderModel_TextureMap_t_Packed_t1702369923;
struct RenderModel_TextureMap_t_Packed_t1702369923_marshaled_com;

extern "C" void RenderModel_TextureMap_t_Packed_t1702369923_marshal_com(const RenderModel_TextureMap_t_Packed_t1702369923& unmarshaled, RenderModel_TextureMap_t_Packed_t1702369923_marshaled_com& marshaled);
extern "C" void RenderModel_TextureMap_t_Packed_t1702369923_marshal_com_back(const RenderModel_TextureMap_t_Packed_t1702369923_marshaled_com& marshaled, RenderModel_TextureMap_t_Packed_t1702369923& unmarshaled);
extern "C" void RenderModel_TextureMap_t_Packed_t1702369923_marshal_com_cleanup(RenderModel_TextureMap_t_Packed_t1702369923_marshaled_com& marshaled);
