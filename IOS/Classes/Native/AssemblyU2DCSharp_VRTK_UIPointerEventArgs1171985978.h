﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UIPointerEventArgs
struct  UIPointerEventArgs_t1171985978 
{
public:
	// System.UInt32 VRTK.UIPointerEventArgs::controllerIndex
	uint32_t ___controllerIndex_0;
	// System.Boolean VRTK.UIPointerEventArgs::isActive
	bool ___isActive_1;
	// UnityEngine.GameObject VRTK.UIPointerEventArgs::currentTarget
	GameObject_t1756533147 * ___currentTarget_2;
	// UnityEngine.GameObject VRTK.UIPointerEventArgs::previousTarget
	GameObject_t1756533147 * ___previousTarget_3;

public:
	inline static int32_t get_offset_of_controllerIndex_0() { return static_cast<int32_t>(offsetof(UIPointerEventArgs_t1171985978, ___controllerIndex_0)); }
	inline uint32_t get_controllerIndex_0() const { return ___controllerIndex_0; }
	inline uint32_t* get_address_of_controllerIndex_0() { return &___controllerIndex_0; }
	inline void set_controllerIndex_0(uint32_t value)
	{
		___controllerIndex_0 = value;
	}

	inline static int32_t get_offset_of_isActive_1() { return static_cast<int32_t>(offsetof(UIPointerEventArgs_t1171985978, ___isActive_1)); }
	inline bool get_isActive_1() const { return ___isActive_1; }
	inline bool* get_address_of_isActive_1() { return &___isActive_1; }
	inline void set_isActive_1(bool value)
	{
		___isActive_1 = value;
	}

	inline static int32_t get_offset_of_currentTarget_2() { return static_cast<int32_t>(offsetof(UIPointerEventArgs_t1171985978, ___currentTarget_2)); }
	inline GameObject_t1756533147 * get_currentTarget_2() const { return ___currentTarget_2; }
	inline GameObject_t1756533147 ** get_address_of_currentTarget_2() { return &___currentTarget_2; }
	inline void set_currentTarget_2(GameObject_t1756533147 * value)
	{
		___currentTarget_2 = value;
		Il2CppCodeGenWriteBarrier(&___currentTarget_2, value);
	}

	inline static int32_t get_offset_of_previousTarget_3() { return static_cast<int32_t>(offsetof(UIPointerEventArgs_t1171985978, ___previousTarget_3)); }
	inline GameObject_t1756533147 * get_previousTarget_3() const { return ___previousTarget_3; }
	inline GameObject_t1756533147 ** get_address_of_previousTarget_3() { return &___previousTarget_3; }
	inline void set_previousTarget_3(GameObject_t1756533147 * value)
	{
		___previousTarget_3 = value;
		Il2CppCodeGenWriteBarrier(&___previousTarget_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VRTK.UIPointerEventArgs
struct UIPointerEventArgs_t1171985978_marshaled_pinvoke
{
	uint32_t ___controllerIndex_0;
	int32_t ___isActive_1;
	GameObject_t1756533147 * ___currentTarget_2;
	GameObject_t1756533147 * ___previousTarget_3;
};
// Native definition for COM marshalling of VRTK.UIPointerEventArgs
struct UIPointerEventArgs_t1171985978_marshaled_com
{
	uint32_t ___controllerIndex_0;
	int32_t ___isActive_1;
	GameObject_t1756533147 * ___currentTarget_2;
	GameObject_t1756533147 * ___previousTarget_3;
};
