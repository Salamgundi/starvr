﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SteamVR_Events/Event`1<Valve.VR.VREvent_t>
struct Event_1_t1285721510;

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREventType6846875.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>
struct  KeyValuePair_2_t48553296 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	Event_1_t1285721510 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t48553296, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t48553296, ___value_1)); }
	inline Event_1_t1285721510 * get_value_1() const { return ___value_1; }
	inline Event_1_t1285721510 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Event_1_t1285721510 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
