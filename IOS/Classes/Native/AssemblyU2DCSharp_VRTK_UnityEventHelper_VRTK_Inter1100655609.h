﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_InteractGrab
struct VRTK_InteractGrab_t124353446;
// VRTK.UnityEventHelper.VRTK_InteractGrab_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t2245989332;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_InteractGrab_UnityEvents
struct  VRTK_InteractGrab_UnityEvents_t1100655609  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_InteractGrab VRTK.UnityEventHelper.VRTK_InteractGrab_UnityEvents::ig
	VRTK_InteractGrab_t124353446 * ___ig_2;
	// VRTK.UnityEventHelper.VRTK_InteractGrab_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_InteractGrab_UnityEvents::OnControllerGrabInteractableObject
	UnityObjectEvent_t2245989332 * ___OnControllerGrabInteractableObject_3;
	// VRTK.UnityEventHelper.VRTK_InteractGrab_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_InteractGrab_UnityEvents::OnControllerUngrabInteractableObject
	UnityObjectEvent_t2245989332 * ___OnControllerUngrabInteractableObject_4;

public:
	inline static int32_t get_offset_of_ig_2() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_UnityEvents_t1100655609, ___ig_2)); }
	inline VRTK_InteractGrab_t124353446 * get_ig_2() const { return ___ig_2; }
	inline VRTK_InteractGrab_t124353446 ** get_address_of_ig_2() { return &___ig_2; }
	inline void set_ig_2(VRTK_InteractGrab_t124353446 * value)
	{
		___ig_2 = value;
		Il2CppCodeGenWriteBarrier(&___ig_2, value);
	}

	inline static int32_t get_offset_of_OnControllerGrabInteractableObject_3() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_UnityEvents_t1100655609, ___OnControllerGrabInteractableObject_3)); }
	inline UnityObjectEvent_t2245989332 * get_OnControllerGrabInteractableObject_3() const { return ___OnControllerGrabInteractableObject_3; }
	inline UnityObjectEvent_t2245989332 ** get_address_of_OnControllerGrabInteractableObject_3() { return &___OnControllerGrabInteractableObject_3; }
	inline void set_OnControllerGrabInteractableObject_3(UnityObjectEvent_t2245989332 * value)
	{
		___OnControllerGrabInteractableObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerGrabInteractableObject_3, value);
	}

	inline static int32_t get_offset_of_OnControllerUngrabInteractableObject_4() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_UnityEvents_t1100655609, ___OnControllerUngrabInteractableObject_4)); }
	inline UnityObjectEvent_t2245989332 * get_OnControllerUngrabInteractableObject_4() const { return ___OnControllerUngrabInteractableObject_4; }
	inline UnityObjectEvent_t2245989332 ** get_address_of_OnControllerUngrabInteractableObject_4() { return &___OnControllerUngrabInteractableObject_4; }
	inline void set_OnControllerUngrabInteractableObject_4(UnityObjectEvent_t2245989332 * value)
	{
		___OnControllerUngrabInteractableObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerUngrabInteractableObject_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
