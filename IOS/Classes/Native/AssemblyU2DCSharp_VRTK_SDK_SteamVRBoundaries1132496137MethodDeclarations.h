﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_SteamVRBoundaries
struct SDK_SteamVRBoundaries_t1132496137;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_SteamVRBoundaries::.ctor()
extern "C"  void SDK_SteamVRBoundaries__ctor_m3975463829 (SDK_SteamVRBoundaries_t1132496137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
