﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.SnapDropZoneGroup_Switcher
struct  SnapDropZoneGroup_Switcher_t643500282  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject VRTK.Examples.SnapDropZoneGroup_Switcher::cubeZone
	GameObject_t1756533147 * ___cubeZone_2;
	// UnityEngine.GameObject VRTK.Examples.SnapDropZoneGroup_Switcher::sphereZone
	GameObject_t1756533147 * ___sphereZone_3;

public:
	inline static int32_t get_offset_of_cubeZone_2() { return static_cast<int32_t>(offsetof(SnapDropZoneGroup_Switcher_t643500282, ___cubeZone_2)); }
	inline GameObject_t1756533147 * get_cubeZone_2() const { return ___cubeZone_2; }
	inline GameObject_t1756533147 ** get_address_of_cubeZone_2() { return &___cubeZone_2; }
	inline void set_cubeZone_2(GameObject_t1756533147 * value)
	{
		___cubeZone_2 = value;
		Il2CppCodeGenWriteBarrier(&___cubeZone_2, value);
	}

	inline static int32_t get_offset_of_sphereZone_3() { return static_cast<int32_t>(offsetof(SnapDropZoneGroup_Switcher_t643500282, ___sphereZone_3)); }
	inline GameObject_t1756533147 * get_sphereZone_3() const { return ___sphereZone_3; }
	inline GameObject_t1756533147 ** get_address_of_sphereZone_3() { return &___sphereZone_3; }
	inline void set_sphereZone_3(GameObject_t1756533147 * value)
	{
		___sphereZone_3 = value;
		Il2CppCodeGenWriteBarrier(&___sphereZone_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
