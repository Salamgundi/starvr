﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4056035046MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m2894101437(__this, ___object0, ___method1, method) ((  void (*) (UnityAction_1_t346736513 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m2836997866_gshared)(__this, ___object0, ___method1, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::Invoke(T0)
#define UnityAction_1_Invoke_m2335625929(__this, ___arg00, method) ((  void (*) (UnityAction_1_t346736513 *, Transform_t3275118058 *, const MethodInfo*))UnityAction_1_Invoke_m1279804060_gshared)(__this, ___arg00, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m4115167046(__this, ___arg00, ___callback1, ___object2, method) ((  Il2CppObject * (*) (UnityAction_1_t346736513 *, Transform_t3275118058 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))UnityAction_1_BeginInvoke_m3462722079_gshared)(__this, ___arg00, ___callback1, ___object2, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m2952455199(__this, ___result0, method) ((  void (*) (UnityAction_1_t346736513 *, Il2CppObject *, const MethodInfo*))UnityAction_1_EndInvoke_m2822290096_gshared)(__this, ___result0, method)
