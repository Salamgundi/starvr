﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.LinearAudioPitch
struct LinearAudioPitch_t1253673161;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.LinearAudioPitch::.ctor()
extern "C"  void LinearAudioPitch__ctor_m1932960403 (LinearAudioPitch_t1253673161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearAudioPitch::Awake()
extern "C"  void LinearAudioPitch_Awake_m182428586 (LinearAudioPitch_t1253673161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearAudioPitch::Update()
extern "C"  void LinearAudioPitch_Update_m3160196552 (LinearAudioPitch_t1253673161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearAudioPitch::Apply()
extern "C"  void LinearAudioPitch_Apply_m4100513155 (LinearAudioPitch_t1253673161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
