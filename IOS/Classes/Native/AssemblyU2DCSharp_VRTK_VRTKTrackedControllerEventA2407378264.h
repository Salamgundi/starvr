﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTKTrackedControllerEventArgs
struct  VRTKTrackedControllerEventArgs_t2407378264 
{
public:
	// System.UInt32 VRTK.VRTKTrackedControllerEventArgs::currentIndex
	uint32_t ___currentIndex_0;
	// System.UInt32 VRTK.VRTKTrackedControllerEventArgs::previousIndex
	uint32_t ___previousIndex_1;

public:
	inline static int32_t get_offset_of_currentIndex_0() { return static_cast<int32_t>(offsetof(VRTKTrackedControllerEventArgs_t2407378264, ___currentIndex_0)); }
	inline uint32_t get_currentIndex_0() const { return ___currentIndex_0; }
	inline uint32_t* get_address_of_currentIndex_0() { return &___currentIndex_0; }
	inline void set_currentIndex_0(uint32_t value)
	{
		___currentIndex_0 = value;
	}

	inline static int32_t get_offset_of_previousIndex_1() { return static_cast<int32_t>(offsetof(VRTKTrackedControllerEventArgs_t2407378264, ___previousIndex_1)); }
	inline uint32_t get_previousIndex_1() const { return ___previousIndex_1; }
	inline uint32_t* get_address_of_previousIndex_1() { return &___previousIndex_1; }
	inline void set_previousIndex_1(uint32_t value)
	{
		___previousIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
