﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSettings/_GetSettingsErrorNameFromEnum
struct _GetSettingsErrorNameFromEnum_t293614055;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRSettingsError4124928198.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSettings/_GetSettingsErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetSettingsErrorNameFromEnum__ctor_m4089028380 (_GetSettingsErrorNameFromEnum_t293614055 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRSettings/_GetSettingsErrorNameFromEnum::Invoke(Valve.VR.EVRSettingsError)
extern "C"  IntPtr_t _GetSettingsErrorNameFromEnum_Invoke_m1498703935 (_GetSettingsErrorNameFromEnum_t293614055 * __this, int32_t ___eError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSettings/_GetSettingsErrorNameFromEnum::BeginInvoke(Valve.VR.EVRSettingsError,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetSettingsErrorNameFromEnum_BeginInvoke_m796991969 (_GetSettingsErrorNameFromEnum_t293614055 * __this, int32_t ___eError0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRSettings/_GetSettingsErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetSettingsErrorNameFromEnum_EndInvoke_m2302931389 (_GetSettingsErrorNameFromEnum_t293614055 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
