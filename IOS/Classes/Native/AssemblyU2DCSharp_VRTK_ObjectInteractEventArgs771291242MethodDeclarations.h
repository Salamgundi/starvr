﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.ObjectInteractEventArgs
struct ObjectInteractEventArgs_t771291242;
struct ObjectInteractEventArgs_t771291242_marshaled_pinvoke;
struct ObjectInteractEventArgs_t771291242_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ObjectInteractEventArgs_t771291242;
struct ObjectInteractEventArgs_t771291242_marshaled_pinvoke;

extern "C" void ObjectInteractEventArgs_t771291242_marshal_pinvoke(const ObjectInteractEventArgs_t771291242& unmarshaled, ObjectInteractEventArgs_t771291242_marshaled_pinvoke& marshaled);
extern "C" void ObjectInteractEventArgs_t771291242_marshal_pinvoke_back(const ObjectInteractEventArgs_t771291242_marshaled_pinvoke& marshaled, ObjectInteractEventArgs_t771291242& unmarshaled);
extern "C" void ObjectInteractEventArgs_t771291242_marshal_pinvoke_cleanup(ObjectInteractEventArgs_t771291242_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ObjectInteractEventArgs_t771291242;
struct ObjectInteractEventArgs_t771291242_marshaled_com;

extern "C" void ObjectInteractEventArgs_t771291242_marshal_com(const ObjectInteractEventArgs_t771291242& unmarshaled, ObjectInteractEventArgs_t771291242_marshaled_com& marshaled);
extern "C" void ObjectInteractEventArgs_t771291242_marshal_com_back(const ObjectInteractEventArgs_t771291242_marshaled_com& marshaled, ObjectInteractEventArgs_t771291242& unmarshaled);
extern "C" void ObjectInteractEventArgs_t771291242_marshal_com_cleanup(ObjectInteractEventArgs_t771291242_marshaled_com& marshaled);
