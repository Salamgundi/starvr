﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MenuHandler/<DoAppear>c__Iterator0
struct U3CDoAppearU3Ec__Iterator0_t3667605745;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MenuHandler/<DoAppear>c__Iterator0::.ctor()
extern "C"  void U3CDoAppearU3Ec__Iterator0__ctor_m1780981058 (U3CDoAppearU3Ec__Iterator0_t3667605745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MenuHandler/<DoAppear>c__Iterator0::MoveNext()
extern "C"  bool U3CDoAppearU3Ec__Iterator0_MoveNext_m784015718 (U3CDoAppearU3Ec__Iterator0_t3667605745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MenuHandler/<DoAppear>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoAppearU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2813940970 (U3CDoAppearU3Ec__Iterator0_t3667605745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MenuHandler/<DoAppear>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoAppearU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m668860978 (U3CDoAppearU3Ec__Iterator0_t3667605745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuHandler/<DoAppear>c__Iterator0::Dispose()
extern "C"  void U3CDoAppearU3Ec__Iterator0_Dispose_m2633606539 (U3CDoAppearU3Ec__Iterator0_t3667605745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuHandler/<DoAppear>c__Iterator0::Reset()
extern "C"  void U3CDoAppearU3Ec__Iterator0_Reset_m706213601 (U3CDoAppearU3Ec__Iterator0_t3667605745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
