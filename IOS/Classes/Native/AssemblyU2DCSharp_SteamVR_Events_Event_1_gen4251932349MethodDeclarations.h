﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Event`1<System.Single>
struct Event_1_t4251932349;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t3443095683;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Events/Event`1<System.Single>::.ctor()
extern "C"  void Event_1__ctor_m2350045080_gshared (Event_1_t4251932349 * __this, const MethodInfo* method);
#define Event_1__ctor_m2350045080(__this, method) ((  void (*) (Event_1_t4251932349 *, const MethodInfo*))Event_1__ctor_m2350045080_gshared)(__this, method)
// System.Void SteamVR_Events/Event`1<System.Single>::Listen(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Listen_m3282637393_gshared (Event_1_t4251932349 * __this, UnityAction_1_t3443095683 * ___action0, const MethodInfo* method);
#define Event_1_Listen_m3282637393(__this, ___action0, method) ((  void (*) (Event_1_t4251932349 *, UnityAction_1_t3443095683 *, const MethodInfo*))Event_1_Listen_m3282637393_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`1<System.Single>::Remove(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Remove_m2571833398_gshared (Event_1_t4251932349 * __this, UnityAction_1_t3443095683 * ___action0, const MethodInfo* method);
#define Event_1_Remove_m2571833398(__this, ___action0, method) ((  void (*) (Event_1_t4251932349 *, UnityAction_1_t3443095683 *, const MethodInfo*))Event_1_Remove_m2571833398_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`1<System.Single>::Send(T)
extern "C"  void Event_1_Send_m2949776906_gshared (Event_1_t4251932349 * __this, float ___arg00, const MethodInfo* method);
#define Event_1_Send_m2949776906(__this, ___arg00, method) ((  void (*) (Event_1_t4251932349 *, float, const MethodInfo*))Event_1_Send_m2949776906_gshared)(__this, ___arg00, method)
