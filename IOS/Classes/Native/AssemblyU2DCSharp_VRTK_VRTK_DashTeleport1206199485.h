﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.DashTeleportEventHandler
struct DashTeleportEventHandler_t1179650517;

#include "AssemblyU2DCSharp_VRTK_VRTK_HeightAdjustTeleport2696079433.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_DashTeleport
struct  VRTK_DashTeleport_t1206199485  : public VRTK_HeightAdjustTeleport_t2696079433
{
public:
	// System.Single VRTK.VRTK_DashTeleport::normalLerpTime
	float ___normalLerpTime_19;
	// System.Single VRTK.VRTK_DashTeleport::minSpeedMps
	float ___minSpeedMps_20;
	// System.Single VRTK.VRTK_DashTeleport::capsuleTopOffset
	float ___capsuleTopOffset_21;
	// System.Single VRTK.VRTK_DashTeleport::capsuleBottomOffset
	float ___capsuleBottomOffset_22;
	// System.Single VRTK.VRTK_DashTeleport::capsuleRadius
	float ___capsuleRadius_23;
	// VRTK.DashTeleportEventHandler VRTK.VRTK_DashTeleport::WillDashThruObjects
	DashTeleportEventHandler_t1179650517 * ___WillDashThruObjects_24;
	// VRTK.DashTeleportEventHandler VRTK.VRTK_DashTeleport::DashedThruObjects
	DashTeleportEventHandler_t1179650517 * ___DashedThruObjects_25;
	// System.Single VRTK.VRTK_DashTeleport::minDistanceForNormalLerp
	float ___minDistanceForNormalLerp_26;
	// System.Single VRTK.VRTK_DashTeleport::lerpTime
	float ___lerpTime_27;

public:
	inline static int32_t get_offset_of_normalLerpTime_19() { return static_cast<int32_t>(offsetof(VRTK_DashTeleport_t1206199485, ___normalLerpTime_19)); }
	inline float get_normalLerpTime_19() const { return ___normalLerpTime_19; }
	inline float* get_address_of_normalLerpTime_19() { return &___normalLerpTime_19; }
	inline void set_normalLerpTime_19(float value)
	{
		___normalLerpTime_19 = value;
	}

	inline static int32_t get_offset_of_minSpeedMps_20() { return static_cast<int32_t>(offsetof(VRTK_DashTeleport_t1206199485, ___minSpeedMps_20)); }
	inline float get_minSpeedMps_20() const { return ___minSpeedMps_20; }
	inline float* get_address_of_minSpeedMps_20() { return &___minSpeedMps_20; }
	inline void set_minSpeedMps_20(float value)
	{
		___minSpeedMps_20 = value;
	}

	inline static int32_t get_offset_of_capsuleTopOffset_21() { return static_cast<int32_t>(offsetof(VRTK_DashTeleport_t1206199485, ___capsuleTopOffset_21)); }
	inline float get_capsuleTopOffset_21() const { return ___capsuleTopOffset_21; }
	inline float* get_address_of_capsuleTopOffset_21() { return &___capsuleTopOffset_21; }
	inline void set_capsuleTopOffset_21(float value)
	{
		___capsuleTopOffset_21 = value;
	}

	inline static int32_t get_offset_of_capsuleBottomOffset_22() { return static_cast<int32_t>(offsetof(VRTK_DashTeleport_t1206199485, ___capsuleBottomOffset_22)); }
	inline float get_capsuleBottomOffset_22() const { return ___capsuleBottomOffset_22; }
	inline float* get_address_of_capsuleBottomOffset_22() { return &___capsuleBottomOffset_22; }
	inline void set_capsuleBottomOffset_22(float value)
	{
		___capsuleBottomOffset_22 = value;
	}

	inline static int32_t get_offset_of_capsuleRadius_23() { return static_cast<int32_t>(offsetof(VRTK_DashTeleport_t1206199485, ___capsuleRadius_23)); }
	inline float get_capsuleRadius_23() const { return ___capsuleRadius_23; }
	inline float* get_address_of_capsuleRadius_23() { return &___capsuleRadius_23; }
	inline void set_capsuleRadius_23(float value)
	{
		___capsuleRadius_23 = value;
	}

	inline static int32_t get_offset_of_WillDashThruObjects_24() { return static_cast<int32_t>(offsetof(VRTK_DashTeleport_t1206199485, ___WillDashThruObjects_24)); }
	inline DashTeleportEventHandler_t1179650517 * get_WillDashThruObjects_24() const { return ___WillDashThruObjects_24; }
	inline DashTeleportEventHandler_t1179650517 ** get_address_of_WillDashThruObjects_24() { return &___WillDashThruObjects_24; }
	inline void set_WillDashThruObjects_24(DashTeleportEventHandler_t1179650517 * value)
	{
		___WillDashThruObjects_24 = value;
		Il2CppCodeGenWriteBarrier(&___WillDashThruObjects_24, value);
	}

	inline static int32_t get_offset_of_DashedThruObjects_25() { return static_cast<int32_t>(offsetof(VRTK_DashTeleport_t1206199485, ___DashedThruObjects_25)); }
	inline DashTeleportEventHandler_t1179650517 * get_DashedThruObjects_25() const { return ___DashedThruObjects_25; }
	inline DashTeleportEventHandler_t1179650517 ** get_address_of_DashedThruObjects_25() { return &___DashedThruObjects_25; }
	inline void set_DashedThruObjects_25(DashTeleportEventHandler_t1179650517 * value)
	{
		___DashedThruObjects_25 = value;
		Il2CppCodeGenWriteBarrier(&___DashedThruObjects_25, value);
	}

	inline static int32_t get_offset_of_minDistanceForNormalLerp_26() { return static_cast<int32_t>(offsetof(VRTK_DashTeleport_t1206199485, ___minDistanceForNormalLerp_26)); }
	inline float get_minDistanceForNormalLerp_26() const { return ___minDistanceForNormalLerp_26; }
	inline float* get_address_of_minDistanceForNormalLerp_26() { return &___minDistanceForNormalLerp_26; }
	inline void set_minDistanceForNormalLerp_26(float value)
	{
		___minDistanceForNormalLerp_26 = value;
	}

	inline static int32_t get_offset_of_lerpTime_27() { return static_cast<int32_t>(offsetof(VRTK_DashTeleport_t1206199485, ___lerpTime_27)); }
	inline float get_lerpTime_27() const { return ___lerpTime_27; }
	inline float* get_address_of_lerpTime_27() { return &___lerpTime_27; }
	inline void set_lerpTime_27(float value)
	{
		___lerpTime_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
