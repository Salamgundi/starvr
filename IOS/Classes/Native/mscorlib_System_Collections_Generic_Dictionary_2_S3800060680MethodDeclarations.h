﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVREventType,System.Object>
struct ShimEnumerator_t3800060680;
// System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>
struct Dictionary_2_t3694935859;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVREventType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m418225379_gshared (ShimEnumerator_t3800060680 * __this, Dictionary_2_t3694935859 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m418225379(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3800060680 *, Dictionary_2_t3694935859 *, const MethodInfo*))ShimEnumerator__ctor_m418225379_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVREventType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1712715176_gshared (ShimEnumerator_t3800060680 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1712715176(__this, method) ((  bool (*) (ShimEnumerator_t3800060680 *, const MethodInfo*))ShimEnumerator_MoveNext_m1712715176_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVREventType,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m953268552_gshared (ShimEnumerator_t3800060680 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m953268552(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3800060680 *, const MethodInfo*))ShimEnumerator_get_Entry_m953268552_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVREventType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m509364227_gshared (ShimEnumerator_t3800060680 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m509364227(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3800060680 *, const MethodInfo*))ShimEnumerator_get_Key_m509364227_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVREventType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m411737899_gshared (ShimEnumerator_t3800060680 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m411737899(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3800060680 *, const MethodInfo*))ShimEnumerator_get_Value_m411737899_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVREventType,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1436372153_gshared (ShimEnumerator_t3800060680 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1436372153(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3800060680 *, const MethodInfo*))ShimEnumerator_get_Current_m1436372153_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVREventType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3173966237_gshared (ShimEnumerator_t3800060680 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3173966237(__this, method) ((  void (*) (ShimEnumerator_t3800060680 *, const MethodInfo*))ShimEnumerator_Reset_m3173966237_gshared)(__this, method)
