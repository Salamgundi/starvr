﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.AfterTimer_Component
struct AfterTimer_Component_t442788763;
// System.Action
struct Action_t3226471752;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"

// System.Void Valve.VR.InteractionSystem.AfterTimer_Component::.ctor()
extern "C"  void AfterTimer_Component__ctor_m3447645401 (AfterTimer_Component_t442788763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.AfterTimer_Component::Init(System.Single,System.Action,System.Boolean)
extern "C"  void AfterTimer_Component_Init_m3949658302 (AfterTimer_Component_t442788763 * __this, float ____time0, Action_t3226471752 * ____callback1, bool ___earlydestroy2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.AfterTimer_Component::Wait()
extern "C"  Il2CppObject * AfterTimer_Component_Wait_m3354343662 (AfterTimer_Component_t442788763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.AfterTimer_Component::OnDestroy()
extern "C"  void AfterTimer_Component_OnDestroy_m2133658222 (AfterTimer_Component_t442788763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
