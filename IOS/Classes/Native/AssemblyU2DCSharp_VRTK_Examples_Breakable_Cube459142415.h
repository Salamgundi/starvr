﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Breakable_Cube
struct  Breakable_Cube_t459142415  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VRTK.Examples.Breakable_Cube::breakForce
	float ___breakForce_2;

public:
	inline static int32_t get_offset_of_breakForce_2() { return static_cast<int32_t>(offsetof(Breakable_Cube_t459142415, ___breakForce_2)); }
	inline float get_breakForce_2() const { return ___breakForce_2; }
	inline float* get_address_of_breakForce_2() { return &___breakForce_2; }
	inline void set_breakForce_2(float value)
	{
		___breakForce_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
