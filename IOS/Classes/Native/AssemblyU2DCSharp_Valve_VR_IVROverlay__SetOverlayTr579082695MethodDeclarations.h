﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceRelative
struct _SetOverlayTransformTrackedDeviceRelative_t579082695;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceRelative::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayTransformTrackedDeviceRelative__ctor_m163753444 (_SetOverlayTransformTrackedDeviceRelative_t579082695 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceRelative::Invoke(System.UInt64,System.UInt32,Valve.VR.HmdMatrix34_t&)
extern "C"  int32_t _SetOverlayTransformTrackedDeviceRelative_Invoke_m2348853633 (_SetOverlayTransformTrackedDeviceRelative_t579082695 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unTrackedDevice1, HmdMatrix34_t_t664273062 * ___pmatTrackedDeviceToOverlayTransform2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceRelative::BeginInvoke(System.UInt64,System.UInt32,Valve.VR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetOverlayTransformTrackedDeviceRelative_BeginInvoke_m4142423774 (_SetOverlayTransformTrackedDeviceRelative_t579082695 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unTrackedDevice1, HmdMatrix34_t_t664273062 * ___pmatTrackedDeviceToOverlayTransform2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceRelative::EndInvoke(Valve.VR.HmdMatrix34_t&,System.IAsyncResult)
extern "C"  int32_t _SetOverlayTransformTrackedDeviceRelative_EndInvoke_m178959756 (_SetOverlayTransformTrackedDeviceRelative_t579082695 * __this, HmdMatrix34_t_t664273062 * ___pmatTrackedDeviceToOverlayTransform0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
