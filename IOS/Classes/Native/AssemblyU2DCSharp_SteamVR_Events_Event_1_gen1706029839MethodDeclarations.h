﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Event`1<System.Boolean>
struct Event_1_t1706029839;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t897193173;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Events/Event`1<System.Boolean>::.ctor()
extern "C"  void Event_1__ctor_m826889986_gshared (Event_1_t1706029839 * __this, const MethodInfo* method);
#define Event_1__ctor_m826889986(__this, method) ((  void (*) (Event_1_t1706029839 *, const MethodInfo*))Event_1__ctor_m826889986_gshared)(__this, method)
// System.Void SteamVR_Events/Event`1<System.Boolean>::Listen(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Listen_m1179862519_gshared (Event_1_t1706029839 * __this, UnityAction_1_t897193173 * ___action0, const MethodInfo* method);
#define Event_1_Listen_m1179862519(__this, ___action0, method) ((  void (*) (Event_1_t1706029839 *, UnityAction_1_t897193173 *, const MethodInfo*))Event_1_Listen_m1179862519_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`1<System.Boolean>::Remove(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Remove_m4204324720_gshared (Event_1_t1706029839 * __this, UnityAction_1_t897193173 * ___action0, const MethodInfo* method);
#define Event_1_Remove_m4204324720(__this, ___action0, method) ((  void (*) (Event_1_t1706029839 *, UnityAction_1_t897193173 *, const MethodInfo*))Event_1_Remove_m4204324720_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`1<System.Boolean>::Send(T)
extern "C"  void Event_1_Send_m3832506532_gshared (Event_1_t1706029839 * __this, bool ___arg00, const MethodInfo* method);
#define Event_1_Send_m3832506532(__this, ___arg00, method) ((  void (*) (Event_1_t1706029839 *, bool, const MethodInfo*))Event_1_Send_m3832506532_gshared)(__this, ___arg00, method)
