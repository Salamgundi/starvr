﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_SnapDropZone/<AttemptForceSnapAtEndOfFrame>c__Iterator1
struct U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_t3586163035;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_SnapDropZone/<AttemptForceSnapAtEndOfFrame>c__Iterator1::.ctor()
extern "C"  void U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1__ctor_m3273889776 (U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_t3586163035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SnapDropZone/<AttemptForceSnapAtEndOfFrame>c__Iterator1::MoveNext()
extern "C"  bool U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_MoveNext_m519589576 (U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_t3586163035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_SnapDropZone/<AttemptForceSnapAtEndOfFrame>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3499012470 (U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_t3586163035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_SnapDropZone/<AttemptForceSnapAtEndOfFrame>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2415173070 (U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_t3586163035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone/<AttemptForceSnapAtEndOfFrame>c__Iterator1::Dispose()
extern "C"  void U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_Dispose_m66087061 (U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_t3586163035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone/<AttemptForceSnapAtEndOfFrame>c__Iterator1::Reset()
extern "C"  void U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_Reset_m3301413571 (U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_t3586163035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
