﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// System.String[]
struct StringU5BU5D_t1642385972;
// Valve.VR.InteractionSystem.VelocityEstimator
struct VelocityEstimator_t1153298725;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1543711741.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.Throwable
struct  Throwable_t3270371398  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.Hand/AttachmentFlags Valve.VR.InteractionSystem.Throwable::attachmentFlags
	int32_t ___attachmentFlags_2;
	// System.String Valve.VR.InteractionSystem.Throwable::attachmentPoint
	String_t* ___attachmentPoint_3;
	// System.Single Valve.VR.InteractionSystem.Throwable::catchSpeedThreshold
	float ___catchSpeedThreshold_4;
	// System.Boolean Valve.VR.InteractionSystem.Throwable::restoreOriginalParent
	bool ___restoreOriginalParent_5;
	// System.Boolean Valve.VR.InteractionSystem.Throwable::attachEaseIn
	bool ___attachEaseIn_6;
	// UnityEngine.AnimationCurve Valve.VR.InteractionSystem.Throwable::snapAttachEaseInCurve
	AnimationCurve_t3306541151 * ___snapAttachEaseInCurve_7;
	// System.Single Valve.VR.InteractionSystem.Throwable::snapAttachEaseInTime
	float ___snapAttachEaseInTime_8;
	// System.String[] Valve.VR.InteractionSystem.Throwable::attachEaseInAttachmentNames
	StringU5BU5D_t1642385972* ___attachEaseInAttachmentNames_9;
	// Valve.VR.InteractionSystem.VelocityEstimator Valve.VR.InteractionSystem.Throwable::velocityEstimator
	VelocityEstimator_t1153298725 * ___velocityEstimator_10;
	// System.Boolean Valve.VR.InteractionSystem.Throwable::attached
	bool ___attached_11;
	// System.Single Valve.VR.InteractionSystem.Throwable::attachTime
	float ___attachTime_12;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.Throwable::attachPosition
	Vector3_t2243707580  ___attachPosition_13;
	// UnityEngine.Quaternion Valve.VR.InteractionSystem.Throwable::attachRotation
	Quaternion_t4030073918  ___attachRotation_14;
	// UnityEngine.Transform Valve.VR.InteractionSystem.Throwable::attachEaseInTransform
	Transform_t3275118058 * ___attachEaseInTransform_15;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.Throwable::onPickUp
	UnityEvent_t408735097 * ___onPickUp_16;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.Throwable::onDetachFromHand
	UnityEvent_t408735097 * ___onDetachFromHand_17;
	// System.Boolean Valve.VR.InteractionSystem.Throwable::snapAttachEaseInCompleted
	bool ___snapAttachEaseInCompleted_18;

public:
	inline static int32_t get_offset_of_attachmentFlags_2() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___attachmentFlags_2)); }
	inline int32_t get_attachmentFlags_2() const { return ___attachmentFlags_2; }
	inline int32_t* get_address_of_attachmentFlags_2() { return &___attachmentFlags_2; }
	inline void set_attachmentFlags_2(int32_t value)
	{
		___attachmentFlags_2 = value;
	}

	inline static int32_t get_offset_of_attachmentPoint_3() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___attachmentPoint_3)); }
	inline String_t* get_attachmentPoint_3() const { return ___attachmentPoint_3; }
	inline String_t** get_address_of_attachmentPoint_3() { return &___attachmentPoint_3; }
	inline void set_attachmentPoint_3(String_t* value)
	{
		___attachmentPoint_3 = value;
		Il2CppCodeGenWriteBarrier(&___attachmentPoint_3, value);
	}

	inline static int32_t get_offset_of_catchSpeedThreshold_4() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___catchSpeedThreshold_4)); }
	inline float get_catchSpeedThreshold_4() const { return ___catchSpeedThreshold_4; }
	inline float* get_address_of_catchSpeedThreshold_4() { return &___catchSpeedThreshold_4; }
	inline void set_catchSpeedThreshold_4(float value)
	{
		___catchSpeedThreshold_4 = value;
	}

	inline static int32_t get_offset_of_restoreOriginalParent_5() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___restoreOriginalParent_5)); }
	inline bool get_restoreOriginalParent_5() const { return ___restoreOriginalParent_5; }
	inline bool* get_address_of_restoreOriginalParent_5() { return &___restoreOriginalParent_5; }
	inline void set_restoreOriginalParent_5(bool value)
	{
		___restoreOriginalParent_5 = value;
	}

	inline static int32_t get_offset_of_attachEaseIn_6() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___attachEaseIn_6)); }
	inline bool get_attachEaseIn_6() const { return ___attachEaseIn_6; }
	inline bool* get_address_of_attachEaseIn_6() { return &___attachEaseIn_6; }
	inline void set_attachEaseIn_6(bool value)
	{
		___attachEaseIn_6 = value;
	}

	inline static int32_t get_offset_of_snapAttachEaseInCurve_7() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___snapAttachEaseInCurve_7)); }
	inline AnimationCurve_t3306541151 * get_snapAttachEaseInCurve_7() const { return ___snapAttachEaseInCurve_7; }
	inline AnimationCurve_t3306541151 ** get_address_of_snapAttachEaseInCurve_7() { return &___snapAttachEaseInCurve_7; }
	inline void set_snapAttachEaseInCurve_7(AnimationCurve_t3306541151 * value)
	{
		___snapAttachEaseInCurve_7 = value;
		Il2CppCodeGenWriteBarrier(&___snapAttachEaseInCurve_7, value);
	}

	inline static int32_t get_offset_of_snapAttachEaseInTime_8() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___snapAttachEaseInTime_8)); }
	inline float get_snapAttachEaseInTime_8() const { return ___snapAttachEaseInTime_8; }
	inline float* get_address_of_snapAttachEaseInTime_8() { return &___snapAttachEaseInTime_8; }
	inline void set_snapAttachEaseInTime_8(float value)
	{
		___snapAttachEaseInTime_8 = value;
	}

	inline static int32_t get_offset_of_attachEaseInAttachmentNames_9() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___attachEaseInAttachmentNames_9)); }
	inline StringU5BU5D_t1642385972* get_attachEaseInAttachmentNames_9() const { return ___attachEaseInAttachmentNames_9; }
	inline StringU5BU5D_t1642385972** get_address_of_attachEaseInAttachmentNames_9() { return &___attachEaseInAttachmentNames_9; }
	inline void set_attachEaseInAttachmentNames_9(StringU5BU5D_t1642385972* value)
	{
		___attachEaseInAttachmentNames_9 = value;
		Il2CppCodeGenWriteBarrier(&___attachEaseInAttachmentNames_9, value);
	}

	inline static int32_t get_offset_of_velocityEstimator_10() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___velocityEstimator_10)); }
	inline VelocityEstimator_t1153298725 * get_velocityEstimator_10() const { return ___velocityEstimator_10; }
	inline VelocityEstimator_t1153298725 ** get_address_of_velocityEstimator_10() { return &___velocityEstimator_10; }
	inline void set_velocityEstimator_10(VelocityEstimator_t1153298725 * value)
	{
		___velocityEstimator_10 = value;
		Il2CppCodeGenWriteBarrier(&___velocityEstimator_10, value);
	}

	inline static int32_t get_offset_of_attached_11() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___attached_11)); }
	inline bool get_attached_11() const { return ___attached_11; }
	inline bool* get_address_of_attached_11() { return &___attached_11; }
	inline void set_attached_11(bool value)
	{
		___attached_11 = value;
	}

	inline static int32_t get_offset_of_attachTime_12() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___attachTime_12)); }
	inline float get_attachTime_12() const { return ___attachTime_12; }
	inline float* get_address_of_attachTime_12() { return &___attachTime_12; }
	inline void set_attachTime_12(float value)
	{
		___attachTime_12 = value;
	}

	inline static int32_t get_offset_of_attachPosition_13() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___attachPosition_13)); }
	inline Vector3_t2243707580  get_attachPosition_13() const { return ___attachPosition_13; }
	inline Vector3_t2243707580 * get_address_of_attachPosition_13() { return &___attachPosition_13; }
	inline void set_attachPosition_13(Vector3_t2243707580  value)
	{
		___attachPosition_13 = value;
	}

	inline static int32_t get_offset_of_attachRotation_14() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___attachRotation_14)); }
	inline Quaternion_t4030073918  get_attachRotation_14() const { return ___attachRotation_14; }
	inline Quaternion_t4030073918 * get_address_of_attachRotation_14() { return &___attachRotation_14; }
	inline void set_attachRotation_14(Quaternion_t4030073918  value)
	{
		___attachRotation_14 = value;
	}

	inline static int32_t get_offset_of_attachEaseInTransform_15() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___attachEaseInTransform_15)); }
	inline Transform_t3275118058 * get_attachEaseInTransform_15() const { return ___attachEaseInTransform_15; }
	inline Transform_t3275118058 ** get_address_of_attachEaseInTransform_15() { return &___attachEaseInTransform_15; }
	inline void set_attachEaseInTransform_15(Transform_t3275118058 * value)
	{
		___attachEaseInTransform_15 = value;
		Il2CppCodeGenWriteBarrier(&___attachEaseInTransform_15, value);
	}

	inline static int32_t get_offset_of_onPickUp_16() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___onPickUp_16)); }
	inline UnityEvent_t408735097 * get_onPickUp_16() const { return ___onPickUp_16; }
	inline UnityEvent_t408735097 ** get_address_of_onPickUp_16() { return &___onPickUp_16; }
	inline void set_onPickUp_16(UnityEvent_t408735097 * value)
	{
		___onPickUp_16 = value;
		Il2CppCodeGenWriteBarrier(&___onPickUp_16, value);
	}

	inline static int32_t get_offset_of_onDetachFromHand_17() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___onDetachFromHand_17)); }
	inline UnityEvent_t408735097 * get_onDetachFromHand_17() const { return ___onDetachFromHand_17; }
	inline UnityEvent_t408735097 ** get_address_of_onDetachFromHand_17() { return &___onDetachFromHand_17; }
	inline void set_onDetachFromHand_17(UnityEvent_t408735097 * value)
	{
		___onDetachFromHand_17 = value;
		Il2CppCodeGenWriteBarrier(&___onDetachFromHand_17, value);
	}

	inline static int32_t get_offset_of_snapAttachEaseInCompleted_18() { return static_cast<int32_t>(offsetof(Throwable_t3270371398, ___snapAttachEaseInCompleted_18)); }
	inline bool get_snapAttachEaseInCompleted_18() const { return ___snapAttachEaseInCompleted_18; }
	inline bool* get_address_of_snapAttachEaseInCompleted_18() { return &___snapAttachEaseInCompleted_18; }
	inline void set_snapAttachEaseInCompleted_18(bool value)
	{
		___snapAttachEaseInCompleted_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
