﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,VRTK.UIPointerEventArgs>
struct UnityEvent_2_t4149639883;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.UIPointerEventArgs>
struct UnityAction_2_t2267441965;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_VRTK_UIPointerEventArgs1171985978.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.UIPointerEventArgs>::.ctor()
extern "C"  void UnityEvent_2__ctor_m3043444613_gshared (UnityEvent_2_t4149639883 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m3043444613(__this, method) ((  void (*) (UnityEvent_2_t4149639883 *, const MethodInfo*))UnityEvent_2__ctor_m3043444613_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.UIPointerEventArgs>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m1031236353_gshared (UnityEvent_2_t4149639883 * __this, UnityAction_2_t2267441965 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m1031236353(__this, ___call0, method) ((  void (*) (UnityEvent_2_t4149639883 *, UnityAction_2_t2267441965 *, const MethodInfo*))UnityEvent_2_AddListener_m1031236353_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.UIPointerEventArgs>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m943032236_gshared (UnityEvent_2_t4149639883 * __this, UnityAction_2_t2267441965 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m943032236(__this, ___call0, method) ((  void (*) (UnityEvent_2_t4149639883 *, UnityAction_2_t2267441965 *, const MethodInfo*))UnityEvent_2_RemoveListener_m943032236_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,VRTK.UIPointerEventArgs>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m2234858569_gshared (UnityEvent_2_t4149639883 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m2234858569(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t4149639883 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m2234858569_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.UIPointerEventArgs>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m1576819037_gshared (UnityEvent_2_t4149639883 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m1576819037(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t4149639883 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m1576819037_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.UIPointerEventArgs>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m3272598820_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t2267441965 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m3272598820(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t2267441965 *, const MethodInfo*))UnityEvent_2_GetDelegate_m3272598820_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.UIPointerEventArgs>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m1037220644_gshared (UnityEvent_2_t4149639883 * __this, Il2CppObject * ___arg00, UIPointerEventArgs_t1171985978  ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m1037220644(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t4149639883 *, Il2CppObject *, UIPointerEventArgs_t1171985978 , const MethodInfo*))UnityEvent_2_Invoke_m1037220644_gshared)(__this, ___arg00, ___arg11, method)
