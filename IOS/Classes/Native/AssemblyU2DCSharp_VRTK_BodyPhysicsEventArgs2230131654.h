﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.BodyPhysicsEventArgs
struct  BodyPhysicsEventArgs_t2230131654 
{
public:
	// UnityEngine.GameObject VRTK.BodyPhysicsEventArgs::target
	GameObject_t1756533147 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(BodyPhysicsEventArgs_t2230131654, ___target_0)); }
	inline GameObject_t1756533147 * get_target_0() const { return ___target_0; }
	inline GameObject_t1756533147 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(GameObject_t1756533147 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier(&___target_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VRTK.BodyPhysicsEventArgs
struct BodyPhysicsEventArgs_t2230131654_marshaled_pinvoke
{
	GameObject_t1756533147 * ___target_0;
};
// Native definition for COM marshalling of VRTK.BodyPhysicsEventArgs
struct BodyPhysicsEventArgs_t2230131654_marshaled_com
{
	GameObject_t1756533147 * ___target_0;
};
