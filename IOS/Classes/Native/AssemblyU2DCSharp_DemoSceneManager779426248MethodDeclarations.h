﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DemoSceneManager
struct DemoSceneManager_t779426248;

#include "codegen/il2cpp-codegen.h"

// System.Void DemoSceneManager::.ctor()
extern "C"  void DemoSceneManager__ctor_m238314145 (DemoSceneManager_t779426248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoSceneManager::Update()
extern "C"  void DemoSceneManager_Update_m3480481970 (DemoSceneManager_t779426248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
