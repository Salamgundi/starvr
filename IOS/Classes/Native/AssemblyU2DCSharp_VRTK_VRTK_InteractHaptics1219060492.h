﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_InteractHaptics
struct  VRTK_InteractHaptics_t1219060492  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VRTK.VRTK_InteractHaptics::strengthOnTouch
	float ___strengthOnTouch_2;
	// System.Single VRTK.VRTK_InteractHaptics::durationOnTouch
	float ___durationOnTouch_3;
	// System.Single VRTK.VRTK_InteractHaptics::intervalOnTouch
	float ___intervalOnTouch_4;
	// System.Single VRTK.VRTK_InteractHaptics::strengthOnGrab
	float ___strengthOnGrab_5;
	// System.Single VRTK.VRTK_InteractHaptics::durationOnGrab
	float ___durationOnGrab_6;
	// System.Single VRTK.VRTK_InteractHaptics::intervalOnGrab
	float ___intervalOnGrab_7;
	// System.Single VRTK.VRTK_InteractHaptics::strengthOnUse
	float ___strengthOnUse_8;
	// System.Single VRTK.VRTK_InteractHaptics::durationOnUse
	float ___durationOnUse_9;
	// System.Single VRTK.VRTK_InteractHaptics::intervalOnUse
	float ___intervalOnUse_10;

public:
	inline static int32_t get_offset_of_strengthOnTouch_2() { return static_cast<int32_t>(offsetof(VRTK_InteractHaptics_t1219060492, ___strengthOnTouch_2)); }
	inline float get_strengthOnTouch_2() const { return ___strengthOnTouch_2; }
	inline float* get_address_of_strengthOnTouch_2() { return &___strengthOnTouch_2; }
	inline void set_strengthOnTouch_2(float value)
	{
		___strengthOnTouch_2 = value;
	}

	inline static int32_t get_offset_of_durationOnTouch_3() { return static_cast<int32_t>(offsetof(VRTK_InteractHaptics_t1219060492, ___durationOnTouch_3)); }
	inline float get_durationOnTouch_3() const { return ___durationOnTouch_3; }
	inline float* get_address_of_durationOnTouch_3() { return &___durationOnTouch_3; }
	inline void set_durationOnTouch_3(float value)
	{
		___durationOnTouch_3 = value;
	}

	inline static int32_t get_offset_of_intervalOnTouch_4() { return static_cast<int32_t>(offsetof(VRTK_InteractHaptics_t1219060492, ___intervalOnTouch_4)); }
	inline float get_intervalOnTouch_4() const { return ___intervalOnTouch_4; }
	inline float* get_address_of_intervalOnTouch_4() { return &___intervalOnTouch_4; }
	inline void set_intervalOnTouch_4(float value)
	{
		___intervalOnTouch_4 = value;
	}

	inline static int32_t get_offset_of_strengthOnGrab_5() { return static_cast<int32_t>(offsetof(VRTK_InteractHaptics_t1219060492, ___strengthOnGrab_5)); }
	inline float get_strengthOnGrab_5() const { return ___strengthOnGrab_5; }
	inline float* get_address_of_strengthOnGrab_5() { return &___strengthOnGrab_5; }
	inline void set_strengthOnGrab_5(float value)
	{
		___strengthOnGrab_5 = value;
	}

	inline static int32_t get_offset_of_durationOnGrab_6() { return static_cast<int32_t>(offsetof(VRTK_InteractHaptics_t1219060492, ___durationOnGrab_6)); }
	inline float get_durationOnGrab_6() const { return ___durationOnGrab_6; }
	inline float* get_address_of_durationOnGrab_6() { return &___durationOnGrab_6; }
	inline void set_durationOnGrab_6(float value)
	{
		___durationOnGrab_6 = value;
	}

	inline static int32_t get_offset_of_intervalOnGrab_7() { return static_cast<int32_t>(offsetof(VRTK_InteractHaptics_t1219060492, ___intervalOnGrab_7)); }
	inline float get_intervalOnGrab_7() const { return ___intervalOnGrab_7; }
	inline float* get_address_of_intervalOnGrab_7() { return &___intervalOnGrab_7; }
	inline void set_intervalOnGrab_7(float value)
	{
		___intervalOnGrab_7 = value;
	}

	inline static int32_t get_offset_of_strengthOnUse_8() { return static_cast<int32_t>(offsetof(VRTK_InteractHaptics_t1219060492, ___strengthOnUse_8)); }
	inline float get_strengthOnUse_8() const { return ___strengthOnUse_8; }
	inline float* get_address_of_strengthOnUse_8() { return &___strengthOnUse_8; }
	inline void set_strengthOnUse_8(float value)
	{
		___strengthOnUse_8 = value;
	}

	inline static int32_t get_offset_of_durationOnUse_9() { return static_cast<int32_t>(offsetof(VRTK_InteractHaptics_t1219060492, ___durationOnUse_9)); }
	inline float get_durationOnUse_9() const { return ___durationOnUse_9; }
	inline float* get_address_of_durationOnUse_9() { return &___durationOnUse_9; }
	inline void set_durationOnUse_9(float value)
	{
		___durationOnUse_9 = value;
	}

	inline static int32_t get_offset_of_intervalOnUse_10() { return static_cast<int32_t>(offsetof(VRTK_InteractHaptics_t1219060492, ___intervalOnUse_10)); }
	inline float get_intervalOnUse_10() const { return ___intervalOnUse_10; }
	inline float* get_address_of_intervalOnUse_10() { return &___intervalOnUse_10; }
	inline void set_intervalOnUse_10(float value)
	{
		___intervalOnUse_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
