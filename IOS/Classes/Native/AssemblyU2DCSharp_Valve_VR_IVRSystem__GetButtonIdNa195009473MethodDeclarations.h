﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetButtonIdNameFromEnum
struct _GetButtonIdNameFromEnum_t195009473;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRButtonId66145412.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetButtonIdNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetButtonIdNameFromEnum__ctor_m1685853460 (_GetButtonIdNameFromEnum_t195009473 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRSystem/_GetButtonIdNameFromEnum::Invoke(Valve.VR.EVRButtonId)
extern "C"  IntPtr_t _GetButtonIdNameFromEnum_Invoke_m1474086367 (_GetButtonIdNameFromEnum_t195009473 * __this, int32_t ___eButtonId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetButtonIdNameFromEnum::BeginInvoke(Valve.VR.EVRButtonId,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetButtonIdNameFromEnum_BeginInvoke_m3543697797 (_GetButtonIdNameFromEnum_t195009473 * __this, int32_t ___eButtonId0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRSystem/_GetButtonIdNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetButtonIdNameFromEnum_EndInvoke_m3510842715 (_GetButtonIdNameFromEnum_t195009473 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
