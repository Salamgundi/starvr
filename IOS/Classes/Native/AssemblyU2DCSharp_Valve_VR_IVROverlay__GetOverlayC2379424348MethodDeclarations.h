﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayColor
struct _GetOverlayColor_t2379424348;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayColor::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayColor__ctor_m1114046821 (_GetOverlayColor_t2379424348 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayColor::Invoke(System.UInt64,System.Single&,System.Single&,System.Single&)
extern "C"  int32_t _GetOverlayColor_Invoke_m83921645 (_GetOverlayColor_t2379424348 * __this, uint64_t ___ulOverlayHandle0, float* ___pfRed1, float* ___pfGreen2, float* ___pfBlue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayColor::BeginInvoke(System.UInt64,System.Single&,System.Single&,System.Single&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayColor_BeginInvoke_m3323846210 (_GetOverlayColor_t2379424348 * __this, uint64_t ___ulOverlayHandle0, float* ___pfRed1, float* ___pfGreen2, float* ___pfBlue3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayColor::EndInvoke(System.Single&,System.Single&,System.Single&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayColor_EndInvoke_m67816402 (_GetOverlayColor_t2379424348 * __this, float* ___pfRed0, float* ___pfGreen1, float* ___pfBlue2, Il2CppObject * ___result3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
