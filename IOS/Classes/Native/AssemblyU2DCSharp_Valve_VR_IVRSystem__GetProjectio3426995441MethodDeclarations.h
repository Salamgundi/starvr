﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetProjectionRaw
struct _GetProjectionRaw_t3426995441;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetProjectionRaw::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetProjectionRaw__ctor_m1598285270 (_GetProjectionRaw_t3426995441 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_GetProjectionRaw::Invoke(Valve.VR.EVREye,System.Single&,System.Single&,System.Single&,System.Single&)
extern "C"  void _GetProjectionRaw_Invoke_m4200820360 (_GetProjectionRaw_t3426995441 * __this, int32_t ___eEye0, float* ___pfLeft1, float* ___pfRight2, float* ___pfTop3, float* ___pfBottom4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetProjectionRaw::BeginInvoke(Valve.VR.EVREye,System.Single&,System.Single&,System.Single&,System.Single&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetProjectionRaw_BeginInvoke_m1246333403 (_GetProjectionRaw_t3426995441 * __this, int32_t ___eEye0, float* ___pfLeft1, float* ___pfRight2, float* ___pfTop3, float* ___pfBottom4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_GetProjectionRaw::EndInvoke(System.Single&,System.Single&,System.Single&,System.Single&,System.IAsyncResult)
extern "C"  void _GetProjectionRaw_EndInvoke_m1852423016 (_GetProjectionRaw_t3426995441 * __this, float* ___pfLeft0, float* ___pfRight1, float* ___pfTop2, float* ___pfBottom3, Il2CppObject * ___result4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
