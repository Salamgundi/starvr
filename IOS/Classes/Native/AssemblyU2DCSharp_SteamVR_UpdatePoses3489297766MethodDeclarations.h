﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_UpdatePoses
struct SteamVR_UpdatePoses_t3489297766;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_UpdatePoses::.ctor()
extern "C"  void SteamVR_UpdatePoses__ctor_m3675379877 (SteamVR_UpdatePoses_t3489297766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_UpdatePoses::Awake()
extern "C"  void SteamVR_UpdatePoses_Awake_m521018950 (SteamVR_UpdatePoses_t3489297766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_UpdatePoses::OnPreCull()
extern "C"  void SteamVR_UpdatePoses_OnPreCull_m2126972753 (SteamVR_UpdatePoses_t3489297766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
