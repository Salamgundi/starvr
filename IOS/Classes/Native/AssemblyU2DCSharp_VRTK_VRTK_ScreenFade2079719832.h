﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_ScreenFade
struct VRTK_ScreenFade_t2079719832;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ScreenFade
struct  VRTK_ScreenFade_t2079719832  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material VRTK.VRTK_ScreenFade::fadeMaterial
	Material_t193706927 * ___fadeMaterial_3;
	// UnityEngine.Color VRTK.VRTK_ScreenFade::currentColor
	Color_t2020392075  ___currentColor_4;
	// UnityEngine.Color VRTK.VRTK_ScreenFade::targetColor
	Color_t2020392075  ___targetColor_5;
	// UnityEngine.Color VRTK.VRTK_ScreenFade::deltaColor
	Color_t2020392075  ___deltaColor_6;

public:
	inline static int32_t get_offset_of_fadeMaterial_3() { return static_cast<int32_t>(offsetof(VRTK_ScreenFade_t2079719832, ___fadeMaterial_3)); }
	inline Material_t193706927 * get_fadeMaterial_3() const { return ___fadeMaterial_3; }
	inline Material_t193706927 ** get_address_of_fadeMaterial_3() { return &___fadeMaterial_3; }
	inline void set_fadeMaterial_3(Material_t193706927 * value)
	{
		___fadeMaterial_3 = value;
		Il2CppCodeGenWriteBarrier(&___fadeMaterial_3, value);
	}

	inline static int32_t get_offset_of_currentColor_4() { return static_cast<int32_t>(offsetof(VRTK_ScreenFade_t2079719832, ___currentColor_4)); }
	inline Color_t2020392075  get_currentColor_4() const { return ___currentColor_4; }
	inline Color_t2020392075 * get_address_of_currentColor_4() { return &___currentColor_4; }
	inline void set_currentColor_4(Color_t2020392075  value)
	{
		___currentColor_4 = value;
	}

	inline static int32_t get_offset_of_targetColor_5() { return static_cast<int32_t>(offsetof(VRTK_ScreenFade_t2079719832, ___targetColor_5)); }
	inline Color_t2020392075  get_targetColor_5() const { return ___targetColor_5; }
	inline Color_t2020392075 * get_address_of_targetColor_5() { return &___targetColor_5; }
	inline void set_targetColor_5(Color_t2020392075  value)
	{
		___targetColor_5 = value;
	}

	inline static int32_t get_offset_of_deltaColor_6() { return static_cast<int32_t>(offsetof(VRTK_ScreenFade_t2079719832, ___deltaColor_6)); }
	inline Color_t2020392075  get_deltaColor_6() const { return ___deltaColor_6; }
	inline Color_t2020392075 * get_address_of_deltaColor_6() { return &___deltaColor_6; }
	inline void set_deltaColor_6(Color_t2020392075  value)
	{
		___deltaColor_6 = value;
	}
};

struct VRTK_ScreenFade_t2079719832_StaticFields
{
public:
	// VRTK.VRTK_ScreenFade VRTK.VRTK_ScreenFade::instance
	VRTK_ScreenFade_t2079719832 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(VRTK_ScreenFade_t2079719832_StaticFields, ___instance_2)); }
	inline VRTK_ScreenFade_t2079719832 * get_instance_2() const { return ___instance_2; }
	inline VRTK_ScreenFade_t2079719832 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(VRTK_ScreenFade_t2079719832 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
