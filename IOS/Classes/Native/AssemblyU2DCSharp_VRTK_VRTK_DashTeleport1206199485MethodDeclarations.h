﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_DashTeleport
struct VRTK_DashTeleport_t1206199485;
// VRTK.DashTeleportEventHandler
struct DashTeleportEventHandler_t1179650517;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1214023521;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_DashTeleportEventHandler1179650517.h"
#include "AssemblyU2DCSharp_VRTK_DashTeleportEventArgs2197253242.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void VRTK.VRTK_DashTeleport::.ctor()
extern "C"  void VRTK_DashTeleport__ctor_m447456377 (VRTK_DashTeleport_t1206199485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DashTeleport::add_WillDashThruObjects(VRTK.DashTeleportEventHandler)
extern "C"  void VRTK_DashTeleport_add_WillDashThruObjects_m3510740282 (VRTK_DashTeleport_t1206199485 * __this, DashTeleportEventHandler_t1179650517 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DashTeleport::remove_WillDashThruObjects(VRTK.DashTeleportEventHandler)
extern "C"  void VRTK_DashTeleport_remove_WillDashThruObjects_m2949544059 (VRTK_DashTeleport_t1206199485 * __this, DashTeleportEventHandler_t1179650517 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DashTeleport::add_DashedThruObjects(VRTK.DashTeleportEventHandler)
extern "C"  void VRTK_DashTeleport_add_DashedThruObjects_m1650977031 (VRTK_DashTeleport_t1206199485 * __this, DashTeleportEventHandler_t1179650517 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DashTeleport::remove_DashedThruObjects(VRTK.DashTeleportEventHandler)
extern "C"  void VRTK_DashTeleport_remove_DashedThruObjects_m3190680850 (VRTK_DashTeleport_t1206199485 * __this, DashTeleportEventHandler_t1179650517 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DashTeleport::OnWillDashThruObjects(VRTK.DashTeleportEventArgs)
extern "C"  void VRTK_DashTeleport_OnWillDashThruObjects_m3088892044 (VRTK_DashTeleport_t1206199485 * __this, DashTeleportEventArgs_t2197253242  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DashTeleport::OnDashedThruObjects(VRTK.DashTeleportEventArgs)
extern "C"  void VRTK_DashTeleport_OnDashedThruObjects_m1366226587 (VRTK_DashTeleport_t1206199485 * __this, DashTeleportEventArgs_t2197253242  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DashTeleport::Awake()
extern "C"  void VRTK_DashTeleport_Awake_m2332609174 (VRTK_DashTeleport_t1206199485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DashTeleport::SetNewPosition(UnityEngine.Vector3,UnityEngine.Transform,System.Boolean)
extern "C"  void VRTK_DashTeleport_SetNewPosition_m2005189783 (VRTK_DashTeleport_t1206199485 * __this, Vector3_t2243707580  ___position0, Transform_t3275118058 * ___target1, bool ___forceDestinationPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_DashTeleport::lerpToPosition(UnityEngine.Vector3,UnityEngine.Transform)
extern "C"  Il2CppObject * VRTK_DashTeleport_lerpToPosition_m2012119984 (VRTK_DashTeleport_t1206199485 * __this, Vector3_t2243707580  ___targetPosition0, Transform_t3275118058 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.DashTeleportEventArgs VRTK.VRTK_DashTeleport::SetDashTeleportEvent(UnityEngine.RaycastHit[])
extern "C"  DashTeleportEventArgs_t2197253242  VRTK_DashTeleport_SetDashTeleportEvent_m1312613069 (VRTK_DashTeleport_t1206199485 * __this, RaycastHitU5BU5D_t1214023521* ___hits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
