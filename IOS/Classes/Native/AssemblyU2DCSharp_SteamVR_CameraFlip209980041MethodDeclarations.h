﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_CameraFlip
struct SteamVR_CameraFlip_t209980041;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_CameraFlip::.ctor()
extern "C"  void SteamVR_CameraFlip__ctor_m3462465976 (SteamVR_CameraFlip_t209980041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_CameraFlip::Awake()
extern "C"  void SteamVR_CameraFlip_Awake_m4095829237 (SteamVR_CameraFlip_t209980041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
