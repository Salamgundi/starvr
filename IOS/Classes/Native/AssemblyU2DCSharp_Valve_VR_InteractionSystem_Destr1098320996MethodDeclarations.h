﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.DestroyOnParticleSystemDeath
struct DestroyOnParticleSystemDeath_t1098320996;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.DestroyOnParticleSystemDeath::.ctor()
extern "C"  void DestroyOnParticleSystemDeath__ctor_m3125595204 (DestroyOnParticleSystemDeath_t1098320996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.DestroyOnParticleSystemDeath::Awake()
extern "C"  void DestroyOnParticleSystemDeath_Awake_m1789927813 (DestroyOnParticleSystemDeath_t1098320996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.DestroyOnParticleSystemDeath::CheckParticleSystem()
extern "C"  void DestroyOnParticleSystemDeath_CheckParticleSystem_m3031768303 (DestroyOnParticleSystemDeath_t1098320996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
