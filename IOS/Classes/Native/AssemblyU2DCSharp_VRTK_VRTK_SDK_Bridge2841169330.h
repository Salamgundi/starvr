﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.SDK_BaseSystem
struct SDK_BaseSystem_t244469351;
// VRTK.SDK_BaseHeadset
struct SDK_BaseHeadset_t3046873914;
// VRTK.SDK_BaseController
struct SDK_BaseController_t197168236;
// VRTK.SDK_BaseBoundaries
struct SDK_BaseBoundaries_t1766380066;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_SDK_Bridge
struct  VRTK_SDK_Bridge_t2841169330  : public Il2CppObject
{
public:

public:
};

struct VRTK_SDK_Bridge_t2841169330_StaticFields
{
public:
	// VRTK.SDK_BaseSystem VRTK.VRTK_SDK_Bridge::systemSDK
	SDK_BaseSystem_t244469351 * ___systemSDK_0;
	// VRTK.SDK_BaseHeadset VRTK.VRTK_SDK_Bridge::headsetSDK
	SDK_BaseHeadset_t3046873914 * ___headsetSDK_1;
	// VRTK.SDK_BaseController VRTK.VRTK_SDK_Bridge::controllerSDK
	SDK_BaseController_t197168236 * ___controllerSDK_2;
	// VRTK.SDK_BaseBoundaries VRTK.VRTK_SDK_Bridge::boundariesSDK
	SDK_BaseBoundaries_t1766380066 * ___boundariesSDK_3;

public:
	inline static int32_t get_offset_of_systemSDK_0() { return static_cast<int32_t>(offsetof(VRTK_SDK_Bridge_t2841169330_StaticFields, ___systemSDK_0)); }
	inline SDK_BaseSystem_t244469351 * get_systemSDK_0() const { return ___systemSDK_0; }
	inline SDK_BaseSystem_t244469351 ** get_address_of_systemSDK_0() { return &___systemSDK_0; }
	inline void set_systemSDK_0(SDK_BaseSystem_t244469351 * value)
	{
		___systemSDK_0 = value;
		Il2CppCodeGenWriteBarrier(&___systemSDK_0, value);
	}

	inline static int32_t get_offset_of_headsetSDK_1() { return static_cast<int32_t>(offsetof(VRTK_SDK_Bridge_t2841169330_StaticFields, ___headsetSDK_1)); }
	inline SDK_BaseHeadset_t3046873914 * get_headsetSDK_1() const { return ___headsetSDK_1; }
	inline SDK_BaseHeadset_t3046873914 ** get_address_of_headsetSDK_1() { return &___headsetSDK_1; }
	inline void set_headsetSDK_1(SDK_BaseHeadset_t3046873914 * value)
	{
		___headsetSDK_1 = value;
		Il2CppCodeGenWriteBarrier(&___headsetSDK_1, value);
	}

	inline static int32_t get_offset_of_controllerSDK_2() { return static_cast<int32_t>(offsetof(VRTK_SDK_Bridge_t2841169330_StaticFields, ___controllerSDK_2)); }
	inline SDK_BaseController_t197168236 * get_controllerSDK_2() const { return ___controllerSDK_2; }
	inline SDK_BaseController_t197168236 ** get_address_of_controllerSDK_2() { return &___controllerSDK_2; }
	inline void set_controllerSDK_2(SDK_BaseController_t197168236 * value)
	{
		___controllerSDK_2 = value;
		Il2CppCodeGenWriteBarrier(&___controllerSDK_2, value);
	}

	inline static int32_t get_offset_of_boundariesSDK_3() { return static_cast<int32_t>(offsetof(VRTK_SDK_Bridge_t2841169330_StaticFields, ___boundariesSDK_3)); }
	inline SDK_BaseBoundaries_t1766380066 * get_boundariesSDK_3() const { return ___boundariesSDK_3; }
	inline SDK_BaseBoundaries_t1766380066 ** get_address_of_boundariesSDK_3() { return &___boundariesSDK_3; }
	inline void set_boundariesSDK_3(SDK_BaseBoundaries_t1766380066 * value)
	{
		___boundariesSDK_3 = value;
		Il2CppCodeGenWriteBarrier(&___boundariesSDK_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
