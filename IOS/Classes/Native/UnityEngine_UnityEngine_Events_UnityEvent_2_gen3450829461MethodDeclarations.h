﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,VRTK.InteractableObjectEventArgs>
struct UnityEvent_2_t3450829461;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.InteractableObjectEventArgs>
struct UnityAction_2_t1568631543;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.InteractableObjectEventArgs>::.ctor()
extern "C"  void UnityEvent_2__ctor_m3429598455_gshared (UnityEvent_2_t3450829461 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m3429598455(__this, method) ((  void (*) (UnityEvent_2_t3450829461 *, const MethodInfo*))UnityEvent_2__ctor_m3429598455_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.InteractableObjectEventArgs>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m2930382495_gshared (UnityEvent_2_t3450829461 * __this, UnityAction_2_t1568631543 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m2930382495(__this, ___call0, method) ((  void (*) (UnityEvent_2_t3450829461 *, UnityAction_2_t1568631543 *, const MethodInfo*))UnityEvent_2_AddListener_m2930382495_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.InteractableObjectEventArgs>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m3705112888_gshared (UnityEvent_2_t3450829461 * __this, UnityAction_2_t1568631543 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m3705112888(__this, ___call0, method) ((  void (*) (UnityEvent_2_t3450829461 *, UnityAction_2_t1568631543 *, const MethodInfo*))UnityEvent_2_RemoveListener_m3705112888_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,VRTK.InteractableObjectEventArgs>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m1003662411_gshared (UnityEvent_2_t3450829461 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m1003662411(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t3450829461 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m1003662411_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.InteractableObjectEventArgs>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m4283089731_gshared (UnityEvent_2_t3450829461 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m4283089731(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t3450829461 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m4283089731_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.InteractableObjectEventArgs>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m3749139450_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t1568631543 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m3749139450(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t1568631543 *, const MethodInfo*))UnityEvent_2_GetDelegate_m3749139450_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.InteractableObjectEventArgs>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m268314530_gshared (UnityEvent_2_t3450829461 * __this, Il2CppObject * ___arg00, InteractableObjectEventArgs_t473175556  ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m268314530(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t3450829461 *, Il2CppObject *, InteractableObjectEventArgs_t473175556 , const MethodInfo*))UnityEvent_2_Invoke_m268314530_gshared)(__this, ___arg00, ___arg11, method)
