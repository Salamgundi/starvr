﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Archery.Follow
struct  Follow_t66488297  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.Examples.Archery.Follow::followPosition
	bool ___followPosition_2;
	// System.Boolean VRTK.Examples.Archery.Follow::followRotation
	bool ___followRotation_3;
	// UnityEngine.Transform VRTK.Examples.Archery.Follow::target
	Transform_t3275118058 * ___target_4;

public:
	inline static int32_t get_offset_of_followPosition_2() { return static_cast<int32_t>(offsetof(Follow_t66488297, ___followPosition_2)); }
	inline bool get_followPosition_2() const { return ___followPosition_2; }
	inline bool* get_address_of_followPosition_2() { return &___followPosition_2; }
	inline void set_followPosition_2(bool value)
	{
		___followPosition_2 = value;
	}

	inline static int32_t get_offset_of_followRotation_3() { return static_cast<int32_t>(offsetof(Follow_t66488297, ___followRotation_3)); }
	inline bool get_followRotation_3() const { return ___followRotation_3; }
	inline bool* get_address_of_followRotation_3() { return &___followRotation_3; }
	inline void set_followRotation_3(bool value)
	{
		___followRotation_3 = value;
	}

	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(Follow_t66488297, ___target_4)); }
	inline Transform_t3275118058 * get_target_4() const { return ___target_4; }
	inline Transform_t3275118058 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3275118058 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier(&___target_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
