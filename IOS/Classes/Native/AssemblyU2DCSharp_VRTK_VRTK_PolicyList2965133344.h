﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PolicyList_OperationTy2997627994.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PolicyList_CheckTypes3385731591.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_PolicyList
struct  VRTK_PolicyList_t2965133344  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_PolicyList/OperationTypes VRTK.VRTK_PolicyList::operation
	int32_t ___operation_2;
	// VRTK.VRTK_PolicyList/CheckTypes VRTK.VRTK_PolicyList::checkType
	int32_t ___checkType_3;
	// System.Collections.Generic.List`1<System.String> VRTK.VRTK_PolicyList::identifiers
	List_1_t1398341365 * ___identifiers_4;

public:
	inline static int32_t get_offset_of_operation_2() { return static_cast<int32_t>(offsetof(VRTK_PolicyList_t2965133344, ___operation_2)); }
	inline int32_t get_operation_2() const { return ___operation_2; }
	inline int32_t* get_address_of_operation_2() { return &___operation_2; }
	inline void set_operation_2(int32_t value)
	{
		___operation_2 = value;
	}

	inline static int32_t get_offset_of_checkType_3() { return static_cast<int32_t>(offsetof(VRTK_PolicyList_t2965133344, ___checkType_3)); }
	inline int32_t get_checkType_3() const { return ___checkType_3; }
	inline int32_t* get_address_of_checkType_3() { return &___checkType_3; }
	inline void set_checkType_3(int32_t value)
	{
		___checkType_3 = value;
	}

	inline static int32_t get_offset_of_identifiers_4() { return static_cast<int32_t>(offsetof(VRTK_PolicyList_t2965133344, ___identifiers_4)); }
	inline List_1_t1398341365 * get_identifiers_4() const { return ___identifiers_4; }
	inline List_1_t1398341365 ** get_address_of_identifiers_4() { return &___identifiers_4; }
	inline void set_identifiers_4(List_1_t1398341365 * value)
	{
		___identifiers_4 = value;
		Il2CppCodeGenWriteBarrier(&___identifiers_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
