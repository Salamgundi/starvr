﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SteamVR_LaserPointer3427343737.h"
#include "AssemblyU2DCSharp_SteamVR_Teleporter4248086861.h"
#include "AssemblyU2DCSharp_SteamVR_Teleporter_TeleportType3646909803.h"
#include "AssemblyU2DCSharp_SteamVR_TestThrow3699093615.h"
#include "AssemblyU2DCSharp_SteamVR_TestTrackedCamera2838176734.h"
#include "AssemblyU2DCSharp_ClickedEventArgs2917034410.h"
#include "AssemblyU2DCSharp_ClickedEventHandler1112331409.h"
#include "AssemblyU2DCSharp_SteamVR_TrackedController3050739949.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_BodyC3516213392.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Circu2658775813.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Circu2696941949.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Circu1618561021.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Compl3339060882.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Compl1851484950.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Controll2644472.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Custo3370079802.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Custo3482038085.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Custom749092352.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Debug3938194035.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Destro585395198.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Destr1098320996.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Destro740445139.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Dista3909819367.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Dista3353154814.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_DontDe814827202.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_EnumF3494685482.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Fallb1563223449.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1719631858.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1543711741.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1387717936.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1126108605.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_2296478156.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_2275351047.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hapti1520216690.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_HideO1605290352.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Ignore476670048.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Input1394580110.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Inter1274046986.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Inter3380341848.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Inter2257352405.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Inter1568724519.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Inter1346270787.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_ItemP3423754743.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_ItemP4052302202.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_ItemP1778794894.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_ItemP1471310371.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Linea3384090715.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Linea3243281208.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Linea1253673161.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Linear393683001.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Linea2578409520.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Linea1068763581.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Linear810676855.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_PlaySo165629647.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Playe4256718089.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_SeeTh2977129686.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Sleep3660103141.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_SoundDe50869408.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Sound1703214483.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_SpawnA115944288.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Spawn1775530049.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Spawn1216576504.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Throw3270371398.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Throwa344522070.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_UIEle1909343576.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Unpar1371103399.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Util1939151698.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Util_4183039461.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_AfterT442788763.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_After1674543291.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Veloc1153298725.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Veloci241413089.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Contr4025449936.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Contr3822090287.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Contro106135473.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Contro185367873.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Contr1805603699.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Arche3389223157.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Arche3537536025.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Arrow2932383743.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_ArrowH565341420.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Arrow4054708145.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Arrow4253007731.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Ballo3376655393.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Ballo1350152013.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Ballo2580220890.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Ballo3404570198.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Ballo4219926993.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Explos605436430.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_FireSo179112773.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Longb2607500110.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Longbo840502957.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Longb3691052025.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_SoundBo88828517.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Contr4074302606.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Contr3802738335.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Contr1955400369.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Inter1462204982.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_AllowT416616227.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (SteamVR_LaserPointer_t3427343737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[11] = 
{
	SteamVR_LaserPointer_t3427343737::get_offset_of_active_2(),
	SteamVR_LaserPointer_t3427343737::get_offset_of_color_3(),
	SteamVR_LaserPointer_t3427343737::get_offset_of_thickness_4(),
	SteamVR_LaserPointer_t3427343737::get_offset_of_holder_5(),
	SteamVR_LaserPointer_t3427343737::get_offset_of_pointer_6(),
	SteamVR_LaserPointer_t3427343737::get_offset_of_isActive_7(),
	SteamVR_LaserPointer_t3427343737::get_offset_of_addRigidBody_8(),
	SteamVR_LaserPointer_t3427343737::get_offset_of_reference_9(),
	SteamVR_LaserPointer_t3427343737::get_offset_of_PointerIn_10(),
	SteamVR_LaserPointer_t3427343737::get_offset_of_PointerOut_11(),
	SteamVR_LaserPointer_t3427343737::get_offset_of_previousContact_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (SteamVR_Teleporter_t4248086861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[2] = 
{
	SteamVR_Teleporter_t4248086861::get_offset_of_teleportOnClick_2(),
	SteamVR_Teleporter_t4248086861::get_offset_of_teleportType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (TeleportType_t3646909803)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2102[4] = 
{
	TeleportType_t3646909803::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (SteamVR_TestThrow_t3699093615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[4] = 
{
	SteamVR_TestThrow_t3699093615::get_offset_of_prefab_2(),
	SteamVR_TestThrow_t3699093615::get_offset_of_attachPoint_3(),
	SteamVR_TestThrow_t3699093615::get_offset_of_trackedObj_4(),
	SteamVR_TestThrow_t3699093615::get_offset_of_joint_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (SteamVR_TestTrackedCamera_t2838176734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[4] = 
{
	SteamVR_TestTrackedCamera_t2838176734::get_offset_of_material_2(),
	SteamVR_TestTrackedCamera_t2838176734::get_offset_of_target_3(),
	SteamVR_TestTrackedCamera_t2838176734::get_offset_of_undistorted_4(),
	SteamVR_TestTrackedCamera_t2838176734::get_offset_of_cropped_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (ClickedEventArgs_t2917034410)+ sizeof (Il2CppObject), sizeof(ClickedEventArgs_t2917034410 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2105[4] = 
{
	ClickedEventArgs_t2917034410::get_offset_of_controllerIndex_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClickedEventArgs_t2917034410::get_offset_of_flags_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClickedEventArgs_t2917034410::get_offset_of_padX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClickedEventArgs_t2917034410::get_offset_of_padY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (ClickedEventHandler_t1112331409), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (SteamVR_TrackedController_t3050739949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[19] = 
{
	SteamVR_TrackedController_t3050739949::get_offset_of_controllerIndex_2(),
	SteamVR_TrackedController_t3050739949::get_offset_of_controllerState_3(),
	SteamVR_TrackedController_t3050739949::get_offset_of_triggerPressed_4(),
	SteamVR_TrackedController_t3050739949::get_offset_of_steamPressed_5(),
	SteamVR_TrackedController_t3050739949::get_offset_of_menuPressed_6(),
	SteamVR_TrackedController_t3050739949::get_offset_of_padPressed_7(),
	SteamVR_TrackedController_t3050739949::get_offset_of_padTouched_8(),
	SteamVR_TrackedController_t3050739949::get_offset_of_gripped_9(),
	SteamVR_TrackedController_t3050739949::get_offset_of_MenuButtonClicked_10(),
	SteamVR_TrackedController_t3050739949::get_offset_of_MenuButtonUnclicked_11(),
	SteamVR_TrackedController_t3050739949::get_offset_of_TriggerClicked_12(),
	SteamVR_TrackedController_t3050739949::get_offset_of_TriggerUnclicked_13(),
	SteamVR_TrackedController_t3050739949::get_offset_of_SteamClicked_14(),
	SteamVR_TrackedController_t3050739949::get_offset_of_PadClicked_15(),
	SteamVR_TrackedController_t3050739949::get_offset_of_PadUnclicked_16(),
	SteamVR_TrackedController_t3050739949::get_offset_of_PadTouched_17(),
	SteamVR_TrackedController_t3050739949::get_offset_of_PadUntouched_18(),
	SteamVR_TrackedController_t3050739949::get_offset_of_Gripped_19(),
	SteamVR_TrackedController_t3050739949::get_offset_of_Ungripped_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (BodyCollider_t3516213392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[2] = 
{
	BodyCollider_t3516213392::get_offset_of_head_2(),
	BodyCollider_t3516213392::get_offset_of_capsuleCollider_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (CircularDrive_t2658775813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[38] = 
{
	CircularDrive_t2658775813::get_offset_of_axisOfRotation_2(),
	CircularDrive_t2658775813::get_offset_of_childCollider_3(),
	CircularDrive_t2658775813::get_offset_of_linearMapping_4(),
	CircularDrive_t2658775813::get_offset_of_hoverLock_5(),
	CircularDrive_t2658775813::get_offset_of_limited_6(),
	CircularDrive_t2658775813::get_offset_of_frozenDistanceMinMaxThreshold_7(),
	CircularDrive_t2658775813::get_offset_of_onFrozenDistanceThreshold_8(),
	CircularDrive_t2658775813::get_offset_of_minAngle_9(),
	CircularDrive_t2658775813::get_offset_of_freezeOnMin_10(),
	CircularDrive_t2658775813::get_offset_of_onMinAngle_11(),
	CircularDrive_t2658775813::get_offset_of_maxAngle_12(),
	CircularDrive_t2658775813::get_offset_of_freezeOnMax_13(),
	CircularDrive_t2658775813::get_offset_of_onMaxAngle_14(),
	CircularDrive_t2658775813::get_offset_of_forceStart_15(),
	CircularDrive_t2658775813::get_offset_of_startAngle_16(),
	CircularDrive_t2658775813::get_offset_of_rotateGameObject_17(),
	CircularDrive_t2658775813::get_offset_of_debugPath_18(),
	CircularDrive_t2658775813::get_offset_of_dbgPathLimit_19(),
	CircularDrive_t2658775813::get_offset_of_debugText_20(),
	CircularDrive_t2658775813::get_offset_of_outAngle_21(),
	CircularDrive_t2658775813::get_offset_of_start_22(),
	CircularDrive_t2658775813::get_offset_of_worldPlaneNormal_23(),
	CircularDrive_t2658775813::get_offset_of_localPlaneNormal_24(),
	CircularDrive_t2658775813::get_offset_of_lastHandProjected_25(),
	CircularDrive_t2658775813::get_offset_of_red_26(),
	CircularDrive_t2658775813::get_offset_of_green_27(),
	CircularDrive_t2658775813::get_offset_of_dbgHandObjects_28(),
	CircularDrive_t2658775813::get_offset_of_dbgProjObjects_29(),
	CircularDrive_t2658775813::get_offset_of_dbgObjectsParent_30(),
	CircularDrive_t2658775813::get_offset_of_dbgObjectCount_31(),
	CircularDrive_t2658775813::get_offset_of_dbgObjectIndex_32(),
	CircularDrive_t2658775813::get_offset_of_driving_33(),
	CircularDrive_t2658775813::get_offset_of_minMaxAngularThreshold_34(),
	CircularDrive_t2658775813::get_offset_of_frozen_35(),
	CircularDrive_t2658775813::get_offset_of_frozenAngle_36(),
	CircularDrive_t2658775813::get_offset_of_frozenHandWorldPos_37(),
	CircularDrive_t2658775813::get_offset_of_frozenSqDistanceMinMaxThreshold_38(),
	CircularDrive_t2658775813::get_offset_of_handHoverLocked_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (Axis_t_t2696941949)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2110[4] = 
{
	Axis_t_t2696941949::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (U3CHapticPulsesU3Ec__Iterator0_t1618561021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2111[9] = 
{
	U3CHapticPulsesU3Ec__Iterator0_t1618561021::get_offset_of_controller_0(),
	U3CHapticPulsesU3Ec__Iterator0_t1618561021::get_offset_of_flMagnitude_1(),
	U3CHapticPulsesU3Ec__Iterator0_t1618561021::get_offset_of_U3CnRangeMaxU3E__0_2(),
	U3CHapticPulsesU3Ec__Iterator0_t1618561021::get_offset_of_nCount_3(),
	U3CHapticPulsesU3Ec__Iterator0_t1618561021::get_offset_of_U3CiU3E__1_4(),
	U3CHapticPulsesU3Ec__Iterator0_t1618561021::get_offset_of_U3CdurationU3E__2_5(),
	U3CHapticPulsesU3Ec__Iterator0_t1618561021::get_offset_of_U24current_6(),
	U3CHapticPulsesU3Ec__Iterator0_t1618561021::get_offset_of_U24disposing_7(),
	U3CHapticPulsesU3Ec__Iterator0_t1618561021::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (ComplexThrowable_t3339060882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[8] = 
{
	ComplexThrowable_t3339060882::get_offset_of_attachForce_2(),
	ComplexThrowable_t3339060882::get_offset_of_attachForceDamper_3(),
	ComplexThrowable_t3339060882::get_offset_of_attachMode_4(),
	ComplexThrowable_t3339060882::get_offset_of_attachmentFlags_5(),
	ComplexThrowable_t3339060882::get_offset_of_holdingHands_6(),
	ComplexThrowable_t3339060882::get_offset_of_holdingBodies_7(),
	ComplexThrowable_t3339060882::get_offset_of_holdingPoints_8(),
	ComplexThrowable_t3339060882::get_offset_of_rigidBodies_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (AttachMode_t1851484950)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2113[3] = 
{
	AttachMode_t1851484950::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (ControllerHoverHighlight_t2644472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[8] = 
{
	ControllerHoverHighlight_t2644472::get_offset_of_highLightMaterial_2(),
	ControllerHoverHighlight_t2644472::get_offset_of_fireHapticsOnHightlight_3(),
	ControllerHoverHighlight_t2644472::get_offset_of_hand_4(),
	ControllerHoverHighlight_t2644472::get_offset_of_bodyMeshRenderer_5(),
	ControllerHoverHighlight_t2644472::get_offset_of_trackingHatMeshRenderer_6(),
	ControllerHoverHighlight_t2644472::get_offset_of_renderModel_7(),
	ControllerHoverHighlight_t2644472::get_offset_of_renderModelLoaded_8(),
	ControllerHoverHighlight_t2644472::get_offset_of_renderModelLoadedAction_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (CustomEvents_t3370079802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (UnityEventSingleFloat_t3482038085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (UnityEventHand_t749092352), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (DebugUI_t3938194035), -1, sizeof(DebugUI_t3938194035_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2118[2] = 
{
	DebugUI_t3938194035::get_offset_of_player_2(),
	DebugUI_t3938194035_StaticFields::get_offset_of__instance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (DestroyOnDetachedFromHand_t585395198), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (DestroyOnParticleSystemDeath_t1098320996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[1] = 
{
	DestroyOnParticleSystemDeath_t1098320996::get_offset_of_particles_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (DestroyOnTriggerEnter_t740445139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[2] = 
{
	DestroyOnTriggerEnter_t740445139::get_offset_of_tagFilter_2(),
	DestroyOnTriggerEnter_t740445139::get_offset_of_useTag_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (DistanceHaptics_t3909819367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[4] = 
{
	DistanceHaptics_t3909819367::get_offset_of_firstTransform_2(),
	DistanceHaptics_t3909819367::get_offset_of_secondTransform_3(),
	DistanceHaptics_t3909819367::get_offset_of_distanceIntensityCurve_4(),
	DistanceHaptics_t3909819367::get_offset_of_pulseIntervalCurve_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (U3CStartU3Ec__Iterator0_t3353154814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[7] = 
{
	U3CStartU3Ec__Iterator0_t3353154814::get_offset_of_U3CdistanceU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3353154814::get_offset_of_U3CtrackedObjectU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3353154814::get_offset_of_U3CnextPulseU3E__2_2(),
	U3CStartU3Ec__Iterator0_t3353154814::get_offset_of_U24this_3(),
	U3CStartU3Ec__Iterator0_t3353154814::get_offset_of_U24current_4(),
	U3CStartU3Ec__Iterator0_t3353154814::get_offset_of_U24disposing_5(),
	U3CStartU3Ec__Iterator0_t3353154814::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (DontDestroyOnLoad_t814827202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (EnumFlags_t3494685482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (FallbackCameraController_t1563223449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[6] = 
{
	FallbackCameraController_t1563223449::get_offset_of_speed_2(),
	FallbackCameraController_t1563223449::get_offset_of_shiftSpeed_3(),
	FallbackCameraController_t1563223449::get_offset_of_showInstructions_4(),
	FallbackCameraController_t1563223449::get_offset_of_startEulerAngles_5(),
	FallbackCameraController_t1563223449::get_offset_of_startMousePosition_6(),
	FallbackCameraController_t1563223449::get_offset_of_realTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (Hand_t379716353), -1, sizeof(Hand_t379716353_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2127[27] = 
{
	0,
	Hand_t379716353::get_offset_of_otherHand_3(),
	Hand_t379716353::get_offset_of_startingHandType_4(),
	Hand_t379716353::get_offset_of_hoverSphereTransform_5(),
	Hand_t379716353::get_offset_of_hoverSphereRadius_6(),
	Hand_t379716353::get_offset_of_hoverLayerMask_7(),
	Hand_t379716353::get_offset_of_hoverUpdateInterval_8(),
	Hand_t379716353::get_offset_of_noSteamVRFallbackCamera_9(),
	Hand_t379716353::get_offset_of_noSteamVRFallbackMaxDistanceNoItem_10(),
	Hand_t379716353::get_offset_of_noSteamVRFallbackMaxDistanceWithItem_11(),
	Hand_t379716353::get_offset_of_noSteamVRFallbackInteractorDistance_12(),
	Hand_t379716353::get_offset_of_controller_13(),
	Hand_t379716353::get_offset_of_controllerPrefab_14(),
	Hand_t379716353::get_offset_of_controllerObject_15(),
	Hand_t379716353::get_offset_of_showDebugText_16(),
	Hand_t379716353::get_offset_of_spewDebugText_17(),
	Hand_t379716353::get_offset_of_attachedObjects_18(),
	Hand_t379716353::get_offset_of_U3ChoverLockedU3Ek__BackingField_19(),
	Hand_t379716353::get_offset_of__hoveringInteractable_20(),
	Hand_t379716353::get_offset_of_debugText_21(),
	Hand_t379716353::get_offset_of_prevOverlappingColliders_22(),
	0,
	Hand_t379716353::get_offset_of_overlappingColliders_24(),
	Hand_t379716353::get_offset_of_playerInstance_25(),
	Hand_t379716353::get_offset_of_applicationLostFocusObject_26(),
	Hand_t379716353::get_offset_of_inputFocusAction_27(),
	Hand_t379716353_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (HandType_t1719631858)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2128[4] = 
{
	HandType_t1719631858::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (AttachmentFlags_t1543711741)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2129[5] = 
{
	AttachmentFlags_t1543711741::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (AttachedObject_t1387717936)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[3] = 
{
	AttachedObject_t1387717936::get_offset_of_attachedObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AttachedObject_t1387717936::get_offset_of_originalParent_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AttachedObject_t1387717936::get_offset_of_isParentedToHand_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (U3CDetachObjectU3Ec__AnonStorey1_t1126108605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[1] = 
{
	U3CDetachObjectU3Ec__AnonStorey1_t1126108605::get_offset_of_objectToDetach_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (U3CStartU3Ec__Iterator0_t2296478156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[4] = 
{
	U3CStartU3Ec__Iterator0_t2296478156::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t2296478156::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t2296478156::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t2296478156::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (U3CUpdateHoveringU3Ec__AnonStorey2_t2275351047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[1] = 
{
	U3CUpdateHoveringU3Ec__AnonStorey2_t2275351047::get_offset_of_contacting_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (HapticRack_t1520216690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[7] = 
{
	HapticRack_t1520216690::get_offset_of_linearMapping_2(),
	HapticRack_t1520216690::get_offset_of_teethCount_3(),
	HapticRack_t1520216690::get_offset_of_minimumPulseDuration_4(),
	HapticRack_t1520216690::get_offset_of_maximumPulseDuration_5(),
	HapticRack_t1520216690::get_offset_of_onPulse_6(),
	HapticRack_t1520216690::get_offset_of_hand_7(),
	HapticRack_t1520216690::get_offset_of_previousToothIndex_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (HideOnHandFocusLost_t1605290352), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (IgnoreHovering_t476670048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[1] = 
{
	IgnoreHovering_t476670048::get_offset_of_onlyIgnoreHand_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (InputModule_t1394580110), -1, sizeof(InputModule_t1394580110_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2137[2] = 
{
	InputModule_t1394580110::get_offset_of_submitObject_8(),
	InputModule_t1394580110_StaticFields::get_offset_of__instance_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (Interactable_t1274046986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[2] = 
{
	Interactable_t1274046986::get_offset_of_onAttachedToHand_2(),
	Interactable_t1274046986::get_offset_of_onDetachedFromHand_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (OnAttachedToHandDelegate_t3380341848), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (OnDetachedFromHandDelegate_t2257352405), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (InteractableButtonEvents_t1568724519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[8] = 
{
	InteractableButtonEvents_t1568724519::get_offset_of_onTriggerDown_2(),
	InteractableButtonEvents_t1568724519::get_offset_of_onTriggerUp_3(),
	InteractableButtonEvents_t1568724519::get_offset_of_onGripDown_4(),
	InteractableButtonEvents_t1568724519::get_offset_of_onGripUp_5(),
	InteractableButtonEvents_t1568724519::get_offset_of_onTouchpadDown_6(),
	InteractableButtonEvents_t1568724519::get_offset_of_onTouchpadUp_7(),
	InteractableButtonEvents_t1568724519::get_offset_of_onTouchpadTouch_8(),
	InteractableButtonEvents_t1568724519::get_offset_of_onTouchpadRelease_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (InteractableHoverEvents_t1346270787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[4] = 
{
	InteractableHoverEvents_t1346270787::get_offset_of_onHandHoverBegin_2(),
	InteractableHoverEvents_t1346270787::get_offset_of_onHandHoverEnd_3(),
	InteractableHoverEvents_t1346270787::get_offset_of_onAttachedToHand_4(),
	InteractableHoverEvents_t1346270787::get_offset_of_onDetachedFromHand_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (ItemPackage_t3423754743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[6] = 
{
	ItemPackage_t3423754743::get_offset_of_name_2(),
	ItemPackage_t3423754743::get_offset_of_packageType_3(),
	ItemPackage_t3423754743::get_offset_of_itemPrefab_4(),
	ItemPackage_t3423754743::get_offset_of_otherHandItemPrefab_5(),
	ItemPackage_t3423754743::get_offset_of_previewPrefab_6(),
	ItemPackage_t3423754743::get_offset_of_fadedPreviewPrefab_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (ItemPackageType_t4052302202)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2144[4] = 
{
	ItemPackageType_t4052302202::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (ItemPackageReference_t1778794894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[1] = 
{
	ItemPackageReference_t1778794894::get_offset_of_itemPackage_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (ItemPackageSpawner_t1471310371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[16] = 
{
	ItemPackageSpawner_t1471310371::get_offset_of__itemPackage_2(),
	ItemPackageSpawner_t1471310371::get_offset_of_useItemPackagePreview_3(),
	ItemPackageSpawner_t1471310371::get_offset_of_useFadedPreview_4(),
	ItemPackageSpawner_t1471310371::get_offset_of_previewObject_5(),
	ItemPackageSpawner_t1471310371::get_offset_of_requireTriggerPressToTake_6(),
	ItemPackageSpawner_t1471310371::get_offset_of_requireTriggerPressToReturn_7(),
	ItemPackageSpawner_t1471310371::get_offset_of_showTriggerHint_8(),
	ItemPackageSpawner_t1471310371::get_offset_of_attachmentFlags_9(),
	ItemPackageSpawner_t1471310371::get_offset_of_attachmentPoint_10(),
	ItemPackageSpawner_t1471310371::get_offset_of_takeBackItem_11(),
	ItemPackageSpawner_t1471310371::get_offset_of_acceptDifferentItems_12(),
	ItemPackageSpawner_t1471310371::get_offset_of_spawnedItem_13(),
	ItemPackageSpawner_t1471310371::get_offset_of_itemIsSpawned_14(),
	ItemPackageSpawner_t1471310371::get_offset_of_pickupEvent_15(),
	ItemPackageSpawner_t1471310371::get_offset_of_dropEvent_16(),
	ItemPackageSpawner_t1471310371::get_offset_of_justPickedUpItem_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (LinearAnimation_t3384090715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[5] = 
{
	LinearAnimation_t3384090715::get_offset_of_linearMapping_2(),
	LinearAnimation_t3384090715::get_offset_of_animation_3(),
	LinearAnimation_t3384090715::get_offset_of_animState_4(),
	LinearAnimation_t3384090715::get_offset_of_animLength_5(),
	LinearAnimation_t3384090715::get_offset_of_lastValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (LinearAnimator_t3243281208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[4] = 
{
	LinearAnimator_t3243281208::get_offset_of_linearMapping_2(),
	LinearAnimator_t3243281208::get_offset_of_animator_3(),
	LinearAnimator_t3243281208::get_offset_of_currentLinearMapping_4(),
	LinearAnimator_t3243281208::get_offset_of_framesUnchanged_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (LinearAudioPitch_t1253673161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[6] = 
{
	LinearAudioPitch_t1253673161::get_offset_of_linearMapping_2(),
	LinearAudioPitch_t1253673161::get_offset_of_pitchCurve_3(),
	LinearAudioPitch_t1253673161::get_offset_of_minPitch_4(),
	LinearAudioPitch_t1253673161::get_offset_of_maxPitch_5(),
	LinearAudioPitch_t1253673161::get_offset_of_applyContinuously_6(),
	LinearAudioPitch_t1253673161::get_offset_of_audioSource_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (LinearBlendshape_t393683001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[3] = 
{
	LinearBlendshape_t393683001::get_offset_of_linearMapping_2(),
	LinearBlendshape_t393683001::get_offset_of_skinnedMesh_3(),
	LinearBlendshape_t393683001::get_offset_of_lastValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (LinearDisplacement_t2578409520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2151[3] = 
{
	LinearDisplacement_t2578409520::get_offset_of_displacement_2(),
	LinearDisplacement_t2578409520::get_offset_of_linearMapping_3(),
	LinearDisplacement_t2578409520::get_offset_of_initialPosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (LinearDrive_t1068763581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[12] = 
{
	LinearDrive_t1068763581::get_offset_of_startPosition_2(),
	LinearDrive_t1068763581::get_offset_of_endPosition_3(),
	LinearDrive_t1068763581::get_offset_of_linearMapping_4(),
	LinearDrive_t1068763581::get_offset_of_repositionGameObject_5(),
	LinearDrive_t1068763581::get_offset_of_maintainMomemntum_6(),
	LinearDrive_t1068763581::get_offset_of_momemtumDampenRate_7(),
	LinearDrive_t1068763581::get_offset_of_initialMappingOffset_8(),
	LinearDrive_t1068763581::get_offset_of_numMappingChangeSamples_9(),
	LinearDrive_t1068763581::get_offset_of_mappingChangeSamples_10(),
	LinearDrive_t1068763581::get_offset_of_prevMapping_11(),
	LinearDrive_t1068763581::get_offset_of_mappingChangeRate_12(),
	LinearDrive_t1068763581::get_offset_of_sampleCount_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (LinearMapping_t810676855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[1] = 
{
	LinearMapping_t810676855::get_offset_of_value_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (PlaySound_t165629647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[21] = 
{
	PlaySound_t165629647::get_offset_of_waveFile_2(),
	PlaySound_t165629647::get_offset_of_stopOnPlay_3(),
	PlaySound_t165629647::get_offset_of_disableOnEnd_4(),
	PlaySound_t165629647::get_offset_of_looping_5(),
	PlaySound_t165629647::get_offset_of_stopOnEnd_6(),
	PlaySound_t165629647::get_offset_of_playOnAwakeWithDelay_7(),
	PlaySound_t165629647::get_offset_of_useRandomVolume_8(),
	PlaySound_t165629647::get_offset_of_volMin_9(),
	PlaySound_t165629647::get_offset_of_volMax_10(),
	PlaySound_t165629647::get_offset_of_useRandomPitch_11(),
	PlaySound_t165629647::get_offset_of_pitchMin_12(),
	PlaySound_t165629647::get_offset_of_pitchMax_13(),
	PlaySound_t165629647::get_offset_of_useRetriggerTime_14(),
	PlaySound_t165629647::get_offset_of_timeInitial_15(),
	PlaySound_t165629647::get_offset_of_timeMin_16(),
	PlaySound_t165629647::get_offset_of_timeMax_17(),
	PlaySound_t165629647::get_offset_of_useRandomSilence_18(),
	PlaySound_t165629647::get_offset_of_percentToNotPlay_19(),
	PlaySound_t165629647::get_offset_of_delayOffsetTime_20(),
	PlaySound_t165629647::get_offset_of_audioSource_21(),
	PlaySound_t165629647::get_offset_of_clip_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (Player_t4256718089), -1, sizeof(Player_t4256718089_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2155[9] = 
{
	Player_t4256718089::get_offset_of_trackingOriginTransform_2(),
	Player_t4256718089::get_offset_of_hmdTransforms_3(),
	Player_t4256718089::get_offset_of_hands_4(),
	Player_t4256718089::get_offset_of_headCollider_5(),
	Player_t4256718089::get_offset_of_rigSteamVR_6(),
	Player_t4256718089::get_offset_of_rig2DFallback_7(),
	Player_t4256718089::get_offset_of_audioListener_8(),
	Player_t4256718089::get_offset_of_allowToggleTo2D_9(),
	Player_t4256718089_StaticFields::get_offset_of__instance_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (SeeThru_t2977129686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[5] = 
{
	SeeThru_t2977129686::get_offset_of_seeThruMaterial_2(),
	SeeThru_t2977129686::get_offset_of_seeThru_3(),
	SeeThru_t2977129686::get_offset_of_interactable_4(),
	SeeThru_t2977129686::get_offset_of_sourceRenderer_5(),
	SeeThru_t2977129686::get_offset_of_destRenderer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (SleepOnAwake_t3660103141), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (SoundDeparent_t50869408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[2] = 
{
	SoundDeparent_t50869408::get_offset_of_destroyAfterPlayOnce_2(),
	SoundDeparent_t50869408::get_offset_of_thisAudioSource_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (SoundPlayOneshot_t1703214483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2159[7] = 
{
	SoundPlayOneshot_t1703214483::get_offset_of_waveFiles_2(),
	SoundPlayOneshot_t1703214483::get_offset_of_thisAudioSource_3(),
	SoundPlayOneshot_t1703214483::get_offset_of_volMin_4(),
	SoundPlayOneshot_t1703214483::get_offset_of_volMax_5(),
	SoundPlayOneshot_t1703214483::get_offset_of_pitchMin_6(),
	SoundPlayOneshot_t1703214483::get_offset_of_pitchMax_7(),
	SoundPlayOneshot_t1703214483::get_offset_of_playOnAwake_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (SpawnAndAttachAfterControllerIsTracking_t115944288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2160[2] = 
{
	SpawnAndAttachAfterControllerIsTracking_t115944288::get_offset_of_hand_2(),
	SpawnAndAttachAfterControllerIsTracking_t115944288::get_offset_of_itemPrefab_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (SpawnAndAttachToHand_t1775530049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2161[2] = 
{
	SpawnAndAttachToHand_t1775530049::get_offset_of_hand_2(),
	SpawnAndAttachToHand_t1775530049::get_offset_of_prefab_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (SpawnRenderModel_t1216576504), -1, sizeof(SpawnRenderModel_t1216576504_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2162[8] = 
{
	SpawnRenderModel_t1216576504::get_offset_of_materials_2(),
	SpawnRenderModel_t1216576504::get_offset_of_renderModels_3(),
	SpawnRenderModel_t1216576504::get_offset_of_hand_4(),
	SpawnRenderModel_t1216576504::get_offset_of_renderers_5(),
	SpawnRenderModel_t1216576504_StaticFields::get_offset_of_spawnRenderModels_6(),
	SpawnRenderModel_t1216576504_StaticFields::get_offset_of_lastFrameUpdated_7(),
	SpawnRenderModel_t1216576504_StaticFields::get_offset_of_spawnRenderModelUpdateIndex_8(),
	SpawnRenderModel_t1216576504::get_offset_of_renderModelLoadedAction_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (Throwable_t3270371398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[17] = 
{
	Throwable_t3270371398::get_offset_of_attachmentFlags_2(),
	Throwable_t3270371398::get_offset_of_attachmentPoint_3(),
	Throwable_t3270371398::get_offset_of_catchSpeedThreshold_4(),
	Throwable_t3270371398::get_offset_of_restoreOriginalParent_5(),
	Throwable_t3270371398::get_offset_of_attachEaseIn_6(),
	Throwable_t3270371398::get_offset_of_snapAttachEaseInCurve_7(),
	Throwable_t3270371398::get_offset_of_snapAttachEaseInTime_8(),
	Throwable_t3270371398::get_offset_of_attachEaseInAttachmentNames_9(),
	Throwable_t3270371398::get_offset_of_velocityEstimator_10(),
	Throwable_t3270371398::get_offset_of_attached_11(),
	Throwable_t3270371398::get_offset_of_attachTime_12(),
	Throwable_t3270371398::get_offset_of_attachPosition_13(),
	Throwable_t3270371398::get_offset_of_attachRotation_14(),
	Throwable_t3270371398::get_offset_of_attachEaseInTransform_15(),
	Throwable_t3270371398::get_offset_of_onPickUp_16(),
	Throwable_t3270371398::get_offset_of_onDetachFromHand_17(),
	Throwable_t3270371398::get_offset_of_snapAttachEaseInCompleted_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (U3CLateDetachU3Ec__Iterator0_t344522070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[5] = 
{
	U3CLateDetachU3Ec__Iterator0_t344522070::get_offset_of_hand_0(),
	U3CLateDetachU3Ec__Iterator0_t344522070::get_offset_of_U24this_1(),
	U3CLateDetachU3Ec__Iterator0_t344522070::get_offset_of_U24current_2(),
	U3CLateDetachU3Ec__Iterator0_t344522070::get_offset_of_U24disposing_3(),
	U3CLateDetachU3Ec__Iterator0_t344522070::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (UIElement_t1909343576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[2] = 
{
	UIElement_t1909343576::get_offset_of_onHandClick_2(),
	UIElement_t1909343576::get_offset_of_currentHand_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (Unparent_t1371103399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[1] = 
{
	Unparent_t1371103399::get_offset_of_oldParent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (Util_t1939151698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (U3CWrapCoroutineU3Ec__Iterator0_t4183039461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[5] = 
{
	U3CWrapCoroutineU3Ec__Iterator0_t4183039461::get_offset_of_coroutine_0(),
	U3CWrapCoroutineU3Ec__Iterator0_t4183039461::get_offset_of_onCoroutineFinished_1(),
	U3CWrapCoroutineU3Ec__Iterator0_t4183039461::get_offset_of_U24current_2(),
	U3CWrapCoroutineU3Ec__Iterator0_t4183039461::get_offset_of_U24disposing_3(),
	U3CWrapCoroutineU3Ec__Iterator0_t4183039461::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (AfterTimer_Component_t442788763), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[4] = 
{
	AfterTimer_Component_t442788763::get_offset_of_callback_2(),
	AfterTimer_Component_t442788763::get_offset_of_triggerTime_3(),
	AfterTimer_Component_t442788763::get_offset_of_timerActive_4(),
	AfterTimer_Component_t442788763::get_offset_of_triggerOnEarlyDestroy_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (U3CWaitU3Ec__Iterator0_t1674543291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[4] = 
{
	U3CWaitU3Ec__Iterator0_t1674543291::get_offset_of_U24this_0(),
	U3CWaitU3Ec__Iterator0_t1674543291::get_offset_of_U24current_1(),
	U3CWaitU3Ec__Iterator0_t1674543291::get_offset_of_U24disposing_2(),
	U3CWaitU3Ec__Iterator0_t1674543291::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (VelocityEstimator_t1153298725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[7] = 
{
	VelocityEstimator_t1153298725::get_offset_of_velocityAverageFrames_2(),
	VelocityEstimator_t1153298725::get_offset_of_angularVelocityAverageFrames_3(),
	VelocityEstimator_t1153298725::get_offset_of_estimateOnAwake_4(),
	VelocityEstimator_t1153298725::get_offset_of_routine_5(),
	VelocityEstimator_t1153298725::get_offset_of_sampleCount_6(),
	VelocityEstimator_t1153298725::get_offset_of_velocitySamples_7(),
	VelocityEstimator_t1153298725::get_offset_of_angularVelocitySamples_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[12] = 
{
	U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089::get_offset_of_U3CpreviousPositionU3E__0_0(),
	U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089::get_offset_of_U3CpreviousRotationU3E__1_1(),
	U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089::get_offset_of_U3CvelocityFactorU3E__2_2(),
	U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089::get_offset_of_U3CvU3E__3_3(),
	U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089::get_offset_of_U3CwU3E__4_4(),
	U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089::get_offset_of_U3CdeltaRotationU3E__5_5(),
	U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089::get_offset_of_U3CthetaU3E__6_6(),
	U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089::get_offset_of_U3CangularVelocityU3E__7_7(),
	U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089::get_offset_of_U24this_8(),
	U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089::get_offset_of_U24current_9(),
	U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089::get_offset_of_U24disposing_10(),
	U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (ControllerButtonHints_t4025449936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[17] = 
{
	ControllerButtonHints_t4025449936::get_offset_of_controllerMaterial_2(),
	ControllerButtonHints_t4025449936::get_offset_of_flashColor_3(),
	ControllerButtonHints_t4025449936::get_offset_of_textHintPrefab_4(),
	ControllerButtonHints_t4025449936::get_offset_of_debugHints_5(),
	ControllerButtonHints_t4025449936::get_offset_of_renderModel_6(),
	ControllerButtonHints_t4025449936::get_offset_of_player_7(),
	ControllerButtonHints_t4025449936::get_offset_of_renderers_8(),
	ControllerButtonHints_t4025449936::get_offset_of_flashingRenderers_9(),
	ControllerButtonHints_t4025449936::get_offset_of_startTime_10(),
	ControllerButtonHints_t4025449936::get_offset_of_tickCount_11(),
	ControllerButtonHints_t4025449936::get_offset_of_buttonHintInfos_12(),
	ControllerButtonHints_t4025449936::get_offset_of_textHintParent_13(),
	ControllerButtonHints_t4025449936::get_offset_of_componentButtonMasks_14(),
	ControllerButtonHints_t4025449936::get_offset_of_colorID_15(),
	ControllerButtonHints_t4025449936::get_offset_of_U3CinitializedU3Ek__BackingField_16(),
	ControllerButtonHints_t4025449936::get_offset_of_centerPosition_17(),
	ControllerButtonHints_t4025449936::get_offset_of_renderModelLoadedAction_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (OffsetType_t3822090287)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2174[5] = 
{
	OffsetType_t3822090287::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (ButtonHintInfo_t106135473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2175[14] = 
{
	ButtonHintInfo_t106135473::get_offset_of_componentName_0(),
	ButtonHintInfo_t106135473::get_offset_of_renderers_1(),
	ButtonHintInfo_t106135473::get_offset_of_localTransform_2(),
	ButtonHintInfo_t106135473::get_offset_of_textHintObject_3(),
	ButtonHintInfo_t106135473::get_offset_of_textStartAnchor_4(),
	ButtonHintInfo_t106135473::get_offset_of_textEndAnchor_5(),
	ButtonHintInfo_t106135473::get_offset_of_textEndOffsetDir_6(),
	ButtonHintInfo_t106135473::get_offset_of_canvasOffset_7(),
	ButtonHintInfo_t106135473::get_offset_of_text_8(),
	ButtonHintInfo_t106135473::get_offset_of_textMesh_9(),
	ButtonHintInfo_t106135473::get_offset_of_textCanvas_10(),
	ButtonHintInfo_t106135473::get_offset_of_line_11(),
	ButtonHintInfo_t106135473::get_offset_of_distanceFromCenter_12(),
	ButtonHintInfo_t106135473::get_offset_of_textHintActive_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (U3CTestButtonHintsU3Ec__Iterator0_t185367873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[4] = 
{
	U3CTestButtonHintsU3Ec__Iterator0_t185367873::get_offset_of_U24this_0(),
	U3CTestButtonHintsU3Ec__Iterator0_t185367873::get_offset_of_U24current_1(),
	U3CTestButtonHintsU3Ec__Iterator0_t185367873::get_offset_of_U24disposing_2(),
	U3CTestButtonHintsU3Ec__Iterator0_t185367873::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (U3CTestTextHintsU3Ec__Iterator1_t1805603699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2177[4] = 
{
	U3CTestTextHintsU3Ec__Iterator1_t1805603699::get_offset_of_U24this_0(),
	U3CTestTextHintsU3Ec__Iterator1_t1805603699::get_offset_of_U24current_1(),
	U3CTestTextHintsU3Ec__Iterator1_t1805603699::get_offset_of_U24disposing_2(),
	U3CTestTextHintsU3Ec__Iterator1_t1805603699::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (ArcheryTarget_t3389223157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2178[8] = 
{
	ArcheryTarget_t3389223157::get_offset_of_onTakeDamage_2(),
	ArcheryTarget_t3389223157::get_offset_of_onceOnly_3(),
	ArcheryTarget_t3389223157::get_offset_of_targetCenter_4(),
	ArcheryTarget_t3389223157::get_offset_of_baseTransform_5(),
	ArcheryTarget_t3389223157::get_offset_of_fallenDownTransform_6(),
	ArcheryTarget_t3389223157::get_offset_of_fallTime_7(),
	0,
	ArcheryTarget_t3389223157::get_offset_of_targetEnabled_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (U3CFallDownU3Ec__Iterator0_t3537536025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[7] = 
{
	U3CFallDownU3Ec__Iterator0_t3537536025::get_offset_of_U3CstartingRotU3E__0_0(),
	U3CFallDownU3Ec__Iterator0_t3537536025::get_offset_of_U3CstartTimeU3E__1_1(),
	U3CFallDownU3Ec__Iterator0_t3537536025::get_offset_of_U3CrotLerpU3E__2_2(),
	U3CFallDownU3Ec__Iterator0_t3537536025::get_offset_of_U24this_3(),
	U3CFallDownU3Ec__Iterator0_t3537536025::get_offset_of_U24current_4(),
	U3CFallDownU3Ec__Iterator0_t3537536025::get_offset_of_U24disposing_5(),
	U3CFallDownU3Ec__Iterator0_t3537536025::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (Arrow_t2932383743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[17] = 
{
	Arrow_t2932383743::get_offset_of_glintParticle_2(),
	Arrow_t2932383743::get_offset_of_arrowHeadRB_3(),
	Arrow_t2932383743::get_offset_of_shaftRB_4(),
	Arrow_t2932383743::get_offset_of_targetPhysMaterial_5(),
	Arrow_t2932383743::get_offset_of_prevPosition_6(),
	Arrow_t2932383743::get_offset_of_prevRotation_7(),
	Arrow_t2932383743::get_offset_of_prevVelocity_8(),
	Arrow_t2932383743::get_offset_of_prevHeadPosition_9(),
	Arrow_t2932383743::get_offset_of_fireReleaseSound_10(),
	Arrow_t2932383743::get_offset_of_airReleaseSound_11(),
	Arrow_t2932383743::get_offset_of_hitTargetSound_12(),
	Arrow_t2932383743::get_offset_of_hitGroundSound_13(),
	Arrow_t2932383743::get_offset_of_inFlight_14(),
	Arrow_t2932383743::get_offset_of_released_15(),
	Arrow_t2932383743::get_offset_of_hasSpreadFire_16(),
	Arrow_t2932383743::get_offset_of_travelledFrames_17(),
	Arrow_t2932383743::get_offset_of_scaleParentObject_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (ArrowHand_t565341420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[17] = 
{
	ArrowHand_t565341420::get_offset_of_hand_2(),
	ArrowHand_t565341420::get_offset_of_bow_3(),
	ArrowHand_t565341420::get_offset_of_currentArrow_4(),
	ArrowHand_t565341420::get_offset_of_arrowPrefab_5(),
	ArrowHand_t565341420::get_offset_of_arrowNockTransform_6(),
	ArrowHand_t565341420::get_offset_of_nockDistance_7(),
	ArrowHand_t565341420::get_offset_of_lerpCompleteDistance_8(),
	ArrowHand_t565341420::get_offset_of_rotationLerpThreshold_9(),
	ArrowHand_t565341420::get_offset_of_positionLerpThreshold_10(),
	ArrowHand_t565341420::get_offset_of_allowArrowSpawn_11(),
	ArrowHand_t565341420::get_offset_of_nocked_12(),
	ArrowHand_t565341420::get_offset_of_inNockRange_13(),
	ArrowHand_t565341420::get_offset_of_arrowLerpComplete_14(),
	ArrowHand_t565341420::get_offset_of_arrowSpawnSound_15(),
	ArrowHand_t565341420::get_offset_of_allowTeleport_16(),
	ArrowHand_t565341420::get_offset_of_maxArrowCount_17(),
	ArrowHand_t565341420::get_offset_of_arrowList_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (U3CArrowReleaseHapticsU3Ec__Iterator0_t4054708145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[4] = 
{
	U3CArrowReleaseHapticsU3Ec__Iterator0_t4054708145::get_offset_of_U24this_0(),
	U3CArrowReleaseHapticsU3Ec__Iterator0_t4054708145::get_offset_of_U24current_1(),
	U3CArrowReleaseHapticsU3Ec__Iterator0_t4054708145::get_offset_of_U24disposing_2(),
	U3CArrowReleaseHapticsU3Ec__Iterator0_t4054708145::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (ArrowheadRotation_t4253007731), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (Balloon_t3376655393), -1, sizeof(Balloon_t3376655393_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2184[15] = 
{
	Balloon_t3376655393::get_offset_of_hand_2(),
	Balloon_t3376655393::get_offset_of_popPrefab_3(),
	Balloon_t3376655393::get_offset_of_maxVelocity_4(),
	Balloon_t3376655393::get_offset_of_lifetime_5(),
	Balloon_t3376655393::get_offset_of_burstOnLifetimeEnd_6(),
	Balloon_t3376655393::get_offset_of_lifetimeEndParticlePrefab_7(),
	Balloon_t3376655393::get_offset_of_lifetimeEndSound_8(),
	Balloon_t3376655393::get_offset_of_destructTime_9(),
	Balloon_t3376655393::get_offset_of_releaseTime_10(),
	Balloon_t3376655393::get_offset_of_collisionSound_11(),
	Balloon_t3376655393::get_offset_of_lastSoundTime_12(),
	Balloon_t3376655393::get_offset_of_soundDelay_13(),
	Balloon_t3376655393::get_offset_of_balloonRigidbody_14(),
	Balloon_t3376655393::get_offset_of_bParticlesSpawned_15(),
	Balloon_t3376655393_StaticFields::get_offset_of_s_flLastDeathSound_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (BalloonColor_t1350152013)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2185[16] = 
{
	BalloonColor_t1350152013::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (BalloonColliders_t2580220890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[4] = 
{
	BalloonColliders_t2580220890::get_offset_of_colliders_2(),
	BalloonColliders_t2580220890::get_offset_of_colliderLocalPositions_3(),
	BalloonColliders_t2580220890::get_offset_of_colliderLocalRotations_4(),
	BalloonColliders_t2580220890::get_offset_of_rb_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (BalloonHapticBump_t3404570198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[1] = 
{
	BalloonHapticBump_t3404570198::get_offset_of_physParent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (BalloonSpawner_t4219926993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2188[15] = 
{
	BalloonSpawner_t4219926993::get_offset_of_minSpawnTime_2(),
	BalloonSpawner_t4219926993::get_offset_of_maxSpawnTime_3(),
	BalloonSpawner_t4219926993::get_offset_of_nextSpawnTime_4(),
	BalloonSpawner_t4219926993::get_offset_of_balloonPrefab_5(),
	BalloonSpawner_t4219926993::get_offset_of_autoSpawn_6(),
	BalloonSpawner_t4219926993::get_offset_of_spawnAtStartup_7(),
	BalloonSpawner_t4219926993::get_offset_of_playSounds_8(),
	BalloonSpawner_t4219926993::get_offset_of_inflateSound_9(),
	BalloonSpawner_t4219926993::get_offset_of_stretchSound_10(),
	BalloonSpawner_t4219926993::get_offset_of_sendSpawnMessageToParent_11(),
	BalloonSpawner_t4219926993::get_offset_of_scale_12(),
	BalloonSpawner_t4219926993::get_offset_of_spawnDirectionTransform_13(),
	BalloonSpawner_t4219926993::get_offset_of_spawnForce_14(),
	BalloonSpawner_t4219926993::get_offset_of_attachBalloon_15(),
	BalloonSpawner_t4219926993::get_offset_of_color_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (ExplosionWobble_t605436430), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (FireSource_t179112773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[11] = 
{
	FireSource_t179112773::get_offset_of_fireParticlePrefab_2(),
	FireSource_t179112773::get_offset_of_startActive_3(),
	FireSource_t179112773::get_offset_of_fireObject_4(),
	FireSource_t179112773::get_offset_of_customParticles_5(),
	FireSource_t179112773::get_offset_of_isBurning_6(),
	FireSource_t179112773::get_offset_of_burnTime_7(),
	FireSource_t179112773::get_offset_of_ignitionDelay_8(),
	FireSource_t179112773::get_offset_of_ignitionTime_9(),
	FireSource_t179112773::get_offset_of_hand_10(),
	FireSource_t179112773::get_offset_of_ignitionSound_11(),
	FireSource_t179112773::get_offset_of_canSpreadFromThisSource_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (Longbow_t2607500110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[46] = 
{
	Longbow_t2607500110::get_offset_of_currentHandGuess_2(),
	Longbow_t2607500110::get_offset_of_timeOfPossibleHandSwitch_3(),
	Longbow_t2607500110::get_offset_of_timeBeforeConfirmingHandSwitch_4(),
	Longbow_t2607500110::get_offset_of_possibleHandSwitch_5(),
	Longbow_t2607500110::get_offset_of_pivotTransform_6(),
	Longbow_t2607500110::get_offset_of_handleTransform_7(),
	Longbow_t2607500110::get_offset_of_hand_8(),
	Longbow_t2607500110::get_offset_of_arrowHand_9(),
	Longbow_t2607500110::get_offset_of_nockTransform_10(),
	Longbow_t2607500110::get_offset_of_nockRestTransform_11(),
	Longbow_t2607500110::get_offset_of_autoSpawnArrowHand_12(),
	Longbow_t2607500110::get_offset_of_arrowHandItemPackage_13(),
	Longbow_t2607500110::get_offset_of_arrowHandPrefab_14(),
	Longbow_t2607500110::get_offset_of_nocked_15(),
	Longbow_t2607500110::get_offset_of_pulled_16(),
	0,
	0,
	Longbow_t2607500110::get_offset_of_nockDistanceTravelled_19(),
	Longbow_t2607500110::get_offset_of_hapticDistanceThreshold_20(),
	Longbow_t2607500110::get_offset_of_lastTickDistance_21(),
	0,
	0,
	Longbow_t2607500110::get_offset_of_bowLeftVector_24(),
	Longbow_t2607500110::get_offset_of_arrowMinVelocity_25(),
	Longbow_t2607500110::get_offset_of_arrowMaxVelocity_26(),
	Longbow_t2607500110::get_offset_of_arrowVelocity_27(),
	Longbow_t2607500110::get_offset_of_minStrainTickTime_28(),
	Longbow_t2607500110::get_offset_of_maxStrainTickTime_29(),
	Longbow_t2607500110::get_offset_of_nextStrainTick_30(),
	Longbow_t2607500110::get_offset_of_lerpBackToZeroRotation_31(),
	Longbow_t2607500110::get_offset_of_lerpStartTime_32(),
	Longbow_t2607500110::get_offset_of_lerpDuration_33(),
	Longbow_t2607500110::get_offset_of_lerpStartRotation_34(),
	Longbow_t2607500110::get_offset_of_nockLerpStartTime_35(),
	Longbow_t2607500110::get_offset_of_nockLerpStartRotation_36(),
	Longbow_t2607500110::get_offset_of_drawOffset_37(),
	Longbow_t2607500110::get_offset_of_bowDrawLinearMapping_38(),
	Longbow_t2607500110::get_offset_of_deferNewPoses_39(),
	Longbow_t2607500110::get_offset_of_lateUpdatePos_40(),
	Longbow_t2607500110::get_offset_of_lateUpdateRot_41(),
	Longbow_t2607500110::get_offset_of_drawSound_42(),
	Longbow_t2607500110::get_offset_of_drawTension_43(),
	Longbow_t2607500110::get_offset_of_arrowSlideSound_44(),
	Longbow_t2607500110::get_offset_of_releaseSound_45(),
	Longbow_t2607500110::get_offset_of_nockSound_46(),
	Longbow_t2607500110::get_offset_of_newPosesAppliedAction_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (Handedness_t840502957)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2192[3] = 
{
	Handedness_t840502957::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (U3CResetDrawAnimU3Ec__Iterator0_t3691052025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[7] = 
{
	U3CResetDrawAnimU3Ec__Iterator0_t3691052025::get_offset_of_U3CstartTimeU3E__0_0(),
	U3CResetDrawAnimU3Ec__Iterator0_t3691052025::get_offset_of_U3CstartLerpU3E__1_1(),
	U3CResetDrawAnimU3Ec__Iterator0_t3691052025::get_offset_of_U3ClerpU3E__2_2(),
	U3CResetDrawAnimU3Ec__Iterator0_t3691052025::get_offset_of_U24this_3(),
	U3CResetDrawAnimU3Ec__Iterator0_t3691052025::get_offset_of_U24current_4(),
	U3CResetDrawAnimU3Ec__Iterator0_t3691052025::get_offset_of_U24disposing_5(),
	U3CResetDrawAnimU3Ec__Iterator0_t3691052025::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (SoundBowClick_t88828517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2194[5] = 
{
	SoundBowClick_t88828517::get_offset_of_bowClick_2(),
	SoundBowClick_t88828517::get_offset_of_pitchTensionCurve_3(),
	SoundBowClick_t88828517::get_offset_of_minPitch_4(),
	SoundBowClick_t88828517::get_offset_of_maxPitch_5(),
	SoundBowClick_t88828517::get_offset_of_thisAudioSource_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (ControllerHintsExample_t4074302606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[2] = 
{
	ControllerHintsExample_t4074302606::get_offset_of_buttonHintCoroutine_2(),
	ControllerHintsExample_t4074302606::get_offset_of_textHintCoroutine_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (U3CTestButtonHintsU3Ec__Iterator0_t3802738335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[4] = 
{
	U3CTestButtonHintsU3Ec__Iterator0_t3802738335::get_offset_of_hand_0(),
	U3CTestButtonHintsU3Ec__Iterator0_t3802738335::get_offset_of_U24current_1(),
	U3CTestButtonHintsU3Ec__Iterator0_t3802738335::get_offset_of_U24disposing_2(),
	U3CTestButtonHintsU3Ec__Iterator0_t3802738335::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (U3CTestTextHintsU3Ec__Iterator1_t1955400369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2197[4] = 
{
	U3CTestTextHintsU3Ec__Iterator1_t1955400369::get_offset_of_hand_0(),
	U3CTestTextHintsU3Ec__Iterator1_t1955400369::get_offset_of_U24current_1(),
	U3CTestTextHintsU3Ec__Iterator1_t1955400369::get_offset_of_U24disposing_2(),
	U3CTestTextHintsU3Ec__Iterator1_t1955400369::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (InteractableExample_t1462204982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[5] = 
{
	InteractableExample_t1462204982::get_offset_of_textMesh_2(),
	InteractableExample_t1462204982::get_offset_of_oldPosition_3(),
	InteractableExample_t1462204982::get_offset_of_oldRotation_4(),
	InteractableExample_t1462204982::get_offset_of_attachTime_5(),
	InteractableExample_t1462204982::get_offset_of_attachmentFlags_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (AllowTeleportWhileAttachedToHand_t416616227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[2] = 
{
	AllowTeleportWhileAttachedToHand_t416616227::get_offset_of_teleportAllowed_2(),
	AllowTeleportWhileAttachedToHand_t416616227::get_offset_of_overrideHoverLock_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
