﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen626063409MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityAction`2<SteamVR_RenderModel,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define UnityAction_2__ctor_m3947782411(__this, ___object0, ___method1, method) ((  void (*) (UnityAction_2_t4073506138 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityAction_2__ctor_m2121637916_gshared)(__this, ___object0, ___method1, method)
// System.Void UnityEngine.Events.UnityAction`2<SteamVR_RenderModel,System.Boolean>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m1090547011(__this, ___arg00, ___arg11, method) ((  void (*) (UnityAction_2_t4073506138 *, SteamVR_RenderModel_t2905485978 *, bool, const MethodInfo*))UnityAction_2_Invoke_m3333140761_gshared)(__this, ___arg00, ___arg11, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`2<SteamVR_RenderModel,System.Boolean>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
#define UnityAction_2_BeginInvoke_m369961622(__this, ___arg00, ___arg11, ___callback2, ___object3, method) ((  Il2CppObject * (*) (UnityAction_2_t4073506138 *, SteamVR_RenderModel_t2905485978 *, bool, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))UnityAction_2_BeginInvoke_m2287629450_gshared)(__this, ___arg00, ___arg11, ___callback2, ___object3, method)
// System.Void UnityEngine.Events.UnityAction`2<SteamVR_RenderModel,System.Boolean>::EndInvoke(System.IAsyncResult)
#define UnityAction_2_EndInvoke_m4293453030(__this, ___result0, method) ((  void (*) (UnityAction_2_t4073506138 *, Il2CppObject *, const MethodInfo*))UnityAction_2_EndInvoke_m2394165802_gshared)(__this, ___result0, method)
