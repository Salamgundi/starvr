﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_IK
struct SteamVR_IK_t3055182089;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void SteamVR_IK::.ctor()
extern "C"  void SteamVR_IK__ctor_m4233402380 (SteamVR_IK_t3055182089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_IK::LateUpdate()
extern "C"  void SteamVR_IK_LateUpdate_m209758209 (SteamVR_IK_t3055182089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_IK::Solve(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  bool SteamVR_IK_Solve_m728531005 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___start0, Vector3_t2243707580  ___end1, Vector3_t2243707580  ___poleVector2, float ___jointDist3, float ___targetDist4, Vector3_t2243707580 * ___result5, Vector3_t2243707580 * ___forward6, Vector3_t2243707580 * ___up7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
