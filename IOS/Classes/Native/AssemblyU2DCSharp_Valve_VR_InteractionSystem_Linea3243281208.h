﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.LinearMapping
struct LinearMapping_t810676855;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.LinearAnimator
struct  LinearAnimator_t3243281208  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.LinearMapping Valve.VR.InteractionSystem.LinearAnimator::linearMapping
	LinearMapping_t810676855 * ___linearMapping_2;
	// UnityEngine.Animator Valve.VR.InteractionSystem.LinearAnimator::animator
	Animator_t69676727 * ___animator_3;
	// System.Single Valve.VR.InteractionSystem.LinearAnimator::currentLinearMapping
	float ___currentLinearMapping_4;
	// System.Int32 Valve.VR.InteractionSystem.LinearAnimator::framesUnchanged
	int32_t ___framesUnchanged_5;

public:
	inline static int32_t get_offset_of_linearMapping_2() { return static_cast<int32_t>(offsetof(LinearAnimator_t3243281208, ___linearMapping_2)); }
	inline LinearMapping_t810676855 * get_linearMapping_2() const { return ___linearMapping_2; }
	inline LinearMapping_t810676855 ** get_address_of_linearMapping_2() { return &___linearMapping_2; }
	inline void set_linearMapping_2(LinearMapping_t810676855 * value)
	{
		___linearMapping_2 = value;
		Il2CppCodeGenWriteBarrier(&___linearMapping_2, value);
	}

	inline static int32_t get_offset_of_animator_3() { return static_cast<int32_t>(offsetof(LinearAnimator_t3243281208, ___animator_3)); }
	inline Animator_t69676727 * get_animator_3() const { return ___animator_3; }
	inline Animator_t69676727 ** get_address_of_animator_3() { return &___animator_3; }
	inline void set_animator_3(Animator_t69676727 * value)
	{
		___animator_3 = value;
		Il2CppCodeGenWriteBarrier(&___animator_3, value);
	}

	inline static int32_t get_offset_of_currentLinearMapping_4() { return static_cast<int32_t>(offsetof(LinearAnimator_t3243281208, ___currentLinearMapping_4)); }
	inline float get_currentLinearMapping_4() const { return ___currentLinearMapping_4; }
	inline float* get_address_of_currentLinearMapping_4() { return &___currentLinearMapping_4; }
	inline void set_currentLinearMapping_4(float value)
	{
		___currentLinearMapping_4 = value;
	}

	inline static int32_t get_offset_of_framesUnchanged_5() { return static_cast<int32_t>(offsetof(LinearAnimator_t3243281208, ___framesUnchanged_5)); }
	inline int32_t get_framesUnchanged_5() const { return ___framesUnchanged_5; }
	inline int32_t* get_address_of_framesUnchanged_5() { return &___framesUnchanged_5; }
	inline void set_framesUnchanged_5(int32_t value)
	{
		___framesUnchanged_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
