﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_FreeTexture
struct _FreeTexture_t4051202214;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_FreeTexture::.ctor(System.Object,System.IntPtr)
extern "C"  void _FreeTexture__ctor_m542112547 (_FreeTexture_t4051202214 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRRenderModels/_FreeTexture::Invoke(System.IntPtr)
extern "C"  void _FreeTexture_Invoke_m28621065 (_FreeTexture_t4051202214 * __this, IntPtr_t ___pTexture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_FreeTexture::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _FreeTexture_BeginInvoke_m3398534740 (_FreeTexture_t4051202214 * __this, IntPtr_t ___pTexture0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRRenderModels/_FreeTexture::EndInvoke(System.IAsyncResult)
extern "C"  void _FreeTexture_EndInvoke_m2226226337 (_FreeTexture_t4051202214 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
