﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRTrackedCamera/_ReleaseVideoStreamingService
struct _ReleaseVideoStreamingService_t1473645041;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraError3529690400.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRTrackedCamera/_ReleaseVideoStreamingService::.ctor(System.Object,System.IntPtr)
extern "C"  void _ReleaseVideoStreamingService__ctor_m3054187466 (_ReleaseVideoStreamingService_t1473645041 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_ReleaseVideoStreamingService::Invoke(System.UInt64)
extern "C"  int32_t _ReleaseVideoStreamingService_Invoke_m2824328004 (_ReleaseVideoStreamingService_t1473645041 * __this, uint64_t ___hTrackedCamera0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRTrackedCamera/_ReleaseVideoStreamingService::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ReleaseVideoStreamingService_BeginInvoke_m798779176 (_ReleaseVideoStreamingService_t1473645041 * __this, uint64_t ___hTrackedCamera0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_ReleaseVideoStreamingService::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _ReleaseVideoStreamingService_EndInvoke_m1284806799 (_ReleaseVideoStreamingService_t1473645041 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
