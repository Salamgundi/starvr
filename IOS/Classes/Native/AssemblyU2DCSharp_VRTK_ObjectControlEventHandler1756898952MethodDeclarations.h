﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.ObjectControlEventHandler
struct ObjectControlEventHandler_t1756898952;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_ObjectControlEventArgs2459490319.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.ObjectControlEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ObjectControlEventHandler__ctor_m3631732026 (ObjectControlEventHandler_t1756898952 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.ObjectControlEventHandler::Invoke(System.Object,VRTK.ObjectControlEventArgs)
extern "C"  void ObjectControlEventHandler_Invoke_m1834464988 (ObjectControlEventHandler_t1756898952 * __this, Il2CppObject * ___sender0, ObjectControlEventArgs_t2459490319  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.ObjectControlEventHandler::BeginInvoke(System.Object,VRTK.ObjectControlEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ObjectControlEventHandler_BeginInvoke_m2820271905 (ObjectControlEventHandler_t1756898952 * __this, Il2CppObject * ___sender0, ObjectControlEventArgs_t2459490319  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.ObjectControlEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ObjectControlEventHandler_EndInvoke_m2085366024 (ObjectControlEventHandler_t1756898952 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
