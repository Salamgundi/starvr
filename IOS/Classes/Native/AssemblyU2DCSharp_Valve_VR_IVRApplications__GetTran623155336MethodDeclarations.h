﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetTransitionState
struct _GetTransitionState_t623155336;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationTransitio3895609521.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetTransitionState::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetTransitionState__ctor_m2235979149 (_GetTransitionState_t623155336 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationTransitionState Valve.VR.IVRApplications/_GetTransitionState::Invoke()
extern "C"  int32_t _GetTransitionState_Invoke_m1566719177 (_GetTransitionState_t623155336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetTransitionState::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetTransitionState_BeginInvoke_m1007764614 (_GetTransitionState_t623155336 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationTransitionState Valve.VR.IVRApplications/_GetTransitionState::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _GetTransitionState_EndInvoke_m3360927335 (_GetTransitionState_t623155336 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
