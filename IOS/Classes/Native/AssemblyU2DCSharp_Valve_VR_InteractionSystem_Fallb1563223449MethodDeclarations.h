﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.FallbackCameraController
struct FallbackCameraController_t1563223449;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.FallbackCameraController::.ctor()
extern "C"  void FallbackCameraController__ctor_m126810791 (FallbackCameraController_t1563223449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.FallbackCameraController::OnEnable()
extern "C"  void FallbackCameraController_OnEnable_m3912759059 (FallbackCameraController_t1563223449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.FallbackCameraController::Update()
extern "C"  void FallbackCameraController_Update_m488250588 (FallbackCameraController_t1563223449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.FallbackCameraController::OnGUI()
extern "C"  void FallbackCameraController_OnGUI_m2822858061 (FallbackCameraController_t1563223449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
