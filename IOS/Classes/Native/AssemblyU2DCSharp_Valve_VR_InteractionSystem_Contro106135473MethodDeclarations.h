﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo
struct ButtonHintInfo_t106135473;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::.ctor()
extern "C"  void ButtonHintInfo__ctor_m65867936 (ButtonHintInfo_t106135473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
