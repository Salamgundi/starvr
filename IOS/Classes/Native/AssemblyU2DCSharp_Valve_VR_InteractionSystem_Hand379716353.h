﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Camera
struct Camera_t189460977;
// SteamVR_Controller/Device
struct Device_t2885069456;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct List_1_t756839068;
// Valve.VR.InteractionSystem.Interactable
struct Interactable_t1274046986;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629;
// Valve.VR.InteractionSystem.Player
struct Player_t4256718089;
// SteamVR_Events/Action
struct Action_t1836998693;
// System.Predicate`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct Predicate_1_t4125655347;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1543711741.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1719631858.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.Hand
struct  Hand_t379716353  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.Hand::otherHand
	Hand_t379716353 * ___otherHand_3;
	// Valve.VR.InteractionSystem.Hand/HandType Valve.VR.InteractionSystem.Hand::startingHandType
	int32_t ___startingHandType_4;
	// UnityEngine.Transform Valve.VR.InteractionSystem.Hand::hoverSphereTransform
	Transform_t3275118058 * ___hoverSphereTransform_5;
	// System.Single Valve.VR.InteractionSystem.Hand::hoverSphereRadius
	float ___hoverSphereRadius_6;
	// UnityEngine.LayerMask Valve.VR.InteractionSystem.Hand::hoverLayerMask
	LayerMask_t3188175821  ___hoverLayerMask_7;
	// System.Single Valve.VR.InteractionSystem.Hand::hoverUpdateInterval
	float ___hoverUpdateInterval_8;
	// UnityEngine.Camera Valve.VR.InteractionSystem.Hand::noSteamVRFallbackCamera
	Camera_t189460977 * ___noSteamVRFallbackCamera_9;
	// System.Single Valve.VR.InteractionSystem.Hand::noSteamVRFallbackMaxDistanceNoItem
	float ___noSteamVRFallbackMaxDistanceNoItem_10;
	// System.Single Valve.VR.InteractionSystem.Hand::noSteamVRFallbackMaxDistanceWithItem
	float ___noSteamVRFallbackMaxDistanceWithItem_11;
	// System.Single Valve.VR.InteractionSystem.Hand::noSteamVRFallbackInteractorDistance
	float ___noSteamVRFallbackInteractorDistance_12;
	// SteamVR_Controller/Device Valve.VR.InteractionSystem.Hand::controller
	Device_t2885069456 * ___controller_13;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Hand::controllerPrefab
	GameObject_t1756533147 * ___controllerPrefab_14;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Hand::controllerObject
	GameObject_t1756533147 * ___controllerObject_15;
	// System.Boolean Valve.VR.InteractionSystem.Hand::showDebugText
	bool ___showDebugText_16;
	// System.Boolean Valve.VR.InteractionSystem.Hand::spewDebugText
	bool ___spewDebugText_17;
	// System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject> Valve.VR.InteractionSystem.Hand::attachedObjects
	List_1_t756839068 * ___attachedObjects_18;
	// System.Boolean Valve.VR.InteractionSystem.Hand::<hoverLocked>k__BackingField
	bool ___U3ChoverLockedU3Ek__BackingField_19;
	// Valve.VR.InteractionSystem.Interactable Valve.VR.InteractionSystem.Hand::_hoveringInteractable
	Interactable_t1274046986 * ____hoveringInteractable_20;
	// UnityEngine.TextMesh Valve.VR.InteractionSystem.Hand::debugText
	TextMesh_t1641806576 * ___debugText_21;
	// System.Int32 Valve.VR.InteractionSystem.Hand::prevOverlappingColliders
	int32_t ___prevOverlappingColliders_22;
	// UnityEngine.Collider[] Valve.VR.InteractionSystem.Hand::overlappingColliders
	ColliderU5BU5D_t462843629* ___overlappingColliders_24;
	// Valve.VR.InteractionSystem.Player Valve.VR.InteractionSystem.Hand::playerInstance
	Player_t4256718089 * ___playerInstance_25;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Hand::applicationLostFocusObject
	GameObject_t1756533147 * ___applicationLostFocusObject_26;
	// SteamVR_Events/Action Valve.VR.InteractionSystem.Hand::inputFocusAction
	Action_t1836998693 * ___inputFocusAction_27;

public:
	inline static int32_t get_offset_of_otherHand_3() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___otherHand_3)); }
	inline Hand_t379716353 * get_otherHand_3() const { return ___otherHand_3; }
	inline Hand_t379716353 ** get_address_of_otherHand_3() { return &___otherHand_3; }
	inline void set_otherHand_3(Hand_t379716353 * value)
	{
		___otherHand_3 = value;
		Il2CppCodeGenWriteBarrier(&___otherHand_3, value);
	}

	inline static int32_t get_offset_of_startingHandType_4() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___startingHandType_4)); }
	inline int32_t get_startingHandType_4() const { return ___startingHandType_4; }
	inline int32_t* get_address_of_startingHandType_4() { return &___startingHandType_4; }
	inline void set_startingHandType_4(int32_t value)
	{
		___startingHandType_4 = value;
	}

	inline static int32_t get_offset_of_hoverSphereTransform_5() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___hoverSphereTransform_5)); }
	inline Transform_t3275118058 * get_hoverSphereTransform_5() const { return ___hoverSphereTransform_5; }
	inline Transform_t3275118058 ** get_address_of_hoverSphereTransform_5() { return &___hoverSphereTransform_5; }
	inline void set_hoverSphereTransform_5(Transform_t3275118058 * value)
	{
		___hoverSphereTransform_5 = value;
		Il2CppCodeGenWriteBarrier(&___hoverSphereTransform_5, value);
	}

	inline static int32_t get_offset_of_hoverSphereRadius_6() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___hoverSphereRadius_6)); }
	inline float get_hoverSphereRadius_6() const { return ___hoverSphereRadius_6; }
	inline float* get_address_of_hoverSphereRadius_6() { return &___hoverSphereRadius_6; }
	inline void set_hoverSphereRadius_6(float value)
	{
		___hoverSphereRadius_6 = value;
	}

	inline static int32_t get_offset_of_hoverLayerMask_7() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___hoverLayerMask_7)); }
	inline LayerMask_t3188175821  get_hoverLayerMask_7() const { return ___hoverLayerMask_7; }
	inline LayerMask_t3188175821 * get_address_of_hoverLayerMask_7() { return &___hoverLayerMask_7; }
	inline void set_hoverLayerMask_7(LayerMask_t3188175821  value)
	{
		___hoverLayerMask_7 = value;
	}

	inline static int32_t get_offset_of_hoverUpdateInterval_8() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___hoverUpdateInterval_8)); }
	inline float get_hoverUpdateInterval_8() const { return ___hoverUpdateInterval_8; }
	inline float* get_address_of_hoverUpdateInterval_8() { return &___hoverUpdateInterval_8; }
	inline void set_hoverUpdateInterval_8(float value)
	{
		___hoverUpdateInterval_8 = value;
	}

	inline static int32_t get_offset_of_noSteamVRFallbackCamera_9() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___noSteamVRFallbackCamera_9)); }
	inline Camera_t189460977 * get_noSteamVRFallbackCamera_9() const { return ___noSteamVRFallbackCamera_9; }
	inline Camera_t189460977 ** get_address_of_noSteamVRFallbackCamera_9() { return &___noSteamVRFallbackCamera_9; }
	inline void set_noSteamVRFallbackCamera_9(Camera_t189460977 * value)
	{
		___noSteamVRFallbackCamera_9 = value;
		Il2CppCodeGenWriteBarrier(&___noSteamVRFallbackCamera_9, value);
	}

	inline static int32_t get_offset_of_noSteamVRFallbackMaxDistanceNoItem_10() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___noSteamVRFallbackMaxDistanceNoItem_10)); }
	inline float get_noSteamVRFallbackMaxDistanceNoItem_10() const { return ___noSteamVRFallbackMaxDistanceNoItem_10; }
	inline float* get_address_of_noSteamVRFallbackMaxDistanceNoItem_10() { return &___noSteamVRFallbackMaxDistanceNoItem_10; }
	inline void set_noSteamVRFallbackMaxDistanceNoItem_10(float value)
	{
		___noSteamVRFallbackMaxDistanceNoItem_10 = value;
	}

	inline static int32_t get_offset_of_noSteamVRFallbackMaxDistanceWithItem_11() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___noSteamVRFallbackMaxDistanceWithItem_11)); }
	inline float get_noSteamVRFallbackMaxDistanceWithItem_11() const { return ___noSteamVRFallbackMaxDistanceWithItem_11; }
	inline float* get_address_of_noSteamVRFallbackMaxDistanceWithItem_11() { return &___noSteamVRFallbackMaxDistanceWithItem_11; }
	inline void set_noSteamVRFallbackMaxDistanceWithItem_11(float value)
	{
		___noSteamVRFallbackMaxDistanceWithItem_11 = value;
	}

	inline static int32_t get_offset_of_noSteamVRFallbackInteractorDistance_12() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___noSteamVRFallbackInteractorDistance_12)); }
	inline float get_noSteamVRFallbackInteractorDistance_12() const { return ___noSteamVRFallbackInteractorDistance_12; }
	inline float* get_address_of_noSteamVRFallbackInteractorDistance_12() { return &___noSteamVRFallbackInteractorDistance_12; }
	inline void set_noSteamVRFallbackInteractorDistance_12(float value)
	{
		___noSteamVRFallbackInteractorDistance_12 = value;
	}

	inline static int32_t get_offset_of_controller_13() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___controller_13)); }
	inline Device_t2885069456 * get_controller_13() const { return ___controller_13; }
	inline Device_t2885069456 ** get_address_of_controller_13() { return &___controller_13; }
	inline void set_controller_13(Device_t2885069456 * value)
	{
		___controller_13 = value;
		Il2CppCodeGenWriteBarrier(&___controller_13, value);
	}

	inline static int32_t get_offset_of_controllerPrefab_14() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___controllerPrefab_14)); }
	inline GameObject_t1756533147 * get_controllerPrefab_14() const { return ___controllerPrefab_14; }
	inline GameObject_t1756533147 ** get_address_of_controllerPrefab_14() { return &___controllerPrefab_14; }
	inline void set_controllerPrefab_14(GameObject_t1756533147 * value)
	{
		___controllerPrefab_14 = value;
		Il2CppCodeGenWriteBarrier(&___controllerPrefab_14, value);
	}

	inline static int32_t get_offset_of_controllerObject_15() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___controllerObject_15)); }
	inline GameObject_t1756533147 * get_controllerObject_15() const { return ___controllerObject_15; }
	inline GameObject_t1756533147 ** get_address_of_controllerObject_15() { return &___controllerObject_15; }
	inline void set_controllerObject_15(GameObject_t1756533147 * value)
	{
		___controllerObject_15 = value;
		Il2CppCodeGenWriteBarrier(&___controllerObject_15, value);
	}

	inline static int32_t get_offset_of_showDebugText_16() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___showDebugText_16)); }
	inline bool get_showDebugText_16() const { return ___showDebugText_16; }
	inline bool* get_address_of_showDebugText_16() { return &___showDebugText_16; }
	inline void set_showDebugText_16(bool value)
	{
		___showDebugText_16 = value;
	}

	inline static int32_t get_offset_of_spewDebugText_17() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___spewDebugText_17)); }
	inline bool get_spewDebugText_17() const { return ___spewDebugText_17; }
	inline bool* get_address_of_spewDebugText_17() { return &___spewDebugText_17; }
	inline void set_spewDebugText_17(bool value)
	{
		___spewDebugText_17 = value;
	}

	inline static int32_t get_offset_of_attachedObjects_18() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___attachedObjects_18)); }
	inline List_1_t756839068 * get_attachedObjects_18() const { return ___attachedObjects_18; }
	inline List_1_t756839068 ** get_address_of_attachedObjects_18() { return &___attachedObjects_18; }
	inline void set_attachedObjects_18(List_1_t756839068 * value)
	{
		___attachedObjects_18 = value;
		Il2CppCodeGenWriteBarrier(&___attachedObjects_18, value);
	}

	inline static int32_t get_offset_of_U3ChoverLockedU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___U3ChoverLockedU3Ek__BackingField_19)); }
	inline bool get_U3ChoverLockedU3Ek__BackingField_19() const { return ___U3ChoverLockedU3Ek__BackingField_19; }
	inline bool* get_address_of_U3ChoverLockedU3Ek__BackingField_19() { return &___U3ChoverLockedU3Ek__BackingField_19; }
	inline void set_U3ChoverLockedU3Ek__BackingField_19(bool value)
	{
		___U3ChoverLockedU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of__hoveringInteractable_20() { return static_cast<int32_t>(offsetof(Hand_t379716353, ____hoveringInteractable_20)); }
	inline Interactable_t1274046986 * get__hoveringInteractable_20() const { return ____hoveringInteractable_20; }
	inline Interactable_t1274046986 ** get_address_of__hoveringInteractable_20() { return &____hoveringInteractable_20; }
	inline void set__hoveringInteractable_20(Interactable_t1274046986 * value)
	{
		____hoveringInteractable_20 = value;
		Il2CppCodeGenWriteBarrier(&____hoveringInteractable_20, value);
	}

	inline static int32_t get_offset_of_debugText_21() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___debugText_21)); }
	inline TextMesh_t1641806576 * get_debugText_21() const { return ___debugText_21; }
	inline TextMesh_t1641806576 ** get_address_of_debugText_21() { return &___debugText_21; }
	inline void set_debugText_21(TextMesh_t1641806576 * value)
	{
		___debugText_21 = value;
		Il2CppCodeGenWriteBarrier(&___debugText_21, value);
	}

	inline static int32_t get_offset_of_prevOverlappingColliders_22() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___prevOverlappingColliders_22)); }
	inline int32_t get_prevOverlappingColliders_22() const { return ___prevOverlappingColliders_22; }
	inline int32_t* get_address_of_prevOverlappingColliders_22() { return &___prevOverlappingColliders_22; }
	inline void set_prevOverlappingColliders_22(int32_t value)
	{
		___prevOverlappingColliders_22 = value;
	}

	inline static int32_t get_offset_of_overlappingColliders_24() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___overlappingColliders_24)); }
	inline ColliderU5BU5D_t462843629* get_overlappingColliders_24() const { return ___overlappingColliders_24; }
	inline ColliderU5BU5D_t462843629** get_address_of_overlappingColliders_24() { return &___overlappingColliders_24; }
	inline void set_overlappingColliders_24(ColliderU5BU5D_t462843629* value)
	{
		___overlappingColliders_24 = value;
		Il2CppCodeGenWriteBarrier(&___overlappingColliders_24, value);
	}

	inline static int32_t get_offset_of_playerInstance_25() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___playerInstance_25)); }
	inline Player_t4256718089 * get_playerInstance_25() const { return ___playerInstance_25; }
	inline Player_t4256718089 ** get_address_of_playerInstance_25() { return &___playerInstance_25; }
	inline void set_playerInstance_25(Player_t4256718089 * value)
	{
		___playerInstance_25 = value;
		Il2CppCodeGenWriteBarrier(&___playerInstance_25, value);
	}

	inline static int32_t get_offset_of_applicationLostFocusObject_26() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___applicationLostFocusObject_26)); }
	inline GameObject_t1756533147 * get_applicationLostFocusObject_26() const { return ___applicationLostFocusObject_26; }
	inline GameObject_t1756533147 ** get_address_of_applicationLostFocusObject_26() { return &___applicationLostFocusObject_26; }
	inline void set_applicationLostFocusObject_26(GameObject_t1756533147 * value)
	{
		___applicationLostFocusObject_26 = value;
		Il2CppCodeGenWriteBarrier(&___applicationLostFocusObject_26, value);
	}

	inline static int32_t get_offset_of_inputFocusAction_27() { return static_cast<int32_t>(offsetof(Hand_t379716353, ___inputFocusAction_27)); }
	inline Action_t1836998693 * get_inputFocusAction_27() const { return ___inputFocusAction_27; }
	inline Action_t1836998693 ** get_address_of_inputFocusAction_27() { return &___inputFocusAction_27; }
	inline void set_inputFocusAction_27(Action_t1836998693 * value)
	{
		___inputFocusAction_27 = value;
		Il2CppCodeGenWriteBarrier(&___inputFocusAction_27, value);
	}
};

struct Hand_t379716353_StaticFields
{
public:
	// System.Predicate`1<Valve.VR.InteractionSystem.Hand/AttachedObject> Valve.VR.InteractionSystem.Hand::<>f__am$cache0
	Predicate_1_t4125655347 * ___U3CU3Ef__amU24cache0_28;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_28() { return static_cast<int32_t>(offsetof(Hand_t379716353_StaticFields, ___U3CU3Ef__amU24cache0_28)); }
	inline Predicate_1_t4125655347 * get_U3CU3Ef__amU24cache0_28() const { return ___U3CU3Ef__amU24cache0_28; }
	inline Predicate_1_t4125655347 ** get_address_of_U3CU3Ef__amU24cache0_28() { return &___U3CU3Ef__amU24cache0_28; }
	inline void set_U3CU3Ef__amU24cache0_28(Predicate_1_t4125655347 * value)
	{
		___U3CU3Ef__amU24cache0_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
