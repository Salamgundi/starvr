﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_UICanvas
struct  VRTK_UICanvas_t1283311654  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.VRTK_UICanvas::clickOnPointerCollision
	bool ___clickOnPointerCollision_2;
	// System.Single VRTK.VRTK_UICanvas::autoActivateWithinDistance
	float ___autoActivateWithinDistance_3;
	// UnityEngine.BoxCollider VRTK.VRTK_UICanvas::canvasBoxCollider
	BoxCollider_t22920061 * ___canvasBoxCollider_4;
	// UnityEngine.Rigidbody VRTK.VRTK_UICanvas::canvasRigidBody
	Rigidbody_t4233889191 * ___canvasRigidBody_5;

public:
	inline static int32_t get_offset_of_clickOnPointerCollision_2() { return static_cast<int32_t>(offsetof(VRTK_UICanvas_t1283311654, ___clickOnPointerCollision_2)); }
	inline bool get_clickOnPointerCollision_2() const { return ___clickOnPointerCollision_2; }
	inline bool* get_address_of_clickOnPointerCollision_2() { return &___clickOnPointerCollision_2; }
	inline void set_clickOnPointerCollision_2(bool value)
	{
		___clickOnPointerCollision_2 = value;
	}

	inline static int32_t get_offset_of_autoActivateWithinDistance_3() { return static_cast<int32_t>(offsetof(VRTK_UICanvas_t1283311654, ___autoActivateWithinDistance_3)); }
	inline float get_autoActivateWithinDistance_3() const { return ___autoActivateWithinDistance_3; }
	inline float* get_address_of_autoActivateWithinDistance_3() { return &___autoActivateWithinDistance_3; }
	inline void set_autoActivateWithinDistance_3(float value)
	{
		___autoActivateWithinDistance_3 = value;
	}

	inline static int32_t get_offset_of_canvasBoxCollider_4() { return static_cast<int32_t>(offsetof(VRTK_UICanvas_t1283311654, ___canvasBoxCollider_4)); }
	inline BoxCollider_t22920061 * get_canvasBoxCollider_4() const { return ___canvasBoxCollider_4; }
	inline BoxCollider_t22920061 ** get_address_of_canvasBoxCollider_4() { return &___canvasBoxCollider_4; }
	inline void set_canvasBoxCollider_4(BoxCollider_t22920061 * value)
	{
		___canvasBoxCollider_4 = value;
		Il2CppCodeGenWriteBarrier(&___canvasBoxCollider_4, value);
	}

	inline static int32_t get_offset_of_canvasRigidBody_5() { return static_cast<int32_t>(offsetof(VRTK_UICanvas_t1283311654, ___canvasRigidBody_5)); }
	inline Rigidbody_t4233889191 * get_canvasRigidBody_5() const { return ___canvasRigidBody_5; }
	inline Rigidbody_t4233889191 ** get_address_of_canvasRigidBody_5() { return &___canvasRigidBody_5; }
	inline void set_canvasRigidBody_5(Rigidbody_t4233889191 * value)
	{
		___canvasRigidBody_5 = value;
		Il2CppCodeGenWriteBarrier(&___canvasRigidBody_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
