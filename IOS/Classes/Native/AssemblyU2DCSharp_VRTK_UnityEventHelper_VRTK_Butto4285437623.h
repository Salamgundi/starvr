﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_Button
struct VRTK_Button_t855474128;
// VRTK.UnityEventHelper.VRTK_Button_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t2046806484;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_Button_UnityEvents
struct  VRTK_Button_UnityEvents_t4285437623  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_Button VRTK.UnityEventHelper.VRTK_Button_UnityEvents::b3d
	VRTK_Button_t855474128 * ___b3d_2;
	// VRTK.UnityEventHelper.VRTK_Button_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_Button_UnityEvents::OnPushed
	UnityObjectEvent_t2046806484 * ___OnPushed_3;

public:
	inline static int32_t get_offset_of_b3d_2() { return static_cast<int32_t>(offsetof(VRTK_Button_UnityEvents_t4285437623, ___b3d_2)); }
	inline VRTK_Button_t855474128 * get_b3d_2() const { return ___b3d_2; }
	inline VRTK_Button_t855474128 ** get_address_of_b3d_2() { return &___b3d_2; }
	inline void set_b3d_2(VRTK_Button_t855474128 * value)
	{
		___b3d_2 = value;
		Il2CppCodeGenWriteBarrier(&___b3d_2, value);
	}

	inline static int32_t get_offset_of_OnPushed_3() { return static_cast<int32_t>(offsetof(VRTK_Button_UnityEvents_t4285437623, ___OnPushed_3)); }
	inline UnityObjectEvent_t2046806484 * get_OnPushed_3() const { return ___OnPushed_3; }
	inline UnityObjectEvent_t2046806484 ** get_address_of_OnPushed_3() { return &___OnPushed_3; }
	inline void set_OnPushed_3(UnityObjectEvent_t2046806484 * value)
	{
		___OnPushed_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnPushed_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
