﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.ArcheryTarget
struct ArcheryTarget_t3389223157;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0
struct  U3CFallDownU3Ec__Iterator0_t3537536025  : public Il2CppObject
{
public:
	// UnityEngine.Quaternion Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0::<startingRot>__0
	Quaternion_t4030073918  ___U3CstartingRotU3E__0_0;
	// System.Single Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0::<startTime>__1
	float ___U3CstartTimeU3E__1_1;
	// System.Single Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0::<rotLerp>__2
	float ___U3CrotLerpU3E__2_2;
	// Valve.VR.InteractionSystem.ArcheryTarget Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0::$this
	ArcheryTarget_t3389223157 * ___U24this_3;
	// System.Object Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CstartingRotU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFallDownU3Ec__Iterator0_t3537536025, ___U3CstartingRotU3E__0_0)); }
	inline Quaternion_t4030073918  get_U3CstartingRotU3E__0_0() const { return ___U3CstartingRotU3E__0_0; }
	inline Quaternion_t4030073918 * get_address_of_U3CstartingRotU3E__0_0() { return &___U3CstartingRotU3E__0_0; }
	inline void set_U3CstartingRotU3E__0_0(Quaternion_t4030073918  value)
	{
		___U3CstartingRotU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CFallDownU3Ec__Iterator0_t3537536025, ___U3CstartTimeU3E__1_1)); }
	inline float get_U3CstartTimeU3E__1_1() const { return ___U3CstartTimeU3E__1_1; }
	inline float* get_address_of_U3CstartTimeU3E__1_1() { return &___U3CstartTimeU3E__1_1; }
	inline void set_U3CstartTimeU3E__1_1(float value)
	{
		___U3CstartTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CrotLerpU3E__2_2() { return static_cast<int32_t>(offsetof(U3CFallDownU3Ec__Iterator0_t3537536025, ___U3CrotLerpU3E__2_2)); }
	inline float get_U3CrotLerpU3E__2_2() const { return ___U3CrotLerpU3E__2_2; }
	inline float* get_address_of_U3CrotLerpU3E__2_2() { return &___U3CrotLerpU3E__2_2; }
	inline void set_U3CrotLerpU3E__2_2(float value)
	{
		___U3CrotLerpU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CFallDownU3Ec__Iterator0_t3537536025, ___U24this_3)); }
	inline ArcheryTarget_t3389223157 * get_U24this_3() const { return ___U24this_3; }
	inline ArcheryTarget_t3389223157 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ArcheryTarget_t3389223157 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CFallDownU3Ec__Iterator0_t3537536025, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CFallDownU3Ec__Iterator0_t3537536025, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CFallDownU3Ec__Iterator0_t3537536025, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
