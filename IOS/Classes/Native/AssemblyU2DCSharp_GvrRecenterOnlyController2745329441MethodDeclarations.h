﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrRecenterOnlyController
struct GvrRecenterOnlyController_t2745329441;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrRecenterOnlyController::.ctor()
extern "C"  void GvrRecenterOnlyController__ctor_m675220662 (GvrRecenterOnlyController_t2745329441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
