﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.UIIgnoreRaycast
struct UIIgnoreRaycast_t3484256983;
// UnityEngine.Camera
struct Camera_t189460977;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"

// System.Void UnityEngine.UI.UIIgnoreRaycast::.ctor()
extern "C"  void UIIgnoreRaycast__ctor_m1849478093 (UIIgnoreRaycast_t3484256983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.UIIgnoreRaycast::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  bool UIIgnoreRaycast_IsRaycastLocationValid_m3443711257 (UIIgnoreRaycast_t3484256983 * __this, Vector2_t2243707579  ___sp0, Camera_t189460977 * ___eventCamera1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
