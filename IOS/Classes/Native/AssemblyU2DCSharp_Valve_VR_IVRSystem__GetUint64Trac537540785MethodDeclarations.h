﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetUint64TrackedDeviceProperty
struct _GetUint64TrackedDeviceProperty_t537540785;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedDeviceProperty3226377054.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedPropertyError3340022390.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetUint64TrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetUint64TrackedDeviceProperty__ctor_m1438347832 (_GetUint64TrackedDeviceProperty_t537540785 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.IVRSystem/_GetUint64TrackedDeviceProperty::Invoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&)
extern "C"  uint64_t _GetUint64TrackedDeviceProperty_Invoke_m179767438 (_GetUint64TrackedDeviceProperty_t537540785 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetUint64TrackedDeviceProperty::BeginInvoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetUint64TrackedDeviceProperty_BeginInvoke_m4293478277 (_GetUint64TrackedDeviceProperty_t537540785 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.IVRSystem/_GetUint64TrackedDeviceProperty::EndInvoke(Valve.VR.ETrackedPropertyError&,System.IAsyncResult)
extern "C"  uint64_t _GetUint64TrackedDeviceProperty_EndInvoke_m101451898 (_GetUint64TrackedDeviceProperty_t537540785 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
