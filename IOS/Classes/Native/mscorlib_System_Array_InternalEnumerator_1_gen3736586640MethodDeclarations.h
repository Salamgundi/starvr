﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3736586640.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerTooltips_Too2877834378.h"

// System.Void System.Array/InternalEnumerator`1<VRTK.VRTK_ControllerTooltips/TooltipButtons>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2898077743_gshared (InternalEnumerator_1_t3736586640 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2898077743(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3736586640 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2898077743_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<VRTK.VRTK_ControllerTooltips/TooltipButtons>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2920869351_gshared (InternalEnumerator_1_t3736586640 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2920869351(__this, method) ((  void (*) (InternalEnumerator_1_t3736586640 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2920869351_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<VRTK.VRTK_ControllerTooltips/TooltipButtons>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2282868303_gshared (InternalEnumerator_1_t3736586640 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2282868303(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3736586640 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2282868303_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<VRTK.VRTK_ControllerTooltips/TooltipButtons>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2522050322_gshared (InternalEnumerator_1_t3736586640 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2522050322(__this, method) ((  void (*) (InternalEnumerator_1_t3736586640 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2522050322_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<VRTK.VRTK_ControllerTooltips/TooltipButtons>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4137411891_gshared (InternalEnumerator_1_t3736586640 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4137411891(__this, method) ((  bool (*) (InternalEnumerator_1_t3736586640 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4137411891_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<VRTK.VRTK_ControllerTooltips/TooltipButtons>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m4254983254_gshared (InternalEnumerator_1_t3736586640 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4254983254(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3736586640 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4254983254_gshared)(__this, method)
