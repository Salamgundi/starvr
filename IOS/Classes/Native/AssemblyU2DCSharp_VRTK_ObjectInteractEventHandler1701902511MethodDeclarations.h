﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.ObjectInteractEventHandler
struct ObjectInteractEventHandler_t1701902511;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventArgs771291242.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.ObjectInteractEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ObjectInteractEventHandler__ctor_m3551307137 (ObjectInteractEventHandler_t1701902511 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.ObjectInteractEventHandler::Invoke(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void ObjectInteractEventHandler_Invoke_m2256093302 (ObjectInteractEventHandler_t1701902511 * __this, Il2CppObject * ___sender0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.ObjectInteractEventHandler::BeginInvoke(System.Object,VRTK.ObjectInteractEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ObjectInteractEventHandler_BeginInvoke_m863584801 (ObjectInteractEventHandler_t1701902511 * __this, Il2CppObject * ___sender0, ObjectInteractEventArgs_t771291242  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.ObjectInteractEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ObjectInteractEventHandler_EndInvoke_m1723169987 (ObjectInteractEventHandler_t1701902511 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
