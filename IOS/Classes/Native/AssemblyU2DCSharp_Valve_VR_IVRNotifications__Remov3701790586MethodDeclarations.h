﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRNotifications/_RemoveNotification
struct _RemoveNotification_t3701790586;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRNotificationError1058814108.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRNotifications/_RemoveNotification::.ctor(System.Object,System.IntPtr)
extern "C"  void _RemoveNotification__ctor_m3610417683 (_RemoveNotification_t3701790586 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRNotificationError Valve.VR.IVRNotifications/_RemoveNotification::Invoke(System.UInt32)
extern "C"  int32_t _RemoveNotification_Invoke_m1479828444 (_RemoveNotification_t3701790586 * __this, uint32_t ___notificationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRNotifications/_RemoveNotification::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _RemoveNotification_BeginInvoke_m268289252 (_RemoveNotification_t3701790586 * __this, uint32_t ___notificationId0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRNotificationError Valve.VR.IVRNotifications/_RemoveNotification::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _RemoveNotification_EndInvoke_m3885401482 (_RemoveNotification_t3701790586 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
