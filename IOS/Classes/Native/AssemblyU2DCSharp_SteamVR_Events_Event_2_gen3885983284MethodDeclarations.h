﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Event`2<System.Int32,System.Boolean>
struct Event_2_t3885983284;
// UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>
struct UnityAction_2_t41828916;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Events/Event`2<System.Int32,System.Boolean>::.ctor()
extern "C"  void Event_2__ctor_m456375898_gshared (Event_2_t3885983284 * __this, const MethodInfo* method);
#define Event_2__ctor_m456375898(__this, method) ((  void (*) (Event_2_t3885983284 *, const MethodInfo*))Event_2__ctor_m456375898_gshared)(__this, method)
// System.Void SteamVR_Events/Event`2<System.Int32,System.Boolean>::Listen(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Event_2_Listen_m494929147_gshared (Event_2_t3885983284 * __this, UnityAction_2_t41828916 * ___action0, const MethodInfo* method);
#define Event_2_Listen_m494929147(__this, ___action0, method) ((  void (*) (Event_2_t3885983284 *, UnityAction_2_t41828916 *, const MethodInfo*))Event_2_Listen_m494929147_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`2<System.Int32,System.Boolean>::Remove(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Event_2_Remove_m4065385546_gshared (Event_2_t3885983284 * __this, UnityAction_2_t41828916 * ___action0, const MethodInfo* method);
#define Event_2_Remove_m4065385546(__this, ___action0, method) ((  void (*) (Event_2_t3885983284 *, UnityAction_2_t41828916 *, const MethodInfo*))Event_2_Remove_m4065385546_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`2<System.Int32,System.Boolean>::Send(T0,T1)
extern "C"  void Event_2_Send_m2682015001_gshared (Event_2_t3885983284 * __this, int32_t ___arg00, bool ___arg11, const MethodInfo* method);
#define Event_2_Send_m2682015001(__this, ___arg00, ___arg11, method) ((  void (*) (Event_2_t3885983284 *, int32_t, bool, const MethodInfo*))Event_2_Send_m2682015001_gshared)(__this, ___arg00, ___arg11, method)
