﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperone
struct IVRChaperone_t1398180532;
struct IVRChaperone_t1398180532_marshaled_pinvoke;
struct IVRChaperone_t1398180532_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IVRChaperone_t1398180532;
struct IVRChaperone_t1398180532_marshaled_pinvoke;

extern "C" void IVRChaperone_t1398180532_marshal_pinvoke(const IVRChaperone_t1398180532& unmarshaled, IVRChaperone_t1398180532_marshaled_pinvoke& marshaled);
extern "C" void IVRChaperone_t1398180532_marshal_pinvoke_back(const IVRChaperone_t1398180532_marshaled_pinvoke& marshaled, IVRChaperone_t1398180532& unmarshaled);
extern "C" void IVRChaperone_t1398180532_marshal_pinvoke_cleanup(IVRChaperone_t1398180532_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IVRChaperone_t1398180532;
struct IVRChaperone_t1398180532_marshaled_com;

extern "C" void IVRChaperone_t1398180532_marshal_com(const IVRChaperone_t1398180532& unmarshaled, IVRChaperone_t1398180532_marshaled_com& marshaled);
extern "C" void IVRChaperone_t1398180532_marshal_com_back(const IVRChaperone_t1398180532_marshaled_com& marshaled, IVRChaperone_t1398180532& unmarshaled);
extern "C" void IVRChaperone_t1398180532_marshal_com_cleanup(IVRChaperone_t1398180532_marshaled_com& marshaled);
