﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.FireSource
struct  FireSource_t179112773  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Valve.VR.InteractionSystem.FireSource::fireParticlePrefab
	GameObject_t1756533147 * ___fireParticlePrefab_2;
	// System.Boolean Valve.VR.InteractionSystem.FireSource::startActive
	bool ___startActive_3;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.FireSource::fireObject
	GameObject_t1756533147 * ___fireObject_4;
	// UnityEngine.ParticleSystem Valve.VR.InteractionSystem.FireSource::customParticles
	ParticleSystem_t3394631041 * ___customParticles_5;
	// System.Boolean Valve.VR.InteractionSystem.FireSource::isBurning
	bool ___isBurning_6;
	// System.Single Valve.VR.InteractionSystem.FireSource::burnTime
	float ___burnTime_7;
	// System.Single Valve.VR.InteractionSystem.FireSource::ignitionDelay
	float ___ignitionDelay_8;
	// System.Single Valve.VR.InteractionSystem.FireSource::ignitionTime
	float ___ignitionTime_9;
	// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.FireSource::hand
	Hand_t379716353 * ___hand_10;
	// UnityEngine.AudioSource Valve.VR.InteractionSystem.FireSource::ignitionSound
	AudioSource_t1135106623 * ___ignitionSound_11;
	// System.Boolean Valve.VR.InteractionSystem.FireSource::canSpreadFromThisSource
	bool ___canSpreadFromThisSource_12;

public:
	inline static int32_t get_offset_of_fireParticlePrefab_2() { return static_cast<int32_t>(offsetof(FireSource_t179112773, ___fireParticlePrefab_2)); }
	inline GameObject_t1756533147 * get_fireParticlePrefab_2() const { return ___fireParticlePrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_fireParticlePrefab_2() { return &___fireParticlePrefab_2; }
	inline void set_fireParticlePrefab_2(GameObject_t1756533147 * value)
	{
		___fireParticlePrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___fireParticlePrefab_2, value);
	}

	inline static int32_t get_offset_of_startActive_3() { return static_cast<int32_t>(offsetof(FireSource_t179112773, ___startActive_3)); }
	inline bool get_startActive_3() const { return ___startActive_3; }
	inline bool* get_address_of_startActive_3() { return &___startActive_3; }
	inline void set_startActive_3(bool value)
	{
		___startActive_3 = value;
	}

	inline static int32_t get_offset_of_fireObject_4() { return static_cast<int32_t>(offsetof(FireSource_t179112773, ___fireObject_4)); }
	inline GameObject_t1756533147 * get_fireObject_4() const { return ___fireObject_4; }
	inline GameObject_t1756533147 ** get_address_of_fireObject_4() { return &___fireObject_4; }
	inline void set_fireObject_4(GameObject_t1756533147 * value)
	{
		___fireObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___fireObject_4, value);
	}

	inline static int32_t get_offset_of_customParticles_5() { return static_cast<int32_t>(offsetof(FireSource_t179112773, ___customParticles_5)); }
	inline ParticleSystem_t3394631041 * get_customParticles_5() const { return ___customParticles_5; }
	inline ParticleSystem_t3394631041 ** get_address_of_customParticles_5() { return &___customParticles_5; }
	inline void set_customParticles_5(ParticleSystem_t3394631041 * value)
	{
		___customParticles_5 = value;
		Il2CppCodeGenWriteBarrier(&___customParticles_5, value);
	}

	inline static int32_t get_offset_of_isBurning_6() { return static_cast<int32_t>(offsetof(FireSource_t179112773, ___isBurning_6)); }
	inline bool get_isBurning_6() const { return ___isBurning_6; }
	inline bool* get_address_of_isBurning_6() { return &___isBurning_6; }
	inline void set_isBurning_6(bool value)
	{
		___isBurning_6 = value;
	}

	inline static int32_t get_offset_of_burnTime_7() { return static_cast<int32_t>(offsetof(FireSource_t179112773, ___burnTime_7)); }
	inline float get_burnTime_7() const { return ___burnTime_7; }
	inline float* get_address_of_burnTime_7() { return &___burnTime_7; }
	inline void set_burnTime_7(float value)
	{
		___burnTime_7 = value;
	}

	inline static int32_t get_offset_of_ignitionDelay_8() { return static_cast<int32_t>(offsetof(FireSource_t179112773, ___ignitionDelay_8)); }
	inline float get_ignitionDelay_8() const { return ___ignitionDelay_8; }
	inline float* get_address_of_ignitionDelay_8() { return &___ignitionDelay_8; }
	inline void set_ignitionDelay_8(float value)
	{
		___ignitionDelay_8 = value;
	}

	inline static int32_t get_offset_of_ignitionTime_9() { return static_cast<int32_t>(offsetof(FireSource_t179112773, ___ignitionTime_9)); }
	inline float get_ignitionTime_9() const { return ___ignitionTime_9; }
	inline float* get_address_of_ignitionTime_9() { return &___ignitionTime_9; }
	inline void set_ignitionTime_9(float value)
	{
		___ignitionTime_9 = value;
	}

	inline static int32_t get_offset_of_hand_10() { return static_cast<int32_t>(offsetof(FireSource_t179112773, ___hand_10)); }
	inline Hand_t379716353 * get_hand_10() const { return ___hand_10; }
	inline Hand_t379716353 ** get_address_of_hand_10() { return &___hand_10; }
	inline void set_hand_10(Hand_t379716353 * value)
	{
		___hand_10 = value;
		Il2CppCodeGenWriteBarrier(&___hand_10, value);
	}

	inline static int32_t get_offset_of_ignitionSound_11() { return static_cast<int32_t>(offsetof(FireSource_t179112773, ___ignitionSound_11)); }
	inline AudioSource_t1135106623 * get_ignitionSound_11() const { return ___ignitionSound_11; }
	inline AudioSource_t1135106623 ** get_address_of_ignitionSound_11() { return &___ignitionSound_11; }
	inline void set_ignitionSound_11(AudioSource_t1135106623 * value)
	{
		___ignitionSound_11 = value;
		Il2CppCodeGenWriteBarrier(&___ignitionSound_11, value);
	}

	inline static int32_t get_offset_of_canSpreadFromThisSource_12() { return static_cast<int32_t>(offsetof(FireSource_t179112773, ___canSpreadFromThisSource_12)); }
	inline bool get_canSpreadFromThisSource_12() const { return ___canSpreadFromThisSource_12; }
	inline bool* get_address_of_canSpreadFromThisSource_12() { return &___canSpreadFromThisSource_12; }
	inline void set_canSpreadFromThisSource_12(bool value)
	{
		___canSpreadFromThisSource_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
