﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// VRTK.VRTK_InteractGrab
struct VRTK_InteractGrab_t124353446;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction
struct  VRTK_BaseGrabAction_t4095736311  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_InteractableObject VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::grabbedObject
	VRTK_InteractableObject_t2604188111 * ___grabbedObject_2;
	// VRTK.VRTK_InteractGrab VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::primaryGrabbingObject
	VRTK_InteractGrab_t124353446 * ___primaryGrabbingObject_3;
	// VRTK.VRTK_InteractGrab VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::secondaryGrabbingObject
	VRTK_InteractGrab_t124353446 * ___secondaryGrabbingObject_4;
	// UnityEngine.Transform VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::primaryInitialGrabPoint
	Transform_t3275118058 * ___primaryInitialGrabPoint_5;
	// UnityEngine.Transform VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::secondaryInitialGrabPoint
	Transform_t3275118058 * ___secondaryInitialGrabPoint_6;
	// System.Boolean VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::initialised
	bool ___initialised_7;
	// System.Boolean VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::isActionable
	bool ___isActionable_8;
	// System.Boolean VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::isSwappable
	bool ___isSwappable_9;

public:
	inline static int32_t get_offset_of_grabbedObject_2() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAction_t4095736311, ___grabbedObject_2)); }
	inline VRTK_InteractableObject_t2604188111 * get_grabbedObject_2() const { return ___grabbedObject_2; }
	inline VRTK_InteractableObject_t2604188111 ** get_address_of_grabbedObject_2() { return &___grabbedObject_2; }
	inline void set_grabbedObject_2(VRTK_InteractableObject_t2604188111 * value)
	{
		___grabbedObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___grabbedObject_2, value);
	}

	inline static int32_t get_offset_of_primaryGrabbingObject_3() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAction_t4095736311, ___primaryGrabbingObject_3)); }
	inline VRTK_InteractGrab_t124353446 * get_primaryGrabbingObject_3() const { return ___primaryGrabbingObject_3; }
	inline VRTK_InteractGrab_t124353446 ** get_address_of_primaryGrabbingObject_3() { return &___primaryGrabbingObject_3; }
	inline void set_primaryGrabbingObject_3(VRTK_InteractGrab_t124353446 * value)
	{
		___primaryGrabbingObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___primaryGrabbingObject_3, value);
	}

	inline static int32_t get_offset_of_secondaryGrabbingObject_4() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAction_t4095736311, ___secondaryGrabbingObject_4)); }
	inline VRTK_InteractGrab_t124353446 * get_secondaryGrabbingObject_4() const { return ___secondaryGrabbingObject_4; }
	inline VRTK_InteractGrab_t124353446 ** get_address_of_secondaryGrabbingObject_4() { return &___secondaryGrabbingObject_4; }
	inline void set_secondaryGrabbingObject_4(VRTK_InteractGrab_t124353446 * value)
	{
		___secondaryGrabbingObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___secondaryGrabbingObject_4, value);
	}

	inline static int32_t get_offset_of_primaryInitialGrabPoint_5() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAction_t4095736311, ___primaryInitialGrabPoint_5)); }
	inline Transform_t3275118058 * get_primaryInitialGrabPoint_5() const { return ___primaryInitialGrabPoint_5; }
	inline Transform_t3275118058 ** get_address_of_primaryInitialGrabPoint_5() { return &___primaryInitialGrabPoint_5; }
	inline void set_primaryInitialGrabPoint_5(Transform_t3275118058 * value)
	{
		___primaryInitialGrabPoint_5 = value;
		Il2CppCodeGenWriteBarrier(&___primaryInitialGrabPoint_5, value);
	}

	inline static int32_t get_offset_of_secondaryInitialGrabPoint_6() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAction_t4095736311, ___secondaryInitialGrabPoint_6)); }
	inline Transform_t3275118058 * get_secondaryInitialGrabPoint_6() const { return ___secondaryInitialGrabPoint_6; }
	inline Transform_t3275118058 ** get_address_of_secondaryInitialGrabPoint_6() { return &___secondaryInitialGrabPoint_6; }
	inline void set_secondaryInitialGrabPoint_6(Transform_t3275118058 * value)
	{
		___secondaryInitialGrabPoint_6 = value;
		Il2CppCodeGenWriteBarrier(&___secondaryInitialGrabPoint_6, value);
	}

	inline static int32_t get_offset_of_initialised_7() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAction_t4095736311, ___initialised_7)); }
	inline bool get_initialised_7() const { return ___initialised_7; }
	inline bool* get_address_of_initialised_7() { return &___initialised_7; }
	inline void set_initialised_7(bool value)
	{
		___initialised_7 = value;
	}

	inline static int32_t get_offset_of_isActionable_8() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAction_t4095736311, ___isActionable_8)); }
	inline bool get_isActionable_8() const { return ___isActionable_8; }
	inline bool* get_address_of_isActionable_8() { return &___isActionable_8; }
	inline void set_isActionable_8(bool value)
	{
		___isActionable_8 = value;
	}

	inline static int32_t get_offset_of_isSwappable_9() { return static_cast<int32_t>(offsetof(VRTK_BaseGrabAction_t4095736311, ___isSwappable_9)); }
	inline bool get_isSwappable_9() const { return ___isSwappable_9; }
	inline bool* get_address_of_isSwappable_9() { return &___isSwappable_9; }
	inline void set_isSwappable_9(bool value)
	{
		___isSwappable_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
