﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_BodyPhysics
struct VRTK_BodyPhysics_t3414085265;
// VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t1260615097;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents
struct  VRTK_BodyPhysics_UnityEvents_t2330929566  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_BodyPhysics VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::bp
	VRTK_BodyPhysics_t3414085265 * ___bp_2;
	// VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::OnStartFalling
	UnityObjectEvent_t1260615097 * ___OnStartFalling_3;
	// VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::OnStopFalling
	UnityObjectEvent_t1260615097 * ___OnStopFalling_4;
	// VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::OnStartMoving
	UnityObjectEvent_t1260615097 * ___OnStartMoving_5;
	// VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::OnStopMoving
	UnityObjectEvent_t1260615097 * ___OnStopMoving_6;
	// VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::OnStartColliding
	UnityObjectEvent_t1260615097 * ___OnStartColliding_7;
	// VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents::OnStopColliding
	UnityObjectEvent_t1260615097 * ___OnStopColliding_8;

public:
	inline static int32_t get_offset_of_bp_2() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_UnityEvents_t2330929566, ___bp_2)); }
	inline VRTK_BodyPhysics_t3414085265 * get_bp_2() const { return ___bp_2; }
	inline VRTK_BodyPhysics_t3414085265 ** get_address_of_bp_2() { return &___bp_2; }
	inline void set_bp_2(VRTK_BodyPhysics_t3414085265 * value)
	{
		___bp_2 = value;
		Il2CppCodeGenWriteBarrier(&___bp_2, value);
	}

	inline static int32_t get_offset_of_OnStartFalling_3() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_UnityEvents_t2330929566, ___OnStartFalling_3)); }
	inline UnityObjectEvent_t1260615097 * get_OnStartFalling_3() const { return ___OnStartFalling_3; }
	inline UnityObjectEvent_t1260615097 ** get_address_of_OnStartFalling_3() { return &___OnStartFalling_3; }
	inline void set_OnStartFalling_3(UnityObjectEvent_t1260615097 * value)
	{
		___OnStartFalling_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnStartFalling_3, value);
	}

	inline static int32_t get_offset_of_OnStopFalling_4() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_UnityEvents_t2330929566, ___OnStopFalling_4)); }
	inline UnityObjectEvent_t1260615097 * get_OnStopFalling_4() const { return ___OnStopFalling_4; }
	inline UnityObjectEvent_t1260615097 ** get_address_of_OnStopFalling_4() { return &___OnStopFalling_4; }
	inline void set_OnStopFalling_4(UnityObjectEvent_t1260615097 * value)
	{
		___OnStopFalling_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnStopFalling_4, value);
	}

	inline static int32_t get_offset_of_OnStartMoving_5() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_UnityEvents_t2330929566, ___OnStartMoving_5)); }
	inline UnityObjectEvent_t1260615097 * get_OnStartMoving_5() const { return ___OnStartMoving_5; }
	inline UnityObjectEvent_t1260615097 ** get_address_of_OnStartMoving_5() { return &___OnStartMoving_5; }
	inline void set_OnStartMoving_5(UnityObjectEvent_t1260615097 * value)
	{
		___OnStartMoving_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnStartMoving_5, value);
	}

	inline static int32_t get_offset_of_OnStopMoving_6() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_UnityEvents_t2330929566, ___OnStopMoving_6)); }
	inline UnityObjectEvent_t1260615097 * get_OnStopMoving_6() const { return ___OnStopMoving_6; }
	inline UnityObjectEvent_t1260615097 ** get_address_of_OnStopMoving_6() { return &___OnStopMoving_6; }
	inline void set_OnStopMoving_6(UnityObjectEvent_t1260615097 * value)
	{
		___OnStopMoving_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnStopMoving_6, value);
	}

	inline static int32_t get_offset_of_OnStartColliding_7() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_UnityEvents_t2330929566, ___OnStartColliding_7)); }
	inline UnityObjectEvent_t1260615097 * get_OnStartColliding_7() const { return ___OnStartColliding_7; }
	inline UnityObjectEvent_t1260615097 ** get_address_of_OnStartColliding_7() { return &___OnStartColliding_7; }
	inline void set_OnStartColliding_7(UnityObjectEvent_t1260615097 * value)
	{
		___OnStartColliding_7 = value;
		Il2CppCodeGenWriteBarrier(&___OnStartColliding_7, value);
	}

	inline static int32_t get_offset_of_OnStopColliding_8() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_UnityEvents_t2330929566, ___OnStopColliding_8)); }
	inline UnityObjectEvent_t1260615097 * get_OnStopColliding_8() const { return ___OnStopColliding_8; }
	inline UnityObjectEvent_t1260615097 ** get_address_of_OnStopColliding_8() { return &___OnStopColliding_8; }
	inline void set_OnStopColliding_8(UnityObjectEvent_t1260615097 * value)
	{
		___OnStopColliding_8 = value;
		Il2CppCodeGenWriteBarrier(&___OnStopColliding_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
