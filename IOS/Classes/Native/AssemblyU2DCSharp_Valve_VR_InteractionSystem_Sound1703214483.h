﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t2203355011;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.SoundPlayOneshot
struct  SoundPlayOneshot_t1703214483  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioClip[] Valve.VR.InteractionSystem.SoundPlayOneshot::waveFiles
	AudioClipU5BU5D_t2203355011* ___waveFiles_2;
	// UnityEngine.AudioSource Valve.VR.InteractionSystem.SoundPlayOneshot::thisAudioSource
	AudioSource_t1135106623 * ___thisAudioSource_3;
	// System.Single Valve.VR.InteractionSystem.SoundPlayOneshot::volMin
	float ___volMin_4;
	// System.Single Valve.VR.InteractionSystem.SoundPlayOneshot::volMax
	float ___volMax_5;
	// System.Single Valve.VR.InteractionSystem.SoundPlayOneshot::pitchMin
	float ___pitchMin_6;
	// System.Single Valve.VR.InteractionSystem.SoundPlayOneshot::pitchMax
	float ___pitchMax_7;
	// System.Boolean Valve.VR.InteractionSystem.SoundPlayOneshot::playOnAwake
	bool ___playOnAwake_8;

public:
	inline static int32_t get_offset_of_waveFiles_2() { return static_cast<int32_t>(offsetof(SoundPlayOneshot_t1703214483, ___waveFiles_2)); }
	inline AudioClipU5BU5D_t2203355011* get_waveFiles_2() const { return ___waveFiles_2; }
	inline AudioClipU5BU5D_t2203355011** get_address_of_waveFiles_2() { return &___waveFiles_2; }
	inline void set_waveFiles_2(AudioClipU5BU5D_t2203355011* value)
	{
		___waveFiles_2 = value;
		Il2CppCodeGenWriteBarrier(&___waveFiles_2, value);
	}

	inline static int32_t get_offset_of_thisAudioSource_3() { return static_cast<int32_t>(offsetof(SoundPlayOneshot_t1703214483, ___thisAudioSource_3)); }
	inline AudioSource_t1135106623 * get_thisAudioSource_3() const { return ___thisAudioSource_3; }
	inline AudioSource_t1135106623 ** get_address_of_thisAudioSource_3() { return &___thisAudioSource_3; }
	inline void set_thisAudioSource_3(AudioSource_t1135106623 * value)
	{
		___thisAudioSource_3 = value;
		Il2CppCodeGenWriteBarrier(&___thisAudioSource_3, value);
	}

	inline static int32_t get_offset_of_volMin_4() { return static_cast<int32_t>(offsetof(SoundPlayOneshot_t1703214483, ___volMin_4)); }
	inline float get_volMin_4() const { return ___volMin_4; }
	inline float* get_address_of_volMin_4() { return &___volMin_4; }
	inline void set_volMin_4(float value)
	{
		___volMin_4 = value;
	}

	inline static int32_t get_offset_of_volMax_5() { return static_cast<int32_t>(offsetof(SoundPlayOneshot_t1703214483, ___volMax_5)); }
	inline float get_volMax_5() const { return ___volMax_5; }
	inline float* get_address_of_volMax_5() { return &___volMax_5; }
	inline void set_volMax_5(float value)
	{
		___volMax_5 = value;
	}

	inline static int32_t get_offset_of_pitchMin_6() { return static_cast<int32_t>(offsetof(SoundPlayOneshot_t1703214483, ___pitchMin_6)); }
	inline float get_pitchMin_6() const { return ___pitchMin_6; }
	inline float* get_address_of_pitchMin_6() { return &___pitchMin_6; }
	inline void set_pitchMin_6(float value)
	{
		___pitchMin_6 = value;
	}

	inline static int32_t get_offset_of_pitchMax_7() { return static_cast<int32_t>(offsetof(SoundPlayOneshot_t1703214483, ___pitchMax_7)); }
	inline float get_pitchMax_7() const { return ___pitchMax_7; }
	inline float* get_address_of_pitchMax_7() { return &___pitchMax_7; }
	inline void set_pitchMax_7(float value)
	{
		___pitchMax_7 = value;
	}

	inline static int32_t get_offset_of_playOnAwake_8() { return static_cast<int32_t>(offsetof(SoundPlayOneshot_t1703214483, ___playOnAwake_8)); }
	inline bool get_playOnAwake_8() const { return ___playOnAwake_8; }
	inline bool* get_address_of_playOnAwake_8() { return &___playOnAwake_8; }
	inline void set_playOnAwake_8(bool value)
	{
		___playOnAwake_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
