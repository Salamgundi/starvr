﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Valve.VR.EVREventType>
struct DefaultComparer_t1628777036;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREventType6846875.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Valve.VR.EVREventType>::.ctor()
extern "C"  void DefaultComparer__ctor_m2712117229_gshared (DefaultComparer_t1628777036 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2712117229(__this, method) ((  void (*) (DefaultComparer_t1628777036 *, const MethodInfo*))DefaultComparer__ctor_m2712117229_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Valve.VR.EVREventType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1959868126_gshared (DefaultComparer_t1628777036 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1959868126(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1628777036 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1959868126_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Valve.VR.EVREventType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1153017734_gshared (DefaultComparer_t1628777036 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1153017734(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1628777036 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1153017734_gshared)(__this, ___x0, ___y1, method)
