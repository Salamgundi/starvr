﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRScreenshots/_UpdateScreenshotProgress
struct _UpdateScreenshotProgress_t2161609358;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotError1400268927.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRScreenshots/_UpdateScreenshotProgress::.ctor(System.Object,System.IntPtr)
extern "C"  void _UpdateScreenshotProgress__ctor_m1711528145 (_UpdateScreenshotProgress_t2161609358 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_UpdateScreenshotProgress::Invoke(System.UInt32,System.Single)
extern "C"  int32_t _UpdateScreenshotProgress_Invoke_m851287384 (_UpdateScreenshotProgress_t2161609358 * __this, uint32_t ___screenshotHandle0, float ___flProgress1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRScreenshots/_UpdateScreenshotProgress::BeginInvoke(System.UInt32,System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _UpdateScreenshotProgress_BeginInvoke_m694852103 (_UpdateScreenshotProgress_t2161609358 * __this, uint32_t ___screenshotHandle0, float ___flProgress1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_UpdateScreenshotProgress::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _UpdateScreenshotProgress_EndInvoke_m3851553093 (_UpdateScreenshotProgress_t2161609358 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
