﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_BasePointer/PointerOriginSmoothingSettings
struct PointerOriginSmoothingSettings_t2805273516;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_BasePointer/PointerOriginSmoothingSettings::.ctor()
extern "C"  void PointerOriginSmoothingSettings__ctor_m1003262875 (PointerOriginSmoothingSettings_t2805273516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
