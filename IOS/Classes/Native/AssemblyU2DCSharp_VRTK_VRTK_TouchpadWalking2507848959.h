﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.ControllerInteractionEventHandler
struct ControllerInteractionEventHandler_t343979916;
// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// VRTK.VRTK_BodyPhysics
struct VRTK_BodyPhysics_t3414085265;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DeviceFinder_Devices2408891389.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_TouchpadWalking
struct  VRTK_TouchpadWalking_t2507848959  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.VRTK_TouchpadWalking::leftController
	bool ___leftController_2;
	// System.Boolean VRTK.VRTK_TouchpadWalking::rightController
	bool ___rightController_3;
	// System.Single VRTK.VRTK_TouchpadWalking::maxWalkSpeed
	float ___maxWalkSpeed_4;
	// System.Single VRTK.VRTK_TouchpadWalking::deceleration
	float ___deceleration_5;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_TouchpadWalking::moveOnButtonPress
	int32_t ___moveOnButtonPress_6;
	// VRTK.VRTK_DeviceFinder/Devices VRTK.VRTK_TouchpadWalking::deviceForDirection
	int32_t ___deviceForDirection_7;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_TouchpadWalking::speedMultiplierButton
	int32_t ___speedMultiplierButton_8;
	// System.Single VRTK.VRTK_TouchpadWalking::speedMultiplier
	float ___speedMultiplier_9;
	// UnityEngine.GameObject VRTK.VRTK_TouchpadWalking::controllerLeftHand
	GameObject_t1756533147 * ___controllerLeftHand_10;
	// UnityEngine.GameObject VRTK.VRTK_TouchpadWalking::controllerRightHand
	GameObject_t1756533147 * ___controllerRightHand_11;
	// UnityEngine.Transform VRTK.VRTK_TouchpadWalking::playArea
	Transform_t3275118058 * ___playArea_12;
	// UnityEngine.Vector2 VRTK.VRTK_TouchpadWalking::touchAxis
	Vector2_t2243707579  ___touchAxis_13;
	// System.Single VRTK.VRTK_TouchpadWalking::movementSpeed
	float ___movementSpeed_14;
	// System.Single VRTK.VRTK_TouchpadWalking::strafeSpeed
	float ___strafeSpeed_15;
	// System.Boolean VRTK.VRTK_TouchpadWalking::leftSubscribed
	bool ___leftSubscribed_16;
	// System.Boolean VRTK.VRTK_TouchpadWalking::rightSubscribed
	bool ___rightSubscribed_17;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_TouchpadWalking::touchpadAxisChanged
	ControllerInteractionEventHandler_t343979916 * ___touchpadAxisChanged_18;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_TouchpadWalking::touchpadUntouched
	ControllerInteractionEventHandler_t343979916 * ___touchpadUntouched_19;
	// System.Boolean VRTK.VRTK_TouchpadWalking::multiplySpeed
	bool ___multiplySpeed_20;
	// VRTK.VRTK_ControllerEvents VRTK.VRTK_TouchpadWalking::controllerEvents
	VRTK_ControllerEvents_t3225224819 * ___controllerEvents_21;
	// VRTK.VRTK_BodyPhysics VRTK.VRTK_TouchpadWalking::bodyPhysics
	VRTK_BodyPhysics_t3414085265 * ___bodyPhysics_22;
	// System.Boolean VRTK.VRTK_TouchpadWalking::wasFalling
	bool ___wasFalling_23;
	// System.Boolean VRTK.VRTK_TouchpadWalking::previousLeftControllerState
	bool ___previousLeftControllerState_24;
	// System.Boolean VRTK.VRTK_TouchpadWalking::previousRightControllerState
	bool ___previousRightControllerState_25;

public:
	inline static int32_t get_offset_of_leftController_2() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___leftController_2)); }
	inline bool get_leftController_2() const { return ___leftController_2; }
	inline bool* get_address_of_leftController_2() { return &___leftController_2; }
	inline void set_leftController_2(bool value)
	{
		___leftController_2 = value;
	}

	inline static int32_t get_offset_of_rightController_3() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___rightController_3)); }
	inline bool get_rightController_3() const { return ___rightController_3; }
	inline bool* get_address_of_rightController_3() { return &___rightController_3; }
	inline void set_rightController_3(bool value)
	{
		___rightController_3 = value;
	}

	inline static int32_t get_offset_of_maxWalkSpeed_4() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___maxWalkSpeed_4)); }
	inline float get_maxWalkSpeed_4() const { return ___maxWalkSpeed_4; }
	inline float* get_address_of_maxWalkSpeed_4() { return &___maxWalkSpeed_4; }
	inline void set_maxWalkSpeed_4(float value)
	{
		___maxWalkSpeed_4 = value;
	}

	inline static int32_t get_offset_of_deceleration_5() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___deceleration_5)); }
	inline float get_deceleration_5() const { return ___deceleration_5; }
	inline float* get_address_of_deceleration_5() { return &___deceleration_5; }
	inline void set_deceleration_5(float value)
	{
		___deceleration_5 = value;
	}

	inline static int32_t get_offset_of_moveOnButtonPress_6() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___moveOnButtonPress_6)); }
	inline int32_t get_moveOnButtonPress_6() const { return ___moveOnButtonPress_6; }
	inline int32_t* get_address_of_moveOnButtonPress_6() { return &___moveOnButtonPress_6; }
	inline void set_moveOnButtonPress_6(int32_t value)
	{
		___moveOnButtonPress_6 = value;
	}

	inline static int32_t get_offset_of_deviceForDirection_7() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___deviceForDirection_7)); }
	inline int32_t get_deviceForDirection_7() const { return ___deviceForDirection_7; }
	inline int32_t* get_address_of_deviceForDirection_7() { return &___deviceForDirection_7; }
	inline void set_deviceForDirection_7(int32_t value)
	{
		___deviceForDirection_7 = value;
	}

	inline static int32_t get_offset_of_speedMultiplierButton_8() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___speedMultiplierButton_8)); }
	inline int32_t get_speedMultiplierButton_8() const { return ___speedMultiplierButton_8; }
	inline int32_t* get_address_of_speedMultiplierButton_8() { return &___speedMultiplierButton_8; }
	inline void set_speedMultiplierButton_8(int32_t value)
	{
		___speedMultiplierButton_8 = value;
	}

	inline static int32_t get_offset_of_speedMultiplier_9() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___speedMultiplier_9)); }
	inline float get_speedMultiplier_9() const { return ___speedMultiplier_9; }
	inline float* get_address_of_speedMultiplier_9() { return &___speedMultiplier_9; }
	inline void set_speedMultiplier_9(float value)
	{
		___speedMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_controllerLeftHand_10() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___controllerLeftHand_10)); }
	inline GameObject_t1756533147 * get_controllerLeftHand_10() const { return ___controllerLeftHand_10; }
	inline GameObject_t1756533147 ** get_address_of_controllerLeftHand_10() { return &___controllerLeftHand_10; }
	inline void set_controllerLeftHand_10(GameObject_t1756533147 * value)
	{
		___controllerLeftHand_10 = value;
		Il2CppCodeGenWriteBarrier(&___controllerLeftHand_10, value);
	}

	inline static int32_t get_offset_of_controllerRightHand_11() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___controllerRightHand_11)); }
	inline GameObject_t1756533147 * get_controllerRightHand_11() const { return ___controllerRightHand_11; }
	inline GameObject_t1756533147 ** get_address_of_controllerRightHand_11() { return &___controllerRightHand_11; }
	inline void set_controllerRightHand_11(GameObject_t1756533147 * value)
	{
		___controllerRightHand_11 = value;
		Il2CppCodeGenWriteBarrier(&___controllerRightHand_11, value);
	}

	inline static int32_t get_offset_of_playArea_12() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___playArea_12)); }
	inline Transform_t3275118058 * get_playArea_12() const { return ___playArea_12; }
	inline Transform_t3275118058 ** get_address_of_playArea_12() { return &___playArea_12; }
	inline void set_playArea_12(Transform_t3275118058 * value)
	{
		___playArea_12 = value;
		Il2CppCodeGenWriteBarrier(&___playArea_12, value);
	}

	inline static int32_t get_offset_of_touchAxis_13() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___touchAxis_13)); }
	inline Vector2_t2243707579  get_touchAxis_13() const { return ___touchAxis_13; }
	inline Vector2_t2243707579 * get_address_of_touchAxis_13() { return &___touchAxis_13; }
	inline void set_touchAxis_13(Vector2_t2243707579  value)
	{
		___touchAxis_13 = value;
	}

	inline static int32_t get_offset_of_movementSpeed_14() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___movementSpeed_14)); }
	inline float get_movementSpeed_14() const { return ___movementSpeed_14; }
	inline float* get_address_of_movementSpeed_14() { return &___movementSpeed_14; }
	inline void set_movementSpeed_14(float value)
	{
		___movementSpeed_14 = value;
	}

	inline static int32_t get_offset_of_strafeSpeed_15() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___strafeSpeed_15)); }
	inline float get_strafeSpeed_15() const { return ___strafeSpeed_15; }
	inline float* get_address_of_strafeSpeed_15() { return &___strafeSpeed_15; }
	inline void set_strafeSpeed_15(float value)
	{
		___strafeSpeed_15 = value;
	}

	inline static int32_t get_offset_of_leftSubscribed_16() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___leftSubscribed_16)); }
	inline bool get_leftSubscribed_16() const { return ___leftSubscribed_16; }
	inline bool* get_address_of_leftSubscribed_16() { return &___leftSubscribed_16; }
	inline void set_leftSubscribed_16(bool value)
	{
		___leftSubscribed_16 = value;
	}

	inline static int32_t get_offset_of_rightSubscribed_17() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___rightSubscribed_17)); }
	inline bool get_rightSubscribed_17() const { return ___rightSubscribed_17; }
	inline bool* get_address_of_rightSubscribed_17() { return &___rightSubscribed_17; }
	inline void set_rightSubscribed_17(bool value)
	{
		___rightSubscribed_17 = value;
	}

	inline static int32_t get_offset_of_touchpadAxisChanged_18() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___touchpadAxisChanged_18)); }
	inline ControllerInteractionEventHandler_t343979916 * get_touchpadAxisChanged_18() const { return ___touchpadAxisChanged_18; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_touchpadAxisChanged_18() { return &___touchpadAxisChanged_18; }
	inline void set_touchpadAxisChanged_18(ControllerInteractionEventHandler_t343979916 * value)
	{
		___touchpadAxisChanged_18 = value;
		Il2CppCodeGenWriteBarrier(&___touchpadAxisChanged_18, value);
	}

	inline static int32_t get_offset_of_touchpadUntouched_19() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___touchpadUntouched_19)); }
	inline ControllerInteractionEventHandler_t343979916 * get_touchpadUntouched_19() const { return ___touchpadUntouched_19; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_touchpadUntouched_19() { return &___touchpadUntouched_19; }
	inline void set_touchpadUntouched_19(ControllerInteractionEventHandler_t343979916 * value)
	{
		___touchpadUntouched_19 = value;
		Il2CppCodeGenWriteBarrier(&___touchpadUntouched_19, value);
	}

	inline static int32_t get_offset_of_multiplySpeed_20() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___multiplySpeed_20)); }
	inline bool get_multiplySpeed_20() const { return ___multiplySpeed_20; }
	inline bool* get_address_of_multiplySpeed_20() { return &___multiplySpeed_20; }
	inline void set_multiplySpeed_20(bool value)
	{
		___multiplySpeed_20 = value;
	}

	inline static int32_t get_offset_of_controllerEvents_21() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___controllerEvents_21)); }
	inline VRTK_ControllerEvents_t3225224819 * get_controllerEvents_21() const { return ___controllerEvents_21; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_controllerEvents_21() { return &___controllerEvents_21; }
	inline void set_controllerEvents_21(VRTK_ControllerEvents_t3225224819 * value)
	{
		___controllerEvents_21 = value;
		Il2CppCodeGenWriteBarrier(&___controllerEvents_21, value);
	}

	inline static int32_t get_offset_of_bodyPhysics_22() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___bodyPhysics_22)); }
	inline VRTK_BodyPhysics_t3414085265 * get_bodyPhysics_22() const { return ___bodyPhysics_22; }
	inline VRTK_BodyPhysics_t3414085265 ** get_address_of_bodyPhysics_22() { return &___bodyPhysics_22; }
	inline void set_bodyPhysics_22(VRTK_BodyPhysics_t3414085265 * value)
	{
		___bodyPhysics_22 = value;
		Il2CppCodeGenWriteBarrier(&___bodyPhysics_22, value);
	}

	inline static int32_t get_offset_of_wasFalling_23() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___wasFalling_23)); }
	inline bool get_wasFalling_23() const { return ___wasFalling_23; }
	inline bool* get_address_of_wasFalling_23() { return &___wasFalling_23; }
	inline void set_wasFalling_23(bool value)
	{
		___wasFalling_23 = value;
	}

	inline static int32_t get_offset_of_previousLeftControllerState_24() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___previousLeftControllerState_24)); }
	inline bool get_previousLeftControllerState_24() const { return ___previousLeftControllerState_24; }
	inline bool* get_address_of_previousLeftControllerState_24() { return &___previousLeftControllerState_24; }
	inline void set_previousLeftControllerState_24(bool value)
	{
		___previousLeftControllerState_24 = value;
	}

	inline static int32_t get_offset_of_previousRightControllerState_25() { return static_cast<int32_t>(offsetof(VRTK_TouchpadWalking_t2507848959, ___previousRightControllerState_25)); }
	inline bool get_previousRightControllerState_25() const { return ___previousRightControllerState_25; }
	inline bool* get_address_of_previousRightControllerState_25() { return &___previousRightControllerState_25; }
	inline void set_previousRightControllerState_25(bool value)
	{
		___previousRightControllerState_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
