﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_GetRenderModelName
struct _GetRenderModelName_t4149685257;
// System.Object
struct Il2CppObject;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_GetRenderModelName::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetRenderModelName__ctor_m1619642450 (_GetRenderModelName_t4149685257 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelName::Invoke(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t _GetRenderModelName_Invoke_m858619975 (_GetRenderModelName_t4149685257 * __this, uint32_t ___unRenderModelIndex0, StringBuilder_t1221177846 * ___pchRenderModelName1, uint32_t ___unRenderModelNameLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetRenderModelName::BeginInvoke(System.UInt32,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetRenderModelName_BeginInvoke_m2011707583 (_GetRenderModelName_t4149685257 * __this, uint32_t ___unRenderModelIndex0, StringBuilder_t1221177846 * ___pchRenderModelName1, uint32_t ___unRenderModelNameLen2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelName::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetRenderModelName_EndInvoke_m475684905 (_GetRenderModelName_t4149685257 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
