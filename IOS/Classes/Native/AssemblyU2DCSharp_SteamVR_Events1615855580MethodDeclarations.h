﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Action
struct Action_t1836998693;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t897193173;
// UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>
struct UnityAction_2_t41828916;
// UnityEngine.Events.UnityAction`3<UnityEngine.Color,System.Single,System.Boolean>
struct UnityAction_3_t1816056522;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t3443095683;
// UnityEngine.Events.UnityAction`1<Valve.VR.TrackedDevicePose_t[]>
struct UnityAction_1_t4263857800;
// UnityEngine.Events.UnityAction`2<SteamVR_RenderModel,System.Boolean>
struct UnityAction_2_t4073506138;
// SteamVR_Events/Event`1<Valve.VR.VREvent_t>
struct Event_1_t1285721510;
// UnityEngine.Events.UnityAction`1<Valve.VR.VREvent_t>
struct UnityAction_1_t476884844;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREventType6846875.h"

// SteamVR_Events/Action SteamVR_Events::CalibratingAction(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern "C"  Action_t1836998693 * SteamVR_Events_CalibratingAction_m1760947898 (Il2CppObject * __this /* static, unused */, UnityAction_1_t897193173 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action SteamVR_Events::DeviceConnectedAction(UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>)
extern "C"  Action_t1836998693 * SteamVR_Events_DeviceConnectedAction_m2873017197 (Il2CppObject * __this /* static, unused */, UnityAction_2_t41828916 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action SteamVR_Events::FadeAction(UnityEngine.Events.UnityAction`3<UnityEngine.Color,System.Single,System.Boolean>)
extern "C"  Action_t1836998693 * SteamVR_Events_FadeAction_m2903191571 (Il2CppObject * __this /* static, unused */, UnityAction_3_t1816056522 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action SteamVR_Events::FadeReadyAction(UnityEngine.Events.UnityAction)
extern "C"  Action_t1836998693 * SteamVR_Events_FadeReadyAction_m3039585765 (Il2CppObject * __this /* static, unused */, UnityAction_t4025899511 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action SteamVR_Events::HideRenderModelsAction(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern "C"  Action_t1836998693 * SteamVR_Events_HideRenderModelsAction_m1062526998 (Il2CppObject * __this /* static, unused */, UnityAction_1_t897193173 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action SteamVR_Events::InitializingAction(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern "C"  Action_t1836998693 * SteamVR_Events_InitializingAction_m3428688473 (Il2CppObject * __this /* static, unused */, UnityAction_1_t897193173 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action SteamVR_Events::InputFocusAction(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern "C"  Action_t1836998693 * SteamVR_Events_InputFocusAction_m2478499368 (Il2CppObject * __this /* static, unused */, UnityAction_1_t897193173 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action SteamVR_Events::LoadingAction(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern "C"  Action_t1836998693 * SteamVR_Events_LoadingAction_m292546548 (Il2CppObject * __this /* static, unused */, UnityAction_1_t897193173 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action SteamVR_Events::LoadingFadeInAction(UnityEngine.Events.UnityAction`1<System.Single>)
extern "C"  Action_t1836998693 * SteamVR_Events_LoadingFadeInAction_m4141659653 (Il2CppObject * __this /* static, unused */, UnityAction_1_t3443095683 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action SteamVR_Events::LoadingFadeOutAction(UnityEngine.Events.UnityAction`1<System.Single>)
extern "C"  Action_t1836998693 * SteamVR_Events_LoadingFadeOutAction_m4053188812 (Il2CppObject * __this /* static, unused */, UnityAction_1_t3443095683 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action SteamVR_Events::NewPosesAction(UnityEngine.Events.UnityAction`1<Valve.VR.TrackedDevicePose_t[]>)
extern "C"  Action_t1836998693 * SteamVR_Events_NewPosesAction_m212074495 (Il2CppObject * __this /* static, unused */, UnityAction_1_t4263857800 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action SteamVR_Events::NewPosesAppliedAction(UnityEngine.Events.UnityAction)
extern "C"  Action_t1836998693 * SteamVR_Events_NewPosesAppliedAction_m1432558815 (Il2CppObject * __this /* static, unused */, UnityAction_t4025899511 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action SteamVR_Events::OutOfRangeAction(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern "C"  Action_t1836998693 * SteamVR_Events_OutOfRangeAction_m3482130582 (Il2CppObject * __this /* static, unused */, UnityAction_1_t897193173 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action SteamVR_Events::RenderModelLoadedAction(UnityEngine.Events.UnityAction`2<SteamVR_RenderModel,System.Boolean>)
extern "C"  Action_t1836998693 * SteamVR_Events_RenderModelLoadedAction_m2358411313 (Il2CppObject * __this /* static, unused */, UnityAction_2_t4073506138 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Event`1<Valve.VR.VREvent_t> SteamVR_Events::System(Valve.VR.EVREventType)
extern "C"  Event_1_t1285721510 * SteamVR_Events_System_m2241539747 (Il2CppObject * __this /* static, unused */, int32_t ___eventType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action SteamVR_Events::SystemAction(Valve.VR.EVREventType,UnityEngine.Events.UnityAction`1<Valve.VR.VREvent_t>)
extern "C"  Action_t1836998693 * SteamVR_Events_SystemAction_m3086779464 (Il2CppObject * __this /* static, unused */, int32_t ___eventType0, UnityAction_1_t476884844 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Events::.cctor()
extern "C"  void SteamVR_Events__cctor_m4119045590 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
