﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Object>
struct AdaptiveSetting_1_t1362001090;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Object>::.ctor(T,System.Int32,System.Int32)
extern "C"  void AdaptiveSetting_1__ctor_m1116240787_gshared (AdaptiveSetting_1_t1362001090 * __this, Il2CppObject * ___currentValue0, int32_t ___increaseFrameCost1, int32_t ___decreaseFrameCost2, const MethodInfo* method);
#define AdaptiveSetting_1__ctor_m1116240787(__this, ___currentValue0, ___increaseFrameCost1, ___decreaseFrameCost2, method) ((  void (*) (AdaptiveSetting_1_t1362001090 *, Il2CppObject *, int32_t, int32_t, const MethodInfo*))AdaptiveSetting_1__ctor_m1116240787_gshared)(__this, ___currentValue0, ___increaseFrameCost1, ___decreaseFrameCost2, method)
// T VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Object>::get_currentValue()
extern "C"  Il2CppObject * AdaptiveSetting_1_get_currentValue_m3267241515_gshared (AdaptiveSetting_1_t1362001090 * __this, const MethodInfo* method);
#define AdaptiveSetting_1_get_currentValue_m3267241515(__this, method) ((  Il2CppObject * (*) (AdaptiveSetting_1_t1362001090 *, const MethodInfo*))AdaptiveSetting_1_get_currentValue_m3267241515_gshared)(__this, method)
// System.Void VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Object>::set_currentValue(T)
extern "C"  void AdaptiveSetting_1_set_currentValue_m3193020840_gshared (AdaptiveSetting_1_t1362001090 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define AdaptiveSetting_1_set_currentValue_m3193020840(__this, ___value0, method) ((  void (*) (AdaptiveSetting_1_t1362001090 *, Il2CppObject *, const MethodInfo*))AdaptiveSetting_1_set_currentValue_m3193020840_gshared)(__this, ___value0, method)
// T VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Object>::get_previousValue()
extern "C"  Il2CppObject * AdaptiveSetting_1_get_previousValue_m159270639_gshared (AdaptiveSetting_1_t1362001090 * __this, const MethodInfo* method);
#define AdaptiveSetting_1_get_previousValue_m159270639(__this, method) ((  Il2CppObject * (*) (AdaptiveSetting_1_t1362001090 *, const MethodInfo*))AdaptiveSetting_1_get_previousValue_m159270639_gshared)(__this, method)
// System.Void VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Object>::set_previousValue(T)
extern "C"  void AdaptiveSetting_1_set_previousValue_m2161578250_gshared (AdaptiveSetting_1_t1362001090 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define AdaptiveSetting_1_set_previousValue_m2161578250(__this, ___value0, method) ((  void (*) (AdaptiveSetting_1_t1362001090 *, Il2CppObject *, const MethodInfo*))AdaptiveSetting_1_set_previousValue_m2161578250_gshared)(__this, ___value0, method)
// System.Int32 VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Object>::get_lastChangeFrameCount()
extern "C"  int32_t AdaptiveSetting_1_get_lastChangeFrameCount_m3750933022_gshared (AdaptiveSetting_1_t1362001090 * __this, const MethodInfo* method);
#define AdaptiveSetting_1_get_lastChangeFrameCount_m3750933022(__this, method) ((  int32_t (*) (AdaptiveSetting_1_t1362001090 *, const MethodInfo*))AdaptiveSetting_1_get_lastChangeFrameCount_m3750933022_gshared)(__this, method)
// System.Void VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Object>::set_lastChangeFrameCount(System.Int32)
extern "C"  void AdaptiveSetting_1_set_lastChangeFrameCount_m2343760585_gshared (AdaptiveSetting_1_t1362001090 * __this, int32_t ___value0, const MethodInfo* method);
#define AdaptiveSetting_1_set_lastChangeFrameCount_m2343760585(__this, ___value0, method) ((  void (*) (AdaptiveSetting_1_t1362001090 *, int32_t, const MethodInfo*))AdaptiveSetting_1_set_lastChangeFrameCount_m2343760585_gshared)(__this, ___value0, method)
