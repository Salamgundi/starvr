﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Archery.BowAnimation
struct BowAnimation_t4022033600;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.Examples.Archery.BowAnimation::.ctor()
extern "C"  void BowAnimation__ctor_m3676030427 (BowAnimation_t4022033600 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.BowAnimation::SetFrame(System.Single)
extern "C"  void BowAnimation_SetFrame_m2671828033 (BowAnimation_t4022033600 * __this, float ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
