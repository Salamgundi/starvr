﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_HideKeyboard
struct _HideKeyboard_t3483797360;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_HideKeyboard::.ctor(System.Object,System.IntPtr)
extern "C"  void _HideKeyboard__ctor_m877884957 (_HideKeyboard_t3483797360 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVROverlay/_HideKeyboard::Invoke()
extern "C"  void _HideKeyboard_Invoke_m2823902165 (_HideKeyboard_t3483797360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_HideKeyboard::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _HideKeyboard_BeginInvoke_m2730127166 (_HideKeyboard_t3483797360 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVROverlay/_HideKeyboard::EndInvoke(System.IAsyncResult)
extern "C"  void _HideKeyboard_EndInvoke_m3095483483 (_HideKeyboard_t3483797360 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
