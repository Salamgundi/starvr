﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.String UnityEngine.VR.VRDevice::get_model()
extern "C"  String_t* VRDevice_get_model_m3040919402 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.VR.VRDevice::get_refreshRate()
extern "C"  float VRDevice_get_refreshRate_m300036111 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.VR.VRDevice::GetNativePtr()
extern "C"  IntPtr_t VRDevice_GetNativePtr_m1938964139 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.VRDevice::INTERNAL_CALL_GetNativePtr(System.IntPtr&)
extern "C"  void VRDevice_INTERNAL_CALL_GetNativePtr_m1786150651 (Il2CppObject * __this /* static, unused */, IntPtr_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
