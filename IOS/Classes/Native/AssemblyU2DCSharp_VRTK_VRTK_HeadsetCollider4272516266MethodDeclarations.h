﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_HeadsetCollider
struct VRTK_HeadsetCollider_t4272516266;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_PolicyList
struct VRTK_PolicyList_t2965133344;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PolicyList2965133344.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventArgs1242373387.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void VRTK.VRTK_HeadsetCollider::.ctor()
extern "C"  void VRTK_HeadsetCollider__ctor_m4114352088 (VRTK_HeadsetCollider_t4272516266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollider::SetParent(UnityEngine.GameObject)
extern "C"  void VRTK_HeadsetCollider_SetParent_m2288106662 (VRTK_HeadsetCollider_t4272516266 * __this, GameObject_t1756533147 * ___setParent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollider::SetIgnoreTarget(VRTK.VRTK_PolicyList)
extern "C"  void VRTK_HeadsetCollider_SetIgnoreTarget_m1408993278 (VRTK_HeadsetCollider_t4272516266 * __this, VRTK_PolicyList_t2965133344 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollider::EndCollision(UnityEngine.Collider)
extern "C"  void VRTK_HeadsetCollider_EndCollision_m2096700764 (VRTK_HeadsetCollider_t4272516266 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollider::OnTriggerStay(UnityEngine.Collider)
extern "C"  void VRTK_HeadsetCollider_OnTriggerStay_m3549112013 (VRTK_HeadsetCollider_t4272516266 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollider::OnTriggerExit(UnityEngine.Collider)
extern "C"  void VRTK_HeadsetCollider_OnTriggerExit_m2020059534 (VRTK_HeadsetCollider_t4272516266 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollider::Update()
extern "C"  void VRTK_HeadsetCollider_Update_m878622043 (VRTK_HeadsetCollider_t4272516266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.HeadsetCollisionEventArgs VRTK.VRTK_HeadsetCollider::SetHeadsetCollisionEvent(UnityEngine.Collider,UnityEngine.Transform)
extern "C"  HeadsetCollisionEventArgs_t1242373387  VRTK_HeadsetCollider_SetHeadsetCollisionEvent_m1556365831 (VRTK_HeadsetCollider_t4272516266 * __this, Collider_t3497673348 * ___collider0, Transform_t3275118058 * ___currentTransform1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_HeadsetCollider::ValidTarget(UnityEngine.Transform)
extern "C"  bool VRTK_HeadsetCollider_ValidTarget_m2256856258 (VRTK_HeadsetCollider_t4272516266 * __this, Transform_t3275118058 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
