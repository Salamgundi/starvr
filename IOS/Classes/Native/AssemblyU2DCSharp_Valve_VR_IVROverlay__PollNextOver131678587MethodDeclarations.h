﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_PollNextOverlayEvent
struct _PollNextOverlayEvent_t131678587;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_t3405266389.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_PollNextOverlayEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void _PollNextOverlayEvent__ctor_m3491457060 (_PollNextOverlayEvent_t131678587 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVROverlay/_PollNextOverlayEvent::Invoke(System.UInt64,Valve.VR.VREvent_t&,System.UInt32)
extern "C"  bool _PollNextOverlayEvent_Invoke_m3241784284 (_PollNextOverlayEvent_t131678587 * __this, uint64_t ___ulOverlayHandle0, VREvent_t_t3405266389 * ___pEvent1, uint32_t ___uncbVREvent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_PollNextOverlayEvent::BeginInvoke(System.UInt64,Valve.VR.VREvent_t&,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _PollNextOverlayEvent_BeginInvoke_m393739523 (_PollNextOverlayEvent_t131678587 * __this, uint64_t ___ulOverlayHandle0, VREvent_t_t3405266389 * ___pEvent1, uint32_t ___uncbVREvent2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVROverlay/_PollNextOverlayEvent::EndInvoke(Valve.VR.VREvent_t&,System.IAsyncResult)
extern "C"  bool _PollNextOverlayEvent_EndInvoke_m3364422245 (_PollNextOverlayEvent_t131678587 * __this, VREvent_t_t3405266389 * ___pEvent0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
