﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.SphereCollider
struct SphereCollider_t1662511355;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "AssemblyU2DCSharp_VRTK_RadialMenuController89584558.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_IndependentRadialMenuController
struct  VRTK_IndependentRadialMenuController_t4268715152  : public RadialMenuController_t89584558
{
public:
	// VRTK.VRTK_InteractableObject VRTK.VRTK_IndependentRadialMenuController::eventsManager
	VRTK_InteractableObject_t2604188111 * ___eventsManager_6;
	// System.Boolean VRTK.VRTK_IndependentRadialMenuController::addMenuCollider
	bool ___addMenuCollider_7;
	// System.Single VRTK.VRTK_IndependentRadialMenuController::colliderRadiusMultiplier
	float ___colliderRadiusMultiplier_8;
	// System.Boolean VRTK.VRTK_IndependentRadialMenuController::hideAfterExecution
	bool ___hideAfterExecution_9;
	// System.Single VRTK.VRTK_IndependentRadialMenuController::offsetMultiplier
	float ___offsetMultiplier_10;
	// UnityEngine.GameObject VRTK.VRTK_IndependentRadialMenuController::rotateTowards
	GameObject_t1756533147 * ___rotateTowards_11;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> VRTK.VRTK_IndependentRadialMenuController::interactingObjects
	List_1_t1125654279 * ___interactingObjects_12;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> VRTK.VRTK_IndependentRadialMenuController::collidingObjects
	List_1_t1125654279 * ___collidingObjects_13;
	// UnityEngine.SphereCollider VRTK.VRTK_IndependentRadialMenuController::menuCollider
	SphereCollider_t1662511355 * ___menuCollider_14;
	// UnityEngine.Coroutine VRTK.VRTK_IndependentRadialMenuController::disableCoroutine
	Coroutine_t2299508840 * ___disableCoroutine_15;
	// UnityEngine.Vector3 VRTK.VRTK_IndependentRadialMenuController::desiredColliderCenter
	Vector3_t2243707580  ___desiredColliderCenter_16;
	// UnityEngine.Quaternion VRTK.VRTK_IndependentRadialMenuController::initialRotation
	Quaternion_t4030073918  ___initialRotation_17;
	// System.Boolean VRTK.VRTK_IndependentRadialMenuController::isClicked
	bool ___isClicked_18;
	// System.Boolean VRTK.VRTK_IndependentRadialMenuController::waitingToDisableCollider
	bool ___waitingToDisableCollider_19;
	// System.Int32 VRTK.VRTK_IndependentRadialMenuController::counter
	int32_t ___counter_20;

public:
	inline static int32_t get_offset_of_eventsManager_6() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___eventsManager_6)); }
	inline VRTK_InteractableObject_t2604188111 * get_eventsManager_6() const { return ___eventsManager_6; }
	inline VRTK_InteractableObject_t2604188111 ** get_address_of_eventsManager_6() { return &___eventsManager_6; }
	inline void set_eventsManager_6(VRTK_InteractableObject_t2604188111 * value)
	{
		___eventsManager_6 = value;
		Il2CppCodeGenWriteBarrier(&___eventsManager_6, value);
	}

	inline static int32_t get_offset_of_addMenuCollider_7() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___addMenuCollider_7)); }
	inline bool get_addMenuCollider_7() const { return ___addMenuCollider_7; }
	inline bool* get_address_of_addMenuCollider_7() { return &___addMenuCollider_7; }
	inline void set_addMenuCollider_7(bool value)
	{
		___addMenuCollider_7 = value;
	}

	inline static int32_t get_offset_of_colliderRadiusMultiplier_8() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___colliderRadiusMultiplier_8)); }
	inline float get_colliderRadiusMultiplier_8() const { return ___colliderRadiusMultiplier_8; }
	inline float* get_address_of_colliderRadiusMultiplier_8() { return &___colliderRadiusMultiplier_8; }
	inline void set_colliderRadiusMultiplier_8(float value)
	{
		___colliderRadiusMultiplier_8 = value;
	}

	inline static int32_t get_offset_of_hideAfterExecution_9() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___hideAfterExecution_9)); }
	inline bool get_hideAfterExecution_9() const { return ___hideAfterExecution_9; }
	inline bool* get_address_of_hideAfterExecution_9() { return &___hideAfterExecution_9; }
	inline void set_hideAfterExecution_9(bool value)
	{
		___hideAfterExecution_9 = value;
	}

	inline static int32_t get_offset_of_offsetMultiplier_10() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___offsetMultiplier_10)); }
	inline float get_offsetMultiplier_10() const { return ___offsetMultiplier_10; }
	inline float* get_address_of_offsetMultiplier_10() { return &___offsetMultiplier_10; }
	inline void set_offsetMultiplier_10(float value)
	{
		___offsetMultiplier_10 = value;
	}

	inline static int32_t get_offset_of_rotateTowards_11() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___rotateTowards_11)); }
	inline GameObject_t1756533147 * get_rotateTowards_11() const { return ___rotateTowards_11; }
	inline GameObject_t1756533147 ** get_address_of_rotateTowards_11() { return &___rotateTowards_11; }
	inline void set_rotateTowards_11(GameObject_t1756533147 * value)
	{
		___rotateTowards_11 = value;
		Il2CppCodeGenWriteBarrier(&___rotateTowards_11, value);
	}

	inline static int32_t get_offset_of_interactingObjects_12() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___interactingObjects_12)); }
	inline List_1_t1125654279 * get_interactingObjects_12() const { return ___interactingObjects_12; }
	inline List_1_t1125654279 ** get_address_of_interactingObjects_12() { return &___interactingObjects_12; }
	inline void set_interactingObjects_12(List_1_t1125654279 * value)
	{
		___interactingObjects_12 = value;
		Il2CppCodeGenWriteBarrier(&___interactingObjects_12, value);
	}

	inline static int32_t get_offset_of_collidingObjects_13() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___collidingObjects_13)); }
	inline List_1_t1125654279 * get_collidingObjects_13() const { return ___collidingObjects_13; }
	inline List_1_t1125654279 ** get_address_of_collidingObjects_13() { return &___collidingObjects_13; }
	inline void set_collidingObjects_13(List_1_t1125654279 * value)
	{
		___collidingObjects_13 = value;
		Il2CppCodeGenWriteBarrier(&___collidingObjects_13, value);
	}

	inline static int32_t get_offset_of_menuCollider_14() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___menuCollider_14)); }
	inline SphereCollider_t1662511355 * get_menuCollider_14() const { return ___menuCollider_14; }
	inline SphereCollider_t1662511355 ** get_address_of_menuCollider_14() { return &___menuCollider_14; }
	inline void set_menuCollider_14(SphereCollider_t1662511355 * value)
	{
		___menuCollider_14 = value;
		Il2CppCodeGenWriteBarrier(&___menuCollider_14, value);
	}

	inline static int32_t get_offset_of_disableCoroutine_15() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___disableCoroutine_15)); }
	inline Coroutine_t2299508840 * get_disableCoroutine_15() const { return ___disableCoroutine_15; }
	inline Coroutine_t2299508840 ** get_address_of_disableCoroutine_15() { return &___disableCoroutine_15; }
	inline void set_disableCoroutine_15(Coroutine_t2299508840 * value)
	{
		___disableCoroutine_15 = value;
		Il2CppCodeGenWriteBarrier(&___disableCoroutine_15, value);
	}

	inline static int32_t get_offset_of_desiredColliderCenter_16() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___desiredColliderCenter_16)); }
	inline Vector3_t2243707580  get_desiredColliderCenter_16() const { return ___desiredColliderCenter_16; }
	inline Vector3_t2243707580 * get_address_of_desiredColliderCenter_16() { return &___desiredColliderCenter_16; }
	inline void set_desiredColliderCenter_16(Vector3_t2243707580  value)
	{
		___desiredColliderCenter_16 = value;
	}

	inline static int32_t get_offset_of_initialRotation_17() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___initialRotation_17)); }
	inline Quaternion_t4030073918  get_initialRotation_17() const { return ___initialRotation_17; }
	inline Quaternion_t4030073918 * get_address_of_initialRotation_17() { return &___initialRotation_17; }
	inline void set_initialRotation_17(Quaternion_t4030073918  value)
	{
		___initialRotation_17 = value;
	}

	inline static int32_t get_offset_of_isClicked_18() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___isClicked_18)); }
	inline bool get_isClicked_18() const { return ___isClicked_18; }
	inline bool* get_address_of_isClicked_18() { return &___isClicked_18; }
	inline void set_isClicked_18(bool value)
	{
		___isClicked_18 = value;
	}

	inline static int32_t get_offset_of_waitingToDisableCollider_19() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___waitingToDisableCollider_19)); }
	inline bool get_waitingToDisableCollider_19() const { return ___waitingToDisableCollider_19; }
	inline bool* get_address_of_waitingToDisableCollider_19() { return &___waitingToDisableCollider_19; }
	inline void set_waitingToDisableCollider_19(bool value)
	{
		___waitingToDisableCollider_19 = value;
	}

	inline static int32_t get_offset_of_counter_20() { return static_cast<int32_t>(offsetof(VRTK_IndependentRadialMenuController_t4268715152, ___counter_20)); }
	inline int32_t get_counter_20() const { return ___counter_20; }
	inline int32_t* get_address_of_counter_20() { return &___counter_20; }
	inline void set_counter_20(int32_t value)
	{
		___counter_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
