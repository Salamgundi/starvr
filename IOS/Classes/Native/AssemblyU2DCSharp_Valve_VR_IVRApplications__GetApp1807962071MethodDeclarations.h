﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetApplicationsTransitionStateNameFromEnum
struct _GetApplicationsTransitionStateNameFromEnum_t1807962071;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationTransitio3895609521.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetApplicationsTransitionStateNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetApplicationsTransitionStateNameFromEnum__ctor_m3346692830 (_GetApplicationsTransitionStateNameFromEnum_t1807962071 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRApplications/_GetApplicationsTransitionStateNameFromEnum::Invoke(Valve.VR.EVRApplicationTransitionState)
extern "C"  IntPtr_t _GetApplicationsTransitionStateNameFromEnum_Invoke_m1735764068 (_GetApplicationsTransitionStateNameFromEnum_t1807962071 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetApplicationsTransitionStateNameFromEnum::BeginInvoke(Valve.VR.EVRApplicationTransitionState,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetApplicationsTransitionStateNameFromEnum_BeginInvoke_m1140037196 (_GetApplicationsTransitionStateNameFromEnum_t1807962071 * __this, int32_t ___state0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRApplications/_GetApplicationsTransitionStateNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetApplicationsTransitionStateNameFromEnum_EndInvoke_m1946576269 (_GetApplicationsTransitionStateNameFromEnum_t1807962071 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
