﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Action`2<System.Object,System.Object>
struct Action_2_t1719057892;
// SteamVR_Events/Event`2<System.Object,System.Object>
struct Event_2_t3334092354;
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t3784905282;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Events/Action`2<System.Object,System.Object>::.ctor(SteamVR_Events/Event`2<T0,T1>,UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Action_2__ctor_m2243214098_gshared (Action_2_t1719057892 * __this, Event_2_t3334092354 * ____event0, UnityAction_2_t3784905282 * ___action1, const MethodInfo* method);
#define Action_2__ctor_m2243214098(__this, ____event0, ___action1, method) ((  void (*) (Action_2_t1719057892 *, Event_2_t3334092354 *, UnityAction_2_t3784905282 *, const MethodInfo*))Action_2__ctor_m2243214098_gshared)(__this, ____event0, ___action1, method)
// System.Void SteamVR_Events/Action`2<System.Object,System.Object>::Enable(System.Boolean)
extern "C"  void Action_2_Enable_m1439186680_gshared (Action_2_t1719057892 * __this, bool ___enabled0, const MethodInfo* method);
#define Action_2_Enable_m1439186680(__this, ___enabled0, method) ((  void (*) (Action_2_t1719057892 *, bool, const MethodInfo*))Action_2_Enable_m1439186680_gshared)(__this, ___enabled0, method)
