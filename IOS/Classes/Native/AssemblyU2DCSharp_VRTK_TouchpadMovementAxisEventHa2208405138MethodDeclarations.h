﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.TouchpadMovementAxisEventHandler
struct TouchpadMovementAxisEventHandler_t2208405138;
// System.Object
struct Il2CppObject;
// VRTK.VRTK_TouchpadMovement
struct VRTK_TouchpadMovement_t1979813355;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement1979813355.h"
#include "AssemblyU2DCSharp_VRTK_TouchpadMovementAxisEventAr1237298133.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.TouchpadMovementAxisEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void TouchpadMovementAxisEventHandler__ctor_m2196863448 (TouchpadMovementAxisEventHandler_t2208405138 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.TouchpadMovementAxisEventHandler::Invoke(VRTK.VRTK_TouchpadMovement,VRTK.TouchpadMovementAxisEventArgs)
extern "C"  void TouchpadMovementAxisEventHandler_Invoke_m414806234 (TouchpadMovementAxisEventHandler_t2208405138 * __this, VRTK_TouchpadMovement_t1979813355 * ___sender0, TouchpadMovementAxisEventArgs_t1237298133  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.TouchpadMovementAxisEventHandler::BeginInvoke(VRTK.VRTK_TouchpadMovement,VRTK.TouchpadMovementAxisEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TouchpadMovementAxisEventHandler_BeginInvoke_m3050040253 (TouchpadMovementAxisEventHandler_t2208405138 * __this, VRTK_TouchpadMovement_t1979813355 * ___sender0, TouchpadMovementAxisEventArgs_t1237298133  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.TouchpadMovementAxisEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void TouchpadMovementAxisEventHandler_EndInvoke_m2611156174 (TouchpadMovementAxisEventHandler_t2208405138 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
