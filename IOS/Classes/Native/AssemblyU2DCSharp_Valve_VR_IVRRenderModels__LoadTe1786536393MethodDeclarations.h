﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_LoadTexture_Async
struct _LoadTexture_Async_t1786536393;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRRenderModelError21703732.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_LoadTexture_Async::.ctor(System.Object,System.IntPtr)
extern "C"  void _LoadTexture_Async__ctor_m2664168578 (_LoadTexture_Async_t1786536393 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRRenderModelError Valve.VR.IVRRenderModels/_LoadTexture_Async::Invoke(System.Int32,System.IntPtr&)
extern "C"  int32_t _LoadTexture_Async_Invoke_m1545540020 (_LoadTexture_Async_t1786536393 * __this, int32_t ___textureId0, IntPtr_t* ___ppTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_LoadTexture_Async::BeginInvoke(System.Int32,System.IntPtr&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _LoadTexture_Async_BeginInvoke_m224759628 (_LoadTexture_Async_t1786536393 * __this, int32_t ___textureId0, IntPtr_t* ___ppTexture1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRRenderModelError Valve.VR.IVRRenderModels/_LoadTexture_Async::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern "C"  int32_t _LoadTexture_Async_EndInvoke_m2404127357 (_LoadTexture_Async_t1786536393 * __this, IntPtr_t* ___ppTexture0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
