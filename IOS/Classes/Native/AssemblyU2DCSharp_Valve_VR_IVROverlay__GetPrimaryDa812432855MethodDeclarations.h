﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetPrimaryDashboardDevice
struct _GetPrimaryDashboardDevice_t812432855;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetPrimaryDashboardDevice::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetPrimaryDashboardDevice__ctor_m1153758350 (_GetPrimaryDashboardDevice_t812432855 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVROverlay/_GetPrimaryDashboardDevice::Invoke()
extern "C"  uint32_t _GetPrimaryDashboardDevice_Invoke_m3275055497 (_GetPrimaryDashboardDevice_t812432855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetPrimaryDashboardDevice::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetPrimaryDashboardDevice_BeginInvoke_m3538026041 (_GetPrimaryDashboardDevice_t812432855 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVROverlay/_GetPrimaryDashboardDevice::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetPrimaryDashboardDevice_EndInvoke_m1798111827 (_GetPrimaryDashboardDevice_t812432855 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
