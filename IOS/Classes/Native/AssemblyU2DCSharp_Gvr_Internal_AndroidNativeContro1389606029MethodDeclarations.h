﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Gvr.Internal.AndroidNativeControllerProvider
struct AndroidNativeControllerProvider_t1389606029;

#include "codegen/il2cpp-codegen.h"

// System.Void Gvr.Internal.AndroidNativeControllerProvider::.ctor()
extern "C"  void AndroidNativeControllerProvider__ctor_m1272877542 (AndroidNativeControllerProvider_t1389606029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
