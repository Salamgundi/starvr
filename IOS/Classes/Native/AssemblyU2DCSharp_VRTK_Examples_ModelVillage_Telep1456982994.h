﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_VRTK_VRTK_DestinationMarker667613644.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.ModelVillage_TeleportLocation
struct  ModelVillage_TeleportLocation_t1456982994  : public VRTK_DestinationMarker_t667613644
{
public:
	// UnityEngine.Transform VRTK.Examples.ModelVillage_TeleportLocation::destination
	Transform_t3275118058 * ___destination_9;
	// System.Boolean VRTK.Examples.ModelVillage_TeleportLocation::lastUsePressedState
	bool ___lastUsePressedState_10;

public:
	inline static int32_t get_offset_of_destination_9() { return static_cast<int32_t>(offsetof(ModelVillage_TeleportLocation_t1456982994, ___destination_9)); }
	inline Transform_t3275118058 * get_destination_9() const { return ___destination_9; }
	inline Transform_t3275118058 ** get_address_of_destination_9() { return &___destination_9; }
	inline void set_destination_9(Transform_t3275118058 * value)
	{
		___destination_9 = value;
		Il2CppCodeGenWriteBarrier(&___destination_9, value);
	}

	inline static int32_t get_offset_of_lastUsePressedState_10() { return static_cast<int32_t>(offsetof(ModelVillage_TeleportLocation_t1456982994, ___lastUsePressedState_10)); }
	inline bool get_lastUsePressedState_10() const { return ___lastUsePressedState_10; }
	inline bool* get_address_of_lastUsePressedState_10() { return &___lastUsePressedState_10; }
	inline void set_lastUsePressedState_10(bool value)
	{
		___lastUsePressedState_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
