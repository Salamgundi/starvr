﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.VRTK_ControllerUIPointerEvents_ListenerExample
struct VRTK_ControllerUIPointerEvents_ListenerExample_t3858876453;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_UIPointerEventArgs1171985978.h"

// System.Void VRTK.Examples.VRTK_ControllerUIPointerEvents_ListenerExample::.ctor()
extern "C"  void VRTK_ControllerUIPointerEvents_ListenerExample__ctor_m1856936700 (VRTK_ControllerUIPointerEvents_ListenerExample_t3858876453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerUIPointerEvents_ListenerExample::Start()
extern "C"  void VRTK_ControllerUIPointerEvents_ListenerExample_Start_m3968193328 (VRTK_ControllerUIPointerEvents_ListenerExample_t3858876453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerUIPointerEvents_ListenerExample::VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementEnter(System.Object,VRTK.UIPointerEventArgs)
extern "C"  void VRTK_ControllerUIPointerEvents_ListenerExample_VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementEnter_m1818780540 (VRTK_ControllerUIPointerEvents_ListenerExample_t3858876453 * __this, Il2CppObject * ___sender0, UIPointerEventArgs_t1171985978  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerUIPointerEvents_ListenerExample::VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementExit(System.Object,VRTK.UIPointerEventArgs)
extern "C"  void VRTK_ControllerUIPointerEvents_ListenerExample_VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementExit_m812594862 (VRTK_ControllerUIPointerEvents_ListenerExample_t3858876453 * __this, Il2CppObject * ___sender0, UIPointerEventArgs_t1171985978  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerUIPointerEvents_ListenerExample::VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementClick(System.Object,VRTK.UIPointerEventArgs)
extern "C"  void VRTK_ControllerUIPointerEvents_ListenerExample_VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementClick_m3301058538 (VRTK_ControllerUIPointerEvents_ListenerExample_t3858876453 * __this, Il2CppObject * ___sender0, UIPointerEventArgs_t1171985978  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerUIPointerEvents_ListenerExample::VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementDragStart(System.Object,VRTK.UIPointerEventArgs)
extern "C"  void VRTK_ControllerUIPointerEvents_ListenerExample_VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementDragStart_m3267606900 (VRTK_ControllerUIPointerEvents_ListenerExample_t3858876453 * __this, Il2CppObject * ___sender0, UIPointerEventArgs_t1171985978  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerUIPointerEvents_ListenerExample::VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementDragEnd(System.Object,VRTK.UIPointerEventArgs)
extern "C"  void VRTK_ControllerUIPointerEvents_ListenerExample_VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementDragEnd_m3887509341 (VRTK_ControllerUIPointerEvents_ListenerExample_t3858876453 * __this, Il2CppObject * ___sender0, UIPointerEventArgs_t1171985978  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
