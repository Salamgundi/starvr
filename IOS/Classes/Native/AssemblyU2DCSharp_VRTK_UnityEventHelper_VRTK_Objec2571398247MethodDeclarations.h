﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents
struct VRTK_ObjectControl_UnityEvents_t2571398247;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ObjectControlEventArgs2459490319.h"

// System.Void VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents::.ctor()
extern "C"  void VRTK_ObjectControl_UnityEvents__ctor_m3726052696 (VRTK_ObjectControl_UnityEvents_t2571398247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents::SetObjectControl()
extern "C"  void VRTK_ObjectControl_UnityEvents_SetObjectControl_m647927114 (VRTK_ObjectControl_UnityEvents_t2571398247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents::OnEnable()
extern "C"  void VRTK_ObjectControl_UnityEvents_OnEnable_m1825238640 (VRTK_ObjectControl_UnityEvents_t2571398247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents::XAxisChanged(System.Object,VRTK.ObjectControlEventArgs)
extern "C"  void VRTK_ObjectControl_UnityEvents_XAxisChanged_m861815671 (VRTK_ObjectControl_UnityEvents_t2571398247 * __this, Il2CppObject * ___o0, ObjectControlEventArgs_t2459490319  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents::YAxisChanged(System.Object,VRTK.ObjectControlEventArgs)
extern "C"  void VRTK_ObjectControl_UnityEvents_YAxisChanged_m2580978904 (VRTK_ObjectControl_UnityEvents_t2571398247 * __this, Il2CppObject * ___o0, ObjectControlEventArgs_t2459490319  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents::OnDisable()
extern "C"  void VRTK_ObjectControl_UnityEvents_OnDisable_m457358845 (VRTK_ObjectControl_UnityEvents_t2571398247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
