﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_VRTK_BasicTeleport3532761337.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_HeightAdjustTeleport
struct  VRTK_HeightAdjustTeleport_t2696079433  : public VRTK_BasicTeleport_t3532761337
{
public:
	// UnityEngine.LayerMask VRTK.VRTK_HeightAdjustTeleport::layersToIgnore
	LayerMask_t3188175821  ___layersToIgnore_18;

public:
	inline static int32_t get_offset_of_layersToIgnore_18() { return static_cast<int32_t>(offsetof(VRTK_HeightAdjustTeleport_t2696079433, ___layersToIgnore_18)); }
	inline LayerMask_t3188175821  get_layersToIgnore_18() const { return ___layersToIgnore_18; }
	inline LayerMask_t3188175821 * get_address_of_layersToIgnore_18() { return &___layersToIgnore_18; }
	inline void set_layersToIgnore_18(LayerMask_t3188175821  value)
	{
		___layersToIgnore_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
