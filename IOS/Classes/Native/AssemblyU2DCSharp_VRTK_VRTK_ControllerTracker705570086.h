﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_TrackedController
struct VRTK_TrackedController_t520756048;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ControllerTracker
struct  VRTK_ControllerTracker_t705570086  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_TrackedController VRTK.VRTK_ControllerTracker::trackedController
	VRTK_TrackedController_t520756048 * ___trackedController_2;

public:
	inline static int32_t get_offset_of_trackedController_2() { return static_cast<int32_t>(offsetof(VRTK_ControllerTracker_t705570086, ___trackedController_2)); }
	inline VRTK_TrackedController_t520756048 * get_trackedController_2() const { return ___trackedController_2; }
	inline VRTK_TrackedController_t520756048 ** get_address_of_trackedController_2() { return &___trackedController_2; }
	inline void set_trackedController_2(VRTK_TrackedController_t520756048 * value)
	{
		___trackedController_2 = value;
		Il2CppCodeGenWriteBarrier(&___trackedController_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
