﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_CancelApplicationLaunch
struct _CancelApplicationLaunch_t2074441983;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_CancelApplicationLaunch::.ctor(System.Object,System.IntPtr)
extern "C"  void _CancelApplicationLaunch__ctor_m1781557186 (_CancelApplicationLaunch_t2074441983 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRApplications/_CancelApplicationLaunch::Invoke(System.String)
extern "C"  bool _CancelApplicationLaunch_Invoke_m3266157620 (_CancelApplicationLaunch_t2074441983 * __this, String_t* ___pchAppKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_CancelApplicationLaunch::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _CancelApplicationLaunch_BeginInvoke_m2957790755 (_CancelApplicationLaunch_t2074441983 * __this, String_t* ___pchAppKey0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRApplications/_CancelApplicationLaunch::EndInvoke(System.IAsyncResult)
extern "C"  bool _CancelApplicationLaunch_EndInvoke_m2604316618 (_CancelApplicationLaunch_t2074441983 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
