﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.HeadsetFadeEventArgs
struct HeadsetFadeEventArgs_t2892542019;
struct HeadsetFadeEventArgs_t2892542019_marshaled_pinvoke;
struct HeadsetFadeEventArgs_t2892542019_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct HeadsetFadeEventArgs_t2892542019;
struct HeadsetFadeEventArgs_t2892542019_marshaled_pinvoke;

extern "C" void HeadsetFadeEventArgs_t2892542019_marshal_pinvoke(const HeadsetFadeEventArgs_t2892542019& unmarshaled, HeadsetFadeEventArgs_t2892542019_marshaled_pinvoke& marshaled);
extern "C" void HeadsetFadeEventArgs_t2892542019_marshal_pinvoke_back(const HeadsetFadeEventArgs_t2892542019_marshaled_pinvoke& marshaled, HeadsetFadeEventArgs_t2892542019& unmarshaled);
extern "C" void HeadsetFadeEventArgs_t2892542019_marshal_pinvoke_cleanup(HeadsetFadeEventArgs_t2892542019_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct HeadsetFadeEventArgs_t2892542019;
struct HeadsetFadeEventArgs_t2892542019_marshaled_com;

extern "C" void HeadsetFadeEventArgs_t2892542019_marshal_com(const HeadsetFadeEventArgs_t2892542019& unmarshaled, HeadsetFadeEventArgs_t2892542019_marshaled_com& marshaled);
extern "C" void HeadsetFadeEventArgs_t2892542019_marshal_com_back(const HeadsetFadeEventArgs_t2892542019_marshaled_com& marshaled, HeadsetFadeEventArgs_t2892542019& unmarshaled);
extern "C" void HeadsetFadeEventArgs_t2892542019_marshal_com_cleanup(HeadsetFadeEventArgs_t2892542019_marshaled_com& marshaled);
