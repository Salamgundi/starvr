﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`1<UnityEngine.Vector3>
struct InvokableCall_1_t438034437;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector3>
struct UnityAction_1_t3610293331;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector3>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m2053072656_gshared (InvokableCall_1_t438034437 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_1__ctor_m2053072656(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_1_t438034437 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m2053072656_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector3>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m4140664578_gshared (InvokableCall_1_t438034437 * __this, UnityAction_1_t3610293331 * ___action0, const MethodInfo* method);
#define InvokableCall_1__ctor_m4140664578(__this, ___action0, method) ((  void (*) (InvokableCall_1_t438034437 *, UnityAction_1_t3610293331 *, const MethodInfo*))InvokableCall_1__ctor_m4140664578_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector3>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m276916015_gshared (InvokableCall_1_t438034437 * __this, UnityAction_1_t3610293331 * ___value0, const MethodInfo* method);
#define InvokableCall_1_add_Delegate_m276916015(__this, ___value0, method) ((  void (*) (InvokableCall_1_t438034437 *, UnityAction_1_t3610293331 *, const MethodInfo*))InvokableCall_1_add_Delegate_m276916015_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector3>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m1055744316_gshared (InvokableCall_1_t438034437 * __this, UnityAction_1_t3610293331 * ___value0, const MethodInfo* method);
#define InvokableCall_1_remove_Delegate_m1055744316(__this, ___value0, method) ((  void (*) (InvokableCall_1_t438034437 *, UnityAction_1_t3610293331 *, const MethodInfo*))InvokableCall_1_remove_Delegate_m1055744316_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector3>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m3738789835_gshared (InvokableCall_1_t438034437 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_1_Invoke_m3738789835(__this, ___args0, method) ((  void (*) (InvokableCall_1_t438034437 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_1_Invoke_m3738789835_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Vector3>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m980157595_gshared (InvokableCall_1_t438034437 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_1_Find_m980157595(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_1_t438034437 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m980157595_gshared)(__this, ___targetObj0, ___method1, method)
