﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GazeEventHandler
struct GazeEventHandler_t1044944507;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_GazeEventArgs2196141074.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void GazeEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void GazeEventHandler__ctor_m3951328388 (GazeEventHandler_t1044944507 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GazeEventHandler::Invoke(System.Object,GazeEventArgs)
extern "C"  void GazeEventHandler_Invoke_m1370777540 (GazeEventHandler_t1044944507 * __this, Il2CppObject * ___sender0, GazeEventArgs_t2196141074  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult GazeEventHandler::BeginInvoke(System.Object,GazeEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GazeEventHandler_BeginInvoke_m626338785 (GazeEventHandler_t1044944507 * __this, Il2CppObject * ___sender0, GazeEventArgs_t2196141074  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GazeEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void GazeEventHandler_EndInvoke_m1866891614 (GazeEventHandler_t1044944507 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
