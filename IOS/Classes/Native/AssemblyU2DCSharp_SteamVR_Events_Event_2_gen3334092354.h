﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen1372135904.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SteamVR_Events/Event`2<System.Object,System.Object>
struct  Event_2_t3334092354  : public UnityEvent_2_t1372135904
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
