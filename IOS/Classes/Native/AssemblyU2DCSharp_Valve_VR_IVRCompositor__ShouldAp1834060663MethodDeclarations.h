﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_ShouldAppRenderWithLowResources
struct _ShouldAppRenderWithLowResources_t1834060663;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_ShouldAppRenderWithLowResources::.ctor(System.Object,System.IntPtr)
extern "C"  void _ShouldAppRenderWithLowResources__ctor_m4152380906 (_ShouldAppRenderWithLowResources_t1834060663 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRCompositor/_ShouldAppRenderWithLowResources::Invoke()
extern "C"  bool _ShouldAppRenderWithLowResources_Invoke_m3343589818 (_ShouldAppRenderWithLowResources_t1834060663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_ShouldAppRenderWithLowResources::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ShouldAppRenderWithLowResources_BeginInvoke_m1339890945 (_ShouldAppRenderWithLowResources_t1834060663 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRCompositor/_ShouldAppRenderWithLowResources::EndInvoke(System.IAsyncResult)
extern "C"  bool _ShouldAppRenderWithLowResources_EndInvoke_m1126446498 (_ShouldAppRenderWithLowResources_t1834060663 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
