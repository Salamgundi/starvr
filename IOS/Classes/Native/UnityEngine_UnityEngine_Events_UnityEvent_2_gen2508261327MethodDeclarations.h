﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,System.Boolean>
struct UnityEvent_2_t2508261327;
// UnityEngine.Events.UnityAction`2<System.Object,System.Boolean>
struct UnityAction_2_t626063409;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Boolean>::.ctor()
extern "C"  void UnityEvent_2__ctor_m4064166462_gshared (UnityEvent_2_t2508261327 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m4064166462(__this, method) ((  void (*) (UnityEvent_2_t2508261327 *, const MethodInfo*))UnityEvent_2__ctor_m4064166462_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Boolean>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m3913511359_gshared (UnityEvent_2_t2508261327 * __this, UnityAction_2_t626063409 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m3913511359(__this, ___call0, method) ((  void (*) (UnityEvent_2_t2508261327 *, UnityAction_2_t626063409 *, const MethodInfo*))UnityEvent_2_AddListener_m3913511359_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Boolean>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m87176962_gshared (UnityEvent_2_t2508261327 * __this, UnityAction_2_t626063409 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m87176962(__this, ___call0, method) ((  void (*) (UnityEvent_2_t2508261327 *, UnityAction_2_t626063409 *, const MethodInfo*))UnityEvent_2_RemoveListener_m87176962_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,System.Boolean>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m3929411811_gshared (UnityEvent_2_t2508261327 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m3929411811(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t2508261327 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m3929411811_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,System.Boolean>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m4060621883_gshared (UnityEvent_2_t2508261327 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m4060621883(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t2508261327 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m4060621883_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,System.Boolean>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m1561906888_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t626063409 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m1561906888(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t626063409 *, const MethodInfo*))UnityEvent_2_GetDelegate_m1561906888_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Boolean>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m202592039_gshared (UnityEvent_2_t2508261327 * __this, Il2CppObject * ___arg00, bool ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m202592039(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t2508261327 *, Il2CppObject *, bool, const MethodInfo*))UnityEvent_2_Invoke_m202592039_gshared)(__this, ___arg00, ___arg11, method)
