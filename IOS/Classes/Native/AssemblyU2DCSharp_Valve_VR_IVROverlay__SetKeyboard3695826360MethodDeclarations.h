﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetKeyboardPositionForOverlay
struct _SetKeyboardPositionForOverlay_t3695826360;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdRect2_t1656020282.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetKeyboardPositionForOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetKeyboardPositionForOverlay__ctor_m2443446811 (_SetKeyboardPositionForOverlay_t3695826360 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVROverlay/_SetKeyboardPositionForOverlay::Invoke(System.UInt64,Valve.VR.HmdRect2_t)
extern "C"  void _SetKeyboardPositionForOverlay_Invoke_m1491298352 (_SetKeyboardPositionForOverlay_t3695826360 * __this, uint64_t ___ulOverlayHandle0, HmdRect2_t_t1656020282  ___avoidRect1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetKeyboardPositionForOverlay::BeginInvoke(System.UInt64,Valve.VR.HmdRect2_t,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetKeyboardPositionForOverlay_BeginInvoke_m1207209723 (_SetKeyboardPositionForOverlay_t3695826360 * __this, uint64_t ___ulOverlayHandle0, HmdRect2_t_t1656020282  ___avoidRect1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVROverlay/_SetKeyboardPositionForOverlay::EndInvoke(System.IAsyncResult)
extern "C"  void _SetKeyboardPositionForOverlay_EndInvoke_m3333355873 (_SetKeyboardPositionForOverlay_t3695826360 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
