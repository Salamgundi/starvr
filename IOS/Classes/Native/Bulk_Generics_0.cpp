﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Google.ProtocolBuffers.AbstractBuilderLite`2/LimitedInputStream<System.Object,System.Object>
struct LimitedInputStream_t2498305289;
// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Google.ProtocolBuffers.AbstractBuilderLite`2/SerializationSurrogate<System.Object,System.Object>
struct SerializationSurrogate_t3471339337;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>
struct AbstractBuilderLite_2_t466644448;
// Google.ProtocolBuffers.ICodedInputStream
struct ICodedInputStream_t1485221380;
// Google.ProtocolBuffers.ExtensionRegistry
struct ExtensionRegistry_t1533220584;
// Google.ProtocolBuffers.ByteString
struct ByteString_t3153909979;
// Google.ProtocolBuffers.IBuilderLite
struct IBuilderLite_t4076259616;
// Google.ProtocolBuffers.IMessageLite
struct IMessageLite_t1402851992;
// Google.ProtocolBuffers.AbstractMessageLite`2/SerializationSurrogate<System.Object,System.Object>
struct SerializationSurrogate_t1165551381;
// Google.ProtocolBuffers.AbstractMessageLite`2<System.Object,System.Object>
struct AbstractMessageLite_2_t2741522770;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;
// System.Collections.Generic.IList`1<System.Single>
struct IList_1_t2617450533;
// Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>
struct PopsicleList_1_t884098166;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>
struct PopsicleList_1_t271158803;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t3847001055;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t2368636977;
// Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>
struct GeneratedBuilderLite_2_t464188747;
// System.String
struct String_t;
// Google.ProtocolBuffers.GeneratedMessageLite`2<System.Object,System.Object>
struct GeneratedMessageLite_2_t1832135119;
// System.IO.TextWriter
struct TextWriter_t4027217640;
// System.Collections.Generic.IEnumerable`1<System.Byte>
struct IEnumerable_1_t3975231481;
// SteamVR_Events/Action`1<System.Boolean>
struct Action_1_t988986007;
// SteamVR_Events/Event`1<System.Boolean>
struct Event_1_t1706029839;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t897193173;
// SteamVR_Events/Action`1<System.Object>
struct Action_1_t4147827880;
// SteamVR_Events/Event`1<System.Object>
struct Event_1_t569904416;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t4056035046;
// SteamVR_Events/Action`1<System.Single>
struct Action_1_t3534888517;
// SteamVR_Events/Event`1<System.Single>
struct Event_1_t4251932349;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t3443095683;
// SteamVR_Events/Action`1<Valve.VR.VREvent_t>
struct Action_1_t568677678;
// SteamVR_Events/Event`1<Valve.VR.VREvent_t>
struct Event_1_t1285721510;
// UnityEngine.Events.UnityAction`1<Valve.VR.VREvent_t>
struct UnityAction_1_t476884844;
// SteamVR_Events/Action`2<System.Int32,System.Boolean>
struct Action_2_t2270948822;
// SteamVR_Events/Event`2<System.Int32,System.Boolean>
struct Event_2_t3885983284;
// UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>
struct UnityAction_2_t41828916;
// SteamVR_Events/Action`2<System.Object,System.Boolean>
struct Action_2_t2855183315;
// SteamVR_Events/Event`2<System.Object,System.Boolean>
struct Event_2_t175250481;
// UnityEngine.Events.UnityAction`2<System.Object,System.Boolean>
struct UnityAction_2_t626063409;
// SteamVR_Events/Action`2<System.Object,System.Object>
struct Action_2_t1719057892;
// SteamVR_Events/Event`2<System.Object,System.Object>
struct Event_2_t3334092354;
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t3784905282;
// SteamVR_Events/Action`3<System.Object,System.Object,System.Object>
struct Action_3_t2223553130;
// SteamVR_Events/Event`3<System.Object,System.Object,System.Object>
struct Event_3_t1695830490;
// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct UnityAction_3_t3482433968;
// SteamVR_Events/Action`3<UnityEngine.Color,System.Single,System.Boolean>
struct Action_3_t557175684;
// SteamVR_Events/Event`3<UnityEngine.Color,System.Single,System.Boolean>
struct Event_3_t29453044;
// UnityEngine.Events.UnityAction`3<UnityEngine.Color,System.Single,System.Boolean>
struct UnityAction_3_t1816056522;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`2<System.Boolean,System.Object>
struct Action_2_t2525452034;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2572051853;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t2445488949;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t4145164493;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t1254237568;
// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t2471096271;
// System.Exception
struct Exception_t1927440687;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t4170771815;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3304067486;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1864648666;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t1279844890;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1075686591;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3268689037;
// System.Array
struct Il2CppArray;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Abst2498305289.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Abst2498305289MethodDeclarations.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_IO_Stream3255436806MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "mscorlib_System_IO_SeekOrigin2475945306.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Abst3471339337.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Abst3471339337MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Object2689449295.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Exte1533220584MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Exte1533220584.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Abstr466644448.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Abstr466644448MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Abst2741522770.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Abst2741522770MethodDeclarations.h"
#include "mscorlib_System_Activator1850728717MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Code3771275139MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Byte3153909979.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Byte3153909979MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Code3771275139.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Abst1165551381.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Abst1165551381MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Code2164392868MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Code2164392868.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Coll1182124910.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Coll1182124910MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2875234987.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2875234987MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Colle569185547.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Colle569185547MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2262295624.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2262295624MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Colle884098166.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Colle884098166MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Throw278852686MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Throw278852686.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Colle271158803.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Colle271158803MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1445631064.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1445631064MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Enum1339677487.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Enum1339677487MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo255040150MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen899601993.h"
#include "mscorlib_System_Reflection_FieldInfo255040150.h"
#include "mscorlib_System_Collections_Generic_List_1_gen899601993MethodDeclarations.h"
#include "mscorlib_System_Reflection_BindingFlags1082350898.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Type1530480861.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge538306496.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge538306496MethodDeclarations.h"
#include "mscorlib_System_Enum2459695545MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Gener464188747.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Gener464188747MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Unin1889871139MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Unin1889871139.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Invali37028716.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Gene1832135119.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Gene1832135119MethodDeclarations.h"
#include "mscorlib_System_IO_StringWriter4139609088MethodDeclarations.h"
#include "mscorlib_System_IO_StringWriter4139609088.h"
#include "mscorlib_System_IO_TextWriter4027217640.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Fram3280999091MethodDeclarations.h"
#include "mscorlib_System_IO_TextWriter4027217640MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "mscorlib_System_Char3454481338.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_1_gen988986007.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_1_gen988986007MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_1_gen1706029839.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen897193173.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action1836998693MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_1_gen1706029839MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_1_gen4147827880.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_1_gen4147827880MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_1_gen569904416.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4056035046.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_1_gen569904416MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_1_gen3534888517.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_1_gen3534888517MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_1_gen4251932349.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3443095683.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_1_gen4251932349MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_1_gen568677678.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_1_gen568677678MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_1_gen1285721510.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen476884844.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_1_gen1285721510MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_2_gen2270948822.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_2_gen2270948822MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_2_gen3885983284.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen41828916.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_2_gen3885983284MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_2_gen2855183315.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_2_gen2855183315MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_2_gen175250481.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen626063409.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_2_gen175250481MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_2_gen1719057892.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_2_gen1719057892MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_2_gen3334092354.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3784905282.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_2_gen3334092354MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_3_gen2223553130.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_3_gen2223553130MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_3_gen1695830490.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen3482433968.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_3_gen1695830490MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_3_gen557175684.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_3_gen557175684MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_3_gen29453044.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen1816056522.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_3_gen29453044MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3863924733MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2727799310MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2114859947MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3443616404MethodDeclarations.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_t3405266389.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen1924026834MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen2508261327MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen1372135904MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen3149477088MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen1483099642MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Action_1_gen1873676830.h"
#include "mscorlib_System_Action_1_gen1873676830MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2491248677.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2525452034.h"
#include "System_Core_System_Action_2_gen2525452034MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2572051853.h"
#include "System_Core_System_Action_2_gen2572051853MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2445488949.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2445488949MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen2471096271.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn4145164493.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn4145164493MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4170771815.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn1254237568.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn1254237568MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1279844890.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen2471096271MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4170771815MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1279844890MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3951913483.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3951913483MethodDeclarations.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Exte3093161221.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3859437264.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3859437264MethodDeclarations.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorTouchEvent_3000685002.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2870158877.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2870158877MethodDeclarations.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex2011406615.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen565169432.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen565169432MethodDeclarations.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4001384466.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2389233123.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2389233123MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3461135388.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3461135388MethodDeclarations.h"
#include "AssemblyU2DCSharp_SteamVR_Utils_RigidTransform2602383126.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3452969744.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3452969744MethodDeclarations.h"
#include "mscorlib_System_ArraySegment_1_gen2594217482.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen389359684.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen389359684MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246889402.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246889402MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen18266304.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen18266304MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3907627660.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3907627660MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1723885533.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1723885533MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li865133271.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3023014317.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3023014317MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22164262055.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3449371276.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3449371276MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22590619014.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3990767863.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3990767863MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen313372414.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen313372414MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2033732330.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2033732330MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen280035060.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen280035060MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen897606907.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen897606907MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen284667544.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen284667544MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23720882578.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1117354526.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1117354526MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_258602264.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1346955310.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1346955310MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_488203048.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3924046188.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3924046188MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23065293926.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2563600722.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2563600722MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21704848460.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2311033343.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2311033343MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21452281081.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3963639764.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3963639764MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23104887502.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3582009740.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3582009740MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Link2723257478.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2881283523.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2881283523MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable_Slot2022531261.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3126312864.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3126312864MethodDeclarations.h"
#include "mscorlib_System_Collections_SortedList_Slot2267560602.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1551957931.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1551957931MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1583453339.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1583453339MethodDeclarations.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen641800647.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen641800647MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen605030880.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen605030880MethodDeclarations.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2930629710.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2930629710MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1767830299.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1767830299MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3362812871.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3362812871MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen952909805.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen952909805MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2356950176.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2356950176MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen275897710.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen275897710MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelD3712112744.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen654694480.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen654694480MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelF4090909514.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1008311600.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1008311600MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo149559338.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2679387182.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2679387182MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterModifier1820634920.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1191988411.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1191988411MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCa333236149.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen496834202.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen496834202MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI3933049236.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen999961858.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen999961858MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi141209596.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1313169811.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1313169811MethodDeclarations.h"
#include "mscorlib_System_SByte454417549.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen842163687.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen842163687MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat4278378721.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2935262194.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2935262194MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3583626735.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3583626735MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Mark2724874473.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4289011211.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4289011211MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1845634873.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1845634873MethodDeclarations.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3008434283.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3008434283MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3767949176.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3767949176MethodDeclarations.h"
#include "mscorlib_System_UInt642909196914.h"

// System.Void Google.ProtocolBuffers.ThrowHelper::ThrowIfAnyNull<System.Object>(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void ThrowHelper_ThrowIfAnyNull_TisIl2CppObject_m2824990704_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___sequence0, const MethodInfo* method);
#define ThrowHelper_ThrowIfAnyNull_TisIl2CppObject_m2824990704(__this /* static, unused */, ___sequence0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))ThrowHelper_ThrowIfAnyNull_TisIl2CppObject_m2824990704_gshared)(__this /* static, unused */, ___sequence0, method)
// System.Void Google.ProtocolBuffers.ThrowHelper::ThrowIfAnyNull<System.Single>(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void ThrowHelper_ThrowIfAnyNull_TisSingle_t2076509932_m1040742265_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___sequence0, const MethodInfo* method);
#define ThrowHelper_ThrowIfAnyNull_TisSingle_t2076509932_m1040742265(__this /* static, unused */, ___sequence0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))ThrowHelper_ThrowIfAnyNull_TisSingle_t2076509932_m1040742265_gshared)(__this /* static, unused */, ___sequence0, method)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2032877681_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, Il2CppObject * p1, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2032877681(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2032877681_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486* p0, CustomAttributeNamedArgument_t94157543  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591* p0, CustomAttributeTypedArgument_t1498197914  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Array::InternalArray__get_Item<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair>(System.Int32)
extern "C"  ExtensionIntPair_t3093161221  Array_InternalArray__get_Item_TisExtensionIntPair_t3093161221_m542414807_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisExtensionIntPair_t3093161221_m542414807(__this, p0, method) ((  ExtensionIntPair_t3093161221  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisExtensionIntPair_t3093161221_m542414807_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Gvr.Internal.EmulatorTouchEvent/Pointer>(System.Int32)
extern "C"  Pointer_t3000685002  Array_InternalArray__get_Item_TisPointer_t3000685002_m1194921694_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPointer_t3000685002_m1194921694(__this, p0, method) ((  Pointer_t3000685002  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPointer_t3000685002_m1194921694_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern "C"  TableRange_t2011406615  Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977(__this, p0, method) ((  TableRange_t2011406615  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<proto.PhoneEvent/Types/Type>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisType_t1530480861_m2855390407_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisType_t1530480861_m2855390407(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisType_t1530480861_m2855390407_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<SteamVR_Utils/RigidTransform>(System.Int32)
extern "C"  RigidTransform_t2602383126  Array_InternalArray__get_Item_TisRigidTransform_t2602383126_m88146750_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRigidTransform_t2602383126_m88146750(__this, p0, method) ((  RigidTransform_t2602383126  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRigidTransform_t2602383126_m88146750_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.ArraySegment`1<System.Byte>>(System.Int32)
extern "C"  ArraySegment_1_t2594217482  Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683(__this, p0, method) ((  ArraySegment_1_t2594217482  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
extern "C"  bool Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639(__this, p0, method) ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisByte_t3683104436_m635665873(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Char>(System.Int32)
extern "C"  Il2CppChar Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547(__this, p0, method) ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
extern "C"  DictionaryEntry_t3048875398  Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320(__this, p0, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
extern "C"  Link_t865133271  Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t865133271_m2489845481(__this, p0, method) ((  Link_t865133271  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t2164262055  Array_InternalArray__get_Item_TisKeyValuePair_2_t2164262055_m414609650_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2164262055_m414609650(__this, p0, method) ((  KeyValuePair_2_t2164262055  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2164262055_m414609650_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,proto.PhoneEvent/Types/Type>>(System.Int32)
extern "C"  KeyValuePair_2_t2590619014  Array_InternalArray__get_Item_TisKeyValuePair_2_t2590619014_m729460343_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2590619014_m729460343(__this, p0, method) ((  KeyValuePair_2_t2590619014  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2590619014_m729460343_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t3132015601  Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841(__this, p0, method) ((  KeyValuePair_2_t3132015601  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t3749587448  Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118(__this, p0, method) ((  KeyValuePair_2_t3749587448  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32)
extern "C"  KeyValuePair_2_t1174980068  Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642(__this, p0, method) ((  KeyValuePair_2_t1174980068  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t3716250094  Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630(__this, p0, method) ((  KeyValuePair_2_t3716250094  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t38854645  Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821(__this, p0, method) ((  KeyValuePair_2_t38854645  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>(System.Int32)
extern "C"  KeyValuePair_2_t3720882578  Array_InternalArray__get_Item_TisKeyValuePair_2_t3720882578_m787884142_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3720882578_m787884142(__this, p0, method) ((  KeyValuePair_2_t3720882578  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3720882578_m787884142_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>(System.Int32)
extern "C"  KeyValuePair_2_t258602264  Array_InternalArray__get_Item_TisKeyValuePair_2_t258602264_m522681728_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t258602264_m522681728(__this, p0, method) ((  KeyValuePair_2_t258602264  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t258602264_m522681728_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>(System.Int32)
extern "C"  KeyValuePair_2_t488203048  Array_InternalArray__get_Item_TisKeyValuePair_2_t488203048_m365898965_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t488203048_m365898965(__this, p0, method) ((  KeyValuePair_2_t488203048  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t488203048_m365898965_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>(System.Int32)
extern "C"  KeyValuePair_2_t3065293926  Array_InternalArray__get_Item_TisKeyValuePair_2_t3065293926_m645868912_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3065293926_m645868912(__this, p0, method) ((  KeyValuePair_2_t3065293926  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3065293926_m645868912_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t1704848460  Array_InternalArray__get_Item_TisKeyValuePair_2_t1704848460_m3960821917_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1704848460_m3960821917(__this, p0, method) ((  KeyValuePair_2_t1704848460  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1704848460_m3960821917_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t1452281081  Array_InternalArray__get_Item_TisKeyValuePair_2_t1452281081_m1517138076_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1452281081_m1517138076(__this, p0, method) ((  KeyValuePair_2_t1452281081  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1452281081_m1517138076_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t3104887502  Array_InternalArray__get_Item_TisKeyValuePair_2_t3104887502_m3445800571_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3104887502_m3445800571(__this, p0, method) ((  KeyValuePair_2_t3104887502  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3104887502_m3445800571_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.Link>(System.Int32)
extern "C"  Link_t2723257478  Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655(__this, p0, method) ((  Link_t2723257478  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern "C"  Slot_t2022531261  Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551(__this, p0, method) ((  Slot_t2022531261  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern "C"  Slot_t2267560602  Array_InternalArray__get_Item_TisSlot_t2267560602_m1004716430_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2267560602_m1004716430(__this, p0, method) ((  Slot_t2267560602  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2267560602_m1004716430_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern "C"  DateTime_t693205669  Array_InternalArray__get_Item_TisDateTime_t693205669_m3661692220_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDateTime_t693205669_m3661692220(__this, p0, method) ((  DateTime_t693205669  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDateTime_t693205669_m3661692220_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern "C"  Decimal_t724701077  Array_InternalArray__get_Item_TisDecimal_t724701077_m4156246600_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDecimal_t724701077_m4156246600(__this, p0, method) ((  Decimal_t724701077  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDecimal_t724701077_m4156246600_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Double>(System.Int32)
extern "C"  double Array_InternalArray__get_Item_TisDouble_t4078015681_m2215331088_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDouble_t4078015681_m2215331088(__this, p0, method) ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDouble_t4078015681_m2215331088_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int16>(System.Int32)
extern "C"  int16_t Array_InternalArray__get_Item_TisInt16_t4041245914_m2533263979_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt16_t4041245914_m2533263979(__this, p0, method) ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt16_t4041245914_m2533263979_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int32>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisInt32_t2071877448_m966348849_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt32_t2071877448_m966348849(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt32_t2071877448_m966348849_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
extern "C"  int64_t Array_InternalArray__get_Item_TisInt64_t909078037_m1431563204_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt64_t909078037_m1431563204(__this, p0, method) ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt64_t909078037_m1431563204_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
extern "C"  IntPtr_t Array_InternalArray__get_Item_TisIntPtr_t_m210946760_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIntPtr_t_m210946760(__this, p0, method) ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIntPtr_t_m210946760_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
extern "C"  Il2CppObject * Array_InternalArray__get_Item_TisIl2CppObject_m371871810_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIl2CppObject_m371871810(__this, p0, method) ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIl2CppObject_m371871810_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern "C"  CustomAttributeNamedArgument_t94157543  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t94157543_m4258992745_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t94157543_m4258992745(__this, p0, method) ((  CustomAttributeNamedArgument_t94157543  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t94157543_m4258992745_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern "C"  CustomAttributeTypedArgument_t1498197914  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1498197914_m1864496094_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1498197914_m1864496094(__this, p0, method) ((  CustomAttributeTypedArgument_t1498197914  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1498197914_m1864496094_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern "C"  LabelData_t3712112744  Array_InternalArray__get_Item_TisLabelData_t3712112744_m863115768_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelData_t3712112744_m863115768(__this, p0, method) ((  LabelData_t3712112744  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelData_t3712112744_m863115768_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern "C"  LabelFixup_t4090909514  Array_InternalArray__get_Item_TisLabelFixup_t4090909514_m2966857142_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelFixup_t4090909514_m2966857142(__this, p0, method) ((  LabelFixup_t4090909514  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelFixup_t4090909514_m2966857142_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern "C"  ILTokenInfo_t149559338  Array_InternalArray__get_Item_TisILTokenInfo_t149559338_m2004750537_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisILTokenInfo_t149559338_m2004750537(__this, p0, method) ((  ILTokenInfo_t149559338  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisILTokenInfo_t149559338_m2004750537_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.ParameterModifier>(System.Int32)
extern "C"  ParameterModifier_t1820634920  Array_InternalArray__get_Item_TisParameterModifier_t1820634920_m1898755304_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisParameterModifier_t1820634920_m1898755304(__this, p0, method) ((  ParameterModifier_t1820634920  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisParameterModifier_t1820634920_m1898755304_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern "C"  ResourceCacheItem_t333236149  Array_InternalArray__get_Item_TisResourceCacheItem_t333236149_m649009631_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceCacheItem_t333236149_m649009631(__this, p0, method) ((  ResourceCacheItem_t333236149  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceCacheItem_t333236149_m649009631_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern "C"  ResourceInfo_t3933049236  Array_InternalArray__get_Item_TisResourceInfo_t3933049236_m107404352_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceInfo_t3933049236_m107404352(__this, p0, method) ((  ResourceInfo_t3933049236  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceInfo_t3933049236_m107404352_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t141209596_m1747911007_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeTag_t141209596_m1747911007(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeTag_t141209596_m1747911007_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t454417549_m3315206452_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSByte_t454417549_m3315206452(__this, p0, method) ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSByte_t454417549_m3315206452_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern "C"  X509ChainStatus_t4278378721  Array_InternalArray__get_Item_TisX509ChainStatus_t4278378721_m4197592500_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisX509ChainStatus_t4278378721_m4197592500(__this, p0, method) ((  X509ChainStatus_t4278378721  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisX509ChainStatus_t4278378721_m4197592500_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern "C"  float Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753(__this, p0, method) ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern "C"  Mark_t2724874473  Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706(__this, p0, method) ((  Mark_t2724874473  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C"  TimeSpan_t3430258949  Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260(__this, p0, method) ((  TimeSpan_t3430258949  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710(__this, p0, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852(__this, p0, method) ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875(__this, p0, method) ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Google.ProtocolBuffers.AbstractBuilderLite`2/LimitedInputStream<System.Object,System.Object>::.ctor(System.IO.Stream,System.Int32)
extern Il2CppClass* Stream_t3255436806_il2cpp_TypeInfo_var;
extern const uint32_t LimitedInputStream__ctor_m1464728195_MetadataUsageId;
extern "C"  void LimitedInputStream__ctor_m1464728195_gshared (LimitedInputStream_t2498305289 * __this, Stream_t3255436806 * ___proxied0, int32_t ___size1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LimitedInputStream__ctor_m1464728195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Stream_t3255436806 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t3255436806_il2cpp_TypeInfo_var);
		Stream__ctor_m1531324023((Stream_t3255436806 *)__this, /*hidden argument*/NULL);
		Stream_t3255436806 * L_0 = ___proxied0;
		__this->set_proxied_1(L_0);
		int32_t L_1 = ___size1;
		__this->set_bytesLeft_2(L_1);
		return;
	}
}
// System.Boolean Google.ProtocolBuffers.AbstractBuilderLite`2/LimitedInputStream<System.Object,System.Object>::get_CanRead()
extern "C"  bool LimitedInputStream_get_CanRead_m4045031330_gshared (LimitedInputStream_t2498305289 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean Google.ProtocolBuffers.AbstractBuilderLite`2/LimitedInputStream<System.Object,System.Object>::get_CanSeek()
extern "C"  bool LimitedInputStream_get_CanSeek_m678109258_gshared (LimitedInputStream_t2498305289 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean Google.ProtocolBuffers.AbstractBuilderLite`2/LimitedInputStream<System.Object,System.Object>::get_CanWrite()
extern "C"  bool LimitedInputStream_get_CanWrite_m391409413_gshared (LimitedInputStream_t2498305289 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Int64 Google.ProtocolBuffers.AbstractBuilderLite`2/LimitedInputStream<System.Object,System.Object>::get_Length()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t LimitedInputStream_get_Length_m2899616465_MetadataUsageId;
extern "C"  int64_t LimitedInputStream_get_Length_m2899616465_gshared (LimitedInputStream_t2498305289 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LimitedInputStream_get_Length_m2899616465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int64 Google.ProtocolBuffers.AbstractBuilderLite`2/LimitedInputStream<System.Object,System.Object>::get_Position()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t LimitedInputStream_get_Position_m4272442068_MetadataUsageId;
extern "C"  int64_t LimitedInputStream_get_Position_m4272442068_gshared (LimitedInputStream_t2498305289 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LimitedInputStream_get_Position_m4272442068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Google.ProtocolBuffers.AbstractBuilderLite`2/LimitedInputStream<System.Object,System.Object>::set_Position(System.Int64)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t LimitedInputStream_set_Position_m3360540247_MetadataUsageId;
extern "C"  void LimitedInputStream_set_Position_m3360540247_gshared (LimitedInputStream_t2498305289 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LimitedInputStream_set_Position_m3360540247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Google.ProtocolBuffers.AbstractBuilderLite`2/LimitedInputStream<System.Object,System.Object>::Flush()
extern "C"  void LimitedInputStream_Flush_m2615453909_gshared (LimitedInputStream_t2498305289 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 Google.ProtocolBuffers.AbstractBuilderLite`2/LimitedInputStream<System.Object,System.Object>::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t LimitedInputStream_Read_m3870134332_gshared (LimitedInputStream_t2498305289 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_bytesLeft_2();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		Stream_t3255436806 * L_1 = (Stream_t3255436806 *)__this->get_proxied_1();
		ByteU5BU5D_t3397334013* L_2 = ___buffer0;
		int32_t L_3 = ___offset1;
		int32_t L_4 = (int32_t)__this->get_bytesLeft_2();
		int32_t L_5 = ___count2;
		int32_t L_6 = Math_Min_m4290821911(NULL /*static, unused*/, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		NullCheck((Stream_t3255436806 *)L_1);
		int32_t L_7 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, (Stream_t3255436806 *)L_1, (ByteU5BU5D_t3397334013*)L_2, (int32_t)L_3, (int32_t)L_6);
		V_0 = (int32_t)L_7;
		int32_t L_8 = (int32_t)__this->get_bytesLeft_2();
		int32_t L_9 = V_0;
		__this->set_bytesLeft_2(((int32_t)((int32_t)L_8-(int32_t)L_9)));
		int32_t L_10 = V_0;
		return L_10;
	}

IL_0036:
	{
		return 0;
	}
}
// System.Int64 Google.ProtocolBuffers.AbstractBuilderLite`2/LimitedInputStream<System.Object,System.Object>::Seek(System.Int64,System.IO.SeekOrigin)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t LimitedInputStream_Seek_m1538041769_MetadataUsageId;
extern "C"  int64_t LimitedInputStream_Seek_m1538041769_gshared (LimitedInputStream_t2498305289 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LimitedInputStream_Seek_m1538041769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Google.ProtocolBuffers.AbstractBuilderLite`2/LimitedInputStream<System.Object,System.Object>::SetLength(System.Int64)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t LimitedInputStream_SetLength_m3517878137_MetadataUsageId;
extern "C"  void LimitedInputStream_SetLength_m3517878137_gshared (LimitedInputStream_t2498305289 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LimitedInputStream_SetLength_m3517878137_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Google.ProtocolBuffers.AbstractBuilderLite`2/LimitedInputStream<System.Object,System.Object>::Write(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t LimitedInputStream_Write_m2588743699_MetadataUsageId;
extern "C"  void LimitedInputStream_Write_m2588743699_gshared (LimitedInputStream_t2498305289 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LimitedInputStream_Write_m2588743699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Google.ProtocolBuffers.AbstractBuilderLite`2/SerializationSurrogate<System.Object,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const Il2CppType* ByteU5BU5D_t3397334013_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral359429027;
extern const uint32_t SerializationSurrogate__ctor_m1878818754_MetadataUsageId;
extern "C"  void SerializationSurrogate__ctor_m1878818754_gshared (SerializationSurrogate_t3471339337 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializationSurrogate__ctor_m1878818754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_0 = ___info0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ByteU5BU5D_t3397334013_0_0_0_var), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t228987430 *)L_0);
		Il2CppObject * L_2 = SerializationInfo_GetValue_m1127314592((SerializationInfo_t228987430 *)L_0, (String_t*)_stringLiteral359429027, (Type_t *)L_1, /*hidden argument*/NULL);
		__this->set__message_1(((ByteU5BU5D_t3397334013*)Castclass(L_2, ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Object Google.ProtocolBuffers.AbstractBuilderLite`2/SerializationSurrogate<System.Object,System.Object>::System.Runtime.Serialization.IObjectReference.GetRealObject(System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* ExtensionRegistry_t1533220584_il2cpp_TypeInfo_var;
extern Il2CppClass* IDeserializationCallback_t327125377_il2cpp_TypeInfo_var;
extern Il2CppClass* StreamingContext_t1417235061_il2cpp_TypeInfo_var;
extern const uint32_t SerializationSurrogate_System_Runtime_Serialization_IObjectReference_GetRealObject_m1320713497_MetadataUsageId;
extern "C"  Il2CppObject * SerializationSurrogate_System_Runtime_Serialization_IObjectReference_GetRealObject_m1320713497_gshared (SerializationSurrogate_t3471339337 * __this, StreamingContext_t1417235061  ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializationSurrogate_System_Runtime_Serialization_IObjectReference_GetRealObject_m1320713497_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ExtensionRegistry_t1533220584 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	ExtensionRegistry_t1533220584 * G_B2_0 = NULL;
	ByteU5BU5D_t3397334013* G_B2_1 = NULL;
	Il2CppObject ** G_B2_2 = NULL;
	ExtensionRegistry_t1533220584 * G_B1_0 = NULL;
	ByteU5BU5D_t3397334013* G_B1_1 = NULL;
	Il2CppObject ** G_B1_2 = NULL;
	{
		Il2CppObject * L_0 = StreamingContext_get_Context_m2568771257((StreamingContext_t1417235061 *)(&___context0), /*hidden argument*/NULL);
		V_0 = (ExtensionRegistry_t1533220584 *)((ExtensionRegistry_t1533220584 *)IsInst(L_0, ExtensionRegistry_t1533220584_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_1 = ((SerializationSurrogate_t3471339337_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_TemplateInstance_0();
		V_2 = (Il2CppObject *)L_1;
		NullCheck((AbstractBuilderLite_2_t466644448 *)(*(&V_2)));
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* TMessage Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::get_DefaultInstanceForType() */, (AbstractBuilderLite_2_t466644448 *)(*(&V_2)));
		V_3 = (Il2CppObject *)L_2;
		NullCheck((AbstractMessageLite_2_t2741522770 *)(*(&V_3)));
		Il2CppObject * L_3 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* TBuilder Google.ProtocolBuffers.AbstractMessageLite`2<System.Object,System.Object>::CreateBuilderForType() */, (AbstractMessageLite_2_t2741522770 *)(*(&V_3)));
		V_1 = (Il2CppObject *)L_3;
		ByteU5BU5D_t3397334013* L_4 = (ByteU5BU5D_t3397334013*)__this->get__message_1();
		ExtensionRegistry_t1533220584 * L_5 = V_0;
		ExtensionRegistry_t1533220584 * L_6 = (ExtensionRegistry_t1533220584 *)L_5;
		G_B1_0 = L_6;
		G_B1_1 = L_4;
		G_B1_2 = (&V_1);
		if (L_6)
		{
			G_B2_0 = L_6;
			G_B2_1 = L_4;
			G_B2_2 = (&V_1);
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ExtensionRegistry_t1533220584_il2cpp_TypeInfo_var);
		ExtensionRegistry_t1533220584 * L_7 = ExtensionRegistry_get_Empty_m128320802(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B2_0 = L_7;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0044:
	{
		NullCheck((AbstractBuilderLite_2_t466644448 *)(*G_B2_2));
		((  Il2CppObject * (*) (AbstractBuilderLite_2_t466644448 *, ByteU5BU5D_t3397334013*, ExtensionRegistry_t1533220584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((AbstractBuilderLite_2_t466644448 *)(*G_B2_2), (ByteU5BU5D_t3397334013*)G_B2_1, (ExtensionRegistry_t1533220584 *)G_B2_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Il2CppObject * L_8 = V_1;
		V_4 = (Il2CppObject *)((Il2CppObject *)IsInst(L_8, IDeserializationCallback_t327125377_il2cpp_TypeInfo_var));
		Il2CppObject * L_9 = V_4;
		if (!L_9)
		{
			goto IL_0071;
		}
	}
	{
		Il2CppObject * L_10 = V_4;
		StreamingContext_t1417235061  L_11 = ___context0;
		StreamingContext_t1417235061  L_12 = L_11;
		Il2CppObject * L_13 = Box(StreamingContext_t1417235061_il2cpp_TypeInfo_var, &L_12);
		NullCheck((Il2CppObject *)L_10);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(0 /* System.Void System.Runtime.Serialization.IDeserializationCallback::OnDeserialization(System.Object) */, IDeserializationCallback_t327125377_il2cpp_TypeInfo_var, (Il2CppObject *)L_10, (Il2CppObject *)L_13);
	}

IL_0071:
	{
		Il2CppObject * L_14 = V_1;
		return L_14;
	}
}
// System.Void Google.ProtocolBuffers.AbstractBuilderLite`2/SerializationSurrogate<System.Object,System.Object>::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppCodeGenString* _stringLiteral359429027;
extern const uint32_t SerializationSurrogate_System_Runtime_Serialization_ISerializable_GetObjectData_m3277519276_MetadataUsageId;
extern "C"  void SerializationSurrogate_System_Runtime_Serialization_ISerializable_GetObjectData_m3277519276_gshared (SerializationSurrogate_t3471339337 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializationSurrogate_System_Runtime_Serialization_ISerializable_GetObjectData_m3277519276_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		ByteU5BU5D_t3397334013* L_1 = (ByteU5BU5D_t3397334013*)__this->get__message_1();
		NullCheck((SerializationInfo_t228987430 *)L_0);
		SerializationInfo_AddValue_m1740888931((SerializationInfo_t228987430 *)L_0, (String_t*)_stringLiteral359429027, (Il2CppObject *)(Il2CppObject *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.ProtocolBuffers.AbstractBuilderLite`2/SerializationSurrogate<System.Object,System.Object>::.cctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t SerializationSurrogate__cctor_m18053472_MetadataUsageId;
extern "C"  void SerializationSurrogate__cctor_m18053472_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializationSurrogate__cctor_m18053472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)), /*hidden argument*/NULL);
		Il2CppObject * L_1 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		((SerializationSurrogate_t3471339337_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_TemplateInstance_0(((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
		return;
	}
}
// System.Void Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::.ctor()
extern "C"  void AbstractBuilderLite_2__ctor_m853197036_gshared (AbstractBuilderLite_2_t466644448 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream)
extern Il2CppClass* ExtensionRegistry_t1533220584_il2cpp_TypeInfo_var;
extern const uint32_t AbstractBuilderLite_2_MergeFrom_m398053804_MetadataUsageId;
extern "C"  Il2CppObject * AbstractBuilderLite_2_MergeFrom_m398053804_gshared (AbstractBuilderLite_2_t466644448 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AbstractBuilderLite_2_MergeFrom_m398053804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(ExtensionRegistry_t1533220584_il2cpp_TypeInfo_var);
		ExtensionRegistry_t1533220584 * L_1 = ExtensionRegistry_CreateInstance_m2437720959(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ExtensionRegistry_t1533220584 * >::Invoke(15 /* TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, (AbstractBuilderLite_2_t466644448 *)__this, (Il2CppObject *)L_0, (ExtensionRegistry_t1533220584 *)L_1);
		return L_2;
	}
}
// TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeDelimitedFrom(System.IO.Stream)
extern Il2CppClass* ExtensionRegistry_t1533220584_il2cpp_TypeInfo_var;
extern const uint32_t AbstractBuilderLite_2_MergeDelimitedFrom_m3265519768_MetadataUsageId;
extern "C"  Il2CppObject * AbstractBuilderLite_2_MergeDelimitedFrom_m3265519768_gshared (AbstractBuilderLite_2_t466644448 * __this, Stream_t3255436806 * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AbstractBuilderLite_2_MergeDelimitedFrom_m3265519768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stream_t3255436806 * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(ExtensionRegistry_t1533220584_il2cpp_TypeInfo_var);
		ExtensionRegistry_t1533220584 * L_1 = ExtensionRegistry_CreateInstance_m2437720959(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (AbstractBuilderLite_2_t466644448 *, Stream_t3255436806 *, ExtensionRegistry_t1533220584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((AbstractBuilderLite_2_t466644448 *)__this, (Stream_t3255436806 *)L_0, (ExtensionRegistry_t1533220584 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_2;
	}
}
// TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C"  Il2CppObject * AbstractBuilderLite_2_MergeDelimitedFrom_m687152940_gshared (AbstractBuilderLite_2_t466644448 * __this, Stream_t3255436806 * ___input0, ExtensionRegistry_t1533220584 * ___extensionRegistry1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Stream_t3255436806 * V_1 = NULL;
	{
		Stream_t3255436806 * L_0 = ___input0;
		uint32_t L_1 = CodedInputStream_ReadRawVarint32_m2586515627(NULL /*static, unused*/, (Stream_t3255436806 *)L_0, /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		Stream_t3255436806 * L_2 = ___input0;
		int32_t L_3 = V_0;
		LimitedInputStream_t2498305289 * L_4 = (LimitedInputStream_t2498305289 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (LimitedInputStream_t2498305289 *, Stream_t3255436806 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_4, (Stream_t3255436806 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_1 = (Stream_t3255436806 *)L_4;
		Stream_t3255436806 * L_5 = V_1;
		ExtensionRegistry_t1533220584 * L_6 = ___extensionRegistry1;
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (AbstractBuilderLite_2_t466644448 *, Stream_t3255436806 *, ExtensionRegistry_t1533220584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((AbstractBuilderLite_2_t466644448 *)__this, (Stream_t3255436806 *)L_5, (ExtensionRegistry_t1533220584 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_7;
	}
}
// TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.ByteString)
extern Il2CppClass* ExtensionRegistry_t1533220584_il2cpp_TypeInfo_var;
extern const uint32_t AbstractBuilderLite_2_MergeFrom_m3007917435_MetadataUsageId;
extern "C"  Il2CppObject * AbstractBuilderLite_2_MergeFrom_m3007917435_gshared (AbstractBuilderLite_2_t466644448 * __this, ByteString_t3153909979 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AbstractBuilderLite_2_MergeFrom_m3007917435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteString_t3153909979 * L_0 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(ExtensionRegistry_t1533220584_il2cpp_TypeInfo_var);
		ExtensionRegistry_t1533220584 * L_1 = ExtensionRegistry_CreateInstance_m2437720959(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (AbstractBuilderLite_2_t466644448 *, ByteString_t3153909979 *, ExtensionRegistry_t1533220584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((AbstractBuilderLite_2_t466644448 *)__this, (ByteString_t3153909979 *)L_0, (ExtensionRegistry_t1533220584 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_2;
	}
}
// TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
extern "C"  Il2CppObject * AbstractBuilderLite_2_MergeFrom_m135221719_gshared (AbstractBuilderLite_2_t466644448 * __this, ByteString_t3153909979 * ___data0, ExtensionRegistry_t1533220584 * ___extensionRegistry1, const MethodInfo* method)
{
	CodedInputStream_t3771275139 * V_0 = NULL;
	{
		ByteString_t3153909979 * L_0 = ___data0;
		NullCheck((ByteString_t3153909979 *)L_0);
		CodedInputStream_t3771275139 * L_1 = ByteString_CreateCodedInput_m2259459549((ByteString_t3153909979 *)L_0, /*hidden argument*/NULL);
		V_0 = (CodedInputStream_t3771275139 *)L_1;
		CodedInputStream_t3771275139 * L_2 = V_0;
		ExtensionRegistry_t1533220584 * L_3 = ___extensionRegistry1;
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ExtensionRegistry_t1533220584 * >::Invoke(15 /* TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, (AbstractBuilderLite_2_t466644448 *)__this, (Il2CppObject *)L_2, (ExtensionRegistry_t1533220584 *)L_3);
		CodedInputStream_t3771275139 * L_4 = V_0;
		NullCheck((CodedInputStream_t3771275139 *)L_4);
		CodedInputStream_CheckLastTagWas_m2807095497((CodedInputStream_t3771275139 *)L_4, (uint32_t)0, /*hidden argument*/NULL);
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_5 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::get_ThisBuilder() */, (AbstractBuilderLite_2_t466644448 *)__this);
		return L_5;
	}
}
// TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(System.Byte[])
extern "C"  Il2CppObject * AbstractBuilderLite_2_MergeFrom_m2344399407_gshared (AbstractBuilderLite_2_t466644448 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	CodedInputStream_t3771275139 * V_0 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		CodedInputStream_t3771275139 * L_1 = CodedInputStream_CreateInstance_m3944294824(NULL /*static, unused*/, (ByteU5BU5D_t3397334013*)L_0, /*hidden argument*/NULL);
		V_0 = (CodedInputStream_t3771275139 *)L_1;
		CodedInputStream_t3771275139 * L_2 = V_0;
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(16 /* TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream) */, (AbstractBuilderLite_2_t466644448 *)__this, (Il2CppObject *)L_2);
		CodedInputStream_t3771275139 * L_3 = V_0;
		NullCheck((CodedInputStream_t3771275139 *)L_3);
		CodedInputStream_CheckLastTagWas_m2807095497((CodedInputStream_t3771275139 *)L_3, (uint32_t)0, /*hidden argument*/NULL);
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_4 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::get_ThisBuilder() */, (AbstractBuilderLite_2_t466644448 *)__this);
		return L_4;
	}
}
// TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
extern "C"  Il2CppObject * AbstractBuilderLite_2_MergeFrom_m3021106595_gshared (AbstractBuilderLite_2_t466644448 * __this, ByteU5BU5D_t3397334013* ___data0, ExtensionRegistry_t1533220584 * ___extensionRegistry1, const MethodInfo* method)
{
	CodedInputStream_t3771275139 * V_0 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		CodedInputStream_t3771275139 * L_1 = CodedInputStream_CreateInstance_m3944294824(NULL /*static, unused*/, (ByteU5BU5D_t3397334013*)L_0, /*hidden argument*/NULL);
		V_0 = (CodedInputStream_t3771275139 *)L_1;
		CodedInputStream_t3771275139 * L_2 = V_0;
		ExtensionRegistry_t1533220584 * L_3 = ___extensionRegistry1;
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ExtensionRegistry_t1533220584 * >::Invoke(15 /* TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, (AbstractBuilderLite_2_t466644448 *)__this, (Il2CppObject *)L_2, (ExtensionRegistry_t1533220584 *)L_3);
		CodedInputStream_t3771275139 * L_4 = V_0;
		NullCheck((CodedInputStream_t3771275139 *)L_4);
		CodedInputStream_CheckLastTagWas_m2807095497((CodedInputStream_t3771275139 *)L_4, (uint32_t)0, /*hidden argument*/NULL);
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_5 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::get_ThisBuilder() */, (AbstractBuilderLite_2_t466644448 *)__this);
		return L_5;
	}
}
// TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(System.IO.Stream)
extern "C"  Il2CppObject * AbstractBuilderLite_2_MergeFrom_m2553864819_gshared (AbstractBuilderLite_2_t466644448 * __this, Stream_t3255436806 * ___input0, const MethodInfo* method)
{
	CodedInputStream_t3771275139 * V_0 = NULL;
	{
		Stream_t3255436806 * L_0 = ___input0;
		CodedInputStream_t3771275139 * L_1 = CodedInputStream_CreateInstance_m4063094634(NULL /*static, unused*/, (Stream_t3255436806 *)L_0, /*hidden argument*/NULL);
		V_0 = (CodedInputStream_t3771275139 *)L_1;
		CodedInputStream_t3771275139 * L_2 = V_0;
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(16 /* TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream) */, (AbstractBuilderLite_2_t466644448 *)__this, (Il2CppObject *)L_2);
		CodedInputStream_t3771275139 * L_3 = V_0;
		NullCheck((CodedInputStream_t3771275139 *)L_3);
		CodedInputStream_CheckLastTagWas_m2807095497((CodedInputStream_t3771275139 *)L_3, (uint32_t)0, /*hidden argument*/NULL);
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_4 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::get_ThisBuilder() */, (AbstractBuilderLite_2_t466644448 *)__this);
		return L_4;
	}
}
// TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C"  Il2CppObject * AbstractBuilderLite_2_MergeFrom_m1846313703_gshared (AbstractBuilderLite_2_t466644448 * __this, Stream_t3255436806 * ___input0, ExtensionRegistry_t1533220584 * ___extensionRegistry1, const MethodInfo* method)
{
	CodedInputStream_t3771275139 * V_0 = NULL;
	{
		Stream_t3255436806 * L_0 = ___input0;
		CodedInputStream_t3771275139 * L_1 = CodedInputStream_CreateInstance_m4063094634(NULL /*static, unused*/, (Stream_t3255436806 *)L_0, /*hidden argument*/NULL);
		V_0 = (CodedInputStream_t3771275139 *)L_1;
		CodedInputStream_t3771275139 * L_2 = V_0;
		ExtensionRegistry_t1533220584 * L_3 = ___extensionRegistry1;
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ExtensionRegistry_t1533220584 * >::Invoke(15 /* TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, (AbstractBuilderLite_2_t466644448 *)__this, (Il2CppObject *)L_2, (ExtensionRegistry_t1533220584 *)L_3);
		CodedInputStream_t3771275139 * L_4 = V_0;
		NullCheck((CodedInputStream_t3771275139 *)L_4);
		CodedInputStream_CheckLastTagWas_m2807095497((CodedInputStream_t3771275139 *)L_4, (uint32_t)0, /*hidden argument*/NULL);
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_5 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::get_ThisBuilder() */, (AbstractBuilderLite_2_t466644448 *)__this);
		return L_5;
	}
}
// Google.ProtocolBuffers.IBuilderLite Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::Google.ProtocolBuffers.IBuilderLite.WeakMergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C"  Il2CppObject * AbstractBuilderLite_2_Google_ProtocolBuffers_IBuilderLite_WeakMergeFrom_m2725263473_gshared (AbstractBuilderLite_2_t466644448 * __this, Il2CppObject * ___input0, ExtensionRegistry_t1533220584 * ___registry1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___input0;
		ExtensionRegistry_t1533220584 * L_1 = ___registry1;
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ExtensionRegistry_t1533220584 * >::Invoke(15 /* TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, (AbstractBuilderLite_2_t466644448 *)__this, (Il2CppObject *)L_0, (ExtensionRegistry_t1533220584 *)L_1);
		return (Il2CppObject *)L_2;
	}
}
// Google.ProtocolBuffers.IMessageLite Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::Google.ProtocolBuffers.IBuilderLite.WeakBuildPartial()
extern "C"  Il2CppObject * AbstractBuilderLite_2_Google_ProtocolBuffers_IBuilderLite_WeakBuildPartial_m1592116274_gshared (AbstractBuilderLite_2_t466644448 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(13 /* TMessage Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::BuildPartial() */, (AbstractBuilderLite_2_t466644448 *)__this);
		return (Il2CppObject *)L_0;
	}
}
// System.Void Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral359429027;
extern const uint32_t AbstractBuilderLite_2_System_Runtime_Serialization_ISerializable_GetObjectData_m3382912655_MetadataUsageId;
extern "C"  void AbstractBuilderLite_2_System_Runtime_Serialization_ISerializable_GetObjectData_m3382912655_gshared (AbstractBuilderLite_2_t466644448 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AbstractBuilderLite_2_System_Runtime_Serialization_ISerializable_GetObjectData_m3382912655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 11)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t228987430 *)L_0);
		SerializationInfo_SetType_m499725474((SerializationInfo_t228987430 *)L_0, (Type_t *)L_1, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_2 = ___info0;
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_3 = VirtFuncInvoker0< Il2CppObject * >::Invoke(11 /* TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::Clone() */, (AbstractBuilderLite_2_t466644448 *)__this);
		V_0 = (Il2CppObject *)L_3;
		NullCheck((AbstractBuilderLite_2_t466644448 *)(*(&V_0)));
		Il2CppObject * L_4 = VirtFuncInvoker0< Il2CppObject * >::Invoke(13 /* TMessage Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::BuildPartial() */, (AbstractBuilderLite_2_t466644448 *)(*(&V_0)));
		V_1 = (Il2CppObject *)L_4;
		NullCheck((AbstractMessageLite_2_t2741522770 *)(*(&V_1)));
		ByteU5BU5D_t3397334013* L_5 = ((  ByteU5BU5D_t3397334013* (*) (AbstractMessageLite_2_t2741522770 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((AbstractMessageLite_2_t2741522770 *)(*(&V_1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		NullCheck((SerializationInfo_t228987430 *)L_2);
		SerializationInfo_AddValue_m1740888931((SerializationInfo_t228987430 *)L_2, (String_t*)_stringLiteral359429027, (Il2CppObject *)(Il2CppObject *)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.ProtocolBuffers.AbstractMessageLite`2/SerializationSurrogate<System.Object,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const Il2CppType* ByteU5BU5D_t3397334013_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral359429027;
extern Il2CppCodeGenString* _stringLiteral159719384;
extern const uint32_t SerializationSurrogate__ctor_m641633370_MetadataUsageId;
extern "C"  void SerializationSurrogate__ctor_m641633370_gshared (SerializationSurrogate_t1165551381 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializationSurrogate__ctor_m641633370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_0 = ___info0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ByteU5BU5D_t3397334013_0_0_0_var), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t228987430 *)L_0);
		Il2CppObject * L_2 = SerializationInfo_GetValue_m1127314592((SerializationInfo_t228987430 *)L_0, (String_t*)_stringLiteral359429027, (Type_t *)L_1, /*hidden argument*/NULL);
		__this->set__message_1(((ByteU5BU5D_t3397334013*)Castclass(L_2, ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var)));
		SerializationInfo_t228987430 * L_3 = ___info0;
		NullCheck((SerializationInfo_t228987430 *)L_3);
		bool L_4 = SerializationInfo_GetBoolean_m3573708305((SerializationInfo_t228987430 *)L_3, (String_t*)_stringLiteral159719384, /*hidden argument*/NULL);
		__this->set__initialized_2(L_4);
		return;
	}
}
// System.Object Google.ProtocolBuffers.AbstractMessageLite`2/SerializationSurrogate<System.Object,System.Object>::System.Runtime.Serialization.IObjectReference.GetRealObject(System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* ExtensionRegistry_t1533220584_il2cpp_TypeInfo_var;
extern Il2CppClass* IDeserializationCallback_t327125377_il2cpp_TypeInfo_var;
extern Il2CppClass* StreamingContext_t1417235061_il2cpp_TypeInfo_var;
extern const uint32_t SerializationSurrogate_System_Runtime_Serialization_IObjectReference_GetRealObject_m3469197279_MetadataUsageId;
extern "C"  Il2CppObject * SerializationSurrogate_System_Runtime_Serialization_IObjectReference_GetRealObject_m3469197279_gshared (SerializationSurrogate_t1165551381 * __this, StreamingContext_t1417235061  ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializationSurrogate_System_Runtime_Serialization_IObjectReference_GetRealObject_m3469197279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ExtensionRegistry_t1533220584 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	ExtensionRegistry_t1533220584 * G_B2_0 = NULL;
	ByteU5BU5D_t3397334013* G_B2_1 = NULL;
	Il2CppObject ** G_B2_2 = NULL;
	ExtensionRegistry_t1533220584 * G_B1_0 = NULL;
	ByteU5BU5D_t3397334013* G_B1_1 = NULL;
	Il2CppObject ** G_B1_2 = NULL;
	Il2CppObject * G_B7_0 = NULL;
	{
		Il2CppObject * L_0 = StreamingContext_get_Context_m2568771257((StreamingContext_t1417235061 *)(&___context0), /*hidden argument*/NULL);
		V_0 = (ExtensionRegistry_t1533220584 *)((ExtensionRegistry_t1533220584 *)IsInst(L_0, ExtensionRegistry_t1533220584_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_1 = ((SerializationSurrogate_t1165551381_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_TemplateInstance_0();
		V_2 = (Il2CppObject *)L_1;
		NullCheck((AbstractBuilderLite_2_t466644448 *)(*(&V_2)));
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* TMessage Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::get_DefaultInstanceForType() */, (AbstractBuilderLite_2_t466644448 *)(*(&V_2)));
		V_3 = (Il2CppObject *)L_2;
		NullCheck((AbstractMessageLite_2_t2741522770 *)(*(&V_3)));
		Il2CppObject * L_3 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* TBuilder Google.ProtocolBuffers.AbstractMessageLite`2<System.Object,System.Object>::CreateBuilderForType() */, (AbstractMessageLite_2_t2741522770 *)(*(&V_3)));
		V_1 = (Il2CppObject *)L_3;
		ByteU5BU5D_t3397334013* L_4 = (ByteU5BU5D_t3397334013*)__this->get__message_1();
		ExtensionRegistry_t1533220584 * L_5 = V_0;
		ExtensionRegistry_t1533220584 * L_6 = (ExtensionRegistry_t1533220584 *)L_5;
		G_B1_0 = L_6;
		G_B1_1 = L_4;
		G_B1_2 = (&V_1);
		if (L_6)
		{
			G_B2_0 = L_6;
			G_B2_1 = L_4;
			G_B2_2 = (&V_1);
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ExtensionRegistry_t1533220584_il2cpp_TypeInfo_var);
		ExtensionRegistry_t1533220584 * L_7 = ExtensionRegistry_get_Empty_m128320802(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B2_0 = L_7;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0044:
	{
		NullCheck((AbstractBuilderLite_2_t466644448 *)(*G_B2_2));
		((  Il2CppObject * (*) (AbstractBuilderLite_2_t466644448 *, ByteU5BU5D_t3397334013*, ExtensionRegistry_t1533220584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((AbstractBuilderLite_2_t466644448 *)(*G_B2_2), (ByteU5BU5D_t3397334013*)G_B2_1, (ExtensionRegistry_t1533220584 *)G_B2_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Il2CppObject * L_8 = V_1;
		V_4 = (Il2CppObject *)((Il2CppObject *)IsInst(L_8, IDeserializationCallback_t327125377_il2cpp_TypeInfo_var));
		Il2CppObject * L_9 = V_4;
		if (!L_9)
		{
			goto IL_0071;
		}
	}
	{
		Il2CppObject * L_10 = V_4;
		StreamingContext_t1417235061  L_11 = ___context0;
		StreamingContext_t1417235061  L_12 = L_11;
		Il2CppObject * L_13 = Box(StreamingContext_t1417235061_il2cpp_TypeInfo_var, &L_12);
		NullCheck((Il2CppObject *)L_10);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(0 /* System.Void System.Runtime.Serialization.IDeserializationCallback::OnDeserialization(System.Object) */, IDeserializationCallback_t327125377_il2cpp_TypeInfo_var, (Il2CppObject *)L_10, (Il2CppObject *)L_13);
	}

IL_0071:
	{
		bool L_14 = (bool)__this->get__initialized_2();
		if (!L_14)
		{
			goto IL_008e;
		}
	}
	{
		NullCheck((AbstractBuilderLite_2_t466644448 *)(*(&V_1)));
		Il2CppObject * L_15 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* TMessage Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::Build() */, (AbstractBuilderLite_2_t466644448 *)(*(&V_1)));
		G_B7_0 = L_15;
		goto IL_009b;
	}

IL_008e:
	{
		NullCheck((AbstractBuilderLite_2_t466644448 *)(*(&V_1)));
		Il2CppObject * L_16 = VirtFuncInvoker0< Il2CppObject * >::Invoke(13 /* TMessage Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::BuildPartial() */, (AbstractBuilderLite_2_t466644448 *)(*(&V_1)));
		G_B7_0 = L_16;
	}

IL_009b:
	{
		V_5 = (Il2CppObject *)G_B7_0;
		Il2CppObject * L_17 = V_5;
		V_4 = (Il2CppObject *)((Il2CppObject *)IsInst(L_17, IDeserializationCallback_t327125377_il2cpp_TypeInfo_var));
		Il2CppObject * L_18 = V_4;
		if (!L_18)
		{
			goto IL_00bf;
		}
	}
	{
		Il2CppObject * L_19 = V_4;
		StreamingContext_t1417235061  L_20 = ___context0;
		StreamingContext_t1417235061  L_21 = L_20;
		Il2CppObject * L_22 = Box(StreamingContext_t1417235061_il2cpp_TypeInfo_var, &L_21);
		NullCheck((Il2CppObject *)L_19);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(0 /* System.Void System.Runtime.Serialization.IDeserializationCallback::OnDeserialization(System.Object) */, IDeserializationCallback_t327125377_il2cpp_TypeInfo_var, (Il2CppObject *)L_19, (Il2CppObject *)L_22);
	}

IL_00bf:
	{
		Il2CppObject * L_23 = V_5;
		return L_23;
	}
}
// System.Void Google.ProtocolBuffers.AbstractMessageLite`2/SerializationSurrogate<System.Object,System.Object>::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppCodeGenString* _stringLiteral359429027;
extern const uint32_t SerializationSurrogate_System_Runtime_Serialization_ISerializable_GetObjectData_m1889839872_MetadataUsageId;
extern "C"  void SerializationSurrogate_System_Runtime_Serialization_ISerializable_GetObjectData_m1889839872_gshared (SerializationSurrogate_t1165551381 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializationSurrogate_System_Runtime_Serialization_ISerializable_GetObjectData_m1889839872_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		ByteU5BU5D_t3397334013* L_1 = (ByteU5BU5D_t3397334013*)__this->get__message_1();
		NullCheck((SerializationInfo_t228987430 *)L_0);
		SerializationInfo_AddValue_m1740888931((SerializationInfo_t228987430 *)L_0, (String_t*)_stringLiteral359429027, (Il2CppObject *)(Il2CppObject *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.ProtocolBuffers.AbstractMessageLite`2/SerializationSurrogate<System.Object,System.Object>::.cctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t SerializationSurrogate__cctor_m3321906972_MetadataUsageId;
extern "C"  void SerializationSurrogate__cctor_m3321906972_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializationSurrogate__cctor_m3321906972_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Il2CppObject * L_1 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		((SerializationSurrogate_t1165551381_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_TemplateInstance_0(((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
		return;
	}
}
// System.Void Google.ProtocolBuffers.AbstractMessageLite`2<System.Object,System.Object>::.ctor()
extern "C"  void AbstractMessageLite_2__ctor_m1302916032_gshared (AbstractMessageLite_2_t2741522770 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] Google.ProtocolBuffers.AbstractMessageLite`2<System.Object,System.Object>::ToByteArray()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* CodedOutputStream_t2164392868_il2cpp_TypeInfo_var;
extern const uint32_t AbstractMessageLite_2_ToByteArray_m1427422972_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* AbstractMessageLite_2_ToByteArray_m1427422972_gshared (AbstractMessageLite_2_t2741522770 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AbstractMessageLite_2_ToByteArray_m1427422972_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	CodedOutputStream_t2164392868 * V_1 = NULL;
	{
		NullCheck((AbstractMessageLite_2_t2741522770 *)__this);
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(11 /* System.Int32 Google.ProtocolBuffers.AbstractMessageLite`2<System.Object,System.Object>::get_SerializedSize() */, (AbstractMessageLite_2_t2741522770 *)__this);
		V_0 = (ByteU5BU5D_t3397334013*)((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_0));
		ByteU5BU5D_t3397334013* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t2164392868_il2cpp_TypeInfo_var);
		CodedOutputStream_t2164392868 * L_2 = CodedOutputStream_CreateInstance_m4038444492(NULL /*static, unused*/, (ByteU5BU5D_t3397334013*)L_1, /*hidden argument*/NULL);
		V_1 = (CodedOutputStream_t2164392868 *)L_2;
		CodedOutputStream_t2164392868 * L_3 = V_1;
		NullCheck((AbstractMessageLite_2_t2741522770 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(14 /* System.Void Google.ProtocolBuffers.AbstractMessageLite`2<System.Object,System.Object>::WriteTo(Google.ProtocolBuffers.ICodedOutputStream) */, (AbstractMessageLite_2_t2741522770 *)__this, (Il2CppObject *)L_3);
		CodedOutputStream_t2164392868 * L_4 = V_1;
		NullCheck((CodedOutputStream_t2164392868 *)L_4);
		CodedOutputStream_CheckNoSpaceLeft_m1082852855((CodedOutputStream_t2164392868 *)L_4, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_5 = V_0;
		return L_5;
	}
}
// Google.ProtocolBuffers.IBuilderLite Google.ProtocolBuffers.AbstractMessageLite`2<System.Object,System.Object>::Google.ProtocolBuffers.IMessageLite.WeakCreateBuilderForType()
extern "C"  Il2CppObject * AbstractMessageLite_2_Google_ProtocolBuffers_IMessageLite_WeakCreateBuilderForType_m1076781271_gshared (AbstractMessageLite_2_t2741522770 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractMessageLite_2_t2741522770 *)__this);
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* TBuilder Google.ProtocolBuffers.AbstractMessageLite`2<System.Object,System.Object>::CreateBuilderForType() */, (AbstractMessageLite_2_t2741522770 *)__this);
		return (Il2CppObject *)L_0;
	}
}
// System.Void Google.ProtocolBuffers.AbstractMessageLite`2<System.Object,System.Object>::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral359429027;
extern Il2CppCodeGenString* _stringLiteral159719384;
extern const uint32_t AbstractMessageLite_2_System_Runtime_Serialization_ISerializable_GetObjectData_m3589202125_MetadataUsageId;
extern "C"  void AbstractMessageLite_2_System_Runtime_Serialization_ISerializable_GetObjectData_m3589202125_gshared (AbstractMessageLite_2_t2741522770 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AbstractMessageLite_2_System_Runtime_Serialization_ISerializable_GetObjectData_m3589202125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t228987430 *)L_0);
		SerializationInfo_SetType_m499725474((SerializationInfo_t228987430 *)L_0, (Type_t *)L_1, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_2 = ___info0;
		NullCheck((AbstractMessageLite_2_t2741522770 *)__this);
		ByteU5BU5D_t3397334013* L_3 = ((  ByteU5BU5D_t3397334013* (*) (AbstractMessageLite_2_t2741522770 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((AbstractMessageLite_2_t2741522770 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((SerializationInfo_t228987430 *)L_2);
		SerializationInfo_AddValue_m1740888931((SerializationInfo_t228987430 *)L_2, (String_t*)_stringLiteral359429027, (Il2CppObject *)(Il2CppObject *)L_3, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_4 = ___info0;
		NullCheck((AbstractMessageLite_2_t2741522770 *)__this);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Google.ProtocolBuffers.AbstractMessageLite`2<System.Object,System.Object>::get_IsInitialized() */, (AbstractMessageLite_2_t2741522770 *)__this);
		NullCheck((SerializationInfo_t228987430 *)L_4);
		SerializationInfo_AddValue_m1192926088((SerializationInfo_t228987430 *)L_4, (String_t*)_stringLiteral159719384, (bool)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IList`1<T> Google.ProtocolBuffers.Collections.Lists`1<System.Object>::AsReadOnly(System.Collections.Generic.IList`1<T>)
extern "C"  Il2CppObject* Lists_1_AsReadOnly_m2801529565_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method)
{
	Il2CppObject* G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = ___list0;
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		Il2CppObject* L_2 = ___list0;
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_0011:
	{
		Il2CppObject* L_3 = ___list0;
		ReadOnlyCollection_1_t2875234987 * L_4 = (ReadOnlyCollection_1_t2875234987 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (ReadOnlyCollection_1_t2875234987 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(L_4, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B3_0 = ((Il2CppObject*)(L_4));
	}

IL_0017:
	{
		return G_B3_0;
	}
}
// System.Void Google.ProtocolBuffers.Collections.Lists`1<System.Object>::.cctor()
extern "C"  void Lists_1__cctor_m3139840917_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		ReadOnlyCollection_1_t2875234987 * L_0 = (ReadOnlyCollection_1_t2875234987 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (ReadOnlyCollection_1_t2875234987 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(L_0, (Il2CppObject*)(Il2CppObject*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (uint32_t)0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((Lists_1_t1182124910_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->static_fields)->set_empty_0(L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<T> Google.ProtocolBuffers.Collections.Lists`1<System.Single>::AsReadOnly(System.Collections.Generic.IList`1<T>)
extern "C"  Il2CppObject* Lists_1_AsReadOnly_m967389356_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method)
{
	Il2CppObject* G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = ___list0;
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Single>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		Il2CppObject* L_2 = ___list0;
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_0011:
	{
		Il2CppObject* L_3 = ___list0;
		ReadOnlyCollection_1_t2262295624 * L_4 = (ReadOnlyCollection_1_t2262295624 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (ReadOnlyCollection_1_t2262295624 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(L_4, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B3_0 = ((Il2CppObject*)(L_4));
	}

IL_0017:
	{
		return G_B3_0;
	}
}
// System.Void Google.ProtocolBuffers.Collections.Lists`1<System.Single>::.cctor()
extern "C"  void Lists_1__cctor_m3769295578_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		ReadOnlyCollection_1_t2262295624 * L_0 = (ReadOnlyCollection_1_t2262295624 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (ReadOnlyCollection_1_t2262295624 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(L_0, (Il2CppObject*)(Il2CppObject*)((SingleU5BU5D_t577127397*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (uint32_t)0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((Lists_1_t569185547_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->static_fields)->set_empty_0(L_0);
		return;
	}
}
// T Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern const uint32_t PopsicleList_1_get_Item_m2496588592_MetadataUsageId;
extern "C"  Il2CppObject * PopsicleList_1_get_Item_m2496588592_gshared (PopsicleList_1_t884098166 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PopsicleList_1_get_Item_m2496588592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_items_2();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m15523695(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		List_1_t2058570427 * L_2 = (List_1_t2058570427 *)__this->get_items_2();
		int32_t L_3 = ___index0;
		NullCheck((List_1_t2058570427 *)L_2);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((List_1_t2058570427 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_4;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void PopsicleList_1_set_Item_m3214929255_gshared (PopsicleList_1_t884098166 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		NullCheck((PopsicleList_1_t884098166 *)__this);
		((  void (*) (PopsicleList_1_t884098166 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((PopsicleList_1_t884098166 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		bool L_0 = ((PopsicleList_1_t884098166_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)->static_fields)->get_CheckForNull_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject * L_1 = ___value1;
		ThrowHelper_ThrowIfNull_m2318750201(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_001b:
	{
		List_1_t2058570427 * L_2 = (List_1_t2058570427 *)__this->get_items_2();
		int32_t L_3 = ___index0;
		Il2CppObject * L_4 = ___value1;
		NullCheck((List_1_t2058570427 *)L_2);
		((  void (*) (List_1_t2058570427 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t2058570427 *)L_2, (int32_t)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Int32 Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::get_Count()
extern "C"  int32_t PopsicleList_1_get_Count_m2842072930_gshared (PopsicleList_1_t884098166 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_items_2();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001c;
	}

IL_0011:
	{
		List_1_t2058570427 * L_1 = (List_1_t2058570427 *)__this->get_items_2();
		NullCheck((List_1_t2058570427 *)L_1);
		int32_t L_2 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t2058570427 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Boolean Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::get_IsReadOnly()
extern "C"  bool PopsicleList_1_get_IsReadOnly_m1423025171_gshared (PopsicleList_1_t884098166 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_readOnly_3();
		return L_0;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::.ctor()
extern "C"  void PopsicleList_1__ctor_m1541599032_gshared (PopsicleList_1_t884098166 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::MakeReadOnly()
extern "C"  void PopsicleList_1_MakeReadOnly_m323929666_gshared (PopsicleList_1_t884098166 * __this, const MethodInfo* method)
{
	{
		__this->set_readOnly_3((bool)1);
		return;
	}
}
// System.Int32 Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::IndexOf(T)
extern "C"  int32_t PopsicleList_1_IndexOf_m1431440187_gshared (PopsicleList_1_t884098166 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_items_2();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = (-1);
		goto IL_001d;
	}

IL_0011:
	{
		List_1_t2058570427 * L_1 = (List_1_t2058570427 *)__this->get_items_2();
		Il2CppObject * L_2 = ___item0;
		NullCheck((List_1_t2058570427 *)L_1);
		int32_t L_3 = ((  int32_t (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2058570427 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		G_B3_0 = L_3;
	}

IL_001d:
	{
		return G_B3_0;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void PopsicleList_1_Insert_m2208313842_gshared (PopsicleList_1_t884098166 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		NullCheck((PopsicleList_1_t884098166 *)__this);
		((  void (*) (PopsicleList_1_t884098166 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((PopsicleList_1_t884098166 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		bool L_0 = ((PopsicleList_1_t884098166_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)->static_fields)->get_CheckForNull_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject * L_1 = ___item1;
		ThrowHelper_ThrowIfNull_m2318750201(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_001b:
	{
		List_1_t2058570427 * L_2 = (List_1_t2058570427 *)__this->get_items_2();
		int32_t L_3 = ___index0;
		Il2CppObject * L_4 = ___item1;
		NullCheck((List_1_t2058570427 *)L_2);
		((  void (*) (List_1_t2058570427 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2058570427 *)L_2, (int32_t)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void PopsicleList_1_RemoveAt_m615757030_gshared (PopsicleList_1_t884098166 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		NullCheck((PopsicleList_1_t884098166 *)__this);
		((  void (*) (PopsicleList_1_t884098166 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((PopsicleList_1_t884098166 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_items_2();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t2058570427 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::Add(T)
extern "C"  void PopsicleList_1_Add_m3904111271_gshared (PopsicleList_1_t884098166 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		NullCheck((PopsicleList_1_t884098166 *)__this);
		((  void (*) (PopsicleList_1_t884098166 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((PopsicleList_1_t884098166 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		bool L_0 = ((PopsicleList_1_t884098166_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)->static_fields)->get_CheckForNull_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject * L_1 = ___item0;
		ThrowHelper_ThrowIfNull_m2318750201(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_001b:
	{
		List_1_t2058570427 * L_2 = (List_1_t2058570427 *)__this->get_items_2();
		Il2CppObject * L_3 = ___item0;
		NullCheck((List_1_t2058570427 *)L_2);
		((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t2058570427 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::Clear()
extern "C"  void PopsicleList_1_Clear_m1743253803_gshared (PopsicleList_1_t884098166 * __this, const MethodInfo* method)
{
	{
		NullCheck((PopsicleList_1_t884098166 *)__this);
		((  void (*) (PopsicleList_1_t884098166 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((PopsicleList_1_t884098166 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_items_2();
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t2058570427 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Boolean Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::Contains(T)
extern "C"  bool PopsicleList_1_Contains_m1407251329_gshared (PopsicleList_1_t884098166 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_items_2();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001d;
	}

IL_0011:
	{
		List_1_t2058570427 * L_1 = (List_1_t2058570427 *)__this->get_items_2();
		Il2CppObject * L_2 = ___item0;
		NullCheck((List_1_t2058570427 *)L_1);
		bool L_3 = ((  bool (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t2058570427 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		G_B3_0 = ((int32_t)(L_3));
	}

IL_001d:
	{
		return (bool)G_B3_0;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void PopsicleList_1_CopyTo_m642281667_gshared (PopsicleList_1_t884098166 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_items_2();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		List_1_t2058570427 * L_1 = (List_1_t2058570427 *)__this->get_items_2();
		ObjectU5BU5D_t3614634134* L_2 = ___array0;
		int32_t L_3 = ___arrayIndex1;
		NullCheck((List_1_t2058570427 *)L_1);
		((  void (*) (List_1_t2058570427 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t2058570427 *)L_1, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0018:
	{
		return;
	}
}
// System.Boolean Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::Remove(T)
extern "C"  bool PopsicleList_1_Remove_m1342765154_gshared (PopsicleList_1_t884098166 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		NullCheck((PopsicleList_1_t884098166 *)__this);
		((  void (*) (PopsicleList_1_t884098166 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((PopsicleList_1_t884098166 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_items_2();
		Il2CppObject * L_1 = ___item0;
		NullCheck((List_1_t2058570427 *)L_0);
		bool L_2 = ((  bool (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t2058570427 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<T> Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* PopsicleList_1_GetEnumerator_m3180968684_gshared (PopsicleList_1_t884098166 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	List_1_t2058570427 * G_B2_0 = NULL;
	List_1_t2058570427 * G_B1_0 = NULL;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_items_2();
		List_1_t2058570427 * L_1 = (List_1_t2058570427 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t3614634134* L_2 = ((PopsicleList_1_t884098166_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)->static_fields)->get_EmptySet_1();
		G_B2_0 = ((List_1_t2058570427 *)(L_2));
	}

IL_0012:
	{
		V_0 = (Il2CppObject*)G_B2_0;
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Collections.IEnumerator Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * PopsicleList_1_System_Collections_IEnumerable_GetEnumerator_m735203871_gshared (PopsicleList_1_t884098166 * __this, const MethodInfo* method)
{
	{
		NullCheck((PopsicleList_1_t884098166 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (PopsicleList_1_t884098166 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((PopsicleList_1_t884098166 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_0;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::Add(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t PopsicleList_1_Add_m4010494820_MetadataUsageId;
extern "C"  void PopsicleList_1_Add_m4010494820_gshared (PopsicleList_1_t884098166 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PopsicleList_1_Add_m4010494820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((PopsicleList_1_t884098166 *)__this);
		((  void (*) (PopsicleList_1_t884098166 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((PopsicleList_1_t884098166 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_0 = ___collection0;
		ThrowHelper_ThrowIfNull_m2318750201(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		bool L_1 = ((PopsicleList_1_t884098166_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)->static_fields)->get_CheckForNull_0();
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Il2CppObject* L_2 = ___collection0;
		if (!((PopsicleList_1_t884098166 *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16))))
		{
			goto IL_0032;
		}
	}

IL_0021:
	{
		List_1_t2058570427 * L_3 = (List_1_t2058570427 *)__this->get_items_2();
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t2058570427 *)L_3);
		((  void (*) (List_1_t2058570427 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t2058570427 *)L_3, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		goto IL_009b;
	}

IL_0032:
	{
		Il2CppObject* L_5 = ___collection0;
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18))))
		{
			goto IL_0054;
		}
	}
	{
		Il2CppObject* L_6 = ___collection0;
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		List_1_t2058570427 * L_7 = (List_1_t2058570427 *)__this->get_items_2();
		Il2CppObject* L_8 = ___collection0;
		NullCheck((List_1_t2058570427 *)L_7);
		((  void (*) (List_1_t2058570427 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t2058570427 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		goto IL_009b;
	}

IL_0054:
	{
		Il2CppObject* L_9 = ___collection0;
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject* L_10 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_9);
		V_1 = (Il2CppObject*)L_10;
	}

IL_005b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_007e;
		}

IL_0060:
		{
			Il2CppObject* L_11 = V_1;
			NullCheck((Il2CppObject*)L_11);
			Il2CppObject * L_12 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_11);
			V_0 = (Il2CppObject *)L_12;
			Il2CppObject * L_13 = V_0;
			ThrowHelper_ThrowIfNull_m2318750201(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/NULL);
			List_1_t2058570427 * L_14 = (List_1_t2058570427 *)__this->get_items_2();
			Il2CppObject * L_15 = V_0;
			NullCheck((List_1_t2058570427 *)L_14);
			((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t2058570427 *)L_14, (Il2CppObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_007e:
		{
			Il2CppObject* L_16 = V_1;
			NullCheck((Il2CppObject *)L_16);
			bool L_17 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_16);
			if (L_17)
			{
				goto IL_0060;
			}
		}

IL_0089:
		{
			IL2CPP_LEAVE(0x9B, FINALLY_008e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_008e;
	}

FINALLY_008e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_18 = V_1;
			if (!L_18)
			{
				goto IL_009a;
			}
		}

IL_0094:
		{
			Il2CppObject* L_19 = V_1;
			NullCheck((Il2CppObject *)L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
		}

IL_009a:
		{
			IL2CPP_END_FINALLY(142)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(142)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_009b:
	{
		return;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::ValidateModification()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2727261631;
extern const uint32_t PopsicleList_1_ValidateModification_m2556941404_MetadataUsageId;
extern "C"  void PopsicleList_1_ValidateModification_m2556941404_gshared (PopsicleList_1_t884098166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PopsicleList_1_ValidateModification_m2556941404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_readOnly_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_1, (String_t*)_stringLiteral2727261631, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		List_1_t2058570427 * L_2 = (List_1_t2058570427 *)__this->get_items_2();
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		List_1_t2058570427 * L_3 = (List_1_t2058570427 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21));
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		__this->set_items_2(L_3);
	}

IL_002c:
	{
		return;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::.cctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t PopsicleList_1__cctor_m3201840669_MetadataUsageId;
extern "C"  void PopsicleList_1__cctor_m3201840669_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PopsicleList_1__cctor_m3201840669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		((PopsicleList_1_t884098166_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_CheckForNull_0((bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0));
		((PopsicleList_1_t884098166_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_EmptySet_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23), (uint32_t)0)));
		return;
	}
}
// T Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern const uint32_t PopsicleList_1_get_Item_m581782398_MetadataUsageId;
extern "C"  float PopsicleList_1_get_Item_m581782398_gshared (PopsicleList_1_t271158803 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PopsicleList_1_get_Item_m581782398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_items_2();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m15523695(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		List_1_t1445631064 * L_2 = (List_1_t1445631064 *)__this->get_items_2();
		int32_t L_3 = ___index0;
		NullCheck((List_1_t1445631064 *)L_2);
		float L_4 = ((  float (*) (List_1_t1445631064 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((List_1_t1445631064 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_4;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::set_Item(System.Int32,T)
extern "C"  void PopsicleList_1_set_Item_m1619534931_gshared (PopsicleList_1_t271158803 * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	{
		NullCheck((PopsicleList_1_t271158803 *)__this);
		((  void (*) (PopsicleList_1_t271158803 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((PopsicleList_1_t271158803 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		bool L_0 = ((PopsicleList_1_t271158803_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)->static_fields)->get_CheckForNull_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		float L_1 = ___value1;
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_2);
		ThrowHelper_ThrowIfNull_m2318750201(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001b:
	{
		List_1_t1445631064 * L_4 = (List_1_t1445631064 *)__this->get_items_2();
		int32_t L_5 = ___index0;
		float L_6 = ___value1;
		NullCheck((List_1_t1445631064 *)L_4);
		((  void (*) (List_1_t1445631064 *, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t1445631064 *)L_4, (int32_t)L_5, (float)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Int32 Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::get_Count()
extern "C"  int32_t PopsicleList_1_get_Count_m259202879_gshared (PopsicleList_1_t271158803 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_items_2();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001c;
	}

IL_0011:
	{
		List_1_t1445631064 * L_1 = (List_1_t1445631064 *)__this->get_items_2();
		NullCheck((List_1_t1445631064 *)L_1);
		int32_t L_2 = ((  int32_t (*) (List_1_t1445631064 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t1445631064 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Boolean Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::get_IsReadOnly()
extern "C"  bool PopsicleList_1_get_IsReadOnly_m1569591974_gshared (PopsicleList_1_t271158803 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_readOnly_3();
		return L_0;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::.ctor()
extern "C"  void PopsicleList_1__ctor_m3355368599_gshared (PopsicleList_1_t271158803 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::MakeReadOnly()
extern "C"  void PopsicleList_1_MakeReadOnly_m1946210019_gshared (PopsicleList_1_t271158803 * __this, const MethodInfo* method)
{
	{
		__this->set_readOnly_3((bool)1);
		return;
	}
}
// System.Int32 Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::IndexOf(T)
extern "C"  int32_t PopsicleList_1_IndexOf_m2246561702_gshared (PopsicleList_1_t271158803 * __this, float ___item0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_items_2();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = (-1);
		goto IL_001d;
	}

IL_0011:
	{
		List_1_t1445631064 * L_1 = (List_1_t1445631064 *)__this->get_items_2();
		float L_2 = ___item0;
		NullCheck((List_1_t1445631064 *)L_1);
		int32_t L_3 = ((  int32_t (*) (List_1_t1445631064 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1445631064 *)L_1, (float)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		G_B3_0 = L_3;
	}

IL_001d:
	{
		return G_B3_0;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::Insert(System.Int32,T)
extern "C"  void PopsicleList_1_Insert_m1505796755_gshared (PopsicleList_1_t271158803 * __this, int32_t ___index0, float ___item1, const MethodInfo* method)
{
	{
		NullCheck((PopsicleList_1_t271158803 *)__this);
		((  void (*) (PopsicleList_1_t271158803 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((PopsicleList_1_t271158803 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		bool L_0 = ((PopsicleList_1_t271158803_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)->static_fields)->get_CheckForNull_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		float L_1 = ___item1;
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_2);
		ThrowHelper_ThrowIfNull_m2318750201(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001b:
	{
		List_1_t1445631064 * L_4 = (List_1_t1445631064 *)__this->get_items_2();
		int32_t L_5 = ___index0;
		float L_6 = ___item1;
		NullCheck((List_1_t1445631064 *)L_4);
		((  void (*) (List_1_t1445631064 *, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1445631064 *)L_4, (int32_t)L_5, (float)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::RemoveAt(System.Int32)
extern "C"  void PopsicleList_1_RemoveAt_m1557170727_gshared (PopsicleList_1_t271158803 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		NullCheck((PopsicleList_1_t271158803 *)__this);
		((  void (*) (PopsicleList_1_t271158803 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((PopsicleList_1_t271158803 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_items_2();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t1445631064 *)L_0);
		((  void (*) (List_1_t1445631064 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t1445631064 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::Add(T)
extern "C"  void PopsicleList_1_Add_m2838327051_gshared (PopsicleList_1_t271158803 * __this, float ___item0, const MethodInfo* method)
{
	{
		NullCheck((PopsicleList_1_t271158803 *)__this);
		((  void (*) (PopsicleList_1_t271158803 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((PopsicleList_1_t271158803 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		bool L_0 = ((PopsicleList_1_t271158803_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)->static_fields)->get_CheckForNull_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		float L_1 = ___item0;
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_2);
		ThrowHelper_ThrowIfNull_m2318750201(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001b:
	{
		List_1_t1445631064 * L_4 = (List_1_t1445631064 *)__this->get_items_2();
		float L_5 = ___item0;
		NullCheck((List_1_t1445631064 *)L_4);
		((  void (*) (List_1_t1445631064 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1445631064 *)L_4, (float)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::Clear()
extern "C"  void PopsicleList_1_Clear_m3420595880_gshared (PopsicleList_1_t271158803 * __this, const MethodInfo* method)
{
	{
		NullCheck((PopsicleList_1_t271158803 *)__this);
		((  void (*) (PopsicleList_1_t271158803 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((PopsicleList_1_t271158803 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_items_2();
		NullCheck((List_1_t1445631064 *)L_0);
		((  void (*) (List_1_t1445631064 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t1445631064 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Boolean Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::Contains(T)
extern "C"  bool PopsicleList_1_Contains_m161145750_gshared (PopsicleList_1_t271158803 * __this, float ___item0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_items_2();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001d;
	}

IL_0011:
	{
		List_1_t1445631064 * L_1 = (List_1_t1445631064 *)__this->get_items_2();
		float L_2 = ___item0;
		NullCheck((List_1_t1445631064 *)L_1);
		bool L_3 = ((  bool (*) (List_1_t1445631064 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1445631064 *)L_1, (float)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		G_B3_0 = ((int32_t)(L_3));
	}

IL_001d:
	{
		return (bool)G_B3_0;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::CopyTo(T[],System.Int32)
extern "C"  void PopsicleList_1_CopyTo_m3966778362_gshared (PopsicleList_1_t271158803 * __this, SingleU5BU5D_t577127397* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_items_2();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		List_1_t1445631064 * L_1 = (List_1_t1445631064 *)__this->get_items_2();
		SingleU5BU5D_t577127397* L_2 = ___array0;
		int32_t L_3 = ___arrayIndex1;
		NullCheck((List_1_t1445631064 *)L_1);
		((  void (*) (List_1_t1445631064 *, SingleU5BU5D_t577127397*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1445631064 *)L_1, (SingleU5BU5D_t577127397*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0018:
	{
		return;
	}
}
// System.Boolean Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::Remove(T)
extern "C"  bool PopsicleList_1_Remove_m4024891427_gshared (PopsicleList_1_t271158803 * __this, float ___item0, const MethodInfo* method)
{
	{
		NullCheck((PopsicleList_1_t271158803 *)__this);
		((  void (*) (PopsicleList_1_t271158803 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((PopsicleList_1_t271158803 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_items_2();
		float L_1 = ___item0;
		NullCheck((List_1_t1445631064 *)L_0);
		bool L_2 = ((  bool (*) (List_1_t1445631064 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t1445631064 *)L_0, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<T> Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::GetEnumerator()
extern "C"  Il2CppObject* PopsicleList_1_GetEnumerator_m4265894606_gshared (PopsicleList_1_t271158803 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	List_1_t1445631064 * G_B2_0 = NULL;
	List_1_t1445631064 * G_B1_0 = NULL;
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_items_2();
		List_1_t1445631064 * L_1 = (List_1_t1445631064 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		SingleU5BU5D_t577127397* L_2 = ((PopsicleList_1_t271158803_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)->static_fields)->get_EmptySet_1();
		G_B2_0 = ((List_1_t1445631064 *)(L_2));
	}

IL_0012:
	{
		V_0 = (Il2CppObject*)G_B2_0;
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Single>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Collections.IEnumerator Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * PopsicleList_1_System_Collections_IEnumerable_GetEnumerator_m205112374_gshared (PopsicleList_1_t271158803 * __this, const MethodInfo* method)
{
	{
		NullCheck((PopsicleList_1_t271158803 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (PopsicleList_1_t271158803 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((PopsicleList_1_t271158803 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_0;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::Add(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t PopsicleList_1_Add_m1782696670_MetadataUsageId;
extern "C"  void PopsicleList_1_Add_m1782696670_gshared (PopsicleList_1_t271158803 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PopsicleList_1_Add_m1782696670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((PopsicleList_1_t271158803 *)__this);
		((  void (*) (PopsicleList_1_t271158803 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((PopsicleList_1_t271158803 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_0 = ___collection0;
		ThrowHelper_ThrowIfNull_m2318750201(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		bool L_1 = ((PopsicleList_1_t271158803_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)->static_fields)->get_CheckForNull_0();
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Il2CppObject* L_2 = ___collection0;
		if (!((PopsicleList_1_t271158803 *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16))))
		{
			goto IL_0032;
		}
	}

IL_0021:
	{
		List_1_t1445631064 * L_3 = (List_1_t1445631064 *)__this->get_items_2();
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t1445631064 *)L_3);
		((  void (*) (List_1_t1445631064 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1445631064 *)L_3, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		goto IL_009b;
	}

IL_0032:
	{
		Il2CppObject* L_5 = ___collection0;
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18))))
		{
			goto IL_0054;
		}
	}
	{
		Il2CppObject* L_6 = ___collection0;
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		List_1_t1445631064 * L_7 = (List_1_t1445631064 *)__this->get_items_2();
		Il2CppObject* L_8 = ___collection0;
		NullCheck((List_1_t1445631064 *)L_7);
		((  void (*) (List_1_t1445631064 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1445631064 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		goto IL_009b;
	}

IL_0054:
	{
		Il2CppObject* L_9 = ___collection0;
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject* L_10 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Single>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_9);
		V_1 = (Il2CppObject*)L_10;
	}

IL_005b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_007e;
		}

IL_0060:
		{
			Il2CppObject* L_11 = V_1;
			NullCheck((Il2CppObject*)L_11);
			float L_12 = InterfaceFuncInvoker0< float >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_11);
			V_0 = (float)L_12;
			float L_13 = V_0;
			float L_14 = L_13;
			Il2CppObject * L_15 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_14);
			ThrowHelper_ThrowIfNull_m2318750201(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			List_1_t1445631064 * L_16 = (List_1_t1445631064 *)__this->get_items_2();
			float L_17 = V_0;
			NullCheck((List_1_t1445631064 *)L_16);
			((  void (*) (List_1_t1445631064 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1445631064 *)L_16, (float)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_007e:
		{
			Il2CppObject* L_18 = V_1;
			NullCheck((Il2CppObject *)L_18);
			bool L_19 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			if (L_19)
			{
				goto IL_0060;
			}
		}

IL_0089:
		{
			IL2CPP_LEAVE(0x9B, FINALLY_008e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_008e;
	}

FINALLY_008e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_20 = V_1;
			if (!L_20)
			{
				goto IL_009a;
			}
		}

IL_0094:
		{
			Il2CppObject* L_21 = V_1;
			NullCheck((Il2CppObject *)L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_21);
		}

IL_009a:
		{
			IL2CPP_END_FINALLY(142)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(142)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_009b:
	{
		return;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::ValidateModification()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2727261631;
extern const uint32_t PopsicleList_1_ValidateModification_m1808242029_MetadataUsageId;
extern "C"  void PopsicleList_1_ValidateModification_m1808242029_gshared (PopsicleList_1_t271158803 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PopsicleList_1_ValidateModification_m1808242029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_readOnly_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_1, (String_t*)_stringLiteral2727261631, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		List_1_t1445631064 * L_2 = (List_1_t1445631064 *)__this->get_items_2();
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		List_1_t1445631064 * L_3 = (List_1_t1445631064 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21));
		((  void (*) (List_1_t1445631064 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		__this->set_items_2(L_3);
	}

IL_002c:
	{
		return;
	}
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::.cctor()
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t PopsicleList_1__cctor_m1869678910_MetadataUsageId;
extern "C"  void PopsicleList_1__cctor_m1869678910_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PopsicleList_1__cctor_m1869678910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = V_0;
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_1);
		((PopsicleList_1_t271158803_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_CheckForNull_0((bool)((((Il2CppObject*)(Il2CppObject *)L_2) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0));
		((PopsicleList_1_t271158803_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_EmptySet_1(((SingleU5BU5D_t577127397*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23), (uint32_t)0)));
		return;
	}
}
// System.Void Google.ProtocolBuffers.EnumParser`1<proto.PhoneEvent/Types/Type>::.cctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t EnumParser_1__cctor_m3306118084_MetadataUsageId;
extern "C"  void EnumParser_1__cctor_m3306118084_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnumParser_1__cctor_m3306118084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t3030399641* V_0 = NULL;
	List_1_t899601993 * V_1 = NULL;
	FieldInfo_t * V_2 = NULL;
	FieldInfoU5BU5D_t125053523* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Int32U5BU5D_t3030399641* V_6 = NULL;
	int32_t V_7 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			List_1_t899601993 * L_0 = (List_1_t899601993 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			((  void (*) (List_1_t899601993 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			V_1 = (List_1_t899601993 *)L_0;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
			NullCheck((Type_t *)L_1);
			FieldInfoU5BU5D_t125053523* L_2 = VirtFuncInvoker1< FieldInfoU5BU5D_t125053523*, int32_t >::Invoke(47 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, (Type_t *)L_1, (int32_t)((int32_t)24));
			V_3 = (FieldInfoU5BU5D_t125053523*)L_2;
			V_4 = (int32_t)0;
			goto IL_005d;
		}

IL_0020:
		{
			FieldInfoU5BU5D_t125053523* L_3 = V_3;
			int32_t L_4 = V_4;
			NullCheck(L_3);
			int32_t L_5 = L_4;
			FieldInfo_t * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
			V_2 = (FieldInfo_t *)L_6;
			FieldInfo_t * L_7 = V_2;
			NullCheck((FieldInfo_t *)L_7);
			bool L_8 = FieldInfo_get_IsLiteral_m267898096((FieldInfo_t *)L_7, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_0057;
			}
		}

IL_0030:
		{
			FieldInfo_t * L_9 = V_2;
			NullCheck((FieldInfo_t *)L_9);
			Type_t * L_10 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, (FieldInfo_t *)L_9);
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_11 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
			if ((!(((Il2CppObject*)(Type_t *)L_10) == ((Il2CppObject*)(Type_t *)L_11))))
			{
				goto IL_0057;
			}
		}

IL_0045:
		{
			List_1_t899601993 * L_12 = V_1;
			FieldInfo_t * L_13 = V_2;
			NullCheck((FieldInfo_t *)L_13);
			Il2CppObject * L_14 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, (FieldInfo_t *)L_13, (Il2CppObject *)NULL);
			NullCheck((List_1_t899601993 *)L_12);
			((  void (*) (List_1_t899601993 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t899601993 *)L_12, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_14, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		}

IL_0057:
		{
			int32_t L_15 = V_4;
			V_4 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
		}

IL_005d:
		{
			int32_t L_16 = V_4;
			FieldInfoU5BU5D_t125053523* L_17 = V_3;
			NullCheck(L_17);
			if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
			{
				goto IL_0020;
			}
		}

IL_0067:
		{
			List_1_t899601993 * L_18 = V_1;
			NullCheck((List_1_t899601993 *)L_18);
			TypeU5BU5D_t2649698064* L_19 = ((  TypeU5BU5D_t2649698064* (*) (List_1_t899601993 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t899601993 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)Castclass(L_19, Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var));
			goto IL_0084;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0078;
		throw e;
	}

CATCH_0078:
	{ // begin catch(System.Object)
		((EnumParser_1_t1339677487_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->static_fields)->set__byNumber_0((Dictionary_2_t538306496 *)NULL);
		goto IL_00cc;
	} // end catch (depth: 1)

IL_0084:
	{
		Int32U5BU5D_t3030399641* L_20 = V_0;
		NullCheck(L_20);
		Dictionary_2_t538306496 * L_21 = (Dictionary_2_t538306496 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (Dictionary_2_t538306496 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)(L_21, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((EnumParser_1_t1339677487_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->static_fields)->set__byNumber_0(L_21);
		Int32U5BU5D_t3030399641* L_22 = V_0;
		V_6 = (Int32U5BU5D_t3030399641*)L_22;
		V_7 = (int32_t)0;
		goto IL_00c1;
	}

IL_009c:
	{
		Int32U5BU5D_t3030399641* L_23 = V_6;
		int32_t L_24 = V_7;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		int32_t L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		V_5 = (int32_t)L_26;
		Dictionary_2_t538306496 * L_27 = ((EnumParser_1_t1339677487_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->static_fields)->get__byNumber_0();
		int32_t L_28 = V_5;
		int32_t L_29 = V_5;
		int32_t L_30 = L_29;
		Il2CppObject * L_31 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_30);
		NullCheck((Dictionary_2_t538306496 *)L_27);
		((  void (*) (Dictionary_2_t538306496 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)((Dictionary_2_t538306496 *)L_27, (int32_t)L_28, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_31, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_32 = V_7;
		V_7 = (int32_t)((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00c1:
	{
		int32_t L_33 = V_7;
		Int32U5BU5D_t3030399641* L_34 = V_6;
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length)))))))
		{
			goto IL_009c;
		}
	}

IL_00cc:
	{
		return;
	}
}
// System.Boolean Google.ProtocolBuffers.EnumParser`1<proto.PhoneEvent/Types/Type>::TryConvert(System.Int32,T&)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern const uint32_t EnumParser_1_TryConvert_m256499718_MetadataUsageId;
extern "C"  bool EnumParser_1_TryConvert_m256499718_gshared (Il2CppObject * __this /* static, unused */, int32_t ___number0, int32_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnumParser_1_TryConvert_m256499718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Dictionary_2_t538306496 * L_0 = ((EnumParser_1_t1339677487_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->static_fields)->get__byNumber_0();
		if (L_0)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		int32_t L_2 = ___number0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		bool L_5 = Enum_IsDefined_m92789062(NULL /*static, unused*/, (Type_t *)L_1, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Dictionary_2_t538306496 * L_6 = ((EnumParser_1_t1339677487_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->static_fields)->get__byNumber_0();
		if (!L_6)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Dictionary_2_t538306496 * L_7 = ((EnumParser_1_t1339677487_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->static_fields)->get__byNumber_0();
		int32_t L_8 = ___number0;
		NullCheck((Dictionary_2_t538306496 *)L_7);
		bool L_9 = ((  bool (*) (Dictionary_2_t538306496 *, int32_t, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->methodPointer)((Dictionary_2_t538306496 *)L_7, (int32_t)L_8, (int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		if (!L_9)
		{
			goto IL_0045;
		}
	}
	{
		int32_t* L_10 = ___value1;
		int32_t L_11 = V_0;
		(*(int32_t*)L_10) = L_11;
		return (bool)1;
	}

IL_0045:
	{
		return (bool)0;
	}
}
// System.Void Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>::.ctor()
extern "C"  void GeneratedBuilderLite_2__ctor_m1371342059_gshared (GeneratedBuilderLite_2_t464188747 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		((  void (*) (AbstractBuilderLite_2_t466644448 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((AbstractBuilderLite_2_t466644448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// TBuilder Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.IMessageLite)
extern "C"  Il2CppObject * GeneratedBuilderLite_2_MergeFrom_m2954085229_gshared (GeneratedBuilderLite_2_t464188747 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* TBuilder Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::get_ThisBuilder() */, (AbstractBuilderLite_2_t466644448 *)__this);
		return L_0;
	}
}
// System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String)
extern Il2CppClass* ICodedInputStream_t1485221380_il2cpp_TypeInfo_var;
extern const uint32_t GeneratedBuilderLite_2_ParseUnknownField_m1674842256_MetadataUsageId;
extern "C"  bool GeneratedBuilderLite_2_ParseUnknownField_m1674842256_gshared (GeneratedBuilderLite_2_t464188747 * __this, Il2CppObject * ___input0, ExtensionRegistry_t1533220584 * ___extensionRegistry1, uint32_t ___tag2, String_t* ___fieldName3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GeneratedBuilderLite_2_ParseUnknownField_m1674842256_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		NullCheck((Il2CppObject *)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(7 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::SkipField() */, ICodedInputStream_t1485221380_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// TMessage Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>::BuildParsed()
extern Il2CppClass* UninitializedMessageException_t1889871139_il2cpp_TypeInfo_var;
extern const uint32_t GeneratedBuilderLite_2_BuildParsed_m4284166938_MetadataUsageId;
extern "C"  Il2CppObject * GeneratedBuilderLite_2_BuildParsed_m4284166938_gshared (GeneratedBuilderLite_2_t464188747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GeneratedBuilderLite_2_BuildParsed_m4284166938_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::get_IsInitialized() */, (AbstractBuilderLite_2_t466644448 *)__this);
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((GeneratedBuilderLite_2_t464188747 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(25 /* TMessage Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>::get_MessageBeingBuilt() */, (GeneratedBuilderLite_2_t464188747 *)__this);
		UninitializedMessageException_t1889871139 * L_2 = (UninitializedMessageException_t1889871139 *)il2cpp_codegen_object_new(UninitializedMessageException_t1889871139_il2cpp_TypeInfo_var);
		UninitializedMessageException__ctor_m1011334392(L_2, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((UninitializedMessageException_t1889871139 *)L_2);
		InvalidProtocolBufferException_t37028716 * L_3 = UninitializedMessageException_AsInvalidProtocolBufferException_m3724436475((UninitializedMessageException_t1889871139 *)L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0021:
	{
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_4 = VirtFuncInvoker0< Il2CppObject * >::Invoke(13 /* TMessage Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::BuildPartial() */, (AbstractBuilderLite_2_t466644448 *)__this);
		return L_4;
	}
}
// TMessage Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>::Build()
extern Il2CppClass* UninitializedMessageException_t1889871139_il2cpp_TypeInfo_var;
extern const uint32_t GeneratedBuilderLite_2_Build_m3973309793_MetadataUsageId;
extern "C"  Il2CppObject * GeneratedBuilderLite_2_Build_m3973309793_gshared (GeneratedBuilderLite_2_t464188747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GeneratedBuilderLite_2_Build_m3973309793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::get_IsInitialized() */, (AbstractBuilderLite_2_t466644448 *)__this);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		NullCheck((GeneratedBuilderLite_2_t464188747 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(25 /* TMessage Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>::get_MessageBeingBuilt() */, (GeneratedBuilderLite_2_t464188747 *)__this);
		UninitializedMessageException_t1889871139 * L_2 = (UninitializedMessageException_t1889871139 *)il2cpp_codegen_object_new(UninitializedMessageException_t1889871139_il2cpp_TypeInfo_var);
		UninitializedMessageException__ctor_m1011334392(L_2, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		NullCheck((AbstractBuilderLite_2_t466644448 *)__this);
		Il2CppObject * L_3 = VirtFuncInvoker0< Il2CppObject * >::Invoke(13 /* TMessage Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::BuildPartial() */, (AbstractBuilderLite_2_t466644448 *)__this);
		return L_3;
	}
}
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<System.Object,System.Object>::.ctor()
extern "C"  void GeneratedMessageLite_2__ctor_m4074645273_gshared (GeneratedMessageLite_2_t1832135119 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractMessageLite_2_t2741522770 *)__this);
		((  void (*) (AbstractMessageLite_2_t2741522770 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((AbstractMessageLite_2_t2741522770 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.String Google.ProtocolBuffers.GeneratedMessageLite`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringWriter_t4139609088_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t GeneratedMessageLite_2_ToString_m2406408764_MetadataUsageId;
extern "C"  String_t* GeneratedMessageLite_2_ToString_m2406408764_gshared (GeneratedMessageLite_2_t1832135119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GeneratedMessageLite_2_ToString_m2406408764_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringWriter_t4139609088 * V_0 = NULL;
	String_t* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringWriter_t4139609088 * L_0 = (StringWriter_t4139609088 *)il2cpp_codegen_object_new(StringWriter_t4139609088_il2cpp_TypeInfo_var);
		StringWriter__ctor_m59456937(L_0, /*hidden argument*/NULL);
		V_0 = (StringWriter_t4139609088 *)L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		StringWriter_t4139609088 * L_1 = V_0;
		NullCheck((AbstractMessageLite_2_t2741522770 *)__this);
		VirtActionInvoker1< TextWriter_t4027217640 * >::Invoke(15 /* System.Void Google.ProtocolBuffers.AbstractMessageLite`2<System.Object,System.Object>::PrintTo(System.IO.TextWriter) */, (AbstractMessageLite_2_t2741522770 *)__this, (TextWriter_t4027217640 *)L_1);
		StringWriter_t4139609088 * L_2 = V_0;
		NullCheck((Il2CppObject *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_2);
		V_1 = (String_t*)L_3;
		IL2CPP_LEAVE(0x26, FINALLY_0019);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0019;
	}

FINALLY_0019:
	{ // begin finally (depth: 1)
		{
			StringWriter_t4139609088 * L_4 = V_0;
			if (!L_4)
			{
				goto IL_0025;
			}
		}

IL_001f:
		{
			StringWriter_t4139609088 * L_5 = V_0;
			NullCheck((Il2CppObject *)L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
		}

IL_0025:
		{
			IL2CPP_END_FINALLY(25)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(25)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		String_t* L_6 = V_1;
		return L_6;
	}
}
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<System.Object,System.Object>::PrintField(System.String,System.Boolean,System.Object,System.IO.TextWriter)
extern Il2CppClass* IMessageLite_t1402851992_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteString_t3153909979_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumLite_t3753411088_il2cpp_TypeInfo_var;
extern Il2CppClass* IConvertible_t908092482_il2cpp_TypeInfo_var;
extern Il2CppClass* FrameworkPortability_t3280999091_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral543009662;
extern Il2CppCodeGenString* _stringLiteral372029393;
extern Il2CppCodeGenString* _stringLiteral1786362692;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern Il2CppCodeGenString* _stringLiteral3632214879;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern const uint32_t GeneratedMessageLite_2_PrintField_m3763088064_MetadataUsageId;
extern "C"  void GeneratedMessageLite_2_PrintField_m3763088064_gshared (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___hasValue1, Il2CppObject * ___value2, TextWriter_t4027217640 * ___writer3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GeneratedMessageLite_2_PrintField_m3763088064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B13_0 = NULL;
	String_t* G_B13_1 = NULL;
	TextWriter_t4027217640 * G_B13_2 = NULL;
	String_t* G_B12_0 = NULL;
	String_t* G_B12_1 = NULL;
	TextWriter_t4027217640 * G_B12_2 = NULL;
	String_t* G_B14_0 = NULL;
	String_t* G_B14_1 = NULL;
	String_t* G_B14_2 = NULL;
	TextWriter_t4027217640 * G_B14_3 = NULL;
	{
		bool L_0 = ___hasValue1;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		Il2CppObject * L_1 = ___value2;
		if (!((Il2CppObject *)IsInst(L_1, IMessageLite_t1402851992_il2cpp_TypeInfo_var)))
		{
			goto IL_003a;
		}
	}
	{
		TextWriter_t4027217640 * L_2 = ___writer3;
		String_t* L_3 = ___name0;
		NullCheck((TextWriter_t4027217640 *)L_2);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(17 /* System.Void System.IO.TextWriter::WriteLine(System.String,System.Object) */, (TextWriter_t4027217640 *)L_2, (String_t*)_stringLiteral543009662, (Il2CppObject *)L_3);
		Il2CppObject * L_4 = ___value2;
		TextWriter_t4027217640 * L_5 = ___writer3;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_4, IMessageLite_t1402851992_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker1< TextWriter_t4027217640 * >::Invoke(2 /* System.Void Google.ProtocolBuffers.IMessageLite::PrintTo(System.IO.TextWriter) */, IMessageLite_t1402851992_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_4, IMessageLite_t1402851992_il2cpp_TypeInfo_var)), (TextWriter_t4027217640 *)L_5);
		TextWriter_t4027217640 * L_6 = ___writer3;
		NullCheck((TextWriter_t4027217640 *)L_6);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::WriteLine(System.String) */, (TextWriter_t4027217640 *)L_6, (String_t*)_stringLiteral372029393);
		goto IL_0117;
	}

IL_003a:
	{
		Il2CppObject * L_7 = ___value2;
		if (((ByteString_t3153909979 *)IsInst(L_7, ByteString_t3153909979_il2cpp_TypeInfo_var)))
		{
			goto IL_0050;
		}
	}
	{
		Il2CppObject * L_8 = ___value2;
		if (!((String_t*)IsInst(L_8, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_009e;
		}
	}

IL_0050:
	{
		TextWriter_t4027217640 * L_9 = ___writer3;
		String_t* L_10 = ___name0;
		NullCheck((TextWriter_t4027217640 *)L_9);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(12 /* System.Void System.IO.TextWriter::Write(System.String,System.Object) */, (TextWriter_t4027217640 *)L_9, (String_t*)_stringLiteral1786362692, (Il2CppObject *)L_10);
		Il2CppObject * L_11 = ___value2;
		if (!((String_t*)IsInst(L_11, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0082;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_12 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_13 = ___value2;
		NullCheck((Encoding_t663144255 *)L_12);
		ByteU5BU5D_t3397334013* L_14 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, (Encoding_t663144255 *)L_12, (String_t*)((String_t*)Castclass(L_13, String_t_il2cpp_TypeInfo_var)));
		TextWriter_t4027217640 * L_15 = ___writer3;
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, TextWriter_t4027217640 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_14, (TextWriter_t4027217640 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_008e;
	}

IL_0082:
	{
		Il2CppObject * L_16 = ___value2;
		TextWriter_t4027217640 * L_17 = ___writer3;
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, TextWriter_t4027217640 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)((ByteString_t3153909979 *)Castclass(L_16, ByteString_t3153909979_il2cpp_TypeInfo_var)), (TextWriter_t4027217640 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
	}

IL_008e:
	{
		TextWriter_t4027217640 * L_18 = ___writer3;
		NullCheck((TextWriter_t4027217640 *)L_18);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::WriteLine(System.String) */, (TextWriter_t4027217640 *)L_18, (String_t*)_stringLiteral372029312);
		goto IL_0117;
	}

IL_009e:
	{
		Il2CppObject * L_19 = ___value2;
		if (!((Il2CppObject *)IsInst(L_19, Boolean_t3825574718_il2cpp_TypeInfo_var)))
		{
			goto IL_00d4;
		}
	}
	{
		TextWriter_t4027217640 * L_20 = ___writer3;
		String_t* L_21 = ___name0;
		Il2CppObject * L_22 = ___value2;
		G_B12_0 = L_21;
		G_B12_1 = _stringLiteral3632214879;
		G_B12_2 = L_20;
		if (!((*(bool*)((bool*)UnBox (L_22, Boolean_t3825574718_il2cpp_TypeInfo_var)))))
		{
			G_B13_0 = L_21;
			G_B13_1 = _stringLiteral3632214879;
			G_B13_2 = L_20;
			goto IL_00c5;
		}
	}
	{
		G_B14_0 = _stringLiteral3323263070;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		goto IL_00ca;
	}

IL_00c5:
	{
		G_B14_0 = _stringLiteral2609877245;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
	}

IL_00ca:
	{
		NullCheck((TextWriter_t4027217640 *)G_B14_3);
		VirtActionInvoker3< String_t*, Il2CppObject *, Il2CppObject * >::Invoke(18 /* System.Void System.IO.TextWriter::WriteLine(System.String,System.Object,System.Object) */, (TextWriter_t4027217640 *)G_B14_3, (String_t*)G_B14_2, (Il2CppObject *)G_B14_1, (Il2CppObject *)G_B14_0);
		goto IL_0117;
	}

IL_00d4:
	{
		Il2CppObject * L_23 = ___value2;
		if (!((Il2CppObject *)IsInst(L_23, IEnumLite_t3753411088_il2cpp_TypeInfo_var)))
		{
			goto IL_00fb;
		}
	}
	{
		TextWriter_t4027217640 * L_24 = ___writer3;
		String_t* L_25 = ___name0;
		Il2CppObject * L_26 = ___value2;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_26, IEnumLite_t3753411088_il2cpp_TypeInfo_var)));
		String_t* L_27 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Google.ProtocolBuffers.IEnumLite::get_Name() */, IEnumLite_t3753411088_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_26, IEnumLite_t3753411088_il2cpp_TypeInfo_var)));
		NullCheck((TextWriter_t4027217640 *)L_24);
		VirtActionInvoker3< String_t*, Il2CppObject *, Il2CppObject * >::Invoke(18 /* System.Void System.IO.TextWriter::WriteLine(System.String,System.Object,System.Object) */, (TextWriter_t4027217640 *)L_24, (String_t*)_stringLiteral3632214879, (Il2CppObject *)L_25, (Il2CppObject *)L_27);
		goto IL_0117;
	}

IL_00fb:
	{
		TextWriter_t4027217640 * L_28 = ___writer3;
		String_t* L_29 = ___name0;
		Il2CppObject * L_30 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(FrameworkPortability_t3280999091_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_31 = FrameworkPortability_get_InvariantCulture_m3542240210(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_30, IConvertible_t908092482_il2cpp_TypeInfo_var)));
		String_t* L_32 = InterfaceFuncInvoker1< String_t*, Il2CppObject * >::Invoke(11 /* System.String System.IConvertible::ToString(System.IFormatProvider) */, IConvertible_t908092482_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_30, IConvertible_t908092482_il2cpp_TypeInfo_var)), (Il2CppObject *)L_31);
		NullCheck((TextWriter_t4027217640 *)L_28);
		VirtActionInvoker3< String_t*, Il2CppObject *, Il2CppObject * >::Invoke(18 /* System.Void System.IO.TextWriter::WriteLine(System.String,System.Object,System.Object) */, (TextWriter_t4027217640 *)L_28, (String_t*)_stringLiteral3632214879, (Il2CppObject *)L_29, (Il2CppObject *)L_32);
	}

IL_0117:
	{
		return;
	}
}
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<System.Object,System.Object>::EscapeBytes(System.Collections.Generic.IEnumerable`1<System.Byte>,System.IO.TextWriter)
extern Il2CppClass* IEnumerable_1_t3975231481_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t1158628263_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral690346061;
extern Il2CppCodeGenString* _stringLiteral1093630588;
extern Il2CppCodeGenString* _stringLiteral3419229416;
extern Il2CppCodeGenString* _stringLiteral3062999056;
extern Il2CppCodeGenString* _stringLiteral381169868;
extern Il2CppCodeGenString* _stringLiteral3869568110;
extern Il2CppCodeGenString* _stringLiteral2706768696;
extern Il2CppCodeGenString* _stringLiteral2088416310;
extern Il2CppCodeGenString* _stringLiteral408021059;
extern Il2CppCodeGenString* _stringLiteral3943473468;
extern const uint32_t GeneratedMessageLite_2_EscapeBytes_m439876676_MetadataUsageId;
extern "C"  void GeneratedMessageLite_2_EscapeBytes_m439876676_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___input0, TextWriter_t4027217640 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GeneratedMessageLite_2_EscapeBytes_m439876676_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t V_0 = 0x0;
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___input0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Byte>::GetEnumerator() */, IEnumerable_1_t3975231481_il2cpp_TypeInfo_var, (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_014c;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			uint8_t L_3 = InterfaceFuncInvoker0< uint8_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Byte>::get_Current() */, IEnumerator_1_t1158628263_il2cpp_TypeInfo_var, (Il2CppObject*)L_2);
			V_0 = (uint8_t)L_3;
			uint8_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)7)) == 0)
			{
				goto IL_0054;
			}
			if (((int32_t)((int32_t)L_4-(int32_t)7)) == 1)
			{
				goto IL_0064;
			}
			if (((int32_t)((int32_t)L_4-(int32_t)7)) == 2)
			{
				goto IL_00a4;
			}
			if (((int32_t)((int32_t)L_4-(int32_t)7)) == 3)
			{
				goto IL_0084;
			}
			if (((int32_t)((int32_t)L_4-(int32_t)7)) == 4)
			{
				goto IL_00b4;
			}
			if (((int32_t)((int32_t)L_4-(int32_t)7)) == 5)
			{
				goto IL_0074;
			}
			if (((int32_t)((int32_t)L_4-(int32_t)7)) == 6)
			{
				goto IL_0094;
			}
		}

IL_0037:
		{
			uint8_t L_5 = V_0;
			if ((((int32_t)L_5) == ((int32_t)((int32_t)34))))
			{
				goto IL_00e4;
			}
		}

IL_003f:
		{
			uint8_t L_6 = V_0;
			if ((((int32_t)L_6) == ((int32_t)((int32_t)39))))
			{
				goto IL_00d4;
			}
		}

IL_0047:
		{
			uint8_t L_7 = V_0;
			if ((((int32_t)L_7) == ((int32_t)((int32_t)92))))
			{
				goto IL_00c4;
			}
		}

IL_004f:
		{
			goto IL_00f4;
		}

IL_0054:
		{
			TextWriter_t4027217640 * L_8 = ___writer1;
			NullCheck((TextWriter_t4027217640 *)L_8);
			VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, (TextWriter_t4027217640 *)L_8, (String_t*)_stringLiteral690346061);
			goto IL_014c;
		}

IL_0064:
		{
			TextWriter_t4027217640 * L_9 = ___writer1;
			NullCheck((TextWriter_t4027217640 *)L_9);
			VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, (TextWriter_t4027217640 *)L_9, (String_t*)_stringLiteral1093630588);
			goto IL_014c;
		}

IL_0074:
		{
			TextWriter_t4027217640 * L_10 = ___writer1;
			NullCheck((TextWriter_t4027217640 *)L_10);
			VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, (TextWriter_t4027217640 *)L_10, (String_t*)_stringLiteral3419229416);
			goto IL_014c;
		}

IL_0084:
		{
			TextWriter_t4027217640 * L_11 = ___writer1;
			NullCheck((TextWriter_t4027217640 *)L_11);
			VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, (TextWriter_t4027217640 *)L_11, (String_t*)_stringLiteral3062999056);
			goto IL_014c;
		}

IL_0094:
		{
			TextWriter_t4027217640 * L_12 = ___writer1;
			NullCheck((TextWriter_t4027217640 *)L_12);
			VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, (TextWriter_t4027217640 *)L_12, (String_t*)_stringLiteral381169868);
			goto IL_014c;
		}

IL_00a4:
		{
			TextWriter_t4027217640 * L_13 = ___writer1;
			NullCheck((TextWriter_t4027217640 *)L_13);
			VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, (TextWriter_t4027217640 *)L_13, (String_t*)_stringLiteral3869568110);
			goto IL_014c;
		}

IL_00b4:
		{
			TextWriter_t4027217640 * L_14 = ___writer1;
			NullCheck((TextWriter_t4027217640 *)L_14);
			VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, (TextWriter_t4027217640 *)L_14, (String_t*)_stringLiteral2706768696);
			goto IL_014c;
		}

IL_00c4:
		{
			TextWriter_t4027217640 * L_15 = ___writer1;
			NullCheck((TextWriter_t4027217640 *)L_15);
			VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, (TextWriter_t4027217640 *)L_15, (String_t*)_stringLiteral2088416310);
			goto IL_014c;
		}

IL_00d4:
		{
			TextWriter_t4027217640 * L_16 = ___writer1;
			NullCheck((TextWriter_t4027217640 *)L_16);
			VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, (TextWriter_t4027217640 *)L_16, (String_t*)_stringLiteral408021059);
			goto IL_014c;
		}

IL_00e4:
		{
			TextWriter_t4027217640 * L_17 = ___writer1;
			NullCheck((TextWriter_t4027217640 *)L_17);
			VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, (TextWriter_t4027217640 *)L_17, (String_t*)_stringLiteral3943473468);
			goto IL_014c;
		}

IL_00f4:
		{
			uint8_t L_18 = V_0;
			if ((((int32_t)L_18) < ((int32_t)((int32_t)32))))
			{
				goto IL_0114;
			}
		}

IL_00fc:
		{
			uint8_t L_19 = V_0;
			if ((((int32_t)L_19) >= ((int32_t)((int32_t)128))))
			{
				goto IL_0114;
			}
		}

IL_0107:
		{
			TextWriter_t4027217640 * L_20 = ___writer1;
			uint8_t L_21 = V_0;
			NullCheck((TextWriter_t4027217640 *)L_20);
			VirtActionInvoker1< Il2CppChar >::Invoke(9 /* System.Void System.IO.TextWriter::Write(System.Char) */, (TextWriter_t4027217640 *)L_20, (Il2CppChar)(((int32_t)((uint16_t)L_21))));
			goto IL_0147;
		}

IL_0114:
		{
			TextWriter_t4027217640 * L_22 = ___writer1;
			NullCheck((TextWriter_t4027217640 *)L_22);
			VirtActionInvoker1< Il2CppChar >::Invoke(9 /* System.Void System.IO.TextWriter::Write(System.Char) */, (TextWriter_t4027217640 *)L_22, (Il2CppChar)((int32_t)92));
			TextWriter_t4027217640 * L_23 = ___writer1;
			uint8_t L_24 = V_0;
			NullCheck((TextWriter_t4027217640 *)L_23);
			VirtActionInvoker1< Il2CppChar >::Invoke(9 /* System.Void System.IO.TextWriter::Write(System.Char) */, (TextWriter_t4027217640 *)L_23, (Il2CppChar)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)48)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_24>>(int32_t)6))&(int32_t)3))))))));
			TextWriter_t4027217640 * L_25 = ___writer1;
			uint8_t L_26 = V_0;
			NullCheck((TextWriter_t4027217640 *)L_25);
			VirtActionInvoker1< Il2CppChar >::Invoke(9 /* System.Void System.IO.TextWriter::Write(System.Char) */, (TextWriter_t4027217640 *)L_25, (Il2CppChar)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)48)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_26>>(int32_t)3))&(int32_t)7))))))));
			TextWriter_t4027217640 * L_27 = ___writer1;
			uint8_t L_28 = V_0;
			NullCheck((TextWriter_t4027217640 *)L_27);
			VirtActionInvoker1< Il2CppChar >::Invoke(9 /* System.Void System.IO.TextWriter::Write(System.Char) */, (TextWriter_t4027217640 *)L_27, (Il2CppChar)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)48)+(int32_t)((int32_t)((int32_t)L_28&(int32_t)7))))))));
		}

IL_0147:
		{
			goto IL_014c;
		}

IL_014c:
		{
			Il2CppObject* L_29 = V_1;
			NullCheck((Il2CppObject *)L_29);
			bool L_30 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_29);
			if (L_30)
			{
				goto IL_000c;
			}
		}

IL_0157:
		{
			IL2CPP_LEAVE(0x169, FINALLY_015c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_015c;
	}

FINALLY_015c:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_31 = V_1;
			if (!L_31)
			{
				goto IL_0168;
			}
		}

IL_0162:
		{
			Il2CppObject* L_32 = V_1;
			NullCheck((Il2CppObject *)L_32);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_32);
		}

IL_0168:
		{
			IL2CPP_END_FINALLY(348)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(348)
	{
		IL2CPP_JUMP_TBL(0x169, IL_0169)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0169:
	{
		return;
	}
}
// System.Void SteamVR_Events/Action`1<System.Boolean>::.ctor(SteamVR_Events/Event`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Action_1__ctor_m3254789872_gshared (Action_1_t988986007 * __this, Event_1_t1706029839 * ____event0, UnityAction_1_t897193173 * ___action1, const MethodInfo* method)
{
	{
		NullCheck((Action_t1836998693 *)__this);
		Action__ctor_m1964921010((Action_t1836998693 *)__this, /*hidden argument*/NULL);
		Event_1_t1706029839 * L_0 = ____event0;
		__this->set__event_0(L_0);
		UnityAction_1_t897193173 * L_1 = ___action1;
		__this->set_action_1(L_1);
		return;
	}
}
// System.Void SteamVR_Events/Action`1<System.Boolean>::Enable(System.Boolean)
extern "C"  void Action_1_Enable_m2891429450_gshared (Action_1_t988986007 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled0;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Event_1_t1706029839 * L_1 = (Event_1_t1706029839 *)__this->get__event_0();
		UnityAction_1_t897193173 * L_2 = (UnityAction_1_t897193173 *)__this->get_action_1();
		NullCheck((Event_1_t1706029839 *)L_1);
		((  void (*) (Event_1_t1706029839 *, UnityAction_1_t897193173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Event_1_t1706029839 *)L_1, (UnityAction_1_t897193173 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		goto IL_002d;
	}

IL_001c:
	{
		Event_1_t1706029839 * L_3 = (Event_1_t1706029839 *)__this->get__event_0();
		UnityAction_1_t897193173 * L_4 = (UnityAction_1_t897193173 *)__this->get_action_1();
		NullCheck((Event_1_t1706029839 *)L_3);
		((  void (*) (Event_1_t1706029839 *, UnityAction_1_t897193173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Event_1_t1706029839 *)L_3, (UnityAction_1_t897193173 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_002d:
	{
		return;
	}
}
// System.Void SteamVR_Events/Action`1<System.Object>::.ctor(SteamVR_Events/Event`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Action_1__ctor_m3060490555_gshared (Action_1_t4147827880 * __this, Event_1_t569904416 * ____event0, UnityAction_1_t4056035046 * ___action1, const MethodInfo* method)
{
	{
		NullCheck((Action_t1836998693 *)__this);
		Action__ctor_m1964921010((Action_t1836998693 *)__this, /*hidden argument*/NULL);
		Event_1_t569904416 * L_0 = ____event0;
		__this->set__event_0(L_0);
		UnityAction_1_t4056035046 * L_1 = ___action1;
		__this->set_action_1(L_1);
		return;
	}
}
// System.Void SteamVR_Events/Action`1<System.Object>::Enable(System.Boolean)
extern "C"  void Action_1_Enable_m3823038047_gshared (Action_1_t4147827880 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled0;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Event_1_t569904416 * L_1 = (Event_1_t569904416 *)__this->get__event_0();
		UnityAction_1_t4056035046 * L_2 = (UnityAction_1_t4056035046 *)__this->get_action_1();
		NullCheck((Event_1_t569904416 *)L_1);
		((  void (*) (Event_1_t569904416 *, UnityAction_1_t4056035046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Event_1_t569904416 *)L_1, (UnityAction_1_t4056035046 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		goto IL_002d;
	}

IL_001c:
	{
		Event_1_t569904416 * L_3 = (Event_1_t569904416 *)__this->get__event_0();
		UnityAction_1_t4056035046 * L_4 = (UnityAction_1_t4056035046 *)__this->get_action_1();
		NullCheck((Event_1_t569904416 *)L_3);
		((  void (*) (Event_1_t569904416 *, UnityAction_1_t4056035046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Event_1_t569904416 *)L_3, (UnityAction_1_t4056035046 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_002d:
	{
		return;
	}
}
// System.Void SteamVR_Events/Action`1<System.Single>::.ctor(SteamVR_Events/Event`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Action_1__ctor_m2638676342_gshared (Action_1_t3534888517 * __this, Event_1_t4251932349 * ____event0, UnityAction_1_t3443095683 * ___action1, const MethodInfo* method)
{
	{
		NullCheck((Action_t1836998693 *)__this);
		Action__ctor_m1964921010((Action_t1836998693 *)__this, /*hidden argument*/NULL);
		Event_1_t4251932349 * L_0 = ____event0;
		__this->set__event_0(L_0);
		UnityAction_1_t3443095683 * L_1 = ___action1;
		__this->set_action_1(L_1);
		return;
	}
}
// System.Void SteamVR_Events/Action`1<System.Single>::Enable(System.Boolean)
extern "C"  void Action_1_Enable_m3309652672_gshared (Action_1_t3534888517 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled0;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Event_1_t4251932349 * L_1 = (Event_1_t4251932349 *)__this->get__event_0();
		UnityAction_1_t3443095683 * L_2 = (UnityAction_1_t3443095683 *)__this->get_action_1();
		NullCheck((Event_1_t4251932349 *)L_1);
		((  void (*) (Event_1_t4251932349 *, UnityAction_1_t3443095683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Event_1_t4251932349 *)L_1, (UnityAction_1_t3443095683 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		goto IL_002d;
	}

IL_001c:
	{
		Event_1_t4251932349 * L_3 = (Event_1_t4251932349 *)__this->get__event_0();
		UnityAction_1_t3443095683 * L_4 = (UnityAction_1_t3443095683 *)__this->get_action_1();
		NullCheck((Event_1_t4251932349 *)L_3);
		((  void (*) (Event_1_t4251932349 *, UnityAction_1_t3443095683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Event_1_t4251932349 *)L_3, (UnityAction_1_t3443095683 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_002d:
	{
		return;
	}
}
// System.Void SteamVR_Events/Action`1<Valve.VR.VREvent_t>::.ctor(SteamVR_Events/Event`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Action_1__ctor_m2897621874_gshared (Action_1_t568677678 * __this, Event_1_t1285721510 * ____event0, UnityAction_1_t476884844 * ___action1, const MethodInfo* method)
{
	{
		NullCheck((Action_t1836998693 *)__this);
		Action__ctor_m1964921010((Action_t1836998693 *)__this, /*hidden argument*/NULL);
		Event_1_t1285721510 * L_0 = ____event0;
		__this->set__event_0(L_0);
		UnityAction_1_t476884844 * L_1 = ___action1;
		__this->set_action_1(L_1);
		return;
	}
}
// System.Void SteamVR_Events/Action`1<Valve.VR.VREvent_t>::Enable(System.Boolean)
extern "C"  void Action_1_Enable_m1016492056_gshared (Action_1_t568677678 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled0;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Event_1_t1285721510 * L_1 = (Event_1_t1285721510 *)__this->get__event_0();
		UnityAction_1_t476884844 * L_2 = (UnityAction_1_t476884844 *)__this->get_action_1();
		NullCheck((Event_1_t1285721510 *)L_1);
		((  void (*) (Event_1_t1285721510 *, UnityAction_1_t476884844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Event_1_t1285721510 *)L_1, (UnityAction_1_t476884844 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		goto IL_002d;
	}

IL_001c:
	{
		Event_1_t1285721510 * L_3 = (Event_1_t1285721510 *)__this->get__event_0();
		UnityAction_1_t476884844 * L_4 = (UnityAction_1_t476884844 *)__this->get_action_1();
		NullCheck((Event_1_t1285721510 *)L_3);
		((  void (*) (Event_1_t1285721510 *, UnityAction_1_t476884844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Event_1_t1285721510 *)L_3, (UnityAction_1_t476884844 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_002d:
	{
		return;
	}
}
// System.Void SteamVR_Events/Action`2<System.Int32,System.Boolean>::.ctor(SteamVR_Events/Event`2<T0,T1>,UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Action_2__ctor_m3584128380_gshared (Action_2_t2270948822 * __this, Event_2_t3885983284 * ____event0, UnityAction_2_t41828916 * ___action1, const MethodInfo* method)
{
	{
		NullCheck((Action_t1836998693 *)__this);
		Action__ctor_m1964921010((Action_t1836998693 *)__this, /*hidden argument*/NULL);
		Event_2_t3885983284 * L_0 = ____event0;
		__this->set__event_0(L_0);
		UnityAction_2_t41828916 * L_1 = ___action1;
		__this->set_action_1(L_1);
		return;
	}
}
// System.Void SteamVR_Events/Action`2<System.Int32,System.Boolean>::Enable(System.Boolean)
extern "C"  void Action_2_Enable_m3258315074_gshared (Action_2_t2270948822 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled0;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Event_2_t3885983284 * L_1 = (Event_2_t3885983284 *)__this->get__event_0();
		UnityAction_2_t41828916 * L_2 = (UnityAction_2_t41828916 *)__this->get_action_1();
		NullCheck((Event_2_t3885983284 *)L_1);
		((  void (*) (Event_2_t3885983284 *, UnityAction_2_t41828916 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Event_2_t3885983284 *)L_1, (UnityAction_2_t41828916 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		goto IL_002d;
	}

IL_001c:
	{
		Event_2_t3885983284 * L_3 = (Event_2_t3885983284 *)__this->get__event_0();
		UnityAction_2_t41828916 * L_4 = (UnityAction_2_t41828916 *)__this->get_action_1();
		NullCheck((Event_2_t3885983284 *)L_3);
		((  void (*) (Event_2_t3885983284 *, UnityAction_2_t41828916 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Event_2_t3885983284 *)L_3, (UnityAction_2_t41828916 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_002d:
	{
		return;
	}
}
// System.Void SteamVR_Events/Action`2<System.Object,System.Boolean>::.ctor(SteamVR_Events/Event`2<T0,T1>,UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Action_2__ctor_m980819815_gshared (Action_2_t2855183315 * __this, Event_2_t175250481 * ____event0, UnityAction_2_t626063409 * ___action1, const MethodInfo* method)
{
	{
		NullCheck((Action_t1836998693 *)__this);
		Action__ctor_m1964921010((Action_t1836998693 *)__this, /*hidden argument*/NULL);
		Event_2_t175250481 * L_0 = ____event0;
		__this->set__event_0(L_0);
		UnityAction_2_t626063409 * L_1 = ___action1;
		__this->set_action_1(L_1);
		return;
	}
}
// System.Void SteamVR_Events/Action`2<System.Object,System.Boolean>::Enable(System.Boolean)
extern "C"  void Action_2_Enable_m822486595_gshared (Action_2_t2855183315 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled0;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Event_2_t175250481 * L_1 = (Event_2_t175250481 *)__this->get__event_0();
		UnityAction_2_t626063409 * L_2 = (UnityAction_2_t626063409 *)__this->get_action_1();
		NullCheck((Event_2_t175250481 *)L_1);
		((  void (*) (Event_2_t175250481 *, UnityAction_2_t626063409 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Event_2_t175250481 *)L_1, (UnityAction_2_t626063409 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		goto IL_002d;
	}

IL_001c:
	{
		Event_2_t175250481 * L_3 = (Event_2_t175250481 *)__this->get__event_0();
		UnityAction_2_t626063409 * L_4 = (UnityAction_2_t626063409 *)__this->get_action_1();
		NullCheck((Event_2_t175250481 *)L_3);
		((  void (*) (Event_2_t175250481 *, UnityAction_2_t626063409 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Event_2_t175250481 *)L_3, (UnityAction_2_t626063409 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_002d:
	{
		return;
	}
}
// System.Void SteamVR_Events/Action`2<System.Object,System.Object>::.ctor(SteamVR_Events/Event`2<T0,T1>,UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Action_2__ctor_m2243214098_gshared (Action_2_t1719057892 * __this, Event_2_t3334092354 * ____event0, UnityAction_2_t3784905282 * ___action1, const MethodInfo* method)
{
	{
		NullCheck((Action_t1836998693 *)__this);
		Action__ctor_m1964921010((Action_t1836998693 *)__this, /*hidden argument*/NULL);
		Event_2_t3334092354 * L_0 = ____event0;
		__this->set__event_0(L_0);
		UnityAction_2_t3784905282 * L_1 = ___action1;
		__this->set_action_1(L_1);
		return;
	}
}
// System.Void SteamVR_Events/Action`2<System.Object,System.Object>::Enable(System.Boolean)
extern "C"  void Action_2_Enable_m1439186680_gshared (Action_2_t1719057892 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled0;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Event_2_t3334092354 * L_1 = (Event_2_t3334092354 *)__this->get__event_0();
		UnityAction_2_t3784905282 * L_2 = (UnityAction_2_t3784905282 *)__this->get_action_1();
		NullCheck((Event_2_t3334092354 *)L_1);
		((  void (*) (Event_2_t3334092354 *, UnityAction_2_t3784905282 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Event_2_t3334092354 *)L_1, (UnityAction_2_t3784905282 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		goto IL_002d;
	}

IL_001c:
	{
		Event_2_t3334092354 * L_3 = (Event_2_t3334092354 *)__this->get__event_0();
		UnityAction_2_t3784905282 * L_4 = (UnityAction_2_t3784905282 *)__this->get_action_1();
		NullCheck((Event_2_t3334092354 *)L_3);
		((  void (*) (Event_2_t3334092354 *, UnityAction_2_t3784905282 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Event_2_t3334092354 *)L_3, (UnityAction_2_t3784905282 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_002d:
	{
		return;
	}
}
// System.Void SteamVR_Events/Action`3<System.Object,System.Object,System.Object>::.ctor(SteamVR_Events/Event`3<T0,T1,T2>,UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void Action_3__ctor_m363946389_gshared (Action_3_t2223553130 * __this, Event_3_t1695830490 * ____event0, UnityAction_3_t3482433968 * ___action1, const MethodInfo* method)
{
	{
		NullCheck((Action_t1836998693 *)__this);
		Action__ctor_m1964921010((Action_t1836998693 *)__this, /*hidden argument*/NULL);
		Event_3_t1695830490 * L_0 = ____event0;
		__this->set__event_0(L_0);
		UnityAction_3_t3482433968 * L_1 = ___action1;
		__this->set_action_1(L_1);
		return;
	}
}
// System.Void SteamVR_Events/Action`3<System.Object,System.Object,System.Object>::Enable(System.Boolean)
extern "C"  void Action_3_Enable_m3822748381_gshared (Action_3_t2223553130 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled0;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Event_3_t1695830490 * L_1 = (Event_3_t1695830490 *)__this->get__event_0();
		UnityAction_3_t3482433968 * L_2 = (UnityAction_3_t3482433968 *)__this->get_action_1();
		NullCheck((Event_3_t1695830490 *)L_1);
		((  void (*) (Event_3_t1695830490 *, UnityAction_3_t3482433968 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Event_3_t1695830490 *)L_1, (UnityAction_3_t3482433968 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		goto IL_002d;
	}

IL_001c:
	{
		Event_3_t1695830490 * L_3 = (Event_3_t1695830490 *)__this->get__event_0();
		UnityAction_3_t3482433968 * L_4 = (UnityAction_3_t3482433968 *)__this->get_action_1();
		NullCheck((Event_3_t1695830490 *)L_3);
		((  void (*) (Event_3_t1695830490 *, UnityAction_3_t3482433968 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Event_3_t1695830490 *)L_3, (UnityAction_3_t3482433968 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_002d:
	{
		return;
	}
}
// System.Void SteamVR_Events/Action`3<UnityEngine.Color,System.Single,System.Boolean>::.ctor(SteamVR_Events/Event`3<T0,T1,T2>,UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void Action_3__ctor_m1084438551_gshared (Action_3_t557175684 * __this, Event_3_t29453044 * ____event0, UnityAction_3_t1816056522 * ___action1, const MethodInfo* method)
{
	{
		NullCheck((Action_t1836998693 *)__this);
		Action__ctor_m1964921010((Action_t1836998693 *)__this, /*hidden argument*/NULL);
		Event_3_t29453044 * L_0 = ____event0;
		__this->set__event_0(L_0);
		UnityAction_3_t1816056522 * L_1 = ___action1;
		__this->set_action_1(L_1);
		return;
	}
}
// System.Void SteamVR_Events/Action`3<UnityEngine.Color,System.Single,System.Boolean>::Enable(System.Boolean)
extern "C"  void Action_3_Enable_m2748940831_gshared (Action_3_t557175684 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled0;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Event_3_t29453044 * L_1 = (Event_3_t29453044 *)__this->get__event_0();
		UnityAction_3_t1816056522 * L_2 = (UnityAction_3_t1816056522 *)__this->get_action_1();
		NullCheck((Event_3_t29453044 *)L_1);
		((  void (*) (Event_3_t29453044 *, UnityAction_3_t1816056522 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Event_3_t29453044 *)L_1, (UnityAction_3_t1816056522 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		goto IL_002d;
	}

IL_001c:
	{
		Event_3_t29453044 * L_3 = (Event_3_t29453044 *)__this->get__event_0();
		UnityAction_3_t1816056522 * L_4 = (UnityAction_3_t1816056522 *)__this->get_action_1();
		NullCheck((Event_3_t29453044 *)L_3);
		((  void (*) (Event_3_t29453044 *, UnityAction_3_t1816056522 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Event_3_t29453044 *)L_3, (UnityAction_3_t1816056522 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_002d:
	{
		return;
	}
}
// System.Void SteamVR_Events/Event`1<System.Boolean>::.ctor()
extern "C"  void Event_1__ctor_m826889986_gshared (Event_1_t1706029839 * __this, const MethodInfo* method)
{
	{
		NullCheck((UnityEvent_1_t3863924733 *)__this);
		((  void (*) (UnityEvent_1_t3863924733 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((UnityEvent_1_t3863924733 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<System.Boolean>::Listen(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Listen_m1179862519_gshared (Event_1_t1706029839 * __this, UnityAction_1_t897193173 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t897193173 * L_0 = ___action0;
		NullCheck((UnityEvent_1_t3863924733 *)__this);
		((  void (*) (UnityEvent_1_t3863924733 *, UnityAction_1_t897193173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((UnityEvent_1_t3863924733 *)__this, (UnityAction_1_t897193173 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<System.Boolean>::Remove(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Remove_m4204324720_gshared (Event_1_t1706029839 * __this, UnityAction_1_t897193173 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t897193173 * L_0 = ___action0;
		NullCheck((UnityEvent_1_t3863924733 *)__this);
		((  void (*) (UnityEvent_1_t3863924733 *, UnityAction_1_t897193173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((UnityEvent_1_t3863924733 *)__this, (UnityAction_1_t897193173 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<System.Boolean>::Send(T)
extern "C"  void Event_1_Send_m3832506532_gshared (Event_1_t1706029839 * __this, bool ___arg00, const MethodInfo* method)
{
	{
		bool L_0 = ___arg00;
		NullCheck((UnityEvent_1_t3863924733 *)__this);
		((  void (*) (UnityEvent_1_t3863924733 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityEvent_1_t3863924733 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<System.Object>::.ctor()
extern "C"  void Event_1__ctor_m28252523_gshared (Event_1_t569904416 * __this, const MethodInfo* method)
{
	{
		NullCheck((UnityEvent_1_t2727799310 *)__this);
		((  void (*) (UnityEvent_1_t2727799310 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((UnityEvent_1_t2727799310 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<System.Object>::Listen(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Listen_m1961284276_gshared (Event_1_t569904416 * __this, UnityAction_1_t4056035046 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t4056035046 * L_0 = ___action0;
		NullCheck((UnityEvent_1_t2727799310 *)__this);
		((  void (*) (UnityEvent_1_t2727799310 *, UnityAction_1_t4056035046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((UnityEvent_1_t2727799310 *)__this, (UnityAction_1_t4056035046 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<System.Object>::Remove(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Remove_m1960904225_gshared (Event_1_t569904416 * __this, UnityAction_1_t4056035046 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t4056035046 * L_0 = ___action0;
		NullCheck((UnityEvent_1_t2727799310 *)__this);
		((  void (*) (UnityEvent_1_t2727799310 *, UnityAction_1_t4056035046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((UnityEvent_1_t2727799310 *)__this, (UnityAction_1_t4056035046 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<System.Object>::Send(T)
extern "C"  void Event_1_Send_m2836426603_gshared (Event_1_t569904416 * __this, Il2CppObject * ___arg00, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___arg00;
		NullCheck((UnityEvent_1_t2727799310 *)__this);
		((  void (*) (UnityEvent_1_t2727799310 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityEvent_1_t2727799310 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<System.Single>::.ctor()
extern "C"  void Event_1__ctor_m2350045080_gshared (Event_1_t4251932349 * __this, const MethodInfo* method)
{
	{
		NullCheck((UnityEvent_1_t2114859947 *)__this);
		((  void (*) (UnityEvent_1_t2114859947 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((UnityEvent_1_t2114859947 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<System.Single>::Listen(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Listen_m3282637393_gshared (Event_1_t4251932349 * __this, UnityAction_1_t3443095683 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t3443095683 * L_0 = ___action0;
		NullCheck((UnityEvent_1_t2114859947 *)__this);
		((  void (*) (UnityEvent_1_t2114859947 *, UnityAction_1_t3443095683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((UnityEvent_1_t2114859947 *)__this, (UnityAction_1_t3443095683 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<System.Single>::Remove(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Remove_m2571833398_gshared (Event_1_t4251932349 * __this, UnityAction_1_t3443095683 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t3443095683 * L_0 = ___action0;
		NullCheck((UnityEvent_1_t2114859947 *)__this);
		((  void (*) (UnityEvent_1_t2114859947 *, UnityAction_1_t3443095683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((UnityEvent_1_t2114859947 *)__this, (UnityAction_1_t3443095683 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<System.Single>::Send(T)
extern "C"  void Event_1_Send_m2949776906_gshared (Event_1_t4251932349 * __this, float ___arg00, const MethodInfo* method)
{
	{
		float L_0 = ___arg00;
		NullCheck((UnityEvent_1_t2114859947 *)__this);
		((  void (*) (UnityEvent_1_t2114859947 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityEvent_1_t2114859947 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<Valve.VR.VREvent_t>::.ctor()
extern "C"  void Event_1__ctor_m2157519104_gshared (Event_1_t1285721510 * __this, const MethodInfo* method)
{
	{
		NullCheck((UnityEvent_1_t3443616404 *)__this);
		((  void (*) (UnityEvent_1_t3443616404 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((UnityEvent_1_t3443616404 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<Valve.VR.VREvent_t>::Listen(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Listen_m2330954003_gshared (Event_1_t1285721510 * __this, UnityAction_1_t476884844 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t476884844 * L_0 = ___action0;
		NullCheck((UnityEvent_1_t3443616404 *)__this);
		((  void (*) (UnityEvent_1_t3443616404 *, UnityAction_1_t476884844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((UnityEvent_1_t3443616404 *)__this, (UnityAction_1_t476884844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<Valve.VR.VREvent_t>::Remove(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Remove_m2607860930_gshared (Event_1_t1285721510 * __this, UnityAction_1_t476884844 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t476884844 * L_0 = ___action0;
		NullCheck((UnityEvent_1_t3443616404 *)__this);
		((  void (*) (UnityEvent_1_t3443616404 *, UnityAction_1_t476884844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((UnityEvent_1_t3443616404 *)__this, (UnityAction_1_t476884844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void SteamVR_Events/Event`1<Valve.VR.VREvent_t>::Send(T)
extern "C"  void Event_1_Send_m2783568422_gshared (Event_1_t1285721510 * __this, VREvent_t_t3405266389  ___arg00, const MethodInfo* method)
{
	{
		VREvent_t_t3405266389  L_0 = ___arg00;
		NullCheck((UnityEvent_1_t3443616404 *)__this);
		((  void (*) (UnityEvent_1_t3443616404 *, VREvent_t_t3405266389 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityEvent_1_t3443616404 *)__this, (VREvent_t_t3405266389 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void SteamVR_Events/Event`2<System.Int32,System.Boolean>::.ctor()
extern "C"  void Event_2__ctor_m456375898_gshared (Event_2_t3885983284 * __this, const MethodInfo* method)
{
	{
		NullCheck((UnityEvent_2_t1924026834 *)__this);
		((  void (*) (UnityEvent_2_t1924026834 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((UnityEvent_2_t1924026834 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void SteamVR_Events/Event`2<System.Int32,System.Boolean>::Listen(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Event_2_Listen_m494929147_gshared (Event_2_t3885983284 * __this, UnityAction_2_t41828916 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_2_t41828916 * L_0 = ___action0;
		NullCheck((UnityEvent_2_t1924026834 *)__this);
		((  void (*) (UnityEvent_2_t1924026834 *, UnityAction_2_t41828916 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((UnityEvent_2_t1924026834 *)__this, (UnityAction_2_t41828916 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void SteamVR_Events/Event`2<System.Int32,System.Boolean>::Remove(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Event_2_Remove_m4065385546_gshared (Event_2_t3885983284 * __this, UnityAction_2_t41828916 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_2_t41828916 * L_0 = ___action0;
		NullCheck((UnityEvent_2_t1924026834 *)__this);
		((  void (*) (UnityEvent_2_t1924026834 *, UnityAction_2_t41828916 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((UnityEvent_2_t1924026834 *)__this, (UnityAction_2_t41828916 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void SteamVR_Events/Event`2<System.Int32,System.Boolean>::Send(T0,T1)
extern "C"  void Event_2_Send_m2682015001_gshared (Event_2_t3885983284 * __this, int32_t ___arg00, bool ___arg11, const MethodInfo* method)
{
	{
		int32_t L_0 = ___arg00;
		bool L_1 = ___arg11;
		NullCheck((UnityEvent_2_t1924026834 *)__this);
		((  void (*) (UnityEvent_2_t1924026834 *, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityEvent_2_t1924026834 *)__this, (int32_t)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void SteamVR_Events/Event`2<System.Object,System.Boolean>::.ctor()
extern "C"  void Event_2__ctor_m1718189359_gshared (Event_2_t175250481 * __this, const MethodInfo* method)
{
	{
		NullCheck((UnityEvent_2_t2508261327 *)__this);
		((  void (*) (UnityEvent_2_t2508261327 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((UnityEvent_2_t2508261327 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void SteamVR_Events/Event`2<System.Object,System.Boolean>::Listen(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Event_2_Listen_m1492392574_gshared (Event_2_t175250481 * __this, UnityAction_2_t626063409 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_2_t626063409 * L_0 = ___action0;
		NullCheck((UnityEvent_2_t2508261327 *)__this);
		((  void (*) (UnityEvent_2_t2508261327 *, UnityAction_2_t626063409 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((UnityEvent_2_t2508261327 *)__this, (UnityAction_2_t626063409 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void SteamVR_Events/Event`2<System.Object,System.Boolean>::Remove(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Event_2_Remove_m760931903_gshared (Event_2_t175250481 * __this, UnityAction_2_t626063409 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_2_t626063409 * L_0 = ___action0;
		NullCheck((UnityEvent_2_t2508261327 *)__this);
		((  void (*) (UnityEvent_2_t2508261327 *, UnityAction_2_t626063409 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((UnityEvent_2_t2508261327 *)__this, (UnityAction_2_t626063409 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void SteamVR_Events/Event`2<System.Object,System.Boolean>::Send(T0,T1)
extern "C"  void Event_2_Send_m2170767364_gshared (Event_2_t175250481 * __this, Il2CppObject * ___arg00, bool ___arg11, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___arg00;
		bool L_1 = ___arg11;
		NullCheck((UnityEvent_2_t2508261327 *)__this);
		((  void (*) (UnityEvent_2_t2508261327 *, Il2CppObject *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityEvent_2_t2508261327 *)__this, (Il2CppObject *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void SteamVR_Events/Event`2<System.Object,System.Object>::.ctor()
extern "C"  void Event_2__ctor_m3642705320_gshared (Event_2_t3334092354 * __this, const MethodInfo* method)
{
	{
		NullCheck((UnityEvent_2_t1372135904 *)__this);
		((  void (*) (UnityEvent_2_t1372135904 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((UnityEvent_2_t1372135904 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void SteamVR_Events/Event`2<System.Object,System.Object>::Listen(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Event_2_Listen_m2446347801_gshared (Event_2_t3334092354 * __this, UnityAction_2_t3784905282 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_2_t3784905282 * L_0 = ___action0;
		NullCheck((UnityEvent_2_t1372135904 *)__this);
		((  void (*) (UnityEvent_2_t1372135904 *, UnityAction_2_t3784905282 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((UnityEvent_2_t1372135904 *)__this, (UnityAction_2_t3784905282 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void SteamVR_Events/Event`2<System.Object,System.Object>::Remove(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Event_2_Remove_m197184152_gshared (Event_2_t3334092354 * __this, UnityAction_2_t3784905282 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_2_t3784905282 * L_0 = ___action0;
		NullCheck((UnityEvent_2_t1372135904 *)__this);
		((  void (*) (UnityEvent_2_t1372135904 *, UnityAction_2_t3784905282 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((UnityEvent_2_t1372135904 *)__this, (UnityAction_2_t3784905282 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void SteamVR_Events/Event`2<System.Object,System.Object>::Send(T0,T1)
extern "C"  void Event_2_Send_m2307785703_gshared (Event_2_t3334092354 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___arg00;
		Il2CppObject * L_1 = ___arg11;
		NullCheck((UnityEvent_2_t1372135904 *)__this);
		((  void (*) (UnityEvent_2_t1372135904 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityEvent_2_t1372135904 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void SteamVR_Events/Event`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Event_3__ctor_m3485785953_gshared (Event_3_t1695830490 * __this, const MethodInfo* method)
{
	{
		NullCheck((UnityEvent_3_t3149477088 *)__this);
		((  void (*) (UnityEvent_3_t3149477088 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((UnityEvent_3_t3149477088 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void SteamVR_Events/Event`3<System.Object,System.Object,System.Object>::Listen(UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void Event_3_Listen_m733424601_gshared (Event_3_t1695830490 * __this, UnityAction_3_t3482433968 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_3_t3482433968 * L_0 = ___action0;
		NullCheck((UnityEvent_3_t3149477088 *)__this);
		((  void (*) (UnityEvent_3_t3149477088 *, UnityAction_3_t3482433968 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((UnityEvent_3_t3149477088 *)__this, (UnityAction_3_t3482433968 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void SteamVR_Events/Event`3<System.Object,System.Object,System.Object>::Remove(UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void Event_3_Remove_m3453444002_gshared (Event_3_t1695830490 * __this, UnityAction_3_t3482433968 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_3_t3482433968 * L_0 = ___action0;
		NullCheck((UnityEvent_3_t3149477088 *)__this);
		((  void (*) (UnityEvent_3_t3149477088 *, UnityAction_3_t3482433968 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((UnityEvent_3_t3149477088 *)__this, (UnityAction_3_t3482433968 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void SteamVR_Events/Event`3<System.Object,System.Object,System.Object>::Send(T0,T1,T2)
extern "C"  void Event_3_Send_m109483862_gshared (Event_3_t1695830490 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___arg00;
		Il2CppObject * L_1 = ___arg11;
		Il2CppObject * L_2 = ___arg22;
		NullCheck((UnityEvent_3_t3149477088 *)__this);
		((  void (*) (UnityEvent_3_t3149477088 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityEvent_3_t3149477088 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void SteamVR_Events/Event`3<UnityEngine.Color,System.Single,System.Boolean>::.ctor()
extern "C"  void Event_3__ctor_m3097877155_gshared (Event_3_t29453044 * __this, const MethodInfo* method)
{
	{
		NullCheck((UnityEvent_3_t1483099642 *)__this);
		((  void (*) (UnityEvent_3_t1483099642 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((UnityEvent_3_t1483099642 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void SteamVR_Events/Event`3<UnityEngine.Color,System.Single,System.Boolean>::Listen(UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void Event_3_Listen_m3063479851_gshared (Event_3_t29453044 * __this, UnityAction_3_t1816056522 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_3_t1816056522 * L_0 = ___action0;
		NullCheck((UnityEvent_3_t1483099642 *)__this);
		((  void (*) (UnityEvent_3_t1483099642 *, UnityAction_3_t1816056522 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((UnityEvent_3_t1483099642 *)__this, (UnityAction_3_t1816056522 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void SteamVR_Events/Event`3<UnityEngine.Color,System.Single,System.Boolean>::Remove(UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void Event_3_Remove_m2106404318_gshared (Event_3_t29453044 * __this, UnityAction_3_t1816056522 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_3_t1816056522 * L_0 = ___action0;
		NullCheck((UnityEvent_3_t1483099642 *)__this);
		((  void (*) (UnityEvent_3_t1483099642 *, UnityAction_3_t1816056522 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((UnityEvent_3_t1483099642 *)__this, (UnityAction_3_t1816056522 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void SteamVR_Events/Event`3<UnityEngine.Color,System.Single,System.Boolean>::Send(T0,T1,T2)
extern "C"  void Event_3_Send_m1882481666_gshared (Event_3_t29453044 * __this, Color_t2020392075  ___arg00, float ___arg11, bool ___arg22, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ___arg00;
		float L_1 = ___arg11;
		bool L_2 = ___arg22;
		NullCheck((UnityEvent_3_t1483099642 *)__this);
		((  void (*) (UnityEvent_3_t1483099642 *, Color_t2020392075 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityEvent_3_t1483099642 *)__this, (Color_t2020392075 )L_0, (float)L_1, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3072925129_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3662000152_gshared (Action_1_t3627374100 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3662000152((Action_1_t3627374100 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m226849422_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m226849422_gshared (Action_1_t3627374100 * __this, bool ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m226849422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2990292511_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m345545414_gshared (Action_1_t1873676830 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Int32>::Invoke(T)
extern "C"  void Action_1_Invoke_m3352874125_gshared (Action_1_t1873676830 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3352874125((Action_1_t1873676830 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1720726178_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1720726178_gshared (Action_1_t1873676830 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1720726178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3128406917_gshared (Action_1_t1873676830 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m584977596_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m1684652980_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1684652980((Action_1_t2491248677 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1305519803_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2057605070_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m946854823_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3842146412_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m3842146412((Action_2_t2525452034 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m3907381723_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3907381723_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m3907381723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2798191693_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3362391082_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1501152969_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m1501152969((Action_2_t2572051853 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1914861552_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3956733788_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1015489335_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1791706206_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2580780957_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m2489948797_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t2471096271 * L_2 = (ArrayReadOnlyList_1_t2471096271 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t2471096271 * L_9 = (ArrayReadOnlyList_1_t2471096271 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1859988746_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1942816078_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeNamedArgument_t94157543  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m285299945_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m480171694_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_U24current_2();
		CustomAttributeNamedArgument_t94157543  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m949306872_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t4170771815 * L_2 = (ArrayReadOnlyList_1_t4170771815 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t4170771815 * L_9 = (ArrayReadOnlyList_1_t4170771815 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_10 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2403602883_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m409316647_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeTypedArgument_t1498197914  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m988222504_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2332089385_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_U24current_2();
		CustomAttributeTypedArgument_t1498197914  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m692741405_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t1279844890 * L_2 = (ArrayReadOnlyList_1_t1279844890 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t1498197914  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t1279844890 * L_9 = (ArrayReadOnlyList_1_t1279844890 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_10 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2201090542_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m2430810679_gshared (ArrayReadOnlyList_1_t2471096271 * __this, ObjectU5BU5D_t3614634134* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m2780765696_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t2471096271 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t2471096271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t2471096271 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m176001975_MetadataUsageId;
extern "C"  Il2CppObject * ArrayReadOnlyList_1_get_Item_m176001975_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m176001975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m314687476_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m962317777_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2717922212_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3970067462_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m2539474626_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1266627404_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m816115094_gshared (ArrayReadOnlyList_1_t2471096271 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m1078352793_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1537228832_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m1136669199_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m1875216835_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2701218731_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2289309720_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2289309720_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2289309720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m691892240_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m3039869667_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t4170771815 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t4170771815 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t4170771815 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m2694472846_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t94157543  ArrayReadOnlyList_1_get_Item_m2694472846_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m2694472846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3536854615_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2661355086_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2189922207_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m961024239_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m1565299387_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1269788217_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m4003949395_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m634288642_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1220844927_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m2938723476_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m2325516426_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m4104441984_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2160816107_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2160816107_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2160816107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m3778554727_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgumentU5BU5D_t1075686591* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m3194679940_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t1279844890 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t1279844890 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t1279844890 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m2045253203_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t1498197914  ArrayReadOnlyList_1_get_Item_m2045253203_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m2045253203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t1498197914  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m1476592004_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, CustomAttributeTypedArgument_t1498197914  ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2272682593_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m745254596_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m592463462_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m638842154_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1984901664_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgument_t1498197914  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (CustomAttributeTypedArgument_t1498197914 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m3708038182_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgumentU5BU5D_t1075686591* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m3821693737_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1809425308_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgument_t1498197914  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (CustomAttributeTypedArgument_t1498197914 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m503707439_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, CustomAttributeTypedArgument_t1498197914  ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m632503387_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2270349795_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2158247090_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2158247090_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2158247090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/InternalEnumerator`1<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4219048862_gshared (InternalEnumerator_1_t3951913483 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4219048862_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3951913483 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3951913483 *>(__this + 1);
	InternalEnumerator_1__ctor_m4219048862(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1836683882_gshared (InternalEnumerator_1_t3951913483 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1836683882_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3951913483 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3951913483 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1836683882(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2195588876_gshared (InternalEnumerator_1_t3951913483 * __this, const MethodInfo* method)
{
	{
		ExtensionIntPair_t3093161221  L_0 = InternalEnumerator_1_get_Current_m2056029337((InternalEnumerator_1_t3951913483 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ExtensionIntPair_t3093161221  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2195588876_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3951913483 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3951913483 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2195588876(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1490851729_gshared (InternalEnumerator_1_t3951913483 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1490851729_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3951913483 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3951913483 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1490851729(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m970367870_gshared (InternalEnumerator_1_t3951913483 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m970367870_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3951913483 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3951913483 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m970367870(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2056029337_MetadataUsageId;
extern "C"  ExtensionIntPair_t3093161221  InternalEnumerator_1_get_Current_m2056029337_gshared (InternalEnumerator_1_t3951913483 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2056029337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ExtensionIntPair_t3093161221  L_8 = ((  ExtensionIntPair_t3093161221  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ExtensionIntPair_t3093161221  InternalEnumerator_1_get_Current_m2056029337_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3951913483 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3951913483 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2056029337(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Gvr.Internal.EmulatorTouchEvent/Pointer>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m722105347_gshared (InternalEnumerator_1_t3859437264 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m722105347_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3859437264 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3859437264 *>(__this + 1);
	InternalEnumerator_1__ctor_m722105347(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Gvr.Internal.EmulatorTouchEvent/Pointer>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3022692659_gshared (InternalEnumerator_1_t3859437264 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3022692659_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3859437264 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3859437264 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3022692659(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Gvr.Internal.EmulatorTouchEvent/Pointer>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4074221803_gshared (InternalEnumerator_1_t3859437264 * __this, const MethodInfo* method)
{
	{
		Pointer_t3000685002  L_0 = InternalEnumerator_1_get_Current_m1007921314((InternalEnumerator_1_t3859437264 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Pointer_t3000685002  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4074221803_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3859437264 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3859437264 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4074221803(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Gvr.Internal.EmulatorTouchEvent/Pointer>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1197161102_gshared (InternalEnumerator_1_t3859437264 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1197161102_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3859437264 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3859437264 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1197161102(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Gvr.Internal.EmulatorTouchEvent/Pointer>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1534192071_gshared (InternalEnumerator_1_t3859437264 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1534192071_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3859437264 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3859437264 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1534192071(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Gvr.Internal.EmulatorTouchEvent/Pointer>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1007921314_MetadataUsageId;
extern "C"  Pointer_t3000685002  InternalEnumerator_1_get_Current_m1007921314_gshared (InternalEnumerator_1_t3859437264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1007921314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Pointer_t3000685002  L_8 = ((  Pointer_t3000685002  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Pointer_t3000685002  InternalEnumerator_1_get_Current_m1007921314_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3859437264 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3859437264 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1007921314(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2265739932_gshared (InternalEnumerator_1_t2870158877 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2265739932_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1__ctor_m2265739932(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		TableRange_t2011406615  L_0 = InternalEnumerator_1_get_Current_m2151132603((InternalEnumerator_1_t2870158877 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TableRange_t2011406615  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1050822571_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1050822571_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1050822571(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1979432532_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1979432532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1979432532(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2151132603_MetadataUsageId;
extern "C"  TableRange_t2011406615  InternalEnumerator_1_get_Current_m2151132603_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2151132603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TableRange_t2011406615  L_8 = ((  TableRange_t2011406615  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TableRange_t2011406615  InternalEnumerator_1_get_Current_m2151132603_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2151132603(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2111763266_gshared (InternalEnumerator_1_t565169432 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2111763266_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1__ctor_m2111763266(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3847951219((InternalEnumerator_1_t565169432 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2038682075_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2038682075_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2038682075(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1182905290_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1182905290_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1182905290(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3847951219_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847951219_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3847951219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847951219_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3847951219(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<proto.PhoneEvent/Types/Type>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3916972288_gshared (InternalEnumerator_1_t2389233123 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3916972288_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2389233123 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2389233123 *>(__this + 1);
	InternalEnumerator_1__ctor_m3916972288(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<proto.PhoneEvent/Types/Type>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3116649964_gshared (InternalEnumerator_1_t2389233123 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3116649964_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2389233123 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2389233123 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3116649964(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<proto.PhoneEvent/Types/Type>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m614726378_gshared (InternalEnumerator_1_t2389233123 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m1798355933((InternalEnumerator_1_t2389233123 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m614726378_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2389233123 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2389233123 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m614726378(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<proto.PhoneEvent/Types/Type>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1398024421_gshared (InternalEnumerator_1_t2389233123 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1398024421_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2389233123 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2389233123 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1398024421(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<proto.PhoneEvent/Types/Type>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3131275056_gshared (InternalEnumerator_1_t2389233123 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3131275056_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2389233123 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2389233123 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3131275056(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<proto.PhoneEvent/Types/Type>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1798355933_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1798355933_gshared (InternalEnumerator_1_t2389233123 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1798355933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1798355933_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2389233123 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2389233123 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1798355933(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<SteamVR_Utils/RigidTransform>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m162628759_gshared (InternalEnumerator_1_t3461135388 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m162628759_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3461135388 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3461135388 *>(__this + 1);
	InternalEnumerator_1__ctor_m162628759(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<SteamVR_Utils/RigidTransform>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m71863143_gshared (InternalEnumerator_1_t3461135388 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m71863143_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3461135388 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3461135388 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m71863143(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<SteamVR_Utils/RigidTransform>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1961150267_gshared (InternalEnumerator_1_t3461135388 * __this, const MethodInfo* method)
{
	{
		RigidTransform_t2602383126  L_0 = InternalEnumerator_1_get_Current_m1620449408((InternalEnumerator_1_t3461135388 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RigidTransform_t2602383126  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1961150267_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3461135388 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3461135388 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1961150267(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<SteamVR_Utils/RigidTransform>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m678020422_gshared (InternalEnumerator_1_t3461135388 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m678020422_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3461135388 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3461135388 *>(__this + 1);
	InternalEnumerator_1_Dispose_m678020422(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<SteamVR_Utils/RigidTransform>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2215102579_gshared (InternalEnumerator_1_t3461135388 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2215102579_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3461135388 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3461135388 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2215102579(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<SteamVR_Utils/RigidTransform>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1620449408_MetadataUsageId;
extern "C"  RigidTransform_t2602383126  InternalEnumerator_1_get_Current_m1620449408_gshared (InternalEnumerator_1_t3461135388 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1620449408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RigidTransform_t2602383126  L_8 = ((  RigidTransform_t2602383126  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  RigidTransform_t2602383126  InternalEnumerator_1_get_Current_m1620449408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3461135388 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3461135388 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1620449408(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1866922360_gshared (InternalEnumerator_1_t3452969744 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1866922360_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	InternalEnumerator_1__ctor_m1866922360(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	{
		ArraySegment_1_t2594217482  L_0 = InternalEnumerator_1_get_Current_m1894741129((InternalEnumerator_1_t3452969744 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ArraySegment_1_t2594217482  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m592267945_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m592267945_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	InternalEnumerator_1_Dispose_m592267945(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1460734872_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1460734872_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1460734872(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1894741129_MetadataUsageId;
extern "C"  ArraySegment_1_t2594217482  InternalEnumerator_1_get_Current_m1894741129_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1894741129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ArraySegment_1_t2594217482  L_8 = ((  ArraySegment_1_t2594217482  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ArraySegment_1_t2594217482  InternalEnumerator_1_get_Current_m1894741129_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1894741129(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4119890600_gshared (InternalEnumerator_1_t389359684 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4119890600_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1__ctor_m4119890600(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		bool L_0 = InternalEnumerator_1_get_Current_m1943362081((InternalEnumerator_1_t389359684 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1640363425_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1640363425_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1640363425(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1595676968_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1595676968_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1595676968(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1943362081_MetadataUsageId;
extern "C"  bool InternalEnumerator_1_get_Current_m1943362081_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1943362081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		bool L_8 = ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  bool InternalEnumerator_1_get_Current_m1943362081_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1943362081(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3043733612_gshared (InternalEnumerator_1_t246889402 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3043733612_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1__ctor_m3043733612(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m4154615771((InternalEnumerator_1_t246889402 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1148506519_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1148506519_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1148506519(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2651026500_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2651026500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2651026500(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4154615771_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4154615771_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4154615771_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4154615771_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4154615771(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m960275522_gshared (InternalEnumerator_1_t18266304 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m960275522_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1__ctor_m960275522(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = InternalEnumerator_1_get_Current_m2960188445((InternalEnumerator_1_t18266304 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppChar L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m811081805_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m811081805_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1_Dispose_m811081805(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m412569442_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m412569442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m412569442(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2960188445_MetadataUsageId;
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m2960188445_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2960188445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppChar L_8 = ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m2960188445_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2960188445(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m675130983_gshared (InternalEnumerator_1_t3907627660 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m675130983_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1__ctor_m675130983(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		DictionaryEntry_t3048875398  L_0 = InternalEnumerator_1_get_Current_m2351441486((InternalEnumerator_1_t3907627660 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3597982928_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3597982928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3597982928(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1636015243_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1636015243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1636015243(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2351441486_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  InternalEnumerator_1_get_Current_m2351441486_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2351441486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DictionaryEntry_t3048875398  L_8 = ((  DictionaryEntry_t3048875398  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  InternalEnumerator_1_get_Current_m2351441486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2351441486(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2688327768_gshared (InternalEnumerator_1_t1723885533 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2688327768_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1__ctor_m2688327768(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		Link_t865133271  L_0 = InternalEnumerator_1_get_Current_m1855333455((InternalEnumerator_1_t1723885533 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t865133271  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1064404287_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1064404287_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1064404287(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3585886944_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3585886944_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3585886944(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1855333455_MetadataUsageId;
extern "C"  Link_t865133271  InternalEnumerator_1_get_Current_m1855333455_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1855333455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t865133271  L_8 = ((  Link_t865133271  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t865133271  InternalEnumerator_1_get_Current_m1855333455_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1855333455(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4040196035_gshared (InternalEnumerator_1_t3023014317 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4040196035_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3023014317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3023014317 *>(__this + 1);
	InternalEnumerator_1__ctor_m4040196035(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2890569115_gshared (InternalEnumerator_1_t3023014317 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2890569115_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3023014317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3023014317 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2890569115(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851672591_gshared (InternalEnumerator_1_t3023014317 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2164262055  L_0 = InternalEnumerator_1_get_Current_m2953187508((InternalEnumerator_1_t3023014317 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2164262055  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851672591_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3023014317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3023014317 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851672591(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2480980026_gshared (InternalEnumerator_1_t3023014317 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2480980026_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3023014317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3023014317 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2480980026(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1330800047_gshared (InternalEnumerator_1_t3023014317 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1330800047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3023014317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3023014317 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1330800047(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2953187508_MetadataUsageId;
extern "C"  KeyValuePair_2_t2164262055  InternalEnumerator_1_get_Current_m2953187508_gshared (InternalEnumerator_1_t3023014317 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2953187508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2164262055  L_8 = ((  KeyValuePair_2_t2164262055  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2164262055  InternalEnumerator_1_get_Current_m2953187508_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3023014317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3023014317 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2953187508(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,proto.PhoneEvent/Types/Type>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m691469074_gshared (InternalEnumerator_1_t3449371276 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m691469074_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3449371276 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3449371276 *>(__this + 1);
	InternalEnumerator_1__ctor_m691469074(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,proto.PhoneEvent/Types/Type>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2913916422_gshared (InternalEnumerator_1_t3449371276 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2913916422_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3449371276 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3449371276 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2913916422(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,proto.PhoneEvent/Types/Type>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2057512592_gshared (InternalEnumerator_1_t3449371276 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2590619014  L_0 = InternalEnumerator_1_get_Current_m1672068565((InternalEnumerator_1_t3449371276 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2590619014  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2057512592_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3449371276 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3449371276 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2057512592(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,proto.PhoneEvent/Types/Type>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2342606037_gshared (InternalEnumerator_1_t3449371276 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2342606037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3449371276 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3449371276 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2342606037(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,proto.PhoneEvent/Types/Type>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2783335938_gshared (InternalEnumerator_1_t3449371276 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2783335938_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3449371276 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3449371276 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2783335938(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,proto.PhoneEvent/Types/Type>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1672068565_MetadataUsageId;
extern "C"  KeyValuePair_2_t2590619014  InternalEnumerator_1_get_Current_m1672068565_gshared (InternalEnumerator_1_t3449371276 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1672068565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2590619014  L_8 = ((  KeyValuePair_2_t2590619014  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2590619014  InternalEnumerator_1_get_Current_m1672068565_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3449371276 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3449371276 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1672068565(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m971224554_gshared (InternalEnumerator_1_t3990767863 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m971224554_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	InternalEnumerator_1__ctor_m971224554(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2418489778_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2418489778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2418489778(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1125095150_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3132015601  L_0 = InternalEnumerator_1_get_Current_m3903656979((InternalEnumerator_1_t3990767863 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1125095150_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1125095150(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2362056211_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2362056211_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2362056211(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m68568274_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m68568274_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m68568274(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3903656979_MetadataUsageId;
extern "C"  KeyValuePair_2_t3132015601  InternalEnumerator_1_get_Current_m3903656979_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3903656979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3132015601  L_8 = ((  KeyValuePair_2_t3132015601  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3132015601  InternalEnumerator_1_get_Current_m3903656979_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3903656979(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3441346029_gshared (InternalEnumerator_1_t313372414 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3441346029_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1__ctor_m3441346029(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3749587448  L_0 = InternalEnumerator_1_get_Current_m3582710858((InternalEnumerator_1_t313372414 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m718416578_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m718416578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1_Dispose_m718416578(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1791963761_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1791963761_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1791963761(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3582710858_MetadataUsageId;
extern "C"  KeyValuePair_2_t3749587448  InternalEnumerator_1_get_Current_m3582710858_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3582710858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3749587448  L_8 = ((  KeyValuePair_2_t3749587448  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3749587448  InternalEnumerator_1_get_Current_m3582710858_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3582710858(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m967618647_gshared (InternalEnumerator_1_t2033732330 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m967618647_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	InternalEnumerator_1__ctor_m967618647(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1174980068  L_0 = InternalEnumerator_1_get_Current_m3900993294((InternalEnumerator_1_t2033732330 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m318835130_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m318835130_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	InternalEnumerator_1_Dispose_m318835130(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4294226955_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4294226955_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4294226955(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3900993294_MetadataUsageId;
extern "C"  KeyValuePair_2_t1174980068  InternalEnumerator_1_get_Current_m3900993294_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3900993294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1174980068  L_8 = ((  KeyValuePair_2_t1174980068  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1174980068  InternalEnumerator_1_get_Current_m3900993294_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3900993294(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3362782841_gshared (InternalEnumerator_1_t280035060 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3362782841_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	InternalEnumerator_1__ctor_m3362782841(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3716250094  L_0 = InternalEnumerator_1_get_Current_m2882946014((InternalEnumerator_1_t280035060 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1748410190_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1748410190_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1748410190(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3486952605_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3486952605_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3486952605(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2882946014_MetadataUsageId;
extern "C"  KeyValuePair_2_t3716250094  InternalEnumerator_1_get_Current_m2882946014_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2882946014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3716250094  L_8 = ((  KeyValuePair_2_t3716250094  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3716250094  InternalEnumerator_1_get_Current_m2882946014_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2882946014(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3587374424_gshared (InternalEnumerator_1_t897606907 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3587374424_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	InternalEnumerator_1__ctor_m3587374424(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = InternalEnumerator_1_get_Current_m2345377791((InternalEnumerator_1_t897606907 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2413981551_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2413981551_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2413981551(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1667794624_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1667794624_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1667794624(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2345377791_MetadataUsageId;
extern "C"  KeyValuePair_2_t38854645  InternalEnumerator_1_get_Current_m2345377791_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2345377791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t38854645  L_8 = ((  KeyValuePair_2_t38854645  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t38854645  InternalEnumerator_1_get_Current_m2345377791_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2345377791(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m693451429_gshared (InternalEnumerator_1_t284667544 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m693451429_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t284667544 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t284667544 *>(__this + 1);
	InternalEnumerator_1__ctor_m693451429(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m428834369_gshared (InternalEnumerator_1_t284667544 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m428834369_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t284667544 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t284667544 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m428834369(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3211838505_gshared (InternalEnumerator_1_t284667544 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3720882578  L_0 = InternalEnumerator_1_get_Current_m1593252484((InternalEnumerator_1_t284667544 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3720882578  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3211838505_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t284667544 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t284667544 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3211838505(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3400531782_gshared (InternalEnumerator_1_t284667544 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3400531782_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t284667544 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t284667544 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3400531782(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4136778385_gshared (InternalEnumerator_1_t284667544 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4136778385_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t284667544 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t284667544 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4136778385(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1593252484_MetadataUsageId;
extern "C"  KeyValuePair_2_t3720882578  InternalEnumerator_1_get_Current_m1593252484_gshared (InternalEnumerator_1_t284667544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1593252484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3720882578  L_8 = ((  KeyValuePair_2_t3720882578  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3720882578  InternalEnumerator_1_get_Current_m1593252484_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t284667544 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t284667544 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1593252484(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3741623327_gshared (InternalEnumerator_1_t1117354526 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3741623327_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1117354526 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1117354526 *>(__this + 1);
	InternalEnumerator_1__ctor_m3741623327(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224452439_gshared (InternalEnumerator_1_t1117354526 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224452439_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1117354526 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1117354526 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224452439(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3915226427_gshared (InternalEnumerator_1_t1117354526 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t258602264  L_0 = InternalEnumerator_1_get_Current_m2268465014((InternalEnumerator_1_t1117354526 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t258602264  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3915226427_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1117354526 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1117354526 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3915226427(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4109208472_gshared (InternalEnumerator_1_t1117354526 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4109208472_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1117354526 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1117354526 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4109208472(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3428235539_gshared (InternalEnumerator_1_t1117354526 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3428235539_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1117354526 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1117354526 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3428235539(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2268465014_MetadataUsageId;
extern "C"  KeyValuePair_2_t258602264  InternalEnumerator_1_get_Current_m2268465014_gshared (InternalEnumerator_1_t1117354526 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2268465014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t258602264  L_8 = ((  KeyValuePair_2_t258602264  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t258602264  InternalEnumerator_1_get_Current_m2268465014_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1117354526 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1117354526 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2268465014(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2236520076_gshared (InternalEnumerator_1_t1346955310 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2236520076_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1346955310 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1346955310 *>(__this + 1);
	InternalEnumerator_1__ctor_m2236520076(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2518871420_gshared (InternalEnumerator_1_t1346955310 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2518871420_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1346955310 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1346955310 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2518871420(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2491718776_gshared (InternalEnumerator_1_t1346955310 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t488203048  L_0 = InternalEnumerator_1_get_Current_m966604531((InternalEnumerator_1_t1346955310 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t488203048  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2491718776_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1346955310 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1346955310 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2491718776(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1132415659_gshared (InternalEnumerator_1_t1346955310 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1132415659_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1346955310 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1346955310 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1132415659(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2724656708_gshared (InternalEnumerator_1_t1346955310 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2724656708_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1346955310 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1346955310 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2724656708(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m966604531_MetadataUsageId;
extern "C"  KeyValuePair_2_t488203048  InternalEnumerator_1_get_Current_m966604531_gshared (InternalEnumerator_1_t1346955310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m966604531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t488203048  L_8 = ((  KeyValuePair_2_t488203048  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t488203048  InternalEnumerator_1_get_Current_m966604531_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1346955310 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1346955310 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m966604531(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1903821871_gshared (InternalEnumerator_1_t3924046188 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1903821871_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3924046188 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3924046188 *>(__this + 1);
	InternalEnumerator_1__ctor_m1903821871(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1072714343_gshared (InternalEnumerator_1_t3924046188 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1072714343_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3924046188 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3924046188 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1072714343(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3490397643_gshared (InternalEnumerator_1_t3924046188 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3065293926  L_0 = InternalEnumerator_1_get_Current_m2800610598((InternalEnumerator_1_t3924046188 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3065293926  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3490397643_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3924046188 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3924046188 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3490397643(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4135396136_gshared (InternalEnumerator_1_t3924046188 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4135396136_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3924046188 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3924046188 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4135396136(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4138226531_gshared (InternalEnumerator_1_t3924046188 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4138226531_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3924046188 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3924046188 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4138226531(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2800610598_MetadataUsageId;
extern "C"  KeyValuePair_2_t3065293926  InternalEnumerator_1_get_Current_m2800610598_gshared (InternalEnumerator_1_t3924046188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2800610598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3065293926  L_8 = ((  KeyValuePair_2_t3065293926  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3065293926  InternalEnumerator_1_get_Current_m2800610598_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3924046188 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3924046188 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2800610598(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2593458328_gshared (InternalEnumerator_1_t2563600722 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2593458328_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2563600722 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2563600722 *>(__this + 1);
	InternalEnumerator_1__ctor_m2593458328(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m771306000_gshared (InternalEnumerator_1_t2563600722 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m771306000_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2563600722 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2563600722 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m771306000(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4241081862_gshared (InternalEnumerator_1_t2563600722 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1704848460  L_0 = InternalEnumerator_1_get_Current_m358423711((InternalEnumerator_1_t2563600722 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1704848460  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4241081862_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2563600722 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2563600722 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4241081862(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m454656587_gshared (InternalEnumerator_1_t2563600722 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m454656587_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2563600722 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2563600722 *>(__this + 1);
	InternalEnumerator_1_Dispose_m454656587(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m177039344_gshared (InternalEnumerator_1_t2563600722 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m177039344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2563600722 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2563600722 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m177039344(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m358423711_MetadataUsageId;
extern "C"  KeyValuePair_2_t1704848460  InternalEnumerator_1_get_Current_m358423711_gshared (InternalEnumerator_1_t2563600722 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m358423711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1704848460  L_8 = ((  KeyValuePair_2_t1704848460  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1704848460  InternalEnumerator_1_get_Current_m358423711_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2563600722 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2563600722 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m358423711(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4273781177_gshared (InternalEnumerator_1_t2311033343 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4273781177_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2311033343 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2311033343 *>(__this + 1);
	InternalEnumerator_1__ctor_m4273781177(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2900799557_gshared (InternalEnumerator_1_t2311033343 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2900799557_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2311033343 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2311033343 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2900799557(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m850308093_gshared (InternalEnumerator_1_t2311033343 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1452281081  L_0 = InternalEnumerator_1_get_Current_m4201080002((InternalEnumerator_1_t2311033343 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1452281081  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m850308093_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2311033343 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2311033343 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m850308093(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1810311080_gshared (InternalEnumerator_1_t2311033343 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1810311080_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2311033343 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2311033343 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1810311080(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m186401949_gshared (InternalEnumerator_1_t2311033343 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m186401949_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2311033343 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2311033343 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m186401949(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4201080002_MetadataUsageId;
extern "C"  KeyValuePair_2_t1452281081  InternalEnumerator_1_get_Current_m4201080002_gshared (InternalEnumerator_1_t2311033343 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4201080002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1452281081  L_8 = ((  KeyValuePair_2_t1452281081  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1452281081  InternalEnumerator_1_get_Current_m4201080002_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2311033343 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2311033343 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4201080002(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2770994422_gshared (InternalEnumerator_1_t3963639764 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2770994422_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3963639764 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3963639764 *>(__this + 1);
	InternalEnumerator_1__ctor_m2770994422(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1056733090_gshared (InternalEnumerator_1_t3963639764 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1056733090_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3963639764 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3963639764 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1056733090(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2023646268_gshared (InternalEnumerator_1_t3963639764 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3104887502  L_0 = InternalEnumerator_1_get_Current_m4277271057((InternalEnumerator_1_t3963639764 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3104887502  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2023646268_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3963639764 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3963639764 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2023646268(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3249713521_gshared (InternalEnumerator_1_t3963639764 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3249713521_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3963639764 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3963639764 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3249713521(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3360821254_gshared (InternalEnumerator_1_t3963639764 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3360821254_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3963639764 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3963639764 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3360821254(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4277271057_MetadataUsageId;
extern "C"  KeyValuePair_2_t3104887502  InternalEnumerator_1_get_Current_m4277271057_gshared (InternalEnumerator_1_t3963639764 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4277271057_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3104887502  L_8 = ((  KeyValuePair_2_t3104887502  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3104887502  InternalEnumerator_1_get_Current_m4277271057_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3963639764 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3963639764 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4277271057(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m439810834_gshared (InternalEnumerator_1_t3582009740 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m439810834_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	InternalEnumerator_1__ctor_m439810834(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1090540230_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1090540230_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1090540230(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3088751576_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	{
		Link_t2723257478  L_0 = InternalEnumerator_1_get_Current_m3444791149((InternalEnumerator_1_t3582009740 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t2723257478  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3088751576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3088751576(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m296683029_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m296683029_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	InternalEnumerator_1_Dispose_m296683029(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1994485778_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1994485778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1994485778(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3444791149_MetadataUsageId;
extern "C"  Link_t2723257478  InternalEnumerator_1_get_Current_m3444791149_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3444791149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t2723257478  L_8 = ((  Link_t2723257478  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t2723257478  InternalEnumerator_1_get_Current_m3444791149_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3444791149(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m488579894_gshared (InternalEnumerator_1_t2881283523 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m488579894_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	InternalEnumerator_1__ctor_m488579894(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m403454978_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m403454978_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m403454978(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4259662004_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	{
		Slot_t2022531261  L_0 = InternalEnumerator_1_get_Current_m198513457((InternalEnumerator_1_t2881283523 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t2022531261  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4259662004_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4259662004(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m802528953_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m802528953_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	InternalEnumerator_1_Dispose_m802528953(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3278167302_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3278167302_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3278167302(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m198513457_MetadataUsageId;
extern "C"  Slot_t2022531261  InternalEnumerator_1_get_Current_m198513457_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m198513457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t2022531261  L_8 = ((  Slot_t2022531261  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Slot_t2022531261  InternalEnumerator_1_get_Current_m198513457_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m198513457(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1405610577_gshared (InternalEnumerator_1_t3126312864 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1405610577_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3126312864 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3126312864 *>(__this + 1);
	InternalEnumerator_1__ctor_m1405610577(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3237341717_gshared (InternalEnumerator_1_t3126312864 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3237341717_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3126312864 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3126312864 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3237341717(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3600601141_gshared (InternalEnumerator_1_t3126312864 * __this, const MethodInfo* method)
{
	{
		Slot_t2267560602  L_0 = InternalEnumerator_1_get_Current_m4193726352((InternalEnumerator_1_t3126312864 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t2267560602  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3600601141_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3126312864 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3126312864 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3600601141(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2337194690_gshared (InternalEnumerator_1_t3126312864 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2337194690_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3126312864 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3126312864 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2337194690(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3476348493_gshared (InternalEnumerator_1_t3126312864 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3476348493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3126312864 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3126312864 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3476348493(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4193726352_MetadataUsageId;
extern "C"  Slot_t2267560602  InternalEnumerator_1_get_Current_m4193726352_gshared (InternalEnumerator_1_t3126312864 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4193726352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t2267560602  L_8 = ((  Slot_t2267560602  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Slot_t2267560602  InternalEnumerator_1_get_Current_m4193726352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3126312864 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3126312864 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4193726352(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m245588437_gshared (InternalEnumerator_1_t1551957931 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m245588437_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	InternalEnumerator_1__ctor_m245588437(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2174159777_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2174159777_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2174159777(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3315293493_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = InternalEnumerator_1_get_Current_m4279678504((InternalEnumerator_1_t1551957931 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DateTime_t693205669  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3315293493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3315293493(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3383574608_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3383574608_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3383574608(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3300932033_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3300932033_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3300932033(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4279678504_MetadataUsageId;
extern "C"  DateTime_t693205669  InternalEnumerator_1_get_Current_m4279678504_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4279678504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DateTime_t693205669  L_8 = ((  DateTime_t693205669  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DateTime_t693205669  InternalEnumerator_1_get_Current_m4279678504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4279678504(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4150855019_gshared (InternalEnumerator_1_t1583453339 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4150855019_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	InternalEnumerator_1__ctor_m4150855019(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963130955_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963130955_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963130955(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1025729343_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	{
		Decimal_t724701077  L_0 = InternalEnumerator_1_get_Current_m245025210((InternalEnumerator_1_t1583453339 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Decimal_t724701077  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1025729343_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1025729343(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3407567388_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3407567388_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3407567388(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4134231455_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4134231455_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4134231455(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m245025210_MetadataUsageId;
extern "C"  Decimal_t724701077  InternalEnumerator_1_get_Current_m245025210_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m245025210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Decimal_t724701077  L_8 = ((  Decimal_t724701077  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Decimal_t724701077  InternalEnumerator_1_get_Current_m245025210_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m245025210(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3589241961_gshared (InternalEnumerator_1_t641800647 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3589241961_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	InternalEnumerator_1__ctor_m3589241961(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3194282029_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3194282029_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3194282029(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2842514953_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	{
		double L_0 = InternalEnumerator_1_get_Current_m1389169756((InternalEnumerator_1_t641800647 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2842514953_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2842514953(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3578333724_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3578333724_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3578333724(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m83303365_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m83303365_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m83303365(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1389169756_MetadataUsageId;
extern "C"  double InternalEnumerator_1_get_Current_m1389169756_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1389169756_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		double L_8 = ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  double InternalEnumerator_1_get_Current_m1389169756_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1389169756(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m557239862_gshared (InternalEnumerator_1_t605030880 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m557239862_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	InternalEnumerator_1__ctor_m557239862(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m487832594_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m487832594_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m487832594(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2068723842_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	{
		int16_t L_0 = InternalEnumerator_1_get_Current_m3259181373((InternalEnumerator_1_t605030880 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2068723842_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2068723842(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2743309309_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2743309309_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2743309309(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4274987126_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4274987126_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4274987126(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3259181373_MetadataUsageId;
extern "C"  int16_t InternalEnumerator_1_get_Current_m3259181373_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3259181373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int16_t L_8 = ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int16_t InternalEnumerator_1_get_Current_m3259181373_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3259181373(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m504913220_gshared (InternalEnumerator_1_t2930629710 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m504913220_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	InternalEnumerator_1__ctor_m504913220(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2726857860_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2726857860_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2726857860(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1527025224_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m10285187((InternalEnumerator_1_t2930629710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1527025224_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1527025224(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3393096515_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3393096515_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3393096515(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3679487948_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3679487948_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3679487948(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m10285187_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m10285187_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m10285187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m10285187_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m10285187(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2597133905_gshared (InternalEnumerator_1_t1767830299 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2597133905_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	InternalEnumerator_1__ctor_m2597133905(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2144409197_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2144409197_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2144409197(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2545039741_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = InternalEnumerator_1_get_Current_m2415979394((InternalEnumerator_1_t1767830299 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2545039741_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2545039741(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m307741520_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m307741520_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	InternalEnumerator_1_Dispose_m307741520(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1683120485_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1683120485_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1683120485(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2415979394_MetadataUsageId;
extern "C"  int64_t InternalEnumerator_1_get_Current_m2415979394_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2415979394_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int64_t L_8 = ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int64_t InternalEnumerator_1_get_Current_m2415979394_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2415979394(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1648185761_gshared (InternalEnumerator_1_t3362812871 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1648185761_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	InternalEnumerator_1__ctor_m1648185761(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1809507733_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1809507733_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1809507733(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m127456009_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = InternalEnumerator_1_get_Current_m1706492988((InternalEnumerator_1_t3362812871 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m127456009_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m127456009(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3933737284_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3933737284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3933737284(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2720582493_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2720582493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2720582493(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1706492988_MetadataUsageId;
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m1706492988_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1706492988_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		IntPtr_t L_8 = ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m1706492988_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1706492988(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m853313801_gshared (InternalEnumerator_1_t3548201557 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m853313801_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	InternalEnumerator_1__ctor_m853313801(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = InternalEnumerator_1_get_Current_m3206960238((InternalEnumerator_1_t3548201557 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1636767846_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1636767846_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1636767846(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1047150157_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1047150157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1047150157(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3206960238_MetadataUsageId;
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m3206960238_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3206960238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m3206960238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3206960238(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m492779768_gshared (InternalEnumerator_1_t952909805 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m492779768_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	InternalEnumerator_1__ctor_m492779768(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2494446096_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2494446096_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2494446096(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1322273508_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = InternalEnumerator_1_get_Current_m1089848479((InternalEnumerator_1_t952909805 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeNamedArgument_t94157543  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1322273508_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1322273508(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m238246335_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m238246335_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	InternalEnumerator_1_Dispose_m238246335(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1548080384_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1548080384_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1548080384(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1089848479_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t94157543  InternalEnumerator_1_get_Current_m1089848479_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1089848479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeNamedArgument_t94157543  L_8 = ((  CustomAttributeNamedArgument_t94157543  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  CustomAttributeNamedArgument_t94157543  InternalEnumerator_1_get_Current_m1089848479_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1089848479(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m821424641_gshared (InternalEnumerator_1_t2356950176 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m821424641_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	InternalEnumerator_1__ctor_m821424641(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2624612805_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2624612805_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2624612805(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2315179333_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = InternalEnumerator_1_get_Current_m1047712960((InternalEnumerator_1_t2356950176 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeTypedArgument_t1498197914  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2315179333_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2315179333(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4038440306_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4038440306_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4038440306(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2904932349_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2904932349_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2904932349(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1047712960_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t1498197914  InternalEnumerator_1_get_Current_m1047712960_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1047712960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeTypedArgument_t1498197914  L_8 = ((  CustomAttributeTypedArgument_t1498197914  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  CustomAttributeTypedArgument_t1498197914  InternalEnumerator_1_get_Current_m1047712960_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1047712960(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3323962057_gshared (InternalEnumerator_1_t275897710 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3323962057_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	InternalEnumerator_1__ctor_m3323962057(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589050037_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589050037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589050037(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4242639349_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	{
		LabelData_t3712112744  L_0 = InternalEnumerator_1_get_Current_m3922357178((InternalEnumerator_1_t275897710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LabelData_t3712112744  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4242639349_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4242639349(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m549215360_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m549215360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	InternalEnumerator_1_Dispose_m549215360(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3389738333_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3389738333_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3389738333(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3922357178_MetadataUsageId;
extern "C"  LabelData_t3712112744  InternalEnumerator_1_get_Current_m3922357178_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3922357178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelData_t3712112744  L_8 = ((  LabelData_t3712112744  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  LabelData_t3712112744  InternalEnumerator_1_get_Current_m3922357178_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3922357178(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3228997263_gshared (InternalEnumerator_1_t654694480 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3228997263_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	InternalEnumerator_1__ctor_m3228997263(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3279821511_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3279821511_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3279821511(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1597849391_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	{
		LabelFixup_t4090909514  L_0 = InternalEnumerator_1_get_Current_m2468740214((InternalEnumerator_1_t654694480 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LabelFixup_t4090909514  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1597849391_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1597849391(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3927915442_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3927915442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3927915442(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4292005299_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4292005299_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4292005299(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2468740214_MetadataUsageId;
extern "C"  LabelFixup_t4090909514  InternalEnumerator_1_get_Current_m2468740214_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2468740214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelFixup_t4090909514  L_8 = ((  LabelFixup_t4090909514  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  LabelFixup_t4090909514  InternalEnumerator_1_get_Current_m2468740214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2468740214(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3387972470_gshared (InternalEnumerator_1_t1008311600 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3387972470_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	InternalEnumerator_1__ctor_m3387972470(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m651165750_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m651165750_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m651165750(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3239681450_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	{
		ILTokenInfo_t149559338  L_0 = InternalEnumerator_1_get_Current_m3296972783((InternalEnumerator_1_t1008311600 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ILTokenInfo_t149559338  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3239681450_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3239681450(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2056889175_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2056889175_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2056889175(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1590907854_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1590907854_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1590907854(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3296972783_MetadataUsageId;
extern "C"  ILTokenInfo_t149559338  InternalEnumerator_1_get_Current_m3296972783_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3296972783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ILTokenInfo_t149559338  L_8 = ((  ILTokenInfo_t149559338  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ILTokenInfo_t149559338  InternalEnumerator_1_get_Current_m3296972783_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3296972783(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2890018883_gshared (InternalEnumerator_1_t2679387182 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2890018883_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	InternalEnumerator_1__ctor_m2890018883(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107040235_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107040235_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107040235(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851415307_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	{
		ParameterModifier_t1820634920  L_0 = InternalEnumerator_1_get_Current_m4083613828((InternalEnumerator_1_t2679387182 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ParameterModifier_t1820634920  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851415307_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851415307(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3952699776_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3952699776_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3952699776(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1594563423_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1594563423_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1594563423(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4083613828_MetadataUsageId;
extern "C"  ParameterModifier_t1820634920  InternalEnumerator_1_get_Current_m4083613828_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4083613828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ParameterModifier_t1820634920  L_8 = ((  ParameterModifier_t1820634920  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ParameterModifier_t1820634920  InternalEnumerator_1_get_Current_m4083613828_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4083613828(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1182539814_gshared (InternalEnumerator_1_t1191988411 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1182539814_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1191988411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1191988411 *>(__this + 1);
	InternalEnumerator_1__ctor_m1182539814(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2821513122_gshared (InternalEnumerator_1_t1191988411 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2821513122_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1191988411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1191988411 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2821513122(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1049770044_gshared (InternalEnumerator_1_t1191988411 * __this, const MethodInfo* method)
{
	{
		ResourceCacheItem_t333236149  L_0 = InternalEnumerator_1_get_Current_m789289033((InternalEnumerator_1_t1191988411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResourceCacheItem_t333236149  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1049770044_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1191988411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1191988411 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1049770044(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4175113225_gshared (InternalEnumerator_1_t1191988411 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4175113225_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1191988411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1191988411 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4175113225(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2302237510_gshared (InternalEnumerator_1_t1191988411 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2302237510_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1191988411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1191988411 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2302237510(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m789289033_MetadataUsageId;
extern "C"  ResourceCacheItem_t333236149  InternalEnumerator_1_get_Current_m789289033_gshared (InternalEnumerator_1_t1191988411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m789289033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceCacheItem_t333236149  L_8 = ((  ResourceCacheItem_t333236149  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ResourceCacheItem_t333236149  InternalEnumerator_1_get_Current_m789289033_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1191988411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1191988411 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m789289033(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1336720787_gshared (InternalEnumerator_1_t496834202 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1336720787_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t496834202 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t496834202 *>(__this + 1);
	InternalEnumerator_1__ctor_m1336720787(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2116079299_gshared (InternalEnumerator_1_t496834202 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2116079299_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t496834202 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t496834202 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2116079299(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4023948615_gshared (InternalEnumerator_1_t496834202 * __this, const MethodInfo* method)
{
	{
		ResourceInfo_t3933049236  L_0 = InternalEnumerator_1_get_Current_m4154059426((InternalEnumerator_1_t496834202 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResourceInfo_t3933049236  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4023948615_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t496834202 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t496834202 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4023948615(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1794459540_gshared (InternalEnumerator_1_t496834202 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1794459540_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t496834202 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t496834202 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1794459540(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2576139351_gshared (InternalEnumerator_1_t496834202 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2576139351_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t496834202 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t496834202 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2576139351(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4154059426_MetadataUsageId;
extern "C"  ResourceInfo_t3933049236  InternalEnumerator_1_get_Current_m4154059426_gshared (InternalEnumerator_1_t496834202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4154059426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceInfo_t3933049236  L_8 = ((  ResourceInfo_t3933049236  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ResourceInfo_t3933049236  InternalEnumerator_1_get_Current_m4154059426_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t496834202 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t496834202 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4154059426(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4063293236_gshared (InternalEnumerator_1_t999961858 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4063293236_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t999961858 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t999961858 *>(__this + 1);
	InternalEnumerator_1__ctor_m4063293236(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1561424184_gshared (InternalEnumerator_1_t999961858 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1561424184_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t999961858 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t999961858 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1561424184(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4088899688_gshared (InternalEnumerator_1_t999961858 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m2286118957((InternalEnumerator_1_t999961858 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4088899688_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t999961858 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t999961858 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4088899688(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1020222893_gshared (InternalEnumerator_1_t999961858 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1020222893_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t999961858 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t999961858 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1020222893(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1686633972_gshared (InternalEnumerator_1_t999961858 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1686633972_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t999961858 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t999961858 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1686633972(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2286118957_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m2286118957_gshared (InternalEnumerator_1_t999961858 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2286118957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m2286118957_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t999961858 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t999961858 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2286118957(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2108401677_gshared (InternalEnumerator_1_t1313169811 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2108401677_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	InternalEnumerator_1__ctor_m2108401677(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4085710193_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4085710193_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4085710193(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2607490481_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	{
		int8_t L_0 = InternalEnumerator_1_get_Current_m314017974((InternalEnumerator_1_t1313169811 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2607490481_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2607490481(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1676985532_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1676985532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1676985532(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3984801393_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3984801393_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3984801393(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m314017974_MetadataUsageId;
extern "C"  int8_t InternalEnumerator_1_get_Current_m314017974_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m314017974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int8_t L_8 = ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int8_t InternalEnumerator_1_get_Current_m314017974_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m314017974(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m655778553_gshared (InternalEnumerator_1_t842163687 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m655778553_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	InternalEnumerator_1__ctor_m655778553(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2198960685_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2198960685_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2198960685(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576641073_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	{
		X509ChainStatus_t4278378721  L_0 = InternalEnumerator_1_get_Current_m1550231132((InternalEnumerator_1_t842163687 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		X509ChainStatus_t4278378721  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576641073_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576641073(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3671580532_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3671580532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3671580532(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1869236997_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1869236997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1869236997(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1550231132_MetadataUsageId;
extern "C"  X509ChainStatus_t4278378721  InternalEnumerator_1_get_Current_m1550231132_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1550231132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		X509ChainStatus_t4278378721  L_8 = ((  X509ChainStatus_t4278378721  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  X509ChainStatus_t4278378721  InternalEnumerator_1_get_Current_m1550231132_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1550231132(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2314640734_gshared (InternalEnumerator_1_t2935262194 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2314640734_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	InternalEnumerator_1__ctor_m2314640734(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	{
		float L_0 = InternalEnumerator_1_get_Current_m727737343((InternalEnumerator_1_t2935262194 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2195973811_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2195973811_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2195973811(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m580128774_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m580128774_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m580128774(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m727737343_MetadataUsageId;
extern "C"  float InternalEnumerator_1_get_Current_m727737343_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m727737343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		float L_8 = ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  float InternalEnumerator_1_get_Current_m727737343_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m727737343(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1240086835_gshared (InternalEnumerator_1_t3583626735 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1240086835_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	InternalEnumerator_1__ctor_m1240086835(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3826378355_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3826378355_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3826378355(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2035754659_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	{
		Mark_t2724874473  L_0 = InternalEnumerator_1_get_Current_m575280506((InternalEnumerator_1_t3583626735 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Mark_t2724874473  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2035754659_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2035754659(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3744916110_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3744916110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3744916110(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1741571735_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1741571735_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1741571735(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m575280506_MetadataUsageId;
extern "C"  Mark_t2724874473  InternalEnumerator_1_get_Current_m575280506_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m575280506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Mark_t2724874473  L_8 = ((  Mark_t2724874473  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Mark_t2724874473  InternalEnumerator_1_get_Current_m575280506_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m575280506(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2189699457_gshared (InternalEnumerator_1_t4289011211 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2189699457_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	InternalEnumerator_1__ctor_m2189699457(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	{
		TimeSpan_t3430258949  L_0 = InternalEnumerator_1_get_Current_m3411759116((InternalEnumerator_1_t4289011211 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TimeSpan_t3430258949  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3838127340_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3838127340_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3838127340(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.TimeSpan>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1674480765_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1674480765_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1674480765(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.TimeSpan>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3411759116_MetadataUsageId;
extern "C"  TimeSpan_t3430258949  InternalEnumerator_1_get_Current_m3411759116_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3411759116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TimeSpan_t3430258949  L_8 = ((  TimeSpan_t3430258949  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TimeSpan_t3430258949  InternalEnumerator_1_get_Current_m3411759116_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3411759116(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2981879621_gshared (InternalEnumerator_1_t1845634873 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2981879621_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	InternalEnumerator_1__ctor_m2981879621(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = InternalEnumerator_1_get_Current_m3179981210((InternalEnumerator_1_t1845634873 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1824402698_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1824402698_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1824402698(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2809569305_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2809569305_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2809569305(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3179981210_MetadataUsageId;
extern "C"  uint16_t InternalEnumerator_1_get_Current_m3179981210_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3179981210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint16_t L_8 = ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint16_t InternalEnumerator_1_get_Current_m3179981210_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3179981210(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m691972083_gshared (InternalEnumerator_1_t3008434283 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m691972083_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	InternalEnumerator_1__ctor_m691972083(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = InternalEnumerator_1_get_Current_m2198364332((InternalEnumerator_1_t3008434283 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2620838688_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2620838688_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2620838688(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m470170271_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m470170271_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m470170271(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2198364332_MetadataUsageId;
extern "C"  uint32_t InternalEnumerator_1_get_Current_m2198364332_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2198364332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint32_t L_8 = ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint32_t InternalEnumerator_1_get_Current_m2198364332_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2198364332(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3084132532_gshared (InternalEnumerator_1_t3767949176 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3084132532_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	InternalEnumerator_1__ctor_m3084132532(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = InternalEnumerator_1_get_Current_m35328337((InternalEnumerator_1_t3767949176 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3642485841_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3642485841_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3642485841(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2954283444_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2954283444_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2954283444(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m35328337_MetadataUsageId;
extern "C"  uint64_t InternalEnumerator_1_get_Current_m35328337_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m35328337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint64_t L_8 = ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint64_t InternalEnumerator_1_get_Current_m35328337_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m35328337(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
