﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_SteamVRSystem
struct SDK_SteamVRSystem_t50103670;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_SteamVRSystem::.ctor()
extern "C"  void SDK_SteamVRSystem__ctor_m1089462996 (SDK_SteamVRSystem_t50103670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
