﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Action
struct Action_t1836998693;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Events/Action::.ctor()
extern "C"  void Action__ctor_m1964921010 (Action_t1836998693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Events/Action::set_enabled(System.Boolean)
extern "C"  void Action_set_enabled_m3847833355 (Action_t1836998693 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
