﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.RC_Car_Controller
struct RC_Car_Controller_t2574376713;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"

// System.Void VRTK.Examples.RC_Car_Controller::.ctor()
extern "C"  void RC_Car_Controller__ctor_m966188738 (RC_Car_Controller_t2574376713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car_Controller::Start()
extern "C"  void RC_Car_Controller_Start_m4277970894 (RC_Car_Controller_t2574376713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car_Controller::DoTouchpadAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void RC_Car_Controller_DoTouchpadAxisChanged_m3092997630 (RC_Car_Controller_t2574376713 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car_Controller::DoTriggerAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void RC_Car_Controller_DoTriggerAxisChanged_m2855257486 (RC_Car_Controller_t2574376713 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car_Controller::DoTouchpadTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void RC_Car_Controller_DoTouchpadTouchEnd_m3359094729 (RC_Car_Controller_t2574376713 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car_Controller::DoTriggerReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void RC_Car_Controller_DoTriggerReleased_m2189250834 (RC_Car_Controller_t2574376713 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car_Controller::DoCarReset(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void RC_Car_Controller_DoCarReset_m829960288 (RC_Car_Controller_t2574376713 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
