﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ControllerHoverHighlight
struct ControllerHoverHighlight_t2644472;
// SteamVR_RenderModel
struct SteamVR_RenderModel_t2905485978;
// Valve.VR.InteractionSystem.Interactable
struct Interactable_t1274046986;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SteamVR_RenderModel2905485978.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Inter1274046986.h"

// System.Void Valve.VR.InteractionSystem.ControllerHoverHighlight::.ctor()
extern "C"  void ControllerHoverHighlight__ctor_m1533545084 (ControllerHoverHighlight_t2644472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHoverHighlight::Start()
extern "C"  void ControllerHoverHighlight_Start_m83019796 (ControllerHoverHighlight_t2644472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHoverHighlight::Awake()
extern "C"  void ControllerHoverHighlight_Awake_m1208758477 (ControllerHoverHighlight_t2644472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHoverHighlight::OnEnable()
extern "C"  void ControllerHoverHighlight_OnEnable_m3431194260 (ControllerHoverHighlight_t2644472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHoverHighlight::OnDisable()
extern "C"  void ControllerHoverHighlight_OnDisable_m867161021 (ControllerHoverHighlight_t2644472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHoverHighlight::OnHandInitialized(System.Int32)
extern "C"  void ControllerHoverHighlight_OnHandInitialized_m1763182963 (ControllerHoverHighlight_t2644472 * __this, int32_t ___deviceIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHoverHighlight::OnRenderModelLoaded(SteamVR_RenderModel,System.Boolean)
extern "C"  void ControllerHoverHighlight_OnRenderModelLoaded_m2532371040 (ControllerHoverHighlight_t2644472 * __this, SteamVR_RenderModel_t2905485978 * ___renderModel0, bool ___success1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHoverHighlight::OnParentHandHoverBegin(Valve.VR.InteractionSystem.Interactable)
extern "C"  void ControllerHoverHighlight_OnParentHandHoverBegin_m2714805414 (ControllerHoverHighlight_t2644472 * __this, Interactable_t1274046986 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHoverHighlight::OnParentHandHoverEnd(Valve.VR.InteractionSystem.Interactable)
extern "C"  void ControllerHoverHighlight_OnParentHandHoverEnd_m4114332758 (ControllerHoverHighlight_t2644472 * __this, Interactable_t1274046986 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHoverHighlight::OnParentHandInputFocusAcquired()
extern "C"  void ControllerHoverHighlight_OnParentHandInputFocusAcquired_m1783293474 (ControllerHoverHighlight_t2644472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHoverHighlight::OnParentHandInputFocusLost()
extern "C"  void ControllerHoverHighlight_OnParentHandInputFocusLost_m3485565040 (ControllerHoverHighlight_t2644472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHoverHighlight::ShowHighlight()
extern "C"  void ControllerHoverHighlight_ShowHighlight_m1837623743 (ControllerHoverHighlight_t2644472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHoverHighlight::HideHighlight()
extern "C"  void ControllerHoverHighlight_HideHighlight_m2057800896 (ControllerHoverHighlight_t2644472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
