﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_ReleaseInputFocus
struct _ReleaseInputFocus_t580725753;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_ReleaseInputFocus::.ctor(System.Object,System.IntPtr)
extern "C"  void _ReleaseInputFocus__ctor_m3491302770 (_ReleaseInputFocus_t580725753 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_ReleaseInputFocus::Invoke()
extern "C"  void _ReleaseInputFocus_Invoke_m3814479200 (_ReleaseInputFocus_t580725753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_ReleaseInputFocus::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ReleaseInputFocus_BeginInvoke_m2096849111 (_ReleaseInputFocus_t580725753 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_ReleaseInputFocus::EndInvoke(System.IAsyncResult)
extern "C"  void _ReleaseInputFocus_EndInvoke_m3880172036 (_ReleaseInputFocus_t580725753 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
