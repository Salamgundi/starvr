﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_TeleportDisableOnHeadsetCollision/<EnableAtEndOfFrame>c__Iterator0
struct U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_TeleportDisableOnHeadsetCollision/<EnableAtEndOfFrame>c__Iterator0::.ctor()
extern "C"  void U3CEnableAtEndOfFrameU3Ec__Iterator0__ctor_m3879803257 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_TeleportDisableOnHeadsetCollision/<EnableAtEndOfFrame>c__Iterator0::MoveNext()
extern "C"  bool U3CEnableAtEndOfFrameU3Ec__Iterator0_MoveNext_m3885006675 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_TeleportDisableOnHeadsetCollision/<EnableAtEndOfFrame>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEnableAtEndOfFrameU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1995470611 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_TeleportDisableOnHeadsetCollision/<EnableAtEndOfFrame>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEnableAtEndOfFrameU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1214637771 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TeleportDisableOnHeadsetCollision/<EnableAtEndOfFrame>c__Iterator0::Dispose()
extern "C"  void U3CEnableAtEndOfFrameU3Ec__Iterator0_Dispose_m1764058052 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TeleportDisableOnHeadsetCollision/<EnableAtEndOfFrame>c__Iterator0::Reset()
extern "C"  void U3CEnableAtEndOfFrameU3Ec__Iterator0_Reset_m1030118538 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
