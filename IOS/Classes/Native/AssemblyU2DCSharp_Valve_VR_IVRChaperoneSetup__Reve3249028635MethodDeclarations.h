﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_RevertWorkingCopy
struct _RevertWorkingCopy_t3249028635;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_RevertWorkingCopy::.ctor(System.Object,System.IntPtr)
extern "C"  void _RevertWorkingCopy__ctor_m406192472 (_RevertWorkingCopy_t3249028635 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperoneSetup/_RevertWorkingCopy::Invoke()
extern "C"  void _RevertWorkingCopy_Invoke_m355524386 (_RevertWorkingCopy_t3249028635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_RevertWorkingCopy::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _RevertWorkingCopy_BeginInvoke_m3907714229 (_RevertWorkingCopy_t3249028635 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperoneSetup/_RevertWorkingCopy::EndInvoke(System.IAsyncResult)
extern "C"  void _RevertWorkingCopy_EndInvoke_m2787937742 (_RevertWorkingCopy_t3249028635 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
