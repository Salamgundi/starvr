﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.InteractableHoverEvents
struct InteractableHoverEvents_t1346270787;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"

// System.Void Valve.VR.InteractionSystem.InteractableHoverEvents::.ctor()
extern "C"  void InteractableHoverEvents__ctor_m243818119 (InteractableHoverEvents_t1346270787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InteractableHoverEvents::OnHandHoverBegin()
extern "C"  void InteractableHoverEvents_OnHandHoverBegin_m1804897260 (InteractableHoverEvents_t1346270787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InteractableHoverEvents::OnHandHoverEnd()
extern "C"  void InteractableHoverEvents_OnHandHoverEnd_m3056375344 (InteractableHoverEvents_t1346270787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InteractableHoverEvents::OnAttachedToHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void InteractableHoverEvents_OnAttachedToHand_m3470502538 (InteractableHoverEvents_t1346270787 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InteractableHoverEvents::OnDetachedFromHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void InteractableHoverEvents_OnDetachedFromHand_m642947511 (InteractableHoverEvents_t1346270787 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
