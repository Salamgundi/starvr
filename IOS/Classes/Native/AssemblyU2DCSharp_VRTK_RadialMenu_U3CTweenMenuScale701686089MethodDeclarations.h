﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.RadialMenu/<TweenMenuScale>c__Iterator0
struct U3CTweenMenuScaleU3Ec__Iterator0_t701686089;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.RadialMenu/<TweenMenuScale>c__Iterator0::.ctor()
extern "C"  void U3CTweenMenuScaleU3Ec__Iterator0__ctor_m3774173504 (U3CTweenMenuScaleU3Ec__Iterator0_t701686089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.RadialMenu/<TweenMenuScale>c__Iterator0::MoveNext()
extern "C"  bool U3CTweenMenuScaleU3Ec__Iterator0_MoveNext_m453010744 (U3CTweenMenuScaleU3Ec__Iterator0_t701686089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.RadialMenu/<TweenMenuScale>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTweenMenuScaleU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4196227884 (U3CTweenMenuScaleU3Ec__Iterator0_t701686089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.RadialMenu/<TweenMenuScale>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTweenMenuScaleU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m438977764 (U3CTweenMenuScaleU3Ec__Iterator0_t701686089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu/<TweenMenuScale>c__Iterator0::Dispose()
extern "C"  void U3CTweenMenuScaleU3Ec__Iterator0_Dispose_m3630952899 (U3CTweenMenuScaleU3Ec__Iterator0_t701686089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu/<TweenMenuScale>c__Iterator0::Reset()
extern "C"  void U3CTweenMenuScaleU3Ec__Iterator0_Reset_m2092544665 (U3CTweenMenuScaleU3Ec__Iterator0_t701686089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
