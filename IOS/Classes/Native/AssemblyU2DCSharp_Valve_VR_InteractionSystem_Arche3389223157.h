﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.ArcheryTarget
struct  ArcheryTarget_t3389223157  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.ArcheryTarget::onTakeDamage
	UnityEvent_t408735097 * ___onTakeDamage_2;
	// System.Boolean Valve.VR.InteractionSystem.ArcheryTarget::onceOnly
	bool ___onceOnly_3;
	// UnityEngine.Transform Valve.VR.InteractionSystem.ArcheryTarget::targetCenter
	Transform_t3275118058 * ___targetCenter_4;
	// UnityEngine.Transform Valve.VR.InteractionSystem.ArcheryTarget::baseTransform
	Transform_t3275118058 * ___baseTransform_5;
	// UnityEngine.Transform Valve.VR.InteractionSystem.ArcheryTarget::fallenDownTransform
	Transform_t3275118058 * ___fallenDownTransform_6;
	// System.Single Valve.VR.InteractionSystem.ArcheryTarget::fallTime
	float ___fallTime_7;
	// System.Boolean Valve.VR.InteractionSystem.ArcheryTarget::targetEnabled
	bool ___targetEnabled_9;

public:
	inline static int32_t get_offset_of_onTakeDamage_2() { return static_cast<int32_t>(offsetof(ArcheryTarget_t3389223157, ___onTakeDamage_2)); }
	inline UnityEvent_t408735097 * get_onTakeDamage_2() const { return ___onTakeDamage_2; }
	inline UnityEvent_t408735097 ** get_address_of_onTakeDamage_2() { return &___onTakeDamage_2; }
	inline void set_onTakeDamage_2(UnityEvent_t408735097 * value)
	{
		___onTakeDamage_2 = value;
		Il2CppCodeGenWriteBarrier(&___onTakeDamage_2, value);
	}

	inline static int32_t get_offset_of_onceOnly_3() { return static_cast<int32_t>(offsetof(ArcheryTarget_t3389223157, ___onceOnly_3)); }
	inline bool get_onceOnly_3() const { return ___onceOnly_3; }
	inline bool* get_address_of_onceOnly_3() { return &___onceOnly_3; }
	inline void set_onceOnly_3(bool value)
	{
		___onceOnly_3 = value;
	}

	inline static int32_t get_offset_of_targetCenter_4() { return static_cast<int32_t>(offsetof(ArcheryTarget_t3389223157, ___targetCenter_4)); }
	inline Transform_t3275118058 * get_targetCenter_4() const { return ___targetCenter_4; }
	inline Transform_t3275118058 ** get_address_of_targetCenter_4() { return &___targetCenter_4; }
	inline void set_targetCenter_4(Transform_t3275118058 * value)
	{
		___targetCenter_4 = value;
		Il2CppCodeGenWriteBarrier(&___targetCenter_4, value);
	}

	inline static int32_t get_offset_of_baseTransform_5() { return static_cast<int32_t>(offsetof(ArcheryTarget_t3389223157, ___baseTransform_5)); }
	inline Transform_t3275118058 * get_baseTransform_5() const { return ___baseTransform_5; }
	inline Transform_t3275118058 ** get_address_of_baseTransform_5() { return &___baseTransform_5; }
	inline void set_baseTransform_5(Transform_t3275118058 * value)
	{
		___baseTransform_5 = value;
		Il2CppCodeGenWriteBarrier(&___baseTransform_5, value);
	}

	inline static int32_t get_offset_of_fallenDownTransform_6() { return static_cast<int32_t>(offsetof(ArcheryTarget_t3389223157, ___fallenDownTransform_6)); }
	inline Transform_t3275118058 * get_fallenDownTransform_6() const { return ___fallenDownTransform_6; }
	inline Transform_t3275118058 ** get_address_of_fallenDownTransform_6() { return &___fallenDownTransform_6; }
	inline void set_fallenDownTransform_6(Transform_t3275118058 * value)
	{
		___fallenDownTransform_6 = value;
		Il2CppCodeGenWriteBarrier(&___fallenDownTransform_6, value);
	}

	inline static int32_t get_offset_of_fallTime_7() { return static_cast<int32_t>(offsetof(ArcheryTarget_t3389223157, ___fallTime_7)); }
	inline float get_fallTime_7() const { return ___fallTime_7; }
	inline float* get_address_of_fallTime_7() { return &___fallTime_7; }
	inline void set_fallTime_7(float value)
	{
		___fallTime_7 = value;
	}

	inline static int32_t get_offset_of_targetEnabled_9() { return static_cast<int32_t>(offsetof(ArcheryTarget_t3389223157, ___targetEnabled_9)); }
	inline bool get_targetEnabled_9() const { return ___targetEnabled_9; }
	inline bool* get_address_of_targetEnabled_9() { return &___targetEnabled_9; }
	inline void set_targetEnabled_9(bool value)
	{
		___targetEnabled_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
