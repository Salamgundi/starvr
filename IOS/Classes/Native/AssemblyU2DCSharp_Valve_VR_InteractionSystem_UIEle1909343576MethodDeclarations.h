﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.UIElement
struct UIElement_t1909343576;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"

// System.Void Valve.VR.InteractionSystem.UIElement::.ctor()
extern "C"  void UIElement__ctor_m1279562174 (UIElement_t1909343576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.UIElement::Awake()
extern "C"  void UIElement_Awake_m1913130233 (UIElement_t1909343576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.UIElement::OnHandHoverBegin(Valve.VR.InteractionSystem.Hand)
extern "C"  void UIElement_OnHandHoverBegin_m1947343001 (UIElement_t1909343576 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.UIElement::OnHandHoverEnd(Valve.VR.InteractionSystem.Hand)
extern "C"  void UIElement_OnHandHoverEnd_m2161565413 (UIElement_t1909343576 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.UIElement::HandHoverUpdate(Valve.VR.InteractionSystem.Hand)
extern "C"  void UIElement_HandHoverUpdate_m243330550 (UIElement_t1909343576 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.UIElement::OnButtonClick()
extern "C"  void UIElement_OnButtonClick_m2964345189 (UIElement_t1909343576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
