﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_HipTracking
struct VRTK_HipTracking_t3141756816;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_HipTracking::.ctor()
extern "C"  void VRTK_HipTracking__ctor_m3765875750 (VRTK_HipTracking_t3141756816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HipTracking::Awake()
extern "C"  void VRTK_HipTracking_Awake_m3992815525 (VRTK_HipTracking_t3141756816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HipTracking::Update()
extern "C"  void VRTK_HipTracking_Update_m450435873 (VRTK_HipTracking_t3141756816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
