﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Menu_Container_Object_Colors
struct Menu_Container_Object_Colors_t1737726098;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void VRTK.Examples.Menu_Container_Object_Colors::.ctor()
extern "C"  void Menu_Container_Object_Colors__ctor_m1812548603 (Menu_Container_Object_Colors_t1737726098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Menu_Container_Object_Colors::SetSelectedColor(UnityEngine.Color)
extern "C"  void Menu_Container_Object_Colors_SetSelectedColor_m578984527 (Menu_Container_Object_Colors_t1737726098 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Menu_Container_Object_Colors::Start()
extern "C"  void Menu_Container_Object_Colors_Start_m1697423311 (Menu_Container_Object_Colors_t1737726098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
