﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t4220419316;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Mesh
struct Mesh_t1356156583;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_SkinQuality1832760476.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"

// UnityEngine.Transform[] UnityEngine.SkinnedMeshRenderer::get_bones()
extern "C"  TransformU5BU5D_t3764228911* SkinnedMeshRenderer_get_bones_m236777101 (SkinnedMeshRenderer_t4220419316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::set_bones(UnityEngine.Transform[])
extern "C"  void SkinnedMeshRenderer_set_bones_m4069982372 (SkinnedMeshRenderer_t4220419316 * __this, TransformU5BU5D_t3764228911* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.SkinnedMeshRenderer::get_rootBone()
extern "C"  Transform_t3275118058 * SkinnedMeshRenderer_get_rootBone_m3389111566 (SkinnedMeshRenderer_t4220419316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::set_rootBone(UnityEngine.Transform)
extern "C"  void SkinnedMeshRenderer_set_rootBone_m557723121 (SkinnedMeshRenderer_t4220419316 * __this, Transform_t3275118058 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SkinQuality UnityEngine.SkinnedMeshRenderer::get_quality()
extern "C"  int32_t SkinnedMeshRenderer_get_quality_m188576701 (SkinnedMeshRenderer_t4220419316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::set_quality(UnityEngine.SkinQuality)
extern "C"  void SkinnedMeshRenderer_set_quality_m2213829412 (SkinnedMeshRenderer_t4220419316 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.SkinnedMeshRenderer::get_sharedMesh()
extern "C"  Mesh_t1356156583 * SkinnedMeshRenderer_get_sharedMesh_m2813560771 (SkinnedMeshRenderer_t4220419316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::set_sharedMesh(UnityEngine.Mesh)
extern "C"  void SkinnedMeshRenderer_set_sharedMesh_m1030183452 (SkinnedMeshRenderer_t4220419316 * __this, Mesh_t1356156583 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SkinnedMeshRenderer::get_updateWhenOffscreen()
extern "C"  bool SkinnedMeshRenderer_get_updateWhenOffscreen_m3148276870 (SkinnedMeshRenderer_t4220419316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::set_updateWhenOffscreen(System.Boolean)
extern "C"  void SkinnedMeshRenderer_set_updateWhenOffscreen_m116240815 (SkinnedMeshRenderer_t4220419316 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::SetBlendShapeWeight(System.Int32,System.Single)
extern "C"  void SkinnedMeshRenderer_SetBlendShapeWeight_m1743301399 (SkinnedMeshRenderer_t4220419316 * __this, int32_t ___index0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
