﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_Valve_VR_IntersectionMaskRectang2372341025.h"
#include "AssemblyU2DCSharp_Valve_VR_IntersectionMaskCircle_2044509700.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.VROverlayIntersectionMaskPrimitive_Data_t
struct  VROverlayIntersectionMaskPrimitive_Data_t_t1248365778 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// Valve.VR.IntersectionMaskRectangle_t Valve.VR.VROverlayIntersectionMaskPrimitive_Data_t::m_Rectangle
			IntersectionMaskRectangle_t_t2372341025  ___m_Rectangle_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			IntersectionMaskRectangle_t_t2372341025  ___m_Rectangle_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// Valve.VR.IntersectionMaskCircle_t Valve.VR.VROverlayIntersectionMaskPrimitive_Data_t::m_Circle
			IntersectionMaskCircle_t_t2044509700  ___m_Circle_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			IntersectionMaskCircle_t_t2044509700  ___m_Circle_1_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_m_Rectangle_0() { return static_cast<int32_t>(offsetof(VROverlayIntersectionMaskPrimitive_Data_t_t1248365778, ___m_Rectangle_0)); }
	inline IntersectionMaskRectangle_t_t2372341025  get_m_Rectangle_0() const { return ___m_Rectangle_0; }
	inline IntersectionMaskRectangle_t_t2372341025 * get_address_of_m_Rectangle_0() { return &___m_Rectangle_0; }
	inline void set_m_Rectangle_0(IntersectionMaskRectangle_t_t2372341025  value)
	{
		___m_Rectangle_0 = value;
	}

	inline static int32_t get_offset_of_m_Circle_1() { return static_cast<int32_t>(offsetof(VROverlayIntersectionMaskPrimitive_Data_t_t1248365778, ___m_Circle_1)); }
	inline IntersectionMaskCircle_t_t2044509700  get_m_Circle_1() const { return ___m_Circle_1; }
	inline IntersectionMaskCircle_t_t2044509700 * get_address_of_m_Circle_1() { return &___m_Circle_1; }
	inline void set_m_Circle_1(IntersectionMaskCircle_t_t2044509700  value)
	{
		___m_Circle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
