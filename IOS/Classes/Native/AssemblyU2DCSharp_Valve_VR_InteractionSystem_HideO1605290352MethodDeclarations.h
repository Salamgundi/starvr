﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.HideOnHandFocusLost
struct HideOnHandFocusLost_t1605290352;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"

// System.Void Valve.VR.InteractionSystem.HideOnHandFocusLost::.ctor()
extern "C"  void HideOnHandFocusLost__ctor_m2441867854 (HideOnHandFocusLost_t1605290352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.HideOnHandFocusLost::OnHandFocusLost(Valve.VR.InteractionSystem.Hand)
extern "C"  void HideOnHandFocusLost_OnHandFocusLost_m4251026288 (HideOnHandFocusLost_t1605290352 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
