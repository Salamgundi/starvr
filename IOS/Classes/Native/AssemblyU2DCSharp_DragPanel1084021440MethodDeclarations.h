﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DragPanel
struct DragPanel_t1084021440;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void DragPanel::.ctor()
extern "C"  void DragPanel__ctor_m816219637 (DragPanel_t1084021440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPanel::Awake()
extern "C"  void DragPanel_Awake_m884329612 (DragPanel_t1084021440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPanel::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DragPanel_OnPointerDown_m1519165881 (DragPanel_t1084021440 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPanel::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DragPanel_OnDrag_m1140396260 (DragPanel_t1084021440 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPanel::ClampToWindow()
extern "C"  void DragPanel_ClampToWindow_m138969157 (DragPanel_t1084021440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
