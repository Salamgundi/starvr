﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.PhysicMaterial
struct PhysicMaterial_t578636151;
// Valve.VR.InteractionSystem.SoundPlayOneshot
struct SoundPlayOneshot_t1703214483;
// Valve.VR.InteractionSystem.PlaySound
struct PlaySound_t165629647;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.Arrow
struct  Arrow_t2932383743  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.ParticleSystem Valve.VR.InteractionSystem.Arrow::glintParticle
	ParticleSystem_t3394631041 * ___glintParticle_2;
	// UnityEngine.Rigidbody Valve.VR.InteractionSystem.Arrow::arrowHeadRB
	Rigidbody_t4233889191 * ___arrowHeadRB_3;
	// UnityEngine.Rigidbody Valve.VR.InteractionSystem.Arrow::shaftRB
	Rigidbody_t4233889191 * ___shaftRB_4;
	// UnityEngine.PhysicMaterial Valve.VR.InteractionSystem.Arrow::targetPhysMaterial
	PhysicMaterial_t578636151 * ___targetPhysMaterial_5;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.Arrow::prevPosition
	Vector3_t2243707580  ___prevPosition_6;
	// UnityEngine.Quaternion Valve.VR.InteractionSystem.Arrow::prevRotation
	Quaternion_t4030073918  ___prevRotation_7;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.Arrow::prevVelocity
	Vector3_t2243707580  ___prevVelocity_8;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.Arrow::prevHeadPosition
	Vector3_t2243707580  ___prevHeadPosition_9;
	// Valve.VR.InteractionSystem.SoundPlayOneshot Valve.VR.InteractionSystem.Arrow::fireReleaseSound
	SoundPlayOneshot_t1703214483 * ___fireReleaseSound_10;
	// Valve.VR.InteractionSystem.SoundPlayOneshot Valve.VR.InteractionSystem.Arrow::airReleaseSound
	SoundPlayOneshot_t1703214483 * ___airReleaseSound_11;
	// Valve.VR.InteractionSystem.SoundPlayOneshot Valve.VR.InteractionSystem.Arrow::hitTargetSound
	SoundPlayOneshot_t1703214483 * ___hitTargetSound_12;
	// Valve.VR.InteractionSystem.PlaySound Valve.VR.InteractionSystem.Arrow::hitGroundSound
	PlaySound_t165629647 * ___hitGroundSound_13;
	// System.Boolean Valve.VR.InteractionSystem.Arrow::inFlight
	bool ___inFlight_14;
	// System.Boolean Valve.VR.InteractionSystem.Arrow::released
	bool ___released_15;
	// System.Boolean Valve.VR.InteractionSystem.Arrow::hasSpreadFire
	bool ___hasSpreadFire_16;
	// System.Int32 Valve.VR.InteractionSystem.Arrow::travelledFrames
	int32_t ___travelledFrames_17;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Arrow::scaleParentObject
	GameObject_t1756533147 * ___scaleParentObject_18;

public:
	inline static int32_t get_offset_of_glintParticle_2() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___glintParticle_2)); }
	inline ParticleSystem_t3394631041 * get_glintParticle_2() const { return ___glintParticle_2; }
	inline ParticleSystem_t3394631041 ** get_address_of_glintParticle_2() { return &___glintParticle_2; }
	inline void set_glintParticle_2(ParticleSystem_t3394631041 * value)
	{
		___glintParticle_2 = value;
		Il2CppCodeGenWriteBarrier(&___glintParticle_2, value);
	}

	inline static int32_t get_offset_of_arrowHeadRB_3() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___arrowHeadRB_3)); }
	inline Rigidbody_t4233889191 * get_arrowHeadRB_3() const { return ___arrowHeadRB_3; }
	inline Rigidbody_t4233889191 ** get_address_of_arrowHeadRB_3() { return &___arrowHeadRB_3; }
	inline void set_arrowHeadRB_3(Rigidbody_t4233889191 * value)
	{
		___arrowHeadRB_3 = value;
		Il2CppCodeGenWriteBarrier(&___arrowHeadRB_3, value);
	}

	inline static int32_t get_offset_of_shaftRB_4() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___shaftRB_4)); }
	inline Rigidbody_t4233889191 * get_shaftRB_4() const { return ___shaftRB_4; }
	inline Rigidbody_t4233889191 ** get_address_of_shaftRB_4() { return &___shaftRB_4; }
	inline void set_shaftRB_4(Rigidbody_t4233889191 * value)
	{
		___shaftRB_4 = value;
		Il2CppCodeGenWriteBarrier(&___shaftRB_4, value);
	}

	inline static int32_t get_offset_of_targetPhysMaterial_5() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___targetPhysMaterial_5)); }
	inline PhysicMaterial_t578636151 * get_targetPhysMaterial_5() const { return ___targetPhysMaterial_5; }
	inline PhysicMaterial_t578636151 ** get_address_of_targetPhysMaterial_5() { return &___targetPhysMaterial_5; }
	inline void set_targetPhysMaterial_5(PhysicMaterial_t578636151 * value)
	{
		___targetPhysMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___targetPhysMaterial_5, value);
	}

	inline static int32_t get_offset_of_prevPosition_6() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___prevPosition_6)); }
	inline Vector3_t2243707580  get_prevPosition_6() const { return ___prevPosition_6; }
	inline Vector3_t2243707580 * get_address_of_prevPosition_6() { return &___prevPosition_6; }
	inline void set_prevPosition_6(Vector3_t2243707580  value)
	{
		___prevPosition_6 = value;
	}

	inline static int32_t get_offset_of_prevRotation_7() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___prevRotation_7)); }
	inline Quaternion_t4030073918  get_prevRotation_7() const { return ___prevRotation_7; }
	inline Quaternion_t4030073918 * get_address_of_prevRotation_7() { return &___prevRotation_7; }
	inline void set_prevRotation_7(Quaternion_t4030073918  value)
	{
		___prevRotation_7 = value;
	}

	inline static int32_t get_offset_of_prevVelocity_8() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___prevVelocity_8)); }
	inline Vector3_t2243707580  get_prevVelocity_8() const { return ___prevVelocity_8; }
	inline Vector3_t2243707580 * get_address_of_prevVelocity_8() { return &___prevVelocity_8; }
	inline void set_prevVelocity_8(Vector3_t2243707580  value)
	{
		___prevVelocity_8 = value;
	}

	inline static int32_t get_offset_of_prevHeadPosition_9() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___prevHeadPosition_9)); }
	inline Vector3_t2243707580  get_prevHeadPosition_9() const { return ___prevHeadPosition_9; }
	inline Vector3_t2243707580 * get_address_of_prevHeadPosition_9() { return &___prevHeadPosition_9; }
	inline void set_prevHeadPosition_9(Vector3_t2243707580  value)
	{
		___prevHeadPosition_9 = value;
	}

	inline static int32_t get_offset_of_fireReleaseSound_10() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___fireReleaseSound_10)); }
	inline SoundPlayOneshot_t1703214483 * get_fireReleaseSound_10() const { return ___fireReleaseSound_10; }
	inline SoundPlayOneshot_t1703214483 ** get_address_of_fireReleaseSound_10() { return &___fireReleaseSound_10; }
	inline void set_fireReleaseSound_10(SoundPlayOneshot_t1703214483 * value)
	{
		___fireReleaseSound_10 = value;
		Il2CppCodeGenWriteBarrier(&___fireReleaseSound_10, value);
	}

	inline static int32_t get_offset_of_airReleaseSound_11() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___airReleaseSound_11)); }
	inline SoundPlayOneshot_t1703214483 * get_airReleaseSound_11() const { return ___airReleaseSound_11; }
	inline SoundPlayOneshot_t1703214483 ** get_address_of_airReleaseSound_11() { return &___airReleaseSound_11; }
	inline void set_airReleaseSound_11(SoundPlayOneshot_t1703214483 * value)
	{
		___airReleaseSound_11 = value;
		Il2CppCodeGenWriteBarrier(&___airReleaseSound_11, value);
	}

	inline static int32_t get_offset_of_hitTargetSound_12() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___hitTargetSound_12)); }
	inline SoundPlayOneshot_t1703214483 * get_hitTargetSound_12() const { return ___hitTargetSound_12; }
	inline SoundPlayOneshot_t1703214483 ** get_address_of_hitTargetSound_12() { return &___hitTargetSound_12; }
	inline void set_hitTargetSound_12(SoundPlayOneshot_t1703214483 * value)
	{
		___hitTargetSound_12 = value;
		Il2CppCodeGenWriteBarrier(&___hitTargetSound_12, value);
	}

	inline static int32_t get_offset_of_hitGroundSound_13() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___hitGroundSound_13)); }
	inline PlaySound_t165629647 * get_hitGroundSound_13() const { return ___hitGroundSound_13; }
	inline PlaySound_t165629647 ** get_address_of_hitGroundSound_13() { return &___hitGroundSound_13; }
	inline void set_hitGroundSound_13(PlaySound_t165629647 * value)
	{
		___hitGroundSound_13 = value;
		Il2CppCodeGenWriteBarrier(&___hitGroundSound_13, value);
	}

	inline static int32_t get_offset_of_inFlight_14() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___inFlight_14)); }
	inline bool get_inFlight_14() const { return ___inFlight_14; }
	inline bool* get_address_of_inFlight_14() { return &___inFlight_14; }
	inline void set_inFlight_14(bool value)
	{
		___inFlight_14 = value;
	}

	inline static int32_t get_offset_of_released_15() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___released_15)); }
	inline bool get_released_15() const { return ___released_15; }
	inline bool* get_address_of_released_15() { return &___released_15; }
	inline void set_released_15(bool value)
	{
		___released_15 = value;
	}

	inline static int32_t get_offset_of_hasSpreadFire_16() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___hasSpreadFire_16)); }
	inline bool get_hasSpreadFire_16() const { return ___hasSpreadFire_16; }
	inline bool* get_address_of_hasSpreadFire_16() { return &___hasSpreadFire_16; }
	inline void set_hasSpreadFire_16(bool value)
	{
		___hasSpreadFire_16 = value;
	}

	inline static int32_t get_offset_of_travelledFrames_17() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___travelledFrames_17)); }
	inline int32_t get_travelledFrames_17() const { return ___travelledFrames_17; }
	inline int32_t* get_address_of_travelledFrames_17() { return &___travelledFrames_17; }
	inline void set_travelledFrames_17(int32_t value)
	{
		___travelledFrames_17 = value;
	}

	inline static int32_t get_offset_of_scaleParentObject_18() { return static_cast<int32_t>(offsetof(Arrow_t2932383743, ___scaleParentObject_18)); }
	inline GameObject_t1756533147 * get_scaleParentObject_18() const { return ___scaleParentObject_18; }
	inline GameObject_t1756533147 ** get_address_of_scaleParentObject_18() { return &___scaleParentObject_18; }
	inline void set_scaleParentObject_18(GameObject_t1756533147 * value)
	{
		___scaleParentObject_18 = value;
		Il2CppCodeGenWriteBarrier(&___scaleParentObject_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
