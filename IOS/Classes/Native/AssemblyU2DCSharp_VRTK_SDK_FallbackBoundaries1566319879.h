﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_SDK_BaseBoundaries1766380066.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.SDK_FallbackBoundaries
struct  SDK_FallbackBoundaries_t1566319879  : public SDK_BaseBoundaries_t1766380066
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
