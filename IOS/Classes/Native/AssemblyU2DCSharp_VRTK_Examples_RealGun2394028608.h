﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.Examples.RealGun_Slide
struct RealGun_Slide_t2998553338;
// VRTK.Examples.RealGun_SafetySwitch
struct RealGun_SafetySwitch_t476099707;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.Collider
struct Collider_t3497673348;
// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;

#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.RealGun
struct  RealGun_t2394028608  : public VRTK_InteractableObject_t2604188111
{
public:
	// System.Single VRTK.Examples.RealGun::bulletSpeed
	float ___bulletSpeed_45;
	// System.Single VRTK.Examples.RealGun::bulletLife
	float ___bulletLife_46;
	// UnityEngine.GameObject VRTK.Examples.RealGun::bullet
	GameObject_t1756533147 * ___bullet_47;
	// UnityEngine.GameObject VRTK.Examples.RealGun::trigger
	GameObject_t1756533147 * ___trigger_48;
	// VRTK.Examples.RealGun_Slide VRTK.Examples.RealGun::slide
	RealGun_Slide_t2998553338 * ___slide_49;
	// VRTK.Examples.RealGun_SafetySwitch VRTK.Examples.RealGun::safetySwitch
	RealGun_SafetySwitch_t476099707 * ___safetySwitch_50;
	// UnityEngine.Rigidbody VRTK.Examples.RealGun::slideRigidbody
	Rigidbody_t4233889191 * ___slideRigidbody_51;
	// UnityEngine.Collider VRTK.Examples.RealGun::slideCollider
	Collider_t3497673348 * ___slideCollider_52;
	// UnityEngine.Rigidbody VRTK.Examples.RealGun::safetySwitchRigidbody
	Rigidbody_t4233889191 * ___safetySwitchRigidbody_53;
	// UnityEngine.Collider VRTK.Examples.RealGun::safetySwitchCollider
	Collider_t3497673348 * ___safetySwitchCollider_54;
	// VRTK.VRTK_ControllerEvents VRTK.Examples.RealGun::controllerEvents
	VRTK_ControllerEvents_t3225224819 * ___controllerEvents_55;
	// VRTK.VRTK_ControllerActions VRTK.Examples.RealGun::controllerActions
	VRTK_ControllerActions_t3642353851 * ___controllerActions_56;
	// System.Single VRTK.Examples.RealGun::minTriggerRotation
	float ___minTriggerRotation_57;
	// System.Single VRTK.Examples.RealGun::maxTriggerRotation
	float ___maxTriggerRotation_58;

public:
	inline static int32_t get_offset_of_bulletSpeed_45() { return static_cast<int32_t>(offsetof(RealGun_t2394028608, ___bulletSpeed_45)); }
	inline float get_bulletSpeed_45() const { return ___bulletSpeed_45; }
	inline float* get_address_of_bulletSpeed_45() { return &___bulletSpeed_45; }
	inline void set_bulletSpeed_45(float value)
	{
		___bulletSpeed_45 = value;
	}

	inline static int32_t get_offset_of_bulletLife_46() { return static_cast<int32_t>(offsetof(RealGun_t2394028608, ___bulletLife_46)); }
	inline float get_bulletLife_46() const { return ___bulletLife_46; }
	inline float* get_address_of_bulletLife_46() { return &___bulletLife_46; }
	inline void set_bulletLife_46(float value)
	{
		___bulletLife_46 = value;
	}

	inline static int32_t get_offset_of_bullet_47() { return static_cast<int32_t>(offsetof(RealGun_t2394028608, ___bullet_47)); }
	inline GameObject_t1756533147 * get_bullet_47() const { return ___bullet_47; }
	inline GameObject_t1756533147 ** get_address_of_bullet_47() { return &___bullet_47; }
	inline void set_bullet_47(GameObject_t1756533147 * value)
	{
		___bullet_47 = value;
		Il2CppCodeGenWriteBarrier(&___bullet_47, value);
	}

	inline static int32_t get_offset_of_trigger_48() { return static_cast<int32_t>(offsetof(RealGun_t2394028608, ___trigger_48)); }
	inline GameObject_t1756533147 * get_trigger_48() const { return ___trigger_48; }
	inline GameObject_t1756533147 ** get_address_of_trigger_48() { return &___trigger_48; }
	inline void set_trigger_48(GameObject_t1756533147 * value)
	{
		___trigger_48 = value;
		Il2CppCodeGenWriteBarrier(&___trigger_48, value);
	}

	inline static int32_t get_offset_of_slide_49() { return static_cast<int32_t>(offsetof(RealGun_t2394028608, ___slide_49)); }
	inline RealGun_Slide_t2998553338 * get_slide_49() const { return ___slide_49; }
	inline RealGun_Slide_t2998553338 ** get_address_of_slide_49() { return &___slide_49; }
	inline void set_slide_49(RealGun_Slide_t2998553338 * value)
	{
		___slide_49 = value;
		Il2CppCodeGenWriteBarrier(&___slide_49, value);
	}

	inline static int32_t get_offset_of_safetySwitch_50() { return static_cast<int32_t>(offsetof(RealGun_t2394028608, ___safetySwitch_50)); }
	inline RealGun_SafetySwitch_t476099707 * get_safetySwitch_50() const { return ___safetySwitch_50; }
	inline RealGun_SafetySwitch_t476099707 ** get_address_of_safetySwitch_50() { return &___safetySwitch_50; }
	inline void set_safetySwitch_50(RealGun_SafetySwitch_t476099707 * value)
	{
		___safetySwitch_50 = value;
		Il2CppCodeGenWriteBarrier(&___safetySwitch_50, value);
	}

	inline static int32_t get_offset_of_slideRigidbody_51() { return static_cast<int32_t>(offsetof(RealGun_t2394028608, ___slideRigidbody_51)); }
	inline Rigidbody_t4233889191 * get_slideRigidbody_51() const { return ___slideRigidbody_51; }
	inline Rigidbody_t4233889191 ** get_address_of_slideRigidbody_51() { return &___slideRigidbody_51; }
	inline void set_slideRigidbody_51(Rigidbody_t4233889191 * value)
	{
		___slideRigidbody_51 = value;
		Il2CppCodeGenWriteBarrier(&___slideRigidbody_51, value);
	}

	inline static int32_t get_offset_of_slideCollider_52() { return static_cast<int32_t>(offsetof(RealGun_t2394028608, ___slideCollider_52)); }
	inline Collider_t3497673348 * get_slideCollider_52() const { return ___slideCollider_52; }
	inline Collider_t3497673348 ** get_address_of_slideCollider_52() { return &___slideCollider_52; }
	inline void set_slideCollider_52(Collider_t3497673348 * value)
	{
		___slideCollider_52 = value;
		Il2CppCodeGenWriteBarrier(&___slideCollider_52, value);
	}

	inline static int32_t get_offset_of_safetySwitchRigidbody_53() { return static_cast<int32_t>(offsetof(RealGun_t2394028608, ___safetySwitchRigidbody_53)); }
	inline Rigidbody_t4233889191 * get_safetySwitchRigidbody_53() const { return ___safetySwitchRigidbody_53; }
	inline Rigidbody_t4233889191 ** get_address_of_safetySwitchRigidbody_53() { return &___safetySwitchRigidbody_53; }
	inline void set_safetySwitchRigidbody_53(Rigidbody_t4233889191 * value)
	{
		___safetySwitchRigidbody_53 = value;
		Il2CppCodeGenWriteBarrier(&___safetySwitchRigidbody_53, value);
	}

	inline static int32_t get_offset_of_safetySwitchCollider_54() { return static_cast<int32_t>(offsetof(RealGun_t2394028608, ___safetySwitchCollider_54)); }
	inline Collider_t3497673348 * get_safetySwitchCollider_54() const { return ___safetySwitchCollider_54; }
	inline Collider_t3497673348 ** get_address_of_safetySwitchCollider_54() { return &___safetySwitchCollider_54; }
	inline void set_safetySwitchCollider_54(Collider_t3497673348 * value)
	{
		___safetySwitchCollider_54 = value;
		Il2CppCodeGenWriteBarrier(&___safetySwitchCollider_54, value);
	}

	inline static int32_t get_offset_of_controllerEvents_55() { return static_cast<int32_t>(offsetof(RealGun_t2394028608, ___controllerEvents_55)); }
	inline VRTK_ControllerEvents_t3225224819 * get_controllerEvents_55() const { return ___controllerEvents_55; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_controllerEvents_55() { return &___controllerEvents_55; }
	inline void set_controllerEvents_55(VRTK_ControllerEvents_t3225224819 * value)
	{
		___controllerEvents_55 = value;
		Il2CppCodeGenWriteBarrier(&___controllerEvents_55, value);
	}

	inline static int32_t get_offset_of_controllerActions_56() { return static_cast<int32_t>(offsetof(RealGun_t2394028608, ___controllerActions_56)); }
	inline VRTK_ControllerActions_t3642353851 * get_controllerActions_56() const { return ___controllerActions_56; }
	inline VRTK_ControllerActions_t3642353851 ** get_address_of_controllerActions_56() { return &___controllerActions_56; }
	inline void set_controllerActions_56(VRTK_ControllerActions_t3642353851 * value)
	{
		___controllerActions_56 = value;
		Il2CppCodeGenWriteBarrier(&___controllerActions_56, value);
	}

	inline static int32_t get_offset_of_minTriggerRotation_57() { return static_cast<int32_t>(offsetof(RealGun_t2394028608, ___minTriggerRotation_57)); }
	inline float get_minTriggerRotation_57() const { return ___minTriggerRotation_57; }
	inline float* get_address_of_minTriggerRotation_57() { return &___minTriggerRotation_57; }
	inline void set_minTriggerRotation_57(float value)
	{
		___minTriggerRotation_57 = value;
	}

	inline static int32_t get_offset_of_maxTriggerRotation_58() { return static_cast<int32_t>(offsetof(RealGun_t2394028608, ___maxTriggerRotation_58)); }
	inline float get_maxTriggerRotation_58() const { return ___maxTriggerRotation_58; }
	inline float* get_address_of_maxTriggerRotation_58() { return &___maxTriggerRotation_58; }
	inline void set_maxTriggerRotation_58(float value)
	{
		___maxTriggerRotation_58 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
