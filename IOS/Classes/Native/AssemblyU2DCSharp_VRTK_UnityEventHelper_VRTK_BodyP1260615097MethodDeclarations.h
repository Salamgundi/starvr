﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t1260615097;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.UnityEventHelper.VRTK_BodyPhysics_UnityEvents/UnityObjectEvent::.ctor()
extern "C"  void UnityObjectEvent__ctor_m2723078874 (UnityObjectEvent_t1260615097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
