﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ObjectCache
struct VRTK_ObjectCache_t513154459;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_ObjectCache::.ctor()
extern "C"  void VRTK_ObjectCache__ctor_m1184785799 (VRTK_ObjectCache_t513154459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectCache::.cctor()
extern "C"  void VRTK_ObjectCache__cctor_m1672700450 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
