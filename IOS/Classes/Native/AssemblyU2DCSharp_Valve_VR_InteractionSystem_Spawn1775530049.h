﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.SpawnAndAttachToHand
struct  SpawnAndAttachToHand_t1775530049  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.SpawnAndAttachToHand::hand
	Hand_t379716353 * ___hand_2;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.SpawnAndAttachToHand::prefab
	GameObject_t1756533147 * ___prefab_3;

public:
	inline static int32_t get_offset_of_hand_2() { return static_cast<int32_t>(offsetof(SpawnAndAttachToHand_t1775530049, ___hand_2)); }
	inline Hand_t379716353 * get_hand_2() const { return ___hand_2; }
	inline Hand_t379716353 ** get_address_of_hand_2() { return &___hand_2; }
	inline void set_hand_2(Hand_t379716353 * value)
	{
		___hand_2 = value;
		Il2CppCodeGenWriteBarrier(&___hand_2, value);
	}

	inline static int32_t get_offset_of_prefab_3() { return static_cast<int32_t>(offsetof(SpawnAndAttachToHand_t1775530049, ___prefab_3)); }
	inline GameObject_t1756533147 * get_prefab_3() const { return ___prefab_3; }
	inline GameObject_t1756533147 ** get_address_of_prefab_3() { return &___prefab_3; }
	inline void set_prefab_3(GameObject_t1756533147 * value)
	{
		___prefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
