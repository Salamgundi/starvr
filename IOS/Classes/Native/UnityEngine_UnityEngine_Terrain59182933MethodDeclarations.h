﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Terrain
struct Terrain_t59182933;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Terrain59182933.h"

// System.Single UnityEngine.Terrain::SampleHeight(UnityEngine.Vector3)
extern "C"  float Terrain_SampleHeight_m3409068828 (Terrain_t59182933 * __this, Vector3_t2243707580  ___worldPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::INTERNAL_CALL_SampleHeight(UnityEngine.Terrain,UnityEngine.Vector3&)
extern "C"  float Terrain_INTERNAL_CALL_SampleHeight_m1467176371 (Il2CppObject * __this /* static, unused */, Terrain_t59182933 * ___self0, Vector3_t2243707580 * ___worldPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Terrain UnityEngine.Terrain::get_activeTerrain()
extern "C"  Terrain_t59182933 * Terrain_get_activeTerrain_m1918525127 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
