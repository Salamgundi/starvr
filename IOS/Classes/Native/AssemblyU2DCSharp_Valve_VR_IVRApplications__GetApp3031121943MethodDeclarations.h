﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetApplicationsErrorNameFromEnum
struct _GetApplicationsErrorNameFromEnum_t3031121943;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetApplicationsErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetApplicationsErrorNameFromEnum__ctor_m2081743900 (_GetApplicationsErrorNameFromEnum_t3031121943 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRApplications/_GetApplicationsErrorNameFromEnum::Invoke(Valve.VR.EVRApplicationError)
extern "C"  IntPtr_t _GetApplicationsErrorNameFromEnum_Invoke_m1286249700 (_GetApplicationsErrorNameFromEnum_t3031121943 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetApplicationsErrorNameFromEnum::BeginInvoke(Valve.VR.EVRApplicationError,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetApplicationsErrorNameFromEnum_BeginInvoke_m2571034444 (_GetApplicationsErrorNameFromEnum_t3031121943 * __this, int32_t ___error0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRApplications/_GetApplicationsErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetApplicationsErrorNameFromEnum_EndInvoke_m2923709869 (_GetApplicationsErrorNameFromEnum_t3031121943 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
