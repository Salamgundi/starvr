﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayA1475163830.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayA4193138162.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT1399555963.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayT2119049239.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT1179269927.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayT3680071147.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayT1493074241.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT2100297354.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayT2557918150.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayTr579082695.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayTr893699371.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT1749665136.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayT3256239660.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowOverlay733914692.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__HideOverlay2737366627.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__IsOverlayVi1564696099.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetTransfor2483359939.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__PollNextOver131678587.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayI3217580654.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayI3575042602.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayM2826364086.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayM2624726138.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ComputeOver1141587151.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__HandleContr2899449918.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__IsHoverTarg4101716668.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetGamepadFo300021750.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetGamepadF2763810618.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayN1117963895.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__MoveGamepad2016645278.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayT2238656700.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ClearOverla2610332521.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayR3268606321.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayFr598184189.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayTe897552288.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ReleaseNati2842744417.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayT1288689867.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__CreateDashbo637199691.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__IsDashboard2020287181.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__IsActiveDas3144660141.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetDashboar1462353074.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetDashboard876496206.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowDashboa4127025320.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetPrimaryDa812432855.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowKeyboar3095606223.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowKeyboar3006565844.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetKeyboardTe91815223.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__HideKeyboar3483797360.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetKeyboardT443079689.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetKeyboard3695826360.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayI3952572284.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayF2891663584.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__ShowMessage3284759035.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels3420796425.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__LoadRe3622270247.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__FreeRe2139843464.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__LoadTe1786536393.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__FreeTe4051202214.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__LoadTe2681806282.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__LoadIn3518850916.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__FreeTe2385986941.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRen4149685257.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRen3364784497.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetComp763371255.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetCom3462998887.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetCom1474657094.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetCom2860930600.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetComp742926735.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__Render1969881317.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRen3954674309.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRen4216085620.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRRenderModels__GetRend298277168.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRNotifications3935579733.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRNotifications__Creat1905156422.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRNotifications__Remov3701790586.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings254931744.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetSettings293614055.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__Sync2978470277.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__SetBool1033875974.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__SetInt323538080526.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__SetFloat3724747224.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__SetString1793856309.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetBool1034551410.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetInt323538756002.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetFloat3725422668.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__GetString1816180801.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__RemoveSect1493649321.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSettings__RemoveKeyI1836506429.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots1006836234.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__Request1956857133.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__HookScr2804207343.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__GetScre3028991757.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__GetScre1122176780.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__UpdateS2161609358.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__TakeSte3387995749.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRScreenshots__SubmitS3156929320.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRResources1092978558.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRResources__LoadShare4289483331.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRResources__GetResourc915790394.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRSystem1953699154.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRSystem__GetController385670509.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (_SetOverlayAutoCurveDistanceRangeInMeters_t1475163830), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (_GetOverlayAutoCurveDistanceRangeInMeters_t4193138162), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (_SetOverlayTextureColorSpace_t1399555963), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (_GetOverlayTextureColorSpace_t2119049239), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (_SetOverlayTextureBounds_t1179269927), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (_GetOverlayTextureBounds_t3680071147), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (_GetOverlayTransformType_t1493074241), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (_SetOverlayTransformAbsolute_t2100297354), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (_GetOverlayTransformAbsolute_t2557918150), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (_SetOverlayTransformTrackedDeviceRelative_t579082695), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (_GetOverlayTransformTrackedDeviceRelative_t893699371), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (_SetOverlayTransformTrackedDeviceComponent_t1749665136), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (_GetOverlayTransformTrackedDeviceComponent_t3256239660), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (_ShowOverlay_t733914692), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (_HideOverlay_t2737366627), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (_IsOverlayVisible_t1564696099), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (_GetTransformForOverlayCoordinates_t2483359939), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (_PollNextOverlayEvent_t131678587), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (_GetOverlayInputMethod_t3217580654), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (_SetOverlayInputMethod_t3575042602), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (_GetOverlayMouseScale_t2826364086), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (_SetOverlayMouseScale_t2624726138), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (_ComputeOverlayIntersection_t1141587151), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (_HandleControllerOverlayInteractionAsMouse_t2899449918), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (_IsHoverTargetOverlay_t4101716668), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (_GetGamepadFocusOverlay_t300021750), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (_SetGamepadFocusOverlay_t2763810618), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (_SetOverlayNeighbor_t1117963895), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (_MoveGamepadFocusToNeighbor_t2016645278), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (_SetOverlayTexture_t2238656700), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (_ClearOverlayTexture_t2610332521), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (_SetOverlayRaw_t3268606321), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (_SetOverlayFromFile_t598184189), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (_GetOverlayTexture_t897552288), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (_ReleaseNativeOverlayHandle_t2842744417), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (_GetOverlayTextureSize_t1288689867), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (_CreateDashboardOverlay_t637199691), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (_IsDashboardVisible_t2020287181), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (_IsActiveDashboardOverlay_t3144660141), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (_SetDashboardOverlaySceneProcess_t1462353074), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (_GetDashboardOverlaySceneProcess_t876496206), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (_ShowDashboard_t4127025320), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (_GetPrimaryDashboardDevice_t812432855), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (_ShowKeyboard_t3095606223), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (_ShowKeyboardForOverlay_t3006565844), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (_GetKeyboardText_t91815223), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (_HideKeyboard_t3483797360), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (_SetKeyboardTransformAbsolute_t443079689), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (_SetKeyboardPositionForOverlay_t3695826360), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (_SetOverlayIntersectionMask_t3952572284), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (_GetOverlayFlags_t2891663584), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (_ShowMessageOverlay_t3284759035), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (IVRRenderModels_t3420796425)+ sizeof (Il2CppObject), sizeof(IVRRenderModels_t3420796425_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2452[18] = 
{
	IVRRenderModels_t3420796425::get_offset_of_LoadRenderModel_Async_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_FreeRenderModel_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_LoadTexture_Async_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_FreeTexture_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_LoadTextureD3D11_Async_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_LoadIntoTextureD3D11_Async_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_FreeTextureD3D11_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_GetRenderModelName_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_GetRenderModelCount_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_GetComponentCount_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_GetComponentName_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_GetComponentButtonMask_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_GetComponentRenderModelName_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_GetComponentState_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_RenderModelHasComponent_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_GetRenderModelThumbnailURL_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_GetRenderModelOriginalPath_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRRenderModels_t3420796425::get_offset_of_GetRenderModelErrorNameFromEnum_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (_LoadRenderModel_Async_t3622270247), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (_FreeRenderModel_t2139843464), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (_LoadTexture_Async_t1786536393), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (_FreeTexture_t4051202214), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (_LoadTextureD3D11_Async_t2681806282), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (_LoadIntoTextureD3D11_Async_t3518850916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (_FreeTextureD3D11_t2385986941), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (_GetRenderModelName_t4149685257), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (_GetRenderModelCount_t3364784497), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (_GetComponentCount_t763371255), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (_GetComponentName_t3462998887), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (_GetComponentButtonMask_t1474657094), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (_GetComponentRenderModelName_t2860930600), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (_GetComponentState_t742926735), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (_RenderModelHasComponent_t1969881317), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (_GetRenderModelThumbnailURL_t3954674309), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (_GetRenderModelOriginalPath_t4216085620), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (_GetRenderModelErrorNameFromEnum_t298277168), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (IVRNotifications_t3935579733)+ sizeof (Il2CppObject), sizeof(IVRNotifications_t3935579733_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2471[2] = 
{
	IVRNotifications_t3935579733::get_offset_of_CreateNotification_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRNotifications_t3935579733::get_offset_of_RemoveNotification_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (_CreateNotification_t1905156422), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (_RemoveNotification_t3701790586), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (IVRSettings_t254931744)+ sizeof (Il2CppObject), sizeof(IVRSettings_t254931744_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2474[12] = 
{
	IVRSettings_t254931744::get_offset_of_GetSettingsErrorNameFromEnum_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSettings_t254931744::get_offset_of_Sync_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSettings_t254931744::get_offset_of_SetBool_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSettings_t254931744::get_offset_of_SetInt32_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSettings_t254931744::get_offset_of_SetFloat_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSettings_t254931744::get_offset_of_SetString_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSettings_t254931744::get_offset_of_GetBool_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSettings_t254931744::get_offset_of_GetInt32_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSettings_t254931744::get_offset_of_GetFloat_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSettings_t254931744::get_offset_of_GetString_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSettings_t254931744::get_offset_of_RemoveSection_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSettings_t254931744::get_offset_of_RemoveKeyInSection_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (_GetSettingsErrorNameFromEnum_t293614055), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (_Sync_t2978470277), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (_SetBool_t1033875974), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (_SetInt32_t3538080526), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (_SetFloat_t3724747224), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (_SetString_t1793856309), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (_GetBool_t1034551410), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (_GetInt32_t3538756002), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (_GetFloat_t3725422668), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (_GetString_t1816180801), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (_RemoveSection_t1493649321), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (_RemoveKeyInSection_t1836506429), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (IVRScreenshots_t1006836234)+ sizeof (Il2CppObject), sizeof(IVRScreenshots_t1006836234_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2487[7] = 
{
	IVRScreenshots_t1006836234::get_offset_of_RequestScreenshot_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRScreenshots_t1006836234::get_offset_of_HookScreenshot_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRScreenshots_t1006836234::get_offset_of_GetScreenshotPropertyType_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRScreenshots_t1006836234::get_offset_of_GetScreenshotPropertyFilename_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRScreenshots_t1006836234::get_offset_of_UpdateScreenshotProgress_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRScreenshots_t1006836234::get_offset_of_TakeStereoScreenshot_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRScreenshots_t1006836234::get_offset_of_SubmitScreenshot_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (_RequestScreenshot_t1956857133), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (_HookScreenshot_t2804207343), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (_GetScreenshotPropertyType_t3028991757), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (_GetScreenshotPropertyFilename_t1122176780), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (_UpdateScreenshotProgress_t2161609358), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (_TakeStereoScreenshot_t3387995749), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (_SubmitScreenshot_t3156929320), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (IVRResources_t1092978558)+ sizeof (Il2CppObject), sizeof(IVRResources_t1092978558_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2495[2] = 
{
	IVRResources_t1092978558::get_offset_of_LoadSharedResource_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRResources_t1092978558::get_offset_of_GetResourceFullPath_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (_LoadSharedResource_t4289483331), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (_GetResourceFullPath_t915790394), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (CVRSystem_t1953699154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2498[1] = 
{
	CVRSystem_t1953699154::get_offset_of_FnTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (_GetControllerStatePacked_t385670509), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
