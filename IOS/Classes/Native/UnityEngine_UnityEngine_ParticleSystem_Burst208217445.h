﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/Burst
struct  Burst_t208217445 
{
public:
	// System.Single UnityEngine.ParticleSystem/Burst::m_Time
	float ___m_Time_0;
	// System.Int16 UnityEngine.ParticleSystem/Burst::m_MinCount
	int16_t ___m_MinCount_1;
	// System.Int16 UnityEngine.ParticleSystem/Burst::m_MaxCount
	int16_t ___m_MaxCount_2;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Burst_t208217445, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_MinCount_1() { return static_cast<int32_t>(offsetof(Burst_t208217445, ___m_MinCount_1)); }
	inline int16_t get_m_MinCount_1() const { return ___m_MinCount_1; }
	inline int16_t* get_address_of_m_MinCount_1() { return &___m_MinCount_1; }
	inline void set_m_MinCount_1(int16_t value)
	{
		___m_MinCount_1 = value;
	}

	inline static int32_t get_offset_of_m_MaxCount_2() { return static_cast<int32_t>(offsetof(Burst_t208217445, ___m_MaxCount_2)); }
	inline int16_t get_m_MaxCount_2() const { return ___m_MaxCount_2; }
	inline int16_t* get_address_of_m_MaxCount_2() { return &___m_MaxCount_2; }
	inline void set_m_MaxCount_2(int16_t value)
	{
		___m_MaxCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
