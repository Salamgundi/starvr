﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Zipline
struct  Zipline_t3027761465  : public VRTK_InteractableObject_t2604188111
{
public:
	// System.Single VRTK.Examples.Zipline::downStartSpeed
	float ___downStartSpeed_45;
	// System.Single VRTK.Examples.Zipline::acceleration
	float ___acceleration_46;
	// System.Single VRTK.Examples.Zipline::upSpeed
	float ___upSpeed_47;
	// UnityEngine.Transform VRTK.Examples.Zipline::handleEndPosition
	Transform_t3275118058 * ___handleEndPosition_48;
	// UnityEngine.Transform VRTK.Examples.Zipline::handleStartPosition
	Transform_t3275118058 * ___handleStartPosition_49;
	// UnityEngine.GameObject VRTK.Examples.Zipline::handle
	GameObject_t1756533147 * ___handle_50;
	// System.Boolean VRTK.Examples.Zipline::isMoving
	bool ___isMoving_51;
	// System.Boolean VRTK.Examples.Zipline::isMovingDown
	bool ___isMovingDown_52;
	// System.Single VRTK.Examples.Zipline::currentSpeed
	float ___currentSpeed_53;

public:
	inline static int32_t get_offset_of_downStartSpeed_45() { return static_cast<int32_t>(offsetof(Zipline_t3027761465, ___downStartSpeed_45)); }
	inline float get_downStartSpeed_45() const { return ___downStartSpeed_45; }
	inline float* get_address_of_downStartSpeed_45() { return &___downStartSpeed_45; }
	inline void set_downStartSpeed_45(float value)
	{
		___downStartSpeed_45 = value;
	}

	inline static int32_t get_offset_of_acceleration_46() { return static_cast<int32_t>(offsetof(Zipline_t3027761465, ___acceleration_46)); }
	inline float get_acceleration_46() const { return ___acceleration_46; }
	inline float* get_address_of_acceleration_46() { return &___acceleration_46; }
	inline void set_acceleration_46(float value)
	{
		___acceleration_46 = value;
	}

	inline static int32_t get_offset_of_upSpeed_47() { return static_cast<int32_t>(offsetof(Zipline_t3027761465, ___upSpeed_47)); }
	inline float get_upSpeed_47() const { return ___upSpeed_47; }
	inline float* get_address_of_upSpeed_47() { return &___upSpeed_47; }
	inline void set_upSpeed_47(float value)
	{
		___upSpeed_47 = value;
	}

	inline static int32_t get_offset_of_handleEndPosition_48() { return static_cast<int32_t>(offsetof(Zipline_t3027761465, ___handleEndPosition_48)); }
	inline Transform_t3275118058 * get_handleEndPosition_48() const { return ___handleEndPosition_48; }
	inline Transform_t3275118058 ** get_address_of_handleEndPosition_48() { return &___handleEndPosition_48; }
	inline void set_handleEndPosition_48(Transform_t3275118058 * value)
	{
		___handleEndPosition_48 = value;
		Il2CppCodeGenWriteBarrier(&___handleEndPosition_48, value);
	}

	inline static int32_t get_offset_of_handleStartPosition_49() { return static_cast<int32_t>(offsetof(Zipline_t3027761465, ___handleStartPosition_49)); }
	inline Transform_t3275118058 * get_handleStartPosition_49() const { return ___handleStartPosition_49; }
	inline Transform_t3275118058 ** get_address_of_handleStartPosition_49() { return &___handleStartPosition_49; }
	inline void set_handleStartPosition_49(Transform_t3275118058 * value)
	{
		___handleStartPosition_49 = value;
		Il2CppCodeGenWriteBarrier(&___handleStartPosition_49, value);
	}

	inline static int32_t get_offset_of_handle_50() { return static_cast<int32_t>(offsetof(Zipline_t3027761465, ___handle_50)); }
	inline GameObject_t1756533147 * get_handle_50() const { return ___handle_50; }
	inline GameObject_t1756533147 ** get_address_of_handle_50() { return &___handle_50; }
	inline void set_handle_50(GameObject_t1756533147 * value)
	{
		___handle_50 = value;
		Il2CppCodeGenWriteBarrier(&___handle_50, value);
	}

	inline static int32_t get_offset_of_isMoving_51() { return static_cast<int32_t>(offsetof(Zipline_t3027761465, ___isMoving_51)); }
	inline bool get_isMoving_51() const { return ___isMoving_51; }
	inline bool* get_address_of_isMoving_51() { return &___isMoving_51; }
	inline void set_isMoving_51(bool value)
	{
		___isMoving_51 = value;
	}

	inline static int32_t get_offset_of_isMovingDown_52() { return static_cast<int32_t>(offsetof(Zipline_t3027761465, ___isMovingDown_52)); }
	inline bool get_isMovingDown_52() const { return ___isMovingDown_52; }
	inline bool* get_address_of_isMovingDown_52() { return &___isMovingDown_52; }
	inline void set_isMovingDown_52(bool value)
	{
		___isMovingDown_52 = value;
	}

	inline static int32_t get_offset_of_currentSpeed_53() { return static_cast<int32_t>(offsetof(Zipline_t3027761465, ___currentSpeed_53)); }
	inline float get_currentSpeed_53() const { return ___currentSpeed_53; }
	inline float* get_address_of_currentSpeed_53() { return &___currentSpeed_53; }
	inline void set_currentSpeed_53(float value)
	{
		___currentSpeed_53 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
