﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4056035046MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityAction`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m1904882665(__this, ___object0, ___method1, method) ((  void (*) (UnityAction_1_t2479292719 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m2836997866_gshared)(__this, ___object0, ___method1, method)
// System.Void UnityEngine.Events.UnityAction`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::Invoke(T0)
#define UnityAction_1_Invoke_m678792157(__this, ___arg00, method) ((  void (*) (UnityAction_1_t2479292719 *, TeleportMarkerBase_t1112706968 *, const MethodInfo*))UnityAction_1_Invoke_m1279804060_gshared)(__this, ___arg00, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m1130470388(__this, ___arg00, ___callback1, ___object2, method) ((  Il2CppObject * (*) (UnityAction_1_t2479292719 *, TeleportMarkerBase_t1112706968 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))UnityAction_1_BeginInvoke_m3462722079_gshared)(__this, ___arg00, ___callback1, ___object2, method)
// System.Void UnityEngine.Events.UnityAction`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m1370448971(__this, ___result0, method) ((  void (*) (UnityAction_1_t2479292719 *, Il2CppObject *, const MethodInfo*))UnityAction_1_EndInvoke_m2822290096_gshared)(__this, ___result0, method)
