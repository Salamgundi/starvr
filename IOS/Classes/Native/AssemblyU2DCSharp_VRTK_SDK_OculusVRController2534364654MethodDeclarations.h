﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_OculusVRController
struct SDK_OculusVRController_t2534364654;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_OculusVRController::.ctor()
extern "C"  void SDK_OculusVRController__ctor_m1347093594 (SDK_OculusVRController_t2534364654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
