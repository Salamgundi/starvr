﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Openable_Door
struct Openable_Door_t677159015;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.Examples.Openable_Door::.ctor()
extern "C"  void Openable_Door__ctor_m2888444888 (Openable_Door_t677159015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Openable_Door::StartUsing(UnityEngine.GameObject)
extern "C"  void Openable_Door_StartUsing_m2988161052 (Openable_Door_t677159015 * __this, GameObject_t1756533147 * ___usingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Openable_Door::Start()
extern "C"  void Openable_Door_Start_m1115409152 (Openable_Door_t677159015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Openable_Door::Update()
extern "C"  void Openable_Door_Update_m1246726085 (Openable_Door_t677159015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Openable_Door::SetRotation()
extern "C"  void Openable_Door_SetRotation_m4074348282 (Openable_Door_t677159015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Openable_Door::SetDoorRotation(UnityEngine.Vector3)
extern "C"  void Openable_Door_SetDoorRotation_m817246945 (Openable_Door_t677159015 * __this, Vector3_t2243707580  ___interacterPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
