﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRSystem
struct CVRSystem_t1953699154;
// Valve.VR.TrackedDevicePose_t[]
struct TrackedDevicePose_tU5BU5D_t2897272049;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix44_t664273159.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"
#include "AssemblyU2DCSharp_Valve_VR_DistortionCoordinates_t2253454723.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingUniverseOrigin1464400093.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedDeviceClass2121051631.h"
#include "AssemblyU2DCSharp_Valve_VR_EDeviceActivityLevel886867856.h"
#include "AssemblyU2DCSharp_Valve_VR_TrackedDevicePose_t1668551120.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedControllerRole361251409.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedDeviceProperty3226377054.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedPropertyError3340022390.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_t3405266389.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREventType6846875.h"
#include "AssemblyU2DCSharp_Valve_VR_HiddenAreaMesh_t3319190843.h"
#include "AssemblyU2DCSharp_Valve_VR_EHiddenAreaMeshType3068936429.h"
#include "AssemblyU2DCSharp_Valve_VR_VRControllerState_t2504874220.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRButtonId66145412.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRControllerAxisType1358176136.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRFirmwareError2321703066.h"

// System.Void Valve.VR.CVRSystem::.ctor(System.IntPtr)
extern "C"  void CVRSystem__ctor_m3703789475 (CVRSystem_t1953699154 * __this, IntPtr_t ___pInterface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSystem::GetRecommendedRenderTargetSize(System.UInt32&,System.UInt32&)
extern "C"  void CVRSystem_GetRecommendedRenderTargetSize_m2340038796 (CVRSystem_t1953699154 * __this, uint32_t* ___pnWidth0, uint32_t* ___pnHeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix44_t Valve.VR.CVRSystem::GetProjectionMatrix(Valve.VR.EVREye,System.Single,System.Single)
extern "C"  HmdMatrix44_t_t664273159  CVRSystem_GetProjectionMatrix_m487452951 (CVRSystem_t1953699154 * __this, int32_t ___eEye0, float ___fNearZ1, float ___fFarZ2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSystem::GetProjectionRaw(Valve.VR.EVREye,System.Single&,System.Single&,System.Single&,System.Single&)
extern "C"  void CVRSystem_GetProjectionRaw_m2411094756 (CVRSystem_t1953699154 * __this, int32_t ___eEye0, float* ___pfLeft1, float* ___pfRight2, float* ___pfTop3, float* ___pfBottom4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSystem::ComputeDistortion(Valve.VR.EVREye,System.Single,System.Single,Valve.VR.DistortionCoordinates_t&)
extern "C"  bool CVRSystem_ComputeDistortion_m127962398 (CVRSystem_t1953699154 * __this, int32_t ___eEye0, float ___fU1, float ___fV2, DistortionCoordinates_t_t2253454723 * ___pDistortionCoordinates3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix34_t Valve.VR.CVRSystem::GetEyeToHeadTransform(Valve.VR.EVREye)
extern "C"  HmdMatrix34_t_t664273062  CVRSystem_GetEyeToHeadTransform_m4266565500 (CVRSystem_t1953699154 * __this, int32_t ___eEye0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSystem::GetTimeSinceLastVsync(System.Single&,System.UInt64&)
extern "C"  bool CVRSystem_GetTimeSinceLastVsync_m3412198937 (CVRSystem_t1953699154 * __this, float* ___pfSecondsSinceLastVsync0, uint64_t* ___pulFrameCounter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Valve.VR.CVRSystem::GetD3D9AdapterIndex()
extern "C"  int32_t CVRSystem_GetD3D9AdapterIndex_m683850724 (CVRSystem_t1953699154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSystem::GetDXGIOutputInfo(System.Int32&)
extern "C"  void CVRSystem_GetDXGIOutputInfo_m2887753567 (CVRSystem_t1953699154 * __this, int32_t* ___pnAdapterIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSystem::IsDisplayOnDesktop()
extern "C"  bool CVRSystem_IsDisplayOnDesktop_m3485043630 (CVRSystem_t1953699154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSystem::SetDisplayVisibility(System.Boolean)
extern "C"  bool CVRSystem_SetDisplayVisibility_m2651807896 (CVRSystem_t1953699154 * __this, bool ___bIsVisibleOnDesktop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSystem::GetDeviceToAbsoluteTrackingPose(Valve.VR.ETrackingUniverseOrigin,System.Single,Valve.VR.TrackedDevicePose_t[])
extern "C"  void CVRSystem_GetDeviceToAbsoluteTrackingPose_m1371303473 (CVRSystem_t1953699154 * __this, int32_t ___eOrigin0, float ___fPredictedSecondsToPhotonsFromNow1, TrackedDevicePose_tU5BU5D_t2897272049* ___pTrackedDevicePoseArray2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSystem::ResetSeatedZeroPose()
extern "C"  void CVRSystem_ResetSeatedZeroPose_m3311602329 (CVRSystem_t1953699154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix34_t Valve.VR.CVRSystem::GetSeatedZeroPoseToStandingAbsoluteTrackingPose()
extern "C"  HmdMatrix34_t_t664273062  CVRSystem_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_m1771682013 (CVRSystem_t1953699154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix34_t Valve.VR.CVRSystem::GetRawZeroPoseToStandingAbsoluteTrackingPose()
extern "C"  HmdMatrix34_t_t664273062  CVRSystem_GetRawZeroPoseToStandingAbsoluteTrackingPose_m2696183631 (CVRSystem_t1953699154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRSystem::GetSortedTrackedDeviceIndicesOfClass(Valve.VR.ETrackedDeviceClass,System.UInt32[],System.UInt32)
extern "C"  uint32_t CVRSystem_GetSortedTrackedDeviceIndicesOfClass_m1289918872 (CVRSystem_t1953699154 * __this, int32_t ___eTrackedDeviceClass0, UInt32U5BU5D_t59386216* ___punTrackedDeviceIndexArray1, uint32_t ___unRelativeToTrackedDeviceIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EDeviceActivityLevel Valve.VR.CVRSystem::GetTrackedDeviceActivityLevel(System.UInt32)
extern "C"  int32_t CVRSystem_GetTrackedDeviceActivityLevel_m2388576383 (CVRSystem_t1953699154 * __this, uint32_t ___unDeviceId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSystem::ApplyTransform(Valve.VR.TrackedDevicePose_t&,Valve.VR.TrackedDevicePose_t&,Valve.VR.HmdMatrix34_t&)
extern "C"  void CVRSystem_ApplyTransform_m533607545 (CVRSystem_t1953699154 * __this, TrackedDevicePose_t_t1668551120 * ___pOutputPose0, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose1, HmdMatrix34_t_t664273062 * ___pTransform2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRSystem::GetTrackedDeviceIndexForControllerRole(Valve.VR.ETrackedControllerRole)
extern "C"  uint32_t CVRSystem_GetTrackedDeviceIndexForControllerRole_m2314038620 (CVRSystem_t1953699154 * __this, int32_t ___unDeviceType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.ETrackedControllerRole Valve.VR.CVRSystem::GetControllerRoleForTrackedDeviceIndex(System.UInt32)
extern "C"  int32_t CVRSystem_GetControllerRoleForTrackedDeviceIndex_m3474729870 (CVRSystem_t1953699154 * __this, uint32_t ___unDeviceIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.ETrackedDeviceClass Valve.VR.CVRSystem::GetTrackedDeviceClass(System.UInt32)
extern "C"  int32_t CVRSystem_GetTrackedDeviceClass_m4194095201 (CVRSystem_t1953699154 * __this, uint32_t ___unDeviceIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSystem::IsTrackedDeviceConnected(System.UInt32)
extern "C"  bool CVRSystem_IsTrackedDeviceConnected_m317150978 (CVRSystem_t1953699154 * __this, uint32_t ___unDeviceIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSystem::GetBoolTrackedDeviceProperty(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&)
extern "C"  bool CVRSystem_GetBoolTrackedDeviceProperty_m1800979206 (CVRSystem_t1953699154 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.CVRSystem::GetFloatTrackedDeviceProperty(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&)
extern "C"  float CVRSystem_GetFloatTrackedDeviceProperty_m683282924 (CVRSystem_t1953699154 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Valve.VR.CVRSystem::GetInt32TrackedDeviceProperty(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&)
extern "C"  int32_t CVRSystem_GetInt32TrackedDeviceProperty_m4000822342 (CVRSystem_t1953699154 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.CVRSystem::GetUint64TrackedDeviceProperty(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&)
extern "C"  uint64_t CVRSystem_GetUint64TrackedDeviceProperty_m2694815338 (CVRSystem_t1953699154 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix34_t Valve.VR.CVRSystem::GetMatrix34TrackedDeviceProperty(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&)
extern "C"  HmdMatrix34_t_t664273062  CVRSystem_GetMatrix34TrackedDeviceProperty_m1311929681 (CVRSystem_t1953699154 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRSystem::GetStringTrackedDeviceProperty(System.UInt32,Valve.VR.ETrackedDeviceProperty,System.Text.StringBuilder,System.UInt32,Valve.VR.ETrackedPropertyError&)
extern "C"  uint32_t CVRSystem_GetStringTrackedDeviceProperty_m1443593454 (CVRSystem_t1953699154 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unBufferSize3, int32_t* ___pError4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.CVRSystem::GetPropErrorNameFromEnum(Valve.VR.ETrackedPropertyError)
extern "C"  String_t* CVRSystem_GetPropErrorNameFromEnum_m2677385557 (CVRSystem_t1953699154 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSystem::PollNextEvent(Valve.VR.VREvent_t&,System.UInt32)
extern "C"  bool CVRSystem_PollNextEvent_m3572312252 (CVRSystem_t1953699154 * __this, VREvent_t_t3405266389 * ___pEvent0, uint32_t ___uncbVREvent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSystem::PollNextEventWithPose(Valve.VR.ETrackingUniverseOrigin,Valve.VR.VREvent_t&,System.UInt32,Valve.VR.TrackedDevicePose_t&)
extern "C"  bool CVRSystem_PollNextEventWithPose_m2757197664 (CVRSystem_t1953699154 * __this, int32_t ___eOrigin0, VREvent_t_t3405266389 * ___pEvent1, uint32_t ___uncbVREvent2, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.CVRSystem::GetEventTypeNameFromEnum(Valve.VR.EVREventType)
extern "C"  String_t* CVRSystem_GetEventTypeNameFromEnum_m639039909 (CVRSystem_t1953699154 * __this, int32_t ___eType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HiddenAreaMesh_t Valve.VR.CVRSystem::GetHiddenAreaMesh(Valve.VR.EVREye,Valve.VR.EHiddenAreaMeshType)
extern "C"  HiddenAreaMesh_t_t3319190843  CVRSystem_GetHiddenAreaMesh_m144613494 (CVRSystem_t1953699154 * __this, int32_t ___eEye0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSystem::GetControllerState(System.UInt32,Valve.VR.VRControllerState_t&,System.UInt32)
extern "C"  bool CVRSystem_GetControllerState_m2344918878 (CVRSystem_t1953699154 * __this, uint32_t ___unControllerDeviceIndex0, VRControllerState_t_t2504874220 * ___pControllerState1, uint32_t ___unControllerStateSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSystem::GetControllerStateWithPose(Valve.VR.ETrackingUniverseOrigin,System.UInt32,Valve.VR.VRControllerState_t&,System.UInt32,Valve.VR.TrackedDevicePose_t&)
extern "C"  bool CVRSystem_GetControllerStateWithPose_m1047691368 (CVRSystem_t1953699154 * __this, int32_t ___eOrigin0, uint32_t ___unControllerDeviceIndex1, VRControllerState_t_t2504874220 * ___pControllerState2, uint32_t ___unControllerStateSize3, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSystem::TriggerHapticPulse(System.UInt32,System.UInt32,System.Char)
extern "C"  void CVRSystem_TriggerHapticPulse_m3548655686 (CVRSystem_t1953699154 * __this, uint32_t ___unControllerDeviceIndex0, uint32_t ___unAxisId1, Il2CppChar ___usDurationMicroSec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.CVRSystem::GetButtonIdNameFromEnum(Valve.VR.EVRButtonId)
extern "C"  String_t* CVRSystem_GetButtonIdNameFromEnum_m1143662885 (CVRSystem_t1953699154 * __this, int32_t ___eButtonId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.CVRSystem::GetControllerAxisTypeNameFromEnum(Valve.VR.EVRControllerAxisType)
extern "C"  String_t* CVRSystem_GetControllerAxisTypeNameFromEnum_m953000165 (CVRSystem_t1953699154 * __this, int32_t ___eAxisType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSystem::CaptureInputFocus()
extern "C"  bool CVRSystem_CaptureInputFocus_m919523215 (CVRSystem_t1953699154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSystem::ReleaseInputFocus()
extern "C"  void CVRSystem_ReleaseInputFocus_m3155296764 (CVRSystem_t1953699154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSystem::IsInputFocusCapturedByAnotherProcess()
extern "C"  bool CVRSystem_IsInputFocusCapturedByAnotherProcess_m3335671804 (CVRSystem_t1953699154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.CVRSystem::DriverDebugRequest(System.UInt32,System.String,System.String,System.UInt32)
extern "C"  uint32_t CVRSystem_DriverDebugRequest_m4089038764 (CVRSystem_t1953699154 * __this, uint32_t ___unDeviceIndex0, String_t* ___pchRequest1, String_t* ___pchResponseBuffer2, uint32_t ___unResponseBufferSize3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRFirmwareError Valve.VR.CVRSystem::PerformFirmwareUpdate(System.UInt32)
extern "C"  int32_t CVRSystem_PerformFirmwareUpdate_m61309487 (CVRSystem_t1953699154 * __this, uint32_t ___unDeviceIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSystem::AcknowledgeQuit_Exiting()
extern "C"  void CVRSystem_AcknowledgeQuit_Exiting_m2589075259 (CVRSystem_t1953699154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSystem::AcknowledgeQuit_UserPrompt()
extern "C"  void CVRSystem_AcknowledgeQuit_UserPrompt_m2661639916 (CVRSystem_t1953699154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
