﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4135883112.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Valve_VR_Texture_t3277130850.h"

// System.Void System.Array/InternalEnumerator`1<Valve.VR.Texture_t>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1441977599_gshared (InternalEnumerator_1_t4135883112 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1441977599(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4135883112 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1441977599_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.Texture_t>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m569624751_gshared (InternalEnumerator_1_t4135883112 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m569624751(__this, method) ((  void (*) (InternalEnumerator_1_t4135883112 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m569624751_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Valve.VR.Texture_t>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2918717275_gshared (InternalEnumerator_1_t4135883112 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2918717275(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4135883112 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2918717275_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.Texture_t>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1315872414_gshared (InternalEnumerator_1_t4135883112 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1315872414(__this, method) ((  void (*) (InternalEnumerator_1_t4135883112 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1315872414_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Valve.VR.Texture_t>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m413069227_gshared (InternalEnumerator_1_t4135883112 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m413069227(__this, method) ((  bool (*) (InternalEnumerator_1_t4135883112 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m413069227_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Valve.VR.Texture_t>::get_Current()
extern "C"  Texture_t_t3277130850  InternalEnumerator_1_get_Current_m2962791072_gshared (InternalEnumerator_1_t4135883112 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2962791072(__this, method) ((  Texture_t_t3277130850  (*) (InternalEnumerator_1_t4135883112 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2962791072_gshared)(__this, method)
