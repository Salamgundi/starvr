﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ControllerInteractionEventArgs>
struct UnityEvent_2_t3265291444;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerInteractionEventArgs>
struct UnityAction_2_t1383093526;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ControllerInteractionEventArgs>::.ctor()
extern "C"  void UnityEvent_2__ctor_m1979127846_gshared (UnityEvent_2_t3265291444 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m1979127846(__this, method) ((  void (*) (UnityEvent_2_t3265291444 *, const MethodInfo*))UnityEvent_2__ctor_m1979127846_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ControllerInteractionEventArgs>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m1115984840_gshared (UnityEvent_2_t3265291444 * __this, UnityAction_2_t1383093526 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m1115984840(__this, ___call0, method) ((  void (*) (UnityEvent_2_t3265291444 *, UnityAction_2_t1383093526 *, const MethodInfo*))UnityEvent_2_AddListener_m1115984840_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ControllerInteractionEventArgs>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m3249457471_gshared (UnityEvent_2_t3265291444 * __this, UnityAction_2_t1383093526 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m3249457471(__this, ___call0, method) ((  void (*) (UnityEvent_2_t3265291444 *, UnityAction_2_t1383093526 *, const MethodInfo*))UnityEvent_2_RemoveListener_m3249457471_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ControllerInteractionEventArgs>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m3958271164_gshared (UnityEvent_2_t3265291444 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m3958271164(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t3265291444 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m3958271164_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ControllerInteractionEventArgs>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m40071928_gshared (UnityEvent_2_t3265291444 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m40071928(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t3265291444 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m40071928_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ControllerInteractionEventArgs>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m3094393243_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t1383093526 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m3094393243(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t1383093526 *, const MethodInfo*))UnityEvent_2_GetDelegate_m3094393243_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ControllerInteractionEventArgs>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m3985411061_gshared (UnityEvent_2_t3265291444 * __this, Il2CppObject * ___arg00, ControllerInteractionEventArgs_t287637539  ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m3985411061(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t3265291444 *, Il2CppObject *, ControllerInteractionEventArgs_t287637539 , const MethodInfo*))UnityEvent_2_Invoke_m3985411061_gshared)(__this, ___arg00, ___arg11, method)
