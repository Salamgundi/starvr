﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>
struct UnityEvent_2_t1336218330;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>
struct UnityAction_2_t3748987708;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetControllerAwareEvent2653531721.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::.ctor()
extern "C"  void UnityEvent_2__ctor_m233901470_gshared (UnityEvent_2_t1336218330 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m233901470(__this, method) ((  void (*) (UnityEvent_2_t1336218330 *, const MethodInfo*))UnityEvent_2__ctor_m233901470_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m4259171238_gshared (UnityEvent_2_t1336218330 * __this, UnityAction_2_t3748987708 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m4259171238(__this, ___call0, method) ((  void (*) (UnityEvent_2_t1336218330 *, UnityAction_2_t3748987708 *, const MethodInfo*))UnityEvent_2_AddListener_m4259171238_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m2963711527_gshared (UnityEvent_2_t1336218330 * __this, UnityAction_2_t3748987708 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m2963711527(__this, ___call0, method) ((  void (*) (UnityEvent_2_t1336218330 *, UnityAction_2_t3748987708 *, const MethodInfo*))UnityEvent_2_RemoveListener_m2963711527_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m4144365794_gshared (UnityEvent_2_t1336218330 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m4144365794(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t1336218330 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m4144365794_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m1295527784_gshared (UnityEvent_2_t1336218330 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m1295527784(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t1336218330 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m1295527784_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m174184863_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t3748987708 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m174184863(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t3748987708 *, const MethodInfo*))UnityEvent_2_GetDelegate_m174184863_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m97298697_gshared (UnityEvent_2_t1336218330 * __this, Il2CppObject * ___arg00, HeadsetControllerAwareEventArgs_t2653531721  ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m97298697(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t1336218330 *, Il2CppObject *, HeadsetControllerAwareEventArgs_t2653531721 , const MethodInfo*))UnityEvent_2_Invoke_m97298697_gshared)(__this, ___arg00, ___arg11, method)
