﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Controller_Hand_Han584661213.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Controller_Hand
struct  Controller_Hand_t3658229830  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.Examples.Controller_Hand/Hands VRTK.Examples.Controller_Hand::hand
	int32_t ___hand_2;
	// UnityEngine.Transform VRTK.Examples.Controller_Hand::pointerFinger
	Transform_t3275118058 * ___pointerFinger_3;
	// UnityEngine.Transform VRTK.Examples.Controller_Hand::gripFingers
	Transform_t3275118058 * ___gripFingers_4;
	// System.Single VRTK.Examples.Controller_Hand::maxRotation
	float ___maxRotation_5;
	// System.Single VRTK.Examples.Controller_Hand::originalPointerRotation
	float ___originalPointerRotation_6;
	// System.Single VRTK.Examples.Controller_Hand::originalGripRotation
	float ___originalGripRotation_7;
	// System.Single VRTK.Examples.Controller_Hand::targetPointerRotation
	float ___targetPointerRotation_8;
	// System.Single VRTK.Examples.Controller_Hand::targetGripRotation
	float ___targetGripRotation_9;

public:
	inline static int32_t get_offset_of_hand_2() { return static_cast<int32_t>(offsetof(Controller_Hand_t3658229830, ___hand_2)); }
	inline int32_t get_hand_2() const { return ___hand_2; }
	inline int32_t* get_address_of_hand_2() { return &___hand_2; }
	inline void set_hand_2(int32_t value)
	{
		___hand_2 = value;
	}

	inline static int32_t get_offset_of_pointerFinger_3() { return static_cast<int32_t>(offsetof(Controller_Hand_t3658229830, ___pointerFinger_3)); }
	inline Transform_t3275118058 * get_pointerFinger_3() const { return ___pointerFinger_3; }
	inline Transform_t3275118058 ** get_address_of_pointerFinger_3() { return &___pointerFinger_3; }
	inline void set_pointerFinger_3(Transform_t3275118058 * value)
	{
		___pointerFinger_3 = value;
		Il2CppCodeGenWriteBarrier(&___pointerFinger_3, value);
	}

	inline static int32_t get_offset_of_gripFingers_4() { return static_cast<int32_t>(offsetof(Controller_Hand_t3658229830, ___gripFingers_4)); }
	inline Transform_t3275118058 * get_gripFingers_4() const { return ___gripFingers_4; }
	inline Transform_t3275118058 ** get_address_of_gripFingers_4() { return &___gripFingers_4; }
	inline void set_gripFingers_4(Transform_t3275118058 * value)
	{
		___gripFingers_4 = value;
		Il2CppCodeGenWriteBarrier(&___gripFingers_4, value);
	}

	inline static int32_t get_offset_of_maxRotation_5() { return static_cast<int32_t>(offsetof(Controller_Hand_t3658229830, ___maxRotation_5)); }
	inline float get_maxRotation_5() const { return ___maxRotation_5; }
	inline float* get_address_of_maxRotation_5() { return &___maxRotation_5; }
	inline void set_maxRotation_5(float value)
	{
		___maxRotation_5 = value;
	}

	inline static int32_t get_offset_of_originalPointerRotation_6() { return static_cast<int32_t>(offsetof(Controller_Hand_t3658229830, ___originalPointerRotation_6)); }
	inline float get_originalPointerRotation_6() const { return ___originalPointerRotation_6; }
	inline float* get_address_of_originalPointerRotation_6() { return &___originalPointerRotation_6; }
	inline void set_originalPointerRotation_6(float value)
	{
		___originalPointerRotation_6 = value;
	}

	inline static int32_t get_offset_of_originalGripRotation_7() { return static_cast<int32_t>(offsetof(Controller_Hand_t3658229830, ___originalGripRotation_7)); }
	inline float get_originalGripRotation_7() const { return ___originalGripRotation_7; }
	inline float* get_address_of_originalGripRotation_7() { return &___originalGripRotation_7; }
	inline void set_originalGripRotation_7(float value)
	{
		___originalGripRotation_7 = value;
	}

	inline static int32_t get_offset_of_targetPointerRotation_8() { return static_cast<int32_t>(offsetof(Controller_Hand_t3658229830, ___targetPointerRotation_8)); }
	inline float get_targetPointerRotation_8() const { return ___targetPointerRotation_8; }
	inline float* get_address_of_targetPointerRotation_8() { return &___targetPointerRotation_8; }
	inline void set_targetPointerRotation_8(float value)
	{
		___targetPointerRotation_8 = value;
	}

	inline static int32_t get_offset_of_targetGripRotation_9() { return static_cast<int32_t>(offsetof(Controller_Hand_t3658229830, ___targetGripRotation_9)); }
	inline float get_targetGripRotation_9() const { return ___targetGripRotation_9; }
	inline float* get_address_of_targetGripRotation_9() { return &___targetGripRotation_9; }
	inline void set_targetGripRotation_9(float value)
	{
		___targetGripRotation_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
