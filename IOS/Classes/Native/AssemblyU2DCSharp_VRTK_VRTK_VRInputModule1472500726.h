﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<VRTK.VRTK_UIPointer>
struct List_1_t2084047587;

#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp1441575871.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_VRInputModule
struct  VRTK_VRInputModule_t1472500726  : public PointerInputModule_t1441575871
{
public:
	// System.Collections.Generic.List`1<VRTK.VRTK_UIPointer> VRTK.VRTK_VRInputModule::pointers
	List_1_t2084047587 * ___pointers_14;

public:
	inline static int32_t get_offset_of_pointers_14() { return static_cast<int32_t>(offsetof(VRTK_VRInputModule_t1472500726, ___pointers_14)); }
	inline List_1_t2084047587 * get_pointers_14() const { return ___pointers_14; }
	inline List_1_t2084047587 ** get_address_of_pointers_14() { return &___pointers_14; }
	inline void set_pointers_14(List_1_t2084047587 * value)
	{
		___pointers_14 = value;
		Il2CppCodeGenWriteBarrier(&___pointers_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
