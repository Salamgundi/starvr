﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Animation
struct Animation_t2068071072;
// UnityEngine.UI.Text
struct Text_t356221433;
// Valve.VR.InteractionSystem.Player
struct Player_t4256718089;

#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Telep1112706968.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Telep4220181178.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.TeleportPoint
struct  TeleportPoint_t3942003985  : public TeleportMarkerBase_t1112706968
{
public:
	// Valve.VR.InteractionSystem.TeleportPoint/TeleportPointType Valve.VR.InteractionSystem.TeleportPoint::teleportType
	int32_t ___teleportType_4;
	// System.String Valve.VR.InteractionSystem.TeleportPoint::title
	String_t* ___title_5;
	// System.String Valve.VR.InteractionSystem.TeleportPoint::switchToScene
	String_t* ___switchToScene_6;
	// UnityEngine.Color Valve.VR.InteractionSystem.TeleportPoint::titleVisibleColor
	Color_t2020392075  ___titleVisibleColor_7;
	// UnityEngine.Color Valve.VR.InteractionSystem.TeleportPoint::titleHighlightedColor
	Color_t2020392075  ___titleHighlightedColor_8;
	// UnityEngine.Color Valve.VR.InteractionSystem.TeleportPoint::titleLockedColor
	Color_t2020392075  ___titleLockedColor_9;
	// System.Boolean Valve.VR.InteractionSystem.TeleportPoint::playerSpawnPoint
	bool ___playerSpawnPoint_10;
	// System.Boolean Valve.VR.InteractionSystem.TeleportPoint::gotReleventComponents
	bool ___gotReleventComponents_11;
	// UnityEngine.MeshRenderer Valve.VR.InteractionSystem.TeleportPoint::markerMesh
	MeshRenderer_t1268241104 * ___markerMesh_12;
	// UnityEngine.MeshRenderer Valve.VR.InteractionSystem.TeleportPoint::switchSceneIcon
	MeshRenderer_t1268241104 * ___switchSceneIcon_13;
	// UnityEngine.MeshRenderer Valve.VR.InteractionSystem.TeleportPoint::moveLocationIcon
	MeshRenderer_t1268241104 * ___moveLocationIcon_14;
	// UnityEngine.MeshRenderer Valve.VR.InteractionSystem.TeleportPoint::lockedIcon
	MeshRenderer_t1268241104 * ___lockedIcon_15;
	// UnityEngine.MeshRenderer Valve.VR.InteractionSystem.TeleportPoint::pointIcon
	MeshRenderer_t1268241104 * ___pointIcon_16;
	// UnityEngine.Transform Valve.VR.InteractionSystem.TeleportPoint::lookAtJointTransform
	Transform_t3275118058 * ___lookAtJointTransform_17;
	// UnityEngine.Animation Valve.VR.InteractionSystem.TeleportPoint::animation
	Animation_t2068071072 * ___animation_18;
	// UnityEngine.UI.Text Valve.VR.InteractionSystem.TeleportPoint::titleText
	Text_t356221433 * ___titleText_19;
	// Valve.VR.InteractionSystem.Player Valve.VR.InteractionSystem.TeleportPoint::player
	Player_t4256718089 * ___player_20;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.TeleportPoint::lookAtPosition
	Vector3_t2243707580  ___lookAtPosition_21;
	// System.Int32 Valve.VR.InteractionSystem.TeleportPoint::tintColorID
	int32_t ___tintColorID_22;
	// UnityEngine.Color Valve.VR.InteractionSystem.TeleportPoint::tintColor
	Color_t2020392075  ___tintColor_23;
	// UnityEngine.Color Valve.VR.InteractionSystem.TeleportPoint::titleColor
	Color_t2020392075  ___titleColor_24;
	// System.Single Valve.VR.InteractionSystem.TeleportPoint::fullTitleAlpha
	float ___fullTitleAlpha_25;

public:
	inline static int32_t get_offset_of_teleportType_4() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___teleportType_4)); }
	inline int32_t get_teleportType_4() const { return ___teleportType_4; }
	inline int32_t* get_address_of_teleportType_4() { return &___teleportType_4; }
	inline void set_teleportType_4(int32_t value)
	{
		___teleportType_4 = value;
	}

	inline static int32_t get_offset_of_title_5() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___title_5)); }
	inline String_t* get_title_5() const { return ___title_5; }
	inline String_t** get_address_of_title_5() { return &___title_5; }
	inline void set_title_5(String_t* value)
	{
		___title_5 = value;
		Il2CppCodeGenWriteBarrier(&___title_5, value);
	}

	inline static int32_t get_offset_of_switchToScene_6() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___switchToScene_6)); }
	inline String_t* get_switchToScene_6() const { return ___switchToScene_6; }
	inline String_t** get_address_of_switchToScene_6() { return &___switchToScene_6; }
	inline void set_switchToScene_6(String_t* value)
	{
		___switchToScene_6 = value;
		Il2CppCodeGenWriteBarrier(&___switchToScene_6, value);
	}

	inline static int32_t get_offset_of_titleVisibleColor_7() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___titleVisibleColor_7)); }
	inline Color_t2020392075  get_titleVisibleColor_7() const { return ___titleVisibleColor_7; }
	inline Color_t2020392075 * get_address_of_titleVisibleColor_7() { return &___titleVisibleColor_7; }
	inline void set_titleVisibleColor_7(Color_t2020392075  value)
	{
		___titleVisibleColor_7 = value;
	}

	inline static int32_t get_offset_of_titleHighlightedColor_8() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___titleHighlightedColor_8)); }
	inline Color_t2020392075  get_titleHighlightedColor_8() const { return ___titleHighlightedColor_8; }
	inline Color_t2020392075 * get_address_of_titleHighlightedColor_8() { return &___titleHighlightedColor_8; }
	inline void set_titleHighlightedColor_8(Color_t2020392075  value)
	{
		___titleHighlightedColor_8 = value;
	}

	inline static int32_t get_offset_of_titleLockedColor_9() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___titleLockedColor_9)); }
	inline Color_t2020392075  get_titleLockedColor_9() const { return ___titleLockedColor_9; }
	inline Color_t2020392075 * get_address_of_titleLockedColor_9() { return &___titleLockedColor_9; }
	inline void set_titleLockedColor_9(Color_t2020392075  value)
	{
		___titleLockedColor_9 = value;
	}

	inline static int32_t get_offset_of_playerSpawnPoint_10() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___playerSpawnPoint_10)); }
	inline bool get_playerSpawnPoint_10() const { return ___playerSpawnPoint_10; }
	inline bool* get_address_of_playerSpawnPoint_10() { return &___playerSpawnPoint_10; }
	inline void set_playerSpawnPoint_10(bool value)
	{
		___playerSpawnPoint_10 = value;
	}

	inline static int32_t get_offset_of_gotReleventComponents_11() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___gotReleventComponents_11)); }
	inline bool get_gotReleventComponents_11() const { return ___gotReleventComponents_11; }
	inline bool* get_address_of_gotReleventComponents_11() { return &___gotReleventComponents_11; }
	inline void set_gotReleventComponents_11(bool value)
	{
		___gotReleventComponents_11 = value;
	}

	inline static int32_t get_offset_of_markerMesh_12() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___markerMesh_12)); }
	inline MeshRenderer_t1268241104 * get_markerMesh_12() const { return ___markerMesh_12; }
	inline MeshRenderer_t1268241104 ** get_address_of_markerMesh_12() { return &___markerMesh_12; }
	inline void set_markerMesh_12(MeshRenderer_t1268241104 * value)
	{
		___markerMesh_12 = value;
		Il2CppCodeGenWriteBarrier(&___markerMesh_12, value);
	}

	inline static int32_t get_offset_of_switchSceneIcon_13() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___switchSceneIcon_13)); }
	inline MeshRenderer_t1268241104 * get_switchSceneIcon_13() const { return ___switchSceneIcon_13; }
	inline MeshRenderer_t1268241104 ** get_address_of_switchSceneIcon_13() { return &___switchSceneIcon_13; }
	inline void set_switchSceneIcon_13(MeshRenderer_t1268241104 * value)
	{
		___switchSceneIcon_13 = value;
		Il2CppCodeGenWriteBarrier(&___switchSceneIcon_13, value);
	}

	inline static int32_t get_offset_of_moveLocationIcon_14() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___moveLocationIcon_14)); }
	inline MeshRenderer_t1268241104 * get_moveLocationIcon_14() const { return ___moveLocationIcon_14; }
	inline MeshRenderer_t1268241104 ** get_address_of_moveLocationIcon_14() { return &___moveLocationIcon_14; }
	inline void set_moveLocationIcon_14(MeshRenderer_t1268241104 * value)
	{
		___moveLocationIcon_14 = value;
		Il2CppCodeGenWriteBarrier(&___moveLocationIcon_14, value);
	}

	inline static int32_t get_offset_of_lockedIcon_15() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___lockedIcon_15)); }
	inline MeshRenderer_t1268241104 * get_lockedIcon_15() const { return ___lockedIcon_15; }
	inline MeshRenderer_t1268241104 ** get_address_of_lockedIcon_15() { return &___lockedIcon_15; }
	inline void set_lockedIcon_15(MeshRenderer_t1268241104 * value)
	{
		___lockedIcon_15 = value;
		Il2CppCodeGenWriteBarrier(&___lockedIcon_15, value);
	}

	inline static int32_t get_offset_of_pointIcon_16() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___pointIcon_16)); }
	inline MeshRenderer_t1268241104 * get_pointIcon_16() const { return ___pointIcon_16; }
	inline MeshRenderer_t1268241104 ** get_address_of_pointIcon_16() { return &___pointIcon_16; }
	inline void set_pointIcon_16(MeshRenderer_t1268241104 * value)
	{
		___pointIcon_16 = value;
		Il2CppCodeGenWriteBarrier(&___pointIcon_16, value);
	}

	inline static int32_t get_offset_of_lookAtJointTransform_17() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___lookAtJointTransform_17)); }
	inline Transform_t3275118058 * get_lookAtJointTransform_17() const { return ___lookAtJointTransform_17; }
	inline Transform_t3275118058 ** get_address_of_lookAtJointTransform_17() { return &___lookAtJointTransform_17; }
	inline void set_lookAtJointTransform_17(Transform_t3275118058 * value)
	{
		___lookAtJointTransform_17 = value;
		Il2CppCodeGenWriteBarrier(&___lookAtJointTransform_17, value);
	}

	inline static int32_t get_offset_of_animation_18() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___animation_18)); }
	inline Animation_t2068071072 * get_animation_18() const { return ___animation_18; }
	inline Animation_t2068071072 ** get_address_of_animation_18() { return &___animation_18; }
	inline void set_animation_18(Animation_t2068071072 * value)
	{
		___animation_18 = value;
		Il2CppCodeGenWriteBarrier(&___animation_18, value);
	}

	inline static int32_t get_offset_of_titleText_19() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___titleText_19)); }
	inline Text_t356221433 * get_titleText_19() const { return ___titleText_19; }
	inline Text_t356221433 ** get_address_of_titleText_19() { return &___titleText_19; }
	inline void set_titleText_19(Text_t356221433 * value)
	{
		___titleText_19 = value;
		Il2CppCodeGenWriteBarrier(&___titleText_19, value);
	}

	inline static int32_t get_offset_of_player_20() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___player_20)); }
	inline Player_t4256718089 * get_player_20() const { return ___player_20; }
	inline Player_t4256718089 ** get_address_of_player_20() { return &___player_20; }
	inline void set_player_20(Player_t4256718089 * value)
	{
		___player_20 = value;
		Il2CppCodeGenWriteBarrier(&___player_20, value);
	}

	inline static int32_t get_offset_of_lookAtPosition_21() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___lookAtPosition_21)); }
	inline Vector3_t2243707580  get_lookAtPosition_21() const { return ___lookAtPosition_21; }
	inline Vector3_t2243707580 * get_address_of_lookAtPosition_21() { return &___lookAtPosition_21; }
	inline void set_lookAtPosition_21(Vector3_t2243707580  value)
	{
		___lookAtPosition_21 = value;
	}

	inline static int32_t get_offset_of_tintColorID_22() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___tintColorID_22)); }
	inline int32_t get_tintColorID_22() const { return ___tintColorID_22; }
	inline int32_t* get_address_of_tintColorID_22() { return &___tintColorID_22; }
	inline void set_tintColorID_22(int32_t value)
	{
		___tintColorID_22 = value;
	}

	inline static int32_t get_offset_of_tintColor_23() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___tintColor_23)); }
	inline Color_t2020392075  get_tintColor_23() const { return ___tintColor_23; }
	inline Color_t2020392075 * get_address_of_tintColor_23() { return &___tintColor_23; }
	inline void set_tintColor_23(Color_t2020392075  value)
	{
		___tintColor_23 = value;
	}

	inline static int32_t get_offset_of_titleColor_24() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___titleColor_24)); }
	inline Color_t2020392075  get_titleColor_24() const { return ___titleColor_24; }
	inline Color_t2020392075 * get_address_of_titleColor_24() { return &___titleColor_24; }
	inline void set_titleColor_24(Color_t2020392075  value)
	{
		___titleColor_24 = value;
	}

	inline static int32_t get_offset_of_fullTitleAlpha_25() { return static_cast<int32_t>(offsetof(TeleportPoint_t3942003985, ___fullTitleAlpha_25)); }
	inline float get_fullTitleAlpha_25() const { return ___fullTitleAlpha_25; }
	inline float* get_address_of_fullTitleAlpha_25() { return &___fullTitleAlpha_25; }
	inline void set_fullTitleAlpha_25(float value)
	{
		___fullTitleAlpha_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
