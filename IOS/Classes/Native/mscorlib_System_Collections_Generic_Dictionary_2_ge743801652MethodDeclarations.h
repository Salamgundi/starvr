﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::.ctor()
#define Dictionary_2__ctor_m1757110354(__this, method) ((  void (*) (Dictionary_2_t743801652 *, const MethodInfo*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m3651256934(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t743801652 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m406310120_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::.ctor(System.Int32)
#define Dictionary_2__ctor_m4246523646(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t743801652 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m206582704_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m4064016664(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t743801652 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1206668798_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1112000487(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t743801652 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m237963271_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1966730404(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t743801652 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3775521570_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m1338867109(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t743801652 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m984276885_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m2130605080(__this, ___key0, method) ((  void (*) (Dictionary_2_t743801652 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2017099222_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1663687711(__this, method) ((  bool (*) (Dictionary_2_t743801652 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m960517203_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m214959187(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t743801652 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1900166091_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3876351765(__this, method) ((  bool (*) (Dictionary_2_t743801652 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4094240197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m751311066(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t743801652 *, KeyValuePair_2_t2796114170 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m990341268_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2438592038(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t743801652 *, KeyValuePair_2_t2796114170 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1058501024_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2470995302(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t743801652 *, KeyValuePair_2U5BU5D_t607812063*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m976354816_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m513681459(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t743801652 *, KeyValuePair_2_t2796114170 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1705959559_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2555258039(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t743801652 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3578539931_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1849087296(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t743801652 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3100111910_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2114731677(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t743801652 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2925090477_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2460196658(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t743801652 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2684932776_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::get_Count()
#define Dictionary_2_get_Count_m973880455(__this, method) ((  int32_t (*) (Dictionary_2_t743801652 *, const MethodInfo*))Dictionary_2_get_Count_m3636113691_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::get_Item(TKey)
#define Dictionary_2_get_Item_m1528606822(__this, ___key0, method) ((  MaterialU5BU5D_t3123989686* (*) (Dictionary_2_t743801652 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m2413909512_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m1701653461(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t743801652 *, String_t*, MaterialU5BU5D_t3123989686*, const MethodInfo*))Dictionary_2_set_Item_m458653679_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m1719774299(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t743801652 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1045257495_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m649547402(__this, ___size0, method) ((  void (*) (Dictionary_2_t743801652 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2270022740_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m1815225008(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t743801652 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2147716750_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m3221635506(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2796114170  (*) (Il2CppObject * /* static, unused */, String_t*, MaterialU5BU5D_t3123989686*, const MethodInfo*))Dictionary_2_make_pair_m2631942124_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m1505547460(__this /* static, unused */, ___key0, ___value1, method) ((  MaterialU5BU5D_t3123989686* (*) (Il2CppObject * /* static, unused */, String_t*, MaterialU5BU5D_t3123989686*, const MethodInfo*))Dictionary_2_pick_value_m1872663242_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m499121455(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t743801652 *, KeyValuePair_2U5BU5D_t607812063*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1495142643_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::Resize()
#define Dictionary_2_Resize_m1277578541(__this, method) ((  void (*) (Dictionary_2_t743801652 *, const MethodInfo*))Dictionary_2_Resize_m2672264133_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::Add(TKey,TValue)
#define Dictionary_2_Add_m2497623850(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t743801652 *, String_t*, MaterialU5BU5D_t3123989686*, const MethodInfo*))Dictionary_2_Add_m1708621268_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::Clear()
#define Dictionary_2_Clear_m42302381(__this, method) ((  void (*) (Dictionary_2_t743801652 *, const MethodInfo*))Dictionary_2_Clear_m2325793156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m3199526737(__this, ___key0, method) ((  bool (*) (Dictionary_2_t743801652 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m3553426152_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m2109338798(__this, ___value0, method) ((  bool (*) (Dictionary_2_t743801652 *, MaterialU5BU5D_t3123989686*, const MethodInfo*))Dictionary_2_ContainsValue_m2375979648_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m486520947(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t743801652 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2864531407_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m928617475(__this, ___sender0, method) ((  void (*) (Dictionary_2_t743801652 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2160537783_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::Remove(TKey)
#define Dictionary_2_Remove_m3084796638(__this, ___key0, method) ((  bool (*) (Dictionary_2_t743801652 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m1366616528_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m1245501899(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t743801652 *, String_t*, MaterialU5BU5D_t3123989686**, const MethodInfo*))Dictionary_2_TryGetValue_m1120370623_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::get_Values()
#define Dictionary_2_get_Values_m2206814590(__this, method) ((  ValueCollection_t3741828791 * (*) (Dictionary_2_t743801652 *, const MethodInfo*))Dictionary_2_get_Values_m825860460_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m3594437373(__this, ___key0, method) ((  String_t* (*) (Dictionary_2_t743801652 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4209561517_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m1737836829(__this, ___value0, method) ((  MaterialU5BU5D_t3123989686* (*) (Dictionary_2_t743801652 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1381983709_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m2683075651(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t743801652 *, KeyValuePair_2_t2796114170 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m663697471_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2864420864(__this, method) ((  Enumerator_t2063826354  (*) (Dictionary_2_t743801652 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1752238884_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m3044953821(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, String_t*, MaterialU5BU5D_t3123989686*, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2061238213_gshared)(__this /* static, unused */, ___key0, ___value1, method)
