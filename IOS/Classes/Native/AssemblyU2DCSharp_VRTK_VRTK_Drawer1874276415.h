﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.FixedJoint
struct FixedJoint_t3848069458;
// UnityEngine.ConfigurableJoint
struct ConfigurableJoint_t454307495;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// UnityEngine.ConstantForce
struct ConstantForce_t3796310167;

#include "AssemblyU2DCSharp_VRTK_VRTK_Control651619021.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_Direction3775008092.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Drawer
struct  VRTK_Drawer_t1874276415  : public VRTK_Control_t651619021
{
public:
	// UnityEngine.GameObject VRTK.VRTK_Drawer::connectedTo
	GameObject_t1756533147 * ___connectedTo_15;
	// VRTK.VRTK_Control/Direction VRTK.VRTK_Drawer::direction
	int32_t ___direction_16;
	// UnityEngine.GameObject VRTK.VRTK_Drawer::body
	GameObject_t1756533147 * ___body_17;
	// UnityEngine.GameObject VRTK.VRTK_Drawer::handle
	GameObject_t1756533147 * ___handle_18;
	// UnityEngine.GameObject VRTK.VRTK_Drawer::content
	GameObject_t1756533147 * ___content_19;
	// System.Boolean VRTK.VRTK_Drawer::hideContent
	bool ___hideContent_20;
	// System.Single VRTK.VRTK_Drawer::minSnapClose
	float ___minSnapClose_21;
	// System.Single VRTK.VRTK_Drawer::maxExtend
	float ___maxExtend_22;
	// UnityEngine.Rigidbody VRTK.VRTK_Drawer::drawerRigidbody
	Rigidbody_t4233889191 * ___drawerRigidbody_23;
	// UnityEngine.Rigidbody VRTK.VRTK_Drawer::handleRigidbody
	Rigidbody_t4233889191 * ___handleRigidbody_24;
	// UnityEngine.FixedJoint VRTK.VRTK_Drawer::handleFixedJoint
	FixedJoint_t3848069458 * ___handleFixedJoint_25;
	// UnityEngine.ConfigurableJoint VRTK.VRTK_Drawer::drawerJoint
	ConfigurableJoint_t454307495 * ___drawerJoint_26;
	// VRTK.VRTK_InteractableObject VRTK.VRTK_Drawer::drawerInteractableObject
	VRTK_InteractableObject_t2604188111 * ___drawerInteractableObject_27;
	// UnityEngine.ConstantForce VRTK.VRTK_Drawer::drawerSnapForce
	ConstantForce_t3796310167 * ___drawerSnapForce_28;
	// VRTK.VRTK_Control/Direction VRTK.VRTK_Drawer::finalDirection
	int32_t ___finalDirection_29;
	// System.Single VRTK.VRTK_Drawer::subDirection
	float ___subDirection_30;
	// System.Single VRTK.VRTK_Drawer::pullDistance
	float ___pullDistance_31;
	// UnityEngine.Vector3 VRTK.VRTK_Drawer::initialPosition
	Vector3_t2243707580  ___initialPosition_32;
	// System.Boolean VRTK.VRTK_Drawer::drawerJointCreated
	bool ___drawerJointCreated_33;
	// System.Boolean VRTK.VRTK_Drawer::drawerSnapForceCreated
	bool ___drawerSnapForceCreated_34;

public:
	inline static int32_t get_offset_of_connectedTo_15() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___connectedTo_15)); }
	inline GameObject_t1756533147 * get_connectedTo_15() const { return ___connectedTo_15; }
	inline GameObject_t1756533147 ** get_address_of_connectedTo_15() { return &___connectedTo_15; }
	inline void set_connectedTo_15(GameObject_t1756533147 * value)
	{
		___connectedTo_15 = value;
		Il2CppCodeGenWriteBarrier(&___connectedTo_15, value);
	}

	inline static int32_t get_offset_of_direction_16() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___direction_16)); }
	inline int32_t get_direction_16() const { return ___direction_16; }
	inline int32_t* get_address_of_direction_16() { return &___direction_16; }
	inline void set_direction_16(int32_t value)
	{
		___direction_16 = value;
	}

	inline static int32_t get_offset_of_body_17() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___body_17)); }
	inline GameObject_t1756533147 * get_body_17() const { return ___body_17; }
	inline GameObject_t1756533147 ** get_address_of_body_17() { return &___body_17; }
	inline void set_body_17(GameObject_t1756533147 * value)
	{
		___body_17 = value;
		Il2CppCodeGenWriteBarrier(&___body_17, value);
	}

	inline static int32_t get_offset_of_handle_18() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___handle_18)); }
	inline GameObject_t1756533147 * get_handle_18() const { return ___handle_18; }
	inline GameObject_t1756533147 ** get_address_of_handle_18() { return &___handle_18; }
	inline void set_handle_18(GameObject_t1756533147 * value)
	{
		___handle_18 = value;
		Il2CppCodeGenWriteBarrier(&___handle_18, value);
	}

	inline static int32_t get_offset_of_content_19() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___content_19)); }
	inline GameObject_t1756533147 * get_content_19() const { return ___content_19; }
	inline GameObject_t1756533147 ** get_address_of_content_19() { return &___content_19; }
	inline void set_content_19(GameObject_t1756533147 * value)
	{
		___content_19 = value;
		Il2CppCodeGenWriteBarrier(&___content_19, value);
	}

	inline static int32_t get_offset_of_hideContent_20() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___hideContent_20)); }
	inline bool get_hideContent_20() const { return ___hideContent_20; }
	inline bool* get_address_of_hideContent_20() { return &___hideContent_20; }
	inline void set_hideContent_20(bool value)
	{
		___hideContent_20 = value;
	}

	inline static int32_t get_offset_of_minSnapClose_21() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___minSnapClose_21)); }
	inline float get_minSnapClose_21() const { return ___minSnapClose_21; }
	inline float* get_address_of_minSnapClose_21() { return &___minSnapClose_21; }
	inline void set_minSnapClose_21(float value)
	{
		___minSnapClose_21 = value;
	}

	inline static int32_t get_offset_of_maxExtend_22() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___maxExtend_22)); }
	inline float get_maxExtend_22() const { return ___maxExtend_22; }
	inline float* get_address_of_maxExtend_22() { return &___maxExtend_22; }
	inline void set_maxExtend_22(float value)
	{
		___maxExtend_22 = value;
	}

	inline static int32_t get_offset_of_drawerRigidbody_23() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___drawerRigidbody_23)); }
	inline Rigidbody_t4233889191 * get_drawerRigidbody_23() const { return ___drawerRigidbody_23; }
	inline Rigidbody_t4233889191 ** get_address_of_drawerRigidbody_23() { return &___drawerRigidbody_23; }
	inline void set_drawerRigidbody_23(Rigidbody_t4233889191 * value)
	{
		___drawerRigidbody_23 = value;
		Il2CppCodeGenWriteBarrier(&___drawerRigidbody_23, value);
	}

	inline static int32_t get_offset_of_handleRigidbody_24() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___handleRigidbody_24)); }
	inline Rigidbody_t4233889191 * get_handleRigidbody_24() const { return ___handleRigidbody_24; }
	inline Rigidbody_t4233889191 ** get_address_of_handleRigidbody_24() { return &___handleRigidbody_24; }
	inline void set_handleRigidbody_24(Rigidbody_t4233889191 * value)
	{
		___handleRigidbody_24 = value;
		Il2CppCodeGenWriteBarrier(&___handleRigidbody_24, value);
	}

	inline static int32_t get_offset_of_handleFixedJoint_25() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___handleFixedJoint_25)); }
	inline FixedJoint_t3848069458 * get_handleFixedJoint_25() const { return ___handleFixedJoint_25; }
	inline FixedJoint_t3848069458 ** get_address_of_handleFixedJoint_25() { return &___handleFixedJoint_25; }
	inline void set_handleFixedJoint_25(FixedJoint_t3848069458 * value)
	{
		___handleFixedJoint_25 = value;
		Il2CppCodeGenWriteBarrier(&___handleFixedJoint_25, value);
	}

	inline static int32_t get_offset_of_drawerJoint_26() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___drawerJoint_26)); }
	inline ConfigurableJoint_t454307495 * get_drawerJoint_26() const { return ___drawerJoint_26; }
	inline ConfigurableJoint_t454307495 ** get_address_of_drawerJoint_26() { return &___drawerJoint_26; }
	inline void set_drawerJoint_26(ConfigurableJoint_t454307495 * value)
	{
		___drawerJoint_26 = value;
		Il2CppCodeGenWriteBarrier(&___drawerJoint_26, value);
	}

	inline static int32_t get_offset_of_drawerInteractableObject_27() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___drawerInteractableObject_27)); }
	inline VRTK_InteractableObject_t2604188111 * get_drawerInteractableObject_27() const { return ___drawerInteractableObject_27; }
	inline VRTK_InteractableObject_t2604188111 ** get_address_of_drawerInteractableObject_27() { return &___drawerInteractableObject_27; }
	inline void set_drawerInteractableObject_27(VRTK_InteractableObject_t2604188111 * value)
	{
		___drawerInteractableObject_27 = value;
		Il2CppCodeGenWriteBarrier(&___drawerInteractableObject_27, value);
	}

	inline static int32_t get_offset_of_drawerSnapForce_28() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___drawerSnapForce_28)); }
	inline ConstantForce_t3796310167 * get_drawerSnapForce_28() const { return ___drawerSnapForce_28; }
	inline ConstantForce_t3796310167 ** get_address_of_drawerSnapForce_28() { return &___drawerSnapForce_28; }
	inline void set_drawerSnapForce_28(ConstantForce_t3796310167 * value)
	{
		___drawerSnapForce_28 = value;
		Il2CppCodeGenWriteBarrier(&___drawerSnapForce_28, value);
	}

	inline static int32_t get_offset_of_finalDirection_29() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___finalDirection_29)); }
	inline int32_t get_finalDirection_29() const { return ___finalDirection_29; }
	inline int32_t* get_address_of_finalDirection_29() { return &___finalDirection_29; }
	inline void set_finalDirection_29(int32_t value)
	{
		___finalDirection_29 = value;
	}

	inline static int32_t get_offset_of_subDirection_30() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___subDirection_30)); }
	inline float get_subDirection_30() const { return ___subDirection_30; }
	inline float* get_address_of_subDirection_30() { return &___subDirection_30; }
	inline void set_subDirection_30(float value)
	{
		___subDirection_30 = value;
	}

	inline static int32_t get_offset_of_pullDistance_31() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___pullDistance_31)); }
	inline float get_pullDistance_31() const { return ___pullDistance_31; }
	inline float* get_address_of_pullDistance_31() { return &___pullDistance_31; }
	inline void set_pullDistance_31(float value)
	{
		___pullDistance_31 = value;
	}

	inline static int32_t get_offset_of_initialPosition_32() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___initialPosition_32)); }
	inline Vector3_t2243707580  get_initialPosition_32() const { return ___initialPosition_32; }
	inline Vector3_t2243707580 * get_address_of_initialPosition_32() { return &___initialPosition_32; }
	inline void set_initialPosition_32(Vector3_t2243707580  value)
	{
		___initialPosition_32 = value;
	}

	inline static int32_t get_offset_of_drawerJointCreated_33() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___drawerJointCreated_33)); }
	inline bool get_drawerJointCreated_33() const { return ___drawerJointCreated_33; }
	inline bool* get_address_of_drawerJointCreated_33() { return &___drawerJointCreated_33; }
	inline void set_drawerJointCreated_33(bool value)
	{
		___drawerJointCreated_33 = value;
	}

	inline static int32_t get_offset_of_drawerSnapForceCreated_34() { return static_cast<int32_t>(offsetof(VRTK_Drawer_t1874276415, ___drawerSnapForceCreated_34)); }
	inline bool get_drawerSnapForceCreated_34() const { return ___drawerSnapForceCreated_34; }
	inline bool* get_address_of_drawerSnapForceCreated_34() { return &___drawerSnapForceCreated_34; }
	inline void set_drawerSnapForceCreated_34(bool value)
	{
		___drawerSnapForceCreated_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
