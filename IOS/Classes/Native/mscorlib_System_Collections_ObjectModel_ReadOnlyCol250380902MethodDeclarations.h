﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>
struct ReadOnlyCollection_1_t250380902;
// System.Collections.Generic.IList`1<UnityEngine.CombineInstance>
struct IList_1_t605535811;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.CombineInstance[]
struct CombineInstanceU5BU5D_t1231324047;
// System.Collections.Generic.IEnumerator`1<UnityEngine.CombineInstance>
struct IEnumerator_1_t1835086333;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_CombineInstance64595210.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m270420083_gshared (ReadOnlyCollection_1_t250380902 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m270420083(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t250380902 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m270420083_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3096045777_gshared (ReadOnlyCollection_1_t250380902 * __this, CombineInstance_t64595210  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3096045777(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t250380902 *, CombineInstance_t64595210 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3096045777_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1874651405_gshared (ReadOnlyCollection_1_t250380902 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1874651405(__this, method) ((  void (*) (ReadOnlyCollection_1_t250380902 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1874651405_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1840999284_gshared (ReadOnlyCollection_1_t250380902 * __this, int32_t ___index0, CombineInstance_t64595210  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1840999284(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t250380902 *, int32_t, CombineInstance_t64595210 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1840999284_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3302979686_gshared (ReadOnlyCollection_1_t250380902 * __this, CombineInstance_t64595210  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3302979686(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t250380902 *, CombineInstance_t64595210 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3302979686_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m965173112_gshared (ReadOnlyCollection_1_t250380902 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m965173112(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t250380902 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m965173112_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  CombineInstance_t64595210  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2816752878_gshared (ReadOnlyCollection_1_t250380902 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2816752878(__this, ___index0, method) ((  CombineInstance_t64595210  (*) (ReadOnlyCollection_1_t250380902 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2816752878_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3398971233_gshared (ReadOnlyCollection_1_t250380902 * __this, int32_t ___index0, CombineInstance_t64595210  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3398971233(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t250380902 *, int32_t, CombineInstance_t64595210 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3398971233_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3619385965_gshared (ReadOnlyCollection_1_t250380902 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3619385965(__this, method) ((  bool (*) (ReadOnlyCollection_1_t250380902 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3619385965_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1505807558_gshared (ReadOnlyCollection_1_t250380902 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1505807558(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t250380902 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1505807558_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m253310953_gshared (ReadOnlyCollection_1_t250380902 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m253310953(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t250380902 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m253310953_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m958947878_gshared (ReadOnlyCollection_1_t250380902 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m958947878(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t250380902 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m958947878_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2447967740_gshared (ReadOnlyCollection_1_t250380902 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2447967740(__this, method) ((  void (*) (ReadOnlyCollection_1_t250380902 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2447967740_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3064555088_gshared (ReadOnlyCollection_1_t250380902 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3064555088(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t250380902 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3064555088_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3471586996_gshared (ReadOnlyCollection_1_t250380902 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3471586996(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t250380902 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3471586996_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1891504825_gshared (ReadOnlyCollection_1_t250380902 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1891504825(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t250380902 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1891504825_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2041773329_gshared (ReadOnlyCollection_1_t250380902 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2041773329(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t250380902 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2041773329_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3029099759_gshared (ReadOnlyCollection_1_t250380902 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3029099759(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t250380902 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3029099759_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m733972270_gshared (ReadOnlyCollection_1_t250380902 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m733972270(__this, method) ((  bool (*) (ReadOnlyCollection_1_t250380902 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m733972270_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m181706470_gshared (ReadOnlyCollection_1_t250380902 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m181706470(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t250380902 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m181706470_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2970492989_gshared (ReadOnlyCollection_1_t250380902 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2970492989(__this, method) ((  bool (*) (ReadOnlyCollection_1_t250380902 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2970492989_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m484601746_gshared (ReadOnlyCollection_1_t250380902 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m484601746(__this, method) ((  bool (*) (ReadOnlyCollection_1_t250380902 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m484601746_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m118080893_gshared (ReadOnlyCollection_1_t250380902 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m118080893(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t250380902 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m118080893_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m436111344_gshared (ReadOnlyCollection_1_t250380902 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m436111344(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t250380902 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m436111344_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m4087811147_gshared (ReadOnlyCollection_1_t250380902 * __this, CombineInstance_t64595210  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m4087811147(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t250380902 *, CombineInstance_t64595210 , const MethodInfo*))ReadOnlyCollection_1_Contains_m4087811147_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2032830149_gshared (ReadOnlyCollection_1_t250380902 * __this, CombineInstanceU5BU5D_t1231324047* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2032830149(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t250380902 *, CombineInstanceU5BU5D_t1231324047*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2032830149_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3830132778_gshared (ReadOnlyCollection_1_t250380902 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3830132778(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t250380902 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3830132778_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m384809617_gshared (ReadOnlyCollection_1_t250380902 * __this, CombineInstance_t64595210  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m384809617(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t250380902 *, CombineInstance_t64595210 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m384809617_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m668461414_gshared (ReadOnlyCollection_1_t250380902 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m668461414(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t250380902 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m668461414_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>::get_Item(System.Int32)
extern "C"  CombineInstance_t64595210  ReadOnlyCollection_1_get_Item_m1241258130_gshared (ReadOnlyCollection_1_t250380902 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1241258130(__this, ___index0, method) ((  CombineInstance_t64595210  (*) (ReadOnlyCollection_1_t250380902 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1241258130_gshared)(__this, ___index0, method)
