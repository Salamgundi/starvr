﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>
struct ValueCollection_t4011008547;
// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>
struct Dictionary_2_t1012981408;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color>
struct IEnumerator_1_t3790883198;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2699514172.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m940132823_gshared (ValueCollection_t4011008547 * __this, Dictionary_2_t1012981408 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m940132823(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4011008547 *, Dictionary_2_t1012981408 *, const MethodInfo*))ValueCollection__ctor_m940132823_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m554401673_gshared (ValueCollection_t4011008547 * __this, Color_t2020392075  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m554401673(__this, ___item0, method) ((  void (*) (ValueCollection_t4011008547 *, Color_t2020392075 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m554401673_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2170264774_gshared (ValueCollection_t4011008547 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2170264774(__this, method) ((  void (*) (ValueCollection_t4011008547 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2170264774_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3237182003_gshared (ValueCollection_t4011008547 * __this, Color_t2020392075  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3237182003(__this, ___item0, method) ((  bool (*) (ValueCollection_t4011008547 *, Color_t2020392075 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3237182003_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3941477308_gshared (ValueCollection_t4011008547 * __this, Color_t2020392075  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3941477308(__this, ___item0, method) ((  bool (*) (ValueCollection_t4011008547 *, Color_t2020392075 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3941477308_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3098831074_gshared (ValueCollection_t4011008547 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3098831074(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4011008547 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3098831074_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m211840322_gshared (ValueCollection_t4011008547 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m211840322(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4011008547 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m211840322_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2688763837_gshared (ValueCollection_t4011008547 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2688763837(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4011008547 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2688763837_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2345452736_gshared (ValueCollection_t4011008547 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2345452736(__this, method) ((  bool (*) (ValueCollection_t4011008547 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2345452736_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4081446986_gshared (ValueCollection_t4011008547 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4081446986(__this, method) ((  bool (*) (ValueCollection_t4011008547 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4081446986_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m4066002578_gshared (ValueCollection_t4011008547 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m4066002578(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4011008547 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m4066002578_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3892259622_gshared (ValueCollection_t4011008547 * __this, ColorU5BU5D_t672350442* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3892259622(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4011008547 *, ColorU5BU5D_t672350442*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3892259622_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::GetEnumerator()
extern "C"  Enumerator_t2699514172  ValueCollection_GetEnumerator_m1849783851_gshared (ValueCollection_t4011008547 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1849783851(__this, method) ((  Enumerator_t2699514172  (*) (ValueCollection_t4011008547 *, const MethodInfo*))ValueCollection_GetEnumerator_m1849783851_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m952813938_gshared (ValueCollection_t4011008547 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m952813938(__this, method) ((  int32_t (*) (ValueCollection_t4011008547 *, const MethodInfo*))ValueCollection_get_Count_m952813938_gshared)(__this, method)
