﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseTile
struct BaseTile_t3549052087;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseTile::.ctor()
extern "C"  void BaseTile__ctor_m3313659900 (BaseTile_t3549052087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
