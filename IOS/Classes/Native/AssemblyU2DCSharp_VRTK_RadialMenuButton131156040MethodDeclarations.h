﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.RadialMenuButton
struct RadialMenuButton_t131156040;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.RadialMenuButton::.ctor()
extern "C"  void RadialMenuButton__ctor_m1845879556 (RadialMenuButton_t131156040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
