﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.VRTK_ControllerUIPointerEvents_ListenerExample
struct  VRTK_ControllerUIPointerEvents_ListenerExample_t3858876453  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.Examples.VRTK_ControllerUIPointerEvents_ListenerExample::togglePointerOnHit
	bool ___togglePointerOnHit_2;

public:
	inline static int32_t get_offset_of_togglePointerOnHit_2() { return static_cast<int32_t>(offsetof(VRTK_ControllerUIPointerEvents_ListenerExample_t3858876453, ___togglePointerOnHit_2)); }
	inline bool get_togglePointerOnHit_2() const { return ___togglePointerOnHit_2; }
	inline bool* get_address_of_togglePointerOnHit_2() { return &___togglePointerOnHit_2; }
	inline void set_togglePointerOnHit_2(bool value)
	{
		___togglePointerOnHit_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
