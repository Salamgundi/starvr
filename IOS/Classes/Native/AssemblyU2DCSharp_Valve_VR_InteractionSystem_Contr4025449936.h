﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// SteamVR_RenderModel
struct SteamVR_RenderModel_t2905485978;
// Valve.VR.InteractionSystem.Player
struct Player_t4256718089;
// System.Collections.Generic.List`1<UnityEngine.MeshRenderer>
struct List_1_t637362236;
// System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>
struct Dictionary_2_t1364189416;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>
struct List_1_t1950442530;
// SteamVR_Events/Action
struct Action_t1836998693;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.ControllerButtonHints
struct  ControllerButtonHints_t4025449936  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material Valve.VR.InteractionSystem.ControllerButtonHints::controllerMaterial
	Material_t193706927 * ___controllerMaterial_2;
	// UnityEngine.Color Valve.VR.InteractionSystem.ControllerButtonHints::flashColor
	Color_t2020392075  ___flashColor_3;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.ControllerButtonHints::textHintPrefab
	GameObject_t1756533147 * ___textHintPrefab_4;
	// System.Boolean Valve.VR.InteractionSystem.ControllerButtonHints::debugHints
	bool ___debugHints_5;
	// SteamVR_RenderModel Valve.VR.InteractionSystem.ControllerButtonHints::renderModel
	SteamVR_RenderModel_t2905485978 * ___renderModel_6;
	// Valve.VR.InteractionSystem.Player Valve.VR.InteractionSystem.ControllerButtonHints::player
	Player_t4256718089 * ___player_7;
	// System.Collections.Generic.List`1<UnityEngine.MeshRenderer> Valve.VR.InteractionSystem.ControllerButtonHints::renderers
	List_1_t637362236 * ___renderers_8;
	// System.Collections.Generic.List`1<UnityEngine.MeshRenderer> Valve.VR.InteractionSystem.ControllerButtonHints::flashingRenderers
	List_1_t637362236 * ___flashingRenderers_9;
	// System.Single Valve.VR.InteractionSystem.ControllerButtonHints::startTime
	float ___startTime_10;
	// System.Single Valve.VR.InteractionSystem.ControllerButtonHints::tickCount
	float ___tickCount_11;
	// System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo> Valve.VR.InteractionSystem.ControllerButtonHints::buttonHintInfos
	Dictionary_2_t1364189416 * ___buttonHintInfos_12;
	// UnityEngine.Transform Valve.VR.InteractionSystem.ControllerButtonHints::textHintParent
	Transform_t3275118058 * ___textHintParent_13;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>> Valve.VR.InteractionSystem.ControllerButtonHints::componentButtonMasks
	List_1_t1950442530 * ___componentButtonMasks_14;
	// System.Int32 Valve.VR.InteractionSystem.ControllerButtonHints::colorID
	int32_t ___colorID_15;
	// System.Boolean Valve.VR.InteractionSystem.ControllerButtonHints::<initialized>k__BackingField
	bool ___U3CinitializedU3Ek__BackingField_16;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.ControllerButtonHints::centerPosition
	Vector3_t2243707580  ___centerPosition_17;
	// SteamVR_Events/Action Valve.VR.InteractionSystem.ControllerButtonHints::renderModelLoadedAction
	Action_t1836998693 * ___renderModelLoadedAction_18;

public:
	inline static int32_t get_offset_of_controllerMaterial_2() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___controllerMaterial_2)); }
	inline Material_t193706927 * get_controllerMaterial_2() const { return ___controllerMaterial_2; }
	inline Material_t193706927 ** get_address_of_controllerMaterial_2() { return &___controllerMaterial_2; }
	inline void set_controllerMaterial_2(Material_t193706927 * value)
	{
		___controllerMaterial_2 = value;
		Il2CppCodeGenWriteBarrier(&___controllerMaterial_2, value);
	}

	inline static int32_t get_offset_of_flashColor_3() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___flashColor_3)); }
	inline Color_t2020392075  get_flashColor_3() const { return ___flashColor_3; }
	inline Color_t2020392075 * get_address_of_flashColor_3() { return &___flashColor_3; }
	inline void set_flashColor_3(Color_t2020392075  value)
	{
		___flashColor_3 = value;
	}

	inline static int32_t get_offset_of_textHintPrefab_4() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___textHintPrefab_4)); }
	inline GameObject_t1756533147 * get_textHintPrefab_4() const { return ___textHintPrefab_4; }
	inline GameObject_t1756533147 ** get_address_of_textHintPrefab_4() { return &___textHintPrefab_4; }
	inline void set_textHintPrefab_4(GameObject_t1756533147 * value)
	{
		___textHintPrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___textHintPrefab_4, value);
	}

	inline static int32_t get_offset_of_debugHints_5() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___debugHints_5)); }
	inline bool get_debugHints_5() const { return ___debugHints_5; }
	inline bool* get_address_of_debugHints_5() { return &___debugHints_5; }
	inline void set_debugHints_5(bool value)
	{
		___debugHints_5 = value;
	}

	inline static int32_t get_offset_of_renderModel_6() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___renderModel_6)); }
	inline SteamVR_RenderModel_t2905485978 * get_renderModel_6() const { return ___renderModel_6; }
	inline SteamVR_RenderModel_t2905485978 ** get_address_of_renderModel_6() { return &___renderModel_6; }
	inline void set_renderModel_6(SteamVR_RenderModel_t2905485978 * value)
	{
		___renderModel_6 = value;
		Il2CppCodeGenWriteBarrier(&___renderModel_6, value);
	}

	inline static int32_t get_offset_of_player_7() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___player_7)); }
	inline Player_t4256718089 * get_player_7() const { return ___player_7; }
	inline Player_t4256718089 ** get_address_of_player_7() { return &___player_7; }
	inline void set_player_7(Player_t4256718089 * value)
	{
		___player_7 = value;
		Il2CppCodeGenWriteBarrier(&___player_7, value);
	}

	inline static int32_t get_offset_of_renderers_8() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___renderers_8)); }
	inline List_1_t637362236 * get_renderers_8() const { return ___renderers_8; }
	inline List_1_t637362236 ** get_address_of_renderers_8() { return &___renderers_8; }
	inline void set_renderers_8(List_1_t637362236 * value)
	{
		___renderers_8 = value;
		Il2CppCodeGenWriteBarrier(&___renderers_8, value);
	}

	inline static int32_t get_offset_of_flashingRenderers_9() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___flashingRenderers_9)); }
	inline List_1_t637362236 * get_flashingRenderers_9() const { return ___flashingRenderers_9; }
	inline List_1_t637362236 ** get_address_of_flashingRenderers_9() { return &___flashingRenderers_9; }
	inline void set_flashingRenderers_9(List_1_t637362236 * value)
	{
		___flashingRenderers_9 = value;
		Il2CppCodeGenWriteBarrier(&___flashingRenderers_9, value);
	}

	inline static int32_t get_offset_of_startTime_10() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___startTime_10)); }
	inline float get_startTime_10() const { return ___startTime_10; }
	inline float* get_address_of_startTime_10() { return &___startTime_10; }
	inline void set_startTime_10(float value)
	{
		___startTime_10 = value;
	}

	inline static int32_t get_offset_of_tickCount_11() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___tickCount_11)); }
	inline float get_tickCount_11() const { return ___tickCount_11; }
	inline float* get_address_of_tickCount_11() { return &___tickCount_11; }
	inline void set_tickCount_11(float value)
	{
		___tickCount_11 = value;
	}

	inline static int32_t get_offset_of_buttonHintInfos_12() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___buttonHintInfos_12)); }
	inline Dictionary_2_t1364189416 * get_buttonHintInfos_12() const { return ___buttonHintInfos_12; }
	inline Dictionary_2_t1364189416 ** get_address_of_buttonHintInfos_12() { return &___buttonHintInfos_12; }
	inline void set_buttonHintInfos_12(Dictionary_2_t1364189416 * value)
	{
		___buttonHintInfos_12 = value;
		Il2CppCodeGenWriteBarrier(&___buttonHintInfos_12, value);
	}

	inline static int32_t get_offset_of_textHintParent_13() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___textHintParent_13)); }
	inline Transform_t3275118058 * get_textHintParent_13() const { return ___textHintParent_13; }
	inline Transform_t3275118058 ** get_address_of_textHintParent_13() { return &___textHintParent_13; }
	inline void set_textHintParent_13(Transform_t3275118058 * value)
	{
		___textHintParent_13 = value;
		Il2CppCodeGenWriteBarrier(&___textHintParent_13, value);
	}

	inline static int32_t get_offset_of_componentButtonMasks_14() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___componentButtonMasks_14)); }
	inline List_1_t1950442530 * get_componentButtonMasks_14() const { return ___componentButtonMasks_14; }
	inline List_1_t1950442530 ** get_address_of_componentButtonMasks_14() { return &___componentButtonMasks_14; }
	inline void set_componentButtonMasks_14(List_1_t1950442530 * value)
	{
		___componentButtonMasks_14 = value;
		Il2CppCodeGenWriteBarrier(&___componentButtonMasks_14, value);
	}

	inline static int32_t get_offset_of_colorID_15() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___colorID_15)); }
	inline int32_t get_colorID_15() const { return ___colorID_15; }
	inline int32_t* get_address_of_colorID_15() { return &___colorID_15; }
	inline void set_colorID_15(int32_t value)
	{
		___colorID_15 = value;
	}

	inline static int32_t get_offset_of_U3CinitializedU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___U3CinitializedU3Ek__BackingField_16)); }
	inline bool get_U3CinitializedU3Ek__BackingField_16() const { return ___U3CinitializedU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CinitializedU3Ek__BackingField_16() { return &___U3CinitializedU3Ek__BackingField_16; }
	inline void set_U3CinitializedU3Ek__BackingField_16(bool value)
	{
		___U3CinitializedU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_centerPosition_17() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___centerPosition_17)); }
	inline Vector3_t2243707580  get_centerPosition_17() const { return ___centerPosition_17; }
	inline Vector3_t2243707580 * get_address_of_centerPosition_17() { return &___centerPosition_17; }
	inline void set_centerPosition_17(Vector3_t2243707580  value)
	{
		___centerPosition_17 = value;
	}

	inline static int32_t get_offset_of_renderModelLoadedAction_18() { return static_cast<int32_t>(offsetof(ControllerButtonHints_t4025449936, ___renderModelLoadedAction_18)); }
	inline Action_t1836998693 * get_renderModelLoadedAction_18() const { return ___renderModelLoadedAction_18; }
	inline Action_t1836998693 ** get_address_of_renderModelLoadedAction_18() { return &___renderModelLoadedAction_18; }
	inline void set_renderModelLoadedAction_18(Action_t1836998693 * value)
	{
		___renderModelLoadedAction_18 = value;
		Il2CppCodeGenWriteBarrier(&___renderModelLoadedAction_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
