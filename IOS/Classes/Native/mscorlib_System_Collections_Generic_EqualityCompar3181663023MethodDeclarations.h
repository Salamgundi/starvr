﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.LogType>
struct DefaultComparer_t3181663023;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.LogType>::.ctor()
extern "C"  void DefaultComparer__ctor_m3775718551_gshared (DefaultComparer_t3181663023 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3775718551(__this, method) ((  void (*) (DefaultComparer_t3181663023 *, const MethodInfo*))DefaultComparer__ctor_m3775718551_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.LogType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3094981414_gshared (DefaultComparer_t3181663023 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3094981414(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3181663023 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m3094981414_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.LogType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3348917982_gshared (DefaultComparer_t3181663023 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3348917982(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3181663023 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3348917982_gshared)(__this, ___x0, ___y1, method)
