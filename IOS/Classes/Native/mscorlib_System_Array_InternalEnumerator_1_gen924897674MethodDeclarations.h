﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen924897674.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRButtonId66145412.h"

// System.Void System.Array/InternalEnumerator`1<Valve.VR.EVRButtonId>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2443074433_gshared (InternalEnumerator_1_t924897674 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2443074433(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t924897674 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2443074433_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.EVRButtonId>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1359216877_gshared (InternalEnumerator_1_t924897674 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1359216877(__this, method) ((  void (*) (InternalEnumerator_1_t924897674 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1359216877_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Valve.VR.EVRButtonId>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m96587157_gshared (InternalEnumerator_1_t924897674 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m96587157(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t924897674 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m96587157_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.EVRButtonId>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3197610568_gshared (InternalEnumerator_1_t924897674 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3197610568(__this, method) ((  void (*) (InternalEnumerator_1_t924897674 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3197610568_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Valve.VR.EVRButtonId>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3914830389_gshared (InternalEnumerator_1_t924897674 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3914830389(__this, method) ((  bool (*) (InternalEnumerator_1_t924897674 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3914830389_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Valve.VR.EVRButtonId>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3281707546_gshared (InternalEnumerator_1_t924897674 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3281707546(__this, method) ((  int32_t (*) (InternalEnumerator_1_t924897674 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3281707546_gshared)(__this, method)
