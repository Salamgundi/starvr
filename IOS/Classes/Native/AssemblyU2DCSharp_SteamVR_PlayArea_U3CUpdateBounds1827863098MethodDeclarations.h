﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_PlayArea/<UpdateBounds>c__Iterator0
struct U3CUpdateBoundsU3Ec__Iterator0_t1827863098;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_PlayArea/<UpdateBounds>c__Iterator0::.ctor()
extern "C"  void U3CUpdateBoundsU3Ec__Iterator0__ctor_m1111242589 (U3CUpdateBoundsU3Ec__Iterator0_t1827863098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_PlayArea/<UpdateBounds>c__Iterator0::MoveNext()
extern "C"  bool U3CUpdateBoundsU3Ec__Iterator0_MoveNext_m401439999 (U3CUpdateBoundsU3Ec__Iterator0_t1827863098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SteamVR_PlayArea/<UpdateBounds>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUpdateBoundsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4119926795 (U3CUpdateBoundsU3Ec__Iterator0_t1827863098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SteamVR_PlayArea/<UpdateBounds>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUpdateBoundsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1906931107 (U3CUpdateBoundsU3Ec__Iterator0_t1827863098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_PlayArea/<UpdateBounds>c__Iterator0::Dispose()
extern "C"  void U3CUpdateBoundsU3Ec__Iterator0_Dispose_m3026516772 (U3CUpdateBoundsU3Ec__Iterator0_t1827863098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_PlayArea/<UpdateBounds>c__Iterator0::Reset()
extern "C"  void U3CUpdateBoundsU3Ec__Iterator0_Reset_m2464346594 (U3CUpdateBoundsU3Ec__Iterator0_t1827863098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
