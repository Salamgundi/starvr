﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRTrackedCamera/_GetCameraErrorNameFromEnum
struct _GetCameraErrorNameFromEnum_t1661571561;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraError3529690400.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRTrackedCamera/_GetCameraErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetCameraErrorNameFromEnum__ctor_m3332400424 (_GetCameraErrorNameFromEnum_t1661571561 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRTrackedCamera/_GetCameraErrorNameFromEnum::Invoke(Valve.VR.EVRTrackedCameraError)
extern "C"  IntPtr_t _GetCameraErrorNameFromEnum_Invoke_m3777825771 (_GetCameraErrorNameFromEnum_t1661571561 * __this, int32_t ___eCameraError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRTrackedCamera/_GetCameraErrorNameFromEnum::BeginInvoke(Valve.VR.EVRTrackedCameraError,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetCameraErrorNameFromEnum_BeginInvoke_m2851961441 (_GetCameraErrorNameFromEnum_t1661571561 * __this, int32_t ___eCameraError0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRTrackedCamera/_GetCameraErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetCameraErrorNameFromEnum_EndInvoke_m3654050979 (_GetCameraErrorNameFromEnum_t1661571561 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
