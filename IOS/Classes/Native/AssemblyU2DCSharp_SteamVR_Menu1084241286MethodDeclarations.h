﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Menu
struct SteamVR_Menu_t1084241286;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Menu::.ctor()
extern "C"  void SteamVR_Menu__ctor_m3553952485 (SteamVR_Menu_t1084241286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture SteamVR_Menu::get_texture()
extern "C"  RenderTexture_t2666733923 * SteamVR_Menu_get_texture_m1482136436 (SteamVR_Menu_t1084241286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR_Menu::get_scale()
extern "C"  float SteamVR_Menu_get_scale_m4292731898 (SteamVR_Menu_t1084241286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Menu::set_scale(System.Single)
extern "C"  void SteamVR_Menu_set_scale_m2226751697 (SteamVR_Menu_t1084241286 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Menu::Awake()
extern "C"  void SteamVR_Menu_Awake_m4187281954 (SteamVR_Menu_t1084241286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Menu::OnGUI()
extern "C"  void SteamVR_Menu_OnGUI_m1599457591 (SteamVR_Menu_t1084241286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Menu::ShowMenu()
extern "C"  void SteamVR_Menu_ShowMenu_m1181966277 (SteamVR_Menu_t1084241286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Menu::HideMenu()
extern "C"  void SteamVR_Menu_HideMenu_m2939457590 (SteamVR_Menu_t1084241286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Menu::Update()
extern "C"  void SteamVR_Menu_Update_m2520293764 (SteamVR_Menu_t1084241286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Menu::SetScale(System.Single)
extern "C"  void SteamVR_Menu_SetScale_m251053228 (SteamVR_Menu_t1084241286 * __this, float ___scale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Menu::SaveCursorState()
extern "C"  void SteamVR_Menu_SaveCursorState_m3485058421 (SteamVR_Menu_t1084241286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Menu::RestoreCursorState()
extern "C"  void SteamVR_Menu_RestoreCursorState_m1664999572 (SteamVR_Menu_t1084241286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
