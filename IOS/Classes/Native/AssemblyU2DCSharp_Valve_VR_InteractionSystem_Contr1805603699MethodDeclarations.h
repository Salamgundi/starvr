﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ControllerButtonHints/<TestTextHints>c__Iterator1
struct U3CTestTextHintsU3Ec__Iterator1_t1805603699;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.ControllerButtonHints/<TestTextHints>c__Iterator1::.ctor()
extern "C"  void U3CTestTextHintsU3Ec__Iterator1__ctor_m1135917334 (U3CTestTextHintsU3Ec__Iterator1_t1805603699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.ControllerButtonHints/<TestTextHints>c__Iterator1::MoveNext()
extern "C"  bool U3CTestTextHintsU3Ec__Iterator1_MoveNext_m3144693258 (U3CTestTextHintsU3Ec__Iterator1_t1805603699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.ControllerButtonHints/<TestTextHints>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTestTextHintsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3991429786 (U3CTestTextHintsU3Ec__Iterator1_t1805603699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.ControllerButtonHints/<TestTextHints>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTestTextHintsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2151467346 (U3CTestTextHintsU3Ec__Iterator1_t1805603699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints/<TestTextHints>c__Iterator1::Dispose()
extern "C"  void U3CTestTextHintsU3Ec__Iterator1_Dispose_m803496961 (U3CTestTextHintsU3Ec__Iterator1_t1805603699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints/<TestTextHints>c__Iterator1::Reset()
extern "C"  void U3CTestTextHintsU3Ec__Iterator1_Reset_m59498791 (U3CTestTextHintsU3Ec__Iterator1_t1805603699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
