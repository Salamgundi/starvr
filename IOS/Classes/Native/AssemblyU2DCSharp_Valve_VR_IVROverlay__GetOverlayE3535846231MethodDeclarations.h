﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayErrorNameFromEnum
struct _GetOverlayErrorNameFromEnum_t3535846231;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayErrorNameFromEnum__ctor_m228589150 (_GetOverlayErrorNameFromEnum_t3535846231 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVROverlay/_GetOverlayErrorNameFromEnum::Invoke(Valve.VR.EVROverlayError)
extern "C"  IntPtr_t _GetOverlayErrorNameFromEnum_Invoke_m1631978224 (_GetOverlayErrorNameFromEnum_t3535846231 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayErrorNameFromEnum::BeginInvoke(Valve.VR.EVROverlayError,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayErrorNameFromEnum_BeginInvoke_m2595245428 (_GetOverlayErrorNameFromEnum_t3535846231 * __this, int32_t ___error0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVROverlay/_GetOverlayErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetOverlayErrorNameFromEnum_EndInvoke_m2732136077 (_GetOverlayErrorNameFromEnum_t3535846231 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
