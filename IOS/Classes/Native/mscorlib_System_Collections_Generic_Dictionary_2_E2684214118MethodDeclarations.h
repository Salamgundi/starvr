﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En972560644MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2468582824(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2684214118 *, Dictionary_2_t1364189416 *, const MethodInfo*))Enumerator__ctor_m4054083607_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m929143319(__this, method) ((  Il2CppObject * (*) (Enumerator_t2684214118 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m623467750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4232380039(__this, method) ((  void (*) (Enumerator_t2684214118 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1160042744_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3432699356(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2684214118 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3244551521_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3326605037(__this, method) ((  Il2CppObject * (*) (Enumerator_t2684214118 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1601136300_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1832544861(__this, method) ((  Il2CppObject * (*) (Enumerator_t2684214118 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m452517118_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::MoveNext()
#define Enumerator_MoveNext_m1460400574(__this, method) ((  bool (*) (Enumerator_t2684214118 *, const MethodInfo*))Enumerator_MoveNext_m2120131144_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::get_Current()
#define Enumerator_get_Current_m970641057(__this, method) ((  KeyValuePair_2_t3416501934  (*) (Enumerator_t2684214118 *, const MethodInfo*))Enumerator_get_Current_m1663316944_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m589343288(__this, method) ((  int32_t (*) (Enumerator_t2684214118 *, const MethodInfo*))Enumerator_get_CurrentKey_m4061335851_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1597754848(__this, method) ((  ButtonHintInfo_t106135473 * (*) (Enumerator_t2684214118 *, const MethodInfo*))Enumerator_get_CurrentValue_m2921613259_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::Reset()
#define Enumerator_Reset_m226873802(__this, method) ((  void (*) (Enumerator_t2684214118 *, const MethodInfo*))Enumerator_Reset_m2558404929_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::VerifyState()
#define Enumerator_VerifyState_m2091652125(__this, method) ((  void (*) (Enumerator_t2684214118 *, const MethodInfo*))Enumerator_VerifyState_m549837356_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m240794423(__this, method) ((  void (*) (Enumerator_t2684214118 *, const MethodInfo*))Enumerator_VerifyCurrent_m1820742208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::Dispose()
#define Enumerator_Dispose_m2323034940(__this, method) ((  void (*) (Enumerator_t2684214118 *, const MethodInfo*))Enumerator_Dispose_m2015859619_gshared)(__this, method)
