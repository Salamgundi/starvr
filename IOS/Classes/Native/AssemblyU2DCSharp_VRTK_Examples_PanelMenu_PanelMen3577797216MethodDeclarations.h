﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.PanelMenu.PanelMenuDemoFlyingSaucer
struct PanelMenuDemoFlyingSaucer_t3577797216;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.Examples.PanelMenu.PanelMenuDemoFlyingSaucer::.ctor()
extern "C"  void PanelMenuDemoFlyingSaucer__ctor_m3947250040 (PanelMenuDemoFlyingSaucer_t3577797216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.PanelMenu.PanelMenuDemoFlyingSaucer::UpdateGridLayoutValue(System.Int32)
extern "C"  void PanelMenuDemoFlyingSaucer_UpdateGridLayoutValue_m3982030099 (PanelMenuDemoFlyingSaucer_t3577797216 * __this, int32_t ___selectedIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
