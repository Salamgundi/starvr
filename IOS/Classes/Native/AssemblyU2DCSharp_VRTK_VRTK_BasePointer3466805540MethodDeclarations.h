﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_BasePointer
struct VRTK_BasePointer_t3466805540;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"

// System.Void VRTK.VRTK_BasePointer::.ctor()
extern "C"  void VRTK_BasePointer__ctor_m493699856 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BasePointer::IsActive()
extern "C"  bool VRTK_BasePointer_IsActive_m1539531514 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BasePointer::CanActivate()
extern "C"  bool VRTK_BasePointer_CanActivate_m775793141 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::ToggleBeam(System.Boolean)
extern "C"  void VRTK_BasePointer_ToggleBeam_m236508848 (VRTK_BasePointer_t3466805540 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::Awake()
extern "C"  void VRTK_BasePointer_Awake_m601327689 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::OnEnable()
extern "C"  void VRTK_BasePointer_OnEnable_m3423012872 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::OnDisable()
extern "C"  void VRTK_BasePointer_OnDisable_m1734521689 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::Start()
extern "C"  void VRTK_BasePointer_Start_m1470165064 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::Update()
extern "C"  void VRTK_BasePointer_Update_m4230927853 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::FixedUpdate()
extern "C"  void VRTK_BasePointer_FixedUpdate_m2383160739 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::AliasRegistration(System.Boolean)
extern "C"  void VRTK_BasePointer_AliasRegistration_m431885860 (VRTK_BasePointer_t3466805540 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::OnValidate()
extern "C"  void VRTK_BasePointer_OnValidate_m4264227323 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_BasePointer::GetOrigin(System.Boolean)
extern "C"  Transform_t3275118058 * VRTK_BasePointer_GetOrigin_m2090108841 (VRTK_BasePointer_t3466805540 * __this, bool ___smoothed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::UpdateObjectInteractor()
extern "C"  void VRTK_BasePointer_UpdateObjectInteractor_m788204331 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::UpdatePointerOriginTransformFollow()
extern "C"  void VRTK_BasePointer_UpdatePointerOriginTransformFollow_m1206499235 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::InitPointer()
extern "C"  void VRTK_BasePointer_InitPointer_m1870606253 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::UpdateDependencies(UnityEngine.Vector3)
extern "C"  void VRTK_BasePointer_UpdateDependencies_m1182257329 (VRTK_BasePointer_t3466805540 * __this, Vector3_t2243707580  ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::EnablePointerBeam(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_BasePointer_EnablePointerBeam_m1697886647 (VRTK_BasePointer_t3466805540 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::DisablePointerBeam(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_BasePointer_DisablePointerBeam_m3786400976 (VRTK_BasePointer_t3466805540 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::SetPointerDestination(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_BasePointer_SetPointerDestination_m2366379387 (VRTK_BasePointer_t3466805540 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::PointerIn()
extern "C"  void VRTK_BasePointer_PointerIn_m3526437662 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::PointerOut()
extern "C"  void VRTK_BasePointer_PointerOut_m2565931153 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::PointerSet()
extern "C"  void VRTK_BasePointer_PointerSet_m529542901 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::TogglePointer(System.Boolean)
extern "C"  void VRTK_BasePointer_TogglePointer_m1676809656 (VRTK_BasePointer_t3466805540 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::ToggleObjectInteraction(System.Boolean)
extern "C"  void VRTK_BasePointer_ToggleObjectInteraction_m905302956 (VRTK_BasePointer_t3466805540 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::ChangeMaterialColor(UnityEngine.GameObject,UnityEngine.Color)
extern "C"  void VRTK_BasePointer_ChangeMaterialColor_m4143188604 (VRTK_BasePointer_t3466805540 * __this, GameObject_t1756533147 * ___obj0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::SetPointerMaterial(UnityEngine.Color)
extern "C"  void VRTK_BasePointer_SetPointerMaterial_m3679577218 (VRTK_BasePointer_t3466805540 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::UpdatePointerMaterial(UnityEngine.Color)
extern "C"  void VRTK_BasePointer_UpdatePointerMaterial_m2117971247 (VRTK_BasePointer_t3466805540 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BasePointer::ValidDestination(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  bool VRTK_BasePointer_ValidDestination_m1108260200 (VRTK_BasePointer_t3466805540 * __this, Transform_t3275118058 * ___target0, Vector3_t2243707580  ___destinationPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::CreateObjectInteractor()
extern "C"  void VRTK_BasePointer_CreateObjectInteractor_m1162110346 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::CreatePointerOriginTransformFollow()
extern "C"  void VRTK_BasePointer_CreatePointerOriginTransformFollow_m3890793066 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_BasePointer::OverrideBeamLength(System.Single)
extern "C"  float VRTK_BasePointer_OverrideBeamLength_m928846972 (VRTK_BasePointer_t3466805540 * __this, float ___currentLength0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::SetupController()
extern "C"  void VRTK_BasePointer_SetupController_m1345284635 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::AttemptSetController()
extern "C"  void VRTK_BasePointer_AttemptSetController_m205571991 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BasePointer::InvalidConstantBeam()
extern "C"  bool VRTK_BasePointer_InvalidConstantBeam_m3833040622 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BasePointer::PointerActivatesUseAction(VRTK.VRTK_InteractableObject)
extern "C"  bool VRTK_BasePointer_PointerActivatesUseAction_m1997883436 (VRTK_BasePointer_t3466805540 * __this, VRTK_InteractableObject_t2604188111 * ___io0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::StartUseAction(UnityEngine.Transform)
extern "C"  void VRTK_BasePointer_StartUseAction_m4043337440 (VRTK_BasePointer_t3466805540 * __this, Transform_t3275118058 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::StopUseAction()
extern "C"  void VRTK_BasePointer_StopUseAction_m2019204337 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::TurnOnBeam(System.UInt32)
extern "C"  void VRTK_BasePointer_TurnOnBeam_m324883823 (VRTK_BasePointer_t3466805540 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::TurnOffBeam(System.UInt32)
extern "C"  void VRTK_BasePointer_TurnOffBeam_m2996846189 (VRTK_BasePointer_t3466805540 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointer::DisableBeam()
extern "C"  void VRTK_BasePointer_DisableBeam_m3333184011 (VRTK_BasePointer_t3466805540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
