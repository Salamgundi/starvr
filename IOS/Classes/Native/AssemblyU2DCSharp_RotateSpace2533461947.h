﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateSpace
struct  RotateSpace_t2533461947  : public MonoBehaviour_t1158329972
{
public:
	// System.Single RotateSpace::degrees
	float ___degrees_2;
	// System.Single RotateSpace::rotatespeed
	float ___rotatespeed_3;
	// System.Boolean RotateSpace::Paused
	bool ___Paused_4;
	// System.Boolean RotateSpace::North
	bool ___North_5;

public:
	inline static int32_t get_offset_of_degrees_2() { return static_cast<int32_t>(offsetof(RotateSpace_t2533461947, ___degrees_2)); }
	inline float get_degrees_2() const { return ___degrees_2; }
	inline float* get_address_of_degrees_2() { return &___degrees_2; }
	inline void set_degrees_2(float value)
	{
		___degrees_2 = value;
	}

	inline static int32_t get_offset_of_rotatespeed_3() { return static_cast<int32_t>(offsetof(RotateSpace_t2533461947, ___rotatespeed_3)); }
	inline float get_rotatespeed_3() const { return ___rotatespeed_3; }
	inline float* get_address_of_rotatespeed_3() { return &___rotatespeed_3; }
	inline void set_rotatespeed_3(float value)
	{
		___rotatespeed_3 = value;
	}

	inline static int32_t get_offset_of_Paused_4() { return static_cast<int32_t>(offsetof(RotateSpace_t2533461947, ___Paused_4)); }
	inline bool get_Paused_4() const { return ___Paused_4; }
	inline bool* get_address_of_Paused_4() { return &___Paused_4; }
	inline void set_Paused_4(bool value)
	{
		___Paused_4 = value;
	}

	inline static int32_t get_offset_of_North_5() { return static_cast<int32_t>(offsetof(RotateSpace_t2533461947, ___North_5)); }
	inline bool get_North_5() const { return ___North_5; }
	inline bool* get_address_of_North_5() { return &___North_5; }
	inline void set_North_5(bool value)
	{
		___North_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
