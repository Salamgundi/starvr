﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Camera
struct SteamVR_Camera_t3632348390;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Camera
struct Camera_t189460977;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"

// System.Void SteamVR_Camera::.ctor()
extern "C"  void SteamVR_Camera__ctor_m4211270275 (SteamVR_Camera_t3632348390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform SteamVR_Camera::get_head()
extern "C"  Transform_t3275118058 * SteamVR_Camera_get_head_m93562096 (SteamVR_Camera_t3632348390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform SteamVR_Camera::get_offset()
extern "C"  Transform_t3275118058 * SteamVR_Camera_get_offset_m1393188523 (SteamVR_Camera_t3632348390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform SteamVR_Camera::get_origin()
extern "C"  Transform_t3275118058 * SteamVR_Camera_get_origin_m2125632028 (SteamVR_Camera_t3632348390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera SteamVR_Camera::get_camera()
extern "C"  Camera_t189460977 * SteamVR_Camera_get_camera_m751295804 (SteamVR_Camera_t3632348390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Camera::set_camera(UnityEngine.Camera)
extern "C"  void SteamVR_Camera_set_camera_m3289054497 (SteamVR_Camera_t3632348390 * __this, Camera_t189460977 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform SteamVR_Camera::get_ears()
extern "C"  Transform_t3275118058 * SteamVR_Camera_get_ears_m3382259239 (SteamVR_Camera_t3632348390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray SteamVR_Camera::GetRay()
extern "C"  Ray_t2469606224  SteamVR_Camera_GetRay_m4173243133 (SteamVR_Camera_t3632348390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR_Camera::get_sceneResolutionScale()
extern "C"  float SteamVR_Camera_get_sceneResolutionScale_m2434281766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Camera::set_sceneResolutionScale(System.Single)
extern "C"  void SteamVR_Camera_set_sceneResolutionScale_m2494135051 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Camera::OnDisable()
extern "C"  void SteamVR_Camera_OnDisable_m983856770 (SteamVR_Camera_t3632348390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Camera::OnEnable()
extern "C"  void SteamVR_Camera_OnEnable_m4109905127 (SteamVR_Camera_t3632348390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Camera::Awake()
extern "C"  void SteamVR_Camera_Awake_m3577940934 (SteamVR_Camera_t3632348390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Camera::ForceLast()
extern "C"  void SteamVR_Camera_ForceLast_m2795903274 (SteamVR_Camera_t3632348390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SteamVR_Camera::get_baseName()
extern "C"  String_t* SteamVR_Camera_get_baseName_m487852293 (SteamVR_Camera_t3632348390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Camera::Expand()
extern "C"  void SteamVR_Camera_Expand_m2175730657 (SteamVR_Camera_t3632348390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Camera::Collapse()
extern "C"  void SteamVR_Camera_Collapse_m268699926 (SteamVR_Camera_t3632348390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
