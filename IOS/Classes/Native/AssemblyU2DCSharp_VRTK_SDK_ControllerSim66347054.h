﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.SDK_ControllerSim
struct  SDK_ControllerSim_t66347054  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 VRTK.SDK_ControllerSim::lastPos
	Vector3_t2243707580  ___lastPos_2;
	// UnityEngine.Vector3 VRTK.SDK_ControllerSim::lastRot
	Vector3_t2243707580  ___lastRot_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> VRTK.SDK_ControllerSim::posList
	List_1_t1612828712 * ___posList_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> VRTK.SDK_ControllerSim::rotList
	List_1_t1612828712 * ___rotList_5;
	// System.Boolean VRTK.SDK_ControllerSim::selected
	bool ___selected_6;

public:
	inline static int32_t get_offset_of_lastPos_2() { return static_cast<int32_t>(offsetof(SDK_ControllerSim_t66347054, ___lastPos_2)); }
	inline Vector3_t2243707580  get_lastPos_2() const { return ___lastPos_2; }
	inline Vector3_t2243707580 * get_address_of_lastPos_2() { return &___lastPos_2; }
	inline void set_lastPos_2(Vector3_t2243707580  value)
	{
		___lastPos_2 = value;
	}

	inline static int32_t get_offset_of_lastRot_3() { return static_cast<int32_t>(offsetof(SDK_ControllerSim_t66347054, ___lastRot_3)); }
	inline Vector3_t2243707580  get_lastRot_3() const { return ___lastRot_3; }
	inline Vector3_t2243707580 * get_address_of_lastRot_3() { return &___lastRot_3; }
	inline void set_lastRot_3(Vector3_t2243707580  value)
	{
		___lastRot_3 = value;
	}

	inline static int32_t get_offset_of_posList_4() { return static_cast<int32_t>(offsetof(SDK_ControllerSim_t66347054, ___posList_4)); }
	inline List_1_t1612828712 * get_posList_4() const { return ___posList_4; }
	inline List_1_t1612828712 ** get_address_of_posList_4() { return &___posList_4; }
	inline void set_posList_4(List_1_t1612828712 * value)
	{
		___posList_4 = value;
		Il2CppCodeGenWriteBarrier(&___posList_4, value);
	}

	inline static int32_t get_offset_of_rotList_5() { return static_cast<int32_t>(offsetof(SDK_ControllerSim_t66347054, ___rotList_5)); }
	inline List_1_t1612828712 * get_rotList_5() const { return ___rotList_5; }
	inline List_1_t1612828712 ** get_address_of_rotList_5() { return &___rotList_5; }
	inline void set_rotList_5(List_1_t1612828712 * value)
	{
		___rotList_5 = value;
		Il2CppCodeGenWriteBarrier(&___rotList_5, value);
	}

	inline static int32_t get_offset_of_selected_6() { return static_cast<int32_t>(offsetof(SDK_ControllerSim_t66347054, ___selected_6)); }
	inline bool get_selected_6() const { return ___selected_6; }
	inline bool* get_address_of_selected_6() { return &___selected_6; }
	inline void set_selected_6(bool value)
	{
		___selected_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
