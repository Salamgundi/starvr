﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Controller_Hand_Han584661213.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Controller_Menu102574584.h"
#include "AssemblyU2DCSharp_VRTK_Examples_ExcludeTeleport515985511.h"
#include "AssemblyU2DCSharp_VRTK_Examples_FireExtinguisher_B3980302883.h"
#include "AssemblyU2DCSharp_VRTK_Examples_FireExtinguisher_S1157307020.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Gun3058999492.h"
#include "AssemblyU2DCSharp_VRTK_Examples_HandLift3915206274.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Lamp3844780392.h"
#include "AssemblyU2DCSharp_VRTK_Examples_LightSaber2639943817.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Menu_Color_Changer2255102764.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Menu_Container_Obj1737726098.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Menu_Object_Spawne1547505270.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Menu_Object_Spawne2203986029.h"
#include "AssemblyU2DCSharp_VRTK_Examples_ModelVillage_Telep1456982994.h"
#include "AssemblyU2DCSharp_MoveBlock1905298658.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Openable_Door677159015.h"
#include "AssemblyU2DCSharp_VRTK_Examples_PanelMenu_PanelMen3577797216.h"
#include "AssemblyU2DCSharp_VRTK_Examples_PanelMenu_PanelMen1711579793.h"
#include "AssemblyU2DCSharp_VRTK_Examples_PanelMenu_PanelMenu729840129.h"
#include "AssemblyU2DCSharp_VRTK_Examples_PanelMenu_PanelMenu904549598.h"
#include "AssemblyU2DCSharp_VRTK_Examples_PanelMenu_PanelMen2320851018.h"
#include "AssemblyU2DCSharp_VRTK_Examples_RC_Car3139601232.h"
#include "AssemblyU2DCSharp_VRTK_Examples_RC_Car_Controller2574376713.h"
#include "AssemblyU2DCSharp_VRTK_Examples_RealGun2394028608.h"
#include "AssemblyU2DCSharp_VRTK_Examples_RealGun_SafetySwitc476099707.h"
#include "AssemblyU2DCSharp_VRTK_Examples_RealGun_Slide2998553338.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Remote_Beam3772907446.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Remote_Beam_Contro3376256279.h"
#include "AssemblyU2DCSharp_VRTK_Examples_RendererOffOnDash2445887305.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Utilities_SceneCha1690824060.h"
#include "AssemblyU2DCSharp_VRTK_Examples_SnapDropZoneGroup_S643500282.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Sphere_Spawner399643700.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Sword3542381029.h"
#include "AssemblyU2DCSharp_VRTK_Examples_UI_Interactions2908460892.h"
#include "AssemblyU2DCSharp_VRTK_Examples_UI_Keyboard3022578864.h"
#include "AssemblyU2DCSharp_VRTK_Examples_UseRotate241092046.h"
#include "AssemblyU2DCSharp_VRTK_Examples_VRTK_ControllerApp4148578061.h"
#include "AssemblyU2DCSharp_VRTK_Examples_VRTK_ControllerEven190763384.h"
#include "AssemblyU2DCSharp_VRTK_Examples_VRTK_ControllerInt3727600851.h"
#include "AssemblyU2DCSharp_VRTK_Examples_VRTK_ControllerPoin261181245.h"
#include "AssemblyU2DCSharp_VRTK_Examples_VRTK_ControllerUIP3858876453.h"
#include "AssemblyU2DCSharp_VRTK_Examples_VRTK_RoomExtender_3110057233.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Whirlygig2495117162.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Zipline3027761465.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ConsoleViewer2446975743.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerRigidbodyAct3039396590.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerTooltips3537184660.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerTooltips_Too2877834378.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DestinationPoint1547370418.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DestinationPoint_U3CMa1570204165.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DestinationPoint_U3CDo3301794915.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_FramesPerSecondViewer2787247731.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectTooltip333831714.h"
#include "AssemblyU2DCSharp_VRTK_SnapDropZoneEventArgs418702774.h"
#include "AssemblyU2DCSharp_VRTK_SnapDropZoneEventHandler172258073.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SnapDropZone1948041105.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SnapDropZone_SnapTypes2013855124.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SnapDropZone_U3CUpdateT664611402.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SnapDropZone_U3CAttemp3586163035.h"
#include "AssemblyU2DCSharp_VRTK_SDK_BaseBoundaries1766380066.h"
#include "AssemblyU2DCSharp_VRTK_SDK_BaseController197168236.h"
#include "AssemblyU2DCSharp_VRTK_SDK_BaseController_ButtonPr1850327506.h"
#include "AssemblyU2DCSharp_VRTK_SDK_BaseController_Controlle447683143.h"
#include "AssemblyU2DCSharp_VRTK_SDK_BaseController_Controlle246418309.h"
#include "AssemblyU2DCSharp_VRTK_SDK_ControllerHapticModifie3871094838.h"
#include "AssemblyU2DCSharp_VRTK_SDK_BaseHeadset3046873914.h"
#include "AssemblyU2DCSharp_VRTK_SDK_BaseSystem244469351.h"
#include "AssemblyU2DCSharp_VRTK_SDK_DaydreamBoundaries2613138380.h"
#include "AssemblyU2DCSharp_VRTK_SDK_DaydreamController2403190306.h"
#include "AssemblyU2DCSharp_VRTK_SDK_DaydreamHeadset1169140976.h"
#include "AssemblyU2DCSharp_VRTK_SDK_DaydreamSystem808099371.h"
#include "AssemblyU2DCSharp_VRTK_SDK_FallbackBoundaries1566319879.h"
#include "AssemblyU2DCSharp_VRTK_SDK_FallbackController773334429.h"
#include "AssemblyU2DCSharp_VRTK_SDK_FallbackHeadset1089992503.h"
#include "AssemblyU2DCSharp_VRTK_SDK_FallbackSystem1344157064.h"
#include "AssemblyU2DCSharp_VRTK_SDK_OculusVRBoundaries3279427364.h"
#include "AssemblyU2DCSharp_VRTK_SDK_OculusVRController2534364654.h"
#include "AssemblyU2DCSharp_VRTK_SDK_OculusVRHeadset2077937592.h"
#include "AssemblyU2DCSharp_VRTK_SDK_OculusVRSystem1733635573.h"
#include "AssemblyU2DCSharp_VRTK_SDK_ControllerSim66347054.h"
#include "AssemblyU2DCSharp_VRTK_SDK_InputSimulator2184297317.h"
#include "AssemblyU2DCSharp_VRTK_SDK_SimBoundaries3645911520.h"
#include "AssemblyU2DCSharp_VRTK_SDK_SimController33406942.h"
#include "AssemblyU2DCSharp_VRTK_SDK_SimHeadset4149936844.h"
#include "AssemblyU2DCSharp_VRTK_SDK_SimSystem2203361345.h"
#include "AssemblyU2DCSharp_VRTK_SDK_SteamVRBoundaries1132496137.h"
#include "AssemblyU2DCSharp_VRTK_SDK_SteamVRController3691290795.h"
#include "AssemblyU2DCSharp_VRTK_SDK_SteamVRHeadset524129709.h"
#include "AssemblyU2DCSharp_VRTK_SDK_SteamVRSystem50103670.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SDK_Bridge2841169330.h"
#include "AssemblyU2DCSharp_VRTK_PanelMenuController2234710621.h"
#include "AssemblyU2DCSharp_VRTK_PanelMenuController_Touchpa1242655431.h"
#include "AssemblyU2DCSharp_VRTK_PanelMenuController_U3CTwee3216112136.h"
#include "AssemblyU2DCSharp_VRTK_PanelMenuItemControllerEven2917504033.h"
#include "AssemblyU2DCSharp_VRTK_PanelMenuItemControllerEvent780308162.h"
#include "AssemblyU2DCSharp_VRTK_PanelMenuItemController3837844790.h"
#include "AssemblyU2DCSharp_VRTK_RadialButtonIcon700423124.h"
#include "AssemblyU2DCSharp_VRTK_HapticPulseEventHandler422205650.h"
#include "AssemblyU2DCSharp_VRTK_RadialMenu1576296262.h"
#include "AssemblyU2DCSharp_VRTK_RadialMenu_U3CTweenMenuScale701686089.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (Hands_t584661213)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2700[3] = 
{
	Hands_t584661213::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (Controller_Menu_t102574584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[4] = 
{
	Controller_Menu_t102574584::get_offset_of_menuObject_2(),
	Controller_Menu_t102574584::get_offset_of_clonedMenuObject_3(),
	Controller_Menu_t102574584::get_offset_of_menuInit_4(),
	Controller_Menu_t102574584::get_offset_of_menuActive_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (ExcludeTeleport_t515985511), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (FireExtinguisher_Base_t3980302883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[4] = 
{
	FireExtinguisher_Base_t3980302883::get_offset_of_leverAnimation_45(),
	FireExtinguisher_Base_t3980302883::get_offset_of_sprayer_46(),
	FireExtinguisher_Base_t3980302883::get_offset_of_controllerEvents_47(),
	FireExtinguisher_Base_t3980302883::get_offset_of_controllerActions_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (FireExtinguisher_Sprayer_t1157307020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[5] = 
{
	FireExtinguisher_Sprayer_t1157307020::get_offset_of_baseCan_45(),
	FireExtinguisher_Sprayer_t1157307020::get_offset_of_breakDistance_46(),
	FireExtinguisher_Sprayer_t1157307020::get_offset_of_maxSprayPower_47(),
	FireExtinguisher_Sprayer_t1157307020::get_offset_of_waterSpray_48(),
	FireExtinguisher_Sprayer_t1157307020::get_offset_of_particles_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (Gun_t3058999492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[3] = 
{
	Gun_t3058999492::get_offset_of_bullet_45(),
	Gun_t3058999492::get_offset_of_bulletSpeed_46(),
	Gun_t3058999492::get_offset_of_bulletLife_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (HandLift_t3915206274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2706[8] = 
{
	HandLift_t3915206274::get_offset_of_speed_45(),
	HandLift_t3915206274::get_offset_of_handleTop_46(),
	HandLift_t3915206274::get_offset_of_ropeTop_47(),
	HandLift_t3915206274::get_offset_of_ropeBottom_48(),
	HandLift_t3915206274::get_offset_of_rope_49(),
	HandLift_t3915206274::get_offset_of_handle_50(),
	HandLift_t3915206274::get_offset_of_isMoving_51(),
	HandLift_t3915206274::get_offset_of_isMovingUp_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (Lamp_t3844780392), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (LightSaber_t2639943817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[8] = 
{
	LightSaber_t2639943817::get_offset_of_beamActive_45(),
	LightSaber_t2639943817::get_offset_of_beamLimits_46(),
	LightSaber_t2639943817::get_offset_of_currentBeamSize_47(),
	LightSaber_t2639943817::get_offset_of_beamExtendSpeed_48(),
	LightSaber_t2639943817::get_offset_of_blade_49(),
	LightSaber_t2639943817::get_offset_of_activeColor_50(),
	LightSaber_t2639943817::get_offset_of_targetColor_51(),
	LightSaber_t2639943817::get_offset_of_bladePhaseColors_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (Menu_Color_Changer_t2255102764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2709[1] = 
{
	Menu_Color_Changer_t2255102764::get_offset_of_newMenuColor_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (Menu_Container_Object_Colors_t1737726098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (Menu_Object_Spawner_t1547505270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[2] = 
{
	Menu_Object_Spawner_t1547505270::get_offset_of_shape_45(),
	Menu_Object_Spawner_t1547505270::get_offset_of_selectedColor_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (PrimitiveTypes_t2203986029)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2712[3] = 
{
	PrimitiveTypes_t2203986029::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (ModelVillage_TeleportLocation_t1456982994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[2] = 
{
	ModelVillage_TeleportLocation_t1456982994::get_offset_of_destination_9(),
	ModelVillage_TeleportLocation_t1456982994::get_offset_of_lastUsePressedState_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (MoveBlock_t1905298658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[8] = 
{
	MoveBlock_t1905298658::get_offset_of_moveYAmount_2(),
	MoveBlock_t1905298658::get_offset_of_moveSpeed_3(),
	MoveBlock_t1905298658::get_offset_of_waitTime_4(),
	MoveBlock_t1905298658::get_offset_of_rotateSpeed_5(),
	MoveBlock_t1905298658::get_offset_of_startY_6(),
	MoveBlock_t1905298658::get_offset_of_goingUp_7(),
	MoveBlock_t1905298658::get_offset_of_stoppedUntilTime_8(),
	MoveBlock_t1905298658::get_offset_of_moveUpAmount_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (Openable_Door_t677159015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2715[9] = 
{
	Openable_Door_t677159015::get_offset_of_flipped_45(),
	Openable_Door_t677159015::get_offset_of_rotated_46(),
	Openable_Door_t677159015::get_offset_of_sideFlip_47(),
	Openable_Door_t677159015::get_offset_of_side_48(),
	Openable_Door_t677159015::get_offset_of_smooth_49(),
	Openable_Door_t677159015::get_offset_of_doorOpenAngle_50(),
	Openable_Door_t677159015::get_offset_of_open_51(),
	Openable_Door_t677159015::get_offset_of_defaultRotation_52(),
	Openable_Door_t677159015::get_offset_of_openRotation_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (PanelMenuDemoFlyingSaucer_t3577797216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2716[1] = 
{
	PanelMenuDemoFlyingSaucer_t3577797216::get_offset_of_colors_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (PanelMenuDemoSphere_t1711579793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2717[1] = 
{
	PanelMenuDemoSphere_t1711579793::get_offset_of_colors_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (PanelMenuUIGrid_t729840129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2718[5] = 
{
	PanelMenuUIGrid_t729840129::get_offset_of_colorDefault_2(),
	PanelMenuUIGrid_t729840129::get_offset_of_colorSelected_3(),
	PanelMenuUIGrid_t729840129::get_offset_of_colorAlpha_4(),
	PanelMenuUIGrid_t729840129::get_offset_of_gridLayoutGroup_5(),
	PanelMenuUIGrid_t729840129::get_offset_of_selectedIndex_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (Direction_t904549598)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2719[6] = 
{
	Direction_t904549598::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (PanelMenuUISlider_t2320851018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2720[1] = 
{
	PanelMenuUISlider_t2320851018::get_offset_of_slider_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (RC_Car_t3139601232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[11] = 
{
	RC_Car_t3139601232::get_offset_of_maxAcceleration_2(),
	RC_Car_t3139601232::get_offset_of_jumpPower_3(),
	RC_Car_t3139601232::get_offset_of_acceleration_4(),
	RC_Car_t3139601232::get_offset_of_movementSpeed_5(),
	RC_Car_t3139601232::get_offset_of_rotationSpeed_6(),
	RC_Car_t3139601232::get_offset_of_isJumping_7(),
	RC_Car_t3139601232::get_offset_of_touchAxis_8(),
	RC_Car_t3139601232::get_offset_of_triggerAxis_9(),
	RC_Car_t3139601232::get_offset_of_rb_10(),
	RC_Car_t3139601232::get_offset_of_defaultPosition_11(),
	RC_Car_t3139601232::get_offset_of_defaultRotation_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (RC_Car_Controller_t2574376713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[2] = 
{
	RC_Car_Controller_t2574376713::get_offset_of_rcCar_2(),
	RC_Car_Controller_t2574376713::get_offset_of_rcCarScript_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (RealGun_t2394028608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2723[14] = 
{
	RealGun_t2394028608::get_offset_of_bulletSpeed_45(),
	RealGun_t2394028608::get_offset_of_bulletLife_46(),
	RealGun_t2394028608::get_offset_of_bullet_47(),
	RealGun_t2394028608::get_offset_of_trigger_48(),
	RealGun_t2394028608::get_offset_of_slide_49(),
	RealGun_t2394028608::get_offset_of_safetySwitch_50(),
	RealGun_t2394028608::get_offset_of_slideRigidbody_51(),
	RealGun_t2394028608::get_offset_of_slideCollider_52(),
	RealGun_t2394028608::get_offset_of_safetySwitchRigidbody_53(),
	RealGun_t2394028608::get_offset_of_safetySwitchCollider_54(),
	RealGun_t2394028608::get_offset_of_controllerEvents_55(),
	RealGun_t2394028608::get_offset_of_controllerActions_56(),
	RealGun_t2394028608::get_offset_of_minTriggerRotation_57(),
	RealGun_t2394028608::get_offset_of_maxTriggerRotation_58(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (RealGun_SafetySwitch_t476099707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2724[3] = 
{
	RealGun_SafetySwitch_t476099707::get_offset_of_safetyOff_45(),
	RealGun_SafetySwitch_t476099707::get_offset_of_offAngle_46(),
	RealGun_SafetySwitch_t476099707::get_offset_of_fixedPosition_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (RealGun_Slide_t2998553338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[4] = 
{
	RealGun_Slide_t2998553338::get_offset_of_restPosition_45(),
	RealGun_Slide_t2998553338::get_offset_of_fireTimer_46(),
	RealGun_Slide_t2998553338::get_offset_of_fireDistance_47(),
	RealGun_Slide_t2998553338::get_offset_of_boltSpeed_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (Remote_Beam_t3772907446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2726[4] = 
{
	Remote_Beam_t3772907446::get_offset_of_touchAxis_2(),
	Remote_Beam_t3772907446::get_offset_of_rotationSpeed_3(),
	Remote_Beam_t3772907446::get_offset_of_currentYaw_4(),
	Remote_Beam_t3772907446::get_offset_of_currentPitch_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (Remote_Beam_Controller_t3376256279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[2] = 
{
	Remote_Beam_Controller_t3376256279::get_offset_of_remoteBeam_2(),
	Remote_Beam_Controller_t3376256279::get_offset_of_remoteBeamScript_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (RendererOffOnDash_t2445887305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[2] = 
{
	RendererOffOnDash_t2445887305::get_offset_of_wasSwitchedOff_2(),
	RendererOffOnDash_t2445887305::get_offset_of_dashTeleporters_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (SceneChanger_t1690824060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2729[2] = 
{
	SceneChanger_t1690824060::get_offset_of_canPress_2(),
	SceneChanger_t1690824060::get_offset_of_controllerIndex_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (SnapDropZoneGroup_Switcher_t643500282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[2] = 
{
	SnapDropZoneGroup_Switcher_t643500282::get_offset_of_cubeZone_2(),
	SnapDropZoneGroup_Switcher_t643500282::get_offset_of_sphereZone_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (Sphere_Spawner_t399643700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2731[2] = 
{
	Sphere_Spawner_t399643700::get_offset_of_spawnMe_2(),
	Sphere_Spawner_t399643700::get_offset_of_position_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (Sword_t3542381029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[4] = 
{
	Sword_t3542381029::get_offset_of_controllerActions_45(),
	Sword_t3542381029::get_offset_of_impactMagnifier_46(),
	Sword_t3542381029::get_offset_of_collisionForce_47(),
	Sword_t3542381029::get_offset_of_maxCollisionForce_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (UI_Interactions_t2908460892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2733[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (UI_Keyboard_t3022578864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[1] = 
{
	UI_Keyboard_t3022578864::get_offset_of_input_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (UseRotate_t241092046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[5] = 
{
	UseRotate_t241092046::get_offset_of_idleSpinSpeed_45(),
	UseRotate_t241092046::get_offset_of_activeSpinSpeed_46(),
	UseRotate_t241092046::get_offset_of_rotatingObject_47(),
	UseRotate_t241092046::get_offset_of_rotationAxis_48(),
	UseRotate_t241092046::get_offset_of_spinSpeed_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (VRTK_ControllerAppearance_Example_t4148578061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2736[4] = 
{
	VRTK_ControllerAppearance_Example_t4148578061::get_offset_of_highlightBodyOnlyOnCollision_2(),
	VRTK_ControllerAppearance_Example_t4148578061::get_offset_of_tooltips_3(),
	VRTK_ControllerAppearance_Example_t4148578061::get_offset_of_actions_4(),
	VRTK_ControllerAppearance_Example_t4148578061::get_offset_of_events_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (VRTK_ControllerEvents_ListenerExample_t190763384), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (VRTK_ControllerInteract_ListenerExample_t3727600851), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (VRTK_ControllerPointerEvents_ListenerExample_t261181245), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (VRTK_ControllerUIPointerEvents_ListenerExample_t3858876453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[1] = 
{
	VRTK_ControllerUIPointerEvents_ListenerExample_t3858876453::get_offset_of_togglePointerOnHit_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (VRTK_RoomExtender_ControllerExample_t3110057233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[1] = 
{
	VRTK_RoomExtender_ControllerExample_t3110057233::get_offset_of_roomExtender_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (Whirlygig_t2495117162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2742[2] = 
{
	Whirlygig_t2495117162::get_offset_of_spinSpeed_45(),
	Whirlygig_t2495117162::get_offset_of_rotator_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (Zipline_t3027761465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[9] = 
{
	Zipline_t3027761465::get_offset_of_downStartSpeed_45(),
	Zipline_t3027761465::get_offset_of_acceleration_46(),
	Zipline_t3027761465::get_offset_of_upSpeed_47(),
	Zipline_t3027761465::get_offset_of_handleEndPosition_48(),
	Zipline_t3027761465::get_offset_of_handleStartPosition_49(),
	Zipline_t3027761465::get_offset_of_handle_50(),
	Zipline_t3027761465::get_offset_of_isMoving_51(),
	Zipline_t3027761465::get_offset_of_isMovingDown_52(),
	Zipline_t3027761465::get_offset_of_currentSpeed_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (VRTK_ConsoleViewer_t2446975743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[15] = 
{
	VRTK_ConsoleViewer_t2446975743::get_offset_of_fontSize_2(),
	VRTK_ConsoleViewer_t2446975743::get_offset_of_infoMessage_3(),
	VRTK_ConsoleViewer_t2446975743::get_offset_of_assertMessage_4(),
	VRTK_ConsoleViewer_t2446975743::get_offset_of_warningMessage_5(),
	VRTK_ConsoleViewer_t2446975743::get_offset_of_errorMessage_6(),
	VRTK_ConsoleViewer_t2446975743::get_offset_of_exceptionMessage_7(),
	VRTK_ConsoleViewer_t2446975743::get_offset_of_logTypeColors_8(),
	VRTK_ConsoleViewer_t2446975743::get_offset_of_scrollWindow_9(),
	VRTK_ConsoleViewer_t2446975743::get_offset_of_consoleRect_10(),
	VRTK_ConsoleViewer_t2446975743::get_offset_of_consoleOutput_11(),
	0,
	VRTK_ConsoleViewer_t2446975743::get_offset_of_lineBuffer_13(),
	VRTK_ConsoleViewer_t2446975743::get_offset_of_currentBuffer_14(),
	VRTK_ConsoleViewer_t2446975743::get_offset_of_lastMessage_15(),
	VRTK_ConsoleViewer_t2446975743::get_offset_of_collapseLog_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (VRTK_ControllerRigidbodyActivator_t3039396590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[1] = 
{
	VRTK_ControllerRigidbodyActivator_t3039396590::get_offset_of_isEnabled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (VRTK_ControllerTooltips_t3537184660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[26] = 
{
	VRTK_ControllerTooltips_t3537184660::get_offset_of_triggerText_2(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_gripText_3(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_touchpadText_4(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_buttonOneText_5(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_buttonTwoText_6(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_startMenuText_7(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_tipBackgroundColor_8(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_tipTextColor_9(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_tipLineColor_10(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_trigger_11(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_grip_12(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_touchpad_13(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_buttonOne_14(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_buttonTwo_15(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_startMenu_16(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_triggerInitialised_17(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_gripInitialised_18(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_touchpadInitialised_19(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_buttonOneInitialised_20(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_buttonTwoInitialised_21(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_startMenuInitialised_22(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_availableButtons_23(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_buttonTooltips_24(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_tooltipStates_25(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_controllerActions_26(),
	VRTK_ControllerTooltips_t3537184660::get_offset_of_headsetControllerAware_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (TooltipButtons_t2877834378)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2747[8] = 
{
	TooltipButtons_t2877834378::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (VRTK_DestinationPoint_t1547370418), -1, sizeof(VRTK_DestinationPoint_t1547370418_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2748[15] = 
{
	VRTK_DestinationPoint_t1547370418::get_offset_of_defaultCursorObject_9(),
	VRTK_DestinationPoint_t1547370418::get_offset_of_hoverCursorObject_10(),
	VRTK_DestinationPoint_t1547370418::get_offset_of_lockedCursorObject_11(),
	VRTK_DestinationPoint_t1547370418::get_offset_of_snapToPoint_12(),
	VRTK_DestinationPoint_t1547370418::get_offset_of_hidePointerCursorOnHover_13(),
	VRTK_DestinationPoint_t1547370418_StaticFields::get_offset_of_currentDestinationPoint_14(),
	VRTK_DestinationPoint_t1547370418::get_offset_of_pointCollider_15(),
	VRTK_DestinationPoint_t1547370418::get_offset_of_createdCollider_16(),
	VRTK_DestinationPoint_t1547370418::get_offset_of_pointRigidbody_17(),
	VRTK_DestinationPoint_t1547370418::get_offset_of_createdRigidbody_18(),
	VRTK_DestinationPoint_t1547370418::get_offset_of_initaliseListeners_19(),
	VRTK_DestinationPoint_t1547370418::get_offset_of_isActive_20(),
	VRTK_DestinationPoint_t1547370418::get_offset_of_storedCursorState_21(),
	VRTK_DestinationPoint_t1547370418::get_offset_of_setDestination_22(),
	VRTK_DestinationPoint_t1547370418::get_offset_of_currentTeleportState_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_t1570204165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[4] = 
{
	U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_t1570204165::get_offset_of_U24this_0(),
	U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_t1570204165::get_offset_of_U24current_1(),
	U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_t1570204165::get_offset_of_U24disposing_2(),
	U3CManageDestinationMarkersAtEndOfFrameU3Ec__Iterator0_t1570204165::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2750[5] = 
{
	U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915::get_offset_of_e_0(),
	U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915::get_offset_of_U24this_1(),
	U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915::get_offset_of_U24current_2(),
	U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915::get_offset_of_U24disposing_3(),
	U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (VRTK_FramesPerSecondViewer_t2787247731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[11] = 
{
	VRTK_FramesPerSecondViewer_t2787247731::get_offset_of_displayFPS_2(),
	VRTK_FramesPerSecondViewer_t2787247731::get_offset_of_targetFPS_3(),
	VRTK_FramesPerSecondViewer_t2787247731::get_offset_of_fontSize_4(),
	VRTK_FramesPerSecondViewer_t2787247731::get_offset_of_position_5(),
	VRTK_FramesPerSecondViewer_t2787247731::get_offset_of_goodColor_6(),
	VRTK_FramesPerSecondViewer_t2787247731::get_offset_of_warnColor_7(),
	VRTK_FramesPerSecondViewer_t2787247731::get_offset_of_badColor_8(),
	0,
	VRTK_FramesPerSecondViewer_t2787247731::get_offset_of_framesCount_10(),
	VRTK_FramesPerSecondViewer_t2787247731::get_offset_of_framesTime_11(),
	VRTK_FramesPerSecondViewer_t2787247731::get_offset_of_text_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (VRTK_ObjectTooltip_t333831714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[10] = 
{
	VRTK_ObjectTooltip_t333831714::get_offset_of_displayText_2(),
	VRTK_ObjectTooltip_t333831714::get_offset_of_fontSize_3(),
	VRTK_ObjectTooltip_t333831714::get_offset_of_containerSize_4(),
	VRTK_ObjectTooltip_t333831714::get_offset_of_drawLineFrom_5(),
	VRTK_ObjectTooltip_t333831714::get_offset_of_drawLineTo_6(),
	VRTK_ObjectTooltip_t333831714::get_offset_of_lineWidth_7(),
	VRTK_ObjectTooltip_t333831714::get_offset_of_fontColor_8(),
	VRTK_ObjectTooltip_t333831714::get_offset_of_containerColor_9(),
	VRTK_ObjectTooltip_t333831714::get_offset_of_lineColor_10(),
	VRTK_ObjectTooltip_t333831714::get_offset_of_line_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (SnapDropZoneEventArgs_t418702774)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[1] = 
{
	SnapDropZoneEventArgs_t418702774::get_offset_of_snappedObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (SnapDropZoneEventHandler_t172258073), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (VRTK_SnapDropZone_t1948041105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2755[27] = 
{
	VRTK_SnapDropZone_t1948041105::get_offset_of_highlightObjectPrefab_2(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_snapType_3(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_snapDuration_4(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_applyScalingOnSnap_5(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_highlightColor_6(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_highlightAlwaysActive_7(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_validObjectListPolicy_8(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_displayDropZoneInEditor_9(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_ObjectEnteredSnapDropZone_10(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_ObjectExitedSnapDropZone_11(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_ObjectSnappedToDropZone_12(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_ObjectUnsnappedFromDropZone_13(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_previousPrefab_14(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_highlightContainer_15(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_highlightObject_16(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_highlightEditorObject_17(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_currentValidSnapObject_18(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_currentSnappedObject_19(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_objectHighlighter_20(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_willSnap_21(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_isSnapped_22(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_isHighlighted_23(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_transitionInPlace_24(),
	VRTK_SnapDropZone_t1948041105::get_offset_of_originalJointCollisionState_25(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (SnapTypes_t2013855124)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2756[4] = 
{
	SnapTypes_t2013855124::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2757[14] = 
{
	U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402::get_offset_of_U3CelapsedTimeU3E__0_0(),
	U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402::get_offset_of_ioCheck_1(),
	U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402::get_offset_of_U3CioTransformU3E__1_2(),
	U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402::get_offset_of_U3CstartPositionU3E__2_3(),
	U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402::get_offset_of_U3CstartRotationU3E__3_4(),
	U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402::get_offset_of_U3CstartScaleU3E__4_5(),
	U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402::get_offset_of_U3CstoredKinematicStateU3E__5_6(),
	U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402::get_offset_of_duration_7(),
	U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402::get_offset_of_endSettings_8(),
	U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402::get_offset_of_endScale_9(),
	U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402::get_offset_of_U24this_10(),
	U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402::get_offset_of_U24current_11(),
	U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402::get_offset_of_U24disposing_12(),
	U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402::get_offset_of_U24PC_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_t3586163035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2758[5] = 
{
	U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_t3586163035::get_offset_of_objectToSnap_0(),
	U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_t3586163035::get_offset_of_U24this_1(),
	U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_t3586163035::get_offset_of_U24current_2(),
	U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_t3586163035::get_offset_of_U24disposing_3(),
	U3CAttemptForceSnapAtEndOfFrameU3Ec__Iterator1_t3586163035::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (SDK_BaseBoundaries_t1766380066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[1] = 
{
	SDK_BaseBoundaries_t1766380066::get_offset_of_cachedPlayArea_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (SDK_BaseController_t197168236), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (ButtonPressTypes_t1850327506)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2761[7] = 
{
	ButtonPressTypes_t1850327506::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (ControllerElements_t447683143)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2762[11] = 
{
	ControllerElements_t447683143::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (ControllerHand_t246418309)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2763[4] = 
{
	ControllerHand_t246418309::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (SDK_ControllerHapticModifiers_t3871094838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[2] = 
{
	SDK_ControllerHapticModifiers_t3871094838::get_offset_of_durationModifier_0(),
	SDK_ControllerHapticModifiers_t3871094838::get_offset_of_intervalModifier_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (SDK_BaseHeadset_t3046873914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[2] = 
{
	SDK_BaseHeadset_t3046873914::get_offset_of_cachedHeadset_2(),
	SDK_BaseHeadset_t3046873914::get_offset_of_cachedHeadsetCamera_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (SDK_BaseSystem_t244469351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (SDK_DaydreamBoundaries_t2613138380), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (SDK_DaydreamController_t2403190306), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (SDK_DaydreamHeadset_t1169140976), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (SDK_DaydreamSystem_t808099371), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (SDK_FallbackBoundaries_t1566319879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (SDK_FallbackController_t773334429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (SDK_FallbackHeadset_t1089992503), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (SDK_FallbackSystem_t1344157064), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (SDK_OculusVRBoundaries_t3279427364), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (SDK_OculusVRController_t2534364654), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (SDK_OculusVRHeadset_t2077937592), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (SDK_OculusVRSystem_t1733635573), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (SDK_ControllerSim_t66347054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2779[5] = 
{
	SDK_ControllerSim_t66347054::get_offset_of_lastPos_2(),
	SDK_ControllerSim_t66347054::get_offset_of_lastRot_3(),
	SDK_ControllerSim_t66347054::get_offset_of_posList_4(),
	SDK_ControllerSim_t66347054::get_offset_of_rotList_5(),
	SDK_ControllerSim_t66347054::get_offset_of_selected_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (SDK_InputSimulator_t2184297317), -1, sizeof(SDK_InputSimulator_t2184297317_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2780[28] = 
{
	SDK_InputSimulator_t2184297317::get_offset_of_hideHandsAtSwitch_2(),
	SDK_InputSimulator_t2184297317::get_offset_of_resetHandsAtSwitch_3(),
	SDK_InputSimulator_t2184297317::get_offset_of_handMoveMultiplier_4(),
	SDK_InputSimulator_t2184297317::get_offset_of_handRotationMultiplier_5(),
	SDK_InputSimulator_t2184297317::get_offset_of_playerMoveMultiplier_6(),
	SDK_InputSimulator_t2184297317::get_offset_of_playerRotationMultiplier_7(),
	SDK_InputSimulator_t2184297317::get_offset_of_changeHands_8(),
	SDK_InputSimulator_t2184297317::get_offset_of_handsOnOff_9(),
	SDK_InputSimulator_t2184297317::get_offset_of_rotationPosition_10(),
	SDK_InputSimulator_t2184297317::get_offset_of_changeAxis_11(),
	SDK_InputSimulator_t2184297317::get_offset_of_triggerAlias_12(),
	SDK_InputSimulator_t2184297317::get_offset_of_gripAlias_13(),
	SDK_InputSimulator_t2184297317::get_offset_of_touchpadAlias_14(),
	SDK_InputSimulator_t2184297317::get_offset_of_buttonOneAlias_15(),
	SDK_InputSimulator_t2184297317::get_offset_of_buttonTwoAlias_16(),
	SDK_InputSimulator_t2184297317::get_offset_of_startMenuAlias_17(),
	SDK_InputSimulator_t2184297317::get_offset_of_touchModifier_18(),
	SDK_InputSimulator_t2184297317::get_offset_of_hairTouchModifier_19(),
	SDK_InputSimulator_t2184297317::get_offset_of_isHand_20(),
	SDK_InputSimulator_t2184297317::get_offset_of_rightHand_21(),
	SDK_InputSimulator_t2184297317::get_offset_of_leftHand_22(),
	SDK_InputSimulator_t2184297317::get_offset_of_currentHand_23(),
	SDK_InputSimulator_t2184297317::get_offset_of_oldPos_24(),
	SDK_InputSimulator_t2184297317::get_offset_of_myCamera_25(),
	SDK_InputSimulator_t2184297317::get_offset_of_rightController_26(),
	SDK_InputSimulator_t2184297317::get_offset_of_leftController_27(),
	SDK_InputSimulator_t2184297317_StaticFields::get_offset_of_cachedCameraRig_28(),
	SDK_InputSimulator_t2184297317_StaticFields::get_offset_of_destroyed_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (SDK_SimBoundaries_t3645911520), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (SDK_SimController_t33406942), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (SDK_SimHeadset_t4149936844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (SDK_SimSystem_t2203361345), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (SDK_SteamVRBoundaries_t1132496137), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (SDK_SteamVRController_t3691290795), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (SDK_SteamVRHeadset_t524129709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (SDK_SteamVRSystem_t50103670), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (VRTK_SDK_Bridge_t2841169330), -1, sizeof(VRTK_SDK_Bridge_t2841169330_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2789[4] = 
{
	VRTK_SDK_Bridge_t2841169330_StaticFields::get_offset_of_systemSDK_0(),
	VRTK_SDK_Bridge_t2841169330_StaticFields::get_offset_of_headsetSDK_1(),
	VRTK_SDK_Bridge_t2841169330_StaticFields::get_offset_of_controllerSDK_2(),
	VRTK_SDK_Bridge_t2841169330_StaticFields::get_offset_of_boundariesSDK_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (PanelMenuController_t2234710621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2790[24] = 
{
	PanelMenuController_t2234710621::get_offset_of_rotateTowards_2(),
	PanelMenuController_t2234710621::get_offset_of_zoomScaleMultiplier_3(),
	PanelMenuController_t2234710621::get_offset_of_topPanelMenuItemController_4(),
	PanelMenuController_t2234710621::get_offset_of_bottomPanelMenuItemController_5(),
	PanelMenuController_t2234710621::get_offset_of_leftPanelMenuItemController_6(),
	PanelMenuController_t2234710621::get_offset_of_rightPanelMenuItemController_7(),
	0,
	0,
	0,
	0,
	PanelMenuController_t2234710621::get_offset_of_controllerEvents_12(),
	PanelMenuController_t2234710621::get_offset_of_currentPanelMenuItemController_13(),
	PanelMenuController_t2234710621::get_offset_of_interactableObject_14(),
	PanelMenuController_t2234710621::get_offset_of_canvasObject_15(),
	PanelMenuController_t2234710621::get_offset_of_xAxis_16(),
	PanelMenuController_t2234710621::get_offset_of_yAxis_17(),
	PanelMenuController_t2234710621::get_offset_of_touchStartPosition_18(),
	PanelMenuController_t2234710621::get_offset_of_touchEndPosition_19(),
	PanelMenuController_t2234710621::get_offset_of_touchStartTime_20(),
	PanelMenuController_t2234710621::get_offset_of_currentAngle_21(),
	PanelMenuController_t2234710621::get_offset_of_isTrackingSwipe_22(),
	PanelMenuController_t2234710621::get_offset_of_isPendingSwipeCheck_23(),
	PanelMenuController_t2234710621::get_offset_of_isGrabbed_24(),
	PanelMenuController_t2234710621::get_offset_of_isShown_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (TouchpadPressPosition_t1242655431)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2791[6] = 
{
	TouchpadPressPosition_t1242655431::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (U3CTweenMenuScaleU3Ec__Iterator0_t3216112136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2792[8] = 
{
	U3CTweenMenuScaleU3Ec__Iterator0_t3216112136::get_offset_of_U3CtargetScaleU3E__0_0(),
	U3CTweenMenuScaleU3Ec__Iterator0_t3216112136::get_offset_of_U3CdirectionU3E__1_1(),
	U3CTweenMenuScaleU3Ec__Iterator0_t3216112136::get_offset_of_show_2(),
	U3CTweenMenuScaleU3Ec__Iterator0_t3216112136::get_offset_of_U3CiU3E__2_3(),
	U3CTweenMenuScaleU3Ec__Iterator0_t3216112136::get_offset_of_U24this_4(),
	U3CTweenMenuScaleU3Ec__Iterator0_t3216112136::get_offset_of_U24current_5(),
	U3CTweenMenuScaleU3Ec__Iterator0_t3216112136::get_offset_of_U24disposing_6(),
	U3CTweenMenuScaleU3Ec__Iterator0_t3216112136::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (PanelMenuItemControllerEventArgs_t2917504033)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2793[1] = 
{
	PanelMenuItemControllerEventArgs_t2917504033::get_offset_of_interactableObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (PanelMenuItemControllerEventHandler_t780308162), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (PanelMenuItemController_t3837844790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[7] = 
{
	PanelMenuItemController_t3837844790::get_offset_of_PanelMenuItemShowing_2(),
	PanelMenuItemController_t3837844790::get_offset_of_PanelMenuItemHiding_3(),
	PanelMenuItemController_t3837844790::get_offset_of_PanelMenuItemSwipeLeft_4(),
	PanelMenuItemController_t3837844790::get_offset_of_PanelMenuItemSwipeRight_5(),
	PanelMenuItemController_t3837844790::get_offset_of_PanelMenuItemSwipeTop_6(),
	PanelMenuItemController_t3837844790::get_offset_of_PanelMenuItemSwipeBottom_7(),
	PanelMenuItemController_t3837844790::get_offset_of_PanelMenuItemTriggerPressed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (RadialButtonIcon_t700423124), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (HapticPulseEventHandler_t422205650), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (RadialMenu_t1576296262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2798[17] = 
{
	RadialMenu_t1576296262::get_offset_of_buttons_2(),
	RadialMenu_t1576296262::get_offset_of_buttonPrefab_3(),
	RadialMenu_t1576296262::get_offset_of_generateOnAwake_4(),
	RadialMenu_t1576296262::get_offset_of_buttonThickness_5(),
	RadialMenu_t1576296262::get_offset_of_buttonColor_6(),
	RadialMenu_t1576296262::get_offset_of_offsetDistance_7(),
	RadialMenu_t1576296262::get_offset_of_offsetRotation_8(),
	RadialMenu_t1576296262::get_offset_of_rotateIcons_9(),
	RadialMenu_t1576296262::get_offset_of_iconMargin_10(),
	RadialMenu_t1576296262::get_offset_of_isShown_11(),
	RadialMenu_t1576296262::get_offset_of_hideOnRelease_12(),
	RadialMenu_t1576296262::get_offset_of_executeOnUnclick_13(),
	RadialMenu_t1576296262::get_offset_of_baseHapticStrength_14(),
	RadialMenu_t1576296262::get_offset_of_FireHapticPulse_15(),
	RadialMenu_t1576296262::get_offset_of_menuButtons_16(),
	RadialMenu_t1576296262::get_offset_of_currentHover_17(),
	RadialMenu_t1576296262::get_offset_of_currentPress_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (U3CTweenMenuScaleU3Ec__Iterator0_t701686089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2799[8] = 
{
	U3CTweenMenuScaleU3Ec__Iterator0_t701686089::get_offset_of_U3CtargetScaleU3E__0_0(),
	U3CTweenMenuScaleU3Ec__Iterator0_t701686089::get_offset_of_U3CDirU3E__1_1(),
	U3CTweenMenuScaleU3Ec__Iterator0_t701686089::get_offset_of_show_2(),
	U3CTweenMenuScaleU3Ec__Iterator0_t701686089::get_offset_of_U3CiU3E__2_3(),
	U3CTweenMenuScaleU3Ec__Iterator0_t701686089::get_offset_of_U24this_4(),
	U3CTweenMenuScaleU3Ec__Iterator0_t701686089::get_offset_of_U24current_5(),
	U3CTweenMenuScaleU3Ec__Iterator0_t701686089::get_offset_of_U24disposing_6(),
	U3CTweenMenuScaleU3Ec__Iterator0_t701686089::get_offset_of_U24PC_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
