﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_GetCurrentFadeColor
struct _GetCurrentFadeColor_t2258064488;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdColor_t1780554589.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_GetCurrentFadeColor::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetCurrentFadeColor__ctor_m1914764027 (_GetCurrentFadeColor_t2258064488 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdColor_t Valve.VR.IVRCompositor/_GetCurrentFadeColor::Invoke(System.Boolean)
extern "C"  HmdColor_t_t1780554589  _GetCurrentFadeColor_Invoke_m2794073120 (_GetCurrentFadeColor_t2258064488 * __this, bool ___bBackground0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_GetCurrentFadeColor::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetCurrentFadeColor_BeginInvoke_m82929927 (_GetCurrentFadeColor_t2258064488 * __this, bool ___bBackground0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdColor_t Valve.VR.IVRCompositor/_GetCurrentFadeColor::EndInvoke(System.IAsyncResult)
extern "C"  HmdColor_t_t1780554589  _GetCurrentFadeColor_EndInvoke_m227763133 (_GetCurrentFadeColor_t2258064488 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
