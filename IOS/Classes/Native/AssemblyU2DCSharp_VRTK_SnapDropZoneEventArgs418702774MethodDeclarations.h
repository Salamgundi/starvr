﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SnapDropZoneEventArgs
struct SnapDropZoneEventArgs_t418702774;
struct SnapDropZoneEventArgs_t418702774_marshaled_pinvoke;
struct SnapDropZoneEventArgs_t418702774_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct SnapDropZoneEventArgs_t418702774;
struct SnapDropZoneEventArgs_t418702774_marshaled_pinvoke;

extern "C" void SnapDropZoneEventArgs_t418702774_marshal_pinvoke(const SnapDropZoneEventArgs_t418702774& unmarshaled, SnapDropZoneEventArgs_t418702774_marshaled_pinvoke& marshaled);
extern "C" void SnapDropZoneEventArgs_t418702774_marshal_pinvoke_back(const SnapDropZoneEventArgs_t418702774_marshaled_pinvoke& marshaled, SnapDropZoneEventArgs_t418702774& unmarshaled);
extern "C" void SnapDropZoneEventArgs_t418702774_marshal_pinvoke_cleanup(SnapDropZoneEventArgs_t418702774_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SnapDropZoneEventArgs_t418702774;
struct SnapDropZoneEventArgs_t418702774_marshaled_com;

extern "C" void SnapDropZoneEventArgs_t418702774_marshal_com(const SnapDropZoneEventArgs_t418702774& unmarshaled, SnapDropZoneEventArgs_t418702774_marshaled_com& marshaled);
extern "C" void SnapDropZoneEventArgs_t418702774_marshal_com_back(const SnapDropZoneEventArgs_t418702774_marshaled_com& marshaled, SnapDropZoneEventArgs_t418702774& unmarshaled);
extern "C" void SnapDropZoneEventArgs_t418702774_marshal_com_cleanup(SnapDropZoneEventArgs_t418702774_marshaled_com& marshaled);
