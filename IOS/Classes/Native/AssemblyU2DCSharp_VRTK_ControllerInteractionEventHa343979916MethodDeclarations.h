﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.ControllerInteractionEventHandler
struct ControllerInteractionEventHandler_t343979916;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.ControllerInteractionEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ControllerInteractionEventHandler__ctor_m4206169134 (ControllerInteractionEventHandler_t343979916 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.ControllerInteractionEventHandler::Invoke(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void ControllerInteractionEventHandler_Invoke_m1032063180 (ControllerInteractionEventHandler_t343979916 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.ControllerInteractionEventHandler::BeginInvoke(System.Object,VRTK.ControllerInteractionEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ControllerInteractionEventHandler_BeginInvoke_m3345163425 (ControllerInteractionEventHandler_t343979916 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.ControllerInteractionEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ControllerInteractionEventHandler_EndInvoke_m1888807812 (ControllerInteractionEventHandler_t343979916 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
