﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_GetFrameTimeRemaining
struct _GetFrameTimeRemaining_t2433513766;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_GetFrameTimeRemaining::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetFrameTimeRemaining__ctor_m3218923733 (_GetFrameTimeRemaining_t2433513766 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.IVRCompositor/_GetFrameTimeRemaining::Invoke()
extern "C"  float _GetFrameTimeRemaining_Invoke_m424959591 (_GetFrameTimeRemaining_t2433513766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_GetFrameTimeRemaining::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetFrameTimeRemaining_BeginInvoke_m3278850932 (_GetFrameTimeRemaining_t2433513766 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.IVRCompositor/_GetFrameTimeRemaining::EndInvoke(System.IAsyncResult)
extern "C"  float _GetFrameTimeRemaining_EndInvoke_m3968694313 (_GetFrameTimeRemaining_t2433513766 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
