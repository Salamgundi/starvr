﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_ShowMessageOverlay
struct _ShowMessageOverlay_t3284759035;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_VRMessageOverlayResponse875548136.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_ShowMessageOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _ShowMessageOverlay__ctor_m547989352 (_ShowMessageOverlay_t3284759035 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.VRMessageOverlayResponse Valve.VR.IVROverlay/_ShowMessageOverlay::Invoke(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  int32_t _ShowMessageOverlay_Invoke_m1793096887 (_ShowMessageOverlay_t3284759035 * __this, String_t* ___pchText0, String_t* ___pchCaption1, String_t* ___pchButton0Text2, String_t* ___pchButton1Text3, String_t* ___pchButton2Text4, String_t* ___pchButton3Text5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_ShowMessageOverlay::BeginInvoke(System.String,System.String,System.String,System.String,System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ShowMessageOverlay_BeginInvoke_m2757357013 (_ShowMessageOverlay_t3284759035 * __this, String_t* ___pchText0, String_t* ___pchCaption1, String_t* ___pchButton0Text2, String_t* ___pchButton1Text3, String_t* ___pchButton2Text4, String_t* ___pchButton3Text5, AsyncCallback_t163412349 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.VRMessageOverlayResponse Valve.VR.IVROverlay/_ShowMessageOverlay::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _ShowMessageOverlay_EndInvoke_m1502031633 (_ShowMessageOverlay_t3284759035 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
