﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4050602123MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3798319381(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3109403176 *, Dictionary_2_t111376037 *, const MethodInfo*))ValueCollection__ctor_m2971809126_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m541681019(__this, ___item0, method) ((  void (*) (ValueCollection_t3109403176 *, VRTK_SDKDetails_t1748250348 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4081813812_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4142648026(__this, method) ((  void (*) (ValueCollection_t3109403176 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1797909237_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1333923461(__this, ___item0, method) ((  bool (*) (ValueCollection_t3109403176 *, VRTK_SDKDetails_t1748250348 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4122873072_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3903200416(__this, ___item0, method) ((  bool (*) (ValueCollection_t3109403176 *, VRTK_SDKDetails_t1748250348 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2354728161_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3039766626(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3109403176 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m633958511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2862905802(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3109403176 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3720259095_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1374743179(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3109403176 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1617558016_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3161797628(__this, method) ((  bool (*) (ValueCollection_t3109403176 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1991166525_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m245077126(__this, method) ((  bool (*) (ValueCollection_t3109403176 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1308370015_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2601942026(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3109403176 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2674763763_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1873749486(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3109403176 *, VRTK_SDKDetailsU5BU5D_t3436018661*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m4115803321_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1373046825(__this, method) ((  Enumerator_t1797908801  (*) (ValueCollection_t3109403176 *, const MethodInfo*))ValueCollection_GetEnumerator_m436904190_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::get_Count()
#define ValueCollection_get_Count_m3688156806(__this, method) ((  int32_t (*) (ValueCollection_t3109403176 *, const MethodInfo*))ValueCollection_get_Count_m3834064743_gshared)(__this, method)
