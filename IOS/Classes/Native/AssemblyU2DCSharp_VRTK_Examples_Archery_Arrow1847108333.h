﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Archery.Arrow
struct  Arrow_t1847108333  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VRTK.Examples.Archery.Arrow::maxArrowLife
	float ___maxArrowLife_2;
	// System.Boolean VRTK.Examples.Archery.Arrow::inFlight
	bool ___inFlight_3;
	// System.Boolean VRTK.Examples.Archery.Arrow::collided
	bool ___collided_4;
	// UnityEngine.Rigidbody VRTK.Examples.Archery.Arrow::rigidBody
	Rigidbody_t4233889191 * ___rigidBody_5;
	// UnityEngine.GameObject VRTK.Examples.Archery.Arrow::arrowHolder
	GameObject_t1756533147 * ___arrowHolder_6;
	// UnityEngine.Vector3 VRTK.Examples.Archery.Arrow::originalPosition
	Vector3_t2243707580  ___originalPosition_7;
	// UnityEngine.Quaternion VRTK.Examples.Archery.Arrow::originalRotation
	Quaternion_t4030073918  ___originalRotation_8;
	// UnityEngine.Vector3 VRTK.Examples.Archery.Arrow::originalScale
	Vector3_t2243707580  ___originalScale_9;

public:
	inline static int32_t get_offset_of_maxArrowLife_2() { return static_cast<int32_t>(offsetof(Arrow_t1847108333, ___maxArrowLife_2)); }
	inline float get_maxArrowLife_2() const { return ___maxArrowLife_2; }
	inline float* get_address_of_maxArrowLife_2() { return &___maxArrowLife_2; }
	inline void set_maxArrowLife_2(float value)
	{
		___maxArrowLife_2 = value;
	}

	inline static int32_t get_offset_of_inFlight_3() { return static_cast<int32_t>(offsetof(Arrow_t1847108333, ___inFlight_3)); }
	inline bool get_inFlight_3() const { return ___inFlight_3; }
	inline bool* get_address_of_inFlight_3() { return &___inFlight_3; }
	inline void set_inFlight_3(bool value)
	{
		___inFlight_3 = value;
	}

	inline static int32_t get_offset_of_collided_4() { return static_cast<int32_t>(offsetof(Arrow_t1847108333, ___collided_4)); }
	inline bool get_collided_4() const { return ___collided_4; }
	inline bool* get_address_of_collided_4() { return &___collided_4; }
	inline void set_collided_4(bool value)
	{
		___collided_4 = value;
	}

	inline static int32_t get_offset_of_rigidBody_5() { return static_cast<int32_t>(offsetof(Arrow_t1847108333, ___rigidBody_5)); }
	inline Rigidbody_t4233889191 * get_rigidBody_5() const { return ___rigidBody_5; }
	inline Rigidbody_t4233889191 ** get_address_of_rigidBody_5() { return &___rigidBody_5; }
	inline void set_rigidBody_5(Rigidbody_t4233889191 * value)
	{
		___rigidBody_5 = value;
		Il2CppCodeGenWriteBarrier(&___rigidBody_5, value);
	}

	inline static int32_t get_offset_of_arrowHolder_6() { return static_cast<int32_t>(offsetof(Arrow_t1847108333, ___arrowHolder_6)); }
	inline GameObject_t1756533147 * get_arrowHolder_6() const { return ___arrowHolder_6; }
	inline GameObject_t1756533147 ** get_address_of_arrowHolder_6() { return &___arrowHolder_6; }
	inline void set_arrowHolder_6(GameObject_t1756533147 * value)
	{
		___arrowHolder_6 = value;
		Il2CppCodeGenWriteBarrier(&___arrowHolder_6, value);
	}

	inline static int32_t get_offset_of_originalPosition_7() { return static_cast<int32_t>(offsetof(Arrow_t1847108333, ___originalPosition_7)); }
	inline Vector3_t2243707580  get_originalPosition_7() const { return ___originalPosition_7; }
	inline Vector3_t2243707580 * get_address_of_originalPosition_7() { return &___originalPosition_7; }
	inline void set_originalPosition_7(Vector3_t2243707580  value)
	{
		___originalPosition_7 = value;
	}

	inline static int32_t get_offset_of_originalRotation_8() { return static_cast<int32_t>(offsetof(Arrow_t1847108333, ___originalRotation_8)); }
	inline Quaternion_t4030073918  get_originalRotation_8() const { return ___originalRotation_8; }
	inline Quaternion_t4030073918 * get_address_of_originalRotation_8() { return &___originalRotation_8; }
	inline void set_originalRotation_8(Quaternion_t4030073918  value)
	{
		___originalRotation_8 = value;
	}

	inline static int32_t get_offset_of_originalScale_9() { return static_cast<int32_t>(offsetof(Arrow_t1847108333, ___originalScale_9)); }
	inline Vector3_t2243707580  get_originalScale_9() const { return ___originalScale_9; }
	inline Vector3_t2243707580 * get_address_of_originalScale_9() { return &___originalScale_9; }
	inline void set_originalScale_9(Vector3_t2243707580  value)
	{
		___originalScale_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
