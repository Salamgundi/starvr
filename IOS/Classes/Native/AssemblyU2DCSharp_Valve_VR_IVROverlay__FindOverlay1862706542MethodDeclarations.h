﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_FindOverlay
struct _FindOverlay_t1862706542;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_FindOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _FindOverlay__ctor_m944048663 (_FindOverlay_t1862706542 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_FindOverlay::Invoke(System.String,System.UInt64&)
extern "C"  int32_t _FindOverlay_Invoke_m1585102990 (_FindOverlay_t1862706542 * __this, String_t* ___pchOverlayKey0, uint64_t* ___pOverlayHandle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_FindOverlay::BeginInvoke(System.String,System.UInt64&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _FindOverlay_BeginInvoke_m104468567 (_FindOverlay_t1862706542 * __this, String_t* ___pchOverlayKey0, uint64_t* ___pOverlayHandle1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_FindOverlay::EndInvoke(System.UInt64&,System.IAsyncResult)
extern "C"  int32_t _FindOverlay_EndInvoke_m2906374432 (_FindOverlay_t1862706542 * __this, uint64_t* ___pOverlayHandle0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
