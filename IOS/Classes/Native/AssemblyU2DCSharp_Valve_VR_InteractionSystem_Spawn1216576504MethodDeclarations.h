﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.SpawnRenderModel
struct SpawnRenderModel_t1216576504;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// SteamVR_RenderModel
struct SteamVR_RenderModel_t2905485978;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"
#include "AssemblyU2DCSharp_SteamVR_RenderModel2905485978.h"

// System.Void Valve.VR.InteractionSystem.SpawnRenderModel::.ctor()
extern "C"  void SpawnRenderModel__ctor_m2085124650 (SpawnRenderModel_t1216576504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SpawnRenderModel::Awake()
extern "C"  void SpawnRenderModel_Awake_m3596599545 (SpawnRenderModel_t1216576504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SpawnRenderModel::OnEnable()
extern "C"  void SpawnRenderModel_OnEnable_m984436018 (SpawnRenderModel_t1216576504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SpawnRenderModel::OnDisable()
extern "C"  void SpawnRenderModel_OnDisable_m609063473 (SpawnRenderModel_t1216576504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SpawnRenderModel::OnAttachedToHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void SpawnRenderModel_OnAttachedToHand_m3308313729 (SpawnRenderModel_t1216576504 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SpawnRenderModel::OnDetachedFromHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void SpawnRenderModel_OnDetachedFromHand_m244929914 (SpawnRenderModel_t1216576504 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SpawnRenderModel::Update()
extern "C"  void SpawnRenderModel_Update_m3177097973 (SpawnRenderModel_t1216576504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SpawnRenderModel::ShowController()
extern "C"  void SpawnRenderModel_ShowController_m942545047 (SpawnRenderModel_t1216576504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SpawnRenderModel::HideController()
extern "C"  void SpawnRenderModel_HideController_m208155032 (SpawnRenderModel_t1216576504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SpawnRenderModel::OnRenderModelLoaded(SteamVR_RenderModel,System.Boolean)
extern "C"  void SpawnRenderModel_OnRenderModelLoaded_m1524821294 (SpawnRenderModel_t1216576504 * __this, SteamVR_RenderModel_t2905485978 * ___renderModel0, bool ___success1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SpawnRenderModel::.cctor()
extern "C"  void SpawnRenderModel__cctor_m991158207 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
