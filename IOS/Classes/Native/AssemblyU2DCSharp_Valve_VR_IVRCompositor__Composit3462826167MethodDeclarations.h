﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_CompositorDumpImages
struct _CompositorDumpImages_t3462826167;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_CompositorDumpImages::.ctor(System.Object,System.IntPtr)
extern "C"  void _CompositorDumpImages__ctor_m2455716874 (_CompositorDumpImages_t3462826167 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_CompositorDumpImages::Invoke()
extern "C"  void _CompositorDumpImages_Invoke_m872594080 (_CompositorDumpImages_t3462826167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_CompositorDumpImages::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _CompositorDumpImages_BeginInvoke_m991421401 (_CompositorDumpImages_t3462826167 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_CompositorDumpImages::EndInvoke(System.IAsyncResult)
extern "C"  void _CompositorDumpImages_EndInvoke_m728076472 (_CompositorDumpImages_t3462826167 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
