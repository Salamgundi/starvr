﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21452281081MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3280482321(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t48553296 *, int32_t, Event_1_t1285721510 *, const MethodInfo*))KeyValuePair_2__ctor_m1776453514_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::get_Key()
#define KeyValuePair_2_get_Key_m460976307(__this, method) ((  int32_t (*) (KeyValuePair_2_t48553296 *, const MethodInfo*))KeyValuePair_2_get_Key_m2053175032_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1044832648(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t48553296 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3000934387_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::get_Value()
#define KeyValuePair_2_get_Value_m3926076947(__this, method) ((  Event_1_t1285721510 * (*) (KeyValuePair_2_t48553296 *, const MethodInfo*))KeyValuePair_2_get_Value_m3238939256_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1534800056(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t48553296 *, Event_1_t1285721510 *, const MethodInfo*))KeyValuePair_2_set_Value_m373824915_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::ToString()
#define KeyValuePair_2_ToString_m1183552610(__this, method) ((  String_t* (*) (KeyValuePair_2_t48553296 *, const MethodInfo*))KeyValuePair_2_ToString_m3968446337_gshared)(__this, method)
