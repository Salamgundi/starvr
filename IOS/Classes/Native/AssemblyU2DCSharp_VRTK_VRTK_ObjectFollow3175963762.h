﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ObjectFollow
struct  VRTK_ObjectFollow_t3175963762  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject VRTK.VRTK_ObjectFollow::gameObjectToFollow
	GameObject_t1756533147 * ___gameObjectToFollow_2;
	// UnityEngine.GameObject VRTK.VRTK_ObjectFollow::gameObjectToChange
	GameObject_t1756533147 * ___gameObjectToChange_3;
	// System.Boolean VRTK.VRTK_ObjectFollow::followsPosition
	bool ___followsPosition_4;
	// System.Boolean VRTK.VRTK_ObjectFollow::smoothsPosition
	bool ___smoothsPosition_5;
	// System.Single VRTK.VRTK_ObjectFollow::maxAllowedPerFrameDistanceDifference
	float ___maxAllowedPerFrameDistanceDifference_6;
	// UnityEngine.Vector3 VRTK.VRTK_ObjectFollow::<targetPosition>k__BackingField
	Vector3_t2243707580  ___U3CtargetPositionU3Ek__BackingField_7;
	// System.Boolean VRTK.VRTK_ObjectFollow::followsRotation
	bool ___followsRotation_8;
	// System.Boolean VRTK.VRTK_ObjectFollow::smoothsRotation
	bool ___smoothsRotation_9;
	// System.Single VRTK.VRTK_ObjectFollow::maxAllowedPerFrameAngleDifference
	float ___maxAllowedPerFrameAngleDifference_10;
	// UnityEngine.Quaternion VRTK.VRTK_ObjectFollow::<targetRotation>k__BackingField
	Quaternion_t4030073918  ___U3CtargetRotationU3Ek__BackingField_11;
	// System.Boolean VRTK.VRTK_ObjectFollow::followsScale
	bool ___followsScale_12;
	// System.Boolean VRTK.VRTK_ObjectFollow::smoothsScale
	bool ___smoothsScale_13;
	// System.Single VRTK.VRTK_ObjectFollow::maxAllowedPerFrameSizeDifference
	float ___maxAllowedPerFrameSizeDifference_14;
	// UnityEngine.Vector3 VRTK.VRTK_ObjectFollow::<targetScale>k__BackingField
	Vector3_t2243707580  ___U3CtargetScaleU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_gameObjectToFollow_2() { return static_cast<int32_t>(offsetof(VRTK_ObjectFollow_t3175963762, ___gameObjectToFollow_2)); }
	inline GameObject_t1756533147 * get_gameObjectToFollow_2() const { return ___gameObjectToFollow_2; }
	inline GameObject_t1756533147 ** get_address_of_gameObjectToFollow_2() { return &___gameObjectToFollow_2; }
	inline void set_gameObjectToFollow_2(GameObject_t1756533147 * value)
	{
		___gameObjectToFollow_2 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectToFollow_2, value);
	}

	inline static int32_t get_offset_of_gameObjectToChange_3() { return static_cast<int32_t>(offsetof(VRTK_ObjectFollow_t3175963762, ___gameObjectToChange_3)); }
	inline GameObject_t1756533147 * get_gameObjectToChange_3() const { return ___gameObjectToChange_3; }
	inline GameObject_t1756533147 ** get_address_of_gameObjectToChange_3() { return &___gameObjectToChange_3; }
	inline void set_gameObjectToChange_3(GameObject_t1756533147 * value)
	{
		___gameObjectToChange_3 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectToChange_3, value);
	}

	inline static int32_t get_offset_of_followsPosition_4() { return static_cast<int32_t>(offsetof(VRTK_ObjectFollow_t3175963762, ___followsPosition_4)); }
	inline bool get_followsPosition_4() const { return ___followsPosition_4; }
	inline bool* get_address_of_followsPosition_4() { return &___followsPosition_4; }
	inline void set_followsPosition_4(bool value)
	{
		___followsPosition_4 = value;
	}

	inline static int32_t get_offset_of_smoothsPosition_5() { return static_cast<int32_t>(offsetof(VRTK_ObjectFollow_t3175963762, ___smoothsPosition_5)); }
	inline bool get_smoothsPosition_5() const { return ___smoothsPosition_5; }
	inline bool* get_address_of_smoothsPosition_5() { return &___smoothsPosition_5; }
	inline void set_smoothsPosition_5(bool value)
	{
		___smoothsPosition_5 = value;
	}

	inline static int32_t get_offset_of_maxAllowedPerFrameDistanceDifference_6() { return static_cast<int32_t>(offsetof(VRTK_ObjectFollow_t3175963762, ___maxAllowedPerFrameDistanceDifference_6)); }
	inline float get_maxAllowedPerFrameDistanceDifference_6() const { return ___maxAllowedPerFrameDistanceDifference_6; }
	inline float* get_address_of_maxAllowedPerFrameDistanceDifference_6() { return &___maxAllowedPerFrameDistanceDifference_6; }
	inline void set_maxAllowedPerFrameDistanceDifference_6(float value)
	{
		___maxAllowedPerFrameDistanceDifference_6 = value;
	}

	inline static int32_t get_offset_of_U3CtargetPositionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(VRTK_ObjectFollow_t3175963762, ___U3CtargetPositionU3Ek__BackingField_7)); }
	inline Vector3_t2243707580  get_U3CtargetPositionU3Ek__BackingField_7() const { return ___U3CtargetPositionU3Ek__BackingField_7; }
	inline Vector3_t2243707580 * get_address_of_U3CtargetPositionU3Ek__BackingField_7() { return &___U3CtargetPositionU3Ek__BackingField_7; }
	inline void set_U3CtargetPositionU3Ek__BackingField_7(Vector3_t2243707580  value)
	{
		___U3CtargetPositionU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_followsRotation_8() { return static_cast<int32_t>(offsetof(VRTK_ObjectFollow_t3175963762, ___followsRotation_8)); }
	inline bool get_followsRotation_8() const { return ___followsRotation_8; }
	inline bool* get_address_of_followsRotation_8() { return &___followsRotation_8; }
	inline void set_followsRotation_8(bool value)
	{
		___followsRotation_8 = value;
	}

	inline static int32_t get_offset_of_smoothsRotation_9() { return static_cast<int32_t>(offsetof(VRTK_ObjectFollow_t3175963762, ___smoothsRotation_9)); }
	inline bool get_smoothsRotation_9() const { return ___smoothsRotation_9; }
	inline bool* get_address_of_smoothsRotation_9() { return &___smoothsRotation_9; }
	inline void set_smoothsRotation_9(bool value)
	{
		___smoothsRotation_9 = value;
	}

	inline static int32_t get_offset_of_maxAllowedPerFrameAngleDifference_10() { return static_cast<int32_t>(offsetof(VRTK_ObjectFollow_t3175963762, ___maxAllowedPerFrameAngleDifference_10)); }
	inline float get_maxAllowedPerFrameAngleDifference_10() const { return ___maxAllowedPerFrameAngleDifference_10; }
	inline float* get_address_of_maxAllowedPerFrameAngleDifference_10() { return &___maxAllowedPerFrameAngleDifference_10; }
	inline void set_maxAllowedPerFrameAngleDifference_10(float value)
	{
		___maxAllowedPerFrameAngleDifference_10 = value;
	}

	inline static int32_t get_offset_of_U3CtargetRotationU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(VRTK_ObjectFollow_t3175963762, ___U3CtargetRotationU3Ek__BackingField_11)); }
	inline Quaternion_t4030073918  get_U3CtargetRotationU3Ek__BackingField_11() const { return ___U3CtargetRotationU3Ek__BackingField_11; }
	inline Quaternion_t4030073918 * get_address_of_U3CtargetRotationU3Ek__BackingField_11() { return &___U3CtargetRotationU3Ek__BackingField_11; }
	inline void set_U3CtargetRotationU3Ek__BackingField_11(Quaternion_t4030073918  value)
	{
		___U3CtargetRotationU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_followsScale_12() { return static_cast<int32_t>(offsetof(VRTK_ObjectFollow_t3175963762, ___followsScale_12)); }
	inline bool get_followsScale_12() const { return ___followsScale_12; }
	inline bool* get_address_of_followsScale_12() { return &___followsScale_12; }
	inline void set_followsScale_12(bool value)
	{
		___followsScale_12 = value;
	}

	inline static int32_t get_offset_of_smoothsScale_13() { return static_cast<int32_t>(offsetof(VRTK_ObjectFollow_t3175963762, ___smoothsScale_13)); }
	inline bool get_smoothsScale_13() const { return ___smoothsScale_13; }
	inline bool* get_address_of_smoothsScale_13() { return &___smoothsScale_13; }
	inline void set_smoothsScale_13(bool value)
	{
		___smoothsScale_13 = value;
	}

	inline static int32_t get_offset_of_maxAllowedPerFrameSizeDifference_14() { return static_cast<int32_t>(offsetof(VRTK_ObjectFollow_t3175963762, ___maxAllowedPerFrameSizeDifference_14)); }
	inline float get_maxAllowedPerFrameSizeDifference_14() const { return ___maxAllowedPerFrameSizeDifference_14; }
	inline float* get_address_of_maxAllowedPerFrameSizeDifference_14() { return &___maxAllowedPerFrameSizeDifference_14; }
	inline void set_maxAllowedPerFrameSizeDifference_14(float value)
	{
		___maxAllowedPerFrameSizeDifference_14 = value;
	}

	inline static int32_t get_offset_of_U3CtargetScaleU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(VRTK_ObjectFollow_t3175963762, ___U3CtargetScaleU3Ek__BackingField_15)); }
	inline Vector3_t2243707580  get_U3CtargetScaleU3Ek__BackingField_15() const { return ___U3CtargetScaleU3Ek__BackingField_15; }
	inline Vector3_t2243707580 * get_address_of_U3CtargetScaleU3Ek__BackingField_15() { return &___U3CtargetScaleU3Ek__BackingField_15; }
	inline void set_U3CtargetScaleU3Ek__BackingField_15(Vector3_t2243707580  value)
	{
		___U3CtargetScaleU3Ek__BackingField_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
