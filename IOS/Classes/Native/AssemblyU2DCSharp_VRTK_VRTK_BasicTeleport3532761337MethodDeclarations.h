﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_BasicTeleport
struct VRTK_BasicTeleport_t3532761337;
// VRTK.TeleportEventHandler
struct TeleportEventHandler_t2417981165;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_TeleportEventHandler2417981165.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventArgs1852451661.h"

// System.Void VRTK.VRTK_BasicTeleport::.ctor()
extern "C"  void VRTK_BasicTeleport__ctor_m1625523177 (VRTK_BasicTeleport_t3532761337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::add_Teleporting(VRTK.TeleportEventHandler)
extern "C"  void VRTK_BasicTeleport_add_Teleporting_m1445546444 (VRTK_BasicTeleport_t3532761337 * __this, TeleportEventHandler_t2417981165 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::remove_Teleporting(VRTK.TeleportEventHandler)
extern "C"  void VRTK_BasicTeleport_remove_Teleporting_m3915067789 (VRTK_BasicTeleport_t3532761337 * __this, TeleportEventHandler_t2417981165 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::add_Teleported(VRTK.TeleportEventHandler)
extern "C"  void VRTK_BasicTeleport_add_Teleported_m3433813507 (VRTK_BasicTeleport_t3532761337 * __this, TeleportEventHandler_t2417981165 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::remove_Teleported(VRTK.TeleportEventHandler)
extern "C"  void VRTK_BasicTeleport_remove_Teleported_m3896814966 (VRTK_BasicTeleport_t3532761337 * __this, TeleportEventHandler_t2417981165 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::InitDestinationSetListener(UnityEngine.GameObject,System.Boolean)
extern "C"  void VRTK_BasicTeleport_InitDestinationSetListener_m2850954256 (VRTK_BasicTeleport_t3532761337 * __this, GameObject_t1756533147 * ___markerMaker0, bool ___register1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::ToggleTeleportEnabled(System.Boolean)
extern "C"  void VRTK_BasicTeleport_ToggleTeleportEnabled_m3946451892 (VRTK_BasicTeleport_t3532761337 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BasicTeleport::ValidLocation(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  bool VRTK_BasicTeleport_ValidLocation_m2015308508 (VRTK_BasicTeleport_t3532761337 * __this, Transform_t3275118058 * ___target0, Vector3_t2243707580  ___destinationPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::Awake()
extern "C"  void VRTK_BasicTeleport_Awake_m2993209954 (VRTK_BasicTeleport_t3532761337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::OnEnable()
extern "C"  void VRTK_BasicTeleport_OnEnable_m1760509769 (VRTK_BasicTeleport_t3532761337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::OnDisable()
extern "C"  void VRTK_BasicTeleport_OnDisable_m1015974658 (VRTK_BasicTeleport_t3532761337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::Blink(System.Single)
extern "C"  void VRTK_BasicTeleport_Blink_m193758336 (VRTK_BasicTeleport_t3532761337 * __this, float ___transitionSpeed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::DoTeleport(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_BasicTeleport_DoTeleport_m757008893 (VRTK_BasicTeleport_t3532761337 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::SetNewPosition(UnityEngine.Vector3,UnityEngine.Transform,System.Boolean)
extern "C"  void VRTK_BasicTeleport_SetNewPosition_m3431091431 (VRTK_BasicTeleport_t3532761337 * __this, Vector3_t2243707580  ___position0, Transform_t3275118058 * ___target1, bool ___forceDestinationPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_BasicTeleport::GetNewPosition(UnityEngine.Vector3,UnityEngine.Transform,System.Boolean)
extern "C"  Vector3_t2243707580  VRTK_BasicTeleport_GetNewPosition_m1622435673 (VRTK_BasicTeleport_t3532761337 * __this, Vector3_t2243707580  ___tipPosition0, Transform_t3275118058 * ___target1, bool ___returnOriginalPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_BasicTeleport::CheckTerrainCollision(UnityEngine.Vector3,UnityEngine.Transform,System.Boolean)
extern "C"  Vector3_t2243707580  VRTK_BasicTeleport_CheckTerrainCollision_m2812390517 (VRTK_BasicTeleport_t3532761337 * __this, Vector3_t2243707580  ___position0, Transform_t3275118058 * ___target1, bool ___useHeadsetForPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::OnTeleporting(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_BasicTeleport_OnTeleporting_m3128957959 (VRTK_BasicTeleport_t3532761337 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::OnTeleported(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_BasicTeleport_OnTeleported_m3966990120 (VRTK_BasicTeleport_t3532761337 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::CalculateBlinkDelay(System.Single,UnityEngine.Vector3)
extern "C"  void VRTK_BasicTeleport_CalculateBlinkDelay_m2807616122 (VRTK_BasicTeleport_t3532761337 * __this, float ___blinkSpeed0, Vector3_t2243707580  ___newPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::ReleaseBlink()
extern "C"  void VRTK_BasicTeleport_ReleaseBlink_m1681858552 (VRTK_BasicTeleport_t3532761337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_BasicTeleport::InitListenersAtEndOfFrame()
extern "C"  Il2CppObject * VRTK_BasicTeleport_InitListenersAtEndOfFrame_m852080354 (VRTK_BasicTeleport_t3532761337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasicTeleport::InitDestinationMarkerListeners(System.Boolean)
extern "C"  void VRTK_BasicTeleport_InitDestinationMarkerListeners_m129907343 (VRTK_BasicTeleport_t3532761337 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
