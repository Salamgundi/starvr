﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectControl724022372.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ButtonControl
struct  VRTK_ButtonControl_t2487860925  : public VRTK_ObjectControl_t724022372
{
public:
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_ButtonControl::forwardButton
	int32_t ___forwardButton_22;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_ButtonControl::backwardButton
	int32_t ___backwardButton_23;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_ButtonControl::leftButton
	int32_t ___leftButton_24;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_ButtonControl::rightButton
	int32_t ___rightButton_25;
	// System.Boolean VRTK.VRTK_ButtonControl::forwardPressed
	bool ___forwardPressed_26;
	// System.Boolean VRTK.VRTK_ButtonControl::backwardPressed
	bool ___backwardPressed_27;
	// System.Boolean VRTK.VRTK_ButtonControl::leftPressed
	bool ___leftPressed_28;
	// System.Boolean VRTK.VRTK_ButtonControl::rightPressed
	bool ___rightPressed_29;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_ButtonControl::subscribedForwardButton
	int32_t ___subscribedForwardButton_30;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_ButtonControl::subscribedBackwardButton
	int32_t ___subscribedBackwardButton_31;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_ButtonControl::subscribedLeftButton
	int32_t ___subscribedLeftButton_32;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_ButtonControl::subscribedRightButton
	int32_t ___subscribedRightButton_33;
	// UnityEngine.Vector2 VRTK.VRTK_ButtonControl::axisDeadzone
	Vector2_t2243707579  ___axisDeadzone_34;

public:
	inline static int32_t get_offset_of_forwardButton_22() { return static_cast<int32_t>(offsetof(VRTK_ButtonControl_t2487860925, ___forwardButton_22)); }
	inline int32_t get_forwardButton_22() const { return ___forwardButton_22; }
	inline int32_t* get_address_of_forwardButton_22() { return &___forwardButton_22; }
	inline void set_forwardButton_22(int32_t value)
	{
		___forwardButton_22 = value;
	}

	inline static int32_t get_offset_of_backwardButton_23() { return static_cast<int32_t>(offsetof(VRTK_ButtonControl_t2487860925, ___backwardButton_23)); }
	inline int32_t get_backwardButton_23() const { return ___backwardButton_23; }
	inline int32_t* get_address_of_backwardButton_23() { return &___backwardButton_23; }
	inline void set_backwardButton_23(int32_t value)
	{
		___backwardButton_23 = value;
	}

	inline static int32_t get_offset_of_leftButton_24() { return static_cast<int32_t>(offsetof(VRTK_ButtonControl_t2487860925, ___leftButton_24)); }
	inline int32_t get_leftButton_24() const { return ___leftButton_24; }
	inline int32_t* get_address_of_leftButton_24() { return &___leftButton_24; }
	inline void set_leftButton_24(int32_t value)
	{
		___leftButton_24 = value;
	}

	inline static int32_t get_offset_of_rightButton_25() { return static_cast<int32_t>(offsetof(VRTK_ButtonControl_t2487860925, ___rightButton_25)); }
	inline int32_t get_rightButton_25() const { return ___rightButton_25; }
	inline int32_t* get_address_of_rightButton_25() { return &___rightButton_25; }
	inline void set_rightButton_25(int32_t value)
	{
		___rightButton_25 = value;
	}

	inline static int32_t get_offset_of_forwardPressed_26() { return static_cast<int32_t>(offsetof(VRTK_ButtonControl_t2487860925, ___forwardPressed_26)); }
	inline bool get_forwardPressed_26() const { return ___forwardPressed_26; }
	inline bool* get_address_of_forwardPressed_26() { return &___forwardPressed_26; }
	inline void set_forwardPressed_26(bool value)
	{
		___forwardPressed_26 = value;
	}

	inline static int32_t get_offset_of_backwardPressed_27() { return static_cast<int32_t>(offsetof(VRTK_ButtonControl_t2487860925, ___backwardPressed_27)); }
	inline bool get_backwardPressed_27() const { return ___backwardPressed_27; }
	inline bool* get_address_of_backwardPressed_27() { return &___backwardPressed_27; }
	inline void set_backwardPressed_27(bool value)
	{
		___backwardPressed_27 = value;
	}

	inline static int32_t get_offset_of_leftPressed_28() { return static_cast<int32_t>(offsetof(VRTK_ButtonControl_t2487860925, ___leftPressed_28)); }
	inline bool get_leftPressed_28() const { return ___leftPressed_28; }
	inline bool* get_address_of_leftPressed_28() { return &___leftPressed_28; }
	inline void set_leftPressed_28(bool value)
	{
		___leftPressed_28 = value;
	}

	inline static int32_t get_offset_of_rightPressed_29() { return static_cast<int32_t>(offsetof(VRTK_ButtonControl_t2487860925, ___rightPressed_29)); }
	inline bool get_rightPressed_29() const { return ___rightPressed_29; }
	inline bool* get_address_of_rightPressed_29() { return &___rightPressed_29; }
	inline void set_rightPressed_29(bool value)
	{
		___rightPressed_29 = value;
	}

	inline static int32_t get_offset_of_subscribedForwardButton_30() { return static_cast<int32_t>(offsetof(VRTK_ButtonControl_t2487860925, ___subscribedForwardButton_30)); }
	inline int32_t get_subscribedForwardButton_30() const { return ___subscribedForwardButton_30; }
	inline int32_t* get_address_of_subscribedForwardButton_30() { return &___subscribedForwardButton_30; }
	inline void set_subscribedForwardButton_30(int32_t value)
	{
		___subscribedForwardButton_30 = value;
	}

	inline static int32_t get_offset_of_subscribedBackwardButton_31() { return static_cast<int32_t>(offsetof(VRTK_ButtonControl_t2487860925, ___subscribedBackwardButton_31)); }
	inline int32_t get_subscribedBackwardButton_31() const { return ___subscribedBackwardButton_31; }
	inline int32_t* get_address_of_subscribedBackwardButton_31() { return &___subscribedBackwardButton_31; }
	inline void set_subscribedBackwardButton_31(int32_t value)
	{
		___subscribedBackwardButton_31 = value;
	}

	inline static int32_t get_offset_of_subscribedLeftButton_32() { return static_cast<int32_t>(offsetof(VRTK_ButtonControl_t2487860925, ___subscribedLeftButton_32)); }
	inline int32_t get_subscribedLeftButton_32() const { return ___subscribedLeftButton_32; }
	inline int32_t* get_address_of_subscribedLeftButton_32() { return &___subscribedLeftButton_32; }
	inline void set_subscribedLeftButton_32(int32_t value)
	{
		___subscribedLeftButton_32 = value;
	}

	inline static int32_t get_offset_of_subscribedRightButton_33() { return static_cast<int32_t>(offsetof(VRTK_ButtonControl_t2487860925, ___subscribedRightButton_33)); }
	inline int32_t get_subscribedRightButton_33() const { return ___subscribedRightButton_33; }
	inline int32_t* get_address_of_subscribedRightButton_33() { return &___subscribedRightButton_33; }
	inline void set_subscribedRightButton_33(int32_t value)
	{
		___subscribedRightButton_33 = value;
	}

	inline static int32_t get_offset_of_axisDeadzone_34() { return static_cast<int32_t>(offsetof(VRTK_ButtonControl_t2487860925, ___axisDeadzone_34)); }
	inline Vector2_t2243707579  get_axisDeadzone_34() const { return ___axisDeadzone_34; }
	inline Vector2_t2243707579 * get_address_of_axisDeadzone_34() { return &___axisDeadzone_34; }
	inline void set_axisDeadzone_34(Vector2_t2243707579  value)
	{
		___axisDeadzone_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
