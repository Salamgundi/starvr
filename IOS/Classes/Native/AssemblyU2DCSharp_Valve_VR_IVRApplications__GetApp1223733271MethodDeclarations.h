﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetApplicationSupportedMimeTypes
struct _GetApplicationSupportedMimeTypes_t1223733271;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetApplicationSupportedMimeTypes::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetApplicationSupportedMimeTypes__ctor_m181596588 (_GetApplicationSupportedMimeTypes_t1223733271 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRApplications/_GetApplicationSupportedMimeTypes::Invoke(System.String,System.String,System.UInt32)
extern "C"  bool _GetApplicationSupportedMimeTypes_Invoke_m369838034 (_GetApplicationSupportedMimeTypes_t1223733271 * __this, String_t* ___pchAppKey0, String_t* ___pchMimeTypesBuffer1, uint32_t ___unMimeTypesBuffer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetApplicationSupportedMimeTypes::BeginInvoke(System.String,System.String,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetApplicationSupportedMimeTypes_BeginInvoke_m1014319697 (_GetApplicationSupportedMimeTypes_t1223733271 * __this, String_t* ___pchAppKey0, String_t* ___pchMimeTypesBuffer1, uint32_t ___unMimeTypesBuffer2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRApplications/_GetApplicationSupportedMimeTypes::EndInvoke(System.IAsyncResult)
extern "C"  bool _GetApplicationSupportedMimeTypes_EndInvoke_m3475804392 (_GetApplicationSupportedMimeTypes_t1223733271 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
