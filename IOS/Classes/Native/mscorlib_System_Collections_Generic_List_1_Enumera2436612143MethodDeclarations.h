﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_BasicTeleport>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2956798965(__this, ___l0, method) ((  void (*) (Enumerator_t2436612143 *, List_1_t2901882469 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_BasicTeleport>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2291017029(__this, method) ((  void (*) (Enumerator_t2436612143 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_BasicTeleport>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m902908365(__this, method) ((  Il2CppObject * (*) (Enumerator_t2436612143 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_BasicTeleport>::Dispose()
#define Enumerator_Dispose_m1570230722(__this, method) ((  void (*) (Enumerator_t2436612143 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_BasicTeleport>::VerifyState()
#define Enumerator_VerifyState_m2696312075(__this, method) ((  void (*) (Enumerator_t2436612143 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_BasicTeleport>::MoveNext()
#define Enumerator_MoveNext_m2946704442(__this, method) ((  bool (*) (Enumerator_t2436612143 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_BasicTeleport>::get_Current()
#define Enumerator_get_Current_m3937533976(__this, method) ((  VRTK_BasicTeleport_t3532761337 * (*) (Enumerator_t2436612143 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
