﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_CreateOverlay
struct _CreateOverlay_t4211228331;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_CreateOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _CreateOverlay__ctor_m3882665454 (_CreateOverlay_t4211228331 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_CreateOverlay::Invoke(System.String,System.String,System.UInt64&)
extern "C"  int32_t _CreateOverlay_Invoke_m2544111771 (_CreateOverlay_t4211228331 * __this, String_t* ___pchOverlayKey0, String_t* ___pchOverlayFriendlyName1, uint64_t* ___pOverlayHandle2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_CreateOverlay::BeginInvoke(System.String,System.String,System.UInt64&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _CreateOverlay_BeginInvoke_m3774658256 (_CreateOverlay_t4211228331 * __this, String_t* ___pchOverlayKey0, String_t* ___pchOverlayFriendlyName1, uint64_t* ___pOverlayHandle2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_CreateOverlay::EndInvoke(System.UInt64&,System.IAsyncResult)
extern "C"  int32_t _CreateOverlay_EndInvoke_m4281463233 (_CreateOverlay_t4211228331 * __this, uint64_t* ___pOverlayHandle0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
