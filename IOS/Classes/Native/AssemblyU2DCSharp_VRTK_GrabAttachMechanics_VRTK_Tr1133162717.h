﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Ba3487134318.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach
struct  VRTK_TrackObjectGrabAttach_t1133162717  : public VRTK_BaseGrabAttach_t3487134318
{
public:
	// System.Single VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach::detachDistance
	float ___detachDistance_18;
	// System.Single VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach::velocityLimit
	float ___velocityLimit_19;
	// System.Single VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach::angularVelocityLimit
	float ___angularVelocityLimit_20;
	// System.Boolean VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach::isReleasable
	bool ___isReleasable_21;

public:
	inline static int32_t get_offset_of_detachDistance_18() { return static_cast<int32_t>(offsetof(VRTK_TrackObjectGrabAttach_t1133162717, ___detachDistance_18)); }
	inline float get_detachDistance_18() const { return ___detachDistance_18; }
	inline float* get_address_of_detachDistance_18() { return &___detachDistance_18; }
	inline void set_detachDistance_18(float value)
	{
		___detachDistance_18 = value;
	}

	inline static int32_t get_offset_of_velocityLimit_19() { return static_cast<int32_t>(offsetof(VRTK_TrackObjectGrabAttach_t1133162717, ___velocityLimit_19)); }
	inline float get_velocityLimit_19() const { return ___velocityLimit_19; }
	inline float* get_address_of_velocityLimit_19() { return &___velocityLimit_19; }
	inline void set_velocityLimit_19(float value)
	{
		___velocityLimit_19 = value;
	}

	inline static int32_t get_offset_of_angularVelocityLimit_20() { return static_cast<int32_t>(offsetof(VRTK_TrackObjectGrabAttach_t1133162717, ___angularVelocityLimit_20)); }
	inline float get_angularVelocityLimit_20() const { return ___angularVelocityLimit_20; }
	inline float* get_address_of_angularVelocityLimit_20() { return &___angularVelocityLimit_20; }
	inline void set_angularVelocityLimit_20(float value)
	{
		___angularVelocityLimit_20 = value;
	}

	inline static int32_t get_offset_of_isReleasable_21() { return static_cast<int32_t>(offsetof(VRTK_TrackObjectGrabAttach_t1133162717, ___isReleasable_21)); }
	inline bool get_isReleasable_21() const { return ___isReleasable_21; }
	inline bool* get_address_of_isReleasable_21() { return &___isReleasable_21; }
	inline void set_isReleasable_21(bool value)
	{
		___isReleasable_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
