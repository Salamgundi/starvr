﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.GrabAttachMechanics.VRTK_ClimbableGrabAttach
struct VRTK_ClimbableGrabAttach_t3258016228;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.GrabAttachMechanics.VRTK_ClimbableGrabAttach::.ctor()
extern "C"  void VRTK_ClimbableGrabAttach__ctor_m3088692676 (VRTK_ClimbableGrabAttach_t3258016228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_ClimbableGrabAttach::Initialise()
extern "C"  void VRTK_ClimbableGrabAttach_Initialise_m464482995 (VRTK_ClimbableGrabAttach_t3258016228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
