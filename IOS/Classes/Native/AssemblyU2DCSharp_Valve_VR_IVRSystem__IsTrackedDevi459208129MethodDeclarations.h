﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_IsTrackedDeviceConnected
struct _IsTrackedDeviceConnected_t459208129;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_IsTrackedDeviceConnected::.ctor(System.Object,System.IntPtr)
extern "C"  void _IsTrackedDeviceConnected__ctor_m1151738584 (_IsTrackedDeviceConnected_t459208129 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_IsTrackedDeviceConnected::Invoke(System.UInt32)
extern "C"  bool _IsTrackedDeviceConnected_Invoke_m3701617466 (_IsTrackedDeviceConnected_t459208129 * __this, uint32_t ___unDeviceIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_IsTrackedDeviceConnected::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _IsTrackedDeviceConnected_BeginInvoke_m1830811519 (_IsTrackedDeviceConnected_t459208129 * __this, uint32_t ___unDeviceIndex0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_IsTrackedDeviceConnected::EndInvoke(System.IAsyncResult)
extern "C"  bool _IsTrackedDeviceConnected_EndInvoke_m297553220 (_IsTrackedDeviceConnected_t459208129 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
