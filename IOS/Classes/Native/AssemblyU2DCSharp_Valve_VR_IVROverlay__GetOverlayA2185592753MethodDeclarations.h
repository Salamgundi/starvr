﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayAlpha
struct _GetOverlayAlpha_t2185592753;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayAlpha::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayAlpha__ctor_m2723175226 (_GetOverlayAlpha_t2185592753 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayAlpha::Invoke(System.UInt64,System.Single&)
extern "C"  int32_t _GetOverlayAlpha_Invoke_m3761791030 (_GetOverlayAlpha_t2185592753 * __this, uint64_t ___ulOverlayHandle0, float* ___pfAlpha1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayAlpha::BeginInvoke(System.UInt64,System.Single&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayAlpha_BeginInvoke_m3896863259 (_GetOverlayAlpha_t2185592753 * __this, uint64_t ___ulOverlayHandle0, float* ___pfAlpha1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayAlpha::EndInvoke(System.Single&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayAlpha_EndInvoke_m804099649 (_GetOverlayAlpha_t2185592753 * __this, float* ___pfAlpha0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
