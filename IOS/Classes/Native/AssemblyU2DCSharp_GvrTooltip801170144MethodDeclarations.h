﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrTooltip
struct GvrTooltip_t801170144;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrTooltip::.ctor()
extern "C"  void GvrTooltip__ctor_m1405618601 (GvrTooltip_t801170144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
