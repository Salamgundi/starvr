﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Menu_Object_Spawner
struct Menu_Object_Spawner_t1547505270;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_PrimitiveType2454390065.h"

// System.Void VRTK.Examples.Menu_Object_Spawner::.ctor()
extern "C"  void Menu_Object_Spawner__ctor_m2752235713 (Menu_Object_Spawner_t1547505270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Menu_Object_Spawner::SetSelectedColor(UnityEngine.Color)
extern "C"  void Menu_Object_Spawner_SetSelectedColor_m920390825 (Menu_Object_Spawner_t1547505270 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Menu_Object_Spawner::StartUsing(UnityEngine.GameObject)
extern "C"  void Menu_Object_Spawner_StartUsing_m3854458443 (Menu_Object_Spawner_t1547505270 * __this, GameObject_t1756533147 * ___usingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Menu_Object_Spawner::CreateShape(UnityEngine.PrimitiveType,UnityEngine.Color)
extern "C"  void Menu_Object_Spawner_CreateShape_m1633612802 (Menu_Object_Spawner_t1547505270 * __this, int32_t ___shape0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Menu_Object_Spawner::ResetMenuItems()
extern "C"  void Menu_Object_Spawner_ResetMenuItems_m1467194185 (Menu_Object_Spawner_t1547505270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
