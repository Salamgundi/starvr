﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_PollNextEvent
struct _PollNextEvent_t3908295690;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_t3405266389.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_PollNextEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void _PollNextEvent__ctor_m959155751 (_PollNextEvent_t3908295690 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_PollNextEvent::Invoke(Valve.VR.VREvent_t&,System.UInt32)
extern "C"  bool _PollNextEvent_Invoke_m2237057424 (_PollNextEvent_t3908295690 * __this, VREvent_t_t3405266389 * ___pEvent0, uint32_t ___uncbVREvent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_PollNextEvent::BeginInvoke(Valve.VR.VREvent_t&,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _PollNextEvent_BeginInvoke_m310802817 (_PollNextEvent_t3908295690 * __this, VREvent_t_t3405266389 * ___pEvent0, uint32_t ___uncbVREvent1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_PollNextEvent::EndInvoke(Valve.VR.VREvent_t&,System.IAsyncResult)
extern "C"  bool _PollNextEvent_EndInvoke_m2168403520 (_PollNextEvent_t3908295690 * __this, VREvent_t_t3405266389 * ___pEvent0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
