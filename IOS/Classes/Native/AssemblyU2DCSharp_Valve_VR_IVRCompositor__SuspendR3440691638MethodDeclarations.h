﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_SuspendRendering
struct _SuspendRendering_t3440691638;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_SuspendRendering::.ctor(System.Object,System.IntPtr)
extern "C"  void _SuspendRendering__ctor_m3786922255 (_SuspendRendering_t3440691638 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_SuspendRendering::Invoke(System.Boolean)
extern "C"  void _SuspendRendering_Invoke_m1699863032 (_SuspendRendering_t3440691638 * __this, bool ___bSuspend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_SuspendRendering::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SuspendRendering_BeginInvoke_m2827649147 (_SuspendRendering_t3440691638 * __this, bool ___bSuspend0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_SuspendRendering::EndInvoke(System.IAsyncResult)
extern "C"  void _SuspendRendering_EndInvoke_m878763577 (_SuspendRendering_t3440691638 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
