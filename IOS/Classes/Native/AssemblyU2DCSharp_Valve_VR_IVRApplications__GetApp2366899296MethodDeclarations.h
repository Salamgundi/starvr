﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetApplicationKeyByIndex
struct _GetApplicationKeyByIndex_t2366899296;
// System.Object
struct Il2CppObject;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetApplicationKeyByIndex::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetApplicationKeyByIndex__ctor_m1049022745 (_GetApplicationKeyByIndex_t2366899296 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_GetApplicationKeyByIndex::Invoke(System.UInt32,System.Text.StringBuilder,System.UInt32)
extern "C"  int32_t _GetApplicationKeyByIndex_Invoke_m1606036005 (_GetApplicationKeyByIndex_t2366899296 * __this, uint32_t ___unApplicationIndex0, StringBuilder_t1221177846 * ___pchAppKeyBuffer1, uint32_t ___unAppKeyBufferLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetApplicationKeyByIndex::BeginInvoke(System.UInt32,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetApplicationKeyByIndex_BeginInvoke_m1398330110 (_GetApplicationKeyByIndex_t2366899296 * __this, uint32_t ___unApplicationIndex0, StringBuilder_t1221177846 * ___pchAppKeyBuffer1, uint32_t ___unAppKeyBufferLen2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_GetApplicationKeyByIndex::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _GetApplicationKeyByIndex_EndInvoke_m4278421419 (_GetApplicationKeyByIndex_t2366899296 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
