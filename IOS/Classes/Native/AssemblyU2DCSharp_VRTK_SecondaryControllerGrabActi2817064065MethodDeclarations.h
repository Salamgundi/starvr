﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction
struct VRTK_AxisScaleGrabAction_t2817064065;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// VRTK.VRTK_InteractGrab
struct VRTK_InteractGrab_t124353446;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractGrab124353446.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::.ctor()
extern "C"  void VRTK_AxisScaleGrabAction__ctor_m3135000982 (VRTK_AxisScaleGrabAction_t2817064065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::Initialise(VRTK.VRTK_InteractableObject,VRTK.VRTK_InteractGrab,VRTK.VRTK_InteractGrab,UnityEngine.Transform,UnityEngine.Transform)
extern "C"  void VRTK_AxisScaleGrabAction_Initialise_m261787475 (VRTK_AxisScaleGrabAction_t2817064065 * __this, VRTK_InteractableObject_t2604188111 * ___currentGrabbdObject0, VRTK_InteractGrab_t124353446 * ___currentPrimaryGrabbingObject1, VRTK_InteractGrab_t124353446 * ___currentSecondaryGrabbingObject2, Transform_t3275118058 * ___primaryGrabPoint3, Transform_t3275118058 * ___secondaryGrabPoint4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::ProcessUpdate()
extern "C"  void VRTK_AxisScaleGrabAction_ProcessUpdate_m1893922010 (VRTK_AxisScaleGrabAction_t2817064065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::ProcessFixedUpdate()
extern "C"  void VRTK_AxisScaleGrabAction_ProcessFixedUpdate_m633523716 (VRTK_AxisScaleGrabAction_t2817064065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::ApplyScale(UnityEngine.Vector3)
extern "C"  void VRTK_AxisScaleGrabAction_ApplyScale_m4224360623 (VRTK_AxisScaleGrabAction_t2817064065 * __this, Vector3_t2243707580  ___newScale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::NonUniformScale()
extern "C"  void VRTK_AxisScaleGrabAction_NonUniformScale_m221338957 (VRTK_AxisScaleGrabAction_t2817064065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::UniformScale()
extern "C"  void VRTK_AxisScaleGrabAction_UniformScale_m2475768270 (VRTK_AxisScaleGrabAction_t2817064065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction::CalculateAxisScale(System.Single,System.Single,System.Single)
extern "C"  float VRTK_AxisScaleGrabAction_CalculateAxisScale_m2713969768 (VRTK_AxisScaleGrabAction_t2817064065 * __this, float ___centerPosition0, float ___initialPosition1, float ___currentPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
