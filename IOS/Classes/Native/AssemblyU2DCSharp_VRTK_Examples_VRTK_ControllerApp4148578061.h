﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_ControllerTooltips
struct VRTK_ControllerTooltips_t3537184660;
// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;
// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.VRTK_ControllerAppearance_Example
struct  VRTK_ControllerAppearance_Example_t4148578061  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.Examples.VRTK_ControllerAppearance_Example::highlightBodyOnlyOnCollision
	bool ___highlightBodyOnlyOnCollision_2;
	// VRTK.VRTK_ControllerTooltips VRTK.Examples.VRTK_ControllerAppearance_Example::tooltips
	VRTK_ControllerTooltips_t3537184660 * ___tooltips_3;
	// VRTK.VRTK_ControllerActions VRTK.Examples.VRTK_ControllerAppearance_Example::actions
	VRTK_ControllerActions_t3642353851 * ___actions_4;
	// VRTK.VRTK_ControllerEvents VRTK.Examples.VRTK_ControllerAppearance_Example::events
	VRTK_ControllerEvents_t3225224819 * ___events_5;

public:
	inline static int32_t get_offset_of_highlightBodyOnlyOnCollision_2() { return static_cast<int32_t>(offsetof(VRTK_ControllerAppearance_Example_t4148578061, ___highlightBodyOnlyOnCollision_2)); }
	inline bool get_highlightBodyOnlyOnCollision_2() const { return ___highlightBodyOnlyOnCollision_2; }
	inline bool* get_address_of_highlightBodyOnlyOnCollision_2() { return &___highlightBodyOnlyOnCollision_2; }
	inline void set_highlightBodyOnlyOnCollision_2(bool value)
	{
		___highlightBodyOnlyOnCollision_2 = value;
	}

	inline static int32_t get_offset_of_tooltips_3() { return static_cast<int32_t>(offsetof(VRTK_ControllerAppearance_Example_t4148578061, ___tooltips_3)); }
	inline VRTK_ControllerTooltips_t3537184660 * get_tooltips_3() const { return ___tooltips_3; }
	inline VRTK_ControllerTooltips_t3537184660 ** get_address_of_tooltips_3() { return &___tooltips_3; }
	inline void set_tooltips_3(VRTK_ControllerTooltips_t3537184660 * value)
	{
		___tooltips_3 = value;
		Il2CppCodeGenWriteBarrier(&___tooltips_3, value);
	}

	inline static int32_t get_offset_of_actions_4() { return static_cast<int32_t>(offsetof(VRTK_ControllerAppearance_Example_t4148578061, ___actions_4)); }
	inline VRTK_ControllerActions_t3642353851 * get_actions_4() const { return ___actions_4; }
	inline VRTK_ControllerActions_t3642353851 ** get_address_of_actions_4() { return &___actions_4; }
	inline void set_actions_4(VRTK_ControllerActions_t3642353851 * value)
	{
		___actions_4 = value;
		Il2CppCodeGenWriteBarrier(&___actions_4, value);
	}

	inline static int32_t get_offset_of_events_5() { return static_cast<int32_t>(offsetof(VRTK_ControllerAppearance_Example_t4148578061, ___events_5)); }
	inline VRTK_ControllerEvents_t3225224819 * get_events_5() const { return ___events_5; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_events_5() { return &___events_5; }
	inline void set_events_5(VRTK_ControllerEvents_t3225224819 * value)
	{
		___events_5 = value;
		Il2CppCodeGenWriteBarrier(&___events_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
