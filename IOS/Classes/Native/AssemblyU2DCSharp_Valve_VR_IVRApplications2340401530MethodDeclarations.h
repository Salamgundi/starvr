﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications
struct IVRApplications_t2340401530;
struct IVRApplications_t2340401530_marshaled_pinvoke;
struct IVRApplications_t2340401530_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IVRApplications_t2340401530;
struct IVRApplications_t2340401530_marshaled_pinvoke;

extern "C" void IVRApplications_t2340401530_marshal_pinvoke(const IVRApplications_t2340401530& unmarshaled, IVRApplications_t2340401530_marshaled_pinvoke& marshaled);
extern "C" void IVRApplications_t2340401530_marshal_pinvoke_back(const IVRApplications_t2340401530_marshaled_pinvoke& marshaled, IVRApplications_t2340401530& unmarshaled);
extern "C" void IVRApplications_t2340401530_marshal_pinvoke_cleanup(IVRApplications_t2340401530_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IVRApplications_t2340401530;
struct IVRApplications_t2340401530_marshaled_com;

extern "C" void IVRApplications_t2340401530_marshal_com(const IVRApplications_t2340401530& unmarshaled, IVRApplications_t2340401530_marshaled_com& marshaled);
extern "C" void IVRApplications_t2340401530_marshal_com_back(const IVRApplications_t2340401530_marshaled_com& marshaled, IVRApplications_t2340401530& unmarshaled);
extern "C" void IVRApplications_t2340401530_marshal_com_cleanup(IVRApplications_t2340401530_marshaled_com& marshaled);
