﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Settings
struct Settings_t4248570851;

#include "codegen/il2cpp-codegen.h"

// System.Void Settings::.ctor()
extern "C"  void Settings__ctor_m3923603048 (Settings_t4248570851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Settings::Start()
extern "C"  void Settings_Start_m2400200836 (Settings_t4248570851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Settings::Volume()
extern "C"  void Settings_Volume_m2233337212 (Settings_t4248570851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Settings::starcon()
extern "C"  void Settings_starcon_m4766376 (Settings_t4248570851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
