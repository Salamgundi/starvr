﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21704848460MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3285120294(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3416501934 *, int32_t, ButtonHintInfo_t106135473 *, const MethodInfo*))KeyValuePair_2__ctor_m424015945_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::get_Key()
#define KeyValuePair_2_get_Key_m1726772204(__this, method) ((  int32_t (*) (KeyValuePair_2_t3416501934 *, const MethodInfo*))KeyValuePair_2_get_Key_m3082357227_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2034180949(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3416501934 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m439326410_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::get_Value()
#define KeyValuePair_2_get_Value_m1611876790(__this, method) ((  ButtonHintInfo_t106135473 * (*) (KeyValuePair_2_t3416501934 *, const MethodInfo*))KeyValuePair_2_get_Value_m1459508371_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1421105013(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3416501934 *, ButtonHintInfo_t106135473 *, const MethodInfo*))KeyValuePair_2_set_Value_m2088473154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo>::ToString()
#define KeyValuePair_2_ToString_m814158631(__this, method) ((  String_t* (*) (KeyValuePair_2_t3416501934 *, const MethodInfo*))KeyValuePair_2_ToString_m3717289452_gshared)(__this, method)
