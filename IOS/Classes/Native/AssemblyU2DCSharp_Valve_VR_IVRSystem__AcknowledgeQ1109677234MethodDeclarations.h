﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_AcknowledgeQuit_Exiting
struct _AcknowledgeQuit_Exiting_t1109677234;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_AcknowledgeQuit_Exiting::.ctor(System.Object,System.IntPtr)
extern "C"  void _AcknowledgeQuit_Exiting__ctor_m4195251985 (_AcknowledgeQuit_Exiting_t1109677234 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_AcknowledgeQuit_Exiting::Invoke()
extern "C"  void _AcknowledgeQuit_Exiting_Invoke_m833227865 (_AcknowledgeQuit_Exiting_t1109677234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_AcknowledgeQuit_Exiting::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _AcknowledgeQuit_Exiting_BeginInvoke_m930824168 (_AcknowledgeQuit_Exiting_t1109677234 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_AcknowledgeQuit_Exiting::EndInvoke(System.IAsyncResult)
extern "C"  void _AcknowledgeQuit_Exiting_EndInvoke_m3947172267 (_AcknowledgeQuit_Exiting_t1109677234 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
