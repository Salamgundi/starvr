﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.Interactable/OnAttachedToHandDelegate
struct OnAttachedToHandDelegate_t3380341848;
// Valve.VR.InteractionSystem.Interactable/OnDetachedFromHandDelegate
struct OnDetachedFromHandDelegate_t2257352405;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.Interactable
struct  Interactable_t1274046986  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.Interactable/OnAttachedToHandDelegate Valve.VR.InteractionSystem.Interactable::onAttachedToHand
	OnAttachedToHandDelegate_t3380341848 * ___onAttachedToHand_2;
	// Valve.VR.InteractionSystem.Interactable/OnDetachedFromHandDelegate Valve.VR.InteractionSystem.Interactable::onDetachedFromHand
	OnDetachedFromHandDelegate_t2257352405 * ___onDetachedFromHand_3;

public:
	inline static int32_t get_offset_of_onAttachedToHand_2() { return static_cast<int32_t>(offsetof(Interactable_t1274046986, ___onAttachedToHand_2)); }
	inline OnAttachedToHandDelegate_t3380341848 * get_onAttachedToHand_2() const { return ___onAttachedToHand_2; }
	inline OnAttachedToHandDelegate_t3380341848 ** get_address_of_onAttachedToHand_2() { return &___onAttachedToHand_2; }
	inline void set_onAttachedToHand_2(OnAttachedToHandDelegate_t3380341848 * value)
	{
		___onAttachedToHand_2 = value;
		Il2CppCodeGenWriteBarrier(&___onAttachedToHand_2, value);
	}

	inline static int32_t get_offset_of_onDetachedFromHand_3() { return static_cast<int32_t>(offsetof(Interactable_t1274046986, ___onDetachedFromHand_3)); }
	inline OnDetachedFromHandDelegate_t2257352405 * get_onDetachedFromHand_3() const { return ___onDetachedFromHand_3; }
	inline OnDetachedFromHandDelegate_t2257352405 ** get_address_of_onDetachedFromHand_3() { return &___onDetachedFromHand_3; }
	inline void set_onDetachedFromHand_3(OnDetachedFromHandDelegate_t2257352405 * value)
	{
		___onDetachedFromHand_3 = value;
		Il2CppCodeGenWriteBarrier(&___onDetachedFromHand_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
