﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2563600722.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21704848460.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2593458328_gshared (InternalEnumerator_1_t2563600722 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2593458328(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2563600722 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2593458328_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m771306000_gshared (InternalEnumerator_1_t2563600722 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m771306000(__this, method) ((  void (*) (InternalEnumerator_1_t2563600722 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m771306000_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4241081862_gshared (InternalEnumerator_1_t2563600722 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4241081862(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2563600722 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4241081862_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m454656587_gshared (InternalEnumerator_1_t2563600722 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m454656587(__this, method) ((  void (*) (InternalEnumerator_1_t2563600722 *, const MethodInfo*))InternalEnumerator_1_Dispose_m454656587_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m177039344_gshared (InternalEnumerator_1_t2563600722 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m177039344(__this, method) ((  bool (*) (InternalEnumerator_1_t2563600722 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m177039344_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1704848460  InternalEnumerator_1_get_Current_m358423711_gshared (InternalEnumerator_1_t2563600722 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m358423711(__this, method) ((  KeyValuePair_2_t1704848460  (*) (InternalEnumerator_1_t2563600722 *, const MethodInfo*))InternalEnumerator_1_get_Current_m358423711_gshared)(__this, method)
