﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_TriggerHapticPulse
struct _TriggerHapticPulse_t158863722;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_TriggerHapticPulse::.ctor(System.Object,System.IntPtr)
extern "C"  void _TriggerHapticPulse__ctor_m2458278063 (_TriggerHapticPulse_t158863722 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_TriggerHapticPulse::Invoke(System.UInt32,System.UInt32,System.Char)
extern "C"  void _TriggerHapticPulse_Invoke_m2330932214 (_TriggerHapticPulse_t158863722 * __this, uint32_t ___unControllerDeviceIndex0, uint32_t ___unAxisId1, Il2CppChar ___usDurationMicroSec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_TriggerHapticPulse::BeginInvoke(System.UInt32,System.UInt32,System.Char,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _TriggerHapticPulse_BeginInvoke_m861065955 (_TriggerHapticPulse_t158863722 * __this, uint32_t ___unControllerDeviceIndex0, uint32_t ___unAxisId1, Il2CppChar ___usDurationMicroSec2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_TriggerHapticPulse::EndInvoke(System.IAsyncResult)
extern "C"  void _TriggerHapticPulse_EndInvoke_m756589765 (_TriggerHapticPulse_t158863722 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
