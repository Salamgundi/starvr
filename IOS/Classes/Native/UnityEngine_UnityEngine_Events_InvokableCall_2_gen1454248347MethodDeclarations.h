﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerActionsEventArgs>
struct InvokableCall_2_t1454248347;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.ControllerActionsEventArgs>
struct UnityAction_2_t1439457463;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerActionsEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m1250901645_gshared (InvokableCall_2_t1454248347 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_2__ctor_m1250901645(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_2_t1454248347 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2__ctor_m1250901645_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerActionsEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m3537835750_gshared (InvokableCall_2_t1454248347 * __this, UnityAction_2_t1439457463 * ___action0, const MethodInfo* method);
#define InvokableCall_2__ctor_m3537835750(__this, ___action0, method) ((  void (*) (InvokableCall_2_t1454248347 *, UnityAction_2_t1439457463 *, const MethodInfo*))InvokableCall_2__ctor_m3537835750_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerActionsEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m679562273_gshared (InvokableCall_2_t1454248347 * __this, UnityAction_2_t1439457463 * ___value0, const MethodInfo* method);
#define InvokableCall_2_add_Delegate_m679562273(__this, ___value0, method) ((  void (*) (InvokableCall_2_t1454248347 *, UnityAction_2_t1439457463 *, const MethodInfo*))InvokableCall_2_add_Delegate_m679562273_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerActionsEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m2574429696_gshared (InvokableCall_2_t1454248347 * __this, UnityAction_2_t1439457463 * ___value0, const MethodInfo* method);
#define InvokableCall_2_remove_Delegate_m2574429696(__this, ___value0, method) ((  void (*) (InvokableCall_2_t1454248347 *, UnityAction_2_t1439457463 *, const MethodInfo*))InvokableCall_2_remove_Delegate_m2574429696_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerActionsEventArgs>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m686209022_gshared (InvokableCall_2_t1454248347 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_2_Invoke_m686209022(__this, ___args0, method) ((  void (*) (InvokableCall_2_t1454248347 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_2_Invoke_m686209022_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.ControllerActionsEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m2712851434_gshared (InvokableCall_2_t1454248347 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_2_Find_m2712851434(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_2_t1454248347 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2_Find_m2712851434_gshared)(__this, ___targetObj0, ___method1, method)
