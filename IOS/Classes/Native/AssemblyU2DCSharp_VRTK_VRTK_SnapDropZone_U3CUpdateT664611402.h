﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_SnapDropZone
struct VRTK_SnapDropZone_t1948041105;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0
struct  U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402  : public Il2CppObject
{
public:
	// System.Single VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_0;
	// VRTK.VRTK_InteractableObject VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::ioCheck
	VRTK_InteractableObject_t2604188111 * ___ioCheck_1;
	// UnityEngine.Transform VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::<ioTransform>__1
	Transform_t3275118058 * ___U3CioTransformU3E__1_2;
	// UnityEngine.Vector3 VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::<startPosition>__2
	Vector3_t2243707580  ___U3CstartPositionU3E__2_3;
	// UnityEngine.Quaternion VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::<startRotation>__3
	Quaternion_t4030073918  ___U3CstartRotationU3E__3_4;
	// UnityEngine.Vector3 VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::<startScale>__4
	Vector3_t2243707580  ___U3CstartScaleU3E__4_5;
	// System.Boolean VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::<storedKinematicState>__5
	bool ___U3CstoredKinematicStateU3E__5_6;
	// System.Single VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::duration
	float ___duration_7;
	// UnityEngine.GameObject VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::endSettings
	GameObject_t1756533147 * ___endSettings_8;
	// UnityEngine.Vector3 VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::endScale
	Vector3_t2243707580  ___endScale_9;
	// VRTK.VRTK_SnapDropZone VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::$this
	VRTK_SnapDropZone_t1948041105 * ___U24this_10;
	// System.Object VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::$current
	Il2CppObject * ___U24current_11;
	// System.Boolean VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::$disposing
	bool ___U24disposing_12;
	// System.Int32 VRTK.VRTK_SnapDropZone/<UpdateTransformDimensions>c__Iterator0::$PC
	int32_t ___U24PC_13;

public:
	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402, ___U3CelapsedTimeU3E__0_0)); }
	inline float get_U3CelapsedTimeU3E__0_0() const { return ___U3CelapsedTimeU3E__0_0; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_0() { return &___U3CelapsedTimeU3E__0_0; }
	inline void set_U3CelapsedTimeU3E__0_0(float value)
	{
		___U3CelapsedTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_ioCheck_1() { return static_cast<int32_t>(offsetof(U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402, ___ioCheck_1)); }
	inline VRTK_InteractableObject_t2604188111 * get_ioCheck_1() const { return ___ioCheck_1; }
	inline VRTK_InteractableObject_t2604188111 ** get_address_of_ioCheck_1() { return &___ioCheck_1; }
	inline void set_ioCheck_1(VRTK_InteractableObject_t2604188111 * value)
	{
		___ioCheck_1 = value;
		Il2CppCodeGenWriteBarrier(&___ioCheck_1, value);
	}

	inline static int32_t get_offset_of_U3CioTransformU3E__1_2() { return static_cast<int32_t>(offsetof(U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402, ___U3CioTransformU3E__1_2)); }
	inline Transform_t3275118058 * get_U3CioTransformU3E__1_2() const { return ___U3CioTransformU3E__1_2; }
	inline Transform_t3275118058 ** get_address_of_U3CioTransformU3E__1_2() { return &___U3CioTransformU3E__1_2; }
	inline void set_U3CioTransformU3E__1_2(Transform_t3275118058 * value)
	{
		___U3CioTransformU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CioTransformU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CstartPositionU3E__2_3() { return static_cast<int32_t>(offsetof(U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402, ___U3CstartPositionU3E__2_3)); }
	inline Vector3_t2243707580  get_U3CstartPositionU3E__2_3() const { return ___U3CstartPositionU3E__2_3; }
	inline Vector3_t2243707580 * get_address_of_U3CstartPositionU3E__2_3() { return &___U3CstartPositionU3E__2_3; }
	inline void set_U3CstartPositionU3E__2_3(Vector3_t2243707580  value)
	{
		___U3CstartPositionU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CstartRotationU3E__3_4() { return static_cast<int32_t>(offsetof(U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402, ___U3CstartRotationU3E__3_4)); }
	inline Quaternion_t4030073918  get_U3CstartRotationU3E__3_4() const { return ___U3CstartRotationU3E__3_4; }
	inline Quaternion_t4030073918 * get_address_of_U3CstartRotationU3E__3_4() { return &___U3CstartRotationU3E__3_4; }
	inline void set_U3CstartRotationU3E__3_4(Quaternion_t4030073918  value)
	{
		___U3CstartRotationU3E__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CstartScaleU3E__4_5() { return static_cast<int32_t>(offsetof(U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402, ___U3CstartScaleU3E__4_5)); }
	inline Vector3_t2243707580  get_U3CstartScaleU3E__4_5() const { return ___U3CstartScaleU3E__4_5; }
	inline Vector3_t2243707580 * get_address_of_U3CstartScaleU3E__4_5() { return &___U3CstartScaleU3E__4_5; }
	inline void set_U3CstartScaleU3E__4_5(Vector3_t2243707580  value)
	{
		___U3CstartScaleU3E__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CstoredKinematicStateU3E__5_6() { return static_cast<int32_t>(offsetof(U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402, ___U3CstoredKinematicStateU3E__5_6)); }
	inline bool get_U3CstoredKinematicStateU3E__5_6() const { return ___U3CstoredKinematicStateU3E__5_6; }
	inline bool* get_address_of_U3CstoredKinematicStateU3E__5_6() { return &___U3CstoredKinematicStateU3E__5_6; }
	inline void set_U3CstoredKinematicStateU3E__5_6(bool value)
	{
		___U3CstoredKinematicStateU3E__5_6 = value;
	}

	inline static int32_t get_offset_of_duration_7() { return static_cast<int32_t>(offsetof(U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402, ___duration_7)); }
	inline float get_duration_7() const { return ___duration_7; }
	inline float* get_address_of_duration_7() { return &___duration_7; }
	inline void set_duration_7(float value)
	{
		___duration_7 = value;
	}

	inline static int32_t get_offset_of_endSettings_8() { return static_cast<int32_t>(offsetof(U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402, ___endSettings_8)); }
	inline GameObject_t1756533147 * get_endSettings_8() const { return ___endSettings_8; }
	inline GameObject_t1756533147 ** get_address_of_endSettings_8() { return &___endSettings_8; }
	inline void set_endSettings_8(GameObject_t1756533147 * value)
	{
		___endSettings_8 = value;
		Il2CppCodeGenWriteBarrier(&___endSettings_8, value);
	}

	inline static int32_t get_offset_of_endScale_9() { return static_cast<int32_t>(offsetof(U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402, ___endScale_9)); }
	inline Vector3_t2243707580  get_endScale_9() const { return ___endScale_9; }
	inline Vector3_t2243707580 * get_address_of_endScale_9() { return &___endScale_9; }
	inline void set_endScale_9(Vector3_t2243707580  value)
	{
		___endScale_9 = value;
	}

	inline static int32_t get_offset_of_U24this_10() { return static_cast<int32_t>(offsetof(U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402, ___U24this_10)); }
	inline VRTK_SnapDropZone_t1948041105 * get_U24this_10() const { return ___U24this_10; }
	inline VRTK_SnapDropZone_t1948041105 ** get_address_of_U24this_10() { return &___U24this_10; }
	inline void set_U24this_10(VRTK_SnapDropZone_t1948041105 * value)
	{
		___U24this_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_10, value);
	}

	inline static int32_t get_offset_of_U24current_11() { return static_cast<int32_t>(offsetof(U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402, ___U24current_11)); }
	inline Il2CppObject * get_U24current_11() const { return ___U24current_11; }
	inline Il2CppObject ** get_address_of_U24current_11() { return &___U24current_11; }
	inline void set_U24current_11(Il2CppObject * value)
	{
		___U24current_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_11, value);
	}

	inline static int32_t get_offset_of_U24disposing_12() { return static_cast<int32_t>(offsetof(U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402, ___U24disposing_12)); }
	inline bool get_U24disposing_12() const { return ___U24disposing_12; }
	inline bool* get_address_of_U24disposing_12() { return &___U24disposing_12; }
	inline void set_U24disposing_12(bool value)
	{
		___U24disposing_12 = value;
	}

	inline static int32_t get_offset_of_U24PC_13() { return static_cast<int32_t>(offsetof(U3CUpdateTransformDimensionsU3Ec__Iterator0_t664611402, ___U24PC_13)); }
	inline int32_t get_U24PC_13() const { return ___U24PC_13; }
	inline int32_t* get_address_of_U24PC_13() { return &___U24PC_13; }
	inline void set_U24PC_13(int32_t value)
	{
		___U24PC_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
