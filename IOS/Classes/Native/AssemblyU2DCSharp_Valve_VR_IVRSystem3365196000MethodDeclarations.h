﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem
struct IVRSystem_t3365196000;
struct IVRSystem_t3365196000_marshaled_pinvoke;
struct IVRSystem_t3365196000_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IVRSystem_t3365196000;
struct IVRSystem_t3365196000_marshaled_pinvoke;

extern "C" void IVRSystem_t3365196000_marshal_pinvoke(const IVRSystem_t3365196000& unmarshaled, IVRSystem_t3365196000_marshaled_pinvoke& marshaled);
extern "C" void IVRSystem_t3365196000_marshal_pinvoke_back(const IVRSystem_t3365196000_marshaled_pinvoke& marshaled, IVRSystem_t3365196000& unmarshaled);
extern "C" void IVRSystem_t3365196000_marshal_pinvoke_cleanup(IVRSystem_t3365196000_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IVRSystem_t3365196000;
struct IVRSystem_t3365196000_marshaled_com;

extern "C" void IVRSystem_t3365196000_marshal_com(const IVRSystem_t3365196000& unmarshaled, IVRSystem_t3365196000_marshaled_com& marshaled);
extern "C" void IVRSystem_t3365196000_marshal_com_back(const IVRSystem_t3365196000_marshaled_com& marshaled, IVRSystem_t3365196000& unmarshaled);
extern "C" void IVRSystem_t3365196000_marshal_com_cleanup(IVRSystem_t3365196000_marshaled_com& marshaled);
