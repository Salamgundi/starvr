﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.UseRotate
struct  UseRotate_t241092046  : public VRTK_InteractableObject_t2604188111
{
public:
	// System.Single VRTK.Examples.UseRotate::idleSpinSpeed
	float ___idleSpinSpeed_45;
	// System.Single VRTK.Examples.UseRotate::activeSpinSpeed
	float ___activeSpinSpeed_46;
	// UnityEngine.Transform VRTK.Examples.UseRotate::rotatingObject
	Transform_t3275118058 * ___rotatingObject_47;
	// UnityEngine.Vector3 VRTK.Examples.UseRotate::rotationAxis
	Vector3_t2243707580  ___rotationAxis_48;
	// System.Single VRTK.Examples.UseRotate::spinSpeed
	float ___spinSpeed_49;

public:
	inline static int32_t get_offset_of_idleSpinSpeed_45() { return static_cast<int32_t>(offsetof(UseRotate_t241092046, ___idleSpinSpeed_45)); }
	inline float get_idleSpinSpeed_45() const { return ___idleSpinSpeed_45; }
	inline float* get_address_of_idleSpinSpeed_45() { return &___idleSpinSpeed_45; }
	inline void set_idleSpinSpeed_45(float value)
	{
		___idleSpinSpeed_45 = value;
	}

	inline static int32_t get_offset_of_activeSpinSpeed_46() { return static_cast<int32_t>(offsetof(UseRotate_t241092046, ___activeSpinSpeed_46)); }
	inline float get_activeSpinSpeed_46() const { return ___activeSpinSpeed_46; }
	inline float* get_address_of_activeSpinSpeed_46() { return &___activeSpinSpeed_46; }
	inline void set_activeSpinSpeed_46(float value)
	{
		___activeSpinSpeed_46 = value;
	}

	inline static int32_t get_offset_of_rotatingObject_47() { return static_cast<int32_t>(offsetof(UseRotate_t241092046, ___rotatingObject_47)); }
	inline Transform_t3275118058 * get_rotatingObject_47() const { return ___rotatingObject_47; }
	inline Transform_t3275118058 ** get_address_of_rotatingObject_47() { return &___rotatingObject_47; }
	inline void set_rotatingObject_47(Transform_t3275118058 * value)
	{
		___rotatingObject_47 = value;
		Il2CppCodeGenWriteBarrier(&___rotatingObject_47, value);
	}

	inline static int32_t get_offset_of_rotationAxis_48() { return static_cast<int32_t>(offsetof(UseRotate_t241092046, ___rotationAxis_48)); }
	inline Vector3_t2243707580  get_rotationAxis_48() const { return ___rotationAxis_48; }
	inline Vector3_t2243707580 * get_address_of_rotationAxis_48() { return &___rotationAxis_48; }
	inline void set_rotationAxis_48(Vector3_t2243707580  value)
	{
		___rotationAxis_48 = value;
	}

	inline static int32_t get_offset_of_spinSpeed_49() { return static_cast<int32_t>(offsetof(UseRotate_t241092046, ___spinSpeed_49)); }
	inline float get_spinSpeed_49() const { return ___spinSpeed_49; }
	inline float* get_address_of_spinSpeed_49() { return &___spinSpeed_49; }
	inline void set_spinSpeed_49(float value)
	{
		___spinSpeed_49 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
