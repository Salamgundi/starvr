﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_BasicTeleport
struct VRTK_BasicTeleport_t3532761337;
// VRTK.VRTK_HeadsetControllerAware
struct VRTK_HeadsetControllerAware_t1678000416;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_TeleportDisableOnControllerObscured
struct  VRTK_TeleportDisableOnControllerObscured_t3503067553  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_BasicTeleport VRTK.VRTK_TeleportDisableOnControllerObscured::basicTeleport
	VRTK_BasicTeleport_t3532761337 * ___basicTeleport_2;
	// VRTK.VRTK_HeadsetControllerAware VRTK.VRTK_TeleportDisableOnControllerObscured::headset
	VRTK_HeadsetControllerAware_t1678000416 * ___headset_3;

public:
	inline static int32_t get_offset_of_basicTeleport_2() { return static_cast<int32_t>(offsetof(VRTK_TeleportDisableOnControllerObscured_t3503067553, ___basicTeleport_2)); }
	inline VRTK_BasicTeleport_t3532761337 * get_basicTeleport_2() const { return ___basicTeleport_2; }
	inline VRTK_BasicTeleport_t3532761337 ** get_address_of_basicTeleport_2() { return &___basicTeleport_2; }
	inline void set_basicTeleport_2(VRTK_BasicTeleport_t3532761337 * value)
	{
		___basicTeleport_2 = value;
		Il2CppCodeGenWriteBarrier(&___basicTeleport_2, value);
	}

	inline static int32_t get_offset_of_headset_3() { return static_cast<int32_t>(offsetof(VRTK_TeleportDisableOnControllerObscured_t3503067553, ___headset_3)); }
	inline VRTK_HeadsetControllerAware_t1678000416 * get_headset_3() const { return ___headset_3; }
	inline VRTK_HeadsetControllerAware_t1678000416 ** get_address_of_headset_3() { return &___headset_3; }
	inline void set_headset_3(VRTK_HeadsetControllerAware_t1678000416 * value)
	{
		___headset_3 = value;
		Il2CppCodeGenWriteBarrier(&___headset_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
