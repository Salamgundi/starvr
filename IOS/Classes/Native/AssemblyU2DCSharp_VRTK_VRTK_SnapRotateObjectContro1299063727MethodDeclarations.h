﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_SnapRotateObjectControlAction
struct VRTK_SnapRotateObjectControlAction_t1299063727;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_SnapRotateObjectControlAction::.ctor()
extern "C"  void VRTK_SnapRotateObjectControlAction__ctor_m4235429919 (VRTK_SnapRotateObjectControlAction_t1299063727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapRotateObjectControlAction::Process(UnityEngine.GameObject,UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Single,System.Boolean,System.Boolean)
extern "C"  void VRTK_SnapRotateObjectControlAction_Process_m2490099298 (VRTK_SnapRotateObjectControlAction_t1299063727 * __this, GameObject_t1756533147 * ___controlledGameObject0, Transform_t3275118058 * ___directionDevice1, Vector3_t2243707580  ___axisDirection2, float ___axis3, float ___deadzone4, bool ___currentlyFalling5, bool ___modifierActive6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SnapRotateObjectControlAction::ValidThreshold(System.Single)
extern "C"  bool VRTK_SnapRotateObjectControlAction_ValidThreshold_m3524679891 (VRTK_SnapRotateObjectControlAction_t1299063727 * __this, float ___axis0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_SnapRotateObjectControlAction::Rotate(System.Single,System.Boolean)
extern "C"  float VRTK_SnapRotateObjectControlAction_Rotate_m2073716054 (VRTK_SnapRotateObjectControlAction_t1299063727 * __this, float ___axis0, bool ___modifierActive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
