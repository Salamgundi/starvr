﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.HingeJoint
struct HingeJoint_t2745110831;
// UnityEngine.ConstantForce
struct ConstantForce_t3796310167;

#include "AssemblyU2DCSharp_VRTK_VRTK_Control651619021.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_Direction3775008092.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Door
struct  VRTK_Door_t1992640360  : public VRTK_Control_t651619021
{
public:
	// VRTK.VRTK_Control/Direction VRTK.VRTK_Door::direction
	int32_t ___direction_15;
	// UnityEngine.GameObject VRTK.VRTK_Door::door
	GameObject_t1756533147 * ___door_16;
	// UnityEngine.GameObject VRTK.VRTK_Door::handles
	GameObject_t1756533147 * ___handles_17;
	// UnityEngine.GameObject VRTK.VRTK_Door::frame
	GameObject_t1756533147 * ___frame_18;
	// UnityEngine.GameObject VRTK.VRTK_Door::content
	GameObject_t1756533147 * ___content_19;
	// System.Boolean VRTK.VRTK_Door::hideContent
	bool ___hideContent_20;
	// System.Single VRTK.VRTK_Door::maxAngle
	float ___maxAngle_21;
	// System.Boolean VRTK.VRTK_Door::openInward
	bool ___openInward_22;
	// System.Boolean VRTK.VRTK_Door::openOutward
	bool ___openOutward_23;
	// System.Single VRTK.VRTK_Door::minSnapClose
	float ___minSnapClose_24;
	// System.Single VRTK.VRTK_Door::releasedFriction
	float ___releasedFriction_25;
	// System.Single VRTK.VRTK_Door::grabbedFriction
	float ___grabbedFriction_26;
	// System.Boolean VRTK.VRTK_Door::handleInteractableOnly
	bool ___handleInteractableOnly_27;
	// System.Single VRTK.VRTK_Door::stepSize
	float ___stepSize_28;
	// UnityEngine.Rigidbody VRTK.VRTK_Door::doorRigidbody
	Rigidbody_t4233889191 * ___doorRigidbody_29;
	// UnityEngine.HingeJoint VRTK.VRTK_Door::doorHinge
	HingeJoint_t2745110831 * ___doorHinge_30;
	// UnityEngine.ConstantForce VRTK.VRTK_Door::doorSnapForce
	ConstantForce_t3796310167 * ___doorSnapForce_31;
	// UnityEngine.Rigidbody VRTK.VRTK_Door::frameRigidbody
	Rigidbody_t4233889191 * ___frameRigidbody_32;
	// VRTK.VRTK_Control/Direction VRTK.VRTK_Door::finalDirection
	int32_t ___finalDirection_33;
	// System.Single VRTK.VRTK_Door::subDirection
	float ___subDirection_34;
	// UnityEngine.Vector3 VRTK.VRTK_Door::secondaryDirection
	Vector3_t2243707580  ___secondaryDirection_35;
	// System.Boolean VRTK.VRTK_Door::doorHingeCreated
	bool ___doorHingeCreated_36;
	// System.Boolean VRTK.VRTK_Door::doorSnapForceCreated
	bool ___doorSnapForceCreated_37;

public:
	inline static int32_t get_offset_of_direction_15() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___direction_15)); }
	inline int32_t get_direction_15() const { return ___direction_15; }
	inline int32_t* get_address_of_direction_15() { return &___direction_15; }
	inline void set_direction_15(int32_t value)
	{
		___direction_15 = value;
	}

	inline static int32_t get_offset_of_door_16() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___door_16)); }
	inline GameObject_t1756533147 * get_door_16() const { return ___door_16; }
	inline GameObject_t1756533147 ** get_address_of_door_16() { return &___door_16; }
	inline void set_door_16(GameObject_t1756533147 * value)
	{
		___door_16 = value;
		Il2CppCodeGenWriteBarrier(&___door_16, value);
	}

	inline static int32_t get_offset_of_handles_17() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___handles_17)); }
	inline GameObject_t1756533147 * get_handles_17() const { return ___handles_17; }
	inline GameObject_t1756533147 ** get_address_of_handles_17() { return &___handles_17; }
	inline void set_handles_17(GameObject_t1756533147 * value)
	{
		___handles_17 = value;
		Il2CppCodeGenWriteBarrier(&___handles_17, value);
	}

	inline static int32_t get_offset_of_frame_18() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___frame_18)); }
	inline GameObject_t1756533147 * get_frame_18() const { return ___frame_18; }
	inline GameObject_t1756533147 ** get_address_of_frame_18() { return &___frame_18; }
	inline void set_frame_18(GameObject_t1756533147 * value)
	{
		___frame_18 = value;
		Il2CppCodeGenWriteBarrier(&___frame_18, value);
	}

	inline static int32_t get_offset_of_content_19() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___content_19)); }
	inline GameObject_t1756533147 * get_content_19() const { return ___content_19; }
	inline GameObject_t1756533147 ** get_address_of_content_19() { return &___content_19; }
	inline void set_content_19(GameObject_t1756533147 * value)
	{
		___content_19 = value;
		Il2CppCodeGenWriteBarrier(&___content_19, value);
	}

	inline static int32_t get_offset_of_hideContent_20() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___hideContent_20)); }
	inline bool get_hideContent_20() const { return ___hideContent_20; }
	inline bool* get_address_of_hideContent_20() { return &___hideContent_20; }
	inline void set_hideContent_20(bool value)
	{
		___hideContent_20 = value;
	}

	inline static int32_t get_offset_of_maxAngle_21() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___maxAngle_21)); }
	inline float get_maxAngle_21() const { return ___maxAngle_21; }
	inline float* get_address_of_maxAngle_21() { return &___maxAngle_21; }
	inline void set_maxAngle_21(float value)
	{
		___maxAngle_21 = value;
	}

	inline static int32_t get_offset_of_openInward_22() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___openInward_22)); }
	inline bool get_openInward_22() const { return ___openInward_22; }
	inline bool* get_address_of_openInward_22() { return &___openInward_22; }
	inline void set_openInward_22(bool value)
	{
		___openInward_22 = value;
	}

	inline static int32_t get_offset_of_openOutward_23() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___openOutward_23)); }
	inline bool get_openOutward_23() const { return ___openOutward_23; }
	inline bool* get_address_of_openOutward_23() { return &___openOutward_23; }
	inline void set_openOutward_23(bool value)
	{
		___openOutward_23 = value;
	}

	inline static int32_t get_offset_of_minSnapClose_24() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___minSnapClose_24)); }
	inline float get_minSnapClose_24() const { return ___minSnapClose_24; }
	inline float* get_address_of_minSnapClose_24() { return &___minSnapClose_24; }
	inline void set_minSnapClose_24(float value)
	{
		___minSnapClose_24 = value;
	}

	inline static int32_t get_offset_of_releasedFriction_25() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___releasedFriction_25)); }
	inline float get_releasedFriction_25() const { return ___releasedFriction_25; }
	inline float* get_address_of_releasedFriction_25() { return &___releasedFriction_25; }
	inline void set_releasedFriction_25(float value)
	{
		___releasedFriction_25 = value;
	}

	inline static int32_t get_offset_of_grabbedFriction_26() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___grabbedFriction_26)); }
	inline float get_grabbedFriction_26() const { return ___grabbedFriction_26; }
	inline float* get_address_of_grabbedFriction_26() { return &___grabbedFriction_26; }
	inline void set_grabbedFriction_26(float value)
	{
		___grabbedFriction_26 = value;
	}

	inline static int32_t get_offset_of_handleInteractableOnly_27() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___handleInteractableOnly_27)); }
	inline bool get_handleInteractableOnly_27() const { return ___handleInteractableOnly_27; }
	inline bool* get_address_of_handleInteractableOnly_27() { return &___handleInteractableOnly_27; }
	inline void set_handleInteractableOnly_27(bool value)
	{
		___handleInteractableOnly_27 = value;
	}

	inline static int32_t get_offset_of_stepSize_28() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___stepSize_28)); }
	inline float get_stepSize_28() const { return ___stepSize_28; }
	inline float* get_address_of_stepSize_28() { return &___stepSize_28; }
	inline void set_stepSize_28(float value)
	{
		___stepSize_28 = value;
	}

	inline static int32_t get_offset_of_doorRigidbody_29() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___doorRigidbody_29)); }
	inline Rigidbody_t4233889191 * get_doorRigidbody_29() const { return ___doorRigidbody_29; }
	inline Rigidbody_t4233889191 ** get_address_of_doorRigidbody_29() { return &___doorRigidbody_29; }
	inline void set_doorRigidbody_29(Rigidbody_t4233889191 * value)
	{
		___doorRigidbody_29 = value;
		Il2CppCodeGenWriteBarrier(&___doorRigidbody_29, value);
	}

	inline static int32_t get_offset_of_doorHinge_30() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___doorHinge_30)); }
	inline HingeJoint_t2745110831 * get_doorHinge_30() const { return ___doorHinge_30; }
	inline HingeJoint_t2745110831 ** get_address_of_doorHinge_30() { return &___doorHinge_30; }
	inline void set_doorHinge_30(HingeJoint_t2745110831 * value)
	{
		___doorHinge_30 = value;
		Il2CppCodeGenWriteBarrier(&___doorHinge_30, value);
	}

	inline static int32_t get_offset_of_doorSnapForce_31() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___doorSnapForce_31)); }
	inline ConstantForce_t3796310167 * get_doorSnapForce_31() const { return ___doorSnapForce_31; }
	inline ConstantForce_t3796310167 ** get_address_of_doorSnapForce_31() { return &___doorSnapForce_31; }
	inline void set_doorSnapForce_31(ConstantForce_t3796310167 * value)
	{
		___doorSnapForce_31 = value;
		Il2CppCodeGenWriteBarrier(&___doorSnapForce_31, value);
	}

	inline static int32_t get_offset_of_frameRigidbody_32() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___frameRigidbody_32)); }
	inline Rigidbody_t4233889191 * get_frameRigidbody_32() const { return ___frameRigidbody_32; }
	inline Rigidbody_t4233889191 ** get_address_of_frameRigidbody_32() { return &___frameRigidbody_32; }
	inline void set_frameRigidbody_32(Rigidbody_t4233889191 * value)
	{
		___frameRigidbody_32 = value;
		Il2CppCodeGenWriteBarrier(&___frameRigidbody_32, value);
	}

	inline static int32_t get_offset_of_finalDirection_33() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___finalDirection_33)); }
	inline int32_t get_finalDirection_33() const { return ___finalDirection_33; }
	inline int32_t* get_address_of_finalDirection_33() { return &___finalDirection_33; }
	inline void set_finalDirection_33(int32_t value)
	{
		___finalDirection_33 = value;
	}

	inline static int32_t get_offset_of_subDirection_34() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___subDirection_34)); }
	inline float get_subDirection_34() const { return ___subDirection_34; }
	inline float* get_address_of_subDirection_34() { return &___subDirection_34; }
	inline void set_subDirection_34(float value)
	{
		___subDirection_34 = value;
	}

	inline static int32_t get_offset_of_secondaryDirection_35() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___secondaryDirection_35)); }
	inline Vector3_t2243707580  get_secondaryDirection_35() const { return ___secondaryDirection_35; }
	inline Vector3_t2243707580 * get_address_of_secondaryDirection_35() { return &___secondaryDirection_35; }
	inline void set_secondaryDirection_35(Vector3_t2243707580  value)
	{
		___secondaryDirection_35 = value;
	}

	inline static int32_t get_offset_of_doorHingeCreated_36() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___doorHingeCreated_36)); }
	inline bool get_doorHingeCreated_36() const { return ___doorHingeCreated_36; }
	inline bool* get_address_of_doorHingeCreated_36() { return &___doorHingeCreated_36; }
	inline void set_doorHingeCreated_36(bool value)
	{
		___doorHingeCreated_36 = value;
	}

	inline static int32_t get_offset_of_doorSnapForceCreated_37() { return static_cast<int32_t>(offsetof(VRTK_Door_t1992640360, ___doorSnapForceCreated_37)); }
	inline bool get_doorSnapForceCreated_37() const { return ___doorSnapForceCreated_37; }
	inline bool* get_address_of_doorSnapForceCreated_37() { return &___doorSnapForceCreated_37; }
	inline void set_doorSnapForceCreated_37(bool value)
	{
		___doorSnapForceCreated_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
