﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3226471752;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.AfterTimer_Component
struct  AfterTimer_Component_t442788763  : public MonoBehaviour_t1158329972
{
public:
	// System.Action Valve.VR.InteractionSystem.AfterTimer_Component::callback
	Action_t3226471752 * ___callback_2;
	// System.Single Valve.VR.InteractionSystem.AfterTimer_Component::triggerTime
	float ___triggerTime_3;
	// System.Boolean Valve.VR.InteractionSystem.AfterTimer_Component::timerActive
	bool ___timerActive_4;
	// System.Boolean Valve.VR.InteractionSystem.AfterTimer_Component::triggerOnEarlyDestroy
	bool ___triggerOnEarlyDestroy_5;

public:
	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(AfterTimer_Component_t442788763, ___callback_2)); }
	inline Action_t3226471752 * get_callback_2() const { return ___callback_2; }
	inline Action_t3226471752 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_t3226471752 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier(&___callback_2, value);
	}

	inline static int32_t get_offset_of_triggerTime_3() { return static_cast<int32_t>(offsetof(AfterTimer_Component_t442788763, ___triggerTime_3)); }
	inline float get_triggerTime_3() const { return ___triggerTime_3; }
	inline float* get_address_of_triggerTime_3() { return &___triggerTime_3; }
	inline void set_triggerTime_3(float value)
	{
		___triggerTime_3 = value;
	}

	inline static int32_t get_offset_of_timerActive_4() { return static_cast<int32_t>(offsetof(AfterTimer_Component_t442788763, ___timerActive_4)); }
	inline bool get_timerActive_4() const { return ___timerActive_4; }
	inline bool* get_address_of_timerActive_4() { return &___timerActive_4; }
	inline void set_timerActive_4(bool value)
	{
		___timerActive_4 = value;
	}

	inline static int32_t get_offset_of_triggerOnEarlyDestroy_5() { return static_cast<int32_t>(offsetof(AfterTimer_Component_t442788763, ___triggerOnEarlyDestroy_5)); }
	inline bool get_triggerOnEarlyDestroy_5() const { return ___triggerOnEarlyDestroy_5; }
	inline bool* get_address_of_triggerOnEarlyDestroy_5() { return &___triggerOnEarlyDestroy_5; }
	inline void set_triggerOnEarlyDestroy_5(bool value)
	{
		___triggerOnEarlyDestroy_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
