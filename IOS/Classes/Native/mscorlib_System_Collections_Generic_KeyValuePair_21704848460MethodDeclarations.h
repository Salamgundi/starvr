﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21704848460.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRButtonId66145412.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m424015945_gshared (KeyValuePair_2_t1704848460 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m424015945(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1704848460 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m424015945_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m3082357227_gshared (KeyValuePair_2_t1704848460 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m3082357227(__this, method) ((  int32_t (*) (KeyValuePair_2_t1704848460 *, const MethodInfo*))KeyValuePair_2_get_Key_m3082357227_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m439326410_gshared (KeyValuePair_2_t1704848460 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m439326410(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1704848460 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m439326410_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1459508371_gshared (KeyValuePair_2_t1704848460 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1459508371(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1704848460 *, const MethodInfo*))KeyValuePair_2_get_Value_m1459508371_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2088473154_gshared (KeyValuePair_2_t1704848460 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2088473154(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1704848460 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2088473154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Valve.VR.EVRButtonId,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3717289452_gshared (KeyValuePair_2_t1704848460 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3717289452(__this, method) ((  String_t* (*) (KeyValuePair_2_t1704848460 *, const MethodInfo*))KeyValuePair_2_ToString_m3717289452_gshared)(__this, method)
