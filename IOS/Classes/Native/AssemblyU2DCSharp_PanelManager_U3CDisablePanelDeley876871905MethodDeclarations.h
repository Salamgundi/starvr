﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PanelManager/<DisablePanelDeleyed>c__Iterator0
struct U3CDisablePanelDeleyedU3Ec__Iterator0_t876871905;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PanelManager/<DisablePanelDeleyed>c__Iterator0::.ctor()
extern "C"  void U3CDisablePanelDeleyedU3Ec__Iterator0__ctor_m1998794686 (U3CDisablePanelDeleyedU3Ec__Iterator0_t876871905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PanelManager/<DisablePanelDeleyed>c__Iterator0::MoveNext()
extern "C"  bool U3CDisablePanelDeleyedU3Ec__Iterator0_MoveNext_m829352782 (U3CDisablePanelDeleyedU3Ec__Iterator0_t876871905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PanelManager/<DisablePanelDeleyed>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDisablePanelDeleyedU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1097380618 (U3CDisablePanelDeleyedU3Ec__Iterator0_t876871905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PanelManager/<DisablePanelDeleyed>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDisablePanelDeleyedU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2211310418 (U3CDisablePanelDeleyedU3Ec__Iterator0_t876871905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelManager/<DisablePanelDeleyed>c__Iterator0::Dispose()
extern "C"  void U3CDisablePanelDeleyedU3Ec__Iterator0_Dispose_m2758513715 (U3CDisablePanelDeleyedU3Ec__Iterator0_t876871905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelManager/<DisablePanelDeleyed>c__Iterator0::Reset()
extern "C"  void U3CDisablePanelDeleyedU3Ec__Iterator0_Reset_m3680980957 (U3CDisablePanelDeleyedU3Ec__Iterator0_t876871905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
