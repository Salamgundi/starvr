﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_Door
struct VRTK_Door_t1992640360;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_ControlValueRa2976216666.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_Direction3775008092.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"

// System.Void VRTK.VRTK_Door::.ctor()
extern "C"  void VRTK_Door__ctor_m1718053784 (VRTK_Door_t1992640360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Door::OnDrawGizmos()
extern "C"  void VRTK_Door_OnDrawGizmos_m3890526942 (VRTK_Door_t1992640360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Door::InitRequiredComponents()
extern "C"  void VRTK_Door_InitRequiredComponents_m3494284655 (VRTK_Door_t1992640360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Door::DetectSetup()
extern "C"  bool VRTK_Door_DetectSetup_m3538786022 (VRTK_Door_t1992640360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_Control/ControlValueRange VRTK.VRTK_Door::RegisterValueRange()
extern "C"  ControlValueRange_t2976216666  VRTK_Door_RegisterValueRange_m1487071294 (VRTK_Door_t1992640360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Door::HandleUpdate()
extern "C"  void VRTK_Door_HandleUpdate_m4290909427 (VRTK_Door_t1992640360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_Door::GetDirectionFromJoint()
extern "C"  float VRTK_Door_GetDirectionFromJoint_m2492525569 (VRTK_Door_t1992640360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_Door::Direction2Axis(VRTK.VRTK_Control/Direction)
extern "C"  Vector3_t2243707580  VRTK_Door_Direction2Axis_m2565335374 (VRTK_Door_t1992640360 * __this, int32_t ___givenDirection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_Control/Direction VRTK.VRTK_Door::DetectDirection()
extern "C"  int32_t VRTK_Door_DetectDirection_m2779723973 (VRTK_Door_t1992640360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Door::InitFrame()
extern "C"  void VRTK_Door_InitFrame_m1833801199 (VRTK_Door_t1992640360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Door::InitDoor()
extern "C"  void VRTK_Door_InitDoor_m3573004716 (VRTK_Door_t1992640360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Door::InitHandle()
extern "C"  void VRTK_Door_InitHandle_m569462662 (VRTK_Door_t1992640360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Door::CreateInteractableObject(UnityEngine.GameObject)
extern "C"  void VRTK_Door_CreateInteractableObject_m1985022893 (VRTK_Door_t1992640360 * __this, GameObject_t1756533147 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Door::InteractableObjectGrabbed(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_Door_InteractableObjectGrabbed_m2374038841 (VRTK_Door_t1992640360 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Door::InteractableObjectUngrabbed(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_Door_InteractableObjectUngrabbed_m2453843766 (VRTK_Door_t1992640360 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_Door::CalculateValue()
extern "C"  float VRTK_Door_CalculateValue_m3223631185 (VRTK_Door_t1992640360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_Door::GetDoor()
extern "C"  GameObject_t1756533147 * VRTK_Door_GetDoor_m524156735 (VRTK_Door_t1992640360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
