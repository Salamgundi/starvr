﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_SimController
struct SDK_SimController_t33406942;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_SimController::.ctor()
extern "C"  void SDK_SimController__ctor_m196618970 (SDK_SimController_t33406942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
