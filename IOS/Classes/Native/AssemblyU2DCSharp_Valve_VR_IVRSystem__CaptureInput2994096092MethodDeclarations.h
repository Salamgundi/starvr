﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_CaptureInputFocus
struct _CaptureInputFocus_t2994096092;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_CaptureInputFocus::.ctor(System.Object,System.IntPtr)
extern "C"  void _CaptureInputFocus__ctor_m951123125 (_CaptureInputFocus_t2994096092 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_CaptureInputFocus::Invoke()
extern "C"  bool _CaptureInputFocus_Invoke_m498423369 (_CaptureInputFocus_t2994096092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_CaptureInputFocus::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _CaptureInputFocus_BeginInvoke_m3273249746 (_CaptureInputFocus_t2994096092 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_CaptureInputFocus::EndInvoke(System.IAsyncResult)
extern "C"  bool _CaptureInputFocus_EndInvoke_m2449820687 (_CaptureInputFocus_t2994096092 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
