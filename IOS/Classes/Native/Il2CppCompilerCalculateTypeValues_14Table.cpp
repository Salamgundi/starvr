﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965.h"
#include "UnityEngine_UnityEngine_ParticleSystemStopBehavior3921148531.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"
#include "UnityEngine_UnityEngine_ParticleSystem_Burst208217445.h"
#include "UnityEngine_UnityEngine_ParticleSystem_MainModule6751348.h"
#include "UnityEngine_UnityEngine_ParticleSystem_EmissionMod2748003162.h"
#include "UnityEngine_UnityEngine_ParticleSystem_Particle250075699.h"
#include "UnityEngine_UnityEngine_ParticleSystem_IteratorDel2419492168.h"
#include "UnityEngine_UnityEngine_ParticleSystem_U3CStopU3Ec1810852071.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints251614631.h"
#include "UnityEngine_UnityEngine_ForceMode1856518252.h"
#include "UnityEngine_UnityEngine_SoftJointLimit3286660309.h"
#include "UnityEngine_UnityEngine_JointDrive1422794032.h"
#include "UnityEngine_UnityEngine_RigidbodyInterpolation993299413.h"
#include "UnityEngine_UnityEngine_JointSpring1540921605.h"
#include "UnityEngine_UnityEngine_JointLimits4282861422.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction478029726.h"
#include "UnityEngine_UnityEngine_Physics634932869.h"
#include "UnityEngine_UnityEngine_ContactPoint1376425630.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_Joint454317436.h"
#include "UnityEngine_UnityEngine_HingeJoint2745110831.h"
#include "UnityEngine_UnityEngine_SpringJoint4147555327.h"
#include "UnityEngine_UnityEngine_FixedJoint3848069458.h"
#include "UnityEngine_UnityEngine_ConfigurableJointMotion1185819153.h"
#include "UnityEngine_UnityEngine_ConfigurableJoint454307495.h"
#include "UnityEngine_UnityEngine_ConstantForce3796310167.h"
#include "UnityEngine_UnityEngine_CollisionDetectionMode841589752.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_BoxCollider22920061.h"
#include "UnityEngine_UnityEngine_SphereCollider1662511355.h"
#include "UnityEngine_UnityEngine_MeshCollider2718867283.h"
#include "UnityEngine_UnityEngine_CapsuleCollider720607407.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_PhysicMaterial578636151.h"
#include "UnityEngine_UnityEngine_CharacterController4094781467.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "UnityEngine_UnityEngine_Physics2D2540166467.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3659330976.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "UnityEngine_UnityEngine_AI_NavMeshHit3805955059.h"
#include "UnityEngine_UnityEngine_AI_NavMesh1481227028.h"
#include "UnityEngine_UnityEngine_AI_NavMeshPath1197654427.h"
#include "UnityEngine_UnityEngine_AudioSpeakerMode3732987812.h"
#include "UnityEngine_UnityEngine_AudioConfiguration2879378008.h"
#include "UnityEngine_UnityEngine_AudioSettings3144015719.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu3743753033.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbac3007145346.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCal421863554.h"
#include "UnityEngine_UnityEngine_AudioListener1996719162.h"
#include "UnityEngine_UnityEngine_FFTWindow2870052902.h"
#include "UnityEngine_UnityEngine_AudioRolloffMode2229549515.h"
#include "UnityEngine_UnityEngine_AudioSourceCurveType4181799310.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_Audio_AudioMixer3244290001.h"
#include "UnityEngine_UnityEngine_Audio_AudioMixerGroup959546644.h"
#include "UnityEngine_UnityEngine_WrapMode255797857.h"
#include "UnityEngine_UnityEngine_AnimationEventSource3560017945.h"
#include "UnityEngine_UnityEngine_AnimationEvent2428323300.h"
#include "UnityEngine_UnityEngine_AnimationClip3510324950.h"
#include "UnityEngine_UnityEngine_PlayMode1184682879.h"
#include "UnityEngine_UnityEngine_Animation2068071072.h"
#include "UnityEngine_UnityEngine_Animation_Enumerator59479314.h"
#include "UnityEngine_UnityEngine_AnimationState1303741697.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3905751349.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2410896200.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_SkeletonBone345082847.h"
#include "UnityEngine_UnityEngine_HumanLimit250797648.h"
#include "UnityEngine_UnityEngine_HumanBone1529896151.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim4078305555.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1693994278.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Cust3423099547.h"
#include "UnityEngine_UnityEngine_Terrain59182933.h"
#include "UnityEngine_UnityEngine_TextGenerationError780770201.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings2543476768.h"
#include "UnityEngine_UnityEngine_TextGenerator647235000.h"
#include "UnityEngine_UnityEngine_TextAlignment1418134952.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode2027154177.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode3668245347.h"
#include "UnityEngine_UnityEngine_GUIText2411476300.h"
#include "UnityEngine_UnityEngine_TextMesh1641806576.h"
#include "UnityEngine_UnityEngine_Font4239498691.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCal1272078033.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "UnityEngine_UnityEngine_RectTransformUtility2941082270.h"
#include "UnityEngine_UnityEngine_RenderMode4280533217.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases3522132132.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1400 = { sizeof (SceneManager_t90660965), -1, sizeof(SceneManager_t90660965_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1400[3] = 
{
	SceneManager_t90660965_StaticFields::get_offset_of_sceneLoaded_0(),
	SceneManager_t90660965_StaticFields::get_offset_of_sceneUnloaded_1(),
	SceneManager_t90660965_StaticFields::get_offset_of_activeSceneChanged_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1401 = { sizeof (ParticleSystemStopBehavior_t3921148531)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1401[3] = 
{
	ParticleSystemStopBehavior_t3921148531::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1402 = { sizeof (ParticleSystem_t3394631041), -1, sizeof(ParticleSystem_t3394631041_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1402[4] = 
{
	ParticleSystem_t3394631041_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	ParticleSystem_t3394631041_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
	ParticleSystem_t3394631041_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
	ParticleSystem_t3394631041_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1403 = { sizeof (Burst_t208217445)+ sizeof (Il2CppObject), sizeof(Burst_t208217445 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1403[3] = 
{
	Burst_t208217445::get_offset_of_m_Time_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Burst_t208217445::get_offset_of_m_MinCount_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Burst_t208217445::get_offset_of_m_MaxCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1404 = { sizeof (MainModule_t6751348)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1404[1] = 
{
	MainModule_t6751348::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1405 = { sizeof (EmissionModule_t2748003162)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1405[1] = 
{
	EmissionModule_t2748003162::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1406 = { sizeof (Particle_t250075699)+ sizeof (Il2CppObject), sizeof(Particle_t250075699 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1406[14] = 
{
	Particle_t250075699::get_offset_of_m_Position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t250075699::get_offset_of_m_Velocity_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t250075699::get_offset_of_m_AnimatedVelocity_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t250075699::get_offset_of_m_InitialVelocity_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t250075699::get_offset_of_m_AxisOfRotation_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t250075699::get_offset_of_m_Rotation_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t250075699::get_offset_of_m_AngularVelocity_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t250075699::get_offset_of_m_StartSize_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t250075699::get_offset_of_m_StartColor_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t250075699::get_offset_of_m_RandomSeed_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t250075699::get_offset_of_m_Lifetime_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t250075699::get_offset_of_m_StartLifetime_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t250075699::get_offset_of_m_EmitAccumulator0_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t250075699::get_offset_of_m_EmitAccumulator1_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1407 = { sizeof (IteratorDelegate_t2419492168), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1408 = { sizeof (U3CStopU3Ec__AnonStorey1_t1810852071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1408[1] = 
{
	U3CStopU3Ec__AnonStorey1_t1810852071::get_offset_of_stopBehavior_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1409 = { sizeof (RigidbodyConstraints_t251614631)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1409[11] = 
{
	RigidbodyConstraints_t251614631::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1410 = { sizeof (ForceMode_t1856518252)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1410[5] = 
{
	ForceMode_t1856518252::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1411 = { sizeof (SoftJointLimit_t3286660309)+ sizeof (Il2CppObject), sizeof(SoftJointLimit_t3286660309 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1411[3] = 
{
	SoftJointLimit_t3286660309::get_offset_of_m_Limit_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SoftJointLimit_t3286660309::get_offset_of_m_Bounciness_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SoftJointLimit_t3286660309::get_offset_of_m_ContactDistance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1412 = { sizeof (JointDrive_t1422794032)+ sizeof (Il2CppObject), sizeof(JointDrive_t1422794032 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1412[3] = 
{
	JointDrive_t1422794032::get_offset_of_m_PositionSpring_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JointDrive_t1422794032::get_offset_of_m_PositionDamper_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JointDrive_t1422794032::get_offset_of_m_MaximumForce_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1413 = { sizeof (RigidbodyInterpolation_t993299413)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1413[4] = 
{
	RigidbodyInterpolation_t993299413::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1414 = { sizeof (JointSpring_t1540921605)+ sizeof (Il2CppObject), sizeof(JointSpring_t1540921605 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1414[3] = 
{
	JointSpring_t1540921605::get_offset_of_spring_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JointSpring_t1540921605::get_offset_of_damper_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JointSpring_t1540921605::get_offset_of_targetPosition_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1415 = { sizeof (JointLimits_t4282861422)+ sizeof (Il2CppObject), sizeof(JointLimits_t4282861422 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1415[7] = 
{
	JointLimits_t4282861422::get_offset_of_m_Min_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JointLimits_t4282861422::get_offset_of_m_Max_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JointLimits_t4282861422::get_offset_of_m_Bounciness_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JointLimits_t4282861422::get_offset_of_m_BounceMinVelocity_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JointLimits_t4282861422::get_offset_of_m_ContactDistance_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JointLimits_t4282861422::get_offset_of_minBounce_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JointLimits_t4282861422::get_offset_of_maxBounce_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1416 = { sizeof (ControllerColliderHit_t4070855101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1416[7] = 
{
	ControllerColliderHit_t4070855101::get_offset_of_m_Controller_0(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Collider_1(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Point_2(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Normal_3(),
	ControllerColliderHit_t4070855101::get_offset_of_m_MoveDirection_4(),
	ControllerColliderHit_t4070855101::get_offset_of_m_MoveLength_5(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Push_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1417 = { sizeof (Collision_t2876846408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1417[5] = 
{
	Collision_t2876846408::get_offset_of_m_Impulse_0(),
	Collision_t2876846408::get_offset_of_m_RelativeVelocity_1(),
	Collision_t2876846408::get_offset_of_m_Rigidbody_2(),
	Collision_t2876846408::get_offset_of_m_Collider_3(),
	Collision_t2876846408::get_offset_of_m_Contacts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1418 = { sizeof (QueryTriggerInteraction_t478029726)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1418[4] = 
{
	QueryTriggerInteraction_t478029726::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1419 = { sizeof (Physics_t634932869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1420 = { sizeof (ContactPoint_t1376425630)+ sizeof (Il2CppObject), sizeof(ContactPoint_t1376425630 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1420[5] = 
{
	ContactPoint_t1376425630::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_ThisColliderInstanceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_OtherColliderInstanceID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_Separation_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1421 = { sizeof (Rigidbody_t4233889191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1422 = { sizeof (Joint_t454317436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1423 = { sizeof (HingeJoint_t2745110831), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1424 = { sizeof (SpringJoint_t4147555327), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1425 = { sizeof (FixedJoint_t3848069458), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1426 = { sizeof (ConfigurableJointMotion_t1185819153)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1426[4] = 
{
	ConfigurableJointMotion_t1185819153::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1427 = { sizeof (ConfigurableJoint_t454307495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1428 = { sizeof (ConstantForce_t3796310167), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1429 = { sizeof (CollisionDetectionMode_t841589752)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1429[4] = 
{
	CollisionDetectionMode_t841589752::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1430 = { sizeof (Collider_t3497673348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1431 = { sizeof (BoxCollider_t22920061), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1432 = { sizeof (SphereCollider_t1662511355), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1433 = { sizeof (MeshCollider_t2718867283), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1434 = { sizeof (CapsuleCollider_t720607407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1435 = { sizeof (RaycastHit_t87180320)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1435[6] = 
{
	RaycastHit_t87180320::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_FaceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_UV_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1436 = { sizeof (PhysicMaterial_t578636151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1437 = { sizeof (CharacterController_t4094781467), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1438 = { sizeof (RaycastHit2D_t4063908774)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1438[6] = 
{
	RaycastHit2D_t4063908774::get_offset_of_m_Centroid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Point_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Normal_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Fraction_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1439 = { sizeof (Physics2D_t2540166467), -1, sizeof(Physics2D_t2540166467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1439[1] = 
{
	Physics2D_t2540166467_StaticFields::get_offset_of_m_LastDisabledRigidbody2D_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1440 = { sizeof (Rigidbody2D_t502193897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1441 = { sizeof (Collider2D_t646061738), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1442 = { sizeof (ContactPoint2D_t3659330976)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1442[4] = 
{
	ContactPoint2D_t3659330976::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_Collider_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_OtherCollider_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1443 = { sizeof (Collision2D_t1539500754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1443[5] = 
{
	Collision2D_t1539500754::get_offset_of_m_Rigidbody_0(),
	Collision2D_t1539500754::get_offset_of_m_Collider_1(),
	Collision2D_t1539500754::get_offset_of_m_Contacts_2(),
	Collision2D_t1539500754::get_offset_of_m_RelativeVelocity_3(),
	Collision2D_t1539500754::get_offset_of_m_Enabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1444 = { sizeof (NavMeshHit_t3805955059)+ sizeof (Il2CppObject), sizeof(NavMeshHit_t3805955059 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1444[5] = 
{
	NavMeshHit_t3805955059::get_offset_of_m_Position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NavMeshHit_t3805955059::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NavMeshHit_t3805955059::get_offset_of_m_Distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NavMeshHit_t3805955059::get_offset_of_m_Mask_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NavMeshHit_t3805955059::get_offset_of_m_Hit_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1445 = { sizeof (NavMesh_t1481227028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1446 = { sizeof (NavMeshPath_t1197654427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1446[2] = 
{
	NavMeshPath_t1197654427::get_offset_of_m_Ptr_0(),
	NavMeshPath_t1197654427::get_offset_of_m_corners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1447 = { sizeof (AudioSpeakerMode_t3732987812)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1447[9] = 
{
	AudioSpeakerMode_t3732987812::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1448 = { sizeof (AudioConfiguration_t2879378008)+ sizeof (Il2CppObject), sizeof(AudioConfiguration_t2879378008 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1448[5] = 
{
	AudioConfiguration_t2879378008::get_offset_of_speakerMode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AudioConfiguration_t2879378008::get_offset_of_dspBufferSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AudioConfiguration_t2879378008::get_offset_of_sampleRate_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AudioConfiguration_t2879378008::get_offset_of_numRealVoices_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AudioConfiguration_t2879378008::get_offset_of_numVirtualVoices_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1449 = { sizeof (AudioSettings_t3144015719), -1, sizeof(AudioSettings_t3144015719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1449[1] = 
{
	AudioSettings_t3144015719_StaticFields::get_offset_of_OnAudioConfigurationChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1450 = { sizeof (AudioConfigurationChangeHandler_t3743753033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1451 = { sizeof (AudioClip_t1932558630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1451[2] = 
{
	AudioClip_t1932558630::get_offset_of_m_PCMReaderCallback_2(),
	AudioClip_t1932558630::get_offset_of_m_PCMSetPositionCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1452 = { sizeof (PCMReaderCallback_t3007145346), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1453 = { sizeof (PCMSetPositionCallback_t421863554), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1454 = { sizeof (AudioListener_t1996719162), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1455 = { sizeof (FFTWindow_t2870052902)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1455[7] = 
{
	FFTWindow_t2870052902::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1456 = { sizeof (AudioRolloffMode_t2229549515)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1456[4] = 
{
	AudioRolloffMode_t2229549515::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1457 = { sizeof (AudioSourceCurveType_t4181799310)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1457[5] = 
{
	AudioSourceCurveType_t4181799310::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1458 = { sizeof (AudioSource_t1135106623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1459 = { sizeof (AudioMixer_t3244290001), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1460 = { sizeof (AudioMixerGroup_t959546644), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1461 = { sizeof (WrapMode_t255797857)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1461[7] = 
{
	WrapMode_t255797857::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1462 = { sizeof (AnimationEventSource_t3560017945)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1462[4] = 
{
	AnimationEventSource_t3560017945::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1463 = { sizeof (AnimationEvent_t2428323300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1463[11] = 
{
	AnimationEvent_t2428323300::get_offset_of_m_Time_0(),
	AnimationEvent_t2428323300::get_offset_of_m_FunctionName_1(),
	AnimationEvent_t2428323300::get_offset_of_m_StringParameter_2(),
	AnimationEvent_t2428323300::get_offset_of_m_ObjectReferenceParameter_3(),
	AnimationEvent_t2428323300::get_offset_of_m_FloatParameter_4(),
	AnimationEvent_t2428323300::get_offset_of_m_IntParameter_5(),
	AnimationEvent_t2428323300::get_offset_of_m_MessageOptions_6(),
	AnimationEvent_t2428323300::get_offset_of_m_Source_7(),
	AnimationEvent_t2428323300::get_offset_of_m_StateSender_8(),
	AnimationEvent_t2428323300::get_offset_of_m_AnimatorStateInfo_9(),
	AnimationEvent_t2428323300::get_offset_of_m_AnimatorClipInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1464 = { sizeof (AnimationClip_t3510324950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1465 = { sizeof (PlayMode_t1184682879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1465[3] = 
{
	PlayMode_t1184682879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1466 = { sizeof (Animation_t2068071072), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1467 = { sizeof (Enumerator_t59479314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1467[2] = 
{
	Enumerator_t59479314::get_offset_of_m_Outer_0(),
	Enumerator_t59479314::get_offset_of_m_CurrentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1468 = { sizeof (AnimationState_t1303741697), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1469 = { sizeof (AnimatorClipInfo_t3905751349)+ sizeof (Il2CppObject), sizeof(AnimatorClipInfo_t3905751349 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1469[2] = 
{
	AnimatorClipInfo_t3905751349::get_offset_of_m_ClipInstanceID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorClipInfo_t3905751349::get_offset_of_m_Weight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1470 = { sizeof (AnimatorStateInfo_t2577870592)+ sizeof (Il2CppObject), sizeof(AnimatorStateInfo_t2577870592 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1470[9] = 
{
	AnimatorStateInfo_t2577870592::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Path_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_FullPath_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Length_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Speed_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_SpeedMultiplier_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Tag_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Loop_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1471 = { sizeof (AnimatorTransitionInfo_t2410896200)+ sizeof (Il2CppObject), sizeof(AnimatorTransitionInfo_t2410896200_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1471[6] = 
{
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_FullPath_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_UserName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_Name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_AnyState_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_TransitionType_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1472 = { sizeof (Animator_t69676727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1473 = { sizeof (SkeletonBone_t345082847)+ sizeof (Il2CppObject), sizeof(SkeletonBone_t345082847_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1473[5] = 
{
	SkeletonBone_t345082847::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_parentName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_position_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_rotation_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_scale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1474 = { sizeof (HumanLimit_t250797648)+ sizeof (Il2CppObject), sizeof(HumanLimit_t250797648 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1474[5] = 
{
	HumanLimit_t250797648::get_offset_of_m_Min_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_Max_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_Center_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_AxisLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_UseDefaultValues_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1475 = { sizeof (HumanBone_t1529896151)+ sizeof (Il2CppObject), sizeof(HumanBone_t1529896151_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1475[3] = 
{
	HumanBone_t1529896151::get_offset_of_m_BoneName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t1529896151::get_offset_of_m_HumanName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t1529896151::get_offset_of_limit_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1476 = { sizeof (RuntimeAnimatorController_t670468573), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1477 = { sizeof (AnimatorControllerPlayable_t4078305555)+ sizeof (Il2CppObject), sizeof(AnimatorControllerPlayable_t4078305555 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1477[1] = 
{
	AnimatorControllerPlayable_t4078305555::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1478 = { sizeof (AnimationPlayable_t1693994278)+ sizeof (Il2CppObject), sizeof(AnimationPlayable_t1693994278 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1478[1] = 
{
	AnimationPlayable_t1693994278::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1479 = { sizeof (CustomAnimationPlayable_t3423099547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1479[1] = 
{
	CustomAnimationPlayable_t3423099547::get_offset_of_handle_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1480 = { sizeof (Terrain_t59182933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1481 = { sizeof (TextGenerationError_t780770201)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1481[5] = 
{
	TextGenerationError_t780770201::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1482 = { sizeof (TextGenerationSettings_t2543476768)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1482[18] = 
{
	TextGenerationSettings_t2543476768::get_offset_of_font_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_color_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_lineSpacing_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_richText_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_scaleFactor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_fontStyle_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_textAnchor_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_alignByGeometry_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextForBestFit_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextMinSize_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextMaxSize_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_updateBounds_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_verticalOverflow_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_horizontalOverflow_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_generationExtents_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_pivot_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_generateOutOfBounds_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1483 = { sizeof (TextGenerator_t647235000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1483[11] = 
{
	TextGenerator_t647235000::get_offset_of_m_Ptr_0(),
	TextGenerator_t647235000::get_offset_of_m_LastString_1(),
	TextGenerator_t647235000::get_offset_of_m_LastSettings_2(),
	TextGenerator_t647235000::get_offset_of_m_HasGenerated_3(),
	TextGenerator_t647235000::get_offset_of_m_LastValid_4(),
	TextGenerator_t647235000::get_offset_of_m_Verts_5(),
	TextGenerator_t647235000::get_offset_of_m_Characters_6(),
	TextGenerator_t647235000::get_offset_of_m_Lines_7(),
	TextGenerator_t647235000::get_offset_of_m_CachedVerts_8(),
	TextGenerator_t647235000::get_offset_of_m_CachedCharacters_9(),
	TextGenerator_t647235000::get_offset_of_m_CachedLines_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1484 = { sizeof (TextAlignment_t1418134952)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1484[4] = 
{
	TextAlignment_t1418134952::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1485 = { sizeof (TextAnchor_t112990806)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1485[10] = 
{
	TextAnchor_t112990806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1486 = { sizeof (HorizontalWrapMode_t2027154177)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1486[3] = 
{
	HorizontalWrapMode_t2027154177::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1487 = { sizeof (VerticalWrapMode_t3668245347)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1487[3] = 
{
	VerticalWrapMode_t3668245347::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1488 = { sizeof (GUIText_t2411476300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1489 = { sizeof (TextMesh_t1641806576), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1490 = { sizeof (Font_t4239498691), -1, sizeof(Font_t4239498691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1490[2] = 
{
	Font_t4239498691_StaticFields::get_offset_of_textureRebuilt_2(),
	Font_t4239498691::get_offset_of_m_FontTextureRebuildCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1491 = { sizeof (FontTextureRebuildCallback_t1272078033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1492 = { sizeof (UICharInfo_t3056636800)+ sizeof (Il2CppObject), sizeof(UICharInfo_t3056636800 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1492[2] = 
{
	UICharInfo_t3056636800::get_offset_of_cursorPos_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UICharInfo_t3056636800::get_offset_of_charWidth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1493 = { sizeof (UILineInfo_t3621277874)+ sizeof (Il2CppObject), sizeof(UILineInfo_t3621277874 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1493[3] = 
{
	UILineInfo_t3621277874::get_offset_of_startCharIdx_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t3621277874::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t3621277874::get_offset_of_topY_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1494 = { sizeof (UIVertex_t1204258818)+ sizeof (Il2CppObject), sizeof(UIVertex_t1204258818 ), sizeof(UIVertex_t1204258818_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1494[9] = 
{
	UIVertex_t1204258818::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_color_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_uv0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_uv1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_tangent_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818_StaticFields::get_offset_of_s_DefaultColor_6(),
	UIVertex_t1204258818_StaticFields::get_offset_of_s_DefaultTangent_7(),
	UIVertex_t1204258818_StaticFields::get_offset_of_simpleVert_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1495 = { sizeof (RectTransformUtility_t2941082270), -1, sizeof(RectTransformUtility_t2941082270_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1495[1] = 
{
	RectTransformUtility_t2941082270_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1496 = { sizeof (RenderMode_t4280533217)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1496[4] = 
{
	RenderMode_t4280533217::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1497 = { sizeof (Canvas_t209405766), -1, sizeof(Canvas_t209405766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1497[1] = 
{
	Canvas_t209405766_StaticFields::get_offset_of_willRenderCanvases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1498 = { sizeof (WillRenderCanvases_t3522132132), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1499 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
