﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>
struct Dictionary_2_t1012981408;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.LogType>
struct IEqualityComparer_1_t772365640;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>[]
struct KeyValuePair_2U5BU5D_t625215299;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>
struct IEnumerator_1_t540817753;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>
struct ValueCollection_t4011008547;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23065293926.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2333006110.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::.ctor()
extern "C"  void Dictionary_2__ctor_m3882333693_gshared (Dictionary_2_t1012981408 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3882333693(__this, method) ((  void (*) (Dictionary_2_t1012981408 *, const MethodInfo*))Dictionary_2__ctor_m3882333693_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1251780437_gshared (Dictionary_2_t1012981408 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1251780437(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1012981408 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1251780437_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2862596999_gshared (Dictionary_2_t1012981408 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2862596999(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1012981408 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2862596999_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m906295697_gshared (Dictionary_2_t1012981408 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m906295697(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1012981408 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m906295697_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1598311248_gshared (Dictionary_2_t1012981408 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1598311248(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1012981408 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1598311248_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1687348269_gshared (Dictionary_2_t1012981408 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1687348269(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1012981408 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1687348269_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1186123866_gshared (Dictionary_2_t1012981408 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1186123866(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1012981408 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1186123866_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2296366833_gshared (Dictionary_2_t1012981408 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2296366833(__this, ___key0, method) ((  void (*) (Dictionary_2_t1012981408 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2296366833_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3062534608_gshared (Dictionary_2_t1012981408 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3062534608(__this, method) ((  bool (*) (Dictionary_2_t1012981408 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3062534608_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1701972388_gshared (Dictionary_2_t1012981408 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1701972388(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1012981408 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1701972388_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18312934_gshared (Dictionary_2_t1012981408 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18312934(__this, method) ((  bool (*) (Dictionary_2_t1012981408 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18312934_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2506776393_gshared (Dictionary_2_t1012981408 * __this, KeyValuePair_2_t3065293926  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2506776393(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1012981408 *, KeyValuePair_2_t3065293926 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2506776393_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2856982979_gshared (Dictionary_2_t1012981408 * __this, KeyValuePair_2_t3065293926  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2856982979(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1012981408 *, KeyValuePair_2_t3065293926 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2856982979_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2075858533_gshared (Dictionary_2_t1012981408 * __this, KeyValuePair_2U5BU5D_t625215299* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2075858533(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1012981408 *, KeyValuePair_2U5BU5D_t625215299*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2075858533_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1436641028_gshared (Dictionary_2_t1012981408 * __this, KeyValuePair_2_t3065293926  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1436641028(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1012981408 *, KeyValuePair_2_t3065293926 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1436641028_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3707886164_gshared (Dictionary_2_t1012981408 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3707886164(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1012981408 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3707886164_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3237190243_gshared (Dictionary_2_t1012981408 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3237190243(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1012981408 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3237190243_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4224479678_gshared (Dictionary_2_t1012981408 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4224479678(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1012981408 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4224479678_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2693026505_gshared (Dictionary_2_t1012981408 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2693026505(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1012981408 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2693026505_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3358384664_gshared (Dictionary_2_t1012981408 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3358384664(__this, method) ((  int32_t (*) (Dictionary_2_t1012981408 *, const MethodInfo*))Dictionary_2_get_Count_m3358384664_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::get_Item(TKey)
extern "C"  Color_t2020392075  Dictionary_2_get_Item_m3187400063_gshared (Dictionary_2_t1012981408 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3187400063(__this, ___key0, method) ((  Color_t2020392075  (*) (Dictionary_2_t1012981408 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m3187400063_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3505079926_gshared (Dictionary_2_t1012981408 * __this, int32_t ___key0, Color_t2020392075  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3505079926(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1012981408 *, int32_t, Color_t2020392075 , const MethodInfo*))Dictionary_2_set_Item_m3505079926_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2967658912_gshared (Dictionary_2_t1012981408 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2967658912(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1012981408 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2967658912_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m1631665819_gshared (Dictionary_2_t1012981408 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1631665819(__this, ___size0, method) ((  void (*) (Dictionary_2_t1012981408 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1631665819_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3391943673_gshared (Dictionary_2_t1012981408 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3391943673(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1012981408 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3391943673_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3065293926  Dictionary_2_make_pair_m1234748479_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Color_t2020392075  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1234748479(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3065293926  (*) (Il2CppObject * /* static, unused */, int32_t, Color_t2020392075 , const MethodInfo*))Dictionary_2_make_pair_m1234748479_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::pick_value(TKey,TValue)
extern "C"  Color_t2020392075  Dictionary_2_pick_value_m2082643271_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Color_t2020392075  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2082643271(__this /* static, unused */, ___key0, ___value1, method) ((  Color_t2020392075  (*) (Il2CppObject * /* static, unused */, int32_t, Color_t2020392075 , const MethodInfo*))Dictionary_2_pick_value_m2082643271_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1635127498_gshared (Dictionary_2_t1012981408 * __this, KeyValuePair_2U5BU5D_t625215299* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1635127498(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1012981408 *, KeyValuePair_2U5BU5D_t625215299*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1635127498_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Resize()
extern "C"  void Dictionary_2_Resize_m1871961706_gshared (Dictionary_2_t1012981408 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1871961706(__this, method) ((  void (*) (Dictionary_2_t1012981408 *, const MethodInfo*))Dictionary_2_Resize_m1871961706_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3465620213_gshared (Dictionary_2_t1012981408 * __this, int32_t ___key0, Color_t2020392075  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3465620213(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1012981408 *, int32_t, Color_t2020392075 , const MethodInfo*))Dictionary_2_Add_m3465620213_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Clear()
extern "C"  void Dictionary_2_Clear_m2260083691_gshared (Dictionary_2_t1012981408 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2260083691(__this, method) ((  void (*) (Dictionary_2_t1012981408 *, const MethodInfo*))Dictionary_2_Clear_m2260083691_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2446354729_gshared (Dictionary_2_t1012981408 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2446354729(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1012981408 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2446354729_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2413253345_gshared (Dictionary_2_t1012981408 * __this, Color_t2020392075  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2413253345(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1012981408 *, Color_t2020392075 , const MethodInfo*))Dictionary_2_ContainsValue_m2413253345_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2679644264_gshared (Dictionary_2_t1012981408 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2679644264(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1012981408 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2679644264_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m418842190_gshared (Dictionary_2_t1012981408 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m418842190(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1012981408 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m418842190_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3794071331_gshared (Dictionary_2_t1012981408 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3794071331(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1012981408 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m3794071331_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3515937250_gshared (Dictionary_2_t1012981408 * __this, int32_t ___key0, Color_t2020392075 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3515937250(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1012981408 *, int32_t, Color_t2020392075 *, const MethodInfo*))Dictionary_2_TryGetValue_m3515937250_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::get_Values()
extern "C"  ValueCollection_t4011008547 * Dictionary_2_get_Values_m241469597_gshared (Dictionary_2_t1012981408 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m241469597(__this, method) ((  ValueCollection_t4011008547 * (*) (Dictionary_2_t1012981408 *, const MethodInfo*))Dictionary_2_get_Values_m241469597_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m3774083260_gshared (Dictionary_2_t1012981408 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3774083260(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1012981408 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3774083260_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::ToTValue(System.Object)
extern "C"  Color_t2020392075  Dictionary_2_ToTValue_m3981749004_gshared (Dictionary_2_t1012981408 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3981749004(__this, ___value0, method) ((  Color_t2020392075  (*) (Dictionary_2_t1012981408 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3981749004_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m3078717266_gshared (Dictionary_2_t1012981408 * __this, KeyValuePair_2_t3065293926  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m3078717266(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1012981408 *, KeyValuePair_2_t3065293926 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3078717266_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::GetEnumerator()
extern "C"  Enumerator_t2333006110  Dictionary_2_GetEnumerator_m2877039339_gshared (Dictionary_2_t1012981408 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2877039339(__this, method) ((  Enumerator_t2333006110  (*) (Dictionary_2_t1012981408 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2877039339_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m1037239940_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Color_t2020392075  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1037239940(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Color_t2020392075 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1037239940_gshared)(__this /* static, unused */, ___key0, ___value1, method)
