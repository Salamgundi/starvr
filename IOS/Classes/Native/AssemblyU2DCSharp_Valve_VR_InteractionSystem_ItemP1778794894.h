﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.ItemPackage
struct ItemPackage_t3423754743;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.ItemPackageReference
struct  ItemPackageReference_t1778794894  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.ItemPackage Valve.VR.InteractionSystem.ItemPackageReference::itemPackage
	ItemPackage_t3423754743 * ___itemPackage_2;

public:
	inline static int32_t get_offset_of_itemPackage_2() { return static_cast<int32_t>(offsetof(ItemPackageReference_t1778794894, ___itemPackage_2)); }
	inline ItemPackage_t3423754743 * get_itemPackage_2() const { return ___itemPackage_2; }
	inline ItemPackage_t3423754743 ** get_address_of_itemPackage_2() { return &___itemPackage_2; }
	inline void set_itemPackage_2(ItemPackage_t3423754743 * value)
	{
		___itemPackage_2 = value;
		Il2CppCodeGenWriteBarrier(&___itemPackage_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
