﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_DeviceFinder
struct VRTK_DeviceFinder_t2083124544;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DeviceFinder_Devices2408891389.h"
#include "AssemblyU2DCSharp_VRTK_SDK_BaseController_Controlle246418309.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DeviceFinder_Headsets2701247739.h"

// System.Void VRTK.VRTK_DeviceFinder::.ctor()
extern "C"  void VRTK_DeviceFinder__ctor_m712245086 (VRTK_DeviceFinder_t2083124544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 VRTK.VRTK_DeviceFinder::GetControllerIndex(UnityEngine.GameObject)
extern "C"  uint32_t VRTK_DeviceFinder_GetControllerIndex_m601770971 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_DeviceFinder::GetControllerByIndex(System.UInt32,System.Boolean)
extern "C"  GameObject_t1756533147 * VRTK_DeviceFinder_GetControllerByIndex_m261285063 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, bool ___getActual1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_DeviceFinder::GetControllerOrigin(UnityEngine.GameObject)
extern "C"  Transform_t3275118058 * VRTK_DeviceFinder_GetControllerOrigin_m1826870348 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_DeviceFinder::DeviceTransform(VRTK.VRTK_DeviceFinder/Devices)
extern "C"  Transform_t3275118058 * VRTK_DeviceFinder_DeviceTransform_m3095571891 (Il2CppObject * __this /* static, unused */, int32_t ___device0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.SDK_BaseController/ControllerHand VRTK.VRTK_DeviceFinder::GetControllerHandType(System.String)
extern "C"  int32_t VRTK_DeviceFinder_GetControllerHandType_m3168623799 (Il2CppObject * __this /* static, unused */, String_t* ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.SDK_BaseController/ControllerHand VRTK.VRTK_DeviceFinder::GetControllerHand(UnityEngine.GameObject)
extern "C"  int32_t VRTK_DeviceFinder_GetControllerHand_m315495173 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_DeviceFinder::GetControllerLeftHand(System.Boolean)
extern "C"  GameObject_t1756533147 * VRTK_DeviceFinder_GetControllerLeftHand_m151411604 (Il2CppObject * __this /* static, unused */, bool ___getActual0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_DeviceFinder::GetControllerRightHand(System.Boolean)
extern "C"  GameObject_t1756533147 * VRTK_DeviceFinder_GetControllerRightHand_m2938812741 (Il2CppObject * __this /* static, unused */, bool ___getActual0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_DeviceFinder::IsControllerOfHand(UnityEngine.GameObject,VRTK.SDK_BaseController/ControllerHand)
extern "C"  bool VRTK_DeviceFinder_IsControllerOfHand_m2697236723 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___checkController0, int32_t ___hand1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_DeviceFinder::IsControllerLeftHand(UnityEngine.GameObject)
extern "C"  bool VRTK_DeviceFinder_IsControllerLeftHand_m2483100704 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___checkController0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_DeviceFinder::IsControllerRightHand(UnityEngine.GameObject)
extern "C"  bool VRTK_DeviceFinder_IsControllerRightHand_m2942768489 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___checkController0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_DeviceFinder::GetActualController(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * VRTK_DeviceFinder_GetActualController_m1755218535 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___givenController0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_DeviceFinder::GetScriptAliasController(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * VRTK_DeviceFinder_GetScriptAliasController_m946420826 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___givenController0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_DeviceFinder::GetModelAliasController(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * VRTK_DeviceFinder_GetModelAliasController_m3353689808 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___givenController0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_DeviceFinder::GetControllerVelocity(UnityEngine.GameObject)
extern "C"  Vector3_t2243707580  VRTK_DeviceFinder_GetControllerVelocity_m3781948331 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___givenController0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_DeviceFinder::GetControllerAngularVelocity(UnityEngine.GameObject)
extern "C"  Vector3_t2243707580  VRTK_DeviceFinder_GetControllerAngularVelocity_m3315851489 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___givenController0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_DeviceFinder::GetHeadsetVelocity()
extern "C"  Vector3_t2243707580  VRTK_DeviceFinder_GetHeadsetVelocity_m4027468929 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_DeviceFinder::GetHeadsetAngularVelocity()
extern "C"  Vector3_t2243707580  VRTK_DeviceFinder_GetHeadsetAngularVelocity_m1271337031 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_DeviceFinder::HeadsetTransform()
extern "C"  Transform_t3275118058 * VRTK_DeviceFinder_HeadsetTransform_m2991712096 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_DeviceFinder::HeadsetCamera()
extern "C"  Transform_t3275118058 * VRTK_DeviceFinder_HeadsetCamera_m2360307863 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_DeviceFinder/Headsets VRTK.VRTK_DeviceFinder::GetHeadsetType(System.Boolean)
extern "C"  int32_t VRTK_DeviceFinder_GetHeadsetType_m652775961 (Il2CppObject * __this /* static, unused */, bool ___summary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_DeviceFinder::PlayAreaTransform()
extern "C"  Transform_t3275118058 * VRTK_DeviceFinder_PlayAreaTransform_m1884263923 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
