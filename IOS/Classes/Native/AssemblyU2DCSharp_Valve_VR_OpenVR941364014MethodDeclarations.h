﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.OpenVR
struct OpenVR_t941364014;
// System.String
struct String_t;
// Valve.VR.OpenVR/COpenVRContext
struct COpenVRContext_t2969167613;
// Valve.VR.CVRSystem
struct CVRSystem_t1953699154;
// Valve.VR.CVRChaperone
struct CVRChaperone_t441701222;
// Valve.VR.CVRChaperoneSetup
struct CVRChaperoneSetup_t1611144107;
// Valve.VR.CVRCompositor
struct CVRCompositor_t197946050;
// Valve.VR.CVROverlay
struct CVROverlay_t3377499315;
// Valve.VR.CVRRenderModels
struct CVRRenderModels_t2019937239;
// Valve.VR.CVRExtendedDisplay
struct CVRExtendedDisplay_t1925229748;
// Valve.VR.CVRSettings
struct CVRSettings_t3592067458;
// Valve.VR.CVRApplications
struct CVRApplications_t1900926488;
// Valve.VR.CVRScreenshots
struct CVRScreenshots_t3241040508;
// Valve.VR.CVRTrackedCamera
struct CVRTrackedCamera_t2050215972;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRInitError1685532365.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationType1952981913.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Valve.VR.OpenVR::.ctor()
extern "C"  void OpenVR__ctor_m1883729407 (OpenVR_t941364014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.OpenVR::InitInternal(Valve.VR.EVRInitError&,Valve.VR.EVRApplicationType)
extern "C"  uint32_t OpenVR_InitInternal_m562845823 (Il2CppObject * __this /* static, unused */, int32_t* ___peError0, int32_t ___eApplicationType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.OpenVR::ShutdownInternal()
extern "C"  void OpenVR_ShutdownInternal_m67096638 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.OpenVR::IsHmdPresent()
extern "C"  bool OpenVR_IsHmdPresent_m459549695 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.OpenVR::IsRuntimeInstalled()
extern "C"  bool OpenVR_IsRuntimeInstalled_m827168557 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.OpenVR::GetStringForHmdError(Valve.VR.EVRInitError)
extern "C"  String_t* OpenVR_GetStringForHmdError_m3942827266 (Il2CppObject * __this /* static, unused */, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.OpenVR::GetGenericInterface(System.String,Valve.VR.EVRInitError&)
extern "C"  IntPtr_t OpenVR_GetGenericInterface_m3241736075 (Il2CppObject * __this /* static, unused */, String_t* ___pchInterfaceVersion0, int32_t* ___peError1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.OpenVR::IsInterfaceVersionValid(System.String)
extern "C"  bool OpenVR_IsInterfaceVersionValid_m1041445328 (Il2CppObject * __this /* static, unused */, String_t* ___pchInterfaceVersion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.OpenVR::GetInitToken()
extern "C"  uint32_t OpenVR_GetInitToken_m4274785731 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.OpenVR::get_VRToken()
extern "C"  uint32_t OpenVR_get_VRToken_m2237977554 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.OpenVR::set_VRToken(System.UInt32)
extern "C"  void OpenVR_set_VRToken_m2365548621 (Il2CppObject * __this /* static, unused */, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.OpenVR/COpenVRContext Valve.VR.OpenVR::get_OpenVRInternal_ModuleContext()
extern "C"  COpenVRContext_t2969167613 * OpenVR_get_OpenVRInternal_ModuleContext_m1187351683 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRSystem Valve.VR.OpenVR::get_System()
extern "C"  CVRSystem_t1953699154 * OpenVR_get_System_m3895122746 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRChaperone Valve.VR.OpenVR::get_Chaperone()
extern "C"  CVRChaperone_t441701222 * OpenVR_get_Chaperone_m1362224178 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRChaperoneSetup Valve.VR.OpenVR::get_ChaperoneSetup()
extern "C"  CVRChaperoneSetup_t1611144107 * OpenVR_get_ChaperoneSetup_m3067921338 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRCompositor Valve.VR.OpenVR::get_Compositor()
extern "C"  CVRCompositor_t197946050 * OpenVR_get_Compositor_m270754106 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVROverlay Valve.VR.OpenVR::get_Overlay()
extern "C"  CVROverlay_t3377499315 * OpenVR_get_Overlay_m1884670880 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRRenderModels Valve.VR.OpenVR::get_RenderModels()
extern "C"  CVRRenderModels_t2019937239 * OpenVR_get_RenderModels_m3805898074 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRExtendedDisplay Valve.VR.OpenVR::get_ExtendedDisplay()
extern "C"  CVRExtendedDisplay_t1925229748 * OpenVR_get_ExtendedDisplay_m3478623498 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRSettings Valve.VR.OpenVR::get_Settings()
extern "C"  CVRSettings_t3592067458 * OpenVR_get_Settings_m451923450 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRApplications Valve.VR.OpenVR::get_Applications()
extern "C"  CVRApplications_t1900926488 * OpenVR_get_Applications_m2695350266 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRScreenshots Valve.VR.OpenVR::get_Screenshots()
extern "C"  CVRScreenshots_t3241040508 * OpenVR_get_Screenshots_m2173412338 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRTrackedCamera Valve.VR.OpenVR::get_TrackedCamera()
extern "C"  CVRTrackedCamera_t2050215972 * OpenVR_get_TrackedCamera_m1041630274 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRSystem Valve.VR.OpenVR::Init(Valve.VR.EVRInitError&,Valve.VR.EVRApplicationType)
extern "C"  CVRSystem_t1953699154 * OpenVR_Init_m1711006846 (Il2CppObject * __this /* static, unused */, int32_t* ___peError0, int32_t ___eApplicationType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.OpenVR::Shutdown()
extern "C"  void OpenVR_Shutdown_m2905253731 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.OpenVR::.cctor()
extern "C"  void OpenVR__cctor_m3463577392 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
