﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,VRTK.SnapDropZoneEventArgs>
struct UnityEvent_2_t3396356679;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.SnapDropZoneEventArgs>
struct UnityAction_2_t1514158761;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_VRTK_SnapDropZoneEventArgs418702774.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.SnapDropZoneEventArgs>::.ctor()
extern "C"  void UnityEvent_2__ctor_m3176146657_gshared (UnityEvent_2_t3396356679 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m3176146657(__this, method) ((  void (*) (UnityEvent_2_t3396356679 *, const MethodInfo*))UnityEvent_2__ctor_m3176146657_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.SnapDropZoneEventArgs>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m997363169_gshared (UnityEvent_2_t3396356679 * __this, UnityAction_2_t1514158761 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m997363169(__this, ___call0, method) ((  void (*) (UnityEvent_2_t3396356679 *, UnityAction_2_t1514158761 *, const MethodInfo*))UnityEvent_2_AddListener_m997363169_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.SnapDropZoneEventArgs>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m291348226_gshared (UnityEvent_2_t3396356679 * __this, UnityAction_2_t1514158761 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m291348226(__this, ___call0, method) ((  void (*) (UnityEvent_2_t3396356679 *, UnityAction_2_t1514158761 *, const MethodInfo*))UnityEvent_2_RemoveListener_m291348226_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,VRTK.SnapDropZoneEventArgs>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m627606857_gshared (UnityEvent_2_t3396356679 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m627606857(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t3396356679 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m627606857_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.SnapDropZoneEventArgs>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m1410550417_gshared (UnityEvent_2_t3396356679 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m1410550417(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t3396356679 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m1410550417_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.SnapDropZoneEventArgs>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m2234861552_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t1514158761 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m2234861552(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t1514158761 *, const MethodInfo*))UnityEvent_2_GetDelegate_m2234861552_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.SnapDropZoneEventArgs>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m33549076_gshared (UnityEvent_2_t3396356679 * __this, Il2CppObject * ___arg00, SnapDropZoneEventArgs_t418702774  ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m33549076(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t3396356679 *, Il2CppObject *, SnapDropZoneEventArgs_t418702774 , const MethodInfo*))UnityEvent_2_Invoke_m33549076_gshared)(__this, ___arg00, ___arg11, method)
