﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_TrackedController/<Enable>c__Iterator0
struct U3CEnableU3Ec__Iterator0_t1493054694;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_TrackedController/<Enable>c__Iterator0::.ctor()
extern "C"  void U3CEnableU3Ec__Iterator0__ctor_m1868869793 (U3CEnableU3Ec__Iterator0_t1493054694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_TrackedController/<Enable>c__Iterator0::MoveNext()
extern "C"  bool U3CEnableU3Ec__Iterator0_MoveNext_m931613375 (U3CEnableU3Ec__Iterator0_t1493054694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_TrackedController/<Enable>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEnableU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2039408495 (U3CEnableU3Ec__Iterator0_t1493054694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_TrackedController/<Enable>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEnableU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m685560423 (U3CEnableU3Ec__Iterator0_t1493054694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController/<Enable>c__Iterator0::Dispose()
extern "C"  void U3CEnableU3Ec__Iterator0_Dispose_m848169744 (U3CEnableU3Ec__Iterator0_t1493054694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController/<Enable>c__Iterator0::Reset()
extern "C"  void U3CEnableU3Ec__Iterator0_Reset_m1074814810 (U3CEnableU3Ec__Iterator0_t1493054694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
