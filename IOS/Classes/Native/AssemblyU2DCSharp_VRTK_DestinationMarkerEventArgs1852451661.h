﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.DestinationMarkerEventArgs
struct  DestinationMarkerEventArgs_t1852451661 
{
public:
	// System.Single VRTK.DestinationMarkerEventArgs::distance
	float ___distance_0;
	// UnityEngine.Transform VRTK.DestinationMarkerEventArgs::target
	Transform_t3275118058 * ___target_1;
	// UnityEngine.RaycastHit VRTK.DestinationMarkerEventArgs::raycastHit
	RaycastHit_t87180320  ___raycastHit_2;
	// UnityEngine.Vector3 VRTK.DestinationMarkerEventArgs::destinationPosition
	Vector3_t2243707580  ___destinationPosition_3;
	// System.Boolean VRTK.DestinationMarkerEventArgs::forceDestinationPosition
	bool ___forceDestinationPosition_4;
	// System.Boolean VRTK.DestinationMarkerEventArgs::enableTeleport
	bool ___enableTeleport_5;
	// System.UInt32 VRTK.DestinationMarkerEventArgs::controllerIndex
	uint32_t ___controllerIndex_6;

public:
	inline static int32_t get_offset_of_distance_0() { return static_cast<int32_t>(offsetof(DestinationMarkerEventArgs_t1852451661, ___distance_0)); }
	inline float get_distance_0() const { return ___distance_0; }
	inline float* get_address_of_distance_0() { return &___distance_0; }
	inline void set_distance_0(float value)
	{
		___distance_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(DestinationMarkerEventArgs_t1852451661, ___target_1)); }
	inline Transform_t3275118058 * get_target_1() const { return ___target_1; }
	inline Transform_t3275118058 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3275118058 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier(&___target_1, value);
	}

	inline static int32_t get_offset_of_raycastHit_2() { return static_cast<int32_t>(offsetof(DestinationMarkerEventArgs_t1852451661, ___raycastHit_2)); }
	inline RaycastHit_t87180320  get_raycastHit_2() const { return ___raycastHit_2; }
	inline RaycastHit_t87180320 * get_address_of_raycastHit_2() { return &___raycastHit_2; }
	inline void set_raycastHit_2(RaycastHit_t87180320  value)
	{
		___raycastHit_2 = value;
	}

	inline static int32_t get_offset_of_destinationPosition_3() { return static_cast<int32_t>(offsetof(DestinationMarkerEventArgs_t1852451661, ___destinationPosition_3)); }
	inline Vector3_t2243707580  get_destinationPosition_3() const { return ___destinationPosition_3; }
	inline Vector3_t2243707580 * get_address_of_destinationPosition_3() { return &___destinationPosition_3; }
	inline void set_destinationPosition_3(Vector3_t2243707580  value)
	{
		___destinationPosition_3 = value;
	}

	inline static int32_t get_offset_of_forceDestinationPosition_4() { return static_cast<int32_t>(offsetof(DestinationMarkerEventArgs_t1852451661, ___forceDestinationPosition_4)); }
	inline bool get_forceDestinationPosition_4() const { return ___forceDestinationPosition_4; }
	inline bool* get_address_of_forceDestinationPosition_4() { return &___forceDestinationPosition_4; }
	inline void set_forceDestinationPosition_4(bool value)
	{
		___forceDestinationPosition_4 = value;
	}

	inline static int32_t get_offset_of_enableTeleport_5() { return static_cast<int32_t>(offsetof(DestinationMarkerEventArgs_t1852451661, ___enableTeleport_5)); }
	inline bool get_enableTeleport_5() const { return ___enableTeleport_5; }
	inline bool* get_address_of_enableTeleport_5() { return &___enableTeleport_5; }
	inline void set_enableTeleport_5(bool value)
	{
		___enableTeleport_5 = value;
	}

	inline static int32_t get_offset_of_controllerIndex_6() { return static_cast<int32_t>(offsetof(DestinationMarkerEventArgs_t1852451661, ___controllerIndex_6)); }
	inline uint32_t get_controllerIndex_6() const { return ___controllerIndex_6; }
	inline uint32_t* get_address_of_controllerIndex_6() { return &___controllerIndex_6; }
	inline void set_controllerIndex_6(uint32_t value)
	{
		___controllerIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VRTK.DestinationMarkerEventArgs
struct DestinationMarkerEventArgs_t1852451661_marshaled_pinvoke
{
	float ___distance_0;
	Transform_t3275118058 * ___target_1;
	RaycastHit_t87180320_marshaled_pinvoke ___raycastHit_2;
	Vector3_t2243707580  ___destinationPosition_3;
	int32_t ___forceDestinationPosition_4;
	int32_t ___enableTeleport_5;
	uint32_t ___controllerIndex_6;
};
// Native definition for COM marshalling of VRTK.DestinationMarkerEventArgs
struct DestinationMarkerEventArgs_t1852451661_marshaled_com
{
	float ___distance_0;
	Transform_t3275118058 * ___target_1;
	RaycastHit_t87180320_marshaled_com ___raycastHit_2;
	Vector3_t2243707580  ___destinationPosition_3;
	int32_t ___forceDestinationPosition_4;
	int32_t ___enableTeleport_5;
	uint32_t ___controllerIndex_6;
};
