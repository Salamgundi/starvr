﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ObjectControl
struct VRTK_ObjectControl_t724022372;
// VRTK.ObjectControlEventHandler
struct ObjectControlEventHandler_t1756898952;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_ObjectControlEventHandler1756898952.h"
#include "AssemblyU2DCSharp_VRTK_ObjectControlEventArgs2459490319.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_ObjectControl::.ctor()
extern "C"  void VRTK_ObjectControl__ctor_m682487718 (VRTK_ObjectControl_t724022372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectControl::add_XAxisChanged(VRTK.ObjectControlEventHandler)
extern "C"  void VRTK_ObjectControl_add_XAxisChanged_m2618721270 (VRTK_ObjectControl_t724022372 * __this, ObjectControlEventHandler_t1756898952 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectControl::remove_XAxisChanged(VRTK.ObjectControlEventHandler)
extern "C"  void VRTK_ObjectControl_remove_XAxisChanged_m417727871 (VRTK_ObjectControl_t724022372 * __this, ObjectControlEventHandler_t1756898952 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectControl::add_YAxisChanged(VRTK.ObjectControlEventHandler)
extern "C"  void VRTK_ObjectControl_add_YAxisChanged_m3639786609 (VRTK_ObjectControl_t724022372 * __this, ObjectControlEventHandler_t1756898952 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectControl::remove_YAxisChanged(VRTK.ObjectControlEventHandler)
extern "C"  void VRTK_ObjectControl_remove_YAxisChanged_m2698189856 (VRTK_ObjectControl_t724022372 * __this, ObjectControlEventHandler_t1756898952 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectControl::OnXAxisChanged(VRTK.ObjectControlEventArgs)
extern "C"  void VRTK_ObjectControl_OnXAxisChanged_m3080656272 (VRTK_ObjectControl_t724022372 * __this, ObjectControlEventArgs_t2459490319  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectControl::OnYAxisChanged(VRTK.ObjectControlEventArgs)
extern "C"  void VRTK_ObjectControl_OnYAxisChanged_m1671256565 (VRTK_ObjectControl_t724022372 * __this, ObjectControlEventArgs_t2459490319  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectControl::OnEnable()
extern "C"  void VRTK_ObjectControl_OnEnable_m2865670526 (VRTK_ObjectControl_t724022372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectControl::OnDisable()
extern "C"  void VRTK_ObjectControl_OnDisable_m1100531125 (VRTK_ObjectControl_t724022372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectControl::Update()
extern "C"  void VRTK_ObjectControl_Update_m2915064793 (VRTK_ObjectControl_t724022372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectControl::FixedUpdate()
extern "C"  void VRTK_ObjectControl_FixedUpdate_m1182013323 (VRTK_ObjectControl_t724022372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.ObjectControlEventArgs VRTK.VRTK_ObjectControl::SetEventArguements(UnityEngine.Vector3,System.Single,System.Single)
extern "C"  ObjectControlEventArgs_t2459490319  VRTK_ObjectControl_SetEventArguements_m1917930093 (VRTK_ObjectControl_t724022372 * __this, Vector3_t2243707580  ___axisDirection0, float ___axis1, float ___axisDeadzone2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectControl::SetControlledObject()
extern "C"  void VRTK_ObjectControl_SetControlledObject_m600458093 (VRTK_ObjectControl_t724022372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectControl::CheckFalling()
extern "C"  void VRTK_ObjectControl_CheckFalling_m2871756361 (VRTK_ObjectControl_t724022372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_ObjectControl::ObjectHeightChange()
extern "C"  bool VRTK_ObjectControl_ObjectHeightChange_m263610814 (VRTK_ObjectControl_t724022372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_ObjectControl::GetDirectionDevice()
extern "C"  Transform_t3275118058 * VRTK_ObjectControl_GetDirectionDevice_m59990519 (VRTK_ObjectControl_t724022372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectControl::CheckDirectionDevice()
extern "C"  void VRTK_ObjectControl_CheckDirectionDevice_m1560484255 (VRTK_ObjectControl_t724022372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
