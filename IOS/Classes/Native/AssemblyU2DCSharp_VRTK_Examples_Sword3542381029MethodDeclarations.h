﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Sword
struct Sword_t3542381029;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void VRTK.Examples.Sword::.ctor()
extern "C"  void Sword__ctor_m1786612104 (Sword_t3542381029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.Examples.Sword::CollisionForce()
extern "C"  float Sword_CollisionForce_m7332919 (Sword_t3542381029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Sword::Grabbed(UnityEngine.GameObject)
extern "C"  void Sword_Grabbed_m4280011727 (Sword_t3542381029 * __this, GameObject_t1756533147 * ___grabbingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Sword::Awake()
extern "C"  void Sword_Awake_m3492090143 (Sword_t3542381029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Sword::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void Sword_OnCollisionEnter_m1510971278 (Sword_t3542381029 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
