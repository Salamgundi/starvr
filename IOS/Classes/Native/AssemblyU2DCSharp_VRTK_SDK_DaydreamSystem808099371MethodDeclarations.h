﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_DaydreamSystem
struct SDK_DaydreamSystem_t808099371;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_DaydreamSystem::.ctor()
extern "C"  void SDK_DaydreamSystem__ctor_m1974066271 (SDK_DaydreamSystem_t808099371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
