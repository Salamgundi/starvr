﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRSystem_GetController1135918473.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRSystem__GetControllerS15306592.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRSystem_GetController3952429278.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRExtendedDisplay1925229748.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRTrackedCamera2050215972.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRApplications1900926488.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRChaperone441701222.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRChaperoneSetup1611144107.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRCompositor197946050.h"
#include "AssemblyU2DCSharp_Valve_VR_CVROverlay3377499315.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRRenderModels2019937239.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRRenderModels__GetCom3386937929.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRRenderModels_GetComp3431133397.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRNotifications523754935.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRSettings3592067458.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRScreenshots3241040508.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRResources2875708384.h"
#include "AssemblyU2DCSharp_Valve_VR_OpenVRInterop3889140593.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"
#include "AssemblyU2DCSharp_Valve_VR_ETextureType992125572.h"
#include "AssemblyU2DCSharp_Valve_VR_EColorSpace2848861630.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingResult3328467893.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedDeviceClass2121051631.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedControllerRole361251409.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingUniverseOrigin1464400093.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedDeviceProperty3226377054.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedPropertyError3340022390.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRSubmitFlags2736259668.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRState735545894.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREventType6846875.h"
#include "AssemblyU2DCSharp_Valve_VR_EDeviceActivityLevel886867856.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRButtonId66145412.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRMouseButton3132288920.h"
#include "AssemblyU2DCSharp_Valve_VR_EHiddenAreaMeshType3068936429.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRControllerAxisType1358176136.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRControllerEventOutpu2483464006.h"
#include "AssemblyU2DCSharp_Valve_VR_ECollisionBoundsStyle1031782391.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationType1952981913.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRFirmwareError2321703066.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRNotificationError1058814108.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRInitError1685532365.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotType611740195.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotPropertyFile29427162.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraError3529690400.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraFrameTy2003723073.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationProperty1959780520.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationTransitio3895609521.h"
#include "AssemblyU2DCSharp_Valve_VR_ChaperoneCalibrationState49870780.h"
#include "AssemblyU2DCSharp_Valve_VR_EChaperoneConfigFile30305944.h"
#include "AssemblyU2DCSharp_Valve_VR_EChaperoneImportFlags3488691528.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRCompositorError3948578210.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayInputMethod3830649193.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayTransformType3148689642.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayFlags2344570851.h"
#include "AssemblyU2DCSharp_Valve_VR_VRMessageOverlayResponse875548136.h"
#include "AssemblyU2DCSharp_Valve_VR_EGamepadTextInputMode418780318.h"
#include "AssemblyU2DCSharp_Valve_VR_EGamepadTextInputLineMo1154374256.h"
#include "AssemblyU2DCSharp_Valve_VR_EOverlayDirection2759670492.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayIntersectionM2680024489.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRRenderModelError21703732.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRComponentProperty2716992123.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRNotificationType408717238.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRNotificationStyle1858720717.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRSettingsError4124928198.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotError1400268927.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_Data_t3621557720.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayIntersectionMa1248365778.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix44_t664273159.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdVector3_t2255224910.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdVector4_t2255224817.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdVector3d_t3032776948.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdVector2_t2255225135.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdQuaternion_t3957151764.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdColor_t1780554589.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdQuad_t2172573705.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdRect2_t1656020282.h"
#include "AssemblyU2DCSharp_Valve_VR_DistortionCoordinates_t2253454723.h"
#include "AssemblyU2DCSharp_Valve_VR_Texture_t3277130850.h"
#include "AssemblyU2DCSharp_Valve_VR_TrackedDevicePose_t1668551120.h"
#include "AssemblyU2DCSharp_Valve_VR_VRTextureBounds_t1897807375.h"
#include "AssemblyU2DCSharp_Valve_VR_VRVulkanTextureData_t1228579497.h"
#include "AssemblyU2DCSharp_Valve_VR_D3D12TextureData_t3278867774.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_Controller_t3913961192.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_Mouse_t2185307987.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_Scroll_t626693055.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_TouchPadMove_t3788177431.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_Notification_t2249359203.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_Process_t1803567141.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_Overlay_t2752044790.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_Status_t4114371706.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_Keyboard_t3649090227.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_Ipd_t1361274599.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_Chaperone_t3718891675.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_Reserved_t1373699568.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_PerformanceTest1362097580.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_SeatedZeroPoseR1793378776.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_Screenshot_t379786388.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (GetControllerStateUnion_t1135918473)+ sizeof (Il2CppObject), sizeof(GetControllerStateUnion_t1135918473_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2500[2] = 
{
	GetControllerStateUnion_t1135918473::get_offset_of_pGetControllerState_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GetControllerStateUnion_t1135918473::get_offset_of_pGetControllerStatePacked_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (_GetControllerStateWithPosePacked_t15306592), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (GetControllerStateWithPoseUnion_t3952429278)+ sizeof (Il2CppObject), sizeof(GetControllerStateWithPoseUnion_t3952429278_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2502[2] = 
{
	GetControllerStateWithPoseUnion_t3952429278::get_offset_of_pGetControllerStateWithPose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GetControllerStateWithPoseUnion_t3952429278::get_offset_of_pGetControllerStateWithPosePacked_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (CVRExtendedDisplay_t1925229748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[1] = 
{
	CVRExtendedDisplay_t1925229748::get_offset_of_FnTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (CVRTrackedCamera_t2050215972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[1] = 
{
	CVRTrackedCamera_t2050215972::get_offset_of_FnTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (CVRApplications_t1900926488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[1] = 
{
	CVRApplications_t1900926488::get_offset_of_FnTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (CVRChaperone_t441701222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[1] = 
{
	CVRChaperone_t441701222::get_offset_of_FnTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (CVRChaperoneSetup_t1611144107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2507[1] = 
{
	CVRChaperoneSetup_t1611144107::get_offset_of_FnTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (CVRCompositor_t197946050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[1] = 
{
	CVRCompositor_t197946050::get_offset_of_FnTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (CVROverlay_t3377499315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[1] = 
{
	CVROverlay_t3377499315::get_offset_of_FnTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (CVRRenderModels_t2019937239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[1] = 
{
	CVRRenderModels_t2019937239::get_offset_of_FnTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (_GetComponentStatePacked_t3386937929), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (GetComponentStateUnion_t3431133397)+ sizeof (Il2CppObject), sizeof(GetComponentStateUnion_t3431133397_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2512[2] = 
{
	GetComponentStateUnion_t3431133397::get_offset_of_pGetComponentState_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GetComponentStateUnion_t3431133397::get_offset_of_pGetComponentStatePacked_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (CVRNotifications_t523754935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[1] = 
{
	CVRNotifications_t523754935::get_offset_of_FnTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (CVRSettings_t3592067458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2514[1] = 
{
	CVRSettings_t3592067458::get_offset_of_FnTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (CVRScreenshots_t3241040508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[1] = 
{
	CVRScreenshots_t3241040508::get_offset_of_FnTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (CVRResources_t2875708384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[1] = 
{
	CVRResources_t2875708384::get_offset_of_FnTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (OpenVRInterop_t3889140593), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (EVREye_t3088716538)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2518[3] = 
{
	EVREye_t3088716538::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (ETextureType_t992125572)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2519[6] = 
{
	ETextureType_t992125572::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (EColorSpace_t2848861630)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2520[4] = 
{
	EColorSpace_t2848861630::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (ETrackingResult_t3328467893)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2521[6] = 
{
	ETrackingResult_t3328467893::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (ETrackedDeviceClass_t2121051631)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2522[6] = 
{
	ETrackedDeviceClass_t2121051631::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (ETrackedControllerRole_t361251409)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2523[4] = 
{
	ETrackedControllerRole_t361251409::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (ETrackingUniverseOrigin_t1464400093)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2524[4] = 
{
	ETrackingUniverseOrigin_t1464400093::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (ETrackedDeviceProperty_t3226377054)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2525[110] = 
{
	ETrackedDeviceProperty_t3226377054::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (ETrackedPropertyError_t3340022390)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2526[13] = 
{
	ETrackedPropertyError_t3340022390::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (EVRSubmitFlags_t2736259668)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2527[5] = 
{
	EVRSubmitFlags_t2736259668::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (EVRState_t735545894)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2528[10] = 
{
	EVRState_t735545894::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (EVREventType_t6846875)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2529[110] = 
{
	EVREventType_t6846875::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (EDeviceActivityLevel_t886867856)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2530[6] = 
{
	EDeviceActivityLevel_t886867856::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (EVRButtonId_t66145412)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2531[19] = 
{
	EVRButtonId_t66145412::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (EVRMouseButton_t3132288920)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2532[4] = 
{
	EVRMouseButton_t3132288920::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (EHiddenAreaMeshType_t3068936429)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2533[5] = 
{
	EHiddenAreaMeshType_t3068936429::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (EVRControllerAxisType_t1358176136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2534[5] = 
{
	EVRControllerAxisType_t1358176136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (EVRControllerEventOutputType_t2483464006)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2535[3] = 
{
	EVRControllerEventOutputType_t2483464006::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (ECollisionBoundsStyle_t1031782391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2536[7] = 
{
	ECollisionBoundsStyle_t1031782391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (EVROverlayError_t3464864153)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2537[22] = 
{
	EVROverlayError_t3464864153::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (EVRApplicationType_t1952981913)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2538[9] = 
{
	EVRApplicationType_t1952981913::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (EVRFirmwareError_t2321703066)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2539[4] = 
{
	EVRFirmwareError_t2321703066::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (EVRNotificationError_t1058814108)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2540[6] = 
{
	EVRNotificationError_t1058814108::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (EVRInitError_t1685532365)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2541[79] = 
{
	EVRInitError_t1685532365::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (EVRScreenshotType_t611740195)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2542[7] = 
{
	EVRScreenshotType_t611740195::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (EVRScreenshotPropertyFilenames_t29427162)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2543[3] = 
{
	EVRScreenshotPropertyFilenames_t29427162::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (EVRTrackedCameraError_t3529690400)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2544[18] = 
{
	EVRTrackedCameraError_t3529690400::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (EVRTrackedCameraFrameType_t2003723073)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2545[5] = 
{
	EVRTrackedCameraFrameType_t2003723073::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (EVRApplicationError_t862086677)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2546[21] = 
{
	EVRApplicationError_t862086677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (EVRApplicationProperty_t1959780520)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2547[16] = 
{
	EVRApplicationProperty_t1959780520::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (EVRApplicationTransitionState_t3895609521)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2548[5] = 
{
	EVRApplicationTransitionState_t3895609521::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (ChaperoneCalibrationState_t49870780)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2549[11] = 
{
	ChaperoneCalibrationState_t49870780::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (EChaperoneConfigFile_t30305944)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2550[3] = 
{
	EChaperoneConfigFile_t30305944::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (EChaperoneImportFlags_t3488691528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2551[2] = 
{
	EChaperoneImportFlags_t3488691528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (EVRCompositorError_t3948578210)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2552[12] = 
{
	EVRCompositorError_t3948578210::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (VROverlayInputMethod_t3830649193)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2553[3] = 
{
	VROverlayInputMethod_t3830649193::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (VROverlayTransformType_t3148689642)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2554[5] = 
{
	VROverlayTransformType_t3148689642::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (VROverlayFlags_t2344570851)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2555[17] = 
{
	VROverlayFlags_t2344570851::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (VRMessageOverlayResponse_t875548136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2556[8] = 
{
	VRMessageOverlayResponse_t875548136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (EGamepadTextInputMode_t418780318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2557[4] = 
{
	EGamepadTextInputMode_t418780318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (EGamepadTextInputLineMode_t1154374256)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2558[3] = 
{
	EGamepadTextInputLineMode_t1154374256::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (EOverlayDirection_t2759670492)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2559[6] = 
{
	EOverlayDirection_t2759670492::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (EVROverlayIntersectionMaskPrimitiveType_t2680024489)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2560[3] = 
{
	EVROverlayIntersectionMaskPrimitiveType_t2680024489::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (EVRRenderModelError_t21703732)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2561[14] = 
{
	EVRRenderModelError_t21703732::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (EVRComponentProperty_t2716992123)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2562[6] = 
{
	EVRComponentProperty_t2716992123::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (EVRNotificationType_t408717238)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2563[4] = 
{
	EVRNotificationType_t408717238::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (EVRNotificationStyle_t1858720717)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2564[6] = 
{
	EVRNotificationStyle_t1858720717::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (EVRSettingsError_t4124928198)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2565[7] = 
{
	EVRSettingsError_t4124928198::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (EVRScreenshotError_t1400268927)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2566[7] = 
{
	EVRScreenshotError_t1400268927::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (VREvent_Data_t_t3621557720)+ sizeof (Il2CppObject), sizeof(VREvent_Data_t_t3621557720 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2567[19] = 
{
	VREvent_Data_t_t3621557720::get_offset_of_reserved_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_controller_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_mouse_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_scroll_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_process_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_notification_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_overlay_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_status_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_ipd_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_chaperone_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_performanceTest_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_touchPadMove_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_seatedZeroPoseReset_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_screenshot_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_screenshotProgress_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_applicationLaunch_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_cameraSurface_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_messageOverlay_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Data_t_t3621557720::get_offset_of_keyboard_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (VROverlayIntersectionMaskPrimitive_Data_t_t1248365778)+ sizeof (Il2CppObject), sizeof(VROverlayIntersectionMaskPrimitive_Data_t_t1248365778 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2568[2] = 
{
	VROverlayIntersectionMaskPrimitive_Data_t_t1248365778::get_offset_of_m_Rectangle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VROverlayIntersectionMaskPrimitive_Data_t_t1248365778::get_offset_of_m_Circle_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (HmdMatrix34_t_t664273062)+ sizeof (Il2CppObject), sizeof(HmdMatrix34_t_t664273062 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2569[12] = 
{
	HmdMatrix34_t_t664273062::get_offset_of_m0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix34_t_t664273062::get_offset_of_m1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix34_t_t664273062::get_offset_of_m2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix34_t_t664273062::get_offset_of_m3_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix34_t_t664273062::get_offset_of_m4_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix34_t_t664273062::get_offset_of_m5_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix34_t_t664273062::get_offset_of_m6_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix34_t_t664273062::get_offset_of_m7_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix34_t_t664273062::get_offset_of_m8_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix34_t_t664273062::get_offset_of_m9_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix34_t_t664273062::get_offset_of_m10_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix34_t_t664273062::get_offset_of_m11_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (HmdMatrix44_t_t664273159)+ sizeof (Il2CppObject), sizeof(HmdMatrix44_t_t664273159 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2570[16] = 
{
	HmdMatrix44_t_t664273159::get_offset_of_m0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m3_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m4_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m5_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m6_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m7_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m8_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m9_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m10_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m11_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m12_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m13_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m14_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdMatrix44_t_t664273159::get_offset_of_m15_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (HmdVector3_t_t2255224910)+ sizeof (Il2CppObject), sizeof(HmdVector3_t_t2255224910 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2571[3] = 
{
	HmdVector3_t_t2255224910::get_offset_of_v0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdVector3_t_t2255224910::get_offset_of_v1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdVector3_t_t2255224910::get_offset_of_v2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (HmdVector4_t_t2255224817)+ sizeof (Il2CppObject), sizeof(HmdVector4_t_t2255224817 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2572[4] = 
{
	HmdVector4_t_t2255224817::get_offset_of_v0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdVector4_t_t2255224817::get_offset_of_v1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdVector4_t_t2255224817::get_offset_of_v2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdVector4_t_t2255224817::get_offset_of_v3_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (HmdVector3d_t_t3032776948)+ sizeof (Il2CppObject), sizeof(HmdVector3d_t_t3032776948 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2573[3] = 
{
	HmdVector3d_t_t3032776948::get_offset_of_v0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdVector3d_t_t3032776948::get_offset_of_v1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdVector3d_t_t3032776948::get_offset_of_v2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (HmdVector2_t_t2255225135)+ sizeof (Il2CppObject), sizeof(HmdVector2_t_t2255225135 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2574[2] = 
{
	HmdVector2_t_t2255225135::get_offset_of_v0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdVector2_t_t2255225135::get_offset_of_v1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (HmdQuaternion_t_t3957151764)+ sizeof (Il2CppObject), sizeof(HmdQuaternion_t_t3957151764 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2575[4] = 
{
	HmdQuaternion_t_t3957151764::get_offset_of_w_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdQuaternion_t_t3957151764::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdQuaternion_t_t3957151764::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdQuaternion_t_t3957151764::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (HmdColor_t_t1780554589)+ sizeof (Il2CppObject), sizeof(HmdColor_t_t1780554589 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2576[4] = 
{
	HmdColor_t_t1780554589::get_offset_of_r_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdColor_t_t1780554589::get_offset_of_g_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdColor_t_t1780554589::get_offset_of_b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdColor_t_t1780554589::get_offset_of_a_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (HmdQuad_t_t2172573705)+ sizeof (Il2CppObject), sizeof(HmdQuad_t_t2172573705 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2577[4] = 
{
	HmdQuad_t_t2172573705::get_offset_of_vCorners0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdQuad_t_t2172573705::get_offset_of_vCorners1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdQuad_t_t2172573705::get_offset_of_vCorners2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdQuad_t_t2172573705::get_offset_of_vCorners3_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (HmdRect2_t_t1656020282)+ sizeof (Il2CppObject), sizeof(HmdRect2_t_t1656020282 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2578[2] = 
{
	HmdRect2_t_t1656020282::get_offset_of_vTopLeft_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HmdRect2_t_t1656020282::get_offset_of_vBottomRight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (DistortionCoordinates_t_t2253454723)+ sizeof (Il2CppObject), sizeof(DistortionCoordinates_t_t2253454723 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2579[6] = 
{
	DistortionCoordinates_t_t2253454723::get_offset_of_rfRed0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DistortionCoordinates_t_t2253454723::get_offset_of_rfRed1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DistortionCoordinates_t_t2253454723::get_offset_of_rfGreen0_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DistortionCoordinates_t_t2253454723::get_offset_of_rfGreen1_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DistortionCoordinates_t_t2253454723::get_offset_of_rfBlue0_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DistortionCoordinates_t_t2253454723::get_offset_of_rfBlue1_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (Texture_t_t3277130850)+ sizeof (Il2CppObject), sizeof(Texture_t_t3277130850 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2580[3] = 
{
	Texture_t_t3277130850::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Texture_t_t3277130850::get_offset_of_eType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Texture_t_t3277130850::get_offset_of_eColorSpace_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (TrackedDevicePose_t_t1668551120)+ sizeof (Il2CppObject), sizeof(TrackedDevicePose_t_t1668551120 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2581[6] = 
{
	TrackedDevicePose_t_t1668551120::get_offset_of_mDeviceToAbsoluteTracking_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackedDevicePose_t_t1668551120::get_offset_of_vVelocity_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackedDevicePose_t_t1668551120::get_offset_of_vAngularVelocity_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackedDevicePose_t_t1668551120::get_offset_of_eTrackingResult_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackedDevicePose_t_t1668551120::get_offset_of_bPoseIsValid_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackedDevicePose_t_t1668551120::get_offset_of_bDeviceIsConnected_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (VRTextureBounds_t_t1897807375)+ sizeof (Il2CppObject), sizeof(VRTextureBounds_t_t1897807375 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2582[4] = 
{
	VRTextureBounds_t_t1897807375::get_offset_of_uMin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRTextureBounds_t_t1897807375::get_offset_of_vMin_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRTextureBounds_t_t1897807375::get_offset_of_uMax_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRTextureBounds_t_t1897807375::get_offset_of_vMax_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (VRVulkanTextureData_t_t1228579497)+ sizeof (Il2CppObject), sizeof(VRVulkanTextureData_t_t1228579497 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2583[10] = 
{
	VRVulkanTextureData_t_t1228579497::get_offset_of_m_nImage_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRVulkanTextureData_t_t1228579497::get_offset_of_m_pDevice_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRVulkanTextureData_t_t1228579497::get_offset_of_m_pPhysicalDevice_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRVulkanTextureData_t_t1228579497::get_offset_of_m_pInstance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRVulkanTextureData_t_t1228579497::get_offset_of_m_pQueue_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRVulkanTextureData_t_t1228579497::get_offset_of_m_nQueueFamilyIndex_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRVulkanTextureData_t_t1228579497::get_offset_of_m_nWidth_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRVulkanTextureData_t_t1228579497::get_offset_of_m_nHeight_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRVulkanTextureData_t_t1228579497::get_offset_of_m_nFormat_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRVulkanTextureData_t_t1228579497::get_offset_of_m_nSampleCount_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (D3D12TextureData_t_t3278867774)+ sizeof (Il2CppObject), sizeof(D3D12TextureData_t_t3278867774 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2584[3] = 
{
	D3D12TextureData_t_t3278867774::get_offset_of_m_pResource_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	D3D12TextureData_t_t3278867774::get_offset_of_m_pCommandQueue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	D3D12TextureData_t_t3278867774::get_offset_of_m_nNodeMask_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (VREvent_Controller_t_t3913961192)+ sizeof (Il2CppObject), sizeof(VREvent_Controller_t_t3913961192 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2585[1] = 
{
	VREvent_Controller_t_t3913961192::get_offset_of_button_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (VREvent_Mouse_t_t2185307987)+ sizeof (Il2CppObject), sizeof(VREvent_Mouse_t_t2185307987 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2586[3] = 
{
	VREvent_Mouse_t_t2185307987::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Mouse_t_t2185307987::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Mouse_t_t2185307987::get_offset_of_button_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (VREvent_Scroll_t_t626693055)+ sizeof (Il2CppObject), sizeof(VREvent_Scroll_t_t626693055 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2587[3] = 
{
	VREvent_Scroll_t_t626693055::get_offset_of_xdelta_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Scroll_t_t626693055::get_offset_of_ydelta_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Scroll_t_t626693055::get_offset_of_repeatCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (VREvent_TouchPadMove_t_t3788177431)+ sizeof (Il2CppObject), sizeof(VREvent_TouchPadMove_t_t3788177431 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2588[6] = 
{
	VREvent_TouchPadMove_t_t3788177431::get_offset_of_bFingerDown_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_TouchPadMove_t_t3788177431::get_offset_of_flSecondsFingerDown_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_TouchPadMove_t_t3788177431::get_offset_of_fValueXFirst_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_TouchPadMove_t_t3788177431::get_offset_of_fValueYFirst_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_TouchPadMove_t_t3788177431::get_offset_of_fValueXRaw_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_TouchPadMove_t_t3788177431::get_offset_of_fValueYRaw_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (VREvent_Notification_t_t2249359203)+ sizeof (Il2CppObject), sizeof(VREvent_Notification_t_t2249359203 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2589[2] = 
{
	VREvent_Notification_t_t2249359203::get_offset_of_ulUserValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Notification_t_t2249359203::get_offset_of_notificationId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (VREvent_Process_t_t1803567141)+ sizeof (Il2CppObject), sizeof(VREvent_Process_t_t1803567141 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2590[3] = 
{
	VREvent_Process_t_t1803567141::get_offset_of_pid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Process_t_t1803567141::get_offset_of_oldPid_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Process_t_t1803567141::get_offset_of_bForced_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (VREvent_Overlay_t_t2752044790)+ sizeof (Il2CppObject), sizeof(VREvent_Overlay_t_t2752044790 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2591[1] = 
{
	VREvent_Overlay_t_t2752044790::get_offset_of_overlayHandle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (VREvent_Status_t_t4114371706)+ sizeof (Il2CppObject), sizeof(VREvent_Status_t_t4114371706 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2592[1] = 
{
	VREvent_Status_t_t4114371706::get_offset_of_statusState_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (VREvent_Keyboard_t_t3649090227)+ sizeof (Il2CppObject), sizeof(VREvent_Keyboard_t_t3649090227 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2593[9] = 
{
	VREvent_Keyboard_t_t3649090227::get_offset_of_cNewInput0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Keyboard_t_t3649090227::get_offset_of_cNewInput1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Keyboard_t_t3649090227::get_offset_of_cNewInput2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Keyboard_t_t3649090227::get_offset_of_cNewInput3_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Keyboard_t_t3649090227::get_offset_of_cNewInput4_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Keyboard_t_t3649090227::get_offset_of_cNewInput5_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Keyboard_t_t3649090227::get_offset_of_cNewInput6_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Keyboard_t_t3649090227::get_offset_of_cNewInput7_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Keyboard_t_t3649090227::get_offset_of_uUserValue_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (VREvent_Ipd_t_t1361274599)+ sizeof (Il2CppObject), sizeof(VREvent_Ipd_t_t1361274599 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2594[1] = 
{
	VREvent_Ipd_t_t1361274599::get_offset_of_ipdMeters_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (VREvent_Chaperone_t_t3718891675)+ sizeof (Il2CppObject), sizeof(VREvent_Chaperone_t_t3718891675 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2595[2] = 
{
	VREvent_Chaperone_t_t3718891675::get_offset_of_m_nPreviousUniverse_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Chaperone_t_t3718891675::get_offset_of_m_nCurrentUniverse_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (VREvent_Reserved_t_t1373699568)+ sizeof (Il2CppObject), sizeof(VREvent_Reserved_t_t1373699568 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2596[2] = 
{
	VREvent_Reserved_t_t1373699568::get_offset_of_reserved0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Reserved_t_t1373699568::get_offset_of_reserved1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (VREvent_PerformanceTest_t_t1362097580)+ sizeof (Il2CppObject), sizeof(VREvent_PerformanceTest_t_t1362097580 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2597[1] = 
{
	VREvent_PerformanceTest_t_t1362097580::get_offset_of_m_nFidelityLevel_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (VREvent_SeatedZeroPoseReset_t_t1793378776)+ sizeof (Il2CppObject), sizeof(VREvent_SeatedZeroPoseReset_t_t1793378776 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2598[1] = 
{
	VREvent_SeatedZeroPoseReset_t_t1793378776::get_offset_of_bResetBySystemMenu_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (VREvent_Screenshot_t_t379786388)+ sizeof (Il2CppObject), sizeof(VREvent_Screenshot_t_t379786388 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2599[2] = 
{
	VREvent_Screenshot_t_t379786388::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Screenshot_t_t379786388::get_offset_of_type_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
