﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Archery.ArrowNotch
struct  ArrowNotch_t640683907  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject VRTK.Examples.Archery.ArrowNotch::arrow
	GameObject_t1756533147 * ___arrow_2;
	// VRTK.VRTK_InteractableObject VRTK.Examples.Archery.ArrowNotch::obj
	VRTK_InteractableObject_t2604188111 * ___obj_3;

public:
	inline static int32_t get_offset_of_arrow_2() { return static_cast<int32_t>(offsetof(ArrowNotch_t640683907, ___arrow_2)); }
	inline GameObject_t1756533147 * get_arrow_2() const { return ___arrow_2; }
	inline GameObject_t1756533147 ** get_address_of_arrow_2() { return &___arrow_2; }
	inline void set_arrow_2(GameObject_t1756533147 * value)
	{
		___arrow_2 = value;
		Il2CppCodeGenWriteBarrier(&___arrow_2, value);
	}

	inline static int32_t get_offset_of_obj_3() { return static_cast<int32_t>(offsetof(ArrowNotch_t640683907, ___obj_3)); }
	inline VRTK_InteractableObject_t2604188111 * get_obj_3() const { return ___obj_3; }
	inline VRTK_InteractableObject_t2604188111 ** get_address_of_obj_3() { return &___obj_3; }
	inline void set_obj_3(VRTK_InteractableObject_t2604188111 * value)
	{
		___obj_3 = value;
		Il2CppCodeGenWriteBarrier(&___obj_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
