﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.Examples.RC_Car
struct RC_Car_t3139601232;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.RC_Car_Controller
struct  RC_Car_Controller_t2574376713  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject VRTK.Examples.RC_Car_Controller::rcCar
	GameObject_t1756533147 * ___rcCar_2;
	// VRTK.Examples.RC_Car VRTK.Examples.RC_Car_Controller::rcCarScript
	RC_Car_t3139601232 * ___rcCarScript_3;

public:
	inline static int32_t get_offset_of_rcCar_2() { return static_cast<int32_t>(offsetof(RC_Car_Controller_t2574376713, ___rcCar_2)); }
	inline GameObject_t1756533147 * get_rcCar_2() const { return ___rcCar_2; }
	inline GameObject_t1756533147 ** get_address_of_rcCar_2() { return &___rcCar_2; }
	inline void set_rcCar_2(GameObject_t1756533147 * value)
	{
		___rcCar_2 = value;
		Il2CppCodeGenWriteBarrier(&___rcCar_2, value);
	}

	inline static int32_t get_offset_of_rcCarScript_3() { return static_cast<int32_t>(offsetof(RC_Car_Controller_t2574376713, ___rcCarScript_3)); }
	inline RC_Car_t3139601232 * get_rcCarScript_3() const { return ___rcCarScript_3; }
	inline RC_Car_t3139601232 ** get_address_of_rcCarScript_3() { return &___rcCarScript_3; }
	inline void set_rcCarScript_3(RC_Car_t3139601232 * value)
	{
		___rcCarScript_3 = value;
		Il2CppCodeGenWriteBarrier(&___rcCarScript_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
