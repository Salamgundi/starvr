﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_InteractGrab
struct VRTK_InteractGrab_t124353446;
// VRTK.ObjectInteractEventHandler
struct ObjectInteractEventHandler_t1701902511;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventHandler1701902511.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventArgs771291242.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"

// System.Void VRTK.VRTK_InteractGrab::.ctor()
extern "C"  void VRTK_InteractGrab__ctor_m2101901816 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::add_ControllerGrabInteractableObject(VRTK.ObjectInteractEventHandler)
extern "C"  void VRTK_InteractGrab_add_ControllerGrabInteractableObject_m2750768973 (VRTK_InteractGrab_t124353446 * __this, ObjectInteractEventHandler_t1701902511 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::remove_ControllerGrabInteractableObject(VRTK.ObjectInteractEventHandler)
extern "C"  void VRTK_InteractGrab_remove_ControllerGrabInteractableObject_m3081963684 (VRTK_InteractGrab_t124353446 * __this, ObjectInteractEventHandler_t1701902511 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::add_ControllerUngrabInteractableObject(VRTK.ObjectInteractEventHandler)
extern "C"  void VRTK_InteractGrab_add_ControllerUngrabInteractableObject_m338827768 (VRTK_InteractGrab_t124353446 * __this, ObjectInteractEventHandler_t1701902511 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::remove_ControllerUngrabInteractableObject(VRTK.ObjectInteractEventHandler)
extern "C"  void VRTK_InteractGrab_remove_ControllerUngrabInteractableObject_m1648901029 (VRTK_InteractGrab_t124353446 * __this, ObjectInteractEventHandler_t1701902511 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::OnControllerGrabInteractableObject(VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_InteractGrab_OnControllerGrabInteractableObject_m1796791641 (VRTK_InteractGrab_t124353446 * __this, ObjectInteractEventArgs_t771291242  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::OnControllerUngrabInteractableObject(VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_InteractGrab_OnControllerUngrabInteractableObject_m2602581008 (VRTK_InteractGrab_t124353446 * __this, ObjectInteractEventArgs_t771291242  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::ForceRelease(System.Boolean)
extern "C"  void VRTK_InteractGrab_ForceRelease_m724762945 (VRTK_InteractGrab_t124353446 * __this, bool ___applyGrabbingObjectVelocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::AttemptGrab()
extern "C"  void VRTK_InteractGrab_AttemptGrab_m1267368765 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_InteractGrab::GetGrabbedObject()
extern "C"  GameObject_t1756533147 * VRTK_InteractGrab_GetGrabbedObject_m169326555 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::Awake()
extern "C"  void VRTK_InteractGrab_Awake_m218642987 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::OnEnable()
extern "C"  void VRTK_InteractGrab_OnEnable_m3447041628 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::OnDisable()
extern "C"  void VRTK_InteractGrab_OnDisable_m2149482727 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::Update()
extern "C"  void VRTK_InteractGrab_Update_m1775724047 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::RegrabUndroppableObject()
extern "C"  void VRTK_InteractGrab_RegrabUndroppableObject_m132951632 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::SetUndroppableObject()
extern "C"  void VRTK_InteractGrab_SetUndroppableObject_m1156008069 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::SetControllerAttachPoint()
extern "C"  void VRTK_InteractGrab_SetControllerAttachPoint_m2155357559 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractGrab::IsObjectGrabbable(UnityEngine.GameObject)
extern "C"  bool VRTK_InteractGrab_IsObjectGrabbable_m3199968951 (VRTK_InteractGrab_t124353446 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractGrab::IsObjectHoldOnGrab(UnityEngine.GameObject)
extern "C"  bool VRTK_InteractGrab_IsObjectHoldOnGrab_m1226082833 (VRTK_InteractGrab_t124353446 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::ChooseGrabSequence(VRTK.VRTK_InteractableObject)
extern "C"  void VRTK_InteractGrab_ChooseGrabSequence_m4033959364 (VRTK_InteractGrab_t124353446 * __this, VRTK_InteractableObject_t2604188111 * ___grabbedObjectScript0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::ToggleControllerVisibility(System.Boolean)
extern "C"  void VRTK_InteractGrab_ToggleControllerVisibility_m1678306685 (VRTK_InteractGrab_t124353446 * __this, bool ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::InitGrabbedObject()
extern "C"  void VRTK_InteractGrab_InitGrabbedObject_m56070136 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::InitPrimaryGrab(VRTK.VRTK_InteractableObject)
extern "C"  void VRTK_InteractGrab_InitPrimaryGrab_m1321043746 (VRTK_InteractGrab_t124353446 * __this, VRTK_InteractableObject_t2604188111 * ___currentGrabbedObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::InitSecondaryGrab(VRTK.VRTK_InteractableObject)
extern "C"  void VRTK_InteractGrab_InitSecondaryGrab_m875103218 (VRTK_InteractGrab_t124353446 * __this, VRTK_InteractableObject_t2604188111 * ___currentGrabbedObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::CheckInfluencingObjectOnRelease()
extern "C"  void VRTK_InteractGrab_CheckInfluencingObjectOnRelease_m437137677 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::InitUngrabbedObject(System.Boolean)
extern "C"  void VRTK_InteractGrab_InitUngrabbedObject_m1573225764 (VRTK_InteractGrab_t124353446 * __this, bool ___applyGrabbingObjectVelocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_InteractGrab::GetGrabbableObject()
extern "C"  GameObject_t1756533147 * VRTK_InteractGrab_GetGrabbableObject_m3065040468 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::IncrementGrabState()
extern "C"  void VRTK_InteractGrab_IncrementGrabState_m552336454 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_InteractGrab::GetUndroppableObject()
extern "C"  GameObject_t1756533147 * VRTK_InteractGrab_GetUndroppableObject_m3974068830 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::AttemptHaptics(System.Boolean)
extern "C"  void VRTK_InteractGrab_AttemptHaptics_m3563060224 (VRTK_InteractGrab_t124353446 * __this, bool ___initialGrabAttempt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::AttemptGrabObject()
extern "C"  void VRTK_InteractGrab_AttemptGrabObject_m1080235684 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::PerformGrabAttempt(UnityEngine.GameObject)
extern "C"  void VRTK_InteractGrab_PerformGrabAttempt_m3865256964 (VRTK_InteractGrab_t124353446 * __this, GameObject_t1756533147 * ___objectToGrab0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractGrab::IsValidGrabAttempt(UnityEngine.GameObject)
extern "C"  bool VRTK_InteractGrab_IsValidGrabAttempt_m3803503297 (VRTK_InteractGrab_t124353446 * __this, GameObject_t1756533147 * ___objectToGrab0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractGrab::CanRelease()
extern "C"  bool VRTK_InteractGrab_CanRelease_m180692183 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::AttemptReleaseObject()
extern "C"  void VRTK_InteractGrab_AttemptReleaseObject_m4062172783 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::DoGrabObject(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_InteractGrab_DoGrabObject_m2303613546 (VRTK_InteractGrab_t124353446 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::DoReleaseObject(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_InteractGrab_DoReleaseObject_m242196779 (VRTK_InteractGrab_t124353446 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::CheckControllerAttachPointSet()
extern "C"  void VRTK_InteractGrab_CheckControllerAttachPointSet_m1240058985 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::CreateNonTouchingRigidbody()
extern "C"  void VRTK_InteractGrab_CreateNonTouchingRigidbody_m1851874633 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractGrab::CheckPrecognitionGrab()
extern "C"  void VRTK_InteractGrab_CheckPrecognitionGrab_m2392416901 (VRTK_InteractGrab_t124353446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
