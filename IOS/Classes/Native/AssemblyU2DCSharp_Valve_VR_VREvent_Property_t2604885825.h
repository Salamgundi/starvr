﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedDeviceProperty3226377054.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.VREvent_Property_t
struct  VREvent_Property_t_t2604885825 
{
public:
	// System.UInt64 Valve.VR.VREvent_Property_t::container
	uint64_t ___container_0;
	// Valve.VR.ETrackedDeviceProperty Valve.VR.VREvent_Property_t::prop
	int32_t ___prop_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(VREvent_Property_t_t2604885825, ___container_0)); }
	inline uint64_t get_container_0() const { return ___container_0; }
	inline uint64_t* get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(uint64_t value)
	{
		___container_0 = value;
	}

	inline static int32_t get_offset_of_prop_1() { return static_cast<int32_t>(offsetof(VREvent_Property_t_t2604885825, ___prop_1)); }
	inline int32_t get_prop_1() const { return ___prop_1; }
	inline int32_t* get_address_of_prop_1() { return &___prop_1; }
	inline void set_prop_1(int32_t value)
	{
		___prop_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
