﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.String
struct String_t;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.NetworkView
struct NetworkView_t172525251;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Object[]
struct ObjectU5BU5D_t4217747464;
// System.Type
struct Type_t;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t574222242;
// UnityEngine.ParticleSystem/IteratorDelegate
struct IteratorDelegate_t2419492168;
// UnityEngine.ParticleSystem/<Stop>c__AnonStorey1
struct U3CStopU3Ec__AnonStorey1_t1810852071;
// UnityEngine.ParticleSystem/Burst[]
struct BurstU5BU5D_t3091130216;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1214023521;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4176517891;
// UnityEngine.PreferBinarySerialization
struct PreferBinarySerialization_t2472773525;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t2606999759;
// UnityEngine.RangeAttribute
struct RangeAttribute_t3336560921;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t2020713228;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_t3033456180;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Material[]
struct MaterialU5BU5D_t3123989686;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3303648957;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.RequireComponent
struct RequireComponent_t864575032;
// UnityEngine.ResourceRequest
struct ResourceRequest_t2560315377;
// UnityEngine.RPC
struct RPC_t3323229423;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_t3126475234;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;
// UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470;
// UnityEngine.Scripting.APIUpdating.MovedFromAttribute
struct MovedFromAttribute_t922195725;
// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_t4182602970;
// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t1913052472;
// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t3212052468;
// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t936505999;
// UnityEngine.GUILayer
struct GUILayer_t3254902478;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t3673080018;
// UnityEngine.SerializeField
struct SerializeField_t3073427462;
// UnityEngine.SerializePrivateVariables
struct SerializePrivateVariables_t2241034664;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.SharedBetweenAnimatorsAttribute
struct SharedBetweenAnimatorsAttribute_t1565472209;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t4220419316;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.Skybox
struct Skybox_t2033495038;
// UnityEngine.Event
struct Event_t3028476042;
// UnityEngine.SliderState
struct SliderState_t1595681032;
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
struct GameCenterPlatform_t2156144444;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
struct Action_1_t3885079697;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
struct Action_1_t2511354027;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t2283071720;
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
struct Action_1_t3039104018;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t4052399267;
// UnityEngine.SocialPlatforms.ILocalUser
struct ILocalUser_t2210666073;
// System.Action`2<System.Boolean,System.String>
struct Action_2_t1865222972;
// UnityEngine.SocialPlatforms.ILeaderboard
struct ILeaderboard_t77027648;
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
struct Action_1_t3263047812;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t2930725895;
// UnityEngine.SocialPlatforms.IAchievement
struct IAchievement_t1752291260;
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform/<UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate>c__AnonStorey0
struct U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t1170095138;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t1333316625;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MeshRenderer1268241104.h"
#include "UnityEngine_UnityEngine_MeshRenderer1268241104MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshTopology3586470668.h"
#include "UnityEngine_UnityEngine_MeshTopology3586470668MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Motion2415020824.h"
#include "UnityEngine_UnityEngine_Motion2415020824MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo614064059.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo614064059MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1243528291.h"
#include "UnityEngine_UnityEngine_NetworkView172525251.h"
#include "UnityEngine_UnityEngine_NetworkViewID3942988548MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkView172525251MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkViewID3942988548.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1243528291MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "mscorlib_System_Type1303803226.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "mscorlib_System_IntPtr2504060609MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "UnityEngine_UnityEngine_OperatingSystemFamily1896948788.h"
#include "UnityEngine_UnityEngine_OperatingSystemFamily1896948788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem_MainModule6751348.h"
#include "UnityEngine_UnityEngine_ParticleSystem_MainModule6751348MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem_EmissionMod2748003162.h"
#include "UnityEngine_UnityEngine_ParticleSystem_EmissionMod2748003162MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem_Particle250075699.h"
#include "UnityEngine_UnityEngine_ParticleSystemStopBehavior3921148531.h"
#include "UnityEngine_UnityEngine_ParticleSystem_IteratorDel2419492168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem_IteratorDel2419492168.h"
#include "UnityEngine_UnityEngine_ParticleSystem_U3CStopU3Ec1810852071MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem_U3CStopU3Ec1810852071.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_ParticleSystem_Burst208217445.h"
#include "UnityEngine_UnityEngine_ParticleSystem_Burst208217445MethodDeclarations.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "UnityEngine_UnityEngine_ParticleSystem_Particle250075699MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "UnityEngine_UnityEngine_ParticleSystemStopBehavior3921148531MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PhysicMaterial578636151.h"
#include "UnityEngine_UnityEngine_PhysicMaterial578636151MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics634932869.h"
#include "UnityEngine_UnityEngine_Physics634932869MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction478029726.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_Ray2469606224MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_Physics2D2540166467.h"
#include "UnityEngine_UnityEngine_Physics2D2540166467MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4166282325MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4166282325.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"
#include "UnityEngine_UnityEngine_Plane3727654732MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayMode1184682879.h"
#include "UnityEngine_UnityEngine_PlayMode1184682879MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PreferBinarySerialization2472773525.h"
#include "UnityEngine_UnityEngine_PreferBinarySerialization2472773525MethodDeclarations.h"
#include "mscorlib_System_Attribute542643598MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PrimitiveType2454390065.h"
#include "UnityEngine_UnityEngine_PrimitiveType2454390065MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PropertyAttribute2606999759.h"
#include "UnityEngine_UnityEngine_PropertyAttribute2606999759MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QualitySettings3238033062.h"
#include "UnityEngine_UnityEngine_QualitySettings3238033062MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorSpace627621177.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityString276356480MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction478029726MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random1170710517.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RangeAttribute3336560921.h"
#include "UnityEngine_UnityEngine_RangeAttribute3336560921MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RangeInt2323401134.h"
#include "UnityEngine_UnityEngine_RangeInt2323401134MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_Collider3497673348MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrive2020713228.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked1625106012.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrive2020713228MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge3306019089.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis3420330537.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis3420330537MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge3306019089MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransformUtility2941082270.h"
#include "UnityEngine_UnityEngine_RectTransformUtility2941082270MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_RemoteSettings392466225.h"
#include "UnityEngine_UnityEngine_RemoteSettings392466225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RemoteSettings_UpdatedEven3033456180MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RemoteSettings_UpdatedEven3033456180.h"
#include "UnityEngine_UnityEngine_RenderBuffer2767087968.h"
#include "UnityEngine_UnityEngine_RenderBuffer2767087968MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_ShadowCastingMod4042762198.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Rendering_LightProbeUsage664674855.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeU1870040434.h"
#include "UnityEngine_UnityEngine_MaterialPropertyBlock3303648957.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask926634530.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask926634530MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction457874581.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction457874581MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_GraphicsDeviceTyp872267891.h"
#include "UnityEngine_UnityEngine_Rendering_GraphicsDeviceTyp872267891MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_LightProbeUsage664674855MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeU1870040434MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_ShadowCastingMod4042762198MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp2936374925.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp2936374925MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderMode4280533217.h"
#include "UnityEngine_UnityEngine_RenderMode4280533217MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderSettings1057812535.h"
#include "UnityEngine_UnityEngine_RenderSettings1057812535MethodDeclarations.h"
#include "UnityEngine_UnityEngine_FogMode2386547659.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat3360518468.h"
#include "UnityEngine_UnityEngine_Texture2243626319MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite2842868372.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat3360518468MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite2842868372MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RequireComponent864575032.h"
#include "UnityEngine_UnityEngine_RequireComponent864575032MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourceRequest2560315377.h"
#include "UnityEngine_UnityEngine_ResourceRequest2560315377MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources339470017MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources339470017.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints251614631.h"
#include "UnityEngine_UnityEngine_CollisionDetectionMode841589752.h"
#include "UnityEngine_UnityEngine_ForceMode1856518252.h"
#include "UnityEngine_UnityEngine_RigidbodyInterpolation993299413.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints251614631MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RigidbodyInterpolation993299413MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RPC3323229423.h"
#include "UnityEngine_UnityEngine_RPC3323229423MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimeInitializeLoadType205334256.h"
#include "UnityEngine_UnityEngine_RuntimeInitializeLoadType205334256MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimeInitializeOnLoadMet3126475234.h"
#include "UnityEngine_UnityEngine_RuntimeInitializeOnLoadMet3126475234MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_PreserveAttribut4182602970MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScaleMode324459649.h"
#include "UnityEngine_UnityEngine_ScaleMode324459649MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1903595547MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1903595547.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3051495417MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3051495417.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen606618774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen606618774.h"
#include "UnityEngine_UnityEngine_Screen786852042.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_APIUpdating_Moved922195725.h"
#include "UnityEngine_UnityEngine_Scripting_APIUpdating_Moved922195725MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_PreserveAttribut4182602970.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute936505999.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute936505999MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3505065032.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3505065032MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayer3254902478MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo1761367055.h"
#include "UnityEngine_UnityEngine_GUILayer3254902478.h"
#include "UnityEngine_UnityEngine_GUIElement3381083099.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_CameraClearFlags452084705.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo1761367055MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SerializeField3073427462.h"
#include "UnityEngine_UnityEngine_SerializeField3073427462MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2241034664.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2241034664MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SetupCoroutine3582942563.h"
#include "UnityEngine_UnityEngine_SetupCoroutine3582942563MethodDeclarations.h"
#include "mscorlib_System_Reflection_BindingFlags1082350898.h"
#include "mscorlib_System_Reflection_Binder3404612058.h"
#include "mscorlib_System_Reflection_ParameterModifier1820634920.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "UnityEngine_UnityEngine_Shader2430389951.h"
#include "UnityEngine_UnityEngine_Shader2430389951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1565472209.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1565472209MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SkeletonBone345082847.h"
#include "UnityEngine_UnityEngine_SkeletonBone345082847MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer4220419316.h"
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer4220419316MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SkinQuality1832760476.h"
#include "UnityEngine_UnityEngine_SkinQuality1832760476MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Skybox2033495038.h"
#include "UnityEngine_UnityEngine_Skybox2033495038MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderHandler3550500579.h"
#include "UnityEngine_UnityEngine_SliderHandler3550500579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventType3919834026.h"
#include "UnityEngine_UnityEngine_Event3028476042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIUtility3275770671MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemClock104337557MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "UnityEngine_UnityEngine_Event3028476042.h"
#include "UnityEngine_UnityEngine_SliderState1595681032.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent4210063000.h"
#include "UnityEngine_UnityEngine_GUIContent4210063000MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderState1595681032MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2156144444.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2156144444MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope2583939667.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie3110978151.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_960725851.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_960725851MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie3110978151MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3885079697.h"
#include "mscorlib_System_Action_1_gen3885079697MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1865222972MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1865222972.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP3365630962.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3198293052.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3198293052MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local3019851150MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local3019851150.h"
#include "mscorlib_System_Action_1_gen2511354027.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1754866149.h"
#include "mscorlib_System_Action_1_gen2511354027MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie1333316625.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1754866149MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3039104018.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3676783238.h"
#include "mscorlib_System_Action_1_gen3039104018MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score2307748940.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3676783238MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1170095138MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1170095138.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP3365630962MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_453887929MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4117976357MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade4160680639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade4160680639.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_453887929.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range3455291607.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4117976357.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope3775842435.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3652706031.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3652706031MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3263047812.h"
#include "mscorlib_System_Action_1_gen3263047812MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie1333316625MethodDeclarations.h"

// T UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2650145732(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// T UnityEngine.GameObject::GetComponent<UnityEngine.ParticleSystem>()
#define GameObject_GetComponent_TisParticleSystem_t3394631041_m2067134504(__this, method) ((  ParticleSystem_t3394631041 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.Object>(!!0&,!!0,!!0)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m2145889806(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.RectTransform/ReapplyDrivenProperties>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisReapplyDrivenProperties_t2020713228_m2152142180(__this /* static, unused */, p0, p1, p2, method) ((  ReapplyDrivenProperties_t2020713228 * (*) (Il2CppObject * /* static, unused */, ReapplyDrivenProperties_t2020713228 **, ReapplyDrivenProperties_t2020713228 *, ReapplyDrivenProperties_t2020713228 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// T UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// T UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
#define Component_GetComponent_TisGUILayer_t3254902478_m4287216801(__this, method) ((  GUILayer_t3254902478 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
extern "C"  Mesh_t1356156583 * MeshFilter_get_mesh_m977520135 (MeshFilter_t3026937449 * __this, const MethodInfo* method)
{
	typedef Mesh_t1356156583 * (*MeshFilter_get_mesh_m977520135_ftn) (MeshFilter_t3026937449 *);
	static MeshFilter_get_mesh_m977520135_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_get_mesh_m977520135_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::get_mesh()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
extern "C"  void MeshFilter_set_mesh_m3839924176 (MeshFilter_t3026937449 * __this, Mesh_t1356156583 * ___value0, const MethodInfo* method)
{
	typedef void (*MeshFilter_set_mesh_m3839924176_ftn) (MeshFilter_t3026937449 *, Mesh_t1356156583 *);
	static MeshFilter_set_mesh_m3839924176_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_set_mesh_m3839924176_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C"  Mesh_t1356156583 * MeshFilter_get_sharedMesh_m1310789932 (MeshFilter_t3026937449 * __this, const MethodInfo* method)
{
	typedef Mesh_t1356156583 * (*MeshFilter_get_sharedMesh_m1310789932_ftn) (MeshFilter_t3026937449 *);
	static MeshFilter_get_sharedMesh_m1310789932_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_get_sharedMesh_m1310789932_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::get_sharedMesh()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
extern "C"  void MeshFilter_set_sharedMesh_m2225370173 (MeshFilter_t3026937449 * __this, Mesh_t1356156583 * ___value0, const MethodInfo* method)
{
	typedef void (*MeshFilter_set_sharedMesh_m2225370173_ftn) (MeshFilter_t3026937449 *, Mesh_t1356156583 *);
	static MeshFilter_set_sharedMesh_m2225370173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_set_sharedMesh_m2225370173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	{
		Behaviour__ctor_m2699265412(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()
extern "C"  void MonoBehaviour_Internal_CancelInvokeAll_m3154116776 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Internal_CancelInvokeAll_m3154116776_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_Internal_CancelInvokeAll_m3154116776_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_CancelInvokeAll_m3154116776_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.MonoBehaviour::Internal_IsInvokingAll()
extern "C"  bool MonoBehaviour_Internal_IsInvokingAll_m3504849565 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_Internal_IsInvokingAll_m3504849565_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_Internal_IsInvokingAll_m3504849565_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_IsInvokingAll_m3504849565_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_IsInvokingAll()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C"  void MonoBehaviour_Invoke_m666563676 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, float ___time1, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Invoke_m666563676_ftn) (MonoBehaviour_t1158329972 *, String_t*, float);
	static MonoBehaviour_Invoke_m666563676_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Invoke_m666563676_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)");
	_il2cpp_icall_func(__this, ___methodName0, ___time1);
}
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern "C"  void MonoBehaviour_InvokeRepeating_m3468262484 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, float ___time1, float ___repeatRate2, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_InvokeRepeating_m3468262484_ftn) (MonoBehaviour_t1158329972 *, String_t*, float, float);
	static MonoBehaviour_InvokeRepeating_m3468262484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_InvokeRepeating_m3468262484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___methodName0, ___time1, ___repeatRate2);
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke()
extern "C"  void MonoBehaviour_CancelInvoke_m744713777 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_Internal_CancelInvokeAll_m3154116776(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
extern "C"  void MonoBehaviour_CancelInvoke_m2508161963 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_CancelInvoke_m2508161963_ftn) (MonoBehaviour_t1158329972 *, String_t*);
	static MonoBehaviour_CancelInvoke_m2508161963_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_CancelInvoke_m2508161963_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::CancelInvoke(System.String)");
	_il2cpp_icall_func(__this, ___methodName0);
}
// System.Boolean UnityEngine.MonoBehaviour::IsInvoking(System.String)
extern "C"  bool MonoBehaviour_IsInvoking_m1469271462 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_IsInvoking_m1469271462_ftn) (MonoBehaviour_t1158329972 *, String_t*);
	static MonoBehaviour_IsInvoking_m1469271462_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_IsInvoking_m1469271462_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::IsInvoking(System.String)");
	return _il2cpp_icall_func(__this, ___methodName0);
}
// System.Boolean UnityEngine.MonoBehaviour::IsInvoking()
extern "C"  bool MonoBehaviour_IsInvoking_m345622956 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = MonoBehaviour_Internal_IsInvokingAll_m3504849565(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m2470621050 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	Coroutine_t2299508840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___routine0;
		Coroutine_t2299508840 * L_1 = MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Coroutine_t2299508840 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_Auto_m1744905232 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	Coroutine_t2299508840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___routine0;
		Coroutine_t2299508840 * L_1 = MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Coroutine_t2299508840 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	typedef Coroutine_t2299508840 * (*MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456_ftn) (MonoBehaviour_t1158329972 *, Il2CppObject *);
	static MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)");
	return _il2cpp_icall_func(__this, ___routine0);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m296997955 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	typedef Coroutine_t2299508840 * (*MonoBehaviour_StartCoroutine_m296997955_ftn) (MonoBehaviour_t1158329972 *, String_t*, Il2CppObject *);
	static MonoBehaviour_StartCoroutine_m296997955_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_m296997955_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)");
	return _il2cpp_icall_func(__this, ___methodName0, ___value1);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m1399371129 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Coroutine_t2299508840 * V_1 = NULL;
	{
		V_0 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_0;
		Coroutine_t2299508840 * L_2 = MonoBehaviour_StartCoroutine_m296997955(__this, L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		Coroutine_t2299508840 * L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
extern "C"  void MonoBehaviour_StopCoroutine_m987450539 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_m987450539_ftn) (MonoBehaviour_t1158329972 *, String_t*);
	static MonoBehaviour_StopCoroutine_m987450539_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_m987450539_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine(System.String)");
	_il2cpp_icall_func(__this, ___methodName0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutine_m1170478282 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___routine0;
		MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_m1668572632 (MonoBehaviour_t1158329972 * __this, Coroutine_t2299508840 * ___routine0, const MethodInfo* method)
{
	{
		Coroutine_t2299508840 * L_0 = ___routine0;
		MonoBehaviour_StopCoroutine_Auto_m1923670638(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826_ftn) (MonoBehaviour_t1158329972 *, Il2CppObject *);
	static MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_Auto_m1923670638 (MonoBehaviour_t1158329972 * __this, Coroutine_t2299508840 * ___routine0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_Auto_m1923670638_ftn) (MonoBehaviour_t1158329972 *, Coroutine_t2299508840 *);
	static MonoBehaviour_StopCoroutine_Auto_m1923670638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_Auto_m1923670638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
extern "C"  void MonoBehaviour_StopAllCoroutines_m1675795839 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopAllCoroutines_m1675795839_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_StopAllCoroutines_m1675795839_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopAllCoroutines_m1675795839_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopAllCoroutines()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviour_print_m3437620292_MetadataUsageId;
extern "C"  void MonoBehaviour_print_m3437620292 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviour_print_m3437620292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.MonoBehaviour::get_useGUILayout()
extern "C"  bool MonoBehaviour_get_useGUILayout_m524237270 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_get_useGUILayout_m524237270_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_get_useGUILayout_m524237270_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_get_useGUILayout_m524237270_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::get_useGUILayout()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
extern "C"  void MonoBehaviour_set_useGUILayout_m2666356651 (MonoBehaviour_t1158329972 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_set_useGUILayout_m2666356651_ftn) (MonoBehaviour_t1158329972 *, bool);
	static MonoBehaviour_set_useGUILayout_m2666356651_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_set_useGUILayout_m2666356651_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Double UnityEngine.NetworkMessageInfo::get_timestamp()
extern "C"  double NetworkMessageInfo_get_timestamp_m462964950 (NetworkMessageInfo_t614064059 * __this, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		double L_0 = __this->get_m_TimeStamp_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		double L_1 = V_0;
		return L_1;
	}
}
extern "C"  double NetworkMessageInfo_get_timestamp_m462964950_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t614064059 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t614064059 *>(__this + 1);
	return NetworkMessageInfo_get_timestamp_m462964950(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkMessageInfo::get_sender()
extern "C"  NetworkPlayer_t1243528291  NetworkMessageInfo_get_sender_m2366451889 (NetworkMessageInfo_t614064059 * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkPlayer_t1243528291  L_0 = __this->get_m_Sender_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		NetworkPlayer_t1243528291  L_1 = V_0;
		return L_1;
	}
}
extern "C"  NetworkPlayer_t1243528291  NetworkMessageInfo_get_sender_m2366451889_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t614064059 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t614064059 *>(__this + 1);
	return NetworkMessageInfo_get_sender_m2366451889(_thisAdjusted, method);
}
// UnityEngine.NetworkView UnityEngine.NetworkMessageInfo::get_networkView()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3523052579;
extern const uint32_t NetworkMessageInfo_get_networkView_m1180629659_MetadataUsageId;
extern "C"  NetworkView_t172525251 * NetworkMessageInfo_get_networkView_m1180629659 (NetworkMessageInfo_t614064059 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NetworkMessageInfo_get_networkView_m1180629659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NetworkView_t172525251 * V_0 = NULL;
	{
		NetworkViewID_t3942988548  L_0 = __this->get_m_ViewID_2();
		NetworkViewID_t3942988548  L_1 = NetworkViewID_get_unassigned_m2814913999(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = NetworkViewID_op_Equality_m4173239775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3523052579, /*hidden argument*/NULL);
		NetworkView_t172525251 * L_3 = NetworkMessageInfo_NullNetworkView_m230600625(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_003e;
	}

IL_002d:
	{
		NetworkViewID_t3942988548  L_4 = __this->get_m_ViewID_2();
		NetworkView_t172525251 * L_5 = NetworkView_Find_m143547961(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_003e;
	}

IL_003e:
	{
		NetworkView_t172525251 * L_6 = V_0;
		return L_6;
	}
}
extern "C"  NetworkView_t172525251 * NetworkMessageInfo_get_networkView_m1180629659_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t614064059 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t614064059 *>(__this + 1);
	return NetworkMessageInfo_get_networkView_m1180629659(_thisAdjusted, method);
}
// UnityEngine.NetworkView UnityEngine.NetworkMessageInfo::NullNetworkView()
extern "C"  NetworkView_t172525251 * NetworkMessageInfo_NullNetworkView_m230600625 (NetworkMessageInfo_t614064059 * __this, const MethodInfo* method)
{
	typedef NetworkView_t172525251 * (*NetworkMessageInfo_NullNetworkView_m230600625_ftn) (NetworkMessageInfo_t614064059 *);
	static NetworkMessageInfo_NullNetworkView_m230600625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkMessageInfo_NullNetworkView_m230600625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkMessageInfo::NullNetworkView()");
	return _il2cpp_icall_func(__this);
}
extern "C"  NetworkView_t172525251 * NetworkMessageInfo_NullNetworkView_m230600625_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t614064059 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t614064059 *>(__this + 1);
	return NetworkMessageInfo_NullNetworkView_m230600625(_thisAdjusted, method);
}
// System.Void UnityEngine.NetworkPlayer::.ctor(System.String,System.Int32)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1640566403;
extern const uint32_t NetworkPlayer__ctor_m3970319947_MetadataUsageId;
extern "C"  void NetworkPlayer__ctor_m3970319947 (NetworkPlayer_t1243528291 * __this, String_t* ___ip0, int32_t ___port1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NetworkPlayer__ctor_m3970319947_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1640566403, /*hidden argument*/NULL);
		__this->set_index_0(0);
		return;
	}
}
extern "C"  void NetworkPlayer__ctor_m3970319947_AdjustorThunk (Il2CppObject * __this, String_t* ___ip0, int32_t ___port1, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	NetworkPlayer__ctor_m3970319947(_thisAdjusted, ___ip0, ___port1, method);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetIPAddress(System.Int32)
extern "C"  String_t* NetworkPlayer_Internal_GetIPAddress_m2394140501 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetIPAddress_m2394140501_ftn) (int32_t);
	static NetworkPlayer_Internal_GetIPAddress_m2394140501_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetIPAddress_m2394140501_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetIPAddress(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetPort(System.Int32)
extern "C"  int32_t NetworkPlayer_Internal_GetPort_m3852797570 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetPort_m3852797570_ftn) (int32_t);
	static NetworkPlayer_Internal_GetPort_m3852797570_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetPort_m3852797570_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetPort(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetExternalIP()
extern "C"  String_t* NetworkPlayer_Internal_GetExternalIP_m2789058499 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetExternalIP_m2789058499_ftn) ();
	static NetworkPlayer_Internal_GetExternalIP_m2789058499_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetExternalIP_m2789058499_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetExternalIP()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetExternalPort()
extern "C"  int32_t NetworkPlayer_Internal_GetExternalPort_m2755622672 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetExternalPort_m2755622672_ftn) ();
	static NetworkPlayer_Internal_GetExternalPort_m2755622672_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetExternalPort_m2755622672_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetExternalPort()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.NetworkPlayer::Internal_GetLocalIP()
extern "C"  String_t* NetworkPlayer_Internal_GetLocalIP_m2626871687 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetLocalIP_m2626871687_ftn) ();
	static NetworkPlayer_Internal_GetLocalIP_m2626871687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalIP_m2626871687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalIP()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetLocalPort()
extern "C"  int32_t NetworkPlayer_Internal_GetLocalPort_m2373269974 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetLocalPort_m2373269974_ftn) ();
	static NetworkPlayer_Internal_GetLocalPort_m2373269974_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalPort_m2373269974_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalPort()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetPlayerIndex()
extern "C"  int32_t NetworkPlayer_Internal_GetPlayerIndex_m2582225917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetPlayerIndex_m2582225917_ftn) ();
	static NetworkPlayer_Internal_GetPlayerIndex_m2582225917_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetPlayerIndex_m2582225917_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetPlayerIndex()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.NetworkPlayer::Internal_GetGUID(System.Int32)
extern "C"  String_t* NetworkPlayer_Internal_GetGUID_m281231101 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetGUID_m281231101_ftn) (int32_t);
	static NetworkPlayer_Internal_GetGUID_m281231101_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetGUID_m281231101_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetGUID(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetLocalGUID()
extern "C"  String_t* NetworkPlayer_Internal_GetLocalGUID_m49665029 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetLocalGUID_m49665029_ftn) ();
	static NetworkPlayer_Internal_GetLocalGUID_m49665029_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalGUID_m49665029_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalGUID()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.NetworkPlayer::op_Equality(UnityEngine.NetworkPlayer,UnityEngine.NetworkPlayer)
extern "C"  bool NetworkPlayer_op_Equality_m2104137122 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___lhs0, NetworkPlayer_t1243528291  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = (&___lhs0)->get_index_0();
		int32_t L_1 = (&___rhs1)->get_index_0();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
		goto IL_0017;
	}

IL_0017:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.NetworkPlayer::op_Inequality(UnityEngine.NetworkPlayer,UnityEngine.NetworkPlayer)
extern "C"  bool NetworkPlayer_op_Inequality_m3070881435 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___lhs0, NetworkPlayer_t1243528291  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = (&___lhs0)->get_index_0();
		int32_t L_1 = (&___rhs1)->get_index_0();
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001a;
	}

IL_001a:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.NetworkPlayer::GetHashCode()
extern "C"  int32_t NetworkPlayer_GetHashCode_m2788349399 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t* L_0 = __this->get_address_of_index_0();
		int32_t L_1 = Int32_GetHashCode_m1381647448(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0018;
	}

IL_0018:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t NetworkPlayer_GetHashCode_m2788349399_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_GetHashCode_m2788349399(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkPlayer::Equals(System.Object)
extern Il2CppClass* NetworkPlayer_t1243528291_il2cpp_TypeInfo_var;
extern const uint32_t NetworkPlayer_Equals_m1960901089_MetadataUsageId;
extern "C"  bool NetworkPlayer_Equals_m1960901089 (NetworkPlayer_t1243528291 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NetworkPlayer_Equals_m1960901089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	NetworkPlayer_t1243528291  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, NetworkPlayer_t1243528291_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002f;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(NetworkPlayer_t1243528291 *)((NetworkPlayer_t1243528291 *)UnBox (L_1, NetworkPlayer_t1243528291_il2cpp_TypeInfo_var))));
		int32_t L_2 = (&V_1)->get_index_0();
		int32_t L_3 = __this->get_index_0();
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
		goto IL_002f;
	}

IL_002f:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool NetworkPlayer_Equals_m1960901089_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_Equals_m1960901089(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.NetworkPlayer::get_ipAddress()
extern "C"  String_t* NetworkPlayer_get_ipAddress_m3020320555 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m2582225917(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_2 = NetworkPlayer_Internal_GetLocalIP_m2626871687(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_002d;
	}

IL_001c:
	{
		int32_t L_3 = __this->get_index_0();
		String_t* L_4 = NetworkPlayer_Internal_GetIPAddress_m2394140501(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002d;
	}

IL_002d:
	{
		String_t* L_5 = V_0;
		return L_5;
	}
}
extern "C"  String_t* NetworkPlayer_get_ipAddress_m3020320555_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_ipAddress_m3020320555(_thisAdjusted, method);
}
// System.Int32 UnityEngine.NetworkPlayer::get_port()
extern "C"  int32_t NetworkPlayer_get_port_m2797602516 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m2582225917(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = NetworkPlayer_Internal_GetLocalPort_m2373269974(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_002d;
	}

IL_001c:
	{
		int32_t L_3 = __this->get_index_0();
		int32_t L_4 = NetworkPlayer_Internal_GetPort_m3852797570(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002d;
	}

IL_002d:
	{
		int32_t L_5 = V_0;
		return L_5;
	}
}
extern "C"  int32_t NetworkPlayer_get_port_m2797602516_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_port_m2797602516(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::get_guid()
extern "C"  String_t* NetworkPlayer_get_guid_m1047639783 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m2582225917(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_2 = NetworkPlayer_Internal_GetLocalGUID_m49665029(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_002d;
	}

IL_001c:
	{
		int32_t L_3 = __this->get_index_0();
		String_t* L_4 = NetworkPlayer_Internal_GetGUID_m281231101(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002d;
	}

IL_002d:
	{
		String_t* L_5 = V_0;
		return L_5;
	}
}
extern "C"  String_t* NetworkPlayer_get_guid_m1047639783_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_guid_m1047639783(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::ToString()
extern "C"  String_t* NetworkPlayer_ToString_m537794039 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_index_0();
		String_t* L_1 = Int32_ToString_m2960866144(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0018;
	}

IL_0018:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
extern "C"  String_t* NetworkPlayer_ToString_m537794039_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_ToString_m537794039(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::get_externalIP()
extern "C"  String_t* NetworkPlayer_get_externalIP_m343056966 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = NetworkPlayer_Internal_GetExternalIP_m2789058499(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
extern "C"  String_t* NetworkPlayer_get_externalIP_m343056966_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_externalIP_m343056966(_thisAdjusted, method);
}
// System.Int32 UnityEngine.NetworkPlayer::get_externalPort()
extern "C"  int32_t NetworkPlayer_get_externalPort_m114464875 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = NetworkPlayer_Internal_GetExternalPort_m2755622672(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t NetworkPlayer_get_externalPort_m114464875_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_externalPort_m114464875(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkPlayer::get_unassigned()
extern "C"  NetworkPlayer_t1243528291  NetworkPlayer_get_unassigned_m3961648431 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkPlayer_t1243528291  V_0;
	memset(&V_0, 0, sizeof(V_0));
	NetworkPlayer_t1243528291  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		(&V_0)->set_index_0((-1));
		NetworkPlayer_t1243528291  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		NetworkPlayer_t1243528291  L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.NetworkView UnityEngine.NetworkView::Find(UnityEngine.NetworkViewID)
extern "C"  NetworkView_t172525251 * NetworkView_Find_m143547961 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___viewID0, const MethodInfo* method)
{
	NetworkView_t172525251 * V_0 = NULL;
	{
		NetworkView_t172525251 * L_0 = NetworkView_INTERNAL_CALL_Find_m526018730(NULL /*static, unused*/, (&___viewID0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		NetworkView_t172525251 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.NetworkView UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)
extern "C"  NetworkView_t172525251 * NetworkView_INTERNAL_CALL_Find_m526018730 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___viewID0, const MethodInfo* method)
{
	typedef NetworkView_t172525251 * (*NetworkView_INTERNAL_CALL_Find_m526018730_ftn) (NetworkViewID_t3942988548 *);
	static NetworkView_INTERNAL_CALL_Find_m526018730_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkView_INTERNAL_CALL_Find_m526018730_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___viewID0);
}
// UnityEngine.NetworkViewID UnityEngine.NetworkViewID::get_unassigned()
extern "C"  NetworkViewID_t3942988548  NetworkViewID_get_unassigned_m2814913999 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkViewID_t3942988548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	NetworkViewID_t3942988548  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		NetworkViewID_INTERNAL_get_unassigned_m132572206(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		NetworkViewID_t3942988548  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		NetworkViewID_t3942988548  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)
extern "C"  void NetworkViewID_INTERNAL_get_unassigned_m132572206 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___value0, const MethodInfo* method)
{
	typedef void (*NetworkViewID_INTERNAL_get_unassigned_m132572206_ftn) (NetworkViewID_t3942988548 *);
	static NetworkViewID_INTERNAL_get_unassigned_m132572206_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_get_unassigned_m132572206_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.NetworkViewID::Internal_IsMine(UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_IsMine_m763014699 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___value0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___value0, const MethodInfo* method)
{
	typedef bool (*NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398_ftn) (NetworkViewID_t3942988548 *);
	static NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.NetworkViewID::Internal_GetOwner(UnityEngine.NetworkViewID,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_Internal_GetOwner_m89862041 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___value0, NetworkPlayer_t1243528291 * ___player1, const MethodInfo* method)
{
	{
		NetworkPlayer_t1243528291 * L_0 = ___player1;
		NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604(NULL /*static, unused*/, (&___value0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___value0, NetworkPlayer_t1243528291 * ___player1, const MethodInfo* method)
{
	typedef void (*NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604_ftn) (NetworkViewID_t3942988548 *, NetworkPlayer_t1243528291 *);
	static NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)");
	_il2cpp_icall_func(___value0, ___player1);
}
// System.String UnityEngine.NetworkViewID::Internal_GetString(UnityEngine.NetworkViewID)
extern "C"  String_t* NetworkViewID_Internal_GetString_m403971590 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___value0, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)
extern "C"  String_t* NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___value0, const MethodInfo* method)
{
	typedef String_t* (*NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803_ftn) (NetworkViewID_t3942988548 *);
	static NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.NetworkViewID::Internal_Compare(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_Compare_m3248992772 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___lhs0, NetworkViewID_t3942988548  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333(NULL /*static, unused*/, (&___lhs0), (&___rhs1), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___lhs0, NetworkViewID_t3942988548 * ___rhs1, const MethodInfo* method)
{
	typedef bool (*NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333_ftn) (NetworkViewID_t3942988548 *, NetworkViewID_t3942988548 *);
	static NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___lhs0, ___rhs1);
}
// System.Boolean UnityEngine.NetworkViewID::op_Equality(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_op_Equality_m4173239775 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___lhs0, NetworkViewID_t3942988548  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		NetworkViewID_t3942988548  L_0 = ___lhs0;
		NetworkViewID_t3942988548  L_1 = ___rhs1;
		bool L_2 = NetworkViewID_Internal_Compare_m3248992772(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.NetworkViewID::op_Inequality(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_op_Inequality_m309368134 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___lhs0, NetworkViewID_t3942988548  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		NetworkViewID_t3942988548  L_0 = ___lhs0;
		NetworkViewID_t3942988548  L_1 = ___rhs1;
		bool L_2 = NetworkViewID_Internal_Compare_m3248992772(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Int32 UnityEngine.NetworkViewID::GetHashCode()
extern "C"  int32_t NetworkViewID_GetHashCode_m3141878442 (NetworkViewID_t3942988548 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_a_0();
		int32_t L_1 = __this->get_b_1();
		int32_t L_2 = __this->get_c_2();
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0^(int32_t)L_1))^(int32_t)L_2));
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
extern "C"  int32_t NetworkViewID_GetHashCode_m3141878442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_GetHashCode_m3141878442(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkViewID::Equals(System.Object)
extern Il2CppClass* NetworkViewID_t3942988548_il2cpp_TypeInfo_var;
extern const uint32_t NetworkViewID_Equals_m809788370_MetadataUsageId;
extern "C"  bool NetworkViewID_Equals_m809788370 (NetworkViewID_t3942988548 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NetworkViewID_Equals_m809788370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	NetworkViewID_t3942988548  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, NetworkViewID_t3942988548_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002c;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(NetworkViewID_t3942988548 *)((NetworkViewID_t3942988548 *)UnBox (L_1, NetworkViewID_t3942988548_il2cpp_TypeInfo_var))));
		NetworkViewID_t3942988548  L_2 = V_1;
		bool L_3 = NetworkViewID_Internal_Compare_m3248992772(NULL /*static, unused*/, (*(NetworkViewID_t3942988548 *)__this), L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool NetworkViewID_Equals_m809788370_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_Equals_m809788370(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.NetworkViewID::get_isMine()
extern "C"  bool NetworkViewID_get_isMine_m1234363003 (NetworkViewID_t3942988548 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = NetworkViewID_Internal_IsMine_m763014699(NULL /*static, unused*/, (*(NetworkViewID_t3942988548 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool NetworkViewID_get_isMine_m1234363003_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_get_isMine_m1234363003(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkViewID::get_owner()
extern "C"  NetworkPlayer_t1243528291  NetworkViewID_get_owner_m1900957708 (NetworkViewID_t3942988548 * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291  V_0;
	memset(&V_0, 0, sizeof(V_0));
	NetworkPlayer_t1243528291  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		NetworkViewID_Internal_GetOwner_m89862041(NULL /*static, unused*/, (*(NetworkViewID_t3942988548 *)__this), (&V_0), /*hidden argument*/NULL);
		NetworkPlayer_t1243528291  L_0 = V_0;
		V_1 = L_0;
		goto IL_0015;
	}

IL_0015:
	{
		NetworkPlayer_t1243528291  L_1 = V_1;
		return L_1;
	}
}
extern "C"  NetworkPlayer_t1243528291  NetworkViewID_get_owner_m1900957708_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_get_owner_m1900957708(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkViewID::ToString()
extern "C"  String_t* NetworkViewID_ToString_m3348378544 (NetworkViewID_t3942988548 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = NetworkViewID_Internal_GetString_m403971590(NULL /*static, unused*/, (*(NetworkViewID_t3942988548 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
extern "C"  String_t* NetworkViewID_ToString_m3348378544_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_ToString_m3348378544(_thisAdjusted, method);
}
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m197157284 (Object_t1021602117 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t1021602117 * Object_Internal_CloneSingle_m260620116 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_Internal_CloneSingle_m260620116_ftn) (Object_t1021602117 *);
	static Object_Internal_CloneSingle_m260620116_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m260620116_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	return _il2cpp_icall_func(___data0);
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t1021602117 * Object_Internal_CloneSingleWithParent_m665572246 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, bool ___worldPositionStays2, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_Internal_CloneSingleWithParent_m665572246_ftn) (Object_t1021602117 *, Transform_t3275118058 *, bool);
	static Object_Internal_CloneSingleWithParent_m665572246_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingleWithParent_m665572246_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)");
	return _il2cpp_icall_func(___data0, ___parent1, ___worldPositionStays2);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Internal_InstantiateSingle_m2776302597_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Internal_InstantiateSingle_m2776302597 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingle_m2776302597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250(NULL /*static, unused*/, L_0, (&___pos1), (&___rot2), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		Object_t1021602117 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1021602117 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Vector3_t2243707580 * ___pos1, Quaternion_t4030073918 * ___rot2, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250_ftn) (Object_t1021602117 *, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data0, ___pos1, ___rot2);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Internal_InstantiateSingleWithParent_m509082884_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Internal_InstantiateSingleWithParent_m509082884 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, Vector3_t2243707580  ___pos2, Quaternion_t4030073918  ___rot3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingleWithParent_m509082884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___data0;
		Transform_t3275118058 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_2 = Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849(NULL /*static, unused*/, L_0, L_1, (&___pos2), (&___rot3), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		Object_t1021602117 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1021602117 * Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, Vector3_t2243707580 * ___pos2, Quaternion_t4030073918 * ___rot3, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849_ftn) (Object_t1021602117 *, Transform_t3275118058 *, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data0, ___parent1, ___pos2, ___rot3);
}
// System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
extern "C"  int32_t Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561_ftn) ();
	static Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Object::EnsureRunningOnMainThread()
extern "C"  void Object_EnsureRunningOnMainThread_m3042842193 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef void (*Object_EnsureRunningOnMainThread_m3042842193_ftn) (Object_t1021602117 *);
	static Object_EnsureRunningOnMainThread_m3042842193_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_EnsureRunningOnMainThread_m3042842193_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::EnsureRunningOnMainThread()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m4279412553 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_Destroy_m4279412553_ftn) (Object_t1021602117 *, float);
	static Object_Destroy_m4279412553_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m4279412553_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Destroy_m4145850038_MetadataUsageId;
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Destroy_m4145850038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t1021602117 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4279412553(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m3563317232 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, bool ___allowDestroyingAssets1, const MethodInfo* method)
{
	typedef void (*Object_DestroyImmediate_m3563317232_ftn) (Object_t1021602117 *, bool);
	static Object_DestroyImmediate_m3563317232_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m3563317232_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj0, ___allowDestroyingAssets1);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_DestroyImmediate_m95027445_MetadataUsageId;
extern "C"  void Object_DestroyImmediate_m95027445 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyImmediate_m95027445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Object_t1021602117 * L_0 = ___obj0;
		bool L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3563317232(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindObjectsOfType_m2121813744 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t4217747464* (*Object_FindObjectsOfType_m2121813744_ftn) (Type_t *);
	static Object_FindObjectsOfType_m2121813744_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m2121813744_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m2079638459 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_get_name_m2079638459_ftn) (Object_t1021602117 *);
	static Object_get_name_m2079638459_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m2079638459_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m4157836998 (Object_t1021602117 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_name_m4157836998_ftn) (Object_t1021602117 *, String_t*);
	static Object_set_name_m4157836998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m4157836998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m2330762974 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___target0, const MethodInfo* method)
{
	typedef void (*Object_DontDestroyOnLoad_m2330762974_ftn) (Object_t1021602117 *);
	static Object_DontDestroyOnLoad_m2330762974_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m2330762974_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target0);
}
// UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
extern "C"  int32_t Object_get_hideFlags_m4158950869 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef int32_t (*Object_get_hideFlags_m4158950869_ftn) (Object_t1021602117 *);
	static Object_get_hideFlags_m4158950869_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_hideFlags_m4158950869_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_hideFlags()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m2204253440 (Object_t1021602117 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_hideFlags_m2204253440_ftn) (Object_t1021602117 *, int32_t);
	static Object_set_hideFlags_m2204253440_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m2204253440_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
extern "C"  void Object_DestroyObject_m282495858 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_DestroyObject_m282495858_ftn) (Object_t1021602117 *, float);
	static Object_DestroyObject_m282495858_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyObject_m282495858_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_DestroyObject_m2343493981_MetadataUsageId;
extern "C"  void Object_DestroyObject_m2343493981 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyObject_m2343493981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t1021602117 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyObject_m282495858(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindSceneObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindSceneObjectsOfType_m1833688338 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t4217747464* (*Object_FindSceneObjectsOfType_m1833688338_ftn) (Type_t *);
	static Object_FindSceneObjectsOfType_m1833688338_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindSceneObjectsOfType_m1833688338_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindSceneObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindObjectsOfTypeIncludingAssets_m3988851426 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t4217747464* (*Object_FindObjectsOfTypeIncludingAssets_m3988851426_ftn) (Type_t *);
	static Object_FindObjectsOfTypeIncludingAssets_m3988851426_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfTypeIncludingAssets_m3988851426_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::ToString()
extern "C"  String_t* Object_ToString_m1947404527 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_ToString_m1947404527_ftn) (Object_t1021602117 *);
	static Object_ToString_m1947404527_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m1947404527_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)
extern "C"  bool Object_DoesObjectWithInstanceIDExist_m2570795274 (Il2CppObject * __this /* static, unused */, int32_t ___instanceID0, const MethodInfo* method)
{
	typedef bool (*Object_DoesObjectWithInstanceIDExist_m2570795274_ftn) (int32_t);
	static Object_DoesObjectWithInstanceIDExist_m2570795274_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DoesObjectWithInstanceIDExist_m2570795274_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)");
	return _il2cpp_icall_func(___instanceID0);
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_GetInstanceID_m1920497914_MetadataUsageId;
extern "C"  int32_t Object_GetInstanceID_m1920497914 (Object_t1021602117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_GetInstanceID_m1920497914_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_0();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		V_0 = 0;
		goto IL_0056;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		int32_t L_3 = ((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->get_OffsetOfInstanceIDInCPlusPlusObject_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		int32_t L_4 = Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->set_OffsetOfInstanceIDInCPlusPlusObject_1(L_4);
	}

IL_0032:
	{
		IntPtr_t* L_5 = __this->get_address_of_m_CachedPtr_0();
		int64_t L_6 = IntPtr_ToInt64_m39971741(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		int32_t L_7 = ((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->get_OffsetOfInstanceIDInCPlusPlusObject_1();
		IntPtr_t L_8;
		memset(&L_8, 0, sizeof(L_8));
		IntPtr__ctor_m3803259710(&L_8, ((int64_t)((int64_t)L_6+(int64_t)(((int64_t)((int64_t)L_7))))), /*hidden argument*/NULL);
		void* L_9 = IntPtr_op_Explicit_m1073656736(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_0 = (*((int32_t*)L_9));
		goto IL_0056;
	}

IL_0056:
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m3431642059 (Object_t1021602117 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Object_GetHashCode_m1715190285(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Equals_m4029628913_MetadataUsageId;
extern "C"  bool Object_Equals_m4029628913 (Object_t1021602117 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Equals_m4029628913_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	bool V_1 = false;
	{
		Il2CppObject * L_0 = ___other0;
		V_0 = ((Object_t1021602117 *)IsInstClass(L_0, Object_t1021602117_il2cpp_TypeInfo_var));
		Object_t1021602117 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = ___other0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_4 = ___other0;
		if (((Object_t1021602117 *)IsInstClass(L_4, Object_t1021602117_il2cpp_TypeInfo_var)))
		{
			goto IL_002c;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0039;
	}

IL_002c:
	{
		Object_t1021602117 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, __this, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0039;
	}

IL_0039:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_op_Implicit_m2856731593_MetadataUsageId;
extern "C"  bool Object_op_Implicit_m2856731593 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___exists0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Implicit_m2856731593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1021602117 * L_0 = ___exists0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_CompareBaseObjects_m3953996214_MetadataUsageId;
extern "C"  bool Object_CompareBaseObjects_m3953996214 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___lhs0, Object_t1021602117 * ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CompareBaseObjects_m3953996214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		Object_t1021602117 * L_0 = ___lhs0;
		V_0 = (bool)((((Il2CppObject*)(Object_t1021602117 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		Object_t1021602117 * L_1 = ___rhs1;
		V_1 = (bool)((((Il2CppObject*)(Object_t1021602117 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0055;
	}

IL_001e:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		Object_t1021602117 * L_5 = ___lhs0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_IsNativeObjectAlive_m4056217615(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0033:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		Object_t1021602117 * L_8 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_IsNativeObjectAlive_m4056217615(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0048:
	{
		Object_t1021602117 * L_10 = ___lhs0;
		Object_t1021602117 * L_11 = ___rhs1;
		bool L_12 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		goto IL_0055;
	}

IL_0055:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_IsNativeObjectAlive_m4056217615_MetadataUsageId;
extern "C"  bool Object_IsNativeObjectAlive_m4056217615 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_IsNativeObjectAlive_m4056217615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1021602117 * L_0 = ___o0;
		NullCheck(L_0);
		IntPtr_t L_1 = Object_GetCachedPtr_m943750213(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_3 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  IntPtr_t Object_GetCachedPtr_m943750213 (Object_t1021602117 * __this, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		IntPtr_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ScriptableObject_t1975622470_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral444318565;
extern Il2CppCodeGenString* _stringLiteral1912870611;
extern const uint32_t Object_Instantiate_m938141395_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m938141395 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m938141395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_0, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_1 = ___original0;
		if (!((ScriptableObject_t1975622470 *)IsInstClass(L_1, ScriptableObject_t1975622470_il2cpp_TypeInfo_var)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, _stringLiteral1912870611, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Object_t1021602117 * L_3 = ___original0;
		Vector3_t2243707580  L_4 = ___position1;
		Quaternion_t4030073918  L_5 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_6 = Object_Internal_InstantiateSingle_m2776302597(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0030;
	}

IL_0030:
	{
		Object_t1021602117 * L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral444318565;
extern const uint32_t Object_Instantiate_m2160322936_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m2160322936 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, Transform_t3275118058 * ___parent3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2160322936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Transform_t3275118058 * L_0 = ___parent3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Object_t1021602117 * L_2 = ___original0;
		Vector3_t2243707580  L_3 = ___position1;
		Quaternion_t4030073918  L_4 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_5 = Object_Internal_InstantiateSingle_m2776302597(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0035;
	}

IL_001b:
	{
		Object_t1021602117 * L_6 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_6, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_7 = ___original0;
		Transform_t3275118058 * L_8 = ___parent3;
		Vector3_t2243707580  L_9 = ___position1;
		Quaternion_t4030073918  L_10 = ___rotation2;
		Object_t1021602117 * L_11 = Object_Internal_InstantiateSingleWithParent_m509082884(NULL /*static, unused*/, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_0035;
	}

IL_0035:
	{
		Object_t1021602117 * L_12 = V_0;
		return L_12;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral444318565;
extern const uint32_t Object_Instantiate_m2439155489_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m2439155489 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2439155489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_0, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_1 = ___original0;
		Object_t1021602117 * L_2 = Object_Internal_CloneSingle_m260620116(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Object_t1021602117 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Instantiate_m2177117080_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m2177117080 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Transform_t3275118058 * ___parent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2177117080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___original0;
		Transform_t3275118058 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_2 = Object_Instantiate_m2489341053(NULL /*static, unused*/, L_0, L_1, (bool)0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000f;
	}

IL_000f:
	{
		Object_t1021602117 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral444318565;
extern const uint32_t Object_Instantiate_m2489341053_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m2489341053 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Transform_t3275118058 * ___parent1, bool ___instantiateInWorldSpace2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2489341053_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Transform_t3275118058 * L_0 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Object_t1021602117 * L_2 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_3 = Object_Internal_CloneSingle_m260620116(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0032;
	}

IL_0019:
	{
		Object_t1021602117 * L_4 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_4, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_5 = ___original0;
		Transform_t3275118058 * L_6 = ___parent1;
		bool L_7 = ___instantiateInWorldSpace2;
		Object_t1021602117 * L_8 = Object_Internal_CloneSingleWithParent_m665572246(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0032;
	}

IL_0032:
	{
		Object_t1021602117 * L_9 = V_0;
		return L_9;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Object_CheckNullArgument_m1711119106_MetadataUsageId;
extern "C"  void Object_CheckNullArgument_m1711119106 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CheckNullArgument_m1711119106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		String_t* L_1 = ___message1;
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_000e:
	{
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectOfType_m2330404063_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_FindObjectOfType_m2330404063 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectOfType_m2330404063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t4217747464* V_0 = NULL;
	Object_t1021602117 * V_1 = NULL;
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_1 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t4217747464* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		ObjectU5BU5D_t4217747464* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = 0;
		Object_t1021602117 * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		goto IL_0021;
	}

IL_001a:
	{
		V_1 = (Object_t1021602117 *)NULL;
		goto IL_0021;
	}

IL_0021:
	{
		Object_t1021602117 * L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_op_Equality_m3764089466_MetadataUsageId;
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___x0, Object_t1021602117 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Equality_m3764089466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1021602117 * L_0 = ___x0;
		Object_t1021602117 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_op_Inequality_m2402264703_MetadataUsageId;
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___x0, Object_t1021602117 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Inequality_m2402264703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1021602117 * L_0 = ___x0;
		Object_t1021602117 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Object::.cctor()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object__cctor_m2991092887_MetadataUsageId;
extern "C"  void Object__cctor_m2991092887 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object__cctor_m2991092887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->set_OffsetOfInstanceIDInCPlusPlusObject_1((-1));
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_pinvoke(const Object_t1021602117& unmarshaled, Object_t1021602117_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void Object_t1021602117_marshal_pinvoke_back(const Object_t1021602117_marshaled_pinvoke& marshaled, Object_t1021602117& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_pinvoke_cleanup(Object_t1021602117_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_com(const Object_t1021602117& unmarshaled, Object_t1021602117_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void Object_t1021602117_marshal_com_back(const Object_t1021602117_marshaled_com& marshaled, Object_t1021602117& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_com_cleanup(Object_t1021602117_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.ParticleSystem::get_isStopped()
extern "C"  bool ParticleSystem_get_isStopped_m700577998 (ParticleSystem_t3394631041 * __this, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_get_isStopped_m700577998_ftn) (ParticleSystem_t3394631041 *);
	static ParticleSystem_get_isStopped_m700577998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isStopped_m700577998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isStopped()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.ParticleSystem::get_isPaused()
extern "C"  bool ParticleSystem_get_isPaused_m3736903421 (ParticleSystem_t3394631041 * __this, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_get_isPaused_m3736903421_ftn) (ParticleSystem_t3394631041 *);
	static ParticleSystem_get_isPaused_m3736903421_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isPaused_m3736903421_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isPaused()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_maxParticles(System.Int32)
extern "C"  void ParticleSystem_set_maxParticles_m1689482563 (ParticleSystem_t3394631041 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_maxParticles_m1689482563_ftn) (ParticleSystem_t3394631041 *, int32_t);
	static ParticleSystem_set_maxParticles_m1689482563_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_maxParticles_m1689482563_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_maxParticles(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
extern "C"  MainModule_t6751348  ParticleSystem_get_main_m1338387869 (ParticleSystem_t3394631041 * __this, const MethodInfo* method)
{
	MainModule_t6751348  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MainModule_t6751348  L_0;
		memset(&L_0, 0, sizeof(L_0));
		MainModule__ctor_m4099059742(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		MainModule_t6751348  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
extern "C"  EmissionModule_t2748003162  ParticleSystem_get_emission_m3968992617 (ParticleSystem_t3394631041 * __this, const MethodInfo* method)
{
	EmissionModule_t2748003162  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		EmissionModule_t2748003162  L_0;
		memset(&L_0, 0, sizeof(L_0));
		EmissionModule__ctor_m1076689768(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		EmissionModule_t2748003162  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
extern "C"  void ParticleSystem_SetParticles_m3035584975 (ParticleSystem_t3394631041 * __this, ParticleU5BU5D_t574222242* ___particles0, int32_t ___size1, const MethodInfo* method)
{
	typedef void (*ParticleSystem_SetParticles_m3035584975_ftn) (ParticleSystem_t3394631041 *, ParticleU5BU5D_t574222242*, int32_t);
	static ParticleSystem_SetParticles_m3035584975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_SetParticles_m3035584975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)");
	_il2cpp_icall_func(__this, ___particles0, ___size1);
}
// System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[])
extern "C"  int32_t ParticleSystem_GetParticles_m1903763264 (ParticleSystem_t3394631041 * __this, ParticleU5BU5D_t574222242* ___particles0, const MethodInfo* method)
{
	typedef int32_t (*ParticleSystem_GetParticles_m1903763264_ftn) (ParticleSystem_t3394631041 *, ParticleU5BU5D_t574222242*);
	static ParticleSystem_GetParticles_m1903763264_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetParticles_m1903763264_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[])");
	return _il2cpp_icall_func(__this, ___particles0);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_Play(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Play_m2372735108 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___self0, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_Play_m2372735108_ftn) (ParticleSystem_t3394631041 *);
	static ParticleSystem_Internal_Play_m2372735108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Play_m2372735108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Play(UnityEngine.ParticleSystem)");
	return _il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_Stop(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemStopBehavior)
extern "C"  bool ParticleSystem_Internal_Stop_m3380666254 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___self0, int32_t ___stopBehavior1, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_Stop_m3380666254_ftn) (ParticleSystem_t3394631041 *, int32_t);
	static ParticleSystem_Internal_Stop_m3380666254_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Stop_m3380666254_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Stop(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemStopBehavior)");
	return _il2cpp_icall_func(___self0, ___stopBehavior1);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_IsAlive(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_IsAlive_m1667610959 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___self0, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_IsAlive_m1667610959_ftn) (ParticleSystem_t3394631041 *);
	static ParticleSystem_Internal_IsAlive_m1667610959_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_IsAlive_m1667610959_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_IsAlive(UnityEngine.ParticleSystem)");
	return _il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.ParticleSystem::Play()
extern "C"  void ParticleSystem_Play_m4171585816 (ParticleSystem_t3394631041 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		ParticleSystem_Play_m1705837075(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern Il2CppClass* ParticleSystem_t3394631041_il2cpp_TypeInfo_var;
extern Il2CppClass* IteratorDelegate_t2419492168_il2cpp_TypeInfo_var;
extern const MethodInfo* ParticleSystem_U3CPlayU3Em__0_m4250446697_MethodInfo_var;
extern const uint32_t ParticleSystem_Play_m1705837075_MetadataUsageId;
extern "C"  void ParticleSystem_Play_m1705837075 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_Play_m1705837075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool G_B2_0 = false;
	ParticleSystem_t3394631041 * G_B2_1 = NULL;
	bool G_B1_0 = false;
	ParticleSystem_t3394631041 * G_B1_1 = NULL;
	{
		bool L_0 = ___withChildren0;
		IteratorDelegate_t2419492168 * L_1 = ((ParticleSystem_t3394631041_StaticFields*)ParticleSystem_t3394631041_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001b;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ParticleSystem_U3CPlayU3Em__0_m4250446697_MethodInfo_var);
		IteratorDelegate_t2419492168 * L_3 = (IteratorDelegate_t2419492168 *)il2cpp_codegen_object_new(IteratorDelegate_t2419492168_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m3692393942(L_3, NULL, L_2, /*hidden argument*/NULL);
		((ParticleSystem_t3394631041_StaticFields*)ParticleSystem_t3394631041_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_2(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001b:
	{
		IteratorDelegate_t2419492168 * L_4 = ((ParticleSystem_t3394631041_StaticFields*)ParticleSystem_t3394631041_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		NullCheck(G_B2_1);
		ParticleSystem_IterateParticleSystems_m1240416587(G_B2_1, G_B2_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean)
extern "C"  void ParticleSystem_Stop_m1901765691 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		bool L_0 = ___withChildren0;
		int32_t L_1 = V_0;
		ParticleSystem_Stop_m3810114649(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop()
extern "C"  void ParticleSystem_Stop_m941760450 (ParticleSystem_t3394631041 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 1;
		V_1 = (bool)1;
		bool L_0 = V_1;
		int32_t L_1 = V_0;
		ParticleSystem_Stop_m3810114649(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern Il2CppClass* U3CStopU3Ec__AnonStorey1_t1810852071_il2cpp_TypeInfo_var;
extern Il2CppClass* IteratorDelegate_t2419492168_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CStopU3Ec__AnonStorey1_U3CU3Em__0_m2516318650_MethodInfo_var;
extern const uint32_t ParticleSystem_Stop_m3810114649_MetadataUsageId;
extern "C"  void ParticleSystem_Stop_m3810114649 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_Stop_m3810114649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStopU3Ec__AnonStorey1_t1810852071 * V_0 = NULL;
	{
		U3CStopU3Ec__AnonStorey1_t1810852071 * L_0 = (U3CStopU3Ec__AnonStorey1_t1810852071 *)il2cpp_codegen_object_new(U3CStopU3Ec__AnonStorey1_t1810852071_il2cpp_TypeInfo_var);
		U3CStopU3Ec__AnonStorey1__ctor_m1832369351(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStopU3Ec__AnonStorey1_t1810852071 * L_1 = V_0;
		int32_t L_2 = ___stopBehavior1;
		NullCheck(L_1);
		L_1->set_stopBehavior_0(L_2);
		bool L_3 = ___withChildren0;
		U3CStopU3Ec__AnonStorey1_t1810852071 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CStopU3Ec__AnonStorey1_U3CU3Em__0_m2516318650_MethodInfo_var);
		IteratorDelegate_t2419492168 * L_6 = (IteratorDelegate_t2419492168 *)il2cpp_codegen_object_new(IteratorDelegate_t2419492168_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m3692393942(L_6, L_4, L_5, /*hidden argument*/NULL);
		ParticleSystem_IterateParticleSystems_m1240416587(__this, L_3, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IsAlive()
extern "C"  bool ParticleSystem_IsAlive_m2418268213 (ParticleSystem_t3394631041 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		bool L_1 = ParticleSystem_IsAlive_m2793794644(__this, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_2 = V_1;
		return L_2;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
extern Il2CppClass* ParticleSystem_t3394631041_il2cpp_TypeInfo_var;
extern Il2CppClass* IteratorDelegate_t2419492168_il2cpp_TypeInfo_var;
extern const MethodInfo* ParticleSystem_U3CIsAliveU3Em__3_m4195117235_MethodInfo_var;
extern const uint32_t ParticleSystem_IsAlive_m2793794644_MetadataUsageId;
extern "C"  bool ParticleSystem_IsAlive_m2793794644 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_IsAlive_m2793794644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool G_B2_0 = false;
	ParticleSystem_t3394631041 * G_B2_1 = NULL;
	bool G_B1_0 = false;
	ParticleSystem_t3394631041 * G_B1_1 = NULL;
	{
		bool L_0 = ___withChildren0;
		IteratorDelegate_t2419492168 * L_1 = ((ParticleSystem_t3394631041_StaticFields*)ParticleSystem_t3394631041_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_5();
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001b;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ParticleSystem_U3CIsAliveU3Em__3_m4195117235_MethodInfo_var);
		IteratorDelegate_t2419492168 * L_3 = (IteratorDelegate_t2419492168 *)il2cpp_codegen_object_new(IteratorDelegate_t2419492168_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m3692393942(L_3, NULL, L_2, /*hidden argument*/NULL);
		((ParticleSystem_t3394631041_StaticFields*)ParticleSystem_t3394631041_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_5(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001b:
	{
		IteratorDelegate_t2419492168 * L_4 = ((ParticleSystem_t3394631041_StaticFields*)ParticleSystem_t3394631041_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_5();
		NullCheck(G_B2_1);
		bool L_5 = ParticleSystem_IterateParticleSystems_m1240416587(G_B2_1, G_B2_0, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_002b;
	}

IL_002b:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IterateParticleSystems(System.Boolean,UnityEngine.ParticleSystem/IteratorDelegate)
extern "C"  bool ParticleSystem_IterateParticleSystems_m1240416587 (ParticleSystem_t3394631041 * __this, bool ___recurse0, IteratorDelegate_t2419492168 * ___func1, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		IteratorDelegate_t2419492168 * L_0 = ___func1;
		NullCheck(L_0);
		bool L_1 = IteratorDelegate_Invoke_m3389138368(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = ___recurse0;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		bool L_3 = V_0;
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		IteratorDelegate_t2419492168 * L_5 = ___func1;
		bool L_6 = ParticleSystem_IterateParticleSystemsRecursive_m3260878897(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = (bool)((int32_t)((int32_t)L_3|(int32_t)L_6));
	}

IL_001e:
	{
		bool L_7 = V_0;
		V_1 = L_7;
		goto IL_0025;
	}

IL_0025:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IterateParticleSystemsRecursive(UnityEngine.Transform,UnityEngine.ParticleSystem/IteratorDelegate)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParticleSystem_t3394631041_m2067134504_MethodInfo_var;
extern const uint32_t ParticleSystem_IterateParticleSystemsRecursive_m3260878897_MetadataUsageId;
extern "C"  bool ParticleSystem_IterateParticleSystemsRecursive_m3260878897 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___transform0, IteratorDelegate_t2419492168 * ___func1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_IterateParticleSystemsRecursive_m3260878897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Transform_t3275118058 * V_3 = NULL;
	ParticleSystem_t3394631041 * V_4 = NULL;
	bool V_5 = false;
	{
		V_0 = (bool)0;
		Transform_t3275118058 * L_0 = ___transform0;
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m881385315(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0057;
	}

IL_0011:
	{
		Transform_t3275118058 * L_2 = ___transform0;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		Transform_t3275118058 * L_4 = Transform_GetChild_m3838588184(L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		Transform_t3275118058 * L_5 = V_3;
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		ParticleSystem_t3394631041 * L_7 = GameObject_GetComponent_TisParticleSystem_t3394631041_m2067134504(L_6, /*hidden argument*/GameObject_GetComponent_TisParticleSystem_t3394631041_m2067134504_MethodInfo_var);
		V_4 = L_7;
		ParticleSystem_t3394631041 * L_8 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_8, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0052;
		}
	}
	{
		IteratorDelegate_t2419492168 * L_10 = ___func1;
		ParticleSystem_t3394631041 * L_11 = V_4;
		NullCheck(L_10);
		bool L_12 = IteratorDelegate_Invoke_m3389138368(L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		bool L_13 = V_0;
		if (!L_13)
		{
			goto IL_0049;
		}
	}
	{
		goto IL_005e;
	}

IL_0049:
	{
		Transform_t3275118058 * L_14 = V_3;
		IteratorDelegate_t2419492168 * L_15 = ___func1;
		ParticleSystem_IterateParticleSystemsRecursive_m3260878897(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
	}

IL_0052:
	{
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_17 = V_2;
		int32_t L_18 = V_1;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0011;
		}
	}

IL_005e:
	{
		bool L_19 = V_0;
		V_5 = L_19;
		goto IL_0066;
	}

IL_0066:
	{
		bool L_20 = V_5;
		return L_20;
	}
}
// System.Boolean UnityEngine.ParticleSystem::<Play>m__0(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CPlayU3Em__0_m4250446697 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t3394631041 * L_0 = ___ps0;
		bool L_1 = ParticleSystem_Internal_Play_m2372735108(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.ParticleSystem::<IsAlive>m__3(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CIsAliveU3Em__3_m4195117235 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t3394631041 * L_0 = ___ps0;
		bool L_1 = ParticleSystem_Internal_IsAlive_m1667610959(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem/<Stop>c__AnonStorey1::.ctor()
extern "C"  void U3CStopU3Ec__AnonStorey1__ctor_m1832369351 (U3CStopU3Ec__AnonStorey1_t1810852071 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem/<Stop>c__AnonStorey1::<>m__0(UnityEngine.ParticleSystem)
extern "C"  bool U3CStopU3Ec__AnonStorey1_U3CU3Em__0_m2516318650 (U3CStopU3Ec__AnonStorey1_t1810852071 * __this, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t3394631041 * L_0 = ___ps0;
		int32_t L_1 = __this->get_stopBehavior_0();
		bool L_2 = ParticleSystem_Internal_Stop_m3380666254(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.ParticleSystem/Burst::set_time(System.Single)
extern "C"  void Burst_set_time_m2249016428 (Burst_t208217445 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Time_0(L_0);
		return;
	}
}
extern "C"  void Burst_set_time_m2249016428_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Burst_t208217445 * _thisAdjusted = reinterpret_cast<Burst_t208217445 *>(__this + 1);
	Burst_set_time_m2249016428(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Burst::set_minCount(System.Int16)
extern "C"  void Burst_set_minCount_m2527778498 (Burst_t208217445 * __this, int16_t ___value0, const MethodInfo* method)
{
	{
		int16_t L_0 = ___value0;
		__this->set_m_MinCount_1(L_0);
		return;
	}
}
extern "C"  void Burst_set_minCount_m2527778498_AdjustorThunk (Il2CppObject * __this, int16_t ___value0, const MethodInfo* method)
{
	Burst_t208217445 * _thisAdjusted = reinterpret_cast<Burst_t208217445 *>(__this + 1);
	Burst_set_minCount_m2527778498(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Burst::set_maxCount(System.Int16)
extern "C"  void Burst_set_maxCount_m3745844044 (Burst_t208217445 * __this, int16_t ___value0, const MethodInfo* method)
{
	{
		int16_t L_0 = ___value0;
		__this->set_m_MaxCount_2(L_0);
		return;
	}
}
extern "C"  void Burst_set_maxCount_m3745844044_AdjustorThunk (Il2CppObject * __this, int16_t ___value0, const MethodInfo* method)
{
	Burst_t208217445 * _thisAdjusted = reinterpret_cast<Burst_t208217445 *>(__this + 1);
	Burst_set_maxCount_m3745844044(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void EmissionModule__ctor_m1076689768 (EmissionModule_t2748003162 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void EmissionModule__ctor_m1076689768_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method)
{
	EmissionModule_t2748003162 * _thisAdjusted = reinterpret_cast<EmissionModule_t2748003162 *>(__this + 1);
	EmissionModule__ctor_m1076689768(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetBursts(UnityEngine.ParticleSystem/Burst[],System.Int32)
extern "C"  void EmissionModule_SetBursts_m1290967351 (EmissionModule_t2748003162 * __this, BurstU5BU5D_t3091130216* ___bursts0, int32_t ___size1, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		BurstU5BU5D_t3091130216* L_1 = ___bursts0;
		int32_t L_2 = ___size1;
		EmissionModule_SetBursts_m3041577753(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void EmissionModule_SetBursts_m1290967351_AdjustorThunk (Il2CppObject * __this, BurstU5BU5D_t3091130216* ___bursts0, int32_t ___size1, const MethodInfo* method)
{
	EmissionModule_t2748003162 * _thisAdjusted = reinterpret_cast<EmissionModule_t2748003162 *>(__this + 1);
	EmissionModule_SetBursts_m1290967351(_thisAdjusted, ___bursts0, ___size1, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetBursts(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/Burst[],System.Int32)
extern "C"  void EmissionModule_SetBursts_m3041577753 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, BurstU5BU5D_t3091130216* ___bursts1, int32_t ___size2, const MethodInfo* method)
{
	typedef void (*EmissionModule_SetBursts_m3041577753_ftn) (ParticleSystem_t3394631041 *, BurstU5BU5D_t3091130216*, int32_t);
	static EmissionModule_SetBursts_m3041577753_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_SetBursts_m3041577753_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::SetBursts(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/Burst[],System.Int32)");
	_il2cpp_icall_func(___system0, ___bursts1, ___size2);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t2748003162_marshal_pinvoke(const EmissionModule_t2748003162& unmarshaled, EmissionModule_t2748003162_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void EmissionModule_t2748003162_marshal_pinvoke_back(const EmissionModule_t2748003162_marshaled_pinvoke& marshaled, EmissionModule_t2748003162& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t2748003162_marshal_pinvoke_cleanup(EmissionModule_t2748003162_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t2748003162_marshal_com(const EmissionModule_t2748003162& unmarshaled, EmissionModule_t2748003162_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void EmissionModule_t2748003162_marshal_com_back(const EmissionModule_t2748003162_marshaled_com& marshaled, EmissionModule_t2748003162& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t2748003162_marshal_com_cleanup(EmissionModule_t2748003162_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/IteratorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void IteratorDelegate__ctor_m3692393942 (IteratorDelegate_t2419492168 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean UnityEngine.ParticleSystem/IteratorDelegate::Invoke(UnityEngine.ParticleSystem)
extern "C"  bool IteratorDelegate_Invoke_m3389138368 (IteratorDelegate_t2419492168 * __this, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		IteratorDelegate_Invoke_m3389138368((IteratorDelegate_t2419492168 *)__this->get_prev_9(),___ps0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.ParticleSystem/IteratorDelegate::BeginInvoke(UnityEngine.ParticleSystem,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * IteratorDelegate_BeginInvoke_m1741046139 (IteratorDelegate_t2419492168 * __this, ParticleSystem_t3394631041 * ___ps0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___ps0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean UnityEngine.ParticleSystem/IteratorDelegate::EndInvoke(System.IAsyncResult)
extern "C"  bool IteratorDelegate_EndInvoke_m121475984 (IteratorDelegate_t2419492168 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m4099059742 (MainModule_t6751348 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void MainModule__ctor_m4099059742_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule__ctor_m4099059742(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeedMultiplier(System.Single)
extern "C"  void MainModule_set_startSpeedMultiplier_m1441818946 (MainModule_t6751348 * __this, float ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = ___value0;
		MainModule_SetStartSpeedMultiplier_m1685138411(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_startSpeedMultiplier_m1441818946_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule_set_startSpeedMultiplier_m1441818946(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartSpeedMultiplier(UnityEngine.ParticleSystem,System.Single)
extern "C"  void MainModule_SetStartSpeedMultiplier_m1685138411 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, float ___value1, const MethodInfo* method)
{
	typedef void (*MainModule_SetStartSpeedMultiplier_m1685138411_ftn) (ParticleSystem_t3394631041 *, float);
	static MainModule_SetStartSpeedMultiplier_m1685138411_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetStartSpeedMultiplier_m1685138411_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetStartSpeedMultiplier(UnityEngine.ParticleSystem,System.Single)");
	_il2cpp_icall_func(___system0, ___value1);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_pinvoke(const MainModule_t6751348& unmarshaled, MainModule_t6751348_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t6751348_marshal_pinvoke_back(const MainModule_t6751348_marshaled_pinvoke& marshaled, MainModule_t6751348& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_pinvoke_cleanup(MainModule_t6751348_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_com(const MainModule_t6751348& unmarshaled, MainModule_t6751348_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t6751348_marshal_com_back(const MainModule_t6751348_marshaled_com& marshaled, MainModule_t6751348& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_com_cleanup(MainModule_t6751348_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3)
extern "C"  void Particle_set_position_m3680513126 (Particle_t250075699 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___value0;
		__this->set_m_Position_0(L_0);
		return;
	}
}
extern "C"  void Particle_set_position_m3680513126_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	Particle_t250075699 * _thisAdjusted = reinterpret_cast<Particle_t250075699 *>(__this + 1);
	Particle_set_position_m3680513126(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_remainingLifetime(System.Single)
extern "C"  void Particle_set_remainingLifetime_m1183181356 (Particle_t250075699 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Lifetime_10(L_0);
		return;
	}
}
extern "C"  void Particle_set_remainingLifetime_m1183181356_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Particle_t250075699 * _thisAdjusted = reinterpret_cast<Particle_t250075699 *>(__this + 1);
	Particle_set_remainingLifetime_m1183181356(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32)
extern "C"  void Particle_set_startColor_m3936512348 (Particle_t250075699 * __this, Color32_t874517518  ___value0, const MethodInfo* method)
{
	{
		Color32_t874517518  L_0 = ___value0;
		__this->set_m_StartColor_8(L_0);
		return;
	}
}
extern "C"  void Particle_set_startColor_m3936512348_AdjustorThunk (Il2CppObject * __this, Color32_t874517518  ___value0, const MethodInfo* method)
{
	Particle_t250075699 * _thisAdjusted = reinterpret_cast<Particle_t250075699 *>(__this + 1);
	Particle_set_startColor_m3936512348(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Physics::get_gravity()
extern "C"  Vector3_t2243707580  Physics_get_gravity_m195764919 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Physics_INTERNAL_get_gravity_m3802008392(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)
extern "C"  void Physics_INTERNAL_get_gravity_m3802008392 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Physics_INTERNAL_get_gravity_m3802008392_ftn) (Vector3_t2243707580 *);
	static Physics_INTERNAL_get_gravity_m3802008392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_get_gravity_m3802008392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m2874007225 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m3475924638(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  bool Physics_Raycast_m89212106 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m3475924638(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Physics_Raycast_m2667915561 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		float L_2 = V_2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m3475924638(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		goto IL_001c;
	}

IL_001c:
	{
		bool L_6 = V_3;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m3475924638 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = ___queryTriggerInteraction4;
		bool L_5 = Physics_Internal_RaycastTest_m3442924926(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m1929115794 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		RaycastHit_t87180320 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m2036777053(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0015;
	}

IL_0015:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m2994111303 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		RaycastHit_t87180320 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m2036777053(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_7 = V_2;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Raycast_m4027183840 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		RaycastHit_t87180320 * L_2 = ___hitInfo2;
		float L_3 = V_2;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m2036777053(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		goto IL_001d;
	}

IL_001d:
	{
		bool L_7 = V_3;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m2036777053 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		RaycastHit_t87180320 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = ___queryTriggerInteraction5;
		bool L_6 = Physics_Internal_Raycast_m1160243045(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0014;
	}

IL_0014:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m2691929452 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = ___layerMask2;
		int32_t L_3 = V_0;
		bool L_4 = Physics_Raycast_m1844392139(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single)
extern "C"  bool Physics_Raycast_m780162053 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		bool L_4 = Physics_Raycast_m1844392139(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		bool L_5 = V_2;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray)
extern "C"  bool Physics_Raycast_m2686676054 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = V_2;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		bool L_4 = Physics_Raycast_m1844392139(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		bool L_5 = V_3;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m1844392139 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t2243707580  L_0 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Ray_get_direction_m4059191533((&___ray0), /*hidden argument*/NULL);
		float L_2 = ___maxDistance1;
		int32_t L_3 = ___layerMask2;
		int32_t L_4 = ___queryTriggerInteraction3;
		bool L_5 = Physics_Raycast_m3475924638(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001d;
	}

IL_001d:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m2009151399 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, RaycastHit_t87180320 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Ray_t2469606224  L_0 = ___ray0;
		RaycastHit_t87180320 * L_1 = ___hitInfo1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m233619224(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m2308457076 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, RaycastHit_t87180320 * ___hitInfo1, float ___maxDistance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t2469606224  L_0 = ___ray0;
		RaycastHit_t87180320 * L_1 = ___hitInfo1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m233619224(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Raycast_m2736931691 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, RaycastHit_t87180320 * ___hitInfo1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Ray_t2469606224  L_0 = ___ray0;
		RaycastHit_t87180320 * L_1 = ___hitInfo1;
		float L_2 = V_2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m233619224(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		goto IL_001c;
	}

IL_001c:
	{
		bool L_6 = V_3;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m233619224 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, RaycastHit_t87180320 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t2243707580  L_0 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Ray_get_direction_m4059191533((&___ray0), /*hidden argument*/NULL);
		RaycastHit_t87180320 * L_2 = ___hitInfo1;
		float L_3 = ___maxDistance2;
		int32_t L_4 = ___layerMask3;
		int32_t L_5 = ___queryTriggerInteraction4;
		bool L_6 = Physics_Raycast_m2036777053(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_001f;
	}

IL_001f:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m233036521 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	RaycastHitU5BU5D_t1214023521* V_1 = NULL;
	{
		V_0 = 0;
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = ___layerMask2;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t1214023521* L_4 = Physics_RaycastAll_m410413656(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0012;
	}

IL_0012:
	{
		RaycastHitU5BU5D_t1214023521* L_5 = V_1;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m3928448900 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RaycastHitU5BU5D_t1214023521* V_2 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t1214023521* L_4 = Physics_RaycastAll_m410413656(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		RaycastHitU5BU5D_t1214023521* L_5 = V_2;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m1246652201 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	RaycastHitU5BU5D_t1214023521* V_3 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = V_2;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t1214023521* L_4 = Physics_RaycastAll_m410413656(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		RaycastHitU5BU5D_t1214023521* L_5 = V_3;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m410413656 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	RaycastHitU5BU5D_t1214023521* V_0 = NULL;
	{
		Vector3_t2243707580  L_0 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Ray_get_direction_m4059191533((&___ray0), /*hidden argument*/NULL);
		float L_2 = ___maxDistance1;
		int32_t L_3 = ___layerMask2;
		int32_t L_4 = ___queryTriggerInteraction3;
		RaycastHitU5BU5D_t1214023521* L_5 = Physics_RaycastAll_m3908263591(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001d;
	}

IL_001d:
	{
		RaycastHitU5BU5D_t1214023521* L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m3908263591 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	RaycastHitU5BU5D_t1214023521* V_0 = NULL;
	{
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = ___queryTriggerInteraction4;
		RaycastHitU5BU5D_t1214023521* L_3 = Physics_INTERNAL_CALL_RaycastAll_m2126789092(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		RaycastHitU5BU5D_t1214023521* L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m3256436970 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layermask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	RaycastHitU5BU5D_t1214023521* V_1 = NULL;
	{
		V_0 = 0;
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t1214023521* L_3 = Physics_INTERNAL_CALL_RaycastAll_m2126789092(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0015;
	}

IL_0015:
	{
		RaycastHitU5BU5D_t1214023521* L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m3484190429 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RaycastHitU5BU5D_t1214023521* V_2 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		float L_0 = ___maxDistance2;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t1214023521* L_3 = Physics_INTERNAL_CALL_RaycastAll_m2126789092(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		goto IL_0018;
	}

IL_0018:
	{
		RaycastHitU5BU5D_t1214023521* L_4 = V_2;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m3650851272 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	RaycastHitU5BU5D_t1214023521* V_3 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		float L_0 = V_2;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t1214023521* L_3 = Physics_INTERNAL_CALL_RaycastAll_m2126789092(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		goto IL_001e;
	}

IL_001e:
	{
		RaycastHitU5BU5D_t1214023521* L_4 = V_3;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_INTERNAL_CALL_RaycastAll_m2126789092 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___origin0, Vector3_t2243707580 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	typedef RaycastHitU5BU5D_t1214023521* (*Physics_INTERNAL_CALL_RaycastAll_m2126789092_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_RaycastAll_m2126789092_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_RaycastAll_m2126789092_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___maxDistance2, ___layermask3, ___queryTriggerInteraction4);
}
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32)
extern "C"  bool Physics_Linecast_m1120910627 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___start0, Vector3_t2243707580  ___end1, RaycastHit_t87180320 * ___hitInfo2, int32_t ___layerMask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Vector3_t2243707580  L_0 = ___start0;
		Vector3_t2243707580  L_1 = ___end1;
		RaycastHit_t87180320 * L_2 = ___hitInfo2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Linecast_m1896185200(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Linecast_m100291276 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___start0, Vector3_t2243707580  ___end1, RaycastHit_t87180320 * ___hitInfo2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Vector3_t2243707580  L_0 = ___start0;
		Vector3_t2243707580  L_1 = ___end1;
		RaycastHit_t87180320 * L_2 = ___hitInfo2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Linecast_m1896185200(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Linecast_m1896185200 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___start0, Vector3_t2243707580  ___end1, RaycastHit_t87180320 * ___hitInfo2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Vector3_t2243707580  L_0 = ___end1;
		Vector3_t2243707580  L_1 = ___start0;
		Vector3_t2243707580  L_2 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = ___start0;
		Vector3_t2243707580  L_4 = V_0;
		RaycastHit_t87180320 * L_5 = ___hitInfo2;
		float L_6 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		int32_t L_7 = ___layerMask3;
		int32_t L_8 = ___queryTriggerInteraction4;
		bool L_9 = Physics_Raycast_m2036777053(NULL /*static, unused*/, L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_SphereCast_m2879660272 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, float ___radius1, Vector3_t2243707580  ___direction2, RaycastHit_t87180320 * ___hitInfo3, float ___maxDistance4, int32_t ___layerMask5, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Vector3_t2243707580  L_0 = ___origin0;
		float L_1 = ___radius1;
		Vector3_t2243707580  L_2 = ___direction2;
		RaycastHit_t87180320 * L_3 = ___hitInfo3;
		float L_4 = ___maxDistance4;
		int32_t L_5 = ___layerMask5;
		int32_t L_6 = V_0;
		bool L_7 = Physics_SphereCast_m1575412719(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_SphereCast_m1575412719 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, float ___radius1, Vector3_t2243707580  ___direction2, RaycastHit_t87180320 * ___hitInfo3, float ___maxDistance4, int32_t ___layerMask5, int32_t ___queryTriggerInteraction6, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___origin0;
		float L_2 = ___radius1;
		Vector3_t2243707580  L_3 = ___direction2;
		RaycastHit_t87180320 * L_4 = ___hitInfo3;
		float L_5 = ___maxDistance4;
		int32_t L_6 = ___layerMask5;
		int32_t L_7 = ___queryTriggerInteraction6;
		bool L_8 = Physics_Internal_CapsuleCast_m1833501922(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::CapsuleCastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_CapsuleCastAll_m3327772496 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___point10, Vector3_t2243707580  ___point21, float ___radius2, Vector3_t2243707580  ___direction3, float ___maxDistance4, int32_t ___layermask5, int32_t ___queryTriggerInteraction6, const MethodInfo* method)
{
	RaycastHitU5BU5D_t1214023521* V_0 = NULL;
	{
		float L_0 = ___radius2;
		float L_1 = ___maxDistance4;
		int32_t L_2 = ___layermask5;
		int32_t L_3 = ___queryTriggerInteraction6;
		RaycastHitU5BU5D_t1214023521* L_4 = Physics_INTERNAL_CALL_CapsuleCastAll_m275481563(NULL /*static, unused*/, (&___point10), (&___point21), L_0, (&___direction3), L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0019;
	}

IL_0019:
	{
		RaycastHitU5BU5D_t1214023521* L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::CapsuleCastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_CapsuleCastAll_m1793396948 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___point10, Vector3_t2243707580  ___point21, float ___radius2, Vector3_t2243707580  ___direction3, float ___maxDistance4, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RaycastHitU5BU5D_t1214023521* V_2 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		float L_0 = ___radius2;
		float L_1 = ___maxDistance4;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t1214023521* L_4 = Physics_INTERNAL_CALL_CapsuleCastAll_m275481563(NULL /*static, unused*/, (&___point10), (&___point21), L_0, (&___direction3), L_1, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		goto IL_001c;
	}

IL_001c:
	{
		RaycastHitU5BU5D_t1214023521* L_5 = V_2;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_CapsuleCastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_INTERNAL_CALL_CapsuleCastAll_m275481563 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___point10, Vector3_t2243707580 * ___point21, float ___radius2, Vector3_t2243707580 * ___direction3, float ___maxDistance4, int32_t ___layermask5, int32_t ___queryTriggerInteraction6, const MethodInfo* method)
{
	typedef RaycastHitU5BU5D_t1214023521* (*Physics_INTERNAL_CALL_CapsuleCastAll_m275481563_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, float, Vector3_t2243707580 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_CapsuleCastAll_m275481563_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_CapsuleCastAll_m275481563_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_CapsuleCastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___point10, ___point21, ___radius2, ___direction3, ___maxDistance4, ___layermask5, ___queryTriggerInteraction6);
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::SphereCastAll(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_SphereCastAll_m3328235726 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, float ___radius1, Vector3_t2243707580  ___direction2, float ___maxDistance3, int32_t ___layerMask4, const MethodInfo* method)
{
	int32_t V_0 = 0;
	RaycastHitU5BU5D_t1214023521* V_1 = NULL;
	{
		V_0 = 0;
		Vector3_t2243707580  L_0 = ___origin0;
		float L_1 = ___radius1;
		Vector3_t2243707580  L_2 = ___direction2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = V_0;
		RaycastHitU5BU5D_t1214023521* L_6 = Physics_SphereCastAll_m3346848973(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0015;
	}

IL_0015:
	{
		RaycastHitU5BU5D_t1214023521* L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::SphereCastAll(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_SphereCastAll_m3346848973 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, float ___radius1, Vector3_t2243707580  ___direction2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	RaycastHitU5BU5D_t1214023521* V_0 = NULL;
	{
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___origin0;
		float L_2 = ___radius1;
		Vector3_t2243707580  L_3 = ___direction2;
		float L_4 = ___maxDistance3;
		int32_t L_5 = ___layerMask4;
		int32_t L_6 = ___queryTriggerInteraction5;
		RaycastHitU5BU5D_t1214023521* L_7 = Physics_CapsuleCastAll_m3327772496(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_0015;
	}

IL_0015:
	{
		RaycastHitU5BU5D_t1214023521* L_8 = V_0;
		return L_8;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::SphereCastAll(UnityEngine.Ray,System.Single,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_SphereCastAll_m3441839003 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___radius1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	RaycastHitU5BU5D_t1214023521* V_1 = NULL;
	{
		V_0 = 0;
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = ___radius1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = V_0;
		RaycastHitU5BU5D_t1214023521* L_5 = Physics_SphereCastAll_m2962218208(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0013;
	}

IL_0013:
	{
		RaycastHitU5BU5D_t1214023521* L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::SphereCastAll(UnityEngine.Ray,System.Single,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_SphereCastAll_m2962218208 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___radius1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	RaycastHitU5BU5D_t1214023521* V_0 = NULL;
	{
		Vector3_t2243707580  L_0 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		float L_2 = ___radius1;
		Vector3_t2243707580  L_3 = Ray_get_direction_m4059191533((&___ray0), /*hidden argument*/NULL);
		float L_4 = ___maxDistance2;
		int32_t L_5 = ___layerMask3;
		int32_t L_6 = ___queryTriggerInteraction4;
		RaycastHitU5BU5D_t1214023521* L_7 = Physics_CapsuleCastAll_m3327772496(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_0026;
	}

IL_0026:
	{
		RaycastHitU5BU5D_t1214023521* L_8 = V_0;
		return L_8;
	}
}
// System.Int32 UnityEngine.Physics::OverlapBoxNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Collider[],UnityEngine.Quaternion,System.Int32)
extern "C"  int32_t Physics_OverlapBoxNonAlloc_m4228637865 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___center0, Vector3_t2243707580  ___halfExtents1, ColliderU5BU5D_t462843629* ___results2, Quaternion_t4030073918  ___orientation3, int32_t ___layerMask4, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		ColliderU5BU5D_t462843629* L_0 = ___results2;
		int32_t L_1 = ___layerMask4;
		int32_t L_2 = V_0;
		int32_t L_3 = Physics_INTERNAL_CALL_OverlapBoxNonAlloc_m1906123881(NULL /*static, unused*/, (&___center0), (&___halfExtents1), L_0, (&___orientation3), L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0018;
	}

IL_0018:
	{
		int32_t L_4 = V_1;
		return L_4;
	}
}
// System.Int32 UnityEngine.Physics::INTERNAL_CALL_OverlapBoxNonAlloc(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Collider[],UnityEngine.Quaternion&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  int32_t Physics_INTERNAL_CALL_OverlapBoxNonAlloc_m1906123881 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___center0, Vector3_t2243707580 * ___halfExtents1, ColliderU5BU5D_t462843629* ___results2, Quaternion_t4030073918 * ___orientation3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	typedef int32_t (*Physics_INTERNAL_CALL_OverlapBoxNonAlloc_m1906123881_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, ColliderU5BU5D_t462843629*, Quaternion_t4030073918 *, int32_t, int32_t);
	static Physics_INTERNAL_CALL_OverlapBoxNonAlloc_m1906123881_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_OverlapBoxNonAlloc_m1906123881_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_OverlapBoxNonAlloc(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Collider[],UnityEngine.Quaternion&,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___center0, ___halfExtents1, ___results2, ___orientation3, ___layerMask4, ___queryTriggerInteraction5);
}
// System.Void UnityEngine.Physics::IgnoreCollision(UnityEngine.Collider,UnityEngine.Collider,System.Boolean)
extern "C"  void Physics_IgnoreCollision_m2971794815 (Il2CppObject * __this /* static, unused */, Collider_t3497673348 * ___collider10, Collider_t3497673348 * ___collider21, bool ___ignore2, const MethodInfo* method)
{
	typedef void (*Physics_IgnoreCollision_m2971794815_ftn) (Collider_t3497673348 *, Collider_t3497673348 *, bool);
	static Physics_IgnoreCollision_m2971794815_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_IgnoreCollision_m2971794815_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::IgnoreCollision(UnityEngine.Collider,UnityEngine.Collider,System.Boolean)");
	_il2cpp_icall_func(___collider10, ___collider21, ___ignore2);
}
// System.Void UnityEngine.Physics::IgnoreCollision(UnityEngine.Collider,UnityEngine.Collider)
extern "C"  void Physics_IgnoreCollision_m2197410464 (Il2CppObject * __this /* static, unused */, Collider_t3497673348 * ___collider10, Collider_t3497673348 * ___collider21, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		Collider_t3497673348 * L_0 = ___collider10;
		Collider_t3497673348 * L_1 = ___collider21;
		bool L_2 = V_0;
		Physics_IgnoreCollision_m2971794815(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_Raycast_m1160243045 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	bool V_0 = false;
	{
		RaycastHit_t87180320 * L_0 = ___hitInfo2;
		float L_1 = ___maxDistance3;
		int32_t L_2 = ___layermask4;
		int32_t L_3 = ___queryTriggerInteraction5;
		bool L_4 = Physics_INTERNAL_CALL_Internal_Raycast_m93849932(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_Raycast_m93849932 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___origin0, Vector3_t2243707580 * ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_Raycast_m93849932_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, RaycastHit_t87180320 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_Raycast_m93849932_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_Raycast_m93849932_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___hitInfo2, ___maxDistance3, ___layermask4, ___queryTriggerInteraction5);
}
// System.Boolean UnityEngine.Physics::Internal_CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_CapsuleCast_m1833501922 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___point10, Vector3_t2243707580  ___point21, float ___radius2, Vector3_t2243707580  ___direction3, RaycastHit_t87180320 * ___hitInfo4, float ___maxDistance5, int32_t ___layermask6, int32_t ___queryTriggerInteraction7, const MethodInfo* method)
{
	bool V_0 = false;
	{
		float L_0 = ___radius2;
		RaycastHit_t87180320 * L_1 = ___hitInfo4;
		float L_2 = ___maxDistance5;
		int32_t L_3 = ___layermask6;
		int32_t L_4 = ___queryTriggerInteraction7;
		bool L_5 = Physics_INTERNAL_CALL_Internal_CapsuleCast_m4097719249(NULL /*static, unused*/, (&___point10), (&___point21), L_0, (&___direction3), L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001b;
	}

IL_001b:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_CapsuleCast(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_CapsuleCast_m4097719249 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___point10, Vector3_t2243707580 * ___point21, float ___radius2, Vector3_t2243707580 * ___direction3, RaycastHit_t87180320 * ___hitInfo4, float ___maxDistance5, int32_t ___layermask6, int32_t ___queryTriggerInteraction7, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_CapsuleCast_m4097719249_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, float, Vector3_t2243707580 *, RaycastHit_t87180320 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_CapsuleCast_m4097719249_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_CapsuleCast_m4097719249_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_CapsuleCast(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___point10, ___point21, ___radius2, ___direction3, ___hitInfo4, ___maxDistance5, ___layermask6, ___queryTriggerInteraction7);
}
// System.Boolean UnityEngine.Physics::Internal_RaycastTest(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_RaycastTest_m3442924926 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	bool V_0 = false;
	{
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = ___queryTriggerInteraction4;
		bool L_3 = Physics_INTERNAL_CALL_Internal_RaycastTest_m3440840981(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_RaycastTest_m3440840981 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___origin0, Vector3_t2243707580 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_RaycastTest_m3440840981_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_RaycastTest_m3440840981_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_RaycastTest_m3440840981_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___maxDistance2, ___layermask3, ___queryTriggerInteraction4);
}
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Internal_Raycast_m683685528_MetadataUsageId;
extern "C"  void Physics2D_Internal_Raycast_m683685528 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, RaycastHit2D_t4063908774 * ___raycastHit6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Internal_Raycast_m683685528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___distance2;
		int32_t L_1 = ___layerMask3;
		float L_2 = ___minDepth4;
		float L_3 = ___maxDepth5;
		RaycastHit2D_t4063908774 * L_4 = ___raycastHit6;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Raycast_m3956067819(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_Raycast_m3956067819 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___origin0, Vector2_t2243707579 * ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, RaycastHit2D_t4063908774 * ___raycastHit6, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Raycast_m3956067819_ftn) (Vector2_t2243707579 *, Vector2_t2243707579 *, float, int32_t, float, float, RaycastHit2D_t4063908774 *);
	static Physics2D_INTERNAL_CALL_Internal_Raycast_m3956067819_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Raycast_m3956067819_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___layerMask3, ___minDepth4, ___maxDepth5, ___raycastHit6);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Raycast_m1220041042_MetadataUsageId;
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m1220041042 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m1220041042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	RaycastHit2D_t4063908774  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		V_0 = (std::numeric_limits<float>::infinity());
		Vector2_t2243707579  L_0 = ___origin0;
		Vector2_t2243707579  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = ___layerMask3;
		float L_4 = ___minDepth4;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_6 = Physics2D_Raycast_m2303387255(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		RaycastHit2D_t4063908774  L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Raycast_m122312471_MetadataUsageId;
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m122312471 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m122312471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	RaycastHit2D_t4063908774  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t2243707579  L_0 = ___origin0;
		Vector2_t2243707579  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = ___layerMask3;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_6 = Physics2D_Raycast_m2303387255(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		goto IL_001e;
	}

IL_001e:
	{
		RaycastHit2D_t4063908774  L_7 = V_2;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Raycast_m3913913442_MetadataUsageId;
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m3913913442 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m3913913442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	RaycastHit2D_t4063908774  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		V_2 = ((int32_t)-5);
		Vector2_t2243707579  L_0 = ___origin0;
		Vector2_t2243707579  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = V_2;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_6 = Physics2D_Raycast_m2303387255(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		goto IL_0021;
	}

IL_0021:
	{
		RaycastHit2D_t4063908774  L_7 = V_3;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Raycast_m2560154475_MetadataUsageId;
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m2560154475 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m2560154475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	RaycastHit2D_t4063908774  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		V_2 = ((int32_t)-5);
		V_3 = (std::numeric_limits<float>::infinity());
		Vector2_t2243707579  L_0 = ___origin0;
		Vector2_t2243707579  L_1 = ___direction1;
		float L_2 = V_3;
		int32_t L_3 = V_2;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_6 = Physics2D_Raycast_m2303387255(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_4 = L_6;
		goto IL_0028;
	}

IL_0028:
	{
		RaycastHit2D_t4063908774  L_7 = V_4;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Raycast_m2303387255_MetadataUsageId;
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m2303387255 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m2303387255_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2D_t4063908774  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2D_t4063908774  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector2_t2243707579  L_0 = ___origin0;
		Vector2_t2243707579  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = ___layerMask3;
		float L_4 = ___minDepth4;
		float L_5 = ___maxDepth5;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_Internal_Raycast_m683685528(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t4063908774  L_6 = V_0;
		V_1 = L_6;
		goto IL_0017;
	}

IL_0017:
	{
		RaycastHit2D_t4063908774  L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single,System.Int32)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_GetRayIntersectionAll_m253330691_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_GetRayIntersectionAll_m253330691 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m253330691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2DU5BU5D_t4176517891* V_0 = NULL;
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4176517891* L_2 = Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998(NULL /*static, unused*/, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		RaycastHit2DU5BU5D_t4176517891* L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_GetRayIntersectionAll_m2808325432_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_GetRayIntersectionAll_m2808325432 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___distance1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m2808325432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RaycastHit2DU5BU5D_t4176517891* V_1 = NULL;
	{
		V_0 = ((int32_t)-5);
		float L_0 = ___distance1;
		int32_t L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4176517891* L_2 = Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998(NULL /*static, unused*/, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		RaycastHit2DU5BU5D_t4176517891* L_3 = V_1;
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_GetRayIntersectionAll_m120415839_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_GetRayIntersectionAll_m120415839 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m120415839_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	RaycastHit2DU5BU5D_t4176517891* V_2 = NULL;
	{
		V_0 = ((int32_t)-5);
		V_1 = (std::numeric_limits<float>::infinity());
		float L_0 = V_1;
		int32_t L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4176517891* L_2 = Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998(NULL /*static, unused*/, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		RaycastHit2DU5BU5D_t4176517891* L_3 = V_2;
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998 (Il2CppObject * __this /* static, unused */, Ray_t2469606224 * ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t4176517891* (*Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998_ftn) (Ray_t2469606224 *, float, int32_t);
	static Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___ray0, ___distance1, ___layerMask2);
}
// System.Void UnityEngine.Physics2D::.cctor()
extern Il2CppClass* List_1_t4166282325_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2338710192_MethodInfo_var;
extern const uint32_t Physics2D__cctor_m3532647019_MetadataUsageId;
extern "C"  void Physics2D__cctor_m3532647019 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D__cctor_m3532647019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4166282325 * L_0 = (List_1_t4166282325 *)il2cpp_codegen_object_new(List_1_t4166282325_il2cpp_TypeInfo_var);
		List_1__ctor_m2338710192(L_0, /*hidden argument*/List_1__ctor_m2338710192_MethodInfo_var);
		((Physics2D_t2540166467_StaticFields*)Physics2D_t2540166467_il2cpp_TypeInfo_var->static_fields)->set_m_LastDisabledRigidbody2D_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m3187718367 (Plane_t3727654732 * __this, Vector3_t2243707580  ___inNormal0, Vector3_t2243707580  ___inPoint1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___inNormal0;
		Vector3_t2243707580  L_1 = Vector3_Normalize_m2140428981(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_m_Normal_0(L_1);
		Vector3_t2243707580  L_2 = ___inNormal0;
		Vector3_t2243707580  L_3 = ___inPoint1;
		float L_4 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_m_Distance_1(((-L_4)));
		return;
	}
}
extern "C"  void Plane__ctor_m3187718367_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___inNormal0, Vector3_t2243707580  ___inPoint1, const MethodInfo* method)
{
	Plane_t3727654732 * _thisAdjusted = reinterpret_cast<Plane_t3727654732 *>(__this + 1);
	Plane__ctor_m3187718367(_thisAdjusted, ___inNormal0, ___inPoint1, method);
}
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,System.Single)
extern "C"  void Plane__ctor_m3209791291 (Plane_t3727654732 * __this, Vector3_t2243707580  ___inNormal0, float ___d1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___inNormal0;
		Vector3_t2243707580  L_1 = Vector3_Normalize_m2140428981(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_m_Normal_0(L_1);
		float L_2 = ___d1;
		__this->set_m_Distance_1(L_2);
		return;
	}
}
extern "C"  void Plane__ctor_m3209791291_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___inNormal0, float ___d1, const MethodInfo* method)
{
	Plane_t3727654732 * _thisAdjusted = reinterpret_cast<Plane_t3727654732 *>(__this + 1);
	Plane__ctor_m3209791291(_thisAdjusted, ___inNormal0, ___d1, method);
}
// UnityEngine.Vector3 UnityEngine.Plane::get_normal()
extern "C"  Vector3_t2243707580  Plane_get_normal_m1872443823 (Plane_t3727654732 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Normal_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  Plane_get_normal_m1872443823_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Plane_t3727654732 * _thisAdjusted = reinterpret_cast<Plane_t3727654732 *>(__this + 1);
	return Plane_get_normal_m1872443823(_thisAdjusted, method);
}
// System.Single UnityEngine.Plane::get_distance()
extern "C"  float Plane_get_distance_m1834776091 (Plane_t3727654732 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Distance_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Plane_get_distance_m1834776091_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Plane_t3727654732 * _thisAdjusted = reinterpret_cast<Plane_t3727654732 *>(__this + 1);
	return Plane_get_distance_m1834776091(_thisAdjusted, method);
}
// System.Single UnityEngine.Plane::GetDistanceToPoint(UnityEngine.Vector3)
extern "C"  float Plane_GetDistanceToPoint_m2885837258 (Plane_t3727654732 * __this, Vector3_t2243707580  ___inPt0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Vector3_t2243707580  L_0 = Plane_get_normal_m1872443823(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = ___inPt0;
		float L_2 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Plane_get_distance_m1834776091(__this, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_2+(float)L_3));
		goto IL_001a;
	}

IL_001a:
	{
		float L_4 = V_0;
		return L_4;
	}
}
extern "C"  float Plane_GetDistanceToPoint_m2885837258_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___inPt0, const MethodInfo* method)
{
	Plane_t3727654732 * _thisAdjusted = reinterpret_cast<Plane_t3727654732 *>(__this + 1);
	return Plane_GetDistanceToPoint_m2885837258(_thisAdjusted, ___inPt0, method);
}
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Plane_Raycast_m2870142810_MetadataUsageId;
extern "C"  bool Plane_Raycast_m2870142810 (Plane_t3727654732 * __this, Ray_t2469606224  ___ray0, float* ___enter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Plane_Raycast_m2870142810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		Vector3_t2243707580  L_0 = Ray_get_direction_m4059191533((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Plane_get_normal_m1872443823(__this, /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Plane_get_normal_m1872443823(__this, /*hidden argument*/NULL);
		float L_5 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Plane_get_distance_m1834776091(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)((-L_5))-(float)L_6));
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_7, (0.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004e;
		}
	}
	{
		float* L_9 = ___enter1;
		*((float*)(L_9)) = (float)(0.0f);
		V_2 = (bool)0;
		goto IL_0062;
	}

IL_004e:
	{
		float* L_10 = ___enter1;
		float L_11 = V_1;
		float L_12 = V_0;
		*((float*)(L_10)) = (float)((float)((float)L_11/(float)L_12));
		float* L_13 = ___enter1;
		V_2 = (bool)((((float)(*((float*)L_13))) > ((float)(0.0f)))? 1 : 0);
		goto IL_0062;
	}

IL_0062:
	{
		bool L_14 = V_2;
		return L_14;
	}
}
extern "C"  bool Plane_Raycast_m2870142810_AdjustorThunk (Il2CppObject * __this, Ray_t2469606224  ___ray0, float* ___enter1, const MethodInfo* method)
{
	Plane_t3727654732 * _thisAdjusted = reinterpret_cast<Plane_t3727654732 *>(__this + 1);
	return Plane_Raycast_m2870142810(_thisAdjusted, ___ray0, ___enter1, method);
}
// System.Void UnityEngine.PreferBinarySerialization::.ctor()
extern "C"  void PreferBinarySerialization__ctor_m2043201510 (PreferBinarySerialization_t2472773525 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m3663555848 (PropertyAttribute_t2606999759 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PropertyAttribute::set_order(System.Int32)
extern "C"  void PropertyAttribute_set_order_m2838113430 (PropertyAttribute_t2606999759 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CorderU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.QualitySettings::set_maxQueuedFrames(System.Int32)
extern "C"  void QualitySettings_set_maxQueuedFrames_m3119066574 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_maxQueuedFrames_m3119066574_ftn) (int32_t);
	static QualitySettings_set_maxQueuedFrames_m3119066574_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_maxQueuedFrames_m3119066574_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_maxQueuedFrames(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.QualitySettings::set_vSyncCount(System.Int32)
extern "C"  void QualitySettings_set_vSyncCount_m1045847619 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_vSyncCount_m1045847619_ftn) (int32_t);
	static QualitySettings_set_vSyncCount_m1045847619_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_vSyncCount_m1045847619_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_vSyncCount(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.QualitySettings::get_antiAliasing()
extern "C"  int32_t QualitySettings_get_antiAliasing_m1084683812 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_antiAliasing_m1084683812_ftn) ();
	static QualitySettings_get_antiAliasing_m1084683812_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_antiAliasing_m1084683812_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_antiAliasing()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_antiAliasing(System.Int32)
extern "C"  void QualitySettings_set_antiAliasing_m3640675405 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_antiAliasing_m3640675405_ftn) (int32_t);
	static QualitySettings_set_antiAliasing_m3640675405_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_antiAliasing_m3640675405_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_antiAliasing(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C"  int32_t QualitySettings_get_activeColorSpace_m580875098 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_activeColorSpace_m580875098_ftn) ();
	static QualitySettings_get_activeColorSpace_m580875098_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_activeColorSpace_m580875098_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_activeColorSpace()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m3196903881 (Quaternion_t4030073918 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		float L_3 = ___w3;
		__this->set_w_3(L_3);
		return;
	}
}
extern "C"  void Quaternion__ctor_m3196903881_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	Quaternion__ctor_m3196903881(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_AngleAxis_m2806222563 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t2243707580  ___axis1, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___angle0;
		Quaternion_INTERNAL_CALL_AngleAxis_m3310327005(NULL /*static, unused*/, L_0, (&___axis1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = V_0;
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Quaternion_t4030073918  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_AngleAxis_m3310327005 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t2243707580 * ___axis1, Quaternion_t4030073918 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_AngleAxis_m3310327005_ftn) (float, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_AngleAxis_m3310327005_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_AngleAxis_m3310327005_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___angle0, ___axis1, ___value2);
}
// System.Void UnityEngine.Quaternion::ToAngleAxis(System.Single&,UnityEngine.Vector3&)
extern "C"  void Quaternion_ToAngleAxis_m2980929840 (Quaternion_t4030073918 * __this, float* ___angle0, Vector3_t2243707580 * ___axis1, const MethodInfo* method)
{
	{
		Vector3_t2243707580 * L_0 = ___axis1;
		float* L_1 = ___angle0;
		Quaternion_Internal_ToAxisAngleRad_m1386331342(NULL /*static, unused*/, (*(Quaternion_t4030073918 *)__this), L_0, L_1, /*hidden argument*/NULL);
		float* L_2 = ___angle0;
		float* L_3 = ___angle0;
		*((float*)(L_2)) = (float)((float)((float)(*((float*)L_3))*(float)(57.29578f)));
		return;
	}
}
extern "C"  void Quaternion_ToAngleAxis_m2980929840_AdjustorThunk (Il2CppObject * __this, float* ___angle0, Vector3_t2243707580 * ___axis1, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	Quaternion_ToAngleAxis_m2980929840(_thisAdjusted, ___angle0, ___axis1, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::FromToRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_FromToRotation_m1685306068 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___fromDirection0, Vector3_t2243707580  ___toDirection1, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_FromToRotation_m2839236110(NULL /*static, unused*/, (&___fromDirection0), (&___toDirection1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		V_1 = L_0;
		goto IL_0013;
	}

IL_0013:
	{
		Quaternion_t4030073918  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_FromToRotation_m2839236110 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___fromDirection0, Vector3_t2243707580 * ___toDirection1, Quaternion_t4030073918 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_FromToRotation_m2839236110_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_FromToRotation_m2839236110_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_FromToRotation_m2839236110_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___fromDirection0, ___toDirection1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_LookRotation_m700700634 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___forward0, Vector3_t2243707580  ___upwards1, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_LookRotation_m3606560944(NULL /*static, unused*/, (&___forward0), (&___upwards1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		V_1 = L_0;
		goto IL_0013;
	}

IL_0013:
	{
		Quaternion_t4030073918  L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_LookRotation_m633695927 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___forward0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Quaternion_t4030073918  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Vector3_t2243707580  L_0 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Quaternion_INTERNAL_CALL_LookRotation_m3606560944(NULL /*static, unused*/, (&___forward0), (&V_0), (&V_1), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = V_1;
		V_2 = L_1;
		goto IL_0019;
	}

IL_0019:
	{
		Quaternion_t4030073918  L_2 = V_2;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m3606560944 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___forward0, Vector3_t2243707580 * ___upwards1, Quaternion_t4030073918 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_LookRotation_m3606560944_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_LookRotation_m3606560944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LookRotation_m3606560944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___forward0, ___upwards1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Slerp_m1992855400 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___a0, Quaternion_t4030073918  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_Slerp_m1926970492(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = V_0;
		V_1 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		Quaternion_t4030073918  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Slerp_m1926970492 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___a0, Quaternion_t4030073918 * ___b1, float ___t2, Quaternion_t4030073918 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Slerp_m1926970492_ftn) (Quaternion_t4030073918 *, Quaternion_t4030073918 *, float, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_Slerp_m1926970492_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Slerp_m1926970492_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::SlerpUnclamped(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_SlerpUnclamped_m276819893 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___a0, Quaternion_t4030073918  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_SlerpUnclamped_m2272819665(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = V_0;
		V_1 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		Quaternion_t4030073918  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_SlerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_SlerpUnclamped_m2272819665 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___a0, Quaternion_t4030073918 * ___b1, float ___t2, Quaternion_t4030073918 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_SlerpUnclamped_m2272819665_ftn) (Quaternion_t4030073918 *, Quaternion_t4030073918 *, float, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_SlerpUnclamped_m2272819665_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_SlerpUnclamped_m2272819665_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_SlerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Lerp_m3623055223 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___a0, Quaternion_t4030073918  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_Lerp_m1920404743(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = V_0;
		V_1 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		Quaternion_t4030073918  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Lerp_m1920404743 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___a0, Quaternion_t4030073918 * ___b1, float ___t2, Quaternion_t4030073918 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Lerp_m1920404743_ftn) (Quaternion_t4030073918 *, Quaternion_t4030073918 *, float, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_Lerp_m1920404743_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Lerp_m1920404743_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::RotateTowards(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_RotateTowards_m83980725_MetadataUsageId;
extern "C"  Quaternion_t4030073918  Quaternion_RotateTowards_m83980725 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___from0, Quaternion_t4030073918  ___to1, float ___maxDegreesDelta2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_RotateTowards_m83980725_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		Quaternion_t4030073918  L_0 = ___from0;
		Quaternion_t4030073918  L_1 = ___to1;
		float L_2 = Quaternion_Angle_m1045775740(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		if ((!(((float)L_3) == ((float)(0.0f)))))
		{
			goto IL_001b;
		}
	}
	{
		Quaternion_t4030073918  L_4 = ___to1;
		V_1 = L_4;
		goto IL_0037;
	}

IL_001b:
	{
		float L_5 = ___maxDegreesDelta2;
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Min_m1648492575(NULL /*static, unused*/, (1.0f), ((float)((float)L_5/(float)L_6)), /*hidden argument*/NULL);
		V_2 = L_7;
		Quaternion_t4030073918  L_8 = ___from0;
		Quaternion_t4030073918  L_9 = ___to1;
		float L_10 = V_2;
		Quaternion_t4030073918  L_11 = Quaternion_SlerpUnclamped_m276819893(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		goto IL_0037;
	}

IL_0037:
	{
		Quaternion_t4030073918  L_12 = V_1;
		return L_12;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  Quaternion_Inverse_m3931399088 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___rotation0, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_Inverse_m1043108654(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Quaternion_t4030073918  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m1043108654 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___rotation0, Quaternion_t4030073918 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Inverse_m1043108654_ftn) (Quaternion_t4030073918 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_Inverse_m1043108654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m1043108654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t2243707580  Quaternion_get_eulerAngles_m3302573991 (Quaternion_t4030073918 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = Quaternion_Internal_ToEulerRad_m2807508879(NULL /*static, unused*/, (*(Quaternion_t4030073918 *)__this), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_0, (57.29578f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Quaternion_Internal_MakePositive_m2921671247(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t2243707580  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector3_t2243707580  Quaternion_get_eulerAngles_m3302573991_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_get_eulerAngles_m3302573991(_thisAdjusted, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Euler_m2887458175 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t2243707580  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2638739322(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_3, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = Quaternion_Internal_FromEulerRad_m1121344272(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001e;
	}

IL_001e:
	{
		Quaternion_t4030073918  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_Euler_m3586339259 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___euler0, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = ___euler0;
		Vector3_t2243707580  L_1 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_2 = Quaternion_Internal_FromEulerRad_m1121344272(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Quaternion_t4030073918  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C"  Vector3_t2243707580  Quaternion_Internal_ToEulerRad_m2807508879 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___rotation0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___rotation0, Vector3_t2243707580 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777_ftn) (Quaternion_t4030073918 *, Vector3_t2243707580 *);
	static Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_Internal_FromEulerRad_m1121344272 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___euler0, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132(NULL /*static, unused*/, (&___euler0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Quaternion_t4030073918  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___euler0, Quaternion_t4030073918 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132_ftn) (Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___euler0, ___value1);
}
// System.Void UnityEngine.Quaternion::Internal_ToAxisAngleRad(UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_Internal_ToAxisAngleRad_m1386331342 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, Vector3_t2243707580 * ___axis1, float* ___angle2, const MethodInfo* method)
{
	{
		Vector3_t2243707580 * L_0 = ___axis1;
		float* L_1 = ___angle2;
		Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m3577625799(NULL /*static, unused*/, (&___q0), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m3577625799 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___q0, Vector3_t2243707580 * ___axis1, float* ___angle2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m3577625799_ftn) (Quaternion_t4030073918 *, Vector3_t2243707580 *, float*);
	static Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m3577625799_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m3577625799_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)");
	_il2cpp_icall_func(___q0, ___axis1, ___angle2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t4030073918  Quaternion_get_identity_m1561886418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t4030073918  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Quaternion__ctor_m3196903881(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Quaternion_t4030073918  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  Quaternion_op_Multiply_m2426727589 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___lhs0, Quaternion_t4030073918  ___rhs1, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___lhs0)->get_w_3();
		float L_1 = (&___rhs1)->get_x_0();
		float L_2 = (&___lhs0)->get_x_0();
		float L_3 = (&___rhs1)->get_w_3();
		float L_4 = (&___lhs0)->get_y_1();
		float L_5 = (&___rhs1)->get_z_2();
		float L_6 = (&___lhs0)->get_z_2();
		float L_7 = (&___rhs1)->get_y_1();
		float L_8 = (&___lhs0)->get_w_3();
		float L_9 = (&___rhs1)->get_y_1();
		float L_10 = (&___lhs0)->get_y_1();
		float L_11 = (&___rhs1)->get_w_3();
		float L_12 = (&___lhs0)->get_z_2();
		float L_13 = (&___rhs1)->get_x_0();
		float L_14 = (&___lhs0)->get_x_0();
		float L_15 = (&___rhs1)->get_z_2();
		float L_16 = (&___lhs0)->get_w_3();
		float L_17 = (&___rhs1)->get_z_2();
		float L_18 = (&___lhs0)->get_z_2();
		float L_19 = (&___rhs1)->get_w_3();
		float L_20 = (&___lhs0)->get_x_0();
		float L_21 = (&___rhs1)->get_y_1();
		float L_22 = (&___lhs0)->get_y_1();
		float L_23 = (&___rhs1)->get_x_0();
		float L_24 = (&___lhs0)->get_w_3();
		float L_25 = (&___rhs1)->get_w_3();
		float L_26 = (&___lhs0)->get_x_0();
		float L_27 = (&___rhs1)->get_x_0();
		float L_28 = (&___lhs0)->get_y_1();
		float L_29 = (&___rhs1)->get_y_1();
		float L_30 = (&___lhs0)->get_z_2();
		float L_31 = (&___rhs1)->get_z_2();
		Quaternion_t4030073918  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Quaternion__ctor_m3196903881(&L_32, ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		V_0 = L_32;
		goto IL_0108;
	}

IL_0108:
	{
		Quaternion_t4030073918  L_33 = V_0;
		return L_33;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Quaternion_op_Multiply_m1483423721 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___rotation0, Vector3_t2243707580  ___point1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		float L_0 = (&___rotation0)->get_x_0();
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = (&___rotation0)->get_y_1();
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = (&___rotation0)->get_z_2();
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = (&___rotation0)->get_x_0();
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = (&___rotation0)->get_y_1();
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = (&___rotation0)->get_z_2();
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = (&___rotation0)->get_x_0();
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = (&___rotation0)->get_x_0();
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = (&___rotation0)->get_y_1();
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = (&___rotation0)->get_w_3();
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = (&___rotation0)->get_w_3();
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = (&___rotation0)->get_w_3();
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = (&___point1)->get_x_1();
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = (&___point1)->get_y_2();
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = (&___point1)->get_z_3();
		(&V_12)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29)))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = (&___point1)->get_x_1();
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = (&___point1)->get_y_2();
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = (&___point1)->get_z_3();
		(&V_12)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38)))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = (&___point1)->get_x_1();
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = (&___point1)->get_y_2();
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = (&___point1)->get_z_3();
		(&V_12)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47)))));
		Vector3_t2243707580  L_48 = V_12;
		V_13 = L_48;
		goto IL_0136;
	}

IL_0136:
	{
		Vector3_t2243707580  L_49 = V_13;
		return L_49;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Equality_m2308156925 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___lhs0, Quaternion_t4030073918  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Quaternion_t4030073918  L_0 = ___lhs0;
		Quaternion_t4030073918  L_1 = ___rhs1;
		float L_2 = Quaternion_Dot_m952616600(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((float)L_2) > ((float)(0.999999f)))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Inequality_m3629786166 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___lhs0, Quaternion_t4030073918  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Quaternion_t4030073918  L_0 = ___lhs0;
		Quaternion_t4030073918  L_1 = ___rhs1;
		float L_2 = Quaternion_Dot_m952616600(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)((!(((float)L_2) <= ((float)(0.999999f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0018;
	}

IL_0018:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m952616600 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___a0, Quaternion_t4030073918  ___b1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		float L_4 = (&___a0)->get_z_2();
		float L_5 = (&___b1)->get_z_2();
		float L_6 = (&___a0)->get_w_3();
		float L_7 = (&___b1)->get_w_3();
		V_0 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		goto IL_0046;
	}

IL_0046:
	{
		float L_8 = V_0;
		return L_8;
	}
}
// System.Single UnityEngine.Quaternion::Angle(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_Angle_m1045775740_MetadataUsageId;
extern "C"  float Quaternion_Angle_m1045775740 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___a0, Quaternion_t4030073918  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Angle_m1045775740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Quaternion_t4030073918  L_0 = ___a0;
		Quaternion_t4030073918  L_1 = ___b1;
		float L_2 = Quaternion_Dot_m952616600(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = fabsf(L_3);
		float L_5 = Mathf_Min_m1648492575(NULL /*static, unused*/, L_4, (1.0f), /*hidden argument*/NULL);
		float L_6 = acosf(L_5);
		V_1 = ((float)((float)((float)((float)L_6*(float)(2.0f)))*(float)(57.29578f)));
		goto IL_0030;
	}

IL_0030:
	{
		float L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_MakePositive(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Quaternion_Internal_MakePositive_m2921671247 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___euler0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		V_0 = (-0.005729578f);
		float L_0 = V_0;
		V_1 = ((float)((float)(360.0f)+(float)L_0));
		float L_1 = (&___euler0)->get_x_1();
		float L_2 = V_0;
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_0034;
		}
	}
	{
		Vector3_t2243707580 * L_3 = (&___euler0);
		float L_4 = L_3->get_x_1();
		L_3->set_x_1(((float)((float)L_4+(float)(360.0f))));
		goto IL_0054;
	}

IL_0034:
	{
		float L_5 = (&___euler0)->get_x_1();
		float L_6 = V_1;
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0054;
		}
	}
	{
		Vector3_t2243707580 * L_7 = (&___euler0);
		float L_8 = L_7->get_x_1();
		L_7->set_x_1(((float)((float)L_8-(float)(360.0f))));
	}

IL_0054:
	{
		float L_9 = (&___euler0)->get_y_2();
		float L_10 = V_0;
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0079;
		}
	}
	{
		Vector3_t2243707580 * L_11 = (&___euler0);
		float L_12 = L_11->get_y_2();
		L_11->set_y_2(((float)((float)L_12+(float)(360.0f))));
		goto IL_0099;
	}

IL_0079:
	{
		float L_13 = (&___euler0)->get_y_2();
		float L_14 = V_1;
		if ((!(((float)L_13) > ((float)L_14))))
		{
			goto IL_0099;
		}
	}
	{
		Vector3_t2243707580 * L_15 = (&___euler0);
		float L_16 = L_15->get_y_2();
		L_15->set_y_2(((float)((float)L_16-(float)(360.0f))));
	}

IL_0099:
	{
		float L_17 = (&___euler0)->get_z_3();
		float L_18 = V_0;
		if ((!(((float)L_17) < ((float)L_18))))
		{
			goto IL_00be;
		}
	}
	{
		Vector3_t2243707580 * L_19 = (&___euler0);
		float L_20 = L_19->get_z_3();
		L_19->set_z_3(((float)((float)L_20+(float)(360.0f))));
		goto IL_00de;
	}

IL_00be:
	{
		float L_21 = (&___euler0)->get_z_3();
		float L_22 = V_1;
		if ((!(((float)L_21) > ((float)L_22))))
		{
			goto IL_00de;
		}
	}
	{
		Vector3_t2243707580 * L_23 = (&___euler0);
		float L_24 = L_23->get_z_3();
		L_23->set_z_3(((float)((float)L_24-(float)(360.0f))));
	}

IL_00de:
	{
		Vector3_t2243707580  L_25 = ___euler0;
		V_2 = L_25;
		goto IL_00e5;
	}

IL_00e5:
	{
		Vector3_t2243707580  L_26 = V_2;
		return L_26;
	}
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m2270520528 (Quaternion_t4030073918 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_0();
		int32_t L_1 = Single_GetHashCode_m3102305584(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_1();
		int32_t L_3 = Single_GetHashCode_m3102305584(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_2();
		int32_t L_5 = Single_GetHashCode_m3102305584(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_3();
		int32_t L_7 = Single_GetHashCode_m3102305584(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0054;
	}

IL_0054:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
extern "C"  int32_t Quaternion_GetHashCode_m2270520528_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_GetHashCode_m2270520528(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern Il2CppClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_Equals_m3730391696_MetadataUsageId;
extern "C"  bool Quaternion_Equals_m3730391696 (Quaternion_t4030073918 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Equals_m3730391696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Quaternion_t4030073918_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Quaternion_t4030073918 *)((Quaternion_t4030073918 *)UnBox (L_1, Quaternion_t4030073918_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_0();
		float L_3 = (&V_1)->get_x_0();
		bool L_4 = Single_Equals_m3359827399(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_1();
		float L_6 = (&V_1)->get_y_1();
		bool L_7 = Single_Equals_m3359827399(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_2();
		float L_9 = (&V_1)->get_z_2();
		bool L_10 = Single_Equals_m3359827399(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_3();
		float L_12 = (&V_1)->get_w_3();
		bool L_13 = Single_Equals_m3359827399(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Quaternion_Equals_m3730391696_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_Equals_m3730391696(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Quaternion::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3587482509;
extern const uint32_t Quaternion_ToString_m2638853272_MetadataUsageId;
extern "C"  String_t* Quaternion_ToString_m2638853272 (Quaternion_t4030073918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_ToString_m2638853272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = __this->get_z_2();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		float L_13 = __this->get_w_3();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral3587482509, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Quaternion_ToString_m2638853272_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_ToString_m2638853272(_thisAdjusted, method);
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m2884721203 (Il2CppObject * __this /* static, unused */, float ___min0, float ___max1, const MethodInfo* method)
{
	typedef float (*Random_Range_m2884721203_ftn) (float, float);
	static Random_Range_m2884721203_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m2884721203_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	return _il2cpp_icall_func(___min0, ___max1);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m694320887 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___min0;
		int32_t L_1 = ___max1;
		int32_t L_2 = Random_RandomRangeInt_m374035151(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C"  int32_t Random_RandomRangeInt_m374035151 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method)
{
	typedef int32_t (*Random_RandomRangeInt_m374035151_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m374035151_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m374035151_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___min0, ___max1);
}
// System.Single UnityEngine.Random::get_value()
extern "C"  float Random_get_value_m976649312 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Random_get_value_m976649312_ftn) ();
	static Random_get_value_m976649312_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_get_value_m976649312_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::get_value()");
	return _il2cpp_icall_func();
}
// UnityEngine.Vector3 UnityEngine.Random::get_onUnitSphere()
extern "C"  Vector3_t2243707580  Random_get_onUnitSphere_m3730445107 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Random_INTERNAL_get_onUnitSphere_m734727474(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Random::INTERNAL_get_onUnitSphere(UnityEngine.Vector3&)
extern "C"  void Random_INTERNAL_get_onUnitSphere_m734727474 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Random_INTERNAL_get_onUnitSphere_m734727474_ftn) (Vector3_t2243707580 *);
	static Random_INTERNAL_get_onUnitSphere_m734727474_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_INTERNAL_get_onUnitSphere_m734727474_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::INTERNAL_get_onUnitSphere(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C"  void RangeAttribute__ctor_m1657271662 (RangeAttribute_t3336560921 * __this, float ___min0, float ___max1, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m3663555848(__this, /*hidden argument*/NULL);
		float L_0 = ___min0;
		__this->set_min_1(L_0);
		float L_1 = ___max1;
		__this->set_max_2(L_1);
		return;
	}
}
// System.Int32 UnityEngine.RangeInt::get_end()
extern "C"  int32_t RangeInt_get_end_m913869897 (RangeInt_t2323401134 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_start_0();
		int32_t L_1 = __this->get_length_1();
		V_0 = ((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t RangeInt_get_end_m913869897_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RangeInt_t2323401134 * _thisAdjusted = reinterpret_cast<RangeInt_t2323401134 *>(__this + 1);
	return RangeInt_get_end_m913869897(_thisAdjusted, method);
}
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m3379034047 (Ray_t2469606224 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___origin0;
		__this->set_m_Origin_0(L_0);
		Vector3_t2243707580  L_1 = Vector3_get_normalized_m936072361((&___direction1), /*hidden argument*/NULL);
		__this->set_m_Direction_1(L_1);
		return;
	}
}
extern "C"  void Ray__ctor_m3379034047_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	Ray__ctor_m3379034047(_thisAdjusted, ___origin0, ___direction1, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t2243707580  Ray_get_origin_m3339262500 (Ray_t2469606224 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Origin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  Ray_get_origin_m3339262500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	return Ray_get_origin_m3339262500(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t2243707580  Ray_get_direction_m4059191533 (Ray_t2469606224 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Direction_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  Ray_get_direction_m4059191533_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	return Ray_get_direction_m4059191533(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t2243707580  Ray_GetPoint_m1353702366 (Ray_t2469606224 * __this, float ___distance0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Origin_0();
		Vector3_t2243707580  L_1 = __this->get_m_Direction_1();
		float L_2 = ___distance0;
		Vector3_t2243707580  L_3 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t2243707580  L_5 = V_0;
		return L_5;
	}
}
extern "C"  Vector3_t2243707580  Ray_GetPoint_m1353702366_AdjustorThunk (Il2CppObject * __this, float ___distance0, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	return Ray_GetPoint_m1353702366(_thisAdjusted, ___distance0, method);
}
// System.String UnityEngine.Ray::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1807026812;
extern const uint32_t Ray_ToString_m2019179238_MetadataUsageId;
extern "C"  String_t* Ray_ToString_m2019179238 (Ray_t2469606224 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ray_ToString_m2019179238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t2243707580  L_1 = __this->get_m_Origin_0();
		Vector3_t2243707580  L_2 = L_1;
		Il2CppObject * L_3 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		Vector3_t2243707580  L_5 = __this->get_m_Direction_1();
		Vector3_t2243707580  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral1807026812, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Ray_ToString_m2019179238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	return Ray_ToString_m2019179238(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C"  Vector3_t2243707580  RaycastHit_get_point_m326143462 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Point_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  RaycastHit_get_point_m326143462_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_point_m326143462(_thisAdjusted, method);
}
// System.Void UnityEngine.RaycastHit::set_point(UnityEngine.Vector3)
extern "C"  void RaycastHit_set_point_m1278922847 (RaycastHit_t87180320 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___value0;
		__this->set_m_Point_0(L_0);
		return;
	}
}
extern "C"  void RaycastHit_set_point_m1278922847_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	RaycastHit_set_point_m1278922847(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C"  Vector3_t2243707580  RaycastHit_get_normal_m817665579 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Normal_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  RaycastHit_get_normal_m817665579_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_normal_m817665579(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C"  float RaycastHit_get_distance_m1178709367 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Distance_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float RaycastHit_get_distance_m1178709367_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_distance_m1178709367(_thisAdjusted, method);
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C"  Collider_t3497673348 * RaycastHit_get_collider_m301198172 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	Collider_t3497673348 * V_0 = NULL;
	{
		Collider_t3497673348 * L_0 = __this->get_m_Collider_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Collider_t3497673348 * L_1 = V_0;
		return L_1;
	}
}
extern "C"  Collider_t3497673348 * RaycastHit_get_collider_m301198172_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_collider_m301198172(_thisAdjusted, method);
}
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t RaycastHit_get_rigidbody_m480380820_MetadataUsageId;
extern "C"  Rigidbody_t4233889191 * RaycastHit_get_rigidbody_m480380820 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit_get_rigidbody_m480380820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody_t4233889191 * V_0 = NULL;
	Rigidbody_t4233889191 * G_B3_0 = NULL;
	{
		Collider_t3497673348 * L_0 = RaycastHit_get_collider_m301198172(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Collider_t3497673348 * L_2 = RaycastHit_get_collider_m301198172(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_t4233889191 * L_3 = Collider_get_attachedRigidbody_m3279305420(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = ((Rigidbody_t4233889191 *)(NULL));
	}

IL_0023:
	{
		V_0 = G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		Rigidbody_t4233889191 * L_4 = V_0;
		return L_4;
	}
}
extern "C"  Rigidbody_t4233889191 * RaycastHit_get_rigidbody_m480380820_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_rigidbody_m480380820(_thisAdjusted, method);
}
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t RaycastHit_get_transform_m3290290036_MetadataUsageId;
extern "C"  Transform_t3275118058 * RaycastHit_get_transform_m3290290036 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit_get_transform_m3290290036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody_t4233889191 * V_0 = NULL;
	Transform_t3275118058 * V_1 = NULL;
	{
		Rigidbody_t4233889191 * L_0 = RaycastHit_get_rigidbody_m480380820(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody_t4233889191 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Rigidbody_t4233889191 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0049;
	}

IL_0020:
	{
		Collider_t3497673348 * L_5 = RaycastHit_get_collider_m301198172(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		Collider_t3497673348 * L_7 = RaycastHit_get_collider_m301198172(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		goto IL_0049;
	}

IL_0042:
	{
		V_1 = (Transform_t3275118058 *)NULL;
		goto IL_0049;
	}

IL_0049:
	{
		Transform_t3275118058 * L_9 = V_1;
		return L_9;
	}
}
extern "C"  Transform_t3275118058 * RaycastHit_get_transform_m3290290036_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_transform_m3290290036(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t87180320_marshal_pinvoke(const RaycastHit_t87180320& unmarshaled, RaycastHit_t87180320_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t87180320_marshal_pinvoke_back(const RaycastHit_t87180320_marshaled_pinvoke& marshaled, RaycastHit_t87180320& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t87180320_marshal_pinvoke_cleanup(RaycastHit_t87180320_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t87180320_marshal_com(const RaycastHit_t87180320& unmarshaled, RaycastHit_t87180320_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t87180320_marshal_com_back(const RaycastHit_t87180320_marshaled_com& marshaled, RaycastHit_t87180320& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t87180320_marshal_com_cleanup(RaycastHit_t87180320_marshaled_com& marshaled)
{
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C"  Vector2_t2243707579  RaycastHit2D_get_point_m442317739 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0 = __this->get_m_Point_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t2243707579  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t2243707579  RaycastHit2D_get_point_m442317739_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_point_m442317739(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C"  Vector2_t2243707579  RaycastHit2D_get_normal_m3768105386 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0 = __this->get_m_Normal_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t2243707579  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t2243707579  RaycastHit2D_get_normal_m3768105386_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_normal_m3768105386(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C"  float RaycastHit2D_get_fraction_m1296150410 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Fraction_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float RaycastHit2D_get_fraction_m1296150410_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_fraction_m1296150410(_thisAdjusted, method);
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C"  Collider2D_t646061738 * RaycastHit2D_get_collider_m2568504212 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	Collider2D_t646061738 * V_0 = NULL;
	{
		Collider2D_t646061738 * L_0 = __this->get_m_Collider_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Collider2D_t646061738 * L_1 = V_0;
		return L_1;
	}
}
extern "C"  Collider2D_t646061738 * RaycastHit2D_get_collider_m2568504212_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_collider_m2568504212(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke(const RaycastHit2D_t4063908774& unmarshaled, RaycastHit2D_t4063908774_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke_back(const RaycastHit2D_t4063908774_marshaled_pinvoke& marshaled, RaycastHit2D_t4063908774& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke_cleanup(RaycastHit2D_t4063908774_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t4063908774_marshal_com(const RaycastHit2D_t4063908774& unmarshaled, RaycastHit2D_t4063908774_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t4063908774_marshal_com_back(const RaycastHit2D_t4063908774_marshaled_com& marshaled, RaycastHit2D_t4063908774& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t4063908774_marshal_com_cleanup(RaycastHit2D_t4063908774_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m1220545469 (Rect_t3681755626 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m1220545469_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect__ctor_m1220545469(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// UnityEngine.Rect UnityEngine.Rect::MinMaxRect(System.Single,System.Single,System.Single,System.Single)
extern "C"  Rect_t3681755626  Rect_MinMaxRect_m4237641803 (Il2CppObject * __this /* static, unused */, float ___xmin0, float ___ymin1, float ___xmax2, float ___ymax3, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___xmin0;
		float L_1 = ___ymin1;
		float L_2 = ___xmax2;
		float L_3 = ___xmin0;
		float L_4 = ___ymax3;
		float L_5 = ___ymin1;
		Rect_t3681755626  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m1220545469(&L_6, L_0, L_1, ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0014;
	}

IL_0014:
	{
		Rect_t3681755626  L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Rect::Set(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect_Set_m1972211443 (Rect_t3681755626 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect_Set_m1972211443_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_Set_m1972211443(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m1393582490 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_x_m1393582490_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_x_m1393582490(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m3783700513 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_XMin_0(L_0);
		return;
	}
}
extern "C"  void Rect_set_x_m3783700513_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_x_m3783700513(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m1393582395 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_y_m1393582395_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_y_m1393582395(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m4294916608 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_YMin_1(L_0);
		return;
	}
}
extern "C"  void Rect_set_y_m4294916608_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_y_m4294916608(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t2243707579  Rect_get_position_m24550734 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m_XMin_0();
		float L_1 = __this->get_m_YMin_1();
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2243707579  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_position_m24550734_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_position_m24550734(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t2243707579  Rect_get_center_m3049923624 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_m_Width_2();
		float L_2 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		float L_3 = __this->get_m_Height_3();
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, ((float)((float)L_0+(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2+(float)((float)((float)L_3/(float)(2.0f))))), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0032;
	}

IL_0032:
	{
		Vector2_t2243707579  L_5 = V_0;
		return L_5;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_center_m3049923624_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_center_m3049923624(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_center(UnityEngine.Vector2)
extern "C"  void Rect_set_center_m198417975 (Rect_t3681755626 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		float L_0 = (&___value0)->get_x_0();
		float L_1 = __this->get_m_Width_2();
		__this->set_m_XMin_0(((float)((float)L_0-(float)((float)((float)L_1/(float)(2.0f))))));
		float L_2 = (&___value0)->get_y_1();
		float L_3 = __this->get_m_Height_3();
		__this->set_m_YMin_1(((float)((float)L_2-(float)((float)((float)L_3/(float)(2.0f))))));
		return;
	}
}
extern "C"  void Rect_set_center_m198417975_AdjustorThunk (Il2CppObject * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_center_m198417975(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t2243707579  Rect_get_min_m2549872833 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2243707579  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_min_m2549872833_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_min_m2549872833(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t2243707579  Rect_get_max_m96665935 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2243707579  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_max_m96665935_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_max_m96665935(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m1138015702 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_width_m1138015702_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_width_m1138015702(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m1921257731 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Width_2(L_0);
		return;
	}
}
extern "C"  void Rect_set_width_m1921257731_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_width_m1921257731(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m3128694305 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_height_m3128694305_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_height_m3128694305(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m2019122814 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Height_3(L_0);
		return;
	}
}
extern "C"  void Rect_set_height_m2019122814_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_height_m2019122814(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t2243707579  Rect_get_size_m3833121112 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_Height_3();
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2243707579  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_size_m3833121112_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_size_m3833121112(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m1161102488 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_xMin_m1161102488_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_xMin_m1161102488(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m4214255623 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_XMin_0(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_xMin_m4214255623_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_xMin_m4214255623(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m1161103577 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_yMin_m1161103577_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_yMin_m1161103577(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m734445288 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_YMin_1(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_yMin_m734445288_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_yMin_m734445288(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m2915145014 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_XMin_0();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_xMax_m2915145014_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_xMax_m2915145014(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m3501625033 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_xMax_m3501625033_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_xMax_m3501625033(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m2915146103 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		float L_1 = __this->get_m_YMin_1();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_yMax_m2915146103_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_yMax_m2915146103(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m21814698 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_yMax_m21814698_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_yMax_m21814698(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m1334685290 (Rect_t3681755626 * __this, Vector2_t2243707579  ___point0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_0();
		float L_1 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = (&___point0)->get_x_0();
		float L_3 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = (&___point0)->get_y_1();
		float L_5 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = (&___point0)->get_y_1();
		float L_7 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Contains_m1334685290_AdjustorThunk (Il2CppObject * __this, Vector2_t2243707579  ___point0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Contains_m1334685290(_thisAdjusted, ___point0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m1334685291 (Rect_t3681755626 * __this, Vector3_t2243707580  ___point0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Contains_m1334685291_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___point0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Contains_m1334685291(_thisAdjusted, ___point0, method);
}
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t3681755626  Rect_OrderMinMax_m1783437776 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___rect0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = Rect_get_xMin_m1161102488((&___rect0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMax_m2915145014((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0034;
		}
	}
	{
		float L_2 = Rect_get_xMin_m1161102488((&___rect0), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_xMax_m2915145014((&___rect0), /*hidden argument*/NULL);
		Rect_set_xMin_m4214255623((&___rect0), L_3, /*hidden argument*/NULL);
		float L_4 = V_0;
		Rect_set_xMax_m3501625033((&___rect0), L_4, /*hidden argument*/NULL);
	}

IL_0034:
	{
		float L_5 = Rect_get_yMin_m1161103577((&___rect0), /*hidden argument*/NULL);
		float L_6 = Rect_get_yMax_m2915146103((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0067;
		}
	}
	{
		float L_7 = Rect_get_yMin_m1161103577((&___rect0), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = Rect_get_yMax_m2915146103((&___rect0), /*hidden argument*/NULL);
		Rect_set_yMin_m734445288((&___rect0), L_8, /*hidden argument*/NULL);
		float L_9 = V_1;
		Rect_set_yMax_m21814698((&___rect0), L_9, /*hidden argument*/NULL);
	}

IL_0067:
	{
		Rect_t3681755626  L_10 = ___rect0;
		V_2 = L_10;
		goto IL_006e;
	}

IL_006e:
	{
		Rect_t3681755626  L_11 = V_2;
		return L_11;
	}
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m210444568 (Rect_t3681755626 * __this, Rect_t3681755626  ___other0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_xMax_m2915145014((&___other0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = Rect_get_xMin_m1161102488((&___other0), /*hidden argument*/NULL);
		float L_3 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = Rect_get_yMax_m2915146103((&___other0), /*hidden argument*/NULL);
		float L_5 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = Rect_get_yMin_m1161103577((&___other0), /*hidden argument*/NULL);
		float L_7 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Overlaps_m210444568_AdjustorThunk (Il2CppObject * __this, Rect_t3681755626  ___other0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Overlaps_m210444568(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m4145874649 (Rect_t3681755626 * __this, Rect_t3681755626  ___other0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		V_0 = (*(Rect_t3681755626 *)__this);
		bool L_0 = ___allowInverse1;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Rect_t3681755626  L_1 = V_0;
		Rect_t3681755626  L_2 = Rect_OrderMinMax_m1783437776(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Rect_t3681755626  L_3 = ___other0;
		Rect_t3681755626  L_4 = Rect_OrderMinMax_m1783437776(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		___other0 = L_4;
	}

IL_001f:
	{
		Rect_t3681755626  L_5 = ___other0;
		bool L_6 = Rect_Overlaps_m210444568((&V_0), L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_002d;
	}

IL_002d:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
extern "C"  bool Rect_Overlaps_m4145874649_AdjustorThunk (Il2CppObject * __this, Rect_t3681755626  ___other0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Overlaps_m4145874649(_thisAdjusted, ___other0, ___allowInverse1, method);
}
// System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Inequality_m3595915756 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___lhs0, Rect_t3681755626  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m1393582490((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m1393582490((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004f;
		}
	}
	{
		float L_2 = Rect_get_y_m1393582395((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m1393582395((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004f;
		}
	}
	{
		float L_4 = Rect_get_width_m1138015702((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m1138015702((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004f;
		}
	}
	{
		float L_6 = Rect_get_height_m3128694305((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m3128694305((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((int32_t)((((float)L_6) == ((float)L_7))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0050;
	}

IL_004f:
	{
		G_B5_0 = 1;
	}

IL_0050:
	{
		V_0 = (bool)G_B5_0;
		goto IL_0056;
	}

IL_0056:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m2793663577 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___lhs0, Rect_t3681755626  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m1393582490((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m1393582490((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004c;
		}
	}
	{
		float L_2 = Rect_get_y_m1393582395((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m1393582395((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004c;
		}
	}
	{
		float L_4 = Rect_get_width_m1138015702((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m1138015702((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004c;
		}
	}
	{
		float L_6 = Rect_get_height_m3128694305((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m3128694305((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004d;
	}

IL_004c:
	{
		G_B5_0 = 0;
	}

IL_004d:
	{
		V_0 = (bool)G_B5_0;
		goto IL_0053;
	}

IL_0053:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m559954498 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	{
		float L_0 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m3102305584((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m1138015702(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m3102305584((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m3102305584((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m3128694305(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m3102305584((&V_3), /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0061;
	}

IL_0061:
	{
		int32_t L_8 = V_4;
		return L_8;
	}
}
extern "C"  int32_t Rect_GetHashCode_m559954498_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_GetHashCode_m559954498(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern Il2CppClass* Rect_t3681755626_il2cpp_TypeInfo_var;
extern const uint32_t Rect_Equals_m3806390726_MetadataUsageId;
extern "C"  bool Rect_Equals_m3806390726 (Rect_t3681755626 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_Equals_m3806390726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Rect_t3681755626_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0088;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox (L_1, Rect_t3681755626_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = Rect_get_x_m1393582490((&V_1), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m3359827399((&V_2), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0081;
		}
	}
	{
		float L_5 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_y_m1393582395((&V_1), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m3359827399((&V_3), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0081;
		}
	}
	{
		float L_8 = Rect_get_width_m1138015702(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_width_m1138015702((&V_1), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m3359827399((&V_4), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		float L_11 = Rect_get_height_m3128694305(__this, /*hidden argument*/NULL);
		V_5 = L_11;
		float L_12 = Rect_get_height_m3128694305((&V_1), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m3359827399((&V_5), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0082;
	}

IL_0081:
	{
		G_B7_0 = 0;
	}

IL_0082:
	{
		V_0 = (bool)G_B7_0;
		goto IL_0088;
	}

IL_0088:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Rect_Equals_m3806390726_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Equals_m3806390726(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Rect::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1853817013;
extern const uint32_t Rect_ToString_m2728794442_MetadataUsageId;
extern "C"  String_t* Rect_ToString_m2728794442 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_ToString_m2728794442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = Rect_get_width_m1138015702(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		float L_13 = Rect_get_height_m3128694305(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral1853817013, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Rect_ToString_m2728794442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_ToString_m2728794442(_thisAdjusted, method);
}
// System.Void UnityEngine.RectOffset::.ctor()
extern "C"  void RectOffset__ctor_m2227510254 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		RectOffset_Init_m4361650(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(UnityEngine.GUIStyle,System.IntPtr)
extern "C"  void RectOffset__ctor_m3541945769 (RectOffset_t3387826427 * __this, GUIStyle_t1799908754 * ___sourceStyle0, IntPtr_t ___source1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_0 = ___sourceStyle0;
		__this->set_m_SourceStyle_1(L_0);
		IntPtr_t L_1 = ___source1;
		__this->set_m_Ptr_0(L_1);
		return;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m4361650 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Init_m4361650_ftn) (RectOffset_t3387826427 *);
	static RectOffset_Init_m4361650_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m4361650_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m3198970074 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Cleanup_m3198970074_ftn) (RectOffset_t3387826427 *);
	static RectOffset_Cleanup_m3198970074_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m3198970074_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m439065308 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_left_m439065308_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_left_m439065308_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m439065308_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C"  void RectOffset_set_left_m620681523 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_left_m620681523_ftn) (RectOffset_t3387826427 *, int32_t);
	static RectOffset_set_left_m620681523_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m620681523_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m281378687 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_right_m281378687_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_right_m281378687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m281378687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C"  void RectOffset_set_right_m1671272302 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_right_m1671272302_ftn) (RectOffset_t3387826427 *, int32_t);
	static RectOffset_set_right_m1671272302_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m1671272302_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m3629049358 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_top_m3629049358_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_top_m3629049358_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m3629049358_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C"  void RectOffset_set_top_m3579196427 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_top_m3579196427_ftn) (RectOffset_t3387826427 *, int32_t);
	static RectOffset_set_top_m3579196427_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m3579196427_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m4112328858 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_bottom_m4112328858_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_bottom_m4112328858_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m4112328858_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C"  void RectOffset_set_bottom_m4065521443 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_bottom_m4065521443_ftn) (RectOffset_t3387826427 *, int32_t);
	static RectOffset_set_bottom_m4065521443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m4065521443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C"  int32_t RectOffset_get_horizontal_m3818523637 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m3818523637_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_horizontal_m3818523637_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m3818523637_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C"  int32_t RectOffset_get_vertical_m3856345169 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_vertical_m3856345169_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_vertical_m3856345169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m3856345169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.RectOffset::Remove(UnityEngine.Rect)
extern "C"  Rect_t3681755626  RectOffset_Remove_m1330811977 (RectOffset_t3387826427 * __this, Rect_t3681755626  ___rect0, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectOffset_INTERNAL_CALL_Remove_m1194095901(NULL /*static, unused*/, __this, (&___rect0), (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Rect_t3681755626  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
extern "C"  void RectOffset_INTERNAL_CALL_Remove_m1194095901 (Il2CppObject * __this /* static, unused */, RectOffset_t3387826427 * ___self0, Rect_t3681755626 * ___rect1, Rect_t3681755626 * ___value2, const MethodInfo* method)
{
	typedef void (*RectOffset_INTERNAL_CALL_Remove_m1194095901_ftn) (RectOffset_t3387826427 *, Rect_t3681755626 *, Rect_t3681755626 *);
	static RectOffset_INTERNAL_CALL_Remove_m1194095901_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Remove_m1194095901_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1, ___value2);
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C"  void RectOffset_Finalize_m901770914 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t1799908754 * L_0 = __this->get_m_SourceStyle_1();
			if (L_0)
			{
				goto IL_0012;
			}
		}

IL_000c:
		{
			RectOffset_Cleanup_m3198970074(__this, /*hidden argument*/NULL);
		}

IL_0012:
		{
			IL2CPP_LEAVE(0x1E, FINALLY_0017);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0017;
	}

FINALLY_0017:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(23)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(23)
	{
		IL2CPP_JUMP_TBL(0x1E, IL_001e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_001e:
	{
		return;
	}
}
// System.String UnityEngine.RectOffset::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3899275604;
extern const uint32_t RectOffset_ToString_m1281517011_MetadataUsageId;
extern "C"  String_t* RectOffset_ToString_m1281517011 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_ToString_m1281517011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_1 = RectOffset_get_left_m439065308(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m281378687(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m3629049358(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m4112328858(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral3899275604, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3387826427_marshal_pinvoke(const RectOffset_t3387826427& unmarshaled, RectOffset_t3387826427_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
extern "C" void RectOffset_t3387826427_marshal_pinvoke_back(const RectOffset_t3387826427_marshaled_pinvoke& marshaled, RectOffset_t3387826427& unmarshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3387826427_marshal_pinvoke_cleanup(RectOffset_t3387826427_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3387826427_marshal_com(const RectOffset_t3387826427& unmarshaled, RectOffset_t3387826427_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
extern "C" void RectOffset_t3387826427_marshal_com_back(const RectOffset_t3387826427_marshaled_com& marshaled, RectOffset_t3387826427& unmarshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3387826427_marshal_com_cleanup(RectOffset_t3387826427_marshaled_com& marshaled)
{
}
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t3681755626  RectTransform_get_rect_m73954734 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_rect_m1177342209(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t3681755626  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m1177342209 (RectTransform_t3349966182 * __this, Rect_t3681755626 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_rect_m1177342209_ftn) (RectTransform_t3349966182 *, Rect_t3681755626 *);
	static RectTransform_INTERNAL_get_rect_m1177342209_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_rect_m1177342209_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t2243707579  RectTransform_get_anchorMin_m1497323108 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchorMin_m3180545469(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2243707579  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m4247668187 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMin_m885423409(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m3180545469 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMin_m3180545469_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_anchorMin_m3180545469_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMin_m3180545469_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m885423409 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMin_m885423409_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_anchorMin_m885423409_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMin_m885423409_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t2243707579  RectTransform_get_anchorMax_m3816015142 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchorMax_m834202955(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2243707579  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m2955899993 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMax_m1551648727(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m834202955 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMax_m834202955_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_anchorMax_m834202955_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMax_m834202955_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m1551648727 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMax_m1551648727_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_anchorMax_m1551648727_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMax_m1551648727_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t2243707579  RectTransform_get_anchoredPosition_m3570822376 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchoredPosition_m3564306187(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2243707579  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m2077229449 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchoredPosition_m693024247(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m3564306187 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchoredPosition_m3564306187_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_anchoredPosition_m3564306187_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchoredPosition_m3564306187_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m693024247 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchoredPosition_m693024247_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_anchoredPosition_m693024247_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchoredPosition_m693024247_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t2243707579  RectTransform_get_sizeDelta_m2157326342 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_sizeDelta_m3975625099(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2243707579  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m2319668137 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_sizeDelta_m1402803191(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m3975625099 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_sizeDelta_m3975625099_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_sizeDelta_m3975625099_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_sizeDelta_m3975625099_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m1402803191 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_sizeDelta_m1402803191_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_sizeDelta_m1402803191_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_sizeDelta_m1402803191_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t2243707579  RectTransform_get_pivot_m759087479 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_pivot_m3003734630(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2243707579  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m1360548980 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_pivot_m2764958706(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m3003734630 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_pivot_m3003734630_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_pivot_m3003734630_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_pivot_m3003734630_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m2764958706 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_pivot_m2764958706_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_pivot_m2764958706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_pivot_m2764958706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppClass* ReapplyDrivenProperties_t2020713228_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_add_reapplyDrivenProperties_m1603911943_MetadataUsageId;
extern "C"  void RectTransform_add_reapplyDrivenProperties_m1603911943 (Il2CppObject * __this /* static, unused */, ReapplyDrivenProperties_t2020713228 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_add_reapplyDrivenProperties_m1603911943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReapplyDrivenProperties_t2020713228 * V_0 = NULL;
	ReapplyDrivenProperties_t2020713228 * V_1 = NULL;
	{
		ReapplyDrivenProperties_t2020713228 * L_0 = ((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ReapplyDrivenProperties_t2020713228 * L_1 = V_0;
		V_1 = L_1;
		ReapplyDrivenProperties_t2020713228 * L_2 = V_1;
		ReapplyDrivenProperties_t2020713228 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ReapplyDrivenProperties_t2020713228 * L_5 = V_0;
		ReapplyDrivenProperties_t2020713228 * L_6 = InterlockedCompareExchangeImpl<ReapplyDrivenProperties_t2020713228 *>((((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_address_of_reapplyDrivenProperties_2()), ((ReapplyDrivenProperties_t2020713228 *)CastclassSealed(L_4, ReapplyDrivenProperties_t2020713228_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ReapplyDrivenProperties_t2020713228 * L_7 = V_0;
		ReapplyDrivenProperties_t2020713228 * L_8 = V_1;
		if ((!(((Il2CppObject*)(ReapplyDrivenProperties_t2020713228 *)L_7) == ((Il2CppObject*)(ReapplyDrivenProperties_t2020713228 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppClass* ReapplyDrivenProperties_t2020713228_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_remove_reapplyDrivenProperties_m4209881182_MetadataUsageId;
extern "C"  void RectTransform_remove_reapplyDrivenProperties_m4209881182 (Il2CppObject * __this /* static, unused */, ReapplyDrivenProperties_t2020713228 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_remove_reapplyDrivenProperties_m4209881182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReapplyDrivenProperties_t2020713228 * V_0 = NULL;
	ReapplyDrivenProperties_t2020713228 * V_1 = NULL;
	{
		ReapplyDrivenProperties_t2020713228 * L_0 = ((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ReapplyDrivenProperties_t2020713228 * L_1 = V_0;
		V_1 = L_1;
		ReapplyDrivenProperties_t2020713228 * L_2 = V_1;
		ReapplyDrivenProperties_t2020713228 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ReapplyDrivenProperties_t2020713228 * L_5 = V_0;
		ReapplyDrivenProperties_t2020713228 * L_6 = InterlockedCompareExchangeImpl<ReapplyDrivenProperties_t2020713228 *>((((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_address_of_reapplyDrivenProperties_2()), ((ReapplyDrivenProperties_t2020713228 *)CastclassSealed(L_4, ReapplyDrivenProperties_t2020713228_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ReapplyDrivenProperties_t2020713228 * L_7 = V_0;
		ReapplyDrivenProperties_t2020713228 * L_8 = V_1;
		if ((!(((Il2CppObject*)(ReapplyDrivenProperties_t2020713228 *)L_7) == ((Il2CppObject*)(ReapplyDrivenProperties_t2020713228 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_SendReapplyDrivenProperties_m90487700_MetadataUsageId;
extern "C"  void RectTransform_SendReapplyDrivenProperties_m90487700 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___driven0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_SendReapplyDrivenProperties_m90487700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReapplyDrivenProperties_t2020713228 * L_0 = ((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ReapplyDrivenProperties_t2020713228 * L_1 = ((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		RectTransform_t3349966182 * L_2 = ___driven0;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m1090213637(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral895140023;
extern const uint32_t RectTransform_GetLocalCorners_m1836626405_MetadataUsageId;
extern "C"  void RectTransform_GetLocalCorners_m1836626405 (RectTransform_t3349966182 * __this, Vector3U5BU5D_t1172311765* ___fourCornersArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetLocalCorners_m1836626405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector3U5BU5D_t1172311765* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral895140023, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_0020:
	{
		Rect_t3681755626  L_2 = RectTransform_get_rect_m73954734(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m1393582490((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = Rect_get_y_m1393582395((&V_0), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Rect_get_xMax_m2915145014((&V_0), /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_yMax_m2915146103((&V_0), /*hidden argument*/NULL);
		V_4 = L_6;
		Vector3U5BU5D_t1172311765* L_7 = ___fourCornersArray0;
		NullCheck(L_7);
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t2243707580  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2638739322(&L_10, L_8, L_9, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_10;
		Vector3U5BU5D_t1172311765* L_11 = ___fourCornersArray0;
		NullCheck(L_11);
		float L_12 = V_1;
		float L_13 = V_4;
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_14;
		Vector3U5BU5D_t1172311765* L_15 = ___fourCornersArray0;
		NullCheck(L_15);
		float L_16 = V_3;
		float L_17 = V_4;
		Vector3_t2243707580  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2638739322(&L_18, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_18;
		Vector3U5BU5D_t1172311765* L_19 = ___fourCornersArray0;
		NullCheck(L_19);
		float L_20 = V_3;
		float L_21 = V_2;
		Vector3_t2243707580  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m2638739322(&L_22, L_20, L_21, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_22;
	}

IL_00aa:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3518247078;
extern const uint32_t RectTransform_GetWorldCorners_m3873546362_MetadataUsageId;
extern "C"  void RectTransform_GetWorldCorners_m3873546362 (RectTransform_t3349966182 * __this, Vector3U5BU5D_t1172311765* ___fourCornersArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetWorldCorners_m3873546362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Vector3U5BU5D_t1172311765* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3518247078, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0020:
	{
		Vector3U5BU5D_t1172311765* L_2 = ___fourCornersArray0;
		RectTransform_GetLocalCorners_m1836626405(__this, L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0057;
	}

IL_0035:
	{
		Vector3U5BU5D_t1172311765* L_4 = ___fourCornersArray0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Transform_t3275118058 * L_6 = V_0;
		Vector3U5BU5D_t1172311765* L_7 = ___fourCornersArray0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		NullCheck(L_6);
		Vector3_t2243707580  L_9 = Transform_TransformPoint_m3272254198(L_6, (*(Vector3_t2243707580 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_0035;
		}
	}

IL_005e:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMin_m2982698987 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0 = ___value0;
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_3 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = Vector2_Scale_m3228063809(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector2_t2243707579  L_7 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = V_0;
		Vector2_t2243707579  L_9 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m2319668137(__this, L_9, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_11 = V_0;
		Vector2_t2243707579  L_12 = Vector2_get_one_m3174311904(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_14 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Vector2_t2243707579  L_15 = Vector2_Scale_m3228063809(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_10, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m2077229449(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMax_m3702115945 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0 = ___value0;
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_3 = Vector2_get_one_m3174311904(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = Vector2_Scale_m3228063809(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_0, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Vector2_t2243707579  L_9 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = V_0;
		Vector2_t2243707579  L_11 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m2319668137(__this, L_11, /*hidden argument*/NULL);
		Vector2_t2243707579  L_12 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = V_0;
		Vector2_t2243707579  L_14 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_15 = Vector2_Scale_m3228063809(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m2077229449(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
extern "C"  void RectTransform_SetInsetAndSizeFromParentEdge_m2835026182 (RectTransform_t3349966182 * __this, int32_t ___edge0, float ___inset1, float ___size2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2243707579  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2243707579  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t G_B4_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	Vector2_t2243707579 * G_B12_1 = NULL;
	int32_t G_B11_0 = 0;
	Vector2_t2243707579 * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	int32_t G_B13_1 = 0;
	Vector2_t2243707579 * G_B13_2 = NULL;
	{
		int32_t L_0 = ___edge0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ___edge0;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0015;
		}
	}

IL_000f:
	{
		G_B4_0 = 1;
		goto IL_0016;
	}

IL_0015:
	{
		G_B4_0 = 0;
	}

IL_0016:
	{
		V_0 = G_B4_0;
		int32_t L_2 = ___edge0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = ___edge0;
		G_B7_0 = ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
		goto IL_0025;
	}

IL_0024:
	{
		G_B7_0 = 1;
	}

IL_0025:
	{
		V_1 = (bool)G_B7_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_0033;
	}

IL_0032:
	{
		G_B10_0 = 0;
	}

IL_0033:
	{
		V_2 = (((float)((float)G_B10_0)));
		Vector2_t2243707579  L_5 = RectTransform_get_anchorMin_m1497323108(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_0;
		float L_7 = V_2;
		Vector2_set_Item_m3881967114((&V_3), L_6, L_7, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = V_3;
		RectTransform_set_anchorMin_m4247668187(__this, L_8, /*hidden argument*/NULL);
		Vector2_t2243707579  L_9 = RectTransform_get_anchorMax_m3816015142(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_0;
		float L_11 = V_2;
		Vector2_set_Item_m3881967114((&V_3), L_10, L_11, /*hidden argument*/NULL);
		Vector2_t2243707579  L_12 = V_3;
		RectTransform_set_anchorMax_m2955899993(__this, L_12, /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_0;
		float L_15 = ___size2;
		Vector2_set_Item_m3881967114((&V_4), L_14, L_15, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = V_4;
		RectTransform_set_sizeDelta_m2319668137(__this, L_16, /*hidden argument*/NULL);
		Vector2_t2243707579  L_17 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		G_B11_0 = L_18;
		G_B11_1 = (&V_5);
		if (!L_19)
		{
			G_B12_0 = L_18;
			G_B12_1 = (&V_5);
			goto IL_00ad;
		}
	}
	{
		float L_20 = ___inset1;
		float L_21 = ___size2;
		Vector2_t2243707579  L_22 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = V_0;
		float L_24 = Vector2_get_Item_m2792130561((&V_6), L_23, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)((-L_20))-(float)((float)((float)L_21*(float)((float)((float)(1.0f)-(float)L_24))))));
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c1;
	}

IL_00ad:
	{
		float L_25 = ___inset1;
		float L_26 = ___size2;
		Vector2_t2243707579  L_27 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_0;
		float L_29 = Vector2_get_Item_m2792130561((&V_7), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)L_25+(float)((float)((float)L_26*(float)L_29))));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c1:
	{
		Vector2_set_Item_m3881967114(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		Vector2_t2243707579  L_30 = V_5;
		RectTransform_set_anchoredPosition_m2077229449(__this, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
extern "C"  void RectTransform_SetSizeWithCurrentAnchors_m2368352721 (RectTransform_t3349966182 * __this, int32_t ___axis0, float ___size1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = ___axis0;
		V_0 = L_0;
		Vector2_t2243707579  L_1 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		float L_3 = ___size1;
		Vector2_t2243707579  L_4 = RectTransform_GetParentSize_m1571597933(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_0;
		float L_6 = Vector2_get_Item_m2792130561((&V_2), L_5, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7 = RectTransform_get_anchorMax_m3816015142(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_0;
		float L_9 = Vector2_get_Item_m2792130561((&V_3), L_8, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = RectTransform_get_anchorMin_m1497323108(__this, /*hidden argument*/NULL);
		V_4 = L_10;
		int32_t L_11 = V_0;
		float L_12 = Vector2_get_Item_m2792130561((&V_4), L_11, /*hidden argument*/NULL);
		Vector2_set_Item_m3881967114((&V_1), L_2, ((float)((float)L_3-(float)((float)((float)L_6*(float)((float)((float)L_9-(float)L_12)))))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = V_1;
		RectTransform_set_sizeDelta_m2319668137(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_GetParentSize_m1571597933_MetadataUsageId;
extern "C"  Vector2_t2243707579  RectTransform_GetParentSize_m1571597933 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetParentSize_m1571597933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3349966182 * V_0 = NULL;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Transform_t3275118058 * L_0 = Transform_get_parent_m147407266(__this, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t3349966182 *)IsInstSealed(L_0, RectTransform_t3349966182_il2cpp_TypeInfo_var));
		RectTransform_t3349966182 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		Vector2_t2243707579  L_3 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0037;
	}

IL_0023:
	{
		RectTransform_t3349966182 * L_4 = V_0;
		NullCheck(L_4);
		Rect_t3681755626  L_5 = RectTransform_get_rect_m73954734(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Vector2_t2243707579  L_6 = Rect_get_size_m3833121112((&V_2), /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0037;
	}

IL_0037:
	{
		Vector2_t2243707579  L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C"  void ReapplyDrivenProperties__ctor_m210072638 (ReapplyDrivenProperties_t2020713228 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m1090213637 (ReapplyDrivenProperties_t2020713228 * __this, RectTransform_t3349966182 * ___driven0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReapplyDrivenProperties_Invoke_m1090213637((ReapplyDrivenProperties_t2020713228 *)__this->get_prev_9(),___driven0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, RectTransform_t3349966182 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RectTransform_t3349966182 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReapplyDrivenProperties_BeginInvoke_m2337529776 (ReapplyDrivenProperties_t2020713228 * __this, RectTransform_t3349966182 * ___driven0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C"  void ReapplyDrivenProperties_EndInvoke_m2375002944 (ReapplyDrivenProperties_t2020713228 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToWorldPointInRectangle_m2304638810_MetadataUsageId;
extern "C"  bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m2304638810 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, Vector2_t2243707579  ___screenPoint1, Camera_t189460977 * ___cam2, Vector3_t2243707580 * ___worldPoint3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToWorldPointInRectangle_m2304638810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Plane_t3727654732  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		Vector3_t2243707580 * L_0 = ___worldPoint3;
		Vector2_t2243707579  L_1 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)L_0) = L_2;
		Camera_t189460977 * L_3 = ___cam2;
		Vector2_t2243707579  L_4 = ___screenPoint1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		Ray_t2469606224  L_5 = RectTransformUtility_ScreenPointToRay_m1842507230(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t3349966182 * L_6 = ___rect0;
		NullCheck(L_6);
		Quaternion_t4030073918  L_7 = Transform_get_rotation_m1033555130(L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_get_back_m4246539215(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_10 = ___rect0;
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_position_m1104419803(L_10, /*hidden argument*/NULL);
		Plane__ctor_m3187718367((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t2469606224  L_12 = V_0;
		bool L_13 = Plane_Raycast_m2870142810((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_004c;
		}
	}
	{
		V_3 = (bool)0;
		goto IL_0061;
	}

IL_004c:
	{
		Vector3_t2243707580 * L_14 = ___worldPoint3;
		float L_15 = V_2;
		Vector3_t2243707580  L_16 = Ray_GetPoint_m1353702366((&V_0), L_15, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)L_14) = L_16;
		V_3 = (bool)1;
		goto IL_0061;
	}

IL_0061:
	{
		bool L_17 = V_3;
		return L_17;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToLocalPointInRectangle_m2398565080_MetadataUsageId;
extern "C"  bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m2398565080 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, Vector2_t2243707579  ___screenPoint1, Camera_t189460977 * ___cam2, Vector2_t2243707579 * ___localPoint3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToLocalPointInRectangle_m2398565080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Vector2_t2243707579 * L_0 = ___localPoint3;
		Vector2_t2243707579  L_1 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)L_0) = L_1;
		RectTransform_t3349966182 * L_2 = ___rect0;
		Vector2_t2243707579  L_3 = ___screenPoint1;
		Camera_t189460977 * L_4 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m2304638810(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		Vector2_t2243707579 * L_6 = ___localPoint3;
		RectTransform_t3349966182 * L_7 = ___rect0;
		Vector3_t2243707580  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t2243707580  L_9 = Transform_InverseTransformPoint_m2648491174(L_7, L_8, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)L_6) = L_10;
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_0035:
	{
		V_1 = (bool)0;
		goto IL_003c;
	}

IL_003c:
	{
		bool L_11 = V_1;
		return L_11;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToRay_m1842507230_MetadataUsageId;
extern "C"  Ray_t2469606224  RectTransformUtility_ScreenPointToRay_m1842507230 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, Vector2_t2243707579  ___screenPos1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToRay_m1842507230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_t189460977 * L_0 = ___cam0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Camera_t189460977 * L_2 = ___cam0;
		Vector2_t2243707579  L_3 = ___screenPos1;
		Vector3_t2243707580  L_4 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t2469606224  L_5 = Camera_ScreenPointToRay_m614889538(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_004a;
	}

IL_001f:
	{
		Vector2_t2243707579  L_6 = ___screenPos1;
		Vector3_t2243707580  L_7 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Vector3_t2243707580 * L_8 = (&V_1);
		float L_9 = L_8->get_z_3();
		L_8->set_z_3(((float)((float)L_9-(float)(100.0f))));
		Vector3_t2243707580  L_10 = V_1;
		Vector3_t2243707580  L_11 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t2469606224  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Ray__ctor_m3379034047(&L_12, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_004a;
	}

IL_004a:
	{
		Ray_t2469606224  L_13 = V_0;
		return L_13;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_FlipLayoutOnAxis_m3920364518_MetadataUsageId;
extern "C"  void RectTransformUtility_FlipLayoutOnAxis_m3920364518 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, int32_t ___axis1, bool ___keepPositioning2, bool ___recursive3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutOnAxis_m3920364518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t3349966182 * V_1 = NULL;
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2243707579  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	{
		RectTransform_t3349966182 * L_0 = ___rect0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_00f3;
	}

IL_0012:
	{
		bool L_2 = ___recursive3;
		if (!L_2)
		{
			goto IL_0055;
		}
	}
	{
		V_0 = 0;
		goto IL_0048;
	}

IL_0020:
	{
		RectTransform_t3349966182 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_5 = Transform_GetChild_m3838588184(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t3349966182 *)IsInstSealed(L_5, RectTransform_t3349966182_il2cpp_TypeInfo_var));
		RectTransform_t3349966182 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0043;
		}
	}
	{
		RectTransform_t3349966182 * L_8 = V_1;
		int32_t L_9 = ___axis1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m3920364518(NULL /*static, unused*/, L_8, L_9, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_11 = V_0;
		RectTransform_t3349966182 * L_12 = ___rect0;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m881385315(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0020;
		}
	}
	{
	}

IL_0055:
	{
		RectTransform_t3349966182 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t2243707579  L_15 = RectTransform_get_pivot_m759087479(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis1;
		int32_t L_17 = ___axis1;
		float L_18 = Vector2_get_Item_m2792130561((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m3881967114((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_19 = ___rect0;
		Vector2_t2243707579  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m1360548980(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning2;
		if (!L_21)
		{
			goto IL_0084;
		}
	}
	{
		goto IL_00f3;
	}

IL_0084:
	{
		RectTransform_t3349966182 * L_22 = ___rect0;
		NullCheck(L_22);
		Vector2_t2243707579  L_23 = RectTransform_get_anchoredPosition_m3570822376(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis1;
		int32_t L_25 = ___axis1;
		float L_26 = Vector2_get_Item_m2792130561((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m3881967114((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_27 = ___rect0;
		Vector2_t2243707579  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m2077229449(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_29 = ___rect0;
		NullCheck(L_29);
		Vector2_t2243707579  L_30 = RectTransform_get_anchorMin_m1497323108(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t3349966182 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t2243707579  L_32 = RectTransform_get_anchorMax_m3816015142(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis1;
		float L_34 = Vector2_get_Item_m2792130561((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis1;
		int32_t L_36 = ___axis1;
		float L_37 = Vector2_get_Item_m2792130561((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m3881967114((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis1;
		float L_39 = V_6;
		Vector2_set_Item_m3881967114((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_40 = ___rect0;
		Vector2_t2243707579  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m4247668187(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_42 = ___rect0;
		Vector2_t2243707579  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m2955899993(L_42, L_43, /*hidden argument*/NULL);
	}

IL_00f3:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_FlipLayoutAxes_m532748168_MetadataUsageId;
extern "C"  void RectTransformUtility_FlipLayoutAxes_m532748168 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, bool ___keepPositioning1, bool ___recursive2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutAxes_m532748168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t3349966182 * V_1 = NULL;
	{
		RectTransform_t3349966182 * L_0 = ___rect0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_00b4;
	}

IL_0012:
	{
		bool L_2 = ___recursive2;
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0020:
	{
		RectTransform_t3349966182 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_5 = Transform_GetChild_m3838588184(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t3349966182 *)IsInstSealed(L_5, RectTransform_t3349966182_il2cpp_TypeInfo_var));
		RectTransform_t3349966182 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		RectTransform_t3349966182 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m532748168(NULL /*static, unused*/, L_8, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_0042:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_10 = V_0;
		RectTransform_t3349966182 * L_11 = ___rect0;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m881385315(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0020;
		}
	}
	{
	}

IL_0054:
	{
		RectTransform_t3349966182 * L_13 = ___rect0;
		RectTransform_t3349966182 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t2243707579  L_15 = RectTransform_get_pivot_m759087479(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_16 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m1360548980(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_17 = ___rect0;
		RectTransform_t3349966182 * L_18 = ___rect0;
		NullCheck(L_18);
		Vector2_t2243707579  L_19 = RectTransform_get_sizeDelta_m2157326342(L_18, /*hidden argument*/NULL);
		Vector2_t2243707579  L_20 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m2319668137(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning1;
		if (!L_21)
		{
			goto IL_0081;
		}
	}
	{
		goto IL_00b4;
	}

IL_0081:
	{
		RectTransform_t3349966182 * L_22 = ___rect0;
		RectTransform_t3349966182 * L_23 = ___rect0;
		NullCheck(L_23);
		Vector2_t2243707579  L_24 = RectTransform_get_anchoredPosition_m3570822376(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_25 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m2077229449(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_26 = ___rect0;
		RectTransform_t3349966182 * L_27 = ___rect0;
		NullCheck(L_27);
		Vector2_t2243707579  L_28 = RectTransform_get_anchorMin_m1497323108(L_27, /*hidden argument*/NULL);
		Vector2_t2243707579  L_29 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m4247668187(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_30 = ___rect0;
		RectTransform_t3349966182 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t2243707579  L_32 = RectTransform_get_anchorMax_m3816015142(L_31, /*hidden argument*/NULL);
		Vector2_t2243707579  L_33 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m2955899993(L_30, L_33, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  RectTransformUtility_GetTransposed_m1770338235 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___input0, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___input0)->get_y_1();
		float L_1 = (&___input0)->get_x_0();
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001a;
	}

IL_001a:
	{
		Vector2_t2243707579  L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_RectangleContainsScreenPoint_m1244853728_MetadataUsageId;
extern "C"  bool RectTransformUtility_RectangleContainsScreenPoint_m1244853728 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, Vector2_t2243707579  ___screenPoint1, Camera_t189460977 * ___cam2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_RectangleContainsScreenPoint_m1244853728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RectTransform_t3349966182 * L_0 = ___rect0;
		Camera_t189460977 * L_1 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993(NULL /*static, unused*/, L_0, (&___screenPoint1), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, Vector2_t2243707579 * ___screenPoint1, Camera_t189460977 * ___cam2, const MethodInfo* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *, Camera_t189460977 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	return _il2cpp_icall_func(___rect0, ___screenPoint1, ___cam2);
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_PixelAdjustPoint_m560908615_MetadataUsageId;
extern "C"  Vector2_t2243707579  RectTransformUtility_PixelAdjustPoint_m560908615 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___point0, Transform_t3275118058 * ___elementTransform1, Canvas_t209405766 * ___canvas2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustPoint_m560908615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3275118058 * L_0 = ___elementTransform1;
		Canvas_t209405766 * L_1 = ___canvas2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669(NULL /*static, unused*/, (&___point0), L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = V_0;
		V_1 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Vector2_t2243707579  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___point0, Transform_t3275118058 * ___elementTransform1, Canvas_t209405766 * ___canvas2, Vector2_t2243707579 * ___value3, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669_ftn) (Vector2_t2243707579 *, Transform_t3275118058 *, Canvas_t209405766 *, Vector2_t2243707579 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point0, ___elementTransform1, ___canvas2, ___value3);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_PixelAdjustRect_m93024038_MetadataUsageId;
extern "C"  Rect_t3681755626  RectTransformUtility_PixelAdjustRect_m93024038 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rectTransform0, Canvas_t209405766 * ___canvas1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustRect_m93024038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_t3349966182 * L_0 = ___rectTransform0;
		Canvas_t209405766 * L_1 = ___canvas1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_2 = V_0;
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		Rect_t3681755626  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rectTransform0, Canvas_t209405766 * ___canvas1, Rect_t3681755626 * ___value2, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542_ftn) (RectTransform_t3349966182 *, Canvas_t209405766 *, Rect_t3681755626 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)");
	_il2cpp_icall_func(___rectTransform0, ___canvas1, ___value2);
}
// System.Void UnityEngine.RectTransformUtility::.cctor()
extern Il2CppClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility__cctor_m1866023382_MetadataUsageId;
extern "C"  void RectTransformUtility__cctor_m1866023382 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility__cctor_m1866023382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((RectTransformUtility_t2941082270_StaticFields*)RectTransformUtility_t2941082270_il2cpp_TypeInfo_var->static_fields)->set_s_Corners_0(((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)4)));
		return;
	}
}
// System.Void UnityEngine.RemoteSettings::CallOnUpdate()
extern Il2CppClass* RemoteSettings_t392466225_il2cpp_TypeInfo_var;
extern const uint32_t RemoteSettings_CallOnUpdate_m1624968574_MetadataUsageId;
extern "C"  void RemoteSettings_CallOnUpdate_m1624968574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RemoteSettings_CallOnUpdate_m1624968574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UpdatedEventHandler_t3033456180 * V_0 = NULL;
	{
		UpdatedEventHandler_t3033456180 * L_0 = ((RemoteSettings_t392466225_StaticFields*)RemoteSettings_t392466225_il2cpp_TypeInfo_var->static_fields)->get_Updated_0();
		V_0 = L_0;
		UpdatedEventHandler_t3033456180 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		UpdatedEventHandler_t3033456180 * L_2 = V_0;
		NullCheck(L_2);
		UpdatedEventHandler_Invoke_m159598802(L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdatedEventHandler__ctor_m1393569768 (UpdatedEventHandler_t3033456180 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::Invoke()
extern "C"  void UpdatedEventHandler_Invoke_m159598802 (UpdatedEventHandler_t3033456180 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UpdatedEventHandler_Invoke_m159598802((UpdatedEventHandler_t3033456180 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180 (UpdatedEventHandler_t3033456180 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.RemoteSettings/UpdatedEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdatedEventHandler_BeginInvoke_m4238510153 (UpdatedEventHandler_t3033456180 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UpdatedEventHandler_EndInvoke_m224684362 (UpdatedEventHandler_t3033456180 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Boolean UnityEngine.Renderer::get_enabled()
extern "C"  bool Renderer_get_enabled_m2362836534 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef bool (*Renderer_get_enabled_m2362836534_ftn) (Renderer_t257310565 *);
	static Renderer_get_enabled_m2362836534_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_enabled_m2362836534_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m142717579 (Renderer_t257310565 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_enabled_m142717579_ftn) (Renderer_t257310565 *, bool);
	static Renderer_set_enabled_m142717579_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_enabled_m142717579_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::set_shadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)
extern "C"  void Renderer_set_shadowCastingMode_m2840732934 (Renderer_t257310565 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_shadowCastingMode_m2840732934_ftn) (Renderer_t257310565 *, int32_t);
	static Renderer_set_shadowCastingMode_m2840732934_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_shadowCastingMode_m2840732934_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_shadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::set_receiveShadows(System.Boolean)
extern "C"  void Renderer_set_receiveShadows_m2366149940 (Renderer_t257310565 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_receiveShadows_m2366149940_ftn) (Renderer_t257310565 *, bool);
	static Renderer_set_receiveShadows_m2366149940_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_receiveShadows_m2366149940_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_receiveShadows(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t193706927 * Renderer_get_material_m2553789785 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef Material_t193706927 * (*Renderer_get_material_m2553789785_ftn) (Renderer_t257310565 *);
	static Renderer_get_material_m2553789785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_material_m2553789785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C"  void Renderer_set_material_m1053097112 (Renderer_t257310565 * __this, Material_t193706927 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_material_m1053097112_ftn) (Renderer_t257310565 *, Material_t193706927 *);
	static Renderer_set_material_m1053097112_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_material_m1053097112_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
extern "C"  Material_t193706927 * Renderer_get_sharedMaterial_m155010392 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef Material_t193706927 * (*Renderer_get_sharedMaterial_m155010392_ftn) (Renderer_t257310565 *);
	static Renderer_get_sharedMaterial_m155010392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterial_m155010392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterial()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
extern "C"  void Renderer_set_sharedMaterial_m391095487 (Renderer_t257310565 * __this, Material_t193706927 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sharedMaterial_m391095487_ftn) (Renderer_t257310565 *, Material_t193706927 *);
	static Renderer_set_sharedMaterial_m391095487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterial_m391095487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_materials()
extern "C"  MaterialU5BU5D_t3123989686* Renderer_get_materials_m810004692 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t3123989686* (*Renderer_get_materials_m810004692_ftn) (Renderer_t257310565 *);
	static Renderer_get_materials_m810004692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_materials_m810004692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_materials()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_materials(UnityEngine.Material[])
extern "C"  void Renderer_set_materials_m1556465155 (Renderer_t257310565 * __this, MaterialU5BU5D_t3123989686* ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_materials_m1556465155_ftn) (Renderer_t257310565 *, MaterialU5BU5D_t3123989686*);
	static Renderer_set_materials_m1556465155_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_materials_m1556465155_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_materials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_sharedMaterials()
extern "C"  MaterialU5BU5D_t3123989686* Renderer_get_sharedMaterials_m4026934221 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t3123989686* (*Renderer_get_sharedMaterials_m4026934221_ftn) (Renderer_t257310565 *);
	static Renderer_get_sharedMaterials_m4026934221_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterials_m4026934221_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterials()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
extern "C"  void Renderer_set_sharedMaterials_m2669445156 (Renderer_t257310565 * __this, MaterialU5BU5D_t3123989686* ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sharedMaterials_m2669445156_ftn) (Renderer_t257310565 *, MaterialU5BU5D_t3123989686*);
	static Renderer_set_sharedMaterials_m2669445156_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterials_m2669445156_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C"  Bounds_t3033363703  Renderer_get_bounds_m3832626589 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	Bounds_t3033363703  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Bounds_t3033363703  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Renderer_INTERNAL_get_bounds_m1299274192(__this, (&V_0), /*hidden argument*/NULL);
		Bounds_t3033363703  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Bounds_t3033363703  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C"  void Renderer_INTERNAL_get_bounds_m1299274192 (Renderer_t257310565 * __this, Bounds_t3033363703 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_INTERNAL_get_bounds_m1299274192_ftn) (Renderer_t257310565 *, Bounds_t3033363703 *);
	static Renderer_INTERNAL_get_bounds_m1299274192_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_INTERNAL_get_bounds_m1299274192_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::set_lightProbeUsage(UnityEngine.Rendering.LightProbeUsage)
extern "C"  void Renderer_set_lightProbeUsage_m1212616902 (Renderer_t257310565 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_lightProbeUsage_m1212616902_ftn) (Renderer_t257310565 *, int32_t);
	static Renderer_set_lightProbeUsage_m1212616902_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_lightProbeUsage_m1212616902_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_lightProbeUsage(UnityEngine.Rendering.LightProbeUsage)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::set_reflectionProbeUsage(UnityEngine.Rendering.ReflectionProbeUsage)
extern "C"  void Renderer_set_reflectionProbeUsage_m2405564324 (Renderer_t257310565 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_reflectionProbeUsage_m2405564324_ftn) (Renderer_t257310565 *, int32_t);
	static Renderer_set_reflectionProbeUsage_m2405564324_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_reflectionProbeUsage_m2405564324_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_reflectionProbeUsage(UnityEngine.Rendering.ReflectionProbeUsage)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern "C"  void Renderer_SetPropertyBlock_m1151988246 (Renderer_t257310565 * __this, MaterialPropertyBlock_t3303648957 * ___properties0, const MethodInfo* method)
{
	typedef void (*Renderer_SetPropertyBlock_m1151988246_ftn) (Renderer_t257310565 *, MaterialPropertyBlock_t3303648957 *);
	static Renderer_SetPropertyBlock_m1151988246_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_SetPropertyBlock_m1151988246_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)");
	_il2cpp_icall_func(__this, ___properties0);
}
// System.Void UnityEngine.Renderer::GetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern "C"  void Renderer_GetPropertyBlock_m827466882 (Renderer_t257310565 * __this, MaterialPropertyBlock_t3303648957 * ___dest0, const MethodInfo* method)
{
	typedef void (*Renderer_GetPropertyBlock_m827466882_ftn) (Renderer_t257310565 *, MaterialPropertyBlock_t3303648957 *);
	static Renderer_GetPropertyBlock_m827466882_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_GetPropertyBlock_m827466882_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::GetPropertyBlock(UnityEngine.MaterialPropertyBlock)");
	_il2cpp_icall_func(__this, ___dest0);
}
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern "C"  int32_t Renderer_get_sortingLayerID_m2403577271 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingLayerID_m2403577271_ftn) (Renderer_t257310565 *);
	static Renderer_get_sortingLayerID_m2403577271_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingLayerID_m2403577271_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern "C"  int32_t Renderer_get_sortingOrder_m1544525007 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingOrder_m1544525007_ftn) (Renderer_t257310565 *);
	static Renderer_get_sortingOrder_m1544525007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingOrder_m1544525007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.FogMode UnityEngine.RenderSettings::get_fogMode()
extern "C"  int32_t RenderSettings_get_fogMode_m2733924375 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*RenderSettings_get_fogMode_m2733924375_ftn) ();
	static RenderSettings_get_fogMode_m2733924375_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_fogMode_m2733924375_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_fogMode()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.RenderSettings::get_fogDensity()
extern "C"  float RenderSettings_get_fogDensity_m943703443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_fogDensity_m943703443_ftn) ();
	static RenderSettings_get_fogDensity_m943703443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_fogDensity_m943703443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_fogDensity()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.RenderSettings::get_fogStartDistance()
extern "C"  float RenderSettings_get_fogStartDistance_m1708962516 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_fogStartDistance_m1708962516_ftn) ();
	static RenderSettings_get_fogStartDistance_m1708962516_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_fogStartDistance_m1708962516_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_fogStartDistance()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.RenderSettings::get_fogEndDistance()
extern "C"  float RenderSettings_get_fogEndDistance_m110551859 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_fogEndDistance_m110551859_ftn) ();
	static RenderSettings_get_fogEndDistance_m110551859_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_fogEndDistance_m110551859_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_fogEndDistance()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture__ctor_m2960228168 (RenderTexture_t2666733923 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, int32_t ___format3, const MethodInfo* method)
{
	{
		Texture__ctor_m4198984292(__this, /*hidden argument*/NULL);
		RenderTexture_Internal_CreateRenderTexture_m1679754911(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		VirtActionInvoker1< int32_t >::Invoke(5 /* System.Void UnityEngine.Texture::set_width(System.Int32) */, __this, L_0);
		int32_t L_1 = ___height1;
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void UnityEngine.Texture::set_height(System.Int32) */, __this, L_1);
		int32_t L_2 = ___depth2;
		RenderTexture_set_depth_m3365909665(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___format3;
		RenderTexture_set_format_m2928390123(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = QualitySettings_get_activeColorSpace_m580875098(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_Internal_SetSRGBReadWrite_m286621198(NULL /*static, unused*/, __this, (bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
extern "C"  void RenderTexture__ctor_m4075605457 (RenderTexture_t2666733923 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, const MethodInfo* method)
{
	{
		Texture__ctor_m4198984292(__this, /*hidden argument*/NULL);
		RenderTexture_Internal_CreateRenderTexture_m1679754911(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		VirtActionInvoker1< int32_t >::Invoke(5 /* System.Void UnityEngine.Texture::set_width(System.Int32) */, __this, L_0);
		int32_t L_1 = ___height1;
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void UnityEngine.Texture::set_height(System.Int32) */, __this, L_1);
		int32_t L_2 = ___depth2;
		RenderTexture_set_depth_m3365909665(__this, L_2, /*hidden argument*/NULL);
		RenderTexture_set_format_m2928390123(__this, 7, /*hidden argument*/NULL);
		int32_t L_3 = QualitySettings_get_activeColorSpace_m580875098(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_Internal_SetSRGBReadWrite_m286621198(NULL /*static, unused*/, __this, (bool)((((int32_t)L_3) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_Internal_CreateRenderTexture_m1679754911 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___rt0, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_CreateRenderTexture_m1679754911_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_Internal_CreateRenderTexture_m1679754911_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_CreateRenderTexture_m1679754911_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___rt0);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)
extern "C"  RenderTexture_t2666733923 * RenderTexture_GetTemporary_m1927959284 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, int32_t ___readWrite4, int32_t ___antiAliasing5, const MethodInfo* method)
{
	typedef RenderTexture_t2666733923 * (*RenderTexture_GetTemporary_m1927959284_ftn) (int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	static RenderTexture_GetTemporary_m1927959284_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetTemporary_m1927959284_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)");
	return _il2cpp_icall_func(___width0, ___height1, ___depthBuffer2, ___format3, ___readWrite4, ___antiAliasing5);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern "C"  RenderTexture_t2666733923 * RenderTexture_GetTemporary_m1800623300 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RenderTexture_t2666733923 * V_2 = NULL;
	{
		V_0 = 1;
		V_1 = 0;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___depthBuffer2;
		int32_t L_3 = ___format3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RenderTexture_t2666733923 * L_6 = RenderTexture_GetTemporary_m1927959284(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		goto IL_0016;
	}

IL_0016:
	{
		RenderTexture_t2666733923 * L_7 = V_2;
		return L_7;
	}
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32)
extern "C"  RenderTexture_t2666733923 * RenderTexture_GetTemporary_m1924862769 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	RenderTexture_t2666733923 * V_3 = NULL;
	{
		V_0 = 1;
		V_1 = 0;
		V_2 = 7;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___depthBuffer2;
		int32_t L_3 = V_2;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RenderTexture_t2666733923 * L_6 = RenderTexture_GetTemporary_m1927959284(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		goto IL_0018;
	}

IL_0018:
	{
		RenderTexture_t2666733923 * L_7 = V_3;
		return L_7;
	}
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32)
extern "C"  RenderTexture_t2666733923 * RenderTexture_GetTemporary_m2226167488 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	RenderTexture_t2666733923 * V_4 = NULL;
	{
		V_0 = 1;
		V_1 = 0;
		V_2 = 7;
		V_3 = 0;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = V_3;
		int32_t L_3 = V_2;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RenderTexture_t2666733923 * L_6 = RenderTexture_GetTemporary_m1927959284(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_4 = L_6;
		goto IL_001b;
	}

IL_001b:
	{
		RenderTexture_t2666733923 * L_7 = V_4;
		return L_7;
	}
}
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_ReleaseTemporary_m1186631014 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___temp0, const MethodInfo* method)
{
	typedef void (*RenderTexture_ReleaseTemporary_m1186631014_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_ReleaseTemporary_m1186631014_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_ReleaseTemporary_m1186631014_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___temp0);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m2317917654 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m2317917654_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_Internal_GetWidth_m2317917654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m2317917654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetWidth_m989760279 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, int32_t ___width1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetWidth_m989760279_ftn) (RenderTexture_t2666733923 *, int32_t);
	static RenderTexture_Internal_SetWidth_m989760279_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetWidth_m989760279_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono0, ___width1);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m2780941261 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m2780941261_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_Internal_GetHeight_m2780941261_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m2780941261_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetHeight_m2900429544 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, int32_t ___width1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetHeight_m2900429544_ftn) (RenderTexture_t2666733923 *, int32_t);
	static RenderTexture_Internal_SetHeight_m2900429544_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetHeight_m2900429544_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono0, ___width1);
}
// System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
extern "C"  void RenderTexture_Internal_SetSRGBReadWrite_m286621198 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, bool ___sRGB1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetSRGBReadWrite_m286621198_ftn) (RenderTexture_t2666733923 *, bool);
	static RenderTexture_Internal_SetSRGBReadWrite_m286621198_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetSRGBReadWrite_m286621198_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)");
	_il2cpp_icall_func(___mono0, ___sRGB1);
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C"  int32_t RenderTexture_get_width_m1471807677 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m2317917654(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.RenderTexture::set_width(System.Int32)
extern "C"  void RenderTexture_set_width_m592588466 (RenderTexture_t2666733923 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		RenderTexture_Internal_SetWidth_m989760279(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C"  int32_t RenderTexture_get_height_m1108175848 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m2780941261(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.RenderTexture::set_height(System.Int32)
extern "C"  void RenderTexture_set_height_m1787187 (RenderTexture_t2666733923 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		RenderTexture_Internal_SetHeight_m2900429544(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern "C"  void RenderTexture_set_depth_m3365909665 (RenderTexture_t2666733923 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_depth_m3365909665_ftn) (RenderTexture_t2666733923 *, int32_t);
	static RenderTexture_set_depth_m3365909665_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_depth_m3365909665_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_depth(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RenderTextureFormat UnityEngine.RenderTexture::get_format()
extern "C"  int32_t RenderTexture_get_format_m2563201870 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_get_format_m2563201870_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_get_format_m2563201870_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_format_m2563201870_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_format()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture_set_format_m2928390123 (RenderTexture_t2666733923 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_format_m2928390123_ftn) (RenderTexture_t2666733923 *, int32_t);
	static RenderTexture_set_format_m2928390123_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_format_m2928390123_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RenderTexture::set_antiAliasing(System.Int32)
extern "C"  void RenderTexture_set_antiAliasing_m1838970818 (RenderTexture_t2666733923 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_antiAliasing_m1838970818_ftn) (RenderTexture_t2666733923 *, int32_t);
	static RenderTexture_set_antiAliasing_m1838970818_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_antiAliasing_m1838970818_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_antiAliasing(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.RenderTexture::Create()
extern "C"  bool RenderTexture_Create_m1259588374 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = RenderTexture_INTERNAL_CALL_Create_m4215814471(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_INTERNAL_CALL_Create_m4215814471 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___self0, const MethodInfo* method)
{
	typedef bool (*RenderTexture_INTERNAL_CALL_Create_m4215814471_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_INTERNAL_CALL_Create_m4215814471_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_Create_m4215814471_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.RenderTexture::Release()
extern "C"  void RenderTexture_Release_m1476285593 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	{
		RenderTexture_INTERNAL_CALL_Release_m1818573204(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_Release_m1818573204 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___self0, const MethodInfo* method)
{
	typedef void (*RenderTexture_INTERNAL_CALL_Release_m1818573204_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_INTERNAL_CALL_Release_m1818573204_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_Release_m1818573204_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.RenderTexture::IsCreated()
extern "C"  bool RenderTexture_IsCreated_m375737400 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = RenderTexture_INTERNAL_CALL_IsCreated_m2796064041(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_INTERNAL_CALL_IsCreated_m2796064041 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___self0, const MethodInfo* method)
{
	typedef bool (*RenderTexture_INTERNAL_CALL_IsCreated_m2796064041_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_INTERNAL_CALL_IsCreated_m2796064041_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_IsCreated_m2796064041_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.RenderTexture::DiscardContents()
extern "C"  void RenderTexture_DiscardContents_m748499988 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	{
		RenderTexture_INTERNAL_CALL_DiscardContents_m4035611219(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_DiscardContents(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_DiscardContents_m4035611219 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___self0, const MethodInfo* method)
{
	typedef void (*RenderTexture_INTERNAL_CALL_DiscardContents_m4035611219_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_INTERNAL_CALL_DiscardContents_m4035611219_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_DiscardContents_m4035611219_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_DiscardContents(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.RenderTexture::MarkRestoreExpected()
extern "C"  void RenderTexture_MarkRestoreExpected_m2952280671 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	{
		RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m181526572(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_MarkRestoreExpected(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m181526572 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___self0, const MethodInfo* method)
{
	typedef void (*RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m181526572_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m181526572_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m181526572_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_MarkRestoreExpected(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_colorBuffer()
extern "C"  RenderBuffer_t2767087968  RenderTexture_get_colorBuffer_m2259248236 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	RenderBuffer_t2767087968  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RenderBuffer_t2767087968  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RenderTexture_GetColorBuffer_m2909232564(__this, (&V_0), /*hidden argument*/NULL);
		RenderBuffer_t2767087968  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		RenderBuffer_t2767087968  L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_depthBuffer()
extern "C"  RenderBuffer_t2767087968  RenderTexture_get_depthBuffer_m583581910 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	RenderBuffer_t2767087968  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RenderBuffer_t2767087968  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RenderTexture_GetDepthBuffer_m1144066090(__this, (&V_0), /*hidden argument*/NULL);
		RenderBuffer_t2767087968  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		RenderBuffer_t2767087968  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)
extern "C"  void RenderTexture_GetColorBuffer_m2909232564 (RenderTexture_t2666733923 * __this, RenderBuffer_t2767087968 * ___res0, const MethodInfo* method)
{
	typedef void (*RenderTexture_GetColorBuffer_m2909232564_ftn) (RenderTexture_t2666733923 *, RenderBuffer_t2767087968 *);
	static RenderTexture_GetColorBuffer_m2909232564_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetColorBuffer_m2909232564_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(__this, ___res0);
}
// System.Void UnityEngine.RenderTexture::GetDepthBuffer(UnityEngine.RenderBuffer&)
extern "C"  void RenderTexture_GetDepthBuffer_m1144066090 (RenderTexture_t2666733923 * __this, RenderBuffer_t2767087968 * ___res0, const MethodInfo* method)
{
	typedef void (*RenderTexture_GetDepthBuffer_m1144066090_ftn) (RenderTexture_t2666733923 *, RenderBuffer_t2767087968 *);
	static RenderTexture_GetDepthBuffer_m1144066090_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetDepthBuffer_m1144066090_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetDepthBuffer(UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(__this, ___res0);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::get_active()
extern "C"  RenderTexture_t2666733923 * RenderTexture_get_active_m561615752 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef RenderTexture_t2666733923 * (*RenderTexture_get_active_m561615752_ftn) ();
	static RenderTexture_get_active_m561615752_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_active_m561615752_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_active()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_set_active_m55464043 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_active_m55464043_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_set_active_m55464043_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_active_m55464043_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C"  void RequireComponent__ctor_m3475141952 (RequireComponent_t864575032 * __this, Type_t * ___requiredComponent0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		return;
	}
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type,System.Type)
extern "C"  void RequireComponent__ctor_m2072403951 (RequireComponent_t864575032 * __this, Type_t * ___requiredComponent0, Type_t * ___requiredComponent21, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		Type_t * L_1 = ___requiredComponent21;
		__this->set_m_Type1_1(L_1);
		return;
	}
}
// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C"  void ResourceRequest__ctor_m3340010930 (ResourceRequest_t2560315377 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m2914860946(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C"  Object_t1021602117 * ResourceRequest_get_asset_m3527928488 (ResourceRequest_t2560315377 * __this, const MethodInfo* method)
{
	Object_t1021602117 * V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_Path_1();
		Type_t * L_1 = __this->get_m_Type_2();
		Object_t1021602117 * L_2 = Resources_Load_m243305716(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Object_t1021602117 * L_3 = V_0;
		return L_3;
	}
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke(const ResourceRequest_t2560315377& unmarshaled, ResourceRequest_t2560315377_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke_back(const ResourceRequest_t2560315377_marshaled_pinvoke& marshaled, ResourceRequest_t2560315377& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke_cleanup(ResourceRequest_t2560315377_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2560315377_marshal_com(const ResourceRequest_t2560315377& unmarshaled, ResourceRequest_t2560315377_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t2560315377_marshal_com_back(const ResourceRequest_t2560315377_marshaled_com& marshaled, ResourceRequest_t2560315377& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2560315377_marshal_com_cleanup(ResourceRequest_t2560315377_marshaled_com& marshaled)
{
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern const Il2CppType* Object_t1021602117_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_Load_m2041782325_MetadataUsageId;
extern "C"  Object_t1021602117 * Resources_Load_m2041782325 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Resources_Load_m2041782325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Object_t1021602117_0_0_0_var), /*hidden argument*/NULL);
		Object_t1021602117 * L_2 = Resources_Load_m243305716(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Object_t1021602117 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t1021602117 * Resources_Load_m243305716 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Resources_Load_m243305716_ftn) (String_t*, Type_t *);
	static Resources_Load_m243305716_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_Load_m243305716_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::Load(System.String,System.Type)");
	return _il2cpp_icall_func(___path0, ___systemTypeInstance1);
}
// UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
extern "C"  Object_t1021602117 * Resources_GetBuiltinResource_m582410469 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___path1, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Resources_GetBuiltinResource_m582410469_ftn) (Type_t *, String_t*);
	static Resources_GetBuiltinResource_m582410469_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_GetBuiltinResource_m582410469_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)");
	return _il2cpp_icall_func(___type0, ___path1);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern "C"  Vector3_t2243707580  Rigidbody_get_velocity_m2022666970 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Rigidbody_INTERNAL_get_velocity_m1938461825(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_velocity_m2514070071 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_velocity_m3384354677(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_velocity_m1938461825 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_velocity_m1938461825_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_get_velocity_m1938461825_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_velocity_m1938461825_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_velocity_m3384354677 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_velocity_m3384354677_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_set_velocity_m3384354677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_velocity_m3384354677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_angularVelocity()
extern "C"  Vector3_t2243707580  Rigidbody_get_angularVelocity_m3820121000 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Rigidbody_INTERNAL_get_angularVelocity_m2914925467(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody::set_angularVelocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_angularVelocity_m824394045 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_angularVelocity_m3276405927(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_angularVelocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_angularVelocity_m2914925467 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_angularVelocity_m2914925467_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_get_angularVelocity_m2914925467_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_angularVelocity_m2914925467_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_angularVelocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_angularVelocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_angularVelocity_m3276405927 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_angularVelocity_m3276405927_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_set_angularVelocity_m3276405927_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_angularVelocity_m3276405927_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_angularVelocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_drag(System.Single)
extern "C"  void Rigidbody_set_drag_m431766562 (Rigidbody_t4233889191 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_drag_m431766562_ftn) (Rigidbody_t4233889191 *, float);
	static Rigidbody_set_drag_m431766562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_drag_m431766562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_drag(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_angularDrag(System.Single)
extern "C"  void Rigidbody_set_angularDrag_m1042416512 (Rigidbody_t4233889191 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_angularDrag_m1042416512_ftn) (Rigidbody_t4233889191 *, float);
	static Rigidbody_set_angularDrag_m1042416512_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_angularDrag_m1042416512_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_angularDrag(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_mass(System.Single)
extern "C"  void Rigidbody_set_mass_m4155402692 (Rigidbody_t4233889191 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_mass_m4155402692_ftn) (Rigidbody_t4233889191 *, float);
	static Rigidbody_set_mass_m4155402692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_mass_m4155402692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_mass(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_useGravity(System.Boolean)
extern "C"  void Rigidbody_set_useGravity_m2606656539 (Rigidbody_t4233889191 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_useGravity_m2606656539_ftn) (Rigidbody_t4233889191 *, bool);
	static Rigidbody_set_useGravity_m2606656539_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_useGravity_m2606656539_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_useGravity(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody::get_isKinematic()
extern "C"  bool Rigidbody_get_isKinematic_m2907467582 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody_get_isKinematic_m2907467582_ftn) (Rigidbody_t4233889191 *);
	static Rigidbody_get_isKinematic_m2907467582_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_isKinematic_m2907467582_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_isKinematic()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
extern "C"  void Rigidbody_set_isKinematic_m738793415 (Rigidbody_t4233889191 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_isKinematic_m738793415_ftn) (Rigidbody_t4233889191 *, bool);
	static Rigidbody_set_isKinematic_m738793415_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_isKinematic_m738793415_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_isKinematic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
extern "C"  void Rigidbody_set_freezeRotation_m2131864169 (Rigidbody_t4233889191 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_freezeRotation_m2131864169_ftn) (Rigidbody_t4233889191 *, bool);
	static Rigidbody_set_freezeRotation_m2131864169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_freezeRotation_m2131864169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)
extern "C"  void Rigidbody_set_constraints_m3474016139 (Rigidbody_t4233889191 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_constraints_m3474016139_ftn) (Rigidbody_t4233889191 *, int32_t);
	static Rigidbody_set_constraints_m3474016139_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_constraints_m3474016139_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_collisionDetectionMode(UnityEngine.CollisionDetectionMode)
extern "C"  void Rigidbody_set_collisionDetectionMode_m314488966 (Rigidbody_t4233889191 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_collisionDetectionMode_m314488966_ftn) (Rigidbody_t4233889191 *, int32_t);
	static Rigidbody_set_collisionDetectionMode_m314488966_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_collisionDetectionMode_m314488966_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_collisionDetectionMode(UnityEngine.CollisionDetectionMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForce_m3219459786 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddForce_m3164777073(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
extern "C"  void Rigidbody_AddForce_m2836187433 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___force0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddForce_m3164777073(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForce_m3164777073 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForce_m3164777073_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForce_m3164777073_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForce_m3164777073_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddRelativeForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddRelativeForce_m2360431750 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddRelativeForce_m394474805(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddRelativeForce(UnityEngine.Vector3)
extern "C"  void Rigidbody_AddRelativeForce_m1733818429 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___force0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddRelativeForce_m394474805(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddRelativeForce_m394474805 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddRelativeForce_m394474805_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddRelativeForce_m394474805_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddRelativeForce_m394474805_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3)
extern "C"  void Rigidbody_AddTorque_m2680584150 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___torque0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddTorque_m929838442(NULL /*static, unused*/, __this, (&___torque0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddTorque_m929838442 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___torque1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddTorque_m929838442_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddTorque_m929838442_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddTorque_m929838442_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___torque1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForceAtPosition_m4129134921 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___force0, Vector3_t2243707580  ___position1, int32_t ___mode2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode2;
		Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522(NULL /*static, unused*/, __this, (&___force0), (&___position1), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___force1, Vector3_t2243707580 * ___position2, int32_t ___mode3, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *, Vector3_t2243707580 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___position2, ___mode3);
}
// System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single)
extern "C"  void Rigidbody_AddExplosionForce_m326026882 (Rigidbody_t4233889191 * __this, float ___explosionForce0, Vector3_t2243707580  ___explosionPosition1, float ___explosionRadius2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = 0;
		V_1 = (0.0f);
		float L_0 = ___explosionForce0;
		float L_1 = ___explosionRadius2;
		float L_2 = V_1;
		int32_t L_3 = V_0;
		Rigidbody_INTERNAL_CALL_AddExplosionForce_m3156773993(NULL /*static, unused*/, __this, L_0, (&___explosionPosition1), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddExplosionForce(UnityEngine.Rigidbody,System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddExplosionForce_m3156773993 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, float ___explosionForce1, Vector3_t2243707580 * ___explosionPosition2, float ___explosionRadius3, float ___upwardsModifier4, int32_t ___mode5, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddExplosionForce_m3156773993_ftn) (Rigidbody_t4233889191 *, float, Vector3_t2243707580 *, float, float, int32_t);
	static Rigidbody_INTERNAL_CALL_AddExplosionForce_m3156773993_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddExplosionForce_m3156773993_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddExplosionForce(UnityEngine.Rigidbody,System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___explosionForce1, ___explosionPosition2, ___explosionRadius3, ___upwardsModifier4, ___mode5);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::GetPointVelocity(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Rigidbody_GetPointVelocity_m1454837702 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___worldPoint0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Rigidbody_INTERNAL_CALL_GetPointVelocity_m726787474(NULL /*static, unused*/, __this, (&___worldPoint0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_GetPointVelocity(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_GetPointVelocity_m726787474 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___worldPoint1, Vector3_t2243707580 * ___value2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_GetPointVelocity_m726787474_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_CALL_GetPointVelocity_m726787474_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_GetPointVelocity_m726787474_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_GetPointVelocity(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___worldPoint1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_centerOfMass()
extern "C"  Vector3_t2243707580  Rigidbody_get_centerOfMass_m3477767353 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Rigidbody_INTERNAL_get_centerOfMass_m1288715756(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_centerOfMass(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_centerOfMass_m1288715756 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_centerOfMass_m1288715756_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_get_centerOfMass_m1288715756_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_centerOfMass_m1288715756_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_centerOfMass(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_worldCenterOfMass()
extern "C"  Vector3_t2243707580  Rigidbody_get_worldCenterOfMass_m2685834503 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Rigidbody_INTERNAL_get_worldCenterOfMass_m82455696(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_worldCenterOfMass(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_worldCenterOfMass_m82455696 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_worldCenterOfMass_m82455696_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_get_worldCenterOfMass_m82455696_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_worldCenterOfMass_m82455696_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_worldCenterOfMass(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_detectCollisions(System.Boolean)
extern "C"  void Rigidbody_set_detectCollisions_m3576511480 (Rigidbody_t4233889191 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_detectCollisions_m3576511480_ftn) (Rigidbody_t4233889191 *, bool);
	static Rigidbody_set_detectCollisions_m3576511480_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_detectCollisions_m3576511480_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_detectCollisions(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_position()
extern "C"  Vector3_t2243707580  Rigidbody_get_position_m3465583110 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Rigidbody_INTERNAL_get_position_m2227872991(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody::set_position(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_position_m47523445 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_position_m447802091(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_position_m2227872991 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_position_m2227872991_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_get_position_m2227872991_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_position_m2227872991_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_position_m447802091 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_position_m447802091_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_set_position_m447802091_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_position_m447802091_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Rigidbody::get_rotation()
extern "C"  Quaternion_t4030073918  Rigidbody_get_rotation_m4203325509 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Rigidbody_INTERNAL_get_rotation_m109669412(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Quaternion_t4030073918  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody::set_rotation(UnityEngine.Quaternion)
extern "C"  void Rigidbody_set_rotation_m2752020144 (Rigidbody_t4233889191 * __this, Quaternion_t4030073918  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_rotation_m1385731864(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_get_rotation_m109669412 (Rigidbody_t4233889191 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_rotation_m109669412_ftn) (Rigidbody_t4233889191 *, Quaternion_t4030073918 *);
	static Rigidbody_INTERNAL_get_rotation_m109669412_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_rotation_m109669412_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_set_rotation_m1385731864 (Rigidbody_t4233889191 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_rotation_m1385731864_ftn) (Rigidbody_t4233889191 *, Quaternion_t4030073918 *);
	static Rigidbody_INTERNAL_set_rotation_m1385731864_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_rotation_m1385731864_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
extern "C"  void Rigidbody_MovePosition_m1810529681 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MovePosition_m2709492198(NULL /*static, unused*/, __this, (&___position0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_MovePosition_m2709492198 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___position1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MovePosition_m2709492198_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_CALL_MovePosition_m2709492198_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MovePosition_m2709492198_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1);
}
// System.Void UnityEngine.Rigidbody::MoveRotation(UnityEngine.Quaternion)
extern "C"  void Rigidbody_MoveRotation_m3412525692 (Rigidbody_t4233889191 * __this, Quaternion_t4030073918  ___rot0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919(NULL /*static, unused*/, __this, (&___rot0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Quaternion_t4030073918 * ___rot1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919_ftn) (Rigidbody_t4233889191 *, Quaternion_t4030073918 *);
	static Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self0, ___rot1);
}
// System.Void UnityEngine.Rigidbody::set_interpolation(UnityEngine.RigidbodyInterpolation)
extern "C"  void Rigidbody_set_interpolation_m3914346443 (Rigidbody_t4233889191 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_interpolation_m3914346443_ftn) (Rigidbody_t4233889191 *, int32_t);
	static Rigidbody_set_interpolation_m3914346443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_interpolation_m3914346443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_interpolation(UnityEngine.RigidbodyInterpolation)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::Sleep()
extern "C"  void Rigidbody_Sleep_m416287251 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_Sleep_m493241514(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)
extern "C"  void Rigidbody_INTERNAL_CALL_Sleep_m493241514 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_Sleep_m493241514_ftn) (Rigidbody_t4233889191 *);
	static Rigidbody_INTERNAL_CALL_Sleep_m493241514_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_Sleep_m493241514_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.Rigidbody::set_maxAngularVelocity(System.Single)
extern "C"  void Rigidbody_set_maxAngularVelocity_m2868165033 (Rigidbody_t4233889191 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_maxAngularVelocity_m2868165033_ftn) (Rigidbody_t4233889191 *, float);
	static Rigidbody_set_maxAngularVelocity_m2868165033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_maxAngularVelocity_m2868165033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_maxAngularVelocity(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RPC::.ctor()
extern "C"  void RPC__ctor_m1432086380 (RPC_t3323229423 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
extern "C"  void RuntimeInitializeOnLoadMethodAttribute__ctor_m1066490054 (RuntimeInitializeOnLoadMethodAttribute_t3126475234 * __this, int32_t ___loadType0, const MethodInfo* method)
{
	{
		PreserveAttribute__ctor_m2437378488(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___loadType0;
		RuntimeInitializeOnLoadMethodAttribute_set_loadType_m1308196171(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::set_loadType(UnityEngine.RuntimeInitializeLoadType)
extern "C"  void RuntimeInitializeOnLoadMethodAttribute_set_loadType_m1308196171 (RuntimeInitializeOnLoadMethodAttribute_t3126475234 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CloadTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m1555912301 (Scene_t1684909666 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Scene_get_handle_m1555912301_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1684909666 * _thisAdjusted = reinterpret_cast<Scene_t1684909666 *>(__this + 1);
	return Scene_get_handle_m1555912301(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern "C"  int32_t Scene_get_buildIndex_m3735680091 (Scene_t1684909666 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Scene_get_handle_m1555912301(__this, /*hidden argument*/NULL);
		int32_t L_1 = Scene_GetBuildIndexInternal_m287561822(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t Scene_get_buildIndex_m3735680091_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1684909666 * _thisAdjusted = reinterpret_cast<Scene_t1684909666 *>(__this + 1);
	return Scene_get_buildIndex_m3735680091(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m3223653899 (Scene_t1684909666 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Scene_GetHashCode_m3223653899_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1684909666 * _thisAdjusted = reinterpret_cast<Scene_t1684909666 *>(__this + 1);
	return Scene_GetHashCode_m3223653899(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern Il2CppClass* Scene_t1684909666_il2cpp_TypeInfo_var;
extern const uint32_t Scene_Equals_m3588907349_MetadataUsageId;
extern "C"  bool Scene_Equals_m3588907349 (Scene_t1684909666 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Scene_Equals_m3588907349_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Scene_t1684909666  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Scene_t1684909666_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002f;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Scene_t1684909666 *)((Scene_t1684909666 *)UnBox (L_1, Scene_t1684909666_il2cpp_TypeInfo_var))));
		int32_t L_2 = Scene_get_handle_m1555912301(__this, /*hidden argument*/NULL);
		int32_t L_3 = Scene_get_handle_m1555912301((&V_1), /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
		goto IL_002f;
	}

IL_002f:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool Scene_Equals_m3588907349_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Scene_t1684909666 * _thisAdjusted = reinterpret_cast<Scene_t1684909666 *>(__this + 1);
	return Scene_Equals_m3588907349(_thisAdjusted, ___other0, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::GetBuildIndexInternal(System.Int32)
extern "C"  int32_t Scene_GetBuildIndexInternal_m287561822 (Il2CppObject * __this /* static, unused */, int32_t ___sceneHandle0, const MethodInfo* method)
{
	typedef int32_t (*Scene_GetBuildIndexInternal_m287561822_ftn) (int32_t);
	static Scene_GetBuildIndexInternal_m287561822_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Scene_GetBuildIndexInternal_m287561822_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.Scene::GetBuildIndexInternal(System.Int32)");
	return _il2cpp_icall_func(___sceneHandle0);
}
// System.Int32 UnityEngine.SceneManagement.SceneManager::get_sceneCountInBuildSettings()
extern "C"  int32_t SceneManager_get_sceneCountInBuildSettings_m2734058893 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*SceneManager_get_sceneCountInBuildSettings_m2734058893_ftn) ();
	static SceneManager_get_sceneCountInBuildSettings_m2734058893_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_get_sceneCountInBuildSettings_m2734058893_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::get_sceneCountInBuildSettings()");
	return _il2cpp_icall_func();
}
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t1684909666  SceneManager_GetActiveScene_m2964039490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Scene_t1684909666  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Scene_t1684909666  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Scene_t1684909666  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)
extern "C"  void SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318 (Il2CppObject * __this /* static, unused */, Scene_t1684909666 * ___value0, const MethodInfo* method)
{
	typedef void (*SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318_ftn) (Scene_t1684909666 *);
	static SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m1386820036 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = ___mode1;
		G_B1_0 = (-1);
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = (-1);
			G_B2_1 = L_0;
			goto IL_0010;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0011;
	}

IL_0010:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0011:
	{
		SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
extern "C"  void SceneManager_LoadScene_m87258056 (Il2CppObject * __this /* static, unused */, int32_t ___sceneBuildIndex0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = ___sceneBuildIndex0;
		int32_t L_1 = V_0;
		SceneManager_LoadScene_m592643733(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m592643733 (Il2CppObject * __this /* static, unused */, int32_t ___sceneBuildIndex0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	Il2CppObject * G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	Il2CppObject * G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	Il2CppObject * G_B3_2 = NULL;
	{
		int32_t L_0 = ___sceneBuildIndex0;
		int32_t L_1 = ___mode1;
		G_B1_0 = L_0;
		G_B1_1 = NULL;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = L_0;
			G_B2_1 = NULL;
			goto IL_0010;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0011;
	}

IL_0010:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0011:
	{
		SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043(NULL /*static, unused*/, (String_t*)G_B3_2, G_B3_1, (bool)G_B3_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  AsyncOperation_t3814632279 * SceneManager_LoadSceneAsync_m2648120039 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method)
{
	AsyncOperation_t3814632279 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = ___mode1;
		G_B1_0 = (-1);
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = (-1);
			G_B2_1 = L_0;
			goto IL_0010;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0011;
	}

IL_0010:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0011:
	{
		AsyncOperation_t3814632279 * L_2 = SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001d;
	}

IL_001d:
	{
		AsyncOperation_t3814632279 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  AsyncOperation_t3814632279 * SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___sceneBuildIndex1, bool ___isAdditive2, bool ___mustCompleteNextFrame3, const MethodInfo* method)
{
	typedef AsyncOperation_t3814632279 * (*SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043_ftn) (String_t*, int32_t, bool, bool);
	static SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___sceneName0, ___sceneBuildIndex1, ___isAdditive2, ___mustCompleteNextFrame3);
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern Il2CppClass* SceneManager_t90660965_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAction_2_Invoke_m1528820797_MethodInfo_var;
extern const uint32_t SceneManager_Internal_SceneLoaded_m4005732915_MetadataUsageId;
extern "C"  void SceneManager_Internal_SceneLoaded_m4005732915 (Il2CppObject * __this /* static, unused */, Scene_t1684909666  ___scene0, int32_t ___mode1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneLoaded_m4005732915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_2_t1903595547 * L_0 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_sceneLoaded_0();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		UnityAction_2_t1903595547 * L_1 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_sceneLoaded_0();
		Scene_t1684909666  L_2 = ___scene0;
		int32_t L_3 = ___mode1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m1528820797(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m1528820797_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneUnloaded(UnityEngine.SceneManagement.Scene)
extern Il2CppClass* SceneManager_t90660965_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAction_1_Invoke_m3061904506_MethodInfo_var;
extern const uint32_t SceneManager_Internal_SceneUnloaded_m4108957131_MetadataUsageId;
extern "C"  void SceneManager_Internal_SceneUnloaded_m4108957131 (Il2CppObject * __this /* static, unused */, Scene_t1684909666  ___scene0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneUnloaded_m4108957131_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_1_t3051495417 * L_0 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_sceneUnloaded_1();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		UnityAction_1_t3051495417 * L_1 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_sceneUnloaded_1();
		Scene_t1684909666  L_2 = ___scene0;
		NullCheck(L_1);
		UnityAction_1_Invoke_m3061904506(L_1, L_2, /*hidden argument*/UnityAction_1_Invoke_m3061904506_MethodInfo_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_ActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern Il2CppClass* SceneManager_t90660965_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAction_2_Invoke_m670567184_MethodInfo_var;
extern const uint32_t SceneManager_Internal_ActiveSceneChanged_m1162592635_MetadataUsageId;
extern "C"  void SceneManager_Internal_ActiveSceneChanged_m1162592635 (Il2CppObject * __this /* static, unused */, Scene_t1684909666  ___previousActiveScene0, Scene_t1684909666  ___newActiveScene1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_ActiveSceneChanged_m1162592635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_2_t606618774 * L_0 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_activeSceneChanged_2();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		UnityAction_2_t606618774 * L_1 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_activeSceneChanged_2();
		Scene_t1684909666  L_2 = ___previousActiveScene0;
		Scene_t1684909666  L_3 = ___newActiveScene1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m670567184(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m670567184_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m41137238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_width_m41137238_ftn) ();
	static Screen_get_width_m41137238_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m41137238_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1051800773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_height_m1051800773_ftn) ();
	static Screen_get_height_m1051800773_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m1051800773_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Screen::get_dpi()
extern "C"  float Screen_get_dpi_m3345126327 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Screen_get_dpi_m3345126327_ftn) ();
	static Screen_get_dpi_m3345126327_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_dpi_m3345126327_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_dpi()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_sleepTimeout(System.Int32)
extern "C"  void Screen_set_sleepTimeout_m2630230709 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_sleepTimeout_m2630230709_ftn) (int32_t);
	static Screen_set_sleepTimeout_m2630230709_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_sleepTimeout_m2630230709_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_sleepTimeout(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ScriptableObject__ctor_m2671490429_MetadataUsageId;
extern "C"  void ScriptableObject__ctor_m2671490429 (ScriptableObject_t1975622470 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScriptableObject__ctor_m2671490429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object__ctor_m197157284(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m1778903390(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m1778903390 (Il2CppObject * __this /* static, unused */, ScriptableObject_t1975622470 * ___self0, const MethodInfo* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m1778903390_ftn) (ScriptableObject_t1975622470 *);
	static ScriptableObject_Internal_CreateScriptableObject_m1778903390_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m1778903390_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
extern "C"  ScriptableObject_t1975622470 * ScriptableObject_CreateInstance_m3921674852 (Il2CppObject * __this /* static, unused */, String_t* ___className0, const MethodInfo* method)
{
	typedef ScriptableObject_t1975622470 * (*ScriptableObject_CreateInstance_m3921674852_ftn) (String_t*);
	static ScriptableObject_CreateInstance_m3921674852_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstance_m3921674852_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstance(System.String)");
	return _il2cpp_icall_func(___className0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C"  ScriptableObject_t1975622470 * ScriptableObject_CreateInstance_m3271154163 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	ScriptableObject_t1975622470 * V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		ScriptableObject_t1975622470 * L_1 = ScriptableObject_CreateInstanceFromType_m4271875689(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		ScriptableObject_t1975622470 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t1975622470 * ScriptableObject_CreateInstanceFromType_m4271875689 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ScriptableObject_t1975622470 * (*ScriptableObject_CreateInstanceFromType_m4271875689_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m4271875689_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m4271875689_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke(const ScriptableObject_t1975622470& unmarshaled, ScriptableObject_t1975622470_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke_back(const ScriptableObject_t1975622470_marshaled_pinvoke& marshaled, ScriptableObject_t1975622470& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke_cleanup(ScriptableObject_t1975622470_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1975622470_marshal_com(const ScriptableObject_t1975622470& unmarshaled, ScriptableObject_t1975622470_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void ScriptableObject_t1975622470_marshal_com_back(const ScriptableObject_t1975622470_marshaled_com& marshaled, ScriptableObject_t1975622470& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1975622470_marshal_com_cleanup(ScriptableObject_t1975622470_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.String)
extern "C"  void MovedFromAttribute__ctor_m4050085439 (MovedFromAttribute_t922195725 * __this, String_t* ___sourceNamespace0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___sourceNamespace0;
		MovedFromAttribute_set_Namespace_m1786632593(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::set_Namespace(System.String)
extern "C"  void MovedFromAttribute_set_Namespace_m1786632593 (MovedFromAttribute_t922195725 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNamespaceU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
extern "C"  void PreserveAttribute__ctor_m2437378488 (PreserveAttribute_t4182602970 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor()
extern "C"  void RequiredByNativeCodeAttribute__ctor_m2374853658 (RequiredByNativeCodeAttribute_t1913052472 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor(System.String)
extern "C"  void RequiredByNativeCodeAttribute__ctor_m3769832076 (RequiredByNativeCodeAttribute_t1913052472 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		RequiredByNativeCodeAttribute_set_Name_m3041405080(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::set_Name(System.String)
extern "C"  void RequiredByNativeCodeAttribute_set_Name_m3041405080 (RequiredByNativeCodeAttribute_t1913052472 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::set_Optional(System.Boolean)
extern "C"  void RequiredByNativeCodeAttribute_set_Optional_m3990085312 (RequiredByNativeCodeAttribute_t1913052472 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3COptionalU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.UsedByNativeCodeAttribute::.ctor()
extern "C"  void UsedByNativeCodeAttribute__ctor_m2459832290 (UsedByNativeCodeAttribute_t3212052468 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C"  void SelectionBaseAttribute__ctor_m1487697870 (SelectionBaseAttribute_t936505999 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SetMouseMoved()
extern Il2CppClass* SendMouseEvents_t3505065032_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents_SetMouseMoved_m532965689_MetadataUsageId;
extern "C"  void SendMouseEvents_SetMouseMoved_m532965689 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SetMouseMoved_m532965689_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)1);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32)
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* SendMouseEvents_t3505065032_il2cpp_TypeInfo_var;
extern Il2CppClass* CameraU5BU5D_t3079764780_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t1761367055_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisGUILayer_t3254902478_m4287216801_MethodInfo_var;
extern const uint32_t SendMouseEvents_DoSendMouseEvents_m701697135_MetadataUsageId;
extern "C"  void SendMouseEvents_DoSendMouseEvents_m701697135 (Il2CppObject * __this /* static, unused */, int32_t ___skipRTCameras0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_DoSendMouseEvents_m701697135_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	HitInfo_t1761367055  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Camera_t189460977 * V_4 = NULL;
	CameraU5BU5D_t3079764780* V_5 = NULL;
	int32_t V_6 = 0;
	Rect_t3681755626  V_7;
	memset(&V_7, 0, sizeof(V_7));
	GUILayer_t3254902478 * V_8 = NULL;
	GUIElement_t3381083099 * V_9 = NULL;
	Ray_t2469606224  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	float V_13 = 0.0f;
	GameObject_t1756533147 * V_14 = NULL;
	GameObject_t1756533147 * V_15 = NULL;
	int32_t V_16 = 0;
	float G_B24_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_0 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Camera_get_allCamerasCount_m989474043(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		CameraU5BU5D_t3079764780* L_2 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		CameraU5BU5D_t3079764780* L_3 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		NullCheck(L_3);
		int32_t L_4 = V_1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) == ((int32_t)L_4)))
		{
			goto IL_002f;
		}
	}

IL_0024:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->set_m_Cameras_4(((CameraU5BU5D_t3079764780*)SZArrayNew(CameraU5BU5D_t3079764780_il2cpp_TypeInfo_var, (uint32_t)L_5)));
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		CameraU5BU5D_t3079764780* L_6 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		Camera_GetAllCameras_m2922515227(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_005e;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_7 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t1761367055  L_9 = V_3;
		(*(HitInfo_t1761367055 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))) = L_9;
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_12 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		bool L_13 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_s_MouseUsed_0();
		if (L_13)
		{
			goto IL_02ec;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		CameraU5BU5D_t3079764780* L_14 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		V_5 = L_14;
		V_6 = 0;
		goto IL_02e0;
	}

IL_0086:
	{
		CameraU5BU5D_t3079764780* L_15 = V_5;
		int32_t L_16 = V_6;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		Camera_t189460977 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_4 = L_18;
		Camera_t189460977 * L_19 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_19, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_21 = ___skipRTCameras0;
		if (!L_21)
		{
			goto IL_00b8;
		}
	}
	{
		Camera_t189460977 * L_22 = V_4;
		NullCheck(L_22);
		RenderTexture_t2666733923 * L_23 = Camera_get_targetTexture_m705925974(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_23, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b8;
		}
	}

IL_00b3:
	{
		goto IL_02da;
	}

IL_00b8:
	{
		Camera_t189460977 * L_25 = V_4;
		NullCheck(L_25);
		Rect_t3681755626  L_26 = Camera_get_pixelRect_m2084185953(L_25, /*hidden argument*/NULL);
		V_7 = L_26;
		Vector3_t2243707580  L_27 = V_0;
		bool L_28 = Rect_Contains_m1334685291((&V_7), L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00d3;
		}
	}
	{
		goto IL_02da;
	}

IL_00d3:
	{
		Camera_t189460977 * L_29 = V_4;
		NullCheck(L_29);
		GUILayer_t3254902478 * L_30 = Component_GetComponent_TisGUILayer_t3254902478_m4287216801(L_29, /*hidden argument*/Component_GetComponent_TisGUILayer_t3254902478_m4287216801_MethodInfo_var);
		V_8 = L_30;
		GUILayer_t3254902478 * L_31 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_32 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0154;
		}
	}
	{
		GUILayer_t3254902478 * L_33 = V_8;
		Vector3_t2243707580  L_34 = V_0;
		NullCheck(L_33);
		GUIElement_t3381083099 * L_35 = GUILayer_HitTest_m2960428006(L_33, L_34, /*hidden argument*/NULL);
		V_9 = L_35;
		GUIElement_t3381083099 * L_36 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_37 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_012f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_38 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_38);
		GUIElement_t3381083099 * L_39 = V_9;
		NullCheck(L_39);
		GameObject_t1756533147 * L_40 = Component_get_gameObject_m3105766835(L_39, /*hidden argument*/NULL);
		((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_target_0(L_40);
		HitInfoU5BU5D_t934504150* L_41 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_41);
		Camera_t189460977 * L_42 = V_4;
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_camera_1(L_42);
		goto IL_0153;
	}

IL_012f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_43 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_43);
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_target_0((GameObject_t1756533147 *)NULL);
		HitInfoU5BU5D_t934504150* L_44 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_44);
		((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_camera_1((Camera_t189460977 *)NULL);
	}

IL_0153:
	{
	}

IL_0154:
	{
		Camera_t189460977 * L_45 = V_4;
		NullCheck(L_45);
		int32_t L_46 = Camera_get_eventMask_m4241372419(L_45, /*hidden argument*/NULL);
		if (L_46)
		{
			goto IL_0165;
		}
	}
	{
		goto IL_02da;
	}

IL_0165:
	{
		Camera_t189460977 * L_47 = V_4;
		Vector3_t2243707580  L_48 = V_0;
		NullCheck(L_47);
		Ray_t2469606224  L_49 = Camera_ScreenPointToRay_m614889538(L_47, L_48, /*hidden argument*/NULL);
		V_10 = L_49;
		Vector3_t2243707580  L_50 = Ray_get_direction_m4059191533((&V_10), /*hidden argument*/NULL);
		V_12 = L_50;
		float L_51 = (&V_12)->get_z_3();
		V_11 = L_51;
		float L_52 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_53 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, (0.0f), L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_019c;
		}
	}
	{
		G_B24_0 = (std::numeric_limits<float>::infinity());
		goto IL_01b3;
	}

IL_019c:
	{
		Camera_t189460977 * L_54 = V_4;
		NullCheck(L_54);
		float L_55 = Camera_get_farClipPlane_m3137713566(L_54, /*hidden argument*/NULL);
		Camera_t189460977 * L_56 = V_4;
		NullCheck(L_56);
		float L_57 = Camera_get_nearClipPlane_m3536967407(L_56, /*hidden argument*/NULL);
		float L_58 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_59 = fabsf(((float)((float)((float)((float)L_55-(float)L_57))/(float)L_58)));
		G_B24_0 = L_59;
	}

IL_01b3:
	{
		V_13 = G_B24_0;
		Camera_t189460977 * L_60 = V_4;
		Ray_t2469606224  L_61 = V_10;
		float L_62 = V_13;
		Camera_t189460977 * L_63 = V_4;
		NullCheck(L_63);
		int32_t L_64 = Camera_get_cullingMask_m73686965(L_63, /*hidden argument*/NULL);
		Camera_t189460977 * L_65 = V_4;
		NullCheck(L_65);
		int32_t L_66 = Camera_get_eventMask_m4241372419(L_65, /*hidden argument*/NULL);
		NullCheck(L_60);
		GameObject_t1756533147 * L_67 = Camera_RaycastTry_m3412198936(L_60, L_61, L_62, ((int32_t)((int32_t)L_64&(int32_t)L_66)), /*hidden argument*/NULL);
		V_14 = L_67;
		GameObject_t1756533147 * L_68 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_69 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_68, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_0209;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_70 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_70);
		GameObject_t1756533147 * L_71 = V_14;
		((L_70)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0(L_71);
		HitInfoU5BU5D_t934504150* L_72 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_72);
		Camera_t189460977 * L_73 = V_4;
		((L_72)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1(L_73);
		goto IL_0247;
	}

IL_0209:
	{
		Camera_t189460977 * L_74 = V_4;
		NullCheck(L_74);
		int32_t L_75 = Camera_get_clearFlags_m1743144302(L_74, /*hidden argument*/NULL);
		if ((((int32_t)L_75) == ((int32_t)1)))
		{
			goto IL_0223;
		}
	}
	{
		Camera_t189460977 * L_76 = V_4;
		NullCheck(L_76);
		int32_t L_77 = Camera_get_clearFlags_m1743144302(L_76, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_77) == ((uint32_t)2))))
		{
			goto IL_0247;
		}
	}

IL_0223:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_78 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_78);
		((L_78)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0((GameObject_t1756533147 *)NULL);
		HitInfoU5BU5D_t934504150* L_79 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_79);
		((L_79)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1((Camera_t189460977 *)NULL);
	}

IL_0247:
	{
		Camera_t189460977 * L_80 = V_4;
		Ray_t2469606224  L_81 = V_10;
		float L_82 = V_13;
		Camera_t189460977 * L_83 = V_4;
		NullCheck(L_83);
		int32_t L_84 = Camera_get_cullingMask_m73686965(L_83, /*hidden argument*/NULL);
		Camera_t189460977 * L_85 = V_4;
		NullCheck(L_85);
		int32_t L_86 = Camera_get_eventMask_m4241372419(L_85, /*hidden argument*/NULL);
		NullCheck(L_80);
		GameObject_t1756533147 * L_87 = Camera_RaycastTry2D_m755036866(L_80, L_81, L_82, ((int32_t)((int32_t)L_84&(int32_t)L_86)), /*hidden argument*/NULL);
		V_15 = L_87;
		GameObject_t1756533147 * L_88 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_89 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_88, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_029b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_90 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_90);
		GameObject_t1756533147 * L_91 = V_15;
		((L_90)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0(L_91);
		HitInfoU5BU5D_t934504150* L_92 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_92);
		Camera_t189460977 * L_93 = V_4;
		((L_92)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1(L_93);
		goto IL_02d9;
	}

IL_029b:
	{
		Camera_t189460977 * L_94 = V_4;
		NullCheck(L_94);
		int32_t L_95 = Camera_get_clearFlags_m1743144302(L_94, /*hidden argument*/NULL);
		if ((((int32_t)L_95) == ((int32_t)1)))
		{
			goto IL_02b5;
		}
	}
	{
		Camera_t189460977 * L_96 = V_4;
		NullCheck(L_96);
		int32_t L_97 = Camera_get_clearFlags_m1743144302(L_96, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_97) == ((uint32_t)2))))
		{
			goto IL_02d9;
		}
	}

IL_02b5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_98 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_98);
		((L_98)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0((GameObject_t1756533147 *)NULL);
		HitInfoU5BU5D_t934504150* L_99 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_99);
		((L_99)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1((Camera_t189460977 *)NULL);
	}

IL_02d9:
	{
	}

IL_02da:
	{
		int32_t L_100 = V_6;
		V_6 = ((int32_t)((int32_t)L_100+(int32_t)1));
	}

IL_02e0:
	{
		int32_t L_101 = V_6;
		CameraU5BU5D_t3079764780* L_102 = V_5;
		NullCheck(L_102);
		if ((((int32_t)L_101) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_102)->max_length)))))))
		{
			goto IL_0086;
		}
	}
	{
	}

IL_02ec:
	{
		V_16 = 0;
		goto IL_0312;
	}

IL_02f4:
	{
		int32_t L_103 = V_16;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_104 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		int32_t L_105 = V_16;
		NullCheck(L_104);
		SendMouseEvents_SendEvents_m2738043830(NULL /*static, unused*/, L_103, (*(HitInfo_t1761367055 *)((L_104)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_105)))), /*hidden argument*/NULL);
		int32_t L_106 = V_16;
		V_16 = ((int32_t)((int32_t)L_106+(int32_t)1));
	}

IL_0312:
	{
		int32_t L_107 = V_16;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_108 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_108);
		if ((((int32_t)L_107) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_108)->max_length)))))))
		{
			goto IL_02f4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)0);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* SendMouseEvents_t3505065032_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t1761367055_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4145672138;
extern Il2CppCodeGenString* _stringLiteral1449131165;
extern Il2CppCodeGenString* _stringLiteral3280802065;
extern Il2CppCodeGenString* _stringLiteral301042844;
extern Il2CppCodeGenString* _stringLiteral368299876;
extern Il2CppCodeGenString* _stringLiteral85975202;
extern Il2CppCodeGenString* _stringLiteral463234816;
extern const uint32_t SendMouseEvents_SendEvents_m2738043830_MetadataUsageId;
extern "C"  void SendMouseEvents_SendEvents_m2738043830 (Il2CppObject * __this /* static, unused */, int32_t ___i0, HitInfo_t1761367055  ___hit1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SendEvents_m2738043830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	HitInfo_t1761367055  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Input_GetMouseButton_m464100923(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_004f;
		}
	}
	{
		HitInfo_t1761367055  L_3 = ___hit1;
		bool L_4 = HitInfo_op_Implicit_m1583347317(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0049;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_5 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_6 = ___i0;
		NullCheck(L_5);
		HitInfo_t1761367055  L_7 = ___hit1;
		(*(HitInfo_t1761367055 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
		HitInfoU5BU5D_t934504150* L_8 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_9 = ___i0;
		NullCheck(L_8);
		HitInfo_SendMessage_m3368777144(((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), _stringLiteral4145672138, /*hidden argument*/NULL);
	}

IL_0049:
	{
		goto IL_0107;
	}

IL_004f:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_00d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_11 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_12 = ___i0;
		NullCheck(L_11);
		bool L_13 = HitInfo_op_Implicit_m1583347317(NULL /*static, unused*/, (*(HitInfo_t1761367055 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00d0;
		}
	}
	{
		HitInfo_t1761367055  L_14 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_15 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_16 = ___i0;
		NullCheck(L_15);
		bool L_17 = HitInfo_Compare_m4272872794(NULL /*static, unused*/, L_14, (*(HitInfo_t1761367055 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00a1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_18 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_19 = ___i0;
		NullCheck(L_18);
		HitInfo_SendMessage_m3368777144(((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19))), _stringLiteral1449131165, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_20 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_21 = ___i0;
		NullCheck(L_20);
		HitInfo_SendMessage_m3368777144(((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21))), _stringLiteral3280802065, /*hidden argument*/NULL);
		HitInfoU5BU5D_t934504150* L_22 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_23 = ___i0;
		NullCheck(L_22);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t1761367055  L_24 = V_2;
		(*(HitInfo_t1761367055 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))) = L_24;
	}

IL_00d0:
	{
		goto IL_0107;
	}

IL_00d6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_25 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_26 = ___i0;
		NullCheck(L_25);
		bool L_27 = HitInfo_op_Implicit_m1583347317(NULL /*static, unused*/, (*(HitInfo_t1761367055 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0107;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_28 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_29 = ___i0;
		NullCheck(L_28);
		HitInfo_SendMessage_m3368777144(((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29))), _stringLiteral301042844, /*hidden argument*/NULL);
	}

IL_0107:
	{
		HitInfo_t1761367055  L_30 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_31 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_32 = ___i0;
		NullCheck(L_31);
		bool L_33 = HitInfo_Compare_m4272872794(NULL /*static, unused*/, L_30, (*(HitInfo_t1761367055 *)((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))), /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0140;
		}
	}
	{
		HitInfo_t1761367055  L_34 = ___hit1;
		bool L_35 = HitInfo_op_Implicit_m1583347317(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_013a;
		}
	}
	{
		HitInfo_SendMessage_m3368777144((&___hit1), _stringLiteral368299876, /*hidden argument*/NULL);
	}

IL_013a:
	{
		goto IL_0198;
	}

IL_0140:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_36 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_37 = ___i0;
		NullCheck(L_36);
		bool L_38 = HitInfo_op_Implicit_m1583347317(NULL /*static, unused*/, (*(HitInfo_t1761367055 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))), /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0172;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_39 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_40 = ___i0;
		NullCheck(L_39);
		HitInfo_SendMessage_m3368777144(((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40))), _stringLiteral85975202, /*hidden argument*/NULL);
	}

IL_0172:
	{
		HitInfo_t1761367055  L_41 = ___hit1;
		bool L_42 = HitInfo_op_Implicit_m1583347317(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0197;
		}
	}
	{
		HitInfo_SendMessage_m3368777144((&___hit1), _stringLiteral463234816, /*hidden argument*/NULL);
		HitInfo_SendMessage_m3368777144((&___hit1), _stringLiteral368299876, /*hidden argument*/NULL);
	}

IL_0197:
	{
	}

IL_0198:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_43 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_44 = ___i0;
		NullCheck(L_43);
		HitInfo_t1761367055  L_45 = ___hit1;
		(*(HitInfo_t1761367055 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))) = L_45;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern Il2CppClass* SendMouseEvents_t3505065032_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfoU5BU5D_t934504150_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t1761367055_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents__cctor_m1655934720_MetadataUsageId;
extern "C"  void SendMouseEvents__cctor_m1655934720 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents__cctor_m1655934720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HitInfo_t1761367055  V_0;
	memset(&V_0, 0, sizeof(V_0));
	HitInfo_t1761367055  V_1;
	memset(&V_1, 0, sizeof(V_1));
	HitInfo_t1761367055  V_2;
	memset(&V_2, 0, sizeof(V_2));
	HitInfo_t1761367055  V_3;
	memset(&V_3, 0, sizeof(V_3));
	HitInfo_t1761367055  V_4;
	memset(&V_4, 0, sizeof(V_4));
	HitInfo_t1761367055  V_5;
	memset(&V_5, 0, sizeof(V_5));
	HitInfo_t1761367055  V_6;
	memset(&V_6, 0, sizeof(V_6));
	HitInfo_t1761367055  V_7;
	memset(&V_7, 0, sizeof(V_7));
	HitInfo_t1761367055  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)0);
		HitInfoU5BU5D_t934504150* L_0 = ((HitInfoU5BU5D_t934504150*)SZArrayNew(HitInfoU5BU5D_t934504150_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_0));
		HitInfo_t1761367055  L_1 = V_0;
		(*(HitInfo_t1761367055 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		HitInfoU5BU5D_t934504150* L_2 = L_0;
		NullCheck(L_2);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_1));
		HitInfo_t1761367055  L_3 = V_1;
		(*(HitInfo_t1761367055 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		HitInfoU5BU5D_t934504150* L_4 = L_2;
		NullCheck(L_4);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t1761367055  L_5 = V_2;
		(*(HitInfo_t1761367055 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_5;
		((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->set_m_LastHit_1(L_4);
		HitInfoU5BU5D_t934504150* L_6 = ((HitInfoU5BU5D_t934504150*)SZArrayNew(HitInfoU5BU5D_t934504150_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_6);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t1761367055  L_7 = V_3;
		(*(HitInfo_t1761367055 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_7;
		HitInfoU5BU5D_t934504150* L_8 = L_6;
		NullCheck(L_8);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_4));
		HitInfo_t1761367055  L_9 = V_4;
		(*(HitInfo_t1761367055 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_9;
		HitInfoU5BU5D_t934504150* L_10 = L_8;
		NullCheck(L_10);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_5));
		HitInfo_t1761367055  L_11 = V_5;
		(*(HitInfo_t1761367055 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_11;
		((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->set_m_MouseDownHit_2(L_10);
		HitInfoU5BU5D_t934504150* L_12 = ((HitInfoU5BU5D_t934504150*)SZArrayNew(HitInfoU5BU5D_t934504150_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_12);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_6));
		HitInfo_t1761367055  L_13 = V_6;
		(*(HitInfo_t1761367055 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_13;
		HitInfoU5BU5D_t934504150* L_14 = L_12;
		NullCheck(L_14);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_7));
		HitInfo_t1761367055  L_15 = V_7;
		(*(HitInfo_t1761367055 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_15;
		HitInfoU5BU5D_t934504150* L_16 = L_14;
		NullCheck(L_16);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_8));
		HitInfo_t1761367055  L_17 = V_8;
		(*(HitInfo_t1761367055 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_17;
		((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->set_m_CurrentHit_3(L_16);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C"  void HitInfo_SendMessage_m3368777144 (HitInfo_t1761367055 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_target_0();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		GameObject_SendMessage_m71956653(L_0, L_1, NULL, 1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void HitInfo_SendMessage_m3368777144_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	HitInfo_t1761367055 * _thisAdjusted = reinterpret_cast<HitInfo_t1761367055 *>(__this + 1);
	HitInfo_SendMessage_m3368777144(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HitInfo_op_Implicit_m1583347317_MetadataUsageId;
extern "C"  bool HitInfo_op_Implicit_m1583347317 (Il2CppObject * __this /* static, unused */, HitInfo_t1761367055  ___exists0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitInfo_op_Implicit_m1583347317_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		GameObject_t1756533147 * L_0 = (&___exists0)->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Camera_t189460977 * L_2 = (&___exists0)->get_camera_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 0;
	}

IL_0023:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HitInfo_Compare_m4272872794_MetadataUsageId;
extern "C"  bool HitInfo_Compare_m4272872794 (Il2CppObject * __this /* static, unused */, HitInfo_t1761367055  ___lhs0, HitInfo_t1761367055  ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitInfo_Compare_m4272872794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		GameObject_t1756533147 * L_0 = (&___lhs0)->get_target_0();
		GameObject_t1756533147 * L_1 = (&___rhs1)->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Camera_t189460977 * L_3 = (&___lhs0)->get_camera_1();
		Camera_t189460977 * L_4 = (&___rhs1)->get_camera_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002f;
	}

IL_002e:
	{
		G_B3_0 = 0;
	}

IL_002f:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0035;
	}

IL_0035:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t1761367055_marshal_pinvoke(const HitInfo_t1761367055& unmarshaled, HitInfo_t1761367055_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t1761367055_marshal_pinvoke_back(const HitInfo_t1761367055_marshaled_pinvoke& marshaled, HitInfo_t1761367055& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t1761367055_marshal_pinvoke_cleanup(HitInfo_t1761367055_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t1761367055_marshal_com(const HitInfo_t1761367055& unmarshaled, HitInfo_t1761367055_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t1761367055_marshal_com_back(const HitInfo_t1761367055_marshaled_com& marshaled, HitInfo_t1761367055& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t1761367055_marshal_com_cleanup(HitInfo_t1761367055_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C"  void FormerlySerializedAsAttribute__ctor_m3551035707 (FormerlySerializedAsAttribute_t3673080018 * __this, String_t* ___oldName0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oldName0;
		__this->set_m_oldName_0(L_0);
		return;
	}
}
// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::get_oldName()
extern "C"  String_t* FormerlySerializedAsAttribute_get_oldName_m3225463145 (FormerlySerializedAsAttribute_t3673080018 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_oldName_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.SerializeField::.ctor()
extern "C"  void SerializeField__ctor_m994129777 (SerializeField_t3073427462 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SerializePrivateVariables::.ctor()
extern "C"  void SerializePrivateVariables__ctor_m806793207 (SerializePrivateVariables_t2241034664 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SetupCoroutine::InvokeMoveNext(System.Collections.IEnumerator,System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral576534231;
extern Il2CppCodeGenString* _stringLiteral1994281847;
extern const uint32_t SetupCoroutine_InvokeMoveNext_m2975616245_MetadataUsageId;
extern "C"  void SetupCoroutine_InvokeMoveNext_m2975616245 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___enumerator0, IntPtr_t ___returnValueAddress1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMoveNext_m2975616245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___returnValueAddress1;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_3, _stringLiteral576534231, _stringLiteral1994281847, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0021:
	{
		IntPtr_t L_4 = ___returnValueAddress1;
		void* L_5 = IntPtr_op_Explicit_m1073656736(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Il2CppObject * L_6 = ___enumerator0;
		NullCheck(L_6);
		bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_6);
		*((int8_t*)(L_5)) = (int8_t)L_7;
		return;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t SetupCoroutine_InvokeMember_m1481430263_MetadataUsageId;
extern "C"  Il2CppObject * SetupCoroutine_InvokeMember_m1481430263 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___behaviour0, String_t* ___name1, Il2CppObject * ___variable2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMember_m1481430263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = (ObjectU5BU5D_t3614634134*)NULL;
		Il2CppObject * L_0 = ___variable2;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		Il2CppObject * L_2 = ___variable2;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
	}

IL_0016:
	{
		Il2CppObject * L_3 = ___behaviour0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m191970594(L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___name1;
		Il2CppObject * L_6 = ___behaviour0;
		ObjectU5BU5D_t3614634134* L_7 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_8 = VirtFuncInvoker8< Il2CppObject *, String_t*, int32_t, Binder_t3404612058 *, Il2CppObject *, ObjectU5BU5D_t3614634134*, ParameterModifierU5BU5D_t963192633*, CultureInfo_t3500843524 *, StringU5BU5D_t1642385972* >::Invoke(77 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_4, L_5, ((int32_t)308), (Binder_t3404612058 *)NULL, L_6, L_7, (ParameterModifierU5BU5D_t963192633*)(ParameterModifierU5BU5D_t963192633*)NULL, (CultureInfo_t3500843524 *)NULL, (StringU5BU5D_t1642385972*)(StringU5BU5D_t1642385972*)NULL);
		V_1 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		Il2CppObject * L_9 = V_1;
		return L_9;
	}
}
// UnityEngine.Shader UnityEngine.Shader::Find(System.String)
extern "C"  Shader_t2430389951 * Shader_Find_m4179408078 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef Shader_t2430389951 * (*Shader_Find_m4179408078_ftn) (String_t*);
	static Shader_Find_m4179408078_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_Find_m4179408078_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::Find(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Boolean UnityEngine.Shader::get_isSupported()
extern "C"  bool Shader_get_isSupported_m344486701 (Shader_t2430389951 * __this, const MethodInfo* method)
{
	typedef bool (*Shader_get_isSupported_m344486701_ftn) (Shader_t2430389951 *);
	static Shader_get_isSupported_m344486701_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_get_isSupported_m344486701_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::get_isSupported()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Shader::EnableKeyword(System.String)
extern "C"  void Shader_EnableKeyword_m432694866 (Il2CppObject * __this /* static, unused */, String_t* ___keyword0, const MethodInfo* method)
{
	typedef void (*Shader_EnableKeyword_m432694866_ftn) (String_t*);
	static Shader_EnableKeyword_m432694866_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_EnableKeyword_m432694866_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::EnableKeyword(System.String)");
	_il2cpp_icall_func(___keyword0);
}
// System.Void UnityEngine.Shader::DisableKeyword(System.String)
extern "C"  void Shader_DisableKeyword_m1324463459 (Il2CppObject * __this /* static, unused */, String_t* ___keyword0, const MethodInfo* method)
{
	typedef void (*Shader_DisableKeyword_m1324463459_ftn) (String_t*);
	static Shader_DisableKeyword_m1324463459_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_DisableKeyword_m1324463459_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::DisableKeyword(System.String)");
	_il2cpp_icall_func(___keyword0);
}
// System.Int32 UnityEngine.Shader::get_renderQueue()
extern "C"  int32_t Shader_get_renderQueue_m3280932318 (Shader_t2430389951 * __this, const MethodInfo* method)
{
	typedef int32_t (*Shader_get_renderQueue_m3280932318_ftn) (Shader_t2430389951 *);
	static Shader_get_renderQueue_m3280932318_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_get_renderQueue_m3280932318_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::get_renderQueue()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Shader::SetGlobalFloat(System.String,System.Single)
extern "C"  void Shader_SetGlobalFloat_m3177582908 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, float ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___value1;
		Shader_SetGlobalFloat_m3515256261(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::SetGlobalFloat(System.Int32,System.Single)
extern "C"  void Shader_SetGlobalFloat_m3515256261 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, float ___value1, const MethodInfo* method)
{
	typedef void (*Shader_SetGlobalFloat_m3515256261_ftn) (int32_t, float);
	static Shader_SetGlobalFloat_m3515256261_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_SetGlobalFloat_m3515256261_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::SetGlobalFloat(System.Int32,System.Single)");
	_il2cpp_icall_func(___nameID0, ___value1);
}
// System.Void UnityEngine.Shader::SetGlobalMatrix(System.String,UnityEngine.Matrix4x4)
extern "C"  void Shader_SetGlobalMatrix_m2171879200 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, Matrix4x4_t2933234003  ___mat1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_2 = ___mat1;
		Shader_SetGlobalMatrix_m1115062043(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::SetGlobalMatrix(System.Int32,UnityEngine.Matrix4x4)
extern "C"  void Shader_SetGlobalMatrix_m1115062043 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, Matrix4x4_t2933234003  ___mat1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___nameID0;
		Shader_INTERNAL_CALL_SetGlobalMatrix_m2391248794(NULL /*static, unused*/, L_0, (&___mat1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::INTERNAL_CALL_SetGlobalMatrix(System.Int32,UnityEngine.Matrix4x4&)
extern "C"  void Shader_INTERNAL_CALL_SetGlobalMatrix_m2391248794 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, Matrix4x4_t2933234003 * ___mat1, const MethodInfo* method)
{
	typedef void (*Shader_INTERNAL_CALL_SetGlobalMatrix_m2391248794_ftn) (int32_t, Matrix4x4_t2933234003 *);
	static Shader_INTERNAL_CALL_SetGlobalMatrix_m2391248794_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_INTERNAL_CALL_SetGlobalMatrix_m2391248794_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::INTERNAL_CALL_SetGlobalMatrix(System.Int32,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___nameID0, ___mat1);
}
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m678579425 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef int32_t (*Shader_PropertyToID_m678579425_ftn) (String_t*);
	static Shader_PropertyToID_m678579425_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_PropertyToID_m678579425_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::PropertyToID(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Void UnityEngine.Shader::WarmupAllShaders()
extern "C"  void Shader_WarmupAllShaders_m1826857499 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*Shader_WarmupAllShaders_m1826857499_ftn) ();
	static Shader_WarmupAllShaders_m1826857499_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_WarmupAllShaders_m1826857499_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::WarmupAllShaders()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern "C"  void SharedBetweenAnimatorsAttribute__ctor_m1221241062 (SharedBetweenAnimatorsAttribute_t1565472209 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.SkeletonBone::get_transformModified()
extern "C"  int32_t SkeletonBone_get_transformModified_m1528786646 (SkeletonBone_t345082847 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0008;
	}

IL_0008:
	{
		int32_t L_0 = V_0;
		return L_0;
	}
}
extern "C"  int32_t SkeletonBone_get_transformModified_m1528786646_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SkeletonBone_t345082847 * _thisAdjusted = reinterpret_cast<SkeletonBone_t345082847 *>(__this + 1);
	return SkeletonBone_get_transformModified_m1528786646(_thisAdjusted, method);
}
// System.Void UnityEngine.SkeletonBone::set_transformModified(System.Int32)
extern "C"  void SkeletonBone_set_transformModified_m497483735 (SkeletonBone_t345082847 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
extern "C"  void SkeletonBone_set_transformModified_m497483735_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	SkeletonBone_t345082847 * _thisAdjusted = reinterpret_cast<SkeletonBone_t345082847 *>(__this + 1);
	SkeletonBone_set_transformModified_m497483735(_thisAdjusted, ___value0, method);
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t345082847_marshal_pinvoke(const SkeletonBone_t345082847& unmarshaled, SkeletonBone_t345082847_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	marshaled.___parentName_1 = il2cpp_codegen_marshal_string(unmarshaled.get_parentName_1());
	marshaled.___position_2 = unmarshaled.get_position_2();
	marshaled.___rotation_3 = unmarshaled.get_rotation_3();
	marshaled.___scale_4 = unmarshaled.get_scale_4();
}
extern "C" void SkeletonBone_t345082847_marshal_pinvoke_back(const SkeletonBone_t345082847_marshaled_pinvoke& marshaled, SkeletonBone_t345082847& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	unmarshaled.set_parentName_1(il2cpp_codegen_marshal_string_result(marshaled.___parentName_1));
	Vector3_t2243707580  unmarshaled_position_temp_2;
	memset(&unmarshaled_position_temp_2, 0, sizeof(unmarshaled_position_temp_2));
	unmarshaled_position_temp_2 = marshaled.___position_2;
	unmarshaled.set_position_2(unmarshaled_position_temp_2);
	Quaternion_t4030073918  unmarshaled_rotation_temp_3;
	memset(&unmarshaled_rotation_temp_3, 0, sizeof(unmarshaled_rotation_temp_3));
	unmarshaled_rotation_temp_3 = marshaled.___rotation_3;
	unmarshaled.set_rotation_3(unmarshaled_rotation_temp_3);
	Vector3_t2243707580  unmarshaled_scale_temp_4;
	memset(&unmarshaled_scale_temp_4, 0, sizeof(unmarshaled_scale_temp_4));
	unmarshaled_scale_temp_4 = marshaled.___scale_4;
	unmarshaled.set_scale_4(unmarshaled_scale_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t345082847_marshal_pinvoke_cleanup(SkeletonBone_t345082847_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___parentName_1);
	marshaled.___parentName_1 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t345082847_marshal_com(const SkeletonBone_t345082847& unmarshaled, SkeletonBone_t345082847_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	marshaled.___parentName_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_parentName_1());
	marshaled.___position_2 = unmarshaled.get_position_2();
	marshaled.___rotation_3 = unmarshaled.get_rotation_3();
	marshaled.___scale_4 = unmarshaled.get_scale_4();
}
extern "C" void SkeletonBone_t345082847_marshal_com_back(const SkeletonBone_t345082847_marshaled_com& marshaled, SkeletonBone_t345082847& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	unmarshaled.set_parentName_1(il2cpp_codegen_marshal_bstring_result(marshaled.___parentName_1));
	Vector3_t2243707580  unmarshaled_position_temp_2;
	memset(&unmarshaled_position_temp_2, 0, sizeof(unmarshaled_position_temp_2));
	unmarshaled_position_temp_2 = marshaled.___position_2;
	unmarshaled.set_position_2(unmarshaled_position_temp_2);
	Quaternion_t4030073918  unmarshaled_rotation_temp_3;
	memset(&unmarshaled_rotation_temp_3, 0, sizeof(unmarshaled_rotation_temp_3));
	unmarshaled_rotation_temp_3 = marshaled.___rotation_3;
	unmarshaled.set_rotation_3(unmarshaled_rotation_temp_3);
	Vector3_t2243707580  unmarshaled_scale_temp_4;
	memset(&unmarshaled_scale_temp_4, 0, sizeof(unmarshaled_scale_temp_4));
	unmarshaled_scale_temp_4 = marshaled.___scale_4;
	unmarshaled.set_scale_4(unmarshaled_scale_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t345082847_marshal_com_cleanup(SkeletonBone_t345082847_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___parentName_1);
	marshaled.___parentName_1 = NULL;
}
// UnityEngine.Transform[] UnityEngine.SkinnedMeshRenderer::get_bones()
extern "C"  TransformU5BU5D_t3764228911* SkinnedMeshRenderer_get_bones_m236777101 (SkinnedMeshRenderer_t4220419316 * __this, const MethodInfo* method)
{
	typedef TransformU5BU5D_t3764228911* (*SkinnedMeshRenderer_get_bones_m236777101_ftn) (SkinnedMeshRenderer_t4220419316 *);
	static SkinnedMeshRenderer_get_bones_m236777101_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_get_bones_m236777101_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::get_bones()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SkinnedMeshRenderer::set_bones(UnityEngine.Transform[])
extern "C"  void SkinnedMeshRenderer_set_bones_m4069982372 (SkinnedMeshRenderer_t4220419316 * __this, TransformU5BU5D_t3764228911* ___value0, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_set_bones_m4069982372_ftn) (SkinnedMeshRenderer_t4220419316 *, TransformU5BU5D_t3764228911*);
	static SkinnedMeshRenderer_set_bones_m4069982372_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_set_bones_m4069982372_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::set_bones(UnityEngine.Transform[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Transform UnityEngine.SkinnedMeshRenderer::get_rootBone()
extern "C"  Transform_t3275118058 * SkinnedMeshRenderer_get_rootBone_m3389111566 (SkinnedMeshRenderer_t4220419316 * __this, const MethodInfo* method)
{
	typedef Transform_t3275118058 * (*SkinnedMeshRenderer_get_rootBone_m3389111566_ftn) (SkinnedMeshRenderer_t4220419316 *);
	static SkinnedMeshRenderer_get_rootBone_m3389111566_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_get_rootBone_m3389111566_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::get_rootBone()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SkinnedMeshRenderer::set_rootBone(UnityEngine.Transform)
extern "C"  void SkinnedMeshRenderer_set_rootBone_m557723121 (SkinnedMeshRenderer_t4220419316 * __this, Transform_t3275118058 * ___value0, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_set_rootBone_m557723121_ftn) (SkinnedMeshRenderer_t4220419316 *, Transform_t3275118058 *);
	static SkinnedMeshRenderer_set_rootBone_m557723121_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_set_rootBone_m557723121_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::set_rootBone(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.SkinQuality UnityEngine.SkinnedMeshRenderer::get_quality()
extern "C"  int32_t SkinnedMeshRenderer_get_quality_m188576701 (SkinnedMeshRenderer_t4220419316 * __this, const MethodInfo* method)
{
	typedef int32_t (*SkinnedMeshRenderer_get_quality_m188576701_ftn) (SkinnedMeshRenderer_t4220419316 *);
	static SkinnedMeshRenderer_get_quality_m188576701_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_get_quality_m188576701_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::get_quality()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SkinnedMeshRenderer::set_quality(UnityEngine.SkinQuality)
extern "C"  void SkinnedMeshRenderer_set_quality_m2213829412 (SkinnedMeshRenderer_t4220419316 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_set_quality_m2213829412_ftn) (SkinnedMeshRenderer_t4220419316 *, int32_t);
	static SkinnedMeshRenderer_set_quality_m2213829412_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_set_quality_m2213829412_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::set_quality(UnityEngine.SkinQuality)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Mesh UnityEngine.SkinnedMeshRenderer::get_sharedMesh()
extern "C"  Mesh_t1356156583 * SkinnedMeshRenderer_get_sharedMesh_m2813560771 (SkinnedMeshRenderer_t4220419316 * __this, const MethodInfo* method)
{
	typedef Mesh_t1356156583 * (*SkinnedMeshRenderer_get_sharedMesh_m2813560771_ftn) (SkinnedMeshRenderer_t4220419316 *);
	static SkinnedMeshRenderer_get_sharedMesh_m2813560771_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_get_sharedMesh_m2813560771_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::get_sharedMesh()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SkinnedMeshRenderer::set_sharedMesh(UnityEngine.Mesh)
extern "C"  void SkinnedMeshRenderer_set_sharedMesh_m1030183452 (SkinnedMeshRenderer_t4220419316 * __this, Mesh_t1356156583 * ___value0, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_set_sharedMesh_m1030183452_ftn) (SkinnedMeshRenderer_t4220419316 *, Mesh_t1356156583 *);
	static SkinnedMeshRenderer_set_sharedMesh_m1030183452_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_set_sharedMesh_m1030183452_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::set_sharedMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.SkinnedMeshRenderer::get_updateWhenOffscreen()
extern "C"  bool SkinnedMeshRenderer_get_updateWhenOffscreen_m3148276870 (SkinnedMeshRenderer_t4220419316 * __this, const MethodInfo* method)
{
	typedef bool (*SkinnedMeshRenderer_get_updateWhenOffscreen_m3148276870_ftn) (SkinnedMeshRenderer_t4220419316 *);
	static SkinnedMeshRenderer_get_updateWhenOffscreen_m3148276870_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_get_updateWhenOffscreen_m3148276870_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::get_updateWhenOffscreen()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SkinnedMeshRenderer::set_updateWhenOffscreen(System.Boolean)
extern "C"  void SkinnedMeshRenderer_set_updateWhenOffscreen_m116240815 (SkinnedMeshRenderer_t4220419316 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_set_updateWhenOffscreen_m116240815_ftn) (SkinnedMeshRenderer_t4220419316 *, bool);
	static SkinnedMeshRenderer_set_updateWhenOffscreen_m116240815_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_set_updateWhenOffscreen_m116240815_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::set_updateWhenOffscreen(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.SkinnedMeshRenderer::SetBlendShapeWeight(System.Int32,System.Single)
extern "C"  void SkinnedMeshRenderer_SetBlendShapeWeight_m1743301399 (SkinnedMeshRenderer_t4220419316 * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_SetBlendShapeWeight_m1743301399_ftn) (SkinnedMeshRenderer_t4220419316 *, int32_t, float);
	static SkinnedMeshRenderer_SetBlendShapeWeight_m1743301399_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_SetBlendShapeWeight_m1743301399_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::SetBlendShapeWeight(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___index0, ___value1);
}
// UnityEngine.Material UnityEngine.Skybox::get_material()
extern "C"  Material_t193706927 * Skybox_get_material_m2038447772 (Skybox_t2033495038 * __this, const MethodInfo* method)
{
	typedef Material_t193706927 * (*Skybox_get_material_m2038447772_ftn) (Skybox_t2033495038 *);
	static Skybox_get_material_m2038447772_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Skybox_get_material_m2038447772_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Skybox::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Skybox::set_material(UnityEngine.Material)
extern "C"  void Skybox_set_material_m1433466175 (Skybox_t2033495038 * __this, Material_t193706927 * ___value0, const MethodInfo* method)
{
	typedef void (*Skybox_set_material_m1433466175_ftn) (Skybox_t2033495038 *, Material_t193706927 *);
	static Skybox_set_material_m1433466175_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Skybox_set_material_m1433466175_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Skybox::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.SliderHandler::.ctor(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean,System.Int32)
extern "C"  void SliderHandler__ctor_m1547880569 (SliderHandler_t3550500579 * __this, Rect_t3681755626  ___position0, float ___currentValue1, float ___size2, float ___start3, float ___end4, GUIStyle_t1799908754 * ___slider5, GUIStyle_t1799908754 * ___thumb6, bool ___horiz7, int32_t ___id8, const MethodInfo* method)
{
	{
		Rect_t3681755626  L_0 = ___position0;
		__this->set_position_0(L_0);
		float L_1 = ___currentValue1;
		__this->set_currentValue_1(L_1);
		float L_2 = ___size2;
		__this->set_size_2(L_2);
		float L_3 = ___start3;
		__this->set_start_3(L_3);
		float L_4 = ___end4;
		__this->set_end_4(L_4);
		GUIStyle_t1799908754 * L_5 = ___slider5;
		__this->set_slider_5(L_5);
		GUIStyle_t1799908754 * L_6 = ___thumb6;
		__this->set_thumb_6(L_6);
		bool L_7 = ___horiz7;
		__this->set_horiz_7(L_7);
		int32_t L_8 = ___id8;
		__this->set_id_8(L_8);
		return;
	}
}
extern "C"  void SliderHandler__ctor_m1547880569_AdjustorThunk (Il2CppObject * __this, Rect_t3681755626  ___position0, float ___currentValue1, float ___size2, float ___start3, float ___end4, GUIStyle_t1799908754 * ___slider5, GUIStyle_t1799908754 * ___thumb6, bool ___horiz7, int32_t ___id8, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	SliderHandler__ctor_m1547880569(_thisAdjusted, ___position0, ___currentValue1, ___size2, ___start3, ___end4, ___slider5, ___thumb6, ___horiz7, ___id8, method);
}
// System.Single UnityEngine.SliderHandler::Handle()
extern "C"  float SliderHandler_Handle_m504867634 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		GUIStyle_t1799908754 * L_0 = __this->get_slider_5();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		GUIStyle_t1799908754 * L_1 = __this->get_thumb_6();
		if (L_1)
		{
			goto IL_0023;
		}
	}

IL_0017:
	{
		float L_2 = __this->get_currentValue_1();
		V_0 = L_2;
		goto IL_0091;
	}

IL_0023:
	{
		int32_t L_3 = SliderHandler_CurrentEventType_m2472981589(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if (L_4 == 0)
		{
			goto IL_0055;
		}
		if (L_4 == 1)
		{
			goto IL_006d;
		}
		if (L_4 == 2)
		{
			goto IL_0085;
		}
		if (L_4 == 3)
		{
			goto IL_0061;
		}
		if (L_4 == 4)
		{
			goto IL_0085;
		}
		if (L_4 == 5)
		{
			goto IL_0085;
		}
		if (L_4 == 6)
		{
			goto IL_0085;
		}
		if (L_4 == 7)
		{
			goto IL_0079;
		}
	}
	{
		goto IL_0085;
	}

IL_0055:
	{
		float L_5 = SliderHandler_OnMouseDown_m2819993578(__this, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0091;
	}

IL_0061:
	{
		float L_6 = SliderHandler_OnMouseDrag_m1069947484(__this, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0091;
	}

IL_006d:
	{
		float L_7 = SliderHandler_OnMouseUp_m3083734299(__this, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_0091;
	}

IL_0079:
	{
		float L_8 = SliderHandler_OnRepaint_m4171175698(__this, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0091;
	}

IL_0085:
	{
		float L_9 = __this->get_currentValue_1();
		V_0 = L_9;
		goto IL_0091;
	}

IL_0091:
	{
		float L_10 = V_0;
		return L_10;
	}
}
extern "C"  float SliderHandler_Handle_m504867634_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_Handle_m504867634(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseDown()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern Il2CppClass* SystemClock_t104337557_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseDown_m2819993578_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseDown_m2819993578 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseDown_m2819993578_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	DateTime_t693205669  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	{
		Rect_t3681755626  L_0 = __this->get_position_0();
		V_0 = L_0;
		Event_t3028476042 * L_1 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t2243707579  L_2 = Event_get_mousePosition_m3789571399(L_1, /*hidden argument*/NULL);
		bool L_3 = Rect_Contains_m1334685290((&V_0), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		bool L_4 = SliderHandler_IsEmptySlider_m2679659864(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}

IL_002a:
	{
		float L_5 = __this->get_currentValue_1();
		V_1 = L_5;
		goto IL_00f7;
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_scrollTroughSide_m1337099359(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_id_8();
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m1071873884(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Event_t3028476042 * L_7 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Event_Use_m3575594482(L_7, /*hidden argument*/NULL);
		Rect_t3681755626  L_8 = SliderHandler_ThumbSelectionRect_m1949915148(__this, /*hidden argument*/NULL);
		V_2 = L_8;
		Event_t3028476042 * L_9 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector2_t2243707579  L_10 = Event_get_mousePosition_m3789571399(L_9, /*hidden argument*/NULL);
		bool L_11 = Rect_Contains_m1334685290((&V_2), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0089;
		}
	}
	{
		float L_12 = SliderHandler_ClampedCurrentValue_m1479539118(__this, /*hidden argument*/NULL);
		SliderHandler_StartDraggingWithValue_m1407392347(__this, L_12, /*hidden argument*/NULL);
		float L_13 = __this->get_currentValue_1();
		V_1 = L_13;
		goto IL_00f7;
	}

IL_0089:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_changed_m470833806(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		bool L_14 = SliderHandler_SupportsPageMovements_m983193435(__this, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00d9;
		}
	}
	{
		SliderState_t1595681032 * L_15 = SliderHandler_SliderState_m3520725942(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->set_isDragging_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t104337557_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_16 = SystemClock_get_now_m4108727544(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_16;
		DateTime_t693205669  L_17 = DateTime_AddMilliseconds_m1813199744((&V_3), (250.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m2724006954(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		int32_t L_18 = SliderHandler_CurrentScrollTroughSide_m2283829530(__this, /*hidden argument*/NULL);
		GUI_set_scrollTroughSide_m1337099359(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		float L_19 = SliderHandler_PageMovementValue_m1651578409(__this, /*hidden argument*/NULL);
		V_1 = L_19;
		goto IL_00f7;
	}

IL_00d9:
	{
		float L_20 = SliderHandler_ValueForCurrentMousePosition_m1752598323(__this, /*hidden argument*/NULL);
		V_4 = L_20;
		float L_21 = V_4;
		SliderHandler_StartDraggingWithValue_m1407392347(__this, L_21, /*hidden argument*/NULL);
		float L_22 = V_4;
		float L_23 = SliderHandler_Clamp_m291298090(__this, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		goto IL_00f7;
	}

IL_00f7:
	{
		float L_24 = V_1;
		return L_24;
	}
}
extern "C"  float SliderHandler_OnMouseDown_m2819993578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_OnMouseDown_m2819993578(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseDrag()
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseDrag_m1069947484_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseDrag_m1069947484 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseDrag_m1069947484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	SliderState_t1595681032 * V_1 = NULL;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_get_hotControl_m466901769(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001d;
		}
	}
	{
		float L_2 = __this->get_currentValue_1();
		V_0 = L_2;
		goto IL_0077;
	}

IL_001d:
	{
		SliderState_t1595681032 * L_3 = SliderHandler_SliderState_m3520725942(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		SliderState_t1595681032 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = L_4->get_isDragging_2();
		if (L_5)
		{
			goto IL_003b;
		}
	}
	{
		float L_6 = __this->get_currentValue_1();
		V_0 = L_6;
		goto IL_0077;
	}

IL_003b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_changed_m470833806(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		Event_t3028476042 * L_7 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Event_Use_m3575594482(L_7, /*hidden argument*/NULL);
		float L_8 = SliderHandler_MousePosition_m4110511062(__this, /*hidden argument*/NULL);
		SliderState_t1595681032 * L_9 = V_1;
		NullCheck(L_9);
		float L_10 = L_9->get_dragStartPos_0();
		V_2 = ((float)((float)L_8-(float)L_10));
		SliderState_t1595681032 * L_11 = V_1;
		NullCheck(L_11);
		float L_12 = L_11->get_dragStartValue_1();
		float L_13 = V_2;
		float L_14 = SliderHandler_ValuesPerPixel_m834671253(__this, /*hidden argument*/NULL);
		V_3 = ((float)((float)L_12+(float)((float)((float)L_13/(float)L_14))));
		float L_15 = V_3;
		float L_16 = SliderHandler_Clamp_m291298090(__this, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_0077;
	}

IL_0077:
	{
		float L_17 = V_0;
		return L_17;
	}
}
extern "C"  float SliderHandler_OnMouseDrag_m1069947484_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_OnMouseDrag_m1069947484(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseUp()
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseUp_m3083734299_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseUp_m3083734299 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseUp_m3083734299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_get_hotControl_m466901769(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0024;
		}
	}
	{
		Event_t3028476042 * L_2 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Event_Use_m3575594482(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m1071873884(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0024:
	{
		float L_3 = __this->get_currentValue_1();
		V_0 = L_3;
		goto IL_0030;
	}

IL_0030:
	{
		float L_4 = V_0;
		return L_4;
	}
}
extern "C"  float SliderHandler_OnMouseUp_m3083734299_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_OnMouseUp_m3083734299(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnRepaint()
extern Il2CppClass* GUIContent_t4210063000_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* SystemClock_t104337557_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnRepaint_m4171175698_MetadataUsageId;
extern "C"  float SliderHandler_OnRepaint_m4171175698 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnRepaint_m4171175698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	DateTime_t693205669  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		GUIStyle_t1799908754 * L_0 = __this->get_slider_5();
		Rect_t3681755626  L_1 = __this->get_position_0();
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t4210063000_il2cpp_TypeInfo_var);
		GUIContent_t4210063000 * L_2 = ((GUIContent_t4210063000_StaticFields*)GUIContent_t4210063000_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		int32_t L_3 = __this->get_id_8();
		NullCheck(L_0);
		GUIStyle_Draw_m2055025106(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		bool L_4 = SliderHandler_IsEmptySlider_m2679659864(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0044;
		}
	}
	{
		GUIStyle_t1799908754 * L_5 = __this->get_thumb_6();
		Rect_t3681755626  L_6 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t4210063000_il2cpp_TypeInfo_var);
		GUIContent_t4210063000 * L_7 = ((GUIContent_t4210063000_StaticFields*)GUIContent_t4210063000_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		int32_t L_8 = __this->get_id_8();
		NullCheck(L_5);
		GUIStyle_Draw_m2055025106(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
	}

IL_0044:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		int32_t L_9 = GUIUtility_get_hotControl_m466901769(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_id_8();
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_007d;
		}
	}
	{
		Rect_t3681755626  L_11 = __this->get_position_0();
		V_0 = L_11;
		Event_t3028476042 * L_12 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector2_t2243707579  L_13 = Event_get_mousePosition_m3789571399(L_12, /*hidden argument*/NULL);
		bool L_14 = Rect_Contains_m1334685290((&V_0), L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007d;
		}
	}
	{
		bool L_15 = SliderHandler_IsEmptySlider_m2679659864(__this, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0089;
		}
	}

IL_007d:
	{
		float L_16 = __this->get_currentValue_1();
		V_1 = L_16;
		goto IL_0158;
	}

IL_0089:
	{
		Rect_t3681755626  L_17 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_2 = L_17;
		Event_t3028476042 * L_18 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector2_t2243707579  L_19 = Event_get_mousePosition_m3789571399(L_18, /*hidden argument*/NULL);
		bool L_20 = Rect_Contains_m1334685290((&V_2), L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00c6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		int32_t L_21 = GUI_get_scrollTroughSide_m237006560(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00ba;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m1071873884(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_00ba:
	{
		float L_22 = __this->get_currentValue_1();
		V_1 = L_22;
		goto IL_0158;
	}

IL_00c6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_InternalRepaintEditorWindow_m219194149(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t104337557_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_23 = SystemClock_get_now_m4108727544(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t693205669  L_24 = GUI_get_nextScrollStepTime_m4045060331(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		bool L_25 = DateTime_op_LessThan_m3944619870(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00eb;
		}
	}
	{
		float L_26 = __this->get_currentValue_1();
		V_1 = L_26;
		goto IL_0158;
	}

IL_00eb:
	{
		int32_t L_27 = SliderHandler_CurrentScrollTroughSide_m2283829530(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		int32_t L_28 = GUI_get_scrollTroughSide_m237006560(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_27) == ((int32_t)L_28)))
		{
			goto IL_0107;
		}
	}
	{
		float L_29 = __this->get_currentValue_1();
		V_1 = L_29;
		goto IL_0158;
	}

IL_0107:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t104337557_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_30 = SystemClock_get_now_m4108727544(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_30;
		DateTime_t693205669  L_31 = DateTime_AddMilliseconds_m1813199744((&V_3), (30.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m2724006954(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		bool L_32 = SliderHandler_SupportsPageMovements_m983193435(__this, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_014c;
		}
	}
	{
		SliderState_t1595681032 * L_33 = SliderHandler_SliderState_m3520725942(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		L_33->set_isDragging_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_changed_m470833806(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		float L_34 = SliderHandler_PageMovementValue_m1651578409(__this, /*hidden argument*/NULL);
		V_1 = L_34;
		goto IL_0158;
	}

IL_014c:
	{
		float L_35 = SliderHandler_ClampedCurrentValue_m1479539118(__this, /*hidden argument*/NULL);
		V_1 = L_35;
		goto IL_0158;
	}

IL_0158:
	{
		float L_36 = V_1;
		return L_36;
	}
}
extern "C"  float SliderHandler_OnRepaint_m4171175698_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_OnRepaint_m4171175698(_thisAdjusted, method);
}
// UnityEngine.EventType UnityEngine.SliderHandler::CurrentEventType()
extern "C"  int32_t SliderHandler_CurrentEventType_m2472981589 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Event_t3028476042 * L_0 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		NullCheck(L_0);
		int32_t L_2 = Event_GetTypeForControl_m3906355766(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
extern "C"  int32_t SliderHandler_CurrentEventType_m2472981589_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_CurrentEventType_m2472981589(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SliderHandler::CurrentScrollTroughSide()
extern "C"  int32_t SliderHandler_CurrentScrollTroughSide_m2283829530 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t3681755626  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	int32_t G_B9_0 = 0;
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		Event_t3028476042 * L_1 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t2243707579  L_2 = Event_get_mousePosition_m3789571399(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = (&V_1)->get_x_0();
		G_B3_0 = L_3;
		goto IL_0037;
	}

IL_0024:
	{
		Event_t3028476042 * L_4 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector2_t2243707579  L_5 = Event_get_mousePosition_m3789571399(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = (&V_2)->get_y_1();
		G_B3_0 = L_6;
	}

IL_0037:
	{
		V_0 = G_B3_0;
		bool L_7 = __this->get_horiz_7();
		if (!L_7)
		{
			goto IL_0057;
		}
	}
	{
		Rect_t3681755626  L_8 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_x_m1393582490((&V_4), /*hidden argument*/NULL);
		G_B6_0 = L_9;
		goto IL_0066;
	}

IL_0057:
	{
		Rect_t3681755626  L_10 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_5 = L_10;
		float L_11 = Rect_get_y_m1393582395((&V_5), /*hidden argument*/NULL);
		G_B6_0 = L_11;
	}

IL_0066:
	{
		V_3 = G_B6_0;
		float L_12 = V_0;
		float L_13 = V_3;
		if ((!(((float)L_12) > ((float)L_13))))
		{
			goto IL_0074;
		}
	}
	{
		G_B9_0 = 1;
		goto IL_0075;
	}

IL_0074:
	{
		G_B9_0 = (-1);
	}

IL_0075:
	{
		V_6 = G_B9_0;
		goto IL_007c;
	}

IL_007c:
	{
		int32_t L_14 = V_6;
		return L_14;
	}
}
extern "C"  int32_t SliderHandler_CurrentScrollTroughSide_m2283829530_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_CurrentScrollTroughSide_m2283829530(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SliderHandler::IsEmptySlider()
extern "C"  bool SliderHandler_IsEmptySlider_m2679659864 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		V_0 = (bool)((((float)L_0) == ((float)L_1))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool SliderHandler_IsEmptySlider_m2679659864_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_IsEmptySlider_m2679659864(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SliderHandler::SupportsPageMovements()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_SupportsPageMovements_m983193435_MetadataUsageId;
extern "C"  bool SliderHandler_SupportsPageMovements_m983193435 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_SupportsPageMovements_m983193435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		float L_0 = __this->get_size_2();
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_1 = GUI_get_usePageScrollbars_m1086009624(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		V_0 = (bool)G_B3_0;
		goto IL_001f;
	}

IL_001f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool SliderHandler_SupportsPageMovements_m983193435_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_SupportsPageMovements_m983193435(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::PageMovementValue()
extern "C"  float SliderHandler_PageMovementValue_m1651578409 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	int32_t G_B3_0 = 0;
	{
		float L_0 = __this->get_currentValue_1();
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) > ((float)L_2))))
		{
			goto IL_001f;
		}
	}
	{
		G_B3_0 = (-1);
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 1;
	}

IL_0020:
	{
		V_1 = G_B3_0;
		float L_3 = SliderHandler_MousePosition_m4110511062(__this, /*hidden argument*/NULL);
		float L_4 = SliderHandler_PageUpMovementBound_m2929319993(__this, /*hidden argument*/NULL);
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0049;
		}
	}
	{
		float L_5 = V_0;
		float L_6 = __this->get_size_2();
		int32_t L_7 = V_1;
		V_0 = ((float)((float)L_5+(float)((float)((float)((float)((float)L_6*(float)(((float)((float)L_7)))))*(float)(0.9f)))));
		goto IL_005b;
	}

IL_0049:
	{
		float L_8 = V_0;
		float L_9 = __this->get_size_2();
		int32_t L_10 = V_1;
		V_0 = ((float)((float)L_8-(float)((float)((float)((float)((float)L_9*(float)(((float)((float)L_10)))))*(float)(0.9f)))));
	}

IL_005b:
	{
		float L_11 = V_0;
		float L_12 = SliderHandler_Clamp_m291298090(__this, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		goto IL_0068;
	}

IL_0068:
	{
		float L_13 = V_2;
		return L_13;
	}
}
extern "C"  float SliderHandler_PageMovementValue_m1651578409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_PageMovementValue_m1651578409(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::PageUpMovementBound()
extern "C"  float SliderHandler_PageUpMovementBound_m2929319993 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		Rect_t3681755626  L_1 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Rect_get_xMax_m2915145014((&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_3 = __this->get_position_0();
		V_1 = L_3;
		float L_4 = Rect_get_x_m1393582490((&V_1), /*hidden argument*/NULL);
		V_2 = ((float)((float)L_2-(float)L_4));
		goto IL_0053;
	}

IL_002f:
	{
		Rect_t3681755626  L_5 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_yMax_m2915146103((&V_3), /*hidden argument*/NULL);
		Rect_t3681755626  L_7 = __this->get_position_0();
		V_4 = L_7;
		float L_8 = Rect_get_y_m1393582395((&V_4), /*hidden argument*/NULL);
		V_2 = ((float)((float)L_6-(float)L_8));
		goto IL_0053;
	}

IL_0053:
	{
		float L_9 = V_2;
		return L_9;
	}
}
extern "C"  float SliderHandler_PageUpMovementBound_m2929319993_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_PageUpMovementBound_m2929319993(_thisAdjusted, method);
}
// UnityEngine.Event UnityEngine.SliderHandler::CurrentEvent()
extern "C"  Event_t3028476042 * SliderHandler_CurrentEvent_m2481129493 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	Event_t3028476042 * V_0 = NULL;
	{
		Event_t3028476042 * L_0 = Event_get_current_m2901774193(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Event_t3028476042 * L_1 = V_0;
		return L_1;
	}
}
extern "C"  Event_t3028476042 * SliderHandler_CurrentEvent_m2481129493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_CurrentEvent_m2481129493(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ValueForCurrentMousePosition()
extern "C"  float SliderHandler_ValueForCurrentMousePosition_m1752598323 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0048;
		}
	}
	{
		float L_1 = SliderHandler_MousePosition_m4110511062(__this, /*hidden argument*/NULL);
		Rect_t3681755626  L_2 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m1138015702((&V_0), /*hidden argument*/NULL);
		float L_4 = SliderHandler_ValuesPerPixel_m834671253(__this, /*hidden argument*/NULL);
		float L_5 = __this->get_start_3();
		float L_6 = __this->get_size_2();
		V_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_1-(float)((float)((float)L_3*(float)(0.5f)))))/(float)L_4))+(float)L_5))-(float)((float)((float)L_6*(float)(0.5f)))));
		goto IL_0084;
	}

IL_0048:
	{
		float L_7 = SliderHandler_MousePosition_m4110511062(__this, /*hidden argument*/NULL);
		Rect_t3681755626  L_8 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = Rect_get_height_m3128694305((&V_2), /*hidden argument*/NULL);
		float L_10 = SliderHandler_ValuesPerPixel_m834671253(__this, /*hidden argument*/NULL);
		float L_11 = __this->get_start_3();
		float L_12 = __this->get_size_2();
		V_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_7-(float)((float)((float)L_9*(float)(0.5f)))))/(float)L_10))+(float)L_11))-(float)((float)((float)L_12*(float)(0.5f)))));
		goto IL_0084;
	}

IL_0084:
	{
		float L_13 = V_1;
		return L_13;
	}
}
extern "C"  float SliderHandler_ValueForCurrentMousePosition_m1752598323_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_ValueForCurrentMousePosition_m1752598323(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::Clamp(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_Clamp_m291298090_MetadataUsageId;
extern "C"  float SliderHandler_Clamp_m291298090 (SliderHandler_t3550500579 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_Clamp_m291298090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___value0;
		float L_1 = SliderHandler_MinValue_m229001767(__this, /*hidden argument*/NULL);
		float L_2 = SliderHandler_MaxValue_m781424109(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0019;
	}

IL_0019:
	{
		float L_4 = V_0;
		return L_4;
	}
}
extern "C"  float SliderHandler_Clamp_m291298090_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_Clamp_m291298090(_thisAdjusted, ___value0, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbSelectionRect()
extern "C"  Rect_t3681755626  SliderHandler_ThumbSelectionRect_m1949915148 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Rect_t3681755626  L_0 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = ((int32_t)12);
		float L_1 = Rect_get_width_m1138015702((&V_0), /*hidden argument*/NULL);
		int32_t L_2 = V_1;
		if ((!(((float)L_1) < ((float)(((float)((float)L_2)))))))
		{
			goto IL_0042;
		}
	}
	{
		Rect_t3681755626 * L_3 = (&V_0);
		float L_4 = Rect_get_x_m1393582490(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		float L_6 = Rect_get_width_m1138015702((&V_0), /*hidden argument*/NULL);
		Rect_set_x_m3783700513(L_3, ((float)((float)L_4-(float)((float)((float)((float)((float)(((float)((float)L_5)))-(float)L_6))/(float)(2.0f))))), /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		Rect_set_width_m1921257731((&V_0), (((float)((float)L_7))), /*hidden argument*/NULL);
	}

IL_0042:
	{
		float L_8 = Rect_get_height_m3128694305((&V_0), /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		if ((!(((float)L_8) < ((float)(((float)((float)L_9)))))))
		{
			goto IL_0079;
		}
	}
	{
		Rect_t3681755626 * L_10 = (&V_0);
		float L_11 = Rect_get_y_m1393582395(L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		float L_13 = Rect_get_height_m3128694305((&V_0), /*hidden argument*/NULL);
		Rect_set_y_m4294916608(L_10, ((float)((float)L_11-(float)((float)((float)((float)((float)(((float)((float)L_12)))-(float)L_13))/(float)(2.0f))))), /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		Rect_set_height_m2019122814((&V_0), (((float)((float)L_14))), /*hidden argument*/NULL);
	}

IL_0079:
	{
		Rect_t3681755626  L_15 = V_0;
		V_2 = L_15;
		goto IL_0080;
	}

IL_0080:
	{
		Rect_t3681755626  L_16 = V_2;
		return L_16;
	}
}
extern "C"  Rect_t3681755626  SliderHandler_ThumbSelectionRect_m1949915148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_ThumbSelectionRect_m1949915148(_thisAdjusted, method);
}
// System.Void UnityEngine.SliderHandler::StartDraggingWithValue(System.Single)
extern "C"  void SliderHandler_StartDraggingWithValue_m1407392347 (SliderHandler_t3550500579 * __this, float ___dragStartValue0, const MethodInfo* method)
{
	SliderState_t1595681032 * V_0 = NULL;
	{
		SliderState_t1595681032 * L_0 = SliderHandler_SliderState_m3520725942(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		SliderState_t1595681032 * L_1 = V_0;
		float L_2 = SliderHandler_MousePosition_m4110511062(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_dragStartPos_0(L_2);
		SliderState_t1595681032 * L_3 = V_0;
		float L_4 = ___dragStartValue0;
		NullCheck(L_3);
		L_3->set_dragStartValue_1(L_4);
		SliderState_t1595681032 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_isDragging_2((bool)1);
		return;
	}
}
extern "C"  void SliderHandler_StartDraggingWithValue_m1407392347_AdjustorThunk (Il2CppObject * __this, float ___dragStartValue0, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	SliderHandler_StartDraggingWithValue_m1407392347(_thisAdjusted, ___dragStartValue0, method);
}
// UnityEngine.SliderState UnityEngine.SliderHandler::SliderState()
extern const Il2CppType* SliderState_t1595681032_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern Il2CppClass* SliderState_t1595681032_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_SliderState_m3520725942_MetadataUsageId;
extern "C"  SliderState_t1595681032 * SliderHandler_SliderState_m3520725942 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_SliderState_m3520725942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SliderState_t1595681032 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(SliderState_t1595681032_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = GUIUtility_GetStateObject_m3509738425(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = ((SliderState_t1595681032 *)CastclassClass(L_2, SliderState_t1595681032_il2cpp_TypeInfo_var));
		goto IL_0021;
	}

IL_0021:
	{
		SliderState_t1595681032 * L_3 = V_0;
		return L_3;
	}
}
extern "C"  SliderState_t1595681032 * SliderHandler_SliderState_m3520725942_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_SliderState_m3520725942(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbRect()
extern "C"  Rect_t3681755626  SliderHandler_ThumbRect_m4193953892 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Rect_t3681755626  L_1 = SliderHandler_HorizontalThumbRect_m1760436800(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0017:
	{
		Rect_t3681755626  L_2 = SliderHandler_VerticalThumbRect_m1555251118(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001d:
	{
		V_0 = G_B3_0;
		goto IL_0023;
	}

IL_0023:
	{
		Rect_t3681755626  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Rect_t3681755626  SliderHandler_ThumbRect_m4193953892_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_ThumbRect_m4193953892(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::VerticalThumbRect()
extern "C"  Rect_t3681755626  SliderHandler_VerticalThumbRect_m1555251118 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t3681755626  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t3681755626  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Rect_t3681755626  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		float L_0 = SliderHandler_ValuesPerPixel_m834671253(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_00a4;
		}
	}
	{
		Rect_t3681755626  L_3 = __this->get_position_0();
		V_1 = L_3;
		float L_4 = Rect_get_x_m1393582490((&V_1), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_5 = __this->get_slider_5();
		NullCheck(L_5);
		RectOffset_t3387826427 * L_6 = GUIStyle_get_padding_m4076916754(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_left_m439065308(L_6, /*hidden argument*/NULL);
		float L_8 = SliderHandler_ClampedCurrentValue_m1479539118(__this, /*hidden argument*/NULL);
		float L_9 = __this->get_start_3();
		float L_10 = V_0;
		Rect_t3681755626  L_11 = __this->get_position_0();
		V_2 = L_11;
		float L_12 = Rect_get_y_m1393582395((&V_2), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_13 = __this->get_slider_5();
		NullCheck(L_13);
		RectOffset_t3387826427 * L_14 = GUIStyle_get_padding_m4076916754(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m3629049358(L_14, /*hidden argument*/NULL);
		Rect_t3681755626  L_16 = __this->get_position_0();
		V_3 = L_16;
		float L_17 = Rect_get_width_m1138015702((&V_3), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_18 = __this->get_slider_5();
		NullCheck(L_18);
		RectOffset_t3387826427 * L_19 = GUIStyle_get_padding_m4076916754(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_horizontal_m3818523637(L_19, /*hidden argument*/NULL);
		float L_21 = __this->get_size_2();
		float L_22 = V_0;
		float L_23 = SliderHandler_ThumbSize_m3714327193(__this, /*hidden argument*/NULL);
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_4+(float)(((float)((float)L_7))))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8-(float)L_9))*(float)L_10))+(float)L_12))+(float)(((float)((float)L_15))))), ((float)((float)L_17-(float)(((float)((float)L_20))))), ((float)((float)((float)((float)L_21*(float)L_22))+(float)L_23)), /*hidden argument*/NULL);
		V_4 = L_24;
		goto IL_013a;
	}

IL_00a4:
	{
		Rect_t3681755626  L_25 = __this->get_position_0();
		V_5 = L_25;
		float L_26 = Rect_get_x_m1393582490((&V_5), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_27 = __this->get_slider_5();
		NullCheck(L_27);
		RectOffset_t3387826427 * L_28 = GUIStyle_get_padding_m4076916754(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = RectOffset_get_left_m439065308(L_28, /*hidden argument*/NULL);
		float L_30 = SliderHandler_ClampedCurrentValue_m1479539118(__this, /*hidden argument*/NULL);
		float L_31 = __this->get_size_2();
		float L_32 = __this->get_start_3();
		float L_33 = V_0;
		Rect_t3681755626  L_34 = __this->get_position_0();
		V_6 = L_34;
		float L_35 = Rect_get_y_m1393582395((&V_6), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_36 = __this->get_slider_5();
		NullCheck(L_36);
		RectOffset_t3387826427 * L_37 = GUIStyle_get_padding_m4076916754(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = RectOffset_get_top_m3629049358(L_37, /*hidden argument*/NULL);
		Rect_t3681755626  L_39 = __this->get_position_0();
		V_7 = L_39;
		float L_40 = Rect_get_width_m1138015702((&V_7), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_41 = __this->get_slider_5();
		NullCheck(L_41);
		RectOffset_t3387826427 * L_42 = GUIStyle_get_padding_m4076916754(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = RectOffset_get_horizontal_m3818523637(L_42, /*hidden argument*/NULL);
		float L_44 = __this->get_size_2();
		float L_45 = V_0;
		float L_46 = SliderHandler_ThumbSize_m3714327193(__this, /*hidden argument*/NULL);
		Rect_t3681755626  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Rect__ctor_m1220545469(&L_47, ((float)((float)L_26+(float)(((float)((float)L_29))))), ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))-(float)L_32))*(float)L_33))+(float)L_35))+(float)(((float)((float)L_38))))), ((float)((float)L_40-(float)(((float)((float)L_43))))), ((float)((float)((float)((float)L_44*(float)((-L_45))))+(float)L_46)), /*hidden argument*/NULL);
		V_4 = L_47;
		goto IL_013a;
	}

IL_013a:
	{
		Rect_t3681755626  L_48 = V_4;
		return L_48;
	}
}
extern "C"  Rect_t3681755626  SliderHandler_VerticalThumbRect_m1555251118_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_VerticalThumbRect_m1555251118(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::HorizontalThumbRect()
extern "C"  Rect_t3681755626  SliderHandler_HorizontalThumbRect_m1760436800 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t3681755626  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t3681755626  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Rect_t3681755626  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		float L_0 = SliderHandler_ValuesPerPixel_m834671253(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_00a4;
		}
	}
	{
		float L_3 = SliderHandler_ClampedCurrentValue_m1479539118(__this, /*hidden argument*/NULL);
		float L_4 = __this->get_start_3();
		float L_5 = V_0;
		Rect_t3681755626  L_6 = __this->get_position_0();
		V_1 = L_6;
		float L_7 = Rect_get_x_m1393582490((&V_1), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_8 = __this->get_slider_5();
		NullCheck(L_8);
		RectOffset_t3387826427 * L_9 = GUIStyle_get_padding_m4076916754(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_left_m439065308(L_9, /*hidden argument*/NULL);
		Rect_t3681755626  L_11 = __this->get_position_0();
		V_2 = L_11;
		float L_12 = Rect_get_y_m1393582395((&V_2), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_13 = __this->get_slider_5();
		NullCheck(L_13);
		RectOffset_t3387826427 * L_14 = GUIStyle_get_padding_m4076916754(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m3629049358(L_14, /*hidden argument*/NULL);
		float L_16 = __this->get_size_2();
		float L_17 = V_0;
		float L_18 = SliderHandler_ThumbSize_m3714327193(__this, /*hidden argument*/NULL);
		Rect_t3681755626  L_19 = __this->get_position_0();
		V_3 = L_19;
		float L_20 = Rect_get_height_m3128694305((&V_3), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_21 = __this->get_slider_5();
		NullCheck(L_21);
		RectOffset_t3387826427 * L_22 = GUIStyle_get_padding_m4076916754(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = RectOffset_get_vertical_m3856345169(L_22, /*hidden argument*/NULL);
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)((float)((float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5))+(float)L_7))+(float)(((float)((float)L_10))))), ((float)((float)L_12+(float)(((float)((float)L_15))))), ((float)((float)((float)((float)L_16*(float)L_17))+(float)L_18)), ((float)((float)L_20-(float)(((float)((float)L_23))))), /*hidden argument*/NULL);
		V_4 = L_24;
		goto IL_0116;
	}

IL_00a4:
	{
		float L_25 = SliderHandler_ClampedCurrentValue_m1479539118(__this, /*hidden argument*/NULL);
		float L_26 = __this->get_size_2();
		float L_27 = __this->get_start_3();
		float L_28 = V_0;
		Rect_t3681755626  L_29 = __this->get_position_0();
		V_5 = L_29;
		float L_30 = Rect_get_x_m1393582490((&V_5), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_31 = __this->get_slider_5();
		NullCheck(L_31);
		RectOffset_t3387826427 * L_32 = GUIStyle_get_padding_m4076916754(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		int32_t L_33 = RectOffset_get_left_m439065308(L_32, /*hidden argument*/NULL);
		Rect_t3681755626  L_34 = __this->get_position_0();
		V_6 = L_34;
		float L_35 = Rect_get_y_m1393582395((&V_6), /*hidden argument*/NULL);
		float L_36 = __this->get_size_2();
		float L_37 = V_0;
		float L_38 = SliderHandler_ThumbSize_m3714327193(__this, /*hidden argument*/NULL);
		Rect_t3681755626  L_39 = __this->get_position_0();
		V_7 = L_39;
		float L_40 = Rect_get_height_m3128694305((&V_7), /*hidden argument*/NULL);
		Rect_t3681755626  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Rect__ctor_m1220545469(&L_41, ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_25+(float)L_26))-(float)L_27))*(float)L_28))+(float)L_30))+(float)(((float)((float)L_33))))), L_35, ((float)((float)((float)((float)L_36*(float)((-L_37))))+(float)L_38)), L_40, /*hidden argument*/NULL);
		V_4 = L_41;
		goto IL_0116;
	}

IL_0116:
	{
		Rect_t3681755626  L_42 = V_4;
		return L_42;
	}
}
extern "C"  Rect_t3681755626  SliderHandler_HorizontalThumbRect_m1760436800_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_HorizontalThumbRect_m1760436800(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ClampedCurrentValue()
extern "C"  float SliderHandler_ClampedCurrentValue_m1479539118 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_currentValue_1();
		float L_1 = SliderHandler_Clamp_m291298090(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float SliderHandler_ClampedCurrentValue_m1479539118_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_ClampedCurrentValue_m1479539118(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MousePosition()
extern "C"  float SliderHandler_MousePosition_m4110511062 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0034;
		}
	}
	{
		Event_t3028476042 * L_1 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t2243707579  L_2 = Event_get_mousePosition_m3789571399(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_0();
		Rect_t3681755626  L_4 = __this->get_position_0();
		V_1 = L_4;
		float L_5 = Rect_get_x_m1393582490((&V_1), /*hidden argument*/NULL);
		V_2 = ((float)((float)L_3-(float)L_5));
		goto IL_005d;
	}

IL_0034:
	{
		Event_t3028476042 * L_6 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector2_t2243707579  L_7 = Event_get_mousePosition_m3789571399(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		float L_8 = (&V_3)->get_y_1();
		Rect_t3681755626  L_9 = __this->get_position_0();
		V_4 = L_9;
		float L_10 = Rect_get_y_m1393582395((&V_4), /*hidden argument*/NULL);
		V_2 = ((float)((float)L_8-(float)L_10));
		goto IL_005d;
	}

IL_005d:
	{
		float L_11 = V_2;
		return L_11;
	}
}
extern "C"  float SliderHandler_MousePosition_m4110511062_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_MousePosition_m4110511062(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ValuesPerPixel()
extern "C"  float SliderHandler_ValuesPerPixel_m834671253 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0047;
		}
	}
	{
		Rect_t3681755626  L_1 = __this->get_position_0();
		V_0 = L_1;
		float L_2 = Rect_get_width_m1138015702((&V_0), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_3 = __this->get_slider_5();
		NullCheck(L_3);
		RectOffset_t3387826427 * L_4 = GUIStyle_get_padding_m4076916754(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_horizontal_m3818523637(L_4, /*hidden argument*/NULL);
		float L_6 = SliderHandler_ThumbSize_m3714327193(__this, /*hidden argument*/NULL);
		float L_7 = __this->get_end_4();
		float L_8 = __this->get_start_3();
		V_1 = ((float)((float)((float)((float)((float)((float)L_2-(float)(((float)((float)L_5)))))-(float)L_6))/(float)((float)((float)L_7-(float)L_8))));
		goto IL_0082;
	}

IL_0047:
	{
		Rect_t3681755626  L_9 = __this->get_position_0();
		V_2 = L_9;
		float L_10 = Rect_get_height_m3128694305((&V_2), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_11 = __this->get_slider_5();
		NullCheck(L_11);
		RectOffset_t3387826427 * L_12 = GUIStyle_get_padding_m4076916754(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_vertical_m3856345169(L_12, /*hidden argument*/NULL);
		float L_14 = SliderHandler_ThumbSize_m3714327193(__this, /*hidden argument*/NULL);
		float L_15 = __this->get_end_4();
		float L_16 = __this->get_start_3();
		V_1 = ((float)((float)((float)((float)((float)((float)L_10-(float)(((float)((float)L_13)))))-(float)L_14))/(float)((float)((float)L_15-(float)L_16))));
		goto IL_0082;
	}

IL_0082:
	{
		float L_17 = V_1;
		return L_17;
	}
}
extern "C"  float SliderHandler_ValuesPerPixel_m834671253_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_ValuesPerPixel_m834671253(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ThumbSize()
extern "C"  float SliderHandler_ThumbSize_m3714327193 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B4_0 = 0.0f;
	float G_B8_0 = 0.0f;
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0048;
		}
	}
	{
		GUIStyle_t1799908754 * L_1 = __this->get_thumb_6();
		NullCheck(L_1);
		float L_2 = GUIStyle_get_fixedWidth_m97997484(L_1, /*hidden argument*/NULL);
		if ((((float)L_2) == ((float)(0.0f))))
		{
			goto IL_0031;
		}
	}
	{
		GUIStyle_t1799908754 * L_3 = __this->get_thumb_6();
		NullCheck(L_3);
		float L_4 = GUIStyle_get_fixedWidth_m97997484(L_3, /*hidden argument*/NULL);
		G_B4_0 = L_4;
		goto IL_0042;
	}

IL_0031:
	{
		GUIStyle_t1799908754 * L_5 = __this->get_thumb_6();
		NullCheck(L_5);
		RectOffset_t3387826427 * L_6 = GUIStyle_get_padding_m4076916754(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_horizontal_m3818523637(L_6, /*hidden argument*/NULL);
		G_B4_0 = (((float)((float)L_7)));
	}

IL_0042:
	{
		V_0 = G_B4_0;
		goto IL_0084;
	}

IL_0048:
	{
		GUIStyle_t1799908754 * L_8 = __this->get_thumb_6();
		NullCheck(L_8);
		float L_9 = GUIStyle_get_fixedHeight_m414733479(L_8, /*hidden argument*/NULL);
		if ((((float)L_9) == ((float)(0.0f))))
		{
			goto IL_006d;
		}
	}
	{
		GUIStyle_t1799908754 * L_10 = __this->get_thumb_6();
		NullCheck(L_10);
		float L_11 = GUIStyle_get_fixedHeight_m414733479(L_10, /*hidden argument*/NULL);
		G_B8_0 = L_11;
		goto IL_007e;
	}

IL_006d:
	{
		GUIStyle_t1799908754 * L_12 = __this->get_thumb_6();
		NullCheck(L_12);
		RectOffset_t3387826427 * L_13 = GUIStyle_get_padding_m4076916754(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_vertical_m3856345169(L_13, /*hidden argument*/NULL);
		G_B8_0 = (((float)((float)L_14)));
	}

IL_007e:
	{
		V_0 = G_B8_0;
		goto IL_0084;
	}

IL_0084:
	{
		float L_15 = V_0;
		return L_15;
	}
}
extern "C"  float SliderHandler_ThumbSize_m3714327193_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_ThumbSize_m3714327193(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MaxValue()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_MaxValue_m781424109_MetadataUsageId;
extern "C"  float SliderHandler_MaxValue_m781424109 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_MaxValue_m781424109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_size_2();
		V_0 = ((float)((float)L_2-(float)L_3));
		goto IL_001f;
	}

IL_001f:
	{
		float L_4 = V_0;
		return L_4;
	}
}
extern "C"  float SliderHandler_MaxValue_m781424109_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_MaxValue_m781424109(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MinValue()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_MinValue_m229001767_MetadataUsageId;
extern "C"  float SliderHandler_MinValue_m229001767 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_MinValue_m229001767_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m1648492575(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		float L_3 = V_0;
		return L_3;
	}
}
extern "C"  float SliderHandler_MinValue_m229001767_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_MinValue_m229001767(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t3550500579_marshal_pinvoke(const SliderHandler_t3550500579& unmarshaled, SliderHandler_t3550500579_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
extern "C" void SliderHandler_t3550500579_marshal_pinvoke_back(const SliderHandler_t3550500579_marshaled_pinvoke& marshaled, SliderHandler_t3550500579& unmarshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t3550500579_marshal_pinvoke_cleanup(SliderHandler_t3550500579_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t3550500579_marshal_com(const SliderHandler_t3550500579& unmarshaled, SliderHandler_t3550500579_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
extern "C" void SliderHandler_t3550500579_marshal_com_back(const SliderHandler_t3550500579_marshaled_com& marshaled, SliderHandler_t3550500579& unmarshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t3550500579_marshal_com_cleanup(SliderHandler_t3550500579_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SliderState::.ctor()
extern "C"  void SliderState__ctor_m1096533539 (SliderState_t1595681032 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.ctor()
extern "C"  void GameCenterPlatform__ctor_m644203297 (GameCenterPlatform_t2156144444 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
extern "C"  void GameCenterPlatform_Internal_Authenticate_m3797365482 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_Authenticate_m3797365482_ftn) ();
	static GameCenterPlatform_Internal_Authenticate_m3797365482_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticate_m3797365482_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()");
	_il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
extern "C"  bool GameCenterPlatform_Internal_Authenticated_m4294501884 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Authenticated_m4294501884_ftn) ();
	static GameCenterPlatform_Internal_Authenticated_m4294501884_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticated_m4294501884_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
extern "C"  String_t* GameCenterPlatform_Internal_UserName_m3048265218 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserName_m3048265218_ftn) ();
	static GameCenterPlatform_Internal_UserName_m3048265218_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserName_m3048265218_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
extern "C"  String_t* GameCenterPlatform_Internal_UserID_m1103178632 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserID_m1103178632_ftn) ();
	static GameCenterPlatform_Internal_UserID_m1103178632_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserID_m1103178632_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
extern "C"  bool GameCenterPlatform_Internal_Underage_m2690511558 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Underage_m2690511558_ftn) ();
	static GameCenterPlatform_Internal_Underage_m2690511558_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Underage_m2690511558_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()");
	return _il2cpp_icall_func();
}
// UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
extern "C"  Texture2D_t3542995729 * GameCenterPlatform_Internal_UserImage_m915316496 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t3542995729 * (*GameCenterPlatform_Internal_UserImage_m915316496_ftn) ();
	static GameCenterPlatform_Internal_UserImage_m915316496_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserImage_m915316496_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends(System.Object)
extern "C"  void GameCenterPlatform_Internal_LoadFriends_m2793443934 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___callback0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadFriends_m2793443934_ftn) (Il2CppObject *);
	static GameCenterPlatform_Internal_LoadFriends_m2793443934_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadFriends_m2793443934_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends(System.Object)");
	_il2cpp_icall_func(___callback0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions(System.Object)
extern "C"  void GameCenterPlatform_Internal_LoadAchievementDescriptions_m3155526163 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___callback0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievementDescriptions_m3155526163_ftn) (Il2CppObject *);
	static GameCenterPlatform_Internal_LoadAchievementDescriptions_m3155526163_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievementDescriptions_m3155526163_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions(System.Object)");
	_il2cpp_icall_func(___callback0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements(System.Object)
extern "C"  void GameCenterPlatform_Internal_LoadAchievements_m4130408457 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___callback0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievements_m4130408457_ftn) (Il2CppObject *);
	static GameCenterPlatform_Internal_LoadAchievements_m4130408457_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievements_m4130408457_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements(System.Object)");
	_il2cpp_icall_func(___callback0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double,System.Object)
extern "C"  void GameCenterPlatform_Internal_ReportProgress_m3080749130 (Il2CppObject * __this /* static, unused */, String_t* ___id0, double ___progress1, Il2CppObject * ___callback2, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportProgress_m3080749130_ftn) (String_t*, double, Il2CppObject *);
	static GameCenterPlatform_Internal_ReportProgress_m3080749130_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportProgress_m3080749130_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double,System.Object)");
	_il2cpp_icall_func(___id0, ___progress1, ___callback2);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String,System.Object)
extern "C"  void GameCenterPlatform_Internal_ReportScore_m759056665 (Il2CppObject * __this /* static, unused */, int64_t ___score0, String_t* ___category1, Il2CppObject * ___callback2, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportScore_m759056665_ftn) (int64_t, String_t*, Il2CppObject *);
	static GameCenterPlatform_Internal_ReportScore_m759056665_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportScore_m759056665_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String,System.Object)");
	_il2cpp_icall_func(___score0, ___category1, ___callback2);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String,System.Object)
extern "C"  void GameCenterPlatform_Internal_LoadScores_m4213513348 (Il2CppObject * __this /* static, unused */, String_t* ___category0, Il2CppObject * ___callback1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadScores_m4213513348_ftn) (String_t*, Il2CppObject *);
	static GameCenterPlatform_Internal_LoadScores_m4213513348_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadScores_m4213513348_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String,System.Object)");
	_il2cpp_icall_func(___category0, ___callback1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
extern "C"  void GameCenterPlatform_Internal_ShowAchievementsUI_m4211451772 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowAchievementsUI_m4211451772_ftn) ();
	static GameCenterPlatform_Internal_ShowAchievementsUI_m4211451772_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowAchievementsUI_m4211451772_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
extern "C"  void GameCenterPlatform_Internal_ShowLeaderboardUI_m3138464075 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowLeaderboardUI_m3138464075_ftn) ();
	static GameCenterPlatform_Internal_ShowLeaderboardUI_m3138464075_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowLeaderboardUI_m3138464075_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[],System.Object)
extern "C"  void GameCenterPlatform_Internal_LoadUsers_m1497175871 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___userIds0, Il2CppObject * ___callback1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadUsers_m1497175871_ftn) (StringU5BU5D_t1642385972*, Il2CppObject *);
	static GameCenterPlatform_Internal_LoadUsers_m1497175871_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadUsers_m1497175871_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[],System.Object)");
	_il2cpp_icall_func(___userIds0, ___callback1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
extern "C"  void GameCenterPlatform_Internal_ResetAllAchievements_m3489790181 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ResetAllAchievements_m3489790181_ftn) ();
	static GameCenterPlatform_Internal_ResetAllAchievements_m3489790181_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ResetAllAchievements_m3489790181_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
extern "C"  void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m4005094965 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m4005094965_ftn) (bool);
	static GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m4005094965_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m4005094965_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ResetAllAchievements(System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ResetAllAchievements_m4114806314_MetadataUsageId;
extern "C"  void GameCenterPlatform_ResetAllAchievements_m4114806314 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ResetAllAchievements_m4114806314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_ResetAchievements_4(L_0);
		GameCenterPlatform_Internal_ResetAllAchievements_m3489790181(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowDefaultAchievementCompletionBanner(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m534321293_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m534321293 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m534321293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m4005094965(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI(System.String,UnityEngine.SocialPlatforms.TimeScope)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowLeaderboardUI_m2527518460_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowLeaderboardUI_m2527518460 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardID0, int32_t ___timeScope1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowLeaderboardUI_m2527518460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID0;
		int32_t L_1 = ___timeScope1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m915894780(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
extern "C"  void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m915894780 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardID0, int32_t ___timeScope1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m915894780_ftn) (String_t*, int32_t);
	static GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m915894780_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m915894780_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)");
	_il2cpp_icall_func(___leaderboardID0, ___timeScope1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearAchievementDescriptions(System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* AchievementDescriptionU5BU5D_t847281182_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearAchievementDescriptions_m4063396811_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearAchievementDescriptions_m4063396811 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearAchievementDescriptions_m4063396811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_1();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_1();
		NullCheck(L_1);
		int32_t L_2 = ___size0;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		int32_t L_3 = ___size0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_adCache_1(((AchievementDescriptionU5BU5D_t847281182*)SZArrayNew(AchievementDescriptionU5BU5D_t847281182_il2cpp_TypeInfo_var, (uint32_t)L_3)));
	}

IL_0023:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescription(UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetAchievementDescription_m1025952251_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetAchievementDescription_m1025952251 (Il2CppObject * __this /* static, unused */, GcAchievementDescriptionData_t960725851  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetAchievementDescription_m1025952251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_1();
		int32_t L_1 = ___number1;
		AchievementDescription_t3110978151 * L_2 = GcAchievementDescriptionData_ToAchievementDescription_m1135716620((&___data0), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (AchievementDescription_t3110978151 *)L_2);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescriptionImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2731130621;
extern const uint32_t GameCenterPlatform_SetAchievementDescriptionImage_m2184571696_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetAchievementDescriptionImage_m2184571696 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetAchievementDescriptionImage_m2184571696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_1();
		NullCheck(L_0);
		int32_t L_1 = ___number1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = ___number1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0025;
		}
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2731130621, /*hidden argument*/NULL);
		goto IL_0032;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_3 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_1();
		int32_t L_4 = ___number1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		AchievementDescription_t3110978151 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Texture2D_t3542995729 * L_7 = ___texture0;
		NullCheck(L_6);
		AchievementDescription_SetImage_m1395221782(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerAchievementDescriptionCallback(System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m943750401_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2035814694;
extern const uint32_t GameCenterPlatform_TriggerAchievementDescriptionCallback_m2053013783_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerAchievementDescriptionCallback_m2053013783 (Il2CppObject * __this /* static, unused */, Action_1_t3885079697 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerAchievementDescriptionCallback_m2053013783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t3885079697 * L_0 = ___callback0;
		if (!L_0)
		{
			goto IL_0034;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_1();
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_2 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_1();
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))
		{
			goto IL_0028;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2035814694, /*hidden argument*/NULL);
	}

IL_0028:
	{
		Action_1_t3885079697 * L_3 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_4 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_1();
		NullCheck(L_3);
		Action_1_Invoke_m943750401(L_3, (IAchievementDescriptionU5BU5D_t4083280315*)(IAchievementDescriptionU5BU5D_t4083280315*)L_4, /*hidden argument*/Action_1_Invoke_m943750401_MethodInfo_var);
	}

IL_0034:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AuthenticateCallbackWrapper(System.Int32,System.String)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m547817495_MethodInfo_var;
extern const uint32_t GameCenterPlatform_AuthenticateCallbackWrapper_m2085660897_MetadataUsageId;
extern "C"  void GameCenterPlatform_AuthenticateCallbackWrapper_m2085660897 (Il2CppObject * __this /* static, unused */, int32_t ___result0, String_t* ___error1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_AuthenticateCallbackWrapper_m2085660897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_t1865222972 * G_B3_0 = NULL;
	Action_2_t1865222972 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	Action_2_t1865222972 * G_B4_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m2282436159(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_2_t1865222972 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_AuthenticateCallback_0();
		if (!L_0)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_2_t1865222972 * L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_AuthenticateCallback_0();
		int32_t L_2 = ___result0;
		G_B2_0 = L_1;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B3_0 = L_1;
			goto IL_0023;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0024;
	}

IL_0023:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0024:
	{
		String_t* L_3 = ___error1;
		NullCheck(G_B4_1);
		Action_2_Invoke_m547817495(G_B4_1, (bool)G_B4_0, L_3, /*hidden argument*/Action_2_Invoke_m547817495_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_AuthenticateCallback_0((Action_2_t1865222972 *)NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearFriends(System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearFriends_m1742022050_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearFriends_m1742022050 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearFriends_m1742022050_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size0;
		GameCenterPlatform_SafeClearArray_m2690967919(NULL /*static, unused*/, (((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_2()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriends(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetFriends_m676763082_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetFriends_m676763082 (Il2CppObject * __this /* static, unused */, GcUserProfileData_t3198293052  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetFriends_m676763082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number1;
		GcUserProfileData_AddToArray_m2451723029((&___data0), (((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_2()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriendImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetFriendImage_m1119516317_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetFriendImage_m1119516317 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetFriendImage_m1119516317_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Texture2D_t3542995729 * L_0 = ___texture0;
		int32_t L_1 = ___number1;
		GameCenterPlatform_SafeSetUserImage_m4283674749(NULL /*static, unused*/, (((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_2()), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerFriendsCallbackWrapper(System.Action`1<System.Boolean>,System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerFriendsCallbackWrapper_m2473591562_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerFriendsCallbackWrapper_m2473591562 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___callback0, int32_t ___result1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerFriendsCallbackWrapper_m2473591562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3627374100 * G_B5_0 = NULL;
	Action_1_t3627374100 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	Action_1_t3627374100 * G_B6_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		UserProfileU5BU5D_t2930725895* L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_friends_2();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		LocalUser_t3019851150 * L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_5();
		UserProfileU5BU5D_t2930725895* L_2 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_friends_2();
		NullCheck(L_1);
		LocalUser_SetFriends_m3706685636(L_1, (IUserProfileU5BU5D_t3461248430*)(IUserProfileU5BU5D_t3461248430*)L_2, /*hidden argument*/NULL);
	}

IL_001a:
	{
		Action_1_t3627374100 * L_3 = ___callback0;
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		Action_1_t3627374100 * L_4 = ___callback0;
		int32_t L_5 = ___result1;
		G_B4_0 = L_4;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			G_B5_0 = L_4;
			goto IL_002e;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_002f;
	}

IL_002e:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_002f:
	{
		NullCheck(G_B6_1);
		Action_1_Invoke_m3662000152(G_B6_1, (bool)G_B6_0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_0034:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AchievementCallbackWrapper(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>,UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[])
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* AchievementU5BU5D_t2450740364_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3760172603_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2309484702;
extern const uint32_t GameCenterPlatform_AchievementCallbackWrapper_m1713444430_MetadataUsageId;
extern "C"  void GameCenterPlatform_AchievementCallbackWrapper_m1713444430 (Il2CppObject * __this /* static, unused */, Action_1_t2511354027 * ___callback0, GcAchievementDataU5BU5D_t2283071720* ___result1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_AchievementCallbackWrapper_m1713444430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AchievementU5BU5D_t2450740364* V_0 = NULL;
	int32_t V_1 = 0;
	{
		Action_1_t2511354027 * L_0 = ___callback0;
		if (!L_0)
		{
			goto IL_004e;
		}
	}
	{
		GcAchievementDataU5BU5D_t2283071720* L_1 = ___result1;
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2309484702, /*hidden argument*/NULL);
	}

IL_001a:
	{
		GcAchievementDataU5BU5D_t2283071720* L_2 = ___result1;
		NullCheck(L_2);
		V_0 = ((AchievementU5BU5D_t2450740364*)SZArrayNew(AchievementU5BU5D_t2450740364_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))));
		V_1 = 0;
		goto IL_003d;
	}

IL_002a:
	{
		AchievementU5BU5D_t2450740364* L_3 = V_0;
		int32_t L_4 = V_1;
		GcAchievementDataU5BU5D_t2283071720* L_5 = ___result1;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Achievement_t1333316625 * L_7 = GcAchievementData_ToAchievement_m962894180(((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))), /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_7);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (Achievement_t1333316625 *)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_9 = V_1;
		GcAchievementDataU5BU5D_t2283071720* L_10 = ___result1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002a;
		}
	}
	{
		Action_1_t2511354027 * L_11 = ___callback0;
		AchievementU5BU5D_t2450740364* L_12 = V_0;
		NullCheck(L_11);
		Action_1_Invoke_m3760172603(L_11, (IAchievementU5BU5D_t2709554645*)(IAchievementU5BU5D_t2709554645*)L_12, /*hidden argument*/Action_1_Invoke_m3760172603_MethodInfo_var);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ProgressCallbackWrapper(System.Action`1<System.Boolean>,System.Boolean)
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ProgressCallbackWrapper_m3872703008_MetadataUsageId;
extern "C"  void GameCenterPlatform_ProgressCallbackWrapper_m3872703008 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___callback0, bool ___success1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ProgressCallbackWrapper_m3872703008_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = ___callback0;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		Action_1_t3627374100 * L_1 = ___callback0;
		bool L_2 = ___success1;
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_000e:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreCallbackWrapper(System.Action`1<System.Boolean>,System.Boolean)
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ScoreCallbackWrapper_m2338076017_MetadataUsageId;
extern "C"  void GameCenterPlatform_ScoreCallbackWrapper_m2338076017 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___callback0, bool ___success1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ScoreCallbackWrapper_m2338076017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = ___callback0;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		Action_1_t3627374100 * L_1 = ___callback0;
		bool L_2 = ___success1;
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_000e:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreLoaderCallbackWrapper(System.Action`1<UnityEngine.SocialPlatforms.IScore[]>,UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern Il2CppClass* ScoreU5BU5D_t299013381_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3504824494_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ScoreLoaderCallbackWrapper_m1120728552_MetadataUsageId;
extern "C"  void GameCenterPlatform_ScoreLoaderCallbackWrapper_m1120728552 (Il2CppObject * __this /* static, unused */, Action_1_t3039104018 * ___callback0, GcScoreDataU5BU5D_t4052399267* ___result1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ScoreLoaderCallbackWrapper_m1120728552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ScoreU5BU5D_t299013381* V_0 = NULL;
	int32_t V_1 = 0;
	{
		Action_1_t3039104018 * L_0 = ___callback0;
		if (!L_0)
		{
			goto IL_003c;
		}
	}
	{
		GcScoreDataU5BU5D_t4052399267* L_1 = ___result1;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t299013381*)SZArrayNew(ScoreU5BU5D_t299013381_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002b;
	}

IL_0018:
	{
		ScoreU5BU5D_t299013381* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t4052399267* L_4 = ___result1;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Score_t2307748940 * L_6 = GcScoreData_ToScore_m3744988639(((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_6);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Score_t2307748940 *)L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t4052399267* L_9 = ___result1;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t3039104018 * L_10 = ___callback0;
		ScoreU5BU5D_t299013381* L_11 = V_0;
		NullCheck(L_10);
		Action_1_Invoke_m3504824494(L_10, (IScoreU5BU5D_t3237304636*)(IScoreU5BU5D_t3237304636*)L_11, /*hidden argument*/Action_1_Invoke_m3504824494_MethodInfo_var);
	}

IL_003c:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m692395677_MetadataUsageId;
extern "C"  void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m692395677 (GameCenterPlatform_t2156144444 * __this, Il2CppObject * ___user0, Action_1_t3627374100 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m692395677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Action_1_t3627374100 * L_1 = ___callback1;
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Action_1_t3627374100 * L_2 = ___callback1;
		NullCheck(L_2);
		Action_1_Invoke_m3662000152(L_2, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_001a:
	{
		goto IL_0025;
	}

IL_001f:
	{
		Action_1_t3627374100 * L_3 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_LoadFriends_m2793443934(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern Il2CppClass* U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t1170095138_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t1865222972_il2cpp_TypeInfo_var;
extern Il2CppClass* ISocialPlatform_t267455441_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_U3CU3Em__0_m648091976_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m759102168_MethodInfo_var;
extern const uint32_t GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m1019748987_MetadataUsageId;
extern "C"  void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m1019748987 (GameCenterPlatform_t2156144444 * __this, Il2CppObject * ___user0, Action_1_t3627374100 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m1019748987_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t1170095138 * V_0 = NULL;
	{
		U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t1170095138 * L_0 = (U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t1170095138 *)il2cpp_codegen_object_new(U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t1170095138_il2cpp_TypeInfo_var);
		U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0__ctor_m1252567582(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t1170095138 * L_1 = V_0;
		Action_1_t3627374100 * L_2 = ___callback1;
		NullCheck(L_1);
		L_1->set_callback_0(L_2);
		Il2CppObject * L_3 = ___user0;
		U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t1170095138 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_U3CU3Em__0_m648091976_MethodInfo_var);
		Action_2_t1865222972 * L_6 = (Action_2_t1865222972 *)il2cpp_codegen_object_new(Action_2_t1865222972_il2cpp_TypeInfo_var);
		Action_2__ctor_m759102168(L_6, L_4, L_5, /*hidden argument*/Action_2__ctor_m759102168_MethodInfo_var);
		InterfaceActionInvoker2< Il2CppObject *, Action_2_t1865222972 * >::Invoke(1 /* System.Void UnityEngine.SocialPlatforms.ISocialPlatform::Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`2<System.Boolean,System.String>) */, ISocialPlatform_t267455441_il2cpp_TypeInfo_var, __this, L_3, L_6);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`2<System.Boolean,System.String>)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m2550982410_MetadataUsageId;
extern "C"  void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m2550982410 (GameCenterPlatform_t2156144444 * __this, Il2CppObject * ___user0, Action_2_t1865222972 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m2550982410_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_2_t1865222972 * L_0 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_AuthenticateCallback_0(L_0);
		GameCenterPlatform_Internal_Authenticate_m3797365482(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SocialPlatforms.ILocalUser UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::get_localUser()
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalUser_t3019851150_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern const uint32_t GameCenterPlatform_get_localUser_m3187393722_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_get_localUser_m3187393722 (GameCenterPlatform_t2156144444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_get_localUser_m3187393722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		LocalUser_t3019851150 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_5();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		LocalUser_t3019851150 * L_1 = (LocalUser_t3019851150 *)il2cpp_codegen_object_new(LocalUser_t3019851150_il2cpp_TypeInfo_var);
		LocalUser__ctor_m456101162(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_m_LocalUser_5(L_1);
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		bool L_2 = GameCenterPlatform_Internal_Authenticated_m4294501884(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		LocalUser_t3019851150 * L_3 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_5();
		NullCheck(L_3);
		String_t* L_4 = UserProfile_get_id_m1121636229(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, _stringLiteral372029326, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m2282436159(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		LocalUser_t3019851150 * L_6 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_5();
		V_0 = L_6;
		goto IL_0048;
	}

IL_0048:
	{
		Il2CppObject * L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::PopulateLocalUser()
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_PopulateLocalUser_m2282436159_MetadataUsageId;
extern "C"  void GameCenterPlatform_PopulateLocalUser_m2282436159 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_PopulateLocalUser_m2282436159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		LocalUser_t3019851150 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_5();
		bool L_1 = GameCenterPlatform_Internal_Authenticated_m4294501884(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocalUser_SetAuthenticated_m3483845210(L_0, L_1, /*hidden argument*/NULL);
		LocalUser_t3019851150 * L_2 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_5();
		String_t* L_3 = GameCenterPlatform_Internal_UserName_m3048265218(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		UserProfile_SetUserName_m3667428096(L_2, L_3, /*hidden argument*/NULL);
		LocalUser_t3019851150 * L_4 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_5();
		String_t* L_5 = GameCenterPlatform_Internal_UserID_m1103178632(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		UserProfile_SetUserID_m3818116510(L_4, L_5, /*hidden argument*/NULL);
		LocalUser_t3019851150 * L_6 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_5();
		bool L_7 = GameCenterPlatform_Internal_Underage_m2690511558(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		LocalUser_SetUnderage_m3689639158(L_6, L_7, /*hidden argument*/NULL);
		LocalUser_t3019851150 * L_8 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_5();
		Texture2D_t3542995729 * L_9 = GameCenterPlatform_Internal_UserImage_m915316496(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		UserProfile_SetImage_m3142478163(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievementDescriptions(System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>)
extern Il2CppClass* AchievementDescriptionU5BU5D_t847281182_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m943750401_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadAchievementDescriptions_m293745755_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadAchievementDescriptions_m293745755 (GameCenterPlatform_t2156144444 * __this, Action_1_t3885079697 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadAchievementDescriptions_m293745755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		Action_1_t3885079697 * L_1 = ___callback0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Action_1_t3885079697 * L_2 = ___callback0;
		NullCheck(L_2);
		Action_1_Invoke_m943750401(L_2, (IAchievementDescriptionU5BU5D_t4083280315*)(IAchievementDescriptionU5BU5D_t4083280315*)((AchievementDescriptionU5BU5D_t847281182*)SZArrayNew(AchievementDescriptionU5BU5D_t847281182_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m943750401_MethodInfo_var);
	}

IL_001f:
	{
		goto IL_002a;
	}

IL_0024:
	{
		Action_1_t3885079697 * L_3 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_LoadAchievementDescriptions_m3155526163(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ReportProgress_m3585652631_MetadataUsageId;
extern "C"  void GameCenterPlatform_ReportProgress_m3585652631 (GameCenterPlatform_t2156144444 * __this, String_t* ___id0, double ___progress1, Action_1_t3627374100 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ReportProgress_m3585652631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Action_1_t3627374100 * L_1 = ___callback2;
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Action_1_t3627374100 * L_2 = ___callback2;
		NullCheck(L_2);
		Action_1_Invoke_m3662000152(L_2, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_001a:
	{
		goto IL_0027;
	}

IL_001f:
	{
		String_t* L_3 = ___id0;
		double L_4 = ___progress1;
		Action_1_t3627374100 * L_5 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ReportProgress_m3080749130(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
extern Il2CppClass* AchievementU5BU5D_t2450740364_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3760172603_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadAchievements_m200011543_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadAchievements_m200011543 (GameCenterPlatform_t2156144444 * __this, Action_1_t2511354027 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadAchievements_m200011543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		Action_1_t2511354027 * L_1 = ___callback0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Action_1_t2511354027 * L_2 = ___callback0;
		NullCheck(L_2);
		Action_1_Invoke_m3760172603(L_2, (IAchievementU5BU5D_t2709554645*)(IAchievementU5BU5D_t2709554645*)((AchievementU5BU5D_t2450740364*)SZArrayNew(AchievementU5BU5D_t2450740364_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m3760172603_MethodInfo_var);
	}

IL_001f:
	{
		goto IL_002a;
	}

IL_0024:
	{
		Action_1_t2511354027 * L_3 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_LoadAchievements_m4130408457(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ReportScore_m3720143724_MetadataUsageId;
extern "C"  void GameCenterPlatform_ReportScore_m3720143724 (GameCenterPlatform_t2156144444 * __this, int64_t ___score0, String_t* ___board1, Action_1_t3627374100 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ReportScore_m3720143724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Action_1_t3627374100 * L_1 = ___callback2;
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Action_1_t3627374100 * L_2 = ___callback2;
		NullCheck(L_2);
		Action_1_Invoke_m3662000152(L_2, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_001a:
	{
		goto IL_0027;
	}

IL_001f:
	{
		int64_t L_3 = ___score0;
		String_t* L_4 = ___board1;
		Action_1_t3627374100 * L_5 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ReportScore_m759056665(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(System.String,System.Action`1<UnityEngine.SocialPlatforms.IScore[]>)
extern Il2CppClass* ScoreU5BU5D_t299013381_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3504824494_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadScores_m2160889205_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadScores_m2160889205 (GameCenterPlatform_t2156144444 * __this, String_t* ___category0, Action_1_t3039104018 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadScores_m2160889205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		Action_1_t3039104018 * L_1 = ___callback1;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Action_1_t3039104018 * L_2 = ___callback1;
		NullCheck(L_2);
		Action_1_Invoke_m3504824494(L_2, (IScoreU5BU5D_t3237304636*)(IScoreU5BU5D_t3237304636*)((ScoreU5BU5D_t299013381*)SZArrayNew(ScoreU5BU5D_t299013381_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m3504824494_MethodInfo_var);
	}

IL_001f:
	{
		goto IL_002b;
	}

IL_0024:
	{
		String_t* L_3 = ___category0;
		Action_1_t3039104018 * L_4 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_LoadScores_m4213513348(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(UnityEngine.SocialPlatforms.ILeaderboard,System.Action`1<System.Boolean>)
extern Il2CppClass* Leaderboard_t4160680639_il2cpp_TypeInfo_var;
extern Il2CppClass* GcLeaderboard_t453887929_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* ILeaderboard_t77027648_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1201759976_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadScores_m2122243871_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadScores_m2122243871 (GameCenterPlatform_t2156144444 * __this, Il2CppObject * ___board0, Action_1_t3627374100 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadScores_m2122243871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Leaderboard_t4160680639 * V_0 = NULL;
	GcLeaderboard_t453887929 * V_1 = NULL;
	StringU5BU5D_t1642385972* V_2 = NULL;
	Range_t3455291607  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Range_t3455291607  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Action_1_t3627374100 * L_1 = ___callback1;
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Action_1_t3627374100 * L_2 = ___callback1;
		NullCheck(L_2);
		Action_1_Invoke_m3662000152(L_2, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_001a:
	{
		goto IL_0080;
	}

IL_001f:
	{
		Il2CppObject * L_3 = ___board0;
		V_0 = ((Leaderboard_t4160680639 *)CastclassClass(L_3, Leaderboard_t4160680639_il2cpp_TypeInfo_var));
		Leaderboard_t4160680639 * L_4 = V_0;
		GcLeaderboard_t453887929 * L_5 = (GcLeaderboard_t453887929 *)il2cpp_codegen_object_new(GcLeaderboard_t453887929_il2cpp_TypeInfo_var);
		GcLeaderboard__ctor_m983739183(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		List_1_t4117976357 * L_6 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_GcBoards_6();
		GcLeaderboard_t453887929 * L_7 = V_1;
		NullCheck(L_6);
		List_1_Add_m1201759976(L_6, L_7, /*hidden argument*/List_1_Add_m1201759976_MethodInfo_var);
		Leaderboard_t4160680639 * L_8 = V_0;
		NullCheck(L_8);
		StringU5BU5D_t1642385972* L_9 = Leaderboard_GetUserFilter_m4114287667(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		StringU5BU5D_t1642385972* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))
		{
			goto IL_0049;
		}
	}
	{
		V_2 = (StringU5BU5D_t1642385972*)NULL;
	}

IL_0049:
	{
		GcLeaderboard_t453887929 * L_11 = V_1;
		Il2CppObject * L_12 = ___board0;
		NullCheck(L_12);
		String_t* L_13 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t77027648_il2cpp_TypeInfo_var, L_12);
		Il2CppObject * L_14 = ___board0;
		NullCheck(L_14);
		Range_t3455291607  L_15 = InterfaceFuncInvoker0< Range_t3455291607  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t77027648_il2cpp_TypeInfo_var, L_14);
		V_3 = L_15;
		int32_t L_16 = (&V_3)->get_from_0();
		Il2CppObject * L_17 = ___board0;
		NullCheck(L_17);
		Range_t3455291607  L_18 = InterfaceFuncInvoker0< Range_t3455291607  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t77027648_il2cpp_TypeInfo_var, L_17);
		V_4 = L_18;
		int32_t L_19 = (&V_4)->get_count_1();
		StringU5BU5D_t1642385972* L_20 = V_2;
		Il2CppObject * L_21 = ___board0;
		NullCheck(L_21);
		int32_t L_22 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope() */, ILeaderboard_t77027648_il2cpp_TypeInfo_var, L_21);
		Il2CppObject * L_23 = ___board0;
		NullCheck(L_23);
		int32_t L_24 = InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t77027648_il2cpp_TypeInfo_var, L_23);
		Action_1_t3627374100 * L_25 = ___callback1;
		NullCheck(L_11);
		GcLeaderboard_Internal_LoadScores_m631571419(L_11, L_13, L_16, L_19, L_20, L_22, L_24, L_25, /*hidden argument*/NULL);
	}

IL_0080:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LeaderboardCallbackWrapper(System.Action`1<System.Boolean>,System.Boolean)
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LeaderboardCallbackWrapper_m2652893820_MetadataUsageId;
extern "C"  void GameCenterPlatform_LeaderboardCallbackWrapper_m2652893820 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___callback0, bool ___success1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LeaderboardCallbackWrapper_m2652893820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = ___callback0;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		Action_1_t3627374100 * L_1 = ___callback0;
		bool L_2 = ___success1;
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_000e:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::GetLoading(UnityEngine.SocialPlatforms.ILeaderboard)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* Leaderboard_t4160680639_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m835756033_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m844722661_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1731154041_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m210984790_MethodInfo_var;
extern const uint32_t GameCenterPlatform_GetLoading_m2902653631_MetadataUsageId;
extern "C"  bool GameCenterPlatform_GetLoading_m2902653631 (GameCenterPlatform_t2156144444 * __this, Il2CppObject * ___board0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_GetLoading_m2902653631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	GcLeaderboard_t453887929 * V_1 = NULL;
	Enumerator_t3652706031  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0071;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		List_1_t4117976357 * L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_GcBoards_6();
		NullCheck(L_1);
		Enumerator_t3652706031  L_2 = List_1_GetEnumerator_m835756033(L_1, /*hidden argument*/List_1_GetEnumerator_m835756033_MethodInfo_var);
		V_2 = L_2;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004b;
		}

IL_0024:
		{
			GcLeaderboard_t453887929 * L_3 = Enumerator_get_Current_m844722661((&V_2), /*hidden argument*/Enumerator_get_Current_m844722661_MethodInfo_var);
			V_1 = L_3;
			GcLeaderboard_t453887929 * L_4 = V_1;
			Il2CppObject * L_5 = ___board0;
			NullCheck(L_4);
			bool L_6 = GcLeaderboard_Contains_m3937847094(L_4, ((Leaderboard_t4160680639 *)CastclassClass(L_5, Leaderboard_t4160680639_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_004a;
			}
		}

IL_003e:
		{
			GcLeaderboard_t453887929 * L_7 = V_1;
			NullCheck(L_7);
			bool L_8 = GcLeaderboard_Loading_m1117879034(L_7, /*hidden argument*/NULL);
			V_0 = L_8;
			IL2CPP_LEAVE(0x71, FINALLY_005c);
		}

IL_004a:
		{
		}

IL_004b:
		{
			bool L_9 = Enumerator_MoveNext_m1731154041((&V_2), /*hidden argument*/Enumerator_MoveNext_m1731154041_MethodInfo_var);
			if (L_9)
			{
				goto IL_0024;
			}
		}

IL_0057:
		{
			IL2CPP_LEAVE(0x6A, FINALLY_005c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_005c;
	}

FINALLY_005c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m210984790((&V_2), /*hidden argument*/Enumerator_Dispose_m210984790_MethodInfo_var);
		IL2CPP_END_FINALLY(92)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(92)
	{
		IL2CPP_JUMP_TBL(0x71, IL_0071)
		IL2CPP_JUMP_TBL(0x6A, IL_006a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_006a:
	{
		V_0 = (bool)0;
		goto IL_0071;
	}

IL_0071:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::VerifyAuthentication()
extern Il2CppClass* ILocalUser_t2210666073_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4014904582;
extern const uint32_t GameCenterPlatform_VerifyAuthentication_m4148852888_MetadataUsageId;
extern "C"  bool GameCenterPlatform_VerifyAuthentication_m4148852888 (GameCenterPlatform_t2156144444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_VerifyAuthentication_m4148852888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Il2CppObject * L_0 = GameCenterPlatform_get_localUser_m3187393722(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t2210666073_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral4014904582, /*hidden argument*/NULL);
		V_0 = (bool)0;
		goto IL_002a;
	}

IL_0023:
	{
		V_0 = (bool)1;
		goto IL_002a;
	}

IL_002a:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowAchievementsUI()
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowAchievementsUI_m217572822_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowAchievementsUI_m217572822 (GameCenterPlatform_t2156144444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowAchievementsUI_m217572822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		goto IL_0016;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowAchievementsUI_m4211451772(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI()
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowLeaderboardUI_m3149996419_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowLeaderboardUI_m3149996419 (GameCenterPlatform_t2156144444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowLeaderboardUI_m3149996419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		goto IL_0016;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowLeaderboardUI_m3138464075(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearUsers(System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearUsers_m28146411_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearUsers_m28146411 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearUsers_m28146411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size0;
		GameCenterPlatform_SafeClearArray_m2690967919(NULL /*static, unused*/, (((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_3()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUser(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetUser_m4136306572_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetUser_m4136306572 (Il2CppObject * __this /* static, unused */, GcUserProfileData_t3198293052  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetUser_m4136306572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number1;
		GcUserProfileData_AddToArray_m2451723029((&___data0), (((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_3()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUserImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetUserImage_m3665873800_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetUserImage_m3665873800 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetUserImage_m3665873800_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Texture2D_t3542995729 * L_0 = ___texture0;
		int32_t L_1 = ___number1;
		GameCenterPlatform_SafeSetUserImage_m4283674749(NULL /*static, unused*/, (((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_3()), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerUsersCallbackWrapper(System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m9088308_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerUsersCallbackWrapper_m2708763894_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerUsersCallbackWrapper_m2708763894 (Il2CppObject * __this /* static, unused */, Action_1_t3263047812 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerUsersCallbackWrapper_m2708763894_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t3263047812 * L_0 = ___callback0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Action_1_t3263047812 * L_1 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		UserProfileU5BU5D_t2930725895* L_2 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_users_3();
		NullCheck(L_1);
		Action_1_Invoke_m9088308(L_1, (IUserProfileU5BU5D_t3461248430*)(IUserProfileU5BU5D_t3461248430*)L_2, /*hidden argument*/Action_1_Invoke_m9088308_MethodInfo_var);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadUsers(System.String[],System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>)
extern Il2CppClass* UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m9088308_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadUsers_m4218470560_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadUsers_m4218470560 (GameCenterPlatform_t2156144444 * __this, StringU5BU5D_t1642385972* ___userIds0, Action_1_t3263047812 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadUsers_m4218470560_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		Action_1_t3263047812 * L_1 = ___callback1;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Action_1_t3263047812 * L_2 = ___callback1;
		NullCheck(L_2);
		Action_1_Invoke_m9088308(L_2, (IUserProfileU5BU5D_t3461248430*)(IUserProfileU5BU5D_t3461248430*)((UserProfileU5BU5D_t2930725895*)SZArrayNew(UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m9088308_MethodInfo_var);
	}

IL_001f:
	{
		goto IL_002b;
	}

IL_0024:
	{
		StringU5BU5D_t1642385972* L_3 = ___userIds0;
		Action_1_t3263047812 * L_4 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_LoadUsers_m1497175871(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeSetUserImage(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3515272376;
extern Il2CppCodeGenString* _stringLiteral1717534897;
extern const uint32_t GameCenterPlatform_SafeSetUserImage_m4283674749_MetadataUsageId;
extern "C"  void GameCenterPlatform_SafeSetUserImage_m4283674749 (Il2CppObject * __this /* static, unused */, UserProfileU5BU5D_t2930725895** ___array0, Texture2D_t3542995729 * ___texture1, int32_t ___number2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SafeSetUserImage_m4283674749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UserProfileU5BU5D_t2930725895** L_0 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2930725895**)L_0)));
		int32_t L_1 = ___number2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2930725895**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = ___number2;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0029;
		}
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3515272376, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_3 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3598323350(L_3, ((int32_t)76), ((int32_t)76), /*hidden argument*/NULL);
		___texture1 = L_3;
	}

IL_0029:
	{
		UserProfileU5BU5D_t2930725895** L_4 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2930725895**)L_4)));
		int32_t L_5 = ___number2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2930725895**)L_4)))->max_length))))) <= ((int32_t)L_5)))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_6 = ___number2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0049;
		}
	}
	{
		UserProfileU5BU5D_t2930725895** L_7 = ___array0;
		int32_t L_8 = ___number2;
		NullCheck((*((UserProfileU5BU5D_t2930725895**)L_7)));
		int32_t L_9 = L_8;
		UserProfile_t3365630962 * L_10 = ((*((UserProfileU5BU5D_t2930725895**)L_7)))->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		Texture2D_t3542995729 * L_11 = ___texture1;
		NullCheck(L_10);
		UserProfile_SetImage_m3142478163(L_10, L_11, /*hidden argument*/NULL);
		goto IL_0053;
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1717534897, /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeClearArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern Il2CppClass* UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SafeClearArray_m2690967919_MetadataUsageId;
extern "C"  void GameCenterPlatform_SafeClearArray_m2690967919 (Il2CppObject * __this /* static, unused */, UserProfileU5BU5D_t2930725895** ___array0, int32_t ___size1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SafeClearArray_m2690967919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UserProfileU5BU5D_t2930725895** L_0 = ___array0;
		if (!(*((UserProfileU5BU5D_t2930725895**)L_0)))
		{
			goto IL_0012;
		}
	}
	{
		UserProfileU5BU5D_t2930725895** L_1 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2930725895**)L_1)));
		int32_t L_2 = ___size1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2930725895**)L_1)))->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_001a;
		}
	}

IL_0012:
	{
		UserProfileU5BU5D_t2930725895** L_3 = ___array0;
		int32_t L_4 = ___size1;
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)((UserProfileU5BU5D_t2930725895*)SZArrayNew(UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var, (uint32_t)L_4));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)((UserProfileU5BU5D_t2930725895*)SZArrayNew(UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var, (uint32_t)L_4)));
	}

IL_001a:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILeaderboard UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateLeaderboard()
extern Il2CppClass* Leaderboard_t4160680639_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_CreateLeaderboard_m1959129937_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_CreateLeaderboard_m1959129937 (GameCenterPlatform_t2156144444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_CreateLeaderboard_m1959129937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Leaderboard_t4160680639 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		Leaderboard_t4160680639 * L_0 = (Leaderboard_t4160680639 *)il2cpp_codegen_object_new(Leaderboard_t4160680639_il2cpp_TypeInfo_var);
		Leaderboard__ctor_m1521627019(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Leaderboard_t4160680639 * L_1 = V_0;
		V_1 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Il2CppObject * L_2 = V_1;
		return L_2;
	}
}
// UnityEngine.SocialPlatforms.IAchievement UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateAchievement()
extern Il2CppClass* Achievement_t1333316625_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_CreateAchievement_m2992667237_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_CreateAchievement_m2992667237 (GameCenterPlatform_t2156144444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_CreateAchievement_m2992667237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Achievement_t1333316625 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		Achievement_t1333316625 * L_0 = (Achievement_t1333316625 *)il2cpp_codegen_object_new(Achievement_t1333316625_il2cpp_TypeInfo_var);
		Achievement__ctor_m3960800585(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Achievement_t1333316625 * L_1 = V_0;
		V_1 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Il2CppObject * L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerResetAchievementCallback(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerResetAchievementCallback_m247723933_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerResetAchievementCallback_m247723933 (Il2CppObject * __this /* static, unused */, bool ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerResetAchievementCallback_m247723933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_ResetAchievements_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_ResetAchievements_4();
		bool L_2 = ___result0;
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.cctor()
extern Il2CppClass* AchievementDescriptionU5BU5D_t847281182_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t4117976357_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2974994212_MethodInfo_var;
extern const uint32_t GameCenterPlatform__cctor_m2403939600_MetadataUsageId;
extern "C"  void GameCenterPlatform__cctor_m2403939600 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform__cctor_m2403939600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_adCache_1(((AchievementDescriptionU5BU5D_t847281182*)SZArrayNew(AchievementDescriptionU5BU5D_t847281182_il2cpp_TypeInfo_var, (uint32_t)0)));
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_friends_2(((UserProfileU5BU5D_t2930725895*)SZArrayNew(UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var, (uint32_t)0)));
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_users_3(((UserProfileU5BU5D_t2930725895*)SZArrayNew(UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var, (uint32_t)0)));
		List_1_t4117976357 * L_0 = (List_1_t4117976357 *)il2cpp_codegen_object_new(List_1_t4117976357_il2cpp_TypeInfo_var);
		List_1__ctor_m2974994212(L_0, /*hidden argument*/List_1__ctor_m2974994212_MethodInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_m_GcBoards_6(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform/<UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate>c__AnonStorey0::.ctor()
extern "C"  void U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0__ctor_m1252567582 (U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t1170095138 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform/<UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate>c__AnonStorey0::<>m__0(System.Boolean,System.String)
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_U3CU3Em__0_m648091976_MetadataUsageId;
extern "C"  void U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_U3CU3Em__0_m648091976 (U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t1170095138 * __this, bool ___success0, String_t* ___error1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_U3CU3Em__0_m648091976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = __this->get_callback_0();
		bool L_1 = ___success0;
		NullCheck(L_0);
		Action_1_Invoke_m3662000152(L_0, L_1, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		return;
	}
}
// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern Il2CppClass* Achievement_t1333316625_il2cpp_TypeInfo_var;
extern const uint32_t GcAchievementData_ToAchievement_m962894180_MetadataUsageId;
extern "C"  Achievement_t1333316625 * GcAchievementData_ToAchievement_m962894180 (GcAchievementData_t1754866149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GcAchievementData_ToAchievement_m962894180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Achievement_t1333316625 * V_1 = NULL;
	double G_B2_0 = 0.0;
	String_t* G_B2_1 = NULL;
	double G_B1_0 = 0.0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	double G_B3_1 = 0.0;
	String_t* G_B3_2 = NULL;
	int32_t G_B5_0 = 0;
	double G_B5_1 = 0.0;
	String_t* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	double G_B4_1 = 0.0;
	String_t* G_B4_2 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	double G_B6_2 = 0.0;
	String_t* G_B6_3 = NULL;
	{
		String_t* L_0 = __this->get_m_Identifier_0();
		double L_1 = __this->get_m_PercentCompleted_1();
		int32_t L_2 = __this->get_m_Completed_2();
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001e;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001f:
	{
		int32_t L_3 = __this->get_m_Hidden_3();
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		if (L_3)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			G_B5_2 = G_B3_2;
			goto IL_0030;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0031;
	}

IL_0030:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0031:
	{
		DateTime__ctor_m2857738939((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_m_LastReportedDate_4();
		DateTime_t693205669  L_5 = DateTime_AddSeconds_m722082155((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		Achievement_t1333316625 * L_6 = (Achievement_t1333316625 *)il2cpp_codegen_object_new(Achievement_t1333316625_il2cpp_TypeInfo_var);
		Achievement__ctor_m4089961863(L_6, G_B6_3, G_B6_2, (bool)G_B6_1, (bool)G_B6_0, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_005c;
	}

IL_005c:
	{
		Achievement_t1333316625 * L_7 = V_1;
		return L_7;
	}
}
extern "C"  Achievement_t1333316625 * GcAchievementData_ToAchievement_m962894180_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcAchievementData_t1754866149 * _thisAdjusted = reinterpret_cast<GcAchievementData_t1754866149 *>(__this + 1);
	return GcAchievementData_ToAchievement_m962894180(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t1754866149_marshal_pinvoke(const GcAchievementData_t1754866149& unmarshaled, GcAchievementData_t1754866149_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_Identifier_0());
	marshaled.___m_PercentCompleted_1 = unmarshaled.get_m_PercentCompleted_1();
	marshaled.___m_Completed_2 = unmarshaled.get_m_Completed_2();
	marshaled.___m_Hidden_3 = unmarshaled.get_m_Hidden_3();
	marshaled.___m_LastReportedDate_4 = unmarshaled.get_m_LastReportedDate_4();
}
extern "C" void GcAchievementData_t1754866149_marshal_pinvoke_back(const GcAchievementData_t1754866149_marshaled_pinvoke& marshaled, GcAchievementData_t1754866149& unmarshaled)
{
	unmarshaled.set_m_Identifier_0(il2cpp_codegen_marshal_string_result(marshaled.___m_Identifier_0));
	double unmarshaled_m_PercentCompleted_temp_1 = 0.0;
	unmarshaled_m_PercentCompleted_temp_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.set_m_PercentCompleted_1(unmarshaled_m_PercentCompleted_temp_1);
	int32_t unmarshaled_m_Completed_temp_2 = 0;
	unmarshaled_m_Completed_temp_2 = marshaled.___m_Completed_2;
	unmarshaled.set_m_Completed_2(unmarshaled_m_Completed_temp_2);
	int32_t unmarshaled_m_Hidden_temp_3 = 0;
	unmarshaled_m_Hidden_temp_3 = marshaled.___m_Hidden_3;
	unmarshaled.set_m_Hidden_3(unmarshaled_m_Hidden_temp_3);
	int32_t unmarshaled_m_LastReportedDate_temp_4 = 0;
	unmarshaled_m_LastReportedDate_temp_4 = marshaled.___m_LastReportedDate_4;
	unmarshaled.set_m_LastReportedDate_4(unmarshaled_m_LastReportedDate_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t1754866149_marshal_pinvoke_cleanup(GcAchievementData_t1754866149_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t1754866149_marshal_com(const GcAchievementData_t1754866149& unmarshaled, GcAchievementData_t1754866149_marshaled_com& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_Identifier_0());
	marshaled.___m_PercentCompleted_1 = unmarshaled.get_m_PercentCompleted_1();
	marshaled.___m_Completed_2 = unmarshaled.get_m_Completed_2();
	marshaled.___m_Hidden_3 = unmarshaled.get_m_Hidden_3();
	marshaled.___m_LastReportedDate_4 = unmarshaled.get_m_LastReportedDate_4();
}
extern "C" void GcAchievementData_t1754866149_marshal_com_back(const GcAchievementData_t1754866149_marshaled_com& marshaled, GcAchievementData_t1754866149& unmarshaled)
{
	unmarshaled.set_m_Identifier_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_Identifier_0));
	double unmarshaled_m_PercentCompleted_temp_1 = 0.0;
	unmarshaled_m_PercentCompleted_temp_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.set_m_PercentCompleted_1(unmarshaled_m_PercentCompleted_temp_1);
	int32_t unmarshaled_m_Completed_temp_2 = 0;
	unmarshaled_m_Completed_temp_2 = marshaled.___m_Completed_2;
	unmarshaled.set_m_Completed_2(unmarshaled_m_Completed_temp_2);
	int32_t unmarshaled_m_Hidden_temp_3 = 0;
	unmarshaled_m_Hidden_temp_3 = marshaled.___m_Hidden_3;
	unmarshaled.set_m_Hidden_3(unmarshaled_m_Hidden_temp_3);
	int32_t unmarshaled_m_LastReportedDate_temp_4 = 0;
	unmarshaled_m_LastReportedDate_temp_4 = marshaled.___m_LastReportedDate_4;
	unmarshaled.set_m_LastReportedDate_4(unmarshaled_m_LastReportedDate_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t1754866149_marshal_com_cleanup(GcAchievementData_t1754866149_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
