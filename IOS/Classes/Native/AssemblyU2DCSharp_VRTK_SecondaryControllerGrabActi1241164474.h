﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "AssemblyU2DCSharp_VRTK_SecondaryControllerGrabActi4095736311.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction
struct  VRTK_ControlDirectionGrabAction_t1241164474  : public VRTK_BaseGrabAction_t4095736311
{
public:
	// System.Single VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::ungrabDistance
	float ___ungrabDistance_10;
	// System.Single VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::releaseSnapSpeed
	float ___releaseSnapSpeed_11;
	// System.Boolean VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::lockZRotation
	bool ___lockZRotation_12;
	// UnityEngine.Vector3 VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::initialPosition
	Vector3_t2243707580  ___initialPosition_13;
	// UnityEngine.Quaternion VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::initialRotation
	Quaternion_t4030073918  ___initialRotation_14;
	// UnityEngine.Quaternion VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::releaseRotation
	Quaternion_t4030073918  ___releaseRotation_15;
	// UnityEngine.Coroutine VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::snappingOnRelease
	Coroutine_t2299508840 * ___snappingOnRelease_16;

public:
	inline static int32_t get_offset_of_ungrabDistance_10() { return static_cast<int32_t>(offsetof(VRTK_ControlDirectionGrabAction_t1241164474, ___ungrabDistance_10)); }
	inline float get_ungrabDistance_10() const { return ___ungrabDistance_10; }
	inline float* get_address_of_ungrabDistance_10() { return &___ungrabDistance_10; }
	inline void set_ungrabDistance_10(float value)
	{
		___ungrabDistance_10 = value;
	}

	inline static int32_t get_offset_of_releaseSnapSpeed_11() { return static_cast<int32_t>(offsetof(VRTK_ControlDirectionGrabAction_t1241164474, ___releaseSnapSpeed_11)); }
	inline float get_releaseSnapSpeed_11() const { return ___releaseSnapSpeed_11; }
	inline float* get_address_of_releaseSnapSpeed_11() { return &___releaseSnapSpeed_11; }
	inline void set_releaseSnapSpeed_11(float value)
	{
		___releaseSnapSpeed_11 = value;
	}

	inline static int32_t get_offset_of_lockZRotation_12() { return static_cast<int32_t>(offsetof(VRTK_ControlDirectionGrabAction_t1241164474, ___lockZRotation_12)); }
	inline bool get_lockZRotation_12() const { return ___lockZRotation_12; }
	inline bool* get_address_of_lockZRotation_12() { return &___lockZRotation_12; }
	inline void set_lockZRotation_12(bool value)
	{
		___lockZRotation_12 = value;
	}

	inline static int32_t get_offset_of_initialPosition_13() { return static_cast<int32_t>(offsetof(VRTK_ControlDirectionGrabAction_t1241164474, ___initialPosition_13)); }
	inline Vector3_t2243707580  get_initialPosition_13() const { return ___initialPosition_13; }
	inline Vector3_t2243707580 * get_address_of_initialPosition_13() { return &___initialPosition_13; }
	inline void set_initialPosition_13(Vector3_t2243707580  value)
	{
		___initialPosition_13 = value;
	}

	inline static int32_t get_offset_of_initialRotation_14() { return static_cast<int32_t>(offsetof(VRTK_ControlDirectionGrabAction_t1241164474, ___initialRotation_14)); }
	inline Quaternion_t4030073918  get_initialRotation_14() const { return ___initialRotation_14; }
	inline Quaternion_t4030073918 * get_address_of_initialRotation_14() { return &___initialRotation_14; }
	inline void set_initialRotation_14(Quaternion_t4030073918  value)
	{
		___initialRotation_14 = value;
	}

	inline static int32_t get_offset_of_releaseRotation_15() { return static_cast<int32_t>(offsetof(VRTK_ControlDirectionGrabAction_t1241164474, ___releaseRotation_15)); }
	inline Quaternion_t4030073918  get_releaseRotation_15() const { return ___releaseRotation_15; }
	inline Quaternion_t4030073918 * get_address_of_releaseRotation_15() { return &___releaseRotation_15; }
	inline void set_releaseRotation_15(Quaternion_t4030073918  value)
	{
		___releaseRotation_15 = value;
	}

	inline static int32_t get_offset_of_snappingOnRelease_16() { return static_cast<int32_t>(offsetof(VRTK_ControlDirectionGrabAction_t1241164474, ___snappingOnRelease_16)); }
	inline Coroutine_t2299508840 * get_snappingOnRelease_16() const { return ___snappingOnRelease_16; }
	inline Coroutine_t2299508840 ** get_address_of_snappingOnRelease_16() { return &___snappingOnRelease_16; }
	inline void set_snappingOnRelease_16(Coroutine_t2299508840 * value)
	{
		___snappingOnRelease_16 = value;
		Il2CppCodeGenWriteBarrier(&___snappingOnRelease_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
