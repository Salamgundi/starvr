﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.PlayerClimbEventHandler
struct PlayerClimbEventHandler_t2826600604;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_PlayerClimbEventArgs2537585745.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.PlayerClimbEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void PlayerClimbEventHandler__ctor_m1821284684 (PlayerClimbEventHandler_t2826600604 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PlayerClimbEventHandler::Invoke(System.Object,VRTK.PlayerClimbEventArgs)
extern "C"  void PlayerClimbEventHandler_Invoke_m1037493820 (PlayerClimbEventHandler_t2826600604 * __this, Il2CppObject * ___sender0, PlayerClimbEventArgs_t2537585745  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.PlayerClimbEventHandler::BeginInvoke(System.Object,VRTK.PlayerClimbEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PlayerClimbEventHandler_BeginInvoke_m2632249121 (PlayerClimbEventHandler_t2826600604 * __this, Il2CppObject * ___sender0, PlayerClimbEventArgs_t2537585745  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PlayerClimbEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void PlayerClimbEventHandler_EndInvoke_m1157519658 (PlayerClimbEventHandler_t2826600604 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
