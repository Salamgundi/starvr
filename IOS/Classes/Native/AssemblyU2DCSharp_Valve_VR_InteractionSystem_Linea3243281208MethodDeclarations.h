﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.LinearAnimator
struct LinearAnimator_t3243281208;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.LinearAnimator::.ctor()
extern "C"  void LinearAnimator__ctor_m1214929386 (LinearAnimator_t3243281208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearAnimator::Awake()
extern "C"  void LinearAnimator_Awake_m172161401 (LinearAnimator_t3243281208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearAnimator::Update()
extern "C"  void LinearAnimator_Update_m4009489717 (LinearAnimator_t3243281208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
