﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_SDK_FallbackBoundaries1566319879.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.SDK_OculusVRBoundaries
struct  SDK_OculusVRBoundaries_t3279427364  : public SDK_FallbackBoundaries_t1566319879
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
