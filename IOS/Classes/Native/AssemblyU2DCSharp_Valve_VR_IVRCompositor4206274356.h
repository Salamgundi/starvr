﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.IVRCompositor/_SetTrackingSpace
struct _SetTrackingSpace_t3662949163;
// Valve.VR.IVRCompositor/_GetTrackingSpace
struct _GetTrackingSpace_t10051991;
// Valve.VR.IVRCompositor/_WaitGetPoses
struct _WaitGetPoses_t4192584901;
// Valve.VR.IVRCompositor/_GetLastPoses
struct _GetLastPoses_t2717021868;
// Valve.VR.IVRCompositor/_GetLastPoseForTrackedDeviceIndex
struct _GetLastPoseForTrackedDeviceIndex_t3158332482;
// Valve.VR.IVRCompositor/_Submit
struct _Submit_t938257482;
// Valve.VR.IVRCompositor/_ClearLastSubmittedFrame
struct _ClearLastSubmittedFrame_t32198265;
// Valve.VR.IVRCompositor/_PostPresentHandoff
struct _PostPresentHandoff_t4129629097;
// Valve.VR.IVRCompositor/_GetFrameTiming
struct _GetFrameTiming_t1729400753;
// Valve.VR.IVRCompositor/_GetFrameTimings
struct _GetFrameTimings_t711480574;
// Valve.VR.IVRCompositor/_GetFrameTimeRemaining
struct _GetFrameTimeRemaining_t2433513766;
// Valve.VR.IVRCompositor/_GetCumulativeStats
struct _GetCumulativeStats_t1488691712;
// Valve.VR.IVRCompositor/_FadeToColor
struct _FadeToColor_t4129572280;
// Valve.VR.IVRCompositor/_GetCurrentFadeColor
struct _GetCurrentFadeColor_t2258064488;
// Valve.VR.IVRCompositor/_FadeGrid
struct _FadeGrid_t1389933364;
// Valve.VR.IVRCompositor/_GetCurrentGridAlpha
struct _GetCurrentGridAlpha_t2030734261;
// Valve.VR.IVRCompositor/_SetSkyboxOverride
struct _SetSkyboxOverride_t2590693784;
// Valve.VR.IVRCompositor/_ClearSkyboxOverride
struct _ClearSkyboxOverride_t745528287;
// Valve.VR.IVRCompositor/_CompositorBringToFront
struct _CompositorBringToFront_t2294069769;
// Valve.VR.IVRCompositor/_CompositorGoToBack
struct _CompositorGoToBack_t2760466547;
// Valve.VR.IVRCompositor/_CompositorQuit
struct _CompositorQuit_t2273085604;
// Valve.VR.IVRCompositor/_IsFullscreen
struct _IsFullscreen_t988890383;
// Valve.VR.IVRCompositor/_GetCurrentSceneFocusProcess
struct _GetCurrentSceneFocusProcess_t1910270748;
// Valve.VR.IVRCompositor/_GetLastFrameRenderer
struct _GetLastFrameRenderer_t1990293024;
// Valve.VR.IVRCompositor/_CanRenderScene
struct _CanRenderScene_t808393456;
// Valve.VR.IVRCompositor/_ShowMirrorWindow
struct _ShowMirrorWindow_t1161105516;
// Valve.VR.IVRCompositor/_HideMirrorWindow
struct _HideMirrorWindow_t396471547;
// Valve.VR.IVRCompositor/_IsMirrorWindowVisible
struct _IsMirrorWindowVisible_t410903031;
// Valve.VR.IVRCompositor/_CompositorDumpImages
struct _CompositorDumpImages_t3462826167;
// Valve.VR.IVRCompositor/_ShouldAppRenderWithLowResources
struct _ShouldAppRenderWithLowResources_t1834060663;
// Valve.VR.IVRCompositor/_ForceInterleavedReprojectionOn
struct _ForceInterleavedReprojectionOn_t78695371;
// Valve.VR.IVRCompositor/_ForceReconnectProcess
struct _ForceReconnectProcess_t3057442189;
// Valve.VR.IVRCompositor/_SuspendRendering
struct _SuspendRendering_t3440691638;
// Valve.VR.IVRCompositor/_GetMirrorTextureD3D11
struct _GetMirrorTextureD3D11_t2322134645;
// Valve.VR.IVRCompositor/_ReleaseMirrorTextureD3D11
struct _ReleaseMirrorTextureD3D11_t2100129624;
// Valve.VR.IVRCompositor/_GetMirrorTextureGL
struct _GetMirrorTextureGL_t3071699813;
// Valve.VR.IVRCompositor/_ReleaseSharedGLTexture
struct _ReleaseSharedGLTexture_t3443550108;
// Valve.VR.IVRCompositor/_LockGLSharedTextureForAccess
struct _LockGLSharedTextureForAccess_t2180462479;
// Valve.VR.IVRCompositor/_UnlockGLSharedTextureForAccess
struct _UnlockGLSharedTextureForAccess_t320290112;
// Valve.VR.IVRCompositor/_GetVulkanInstanceExtensionsRequired
struct _GetVulkanInstanceExtensionsRequired_t2249042297;
// Valve.VR.IVRCompositor/_GetVulkanDeviceExtensionsRequired
struct _GetVulkanDeviceExtensionsRequired_t3459249220;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.IVRCompositor
struct  IVRCompositor_t4206274356 
{
public:
	// Valve.VR.IVRCompositor/_SetTrackingSpace Valve.VR.IVRCompositor::SetTrackingSpace
	_SetTrackingSpace_t3662949163 * ___SetTrackingSpace_0;
	// Valve.VR.IVRCompositor/_GetTrackingSpace Valve.VR.IVRCompositor::GetTrackingSpace
	_GetTrackingSpace_t10051991 * ___GetTrackingSpace_1;
	// Valve.VR.IVRCompositor/_WaitGetPoses Valve.VR.IVRCompositor::WaitGetPoses
	_WaitGetPoses_t4192584901 * ___WaitGetPoses_2;
	// Valve.VR.IVRCompositor/_GetLastPoses Valve.VR.IVRCompositor::GetLastPoses
	_GetLastPoses_t2717021868 * ___GetLastPoses_3;
	// Valve.VR.IVRCompositor/_GetLastPoseForTrackedDeviceIndex Valve.VR.IVRCompositor::GetLastPoseForTrackedDeviceIndex
	_GetLastPoseForTrackedDeviceIndex_t3158332482 * ___GetLastPoseForTrackedDeviceIndex_4;
	// Valve.VR.IVRCompositor/_Submit Valve.VR.IVRCompositor::Submit
	_Submit_t938257482 * ___Submit_5;
	// Valve.VR.IVRCompositor/_ClearLastSubmittedFrame Valve.VR.IVRCompositor::ClearLastSubmittedFrame
	_ClearLastSubmittedFrame_t32198265 * ___ClearLastSubmittedFrame_6;
	// Valve.VR.IVRCompositor/_PostPresentHandoff Valve.VR.IVRCompositor::PostPresentHandoff
	_PostPresentHandoff_t4129629097 * ___PostPresentHandoff_7;
	// Valve.VR.IVRCompositor/_GetFrameTiming Valve.VR.IVRCompositor::GetFrameTiming
	_GetFrameTiming_t1729400753 * ___GetFrameTiming_8;
	// Valve.VR.IVRCompositor/_GetFrameTimings Valve.VR.IVRCompositor::GetFrameTimings
	_GetFrameTimings_t711480574 * ___GetFrameTimings_9;
	// Valve.VR.IVRCompositor/_GetFrameTimeRemaining Valve.VR.IVRCompositor::GetFrameTimeRemaining
	_GetFrameTimeRemaining_t2433513766 * ___GetFrameTimeRemaining_10;
	// Valve.VR.IVRCompositor/_GetCumulativeStats Valve.VR.IVRCompositor::GetCumulativeStats
	_GetCumulativeStats_t1488691712 * ___GetCumulativeStats_11;
	// Valve.VR.IVRCompositor/_FadeToColor Valve.VR.IVRCompositor::FadeToColor
	_FadeToColor_t4129572280 * ___FadeToColor_12;
	// Valve.VR.IVRCompositor/_GetCurrentFadeColor Valve.VR.IVRCompositor::GetCurrentFadeColor
	_GetCurrentFadeColor_t2258064488 * ___GetCurrentFadeColor_13;
	// Valve.VR.IVRCompositor/_FadeGrid Valve.VR.IVRCompositor::FadeGrid
	_FadeGrid_t1389933364 * ___FadeGrid_14;
	// Valve.VR.IVRCompositor/_GetCurrentGridAlpha Valve.VR.IVRCompositor::GetCurrentGridAlpha
	_GetCurrentGridAlpha_t2030734261 * ___GetCurrentGridAlpha_15;
	// Valve.VR.IVRCompositor/_SetSkyboxOverride Valve.VR.IVRCompositor::SetSkyboxOverride
	_SetSkyboxOverride_t2590693784 * ___SetSkyboxOverride_16;
	// Valve.VR.IVRCompositor/_ClearSkyboxOverride Valve.VR.IVRCompositor::ClearSkyboxOverride
	_ClearSkyboxOverride_t745528287 * ___ClearSkyboxOverride_17;
	// Valve.VR.IVRCompositor/_CompositorBringToFront Valve.VR.IVRCompositor::CompositorBringToFront
	_CompositorBringToFront_t2294069769 * ___CompositorBringToFront_18;
	// Valve.VR.IVRCompositor/_CompositorGoToBack Valve.VR.IVRCompositor::CompositorGoToBack
	_CompositorGoToBack_t2760466547 * ___CompositorGoToBack_19;
	// Valve.VR.IVRCompositor/_CompositorQuit Valve.VR.IVRCompositor::CompositorQuit
	_CompositorQuit_t2273085604 * ___CompositorQuit_20;
	// Valve.VR.IVRCompositor/_IsFullscreen Valve.VR.IVRCompositor::IsFullscreen
	_IsFullscreen_t988890383 * ___IsFullscreen_21;
	// Valve.VR.IVRCompositor/_GetCurrentSceneFocusProcess Valve.VR.IVRCompositor::GetCurrentSceneFocusProcess
	_GetCurrentSceneFocusProcess_t1910270748 * ___GetCurrentSceneFocusProcess_22;
	// Valve.VR.IVRCompositor/_GetLastFrameRenderer Valve.VR.IVRCompositor::GetLastFrameRenderer
	_GetLastFrameRenderer_t1990293024 * ___GetLastFrameRenderer_23;
	// Valve.VR.IVRCompositor/_CanRenderScene Valve.VR.IVRCompositor::CanRenderScene
	_CanRenderScene_t808393456 * ___CanRenderScene_24;
	// Valve.VR.IVRCompositor/_ShowMirrorWindow Valve.VR.IVRCompositor::ShowMirrorWindow
	_ShowMirrorWindow_t1161105516 * ___ShowMirrorWindow_25;
	// Valve.VR.IVRCompositor/_HideMirrorWindow Valve.VR.IVRCompositor::HideMirrorWindow
	_HideMirrorWindow_t396471547 * ___HideMirrorWindow_26;
	// Valve.VR.IVRCompositor/_IsMirrorWindowVisible Valve.VR.IVRCompositor::IsMirrorWindowVisible
	_IsMirrorWindowVisible_t410903031 * ___IsMirrorWindowVisible_27;
	// Valve.VR.IVRCompositor/_CompositorDumpImages Valve.VR.IVRCompositor::CompositorDumpImages
	_CompositorDumpImages_t3462826167 * ___CompositorDumpImages_28;
	// Valve.VR.IVRCompositor/_ShouldAppRenderWithLowResources Valve.VR.IVRCompositor::ShouldAppRenderWithLowResources
	_ShouldAppRenderWithLowResources_t1834060663 * ___ShouldAppRenderWithLowResources_29;
	// Valve.VR.IVRCompositor/_ForceInterleavedReprojectionOn Valve.VR.IVRCompositor::ForceInterleavedReprojectionOn
	_ForceInterleavedReprojectionOn_t78695371 * ___ForceInterleavedReprojectionOn_30;
	// Valve.VR.IVRCompositor/_ForceReconnectProcess Valve.VR.IVRCompositor::ForceReconnectProcess
	_ForceReconnectProcess_t3057442189 * ___ForceReconnectProcess_31;
	// Valve.VR.IVRCompositor/_SuspendRendering Valve.VR.IVRCompositor::SuspendRendering
	_SuspendRendering_t3440691638 * ___SuspendRendering_32;
	// Valve.VR.IVRCompositor/_GetMirrorTextureD3D11 Valve.VR.IVRCompositor::GetMirrorTextureD3D11
	_GetMirrorTextureD3D11_t2322134645 * ___GetMirrorTextureD3D11_33;
	// Valve.VR.IVRCompositor/_ReleaseMirrorTextureD3D11 Valve.VR.IVRCompositor::ReleaseMirrorTextureD3D11
	_ReleaseMirrorTextureD3D11_t2100129624 * ___ReleaseMirrorTextureD3D11_34;
	// Valve.VR.IVRCompositor/_GetMirrorTextureGL Valve.VR.IVRCompositor::GetMirrorTextureGL
	_GetMirrorTextureGL_t3071699813 * ___GetMirrorTextureGL_35;
	// Valve.VR.IVRCompositor/_ReleaseSharedGLTexture Valve.VR.IVRCompositor::ReleaseSharedGLTexture
	_ReleaseSharedGLTexture_t3443550108 * ___ReleaseSharedGLTexture_36;
	// Valve.VR.IVRCompositor/_LockGLSharedTextureForAccess Valve.VR.IVRCompositor::LockGLSharedTextureForAccess
	_LockGLSharedTextureForAccess_t2180462479 * ___LockGLSharedTextureForAccess_37;
	// Valve.VR.IVRCompositor/_UnlockGLSharedTextureForAccess Valve.VR.IVRCompositor::UnlockGLSharedTextureForAccess
	_UnlockGLSharedTextureForAccess_t320290112 * ___UnlockGLSharedTextureForAccess_38;
	// Valve.VR.IVRCompositor/_GetVulkanInstanceExtensionsRequired Valve.VR.IVRCompositor::GetVulkanInstanceExtensionsRequired
	_GetVulkanInstanceExtensionsRequired_t2249042297 * ___GetVulkanInstanceExtensionsRequired_39;
	// Valve.VR.IVRCompositor/_GetVulkanDeviceExtensionsRequired Valve.VR.IVRCompositor::GetVulkanDeviceExtensionsRequired
	_GetVulkanDeviceExtensionsRequired_t3459249220 * ___GetVulkanDeviceExtensionsRequired_40;

public:
	inline static int32_t get_offset_of_SetTrackingSpace_0() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___SetTrackingSpace_0)); }
	inline _SetTrackingSpace_t3662949163 * get_SetTrackingSpace_0() const { return ___SetTrackingSpace_0; }
	inline _SetTrackingSpace_t3662949163 ** get_address_of_SetTrackingSpace_0() { return &___SetTrackingSpace_0; }
	inline void set_SetTrackingSpace_0(_SetTrackingSpace_t3662949163 * value)
	{
		___SetTrackingSpace_0 = value;
		Il2CppCodeGenWriteBarrier(&___SetTrackingSpace_0, value);
	}

	inline static int32_t get_offset_of_GetTrackingSpace_1() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetTrackingSpace_1)); }
	inline _GetTrackingSpace_t10051991 * get_GetTrackingSpace_1() const { return ___GetTrackingSpace_1; }
	inline _GetTrackingSpace_t10051991 ** get_address_of_GetTrackingSpace_1() { return &___GetTrackingSpace_1; }
	inline void set_GetTrackingSpace_1(_GetTrackingSpace_t10051991 * value)
	{
		___GetTrackingSpace_1 = value;
		Il2CppCodeGenWriteBarrier(&___GetTrackingSpace_1, value);
	}

	inline static int32_t get_offset_of_WaitGetPoses_2() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___WaitGetPoses_2)); }
	inline _WaitGetPoses_t4192584901 * get_WaitGetPoses_2() const { return ___WaitGetPoses_2; }
	inline _WaitGetPoses_t4192584901 ** get_address_of_WaitGetPoses_2() { return &___WaitGetPoses_2; }
	inline void set_WaitGetPoses_2(_WaitGetPoses_t4192584901 * value)
	{
		___WaitGetPoses_2 = value;
		Il2CppCodeGenWriteBarrier(&___WaitGetPoses_2, value);
	}

	inline static int32_t get_offset_of_GetLastPoses_3() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetLastPoses_3)); }
	inline _GetLastPoses_t2717021868 * get_GetLastPoses_3() const { return ___GetLastPoses_3; }
	inline _GetLastPoses_t2717021868 ** get_address_of_GetLastPoses_3() { return &___GetLastPoses_3; }
	inline void set_GetLastPoses_3(_GetLastPoses_t2717021868 * value)
	{
		___GetLastPoses_3 = value;
		Il2CppCodeGenWriteBarrier(&___GetLastPoses_3, value);
	}

	inline static int32_t get_offset_of_GetLastPoseForTrackedDeviceIndex_4() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetLastPoseForTrackedDeviceIndex_4)); }
	inline _GetLastPoseForTrackedDeviceIndex_t3158332482 * get_GetLastPoseForTrackedDeviceIndex_4() const { return ___GetLastPoseForTrackedDeviceIndex_4; }
	inline _GetLastPoseForTrackedDeviceIndex_t3158332482 ** get_address_of_GetLastPoseForTrackedDeviceIndex_4() { return &___GetLastPoseForTrackedDeviceIndex_4; }
	inline void set_GetLastPoseForTrackedDeviceIndex_4(_GetLastPoseForTrackedDeviceIndex_t3158332482 * value)
	{
		___GetLastPoseForTrackedDeviceIndex_4 = value;
		Il2CppCodeGenWriteBarrier(&___GetLastPoseForTrackedDeviceIndex_4, value);
	}

	inline static int32_t get_offset_of_Submit_5() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___Submit_5)); }
	inline _Submit_t938257482 * get_Submit_5() const { return ___Submit_5; }
	inline _Submit_t938257482 ** get_address_of_Submit_5() { return &___Submit_5; }
	inline void set_Submit_5(_Submit_t938257482 * value)
	{
		___Submit_5 = value;
		Il2CppCodeGenWriteBarrier(&___Submit_5, value);
	}

	inline static int32_t get_offset_of_ClearLastSubmittedFrame_6() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___ClearLastSubmittedFrame_6)); }
	inline _ClearLastSubmittedFrame_t32198265 * get_ClearLastSubmittedFrame_6() const { return ___ClearLastSubmittedFrame_6; }
	inline _ClearLastSubmittedFrame_t32198265 ** get_address_of_ClearLastSubmittedFrame_6() { return &___ClearLastSubmittedFrame_6; }
	inline void set_ClearLastSubmittedFrame_6(_ClearLastSubmittedFrame_t32198265 * value)
	{
		___ClearLastSubmittedFrame_6 = value;
		Il2CppCodeGenWriteBarrier(&___ClearLastSubmittedFrame_6, value);
	}

	inline static int32_t get_offset_of_PostPresentHandoff_7() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___PostPresentHandoff_7)); }
	inline _PostPresentHandoff_t4129629097 * get_PostPresentHandoff_7() const { return ___PostPresentHandoff_7; }
	inline _PostPresentHandoff_t4129629097 ** get_address_of_PostPresentHandoff_7() { return &___PostPresentHandoff_7; }
	inline void set_PostPresentHandoff_7(_PostPresentHandoff_t4129629097 * value)
	{
		___PostPresentHandoff_7 = value;
		Il2CppCodeGenWriteBarrier(&___PostPresentHandoff_7, value);
	}

	inline static int32_t get_offset_of_GetFrameTiming_8() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetFrameTiming_8)); }
	inline _GetFrameTiming_t1729400753 * get_GetFrameTiming_8() const { return ___GetFrameTiming_8; }
	inline _GetFrameTiming_t1729400753 ** get_address_of_GetFrameTiming_8() { return &___GetFrameTiming_8; }
	inline void set_GetFrameTiming_8(_GetFrameTiming_t1729400753 * value)
	{
		___GetFrameTiming_8 = value;
		Il2CppCodeGenWriteBarrier(&___GetFrameTiming_8, value);
	}

	inline static int32_t get_offset_of_GetFrameTimings_9() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetFrameTimings_9)); }
	inline _GetFrameTimings_t711480574 * get_GetFrameTimings_9() const { return ___GetFrameTimings_9; }
	inline _GetFrameTimings_t711480574 ** get_address_of_GetFrameTimings_9() { return &___GetFrameTimings_9; }
	inline void set_GetFrameTimings_9(_GetFrameTimings_t711480574 * value)
	{
		___GetFrameTimings_9 = value;
		Il2CppCodeGenWriteBarrier(&___GetFrameTimings_9, value);
	}

	inline static int32_t get_offset_of_GetFrameTimeRemaining_10() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetFrameTimeRemaining_10)); }
	inline _GetFrameTimeRemaining_t2433513766 * get_GetFrameTimeRemaining_10() const { return ___GetFrameTimeRemaining_10; }
	inline _GetFrameTimeRemaining_t2433513766 ** get_address_of_GetFrameTimeRemaining_10() { return &___GetFrameTimeRemaining_10; }
	inline void set_GetFrameTimeRemaining_10(_GetFrameTimeRemaining_t2433513766 * value)
	{
		___GetFrameTimeRemaining_10 = value;
		Il2CppCodeGenWriteBarrier(&___GetFrameTimeRemaining_10, value);
	}

	inline static int32_t get_offset_of_GetCumulativeStats_11() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetCumulativeStats_11)); }
	inline _GetCumulativeStats_t1488691712 * get_GetCumulativeStats_11() const { return ___GetCumulativeStats_11; }
	inline _GetCumulativeStats_t1488691712 ** get_address_of_GetCumulativeStats_11() { return &___GetCumulativeStats_11; }
	inline void set_GetCumulativeStats_11(_GetCumulativeStats_t1488691712 * value)
	{
		___GetCumulativeStats_11 = value;
		Il2CppCodeGenWriteBarrier(&___GetCumulativeStats_11, value);
	}

	inline static int32_t get_offset_of_FadeToColor_12() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___FadeToColor_12)); }
	inline _FadeToColor_t4129572280 * get_FadeToColor_12() const { return ___FadeToColor_12; }
	inline _FadeToColor_t4129572280 ** get_address_of_FadeToColor_12() { return &___FadeToColor_12; }
	inline void set_FadeToColor_12(_FadeToColor_t4129572280 * value)
	{
		___FadeToColor_12 = value;
		Il2CppCodeGenWriteBarrier(&___FadeToColor_12, value);
	}

	inline static int32_t get_offset_of_GetCurrentFadeColor_13() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetCurrentFadeColor_13)); }
	inline _GetCurrentFadeColor_t2258064488 * get_GetCurrentFadeColor_13() const { return ___GetCurrentFadeColor_13; }
	inline _GetCurrentFadeColor_t2258064488 ** get_address_of_GetCurrentFadeColor_13() { return &___GetCurrentFadeColor_13; }
	inline void set_GetCurrentFadeColor_13(_GetCurrentFadeColor_t2258064488 * value)
	{
		___GetCurrentFadeColor_13 = value;
		Il2CppCodeGenWriteBarrier(&___GetCurrentFadeColor_13, value);
	}

	inline static int32_t get_offset_of_FadeGrid_14() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___FadeGrid_14)); }
	inline _FadeGrid_t1389933364 * get_FadeGrid_14() const { return ___FadeGrid_14; }
	inline _FadeGrid_t1389933364 ** get_address_of_FadeGrid_14() { return &___FadeGrid_14; }
	inline void set_FadeGrid_14(_FadeGrid_t1389933364 * value)
	{
		___FadeGrid_14 = value;
		Il2CppCodeGenWriteBarrier(&___FadeGrid_14, value);
	}

	inline static int32_t get_offset_of_GetCurrentGridAlpha_15() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetCurrentGridAlpha_15)); }
	inline _GetCurrentGridAlpha_t2030734261 * get_GetCurrentGridAlpha_15() const { return ___GetCurrentGridAlpha_15; }
	inline _GetCurrentGridAlpha_t2030734261 ** get_address_of_GetCurrentGridAlpha_15() { return &___GetCurrentGridAlpha_15; }
	inline void set_GetCurrentGridAlpha_15(_GetCurrentGridAlpha_t2030734261 * value)
	{
		___GetCurrentGridAlpha_15 = value;
		Il2CppCodeGenWriteBarrier(&___GetCurrentGridAlpha_15, value);
	}

	inline static int32_t get_offset_of_SetSkyboxOverride_16() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___SetSkyboxOverride_16)); }
	inline _SetSkyboxOverride_t2590693784 * get_SetSkyboxOverride_16() const { return ___SetSkyboxOverride_16; }
	inline _SetSkyboxOverride_t2590693784 ** get_address_of_SetSkyboxOverride_16() { return &___SetSkyboxOverride_16; }
	inline void set_SetSkyboxOverride_16(_SetSkyboxOverride_t2590693784 * value)
	{
		___SetSkyboxOverride_16 = value;
		Il2CppCodeGenWriteBarrier(&___SetSkyboxOverride_16, value);
	}

	inline static int32_t get_offset_of_ClearSkyboxOverride_17() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___ClearSkyboxOverride_17)); }
	inline _ClearSkyboxOverride_t745528287 * get_ClearSkyboxOverride_17() const { return ___ClearSkyboxOverride_17; }
	inline _ClearSkyboxOverride_t745528287 ** get_address_of_ClearSkyboxOverride_17() { return &___ClearSkyboxOverride_17; }
	inline void set_ClearSkyboxOverride_17(_ClearSkyboxOverride_t745528287 * value)
	{
		___ClearSkyboxOverride_17 = value;
		Il2CppCodeGenWriteBarrier(&___ClearSkyboxOverride_17, value);
	}

	inline static int32_t get_offset_of_CompositorBringToFront_18() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___CompositorBringToFront_18)); }
	inline _CompositorBringToFront_t2294069769 * get_CompositorBringToFront_18() const { return ___CompositorBringToFront_18; }
	inline _CompositorBringToFront_t2294069769 ** get_address_of_CompositorBringToFront_18() { return &___CompositorBringToFront_18; }
	inline void set_CompositorBringToFront_18(_CompositorBringToFront_t2294069769 * value)
	{
		___CompositorBringToFront_18 = value;
		Il2CppCodeGenWriteBarrier(&___CompositorBringToFront_18, value);
	}

	inline static int32_t get_offset_of_CompositorGoToBack_19() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___CompositorGoToBack_19)); }
	inline _CompositorGoToBack_t2760466547 * get_CompositorGoToBack_19() const { return ___CompositorGoToBack_19; }
	inline _CompositorGoToBack_t2760466547 ** get_address_of_CompositorGoToBack_19() { return &___CompositorGoToBack_19; }
	inline void set_CompositorGoToBack_19(_CompositorGoToBack_t2760466547 * value)
	{
		___CompositorGoToBack_19 = value;
		Il2CppCodeGenWriteBarrier(&___CompositorGoToBack_19, value);
	}

	inline static int32_t get_offset_of_CompositorQuit_20() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___CompositorQuit_20)); }
	inline _CompositorQuit_t2273085604 * get_CompositorQuit_20() const { return ___CompositorQuit_20; }
	inline _CompositorQuit_t2273085604 ** get_address_of_CompositorQuit_20() { return &___CompositorQuit_20; }
	inline void set_CompositorQuit_20(_CompositorQuit_t2273085604 * value)
	{
		___CompositorQuit_20 = value;
		Il2CppCodeGenWriteBarrier(&___CompositorQuit_20, value);
	}

	inline static int32_t get_offset_of_IsFullscreen_21() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___IsFullscreen_21)); }
	inline _IsFullscreen_t988890383 * get_IsFullscreen_21() const { return ___IsFullscreen_21; }
	inline _IsFullscreen_t988890383 ** get_address_of_IsFullscreen_21() { return &___IsFullscreen_21; }
	inline void set_IsFullscreen_21(_IsFullscreen_t988890383 * value)
	{
		___IsFullscreen_21 = value;
		Il2CppCodeGenWriteBarrier(&___IsFullscreen_21, value);
	}

	inline static int32_t get_offset_of_GetCurrentSceneFocusProcess_22() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetCurrentSceneFocusProcess_22)); }
	inline _GetCurrentSceneFocusProcess_t1910270748 * get_GetCurrentSceneFocusProcess_22() const { return ___GetCurrentSceneFocusProcess_22; }
	inline _GetCurrentSceneFocusProcess_t1910270748 ** get_address_of_GetCurrentSceneFocusProcess_22() { return &___GetCurrentSceneFocusProcess_22; }
	inline void set_GetCurrentSceneFocusProcess_22(_GetCurrentSceneFocusProcess_t1910270748 * value)
	{
		___GetCurrentSceneFocusProcess_22 = value;
		Il2CppCodeGenWriteBarrier(&___GetCurrentSceneFocusProcess_22, value);
	}

	inline static int32_t get_offset_of_GetLastFrameRenderer_23() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetLastFrameRenderer_23)); }
	inline _GetLastFrameRenderer_t1990293024 * get_GetLastFrameRenderer_23() const { return ___GetLastFrameRenderer_23; }
	inline _GetLastFrameRenderer_t1990293024 ** get_address_of_GetLastFrameRenderer_23() { return &___GetLastFrameRenderer_23; }
	inline void set_GetLastFrameRenderer_23(_GetLastFrameRenderer_t1990293024 * value)
	{
		___GetLastFrameRenderer_23 = value;
		Il2CppCodeGenWriteBarrier(&___GetLastFrameRenderer_23, value);
	}

	inline static int32_t get_offset_of_CanRenderScene_24() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___CanRenderScene_24)); }
	inline _CanRenderScene_t808393456 * get_CanRenderScene_24() const { return ___CanRenderScene_24; }
	inline _CanRenderScene_t808393456 ** get_address_of_CanRenderScene_24() { return &___CanRenderScene_24; }
	inline void set_CanRenderScene_24(_CanRenderScene_t808393456 * value)
	{
		___CanRenderScene_24 = value;
		Il2CppCodeGenWriteBarrier(&___CanRenderScene_24, value);
	}

	inline static int32_t get_offset_of_ShowMirrorWindow_25() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___ShowMirrorWindow_25)); }
	inline _ShowMirrorWindow_t1161105516 * get_ShowMirrorWindow_25() const { return ___ShowMirrorWindow_25; }
	inline _ShowMirrorWindow_t1161105516 ** get_address_of_ShowMirrorWindow_25() { return &___ShowMirrorWindow_25; }
	inline void set_ShowMirrorWindow_25(_ShowMirrorWindow_t1161105516 * value)
	{
		___ShowMirrorWindow_25 = value;
		Il2CppCodeGenWriteBarrier(&___ShowMirrorWindow_25, value);
	}

	inline static int32_t get_offset_of_HideMirrorWindow_26() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___HideMirrorWindow_26)); }
	inline _HideMirrorWindow_t396471547 * get_HideMirrorWindow_26() const { return ___HideMirrorWindow_26; }
	inline _HideMirrorWindow_t396471547 ** get_address_of_HideMirrorWindow_26() { return &___HideMirrorWindow_26; }
	inline void set_HideMirrorWindow_26(_HideMirrorWindow_t396471547 * value)
	{
		___HideMirrorWindow_26 = value;
		Il2CppCodeGenWriteBarrier(&___HideMirrorWindow_26, value);
	}

	inline static int32_t get_offset_of_IsMirrorWindowVisible_27() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___IsMirrorWindowVisible_27)); }
	inline _IsMirrorWindowVisible_t410903031 * get_IsMirrorWindowVisible_27() const { return ___IsMirrorWindowVisible_27; }
	inline _IsMirrorWindowVisible_t410903031 ** get_address_of_IsMirrorWindowVisible_27() { return &___IsMirrorWindowVisible_27; }
	inline void set_IsMirrorWindowVisible_27(_IsMirrorWindowVisible_t410903031 * value)
	{
		___IsMirrorWindowVisible_27 = value;
		Il2CppCodeGenWriteBarrier(&___IsMirrorWindowVisible_27, value);
	}

	inline static int32_t get_offset_of_CompositorDumpImages_28() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___CompositorDumpImages_28)); }
	inline _CompositorDumpImages_t3462826167 * get_CompositorDumpImages_28() const { return ___CompositorDumpImages_28; }
	inline _CompositorDumpImages_t3462826167 ** get_address_of_CompositorDumpImages_28() { return &___CompositorDumpImages_28; }
	inline void set_CompositorDumpImages_28(_CompositorDumpImages_t3462826167 * value)
	{
		___CompositorDumpImages_28 = value;
		Il2CppCodeGenWriteBarrier(&___CompositorDumpImages_28, value);
	}

	inline static int32_t get_offset_of_ShouldAppRenderWithLowResources_29() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___ShouldAppRenderWithLowResources_29)); }
	inline _ShouldAppRenderWithLowResources_t1834060663 * get_ShouldAppRenderWithLowResources_29() const { return ___ShouldAppRenderWithLowResources_29; }
	inline _ShouldAppRenderWithLowResources_t1834060663 ** get_address_of_ShouldAppRenderWithLowResources_29() { return &___ShouldAppRenderWithLowResources_29; }
	inline void set_ShouldAppRenderWithLowResources_29(_ShouldAppRenderWithLowResources_t1834060663 * value)
	{
		___ShouldAppRenderWithLowResources_29 = value;
		Il2CppCodeGenWriteBarrier(&___ShouldAppRenderWithLowResources_29, value);
	}

	inline static int32_t get_offset_of_ForceInterleavedReprojectionOn_30() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___ForceInterleavedReprojectionOn_30)); }
	inline _ForceInterleavedReprojectionOn_t78695371 * get_ForceInterleavedReprojectionOn_30() const { return ___ForceInterleavedReprojectionOn_30; }
	inline _ForceInterleavedReprojectionOn_t78695371 ** get_address_of_ForceInterleavedReprojectionOn_30() { return &___ForceInterleavedReprojectionOn_30; }
	inline void set_ForceInterleavedReprojectionOn_30(_ForceInterleavedReprojectionOn_t78695371 * value)
	{
		___ForceInterleavedReprojectionOn_30 = value;
		Il2CppCodeGenWriteBarrier(&___ForceInterleavedReprojectionOn_30, value);
	}

	inline static int32_t get_offset_of_ForceReconnectProcess_31() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___ForceReconnectProcess_31)); }
	inline _ForceReconnectProcess_t3057442189 * get_ForceReconnectProcess_31() const { return ___ForceReconnectProcess_31; }
	inline _ForceReconnectProcess_t3057442189 ** get_address_of_ForceReconnectProcess_31() { return &___ForceReconnectProcess_31; }
	inline void set_ForceReconnectProcess_31(_ForceReconnectProcess_t3057442189 * value)
	{
		___ForceReconnectProcess_31 = value;
		Il2CppCodeGenWriteBarrier(&___ForceReconnectProcess_31, value);
	}

	inline static int32_t get_offset_of_SuspendRendering_32() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___SuspendRendering_32)); }
	inline _SuspendRendering_t3440691638 * get_SuspendRendering_32() const { return ___SuspendRendering_32; }
	inline _SuspendRendering_t3440691638 ** get_address_of_SuspendRendering_32() { return &___SuspendRendering_32; }
	inline void set_SuspendRendering_32(_SuspendRendering_t3440691638 * value)
	{
		___SuspendRendering_32 = value;
		Il2CppCodeGenWriteBarrier(&___SuspendRendering_32, value);
	}

	inline static int32_t get_offset_of_GetMirrorTextureD3D11_33() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetMirrorTextureD3D11_33)); }
	inline _GetMirrorTextureD3D11_t2322134645 * get_GetMirrorTextureD3D11_33() const { return ___GetMirrorTextureD3D11_33; }
	inline _GetMirrorTextureD3D11_t2322134645 ** get_address_of_GetMirrorTextureD3D11_33() { return &___GetMirrorTextureD3D11_33; }
	inline void set_GetMirrorTextureD3D11_33(_GetMirrorTextureD3D11_t2322134645 * value)
	{
		___GetMirrorTextureD3D11_33 = value;
		Il2CppCodeGenWriteBarrier(&___GetMirrorTextureD3D11_33, value);
	}

	inline static int32_t get_offset_of_ReleaseMirrorTextureD3D11_34() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___ReleaseMirrorTextureD3D11_34)); }
	inline _ReleaseMirrorTextureD3D11_t2100129624 * get_ReleaseMirrorTextureD3D11_34() const { return ___ReleaseMirrorTextureD3D11_34; }
	inline _ReleaseMirrorTextureD3D11_t2100129624 ** get_address_of_ReleaseMirrorTextureD3D11_34() { return &___ReleaseMirrorTextureD3D11_34; }
	inline void set_ReleaseMirrorTextureD3D11_34(_ReleaseMirrorTextureD3D11_t2100129624 * value)
	{
		___ReleaseMirrorTextureD3D11_34 = value;
		Il2CppCodeGenWriteBarrier(&___ReleaseMirrorTextureD3D11_34, value);
	}

	inline static int32_t get_offset_of_GetMirrorTextureGL_35() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetMirrorTextureGL_35)); }
	inline _GetMirrorTextureGL_t3071699813 * get_GetMirrorTextureGL_35() const { return ___GetMirrorTextureGL_35; }
	inline _GetMirrorTextureGL_t3071699813 ** get_address_of_GetMirrorTextureGL_35() { return &___GetMirrorTextureGL_35; }
	inline void set_GetMirrorTextureGL_35(_GetMirrorTextureGL_t3071699813 * value)
	{
		___GetMirrorTextureGL_35 = value;
		Il2CppCodeGenWriteBarrier(&___GetMirrorTextureGL_35, value);
	}

	inline static int32_t get_offset_of_ReleaseSharedGLTexture_36() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___ReleaseSharedGLTexture_36)); }
	inline _ReleaseSharedGLTexture_t3443550108 * get_ReleaseSharedGLTexture_36() const { return ___ReleaseSharedGLTexture_36; }
	inline _ReleaseSharedGLTexture_t3443550108 ** get_address_of_ReleaseSharedGLTexture_36() { return &___ReleaseSharedGLTexture_36; }
	inline void set_ReleaseSharedGLTexture_36(_ReleaseSharedGLTexture_t3443550108 * value)
	{
		___ReleaseSharedGLTexture_36 = value;
		Il2CppCodeGenWriteBarrier(&___ReleaseSharedGLTexture_36, value);
	}

	inline static int32_t get_offset_of_LockGLSharedTextureForAccess_37() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___LockGLSharedTextureForAccess_37)); }
	inline _LockGLSharedTextureForAccess_t2180462479 * get_LockGLSharedTextureForAccess_37() const { return ___LockGLSharedTextureForAccess_37; }
	inline _LockGLSharedTextureForAccess_t2180462479 ** get_address_of_LockGLSharedTextureForAccess_37() { return &___LockGLSharedTextureForAccess_37; }
	inline void set_LockGLSharedTextureForAccess_37(_LockGLSharedTextureForAccess_t2180462479 * value)
	{
		___LockGLSharedTextureForAccess_37 = value;
		Il2CppCodeGenWriteBarrier(&___LockGLSharedTextureForAccess_37, value);
	}

	inline static int32_t get_offset_of_UnlockGLSharedTextureForAccess_38() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___UnlockGLSharedTextureForAccess_38)); }
	inline _UnlockGLSharedTextureForAccess_t320290112 * get_UnlockGLSharedTextureForAccess_38() const { return ___UnlockGLSharedTextureForAccess_38; }
	inline _UnlockGLSharedTextureForAccess_t320290112 ** get_address_of_UnlockGLSharedTextureForAccess_38() { return &___UnlockGLSharedTextureForAccess_38; }
	inline void set_UnlockGLSharedTextureForAccess_38(_UnlockGLSharedTextureForAccess_t320290112 * value)
	{
		___UnlockGLSharedTextureForAccess_38 = value;
		Il2CppCodeGenWriteBarrier(&___UnlockGLSharedTextureForAccess_38, value);
	}

	inline static int32_t get_offset_of_GetVulkanInstanceExtensionsRequired_39() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetVulkanInstanceExtensionsRequired_39)); }
	inline _GetVulkanInstanceExtensionsRequired_t2249042297 * get_GetVulkanInstanceExtensionsRequired_39() const { return ___GetVulkanInstanceExtensionsRequired_39; }
	inline _GetVulkanInstanceExtensionsRequired_t2249042297 ** get_address_of_GetVulkanInstanceExtensionsRequired_39() { return &___GetVulkanInstanceExtensionsRequired_39; }
	inline void set_GetVulkanInstanceExtensionsRequired_39(_GetVulkanInstanceExtensionsRequired_t2249042297 * value)
	{
		___GetVulkanInstanceExtensionsRequired_39 = value;
		Il2CppCodeGenWriteBarrier(&___GetVulkanInstanceExtensionsRequired_39, value);
	}

	inline static int32_t get_offset_of_GetVulkanDeviceExtensionsRequired_40() { return static_cast<int32_t>(offsetof(IVRCompositor_t4206274356, ___GetVulkanDeviceExtensionsRequired_40)); }
	inline _GetVulkanDeviceExtensionsRequired_t3459249220 * get_GetVulkanDeviceExtensionsRequired_40() const { return ___GetVulkanDeviceExtensionsRequired_40; }
	inline _GetVulkanDeviceExtensionsRequired_t3459249220 ** get_address_of_GetVulkanDeviceExtensionsRequired_40() { return &___GetVulkanDeviceExtensionsRequired_40; }
	inline void set_GetVulkanDeviceExtensionsRequired_40(_GetVulkanDeviceExtensionsRequired_t3459249220 * value)
	{
		___GetVulkanDeviceExtensionsRequired_40 = value;
		Il2CppCodeGenWriteBarrier(&___GetVulkanDeviceExtensionsRequired_40, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Valve.VR.IVRCompositor
struct IVRCompositor_t4206274356_marshaled_pinvoke
{
	Il2CppMethodPointer ___SetTrackingSpace_0;
	Il2CppMethodPointer ___GetTrackingSpace_1;
	Il2CppMethodPointer ___WaitGetPoses_2;
	Il2CppMethodPointer ___GetLastPoses_3;
	Il2CppMethodPointer ___GetLastPoseForTrackedDeviceIndex_4;
	Il2CppMethodPointer ___Submit_5;
	Il2CppMethodPointer ___ClearLastSubmittedFrame_6;
	Il2CppMethodPointer ___PostPresentHandoff_7;
	Il2CppMethodPointer ___GetFrameTiming_8;
	Il2CppMethodPointer ___GetFrameTimings_9;
	Il2CppMethodPointer ___GetFrameTimeRemaining_10;
	Il2CppMethodPointer ___GetCumulativeStats_11;
	Il2CppMethodPointer ___FadeToColor_12;
	Il2CppMethodPointer ___GetCurrentFadeColor_13;
	Il2CppMethodPointer ___FadeGrid_14;
	Il2CppMethodPointer ___GetCurrentGridAlpha_15;
	Il2CppMethodPointer ___SetSkyboxOverride_16;
	Il2CppMethodPointer ___ClearSkyboxOverride_17;
	Il2CppMethodPointer ___CompositorBringToFront_18;
	Il2CppMethodPointer ___CompositorGoToBack_19;
	Il2CppMethodPointer ___CompositorQuit_20;
	Il2CppMethodPointer ___IsFullscreen_21;
	Il2CppMethodPointer ___GetCurrentSceneFocusProcess_22;
	Il2CppMethodPointer ___GetLastFrameRenderer_23;
	Il2CppMethodPointer ___CanRenderScene_24;
	Il2CppMethodPointer ___ShowMirrorWindow_25;
	Il2CppMethodPointer ___HideMirrorWindow_26;
	Il2CppMethodPointer ___IsMirrorWindowVisible_27;
	Il2CppMethodPointer ___CompositorDumpImages_28;
	Il2CppMethodPointer ___ShouldAppRenderWithLowResources_29;
	Il2CppMethodPointer ___ForceInterleavedReprojectionOn_30;
	Il2CppMethodPointer ___ForceReconnectProcess_31;
	Il2CppMethodPointer ___SuspendRendering_32;
	Il2CppMethodPointer ___GetMirrorTextureD3D11_33;
	Il2CppMethodPointer ___ReleaseMirrorTextureD3D11_34;
	Il2CppMethodPointer ___GetMirrorTextureGL_35;
	Il2CppMethodPointer ___ReleaseSharedGLTexture_36;
	Il2CppMethodPointer ___LockGLSharedTextureForAccess_37;
	Il2CppMethodPointer ___UnlockGLSharedTextureForAccess_38;
	Il2CppMethodPointer ___GetVulkanInstanceExtensionsRequired_39;
	Il2CppMethodPointer ___GetVulkanDeviceExtensionsRequired_40;
};
// Native definition for COM marshalling of Valve.VR.IVRCompositor
struct IVRCompositor_t4206274356_marshaled_com
{
	Il2CppMethodPointer ___SetTrackingSpace_0;
	Il2CppMethodPointer ___GetTrackingSpace_1;
	Il2CppMethodPointer ___WaitGetPoses_2;
	Il2CppMethodPointer ___GetLastPoses_3;
	Il2CppMethodPointer ___GetLastPoseForTrackedDeviceIndex_4;
	Il2CppMethodPointer ___Submit_5;
	Il2CppMethodPointer ___ClearLastSubmittedFrame_6;
	Il2CppMethodPointer ___PostPresentHandoff_7;
	Il2CppMethodPointer ___GetFrameTiming_8;
	Il2CppMethodPointer ___GetFrameTimings_9;
	Il2CppMethodPointer ___GetFrameTimeRemaining_10;
	Il2CppMethodPointer ___GetCumulativeStats_11;
	Il2CppMethodPointer ___FadeToColor_12;
	Il2CppMethodPointer ___GetCurrentFadeColor_13;
	Il2CppMethodPointer ___FadeGrid_14;
	Il2CppMethodPointer ___GetCurrentGridAlpha_15;
	Il2CppMethodPointer ___SetSkyboxOverride_16;
	Il2CppMethodPointer ___ClearSkyboxOverride_17;
	Il2CppMethodPointer ___CompositorBringToFront_18;
	Il2CppMethodPointer ___CompositorGoToBack_19;
	Il2CppMethodPointer ___CompositorQuit_20;
	Il2CppMethodPointer ___IsFullscreen_21;
	Il2CppMethodPointer ___GetCurrentSceneFocusProcess_22;
	Il2CppMethodPointer ___GetLastFrameRenderer_23;
	Il2CppMethodPointer ___CanRenderScene_24;
	Il2CppMethodPointer ___ShowMirrorWindow_25;
	Il2CppMethodPointer ___HideMirrorWindow_26;
	Il2CppMethodPointer ___IsMirrorWindowVisible_27;
	Il2CppMethodPointer ___CompositorDumpImages_28;
	Il2CppMethodPointer ___ShouldAppRenderWithLowResources_29;
	Il2CppMethodPointer ___ForceInterleavedReprojectionOn_30;
	Il2CppMethodPointer ___ForceReconnectProcess_31;
	Il2CppMethodPointer ___SuspendRendering_32;
	Il2CppMethodPointer ___GetMirrorTextureD3D11_33;
	Il2CppMethodPointer ___ReleaseMirrorTextureD3D11_34;
	Il2CppMethodPointer ___GetMirrorTextureGL_35;
	Il2CppMethodPointer ___ReleaseSharedGLTexture_36;
	Il2CppMethodPointer ___LockGLSharedTextureForAccess_37;
	Il2CppMethodPointer ___UnlockGLSharedTextureForAccess_38;
	Il2CppMethodPointer ___GetVulkanInstanceExtensionsRequired_39;
	Il2CppMethodPointer ___GetVulkanDeviceExtensionsRequired_40;
};
