﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_InteractableObject/<RegisterTeleportersAtEndOfFrame>c__Iterator0
struct U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_InteractableObject/<RegisterTeleportersAtEndOfFrame>c__Iterator0::.ctor()
extern "C"  void U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0__ctor_m2049363233 (U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractableObject/<RegisterTeleportersAtEndOfFrame>c__Iterator0::MoveNext()
extern "C"  bool U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_MoveNext_m3987823239 (U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_InteractableObject/<RegisterTeleportersAtEndOfFrame>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m422153327 (U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_InteractableObject/<RegisterTeleportersAtEndOfFrame>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m775029799 (U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject/<RegisterTeleportersAtEndOfFrame>c__Iterator0::Dispose()
extern "C"  void U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_Dispose_m3091396662 (U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject/<RegisterTeleportersAtEndOfFrame>c__Iterator0::Reset()
extern "C"  void U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_Reset_m89694032 (U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
