﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct Collection_1_t929462690;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Valve.VR.InteractionSystem.Hand/AttachedObject[]
struct AttachedObjectU5BU5D_t2422108177;
// System.Collections.Generic.IEnumerator`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct IEnumerator_1_t3158209059;
// System.Collections.Generic.IList`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct IList_1_t1928658537;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1387717936.h"

// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::.ctor()
extern "C"  void Collection_1__ctor_m3255041819_gshared (Collection_1_t929462690 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3255041819(__this, method) ((  void (*) (Collection_1_t929462690 *, const MethodInfo*))Collection_1__ctor_m3255041819_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2396883244_gshared (Collection_1_t929462690 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2396883244(__this, method) ((  bool (*) (Collection_1_t929462690 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2396883244_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1724257095_gshared (Collection_1_t929462690 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1724257095(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t929462690 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1724257095_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m899652192_gshared (Collection_1_t929462690 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m899652192(__this, method) ((  Il2CppObject * (*) (Collection_1_t929462690 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m899652192_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2239457887_gshared (Collection_1_t929462690 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2239457887(__this, ___value0, method) ((  int32_t (*) (Collection_1_t929462690 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2239457887_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2809682047_gshared (Collection_1_t929462690 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2809682047(__this, ___value0, method) ((  bool (*) (Collection_1_t929462690 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2809682047_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2358555825_gshared (Collection_1_t929462690 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2358555825(__this, ___value0, method) ((  int32_t (*) (Collection_1_t929462690 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2358555825_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1616914754_gshared (Collection_1_t929462690 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1616914754(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t929462690 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1616914754_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2039851844_gshared (Collection_1_t929462690 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2039851844(__this, ___value0, method) ((  void (*) (Collection_1_t929462690 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2039851844_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3425764935_gshared (Collection_1_t929462690 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3425764935(__this, method) ((  bool (*) (Collection_1_t929462690 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3425764935_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2543151187_gshared (Collection_1_t929462690 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2543151187(__this, method) ((  Il2CppObject * (*) (Collection_1_t929462690 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2543151187_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2945231542_gshared (Collection_1_t929462690 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2945231542(__this, method) ((  bool (*) (Collection_1_t929462690 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2945231542_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m689293235_gshared (Collection_1_t929462690 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m689293235(__this, method) ((  bool (*) (Collection_1_t929462690 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m689293235_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2524562464_gshared (Collection_1_t929462690 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2524562464(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t929462690 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2524562464_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m89284341_gshared (Collection_1_t929462690 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m89284341(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t929462690 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m89284341_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Add(T)
extern "C"  void Collection_1_Add_m1325833510_gshared (Collection_1_t929462690 * __this, AttachedObject_t1387717936  ___item0, const MethodInfo* method);
#define Collection_1_Add_m1325833510(__this, ___item0, method) ((  void (*) (Collection_1_t929462690 *, AttachedObject_t1387717936 , const MethodInfo*))Collection_1_Add_m1325833510_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Clear()
extern "C"  void Collection_1_Clear_m4225989322_gshared (Collection_1_t929462690 * __this, const MethodInfo* method);
#define Collection_1_Clear_m4225989322(__this, method) ((  void (*) (Collection_1_t929462690 *, const MethodInfo*))Collection_1_Clear_m4225989322_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2995906632_gshared (Collection_1_t929462690 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2995906632(__this, method) ((  void (*) (Collection_1_t929462690 *, const MethodInfo*))Collection_1_ClearItems_m2995906632_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Contains(T)
extern "C"  bool Collection_1_Contains_m3107290196_gshared (Collection_1_t929462690 * __this, AttachedObject_t1387717936  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3107290196(__this, ___item0, method) ((  bool (*) (Collection_1_t929462690 *, AttachedObject_t1387717936 , const MethodInfo*))Collection_1_Contains_m3107290196_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2577931602_gshared (Collection_1_t929462690 * __this, AttachedObjectU5BU5D_t2422108177* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2577931602(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t929462690 *, AttachedObjectU5BU5D_t2422108177*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2577931602_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m4173744687_gshared (Collection_1_t929462690 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m4173744687(__this, method) ((  Il2CppObject* (*) (Collection_1_t929462690 *, const MethodInfo*))Collection_1_GetEnumerator_m4173744687_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3830021140_gshared (Collection_1_t929462690 * __this, AttachedObject_t1387717936  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3830021140(__this, ___item0, method) ((  int32_t (*) (Collection_1_t929462690 *, AttachedObject_t1387717936 , const MethodInfo*))Collection_1_IndexOf_m3830021140_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m765227033_gshared (Collection_1_t929462690 * __this, int32_t ___index0, AttachedObject_t1387717936  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m765227033(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t929462690 *, int32_t, AttachedObject_t1387717936 , const MethodInfo*))Collection_1_Insert_m765227033_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2206588336_gshared (Collection_1_t929462690 * __this, int32_t ___index0, AttachedObject_t1387717936  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2206588336(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t929462690 *, int32_t, AttachedObject_t1387717936 , const MethodInfo*))Collection_1_InsertItem_m2206588336_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Remove(T)
extern "C"  bool Collection_1_Remove_m2147645169_gshared (Collection_1_t929462690 * __this, AttachedObject_t1387717936  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2147645169(__this, ___item0, method) ((  bool (*) (Collection_1_t929462690 *, AttachedObject_t1387717936 , const MethodInfo*))Collection_1_Remove_m2147645169_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2305409821_gshared (Collection_1_t929462690 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2305409821(__this, ___index0, method) ((  void (*) (Collection_1_t929462690 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2305409821_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3041606929_gshared (Collection_1_t929462690 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3041606929(__this, ___index0, method) ((  void (*) (Collection_1_t929462690 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3041606929_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m441151439_gshared (Collection_1_t929462690 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m441151439(__this, method) ((  int32_t (*) (Collection_1_t929462690 *, const MethodInfo*))Collection_1_get_Count_m441151439_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::get_Item(System.Int32)
extern "C"  AttachedObject_t1387717936  Collection_1_get_Item_m264844945_gshared (Collection_1_t929462690 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m264844945(__this, ___index0, method) ((  AttachedObject_t1387717936  (*) (Collection_1_t929462690 *, int32_t, const MethodInfo*))Collection_1_get_Item_m264844945_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m298060984_gshared (Collection_1_t929462690 * __this, int32_t ___index0, AttachedObject_t1387717936  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m298060984(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t929462690 *, int32_t, AttachedObject_t1387717936 , const MethodInfo*))Collection_1_set_Item_m298060984_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2664538229_gshared (Collection_1_t929462690 * __this, int32_t ___index0, AttachedObject_t1387717936  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2664538229(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t929462690 *, int32_t, AttachedObject_t1387717936 , const MethodInfo*))Collection_1_SetItem_m2664538229_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3275877114_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3275877114(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3275877114_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::ConvertItem(System.Object)
extern "C"  AttachedObject_t1387717936  Collection_1_ConvertItem_m84734890_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m84734890(__this /* static, unused */, ___item0, method) ((  AttachedObject_t1387717936  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m84734890_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m4043487254_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m4043487254(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m4043487254_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3896548032_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3896548032(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3896548032_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1642349297_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1642349297(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1642349297_gshared)(__this /* static, unused */, ___list0, method)
