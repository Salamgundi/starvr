﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRNotifications/_CreateNotification
struct _CreateNotification_t1905156422;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRNotificationError1058814108.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRNotificationType408717238.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRNotificationStyle1858720717.h"
#include "AssemblyU2DCSharp_Valve_VR_NotificationBitmap_t1973232283.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRNotifications/_CreateNotification::.ctor(System.Object,System.IntPtr)
extern "C"  void _CreateNotification__ctor_m3109111065 (_CreateNotification_t1905156422 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRNotificationError Valve.VR.IVRNotifications/_CreateNotification::Invoke(System.UInt64,System.UInt64,Valve.VR.EVRNotificationType,System.String,Valve.VR.EVRNotificationStyle,Valve.VR.NotificationBitmap_t&,System.UInt32&)
extern "C"  int32_t _CreateNotification_Invoke_m444030182 (_CreateNotification_t1905156422 * __this, uint64_t ___ulOverlayHandle0, uint64_t ___ulUserValue1, int32_t ___type2, String_t* ___pchText3, int32_t ___style4, NotificationBitmap_t_t1973232283 * ___pImage5, uint32_t* ___pNotificationId6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRNotifications/_CreateNotification::BeginInvoke(System.UInt64,System.UInt64,Valve.VR.EVRNotificationType,System.String,Valve.VR.EVRNotificationStyle,Valve.VR.NotificationBitmap_t&,System.UInt32&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _CreateNotification_BeginInvoke_m740096502 (_CreateNotification_t1905156422 * __this, uint64_t ___ulOverlayHandle0, uint64_t ___ulUserValue1, int32_t ___type2, String_t* ___pchText3, int32_t ___style4, NotificationBitmap_t_t1973232283 * ___pImage5, uint32_t* ___pNotificationId6, AsyncCallback_t163412349 * ___callback7, Il2CppObject * ___object8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRNotificationError Valve.VR.IVRNotifications/_CreateNotification::EndInvoke(Valve.VR.NotificationBitmap_t&,System.UInt32&,System.IAsyncResult)
extern "C"  int32_t _CreateNotification_EndInvoke_m230273733 (_CreateNotification_t1905156422 * __this, NotificationBitmap_t_t1973232283 * ___pImage0, uint32_t* ___pNotificationId1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
