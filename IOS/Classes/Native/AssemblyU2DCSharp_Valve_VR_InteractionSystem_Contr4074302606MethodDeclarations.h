﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ControllerHintsExample
struct ControllerHintsExample_t4074302606;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"

// System.Void Valve.VR.InteractionSystem.ControllerHintsExample::.ctor()
extern "C"  void ControllerHintsExample__ctor_m2337707726 (ControllerHintsExample_t4074302606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHintsExample::ShowButtonHints(Valve.VR.InteractionSystem.Hand)
extern "C"  void ControllerHintsExample_ShowButtonHints_m624353889 (ControllerHintsExample_t4074302606 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHintsExample::ShowTextHints(Valve.VR.InteractionSystem.Hand)
extern "C"  void ControllerHintsExample_ShowTextHints_m1640940074 (ControllerHintsExample_t4074302606 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHintsExample::DisableHints()
extern "C"  void ControllerHintsExample_DisableHints_m1397343222 (ControllerHintsExample_t4074302606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.ControllerHintsExample::TestButtonHints(Valve.VR.InteractionSystem.Hand)
extern "C"  Il2CppObject * ControllerHintsExample_TestButtonHints_m2451990724 (ControllerHintsExample_t4074302606 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.ControllerHintsExample::TestTextHints(Valve.VR.InteractionSystem.Hand)
extern "C"  Il2CppObject * ControllerHintsExample_TestTextHints_m2605959845 (ControllerHintsExample_t4074302606 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
