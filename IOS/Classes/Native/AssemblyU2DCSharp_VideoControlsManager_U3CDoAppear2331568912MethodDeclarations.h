﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VideoControlsManager/<DoAppear>c__Iterator0
struct U3CDoAppearU3Ec__Iterator0_t2331568912;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VideoControlsManager/<DoAppear>c__Iterator0::.ctor()
extern "C"  void U3CDoAppearU3Ec__Iterator0__ctor_m2527038213 (U3CDoAppearU3Ec__Iterator0_t2331568912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VideoControlsManager/<DoAppear>c__Iterator0::MoveNext()
extern "C"  bool U3CDoAppearU3Ec__Iterator0_MoveNext_m3014764719 (U3CDoAppearU3Ec__Iterator0_t2331568912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VideoControlsManager/<DoAppear>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoAppearU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m93984395 (U3CDoAppearU3Ec__Iterator0_t2331568912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VideoControlsManager/<DoAppear>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoAppearU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1489616819 (U3CDoAppearU3Ec__Iterator0_t2331568912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoControlsManager/<DoAppear>c__Iterator0::Dispose()
extern "C"  void U3CDoAppearU3Ec__Iterator0_Dispose_m1523471434 (U3CDoAppearU3Ec__Iterator0_t2331568912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoControlsManager/<DoAppear>c__Iterator0::Reset()
extern "C"  void U3CDoAppearU3Ec__Iterator0_Reset_m1434524216 (U3CDoAppearU3Ec__Iterator0_t2331568912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
