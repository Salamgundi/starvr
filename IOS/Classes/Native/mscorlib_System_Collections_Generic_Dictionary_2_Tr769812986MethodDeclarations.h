﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr948428459MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m2337187018(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t769812986 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3287735239_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2520237178(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t769812986 *, int32_t, VRTK_SDKDetails_t1748250348 *, const MethodInfo*))Transform_1_Invoke_m3149315059_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m3264368673(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t769812986 *, int32_t, VRTK_SDKDetails_t1748250348 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2241420452_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m4080844636(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t769812986 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1581059437_gshared)(__this, ___result0, method)
