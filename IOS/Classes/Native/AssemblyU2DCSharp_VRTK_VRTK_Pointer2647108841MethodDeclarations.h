﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_Pointer
struct VRTK_Pointer_t2647108841;
// System.Object
struct Il2CppObject;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void VRTK.VRTK_Pointer::.ctor()
extern "C"  void VRTK_Pointer__ctor_m4172227245 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::PointerEnter(UnityEngine.RaycastHit)
extern "C"  void VRTK_Pointer_PointerEnter_m1407371743 (VRTK_Pointer_t2647108841 * __this, RaycastHit_t87180320  ___givenHit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::PointerExit(UnityEngine.RaycastHit)
extern "C"  void VRTK_Pointer_PointerExit_m2374584099 (VRTK_Pointer_t2647108841 * __this, RaycastHit_t87180320  ___givenHit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Pointer::CanActivate()
extern "C"  bool VRTK_Pointer_CanActivate_m1017049564 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Pointer::IsPointerActive()
extern "C"  bool VRTK_Pointer_IsPointerActive_m1749490224 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::ResetActivationTimer(System.Boolean)
extern "C"  void VRTK_Pointer_ResetActivationTimer_m976761708 (VRTK_Pointer_t2647108841 * __this, bool ___forceZero0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::Toggle(System.Boolean)
extern "C"  void VRTK_Pointer_Toggle_m3728954148 (VRTK_Pointer_t2647108841 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::OnEnable()
extern "C"  void VRTK_Pointer_OnEnable_m254120077 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::OnDisable()
extern "C"  void VRTK_Pointer_OnDisable_m3553624222 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::Start()
extern "C"  void VRTK_Pointer_Start_m297722381 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::Update()
extern "C"  void VRTK_Pointer_Update_m1967174448 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Pointer::EnabledPointerRenderer()
extern "C"  bool VRTK_Pointer_EnabledPointerRenderer_m2876450834 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Pointer::NoPointerRenderer()
extern "C"  bool VRTK_Pointer_NoPointerRenderer_m2127369236 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Pointer::CanActivateOnToggleButton(System.Boolean)
extern "C"  bool VRTK_Pointer_CanActivateOnToggleButton_m2181741988 (VRTK_Pointer_t2647108841 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::FindController()
extern "C"  void VRTK_Pointer_FindController_m1755619338 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::SetupController()
extern "C"  void VRTK_Pointer_SetupController_m3955684672 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::SetupRenderer()
extern "C"  void VRTK_Pointer_SetupRenderer_m2173230195 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Pointer::ButtonMappingIsUndefined(VRTK.VRTK_ControllerEvents/ButtonAlias,VRTK.VRTK_ControllerEvents/ButtonAlias)
extern "C"  bool VRTK_Pointer_ButtonMappingIsUndefined_m715092459 (VRTK_Pointer_t2647108841 * __this, int32_t ___givenButton0, int32_t ___givenSubscribedButton1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::CheckButtonMappingConflict()
extern "C"  void VRTK_Pointer_CheckButtonMappingConflict_m917756563 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::CheckButtonSubscriptions()
extern "C"  void VRTK_Pointer_CheckButtonSubscriptions_m2587795945 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::SubscribeActivationButton()
extern "C"  void VRTK_Pointer_SubscribeActivationButton_m2092682447 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::UnsubscribeActivationButton()
extern "C"  void VRTK_Pointer_UnsubscribeActivationButton_m3110733880 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::ActivationButtonPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_Pointer_ActivationButtonPressed_m330861319 (VRTK_Pointer_t2647108841 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::ActivationButtonReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_Pointer_ActivationButtonReleased_m3727314942 (VRTK_Pointer_t2647108841 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::SubscribeSelectionButton()
extern "C"  void VRTK_Pointer_SubscribeSelectionButton_m3571783803 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::UnsubscribeSelectionButton()
extern "C"  void VRTK_Pointer_UnsubscribeSelectionButton_m1758019276 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::SelectionButtonAction(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_Pointer_SelectionButtonAction_m3930267767 (VRTK_Pointer_t2647108841 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Pointer::CanResetActivationState(System.Boolean)
extern "C"  bool VRTK_Pointer_CanResetActivationState_m1385310748 (VRTK_Pointer_t2647108841 * __this, bool ___givenState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::ManageActivationState(System.Boolean)
extern "C"  void VRTK_Pointer_ManageActivationState_m2439564176 (VRTK_Pointer_t2647108841 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Pointer::PointerActivatesUseAction(VRTK.VRTK_InteractableObject)
extern "C"  bool VRTK_Pointer_PointerActivatesUseAction_m2293475429 (VRTK_Pointer_t2647108841 * __this, VRTK_InteractableObject_t2604188111 * ___givenInteractableObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::StartUseAction(UnityEngine.Transform)
extern "C"  void VRTK_Pointer_StartUseAction_m559326947 (VRTK_Pointer_t2647108841 * __this, Transform_t3275118058 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::StopUseAction()
extern "C"  void VRTK_Pointer_StopUseAction_m2801561804 (VRTK_Pointer_t2647108841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Pointer::AttemptUseOnSet(UnityEngine.Transform)
extern "C"  void VRTK_Pointer_AttemptUseOnSet_m1100701627 (VRTK_Pointer_t2647108841 * __this, Transform_t3275118058 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
