﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureSize
struct _GetVideoStreamTextureSize_t2646805685;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraError3529690400.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraFrameTy2003723073.h"
#include "AssemblyU2DCSharp_Valve_VR_VRTextureBounds_t1897807375.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureSize::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetVideoStreamTextureSize__ctor_m964704542 (_GetVideoStreamTextureSize_t2646805685 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureSize::Invoke(System.UInt32,Valve.VR.EVRTrackedCameraFrameType,Valve.VR.VRTextureBounds_t&,System.UInt32&,System.UInt32&)
extern "C"  int32_t _GetVideoStreamTextureSize_Invoke_m228891905 (_GetVideoStreamTextureSize_t2646805685 * __this, uint32_t ___nDeviceIndex0, int32_t ___eFrameType1, VRTextureBounds_t_t1897807375 * ___pTextureBounds2, uint32_t* ___pnWidth3, uint32_t* ___pnHeight4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureSize::BeginInvoke(System.UInt32,Valve.VR.EVRTrackedCameraFrameType,Valve.VR.VRTextureBounds_t&,System.UInt32&,System.UInt32&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetVideoStreamTextureSize_BeginInvoke_m4194020771 (_GetVideoStreamTextureSize_t2646805685 * __this, uint32_t ___nDeviceIndex0, int32_t ___eFrameType1, VRTextureBounds_t_t1897807375 * ___pTextureBounds2, uint32_t* ___pnWidth3, uint32_t* ___pnHeight4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureSize::EndInvoke(Valve.VR.VRTextureBounds_t&,System.UInt32&,System.UInt32&,System.IAsyncResult)
extern "C"  int32_t _GetVideoStreamTextureSize_EndInvoke_m766578350 (_GetVideoStreamTextureSize_t2646805685 * __this, VRTextureBounds_t_t1897807375 * ___pTextureBounds0, uint32_t* ___pnWidth1, uint32_t* ___pnHeight2, Il2CppObject * ___result3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
