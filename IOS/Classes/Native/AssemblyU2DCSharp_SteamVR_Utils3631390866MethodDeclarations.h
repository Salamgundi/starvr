﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// SteamVR_Utils/SystemFn
struct SystemFn_t1182840554;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "AssemblyU2DCSharp_SteamVR_Utils_SystemFn1182840554.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"

// UnityEngine.Quaternion SteamVR_Utils::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t4030073918  SteamVR_Utils_Slerp_m2216669038 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___A0, Quaternion_t4030073918  ___B1, float ___t2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SteamVR_Utils::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  SteamVR_Utils_Lerp_m107553497 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___A0, Vector3_t2243707580  ___B1, float ___t2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR_Utils::Lerp(System.Single,System.Single,System.Single)
extern "C"  float SteamVR_Utils_Lerp_m3968093561 (Il2CppObject * __this /* static, unused */, float ___A0, float ___B1, float ___t2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double SteamVR_Utils::Lerp(System.Double,System.Double,System.Double)
extern "C"  double SteamVR_Utils_Lerp_m3791015481 (Il2CppObject * __this /* static, unused */, double ___A0, double ___B1, double ___t2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR_Utils::InverseLerp(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float SteamVR_Utils_InverseLerp_m2632145025 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___A0, Vector3_t2243707580  ___B1, Vector3_t2243707580  ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR_Utils::InverseLerp(System.Single,System.Single,System.Single)
extern "C"  float SteamVR_Utils_InverseLerp_m3134333649 (Il2CppObject * __this /* static, unused */, float ___A0, float ___B1, float ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double SteamVR_Utils::InverseLerp(System.Double,System.Double,System.Double)
extern "C"  double SteamVR_Utils_InverseLerp_m2404593961 (Il2CppObject * __this /* static, unused */, double ___A0, double ___B1, double ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR_Utils::Saturate(System.Single)
extern "C"  float SteamVR_Utils_Saturate_m2864395063 (Il2CppObject * __this /* static, unused */, float ___A0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 SteamVR_Utils::Saturate(UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  SteamVR_Utils_Saturate_m2914256375 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___A0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR_Utils::Abs(System.Single)
extern "C"  float SteamVR_Utils_Abs_m2600678036 (Il2CppObject * __this /* static, unused */, float ___A0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 SteamVR_Utils::Abs(UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  SteamVR_Utils_Abs_m162090334 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___A0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR_Utils::_copysign(System.Single,System.Single)
extern "C"  float SteamVR_Utils__copysign_m100674536 (Il2CppObject * __this /* static, unused */, float ___sizeval0, float ___signval1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion SteamVR_Utils::GetRotation(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t4030073918  SteamVR_Utils_GetRotation_m3342757751 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SteamVR_Utils::GetPosition(UnityEngine.Matrix4x4)
extern "C"  Vector3_t2243707580  SteamVR_Utils_GetPosition_m1529817200 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SteamVR_Utils::GetScale(UnityEngine.Matrix4x4)
extern "C"  Vector3_t2243707580  SteamVR_Utils_GetScale_m1310982401 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SteamVR_Utils::CallSystemFn(SteamVR_Utils/SystemFn,System.Object[])
extern "C"  Il2CppObject * SteamVR_Utils_CallSystemFn_m988611835 (Il2CppObject * __this /* static, unused */, SystemFn_t1182840554 * ___fn0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Utils::TakeStereoScreenshot(System.UInt32,UnityEngine.GameObject,System.Int32,System.Single,System.String&,System.String&)
extern "C"  void SteamVR_Utils_TakeStereoScreenshot_m2927137160 (Il2CppObject * __this /* static, unused */, uint32_t ___screenshotHandle0, GameObject_t1756533147 * ___target1, int32_t ___cellSize2, float ___ipd3, String_t** ___previewFilename4, String_t** ___VRFilename5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
