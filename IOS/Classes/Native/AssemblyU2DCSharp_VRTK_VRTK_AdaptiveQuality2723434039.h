﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>
struct ReadOnlyCollection_1_t2262295624;
// VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Int32>
struct AdaptiveSetting_1_t744429243;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;
// VRTK.VRTK_AdaptiveQuality/Timing
struct Timing_t3878883323;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Material
struct Material_t193706927;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.Func`2<UnityEngine.KeyCode,System.Boolean>
struct Func_2_t3687671679;
// System.Predicate`1<System.Single>
struct Predicate_1_t519480047;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_AdaptiveQuality
struct  VRTK_AdaptiveQuality_t2723434039  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.VRTK_AdaptiveQuality::drawDebugVisualization
	bool ___drawDebugVisualization_2;
	// System.Boolean VRTK.VRTK_AdaptiveQuality::allowKeyboardShortcuts
	bool ___allowKeyboardShortcuts_3;
	// System.Boolean VRTK.VRTK_AdaptiveQuality::allowCommandLineArguments
	bool ___allowCommandLineArguments_4;
	// System.Int32 VRTK.VRTK_AdaptiveQuality::msaaLevel
	int32_t ___msaaLevel_5;
	// System.Boolean VRTK.VRTK_AdaptiveQuality::scaleRenderViewport
	bool ___scaleRenderViewport_6;
	// System.Single VRTK.VRTK_AdaptiveQuality::minimumRenderScale
	float ___minimumRenderScale_7;
	// System.Single VRTK.VRTK_AdaptiveQuality::maximumRenderScale
	float ___maximumRenderScale_8;
	// System.Int32 VRTK.VRTK_AdaptiveQuality::maximumRenderTargetDimension
	int32_t ___maximumRenderTargetDimension_9;
	// System.Int32 VRTK.VRTK_AdaptiveQuality::renderScaleFillRateStepSizeInPercent
	int32_t ___renderScaleFillRateStepSizeInPercent_10;
	// System.Boolean VRTK.VRTK_AdaptiveQuality::scaleRenderTargetResolution
	bool ___scaleRenderTargetResolution_11;
	// System.Boolean VRTK.VRTK_AdaptiveQuality::overrideRenderViewportScale
	bool ___overrideRenderViewportScale_12;
	// System.Int32 VRTK.VRTK_AdaptiveQuality::overrideRenderViewportScaleLevel
	int32_t ___overrideRenderViewportScaleLevel_13;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> VRTK.VRTK_AdaptiveQuality::renderScales
	ReadOnlyCollection_1_t2262295624 * ___renderScales_14;
	// VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Int32> VRTK.VRTK_AdaptiveQuality::renderViewportScaleSetting
	AdaptiveSetting_1_t744429243 * ___renderViewportScaleSetting_16;
	// VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Int32> VRTK.VRTK_AdaptiveQuality::renderScaleSetting
	AdaptiveSetting_1_t744429243 * ___renderScaleSetting_17;
	// System.Collections.Generic.List`1<System.Single> VRTK.VRTK_AdaptiveQuality::allRenderScales
	List_1_t1445631064 * ___allRenderScales_18;
	// System.Int32 VRTK.VRTK_AdaptiveQuality::defaultRenderViewportScaleLevel
	int32_t ___defaultRenderViewportScaleLevel_19;
	// System.Single VRTK.VRTK_AdaptiveQuality::previousMinimumRenderScale
	float ___previousMinimumRenderScale_20;
	// System.Single VRTK.VRTK_AdaptiveQuality::previousMaximumRenderScale
	float ___previousMaximumRenderScale_21;
	// System.Single VRTK.VRTK_AdaptiveQuality::previousRenderScaleFillRateStepSizeInPercent
	float ___previousRenderScaleFillRateStepSizeInPercent_22;
	// VRTK.VRTK_AdaptiveQuality/Timing VRTK.VRTK_AdaptiveQuality::timing
	Timing_t3878883323 * ___timing_23;
	// System.Int32 VRTK.VRTK_AdaptiveQuality::lastRenderViewportScaleLevelBelowRenderScaleLevelFrameCount
	int32_t ___lastRenderViewportScaleLevelBelowRenderScaleLevelFrameCount_24;
	// System.Boolean VRTK.VRTK_AdaptiveQuality::interleavedReprojectionEnabled
	bool ___interleavedReprojectionEnabled_25;
	// System.Boolean VRTK.VRTK_AdaptiveQuality::hmdDisplayIsOnDesktop
	bool ___hmdDisplayIsOnDesktop_26;
	// System.Single VRTK.VRTK_AdaptiveQuality::singleFrameDurationInMilliseconds
	float ___singleFrameDurationInMilliseconds_27;
	// UnityEngine.GameObject VRTK.VRTK_AdaptiveQuality::debugVisualizationQuad
	GameObject_t1756533147 * ___debugVisualizationQuad_28;
	// UnityEngine.Material VRTK.VRTK_AdaptiveQuality::debugVisualizationQuadMaterial
	Material_t193706927 * ___debugVisualizationQuadMaterial_29;

public:
	inline static int32_t get_offset_of_drawDebugVisualization_2() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___drawDebugVisualization_2)); }
	inline bool get_drawDebugVisualization_2() const { return ___drawDebugVisualization_2; }
	inline bool* get_address_of_drawDebugVisualization_2() { return &___drawDebugVisualization_2; }
	inline void set_drawDebugVisualization_2(bool value)
	{
		___drawDebugVisualization_2 = value;
	}

	inline static int32_t get_offset_of_allowKeyboardShortcuts_3() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___allowKeyboardShortcuts_3)); }
	inline bool get_allowKeyboardShortcuts_3() const { return ___allowKeyboardShortcuts_3; }
	inline bool* get_address_of_allowKeyboardShortcuts_3() { return &___allowKeyboardShortcuts_3; }
	inline void set_allowKeyboardShortcuts_3(bool value)
	{
		___allowKeyboardShortcuts_3 = value;
	}

	inline static int32_t get_offset_of_allowCommandLineArguments_4() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___allowCommandLineArguments_4)); }
	inline bool get_allowCommandLineArguments_4() const { return ___allowCommandLineArguments_4; }
	inline bool* get_address_of_allowCommandLineArguments_4() { return &___allowCommandLineArguments_4; }
	inline void set_allowCommandLineArguments_4(bool value)
	{
		___allowCommandLineArguments_4 = value;
	}

	inline static int32_t get_offset_of_msaaLevel_5() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___msaaLevel_5)); }
	inline int32_t get_msaaLevel_5() const { return ___msaaLevel_5; }
	inline int32_t* get_address_of_msaaLevel_5() { return &___msaaLevel_5; }
	inline void set_msaaLevel_5(int32_t value)
	{
		___msaaLevel_5 = value;
	}

	inline static int32_t get_offset_of_scaleRenderViewport_6() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___scaleRenderViewport_6)); }
	inline bool get_scaleRenderViewport_6() const { return ___scaleRenderViewport_6; }
	inline bool* get_address_of_scaleRenderViewport_6() { return &___scaleRenderViewport_6; }
	inline void set_scaleRenderViewport_6(bool value)
	{
		___scaleRenderViewport_6 = value;
	}

	inline static int32_t get_offset_of_minimumRenderScale_7() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___minimumRenderScale_7)); }
	inline float get_minimumRenderScale_7() const { return ___minimumRenderScale_7; }
	inline float* get_address_of_minimumRenderScale_7() { return &___minimumRenderScale_7; }
	inline void set_minimumRenderScale_7(float value)
	{
		___minimumRenderScale_7 = value;
	}

	inline static int32_t get_offset_of_maximumRenderScale_8() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___maximumRenderScale_8)); }
	inline float get_maximumRenderScale_8() const { return ___maximumRenderScale_8; }
	inline float* get_address_of_maximumRenderScale_8() { return &___maximumRenderScale_8; }
	inline void set_maximumRenderScale_8(float value)
	{
		___maximumRenderScale_8 = value;
	}

	inline static int32_t get_offset_of_maximumRenderTargetDimension_9() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___maximumRenderTargetDimension_9)); }
	inline int32_t get_maximumRenderTargetDimension_9() const { return ___maximumRenderTargetDimension_9; }
	inline int32_t* get_address_of_maximumRenderTargetDimension_9() { return &___maximumRenderTargetDimension_9; }
	inline void set_maximumRenderTargetDimension_9(int32_t value)
	{
		___maximumRenderTargetDimension_9 = value;
	}

	inline static int32_t get_offset_of_renderScaleFillRateStepSizeInPercent_10() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___renderScaleFillRateStepSizeInPercent_10)); }
	inline int32_t get_renderScaleFillRateStepSizeInPercent_10() const { return ___renderScaleFillRateStepSizeInPercent_10; }
	inline int32_t* get_address_of_renderScaleFillRateStepSizeInPercent_10() { return &___renderScaleFillRateStepSizeInPercent_10; }
	inline void set_renderScaleFillRateStepSizeInPercent_10(int32_t value)
	{
		___renderScaleFillRateStepSizeInPercent_10 = value;
	}

	inline static int32_t get_offset_of_scaleRenderTargetResolution_11() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___scaleRenderTargetResolution_11)); }
	inline bool get_scaleRenderTargetResolution_11() const { return ___scaleRenderTargetResolution_11; }
	inline bool* get_address_of_scaleRenderTargetResolution_11() { return &___scaleRenderTargetResolution_11; }
	inline void set_scaleRenderTargetResolution_11(bool value)
	{
		___scaleRenderTargetResolution_11 = value;
	}

	inline static int32_t get_offset_of_overrideRenderViewportScale_12() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___overrideRenderViewportScale_12)); }
	inline bool get_overrideRenderViewportScale_12() const { return ___overrideRenderViewportScale_12; }
	inline bool* get_address_of_overrideRenderViewportScale_12() { return &___overrideRenderViewportScale_12; }
	inline void set_overrideRenderViewportScale_12(bool value)
	{
		___overrideRenderViewportScale_12 = value;
	}

	inline static int32_t get_offset_of_overrideRenderViewportScaleLevel_13() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___overrideRenderViewportScaleLevel_13)); }
	inline int32_t get_overrideRenderViewportScaleLevel_13() const { return ___overrideRenderViewportScaleLevel_13; }
	inline int32_t* get_address_of_overrideRenderViewportScaleLevel_13() { return &___overrideRenderViewportScaleLevel_13; }
	inline void set_overrideRenderViewportScaleLevel_13(int32_t value)
	{
		___overrideRenderViewportScaleLevel_13 = value;
	}

	inline static int32_t get_offset_of_renderScales_14() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___renderScales_14)); }
	inline ReadOnlyCollection_1_t2262295624 * get_renderScales_14() const { return ___renderScales_14; }
	inline ReadOnlyCollection_1_t2262295624 ** get_address_of_renderScales_14() { return &___renderScales_14; }
	inline void set_renderScales_14(ReadOnlyCollection_1_t2262295624 * value)
	{
		___renderScales_14 = value;
		Il2CppCodeGenWriteBarrier(&___renderScales_14, value);
	}

	inline static int32_t get_offset_of_renderViewportScaleSetting_16() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___renderViewportScaleSetting_16)); }
	inline AdaptiveSetting_1_t744429243 * get_renderViewportScaleSetting_16() const { return ___renderViewportScaleSetting_16; }
	inline AdaptiveSetting_1_t744429243 ** get_address_of_renderViewportScaleSetting_16() { return &___renderViewportScaleSetting_16; }
	inline void set_renderViewportScaleSetting_16(AdaptiveSetting_1_t744429243 * value)
	{
		___renderViewportScaleSetting_16 = value;
		Il2CppCodeGenWriteBarrier(&___renderViewportScaleSetting_16, value);
	}

	inline static int32_t get_offset_of_renderScaleSetting_17() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___renderScaleSetting_17)); }
	inline AdaptiveSetting_1_t744429243 * get_renderScaleSetting_17() const { return ___renderScaleSetting_17; }
	inline AdaptiveSetting_1_t744429243 ** get_address_of_renderScaleSetting_17() { return &___renderScaleSetting_17; }
	inline void set_renderScaleSetting_17(AdaptiveSetting_1_t744429243 * value)
	{
		___renderScaleSetting_17 = value;
		Il2CppCodeGenWriteBarrier(&___renderScaleSetting_17, value);
	}

	inline static int32_t get_offset_of_allRenderScales_18() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___allRenderScales_18)); }
	inline List_1_t1445631064 * get_allRenderScales_18() const { return ___allRenderScales_18; }
	inline List_1_t1445631064 ** get_address_of_allRenderScales_18() { return &___allRenderScales_18; }
	inline void set_allRenderScales_18(List_1_t1445631064 * value)
	{
		___allRenderScales_18 = value;
		Il2CppCodeGenWriteBarrier(&___allRenderScales_18, value);
	}

	inline static int32_t get_offset_of_defaultRenderViewportScaleLevel_19() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___defaultRenderViewportScaleLevel_19)); }
	inline int32_t get_defaultRenderViewportScaleLevel_19() const { return ___defaultRenderViewportScaleLevel_19; }
	inline int32_t* get_address_of_defaultRenderViewportScaleLevel_19() { return &___defaultRenderViewportScaleLevel_19; }
	inline void set_defaultRenderViewportScaleLevel_19(int32_t value)
	{
		___defaultRenderViewportScaleLevel_19 = value;
	}

	inline static int32_t get_offset_of_previousMinimumRenderScale_20() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___previousMinimumRenderScale_20)); }
	inline float get_previousMinimumRenderScale_20() const { return ___previousMinimumRenderScale_20; }
	inline float* get_address_of_previousMinimumRenderScale_20() { return &___previousMinimumRenderScale_20; }
	inline void set_previousMinimumRenderScale_20(float value)
	{
		___previousMinimumRenderScale_20 = value;
	}

	inline static int32_t get_offset_of_previousMaximumRenderScale_21() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___previousMaximumRenderScale_21)); }
	inline float get_previousMaximumRenderScale_21() const { return ___previousMaximumRenderScale_21; }
	inline float* get_address_of_previousMaximumRenderScale_21() { return &___previousMaximumRenderScale_21; }
	inline void set_previousMaximumRenderScale_21(float value)
	{
		___previousMaximumRenderScale_21 = value;
	}

	inline static int32_t get_offset_of_previousRenderScaleFillRateStepSizeInPercent_22() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___previousRenderScaleFillRateStepSizeInPercent_22)); }
	inline float get_previousRenderScaleFillRateStepSizeInPercent_22() const { return ___previousRenderScaleFillRateStepSizeInPercent_22; }
	inline float* get_address_of_previousRenderScaleFillRateStepSizeInPercent_22() { return &___previousRenderScaleFillRateStepSizeInPercent_22; }
	inline void set_previousRenderScaleFillRateStepSizeInPercent_22(float value)
	{
		___previousRenderScaleFillRateStepSizeInPercent_22 = value;
	}

	inline static int32_t get_offset_of_timing_23() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___timing_23)); }
	inline Timing_t3878883323 * get_timing_23() const { return ___timing_23; }
	inline Timing_t3878883323 ** get_address_of_timing_23() { return &___timing_23; }
	inline void set_timing_23(Timing_t3878883323 * value)
	{
		___timing_23 = value;
		Il2CppCodeGenWriteBarrier(&___timing_23, value);
	}

	inline static int32_t get_offset_of_lastRenderViewportScaleLevelBelowRenderScaleLevelFrameCount_24() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___lastRenderViewportScaleLevelBelowRenderScaleLevelFrameCount_24)); }
	inline int32_t get_lastRenderViewportScaleLevelBelowRenderScaleLevelFrameCount_24() const { return ___lastRenderViewportScaleLevelBelowRenderScaleLevelFrameCount_24; }
	inline int32_t* get_address_of_lastRenderViewportScaleLevelBelowRenderScaleLevelFrameCount_24() { return &___lastRenderViewportScaleLevelBelowRenderScaleLevelFrameCount_24; }
	inline void set_lastRenderViewportScaleLevelBelowRenderScaleLevelFrameCount_24(int32_t value)
	{
		___lastRenderViewportScaleLevelBelowRenderScaleLevelFrameCount_24 = value;
	}

	inline static int32_t get_offset_of_interleavedReprojectionEnabled_25() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___interleavedReprojectionEnabled_25)); }
	inline bool get_interleavedReprojectionEnabled_25() const { return ___interleavedReprojectionEnabled_25; }
	inline bool* get_address_of_interleavedReprojectionEnabled_25() { return &___interleavedReprojectionEnabled_25; }
	inline void set_interleavedReprojectionEnabled_25(bool value)
	{
		___interleavedReprojectionEnabled_25 = value;
	}

	inline static int32_t get_offset_of_hmdDisplayIsOnDesktop_26() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___hmdDisplayIsOnDesktop_26)); }
	inline bool get_hmdDisplayIsOnDesktop_26() const { return ___hmdDisplayIsOnDesktop_26; }
	inline bool* get_address_of_hmdDisplayIsOnDesktop_26() { return &___hmdDisplayIsOnDesktop_26; }
	inline void set_hmdDisplayIsOnDesktop_26(bool value)
	{
		___hmdDisplayIsOnDesktop_26 = value;
	}

	inline static int32_t get_offset_of_singleFrameDurationInMilliseconds_27() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___singleFrameDurationInMilliseconds_27)); }
	inline float get_singleFrameDurationInMilliseconds_27() const { return ___singleFrameDurationInMilliseconds_27; }
	inline float* get_address_of_singleFrameDurationInMilliseconds_27() { return &___singleFrameDurationInMilliseconds_27; }
	inline void set_singleFrameDurationInMilliseconds_27(float value)
	{
		___singleFrameDurationInMilliseconds_27 = value;
	}

	inline static int32_t get_offset_of_debugVisualizationQuad_28() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___debugVisualizationQuad_28)); }
	inline GameObject_t1756533147 * get_debugVisualizationQuad_28() const { return ___debugVisualizationQuad_28; }
	inline GameObject_t1756533147 ** get_address_of_debugVisualizationQuad_28() { return &___debugVisualizationQuad_28; }
	inline void set_debugVisualizationQuad_28(GameObject_t1756533147 * value)
	{
		___debugVisualizationQuad_28 = value;
		Il2CppCodeGenWriteBarrier(&___debugVisualizationQuad_28, value);
	}

	inline static int32_t get_offset_of_debugVisualizationQuadMaterial_29() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039, ___debugVisualizationQuadMaterial_29)); }
	inline Material_t193706927 * get_debugVisualizationQuadMaterial_29() const { return ___debugVisualizationQuadMaterial_29; }
	inline Material_t193706927 ** get_address_of_debugVisualizationQuadMaterial_29() { return &___debugVisualizationQuadMaterial_29; }
	inline void set_debugVisualizationQuadMaterial_29(Material_t193706927 * value)
	{
		___debugVisualizationQuadMaterial_29 = value;
		Il2CppCodeGenWriteBarrier(&___debugVisualizationQuadMaterial_29, value);
	}
};

struct VRTK_AdaptiveQuality_t2723434039_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> VRTK.VRTK_AdaptiveQuality::<>f__switch$map0
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map0_30;
	// System.Func`2<UnityEngine.KeyCode,System.Boolean> VRTK.VRTK_AdaptiveQuality::<>f__mg$cache0
	Func_2_t3687671679 * ___U3CU3Ef__mgU24cache0_31;
	// System.Predicate`1<System.Single> VRTK.VRTK_AdaptiveQuality::<>f__am$cache0
	Predicate_1_t519480047 * ___U3CU3Ef__amU24cache0_32;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_30() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039_StaticFields, ___U3CU3Ef__switchU24map0_30)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map0_30() const { return ___U3CU3Ef__switchU24map0_30; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map0_30() { return &___U3CU3Ef__switchU24map0_30; }
	inline void set_U3CU3Ef__switchU24map0_30(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map0_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_30, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_31() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039_StaticFields, ___U3CU3Ef__mgU24cache0_31)); }
	inline Func_2_t3687671679 * get_U3CU3Ef__mgU24cache0_31() const { return ___U3CU3Ef__mgU24cache0_31; }
	inline Func_2_t3687671679 ** get_address_of_U3CU3Ef__mgU24cache0_31() { return &___U3CU3Ef__mgU24cache0_31; }
	inline void set_U3CU3Ef__mgU24cache0_31(Func_2_t3687671679 * value)
	{
		___U3CU3Ef__mgU24cache0_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_31, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_32() { return static_cast<int32_t>(offsetof(VRTK_AdaptiveQuality_t2723434039_StaticFields, ___U3CU3Ef__amU24cache0_32)); }
	inline Predicate_1_t519480047 * get_U3CU3Ef__amU24cache0_32() const { return ___U3CU3Ef__amU24cache0_32; }
	inline Predicate_1_t519480047 ** get_address_of_U3CU3Ef__amU24cache0_32() { return &___U3CU3Ef__amU24cache0_32; }
	inline void set_U3CU3Ef__amU24cache0_32(Predicate_1_t519480047 * value)
	{
		___U3CU3Ef__amU24cache0_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
