﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.PlaySound
struct PlaySound_t165629647;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.PlaySound::.ctor()
extern "C"  void PlaySound__ctor_m4142516793 (PlaySound_t165629647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.PlaySound::Awake()
extern "C"  void PlaySound_Awake_m3515064788 (PlaySound_t165629647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.PlaySound::Play()
extern "C"  void PlaySound_Play_m265987193 (PlaySound_t165629647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.PlaySound::PlayWithDelay(System.Single)
extern "C"  void PlaySound_PlayWithDelay_m280265827 (PlaySound_t165629647 * __this, float ___delayTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip Valve.VR.InteractionSystem.PlaySound::PlayOneShotSound()
extern "C"  AudioClip_t1932558630 * PlaySound_PlayOneShotSound_m2084403808 (PlaySound_t165629647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip Valve.VR.InteractionSystem.PlaySound::PlayLooping()
extern "C"  AudioClip_t1932558630 * PlaySound_PlayLooping_m3566813993 (PlaySound_t165629647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.PlaySound::Disable()
extern "C"  void PlaySound_Disable_m939175689 (PlaySound_t165629647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.PlaySound::Stop()
extern "C"  void PlaySound_Stop_m887889225 (PlaySound_t165629647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.PlaySound::SetAudioSource()
extern "C"  void PlaySound_SetAudioSource_m2389462152 (PlaySound_t165629647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
