﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ControllerActions/<CycleColor>c__Iterator2
struct U3CCycleColorU3Ec__Iterator2_t3567685877;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_ControllerActions/<CycleColor>c__Iterator2::.ctor()
extern "C"  void U3CCycleColorU3Ec__Iterator2__ctor_m912285428 (U3CCycleColorU3Ec__Iterator2_t3567685877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_ControllerActions/<CycleColor>c__Iterator2::MoveNext()
extern "C"  bool U3CCycleColorU3Ec__Iterator2_MoveNext_m1433828084 (U3CCycleColorU3Ec__Iterator2_t3567685877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_ControllerActions/<CycleColor>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCycleColorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3177182776 (U3CCycleColorU3Ec__Iterator2_t3567685877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_ControllerActions/<CycleColor>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCycleColorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2484509696 (U3CCycleColorU3Ec__Iterator2_t3567685877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions/<CycleColor>c__Iterator2::Dispose()
extern "C"  void U3CCycleColorU3Ec__Iterator2_Dispose_m3535925279 (U3CCycleColorU3Ec__Iterator2_t3567685877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions/<CycleColor>c__Iterator2::Reset()
extern "C"  void U3CCycleColorU3Ec__Iterator2_Reset_m3522394341 (U3CCycleColorU3Ec__Iterator2_t3567685877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
