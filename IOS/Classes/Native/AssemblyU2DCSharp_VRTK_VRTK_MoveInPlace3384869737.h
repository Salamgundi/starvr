﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_BodyPhysics
struct VRTK_BodyPhysics_t3414085265;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>
struct Dictionary_2_t1187285969;
// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,System.Single>
struct Dictionary_2_t1818164837;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_MoveInPlace_ControlOpt4045712330.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_MoveInPlace_Directiona2902995460.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_MoveInPlace
struct  VRTK_MoveInPlace_t3384869737  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.VRTK_MoveInPlace::leftController
	bool ___leftController_2;
	// System.Boolean VRTK.VRTK_MoveInPlace::rightController
	bool ___rightController_3;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_MoveInPlace::engageButton
	int32_t ___engageButton_4;
	// VRTK.VRTK_MoveInPlace/ControlOptions VRTK.VRTK_MoveInPlace::controlOptions
	int32_t ___controlOptions_5;
	// System.Single VRTK.VRTK_MoveInPlace::speedScale
	float ___speedScale_6;
	// System.Single VRTK.VRTK_MoveInPlace::maxSpeed
	float ___maxSpeed_7;
	// System.Single VRTK.VRTK_MoveInPlace::deceleration
	float ___deceleration_8;
	// System.Single VRTK.VRTK_MoveInPlace::fallingDeceleration
	float ___fallingDeceleration_9;
	// VRTK.VRTK_MoveInPlace/DirectionalMethod VRTK.VRTK_MoveInPlace::directionMethod
	int32_t ___directionMethod_10;
	// System.Single VRTK.VRTK_MoveInPlace::smartDecoupleThreshold
	float ___smartDecoupleThreshold_11;
	// System.Single VRTK.VRTK_MoveInPlace::sensitivity
	float ___sensitivity_12;
	// UnityEngine.Transform VRTK.VRTK_MoveInPlace::playArea
	Transform_t3275118058 * ___playArea_13;
	// UnityEngine.GameObject VRTK.VRTK_MoveInPlace::controllerLeftHand
	GameObject_t1756533147 * ___controllerLeftHand_14;
	// UnityEngine.GameObject VRTK.VRTK_MoveInPlace::controllerRightHand
	GameObject_t1756533147 * ___controllerRightHand_15;
	// UnityEngine.Transform VRTK.VRTK_MoveInPlace::headset
	Transform_t3275118058 * ___headset_16;
	// System.Boolean VRTK.VRTK_MoveInPlace::leftSubscribed
	bool ___leftSubscribed_17;
	// System.Boolean VRTK.VRTK_MoveInPlace::rightSubscribed
	bool ___rightSubscribed_18;
	// System.Boolean VRTK.VRTK_MoveInPlace::previousLeftControllerState
	bool ___previousLeftControllerState_19;
	// System.Boolean VRTK.VRTK_MoveInPlace::previousRightControllerState
	bool ___previousRightControllerState_20;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_MoveInPlace::previousEngageButton
	int32_t ___previousEngageButton_21;
	// VRTK.VRTK_BodyPhysics VRTK.VRTK_MoveInPlace::bodyPhysics
	VRTK_BodyPhysics_t3414085265 * ___bodyPhysics_22;
	// System.Boolean VRTK.VRTK_MoveInPlace::currentlyFalling
	bool ___currentlyFalling_23;
	// System.Int32 VRTK.VRTK_MoveInPlace::averagePeriod
	int32_t ___averagePeriod_24;
	// System.Collections.Generic.List`1<UnityEngine.Transform> VRTK.VRTK_MoveInPlace::trackedObjects
	List_1_t2644239190 * ___trackedObjects_25;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>> VRTK.VRTK_MoveInPlace::movementList
	Dictionary_2_t1187285969 * ___movementList_26;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,System.Single> VRTK.VRTK_MoveInPlace::previousYPositions
	Dictionary_2_t1818164837 * ___previousYPositions_27;
	// UnityEngine.Vector3 VRTK.VRTK_MoveInPlace::initalGaze
	Vector3_t2243707580  ___initalGaze_28;
	// System.Single VRTK.VRTK_MoveInPlace::currentSpeed
	float ___currentSpeed_29;
	// UnityEngine.Vector3 VRTK.VRTK_MoveInPlace::direction
	Vector3_t2243707580  ___direction_30;
	// System.Boolean VRTK.VRTK_MoveInPlace::active
	bool ___active_31;

public:
	inline static int32_t get_offset_of_leftController_2() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___leftController_2)); }
	inline bool get_leftController_2() const { return ___leftController_2; }
	inline bool* get_address_of_leftController_2() { return &___leftController_2; }
	inline void set_leftController_2(bool value)
	{
		___leftController_2 = value;
	}

	inline static int32_t get_offset_of_rightController_3() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___rightController_3)); }
	inline bool get_rightController_3() const { return ___rightController_3; }
	inline bool* get_address_of_rightController_3() { return &___rightController_3; }
	inline void set_rightController_3(bool value)
	{
		___rightController_3 = value;
	}

	inline static int32_t get_offset_of_engageButton_4() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___engageButton_4)); }
	inline int32_t get_engageButton_4() const { return ___engageButton_4; }
	inline int32_t* get_address_of_engageButton_4() { return &___engageButton_4; }
	inline void set_engageButton_4(int32_t value)
	{
		___engageButton_4 = value;
	}

	inline static int32_t get_offset_of_controlOptions_5() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___controlOptions_5)); }
	inline int32_t get_controlOptions_5() const { return ___controlOptions_5; }
	inline int32_t* get_address_of_controlOptions_5() { return &___controlOptions_5; }
	inline void set_controlOptions_5(int32_t value)
	{
		___controlOptions_5 = value;
	}

	inline static int32_t get_offset_of_speedScale_6() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___speedScale_6)); }
	inline float get_speedScale_6() const { return ___speedScale_6; }
	inline float* get_address_of_speedScale_6() { return &___speedScale_6; }
	inline void set_speedScale_6(float value)
	{
		___speedScale_6 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_7() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___maxSpeed_7)); }
	inline float get_maxSpeed_7() const { return ___maxSpeed_7; }
	inline float* get_address_of_maxSpeed_7() { return &___maxSpeed_7; }
	inline void set_maxSpeed_7(float value)
	{
		___maxSpeed_7 = value;
	}

	inline static int32_t get_offset_of_deceleration_8() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___deceleration_8)); }
	inline float get_deceleration_8() const { return ___deceleration_8; }
	inline float* get_address_of_deceleration_8() { return &___deceleration_8; }
	inline void set_deceleration_8(float value)
	{
		___deceleration_8 = value;
	}

	inline static int32_t get_offset_of_fallingDeceleration_9() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___fallingDeceleration_9)); }
	inline float get_fallingDeceleration_9() const { return ___fallingDeceleration_9; }
	inline float* get_address_of_fallingDeceleration_9() { return &___fallingDeceleration_9; }
	inline void set_fallingDeceleration_9(float value)
	{
		___fallingDeceleration_9 = value;
	}

	inline static int32_t get_offset_of_directionMethod_10() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___directionMethod_10)); }
	inline int32_t get_directionMethod_10() const { return ___directionMethod_10; }
	inline int32_t* get_address_of_directionMethod_10() { return &___directionMethod_10; }
	inline void set_directionMethod_10(int32_t value)
	{
		___directionMethod_10 = value;
	}

	inline static int32_t get_offset_of_smartDecoupleThreshold_11() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___smartDecoupleThreshold_11)); }
	inline float get_smartDecoupleThreshold_11() const { return ___smartDecoupleThreshold_11; }
	inline float* get_address_of_smartDecoupleThreshold_11() { return &___smartDecoupleThreshold_11; }
	inline void set_smartDecoupleThreshold_11(float value)
	{
		___smartDecoupleThreshold_11 = value;
	}

	inline static int32_t get_offset_of_sensitivity_12() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___sensitivity_12)); }
	inline float get_sensitivity_12() const { return ___sensitivity_12; }
	inline float* get_address_of_sensitivity_12() { return &___sensitivity_12; }
	inline void set_sensitivity_12(float value)
	{
		___sensitivity_12 = value;
	}

	inline static int32_t get_offset_of_playArea_13() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___playArea_13)); }
	inline Transform_t3275118058 * get_playArea_13() const { return ___playArea_13; }
	inline Transform_t3275118058 ** get_address_of_playArea_13() { return &___playArea_13; }
	inline void set_playArea_13(Transform_t3275118058 * value)
	{
		___playArea_13 = value;
		Il2CppCodeGenWriteBarrier(&___playArea_13, value);
	}

	inline static int32_t get_offset_of_controllerLeftHand_14() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___controllerLeftHand_14)); }
	inline GameObject_t1756533147 * get_controllerLeftHand_14() const { return ___controllerLeftHand_14; }
	inline GameObject_t1756533147 ** get_address_of_controllerLeftHand_14() { return &___controllerLeftHand_14; }
	inline void set_controllerLeftHand_14(GameObject_t1756533147 * value)
	{
		___controllerLeftHand_14 = value;
		Il2CppCodeGenWriteBarrier(&___controllerLeftHand_14, value);
	}

	inline static int32_t get_offset_of_controllerRightHand_15() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___controllerRightHand_15)); }
	inline GameObject_t1756533147 * get_controllerRightHand_15() const { return ___controllerRightHand_15; }
	inline GameObject_t1756533147 ** get_address_of_controllerRightHand_15() { return &___controllerRightHand_15; }
	inline void set_controllerRightHand_15(GameObject_t1756533147 * value)
	{
		___controllerRightHand_15 = value;
		Il2CppCodeGenWriteBarrier(&___controllerRightHand_15, value);
	}

	inline static int32_t get_offset_of_headset_16() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___headset_16)); }
	inline Transform_t3275118058 * get_headset_16() const { return ___headset_16; }
	inline Transform_t3275118058 ** get_address_of_headset_16() { return &___headset_16; }
	inline void set_headset_16(Transform_t3275118058 * value)
	{
		___headset_16 = value;
		Il2CppCodeGenWriteBarrier(&___headset_16, value);
	}

	inline static int32_t get_offset_of_leftSubscribed_17() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___leftSubscribed_17)); }
	inline bool get_leftSubscribed_17() const { return ___leftSubscribed_17; }
	inline bool* get_address_of_leftSubscribed_17() { return &___leftSubscribed_17; }
	inline void set_leftSubscribed_17(bool value)
	{
		___leftSubscribed_17 = value;
	}

	inline static int32_t get_offset_of_rightSubscribed_18() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___rightSubscribed_18)); }
	inline bool get_rightSubscribed_18() const { return ___rightSubscribed_18; }
	inline bool* get_address_of_rightSubscribed_18() { return &___rightSubscribed_18; }
	inline void set_rightSubscribed_18(bool value)
	{
		___rightSubscribed_18 = value;
	}

	inline static int32_t get_offset_of_previousLeftControllerState_19() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___previousLeftControllerState_19)); }
	inline bool get_previousLeftControllerState_19() const { return ___previousLeftControllerState_19; }
	inline bool* get_address_of_previousLeftControllerState_19() { return &___previousLeftControllerState_19; }
	inline void set_previousLeftControllerState_19(bool value)
	{
		___previousLeftControllerState_19 = value;
	}

	inline static int32_t get_offset_of_previousRightControllerState_20() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___previousRightControllerState_20)); }
	inline bool get_previousRightControllerState_20() const { return ___previousRightControllerState_20; }
	inline bool* get_address_of_previousRightControllerState_20() { return &___previousRightControllerState_20; }
	inline void set_previousRightControllerState_20(bool value)
	{
		___previousRightControllerState_20 = value;
	}

	inline static int32_t get_offset_of_previousEngageButton_21() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___previousEngageButton_21)); }
	inline int32_t get_previousEngageButton_21() const { return ___previousEngageButton_21; }
	inline int32_t* get_address_of_previousEngageButton_21() { return &___previousEngageButton_21; }
	inline void set_previousEngageButton_21(int32_t value)
	{
		___previousEngageButton_21 = value;
	}

	inline static int32_t get_offset_of_bodyPhysics_22() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___bodyPhysics_22)); }
	inline VRTK_BodyPhysics_t3414085265 * get_bodyPhysics_22() const { return ___bodyPhysics_22; }
	inline VRTK_BodyPhysics_t3414085265 ** get_address_of_bodyPhysics_22() { return &___bodyPhysics_22; }
	inline void set_bodyPhysics_22(VRTK_BodyPhysics_t3414085265 * value)
	{
		___bodyPhysics_22 = value;
		Il2CppCodeGenWriteBarrier(&___bodyPhysics_22, value);
	}

	inline static int32_t get_offset_of_currentlyFalling_23() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___currentlyFalling_23)); }
	inline bool get_currentlyFalling_23() const { return ___currentlyFalling_23; }
	inline bool* get_address_of_currentlyFalling_23() { return &___currentlyFalling_23; }
	inline void set_currentlyFalling_23(bool value)
	{
		___currentlyFalling_23 = value;
	}

	inline static int32_t get_offset_of_averagePeriod_24() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___averagePeriod_24)); }
	inline int32_t get_averagePeriod_24() const { return ___averagePeriod_24; }
	inline int32_t* get_address_of_averagePeriod_24() { return &___averagePeriod_24; }
	inline void set_averagePeriod_24(int32_t value)
	{
		___averagePeriod_24 = value;
	}

	inline static int32_t get_offset_of_trackedObjects_25() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___trackedObjects_25)); }
	inline List_1_t2644239190 * get_trackedObjects_25() const { return ___trackedObjects_25; }
	inline List_1_t2644239190 ** get_address_of_trackedObjects_25() { return &___trackedObjects_25; }
	inline void set_trackedObjects_25(List_1_t2644239190 * value)
	{
		___trackedObjects_25 = value;
		Il2CppCodeGenWriteBarrier(&___trackedObjects_25, value);
	}

	inline static int32_t get_offset_of_movementList_26() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___movementList_26)); }
	inline Dictionary_2_t1187285969 * get_movementList_26() const { return ___movementList_26; }
	inline Dictionary_2_t1187285969 ** get_address_of_movementList_26() { return &___movementList_26; }
	inline void set_movementList_26(Dictionary_2_t1187285969 * value)
	{
		___movementList_26 = value;
		Il2CppCodeGenWriteBarrier(&___movementList_26, value);
	}

	inline static int32_t get_offset_of_previousYPositions_27() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___previousYPositions_27)); }
	inline Dictionary_2_t1818164837 * get_previousYPositions_27() const { return ___previousYPositions_27; }
	inline Dictionary_2_t1818164837 ** get_address_of_previousYPositions_27() { return &___previousYPositions_27; }
	inline void set_previousYPositions_27(Dictionary_2_t1818164837 * value)
	{
		___previousYPositions_27 = value;
		Il2CppCodeGenWriteBarrier(&___previousYPositions_27, value);
	}

	inline static int32_t get_offset_of_initalGaze_28() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___initalGaze_28)); }
	inline Vector3_t2243707580  get_initalGaze_28() const { return ___initalGaze_28; }
	inline Vector3_t2243707580 * get_address_of_initalGaze_28() { return &___initalGaze_28; }
	inline void set_initalGaze_28(Vector3_t2243707580  value)
	{
		___initalGaze_28 = value;
	}

	inline static int32_t get_offset_of_currentSpeed_29() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___currentSpeed_29)); }
	inline float get_currentSpeed_29() const { return ___currentSpeed_29; }
	inline float* get_address_of_currentSpeed_29() { return &___currentSpeed_29; }
	inline void set_currentSpeed_29(float value)
	{
		___currentSpeed_29 = value;
	}

	inline static int32_t get_offset_of_direction_30() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___direction_30)); }
	inline Vector3_t2243707580  get_direction_30() const { return ___direction_30; }
	inline Vector3_t2243707580 * get_address_of_direction_30() { return &___direction_30; }
	inline void set_direction_30(Vector3_t2243707580  value)
	{
		___direction_30 = value;
	}

	inline static int32_t get_offset_of_active_31() { return static_cast<int32_t>(offsetof(VRTK_MoveInPlace_t3384869737, ___active_31)); }
	inline bool get_active_31() const { return ___active_31; }
	inline bool* get_address_of_active_31() { return &___active_31; }
	inline void set_active_31(bool value)
	{
		___active_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
