﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_UIDropZone
struct VRTK_UIDropZone_t1661569883;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void VRTK.VRTK_UIDropZone::.ctor()
extern "C"  void VRTK_UIDropZone__ctor_m2283027709 (VRTK_UIDropZone_t1661569883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIDropZone::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void VRTK_UIDropZone_OnPointerEnter_m4214959127 (VRTK_UIDropZone_t1661569883 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIDropZone::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void VRTK_UIDropZone_OnPointerExit_m4192455799 (VRTK_UIDropZone_t1661569883 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
