﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_BaseController
struct SDK_BaseController_t197168236;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_VRTK_SDK_BaseController_Controlle246418309.h"

// System.Void VRTK.SDK_BaseController::.ctor()
extern "C"  void SDK_BaseController__ctor_m815260764 (SDK_BaseController_t197168236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.SDK_BaseController::GetSDKManagerControllerLeftHand(System.Boolean)
extern "C"  GameObject_t1756533147 * SDK_BaseController_GetSDKManagerControllerLeftHand_m2484726421 (SDK_BaseController_t197168236 * __this, bool ___actual0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.SDK_BaseController::GetSDKManagerControllerRightHand(System.Boolean)
extern "C"  GameObject_t1756533147 * SDK_BaseController_GetSDKManagerControllerRightHand_m1199206966 (SDK_BaseController_t197168236 * __this, bool ___actual0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_BaseController::CheckActualOrScriptAliasControllerIsLeftHand(UnityEngine.GameObject)
extern "C"  bool SDK_BaseController_CheckActualOrScriptAliasControllerIsLeftHand_m2943739314 (SDK_BaseController_t197168236 * __this, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_BaseController::CheckActualOrScriptAliasControllerIsRightHand(UnityEngine.GameObject)
extern "C"  bool SDK_BaseController_CheckActualOrScriptAliasControllerIsRightHand_m3331695229 (SDK_BaseController_t197168236 * __this, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_BaseController::CheckControllerLeftHand(UnityEngine.GameObject,System.Boolean)
extern "C"  bool SDK_BaseController_CheckControllerLeftHand_m1102191601 (SDK_BaseController_t197168236 * __this, GameObject_t1756533147 * ___controller0, bool ___actual1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_BaseController::CheckControllerRightHand(UnityEngine.GameObject,System.Boolean)
extern "C"  bool SDK_BaseController_CheckControllerRightHand_m2234768184 (SDK_BaseController_t197168236 * __this, GameObject_t1756533147 * ___controller0, bool ___actual1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.SDK_BaseController::GetControllerModelFromController(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * SDK_BaseController_GetControllerModelFromController_m1610006898 (SDK_BaseController_t197168236 * __this, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.SDK_BaseController::GetSDKManagerControllerModelForHand(VRTK.SDK_BaseController/ControllerHand)
extern "C"  GameObject_t1756533147 * SDK_BaseController_GetSDKManagerControllerModelForHand_m951013970 (SDK_BaseController_t197168236 * __this, int32_t ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.SDK_BaseController::GetActualController(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * SDK_BaseController_GetActualController_m2965772263 (SDK_BaseController_t197168236 * __this, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
