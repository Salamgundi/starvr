﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.DestinationMarkerEventHandler
struct DestinationMarkerEventHandler_t1148830384;
// VRTK.VRTK_PolicyList
struct VRTK_PolicyList_t2965133344;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_DestinationMarker
struct  VRTK_DestinationMarker_t667613644  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.VRTK_DestinationMarker::enableTeleport
	bool ___enableTeleport_2;
	// VRTK.DestinationMarkerEventHandler VRTK.VRTK_DestinationMarker::DestinationMarkerEnter
	DestinationMarkerEventHandler_t1148830384 * ___DestinationMarkerEnter_3;
	// VRTK.DestinationMarkerEventHandler VRTK.VRTK_DestinationMarker::DestinationMarkerExit
	DestinationMarkerEventHandler_t1148830384 * ___DestinationMarkerExit_4;
	// VRTK.DestinationMarkerEventHandler VRTK.VRTK_DestinationMarker::DestinationMarkerSet
	DestinationMarkerEventHandler_t1148830384 * ___DestinationMarkerSet_5;
	// VRTK.VRTK_PolicyList VRTK.VRTK_DestinationMarker::invalidListPolicy
	VRTK_PolicyList_t2965133344 * ___invalidListPolicy_6;
	// System.Single VRTK.VRTK_DestinationMarker::navMeshCheckDistance
	float ___navMeshCheckDistance_7;
	// System.Boolean VRTK.VRTK_DestinationMarker::headsetPositionCompensation
	bool ___headsetPositionCompensation_8;

public:
	inline static int32_t get_offset_of_enableTeleport_2() { return static_cast<int32_t>(offsetof(VRTK_DestinationMarker_t667613644, ___enableTeleport_2)); }
	inline bool get_enableTeleport_2() const { return ___enableTeleport_2; }
	inline bool* get_address_of_enableTeleport_2() { return &___enableTeleport_2; }
	inline void set_enableTeleport_2(bool value)
	{
		___enableTeleport_2 = value;
	}

	inline static int32_t get_offset_of_DestinationMarkerEnter_3() { return static_cast<int32_t>(offsetof(VRTK_DestinationMarker_t667613644, ___DestinationMarkerEnter_3)); }
	inline DestinationMarkerEventHandler_t1148830384 * get_DestinationMarkerEnter_3() const { return ___DestinationMarkerEnter_3; }
	inline DestinationMarkerEventHandler_t1148830384 ** get_address_of_DestinationMarkerEnter_3() { return &___DestinationMarkerEnter_3; }
	inline void set_DestinationMarkerEnter_3(DestinationMarkerEventHandler_t1148830384 * value)
	{
		___DestinationMarkerEnter_3 = value;
		Il2CppCodeGenWriteBarrier(&___DestinationMarkerEnter_3, value);
	}

	inline static int32_t get_offset_of_DestinationMarkerExit_4() { return static_cast<int32_t>(offsetof(VRTK_DestinationMarker_t667613644, ___DestinationMarkerExit_4)); }
	inline DestinationMarkerEventHandler_t1148830384 * get_DestinationMarkerExit_4() const { return ___DestinationMarkerExit_4; }
	inline DestinationMarkerEventHandler_t1148830384 ** get_address_of_DestinationMarkerExit_4() { return &___DestinationMarkerExit_4; }
	inline void set_DestinationMarkerExit_4(DestinationMarkerEventHandler_t1148830384 * value)
	{
		___DestinationMarkerExit_4 = value;
		Il2CppCodeGenWriteBarrier(&___DestinationMarkerExit_4, value);
	}

	inline static int32_t get_offset_of_DestinationMarkerSet_5() { return static_cast<int32_t>(offsetof(VRTK_DestinationMarker_t667613644, ___DestinationMarkerSet_5)); }
	inline DestinationMarkerEventHandler_t1148830384 * get_DestinationMarkerSet_5() const { return ___DestinationMarkerSet_5; }
	inline DestinationMarkerEventHandler_t1148830384 ** get_address_of_DestinationMarkerSet_5() { return &___DestinationMarkerSet_5; }
	inline void set_DestinationMarkerSet_5(DestinationMarkerEventHandler_t1148830384 * value)
	{
		___DestinationMarkerSet_5 = value;
		Il2CppCodeGenWriteBarrier(&___DestinationMarkerSet_5, value);
	}

	inline static int32_t get_offset_of_invalidListPolicy_6() { return static_cast<int32_t>(offsetof(VRTK_DestinationMarker_t667613644, ___invalidListPolicy_6)); }
	inline VRTK_PolicyList_t2965133344 * get_invalidListPolicy_6() const { return ___invalidListPolicy_6; }
	inline VRTK_PolicyList_t2965133344 ** get_address_of_invalidListPolicy_6() { return &___invalidListPolicy_6; }
	inline void set_invalidListPolicy_6(VRTK_PolicyList_t2965133344 * value)
	{
		___invalidListPolicy_6 = value;
		Il2CppCodeGenWriteBarrier(&___invalidListPolicy_6, value);
	}

	inline static int32_t get_offset_of_navMeshCheckDistance_7() { return static_cast<int32_t>(offsetof(VRTK_DestinationMarker_t667613644, ___navMeshCheckDistance_7)); }
	inline float get_navMeshCheckDistance_7() const { return ___navMeshCheckDistance_7; }
	inline float* get_address_of_navMeshCheckDistance_7() { return &___navMeshCheckDistance_7; }
	inline void set_navMeshCheckDistance_7(float value)
	{
		___navMeshCheckDistance_7 = value;
	}

	inline static int32_t get_offset_of_headsetPositionCompensation_8() { return static_cast<int32_t>(offsetof(VRTK_DestinationMarker_t667613644, ___headsetPositionCompensation_8)); }
	inline bool get_headsetPositionCompensation_8() const { return ___headsetPositionCompensation_8; }
	inline bool* get_address_of_headsetPositionCompensation_8() { return &___headsetPositionCompensation_8; }
	inline void set_headsetPositionCompensation_8(bool value)
	{
		___headsetPositionCompensation_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
