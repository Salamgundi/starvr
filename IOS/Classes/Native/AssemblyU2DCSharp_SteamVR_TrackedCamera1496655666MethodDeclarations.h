﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_TrackedCamera
struct SteamVR_TrackedCamera_t1496655666;
// SteamVR_TrackedCamera/VideoStreamTexture
struct VideoStreamTexture_t930129953;
// SteamVR_TrackedCamera/VideoStream
struct VideoStream_t676682966;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_TrackedCamera::.ctor()
extern "C"  void SteamVR_TrackedCamera__ctor_m3609226725 (SteamVR_TrackedCamera_t1496655666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_TrackedCamera/VideoStreamTexture SteamVR_TrackedCamera::Distorted(System.Int32)
extern "C"  VideoStreamTexture_t930129953 * SteamVR_TrackedCamera_Distorted_m663114234 (Il2CppObject * __this /* static, unused */, int32_t ___deviceIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_TrackedCamera/VideoStreamTexture SteamVR_TrackedCamera::Undistorted(System.Int32)
extern "C"  VideoStreamTexture_t930129953 * SteamVR_TrackedCamera_Undistorted_m425667249 (Il2CppObject * __this /* static, unused */, int32_t ___deviceIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_TrackedCamera/VideoStreamTexture SteamVR_TrackedCamera::Source(System.Boolean,System.Int32)
extern "C"  VideoStreamTexture_t930129953 * SteamVR_TrackedCamera_Source_m3473278044 (Il2CppObject * __this /* static, unused */, bool ___undistorted0, int32_t ___deviceIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_TrackedCamera/VideoStream SteamVR_TrackedCamera::Stream(System.UInt32)
extern "C"  VideoStream_t676682966 * SteamVR_TrackedCamera_Stream_m1045929374 (Il2CppObject * __this /* static, unused */, uint32_t ___deviceIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
