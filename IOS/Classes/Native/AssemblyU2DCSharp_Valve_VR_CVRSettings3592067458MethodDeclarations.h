﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRSettings
struct CVRSettings_t3592067458;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRSettingsError4124928198.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"

// System.Void Valve.VR.CVRSettings::.ctor(System.IntPtr)
extern "C"  void CVRSettings__ctor_m4217265635 (CVRSettings_t3592067458 * __this, IntPtr_t ___pInterface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.CVRSettings::GetSettingsErrorNameFromEnum(Valve.VR.EVRSettingsError)
extern "C"  String_t* CVRSettings_GetSettingsErrorNameFromEnum_m761205125 (CVRSettings_t3592067458 * __this, int32_t ___eError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSettings::Sync(System.Boolean,Valve.VR.EVRSettingsError&)
extern "C"  bool CVRSettings_Sync_m132424075 (CVRSettings_t3592067458 * __this, bool ___bForce0, int32_t* ___peError1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSettings::SetBool(System.String,System.String,System.Boolean,Valve.VR.EVRSettingsError&)
extern "C"  void CVRSettings_SetBool_m2954698266 (CVRSettings_t3592067458 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, bool ___bValue2, int32_t* ___peError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSettings::SetInt32(System.String,System.String,System.Int32,Valve.VR.EVRSettingsError&)
extern "C"  void CVRSettings_SetInt32_m368756686 (CVRSettings_t3592067458 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t ___nValue2, int32_t* ___peError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSettings::SetFloat(System.String,System.String,System.Single,Valve.VR.EVRSettingsError&)
extern "C"  void CVRSettings_SetFloat_m2925725254 (CVRSettings_t3592067458 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, float ___flValue2, int32_t* ___peError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSettings::SetString(System.String,System.String,System.String,Valve.VR.EVRSettingsError&)
extern "C"  void CVRSettings_SetString_m1760790676 (CVRSettings_t3592067458 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, String_t* ___pchValue2, int32_t* ___peError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRSettings::GetBool(System.String,System.String,Valve.VR.EVRSettingsError&)
extern "C"  bool CVRSettings_GetBool_m653321307 (CVRSettings_t3592067458 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Valve.VR.CVRSettings::GetInt32(System.String,System.String,Valve.VR.EVRSettingsError&)
extern "C"  int32_t CVRSettings_GetInt32_m985362161 (CVRSettings_t3592067458 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.CVRSettings::GetFloat(System.String,System.String,Valve.VR.EVRSettingsError&)
extern "C"  float CVRSettings_GetFloat_m991429183 (CVRSettings_t3592067458 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSettings::GetString(System.String,System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRSettingsError&)
extern "C"  void CVRSettings_GetString_m3704525220 (CVRSettings_t3592067458 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unValueLen3, int32_t* ___peError4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSettings::RemoveSection(System.String,Valve.VR.EVRSettingsError&)
extern "C"  void CVRSettings_RemoveSection_m2060690670 (CVRSettings_t3592067458 * __this, String_t* ___pchSection0, int32_t* ___peError1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRSettings::RemoveKeyInSection(System.String,System.String,Valve.VR.EVRSettingsError&)
extern "C"  void CVRSettings_RemoveKeyInSection_m3771668278 (CVRSettings_t3592067458 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
