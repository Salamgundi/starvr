﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_Control
struct VRTK_Control_t651619021;
// VRTK.Control3DEventHandler
struct Control3DEventHandler_t2392187186;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_Control3DEventHandler2392187186.h"
#include "AssemblyU2DCSharp_VRTK_Control3DEventArgs4095025701.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_Control::.ctor()
extern "C"  void VRTK_Control__ctor_m394111009 (VRTK_Control_t651619021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Control::add_ValueChanged(VRTK.Control3DEventHandler)
extern "C"  void VRTK_Control_add_ValueChanged_m710643155 (VRTK_Control_t651619021 * __this, Control3DEventHandler_t2392187186 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Control::remove_ValueChanged(VRTK.Control3DEventHandler)
extern "C"  void VRTK_Control_remove_ValueChanged_m539212340 (VRTK_Control_t651619021 * __this, Control3DEventHandler_t2392187186 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Control::OnValueChanged(VRTK.Control3DEventArgs)
extern "C"  void VRTK_Control_OnValueChanged_m1517031663 (VRTK_Control_t651619021 * __this, Control3DEventArgs_t4095025701  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_Control::GetValue()
extern "C"  float VRTK_Control_GetValue_m1253048790 (VRTK_Control_t651619021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_Control::GetNormalizedValue()
extern "C"  float VRTK_Control_GetNormalizedValue_m300209495 (VRTK_Control_t651619021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Control::SetContent(UnityEngine.GameObject,System.Boolean)
extern "C"  void VRTK_Control_SetContent_m3139211473 (VRTK_Control_t651619021 * __this, GameObject_t1756533147 * ___content0, bool ___hideContent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_Control::GetContent()
extern "C"  GameObject_t1756533147 * VRTK_Control_GetContent_m3222998537 (VRTK_Control_t651619021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Control::Awake()
extern "C"  void VRTK_Control_Awake_m3353402466 (VRTK_Control_t651619021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Control::Update()
extern "C"  void VRTK_Control_Update_m2815376212 (VRTK_Control_t651619021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.Control3DEventArgs VRTK.VRTK_Control::SetControlEvent()
extern "C"  Control3DEventArgs_t4095025701  VRTK_Control_SetControlEvent_m3279893869 (VRTK_Control_t651619021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Control::OnDrawGizmos()
extern "C"  void VRTK_Control_OnDrawGizmos_m3863527877 (VRTK_Control_t651619021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Control::CreateTriggerVolume()
extern "C"  void VRTK_Control_CreateTriggerVolume_m2638558941 (VRTK_Control_t651619021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_Control::GetThirdDirection(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  VRTK_Control_GetThirdDirection_m3123742211 (VRTK_Control_t651619021 * __this, Vector3_t2243707580  ___axis10, Vector3_t2243707580  ___axis21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Control::HandleInteractables()
extern "C"  void VRTK_Control_HandleInteractables_m595105508 (VRTK_Control_t651619021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Control::.cctor()
extern "C"  void VRTK_Control__cctor_m1372074304 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
