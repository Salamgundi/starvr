﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetOverlayTexture
struct _SetOverlayTexture_t2238656700;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_Texture_t3277130850.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetOverlayTexture::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayTexture__ctor_m2069476867 (_SetOverlayTexture_t2238656700 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTexture::Invoke(System.UInt64,Valve.VR.Texture_t&)
extern "C"  int32_t _SetOverlayTexture_Invoke_m1888514032 (_SetOverlayTexture_t2238656700 * __this, uint64_t ___ulOverlayHandle0, Texture_t_t3277130850 * ___pTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayTexture::BeginInvoke(System.UInt64,Valve.VR.Texture_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetOverlayTexture_BeginInvoke_m2576935409 (_SetOverlayTexture_t2238656700 * __this, uint64_t ___ulOverlayHandle0, Texture_t_t3277130850 * ___pTexture1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTexture::EndInvoke(Valve.VR.Texture_t&,System.IAsyncResult)
extern "C"  int32_t _SetOverlayTexture_EndInvoke_m4001221015 (_SetOverlayTexture_t2238656700 * __this, Texture_t_t3277130850 * ___pTexture0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
