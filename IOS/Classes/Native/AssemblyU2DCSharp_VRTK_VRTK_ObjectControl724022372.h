﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.ObjectControlEventHandler
struct ObjectControlEventHandler_t1756898952;
// VRTK.VRTK_BodyPhysics
struct VRTK_BodyPhysics_t3414085265;
// VRTK.VRTK_ObjectControl
struct VRTK_ObjectControl_t724022372;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectControl_Direction626792020.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ObjectControl
struct  VRTK_ObjectControl_t724022372  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_ControllerEvents VRTK.VRTK_ObjectControl::controller
	VRTK_ControllerEvents_t3225224819 * ___controller_2;
	// VRTK.VRTK_ObjectControl/DirectionDevices VRTK.VRTK_ObjectControl::deviceForDirection
	int32_t ___deviceForDirection_3;
	// System.Boolean VRTK.VRTK_ObjectControl::disableOtherControlsOnActive
	bool ___disableOtherControlsOnActive_4;
	// System.Boolean VRTK.VRTK_ObjectControl::affectOnFalling
	bool ___affectOnFalling_5;
	// UnityEngine.GameObject VRTK.VRTK_ObjectControl::controlOverrideObject
	GameObject_t1756533147 * ___controlOverrideObject_6;
	// VRTK.ObjectControlEventHandler VRTK.VRTK_ObjectControl::XAxisChanged
	ObjectControlEventHandler_t1756898952 * ___XAxisChanged_7;
	// VRTK.ObjectControlEventHandler VRTK.VRTK_ObjectControl::YAxisChanged
	ObjectControlEventHandler_t1756898952 * ___YAxisChanged_8;
	// VRTK.VRTK_ControllerEvents VRTK.VRTK_ObjectControl::controllerEvents
	VRTK_ControllerEvents_t3225224819 * ___controllerEvents_9;
	// VRTK.VRTK_BodyPhysics VRTK.VRTK_ObjectControl::bodyPhysics
	VRTK_BodyPhysics_t3414085265 * ___bodyPhysics_10;
	// VRTK.VRTK_ObjectControl VRTK.VRTK_ObjectControl::otherObjectControl
	VRTK_ObjectControl_t724022372 * ___otherObjectControl_11;
	// UnityEngine.GameObject VRTK.VRTK_ObjectControl::controlledGameObject
	GameObject_t1756533147 * ___controlledGameObject_12;
	// UnityEngine.GameObject VRTK.VRTK_ObjectControl::setControlOverrideObject
	GameObject_t1756533147 * ___setControlOverrideObject_13;
	// UnityEngine.Transform VRTK.VRTK_ObjectControl::directionDevice
	Transform_t3275118058 * ___directionDevice_14;
	// VRTK.VRTK_ObjectControl/DirectionDevices VRTK.VRTK_ObjectControl::previousDeviceForDirection
	int32_t ___previousDeviceForDirection_15;
	// UnityEngine.Vector2 VRTK.VRTK_ObjectControl::currentAxis
	Vector2_t2243707579  ___currentAxis_16;
	// UnityEngine.Vector2 VRTK.VRTK_ObjectControl::storedAxis
	Vector2_t2243707579  ___storedAxis_17;
	// System.Boolean VRTK.VRTK_ObjectControl::currentlyFalling
	bool ___currentlyFalling_18;
	// System.Boolean VRTK.VRTK_ObjectControl::modifierActive
	bool ___modifierActive_19;
	// System.Single VRTK.VRTK_ObjectControl::controlledGameObjectPreviousY
	float ___controlledGameObjectPreviousY_20;
	// System.Single VRTK.VRTK_ObjectControl::controlledGameObjectPreviousYOffset
	float ___controlledGameObjectPreviousYOffset_21;

public:
	inline static int32_t get_offset_of_controller_2() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___controller_2)); }
	inline VRTK_ControllerEvents_t3225224819 * get_controller_2() const { return ___controller_2; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_controller_2() { return &___controller_2; }
	inline void set_controller_2(VRTK_ControllerEvents_t3225224819 * value)
	{
		___controller_2 = value;
		Il2CppCodeGenWriteBarrier(&___controller_2, value);
	}

	inline static int32_t get_offset_of_deviceForDirection_3() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___deviceForDirection_3)); }
	inline int32_t get_deviceForDirection_3() const { return ___deviceForDirection_3; }
	inline int32_t* get_address_of_deviceForDirection_3() { return &___deviceForDirection_3; }
	inline void set_deviceForDirection_3(int32_t value)
	{
		___deviceForDirection_3 = value;
	}

	inline static int32_t get_offset_of_disableOtherControlsOnActive_4() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___disableOtherControlsOnActive_4)); }
	inline bool get_disableOtherControlsOnActive_4() const { return ___disableOtherControlsOnActive_4; }
	inline bool* get_address_of_disableOtherControlsOnActive_4() { return &___disableOtherControlsOnActive_4; }
	inline void set_disableOtherControlsOnActive_4(bool value)
	{
		___disableOtherControlsOnActive_4 = value;
	}

	inline static int32_t get_offset_of_affectOnFalling_5() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___affectOnFalling_5)); }
	inline bool get_affectOnFalling_5() const { return ___affectOnFalling_5; }
	inline bool* get_address_of_affectOnFalling_5() { return &___affectOnFalling_5; }
	inline void set_affectOnFalling_5(bool value)
	{
		___affectOnFalling_5 = value;
	}

	inline static int32_t get_offset_of_controlOverrideObject_6() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___controlOverrideObject_6)); }
	inline GameObject_t1756533147 * get_controlOverrideObject_6() const { return ___controlOverrideObject_6; }
	inline GameObject_t1756533147 ** get_address_of_controlOverrideObject_6() { return &___controlOverrideObject_6; }
	inline void set_controlOverrideObject_6(GameObject_t1756533147 * value)
	{
		___controlOverrideObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___controlOverrideObject_6, value);
	}

	inline static int32_t get_offset_of_XAxisChanged_7() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___XAxisChanged_7)); }
	inline ObjectControlEventHandler_t1756898952 * get_XAxisChanged_7() const { return ___XAxisChanged_7; }
	inline ObjectControlEventHandler_t1756898952 ** get_address_of_XAxisChanged_7() { return &___XAxisChanged_7; }
	inline void set_XAxisChanged_7(ObjectControlEventHandler_t1756898952 * value)
	{
		___XAxisChanged_7 = value;
		Il2CppCodeGenWriteBarrier(&___XAxisChanged_7, value);
	}

	inline static int32_t get_offset_of_YAxisChanged_8() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___YAxisChanged_8)); }
	inline ObjectControlEventHandler_t1756898952 * get_YAxisChanged_8() const { return ___YAxisChanged_8; }
	inline ObjectControlEventHandler_t1756898952 ** get_address_of_YAxisChanged_8() { return &___YAxisChanged_8; }
	inline void set_YAxisChanged_8(ObjectControlEventHandler_t1756898952 * value)
	{
		___YAxisChanged_8 = value;
		Il2CppCodeGenWriteBarrier(&___YAxisChanged_8, value);
	}

	inline static int32_t get_offset_of_controllerEvents_9() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___controllerEvents_9)); }
	inline VRTK_ControllerEvents_t3225224819 * get_controllerEvents_9() const { return ___controllerEvents_9; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_controllerEvents_9() { return &___controllerEvents_9; }
	inline void set_controllerEvents_9(VRTK_ControllerEvents_t3225224819 * value)
	{
		___controllerEvents_9 = value;
		Il2CppCodeGenWriteBarrier(&___controllerEvents_9, value);
	}

	inline static int32_t get_offset_of_bodyPhysics_10() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___bodyPhysics_10)); }
	inline VRTK_BodyPhysics_t3414085265 * get_bodyPhysics_10() const { return ___bodyPhysics_10; }
	inline VRTK_BodyPhysics_t3414085265 ** get_address_of_bodyPhysics_10() { return &___bodyPhysics_10; }
	inline void set_bodyPhysics_10(VRTK_BodyPhysics_t3414085265 * value)
	{
		___bodyPhysics_10 = value;
		Il2CppCodeGenWriteBarrier(&___bodyPhysics_10, value);
	}

	inline static int32_t get_offset_of_otherObjectControl_11() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___otherObjectControl_11)); }
	inline VRTK_ObjectControl_t724022372 * get_otherObjectControl_11() const { return ___otherObjectControl_11; }
	inline VRTK_ObjectControl_t724022372 ** get_address_of_otherObjectControl_11() { return &___otherObjectControl_11; }
	inline void set_otherObjectControl_11(VRTK_ObjectControl_t724022372 * value)
	{
		___otherObjectControl_11 = value;
		Il2CppCodeGenWriteBarrier(&___otherObjectControl_11, value);
	}

	inline static int32_t get_offset_of_controlledGameObject_12() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___controlledGameObject_12)); }
	inline GameObject_t1756533147 * get_controlledGameObject_12() const { return ___controlledGameObject_12; }
	inline GameObject_t1756533147 ** get_address_of_controlledGameObject_12() { return &___controlledGameObject_12; }
	inline void set_controlledGameObject_12(GameObject_t1756533147 * value)
	{
		___controlledGameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___controlledGameObject_12, value);
	}

	inline static int32_t get_offset_of_setControlOverrideObject_13() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___setControlOverrideObject_13)); }
	inline GameObject_t1756533147 * get_setControlOverrideObject_13() const { return ___setControlOverrideObject_13; }
	inline GameObject_t1756533147 ** get_address_of_setControlOverrideObject_13() { return &___setControlOverrideObject_13; }
	inline void set_setControlOverrideObject_13(GameObject_t1756533147 * value)
	{
		___setControlOverrideObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___setControlOverrideObject_13, value);
	}

	inline static int32_t get_offset_of_directionDevice_14() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___directionDevice_14)); }
	inline Transform_t3275118058 * get_directionDevice_14() const { return ___directionDevice_14; }
	inline Transform_t3275118058 ** get_address_of_directionDevice_14() { return &___directionDevice_14; }
	inline void set_directionDevice_14(Transform_t3275118058 * value)
	{
		___directionDevice_14 = value;
		Il2CppCodeGenWriteBarrier(&___directionDevice_14, value);
	}

	inline static int32_t get_offset_of_previousDeviceForDirection_15() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___previousDeviceForDirection_15)); }
	inline int32_t get_previousDeviceForDirection_15() const { return ___previousDeviceForDirection_15; }
	inline int32_t* get_address_of_previousDeviceForDirection_15() { return &___previousDeviceForDirection_15; }
	inline void set_previousDeviceForDirection_15(int32_t value)
	{
		___previousDeviceForDirection_15 = value;
	}

	inline static int32_t get_offset_of_currentAxis_16() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___currentAxis_16)); }
	inline Vector2_t2243707579  get_currentAxis_16() const { return ___currentAxis_16; }
	inline Vector2_t2243707579 * get_address_of_currentAxis_16() { return &___currentAxis_16; }
	inline void set_currentAxis_16(Vector2_t2243707579  value)
	{
		___currentAxis_16 = value;
	}

	inline static int32_t get_offset_of_storedAxis_17() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___storedAxis_17)); }
	inline Vector2_t2243707579  get_storedAxis_17() const { return ___storedAxis_17; }
	inline Vector2_t2243707579 * get_address_of_storedAxis_17() { return &___storedAxis_17; }
	inline void set_storedAxis_17(Vector2_t2243707579  value)
	{
		___storedAxis_17 = value;
	}

	inline static int32_t get_offset_of_currentlyFalling_18() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___currentlyFalling_18)); }
	inline bool get_currentlyFalling_18() const { return ___currentlyFalling_18; }
	inline bool* get_address_of_currentlyFalling_18() { return &___currentlyFalling_18; }
	inline void set_currentlyFalling_18(bool value)
	{
		___currentlyFalling_18 = value;
	}

	inline static int32_t get_offset_of_modifierActive_19() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___modifierActive_19)); }
	inline bool get_modifierActive_19() const { return ___modifierActive_19; }
	inline bool* get_address_of_modifierActive_19() { return &___modifierActive_19; }
	inline void set_modifierActive_19(bool value)
	{
		___modifierActive_19 = value;
	}

	inline static int32_t get_offset_of_controlledGameObjectPreviousY_20() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___controlledGameObjectPreviousY_20)); }
	inline float get_controlledGameObjectPreviousY_20() const { return ___controlledGameObjectPreviousY_20; }
	inline float* get_address_of_controlledGameObjectPreviousY_20() { return &___controlledGameObjectPreviousY_20; }
	inline void set_controlledGameObjectPreviousY_20(float value)
	{
		___controlledGameObjectPreviousY_20 = value;
	}

	inline static int32_t get_offset_of_controlledGameObjectPreviousYOffset_21() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_t724022372, ___controlledGameObjectPreviousYOffset_21)); }
	inline float get_controlledGameObjectPreviousYOffset_21() const { return ___controlledGameObjectPreviousYOffset_21; }
	inline float* get_address_of_controlledGameObjectPreviousYOffset_21() { return &___controlledGameObjectPreviousYOffset_21; }
	inline void set_controlledGameObjectPreviousYOffset_21(float value)
	{
		___controlledGameObjectPreviousYOffset_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
