﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_FadeToColor
struct _FadeToColor_t4129572280;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_FadeToColor::.ctor(System.Object,System.IntPtr)
extern "C"  void _FadeToColor__ctor_m2579795111 (_FadeToColor_t4129572280 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_FadeToColor::Invoke(System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C"  void _FadeToColor_Invoke_m2779999805 (_FadeToColor_t4129572280 * __this, float ___fSeconds0, float ___fRed1, float ___fGreen2, float ___fBlue3, float ___fAlpha4, bool ___bBackground5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_FadeToColor::BeginInvoke(System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _FadeToColor_BeginInvoke_m164301960 (_FadeToColor_t4129572280 * __this, float ___fSeconds0, float ___fRed1, float ___fGreen2, float ___fBlue3, float ___fAlpha4, bool ___bBackground5, AsyncCallback_t163412349 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_FadeToColor::EndInvoke(System.IAsyncResult)
extern "C"  void _FadeToColor_EndInvoke_m3009580457 (_FadeToColor_t4129572280 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
