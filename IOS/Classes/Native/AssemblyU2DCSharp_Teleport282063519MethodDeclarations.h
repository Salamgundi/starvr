﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Teleport
struct Teleport_t282063519;

#include "codegen/il2cpp-codegen.h"

// System.Void Teleport::.ctor()
extern "C"  void Teleport__ctor_m3962398974 (Teleport_t282063519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Teleport::Start()
extern "C"  void Teleport_Start_m3577123338 (Teleport_t282063519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Teleport::SetGazedAt(System.Boolean)
extern "C"  void Teleport_SetGazedAt_m3211557439 (Teleport_t282063519 * __this, bool ___gazedAt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Teleport::Reset()
extern "C"  void Teleport_Reset_m3778424687 (Teleport_t282063519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Teleport::TeleportRandomly()
extern "C"  void Teleport_TeleportRandomly_m608180529 (Teleport_t282063519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
