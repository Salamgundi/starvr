﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Event`3<UnityEngine.Color,System.Single,System.Boolean>
struct Event_3_t29453044;
// UnityEngine.Events.UnityAction`3<UnityEngine.Color,System.Single,System.Boolean>
struct UnityAction_3_t1816056522;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void SteamVR_Events/Event`3<UnityEngine.Color,System.Single,System.Boolean>::.ctor()
extern "C"  void Event_3__ctor_m3097877155_gshared (Event_3_t29453044 * __this, const MethodInfo* method);
#define Event_3__ctor_m3097877155(__this, method) ((  void (*) (Event_3_t29453044 *, const MethodInfo*))Event_3__ctor_m3097877155_gshared)(__this, method)
// System.Void SteamVR_Events/Event`3<UnityEngine.Color,System.Single,System.Boolean>::Listen(UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void Event_3_Listen_m3063479851_gshared (Event_3_t29453044 * __this, UnityAction_3_t1816056522 * ___action0, const MethodInfo* method);
#define Event_3_Listen_m3063479851(__this, ___action0, method) ((  void (*) (Event_3_t29453044 *, UnityAction_3_t1816056522 *, const MethodInfo*))Event_3_Listen_m3063479851_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`3<UnityEngine.Color,System.Single,System.Boolean>::Remove(UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void Event_3_Remove_m2106404318_gshared (Event_3_t29453044 * __this, UnityAction_3_t1816056522 * ___action0, const MethodInfo* method);
#define Event_3_Remove_m2106404318(__this, ___action0, method) ((  void (*) (Event_3_t29453044 *, UnityAction_3_t1816056522 *, const MethodInfo*))Event_3_Remove_m2106404318_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`3<UnityEngine.Color,System.Single,System.Boolean>::Send(T0,T1,T2)
extern "C"  void Event_3_Send_m1882481666_gshared (Event_3_t29453044 * __this, Color_t2020392075  ___arg00, float ___arg11, bool ___arg22, const MethodInfo* method);
#define Event_3_Send_m1882481666(__this, ___arg00, ___arg11, ___arg22, method) ((  void (*) (Event_3_t29453044 *, Color_t2020392075 , float, bool, const MethodInfo*))Event_3_Send_m1882481666_gshared)(__this, ___arg00, ___arg11, ___arg22, method)
