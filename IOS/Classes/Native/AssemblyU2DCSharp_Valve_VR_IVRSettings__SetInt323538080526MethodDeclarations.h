﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSettings/_SetInt32
struct _SetInt32_t3538080526;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRSettingsError4124928198.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSettings/_SetInt32::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetInt32__ctor_m90039639 (_SetInt32_t3538080526 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSettings/_SetInt32::Invoke(System.String,System.String,System.Int32,Valve.VR.EVRSettingsError&)
extern "C"  void _SetInt32_Invoke_m1833108778 (_SetInt32_t3538080526 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t ___nValue2, int32_t* ___peError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSettings/_SetInt32::BeginInvoke(System.String,System.String,System.Int32,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetInt32_BeginInvoke_m3820684659 (_SetInt32_t3538080526 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t ___nValue2, int32_t* ___peError3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSettings/_SetInt32::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  void _SetInt32_EndInvoke_m3072726115 (_SetInt32_t3538080526 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
