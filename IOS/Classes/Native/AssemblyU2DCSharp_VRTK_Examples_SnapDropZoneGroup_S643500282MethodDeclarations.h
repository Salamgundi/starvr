﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.SnapDropZoneGroup_Switcher
struct SnapDropZoneGroup_Switcher_t643500282;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_SnapDropZoneEventArgs418702774.h"

// System.Void VRTK.Examples.SnapDropZoneGroup_Switcher::.ctor()
extern "C"  void SnapDropZoneGroup_Switcher__ctor_m3152440785 (SnapDropZoneGroup_Switcher_t643500282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.SnapDropZoneGroup_Switcher::Start()
extern "C"  void SnapDropZoneGroup_Switcher_Start_m52739865 (SnapDropZoneGroup_Switcher_t643500282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.SnapDropZoneGroup_Switcher::DoCubeZoneSnapped(System.Object,VRTK.SnapDropZoneEventArgs)
extern "C"  void SnapDropZoneGroup_Switcher_DoCubeZoneSnapped_m911949647 (SnapDropZoneGroup_Switcher_t643500282 * __this, Il2CppObject * ___sender0, SnapDropZoneEventArgs_t418702774  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.SnapDropZoneGroup_Switcher::DoCubeZoneUnsnapped(System.Object,VRTK.SnapDropZoneEventArgs)
extern "C"  void SnapDropZoneGroup_Switcher_DoCubeZoneUnsnapped_m3175416602 (SnapDropZoneGroup_Switcher_t643500282 * __this, Il2CppObject * ___sender0, SnapDropZoneEventArgs_t418702774  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.SnapDropZoneGroup_Switcher::DoSphereZoneSnapped(System.Object,VRTK.SnapDropZoneEventArgs)
extern "C"  void SnapDropZoneGroup_Switcher_DoSphereZoneSnapped_m2819549975 (SnapDropZoneGroup_Switcher_t643500282 * __this, Il2CppObject * ___sender0, SnapDropZoneEventArgs_t418702774  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.SnapDropZoneGroup_Switcher::DoSphereZoneUnsnapped(System.Object,VRTK.SnapDropZoneEventArgs)
extern "C"  void SnapDropZoneGroup_Switcher_DoSphereZoneUnsnapped_m57498210 (SnapDropZoneGroup_Switcher_t643500282 * __this, Il2CppObject * ___sender0, SnapDropZoneEventArgs_t418702774  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
