﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Teleporter
struct SteamVR_Teleporter_t4248086861;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ClickedEventArgs2917034410.h"

// System.Void SteamVR_Teleporter::.ctor()
extern "C"  void SteamVR_Teleporter__ctor_m464502804 (SteamVR_Teleporter_t4248086861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform SteamVR_Teleporter::get_reference()
extern "C"  Transform_t3275118058 * SteamVR_Teleporter_get_reference_m1466046380 (SteamVR_Teleporter_t4248086861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Teleporter::Start()
extern "C"  void SteamVR_Teleporter_Start_m2776289804 (SteamVR_Teleporter_t4248086861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Teleporter::DoClick(System.Object,ClickedEventArgs)
extern "C"  void SteamVR_Teleporter_DoClick_m3587697943 (SteamVR_Teleporter_t4248086861 * __this, Il2CppObject * ___sender0, ClickedEventArgs_t2917034410  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
