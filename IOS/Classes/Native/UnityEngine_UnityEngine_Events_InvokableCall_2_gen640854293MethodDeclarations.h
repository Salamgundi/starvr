﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`2<System.Object,System.Boolean>
struct InvokableCall_2_t640854293;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`2<System.Object,System.Boolean>
struct UnityAction_2_t626063409;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m1806504405_gshared (InvokableCall_2_t640854293 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_2__ctor_m1806504405(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_2_t640854293 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2__ctor_m1806504405_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Boolean>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m2433638040_gshared (InvokableCall_2_t640854293 * __this, UnityAction_2_t626063409 * ___action0, const MethodInfo* method);
#define InvokableCall_2__ctor_m2433638040(__this, ___action0, method) ((  void (*) (InvokableCall_2_t640854293 *, UnityAction_2_t626063409 *, const MethodInfo*))InvokableCall_2__ctor_m2433638040_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Boolean>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m22451329_gshared (InvokableCall_2_t640854293 * __this, UnityAction_2_t626063409 * ___value0, const MethodInfo* method);
#define InvokableCall_2_add_Delegate_m22451329(__this, ___value0, method) ((  void (*) (InvokableCall_2_t640854293 *, UnityAction_2_t626063409 *, const MethodInfo*))InvokableCall_2_add_Delegate_m22451329_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Boolean>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m1464363114_gshared (InvokableCall_2_t640854293 * __this, UnityAction_2_t626063409 * ___value0, const MethodInfo* method);
#define InvokableCall_2_remove_Delegate_m1464363114(__this, ___value0, method) ((  void (*) (InvokableCall_2_t640854293 *, UnityAction_2_t626063409 *, const MethodInfo*))InvokableCall_2_remove_Delegate_m1464363114_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Boolean>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m849933656_gshared (InvokableCall_2_t640854293 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_2_Invoke_m849933656(__this, ___args0, method) ((  void (*) (InvokableCall_2_t640854293 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_2_Invoke_m849933656_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m1151619372_gshared (InvokableCall_2_t640854293 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_2_Find_m1151619372(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_2_t640854293 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2_Find_m1151619372_gshared)(__this, ___targetObj0, ___method1, method)
