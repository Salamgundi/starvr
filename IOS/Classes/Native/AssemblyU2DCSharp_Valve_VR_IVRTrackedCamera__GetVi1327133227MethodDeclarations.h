﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureD3D11
struct _GetVideoStreamTextureD3D11_t1327133227;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraError3529690400.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraFrameTy2003723073.h"
#include "AssemblyU2DCSharp_Valve_VR_CameraVideoStreamFrameHe968213647.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureD3D11::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetVideoStreamTextureD3D11__ctor_m2997847626 (_GetVideoStreamTextureD3D11_t1327133227 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureD3D11::Invoke(System.UInt64,Valve.VR.EVRTrackedCameraFrameType,System.IntPtr,System.IntPtr&,Valve.VR.CameraVideoStreamFrameHeader_t&,System.UInt32)
extern "C"  int32_t _GetVideoStreamTextureD3D11_Invoke_m2333468652 (_GetVideoStreamTextureD3D11_t1327133227 * __this, uint64_t ___hTrackedCamera0, int32_t ___eFrameType1, IntPtr_t ___pD3D11DeviceOrResource2, IntPtr_t* ___ppD3D11ShaderResourceView3, CameraVideoStreamFrameHeader_t_t968213647 * ___pFrameHeader4, uint32_t ___nFrameHeaderSize5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureD3D11::BeginInvoke(System.UInt64,Valve.VR.EVRTrackedCameraFrameType,System.IntPtr,System.IntPtr&,Valve.VR.CameraVideoStreamFrameHeader_t&,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetVideoStreamTextureD3D11_BeginInvoke_m3389778092 (_GetVideoStreamTextureD3D11_t1327133227 * __this, uint64_t ___hTrackedCamera0, int32_t ___eFrameType1, IntPtr_t ___pD3D11DeviceOrResource2, IntPtr_t* ___ppD3D11ShaderResourceView3, CameraVideoStreamFrameHeader_t_t968213647 * ___pFrameHeader4, uint32_t ___nFrameHeaderSize5, AsyncCallback_t163412349 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_GetVideoStreamTextureD3D11::EndInvoke(System.IntPtr&,Valve.VR.CameraVideoStreamFrameHeader_t&,System.IAsyncResult)
extern "C"  int32_t _GetVideoStreamTextureD3D11_EndInvoke_m3261966874 (_GetVideoStreamTextureD3D11_t1327133227 * __this, IntPtr_t* ___ppD3D11ShaderResourceView0, CameraVideoStreamFrameHeader_t_t968213647 * ___pFrameHeader1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
