﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetGamepadFocusOverlay
struct _GetGamepadFocusOverlay_t300021750;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetGamepadFocusOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetGamepadFocusOverlay__ctor_m1129734351 (_GetGamepadFocusOverlay_t300021750 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.IVROverlay/_GetGamepadFocusOverlay::Invoke()
extern "C"  uint64_t _GetGamepadFocusOverlay_Invoke_m724604195 (_GetGamepadFocusOverlay_t300021750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetGamepadFocusOverlay::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetGamepadFocusOverlay_BeginInvoke_m2262006616 (_GetGamepadFocusOverlay_t300021750 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.IVROverlay/_GetGamepadFocusOverlay::EndInvoke(System.IAsyncResult)
extern "C"  uint64_t _GetGamepadFocusOverlay_EndInvoke_m1079529677 (_GetGamepadFocusOverlay_t300021750 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
