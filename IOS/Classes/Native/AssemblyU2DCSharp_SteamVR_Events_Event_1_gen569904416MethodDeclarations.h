﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Event`1<System.Object>
struct Event_1_t569904416;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t4056035046;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void SteamVR_Events/Event`1<System.Object>::.ctor()
extern "C"  void Event_1__ctor_m28252523_gshared (Event_1_t569904416 * __this, const MethodInfo* method);
#define Event_1__ctor_m28252523(__this, method) ((  void (*) (Event_1_t569904416 *, const MethodInfo*))Event_1__ctor_m28252523_gshared)(__this, method)
// System.Void SteamVR_Events/Event`1<System.Object>::Listen(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Listen_m1961284276_gshared (Event_1_t569904416 * __this, UnityAction_1_t4056035046 * ___action0, const MethodInfo* method);
#define Event_1_Listen_m1961284276(__this, ___action0, method) ((  void (*) (Event_1_t569904416 *, UnityAction_1_t4056035046 *, const MethodInfo*))Event_1_Listen_m1961284276_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`1<System.Object>::Remove(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Remove_m1960904225_gshared (Event_1_t569904416 * __this, UnityAction_1_t4056035046 * ___action0, const MethodInfo* method);
#define Event_1_Remove_m1960904225(__this, ___action0, method) ((  void (*) (Event_1_t569904416 *, UnityAction_1_t4056035046 *, const MethodInfo*))Event_1_Remove_m1960904225_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`1<System.Object>::Send(T)
extern "C"  void Event_1_Send_m2836426603_gshared (Event_1_t569904416 * __this, Il2CppObject * ___arg00, const MethodInfo* method);
#define Event_1_Send_m2836426603(__this, ___arg00, method) ((  void (*) (Event_1_t569904416 *, Il2CppObject *, const MethodInfo*))Event_1_Send_m2836426603_gshared)(__this, ___arg00, method)
