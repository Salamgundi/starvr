﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_SnapDropZone
struct VRTK_SnapDropZone_t1948041105;
// VRTK.SnapDropZoneEventHandler
struct SnapDropZoneEventHandler_t172258073;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Collider
struct Collider_t3497673348;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Component
struct Component_t3819376471;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_SnapDropZoneEventHandler172258073.h"
#include "AssemblyU2DCSharp_VRTK_SnapDropZoneEventArgs418702774.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"

// System.Void VRTK.VRTK_SnapDropZone::.ctor()
extern "C"  void VRTK_SnapDropZone__ctor_m2746481229 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::add_ObjectEnteredSnapDropZone(VRTK.SnapDropZoneEventHandler)
extern "C"  void VRTK_SnapDropZone_add_ObjectEnteredSnapDropZone_m1497037360 (VRTK_SnapDropZone_t1948041105 * __this, SnapDropZoneEventHandler_t172258073 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::remove_ObjectEnteredSnapDropZone(VRTK.SnapDropZoneEventHandler)
extern "C"  void VRTK_SnapDropZone_remove_ObjectEnteredSnapDropZone_m219389521 (VRTK_SnapDropZone_t1948041105 * __this, SnapDropZoneEventHandler_t172258073 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::add_ObjectExitedSnapDropZone(VRTK.SnapDropZoneEventHandler)
extern "C"  void VRTK_SnapDropZone_add_ObjectExitedSnapDropZone_m2496113416 (VRTK_SnapDropZone_t1948041105 * __this, SnapDropZoneEventHandler_t172258073 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::remove_ObjectExitedSnapDropZone(VRTK.SnapDropZoneEventHandler)
extern "C"  void VRTK_SnapDropZone_remove_ObjectExitedSnapDropZone_m1849217061 (VRTK_SnapDropZone_t1948041105 * __this, SnapDropZoneEventHandler_t172258073 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::add_ObjectSnappedToDropZone(VRTK.SnapDropZoneEventHandler)
extern "C"  void VRTK_SnapDropZone_add_ObjectSnappedToDropZone_m2909826357 (VRTK_SnapDropZone_t1948041105 * __this, SnapDropZoneEventHandler_t172258073 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::remove_ObjectSnappedToDropZone(VRTK.SnapDropZoneEventHandler)
extern "C"  void VRTK_SnapDropZone_remove_ObjectSnappedToDropZone_m1440807444 (VRTK_SnapDropZone_t1948041105 * __this, SnapDropZoneEventHandler_t172258073 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::add_ObjectUnsnappedFromDropZone(VRTK.SnapDropZoneEventHandler)
extern "C"  void VRTK_SnapDropZone_add_ObjectUnsnappedFromDropZone_m4272489251 (VRTK_SnapDropZone_t1948041105 * __this, SnapDropZoneEventHandler_t172258073 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::remove_ObjectUnsnappedFromDropZone(VRTK.SnapDropZoneEventHandler)
extern "C"  void VRTK_SnapDropZone_remove_ObjectUnsnappedFromDropZone_m1026859984 (VRTK_SnapDropZone_t1948041105 * __this, SnapDropZoneEventHandler_t172258073 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::OnObjectEnteredSnapDropZone(VRTK.SnapDropZoneEventArgs)
extern "C"  void VRTK_SnapDropZone_OnObjectEnteredSnapDropZone_m4242403298 (VRTK_SnapDropZone_t1948041105 * __this, SnapDropZoneEventArgs_t418702774  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::OnObjectExitedSnapDropZone(VRTK.SnapDropZoneEventArgs)
extern "C"  void VRTK_SnapDropZone_OnObjectExitedSnapDropZone_m1485206016 (VRTK_SnapDropZone_t1948041105 * __this, SnapDropZoneEventArgs_t418702774  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::OnObjectSnappedToDropZone(VRTK.SnapDropZoneEventArgs)
extern "C"  void VRTK_SnapDropZone_OnObjectSnappedToDropZone_m3314610193 (VRTK_SnapDropZone_t1948041105 * __this, SnapDropZoneEventArgs_t418702774  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::OnObjectUnsnappedFromDropZone(VRTK.SnapDropZoneEventArgs)
extern "C"  void VRTK_SnapDropZone_OnObjectUnsnappedFromDropZone_m276815023 (VRTK_SnapDropZone_t1948041105 * __this, SnapDropZoneEventArgs_t418702774  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.SnapDropZoneEventArgs VRTK.VRTK_SnapDropZone::SetSnapDropZoneEvent(UnityEngine.GameObject)
extern "C"  SnapDropZoneEventArgs_t418702774  VRTK_SnapDropZone_SetSnapDropZoneEvent_m2121155272 (VRTK_SnapDropZone_t1948041105 * __this, GameObject_t1756533147 * ___interactableObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::InitaliseHighlightObject(System.Boolean)
extern "C"  void VRTK_SnapDropZone_InitaliseHighlightObject_m2986383975 (VRTK_SnapDropZone_t1948041105 * __this, bool ___removeOldObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::ForceSnap(UnityEngine.GameObject)
extern "C"  void VRTK_SnapDropZone_ForceSnap_m4004088104 (VRTK_SnapDropZone_t1948041105 * __this, GameObject_t1756533147 * ___objectToSnap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::ForceUnsnap()
extern "C"  void VRTK_SnapDropZone_ForceUnsnap_m2693480211 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::Awake()
extern "C"  void VRTK_SnapDropZone_Awake_m2114528618 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::OnApplicationQuit()
extern "C"  void VRTK_SnapDropZone_OnApplicationQuit_m760073679 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::Update()
extern "C"  void VRTK_SnapDropZone_Update_m588818364 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void VRTK_SnapDropZone_OnTriggerEnter_m1725579985 (VRTK_SnapDropZone_t1948041105 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::OnTriggerExit(UnityEngine.Collider)
extern "C"  void VRTK_SnapDropZone_OnTriggerExit_m3106737041 (VRTK_SnapDropZone_t1948041105 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::OnTriggerStay(UnityEngine.Collider)
extern "C"  void VRTK_SnapDropZone_OnTriggerStay_m4047809038 (VRTK_SnapDropZone_t1948041105 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_InteractableObject VRTK.VRTK_SnapDropZone::ValidSnapObject(UnityEngine.GameObject,System.Boolean)
extern "C"  VRTK_InteractableObject_t2604188111 * VRTK_SnapDropZone_ValidSnapObject_m765544202 (VRTK_SnapDropZone_t1948041105 * __this, GameObject_t1756533147 * ___checkObject0, bool ___grabState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VRTK.VRTK_SnapDropZone::ObjectPath(System.String)
extern "C"  String_t* VRTK_SnapDropZone_ObjectPath_m898019566 (VRTK_SnapDropZone_t1948041105 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::CreateHighlightersInEditor()
extern "C"  void VRTK_SnapDropZone_CreateHighlightersInEditor_m535835223 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::CheckCurrentValidSnapObjectStillValid()
extern "C"  void VRTK_SnapDropZone_CheckCurrentValidSnapObjectStillValid_m710961805 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::ForceSetObjects()
extern "C"  void VRTK_SnapDropZone_ForceSetObjects_m2953512626 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::GenerateContainer()
extern "C"  void VRTK_SnapDropZone_GenerateContainer_m2378473381 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::SetContainer()
extern "C"  void VRTK_SnapDropZone_SetContainer_m1159602638 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::GenerateObjects()
extern "C"  void VRTK_SnapDropZone_GenerateObjects_m1725738900 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::SnapObject(UnityEngine.Collider)
extern "C"  void VRTK_SnapDropZone_SnapObject_m1928641513 (VRTK_SnapDropZone_t1948041105 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::UnsnapObject()
extern "C"  void VRTK_SnapDropZone_UnsnapObject_m3469354191 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_SnapDropZone::GetNewLocalScale(VRTK.VRTK_InteractableObject)
extern "C"  Vector3_t2243707580  VRTK_SnapDropZone_GetNewLocalScale_m1105477286 (VRTK_SnapDropZone_t1948041105 * __this, VRTK_InteractableObject_t2604188111 * ___ioCheck0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_SnapDropZone::UpdateTransformDimensions(VRTK.VRTK_InteractableObject,UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  Il2CppObject * VRTK_SnapDropZone_UpdateTransformDimensions_m69138013 (VRTK_SnapDropZone_t1948041105 * __this, VRTK_InteractableObject_t2604188111 * ___ioCheck0, GameObject_t1756533147 * ___endSettings1, Vector3_t2243707580  ___endScale2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::SetDropSnapType(VRTK.VRTK_InteractableObject)
extern "C"  void VRTK_SnapDropZone_SetDropSnapType_m3300677064 (VRTK_SnapDropZone_t1948041105 * __this, VRTK_InteractableObject_t2604188111 * ___ioCheck0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::SetSnapDropZoneJoint(UnityEngine.Rigidbody)
extern "C"  void VRTK_SnapDropZone_SetSnapDropZoneJoint_m3254889872 (VRTK_SnapDropZone_t1948041105 * __this, Rigidbody_t4233889191 * ___snapTo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::ResetSnapDropZoneJoint()
extern "C"  void VRTK_SnapDropZone_ResetSnapDropZoneJoint_m3872280417 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::AttemptForceSnap(UnityEngine.GameObject)
extern "C"  void VRTK_SnapDropZone_AttemptForceSnap_m2278651001 (VRTK_SnapDropZone_t1948041105 * __this, GameObject_t1756533147 * ___objectToSnap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_SnapDropZone::AttemptForceSnapAtEndOfFrame(UnityEngine.GameObject)
extern "C"  Il2CppObject * VRTK_SnapDropZone_AttemptForceSnapAtEndOfFrame_m3353186513 (VRTK_SnapDropZone_t1948041105 * __this, GameObject_t1756533147 * ___objectToSnap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::ToggleHighlight(UnityEngine.Collider,System.Boolean)
extern "C"  void VRTK_SnapDropZone_ToggleHighlight_m848553937 (VRTK_SnapDropZone_t1948041105 * __this, Collider_t3497673348 * ___collider0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::CopyObject(UnityEngine.GameObject,UnityEngine.GameObject&,System.String)
extern "C"  void VRTK_SnapDropZone_CopyObject_m2865133513 (VRTK_SnapDropZone_t1948041105 * __this, GameObject_t1756533147 * ___objectBlueprint0, GameObject_t1756533147 ** ___clonedObject1, String_t* ___name2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::GenerateHighlightObject()
extern "C"  void VRTK_SnapDropZone_GenerateHighlightObject_m4107374731 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::DeleteHighlightObject()
extern "C"  void VRTK_SnapDropZone_DeleteHighlightObject_m2957565301 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::GenerateEditorHighlightObject()
extern "C"  void VRTK_SnapDropZone_GenerateEditorHighlightObject_m3933199700 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::CleanHighlightObject(UnityEngine.GameObject)
extern "C"  void VRTK_SnapDropZone_CleanHighlightObject_m1028486221 (VRTK_SnapDropZone_t1948041105 * __this, GameObject_t1756533147 * ___objectToClean0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::InitialiseHighlighter()
extern "C"  void VRTK_SnapDropZone_InitialiseHighlighter_m22379763 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::ChooseDestroyType(UnityEngine.Transform)
extern "C"  void VRTK_SnapDropZone_ChooseDestroyType_m3658345193 (VRTK_SnapDropZone_t1948041105 * __this, Transform_t3275118058 * ___deleteTransform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::ChooseDestroyType(UnityEngine.GameObject)
extern "C"  void VRTK_SnapDropZone_ChooseDestroyType_m3610185442 (VRTK_SnapDropZone_t1948041105 * __this, GameObject_t1756533147 * ___deleteObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::ChooseDestroyType(UnityEngine.Component)
extern "C"  void VRTK_SnapDropZone_ChooseDestroyType_m3570025300 (VRTK_SnapDropZone_t1948041105 * __this, Component_t3819376471 * ___deleteComponent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SnapDropZone::OnDrawGizmosSelected()
extern "C"  void VRTK_SnapDropZone_OnDrawGizmosSelected_m2845998736 (VRTK_SnapDropZone_t1948041105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
