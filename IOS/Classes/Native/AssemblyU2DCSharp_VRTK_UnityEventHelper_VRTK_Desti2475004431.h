﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_DestinationMarker
struct VRTK_DestinationMarker_t667613644;
// VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t599160530;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents
struct  VRTK_DestinationMarker_UnityEvents_t2475004431  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_DestinationMarker VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents::dm
	VRTK_DestinationMarker_t667613644 * ___dm_2;
	// VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents::OnDestinationMarkerEnter
	UnityObjectEvent_t599160530 * ___OnDestinationMarkerEnter_3;
	// VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents::OnDestinationMarkerExit
	UnityObjectEvent_t599160530 * ___OnDestinationMarkerExit_4;
	// VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_DestinationMarker_UnityEvents::OnDestinationMarkerSet
	UnityObjectEvent_t599160530 * ___OnDestinationMarkerSet_5;

public:
	inline static int32_t get_offset_of_dm_2() { return static_cast<int32_t>(offsetof(VRTK_DestinationMarker_UnityEvents_t2475004431, ___dm_2)); }
	inline VRTK_DestinationMarker_t667613644 * get_dm_2() const { return ___dm_2; }
	inline VRTK_DestinationMarker_t667613644 ** get_address_of_dm_2() { return &___dm_2; }
	inline void set_dm_2(VRTK_DestinationMarker_t667613644 * value)
	{
		___dm_2 = value;
		Il2CppCodeGenWriteBarrier(&___dm_2, value);
	}

	inline static int32_t get_offset_of_OnDestinationMarkerEnter_3() { return static_cast<int32_t>(offsetof(VRTK_DestinationMarker_UnityEvents_t2475004431, ___OnDestinationMarkerEnter_3)); }
	inline UnityObjectEvent_t599160530 * get_OnDestinationMarkerEnter_3() const { return ___OnDestinationMarkerEnter_3; }
	inline UnityObjectEvent_t599160530 ** get_address_of_OnDestinationMarkerEnter_3() { return &___OnDestinationMarkerEnter_3; }
	inline void set_OnDestinationMarkerEnter_3(UnityObjectEvent_t599160530 * value)
	{
		___OnDestinationMarkerEnter_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnDestinationMarkerEnter_3, value);
	}

	inline static int32_t get_offset_of_OnDestinationMarkerExit_4() { return static_cast<int32_t>(offsetof(VRTK_DestinationMarker_UnityEvents_t2475004431, ___OnDestinationMarkerExit_4)); }
	inline UnityObjectEvent_t599160530 * get_OnDestinationMarkerExit_4() const { return ___OnDestinationMarkerExit_4; }
	inline UnityObjectEvent_t599160530 ** get_address_of_OnDestinationMarkerExit_4() { return &___OnDestinationMarkerExit_4; }
	inline void set_OnDestinationMarkerExit_4(UnityObjectEvent_t599160530 * value)
	{
		___OnDestinationMarkerExit_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnDestinationMarkerExit_4, value);
	}

	inline static int32_t get_offset_of_OnDestinationMarkerSet_5() { return static_cast<int32_t>(offsetof(VRTK_DestinationMarker_UnityEvents_t2475004431, ___OnDestinationMarkerSet_5)); }
	inline UnityObjectEvent_t599160530 * get_OnDestinationMarkerSet_5() const { return ___OnDestinationMarkerSet_5; }
	inline UnityObjectEvent_t599160530 ** get_address_of_OnDestinationMarkerSet_5() { return &___OnDestinationMarkerSet_5; }
	inline void set_OnDestinationMarkerSet_5(UnityObjectEvent_t599160530 * value)
	{
		___OnDestinationMarkerSet_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnDestinationMarkerSet_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
