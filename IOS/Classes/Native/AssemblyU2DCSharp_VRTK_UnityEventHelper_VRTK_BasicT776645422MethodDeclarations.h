﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents
struct VRTK_BasicTeleport_UnityEvents_t776645422;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventArgs1852451661.h"

// System.Void VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents::.ctor()
extern "C"  void VRTK_BasicTeleport_UnityEvents__ctor_m1070694665 (VRTK_BasicTeleport_UnityEvents_t776645422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents::SetBasicTeleport()
extern "C"  void VRTK_BasicTeleport_UnityEvents_SetBasicTeleport_m2239659114 (VRTK_BasicTeleport_UnityEvents_t776645422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents::OnEnable()
extern "C"  void VRTK_BasicTeleport_UnityEvents_OnEnable_m383523001 (VRTK_BasicTeleport_UnityEvents_t776645422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents::Teleporting(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_BasicTeleport_UnityEvents_Teleporting_m4285258836 (VRTK_BasicTeleport_UnityEvents_t776645422 * __this, Il2CppObject * ___o0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents::Teleported(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_BasicTeleport_UnityEvents_Teleported_m3685897007 (VRTK_BasicTeleport_UnityEvents_t776645422 * __this, Il2CppObject * ___o0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents::OnDisable()
extern "C"  void VRTK_BasicTeleport_UnityEvents_OnDisable_m1022495804 (VRTK_BasicTeleport_UnityEvents_t776645422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
