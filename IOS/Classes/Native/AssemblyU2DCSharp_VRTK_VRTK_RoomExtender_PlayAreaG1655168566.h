﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.VRTK_RoomExtender
struct VRTK_RoomExtender_t3041247552;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_RoomExtender_PlayAreaGizmo
struct  VRTK_RoomExtender_PlayAreaGizmo_t1655168566  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color VRTK.VRTK_RoomExtender_PlayAreaGizmo::color
	Color_t2020392075  ___color_2;
	// System.Single VRTK.VRTK_RoomExtender_PlayAreaGizmo::wireframeHeight
	float ___wireframeHeight_3;
	// System.Boolean VRTK.VRTK_RoomExtender_PlayAreaGizmo::drawWireframeWhenSelectedOnly
	bool ___drawWireframeWhenSelectedOnly_4;
	// UnityEngine.Transform VRTK.VRTK_RoomExtender_PlayAreaGizmo::playArea
	Transform_t3275118058 * ___playArea_5;
	// VRTK.VRTK_RoomExtender VRTK.VRTK_RoomExtender_PlayAreaGizmo::roomExtender
	VRTK_RoomExtender_t3041247552 * ___roomExtender_6;

public:
	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_PlayAreaGizmo_t1655168566, ___color_2)); }
	inline Color_t2020392075  get_color_2() const { return ___color_2; }
	inline Color_t2020392075 * get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(Color_t2020392075  value)
	{
		___color_2 = value;
	}

	inline static int32_t get_offset_of_wireframeHeight_3() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_PlayAreaGizmo_t1655168566, ___wireframeHeight_3)); }
	inline float get_wireframeHeight_3() const { return ___wireframeHeight_3; }
	inline float* get_address_of_wireframeHeight_3() { return &___wireframeHeight_3; }
	inline void set_wireframeHeight_3(float value)
	{
		___wireframeHeight_3 = value;
	}

	inline static int32_t get_offset_of_drawWireframeWhenSelectedOnly_4() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_PlayAreaGizmo_t1655168566, ___drawWireframeWhenSelectedOnly_4)); }
	inline bool get_drawWireframeWhenSelectedOnly_4() const { return ___drawWireframeWhenSelectedOnly_4; }
	inline bool* get_address_of_drawWireframeWhenSelectedOnly_4() { return &___drawWireframeWhenSelectedOnly_4; }
	inline void set_drawWireframeWhenSelectedOnly_4(bool value)
	{
		___drawWireframeWhenSelectedOnly_4 = value;
	}

	inline static int32_t get_offset_of_playArea_5() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_PlayAreaGizmo_t1655168566, ___playArea_5)); }
	inline Transform_t3275118058 * get_playArea_5() const { return ___playArea_5; }
	inline Transform_t3275118058 ** get_address_of_playArea_5() { return &___playArea_5; }
	inline void set_playArea_5(Transform_t3275118058 * value)
	{
		___playArea_5 = value;
		Il2CppCodeGenWriteBarrier(&___playArea_5, value);
	}

	inline static int32_t get_offset_of_roomExtender_6() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_PlayAreaGizmo_t1655168566, ___roomExtender_6)); }
	inline VRTK_RoomExtender_t3041247552 * get_roomExtender_6() const { return ___roomExtender_6; }
	inline VRTK_RoomExtender_t3041247552 ** get_address_of_roomExtender_6() { return &___roomExtender_6; }
	inline void set_roomExtender_6(VRTK_RoomExtender_t3041247552 * value)
	{
		___roomExtender_6 = value;
		Il2CppCodeGenWriteBarrier(&___roomExtender_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
