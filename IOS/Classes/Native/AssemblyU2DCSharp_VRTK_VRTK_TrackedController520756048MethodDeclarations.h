﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_TrackedController
struct VRTK_TrackedController_t520756048;
// VRTK.VRTKTrackedControllerEventHandler
struct VRTKTrackedControllerEventHandler_t2437916365;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTKTrackedControllerEventH2437916365.h"
#include "AssemblyU2DCSharp_VRTK_VRTKTrackedControllerEventA2407378264.h"

// System.Void VRTK.VRTK_TrackedController::.ctor()
extern "C"  void VRTK_TrackedController__ctor_m2801563792 (VRTK_TrackedController_t520756048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController::add_ControllerEnabled(VRTK.VRTKTrackedControllerEventHandler)
extern "C"  void VRTK_TrackedController_add_ControllerEnabled_m1420062455 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventHandler_t2437916365 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController::remove_ControllerEnabled(VRTK.VRTKTrackedControllerEventHandler)
extern "C"  void VRTK_TrackedController_remove_ControllerEnabled_m3397407694 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventHandler_t2437916365 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController::add_ControllerDisabled(VRTK.VRTKTrackedControllerEventHandler)
extern "C"  void VRTK_TrackedController_add_ControllerDisabled_m102111392 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventHandler_t2437916365 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController::remove_ControllerDisabled(VRTK.VRTKTrackedControllerEventHandler)
extern "C"  void VRTK_TrackedController_remove_ControllerDisabled_m3736811585 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventHandler_t2437916365 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController::add_ControllerIndexChanged(VRTK.VRTKTrackedControllerEventHandler)
extern "C"  void VRTK_TrackedController_add_ControllerIndexChanged_m3987335020 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventHandler_t2437916365 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController::remove_ControllerIndexChanged(VRTK.VRTKTrackedControllerEventHandler)
extern "C"  void VRTK_TrackedController_remove_ControllerIndexChanged_m779748417 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventHandler_t2437916365 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController::OnControllerEnabled(VRTK.VRTKTrackedControllerEventArgs)
extern "C"  void VRTK_TrackedController_OnControllerEnabled_m2710234991 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventArgs_t2407378264  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController::OnControllerDisabled(VRTK.VRTKTrackedControllerEventArgs)
extern "C"  void VRTK_TrackedController_OnControllerDisabled_m3012508802 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventArgs_t2407378264  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController::OnControllerIndexChanged(VRTK.VRTKTrackedControllerEventArgs)
extern "C"  void VRTK_TrackedController_OnControllerIndexChanged_m996213926 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventArgs_t2407378264  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTKTrackedControllerEventArgs VRTK.VRTK_TrackedController::SetEventPayload(System.UInt32)
extern "C"  VRTKTrackedControllerEventArgs_t2407378264  VRTK_TrackedController_SetEventPayload_m3983054276 (VRTK_TrackedController_t520756048 * __this, uint32_t ___previousIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController::OnEnable()
extern "C"  void VRTK_TrackedController_OnEnable_m1630703200 (VRTK_TrackedController_t520756048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController::OnDisable()
extern "C"  void VRTK_TrackedController_OnDisable_m2752210889 (VRTK_TrackedController_t520756048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController::Disable()
extern "C"  void VRTK_TrackedController_Disable_m3589670064 (VRTK_TrackedController_t520756048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedController::Update()
extern "C"  void VRTK_TrackedController_Update_m1693704765 (VRTK_TrackedController_t520756048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_TrackedController::Enable()
extern "C"  Il2CppObject * VRTK_TrackedController_Enable_m2953694943 (VRTK_TrackedController_t520756048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
