﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayMouseScale
struct _GetOverlayMouseScale_t2826364086;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdVector2_t2255225135.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayMouseScale::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayMouseScale__ctor_m638990725 (_GetOverlayMouseScale_t2826364086 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayMouseScale::Invoke(System.UInt64,Valve.VR.HmdVector2_t&)
extern "C"  int32_t _GetOverlayMouseScale_Invoke_m667374085 (_GetOverlayMouseScale_t2826364086 * __this, uint64_t ___ulOverlayHandle0, HmdVector2_t_t2255225135 * ___pvecMouseScale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayMouseScale::BeginInvoke(System.UInt64,Valve.VR.HmdVector2_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayMouseScale_BeginInvoke_m415897458 (_GetOverlayMouseScale_t2826364086 * __this, uint64_t ___ulOverlayHandle0, HmdVector2_t_t2255225135 * ___pvecMouseScale1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayMouseScale::EndInvoke(Valve.VR.HmdVector2_t&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayMouseScale_EndInvoke_m4291159934 (_GetOverlayMouseScale_t2826364086 * __this, HmdVector2_t_t2255225135 * ___pvecMouseScale0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
