﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRExtendedDisplay
struct CVRExtendedDisplay_t1925229748;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"

// System.Void Valve.VR.CVRExtendedDisplay::.ctor(System.IntPtr)
extern "C"  void CVRExtendedDisplay__ctor_m3979339075 (CVRExtendedDisplay_t1925229748 * __this, IntPtr_t ___pInterface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRExtendedDisplay::GetWindowBounds(System.Int32&,System.Int32&,System.UInt32&,System.UInt32&)
extern "C"  void CVRExtendedDisplay_GetWindowBounds_m2253417120 (CVRExtendedDisplay_t1925229748 * __this, int32_t* ___pnX0, int32_t* ___pnY1, uint32_t* ___pnWidth2, uint32_t* ___pnHeight3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRExtendedDisplay::GetEyeOutputViewport(Valve.VR.EVREye,System.UInt32&,System.UInt32&,System.UInt32&,System.UInt32&)
extern "C"  void CVRExtendedDisplay_GetEyeOutputViewport_m4282147365 (CVRExtendedDisplay_t1925229748 * __this, int32_t ___eEye0, uint32_t* ___pnX1, uint32_t* ___pnY2, uint32_t* ___pnWidth3, uint32_t* ___pnHeight4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRExtendedDisplay::GetDXGIOutputInfo(System.Int32&,System.Int32&)
extern "C"  void CVRExtendedDisplay_GetDXGIOutputInfo_m1249627484 (CVRExtendedDisplay_t1925229748 * __this, int32_t* ___pnAdapterIndex0, int32_t* ___pnAdapterOutputIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
