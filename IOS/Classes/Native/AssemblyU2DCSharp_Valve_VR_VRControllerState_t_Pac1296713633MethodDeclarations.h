﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_VRControllerState_t_Pac1296713633.h"
#include "AssemblyU2DCSharp_Valve_VR_VRControllerState_t2504874220.h"

// System.Void Valve.VR.VRControllerState_t_Packed::Unpack(Valve.VR.VRControllerState_t&)
extern "C"  void VRControllerState_t_Packed_Unpack_m448770320 (VRControllerState_t_Packed_t1296713633 * __this, VRControllerState_t_t2504874220 * ___unpacked0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
