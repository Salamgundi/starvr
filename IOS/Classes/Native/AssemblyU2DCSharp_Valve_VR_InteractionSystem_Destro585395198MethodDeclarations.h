﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.DestroyOnDetachedFromHand
struct DestroyOnDetachedFromHand_t585395198;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"

// System.Void Valve.VR.InteractionSystem.DestroyOnDetachedFromHand::.ctor()
extern "C"  void DestroyOnDetachedFromHand__ctor_m977076032 (DestroyOnDetachedFromHand_t585395198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.DestroyOnDetachedFromHand::OnDetachedFromHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void DestroyOnDetachedFromHand_OnDetachedFromHand_m1301351216 (DestroyOnDetachedFromHand_t585395198 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
