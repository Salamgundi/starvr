﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.PanelMenuItemControllerEventHandler
struct PanelMenuItemControllerEventHandler_t780308162;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.PanelMenuItemController
struct  PanelMenuItemController_t3837844790  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.PanelMenuItemControllerEventHandler VRTK.PanelMenuItemController::PanelMenuItemShowing
	PanelMenuItemControllerEventHandler_t780308162 * ___PanelMenuItemShowing_2;
	// VRTK.PanelMenuItemControllerEventHandler VRTK.PanelMenuItemController::PanelMenuItemHiding
	PanelMenuItemControllerEventHandler_t780308162 * ___PanelMenuItemHiding_3;
	// VRTK.PanelMenuItemControllerEventHandler VRTK.PanelMenuItemController::PanelMenuItemSwipeLeft
	PanelMenuItemControllerEventHandler_t780308162 * ___PanelMenuItemSwipeLeft_4;
	// VRTK.PanelMenuItemControllerEventHandler VRTK.PanelMenuItemController::PanelMenuItemSwipeRight
	PanelMenuItemControllerEventHandler_t780308162 * ___PanelMenuItemSwipeRight_5;
	// VRTK.PanelMenuItemControllerEventHandler VRTK.PanelMenuItemController::PanelMenuItemSwipeTop
	PanelMenuItemControllerEventHandler_t780308162 * ___PanelMenuItemSwipeTop_6;
	// VRTK.PanelMenuItemControllerEventHandler VRTK.PanelMenuItemController::PanelMenuItemSwipeBottom
	PanelMenuItemControllerEventHandler_t780308162 * ___PanelMenuItemSwipeBottom_7;
	// VRTK.PanelMenuItemControllerEventHandler VRTK.PanelMenuItemController::PanelMenuItemTriggerPressed
	PanelMenuItemControllerEventHandler_t780308162 * ___PanelMenuItemTriggerPressed_8;

public:
	inline static int32_t get_offset_of_PanelMenuItemShowing_2() { return static_cast<int32_t>(offsetof(PanelMenuItemController_t3837844790, ___PanelMenuItemShowing_2)); }
	inline PanelMenuItemControllerEventHandler_t780308162 * get_PanelMenuItemShowing_2() const { return ___PanelMenuItemShowing_2; }
	inline PanelMenuItemControllerEventHandler_t780308162 ** get_address_of_PanelMenuItemShowing_2() { return &___PanelMenuItemShowing_2; }
	inline void set_PanelMenuItemShowing_2(PanelMenuItemControllerEventHandler_t780308162 * value)
	{
		___PanelMenuItemShowing_2 = value;
		Il2CppCodeGenWriteBarrier(&___PanelMenuItemShowing_2, value);
	}

	inline static int32_t get_offset_of_PanelMenuItemHiding_3() { return static_cast<int32_t>(offsetof(PanelMenuItemController_t3837844790, ___PanelMenuItemHiding_3)); }
	inline PanelMenuItemControllerEventHandler_t780308162 * get_PanelMenuItemHiding_3() const { return ___PanelMenuItemHiding_3; }
	inline PanelMenuItemControllerEventHandler_t780308162 ** get_address_of_PanelMenuItemHiding_3() { return &___PanelMenuItemHiding_3; }
	inline void set_PanelMenuItemHiding_3(PanelMenuItemControllerEventHandler_t780308162 * value)
	{
		___PanelMenuItemHiding_3 = value;
		Il2CppCodeGenWriteBarrier(&___PanelMenuItemHiding_3, value);
	}

	inline static int32_t get_offset_of_PanelMenuItemSwipeLeft_4() { return static_cast<int32_t>(offsetof(PanelMenuItemController_t3837844790, ___PanelMenuItemSwipeLeft_4)); }
	inline PanelMenuItemControllerEventHandler_t780308162 * get_PanelMenuItemSwipeLeft_4() const { return ___PanelMenuItemSwipeLeft_4; }
	inline PanelMenuItemControllerEventHandler_t780308162 ** get_address_of_PanelMenuItemSwipeLeft_4() { return &___PanelMenuItemSwipeLeft_4; }
	inline void set_PanelMenuItemSwipeLeft_4(PanelMenuItemControllerEventHandler_t780308162 * value)
	{
		___PanelMenuItemSwipeLeft_4 = value;
		Il2CppCodeGenWriteBarrier(&___PanelMenuItemSwipeLeft_4, value);
	}

	inline static int32_t get_offset_of_PanelMenuItemSwipeRight_5() { return static_cast<int32_t>(offsetof(PanelMenuItemController_t3837844790, ___PanelMenuItemSwipeRight_5)); }
	inline PanelMenuItemControllerEventHandler_t780308162 * get_PanelMenuItemSwipeRight_5() const { return ___PanelMenuItemSwipeRight_5; }
	inline PanelMenuItemControllerEventHandler_t780308162 ** get_address_of_PanelMenuItemSwipeRight_5() { return &___PanelMenuItemSwipeRight_5; }
	inline void set_PanelMenuItemSwipeRight_5(PanelMenuItemControllerEventHandler_t780308162 * value)
	{
		___PanelMenuItemSwipeRight_5 = value;
		Il2CppCodeGenWriteBarrier(&___PanelMenuItemSwipeRight_5, value);
	}

	inline static int32_t get_offset_of_PanelMenuItemSwipeTop_6() { return static_cast<int32_t>(offsetof(PanelMenuItemController_t3837844790, ___PanelMenuItemSwipeTop_6)); }
	inline PanelMenuItemControllerEventHandler_t780308162 * get_PanelMenuItemSwipeTop_6() const { return ___PanelMenuItemSwipeTop_6; }
	inline PanelMenuItemControllerEventHandler_t780308162 ** get_address_of_PanelMenuItemSwipeTop_6() { return &___PanelMenuItemSwipeTop_6; }
	inline void set_PanelMenuItemSwipeTop_6(PanelMenuItemControllerEventHandler_t780308162 * value)
	{
		___PanelMenuItemSwipeTop_6 = value;
		Il2CppCodeGenWriteBarrier(&___PanelMenuItemSwipeTop_6, value);
	}

	inline static int32_t get_offset_of_PanelMenuItemSwipeBottom_7() { return static_cast<int32_t>(offsetof(PanelMenuItemController_t3837844790, ___PanelMenuItemSwipeBottom_7)); }
	inline PanelMenuItemControllerEventHandler_t780308162 * get_PanelMenuItemSwipeBottom_7() const { return ___PanelMenuItemSwipeBottom_7; }
	inline PanelMenuItemControllerEventHandler_t780308162 ** get_address_of_PanelMenuItemSwipeBottom_7() { return &___PanelMenuItemSwipeBottom_7; }
	inline void set_PanelMenuItemSwipeBottom_7(PanelMenuItemControllerEventHandler_t780308162 * value)
	{
		___PanelMenuItemSwipeBottom_7 = value;
		Il2CppCodeGenWriteBarrier(&___PanelMenuItemSwipeBottom_7, value);
	}

	inline static int32_t get_offset_of_PanelMenuItemTriggerPressed_8() { return static_cast<int32_t>(offsetof(PanelMenuItemController_t3837844790, ___PanelMenuItemTriggerPressed_8)); }
	inline PanelMenuItemControllerEventHandler_t780308162 * get_PanelMenuItemTriggerPressed_8() const { return ___PanelMenuItemTriggerPressed_8; }
	inline PanelMenuItemControllerEventHandler_t780308162 ** get_address_of_PanelMenuItemTriggerPressed_8() { return &___PanelMenuItemTriggerPressed_8; }
	inline void set_PanelMenuItemTriggerPressed_8(PanelMenuItemControllerEventHandler_t780308162 * value)
	{
		___PanelMenuItemTriggerPressed_8 = value;
		Il2CppCodeGenWriteBarrier(&___PanelMenuItemTriggerPressed_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
