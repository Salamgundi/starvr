﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_ClearOverlayTexture
struct _ClearOverlayTexture_t2610332521;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_ClearOverlayTexture::.ctor(System.Object,System.IntPtr)
extern "C"  void _ClearOverlayTexture__ctor_m279786860 (_ClearOverlayTexture_t2610332521 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_ClearOverlayTexture::Invoke(System.UInt64)
extern "C"  int32_t _ClearOverlayTexture_Invoke_m2941807107 (_ClearOverlayTexture_t2610332521 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_ClearOverlayTexture::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ClearOverlayTexture_BeginInvoke_m4057208618 (_ClearOverlayTexture_t2610332521 * __this, uint64_t ___ulOverlayHandle0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_ClearOverlayTexture::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _ClearOverlayTexture_EndInvoke_m2518693076 (_ClearOverlayTexture_t2610332521 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
