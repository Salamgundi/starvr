﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Player
struct Player_t4256718089;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// SteamVR_Controller/Device
struct Device_t2885069456;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void Valve.VR.InteractionSystem.Player::.ctor()
extern "C"  void Player__ctor_m3095529409 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.InteractionSystem.Player Valve.VR.InteractionSystem.Player::get_instance()
extern "C"  Player_t4256718089 * Player_get_instance_m2960735442 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Valve.VR.InteractionSystem.Player::get_handCount()
extern "C"  int32_t Player_get_handCount_m3035749952 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.Player::GetHand(System.Int32)
extern "C"  Hand_t379716353 * Player_GetHand_m3087617038 (Player_t4256718089 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.Player::get_leftHand()
extern "C"  Hand_t379716353 * Player_get_leftHand_m544516909 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.Player::get_rightHand()
extern "C"  Hand_t379716353 * Player_get_rightHand_m2129836990 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Controller/Device Valve.VR.InteractionSystem.Player::get_leftController()
extern "C"  Device_t2885069456 * Player_get_leftController_m3749541130 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Controller/Device Valve.VR.InteractionSystem.Player::get_rightController()
extern "C"  Device_t2885069456 * Player_get_rightController_m626120881 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Valve.VR.InteractionSystem.Player::get_hmdTransform()
extern "C"  Transform_t3275118058 * Player_get_hmdTransform_m1283636297 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.InteractionSystem.Player::get_eyeHeight()
extern "C"  float Player_get_eyeHeight_m658171214 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Valve.VR.InteractionSystem.Player::get_feetPositionGuess()
extern "C"  Vector3_t2243707580  Player_get_feetPositionGuess_m936837672 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Valve.VR.InteractionSystem.Player::get_bodyDirectionGuess()
extern "C"  Vector3_t2243707580  Player_get_bodyDirectionGuess_m1203812204 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Player::Awake()
extern "C"  void Player_Awake_m1724670202 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Player::OnEnable()
extern "C"  void Player_OnEnable_m3742224185 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Player::OnDrawGizmos()
extern "C"  void Player_OnDrawGizmos_m483330629 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Player::Draw2DDebug()
extern "C"  void Player_Draw2DDebug_m3256464530 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Player::ActivateRig(UnityEngine.GameObject)
extern "C"  void Player_ActivateRig_m1600406210 (Player_t4256718089 * __this, GameObject_t1756533147 * ___rig0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Player::PlayerShotSelf()
extern "C"  void Player_PlayerShotSelf_m4158547946 (Player_t4256718089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
