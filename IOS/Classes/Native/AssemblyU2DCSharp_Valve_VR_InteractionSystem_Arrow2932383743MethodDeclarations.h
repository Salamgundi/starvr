﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Arrow
struct Arrow_t2932383743;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void Valve.VR.InteractionSystem.Arrow::.ctor()
extern "C"  void Arrow__ctor_m1198950615 (Arrow_t2932383743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Arrow::Start()
extern "C"  void Arrow_Start_m469191123 (Arrow_t2932383743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Arrow::FixedUpdate()
extern "C"  void Arrow_FixedUpdate_m802957384 (Arrow_t2932383743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Arrow::ArrowReleased(System.Single)
extern "C"  void Arrow_ArrowReleased_m189506778 (Arrow_t2932383743 * __this, float ___inputVelocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Arrow::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void Arrow_OnCollisionEnter_m2226711813 (Arrow_t2932383743 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Arrow::StickInTarget(UnityEngine.Collision,System.Boolean)
extern "C"  void Arrow_StickInTarget_m1985938857 (Arrow_t2932383743 * __this, Collision_t2876846408 * ___collision0, bool ___bSkipRayCast1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Arrow::OnDestroy()
extern "C"  void Arrow_OnDestroy_m361956506 (Arrow_t2932383743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
