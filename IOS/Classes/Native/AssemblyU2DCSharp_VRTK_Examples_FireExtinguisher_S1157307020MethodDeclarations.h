﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.FireExtinguisher_Sprayer
struct FireExtinguisher_Sprayer_t1157307020;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.Examples.FireExtinguisher_Sprayer::.ctor()
extern "C"  void FireExtinguisher_Sprayer__ctor_m1697788727 (FireExtinguisher_Sprayer_t1157307020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.FireExtinguisher_Sprayer::Spray(System.Single)
extern "C"  void FireExtinguisher_Sprayer_Spray_m1249138359 (FireExtinguisher_Sprayer_t1157307020 * __this, float ___power0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.FireExtinguisher_Sprayer::Awake()
extern "C"  void FireExtinguisher_Sprayer_Awake_m2329885082 (FireExtinguisher_Sprayer_t1157307020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.FireExtinguisher_Sprayer::Update()
extern "C"  void FireExtinguisher_Sprayer_Update_m3852336856 (FireExtinguisher_Sprayer_t1157307020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
