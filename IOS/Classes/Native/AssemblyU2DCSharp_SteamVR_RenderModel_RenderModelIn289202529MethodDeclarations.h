﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_RenderModel/RenderModelInterfaceHolder
struct RenderModelInterfaceHolder_t289202529;
// Valve.VR.CVRRenderModels
struct CVRRenderModels_t2019937239;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_RenderModel/RenderModelInterfaceHolder::.ctor()
extern "C"  void RenderModelInterfaceHolder__ctor_m3243811112 (RenderModelInterfaceHolder_t289202529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRRenderModels SteamVR_RenderModel/RenderModelInterfaceHolder::get_instance()
extern "C"  CVRRenderModels_t2019937239 * RenderModelInterfaceHolder_get_instance_m2465009780 (RenderModelInterfaceHolder_t289202529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel/RenderModelInterfaceHolder::Dispose()
extern "C"  void RenderModelInterfaceHolder_Dispose_m1688095435 (RenderModelInterfaceHolder_t289202529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
