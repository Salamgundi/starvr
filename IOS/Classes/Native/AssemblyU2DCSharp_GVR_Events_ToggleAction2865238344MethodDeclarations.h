﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GVR.Events.ToggleAction
struct ToggleAction_t2865238344;

#include "codegen/il2cpp-codegen.h"

// System.Void GVR.Events.ToggleAction::.ctor()
extern "C"  void ToggleAction__ctor_m1487284959 (ToggleAction_t2865238344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GVR.Events.ToggleAction::Start()
extern "C"  void ToggleAction_Start_m1766399539 (ToggleAction_t2865238344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GVR.Events.ToggleAction::Toggle()
extern "C"  void ToggleAction_Toggle_m2354549425 (ToggleAction_t2865238344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GVR.Events.ToggleAction::Set(System.Boolean)
extern "C"  void ToggleAction_Set_m2917802110 (ToggleAction_t2865238344 * __this, bool ___on0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GVR.Events.ToggleAction::RaiseToggleEvent(System.Boolean)
extern "C"  void ToggleAction_RaiseToggleEvent_m3929023862 (ToggleAction_t2865238344 * __this, bool ___on0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
