﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayTextureSize
struct _GetOverlayTextureSize_t1288689867;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayTextureSize::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayTextureSize__ctor_m3756566500 (_GetOverlayTextureSize_t1288689867 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTextureSize::Invoke(System.UInt64,System.UInt32&,System.UInt32&)
extern "C"  int32_t _GetOverlayTextureSize_Invoke_m1262823405 (_GetOverlayTextureSize_t1288689867 * __this, uint64_t ___ulOverlayHandle0, uint32_t* ___pWidth1, uint32_t* ___pHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayTextureSize::BeginInvoke(System.UInt64,System.UInt32&,System.UInt32&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayTextureSize_BeginInvoke_m1436549358 (_GetOverlayTextureSize_t1288689867 * __this, uint64_t ___ulOverlayHandle0, uint32_t* ___pWidth1, uint32_t* ___pHeight2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTextureSize::EndInvoke(System.UInt32&,System.UInt32&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayTextureSize_EndInvoke_m727875276 (_GetOverlayTextureSize_t1288689867 * __this, uint32_t* ___pWidth0, uint32_t* ___pHeight1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
