﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.PanelMenuController/<TweenMenuScale>c__Iterator0
struct U3CTweenMenuScaleU3Ec__Iterator0_t3216112136;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.PanelMenuController/<TweenMenuScale>c__Iterator0::.ctor()
extern "C"  void U3CTweenMenuScaleU3Ec__Iterator0__ctor_m1631724777 (U3CTweenMenuScaleU3Ec__Iterator0_t3216112136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.PanelMenuController/<TweenMenuScale>c__Iterator0::MoveNext()
extern "C"  bool U3CTweenMenuScaleU3Ec__Iterator0_MoveNext_m3407477087 (U3CTweenMenuScaleU3Ec__Iterator0_t3216112136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.PanelMenuController/<TweenMenuScale>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTweenMenuScaleU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m254787299 (U3CTweenMenuScaleU3Ec__Iterator0_t3216112136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.PanelMenuController/<TweenMenuScale>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTweenMenuScaleU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2324997323 (U3CTweenMenuScaleU3Ec__Iterator0_t3216112136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController/<TweenMenuScale>c__Iterator0::Dispose()
extern "C"  void U3CTweenMenuScaleU3Ec__Iterator0_Dispose_m4117215130 (U3CTweenMenuScaleU3Ec__Iterator0_t3216112136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController/<TweenMenuScale>c__Iterator0::Reset()
extern "C"  void U3CTweenMenuScaleU3Ec__Iterator0_Reset_m3233619468 (U3CTweenMenuScaleU3Ec__Iterator0_t3216112136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
