﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ConstantForce
struct ConstantForce_t3796310167;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void UnityEngine.ConstantForce::set_force(UnityEngine.Vector3)
extern "C"  void ConstantForce_set_force_m656826723 (ConstantForce_t3796310167 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::INTERNAL_set_force(UnityEngine.Vector3&)
extern "C"  void ConstantForce_INTERNAL_set_force_m3349249693 (ConstantForce_t3796310167 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::set_relativeForce(UnityEngine.Vector3)
extern "C"  void ConstantForce_set_relativeForce_m1603114511 (ConstantForce_t3796310167 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::INTERNAL_set_relativeForce(UnityEngine.Vector3&)
extern "C"  void ConstantForce_INTERNAL_set_relativeForce_m1193423729 (ConstantForce_t3796310167 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
