﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;
// VRTK.ControllerActionsEventHandler
struct ControllerActionsEventHandler_t1521142243;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.Highlighters.VRTK_BaseHighlighter
struct VRTK_BaseHighlighter_t3110203740;
// UnityEngine.Material
struct Material_t193706927;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_ControllerActionsEventHandl1521142243.h"
#include "AssemblyU2DCSharp_VRTK_ControllerActionsEventArgs344001476.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Nullable_1_gen283458390.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "AssemblyU2DCSharp_VRTK_Highlighters_VRTK_BaseHighl3110203740.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_String2029220233.h"

// System.Void VRTK.VRTK_ControllerActions::.ctor()
extern "C"  void VRTK_ControllerActions__ctor_m704242287 (VRTK_ControllerActions_t3642353851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::add_ControllerModelVisible(VRTK.ControllerActionsEventHandler)
extern "C"  void VRTK_ControllerActions_add_ControllerModelVisible_m1541846778 (VRTK_ControllerActions_t3642353851 * __this, ControllerActionsEventHandler_t1521142243 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::remove_ControllerModelVisible(VRTK.ControllerActionsEventHandler)
extern "C"  void VRTK_ControllerActions_remove_ControllerModelVisible_m1722858375 (VRTK_ControllerActions_t3642353851 * __this, ControllerActionsEventHandler_t1521142243 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::add_ControllerModelInvisible(VRTK.ControllerActionsEventHandler)
extern "C"  void VRTK_ControllerActions_add_ControllerModelInvisible_m2100416299 (VRTK_ControllerActions_t3642353851 * __this, ControllerActionsEventHandler_t1521142243 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::remove_ControllerModelInvisible(VRTK.ControllerActionsEventHandler)
extern "C"  void VRTK_ControllerActions_remove_ControllerModelInvisible_m1876877494 (VRTK_ControllerActions_t3642353851 * __this, ControllerActionsEventHandler_t1521142243 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::OnControllerModelVisible(VRTK.ControllerActionsEventArgs)
extern "C"  void VRTK_ControllerActions_OnControllerModelVisible_m1006460428 (VRTK_ControllerActions_t3642353851 * __this, ControllerActionsEventArgs_t344001476  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::OnControllerModelInvisible(VRTK.ControllerActionsEventArgs)
extern "C"  void VRTK_ControllerActions_OnControllerModelInvisible_m3498902823 (VRTK_ControllerActions_t3642353851 * __this, ControllerActionsEventArgs_t344001476  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_ControllerActions::IsControllerVisible()
extern "C"  bool VRTK_ControllerActions_IsControllerVisible_m1859448959 (VRTK_ControllerActions_t3642353851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::ToggleControllerModel(System.Boolean,UnityEngine.GameObject)
extern "C"  void VRTK_ControllerActions_ToggleControllerModel_m4243205817 (VRTK_ControllerActions_t3642353851 * __this, bool ___state0, GameObject_t1756533147 * ___grabbedChildObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::SetControllerOpacity(System.Single)
extern "C"  void VRTK_ControllerActions_SetControllerOpacity_m670260713 (VRTK_ControllerActions_t3642353851 * __this, float ___alpha0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::HighlightControllerElement(UnityEngine.GameObject,System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_ControllerActions_HighlightControllerElement_m2510532859 (VRTK_ControllerActions_t3642353851 * __this, GameObject_t1756533147 * ___element0, Nullable_1_t283458390  ___highlight1, float ___fadeDuration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::UnhighlightControllerElement(UnityEngine.GameObject)
extern "C"  void VRTK_ControllerActions_UnhighlightControllerElement_m1757891550 (VRTK_ControllerActions_t3642353851 * __this, GameObject_t1756533147 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::ToggleHighlightControllerElement(System.Boolean,UnityEngine.GameObject,System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_ControllerActions_ToggleHighlightControllerElement_m4030195156 (VRTK_ControllerActions_t3642353851 * __this, bool ___state0, GameObject_t1756533147 * ___element1, Nullable_1_t283458390  ___highlight2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::ToggleHighlightTrigger(System.Boolean,System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_ControllerActions_ToggleHighlightTrigger_m2612417196 (VRTK_ControllerActions_t3642353851 * __this, bool ___state0, Nullable_1_t283458390  ___highlight1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::ToggleHighlightGrip(System.Boolean,System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_ControllerActions_ToggleHighlightGrip_m2330600572 (VRTK_ControllerActions_t3642353851 * __this, bool ___state0, Nullable_1_t283458390  ___highlight1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::ToggleHighlightTouchpad(System.Boolean,System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_ControllerActions_ToggleHighlightTouchpad_m1792685108 (VRTK_ControllerActions_t3642353851 * __this, bool ___state0, Nullable_1_t283458390  ___highlight1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::ToggleHighlightButtonOne(System.Boolean,System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_ControllerActions_ToggleHighlightButtonOne_m3483446332 (VRTK_ControllerActions_t3642353851 * __this, bool ___state0, Nullable_1_t283458390  ___highlight1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::ToggleHighlightButtonTwo(System.Boolean,System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_ControllerActions_ToggleHighlightButtonTwo_m2524342222 (VRTK_ControllerActions_t3642353851 * __this, bool ___state0, Nullable_1_t283458390  ___highlight1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::ToggleHighlightStartMenu(System.Boolean,System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_ControllerActions_ToggleHighlightStartMenu_m1943472779 (VRTK_ControllerActions_t3642353851 * __this, bool ___state0, Nullable_1_t283458390  ___highlight1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::ToggleHighlighBody(System.Boolean,System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_ControllerActions_ToggleHighlighBody_m3823935052 (VRTK_ControllerActions_t3642353851 * __this, bool ___state0, Nullable_1_t283458390  ___highlight1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::ToggleHighlightController(System.Boolean,System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_ControllerActions_ToggleHighlightController_m2591413452 (VRTK_ControllerActions_t3642353851 * __this, bool ___state0, Nullable_1_t283458390  ___highlight1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::TriggerHapticPulse(System.Single)
extern "C"  void VRTK_ControllerActions_TriggerHapticPulse_m3891144560 (VRTK_ControllerActions_t3642353851 * __this, float ___strength0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::TriggerHapticPulse(System.Single,System.Single,System.Single)
extern "C"  void VRTK_ControllerActions_TriggerHapticPulse_m3895550992 (VRTK_ControllerActions_t3642353851 * __this, float ___strength0, float ___duration1, float ___pulseInterval2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::InitaliseHighlighters()
extern "C"  void VRTK_ControllerActions_InitaliseHighlighters_m231200035 (VRTK_ControllerActions_t3642353851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::Awake()
extern "C"  void VRTK_ControllerActions_Awake_m2076007744 (VRTK_ControllerActions_t3642353851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::OnEnable()
extern "C"  void VRTK_ControllerActions_OnEnable_m3891934219 (VRTK_ControllerActions_t3642353851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_ControllerActions::WaitForModel()
extern "C"  Il2CppObject * VRTK_ControllerActions_WaitForModel_m3416086782 (VRTK_ControllerActions_t3642353851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::AddHighlighterToElement(UnityEngine.Transform,VRTK.Highlighters.VRTK_BaseHighlighter,VRTK.Highlighters.VRTK_BaseHighlighter)
extern "C"  void VRTK_ControllerActions_AddHighlighterToElement_m1789208883 (VRTK_ControllerActions_t3642353851 * __this, Transform_t3275118058 * ___element0, VRTK_BaseHighlighter_t3110203740 * ___parentHighlighter1, VRTK_BaseHighlighter_t3110203740 * ___overrideHighlighter2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::CancelHapticPulse()
extern "C"  void VRTK_ControllerActions_CancelHapticPulse_m3222038409 (VRTK_ControllerActions_t3642353851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_ControllerActions::HapticPulse(System.Single,System.Single,System.Single)
extern "C"  Il2CppObject * VRTK_ControllerActions_HapticPulse_m3193097808 (VRTK_ControllerActions_t3642353851 * __this, float ___duration0, float ___hapticPulseStrength1, float ___pulseInterval2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_ControllerActions::CycleColor(UnityEngine.Material,UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  Il2CppObject * VRTK_ControllerActions_CycleColor_m627780801 (VRTK_ControllerActions_t3642353851 * __this, Material_t193706927 * ___material0, Color_t2020392075  ___startColor1, Color_t2020392075  ___endColor2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_ControllerActions::GetElementTransform(System.String)
extern "C"  Transform_t3275118058 * VRTK_ControllerActions_GetElementTransform_m2332089833 (VRTK_ControllerActions_t3642353851 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::ToggleHighlightAlias(System.Boolean,System.String,System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_ControllerActions_ToggleHighlightAlias_m479941992 (VRTK_ControllerActions_t3642353851 * __this, bool ___state0, String_t* ___transformPath1, Nullable_1_t283458390  ___highlight2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.ControllerActionsEventArgs VRTK.VRTK_ControllerActions::SetActionEvent(System.UInt32)
extern "C"  ControllerActionsEventArgs_t344001476  VRTK_ControllerActions_SetActionEvent_m1794703687 (VRTK_ControllerActions_t3642353851 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::ToggleModelRenderers(UnityEngine.GameObject,System.Boolean,UnityEngine.GameObject)
extern "C"  void VRTK_ControllerActions_ToggleModelRenderers_m3359335155 (VRTK_ControllerActions_t3642353851 * __this, GameObject_t1756533147 * ___obj0, bool ___state1, GameObject_t1756533147 * ___grabbedChildObject2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions::SetModelOpacity(UnityEngine.GameObject,System.Single)
extern "C"  void VRTK_ControllerActions_SetModelOpacity_m3186392908 (VRTK_ControllerActions_t3642353851 * __this, GameObject_t1756533147 * ___obj0, float ___alpha1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
