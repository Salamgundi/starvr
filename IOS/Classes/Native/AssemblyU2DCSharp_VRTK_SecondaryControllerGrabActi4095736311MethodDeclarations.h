﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction
struct VRTK_BaseGrabAction_t4095736311;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// VRTK.VRTK_InteractGrab
struct VRTK_InteractGrab_t124353446;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractGrab124353446.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::.ctor()
extern "C"  void VRTK_BaseGrabAction__ctor_m810620426 (VRTK_BaseGrabAction_t4095736311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::Initialise(VRTK.VRTK_InteractableObject,VRTK.VRTK_InteractGrab,VRTK.VRTK_InteractGrab,UnityEngine.Transform,UnityEngine.Transform)
extern "C"  void VRTK_BaseGrabAction_Initialise_m3163055361 (VRTK_BaseGrabAction_t4095736311 * __this, VRTK_InteractableObject_t2604188111 * ___currentGrabbdObject0, VRTK_InteractGrab_t124353446 * ___currentPrimaryGrabbingObject1, VRTK_InteractGrab_t124353446 * ___currentSecondaryGrabbingObject2, Transform_t3275118058 * ___primaryGrabPoint3, Transform_t3275118058 * ___secondaryGrabPoint4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::ResetAction()
extern "C"  void VRTK_BaseGrabAction_ResetAction_m1776830831 (VRTK_BaseGrabAction_t4095736311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::IsActionable()
extern "C"  bool VRTK_BaseGrabAction_IsActionable_m3607280656 (VRTK_BaseGrabAction_t4095736311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::IsSwappable()
extern "C"  bool VRTK_BaseGrabAction_IsSwappable_m3768114549 (VRTK_BaseGrabAction_t4095736311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::ProcessUpdate()
extern "C"  void VRTK_BaseGrabAction_ProcessUpdate_m2161626366 (VRTK_BaseGrabAction_t4095736311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::ProcessFixedUpdate()
extern "C"  void VRTK_BaseGrabAction_ProcessFixedUpdate_m2248964524 (VRTK_BaseGrabAction_t4095736311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::OnDropAction()
extern "C"  void VRTK_BaseGrabAction_OnDropAction_m788388386 (VRTK_BaseGrabAction_t4095736311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction::CheckForceStopDistance(System.Single)
extern "C"  void VRTK_BaseGrabAction_CheckForceStopDistance_m2503706905 (VRTK_BaseGrabAction_t4095736311 * __this, float ___ungrabDistance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
