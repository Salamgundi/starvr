﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_RenderModel
struct SteamVR_RenderModel_t2905485978;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// SteamVR_RenderModel/RenderModel
struct RenderModel_t582033572;
// Valve.VR.CVRRenderModels
struct CVRRenderModels_t2019937239;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// SteamVR_RenderModel/RenderModelInterfaceHolder
struct RenderModelInterfaceHolder_t289202529;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_t3405266389.h"
#include "AssemblyU2DCSharp_Valve_VR_CVRRenderModels2019937239.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_SteamVR_RenderModel_RenderModelIn289202529.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_t1723493896.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_TextureMap_1828165156.h"

// System.Void SteamVR_RenderModel::.ctor()
extern "C"  void SteamVR_RenderModel__ctor_m2018759355 (SteamVR_RenderModel_t2905485978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SteamVR_RenderModel::get_renderModelName()
extern "C"  String_t* SteamVR_RenderModel_get_renderModelName_m1603945469 (SteamVR_RenderModel_t2905485978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel::set_renderModelName(System.String)
extern "C"  void SteamVR_RenderModel_set_renderModelName_m3110854490 (SteamVR_RenderModel_t2905485978 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel::OnModelSkinSettingsHaveChanged(Valve.VR.VREvent_t)
extern "C"  void SteamVR_RenderModel_OnModelSkinSettingsHaveChanged_m909426910 (SteamVR_RenderModel_t2905485978 * __this, VREvent_t_t3405266389  ___vrEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel::OnHideRenderModels(System.Boolean)
extern "C"  void SteamVR_RenderModel_OnHideRenderModels_m3987245159 (SteamVR_RenderModel_t2905485978 * __this, bool ___hidden0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel::OnDeviceConnected(System.Int32,System.Boolean)
extern "C"  void SteamVR_RenderModel_OnDeviceConnected_m2556417799 (SteamVR_RenderModel_t2905485978 * __this, int32_t ___i0, bool ___connected1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel::UpdateModel()
extern "C"  void SteamVR_RenderModel_UpdateModel_m3260925377 (SteamVR_RenderModel_t2905485978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SteamVR_RenderModel::SetModelAsync(System.String)
extern "C"  Il2CppObject * SteamVR_RenderModel_SetModelAsync_m3329038796 (SteamVR_RenderModel_t2905485978 * __this, String_t* ___renderModelName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_RenderModel::SetModel(System.String)
extern "C"  bool SteamVR_RenderModel_SetModel_m2782523848 (SteamVR_RenderModel_t2905485978 * __this, String_t* ___renderModelName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_RenderModel/RenderModel SteamVR_RenderModel::LoadRenderModel(Valve.VR.CVRRenderModels,System.String,System.String)
extern "C"  RenderModel_t582033572 * SteamVR_RenderModel_LoadRenderModel_m2520163206 (SteamVR_RenderModel_t2905485978 * __this, CVRRenderModels_t2019937239 * ___renderModels0, String_t* ___renderModelName1, String_t* ___baseName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SteamVR_RenderModel::FreeRenderModel(System.IntPtr)
extern "C"  Il2CppObject * SteamVR_RenderModel_FreeRenderModel_m3605243372 (SteamVR_RenderModel_t2905485978 * __this, IntPtr_t ___pRenderModel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform SteamVR_RenderModel::FindComponent(System.String)
extern "C"  Transform_t3275118058 * SteamVR_RenderModel_FindComponent_m3469686483 (SteamVR_RenderModel_t2905485978 * __this, String_t* ___componentName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel::StripMesh(UnityEngine.GameObject)
extern "C"  void SteamVR_RenderModel_StripMesh_m3468718388 (SteamVR_RenderModel_t2905485978 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_RenderModel::LoadComponents(SteamVR_RenderModel/RenderModelInterfaceHolder,System.String)
extern "C"  bool SteamVR_RenderModel_LoadComponents_m3218948104 (SteamVR_RenderModel_t2905485978 * __this, RenderModelInterfaceHolder_t289202529 * ___holder0, String_t* ___renderModelName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel::Awake()
extern "C"  void SteamVR_RenderModel_Awake_m1814905514 (SteamVR_RenderModel_t2905485978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel::OnEnable()
extern "C"  void SteamVR_RenderModel_OnEnable_m2777026943 (SteamVR_RenderModel_t2905485978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel::OnDisable()
extern "C"  void SteamVR_RenderModel_OnDisable_m2395470270 (SteamVR_RenderModel_t2905485978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel::Update()
extern "C"  void SteamVR_RenderModel_Update_m4044174536 (SteamVR_RenderModel_t2905485978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel::UpdateComponents(Valve.VR.CVRRenderModels)
extern "C"  void SteamVR_RenderModel_UpdateComponents_m530869607 (SteamVR_RenderModel_t2905485978 * __this, CVRRenderModels_t2019937239 * ___renderModels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel::SetDeviceIndex(System.Int32)
extern "C"  void SteamVR_RenderModel_SetDeviceIndex_m16763110 (SteamVR_RenderModel_t2905485978 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.RenderModel_t SteamVR_RenderModel::MarshalRenderModel(System.IntPtr)
extern "C"  RenderModel_t_t1723493896  SteamVR_RenderModel_MarshalRenderModel_m3460819789 (SteamVR_RenderModel_t2905485978 * __this, IntPtr_t ___pRenderModel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.RenderModel_TextureMap_t SteamVR_RenderModel::MarshalRenderModel_TextureMap(System.IntPtr)
extern "C"  RenderModel_TextureMap_t_t1828165156  SteamVR_RenderModel_MarshalRenderModel_TextureMap_m3887169433 (SteamVR_RenderModel_t2905485978 * __this, IntPtr_t ___pRenderModel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel::.cctor()
extern "C"  void SteamVR_RenderModel__cctor_m3282104304 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
