﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Overlay
struct SteamVR_Overlay_t1916653097;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SteamVR_Overlay1916653097.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_t3405266389.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_SteamVR_Overlay_IntersectionResul676309149.h"

// System.Void SteamVR_Overlay::.ctor()
extern "C"  void SteamVR_Overlay__ctor_m3583707854 (SteamVR_Overlay_t1916653097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Overlay SteamVR_Overlay::get_instance()
extern "C"  SteamVR_Overlay_t1916653097 * SteamVR_Overlay_get_instance_m3475490194 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Overlay::set_instance(SteamVR_Overlay)
extern "C"  void SteamVR_Overlay_set_instance_m2725483827 (Il2CppObject * __this /* static, unused */, SteamVR_Overlay_t1916653097 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SteamVR_Overlay::get_key()
extern "C"  String_t* SteamVR_Overlay_get_key_m1776983203 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Overlay::OnEnable()
extern "C"  void SteamVR_Overlay_OnEnable_m2180512974 (SteamVR_Overlay_t1916653097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Overlay::OnDisable()
extern "C"  void SteamVR_Overlay_OnDisable_m901800669 (SteamVR_Overlay_t1916653097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Overlay::UpdateOverlay()
extern "C"  void SteamVR_Overlay_UpdateOverlay_m684631795 (SteamVR_Overlay_t1916653097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Overlay::PollNextEvent(Valve.VR.VREvent_t&)
extern "C"  bool SteamVR_Overlay_PollNextEvent_m466203873 (SteamVR_Overlay_t1916653097 * __this, VREvent_t_t3405266389 * ___pEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Overlay::ComputeIntersection(UnityEngine.Vector3,UnityEngine.Vector3,SteamVR_Overlay/IntersectionResults&)
extern "C"  bool SteamVR_Overlay_ComputeIntersection_m2334622965 (SteamVR_Overlay_t1916653097 * __this, Vector3_t2243707580  ___source0, Vector3_t2243707580  ___direction1, IntersectionResults_t676309149 * ___results2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
