﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_FadeGrid
struct _FadeGrid_t1389933364;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_FadeGrid::.ctor(System.Object,System.IntPtr)
extern "C"  void _FadeGrid__ctor_m2671564441 (_FadeGrid_t1389933364 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_FadeGrid::Invoke(System.Single,System.Boolean)
extern "C"  void _FadeGrid_Invoke_m2403867707 (_FadeGrid_t1389933364 * __this, float ___fSeconds0, bool ___bFadeIn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_FadeGrid::BeginInvoke(System.Single,System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _FadeGrid_BeginInvoke_m877443756 (_FadeGrid_t1389933364 * __this, float ___fSeconds0, bool ___bFadeIn1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_FadeGrid::EndInvoke(System.IAsyncResult)
extern "C"  void _FadeGrid_EndInvoke_m1094690363 (_FadeGrid_t1389933364 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
