﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.UI_Keyboard
struct UI_Keyboard_t3022578864;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void VRTK.Examples.UI_Keyboard::.ctor()
extern "C"  void UI_Keyboard__ctor_m4137164713 (UI_Keyboard_t3022578864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.UI_Keyboard::ClickKey(System.String)
extern "C"  void UI_Keyboard_ClickKey_m4229808888 (UI_Keyboard_t3022578864 * __this, String_t* ___character0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.UI_Keyboard::Backspace()
extern "C"  void UI_Keyboard_Backspace_m4251712130 (UI_Keyboard_t3022578864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.UI_Keyboard::Enter()
extern "C"  void UI_Keyboard_Enter_m2144404917 (UI_Keyboard_t3022578864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.UI_Keyboard::Start()
extern "C"  void UI_Keyboard_Start_m262651401 (UI_Keyboard_t3022578864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
