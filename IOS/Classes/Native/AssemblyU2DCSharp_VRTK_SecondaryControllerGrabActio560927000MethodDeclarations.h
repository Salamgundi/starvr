﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction/<RealignOnRelease>c__Iterator0
struct U3CRealignOnReleaseU3Ec__Iterator0_t560927000;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction/<RealignOnRelease>c__Iterator0::.ctor()
extern "C"  void U3CRealignOnReleaseU3Ec__Iterator0__ctor_m156979365 (U3CRealignOnReleaseU3Ec__Iterator0_t560927000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction/<RealignOnRelease>c__Iterator0::MoveNext()
extern "C"  bool U3CRealignOnReleaseU3Ec__Iterator0_MoveNext_m3219681523 (U3CRealignOnReleaseU3Ec__Iterator0_t560927000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction/<RealignOnRelease>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRealignOnReleaseU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1183417515 (U3CRealignOnReleaseU3Ec__Iterator0_t560927000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction/<RealignOnRelease>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRealignOnReleaseU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2716661795 (U3CRealignOnReleaseU3Ec__Iterator0_t560927000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction/<RealignOnRelease>c__Iterator0::Dispose()
extern "C"  void U3CRealignOnReleaseU3Ec__Iterator0_Dispose_m368099186 (U3CRealignOnReleaseU3Ec__Iterator0_t560927000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction/<RealignOnRelease>c__Iterator0::Reset()
extern "C"  void U3CRealignOnReleaseU3Ec__Iterator0_Reset_m2490399756 (U3CRealignOnReleaseU3Ec__Iterator0_t560927000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
