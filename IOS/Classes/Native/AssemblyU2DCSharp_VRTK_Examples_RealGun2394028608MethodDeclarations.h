﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.RealGun
struct RealGun_t2394028608;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.Examples.RealGun::.ctor()
extern "C"  void RealGun__ctor_m3793048749 (RealGun_t2394028608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun::ToggleCollision(UnityEngine.Rigidbody,UnityEngine.Collider,System.Boolean)
extern "C"  void RealGun_ToggleCollision_m2061533921 (RealGun_t2394028608 * __this, Rigidbody_t4233889191 * ___objRB0, Collider_t3497673348 * ___objCol1, bool ___state2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun::ToggleSlide(System.Boolean)
extern "C"  void RealGun_ToggleSlide_m51529819 (RealGun_t2394028608 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun::ToggleSafetySwitch(System.Boolean)
extern "C"  void RealGun_ToggleSafetySwitch_m3075610414 (RealGun_t2394028608 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun::Grabbed(UnityEngine.GameObject)
extern "C"  void RealGun_Grabbed_m921006778 (RealGun_t2394028608 * __this, GameObject_t1756533147 * ___currentGrabbingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun::Ungrabbed(UnityEngine.GameObject)
extern "C"  void RealGun_Ungrabbed_m462029407 (RealGun_t2394028608 * __this, GameObject_t1756533147 * ___previousGrabbingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun::StartUsing(UnityEngine.GameObject)
extern "C"  void RealGun_StartUsing_m1288619667 (RealGun_t2394028608 * __this, GameObject_t1756533147 * ___currentUsingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun::Awake()
extern "C"  void RealGun_Awake_m2592140830 (RealGun_t2394028608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun::Update()
extern "C"  void RealGun_Update_m3868343584 (RealGun_t2394028608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun::FireBullet()
extern "C"  void RealGun_FireBullet_m1763903859 (RealGun_t2394028608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
