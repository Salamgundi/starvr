﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.SoundDeparent
struct SoundDeparent_t50869408;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.SoundDeparent::.ctor()
extern "C"  void SoundDeparent__ctor_m3658928448 (SoundDeparent_t50869408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SoundDeparent::Awake()
extern "C"  void SoundDeparent_Awake_m1247532165 (SoundDeparent_t50869408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SoundDeparent::Start()
extern "C"  void SoundDeparent_Start_m1992985440 (SoundDeparent_t50869408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
