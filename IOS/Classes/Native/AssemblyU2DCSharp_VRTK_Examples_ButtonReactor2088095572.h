﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.UnityEventHelper.VRTK_Button_UnityEvents
struct VRTK_Button_UnityEvents_t4285437623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.ButtonReactor
struct  ButtonReactor_t2088095572  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject VRTK.Examples.ButtonReactor::go
	GameObject_t1756533147 * ___go_2;
	// UnityEngine.Transform VRTK.Examples.ButtonReactor::dispenseLocation
	Transform_t3275118058 * ___dispenseLocation_3;
	// VRTK.UnityEventHelper.VRTK_Button_UnityEvents VRTK.Examples.ButtonReactor::buttonEvents
	VRTK_Button_UnityEvents_t4285437623 * ___buttonEvents_4;

public:
	inline static int32_t get_offset_of_go_2() { return static_cast<int32_t>(offsetof(ButtonReactor_t2088095572, ___go_2)); }
	inline GameObject_t1756533147 * get_go_2() const { return ___go_2; }
	inline GameObject_t1756533147 ** get_address_of_go_2() { return &___go_2; }
	inline void set_go_2(GameObject_t1756533147 * value)
	{
		___go_2 = value;
		Il2CppCodeGenWriteBarrier(&___go_2, value);
	}

	inline static int32_t get_offset_of_dispenseLocation_3() { return static_cast<int32_t>(offsetof(ButtonReactor_t2088095572, ___dispenseLocation_3)); }
	inline Transform_t3275118058 * get_dispenseLocation_3() const { return ___dispenseLocation_3; }
	inline Transform_t3275118058 ** get_address_of_dispenseLocation_3() { return &___dispenseLocation_3; }
	inline void set_dispenseLocation_3(Transform_t3275118058 * value)
	{
		___dispenseLocation_3 = value;
		Il2CppCodeGenWriteBarrier(&___dispenseLocation_3, value);
	}

	inline static int32_t get_offset_of_buttonEvents_4() { return static_cast<int32_t>(offsetof(ButtonReactor_t2088095572, ___buttonEvents_4)); }
	inline VRTK_Button_UnityEvents_t4285437623 * get_buttonEvents_4() const { return ___buttonEvents_4; }
	inline VRTK_Button_UnityEvents_t4285437623 ** get_address_of_buttonEvents_4() { return &___buttonEvents_4; }
	inline void set_buttonEvents_4(VRTK_Button_UnityEvents_t4285437623 * value)
	{
		___buttonEvents_4 = value;
		Il2CppCodeGenWriteBarrier(&___buttonEvents_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
