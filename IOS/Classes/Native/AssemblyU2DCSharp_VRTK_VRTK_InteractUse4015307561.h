﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.ObjectInteractEventHandler
struct ObjectInteractEventHandler_t1701902511;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_InteractTouch
struct VRTK_InteractTouch_t4022091061;
// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;
// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_InteractUse
struct  VRTK_InteractUse_t4015307561  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.ObjectInteractEventHandler VRTK.VRTK_InteractUse::ControllerUseInteractableObject
	ObjectInteractEventHandler_t1701902511 * ___ControllerUseInteractableObject_2;
	// VRTK.ObjectInteractEventHandler VRTK.VRTK_InteractUse::ControllerUnuseInteractableObject
	ObjectInteractEventHandler_t1701902511 * ___ControllerUnuseInteractableObject_3;
	// UnityEngine.GameObject VRTK.VRTK_InteractUse::usingObject
	GameObject_t1756533147 * ___usingObject_4;
	// VRTK.VRTK_InteractTouch VRTK.VRTK_InteractUse::interactTouch
	VRTK_InteractTouch_t4022091061 * ___interactTouch_5;
	// VRTK.VRTK_ControllerActions VRTK.VRTK_InteractUse::controllerActions
	VRTK_ControllerActions_t3642353851 * ___controllerActions_6;
	// VRTK.VRTK_ControllerEvents VRTK.VRTK_InteractUse::controllerEvents
	VRTK_ControllerEvents_t3225224819 * ___controllerEvents_7;

public:
	inline static int32_t get_offset_of_ControllerUseInteractableObject_2() { return static_cast<int32_t>(offsetof(VRTK_InteractUse_t4015307561, ___ControllerUseInteractableObject_2)); }
	inline ObjectInteractEventHandler_t1701902511 * get_ControllerUseInteractableObject_2() const { return ___ControllerUseInteractableObject_2; }
	inline ObjectInteractEventHandler_t1701902511 ** get_address_of_ControllerUseInteractableObject_2() { return &___ControllerUseInteractableObject_2; }
	inline void set_ControllerUseInteractableObject_2(ObjectInteractEventHandler_t1701902511 * value)
	{
		___ControllerUseInteractableObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerUseInteractableObject_2, value);
	}

	inline static int32_t get_offset_of_ControllerUnuseInteractableObject_3() { return static_cast<int32_t>(offsetof(VRTK_InteractUse_t4015307561, ___ControllerUnuseInteractableObject_3)); }
	inline ObjectInteractEventHandler_t1701902511 * get_ControllerUnuseInteractableObject_3() const { return ___ControllerUnuseInteractableObject_3; }
	inline ObjectInteractEventHandler_t1701902511 ** get_address_of_ControllerUnuseInteractableObject_3() { return &___ControllerUnuseInteractableObject_3; }
	inline void set_ControllerUnuseInteractableObject_3(ObjectInteractEventHandler_t1701902511 * value)
	{
		___ControllerUnuseInteractableObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerUnuseInteractableObject_3, value);
	}

	inline static int32_t get_offset_of_usingObject_4() { return static_cast<int32_t>(offsetof(VRTK_InteractUse_t4015307561, ___usingObject_4)); }
	inline GameObject_t1756533147 * get_usingObject_4() const { return ___usingObject_4; }
	inline GameObject_t1756533147 ** get_address_of_usingObject_4() { return &___usingObject_4; }
	inline void set_usingObject_4(GameObject_t1756533147 * value)
	{
		___usingObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___usingObject_4, value);
	}

	inline static int32_t get_offset_of_interactTouch_5() { return static_cast<int32_t>(offsetof(VRTK_InteractUse_t4015307561, ___interactTouch_5)); }
	inline VRTK_InteractTouch_t4022091061 * get_interactTouch_5() const { return ___interactTouch_5; }
	inline VRTK_InteractTouch_t4022091061 ** get_address_of_interactTouch_5() { return &___interactTouch_5; }
	inline void set_interactTouch_5(VRTK_InteractTouch_t4022091061 * value)
	{
		___interactTouch_5 = value;
		Il2CppCodeGenWriteBarrier(&___interactTouch_5, value);
	}

	inline static int32_t get_offset_of_controllerActions_6() { return static_cast<int32_t>(offsetof(VRTK_InteractUse_t4015307561, ___controllerActions_6)); }
	inline VRTK_ControllerActions_t3642353851 * get_controllerActions_6() const { return ___controllerActions_6; }
	inline VRTK_ControllerActions_t3642353851 ** get_address_of_controllerActions_6() { return &___controllerActions_6; }
	inline void set_controllerActions_6(VRTK_ControllerActions_t3642353851 * value)
	{
		___controllerActions_6 = value;
		Il2CppCodeGenWriteBarrier(&___controllerActions_6, value);
	}

	inline static int32_t get_offset_of_controllerEvents_7() { return static_cast<int32_t>(offsetof(VRTK_InteractUse_t4015307561, ___controllerEvents_7)); }
	inline VRTK_ControllerEvents_t3225224819 * get_controllerEvents_7() const { return ___controllerEvents_7; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_controllerEvents_7() { return &___controllerEvents_7; }
	inline void set_controllerEvents_7(VRTK_ControllerEvents_t3225224819 * value)
	{
		___controllerEvents_7 = value;
		Il2CppCodeGenWriteBarrier(&___controllerEvents_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
