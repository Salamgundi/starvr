﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.RadialMenu
struct RadialMenu_t1576296262;
// VRTK.HapticPulseEventHandler
struct HapticPulseEventHandler_t422205650;
// VRTK.RadialMenuButton
struct RadialMenuButton_t131156040;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_HapticPulseEventHandler422205650.h"
#include "AssemblyU2DCSharp_VRTK_ButtonEvent2865057236.h"
#include "AssemblyU2DCSharp_VRTK_RadialMenuButton131156040.h"

// System.Void VRTK.RadialMenu::.ctor()
extern "C"  void RadialMenu__ctor_m1555666192 (RadialMenu_t1576296262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::add_FireHapticPulse(VRTK.HapticPulseEventHandler)
extern "C"  void RadialMenu_add_FireHapticPulse_m2116441689 (RadialMenu_t1576296262 * __this, HapticPulseEventHandler_t422205650 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::remove_FireHapticPulse(VRTK.HapticPulseEventHandler)
extern "C"  void RadialMenu_remove_FireHapticPulse_m4218587700 (RadialMenu_t1576296262 * __this, HapticPulseEventHandler_t422205650 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::Awake()
extern "C"  void RadialMenu_Awake_m3261093007 (RadialMenu_t1576296262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::Update()
extern "C"  void RadialMenu_Update_m423433563 (RadialMenu_t1576296262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::InteractButton(System.Single,VRTK.ButtonEvent)
extern "C"  void RadialMenu_InteractButton_m1439958816 (RadialMenu_t1576296262 * __this, float ___angle0, int32_t ___evt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::HoverButton(System.Single)
extern "C"  void RadialMenu_HoverButton_m2750530867 (RadialMenu_t1576296262 * __this, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::ClickButton(System.Single)
extern "C"  void RadialMenu_ClickButton_m618105415 (RadialMenu_t1576296262 * __this, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::UnClickButton(System.Single)
extern "C"  void RadialMenu_UnClickButton_m1772991478 (RadialMenu_t1576296262 * __this, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::ToggleMenu()
extern "C"  void RadialMenu_ToggleMenu_m4176834135 (RadialMenu_t1576296262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::StopTouching()
extern "C"  void RadialMenu_StopTouching_m1557908591 (RadialMenu_t1576296262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::ShowMenu()
extern "C"  void RadialMenu_ShowMenu_m3656937908 (RadialMenu_t1576296262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.RadialMenuButton VRTK.RadialMenu::GetButton(System.Int32)
extern "C"  RadialMenuButton_t131156040 * RadialMenu_GetButton_m3166638651 (RadialMenu_t1576296262 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::HideMenu(System.Boolean)
extern "C"  void RadialMenu_HideMenu_m600998696 (RadialMenu_t1576296262 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.RadialMenu::TweenMenuScale(System.Boolean)
extern "C"  Il2CppObject * RadialMenu_TweenMenuScale_m382015317 (RadialMenu_t1576296262 * __this, bool ___show0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::AttempHapticPulse(System.Single)
extern "C"  void RadialMenu_AttempHapticPulse_m4076653462 (RadialMenu_t1576296262 * __this, float ___strength0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::RegenerateButtons()
extern "C"  void RadialMenu_RegenerateButtons_m3528811895 (RadialMenu_t1576296262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::AddButton(VRTK.RadialMenuButton)
extern "C"  void RadialMenu_AddButton_m1112752472 (RadialMenu_t1576296262 * __this, RadialMenuButton_t131156040 * ___newButton0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.RadialMenu::RemoveAllButtons()
extern "C"  void RadialMenu_RemoveAllButtons_m1093791638 (RadialMenu_t1576296262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.RadialMenu::mod(System.Single,System.Single)
extern "C"  float RadialMenu_mod_m719269952 (RadialMenu_t1576296262 * __this, float ___a0, float ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
