﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectFollow3175963762.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TransformFollow_Follow1516682274.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_TransformFollow
struct  VRTK_TransformFollow_t3532748285  : public VRTK_ObjectFollow_t3175963762
{
public:
	// VRTK.VRTK_TransformFollow/FollowMoment VRTK.VRTK_TransformFollow::moment
	int32_t ___moment_16;
	// UnityEngine.Transform VRTK.VRTK_TransformFollow::transformToFollow
	Transform_t3275118058 * ___transformToFollow_17;
	// UnityEngine.Transform VRTK.VRTK_TransformFollow::transformToChange
	Transform_t3275118058 * ___transformToChange_18;
	// System.Boolean VRTK.VRTK_TransformFollow::isListeningToOnPreRender
	bool ___isListeningToOnPreRender_19;

public:
	inline static int32_t get_offset_of_moment_16() { return static_cast<int32_t>(offsetof(VRTK_TransformFollow_t3532748285, ___moment_16)); }
	inline int32_t get_moment_16() const { return ___moment_16; }
	inline int32_t* get_address_of_moment_16() { return &___moment_16; }
	inline void set_moment_16(int32_t value)
	{
		___moment_16 = value;
	}

	inline static int32_t get_offset_of_transformToFollow_17() { return static_cast<int32_t>(offsetof(VRTK_TransformFollow_t3532748285, ___transformToFollow_17)); }
	inline Transform_t3275118058 * get_transformToFollow_17() const { return ___transformToFollow_17; }
	inline Transform_t3275118058 ** get_address_of_transformToFollow_17() { return &___transformToFollow_17; }
	inline void set_transformToFollow_17(Transform_t3275118058 * value)
	{
		___transformToFollow_17 = value;
		Il2CppCodeGenWriteBarrier(&___transformToFollow_17, value);
	}

	inline static int32_t get_offset_of_transformToChange_18() { return static_cast<int32_t>(offsetof(VRTK_TransformFollow_t3532748285, ___transformToChange_18)); }
	inline Transform_t3275118058 * get_transformToChange_18() const { return ___transformToChange_18; }
	inline Transform_t3275118058 ** get_address_of_transformToChange_18() { return &___transformToChange_18; }
	inline void set_transformToChange_18(Transform_t3275118058 * value)
	{
		___transformToChange_18 = value;
		Il2CppCodeGenWriteBarrier(&___transformToChange_18, value);
	}

	inline static int32_t get_offset_of_isListeningToOnPreRender_19() { return static_cast<int32_t>(offsetof(VRTK_TransformFollow_t3532748285, ___isListeningToOnPreRender_19)); }
	inline bool get_isListeningToOnPreRender_19() const { return ___isListeningToOnPreRender_19; }
	inline bool* get_address_of_isListeningToOnPreRender_19() { return &___isListeningToOnPreRender_19; }
	inline void set_isListeningToOnPreRender_19(bool value)
	{
		___isListeningToOnPreRender_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
