﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_ResetSeatedZeroPose
struct _ResetSeatedZeroPose_t3471614486;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_ResetSeatedZeroPose::.ctor(System.Object,System.IntPtr)
extern "C"  void _ResetSeatedZeroPose__ctor_m3164115519 (_ResetSeatedZeroPose_t3471614486 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_ResetSeatedZeroPose::Invoke()
extern "C"  void _ResetSeatedZeroPose_Invoke_m1696818299 (_ResetSeatedZeroPose_t3471614486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_ResetSeatedZeroPose::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ResetSeatedZeroPose_BeginInvoke_m3541701336 (_ResetSeatedZeroPose_t3471614486 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_ResetSeatedZeroPose::EndInvoke(System.IAsyncResult)
extern "C"  void _ResetSeatedZeroPose_EndInvoke_m2933657993 (_ResetSeatedZeroPose_t3471614486 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
