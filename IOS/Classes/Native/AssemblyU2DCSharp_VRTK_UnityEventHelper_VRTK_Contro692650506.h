﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_Control
struct VRTK_Control_t651619021;
// VRTK.UnityEventHelper.VRTK_Control_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t3966949053;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_Control_UnityEvents
struct  VRTK_Control_UnityEvents_t692650506  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_Control VRTK.UnityEventHelper.VRTK_Control_UnityEvents::c3d
	VRTK_Control_t651619021 * ___c3d_2;
	// VRTK.UnityEventHelper.VRTK_Control_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_Control_UnityEvents::OnValueChanged
	UnityObjectEvent_t3966949053 * ___OnValueChanged_3;

public:
	inline static int32_t get_offset_of_c3d_2() { return static_cast<int32_t>(offsetof(VRTK_Control_UnityEvents_t692650506, ___c3d_2)); }
	inline VRTK_Control_t651619021 * get_c3d_2() const { return ___c3d_2; }
	inline VRTK_Control_t651619021 ** get_address_of_c3d_2() { return &___c3d_2; }
	inline void set_c3d_2(VRTK_Control_t651619021 * value)
	{
		___c3d_2 = value;
		Il2CppCodeGenWriteBarrier(&___c3d_2, value);
	}

	inline static int32_t get_offset_of_OnValueChanged_3() { return static_cast<int32_t>(offsetof(VRTK_Control_UnityEvents_t692650506, ___OnValueChanged_3)); }
	inline UnityObjectEvent_t3966949053 * get_OnValueChanged_3() const { return ___OnValueChanged_3; }
	inline UnityObjectEvent_t3966949053 ** get_address_of_OnValueChanged_3() { return &___OnValueChanged_3; }
	inline void set_OnValueChanged_3(UnityObjectEvent_t3966949053 * value)
	{
		___OnValueChanged_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnValueChanged_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
