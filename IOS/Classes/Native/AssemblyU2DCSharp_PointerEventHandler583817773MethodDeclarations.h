﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PointerEventHandler
struct PointerEventHandler_t583817773;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_PointerEventArgs4020535570.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void PointerEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void PointerEventHandler__ctor_m3057275488 (PointerEventHandler_t583817773 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointerEventHandler::Invoke(System.Object,PointerEventArgs)
extern "C"  void PointerEventHandler_Invoke_m1554956502 (PointerEventHandler_t583817773 * __this, Il2CppObject * ___sender0, PointerEventArgs_t4020535570  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult PointerEventHandler::BeginInvoke(System.Object,PointerEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PointerEventHandler_BeginInvoke_m2418470709 (PointerEventHandler_t583817773 * __this, Il2CppObject * ___sender0, PointerEventArgs_t4020535570  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointerEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void PointerEventHandler_EndInvoke_m91844190 (PointerEventHandler_t583817773 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
