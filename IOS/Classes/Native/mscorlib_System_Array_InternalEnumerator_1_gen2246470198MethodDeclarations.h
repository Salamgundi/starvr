﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2246470198.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1387717936.h"

// System.Void System.Array/InternalEnumerator`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2417358897_gshared (InternalEnumerator_1_t2246470198 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2417358897(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2246470198 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2417358897_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m656560541_gshared (InternalEnumerator_1_t2246470198 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m656560541(__this, method) ((  void (*) (InternalEnumerator_1_t2246470198 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m656560541_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4084398445_gshared (InternalEnumerator_1_t2246470198 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4084398445(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2246470198 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4084398445_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2699591608_gshared (InternalEnumerator_1_t2246470198 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2699591608(__this, method) ((  void (*) (InternalEnumerator_1_t2246470198 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2699591608_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1447358581_gshared (InternalEnumerator_1_t2246470198 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1447358581(__this, method) ((  bool (*) (InternalEnumerator_1_t2246470198 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1447358581_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::get_Current()
extern "C"  AttachedObject_t1387717936  InternalEnumerator_1_get_Current_m2929631186_gshared (InternalEnumerator_1_t2246470198 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2929631186(__this, method) ((  AttachedObject_t1387717936  (*) (InternalEnumerator_1_t2246470198 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2929631186_gshared)(__this, method)
