﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.TeleportPoint
struct TeleportPoint_t3942003985;
// UnityEngine.Material
struct Material_t193706927;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void Valve.VR.InteractionSystem.TeleportPoint::.ctor()
extern "C"  void TeleportPoint__ctor_m201007567 (TeleportPoint_t3942003985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.TeleportPoint::get_showReticle()
extern "C"  bool TeleportPoint_get_showReticle_m2565738765 (TeleportPoint_t3942003985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportPoint::Awake()
extern "C"  void TeleportPoint_Awake_m2091649618 (TeleportPoint_t3942003985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportPoint::Start()
extern "C"  void TeleportPoint_Start_m1725729779 (TeleportPoint_t3942003985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportPoint::Update()
extern "C"  void TeleportPoint_Update_m1068339856 (TeleportPoint_t3942003985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.TeleportPoint::ShouldActivate(UnityEngine.Vector3)
extern "C"  bool TeleportPoint_ShouldActivate_m3616915998 (TeleportPoint_t3942003985 * __this, Vector3_t2243707580  ___playerPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.TeleportPoint::ShouldMovePlayer()
extern "C"  bool TeleportPoint_ShouldMovePlayer_m1597422920 (TeleportPoint_t3942003985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportPoint::Highlight(System.Boolean)
extern "C"  void TeleportPoint_Highlight_m2900728652 (TeleportPoint_t3942003985 * __this, bool ___highlight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportPoint::UpdateVisuals()
extern "C"  void TeleportPoint_UpdateVisuals_m1557667761 (TeleportPoint_t3942003985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportPoint::SetAlpha(System.Single,System.Single)
extern "C"  void TeleportPoint_SetAlpha_m1919113849 (TeleportPoint_t3942003985 * __this, float ___tintAlpha0, float ___alphaPercent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportPoint::SetMeshMaterials(UnityEngine.Material,UnityEngine.Color)
extern "C"  void TeleportPoint_SetMeshMaterials_m2948693256 (TeleportPoint_t3942003985 * __this, Material_t193706927 * ___material0, Color_t2020392075  ___textColor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportPoint::TeleportToScene()
extern "C"  void TeleportPoint_TeleportToScene_m937067171 (TeleportPoint_t3942003985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportPoint::GetRelevantComponents()
extern "C"  void TeleportPoint_GetRelevantComponents_m1142571432 (TeleportPoint_t3942003985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportPoint::ReleaseRelevantComponents()
extern "C"  void TeleportPoint_ReleaseRelevantComponents_m3542128453 (TeleportPoint_t3942003985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportPoint::UpdateVisualsInEditor()
extern "C"  void TeleportPoint_UpdateVisualsInEditor_m2499645203 (TeleportPoint_t3942003985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
