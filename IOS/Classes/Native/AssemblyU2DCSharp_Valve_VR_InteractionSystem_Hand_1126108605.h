﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.Hand/<DetachObject>c__AnonStorey1
struct  U3CDetachObjectU3Ec__AnonStorey1_t1126108605  : public Il2CppObject
{
public:
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Hand/<DetachObject>c__AnonStorey1::objectToDetach
	GameObject_t1756533147 * ___objectToDetach_0;

public:
	inline static int32_t get_offset_of_objectToDetach_0() { return static_cast<int32_t>(offsetof(U3CDetachObjectU3Ec__AnonStorey1_t1126108605, ___objectToDetach_0)); }
	inline GameObject_t1756533147 * get_objectToDetach_0() const { return ___objectToDetach_0; }
	inline GameObject_t1756533147 ** get_address_of_objectToDetach_0() { return &___objectToDetach_0; }
	inline void set_objectToDetach_0(GameObject_t1756533147 * value)
	{
		___objectToDetach_0 = value;
		Il2CppCodeGenWriteBarrier(&___objectToDetach_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
