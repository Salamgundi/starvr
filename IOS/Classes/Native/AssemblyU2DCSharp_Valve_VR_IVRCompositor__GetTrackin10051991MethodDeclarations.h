﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_GetTrackingSpace
struct _GetTrackingSpace_t10051991;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingUniverseOrigin1464400093.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_GetTrackingSpace::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetTrackingSpace__ctor_m3480937050 (_GetTrackingSpace_t10051991 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.ETrackingUniverseOrigin Valve.VR.IVRCompositor/_GetTrackingSpace::Invoke()
extern "C"  int32_t _GetTrackingSpace_Invoke_m3027427904 (_GetTrackingSpace_t10051991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_GetTrackingSpace::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetTrackingSpace_BeginInvoke_m786844505 (_GetTrackingSpace_t10051991 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.ETrackingUniverseOrigin Valve.VR.IVRCompositor/_GetTrackingSpace::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _GetTrackingSpace_EndInvoke_m3309768912 (_GetTrackingSpace_t10051991 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
