﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ViveControllerInput
struct ViveControllerInput_t1577066400;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void ViveControllerInput::.ctor()
extern "C"  void ViveControllerInput__ctor_m1864784221 (ViveControllerInput_t1577066400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ViveControllerInput::Start()
extern "C"  void ViveControllerInput_Start_m2008975781 (ViveControllerInput_t1577066400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ViveControllerInput::GetLookPointerEventData(System.Int32)
extern "C"  bool ViveControllerInput_GetLookPointerEventData_m2506807544 (ViveControllerInput_t1577066400 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ViveControllerInput::UpdateCursor(System.Int32,UnityEngine.EventSystems.PointerEventData)
extern "C"  void ViveControllerInput_UpdateCursor_m1386049723 (ViveControllerInput_t1577066400 * __this, int32_t ___index0, PointerEventData_t1599784723 * ___pointData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ViveControllerInput::ClearSelection()
extern "C"  void ViveControllerInput_ClearSelection_m1354062958 (ViveControllerInput_t1577066400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ViveControllerInput::Select(UnityEngine.GameObject)
extern "C"  void ViveControllerInput_Select_m1012109777 (ViveControllerInput_t1577066400 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ViveControllerInput::SendUpdateEventToSelectedObject()
extern "C"  bool ViveControllerInput_SendUpdateEventToSelectedObject_m2906015151 (ViveControllerInput_t1577066400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ViveControllerInput::UpdateCameraPosition(System.Int32)
extern "C"  void ViveControllerInput_UpdateCameraPosition_m1681571385 (ViveControllerInput_t1577066400 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ViveControllerInput::InitializeControllers()
extern "C"  void ViveControllerInput_InitializeControllers_m1481665832 (ViveControllerInput_t1577066400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ViveControllerInput::Process()
extern "C"  void ViveControllerInput_Process_m1061887390 (ViveControllerInput_t1577066400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ViveControllerInput::ButtonDown(System.Int32)
extern "C"  bool ViveControllerInput_ButtonDown_m3945623188 (ViveControllerInput_t1577066400 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ViveControllerInput::ButtonUp(System.Int32)
extern "C"  bool ViveControllerInput_ButtonUp_m4258280427 (ViveControllerInput_t1577066400 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
