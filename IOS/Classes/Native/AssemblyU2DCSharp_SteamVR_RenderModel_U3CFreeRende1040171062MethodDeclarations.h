﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_RenderModel/<FreeRenderModel>c__Iterator1
struct U3CFreeRenderModelU3Ec__Iterator1_t1040171062;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_RenderModel/<FreeRenderModel>c__Iterator1::.ctor()
extern "C"  void U3CFreeRenderModelU3Ec__Iterator1__ctor_m3471182943 (U3CFreeRenderModelU3Ec__Iterator1_t1040171062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_RenderModel/<FreeRenderModel>c__Iterator1::MoveNext()
extern "C"  bool U3CFreeRenderModelU3Ec__Iterator1_MoveNext_m4179148737 (U3CFreeRenderModelU3Ec__Iterator1_t1040171062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SteamVR_RenderModel/<FreeRenderModel>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFreeRenderModelU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1958576465 (U3CFreeRenderModelU3Ec__Iterator1_t1040171062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SteamVR_RenderModel/<FreeRenderModel>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFreeRenderModelU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1757808745 (U3CFreeRenderModelU3Ec__Iterator1_t1040171062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel/<FreeRenderModel>c__Iterator1::Dispose()
extern "C"  void U3CFreeRenderModelU3Ec__Iterator1_Dispose_m148053024 (U3CFreeRenderModelU3Ec__Iterator1_t1040171062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel/<FreeRenderModel>c__Iterator1::Reset()
extern "C"  void U3CFreeRenderModelU3Ec__Iterator1_Reset_m4287945378 (U3CFreeRenderModelU3Ec__Iterator1_t1040171062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
