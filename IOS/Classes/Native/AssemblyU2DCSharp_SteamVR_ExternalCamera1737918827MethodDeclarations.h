﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_ExternalCamera
struct SteamVR_ExternalCamera_t1737918827;
// SteamVR_Camera
struct SteamVR_Camera_t3632348390;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SteamVR_Camera3632348390.h"

// System.Void SteamVR_ExternalCamera::.ctor()
extern "C"  void SteamVR_ExternalCamera__ctor_m278576724 (SteamVR_ExternalCamera_t1737918827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ExternalCamera::ReadConfig()
extern "C"  void SteamVR_ExternalCamera_ReadConfig_m1929011568 (SteamVR_ExternalCamera_t1737918827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ExternalCamera::AttachToCamera(SteamVR_Camera)
extern "C"  void SteamVR_ExternalCamera_AttachToCamera_m1072751751 (SteamVR_ExternalCamera_t1737918827 * __this, SteamVR_Camera_t3632348390 * ___vrcam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR_ExternalCamera::GetTargetDistance()
extern "C"  float SteamVR_ExternalCamera_GetTargetDistance_m1189912356 (SteamVR_ExternalCamera_t1737918827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ExternalCamera::RenderNear()
extern "C"  void SteamVR_ExternalCamera_RenderNear_m3416447468 (SteamVR_ExternalCamera_t1737918827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ExternalCamera::RenderFar()
extern "C"  void SteamVR_ExternalCamera_RenderFar_m1513521889 (SteamVR_ExternalCamera_t1737918827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ExternalCamera::OnGUI()
extern "C"  void SteamVR_ExternalCamera_OnGUI_m2857883712 (SteamVR_ExternalCamera_t1737918827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ExternalCamera::OnEnable()
extern "C"  void SteamVR_ExternalCamera_OnEnable_m2993646488 (SteamVR_ExternalCamera_t1737918827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ExternalCamera::OnDisable()
extern "C"  void SteamVR_ExternalCamera_OnDisable_m2655753451 (SteamVR_ExternalCamera_t1737918827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
