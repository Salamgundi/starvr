﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_FallbackSystem
struct SDK_FallbackSystem_t1344157064;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_FallbackSystem::.ctor()
extern "C"  void SDK_FallbackSystem__ctor_m3926005538 (SDK_FallbackSystem_t1344157064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackSystem::IsDisplayOnDesktop()
extern "C"  bool SDK_FallbackSystem_IsDisplayOnDesktop_m2116247127 (SDK_FallbackSystem_t1344157064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackSystem::ShouldAppRenderWithLowResources()
extern "C"  bool SDK_FallbackSystem_ShouldAppRenderWithLowResources_m2213751789 (SDK_FallbackSystem_t1344157064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_FallbackSystem::ForceInterleavedReprojectionOn(System.Boolean)
extern "C"  void SDK_FallbackSystem_ForceInterleavedReprojectionOn_m45106262 (SDK_FallbackSystem_t1344157064 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_FallbackSystem::Awake()
extern "C"  void SDK_FallbackSystem_Awake_m4112156961 (SDK_FallbackSystem_t1344157064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
