﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectInteractEventArgs>
struct UnityEvent_2_t3748945147;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.ObjectInteractEventArgs>
struct UnityAction_2_t1866747229;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventArgs771291242.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectInteractEventArgs>::.ctor()
extern "C"  void UnityEvent_2__ctor_m516456261_gshared (UnityEvent_2_t3748945147 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m516456261(__this, method) ((  void (*) (UnityEvent_2_t3748945147 *, const MethodInfo*))UnityEvent_2__ctor_m516456261_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectInteractEventArgs>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m566246061_gshared (UnityEvent_2_t3748945147 * __this, UnityAction_2_t1866747229 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m566246061(__this, ___call0, method) ((  void (*) (UnityEvent_2_t3748945147 *, UnityAction_2_t1866747229 *, const MethodInfo*))UnityEvent_2_AddListener_m566246061_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectInteractEventArgs>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m1271145780_gshared (UnityEvent_2_t3748945147 * __this, UnityAction_2_t1866747229 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m1271145780(__this, ___call0, method) ((  void (*) (UnityEvent_2_t3748945147 *, UnityAction_2_t1866747229 *, const MethodInfo*))UnityEvent_2_RemoveListener_m1271145780_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectInteractEventArgs>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m3795942517_gshared (UnityEvent_2_t3748945147 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m3795942517(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t3748945147 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m3795942517_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectInteractEventArgs>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m1087493645_gshared (UnityEvent_2_t3748945147 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m1087493645(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t3748945147 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m1087493645_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectInteractEventArgs>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m3298790806_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t1866747229 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m3298790806(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t1866747229 *, const MethodInfo*))UnityEvent_2_GetDelegate_m3298790806_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.ObjectInteractEventArgs>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m1511370890_gshared (UnityEvent_2_t3748945147 * __this, Il2CppObject * ___arg00, ObjectInteractEventArgs_t771291242  ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m1511370890(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t3748945147 *, Il2CppObject *, ObjectInteractEventArgs_t771291242 , const MethodInfo*))UnityEvent_2_Invoke_m1511370890_gshared)(__this, ___arg00, ___arg11, method)
