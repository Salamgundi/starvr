﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.HeadsetFadeEventHandler
struct HeadsetFadeEventHandler_t2012619754;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetFadeEventArgs2892542019.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.HeadsetFadeEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void HeadsetFadeEventHandler__ctor_m2118547718 (HeadsetFadeEventHandler_t2012619754 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.HeadsetFadeEventHandler::Invoke(System.Object,VRTK.HeadsetFadeEventArgs)
extern "C"  void HeadsetFadeEventHandler_Invoke_m134876260 (HeadsetFadeEventHandler_t2012619754 * __this, Il2CppObject * ___sender0, HeadsetFadeEventArgs_t2892542019  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.HeadsetFadeEventHandler::BeginInvoke(System.Object,VRTK.HeadsetFadeEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * HeadsetFadeEventHandler_BeginInvoke_m216089249 (HeadsetFadeEventHandler_t2012619754 * __this, Il2CppObject * ___sender0, HeadsetFadeEventArgs_t2892542019  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.HeadsetFadeEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void HeadsetFadeEventHandler_EndInvoke_m3077588604 (HeadsetFadeEventHandler_t2012619754 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
