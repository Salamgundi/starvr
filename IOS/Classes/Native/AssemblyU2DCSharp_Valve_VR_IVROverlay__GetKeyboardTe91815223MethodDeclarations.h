﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetKeyboardText
struct _GetKeyboardText_t91815223;
// System.Object
struct Il2CppObject;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetKeyboardText::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetKeyboardText__ctor_m3179068316 (_GetKeyboardText_t91815223 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVROverlay/_GetKeyboardText::Invoke(System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t _GetKeyboardText_Invoke_m3621408937 (_GetKeyboardText_t91815223 * __this, StringBuilder_t1221177846 * ___pchText0, uint32_t ___cchText1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetKeyboardText::BeginInvoke(System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetKeyboardText_BeginInvoke_m3630941361 (_GetKeyboardText_t91815223 * __this, StringBuilder_t1221177846 * ___pchText0, uint32_t ___cchText1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVROverlay/_GetKeyboardText::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetKeyboardText_EndInvoke_m1319628103 (_GetKeyboardText_t91815223 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
