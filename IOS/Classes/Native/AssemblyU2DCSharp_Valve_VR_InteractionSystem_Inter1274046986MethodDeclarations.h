﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Interactable
struct Interactable_t1274046986;
// Valve.VR.InteractionSystem.Interactable/OnAttachedToHandDelegate
struct OnAttachedToHandDelegate_t3380341848;
// Valve.VR.InteractionSystem.Interactable/OnDetachedFromHandDelegate
struct OnDetachedFromHandDelegate_t2257352405;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Inter3380341848.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Inter2257352405.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"

// System.Void Valve.VR.InteractionSystem.Interactable::.ctor()
extern "C"  void Interactable__ctor_m1598080024 (Interactable_t1274046986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Interactable::add_onAttachedToHand(Valve.VR.InteractionSystem.Interactable/OnAttachedToHandDelegate)
extern "C"  void Interactable_add_onAttachedToHand_m3281136413 (Interactable_t1274046986 * __this, OnAttachedToHandDelegate_t3380341848 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Interactable::remove_onAttachedToHand(Valve.VR.InteractionSystem.Interactable/OnAttachedToHandDelegate)
extern "C"  void Interactable_remove_onAttachedToHand_m3209191768 (Interactable_t1274046986 * __this, OnAttachedToHandDelegate_t3380341848 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Interactable::add_onDetachedFromHand(Valve.VR.InteractionSystem.Interactable/OnDetachedFromHandDelegate)
extern "C"  void Interactable_add_onDetachedFromHand_m2411084705 (Interactable_t1274046986 * __this, OnDetachedFromHandDelegate_t2257352405 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Interactable::remove_onDetachedFromHand(Valve.VR.InteractionSystem.Interactable/OnDetachedFromHandDelegate)
extern "C"  void Interactable_remove_onDetachedFromHand_m2506970706 (Interactable_t1274046986 * __this, OnDetachedFromHandDelegate_t2257352405 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Interactable::OnAttachedToHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void Interactable_OnAttachedToHand_m1309423791 (Interactable_t1274046986 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Interactable::OnDetachedFromHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void Interactable_OnDetachedFromHand_m1915377768 (Interactable_t1274046986 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
