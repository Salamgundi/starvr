﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`3<UnityEngine.Color,System.Single,System.Boolean>
struct UnityEvent_3_t1483099642;
// UnityEngine.Events.UnityAction`3<UnityEngine.Color,System.Single,System.Boolean>
struct UnityAction_3_t1816056522;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void UnityEngine.Events.UnityEvent`3<UnityEngine.Color,System.Single,System.Boolean>::.ctor()
extern "C"  void UnityEvent_3__ctor_m462109996_gshared (UnityEvent_3_t1483099642 * __this, const MethodInfo* method);
#define UnityEvent_3__ctor_m462109996(__this, method) ((  void (*) (UnityEvent_3_t1483099642 *, const MethodInfo*))UnityEvent_3__ctor_m462109996_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`3<UnityEngine.Color,System.Single,System.Boolean>::AddListener(UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void UnityEvent_3_AddListener_m43529516_gshared (UnityEvent_3_t1483099642 * __this, UnityAction_3_t1816056522 * ___call0, const MethodInfo* method);
#define UnityEvent_3_AddListener_m43529516(__this, ___call0, method) ((  void (*) (UnityEvent_3_t1483099642 *, UnityAction_3_t1816056522 *, const MethodInfo*))UnityEvent_3_AddListener_m43529516_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`3<UnityEngine.Color,System.Single,System.Boolean>::RemoveListener(UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void UnityEvent_3_RemoveListener_m235386003_gshared (UnityEvent_3_t1483099642 * __this, UnityAction_3_t1816056522 * ___call0, const MethodInfo* method);
#define UnityEvent_3_RemoveListener_m235386003(__this, ___call0, method) ((  void (*) (UnityEvent_3_t1483099642 *, UnityAction_3_t1816056522 *, const MethodInfo*))UnityEvent_3_RemoveListener_m235386003_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3<UnityEngine.Color,System.Single,System.Boolean>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_3_FindMethod_Impl_m1546428567_gshared (UnityEvent_3_t1483099642 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_3_FindMethod_Impl_m1546428567(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_3_t1483099642 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_3_FindMethod_Impl_m1546428567_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3<UnityEngine.Color,System.Single,System.Boolean>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_3_GetDelegate_m1101510583_gshared (UnityEvent_3_t1483099642 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_3_GetDelegate_m1101510583(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_3_t1483099642 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_3_GetDelegate_m1101510583_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3<UnityEngine.Color,System.Single,System.Boolean>::GetDelegate(UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_3_GetDelegate_m3848801799_gshared (Il2CppObject * __this /* static, unused */, UnityAction_3_t1816056522 * ___action0, const MethodInfo* method);
#define UnityEvent_3_GetDelegate_m3848801799(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_3_t1816056522 *, const MethodInfo*))UnityEvent_3_GetDelegate_m3848801799_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`3<UnityEngine.Color,System.Single,System.Boolean>::Invoke(T0,T1,T2)
extern "C"  void UnityEvent_3_Invoke_m2489222237_gshared (UnityEvent_3_t1483099642 * __this, Color_t2020392075  ___arg00, float ___arg11, bool ___arg22, const MethodInfo* method);
#define UnityEvent_3_Invoke_m2489222237(__this, ___arg00, ___arg11, ___arg22, method) ((  void (*) (UnityEvent_3_t1483099642 *, Color_t2020392075 , float, bool, const MethodInfo*))UnityEvent_3_Invoke_m2489222237_gshared)(__this, ___arg00, ___arg11, ___arg22, method)
