﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.BodyCollider
struct BodyCollider_t3516213392;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.BodyCollider::.ctor()
extern "C"  void BodyCollider__ctor_m3923259378 (BodyCollider_t3516213392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.BodyCollider::Awake()
extern "C"  void BodyCollider_Awake_m3719405537 (BodyCollider_t3516213392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.BodyCollider::FixedUpdate()
extern "C"  void BodyCollider_FixedUpdate_m1487362379 (BodyCollider_t3516213392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
