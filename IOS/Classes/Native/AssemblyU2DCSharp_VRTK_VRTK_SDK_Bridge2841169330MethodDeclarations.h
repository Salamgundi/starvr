﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_SDK_Bridge
struct VRTK_SDK_Bridge_t2841169330;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.SDK_ControllerHapticModifiers
struct SDK_ControllerHapticModifiers_t3871094838;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// VRTK.SDK_BaseSystem
struct SDK_BaseSystem_t244469351;
// VRTK.SDK_BaseHeadset
struct SDK_BaseHeadset_t3046873914;
// VRTK.SDK_BaseController
struct SDK_BaseController_t197168236;
// VRTK.SDK_BaseBoundaries
struct SDK_BaseBoundaries_t1766380066;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_SDK_BaseController_Controlle246418309.h"
#include "AssemblyU2DCSharp_VRTK_SDK_BaseController_Controlle447683143.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void VRTK.VRTK_SDK_Bridge::.ctor()
extern "C"  void VRTK_SDK_Bridge__ctor_m3486178448 (VRTK_SDK_Bridge_t2841169330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SDK_Bridge::HeadsetProcessUpdate(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void VRTK_SDK_Bridge_HeadsetProcessUpdate_m638289833 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SDK_Bridge::ControllerProcessUpdate(System.UInt32,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void VRTK_SDK_Bridge_ControllerProcessUpdate_m116141295 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, Dictionary_2_t309261261 * ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VRTK.VRTK_SDK_Bridge::GetControllerDefaultColliderPath(VRTK.SDK_BaseController/ControllerHand)
extern "C"  String_t* VRTK_SDK_Bridge_GetControllerDefaultColliderPath_m508638080 (Il2CppObject * __this /* static, unused */, int32_t ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VRTK.VRTK_SDK_Bridge::GetControllerElementPath(VRTK.SDK_BaseController/ControllerElements,VRTK.SDK_BaseController/ControllerHand,System.Boolean)
extern "C"  String_t* VRTK_SDK_Bridge_GetControllerElementPath_m314351791 (Il2CppObject * __this /* static, unused */, int32_t ___element0, int32_t ___hand1, bool ___fullPath2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 VRTK.VRTK_SDK_Bridge::GetControllerIndex(UnityEngine.GameObject)
extern "C"  uint32_t VRTK_SDK_Bridge_GetControllerIndex_m2377145161 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_SDK_Bridge::GetControllerByIndex(System.UInt32,System.Boolean)
extern "C"  GameObject_t1756533147 * VRTK_SDK_Bridge_GetControllerByIndex_m1136981449 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, bool ___actual1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_SDK_Bridge::GetControllerOrigin(UnityEngine.GameObject)
extern "C"  Transform_t3275118058 * VRTK_SDK_Bridge_GetControllerOrigin_m1162301662 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_SDK_Bridge::GenerateControllerPointerOrigin(UnityEngine.GameObject)
extern "C"  Transform_t3275118058 * VRTK_SDK_Bridge_GenerateControllerPointerOrigin_m3971638110 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_SDK_Bridge::GetControllerLeftHand(System.Boolean)
extern "C"  GameObject_t1756533147 * VRTK_SDK_Bridge_GetControllerLeftHand_m1434923878 (Il2CppObject * __this /* static, unused */, bool ___actual0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_SDK_Bridge::GetControllerRightHand(System.Boolean)
extern "C"  GameObject_t1756533147 * VRTK_SDK_Bridge_GetControllerRightHand_m3499063011 (Il2CppObject * __this /* static, unused */, bool ___actual0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsControllerLeftHand(UnityEngine.GameObject)
extern "C"  bool VRTK_SDK_Bridge_IsControllerLeftHand_m4225822554 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsControllerRightHand(UnityEngine.GameObject)
extern "C"  bool VRTK_SDK_Bridge_IsControllerRightHand_m3539854495 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsControllerLeftHand(UnityEngine.GameObject,System.Boolean)
extern "C"  bool VRTK_SDK_Bridge_IsControllerLeftHand_m2838031131 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___controller0, bool ___actual1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsControllerRightHand(UnityEngine.GameObject,System.Boolean)
extern "C"  bool VRTK_SDK_Bridge_IsControllerRightHand_m1661131950 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___controller0, bool ___actual1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_SDK_Bridge::GetControllerModel(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * VRTK_SDK_Bridge_GetControllerModel_m3735448532 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_SDK_Bridge::GetControllerModel(VRTK.SDK_BaseController/ControllerHand)
extern "C"  GameObject_t1756533147 * VRTK_SDK_Bridge_GetControllerModel_m2660041235 (Il2CppObject * __this /* static, unused */, int32_t ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_SDK_Bridge::GetControllerRenderModel(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * VRTK_SDK_Bridge_GetControllerRenderModel_m3663027324 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SDK_Bridge::SetControllerRenderModelWheel(UnityEngine.GameObject,System.Boolean)
extern "C"  void VRTK_SDK_Bridge_SetControllerRenderModelWheel_m3652369569 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___renderModel0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SDK_Bridge::HapticPulseOnIndex(System.UInt32,System.Single)
extern "C"  void VRTK_SDK_Bridge_HapticPulseOnIndex_m1072549706 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, float ___strength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.SDK_ControllerHapticModifiers VRTK.VRTK_SDK_Bridge::GetHapticModifiers()
extern "C"  SDK_ControllerHapticModifiers_t3871094838 * VRTK_SDK_Bridge_GetHapticModifiers_m2281072397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_SDK_Bridge::GetVelocityOnIndex(System.UInt32)
extern "C"  Vector3_t2243707580  VRTK_SDK_Bridge_GetVelocityOnIndex_m1660869012 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_SDK_Bridge::GetAngularVelocityOnIndex(System.UInt32)
extern "C"  Vector3_t2243707580  VRTK_SDK_Bridge_GetAngularVelocityOnIndex_m61919024 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_SDK_Bridge::GetHeadsetVelocity()
extern "C"  Vector3_t2243707580  VRTK_SDK_Bridge_GetHeadsetVelocity_m1600660947 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_SDK_Bridge::GetHeadsetAngularVelocity()
extern "C"  Vector3_t2243707580  VRTK_SDK_Bridge_GetHeadsetAngularVelocity_m1600527169 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 VRTK.VRTK_SDK_Bridge::GetTouchpadAxisOnIndex(System.UInt32)
extern "C"  Vector2_t2243707579  VRTK_SDK_Bridge_GetTouchpadAxisOnIndex_m956128331 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 VRTK.VRTK_SDK_Bridge::GetTriggerAxisOnIndex(System.UInt32)
extern "C"  Vector2_t2243707579  VRTK_SDK_Bridge_GetTriggerAxisOnIndex_m4089235175 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 VRTK.VRTK_SDK_Bridge::GetGripAxisOnIndex(System.UInt32)
extern "C"  Vector2_t2243707579  VRTK_SDK_Bridge_GetGripAxisOnIndex_m2082721503 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_SDK_Bridge::GetTriggerHairlineDeltaOnIndex(System.UInt32)
extern "C"  float VRTK_SDK_Bridge_GetTriggerHairlineDeltaOnIndex_m1809212317 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_SDK_Bridge::GetGripHairlineDeltaOnIndex(System.UInt32)
extern "C"  float VRTK_SDK_Bridge_GetGripHairlineDeltaOnIndex_m3089906761 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsTriggerPressedOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsTriggerPressedOnIndex_m2854744469 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsTriggerPressedDownOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsTriggerPressedDownOnIndex_m1929655103 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsTriggerPressedUpOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsTriggerPressedUpOnIndex_m2429922702 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsTriggerTouchedOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsTriggerTouchedOnIndex_m1360646415 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsTriggerTouchedDownOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsTriggerTouchedDownOnIndex_m3675494385 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsTriggerTouchedUpOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsTriggerTouchedUpOnIndex_m318684502 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsHairTriggerDownOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsHairTriggerDownOnIndex_m388589599 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsHairTriggerUpOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsHairTriggerUpOnIndex_m1046494550 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsGripPressedOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsGripPressedOnIndex_m4186835997 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsGripPressedDownOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsGripPressedDownOnIndex_m1324565395 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsGripPressedUpOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsGripPressedUpOnIndex_m1218009080 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsGripTouchedOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsGripTouchedOnIndex_m3737611163 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsGripTouchedDownOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsGripTouchedDownOnIndex_m154316745 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsGripTouchedUpOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsGripTouchedUpOnIndex_m1810660544 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsHairGripDownOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsHairGripDownOnIndex_m1928656259 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsHairGripUpOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsHairGripUpOnIndex_m3109290428 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsTouchpadPressedOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsTouchpadPressedOnIndex_m4277030165 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsTouchpadPressedDownOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsTouchpadPressedDownOnIndex_m342072615 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsTouchpadPressedUpOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsTouchpadPressedUpOnIndex_m4088030608 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsTouchpadTouchedOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsTouchpadTouchedOnIndex_m406079303 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsTouchpadTouchedDownOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsTouchpadTouchedDownOnIndex_m1177218449 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsTouchpadTouchedUpOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsTouchpadTouchedUpOnIndex_m2020381964 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsButtonOnePressedOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsButtonOnePressedOnIndex_m2018222023 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsButtonOnePressedDownOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsButtonOnePressedDownOnIndex_m4121927901 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsButtonOnePressedUpOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsButtonOnePressedUpOnIndex_m2665232182 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsButtonOneTouchedOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsButtonOneTouchedOnIndex_m1476588041 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsButtonOneTouchedDownOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsButtonOneTouchedDownOnIndex_m2673626983 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsButtonOneTouchedUpOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsButtonOneTouchedUpOnIndex_m1629434842 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsButtonTwoPressedOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsButtonTwoPressedOnIndex_m2173095307 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsButtonTwoPressedDownOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsButtonTwoPressedDownOnIndex_m3473724809 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsButtonTwoPressedUpOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsButtonTwoPressedUpOnIndex_m2499335748 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsButtonTwoTouchedOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsButtonTwoTouchedOnIndex_m3532578629 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsButtonTwoTouchedDownOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsButtonTwoTouchedDownOnIndex_m3342338811 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsButtonTwoTouchedUpOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsButtonTwoTouchedUpOnIndex_m4029375436 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsStartMenuPressedOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsStartMenuPressedOnIndex_m3937780634 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsStartMenuPressedDownOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsStartMenuPressedDownOnIndex_m768040762 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsStartMenuPressedUpOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsStartMenuPressedUpOnIndex_m423587659 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsStartMenuTouchedOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsStartMenuTouchedOnIndex_m3590768054 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsStartMenuTouchedDownOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsStartMenuTouchedDownOnIndex_m1000721290 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsStartMenuTouchedUpOnIndex(System.UInt32)
extern "C"  bool VRTK_SDK_Bridge_IsStartMenuTouchedUpOnIndex_m2172821829 (Il2CppObject * __this /* static, unused */, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_SDK_Bridge::GetHeadset()
extern "C"  Transform_t3275118058 * VRTK_SDK_Bridge_GetHeadset_m1526371518 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_SDK_Bridge::GetHeadsetCamera()
extern "C"  Transform_t3275118058 * VRTK_SDK_Bridge_GetHeadsetCamera_m3254825739 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SDK_Bridge::HeadsetFade(UnityEngine.Color,System.Single,System.Boolean)
extern "C"  void VRTK_SDK_Bridge_HeadsetFade_m28453102 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, float ___duration1, bool ___fadeOverlay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::HasHeadsetFade(UnityEngine.Transform)
extern "C"  bool VRTK_SDK_Bridge_HasHeadsetFade_m3363251479 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SDK_Bridge::AddHeadsetFade(UnityEngine.Transform)
extern "C"  void VRTK_SDK_Bridge_AddHeadsetFade_m3943507904 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___camera0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_SDK_Bridge::GetPlayArea()
extern "C"  Transform_t3275118058 * VRTK_SDK_Bridge_GetPlayArea_m1444889743 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] VRTK.VRTK_SDK_Bridge::GetPlayAreaVertices(UnityEngine.GameObject)
extern "C"  Vector3U5BU5D_t1172311765* VRTK_SDK_Bridge_GetPlayAreaVertices_m741552552 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___playArea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_SDK_Bridge::GetPlayAreaBorderThickness(UnityEngine.GameObject)
extern "C"  float VRTK_SDK_Bridge_GetPlayAreaBorderThickness_m3889393971 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___playArea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsPlayAreaSizeCalibrated(UnityEngine.GameObject)
extern "C"  bool VRTK_SDK_Bridge_IsPlayAreaSizeCalibrated_m3724671281 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___playArea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::IsDisplayOnDesktop()
extern "C"  bool VRTK_SDK_Bridge_IsDisplayOnDesktop_m1104068353 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_SDK_Bridge::ShouldAppRenderWithLowResources()
extern "C"  bool VRTK_SDK_Bridge_ShouldAppRenderWithLowResources_m3152948531 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SDK_Bridge::ForceInterleavedReprojectionOn(System.Boolean)
extern "C"  void VRTK_SDK_Bridge_ForceInterleavedReprojectionOn_m2102099752 (Il2CppObject * __this /* static, unused */, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.SDK_BaseSystem VRTK.VRTK_SDK_Bridge::GetSystemSDK()
extern "C"  SDK_BaseSystem_t244469351 * VRTK_SDK_Bridge_GetSystemSDK_m690706716 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.SDK_BaseHeadset VRTK.VRTK_SDK_Bridge::GetHeadsetSDK()
extern "C"  SDK_BaseHeadset_t3046873914 * VRTK_SDK_Bridge_GetHeadsetSDK_m1406651138 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.SDK_BaseController VRTK.VRTK_SDK_Bridge::GetControllerSDK()
extern "C"  SDK_BaseController_t197168236 * VRTK_SDK_Bridge_GetControllerSDK_m2284326620 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.SDK_BaseBoundaries VRTK.VRTK_SDK_Bridge::GetBoundariesSDK()
extern "C"  SDK_BaseBoundaries_t1766380066 * VRTK_SDK_Bridge_GetBoundariesSDK_m2162935068 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SDK_Bridge::.cctor()
extern "C"  void VRTK_SDK_Bridge__cctor_m1481948877 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
