﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ViveControllerInput
struct ViveControllerInput_t1577066400;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t3948421699;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.EventSystems.PointerEventData[]
struct PointerEventDataU5BU5D_t2167427906;
// UnityEngine.Camera
struct Camera_t189460977;
// SteamVR_ControllerManager
struct SteamVR_ControllerManager_t3520649604;
// SteamVR_TrackedObject[]
struct SteamVR_TrackedObjectU5BU5D_t161912003;
// SteamVR_Controller/Device[]
struct DeviceU5BU5D_t2224228657;

#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM1295781545.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ViveControllerInput
struct  ViveControllerInput_t1577066400  : public BaseInputModule_t1295781545
{
public:
	// UnityEngine.Sprite ViveControllerInput::CursorSprite
	Sprite_t309593783 * ___CursorSprite_9;
	// UnityEngine.Material ViveControllerInput::CursorMaterial
	Material_t193706927 * ___CursorMaterial_10;
	// System.Single ViveControllerInput::NormalCursorScale
	float ___NormalCursorScale_11;
	// System.Boolean ViveControllerInput::GuiHit
	bool ___GuiHit_12;
	// System.Boolean ViveControllerInput::ButtonUsed
	bool ___ButtonUsed_13;
	// UnityEngine.RectTransform[] ViveControllerInput::Cursors
	RectTransformU5BU5D_t3948421699* ___Cursors_14;
	// UnityEngine.GameObject[] ViveControllerInput::CurrentPoint
	GameObjectU5BU5D_t3057952154* ___CurrentPoint_15;
	// UnityEngine.GameObject[] ViveControllerInput::CurrentPressed
	GameObjectU5BU5D_t3057952154* ___CurrentPressed_16;
	// UnityEngine.GameObject[] ViveControllerInput::CurrentDragging
	GameObjectU5BU5D_t3057952154* ___CurrentDragging_17;
	// UnityEngine.EventSystems.PointerEventData[] ViveControllerInput::PointEvents
	PointerEventDataU5BU5D_t2167427906* ___PointEvents_18;
	// System.Boolean ViveControllerInput::Initialized
	bool ___Initialized_19;
	// UnityEngine.Camera ViveControllerInput::ControllerCamera
	Camera_t189460977 * ___ControllerCamera_20;
	// SteamVR_ControllerManager ViveControllerInput::ControllerManager
	SteamVR_ControllerManager_t3520649604 * ___ControllerManager_21;
	// SteamVR_TrackedObject[] ViveControllerInput::Controllers
	SteamVR_TrackedObjectU5BU5D_t161912003* ___Controllers_22;
	// SteamVR_Controller/Device[] ViveControllerInput::ControllerDevices
	DeviceU5BU5D_t2224228657* ___ControllerDevices_23;

public:
	inline static int32_t get_offset_of_CursorSprite_9() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___CursorSprite_9)); }
	inline Sprite_t309593783 * get_CursorSprite_9() const { return ___CursorSprite_9; }
	inline Sprite_t309593783 ** get_address_of_CursorSprite_9() { return &___CursorSprite_9; }
	inline void set_CursorSprite_9(Sprite_t309593783 * value)
	{
		___CursorSprite_9 = value;
		Il2CppCodeGenWriteBarrier(&___CursorSprite_9, value);
	}

	inline static int32_t get_offset_of_CursorMaterial_10() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___CursorMaterial_10)); }
	inline Material_t193706927 * get_CursorMaterial_10() const { return ___CursorMaterial_10; }
	inline Material_t193706927 ** get_address_of_CursorMaterial_10() { return &___CursorMaterial_10; }
	inline void set_CursorMaterial_10(Material_t193706927 * value)
	{
		___CursorMaterial_10 = value;
		Il2CppCodeGenWriteBarrier(&___CursorMaterial_10, value);
	}

	inline static int32_t get_offset_of_NormalCursorScale_11() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___NormalCursorScale_11)); }
	inline float get_NormalCursorScale_11() const { return ___NormalCursorScale_11; }
	inline float* get_address_of_NormalCursorScale_11() { return &___NormalCursorScale_11; }
	inline void set_NormalCursorScale_11(float value)
	{
		___NormalCursorScale_11 = value;
	}

	inline static int32_t get_offset_of_GuiHit_12() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___GuiHit_12)); }
	inline bool get_GuiHit_12() const { return ___GuiHit_12; }
	inline bool* get_address_of_GuiHit_12() { return &___GuiHit_12; }
	inline void set_GuiHit_12(bool value)
	{
		___GuiHit_12 = value;
	}

	inline static int32_t get_offset_of_ButtonUsed_13() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___ButtonUsed_13)); }
	inline bool get_ButtonUsed_13() const { return ___ButtonUsed_13; }
	inline bool* get_address_of_ButtonUsed_13() { return &___ButtonUsed_13; }
	inline void set_ButtonUsed_13(bool value)
	{
		___ButtonUsed_13 = value;
	}

	inline static int32_t get_offset_of_Cursors_14() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___Cursors_14)); }
	inline RectTransformU5BU5D_t3948421699* get_Cursors_14() const { return ___Cursors_14; }
	inline RectTransformU5BU5D_t3948421699** get_address_of_Cursors_14() { return &___Cursors_14; }
	inline void set_Cursors_14(RectTransformU5BU5D_t3948421699* value)
	{
		___Cursors_14 = value;
		Il2CppCodeGenWriteBarrier(&___Cursors_14, value);
	}

	inline static int32_t get_offset_of_CurrentPoint_15() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___CurrentPoint_15)); }
	inline GameObjectU5BU5D_t3057952154* get_CurrentPoint_15() const { return ___CurrentPoint_15; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_CurrentPoint_15() { return &___CurrentPoint_15; }
	inline void set_CurrentPoint_15(GameObjectU5BU5D_t3057952154* value)
	{
		___CurrentPoint_15 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentPoint_15, value);
	}

	inline static int32_t get_offset_of_CurrentPressed_16() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___CurrentPressed_16)); }
	inline GameObjectU5BU5D_t3057952154* get_CurrentPressed_16() const { return ___CurrentPressed_16; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_CurrentPressed_16() { return &___CurrentPressed_16; }
	inline void set_CurrentPressed_16(GameObjectU5BU5D_t3057952154* value)
	{
		___CurrentPressed_16 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentPressed_16, value);
	}

	inline static int32_t get_offset_of_CurrentDragging_17() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___CurrentDragging_17)); }
	inline GameObjectU5BU5D_t3057952154* get_CurrentDragging_17() const { return ___CurrentDragging_17; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_CurrentDragging_17() { return &___CurrentDragging_17; }
	inline void set_CurrentDragging_17(GameObjectU5BU5D_t3057952154* value)
	{
		___CurrentDragging_17 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentDragging_17, value);
	}

	inline static int32_t get_offset_of_PointEvents_18() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___PointEvents_18)); }
	inline PointerEventDataU5BU5D_t2167427906* get_PointEvents_18() const { return ___PointEvents_18; }
	inline PointerEventDataU5BU5D_t2167427906** get_address_of_PointEvents_18() { return &___PointEvents_18; }
	inline void set_PointEvents_18(PointerEventDataU5BU5D_t2167427906* value)
	{
		___PointEvents_18 = value;
		Il2CppCodeGenWriteBarrier(&___PointEvents_18, value);
	}

	inline static int32_t get_offset_of_Initialized_19() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___Initialized_19)); }
	inline bool get_Initialized_19() const { return ___Initialized_19; }
	inline bool* get_address_of_Initialized_19() { return &___Initialized_19; }
	inline void set_Initialized_19(bool value)
	{
		___Initialized_19 = value;
	}

	inline static int32_t get_offset_of_ControllerCamera_20() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___ControllerCamera_20)); }
	inline Camera_t189460977 * get_ControllerCamera_20() const { return ___ControllerCamera_20; }
	inline Camera_t189460977 ** get_address_of_ControllerCamera_20() { return &___ControllerCamera_20; }
	inline void set_ControllerCamera_20(Camera_t189460977 * value)
	{
		___ControllerCamera_20 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerCamera_20, value);
	}

	inline static int32_t get_offset_of_ControllerManager_21() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___ControllerManager_21)); }
	inline SteamVR_ControllerManager_t3520649604 * get_ControllerManager_21() const { return ___ControllerManager_21; }
	inline SteamVR_ControllerManager_t3520649604 ** get_address_of_ControllerManager_21() { return &___ControllerManager_21; }
	inline void set_ControllerManager_21(SteamVR_ControllerManager_t3520649604 * value)
	{
		___ControllerManager_21 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerManager_21, value);
	}

	inline static int32_t get_offset_of_Controllers_22() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___Controllers_22)); }
	inline SteamVR_TrackedObjectU5BU5D_t161912003* get_Controllers_22() const { return ___Controllers_22; }
	inline SteamVR_TrackedObjectU5BU5D_t161912003** get_address_of_Controllers_22() { return &___Controllers_22; }
	inline void set_Controllers_22(SteamVR_TrackedObjectU5BU5D_t161912003* value)
	{
		___Controllers_22 = value;
		Il2CppCodeGenWriteBarrier(&___Controllers_22, value);
	}

	inline static int32_t get_offset_of_ControllerDevices_23() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400, ___ControllerDevices_23)); }
	inline DeviceU5BU5D_t2224228657* get_ControllerDevices_23() const { return ___ControllerDevices_23; }
	inline DeviceU5BU5D_t2224228657** get_address_of_ControllerDevices_23() { return &___ControllerDevices_23; }
	inline void set_ControllerDevices_23(DeviceU5BU5D_t2224228657* value)
	{
		___ControllerDevices_23 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerDevices_23, value);
	}
};

struct ViveControllerInput_t1577066400_StaticFields
{
public:
	// ViveControllerInput ViveControllerInput::Instance
	ViveControllerInput_t1577066400 * ___Instance_8;

public:
	inline static int32_t get_offset_of_Instance_8() { return static_cast<int32_t>(offsetof(ViveControllerInput_t1577066400_StaticFields, ___Instance_8)); }
	inline ViveControllerInput_t1577066400 * get_Instance_8() const { return ___Instance_8; }
	inline ViveControllerInput_t1577066400 ** get_address_of_Instance_8() { return &___Instance_8; }
	inline void set_Instance_8(ViveControllerInput_t1577066400 * value)
	{
		___Instance_8 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
