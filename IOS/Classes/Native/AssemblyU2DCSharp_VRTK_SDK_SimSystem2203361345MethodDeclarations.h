﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_SimSystem
struct SDK_SimSystem_t2203361345;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_SimSystem::.ctor()
extern "C"  void SDK_SimSystem__ctor_m1954760715 (SDK_SimSystem_t2203361345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
