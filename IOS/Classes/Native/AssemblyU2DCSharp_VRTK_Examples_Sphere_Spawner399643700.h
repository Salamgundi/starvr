﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Sphere_Spawner
struct  Sphere_Spawner_t399643700  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject VRTK.Examples.Sphere_Spawner::spawnMe
	GameObject_t1756533147 * ___spawnMe_2;
	// UnityEngine.Vector3 VRTK.Examples.Sphere_Spawner::position
	Vector3_t2243707580  ___position_3;

public:
	inline static int32_t get_offset_of_spawnMe_2() { return static_cast<int32_t>(offsetof(Sphere_Spawner_t399643700, ___spawnMe_2)); }
	inline GameObject_t1756533147 * get_spawnMe_2() const { return ___spawnMe_2; }
	inline GameObject_t1756533147 ** get_address_of_spawnMe_2() { return &___spawnMe_2; }
	inline void set_spawnMe_2(GameObject_t1756533147 * value)
	{
		___spawnMe_2 = value;
		Il2CppCodeGenWriteBarrier(&___spawnMe_2, value);
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(Sphere_Spawner_t399643700, ___position_3)); }
	inline Vector3_t2243707580  get_position_3() const { return ___position_3; }
	inline Vector3_t2243707580 * get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(Vector3_t2243707580  value)
	{
		___position_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
