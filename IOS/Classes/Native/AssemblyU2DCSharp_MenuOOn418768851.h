﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// SteamVR_TrackedObject
struct SteamVR_TrackedObject_t2338458854;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuOOn
struct  MenuOOn_t418768851  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean MenuOOn::MenuOn
	bool ___MenuOn_2;
	// UnityEngine.GameObject MenuOOn::Menu1
	GameObject_t1756533147 * ___Menu1_3;
	// SteamVR_TrackedObject MenuOOn::trackedObj
	SteamVR_TrackedObject_t2338458854 * ___trackedObj_4;

public:
	inline static int32_t get_offset_of_MenuOn_2() { return static_cast<int32_t>(offsetof(MenuOOn_t418768851, ___MenuOn_2)); }
	inline bool get_MenuOn_2() const { return ___MenuOn_2; }
	inline bool* get_address_of_MenuOn_2() { return &___MenuOn_2; }
	inline void set_MenuOn_2(bool value)
	{
		___MenuOn_2 = value;
	}

	inline static int32_t get_offset_of_Menu1_3() { return static_cast<int32_t>(offsetof(MenuOOn_t418768851, ___Menu1_3)); }
	inline GameObject_t1756533147 * get_Menu1_3() const { return ___Menu1_3; }
	inline GameObject_t1756533147 ** get_address_of_Menu1_3() { return &___Menu1_3; }
	inline void set_Menu1_3(GameObject_t1756533147 * value)
	{
		___Menu1_3 = value;
		Il2CppCodeGenWriteBarrier(&___Menu1_3, value);
	}

	inline static int32_t get_offset_of_trackedObj_4() { return static_cast<int32_t>(offsetof(MenuOOn_t418768851, ___trackedObj_4)); }
	inline SteamVR_TrackedObject_t2338458854 * get_trackedObj_4() const { return ___trackedObj_4; }
	inline SteamVR_TrackedObject_t2338458854 ** get_address_of_trackedObj_4() { return &___trackedObj_4; }
	inline void set_trackedObj_4(SteamVR_TrackedObject_t2338458854 * value)
	{
		___trackedObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___trackedObj_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
