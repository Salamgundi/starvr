﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_ScreenshotProgres41048547.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_ApplicationLaun3039414817.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_EditingCameraSu4154643070.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_MessageOverlay_1071642671.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_Property_t2604885825.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_t3405266389.h"
#include "AssemblyU2DCSharp_Valve_VR_HiddenAreaMesh_t3319190843.h"
#include "AssemblyU2DCSharp_Valve_VR_VRControllerAxis_t1854119560.h"
#include "AssemblyU2DCSharp_Valve_VR_VRControllerState_t2504874220.h"
#include "AssemblyU2DCSharp_Valve_VR_VRControllerState_t_Pac1296713633.h"
#include "AssemblyU2DCSharp_Valve_VR_Compositor_OverlaySetti2947234819.h"
#include "AssemblyU2DCSharp_Valve_VR_CameraVideoStreamFrameHe968213647.h"
#include "AssemblyU2DCSharp_Valve_VR_AppOverrideKeys_t1098481522.h"
#include "AssemblyU2DCSharp_Valve_VR_Compositor_FrameTiming2839634313.h"
#include "AssemblyU2DCSharp_Valve_VR_Compositor_CumulativeSta450065686.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayIntersectionPa3201480230.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayIntersectionRe2886517940.h"
#include "AssemblyU2DCSharp_Valve_VR_IntersectionMaskRectang2372341025.h"
#include "AssemblyU2DCSharp_Valve_VR_IntersectionMaskCircle_2044509700.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayIntersectionMa4278514679.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_ComponentSt2032012879.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_Vertex_t3505223259.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_TextureMap_1828165156.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_TextureMap_1702369923.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_t1723493896.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_t_Packed393767115.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_ControllerM1298199406.h"
#include "AssemblyU2DCSharp_Valve_VR_NotificationBitmap_t1973232283.h"
#include "AssemblyU2DCSharp_Valve_VR_COpenVRContext2015130898.h"
#include "AssemblyU2DCSharp_Valve_VR_OpenVR941364014.h"
#include "AssemblyU2DCSharp_Valve_VR_OpenVR_COpenVRContext2969167613.h"
#include "AssemblyU2DCSharp_SteamVR3920918452.h"
#include "AssemblyU2DCSharp_SteamVR_Camera3632348390.h"
#include "AssemblyU2DCSharp_SteamVR_CameraFlip209980041.h"
#include "AssemblyU2DCSharp_SteamVR_CameraMask1809821990.h"
#include "AssemblyU2DCSharp_SteamVR_Controller4041762851.h"
#include "AssemblyU2DCSharp_SteamVR_Controller_ButtonMask2972238878.h"
#include "AssemblyU2DCSharp_SteamVR_Controller_Device2885069456.h"
#include "AssemblyU2DCSharp_SteamVR_Controller_DeviceRelatio2314381548.h"
#include "AssemblyU2DCSharp_SteamVR_ControllerManager3520649604.h"
#include "AssemblyU2DCSharp_SteamVR_Ears1326160604.h"
#include "AssemblyU2DCSharp_SteamVR_Events1615855580.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action1836998693.h"
#include "AssemblyU2DCSharp_SteamVR_Events_ActionNoArgs953081287.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event1855872343.h"
#include "AssemblyU2DCSharp_SteamVR_ExternalCamera1737918827.h"
#include "AssemblyU2DCSharp_SteamVR_ExternalCamera_Config1802482304.h"
#include "AssemblyU2DCSharp_SteamVR_Fade3745867721.h"
#include "AssemblyU2DCSharp_SteamVR_Frustum3286408177.h"
#include "AssemblyU2DCSharp_SteamVR_GameView3723793646.h"
#include "AssemblyU2DCSharp_SteamVR_IK3055182089.h"
#include "AssemblyU2DCSharp_SteamVR_LoadLevel3225691259.h"
#include "AssemblyU2DCSharp_SteamVR_LoadLevel_U3CLoadLevelU33488978783.h"
#include "AssemblyU2DCSharp_SteamVR_Menu1084241286.h"
#include "AssemblyU2DCSharp_SteamVR_Overlay1916653097.h"
#include "AssemblyU2DCSharp_SteamVR_Overlay_IntersectionResul676309149.h"
#include "AssemblyU2DCSharp_SteamVR_PlayArea580405566.h"
#include "AssemblyU2DCSharp_SteamVR_PlayArea_Size1963019026.h"
#include "AssemblyU2DCSharp_SteamVR_PlayArea_U3CUpdateBounds1827863098.h"
#include "AssemblyU2DCSharp_SteamVR_Render595857297.h"
#include "AssemblyU2DCSharp_SteamVR_Render_U3CRenderLoopU3Ec3103906043.h"
#include "AssemblyU2DCSharp_SteamVR_RenderModel2905485978.h"
#include "AssemblyU2DCSharp_SteamVR_RenderModel_RenderModel582033572.h"
#include "AssemblyU2DCSharp_SteamVR_RenderModel_RenderModelIn289202529.h"
#include "AssemblyU2DCSharp_SteamVR_RenderModel_U3CSetModelAs660290373.h"
#include "AssemblyU2DCSharp_SteamVR_RenderModel_U3CFreeRende1040171062.h"
#include "AssemblyU2DCSharp_SteamVR_Skybox52115419.h"
#include "AssemblyU2DCSharp_SteamVR_Skybox_CellSize1979358963.h"
#include "AssemblyU2DCSharp_SteamVR_SphericalProjection2273435385.h"
#include "AssemblyU2DCSharp_SteamVR_Stats2562702356.h"
#include "AssemblyU2DCSharp_SteamVR_TestController1860367601.h"
#include "AssemblyU2DCSharp_SteamVR_TrackedCamera1496655666.h"
#include "AssemblyU2DCSharp_SteamVR_TrackedCamera_VideoStream930129953.h"
#include "AssemblyU2DCSharp_SteamVR_TrackedCamera_VideoStream676682966.h"
#include "AssemblyU2DCSharp_SteamVR_TrackedObject2338458854.h"
#include "AssemblyU2DCSharp_SteamVR_TrackedObject_EIndex129448938.h"
#include "AssemblyU2DCSharp_SteamVR_UpdatePoses3489297766.h"
#include "AssemblyU2DCSharp_SteamVR_Utils3631390866.h"
#include "AssemblyU2DCSharp_SteamVR_Utils_RigidTransform2602383126.h"
#include "AssemblyU2DCSharp_SteamVR_Utils_SystemFn1182840554.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_UIIgnoreRaycast3484256983.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Archery_Arrow1847108333.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Archery_ArrowNotch640683907.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Archery_ArrowSpawn3691485253.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Archery_BowAim1262532443.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Archery_BowAim_U3C4018195382.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Archery_BowAnimati4022033600.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Archery_BowHandle1058184516.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Archery_Follow66488297.h"
#include "AssemblyU2DCSharp_VRTK_Examples_AutoRotation3877681447.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Breakable_Cube459142415.h"
#include "AssemblyU2DCSharp_VRTK_Examples_ButtonReactor2088095572.h"
#include "AssemblyU2DCSharp_VRTK_Examples_ControlReactor2704852751.h"
#include "AssemblyU2DCSharp_VRTK_Examples_Controller_Hand3658229830.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (VREvent_ScreenshotProgress_t_t41048547)+ sizeof (Il2CppObject), sizeof(VREvent_ScreenshotProgress_t_t41048547 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2600[1] = 
{
	VREvent_ScreenshotProgress_t_t41048547::get_offset_of_progress_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (VREvent_ApplicationLaunch_t_t3039414817)+ sizeof (Il2CppObject), sizeof(VREvent_ApplicationLaunch_t_t3039414817 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2601[2] = 
{
	VREvent_ApplicationLaunch_t_t3039414817::get_offset_of_pid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_ApplicationLaunch_t_t3039414817::get_offset_of_unArgsHandle_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (VREvent_EditingCameraSurface_t_t4154643070)+ sizeof (Il2CppObject), sizeof(VREvent_EditingCameraSurface_t_t4154643070 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2602[2] = 
{
	VREvent_EditingCameraSurface_t_t4154643070::get_offset_of_overlayHandle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_EditingCameraSurface_t_t4154643070::get_offset_of_nVisualMode_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (VREvent_MessageOverlay_t_t1071642671)+ sizeof (Il2CppObject), sizeof(VREvent_MessageOverlay_t_t1071642671 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2603[1] = 
{
	VREvent_MessageOverlay_t_t1071642671::get_offset_of_unVRMessageOverlayResponse_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (VREvent_Property_t_t2604885825)+ sizeof (Il2CppObject), sizeof(VREvent_Property_t_t2604885825 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2604[2] = 
{
	VREvent_Property_t_t2604885825::get_offset_of_container_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_Property_t_t2604885825::get_offset_of_prop_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (VREvent_t_t3405266389)+ sizeof (Il2CppObject), sizeof(VREvent_t_t3405266389 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2605[4] = 
{
	VREvent_t_t3405266389::get_offset_of_eventType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_t_t3405266389::get_offset_of_trackedDeviceIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_t_t3405266389::get_offset_of_eventAgeSeconds_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VREvent_t_t3405266389::get_offset_of_data_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (HiddenAreaMesh_t_t3319190843)+ sizeof (Il2CppObject), sizeof(HiddenAreaMesh_t_t3319190843 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2606[2] = 
{
	HiddenAreaMesh_t_t3319190843::get_offset_of_pVertexData_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HiddenAreaMesh_t_t3319190843::get_offset_of_unTriangleCount_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (VRControllerAxis_t_t1854119560)+ sizeof (Il2CppObject), sizeof(VRControllerAxis_t_t1854119560 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2607[2] = 
{
	VRControllerAxis_t_t1854119560::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerAxis_t_t1854119560::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (VRControllerState_t_t2504874220)+ sizeof (Il2CppObject), sizeof(VRControllerState_t_t2504874220 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2608[8] = 
{
	VRControllerState_t_t2504874220::get_offset_of_unPacketNum_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerState_t_t2504874220::get_offset_of_ulButtonPressed_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerState_t_t2504874220::get_offset_of_ulButtonTouched_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerState_t_t2504874220::get_offset_of_rAxis0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerState_t_t2504874220::get_offset_of_rAxis1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerState_t_t2504874220::get_offset_of_rAxis2_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerState_t_t2504874220::get_offset_of_rAxis3_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerState_t_t2504874220::get_offset_of_rAxis4_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (VRControllerState_t_Packed_t1296713633)+ sizeof (Il2CppObject), sizeof(VRControllerState_t_Packed_t1296713633 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2609[8] = 
{
	VRControllerState_t_Packed_t1296713633::get_offset_of_unPacketNum_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerState_t_Packed_t1296713633::get_offset_of_ulButtonPressed_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerState_t_Packed_t1296713633::get_offset_of_ulButtonTouched_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerState_t_Packed_t1296713633::get_offset_of_rAxis0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerState_t_Packed_t1296713633::get_offset_of_rAxis1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerState_t_Packed_t1296713633::get_offset_of_rAxis2_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerState_t_Packed_t1296713633::get_offset_of_rAxis3_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRControllerState_t_Packed_t1296713633::get_offset_of_rAxis4_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (Compositor_OverlaySettings_t2947234819)+ sizeof (Il2CppObject), sizeof(Compositor_OverlaySettings_t2947234819 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2610[14] = 
{
	Compositor_OverlaySettings_t2947234819::get_offset_of_size_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_OverlaySettings_t2947234819::get_offset_of_curved_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_OverlaySettings_t2947234819::get_offset_of_antialias_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_OverlaySettings_t2947234819::get_offset_of_scale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_OverlaySettings_t2947234819::get_offset_of_distance_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_OverlaySettings_t2947234819::get_offset_of_alpha_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_OverlaySettings_t2947234819::get_offset_of_uOffset_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_OverlaySettings_t2947234819::get_offset_of_vOffset_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_OverlaySettings_t2947234819::get_offset_of_uScale_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_OverlaySettings_t2947234819::get_offset_of_vScale_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_OverlaySettings_t2947234819::get_offset_of_gridDivs_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_OverlaySettings_t2947234819::get_offset_of_gridWidth_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_OverlaySettings_t2947234819::get_offset_of_gridScale_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_OverlaySettings_t2947234819::get_offset_of_transform_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (CameraVideoStreamFrameHeader_t_t968213647)+ sizeof (Il2CppObject), sizeof(CameraVideoStreamFrameHeader_t_t968213647 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2611[6] = 
{
	CameraVideoStreamFrameHeader_t_t968213647::get_offset_of_eFrameType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CameraVideoStreamFrameHeader_t_t968213647::get_offset_of_nWidth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CameraVideoStreamFrameHeader_t_t968213647::get_offset_of_nHeight_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CameraVideoStreamFrameHeader_t_t968213647::get_offset_of_nBytesPerPixel_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CameraVideoStreamFrameHeader_t_t968213647::get_offset_of_nFrameSequence_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CameraVideoStreamFrameHeader_t_t968213647::get_offset_of_standingTrackedDevicePose_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (AppOverrideKeys_t_t1098481522)+ sizeof (Il2CppObject), sizeof(AppOverrideKeys_t_t1098481522 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2612[2] = 
{
	AppOverrideKeys_t_t1098481522::get_offset_of_pchKey_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AppOverrideKeys_t_t1098481522::get_offset_of_pchValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (Compositor_FrameTiming_t2839634313)+ sizeof (Il2CppObject), sizeof(Compositor_FrameTiming_t2839634313 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2613[24] = 
{
	Compositor_FrameTiming_t2839634313::get_offset_of_m_nSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_nFrameIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_nNumFramePresents_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_nNumMisPresented_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_nNumDroppedFrames_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_nReprojectionFlags_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flSystemTimeInSeconds_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flPreSubmitGpuMs_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flPostSubmitGpuMs_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flTotalRenderGpuMs_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flCompositorRenderGpuMs_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flCompositorRenderCpuMs_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flCompositorIdleCpuMs_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flClientFrameIntervalMs_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flPresentCallCpuMs_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flWaitForPresentCpuMs_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flSubmitFrameMs_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flWaitGetPosesCalledMs_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flNewPosesReadyMs_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flNewFrameReadyMs_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flCompositorUpdateStartMs_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flCompositorUpdateEndMs_21() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_flCompositorRenderStartMs_22() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_FrameTiming_t2839634313::get_offset_of_m_HmdPose_23() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (Compositor_CumulativeStats_t450065686)+ sizeof (Il2CppObject), sizeof(Compositor_CumulativeStats_t450065686 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2614[15] = 
{
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nPid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nNumFramePresents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nNumDroppedFrames_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nNumReprojectedFrames_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nNumFramePresentsOnStartup_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nNumDroppedFramesOnStartup_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nNumReprojectedFramesOnStartup_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nNumLoading_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nNumFramePresentsLoading_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nNumDroppedFramesLoading_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nNumReprojectedFramesLoading_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nNumTimedOut_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nNumFramePresentsTimedOut_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nNumDroppedFramesTimedOut_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Compositor_CumulativeStats_t450065686::get_offset_of_m_nNumReprojectedFramesTimedOut_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (VROverlayIntersectionParams_t_t3201480230)+ sizeof (Il2CppObject), sizeof(VROverlayIntersectionParams_t_t3201480230 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2615[3] = 
{
	VROverlayIntersectionParams_t_t3201480230::get_offset_of_vSource_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VROverlayIntersectionParams_t_t3201480230::get_offset_of_vDirection_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VROverlayIntersectionParams_t_t3201480230::get_offset_of_eOrigin_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (VROverlayIntersectionResults_t_t2886517940)+ sizeof (Il2CppObject), sizeof(VROverlayIntersectionResults_t_t2886517940 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2616[4] = 
{
	VROverlayIntersectionResults_t_t2886517940::get_offset_of_vPoint_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VROverlayIntersectionResults_t_t2886517940::get_offset_of_vNormal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VROverlayIntersectionResults_t_t2886517940::get_offset_of_vUVs_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VROverlayIntersectionResults_t_t2886517940::get_offset_of_fDistance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (IntersectionMaskRectangle_t_t2372341025)+ sizeof (Il2CppObject), sizeof(IntersectionMaskRectangle_t_t2372341025 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2617[4] = 
{
	IntersectionMaskRectangle_t_t2372341025::get_offset_of_m_flTopLeftX_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntersectionMaskRectangle_t_t2372341025::get_offset_of_m_flTopLeftY_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntersectionMaskRectangle_t_t2372341025::get_offset_of_m_flWidth_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntersectionMaskRectangle_t_t2372341025::get_offset_of_m_flHeight_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (IntersectionMaskCircle_t_t2044509700)+ sizeof (Il2CppObject), sizeof(IntersectionMaskCircle_t_t2044509700 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2618[3] = 
{
	IntersectionMaskCircle_t_t2044509700::get_offset_of_m_flCenterX_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntersectionMaskCircle_t_t2044509700::get_offset_of_m_flCenterY_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntersectionMaskCircle_t_t2044509700::get_offset_of_m_flRadius_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (VROverlayIntersectionMaskPrimitive_t_t4278514679)+ sizeof (Il2CppObject), sizeof(VROverlayIntersectionMaskPrimitive_t_t4278514679 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2619[2] = 
{
	VROverlayIntersectionMaskPrimitive_t_t4278514679::get_offset_of_m_nPrimitiveType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VROverlayIntersectionMaskPrimitive_t_t4278514679::get_offset_of_m_Primitive_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (RenderModel_ComponentState_t_t2032012879)+ sizeof (Il2CppObject), sizeof(RenderModel_ComponentState_t_t2032012879 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2620[3] = 
{
	RenderModel_ComponentState_t_t2032012879::get_offset_of_mTrackingToComponentRenderModel_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_ComponentState_t_t2032012879::get_offset_of_mTrackingToComponentLocal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_ComponentState_t_t2032012879::get_offset_of_uProperties_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (RenderModel_Vertex_t_t3505223259)+ sizeof (Il2CppObject), sizeof(RenderModel_Vertex_t_t3505223259 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2621[4] = 
{
	RenderModel_Vertex_t_t3505223259::get_offset_of_vPosition_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_Vertex_t_t3505223259::get_offset_of_vNormal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_Vertex_t_t3505223259::get_offset_of_rfTextureCoord0_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_Vertex_t_t3505223259::get_offset_of_rfTextureCoord1_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (RenderModel_TextureMap_t_t1828165156)+ sizeof (Il2CppObject), sizeof(RenderModel_TextureMap_t_t1828165156_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2622[3] = 
{
	RenderModel_TextureMap_t_t1828165156::get_offset_of_unWidth_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_TextureMap_t_t1828165156::get_offset_of_unHeight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_TextureMap_t_t1828165156::get_offset_of_rubTextureMapData_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (RenderModel_TextureMap_t_Packed_t1702369923)+ sizeof (Il2CppObject), sizeof(RenderModel_TextureMap_t_Packed_t1702369923_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2623[3] = 
{
	RenderModel_TextureMap_t_Packed_t1702369923::get_offset_of_unWidth_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_TextureMap_t_Packed_t1702369923::get_offset_of_unHeight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_TextureMap_t_Packed_t1702369923::get_offset_of_rubTextureMapData_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (RenderModel_t_t1723493896)+ sizeof (Il2CppObject), sizeof(RenderModel_t_t1723493896 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2624[5] = 
{
	RenderModel_t_t1723493896::get_offset_of_rVertexData_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_t_t1723493896::get_offset_of_unVertexCount_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_t_t1723493896::get_offset_of_rIndexData_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_t_t1723493896::get_offset_of_unTriangleCount_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_t_t1723493896::get_offset_of_diffuseTextureId_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (RenderModel_t_Packed_t393767115)+ sizeof (Il2CppObject), sizeof(RenderModel_t_Packed_t393767115 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2625[5] = 
{
	RenderModel_t_Packed_t393767115::get_offset_of_rVertexData_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_t_Packed_t393767115::get_offset_of_unVertexCount_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_t_Packed_t393767115::get_offset_of_rIndexData_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_t_Packed_t393767115::get_offset_of_unTriangleCount_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderModel_t_Packed_t393767115::get_offset_of_diffuseTextureId_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (RenderModel_ControllerMode_State_t_t1298199406)+ sizeof (Il2CppObject), sizeof(RenderModel_ControllerMode_State_t_t1298199406 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2626[1] = 
{
	RenderModel_ControllerMode_State_t_t1298199406::get_offset_of_bScrollWheelVisible_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (NotificationBitmap_t_t1973232283)+ sizeof (Il2CppObject), sizeof(NotificationBitmap_t_t1973232283 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2627[4] = 
{
	NotificationBitmap_t_t1973232283::get_offset_of_m_pImageData_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NotificationBitmap_t_t1973232283::get_offset_of_m_nWidth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NotificationBitmap_t_t1973232283::get_offset_of_m_nHeight_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NotificationBitmap_t_t1973232283::get_offset_of_m_nBytesPerPixel_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (COpenVRContext_t2015130898)+ sizeof (Il2CppObject), sizeof(COpenVRContext_t2015130898 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2628[12] = 
{
	COpenVRContext_t2015130898::get_offset_of_m_pVRSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	COpenVRContext_t2015130898::get_offset_of_m_pVRChaperone_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	COpenVRContext_t2015130898::get_offset_of_m_pVRChaperoneSetup_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	COpenVRContext_t2015130898::get_offset_of_m_pVRCompositor_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	COpenVRContext_t2015130898::get_offset_of_m_pVROverlay_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	COpenVRContext_t2015130898::get_offset_of_m_pVRResources_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	COpenVRContext_t2015130898::get_offset_of_m_pVRRenderModels_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	COpenVRContext_t2015130898::get_offset_of_m_pVRExtendedDisplay_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	COpenVRContext_t2015130898::get_offset_of_m_pVRSettings_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	COpenVRContext_t2015130898::get_offset_of_m_pVRApplications_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	COpenVRContext_t2015130898::get_offset_of_m_pVRTrackedCamera_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	COpenVRContext_t2015130898::get_offset_of_m_pVRScreenshots_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (OpenVR_t941364014), -1, sizeof(OpenVR_t941364014_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2629[174] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	OpenVR_t941364014_StaticFields::get_offset_of_U3CVRTokenU3Ek__BackingField_171(),
	0,
	OpenVR_t941364014_StaticFields::get_offset_of__OpenVRInternal_ModuleContext_173(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (COpenVRContext_t2969167613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2630[11] = 
{
	COpenVRContext_t2969167613::get_offset_of_m_pVRSystem_0(),
	COpenVRContext_t2969167613::get_offset_of_m_pVRChaperone_1(),
	COpenVRContext_t2969167613::get_offset_of_m_pVRChaperoneSetup_2(),
	COpenVRContext_t2969167613::get_offset_of_m_pVRCompositor_3(),
	COpenVRContext_t2969167613::get_offset_of_m_pVROverlay_4(),
	COpenVRContext_t2969167613::get_offset_of_m_pVRRenderModels_5(),
	COpenVRContext_t2969167613::get_offset_of_m_pVRExtendedDisplay_6(),
	COpenVRContext_t2969167613::get_offset_of_m_pVRSettings_7(),
	COpenVRContext_t2969167613::get_offset_of_m_pVRApplications_8(),
	COpenVRContext_t2969167613::get_offset_of_m_pVRScreenshots_9(),
	COpenVRContext_t2969167613::get_offset_of_m_pVRTrackedCamera_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (SteamVR_t3920918452), -1, sizeof(SteamVR_t3920918452_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2631[17] = 
{
	SteamVR_t3920918452_StaticFields::get_offset_of__enabled_0(),
	SteamVR_t3920918452_StaticFields::get_offset_of__instance_1(),
	SteamVR_t3920918452::get_offset_of_U3ChmdU3Ek__BackingField_2(),
	SteamVR_t3920918452::get_offset_of_U3CcompositorU3Ek__BackingField_3(),
	SteamVR_t3920918452::get_offset_of_U3CoverlayU3Ek__BackingField_4(),
	SteamVR_t3920918452_StaticFields::get_offset_of_U3CinitializingU3Ek__BackingField_5(),
	SteamVR_t3920918452_StaticFields::get_offset_of_U3CcalibratingU3Ek__BackingField_6(),
	SteamVR_t3920918452_StaticFields::get_offset_of_U3CoutOfRangeU3Ek__BackingField_7(),
	SteamVR_t3920918452_StaticFields::get_offset_of_connected_8(),
	SteamVR_t3920918452::get_offset_of_U3CsceneWidthU3Ek__BackingField_9(),
	SteamVR_t3920918452::get_offset_of_U3CsceneHeightU3Ek__BackingField_10(),
	SteamVR_t3920918452::get_offset_of_U3CaspectU3Ek__BackingField_11(),
	SteamVR_t3920918452::get_offset_of_U3CfieldOfViewU3Ek__BackingField_12(),
	SteamVR_t3920918452::get_offset_of_U3CtanHalfFovU3Ek__BackingField_13(),
	SteamVR_t3920918452::get_offset_of_U3CtextureBoundsU3Ek__BackingField_14(),
	SteamVR_t3920918452::get_offset_of_U3CeyesU3Ek__BackingField_15(),
	SteamVR_t3920918452::get_offset_of_textureType_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (SteamVR_Camera_t3632348390), -1, sizeof(SteamVR_Camera_t3632348390_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2632[9] = 
{
	SteamVR_Camera_t3632348390::get_offset_of__head_2(),
	SteamVR_Camera_t3632348390::get_offset_of_U3CcameraU3Ek__BackingField_3(),
	SteamVR_Camera_t3632348390::get_offset_of__ears_4(),
	SteamVR_Camera_t3632348390::get_offset_of_wireframe_5(),
	SteamVR_Camera_t3632348390_StaticFields::get_offset_of_values_6(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (SteamVR_CameraFlip_t209980041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (SteamVR_CameraMask_t1809821990), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (SteamVR_Controller_t4041762851), -1, sizeof(SteamVR_Controller_t4041762851_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2635[1] = 
{
	SteamVR_Controller_t4041762851_StaticFields::get_offset_of_devices_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (ButtonMask_t2972238878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2636[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (Device_t2885069456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2637[10] = 
{
	Device_t2885069456::get_offset_of_U3CindexU3Ek__BackingField_0(),
	Device_t2885069456::get_offset_of_U3CvalidU3Ek__BackingField_1(),
	Device_t2885069456::get_offset_of_state_2(),
	Device_t2885069456::get_offset_of_prevState_3(),
	Device_t2885069456::get_offset_of_pose_4(),
	Device_t2885069456::get_offset_of_prevFrameCount_5(),
	Device_t2885069456::get_offset_of_hairTriggerDelta_6(),
	Device_t2885069456::get_offset_of_hairTriggerLimit_7(),
	Device_t2885069456::get_offset_of_hairTriggerState_8(),
	Device_t2885069456::get_offset_of_hairTriggerPrevState_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (DeviceRelation_t2314381548)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2638[6] = 
{
	DeviceRelation_t2314381548::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (SteamVR_ControllerManager_t3520649604), -1, sizeof(SteamVR_ControllerManager_t3520649604_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2639[12] = 
{
	SteamVR_ControllerManager_t3520649604::get_offset_of_left_2(),
	SteamVR_ControllerManager_t3520649604::get_offset_of_right_3(),
	SteamVR_ControllerManager_t3520649604::get_offset_of_objects_4(),
	SteamVR_ControllerManager_t3520649604::get_offset_of_assignAllBeforeIdentified_5(),
	SteamVR_ControllerManager_t3520649604::get_offset_of_indices_6(),
	SteamVR_ControllerManager_t3520649604::get_offset_of_connected_7(),
	SteamVR_ControllerManager_t3520649604::get_offset_of_leftIndex_8(),
	SteamVR_ControllerManager_t3520649604::get_offset_of_rightIndex_9(),
	SteamVR_ControllerManager_t3520649604::get_offset_of_inputFocusAction_10(),
	SteamVR_ControllerManager_t3520649604::get_offset_of_deviceConnectedAction_11(),
	SteamVR_ControllerManager_t3520649604::get_offset_of_trackedDeviceRoleChangedAction_12(),
	SteamVR_ControllerManager_t3520649604_StaticFields::get_offset_of_labels_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (SteamVR_Ears_t1326160604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[3] = 
{
	SteamVR_Ears_t1326160604::get_offset_of_vrcam_2(),
	SteamVR_Ears_t1326160604::get_offset_of_usingSpeakers_3(),
	SteamVR_Ears_t1326160604::get_offset_of_offset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (SteamVR_Events_t1615855580), -1, sizeof(SteamVR_Events_t1615855580_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2641[15] = 
{
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_Calibrating_0(),
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_DeviceConnected_1(),
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_Fade_2(),
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_FadeReady_3(),
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_HideRenderModels_4(),
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_Initializing_5(),
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_InputFocus_6(),
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_Loading_7(),
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_LoadingFadeIn_8(),
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_LoadingFadeOut_9(),
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_NewPoses_10(),
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_NewPosesApplied_11(),
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_OutOfRange_12(),
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_RenderModelLoaded_13(),
	SteamVR_Events_t1615855580_StaticFields::get_offset_of_systemEvents_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (Action_t1836998693), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (ActionNoArgs_t953081287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[2] = 
{
	ActionNoArgs_t953081287::get_offset_of__event_0(),
	ActionNoArgs_t953081287::get_offset_of_action_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2645[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2646[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (Event_t1855872343), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (SteamVR_ExternalCamera_t1737918827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2651[11] = 
{
	SteamVR_ExternalCamera_t1737918827::get_offset_of_config_2(),
	SteamVR_ExternalCamera_t1737918827::get_offset_of_configPath_3(),
	SteamVR_ExternalCamera_t1737918827::get_offset_of_cam_4(),
	SteamVR_ExternalCamera_t1737918827::get_offset_of_target_5(),
	SteamVR_ExternalCamera_t1737918827::get_offset_of_clipQuad_6(),
	SteamVR_ExternalCamera_t1737918827::get_offset_of_clipMaterial_7(),
	SteamVR_ExternalCamera_t1737918827::get_offset_of_colorMat_8(),
	SteamVR_ExternalCamera_t1737918827::get_offset_of_alphaMat_9(),
	SteamVR_ExternalCamera_t1737918827::get_offset_of_cameras_10(),
	SteamVR_ExternalCamera_t1737918827::get_offset_of_cameraRects_11(),
	SteamVR_ExternalCamera_t1737918827::get_offset_of_sceneResolutionScale_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (Config_t1802482304)+ sizeof (Il2CppObject), sizeof(Config_t1802482304_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2652[15] = 
{
	Config_t1802482304::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Config_t1802482304::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Config_t1802482304::get_offset_of_z_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Config_t1802482304::get_offset_of_rx_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Config_t1802482304::get_offset_of_ry_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Config_t1802482304::get_offset_of_rz_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Config_t1802482304::get_offset_of_fov_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Config_t1802482304::get_offset_of_near_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Config_t1802482304::get_offset_of_far_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Config_t1802482304::get_offset_of_sceneResolutionScale_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Config_t1802482304::get_offset_of_frameSkip_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Config_t1802482304::get_offset_of_nearOffset_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Config_t1802482304::get_offset_of_farOffset_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Config_t1802482304::get_offset_of_hmdOffset_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Config_t1802482304::get_offset_of_disableStandardAssets_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (SteamVR_Fade_t3745867721), -1, sizeof(SteamVR_Fade_t3745867721_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2653[6] = 
{
	SteamVR_Fade_t3745867721::get_offset_of_currentColor_2(),
	SteamVR_Fade_t3745867721::get_offset_of_targetColor_3(),
	SteamVR_Fade_t3745867721::get_offset_of_deltaColor_4(),
	SteamVR_Fade_t3745867721::get_offset_of_fadeOverlay_5(),
	SteamVR_Fade_t3745867721_StaticFields::get_offset_of_fadeMaterial_6(),
	SteamVR_Fade_t3745867721_StaticFields::get_offset_of_fadeMaterialColorID_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (SteamVR_Frustum_t3286408177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[7] = 
{
	SteamVR_Frustum_t3286408177::get_offset_of_index_2(),
	SteamVR_Frustum_t3286408177::get_offset_of_fovLeft_3(),
	SteamVR_Frustum_t3286408177::get_offset_of_fovRight_4(),
	SteamVR_Frustum_t3286408177::get_offset_of_fovTop_5(),
	SteamVR_Frustum_t3286408177::get_offset_of_fovBottom_6(),
	SteamVR_Frustum_t3286408177::get_offset_of_nearZ_7(),
	SteamVR_Frustum_t3286408177::get_offset_of_farZ_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (SteamVR_GameView_t3723793646), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (SteamVR_IK_t3055182089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2656[10] = 
{
	SteamVR_IK_t3055182089::get_offset_of_target_2(),
	SteamVR_IK_t3055182089::get_offset_of_start_3(),
	SteamVR_IK_t3055182089::get_offset_of_joint_4(),
	SteamVR_IK_t3055182089::get_offset_of_end_5(),
	SteamVR_IK_t3055182089::get_offset_of_poleVector_6(),
	SteamVR_IK_t3055182089::get_offset_of_upVector_7(),
	SteamVR_IK_t3055182089::get_offset_of_blendPct_8(),
	SteamVR_IK_t3055182089::get_offset_of_startXform_9(),
	SteamVR_IK_t3055182089::get_offset_of_jointXform_10(),
	SteamVR_IK_t3055182089::get_offset_of_endXform_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (SteamVR_LoadLevel_t3225691259), -1, sizeof(SteamVR_LoadLevel_t3225691259_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2657[34] = 
{
	SteamVR_LoadLevel_t3225691259_StaticFields::get_offset_of__active_2(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_levelName_3(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_internalProcessPath_4(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_internalProcessArgs_5(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_loadAdditive_6(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_loadAsync_7(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_loadingScreen_8(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_progressBarEmpty_9(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_progressBarFull_10(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_loadingScreenWidthInMeters_11(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_progressBarWidthInMeters_12(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_loadingScreenDistance_13(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_loadingScreenTransform_14(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_progressBarTransform_15(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_front_16(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_back_17(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_left_18(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_right_19(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_top_20(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_bottom_21(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_backgroundColor_22(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_showGrid_23(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_fadeOutTime_24(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_fadeInTime_25(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_postLoadSettleTime_26(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_loadingScreenFadeInTime_27(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_loadingScreenFadeOutTime_28(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_fadeRate_29(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_alpha_30(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_async_31(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_renderTexture_32(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_loadingScreenOverlayHandle_33(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_progressBarOverlayHandle_34(),
	SteamVR_LoadLevel_t3225691259::get_offset_of_autoTriggerOnEnable_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (U3CLoadLevelU3Ec__Iterator0_t3488978783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[11] = 
{
	U3CLoadLevelU3Ec__Iterator0_t3488978783::get_offset_of_U3ChmdU3E__0_0(),
	U3CLoadLevelU3Ec__Iterator0_t3488978783::get_offset_of_U3CtloadingU3E__1_1(),
	U3CLoadLevelU3Ec__Iterator0_t3488978783::get_offset_of_U3CtU3E__2_2(),
	U3CLoadLevelU3Ec__Iterator0_t3488978783::get_offset_of_U3CoverlayU3E__3_3(),
	U3CLoadLevelU3Ec__Iterator0_t3488978783::get_offset_of_U3CfadedForegroundU3E__4_4(),
	U3CLoadLevelU3Ec__Iterator0_t3488978783::get_offset_of_U3CcompositorU3E__5_5(),
	U3CLoadLevelU3Ec__Iterator0_t3488978783::get_offset_of_U3CmodeU3E__6_6(),
	U3CLoadLevelU3Ec__Iterator0_t3488978783::get_offset_of_U24this_7(),
	U3CLoadLevelU3Ec__Iterator0_t3488978783::get_offset_of_U24current_8(),
	U3CLoadLevelU3Ec__Iterator0_t3488978783::get_offset_of_U24disposing_9(),
	U3CLoadLevelU3Ec__Iterator0_t3488978783::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (SteamVR_Menu_t1084241286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2659[17] = 
{
	SteamVR_Menu_t1084241286::get_offset_of_cursor_2(),
	SteamVR_Menu_t1084241286::get_offset_of_background_3(),
	SteamVR_Menu_t1084241286::get_offset_of_logo_4(),
	SteamVR_Menu_t1084241286::get_offset_of_logoHeight_5(),
	SteamVR_Menu_t1084241286::get_offset_of_menuOffset_6(),
	SteamVR_Menu_t1084241286::get_offset_of_scaleLimits_7(),
	SteamVR_Menu_t1084241286::get_offset_of_scaleRate_8(),
	SteamVR_Menu_t1084241286::get_offset_of_overlay_9(),
	SteamVR_Menu_t1084241286::get_offset_of_overlayCam_10(),
	SteamVR_Menu_t1084241286::get_offset_of_uvOffset_11(),
	SteamVR_Menu_t1084241286::get_offset_of_distance_12(),
	SteamVR_Menu_t1084241286::get_offset_of_U3CscaleU3Ek__BackingField_13(),
	SteamVR_Menu_t1084241286::get_offset_of_scaleLimitX_14(),
	SteamVR_Menu_t1084241286::get_offset_of_scaleLimitY_15(),
	SteamVR_Menu_t1084241286::get_offset_of_scaleRateText_16(),
	SteamVR_Menu_t1084241286::get_offset_of_savedCursorLockState_17(),
	SteamVR_Menu_t1084241286::get_offset_of_savedCursorVisible_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (SteamVR_Overlay_t1916653097), -1, sizeof(SteamVR_Overlay_t1916653097_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2660[13] = 
{
	SteamVR_Overlay_t1916653097::get_offset_of_texture_2(),
	SteamVR_Overlay_t1916653097::get_offset_of_curved_3(),
	SteamVR_Overlay_t1916653097::get_offset_of_antialias_4(),
	SteamVR_Overlay_t1916653097::get_offset_of_highquality_5(),
	SteamVR_Overlay_t1916653097::get_offset_of_scale_6(),
	SteamVR_Overlay_t1916653097::get_offset_of_distance_7(),
	SteamVR_Overlay_t1916653097::get_offset_of_alpha_8(),
	SteamVR_Overlay_t1916653097::get_offset_of_uvOffset_9(),
	SteamVR_Overlay_t1916653097::get_offset_of_mouseScale_10(),
	SteamVR_Overlay_t1916653097::get_offset_of_curvedRange_11(),
	SteamVR_Overlay_t1916653097::get_offset_of_inputMethod_12(),
	SteamVR_Overlay_t1916653097_StaticFields::get_offset_of_U3CinstanceU3Ek__BackingField_13(),
	SteamVR_Overlay_t1916653097::get_offset_of_handle_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (IntersectionResults_t676309149)+ sizeof (Il2CppObject), sizeof(IntersectionResults_t676309149 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2661[4] = 
{
	IntersectionResults_t676309149::get_offset_of_point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntersectionResults_t676309149::get_offset_of_normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntersectionResults_t676309149::get_offset_of_UVs_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntersectionResults_t676309149::get_offset_of_distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (SteamVR_PlayArea_t580405566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2662[7] = 
{
	SteamVR_PlayArea_t580405566::get_offset_of_borderThickness_2(),
	SteamVR_PlayArea_t580405566::get_offset_of_wireframeHeight_3(),
	SteamVR_PlayArea_t580405566::get_offset_of_drawWireframeWhenSelectedOnly_4(),
	SteamVR_PlayArea_t580405566::get_offset_of_drawInGame_5(),
	SteamVR_PlayArea_t580405566::get_offset_of_size_6(),
	SteamVR_PlayArea_t580405566::get_offset_of_color_7(),
	SteamVR_PlayArea_t580405566::get_offset_of_vertices_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (Size_t1963019026)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2663[5] = 
{
	Size_t1963019026::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (U3CUpdateBoundsU3Ec__Iterator0_t1827863098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2664[5] = 
{
	U3CUpdateBoundsU3Ec__Iterator0_t1827863098::get_offset_of_U3CchaperoneU3E__0_0(),
	U3CUpdateBoundsU3Ec__Iterator0_t1827863098::get_offset_of_U24this_1(),
	U3CUpdateBoundsU3Ec__Iterator0_t1827863098::get_offset_of_U24current_2(),
	U3CUpdateBoundsU3Ec__Iterator0_t1827863098::get_offset_of_U24disposing_3(),
	U3CUpdateBoundsU3Ec__Iterator0_t1827863098::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (SteamVR_Render_t595857297), -1, sizeof(SteamVR_Render_t595857297_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2665[16] = 
{
	SteamVR_Render_t595857297::get_offset_of_pauseGameWhenDashboardIsVisible_2(),
	SteamVR_Render_t595857297::get_offset_of_lockPhysicsUpdateRateToRenderFrequency_3(),
	SteamVR_Render_t595857297::get_offset_of_externalCamera_4(),
	SteamVR_Render_t595857297::get_offset_of_externalCameraConfigPath_5(),
	SteamVR_Render_t595857297::get_offset_of_trackingSpace_6(),
	SteamVR_Render_t595857297_StaticFields::get_offset_of_U3CeyeU3Ek__BackingField_7(),
	SteamVR_Render_t595857297_StaticFields::get_offset_of__instance_8(),
	SteamVR_Render_t595857297_StaticFields::get_offset_of_isQuitting_9(),
	SteamVR_Render_t595857297::get_offset_of_cameras_10(),
	SteamVR_Render_t595857297::get_offset_of_poses_11(),
	SteamVR_Render_t595857297::get_offset_of_gamePoses_12(),
	SteamVR_Render_t595857297_StaticFields::get_offset_of__pauseRendering_13(),
	SteamVR_Render_t595857297::get_offset_of_waitForEndOfFrame_14(),
	SteamVR_Render_t595857297::get_offset_of_sceneResolutionScale_15(),
	SteamVR_Render_t595857297::get_offset_of_timeScale_16(),
	SteamVR_Render_t595857297::get_offset_of_poseUpdater_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (U3CRenderLoopU3Ec__Iterator0_t3103906043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2666[6] = 
{
	U3CRenderLoopU3Ec__Iterator0_t3103906043::get_offset_of_U3CcompositorU3E__0_0(),
	U3CRenderLoopU3Ec__Iterator0_t3103906043::get_offset_of_U3CoverlayU3E__1_1(),
	U3CRenderLoopU3Ec__Iterator0_t3103906043::get_offset_of_U24this_2(),
	U3CRenderLoopU3Ec__Iterator0_t3103906043::get_offset_of_U24current_3(),
	U3CRenderLoopU3Ec__Iterator0_t3103906043::get_offset_of_U24disposing_4(),
	U3CRenderLoopU3Ec__Iterator0_t3103906043::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (SteamVR_RenderModel_t2905485978), -1, sizeof(SteamVR_RenderModel_t2905485978_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2667[15] = 
{
	SteamVR_RenderModel_t2905485978::get_offset_of_index_2(),
	SteamVR_RenderModel_t2905485978::get_offset_of_modelOverride_3(),
	SteamVR_RenderModel_t2905485978::get_offset_of_shader_4(),
	SteamVR_RenderModel_t2905485978::get_offset_of_verbose_5(),
	SteamVR_RenderModel_t2905485978::get_offset_of_createComponents_6(),
	SteamVR_RenderModel_t2905485978::get_offset_of_updateDynamically_7(),
	SteamVR_RenderModel_t2905485978::get_offset_of_controllerModeState_8(),
	0,
	SteamVR_RenderModel_t2905485978::get_offset_of_U3CrenderModelNameU3Ek__BackingField_10(),
	SteamVR_RenderModel_t2905485978_StaticFields::get_offset_of_models_11(),
	SteamVR_RenderModel_t2905485978_StaticFields::get_offset_of_materials_12(),
	SteamVR_RenderModel_t2905485978::get_offset_of_deviceConnectedAction_13(),
	SteamVR_RenderModel_t2905485978::get_offset_of_hideRenderModelsAction_14(),
	SteamVR_RenderModel_t2905485978::get_offset_of_modelSkinSettingsHaveChangedAction_15(),
	SteamVR_RenderModel_t2905485978::get_offset_of_nameCache_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (RenderModel_t582033572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2668[2] = 
{
	RenderModel_t582033572::get_offset_of_U3CmeshU3Ek__BackingField_0(),
	RenderModel_t582033572::get_offset_of_U3CmaterialU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (RenderModelInterfaceHolder_t289202529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[3] = 
{
	RenderModelInterfaceHolder_t289202529::get_offset_of_needsShutdown_0(),
	RenderModelInterfaceHolder_t289202529::get_offset_of_failedLoadInterface_1(),
	RenderModelInterfaceHolder_t289202529::get_offset_of__instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (U3CSetModelAsyncU3Ec__Iterator0_t660290373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[13] = 
{
	U3CSetModelAsyncU3Ec__Iterator0_t660290373::get_offset_of_renderModelName_0(),
	U3CSetModelAsyncU3Ec__Iterator0_t660290373::get_offset_of_U3CholderU3E__0_1(),
	U3CSetModelAsyncU3Ec__Iterator0_t660290373::get_offset_of_U3CrenderModelsU3E__1_2(),
	U3CSetModelAsyncU3Ec__Iterator0_t660290373::get_offset_of_U3CcountU3E__2_3(),
	U3CSetModelAsyncU3Ec__Iterator0_t660290373::get_offset_of_U3CrenderModelNamesU3E__3_4(),
	U3CSetModelAsyncU3Ec__Iterator0_t660290373::get_offset_of_U3CloadingU3E__4_5(),
	U3CSetModelAsyncU3Ec__Iterator0_t660290373::get_offset_of_U24locvar0_6(),
	U3CSetModelAsyncU3Ec__Iterator0_t660290373::get_offset_of_U24locvar1_7(),
	U3CSetModelAsyncU3Ec__Iterator0_t660290373::get_offset_of_U3CsuccessU3E__5_8(),
	U3CSetModelAsyncU3Ec__Iterator0_t660290373::get_offset_of_U24this_9(),
	U3CSetModelAsyncU3Ec__Iterator0_t660290373::get_offset_of_U24current_10(),
	U3CSetModelAsyncU3Ec__Iterator0_t660290373::get_offset_of_U24disposing_11(),
	U3CSetModelAsyncU3Ec__Iterator0_t660290373::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (U3CFreeRenderModelU3Ec__Iterator1_t1040171062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[4] = 
{
	U3CFreeRenderModelU3Ec__Iterator1_t1040171062::get_offset_of_pRenderModel_0(),
	U3CFreeRenderModelU3Ec__Iterator1_t1040171062::get_offset_of_U24current_1(),
	U3CFreeRenderModelU3Ec__Iterator1_t1040171062::get_offset_of_U24disposing_2(),
	U3CFreeRenderModelU3Ec__Iterator1_t1040171062::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (SteamVR_Skybox_t52115419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2672[8] = 
{
	SteamVR_Skybox_t52115419::get_offset_of_front_2(),
	SteamVR_Skybox_t52115419::get_offset_of_back_3(),
	SteamVR_Skybox_t52115419::get_offset_of_left_4(),
	SteamVR_Skybox_t52115419::get_offset_of_right_5(),
	SteamVR_Skybox_t52115419::get_offset_of_top_6(),
	SteamVR_Skybox_t52115419::get_offset_of_bottom_7(),
	SteamVR_Skybox_t52115419::get_offset_of_StereoCellSize_8(),
	SteamVR_Skybox_t52115419::get_offset_of_StereoIpdMm_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (CellSize_t1979358963)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2673[6] = 
{
	CellSize_t1979358963::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (SteamVR_SphericalProjection_t2273435385), -1, sizeof(SteamVR_SphericalProjection_t2273435385_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2674[1] = 
{
	SteamVR_SphericalProjection_t2273435385_StaticFields::get_offset_of_material_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (SteamVR_Stats_t2562702356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2675[4] = 
{
	SteamVR_Stats_t2562702356::get_offset_of_text_2(),
	SteamVR_Stats_t2562702356::get_offset_of_fadeColor_3(),
	SteamVR_Stats_t2562702356::get_offset_of_fadeDuration_4(),
	SteamVR_Stats_t2562702356::get_offset_of_lastUpdate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (SteamVR_TestController_t1860367601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2676[5] = 
{
	SteamVR_TestController_t1860367601::get_offset_of_controllerIndices_2(),
	SteamVR_TestController_t1860367601::get_offset_of_buttonIds_3(),
	SteamVR_TestController_t1860367601::get_offset_of_axisIds_4(),
	SteamVR_TestController_t1860367601::get_offset_of_point_5(),
	SteamVR_TestController_t1860367601::get_offset_of_pointer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (SteamVR_TrackedCamera_t1496655666), -1, sizeof(SteamVR_TrackedCamera_t1496655666_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2677[3] = 
{
	SteamVR_TrackedCamera_t1496655666_StaticFields::get_offset_of_distorted_0(),
	SteamVR_TrackedCamera_t1496655666_StaticFields::get_offset_of_undistorted_1(),
	SteamVR_TrackedCamera_t1496655666_StaticFields::get_offset_of_videostreams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (VideoStreamTexture_t930129953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2678[7] = 
{
	VideoStreamTexture_t930129953::get_offset_of_U3CundistortedU3Ek__BackingField_0(),
	VideoStreamTexture_t930129953::get_offset_of_U3CframeBoundsU3Ek__BackingField_1(),
	VideoStreamTexture_t930129953::get_offset_of__texture_2(),
	VideoStreamTexture_t930129953::get_offset_of_prevFrameCount_3(),
	VideoStreamTexture_t930129953::get_offset_of_glTextureId_4(),
	VideoStreamTexture_t930129953::get_offset_of_videostream_5(),
	VideoStreamTexture_t930129953::get_offset_of_header_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (VideoStream_t676682966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[4] = 
{
	VideoStream_t676682966::get_offset_of_U3CdeviceIndexU3Ek__BackingField_0(),
	VideoStream_t676682966::get_offset_of__handle_1(),
	VideoStream_t676682966::get_offset_of__hasCamera_2(),
	VideoStream_t676682966::get_offset_of_refCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (SteamVR_TrackedObject_t2338458854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2680[4] = 
{
	SteamVR_TrackedObject_t2338458854::get_offset_of_index_2(),
	SteamVR_TrackedObject_t2338458854::get_offset_of_origin_3(),
	SteamVR_TrackedObject_t2338458854::get_offset_of_isValid_4(),
	SteamVR_TrackedObject_t2338458854::get_offset_of_newPosesAction_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (EIndex_t129448938)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2681[18] = 
{
	EIndex_t129448938::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (SteamVR_UpdatePoses_t3489297766), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (SteamVR_Utils_t3631390866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (RigidTransform_t2602383126)+ sizeof (Il2CppObject), sizeof(RigidTransform_t2602383126 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2684[2] = 
{
	RigidTransform_t2602383126::get_offset_of_pos_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RigidTransform_t2602383126::get_offset_of_rot_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (SystemFn_t1182840554), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (UIIgnoreRaycast_t3484256983), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (Arrow_t1847108333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[8] = 
{
	Arrow_t1847108333::get_offset_of_maxArrowLife_2(),
	Arrow_t1847108333::get_offset_of_inFlight_3(),
	Arrow_t1847108333::get_offset_of_collided_4(),
	Arrow_t1847108333::get_offset_of_rigidBody_5(),
	Arrow_t1847108333::get_offset_of_arrowHolder_6(),
	Arrow_t1847108333::get_offset_of_originalPosition_7(),
	Arrow_t1847108333::get_offset_of_originalRotation_8(),
	Arrow_t1847108333::get_offset_of_originalScale_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (ArrowNotch_t640683907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[2] = 
{
	ArrowNotch_t640683907::get_offset_of_arrow_2(),
	ArrowNotch_t640683907::get_offset_of_obj_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (ArrowSpawner_t3691485253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[4] = 
{
	ArrowSpawner_t3691485253::get_offset_of_arrowPrefab_2(),
	ArrowSpawner_t3691485253::get_offset_of_spawnDelay_3(),
	ArrowSpawner_t3691485253::get_offset_of_spawnDelayTimer_4(),
	ArrowSpawner_t3691485253::get_offset_of_bow_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (BowAim_t1262532443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[20] = 
{
	BowAim_t1262532443::get_offset_of_powerMultiplier_2(),
	BowAim_t1262532443::get_offset_of_pullMultiplier_3(),
	BowAim_t1262532443::get_offset_of_pullOffset_4(),
	BowAim_t1262532443::get_offset_of_maxPullDistance_5(),
	BowAim_t1262532443::get_offset_of_bowVibration_6(),
	BowAim_t1262532443::get_offset_of_stringVibration_7(),
	BowAim_t1262532443::get_offset_of_bowAnimation_8(),
	BowAim_t1262532443::get_offset_of_currentArrow_9(),
	BowAim_t1262532443::get_offset_of_handle_10(),
	BowAim_t1262532443::get_offset_of_interact_11(),
	BowAim_t1262532443::get_offset_of_holdControl_12(),
	BowAim_t1262532443::get_offset_of_stringControl_13(),
	BowAim_t1262532443::get_offset_of_stringActions_14(),
	BowAim_t1262532443::get_offset_of_holdActions_15(),
	BowAim_t1262532443::get_offset_of_releaseRotation_16(),
	BowAim_t1262532443::get_offset_of_baseRotation_17(),
	BowAim_t1262532443::get_offset_of_fired_18(),
	BowAim_t1262532443::get_offset_of_fireOffset_19(),
	BowAim_t1262532443::get_offset_of_currentPull_20(),
	BowAim_t1262532443::get_offset_of_previousPull_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (U3CGetBaseRotationU3Ec__Iterator0_t4018195382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[4] = 
{
	U3CGetBaseRotationU3Ec__Iterator0_t4018195382::get_offset_of_U24this_0(),
	U3CGetBaseRotationU3Ec__Iterator0_t4018195382::get_offset_of_U24current_1(),
	U3CGetBaseRotationU3Ec__Iterator0_t4018195382::get_offset_of_U24disposing_2(),
	U3CGetBaseRotationU3Ec__Iterator0_t4018195382::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (BowAnimation_t4022033600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[1] = 
{
	BowAnimation_t4022033600::get_offset_of_animationTimeline_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (BowHandle_t1058184516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2693[3] = 
{
	BowHandle_t1058184516::get_offset_of_arrowNockingPoint_2(),
	BowHandle_t1058184516::get_offset_of_aim_3(),
	BowHandle_t1058184516::get_offset_of_nockSide_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (Follow_t66488297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[3] = 
{
	Follow_t66488297::get_offset_of_followPosition_2(),
	Follow_t66488297::get_offset_of_followRotation_3(),
	Follow_t66488297::get_offset_of_target_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (AutoRotation_t3877681447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2695[2] = 
{
	AutoRotation_t3877681447::get_offset_of_degPerSec_2(),
	AutoRotation_t3877681447::get_offset_of_rotAxis_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (Breakable_Cube_t459142415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2696[1] = 
{
	Breakable_Cube_t459142415::get_offset_of_breakForce_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (ButtonReactor_t2088095572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[3] = 
{
	ButtonReactor_t2088095572::get_offset_of_go_2(),
	ButtonReactor_t2088095572::get_offset_of_dispenseLocation_3(),
	ButtonReactor_t2088095572::get_offset_of_buttonEvents_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (ControlReactor_t2704852751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[2] = 
{
	ControlReactor_t2704852751::get_offset_of_go_2(),
	ControlReactor_t2704852751::get_offset_of_controlEvents_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (Controller_Hand_t3658229830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[8] = 
{
	Controller_Hand_t3658229830::get_offset_of_hand_2(),
	Controller_Hand_t3658229830::get_offset_of_pointerFinger_3(),
	Controller_Hand_t3658229830::get_offset_of_gripFingers_4(),
	Controller_Hand_t3658229830::get_offset_of_maxRotation_5(),
	Controller_Hand_t3658229830::get_offset_of_originalPointerRotation_6(),
	Controller_Hand_t3658229830::get_offset_of_originalGripRotation_7(),
	Controller_Hand_t3658229830::get_offset_of_targetPointerRotation_8(),
	Controller_Hand_t3658229830::get_offset_of_targetGripRotation_9(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
