﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_PolicyList
struct VRTK_PolicyList_t2965133344;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2810717544;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_PlayAreaCursor
struct  VRTK_PlayAreaCursor_t3566057915  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector2 VRTK.VRTK_PlayAreaCursor::playAreaCursorDimensions
	Vector2_t2243707579  ___playAreaCursorDimensions_2;
	// System.Boolean VRTK.VRTK_PlayAreaCursor::handlePlayAreaCursorCollisions
	bool ___handlePlayAreaCursorCollisions_3;
	// System.Boolean VRTK.VRTK_PlayAreaCursor::headsetOutOfBoundsIsCollision
	bool ___headsetOutOfBoundsIsCollision_4;
	// VRTK.VRTK_PolicyList VRTK.VRTK_PlayAreaCursor::targetListPolicy
	VRTK_PolicyList_t2965133344 * ___targetListPolicy_5;
	// System.Boolean VRTK.VRTK_PlayAreaCursor::headsetPositionCompensation
	bool ___headsetPositionCompensation_6;
	// System.Boolean VRTK.VRTK_PlayAreaCursor::playAreaCursorCollided
	bool ___playAreaCursorCollided_7;
	// System.Boolean VRTK.VRTK_PlayAreaCursor::headsetOutOfBounds
	bool ___headsetOutOfBounds_8;
	// UnityEngine.Transform VRTK.VRTK_PlayAreaCursor::playArea
	Transform_t3275118058 * ___playArea_9;
	// UnityEngine.GameObject VRTK.VRTK_PlayAreaCursor::playAreaCursor
	GameObject_t1756533147 * ___playAreaCursor_10;
	// UnityEngine.GameObject[] VRTK.VRTK_PlayAreaCursor::playAreaCursorBoundaries
	GameObjectU5BU5D_t3057952154* ___playAreaCursorBoundaries_11;
	// UnityEngine.BoxCollider VRTK.VRTK_PlayAreaCursor::playAreaCursorCollider
	BoxCollider_t22920061 * ___playAreaCursorCollider_12;
	// UnityEngine.Transform VRTK.VRTK_PlayAreaCursor::headset
	Transform_t3275118058 * ___headset_13;
	// UnityEngine.Renderer[] VRTK.VRTK_PlayAreaCursor::boundaryRenderers
	RendererU5BU5D_t2810717544* ___boundaryRenderers_14;

public:
	inline static int32_t get_offset_of_playAreaCursorDimensions_2() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCursor_t3566057915, ___playAreaCursorDimensions_2)); }
	inline Vector2_t2243707579  get_playAreaCursorDimensions_2() const { return ___playAreaCursorDimensions_2; }
	inline Vector2_t2243707579 * get_address_of_playAreaCursorDimensions_2() { return &___playAreaCursorDimensions_2; }
	inline void set_playAreaCursorDimensions_2(Vector2_t2243707579  value)
	{
		___playAreaCursorDimensions_2 = value;
	}

	inline static int32_t get_offset_of_handlePlayAreaCursorCollisions_3() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCursor_t3566057915, ___handlePlayAreaCursorCollisions_3)); }
	inline bool get_handlePlayAreaCursorCollisions_3() const { return ___handlePlayAreaCursorCollisions_3; }
	inline bool* get_address_of_handlePlayAreaCursorCollisions_3() { return &___handlePlayAreaCursorCollisions_3; }
	inline void set_handlePlayAreaCursorCollisions_3(bool value)
	{
		___handlePlayAreaCursorCollisions_3 = value;
	}

	inline static int32_t get_offset_of_headsetOutOfBoundsIsCollision_4() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCursor_t3566057915, ___headsetOutOfBoundsIsCollision_4)); }
	inline bool get_headsetOutOfBoundsIsCollision_4() const { return ___headsetOutOfBoundsIsCollision_4; }
	inline bool* get_address_of_headsetOutOfBoundsIsCollision_4() { return &___headsetOutOfBoundsIsCollision_4; }
	inline void set_headsetOutOfBoundsIsCollision_4(bool value)
	{
		___headsetOutOfBoundsIsCollision_4 = value;
	}

	inline static int32_t get_offset_of_targetListPolicy_5() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCursor_t3566057915, ___targetListPolicy_5)); }
	inline VRTK_PolicyList_t2965133344 * get_targetListPolicy_5() const { return ___targetListPolicy_5; }
	inline VRTK_PolicyList_t2965133344 ** get_address_of_targetListPolicy_5() { return &___targetListPolicy_5; }
	inline void set_targetListPolicy_5(VRTK_PolicyList_t2965133344 * value)
	{
		___targetListPolicy_5 = value;
		Il2CppCodeGenWriteBarrier(&___targetListPolicy_5, value);
	}

	inline static int32_t get_offset_of_headsetPositionCompensation_6() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCursor_t3566057915, ___headsetPositionCompensation_6)); }
	inline bool get_headsetPositionCompensation_6() const { return ___headsetPositionCompensation_6; }
	inline bool* get_address_of_headsetPositionCompensation_6() { return &___headsetPositionCompensation_6; }
	inline void set_headsetPositionCompensation_6(bool value)
	{
		___headsetPositionCompensation_6 = value;
	}

	inline static int32_t get_offset_of_playAreaCursorCollided_7() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCursor_t3566057915, ___playAreaCursorCollided_7)); }
	inline bool get_playAreaCursorCollided_7() const { return ___playAreaCursorCollided_7; }
	inline bool* get_address_of_playAreaCursorCollided_7() { return &___playAreaCursorCollided_7; }
	inline void set_playAreaCursorCollided_7(bool value)
	{
		___playAreaCursorCollided_7 = value;
	}

	inline static int32_t get_offset_of_headsetOutOfBounds_8() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCursor_t3566057915, ___headsetOutOfBounds_8)); }
	inline bool get_headsetOutOfBounds_8() const { return ___headsetOutOfBounds_8; }
	inline bool* get_address_of_headsetOutOfBounds_8() { return &___headsetOutOfBounds_8; }
	inline void set_headsetOutOfBounds_8(bool value)
	{
		___headsetOutOfBounds_8 = value;
	}

	inline static int32_t get_offset_of_playArea_9() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCursor_t3566057915, ___playArea_9)); }
	inline Transform_t3275118058 * get_playArea_9() const { return ___playArea_9; }
	inline Transform_t3275118058 ** get_address_of_playArea_9() { return &___playArea_9; }
	inline void set_playArea_9(Transform_t3275118058 * value)
	{
		___playArea_9 = value;
		Il2CppCodeGenWriteBarrier(&___playArea_9, value);
	}

	inline static int32_t get_offset_of_playAreaCursor_10() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCursor_t3566057915, ___playAreaCursor_10)); }
	inline GameObject_t1756533147 * get_playAreaCursor_10() const { return ___playAreaCursor_10; }
	inline GameObject_t1756533147 ** get_address_of_playAreaCursor_10() { return &___playAreaCursor_10; }
	inline void set_playAreaCursor_10(GameObject_t1756533147 * value)
	{
		___playAreaCursor_10 = value;
		Il2CppCodeGenWriteBarrier(&___playAreaCursor_10, value);
	}

	inline static int32_t get_offset_of_playAreaCursorBoundaries_11() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCursor_t3566057915, ___playAreaCursorBoundaries_11)); }
	inline GameObjectU5BU5D_t3057952154* get_playAreaCursorBoundaries_11() const { return ___playAreaCursorBoundaries_11; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_playAreaCursorBoundaries_11() { return &___playAreaCursorBoundaries_11; }
	inline void set_playAreaCursorBoundaries_11(GameObjectU5BU5D_t3057952154* value)
	{
		___playAreaCursorBoundaries_11 = value;
		Il2CppCodeGenWriteBarrier(&___playAreaCursorBoundaries_11, value);
	}

	inline static int32_t get_offset_of_playAreaCursorCollider_12() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCursor_t3566057915, ___playAreaCursorCollider_12)); }
	inline BoxCollider_t22920061 * get_playAreaCursorCollider_12() const { return ___playAreaCursorCollider_12; }
	inline BoxCollider_t22920061 ** get_address_of_playAreaCursorCollider_12() { return &___playAreaCursorCollider_12; }
	inline void set_playAreaCursorCollider_12(BoxCollider_t22920061 * value)
	{
		___playAreaCursorCollider_12 = value;
		Il2CppCodeGenWriteBarrier(&___playAreaCursorCollider_12, value);
	}

	inline static int32_t get_offset_of_headset_13() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCursor_t3566057915, ___headset_13)); }
	inline Transform_t3275118058 * get_headset_13() const { return ___headset_13; }
	inline Transform_t3275118058 ** get_address_of_headset_13() { return &___headset_13; }
	inline void set_headset_13(Transform_t3275118058 * value)
	{
		___headset_13 = value;
		Il2CppCodeGenWriteBarrier(&___headset_13, value);
	}

	inline static int32_t get_offset_of_boundaryRenderers_14() { return static_cast<int32_t>(offsetof(VRTK_PlayAreaCursor_t3566057915, ___boundaryRenderers_14)); }
	inline RendererU5BU5D_t2810717544* get_boundaryRenderers_14() const { return ___boundaryRenderers_14; }
	inline RendererU5BU5D_t2810717544** get_address_of_boundaryRenderers_14() { return &___boundaryRenderers_14; }
	inline void set_boundaryRenderers_14(RendererU5BU5D_t2810717544* value)
	{
		___boundaryRenderers_14 = value;
		Il2CppCodeGenWriteBarrier(&___boundaryRenderers_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
