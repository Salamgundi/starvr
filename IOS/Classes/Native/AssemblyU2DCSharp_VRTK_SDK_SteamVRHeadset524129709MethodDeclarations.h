﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_SteamVRHeadset
struct SDK_SteamVRHeadset_t524129709;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_SteamVRHeadset::.ctor()
extern "C"  void SDK_SteamVRHeadset__ctor_m893285449 (SDK_SteamVRHeadset_t524129709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
