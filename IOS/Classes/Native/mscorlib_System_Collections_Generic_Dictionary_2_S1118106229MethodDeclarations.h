﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>
struct ShimEnumerator_t1118106229;
// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>
struct Dictionary_2_t1012981408;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1537083721_gshared (ShimEnumerator_t1118106229 * __this, Dictionary_2_t1012981408 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1537083721(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1118106229 *, Dictionary_2_t1012981408 *, const MethodInfo*))ShimEnumerator__ctor_m1537083721_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2397390536_gshared (ShimEnumerator_t1118106229 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2397390536(__this, method) ((  bool (*) (ShimEnumerator_t1118106229 *, const MethodInfo*))ShimEnumerator_MoveNext_m2397390536_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2157846820_gshared (ShimEnumerator_t1118106229 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2157846820(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1118106229 *, const MethodInfo*))ShimEnumerator_get_Entry_m2157846820_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2701528517_gshared (ShimEnumerator_t1118106229 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2701528517(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1118106229 *, const MethodInfo*))ShimEnumerator_get_Key_m2701528517_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1355949621_gshared (ShimEnumerator_t1118106229 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1355949621(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1118106229 *, const MethodInfo*))ShimEnumerator_get_Value_m1355949621_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m4247766703_gshared (ShimEnumerator_t1118106229 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m4247766703(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1118106229 *, const MethodInfo*))ShimEnumerator_get_Current_m4247766703_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>::Reset()
extern "C"  void ShimEnumerator_Reset_m957513263_gshared (ShimEnumerator_t1118106229 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m957513263(__this, method) ((  void (*) (ShimEnumerator_t1118106229 *, const MethodInfo*))ShimEnumerator_Reset_m957513263_gshared)(__this, method)
