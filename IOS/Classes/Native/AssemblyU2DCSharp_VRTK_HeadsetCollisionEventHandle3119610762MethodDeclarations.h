﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.HeadsetCollisionEventHandler
struct HeadsetCollisionEventHandler_t3119610762;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventArgs1242373387.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.HeadsetCollisionEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void HeadsetCollisionEventHandler__ctor_m3587092406 (HeadsetCollisionEventHandler_t3119610762 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.HeadsetCollisionEventHandler::Invoke(System.Object,VRTK.HeadsetCollisionEventArgs)
extern "C"  void HeadsetCollisionEventHandler_Invoke_m815480886 (HeadsetCollisionEventHandler_t3119610762 * __this, Il2CppObject * ___sender0, HeadsetCollisionEventArgs_t1242373387  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.HeadsetCollisionEventHandler::BeginInvoke(System.Object,VRTK.HeadsetCollisionEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * HeadsetCollisionEventHandler_BeginInvoke_m1140829041 (HeadsetCollisionEventHandler_t3119610762 * __this, Il2CppObject * ___sender0, HeadsetCollisionEventArgs_t1242373387  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.HeadsetCollisionEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void HeadsetCollisionEventHandler_EndInvoke_m1110430996 (HeadsetCollisionEventHandler_t3119610762 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
