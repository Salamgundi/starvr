﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_TouchpadControl
struct VRTK_TouchpadControl_t3191058221;
// VRTK.VRTK_ObjectControl
struct VRTK_ObjectControl_t724022372;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"

// System.Void VRTK.VRTK_TouchpadControl::.ctor()
extern "C"  void VRTK_TouchpadControl__ctor_m3214216801 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadControl::OnEnable()
extern "C"  void VRTK_TouchpadControl_OnEnable_m2800640977 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadControl::ControlFixedUpdate()
extern "C"  void VRTK_TouchpadControl_ControlFixedUpdate_m251177845 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_ObjectControl VRTK.VRTK_TouchpadControl::GetOtherControl()
extern "C"  VRTK_ObjectControl_t724022372 * VRTK_TouchpadControl_GetOtherControl_m3058459202 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadControl::SetListeners(System.Boolean)
extern "C"  void VRTK_TouchpadControl_SetListeners_m3984552921 (VRTK_TouchpadControl_t3191058221 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_TouchpadControl::IsInAction()
extern "C"  bool VRTK_TouchpadControl_IsInAction_m882793342 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_TouchpadControl::OutsideDeadzone(System.Single,System.Single)
extern "C"  bool VRTK_TouchpadControl_OutsideDeadzone_m1532533014 (VRTK_TouchpadControl_t3191058221 * __this, float ___axisValue0, float ___deadzoneThreshold1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_TouchpadControl::ValidPrimaryButton()
extern "C"  bool VRTK_TouchpadControl_ValidPrimaryButton_m2469929053 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadControl::ModifierButtonActive()
extern "C"  void VRTK_TouchpadControl_ModifierButtonActive_m4102088174 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_TouchpadControl::TouchpadTouched()
extern "C"  bool VRTK_TouchpadControl_TouchpadTouched_m1583952841 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadControl::TouchpadAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_TouchpadControl_TouchpadAxisChanged_m528373126 (VRTK_TouchpadControl_t3191058221 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadControl::TouchpadTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_TouchpadControl_TouchpadTouchEnd_m1541439557 (VRTK_TouchpadControl_t3191058221 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
