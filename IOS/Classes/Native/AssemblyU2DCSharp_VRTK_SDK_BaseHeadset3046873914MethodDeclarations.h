﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_BaseHeadset
struct SDK_BaseHeadset_t3046873914;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_BaseHeadset::.ctor()
extern "C"  void SDK_BaseHeadset__ctor_m4287288028 (SDK_BaseHeadset_t3046873914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.SDK_BaseHeadset::GetSDKManagerHeadset()
extern "C"  Transform_t3275118058 * SDK_BaseHeadset_GetSDKManagerHeadset_m4089438901 (SDK_BaseHeadset_t3046873914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
