﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRExtendedDisplay/_GetDXGIOutputInfo
struct _GetDXGIOutputInfo_t2084284319;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRExtendedDisplay/_GetDXGIOutputInfo::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetDXGIOutputInfo__ctor_m4002233304 (_GetDXGIOutputInfo_t2084284319 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRExtendedDisplay/_GetDXGIOutputInfo::Invoke(System.Int32&,System.Int32&)
extern "C"  void _GetDXGIOutputInfo_Invoke_m3679741020 (_GetDXGIOutputInfo_t2084284319 * __this, int32_t* ___pnAdapterIndex0, int32_t* ___pnAdapterOutputIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRExtendedDisplay/_GetDXGIOutputInfo::BeginInvoke(System.Int32&,System.Int32&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetDXGIOutputInfo_BeginInvoke_m2506986361 (_GetDXGIOutputInfo_t2084284319 * __this, int32_t* ___pnAdapterIndex0, int32_t* ___pnAdapterOutputIndex1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRExtendedDisplay/_GetDXGIOutputInfo::EndInvoke(System.Int32&,System.Int32&,System.IAsyncResult)
extern "C"  void _GetDXGIOutputInfo_EndInvoke_m2113243226 (_GetDXGIOutputInfo_t2084284319 * __this, int32_t* ___pnAdapterIndex0, int32_t* ___pnAdapterOutputIndex1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
