﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_IntPtr2504060609.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.RenderModel_t_Packed
#pragma pack(push, tp, 4)
struct  RenderModel_t_Packed_t393767115 
{
public:
	// System.IntPtr Valve.VR.RenderModel_t_Packed::rVertexData
	IntPtr_t ___rVertexData_0;
	// System.UInt32 Valve.VR.RenderModel_t_Packed::unVertexCount
	uint32_t ___unVertexCount_1;
	// System.IntPtr Valve.VR.RenderModel_t_Packed::rIndexData
	IntPtr_t ___rIndexData_2;
	// System.UInt32 Valve.VR.RenderModel_t_Packed::unTriangleCount
	uint32_t ___unTriangleCount_3;
	// System.Int32 Valve.VR.RenderModel_t_Packed::diffuseTextureId
	int32_t ___diffuseTextureId_4;

public:
	inline static int32_t get_offset_of_rVertexData_0() { return static_cast<int32_t>(offsetof(RenderModel_t_Packed_t393767115, ___rVertexData_0)); }
	inline IntPtr_t get_rVertexData_0() const { return ___rVertexData_0; }
	inline IntPtr_t* get_address_of_rVertexData_0() { return &___rVertexData_0; }
	inline void set_rVertexData_0(IntPtr_t value)
	{
		___rVertexData_0 = value;
	}

	inline static int32_t get_offset_of_unVertexCount_1() { return static_cast<int32_t>(offsetof(RenderModel_t_Packed_t393767115, ___unVertexCount_1)); }
	inline uint32_t get_unVertexCount_1() const { return ___unVertexCount_1; }
	inline uint32_t* get_address_of_unVertexCount_1() { return &___unVertexCount_1; }
	inline void set_unVertexCount_1(uint32_t value)
	{
		___unVertexCount_1 = value;
	}

	inline static int32_t get_offset_of_rIndexData_2() { return static_cast<int32_t>(offsetof(RenderModel_t_Packed_t393767115, ___rIndexData_2)); }
	inline IntPtr_t get_rIndexData_2() const { return ___rIndexData_2; }
	inline IntPtr_t* get_address_of_rIndexData_2() { return &___rIndexData_2; }
	inline void set_rIndexData_2(IntPtr_t value)
	{
		___rIndexData_2 = value;
	}

	inline static int32_t get_offset_of_unTriangleCount_3() { return static_cast<int32_t>(offsetof(RenderModel_t_Packed_t393767115, ___unTriangleCount_3)); }
	inline uint32_t get_unTriangleCount_3() const { return ___unTriangleCount_3; }
	inline uint32_t* get_address_of_unTriangleCount_3() { return &___unTriangleCount_3; }
	inline void set_unTriangleCount_3(uint32_t value)
	{
		___unTriangleCount_3 = value;
	}

	inline static int32_t get_offset_of_diffuseTextureId_4() { return static_cast<int32_t>(offsetof(RenderModel_t_Packed_t393767115, ___diffuseTextureId_4)); }
	inline int32_t get_diffuseTextureId_4() const { return ___diffuseTextureId_4; }
	inline int32_t* get_address_of_diffuseTextureId_4() { return &___diffuseTextureId_4; }
	inline void set_diffuseTextureId_4(int32_t value)
	{
		___diffuseTextureId_4 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
