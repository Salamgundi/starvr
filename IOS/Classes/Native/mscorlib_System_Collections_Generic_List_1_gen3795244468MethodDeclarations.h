﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::.ctor()
#define List_1__ctor_m3698296865(__this, method) ((  void (*) (List_1_t3795244468 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m515848946(__this, ___collection0, method) ((  void (*) (List_1_t3795244468 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m454375187_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::.ctor(System.Int32)
#define List_1__ctor_m2042796428(__this, ___capacity0, method) ((  void (*) (List_1_t3795244468 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::.ctor(T[],System.Int32)
#define List_1__ctor_m1484202976(__this, ___data0, ___size1, method) ((  void (*) (List_1_t3795244468 *, RadialMenuButtonU5BU5D_t2518050201*, int32_t, const MethodInfo*))List_1__ctor_m3410056391_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::.cctor()
#define List_1__cctor_m901062060(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2971622549(__this, method) ((  Il2CppObject* (*) (List_1_t3795244468 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m508829505(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3795244468 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m876740498(__this, method) ((  Il2CppObject * (*) (List_1_t3795244468 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3717962509(__this, ___item0, method) ((  int32_t (*) (List_1_t3795244468 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2185878465(__this, ___item0, method) ((  bool (*) (List_1_t3795244468 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m2701747303(__this, ___item0, method) ((  int32_t (*) (List_1_t3795244468 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3833309316(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3795244468 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1295115326(__this, ___item0, method) ((  void (*) (List_1_t3795244468 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3205253566(__this, method) ((  bool (*) (List_1_t3795244468 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1321378333(__this, method) ((  bool (*) (List_1_t3795244468 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m4000470193(__this, method) ((  Il2CppObject * (*) (List_1_t3795244468 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m3129729460(__this, method) ((  bool (*) (List_1_t3795244468 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m385284353(__this, method) ((  bool (*) (List_1_t3795244468 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m2670681672(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3795244468 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2196857327(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3795244468 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::Add(T)
#define List_1_Add_m811981494(__this, ___item0, method) ((  void (*) (List_1_t3795244468 *, RadialMenuButton_t131156040 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1096352961(__this, ___newCount0, method) ((  void (*) (List_1_t3795244468 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m2642573924(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3795244468 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1904903857_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m951612681(__this, ___collection0, method) ((  void (*) (List_1_t3795244468 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1404543609(__this, ___enumerable0, method) ((  void (*) (List_1_t3795244468 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m765584918(__this, ___collection0, method) ((  void (*) (List_1_t3795244468 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<VRTK.RadialMenuButton>::AsReadOnly()
#define List_1_AsReadOnly_m850040025(__this, method) ((  ReadOnlyCollection_1_t316941732 * (*) (List_1_t3795244468 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::Clear()
#define List_1_Clear_m3808751356(__this, method) ((  void (*) (List_1_t3795244468 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VRTK.RadialMenuButton>::Contains(T)
#define List_1_Contains_m2189216670(__this, ___item0, method) ((  bool (*) (List_1_t3795244468 *, RadialMenuButton_t131156040 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m261892848(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3795244468 *, RadialMenuButtonU5BU5D_t2518050201*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<VRTK.RadialMenuButton>::Find(System.Predicate`1<T>)
#define List_1_Find_m851807112(__this, ___match0, method) ((  RadialMenuButton_t131156040 * (*) (List_1_t3795244468 *, Predicate_1_t2869093451 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2360013233(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2869093451 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VRTK.RadialMenuButton>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m3297257969(__this, ___match0, method) ((  List_1_t3795244468 * (*) (List_1_t3795244468 *, Predicate_1_t2869093451 *, const MethodInfo*))List_1_FindAll_m1864150752_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VRTK.RadialMenuButton>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m1306263067(__this, ___match0, method) ((  List_1_t3795244468 * (*) (List_1_t3795244468 *, Predicate_1_t2869093451 *, const MethodInfo*))List_1_FindAllStackBits_m1687626136_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VRTK.RadialMenuButton>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m3891173451(__this, ___match0, method) ((  List_1_t3795244468 * (*) (List_1_t3795244468 *, Predicate_1_t2869093451 *, const MethodInfo*))List_1_FindAllList_m3013325848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.RadialMenuButton>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m326396469(__this, ___match0, method) ((  int32_t (*) (List_1_t3795244468 *, Predicate_1_t2869093451 *, const MethodInfo*))List_1_FindIndex_m552623364_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.RadialMenuButton>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3359370136(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3795244468 *, int32_t, int32_t, Predicate_1_t2869093451 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<VRTK.RadialMenuButton>::GetEnumerator()
#define List_1_GetEnumerator_m333584057(__this, method) ((  Enumerator_t3329974142  (*) (List_1_t3795244468 *, const MethodInfo*))List_1_GetEnumerator_m3294992758_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.RadialMenuButton>::IndexOf(T)
#define List_1_IndexOf_m72967578(__this, ___item0, method) ((  int32_t (*) (List_1_t3795244468 *, RadialMenuButton_t131156040 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3056573069(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3795244468 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3313984636(__this, ___index0, method) ((  void (*) (List_1_t3795244468 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::Insert(System.Int32,T)
#define List_1_Insert_m2502934039(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3795244468 *, int32_t, RadialMenuButton_t131156040 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m619546694(__this, ___collection0, method) ((  void (*) (List_1_t3795244468 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<VRTK.RadialMenuButton>::Remove(T)
#define List_1_Remove_m3245589499(__this, ___item0, method) ((  bool (*) (List_1_t3795244468 *, RadialMenuButton_t131156040 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.RadialMenuButton>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m728767525(__this, ___match0, method) ((  int32_t (*) (List_1_t3795244468 *, Predicate_1_t2869093451 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m794430147(__this, ___index0, method) ((  void (*) (List_1_t3795244468 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m274616928(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3795244468 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3877627853_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::Reverse()
#define List_1_Reverse_m3765249285(__this, method) ((  void (*) (List_1_t3795244468 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::Sort()
#define List_1_Sort_m956274659(__this, method) ((  void (*) (List_1_t3795244468 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m4010845332(__this, ___comparison0, method) ((  void (*) (List_1_t3795244468 *, Comparison_1_t1392894891 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<VRTK.RadialMenuButton>::ToArray()
#define List_1_ToArray_m3232131748(__this, method) ((  RadialMenuButtonU5BU5D_t2518050201* (*) (List_1_t3795244468 *, const MethodInfo*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::TrimExcess()
#define List_1_TrimExcess_m991182122(__this, method) ((  void (*) (List_1_t3795244468 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.RadialMenuButton>::get_Capacity()
#define List_1_get_Capacity_m4073219912(__this, method) ((  int32_t (*) (List_1_t3795244468 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1930510345(__this, ___value0, method) ((  void (*) (List_1_t3795244468 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.RadialMenuButton>::get_Count()
#define List_1_get_Count_m3494777816(__this, method) ((  int32_t (*) (List_1_t3795244468 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// T System.Collections.Generic.List`1<VRTK.RadialMenuButton>::get_Item(System.Int32)
#define List_1_get_Item_m3951968901(__this, ___index0, method) ((  RadialMenuButton_t131156040 * (*) (List_1_t3795244468 *, int32_t, const MethodInfo*))List_1_get_Item_m1354830498_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VRTK.RadialMenuButton>::set_Item(System.Int32,T)
#define List_1_set_Item_m3964075322(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3795244468 *, int32_t, RadialMenuButton_t131156040 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
