﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// SteamVR_Events/Action
struct Action_t1836998693;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SteamVR_ControllerManager
struct  SteamVR_ControllerManager_t3520649604  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject SteamVR_ControllerManager::left
	GameObject_t1756533147 * ___left_2;
	// UnityEngine.GameObject SteamVR_ControllerManager::right
	GameObject_t1756533147 * ___right_3;
	// UnityEngine.GameObject[] SteamVR_ControllerManager::objects
	GameObjectU5BU5D_t3057952154* ___objects_4;
	// System.Boolean SteamVR_ControllerManager::assignAllBeforeIdentified
	bool ___assignAllBeforeIdentified_5;
	// System.UInt32[] SteamVR_ControllerManager::indices
	UInt32U5BU5D_t59386216* ___indices_6;
	// System.Boolean[] SteamVR_ControllerManager::connected
	BooleanU5BU5D_t3568034315* ___connected_7;
	// System.UInt32 SteamVR_ControllerManager::leftIndex
	uint32_t ___leftIndex_8;
	// System.UInt32 SteamVR_ControllerManager::rightIndex
	uint32_t ___rightIndex_9;
	// SteamVR_Events/Action SteamVR_ControllerManager::inputFocusAction
	Action_t1836998693 * ___inputFocusAction_10;
	// SteamVR_Events/Action SteamVR_ControllerManager::deviceConnectedAction
	Action_t1836998693 * ___deviceConnectedAction_11;
	// SteamVR_Events/Action SteamVR_ControllerManager::trackedDeviceRoleChangedAction
	Action_t1836998693 * ___trackedDeviceRoleChangedAction_12;

public:
	inline static int32_t get_offset_of_left_2() { return static_cast<int32_t>(offsetof(SteamVR_ControllerManager_t3520649604, ___left_2)); }
	inline GameObject_t1756533147 * get_left_2() const { return ___left_2; }
	inline GameObject_t1756533147 ** get_address_of_left_2() { return &___left_2; }
	inline void set_left_2(GameObject_t1756533147 * value)
	{
		___left_2 = value;
		Il2CppCodeGenWriteBarrier(&___left_2, value);
	}

	inline static int32_t get_offset_of_right_3() { return static_cast<int32_t>(offsetof(SteamVR_ControllerManager_t3520649604, ___right_3)); }
	inline GameObject_t1756533147 * get_right_3() const { return ___right_3; }
	inline GameObject_t1756533147 ** get_address_of_right_3() { return &___right_3; }
	inline void set_right_3(GameObject_t1756533147 * value)
	{
		___right_3 = value;
		Il2CppCodeGenWriteBarrier(&___right_3, value);
	}

	inline static int32_t get_offset_of_objects_4() { return static_cast<int32_t>(offsetof(SteamVR_ControllerManager_t3520649604, ___objects_4)); }
	inline GameObjectU5BU5D_t3057952154* get_objects_4() const { return ___objects_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_objects_4() { return &___objects_4; }
	inline void set_objects_4(GameObjectU5BU5D_t3057952154* value)
	{
		___objects_4 = value;
		Il2CppCodeGenWriteBarrier(&___objects_4, value);
	}

	inline static int32_t get_offset_of_assignAllBeforeIdentified_5() { return static_cast<int32_t>(offsetof(SteamVR_ControllerManager_t3520649604, ___assignAllBeforeIdentified_5)); }
	inline bool get_assignAllBeforeIdentified_5() const { return ___assignAllBeforeIdentified_5; }
	inline bool* get_address_of_assignAllBeforeIdentified_5() { return &___assignAllBeforeIdentified_5; }
	inline void set_assignAllBeforeIdentified_5(bool value)
	{
		___assignAllBeforeIdentified_5 = value;
	}

	inline static int32_t get_offset_of_indices_6() { return static_cast<int32_t>(offsetof(SteamVR_ControllerManager_t3520649604, ___indices_6)); }
	inline UInt32U5BU5D_t59386216* get_indices_6() const { return ___indices_6; }
	inline UInt32U5BU5D_t59386216** get_address_of_indices_6() { return &___indices_6; }
	inline void set_indices_6(UInt32U5BU5D_t59386216* value)
	{
		___indices_6 = value;
		Il2CppCodeGenWriteBarrier(&___indices_6, value);
	}

	inline static int32_t get_offset_of_connected_7() { return static_cast<int32_t>(offsetof(SteamVR_ControllerManager_t3520649604, ___connected_7)); }
	inline BooleanU5BU5D_t3568034315* get_connected_7() const { return ___connected_7; }
	inline BooleanU5BU5D_t3568034315** get_address_of_connected_7() { return &___connected_7; }
	inline void set_connected_7(BooleanU5BU5D_t3568034315* value)
	{
		___connected_7 = value;
		Il2CppCodeGenWriteBarrier(&___connected_7, value);
	}

	inline static int32_t get_offset_of_leftIndex_8() { return static_cast<int32_t>(offsetof(SteamVR_ControllerManager_t3520649604, ___leftIndex_8)); }
	inline uint32_t get_leftIndex_8() const { return ___leftIndex_8; }
	inline uint32_t* get_address_of_leftIndex_8() { return &___leftIndex_8; }
	inline void set_leftIndex_8(uint32_t value)
	{
		___leftIndex_8 = value;
	}

	inline static int32_t get_offset_of_rightIndex_9() { return static_cast<int32_t>(offsetof(SteamVR_ControllerManager_t3520649604, ___rightIndex_9)); }
	inline uint32_t get_rightIndex_9() const { return ___rightIndex_9; }
	inline uint32_t* get_address_of_rightIndex_9() { return &___rightIndex_9; }
	inline void set_rightIndex_9(uint32_t value)
	{
		___rightIndex_9 = value;
	}

	inline static int32_t get_offset_of_inputFocusAction_10() { return static_cast<int32_t>(offsetof(SteamVR_ControllerManager_t3520649604, ___inputFocusAction_10)); }
	inline Action_t1836998693 * get_inputFocusAction_10() const { return ___inputFocusAction_10; }
	inline Action_t1836998693 ** get_address_of_inputFocusAction_10() { return &___inputFocusAction_10; }
	inline void set_inputFocusAction_10(Action_t1836998693 * value)
	{
		___inputFocusAction_10 = value;
		Il2CppCodeGenWriteBarrier(&___inputFocusAction_10, value);
	}

	inline static int32_t get_offset_of_deviceConnectedAction_11() { return static_cast<int32_t>(offsetof(SteamVR_ControllerManager_t3520649604, ___deviceConnectedAction_11)); }
	inline Action_t1836998693 * get_deviceConnectedAction_11() const { return ___deviceConnectedAction_11; }
	inline Action_t1836998693 ** get_address_of_deviceConnectedAction_11() { return &___deviceConnectedAction_11; }
	inline void set_deviceConnectedAction_11(Action_t1836998693 * value)
	{
		___deviceConnectedAction_11 = value;
		Il2CppCodeGenWriteBarrier(&___deviceConnectedAction_11, value);
	}

	inline static int32_t get_offset_of_trackedDeviceRoleChangedAction_12() { return static_cast<int32_t>(offsetof(SteamVR_ControllerManager_t3520649604, ___trackedDeviceRoleChangedAction_12)); }
	inline Action_t1836998693 * get_trackedDeviceRoleChangedAction_12() const { return ___trackedDeviceRoleChangedAction_12; }
	inline Action_t1836998693 ** get_address_of_trackedDeviceRoleChangedAction_12() { return &___trackedDeviceRoleChangedAction_12; }
	inline void set_trackedDeviceRoleChangedAction_12(Action_t1836998693 * value)
	{
		___trackedDeviceRoleChangedAction_12 = value;
		Il2CppCodeGenWriteBarrier(&___trackedDeviceRoleChangedAction_12, value);
	}
};

struct SteamVR_ControllerManager_t3520649604_StaticFields
{
public:
	// System.String[] SteamVR_ControllerManager::labels
	StringU5BU5D_t1642385972* ___labels_13;

public:
	inline static int32_t get_offset_of_labels_13() { return static_cast<int32_t>(offsetof(SteamVR_ControllerManager_t3520649604_StaticFields, ___labels_13)); }
	inline StringU5BU5D_t1642385972* get_labels_13() const { return ___labels_13; }
	inline StringU5BU5D_t1642385972** get_address_of_labels_13() { return &___labels_13; }
	inline void set_labels_13(StringU5BU5D_t1642385972* value)
	{
		___labels_13 = value;
		Il2CppCodeGenWriteBarrier(&___labels_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
