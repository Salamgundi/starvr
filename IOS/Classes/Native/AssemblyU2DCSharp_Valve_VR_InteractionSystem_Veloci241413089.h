﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.VelocityEstimator
struct VelocityEstimator_t1153298725;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0
struct  U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::<previousPosition>__0
	Vector3_t2243707580  ___U3CpreviousPositionU3E__0_0;
	// UnityEngine.Quaternion Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::<previousRotation>__1
	Quaternion_t4030073918  ___U3CpreviousRotationU3E__1_1;
	// System.Single Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::<velocityFactor>__2
	float ___U3CvelocityFactorU3E__2_2;
	// System.Int32 Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::<v>__3
	int32_t ___U3CvU3E__3_3;
	// System.Int32 Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::<w>__4
	int32_t ___U3CwU3E__4_4;
	// UnityEngine.Quaternion Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::<deltaRotation>__5
	Quaternion_t4030073918  ___U3CdeltaRotationU3E__5_5;
	// System.Single Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::<theta>__6
	float ___U3CthetaU3E__6_6;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::<angularVelocity>__7
	Vector3_t2243707580  ___U3CangularVelocityU3E__7_7;
	// Valve.VR.InteractionSystem.VelocityEstimator Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::$this
	VelocityEstimator_t1153298725 * ___U24this_8;
	// System.Object Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::$current
	Il2CppObject * ___U24current_9;
	// System.Boolean Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 Valve.VR.InteractionSystem.VelocityEstimator/<EstimateVelocityCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CpreviousPositionU3E__0_0() { return static_cast<int32_t>(offsetof(U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089, ___U3CpreviousPositionU3E__0_0)); }
	inline Vector3_t2243707580  get_U3CpreviousPositionU3E__0_0() const { return ___U3CpreviousPositionU3E__0_0; }
	inline Vector3_t2243707580 * get_address_of_U3CpreviousPositionU3E__0_0() { return &___U3CpreviousPositionU3E__0_0; }
	inline void set_U3CpreviousPositionU3E__0_0(Vector3_t2243707580  value)
	{
		___U3CpreviousPositionU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CpreviousRotationU3E__1_1() { return static_cast<int32_t>(offsetof(U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089, ___U3CpreviousRotationU3E__1_1)); }
	inline Quaternion_t4030073918  get_U3CpreviousRotationU3E__1_1() const { return ___U3CpreviousRotationU3E__1_1; }
	inline Quaternion_t4030073918 * get_address_of_U3CpreviousRotationU3E__1_1() { return &___U3CpreviousRotationU3E__1_1; }
	inline void set_U3CpreviousRotationU3E__1_1(Quaternion_t4030073918  value)
	{
		___U3CpreviousRotationU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CvelocityFactorU3E__2_2() { return static_cast<int32_t>(offsetof(U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089, ___U3CvelocityFactorU3E__2_2)); }
	inline float get_U3CvelocityFactorU3E__2_2() const { return ___U3CvelocityFactorU3E__2_2; }
	inline float* get_address_of_U3CvelocityFactorU3E__2_2() { return &___U3CvelocityFactorU3E__2_2; }
	inline void set_U3CvelocityFactorU3E__2_2(float value)
	{
		___U3CvelocityFactorU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CvU3E__3_3() { return static_cast<int32_t>(offsetof(U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089, ___U3CvU3E__3_3)); }
	inline int32_t get_U3CvU3E__3_3() const { return ___U3CvU3E__3_3; }
	inline int32_t* get_address_of_U3CvU3E__3_3() { return &___U3CvU3E__3_3; }
	inline void set_U3CvU3E__3_3(int32_t value)
	{
		___U3CvU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CwU3E__4_4() { return static_cast<int32_t>(offsetof(U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089, ___U3CwU3E__4_4)); }
	inline int32_t get_U3CwU3E__4_4() const { return ___U3CwU3E__4_4; }
	inline int32_t* get_address_of_U3CwU3E__4_4() { return &___U3CwU3E__4_4; }
	inline void set_U3CwU3E__4_4(int32_t value)
	{
		___U3CwU3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaRotationU3E__5_5() { return static_cast<int32_t>(offsetof(U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089, ___U3CdeltaRotationU3E__5_5)); }
	inline Quaternion_t4030073918  get_U3CdeltaRotationU3E__5_5() const { return ___U3CdeltaRotationU3E__5_5; }
	inline Quaternion_t4030073918 * get_address_of_U3CdeltaRotationU3E__5_5() { return &___U3CdeltaRotationU3E__5_5; }
	inline void set_U3CdeltaRotationU3E__5_5(Quaternion_t4030073918  value)
	{
		___U3CdeltaRotationU3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U3CthetaU3E__6_6() { return static_cast<int32_t>(offsetof(U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089, ___U3CthetaU3E__6_6)); }
	inline float get_U3CthetaU3E__6_6() const { return ___U3CthetaU3E__6_6; }
	inline float* get_address_of_U3CthetaU3E__6_6() { return &___U3CthetaU3E__6_6; }
	inline void set_U3CthetaU3E__6_6(float value)
	{
		___U3CthetaU3E__6_6 = value;
	}

	inline static int32_t get_offset_of_U3CangularVelocityU3E__7_7() { return static_cast<int32_t>(offsetof(U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089, ___U3CangularVelocityU3E__7_7)); }
	inline Vector3_t2243707580  get_U3CangularVelocityU3E__7_7() const { return ___U3CangularVelocityU3E__7_7; }
	inline Vector3_t2243707580 * get_address_of_U3CangularVelocityU3E__7_7() { return &___U3CangularVelocityU3E__7_7; }
	inline void set_U3CangularVelocityU3E__7_7(Vector3_t2243707580  value)
	{
		___U3CangularVelocityU3E__7_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089, ___U24this_8)); }
	inline VelocityEstimator_t1153298725 * get_U24this_8() const { return ___U24this_8; }
	inline VelocityEstimator_t1153298725 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(VelocityEstimator_t1153298725 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_8, value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CEstimateVelocityCoroutineU3Ec__Iterator0_t241413089, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
