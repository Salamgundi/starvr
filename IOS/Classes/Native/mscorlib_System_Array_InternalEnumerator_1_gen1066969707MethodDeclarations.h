﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1066969707.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_UnityEngine_ParticleSystem_Burst208217445.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.ParticleSystem/Burst>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m54940386_gshared (InternalEnumerator_1_t1066969707 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m54940386(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1066969707 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m54940386_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ParticleSystem/Burst>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4194936710_gshared (InternalEnumerator_1_t1066969707 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4194936710(__this, method) ((  void (*) (InternalEnumerator_1_t1066969707 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4194936710_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ParticleSystem/Burst>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4240423430_gshared (InternalEnumerator_1_t1066969707 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4240423430(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1066969707 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4240423430_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ParticleSystem/Burst>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1716881905_gshared (InternalEnumerator_1_t1066969707 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1716881905(__this, method) ((  void (*) (InternalEnumerator_1_t1066969707 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1716881905_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ParticleSystem/Burst>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2849907874_gshared (InternalEnumerator_1_t1066969707 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2849907874(__this, method) ((  bool (*) (InternalEnumerator_1_t1066969707 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2849907874_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.ParticleSystem/Burst>::get_Current()
extern "C"  Burst_t208217445  InternalEnumerator_1_get_Current_m2065109985_gshared (InternalEnumerator_1_t1066969707 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2065109985(__this, method) ((  Burst_t208217445  (*) (InternalEnumerator_1_t1066969707 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2065109985_gshared)(__this, method)
