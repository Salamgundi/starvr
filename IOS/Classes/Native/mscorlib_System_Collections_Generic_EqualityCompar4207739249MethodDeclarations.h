﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<VRTK.VRTK_SDKManager/SupportedSDKs>
struct EqualityComparer_1_t4207739249;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<VRTK.VRTK_SDKManager/SupportedSDKs>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m647393069_gshared (EqualityComparer_1_t4207739249 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m647393069(__this, method) ((  void (*) (EqualityComparer_1_t4207739249 *, const MethodInfo*))EqualityComparer_1__ctor_m647393069_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<VRTK.VRTK_SDKManager/SupportedSDKs>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2533425008_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m2533425008(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m2533425008_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<VRTK.VRTK_SDKManager/SupportedSDKs>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2931945694_gshared (EqualityComparer_1_t4207739249 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2931945694(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t4207739249 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2931945694_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<VRTK.VRTK_SDKManager/SupportedSDKs>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m345250020_gshared (EqualityComparer_1_t4207739249 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m345250020(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t4207739249 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m345250020_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<VRTK.VRTK_SDKManager/SupportedSDKs>::get_Default()
extern "C"  EqualityComparer_1_t4207739249 * EqualityComparer_1_get_Default_m2246220017_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m2246220017(__this /* static, unused */, method) ((  EqualityComparer_1_t4207739249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m2246220017_gshared)(__this /* static, unused */, method)
