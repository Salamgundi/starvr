﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1
struct  U3CHapticPulseU3Ec__Iterator1_t288649631  : public Il2CppObject
{
public:
	// System.Single VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1::pulseInterval
	float ___pulseInterval_0;
	// System.Single VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1::duration
	float ___duration_1;
	// System.Single VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1::hapticPulseStrength
	float ___hapticPulseStrength_2;
	// VRTK.VRTK_ControllerActions VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1::$this
	VRTK_ControllerActions_t3642353851 * ___U24this_3;
	// System.Object VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_pulseInterval_0() { return static_cast<int32_t>(offsetof(U3CHapticPulseU3Ec__Iterator1_t288649631, ___pulseInterval_0)); }
	inline float get_pulseInterval_0() const { return ___pulseInterval_0; }
	inline float* get_address_of_pulseInterval_0() { return &___pulseInterval_0; }
	inline void set_pulseInterval_0(float value)
	{
		___pulseInterval_0 = value;
	}

	inline static int32_t get_offset_of_duration_1() { return static_cast<int32_t>(offsetof(U3CHapticPulseU3Ec__Iterator1_t288649631, ___duration_1)); }
	inline float get_duration_1() const { return ___duration_1; }
	inline float* get_address_of_duration_1() { return &___duration_1; }
	inline void set_duration_1(float value)
	{
		___duration_1 = value;
	}

	inline static int32_t get_offset_of_hapticPulseStrength_2() { return static_cast<int32_t>(offsetof(U3CHapticPulseU3Ec__Iterator1_t288649631, ___hapticPulseStrength_2)); }
	inline float get_hapticPulseStrength_2() const { return ___hapticPulseStrength_2; }
	inline float* get_address_of_hapticPulseStrength_2() { return &___hapticPulseStrength_2; }
	inline void set_hapticPulseStrength_2(float value)
	{
		___hapticPulseStrength_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CHapticPulseU3Ec__Iterator1_t288649631, ___U24this_3)); }
	inline VRTK_ControllerActions_t3642353851 * get_U24this_3() const { return ___U24this_3; }
	inline VRTK_ControllerActions_t3642353851 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(VRTK_ControllerActions_t3642353851 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CHapticPulseU3Ec__Iterator1_t288649631, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CHapticPulseU3Ec__Iterator1_t288649631, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CHapticPulseU3Ec__Iterator1_t288649631, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
