﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0
struct U3ClerpToPositionU3Ec__Iterator0_t2809459013;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::.ctor()
extern "C"  void U3ClerpToPositionU3Ec__Iterator0__ctor_m2112275154 (U3ClerpToPositionU3Ec__Iterator0_t2809459013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::MoveNext()
extern "C"  bool U3ClerpToPositionU3Ec__Iterator0_MoveNext_m4095569902 (U3ClerpToPositionU3Ec__Iterator0_t2809459013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3ClerpToPositionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2128223136 (U3ClerpToPositionU3Ec__Iterator0_t2809459013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3ClerpToPositionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3671224120 (U3ClerpToPositionU3Ec__Iterator0_t2809459013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::Dispose()
extern "C"  void U3ClerpToPositionU3Ec__Iterator0_Dispose_m1720877903 (U3ClerpToPositionU3Ec__Iterator0_t2809459013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::Reset()
extern "C"  void U3ClerpToPositionU3Ec__Iterator0_Reset_m402193069 (U3ClerpToPositionU3Ec__Iterator0_t2809459013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
