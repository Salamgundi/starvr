﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ItemPackage
struct ItemPackage_t3423754743;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.ItemPackage::.ctor()
extern "C"  void ItemPackage__ctor_m2125398353 (ItemPackage_t3423754743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
