﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Int32>
struct  AdaptiveSetting_1_t744429243  : public Il2CppObject
{
public:
	// T VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1::<previousValue>k__BackingField
	int32_t ___U3CpreviousValueU3Ek__BackingField_0;
	// System.Int32 VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1::<lastChangeFrameCount>k__BackingField
	int32_t ___U3ClastChangeFrameCountU3Ek__BackingField_1;
	// System.Int32 VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1::increaseFrameCost
	int32_t ___increaseFrameCost_2;
	// System.Int32 VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1::decreaseFrameCost
	int32_t ___decreaseFrameCost_3;
	// T VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1::_currentValue
	int32_t ____currentValue_4;

public:
	inline static int32_t get_offset_of_U3CpreviousValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdaptiveSetting_1_t744429243, ___U3CpreviousValueU3Ek__BackingField_0)); }
	inline int32_t get_U3CpreviousValueU3Ek__BackingField_0() const { return ___U3CpreviousValueU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CpreviousValueU3Ek__BackingField_0() { return &___U3CpreviousValueU3Ek__BackingField_0; }
	inline void set_U3CpreviousValueU3Ek__BackingField_0(int32_t value)
	{
		___U3CpreviousValueU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3ClastChangeFrameCountU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdaptiveSetting_1_t744429243, ___U3ClastChangeFrameCountU3Ek__BackingField_1)); }
	inline int32_t get_U3ClastChangeFrameCountU3Ek__BackingField_1() const { return ___U3ClastChangeFrameCountU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3ClastChangeFrameCountU3Ek__BackingField_1() { return &___U3ClastChangeFrameCountU3Ek__BackingField_1; }
	inline void set_U3ClastChangeFrameCountU3Ek__BackingField_1(int32_t value)
	{
		___U3ClastChangeFrameCountU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_increaseFrameCost_2() { return static_cast<int32_t>(offsetof(AdaptiveSetting_1_t744429243, ___increaseFrameCost_2)); }
	inline int32_t get_increaseFrameCost_2() const { return ___increaseFrameCost_2; }
	inline int32_t* get_address_of_increaseFrameCost_2() { return &___increaseFrameCost_2; }
	inline void set_increaseFrameCost_2(int32_t value)
	{
		___increaseFrameCost_2 = value;
	}

	inline static int32_t get_offset_of_decreaseFrameCost_3() { return static_cast<int32_t>(offsetof(AdaptiveSetting_1_t744429243, ___decreaseFrameCost_3)); }
	inline int32_t get_decreaseFrameCost_3() const { return ___decreaseFrameCost_3; }
	inline int32_t* get_address_of_decreaseFrameCost_3() { return &___decreaseFrameCost_3; }
	inline void set_decreaseFrameCost_3(int32_t value)
	{
		___decreaseFrameCost_3 = value;
	}

	inline static int32_t get_offset_of__currentValue_4() { return static_cast<int32_t>(offsetof(AdaptiveSetting_1_t744429243, ____currentValue_4)); }
	inline int32_t get__currentValue_4() const { return ____currentValue_4; }
	inline int32_t* get_address_of__currentValue_4() { return &____currentValue_4; }
	inline void set__currentValue_4(int32_t value)
	{
		____currentValue_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
