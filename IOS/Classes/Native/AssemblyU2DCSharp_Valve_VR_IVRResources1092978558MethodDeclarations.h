﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRResources
struct IVRResources_t1092978558;
struct IVRResources_t1092978558_marshaled_pinvoke;
struct IVRResources_t1092978558_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IVRResources_t1092978558;
struct IVRResources_t1092978558_marshaled_pinvoke;

extern "C" void IVRResources_t1092978558_marshal_pinvoke(const IVRResources_t1092978558& unmarshaled, IVRResources_t1092978558_marshaled_pinvoke& marshaled);
extern "C" void IVRResources_t1092978558_marshal_pinvoke_back(const IVRResources_t1092978558_marshaled_pinvoke& marshaled, IVRResources_t1092978558& unmarshaled);
extern "C" void IVRResources_t1092978558_marshal_pinvoke_cleanup(IVRResources_t1092978558_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IVRResources_t1092978558;
struct IVRResources_t1092978558_marshaled_com;

extern "C" void IVRResources_t1092978558_marshal_com(const IVRResources_t1092978558& unmarshaled, IVRResources_t1092978558_marshaled_com& marshaled);
extern "C" void IVRResources_t1092978558_marshal_com_back(const IVRResources_t1092978558_marshaled_com& marshaled, IVRResources_t1092978558& unmarshaled);
extern "C" void IVRResources_t1092978558_marshal_com_cleanup(IVRResources_t1092978558_marshaled_com& marshaled);
