﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShowSliderValue
struct ShowSliderValue_t1409884743;

#include "codegen/il2cpp-codegen.h"

// System.Void ShowSliderValue::.ctor()
extern "C"  void ShowSliderValue__ctor_m2566059824 (ShowSliderValue_t1409884743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowSliderValue::UpdateLabel(System.Single)
extern "C"  void ShowSliderValue_UpdateLabel_m2309436466 (ShowSliderValue_t1409884743 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
