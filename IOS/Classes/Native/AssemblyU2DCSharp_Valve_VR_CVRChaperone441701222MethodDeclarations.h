﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRChaperone
struct CVRChaperone_t441701222;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ChaperoneCalibrationState49870780.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdQuad_t2172573705.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdColor_t1780554589.h"

// System.Void Valve.VR.CVRChaperone::.ctor(System.IntPtr)
extern "C"  void CVRChaperone__ctor_m2910788459 (CVRChaperone_t441701222 * __this, IntPtr_t ___pInterface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.ChaperoneCalibrationState Valve.VR.CVRChaperone::GetCalibrationState()
extern "C"  int32_t CVRChaperone_GetCalibrationState_m2270786813 (CVRChaperone_t441701222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperone::GetPlayAreaSize(System.Single&,System.Single&)
extern "C"  bool CVRChaperone_GetPlayAreaSize_m1135040877 (CVRChaperone_t441701222 * __this, float* ___pSizeX0, float* ___pSizeZ1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperone::GetPlayAreaRect(Valve.VR.HmdQuad_t&)
extern "C"  bool CVRChaperone_GetPlayAreaRect_m1380605953 (CVRChaperone_t441701222 * __this, HmdQuad_t_t2172573705 * ___rect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRChaperone::ReloadInfo()
extern "C"  void CVRChaperone_ReloadInfo_m2856570428 (CVRChaperone_t441701222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRChaperone::SetSceneColor(Valve.VR.HmdColor_t)
extern "C"  void CVRChaperone_SetSceneColor_m531375751 (CVRChaperone_t441701222 * __this, HmdColor_t_t1780554589  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRChaperone::GetBoundsColor(Valve.VR.HmdColor_t&,System.Int32,System.Single,Valve.VR.HmdColor_t&)
extern "C"  void CVRChaperone_GetBoundsColor_m461264371 (CVRChaperone_t441701222 * __this, HmdColor_t_t1780554589 * ___pOutputColorArray0, int32_t ___nNumOutputColors1, float ___flCollisionBoundsFadeDistance2, HmdColor_t_t1780554589 * ___pOutputCameraColor3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperone::AreBoundsVisible()
extern "C"  bool CVRChaperone_AreBoundsVisible_m2858993676 (CVRChaperone_t441701222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRChaperone::ForceBoundsVisible(System.Boolean)
extern "C"  void CVRChaperone_ForceBoundsVisible_m3834414776 (CVRChaperone_t441701222 * __this, bool ___bForce0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
