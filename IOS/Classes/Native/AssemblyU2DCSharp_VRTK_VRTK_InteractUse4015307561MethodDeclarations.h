﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_InteractUse
struct VRTK_InteractUse_t4015307561;
// VRTK.ObjectInteractEventHandler
struct ObjectInteractEventHandler_t1701902511;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventHandler1701902511.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventArgs771291242.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"

// System.Void VRTK.VRTK_InteractUse::.ctor()
extern "C"  void VRTK_InteractUse__ctor_m386860939 (VRTK_InteractUse_t4015307561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::add_ControllerUseInteractableObject(VRTK.ObjectInteractEventHandler)
extern "C"  void VRTK_InteractUse_add_ControllerUseInteractableObject_m3391680585 (VRTK_InteractUse_t4015307561 * __this, ObjectInteractEventHandler_t1701902511 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::remove_ControllerUseInteractableObject(VRTK.ObjectInteractEventHandler)
extern "C"  void VRTK_InteractUse_remove_ControllerUseInteractableObject_m9745286 (VRTK_InteractUse_t4015307561 * __this, ObjectInteractEventHandler_t1701902511 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::add_ControllerUnuseInteractableObject(VRTK.ObjectInteractEventHandler)
extern "C"  void VRTK_InteractUse_add_ControllerUnuseInteractableObject_m1418480512 (VRTK_InteractUse_t4015307561 * __this, ObjectInteractEventHandler_t1701902511 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::remove_ControllerUnuseInteractableObject(VRTK.ObjectInteractEventHandler)
extern "C"  void VRTK_InteractUse_remove_ControllerUnuseInteractableObject_m2762529237 (VRTK_InteractUse_t4015307561 * __this, ObjectInteractEventHandler_t1701902511 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::OnControllerUseInteractableObject(VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_InteractUse_OnControllerUseInteractableObject_m3639181733 (VRTK_InteractUse_t4015307561 * __this, ObjectInteractEventArgs_t771291242  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::OnControllerUnuseInteractableObject(VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_InteractUse_OnControllerUnuseInteractableObject_m2363097714 (VRTK_InteractUse_t4015307561 * __this, ObjectInteractEventArgs_t771291242  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_InteractUse::GetUsingObject()
extern "C"  GameObject_t1756533147 * VRTK_InteractUse_GetUsingObject_m1010611021 (VRTK_InteractUse_t4015307561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::ForceStopUsing()
extern "C"  void VRTK_InteractUse_ForceStopUsing_m2870635680 (VRTK_InteractUse_t4015307561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::ForceResetUsing()
extern "C"  void VRTK_InteractUse_ForceResetUsing_m1901275485 (VRTK_InteractUse_t4015307561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::Awake()
extern "C"  void VRTK_InteractUse_Awake_m1909422994 (VRTK_InteractUse_t4015307561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::OnEnable()
extern "C"  void VRTK_InteractUse_OnEnable_m2381944407 (VRTK_InteractUse_t4015307561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::OnDisable()
extern "C"  void VRTK_InteractUse_OnDisable_m3867172142 (VRTK_InteractUse_t4015307561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractUse::IsObjectUsable(UnityEngine.GameObject)
extern "C"  bool VRTK_InteractUse_IsObjectUsable_m2898610150 (VRTK_InteractUse_t4015307561 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractUse::IsObjectHoldOnUse(UnityEngine.GameObject)
extern "C"  bool VRTK_InteractUse_IsObjectHoldOnUse_m1155559301 (VRTK_InteractUse_t4015307561 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VRTK.VRTK_InteractUse::GetObjectUsingState(UnityEngine.GameObject)
extern "C"  int32_t VRTK_InteractUse_GetObjectUsingState_m924639303 (VRTK_InteractUse_t4015307561 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::SetObjectUsingState(UnityEngine.GameObject,System.Int32)
extern "C"  void VRTK_InteractUse_SetObjectUsingState_m2178625328 (VRTK_InteractUse_t4015307561 * __this, GameObject_t1756533147 * ___obj0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::AttemptHaptics()
extern "C"  void VRTK_InteractUse_AttemptHaptics_m995055394 (VRTK_InteractUse_t4015307561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::ToggleControllerVisibility(System.Boolean)
extern "C"  void VRTK_InteractUse_ToggleControllerVisibility_m2093231188 (VRTK_InteractUse_t4015307561 * __this, bool ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::UseInteractedObject(UnityEngine.GameObject)
extern "C"  void VRTK_InteractUse_UseInteractedObject_m3826391904 (VRTK_InteractUse_t4015307561 * __this, GameObject_t1756533147 * ___touchedObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::UnuseInteractedObject(System.Boolean)
extern "C"  void VRTK_InteractUse_UnuseInteractedObject_m2728642554 (VRTK_InteractUse_t4015307561 * __this, bool ___completeStop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_InteractUse::GetFromGrab()
extern "C"  GameObject_t1756533147 * VRTK_InteractUse_GetFromGrab_m2589218262 (VRTK_InteractUse_t4015307561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::StopUsing()
extern "C"  void VRTK_InteractUse_StopUsing_m3289072569 (VRTK_InteractUse_t4015307561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::DoStartUseObject(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_InteractUse_DoStartUseObject_m3625878128 (VRTK_InteractUse_t4015307561 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractUse::DoStopUseObject(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_InteractUse_DoStopUseObject_m2749055890 (VRTK_InteractUse_t4015307561 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
