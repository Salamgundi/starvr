﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.InteractableExample
struct InteractableExample_t1462204982;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"

// System.Void Valve.VR.InteractionSystem.InteractableExample::.ctor()
extern "C"  void InteractableExample__ctor_m1683527144 (InteractableExample_t1462204982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InteractableExample::Awake()
extern "C"  void InteractableExample_Awake_m1054231227 (InteractableExample_t1462204982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InteractableExample::OnHandHoverBegin(Valve.VR.InteractionSystem.Hand)
extern "C"  void InteractableExample_OnHandHoverBegin_m999152175 (InteractableExample_t1462204982 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InteractableExample::OnHandHoverEnd(Valve.VR.InteractionSystem.Hand)
extern "C"  void InteractableExample_OnHandHoverEnd_m1700093759 (InteractableExample_t1462204982 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InteractableExample::HandHoverUpdate(Valve.VR.InteractionSystem.Hand)
extern "C"  void InteractableExample_HandHoverUpdate_m3334605332 (InteractableExample_t1462204982 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InteractableExample::OnAttachedToHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void InteractableExample_OnAttachedToHand_m3492127563 (InteractableExample_t1462204982 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InteractableExample::OnDetachedFromHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void InteractableExample_OnDetachedFromHand_m48511640 (InteractableExample_t1462204982 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InteractableExample::HandAttachedUpdate(Valve.VR.InteractionSystem.Hand)
extern "C"  void InteractableExample_HandAttachedUpdate_m3123470798 (InteractableExample_t1462204982 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InteractableExample::OnHandFocusAcquired(Valve.VR.InteractionSystem.Hand)
extern "C"  void InteractableExample_OnHandFocusAcquired_m2826328016 (InteractableExample_t1462204982 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InteractableExample::OnHandFocusLost(Valve.VR.InteractionSystem.Hand)
extern "C"  void InteractableExample_OnHandFocusLost_m4014123706 (InteractableExample_t1462204982 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
