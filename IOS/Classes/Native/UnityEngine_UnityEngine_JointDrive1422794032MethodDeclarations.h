﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointDrive1422794032.h"

// System.Void UnityEngine.JointDrive::set_positionSpring(System.Single)
extern "C"  void JointDrive_set_positionSpring_m2672093087 (JointDrive_t1422794032 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointDrive::set_positionDamper(System.Single)
extern "C"  void JointDrive_set_positionDamper_m2345556497 (JointDrive_t1422794032 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointDrive::set_maximumForce(System.Single)
extern "C"  void JointDrive_set_maximumForce_m2633971654 (JointDrive_t1422794032 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
