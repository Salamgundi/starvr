﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2372599686MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2638917094(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1431400739 *, Dictionary_2_t111376037 *, const MethodInfo*))Enumerator__ctor_m1222508305_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3627116073(__this, method) ((  Il2CppObject * (*) (Enumerator_t1431400739 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m588288164_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3911737705(__this, method) ((  void (*) (Enumerator_t1431400739 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3676260506_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1134046492(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1431400739 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1435760167_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m897627507(__this, method) ((  Il2CppObject * (*) (Enumerator_t1431400739 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2067661622_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1904142051(__this, method) ((  Il2CppObject * (*) (Enumerator_t1431400739 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1639305936_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::MoveNext()
#define Enumerator_MoveNext_m3539004505(__this, method) ((  bool (*) (Enumerator_t1431400739 *, const MethodInfo*))Enumerator_MoveNext_m291900446_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::get_Current()
#define Enumerator_get_Current_m1564610973(__this, method) ((  KeyValuePair_2_t2163688555  (*) (Enumerator_t1431400739 *, const MethodInfo*))Enumerator_get_Current_m2601154750_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2362882534(__this, method) ((  int32_t (*) (Enumerator_t1431400739 *, const MethodInfo*))Enumerator_get_CurrentKey_m1942024941_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m438628518(__this, method) ((  VRTK_SDKDetails_t1748250348 * (*) (Enumerator_t1431400739 *, const MethodInfo*))Enumerator_get_CurrentValue_m253268421_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::Reset()
#define Enumerator_Reset_m3764224420(__this, method) ((  void (*) (Enumerator_t1431400739 *, const MethodInfo*))Enumerator_Reset_m256676999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::VerifyState()
#define Enumerator_VerifyState_m3258112515(__this, method) ((  void (*) (Enumerator_t1431400739 *, const MethodInfo*))Enumerator_VerifyState_m2260481914_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2850093657(__this, method) ((  void (*) (Enumerator_t1431400739 *, const MethodInfo*))Enumerator_VerifyCurrent_m1279197290_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>::Dispose()
#define Enumerator_Dispose_m1086159214(__this, method) ((  void (*) (Enumerator_t1431400739 *, const MethodInfo*))Enumerator_Dispose_m1508571049_gshared)(__this, method)
