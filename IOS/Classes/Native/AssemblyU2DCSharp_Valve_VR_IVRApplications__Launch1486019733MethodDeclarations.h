﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_LaunchTemplateApplication
struct _LaunchTemplateApplication_t1486019733;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// Valve.VR.AppOverrideKeys_t[]
struct AppOverrideKeys_tU5BU5D_t3538561671;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_LaunchTemplateApplication::.ctor(System.Object,System.IntPtr)
extern "C"  void _LaunchTemplateApplication__ctor_m2817473476 (_LaunchTemplateApplication_t1486019733 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_LaunchTemplateApplication::Invoke(System.String,System.String,Valve.VR.AppOverrideKeys_t[],System.UInt32)
extern "C"  int32_t _LaunchTemplateApplication_Invoke_m3349756584 (_LaunchTemplateApplication_t1486019733 * __this, String_t* ___pchTemplateAppKey0, String_t* ___pchNewAppKey1, AppOverrideKeys_tU5BU5D_t3538561671* ___pKeys2, uint32_t ___unKeys3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_LaunchTemplateApplication::BeginInvoke(System.String,System.String,Valve.VR.AppOverrideKeys_t[],System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _LaunchTemplateApplication_BeginInvoke_m3866601823 (_LaunchTemplateApplication_t1486019733 * __this, String_t* ___pchTemplateAppKey0, String_t* ___pchNewAppKey1, AppOverrideKeys_tU5BU5D_t3538561671* ___pKeys2, uint32_t ___unKeys3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_LaunchTemplateApplication::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _LaunchTemplateApplication_EndInvoke_m3116768766 (_LaunchTemplateApplication_t1486019733 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
