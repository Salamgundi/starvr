﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_HeadsetFade
struct VRTK_HeadsetFade_t3539061086;
// VRTK.HeadsetFadeEventHandler
struct HeadsetFadeEventHandler_t2012619754;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetFadeEventHandler2012619754.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetFadeEventArgs2892542019.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void VRTK.VRTK_HeadsetFade::.ctor()
extern "C"  void VRTK_HeadsetFade__ctor_m163683682 (VRTK_HeadsetFade_t3539061086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::add_HeadsetFadeStart(VRTK.HeadsetFadeEventHandler)
extern "C"  void VRTK_HeadsetFade_add_HeadsetFadeStart_m1769941205 (VRTK_HeadsetFade_t3539061086 * __this, HeadsetFadeEventHandler_t2012619754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::remove_HeadsetFadeStart(VRTK.HeadsetFadeEventHandler)
extern "C"  void VRTK_HeadsetFade_remove_HeadsetFadeStart_m3421445150 (VRTK_HeadsetFade_t3539061086 * __this, HeadsetFadeEventHandler_t2012619754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::add_HeadsetFadeComplete(VRTK.HeadsetFadeEventHandler)
extern "C"  void VRTK_HeadsetFade_add_HeadsetFadeComplete_m4230922880 (VRTK_HeadsetFade_t3539061086 * __this, HeadsetFadeEventHandler_t2012619754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::remove_HeadsetFadeComplete(VRTK.HeadsetFadeEventHandler)
extern "C"  void VRTK_HeadsetFade_remove_HeadsetFadeComplete_m2772395079 (VRTK_HeadsetFade_t3539061086 * __this, HeadsetFadeEventHandler_t2012619754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::add_HeadsetUnfadeStart(VRTK.HeadsetFadeEventHandler)
extern "C"  void VRTK_HeadsetFade_add_HeadsetUnfadeStart_m2831016544 (VRTK_HeadsetFade_t3539061086 * __this, HeadsetFadeEventHandler_t2012619754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::remove_HeadsetUnfadeStart(VRTK.HeadsetFadeEventHandler)
extern "C"  void VRTK_HeadsetFade_remove_HeadsetUnfadeStart_m145772125 (VRTK_HeadsetFade_t3539061086 * __this, HeadsetFadeEventHandler_t2012619754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::add_HeadsetUnfadeComplete(VRTK.HeadsetFadeEventHandler)
extern "C"  void VRTK_HeadsetFade_add_HeadsetUnfadeComplete_m2722477275 (VRTK_HeadsetFade_t3539061086 * __this, HeadsetFadeEventHandler_t2012619754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::remove_HeadsetUnfadeComplete(VRTK.HeadsetFadeEventHandler)
extern "C"  void VRTK_HeadsetFade_remove_HeadsetUnfadeComplete_m1330363174 (VRTK_HeadsetFade_t3539061086 * __this, HeadsetFadeEventHandler_t2012619754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::OnHeadsetFadeStart(VRTK.HeadsetFadeEventArgs)
extern "C"  void VRTK_HeadsetFade_OnHeadsetFadeStart_m1137209017 (VRTK_HeadsetFade_t3539061086 * __this, HeadsetFadeEventArgs_t2892542019  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::OnHeadsetFadeComplete(VRTK.HeadsetFadeEventArgs)
extern "C"  void VRTK_HeadsetFade_OnHeadsetFadeComplete_m2414773996 (VRTK_HeadsetFade_t3539061086 * __this, HeadsetFadeEventArgs_t2892542019  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::OnHeadsetUnfadeStart(VRTK.HeadsetFadeEventArgs)
extern "C"  void VRTK_HeadsetFade_OnHeadsetUnfadeStart_m4003126498 (VRTK_HeadsetFade_t3539061086 * __this, HeadsetFadeEventArgs_t2892542019  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::OnHeadsetUnfadeComplete(VRTK.HeadsetFadeEventArgs)
extern "C"  void VRTK_HeadsetFade_OnHeadsetUnfadeComplete_m1163743603 (VRTK_HeadsetFade_t3539061086 * __this, HeadsetFadeEventArgs_t2892542019  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_HeadsetFade::IsFaded()
extern "C"  bool VRTK_HeadsetFade_IsFaded_m3039125750 (VRTK_HeadsetFade_t3539061086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_HeadsetFade::IsTransitioning()
extern "C"  bool VRTK_HeadsetFade_IsTransitioning_m743716987 (VRTK_HeadsetFade_t3539061086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::Fade(UnityEngine.Color,System.Single)
extern "C"  void VRTK_HeadsetFade_Fade_m4136994763 (VRTK_HeadsetFade_t3539061086 * __this, Color_t2020392075  ___color0, float ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::Unfade(System.Single)
extern "C"  void VRTK_HeadsetFade_Unfade_m49256858 (VRTK_HeadsetFade_t3539061086 * __this, float ___duration0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::Start()
extern "C"  void VRTK_HeadsetFade_Start_m3164890414 (VRTK_HeadsetFade_t3539061086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.HeadsetFadeEventArgs VRTK.VRTK_HeadsetFade::SetHeadsetFadeEvent(UnityEngine.Transform,System.Single)
extern "C"  HeadsetFadeEventArgs_t2892542019  VRTK_HeadsetFade_SetHeadsetFadeEvent_m2397418235 (VRTK_HeadsetFade_t3539061086 * __this, Transform_t3275118058 * ___currentTransform0, float ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::FadeComplete()
extern "C"  void VRTK_HeadsetFade_FadeComplete_m4091133561 (VRTK_HeadsetFade_t3539061086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetFade::UnfadeComplete()
extern "C"  void VRTK_HeadsetFade_UnfadeComplete_m823512498 (VRTK_HeadsetFade_t3539061086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
