﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Control3DEventHandler
struct Control3DEventHandler_t2392187186;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_Control3DEventArgs4095025701.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.Control3DEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void Control3DEventHandler__ctor_m3591568976 (Control3DEventHandler_t2392187186 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Control3DEventHandler::Invoke(System.Object,VRTK.Control3DEventArgs)
extern "C"  void Control3DEventHandler_Invoke_m3974302164 (Control3DEventHandler_t2392187186 * __this, Il2CppObject * ___sender0, Control3DEventArgs_t4095025701  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.Control3DEventHandler::BeginInvoke(System.Object,VRTK.Control3DEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Control3DEventHandler_BeginInvoke_m2157100449 (Control3DEventHandler_t2392187186 * __this, Il2CppObject * ___sender0, Control3DEventArgs_t4095025701  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Control3DEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void Control3DEventHandler_EndInvoke_m113318750 (Control3DEventHandler_t2392187186 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
