﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.PanelMenu.PanelMenuUIGrid
struct PanelMenuUIGrid_t729840129;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_Examples_PanelMenu_PanelMenu904549598.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_PanelMenuItemControllerEven2917504033.h"

// System.Void VRTK.Examples.PanelMenu.PanelMenuUIGrid::.ctor()
extern "C"  void PanelMenuUIGrid__ctor_m148595421 (PanelMenuUIGrid_t729840129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.PanelMenu.PanelMenuUIGrid::Start()
extern "C"  void PanelMenuUIGrid_Start_m3491143589 (PanelMenuUIGrid_t729840129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.Examples.PanelMenu.PanelMenuUIGrid::MoveSelectGridLayoutItem(VRTK.Examples.PanelMenu.PanelMenuUIGrid/Direction,UnityEngine.GameObject)
extern "C"  bool PanelMenuUIGrid_MoveSelectGridLayoutItem_m1139951691 (PanelMenuUIGrid_t729840129 * __this, int32_t ___direction0, GameObject_t1756533147 * ___interactableObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VRTK.Examples.PanelMenu.PanelMenuUIGrid::FindNextItemBasedOnMoveDirection(VRTK.Examples.PanelMenu.PanelMenuUIGrid/Direction)
extern "C"  int32_t PanelMenuUIGrid_FindNextItemBasedOnMoveDirection_m1572600748 (PanelMenuUIGrid_t729840129 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.PanelMenu.PanelMenuUIGrid::SetGridLayoutItemSelectedState(System.Int32)
extern "C"  void PanelMenuUIGrid_SetGridLayoutItemSelectedState_m2485743519 (PanelMenuUIGrid_t729840129 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.PanelMenu.PanelMenuUIGrid::OnPanelMenuItemSwipeTop(System.Object,VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuUIGrid_OnPanelMenuItemSwipeTop_m18612911 (PanelMenuUIGrid_t729840129 * __this, Il2CppObject * ___sender0, PanelMenuItemControllerEventArgs_t2917504033  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.PanelMenu.PanelMenuUIGrid::OnPanelMenuItemSwipeBottom(System.Object,VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuUIGrid_OnPanelMenuItemSwipeBottom_m3322368611 (PanelMenuUIGrid_t729840129 * __this, Il2CppObject * ___sender0, PanelMenuItemControllerEventArgs_t2917504033  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.PanelMenu.PanelMenuUIGrid::OnPanelMenuItemSwipeLeft(System.Object,VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuUIGrid_OnPanelMenuItemSwipeLeft_m1102343483 (PanelMenuUIGrid_t729840129 * __this, Il2CppObject * ___sender0, PanelMenuItemControllerEventArgs_t2917504033  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.PanelMenu.PanelMenuUIGrid::OnPanelMenuItemSwipeRight(System.Object,VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuUIGrid_OnPanelMenuItemSwipeRight_m2851617784 (PanelMenuUIGrid_t729840129 * __this, Il2CppObject * ___sender0, PanelMenuItemControllerEventArgs_t2917504033  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.PanelMenu.PanelMenuUIGrid::OnPanelMenuItemTriggerPressed(System.Object,VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuUIGrid_OnPanelMenuItemTriggerPressed_m1869367014 (PanelMenuUIGrid_t729840129 * __this, Il2CppObject * ___sender0, PanelMenuItemControllerEventArgs_t2917504033  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.PanelMenu.PanelMenuUIGrid::SendMessageToInteractableObject(UnityEngine.GameObject)
extern "C"  void PanelMenuUIGrid_SendMessageToInteractableObject_m3880608760 (PanelMenuUIGrid_t729840129 * __this, GameObject_t1756533147 * ___interactableObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
