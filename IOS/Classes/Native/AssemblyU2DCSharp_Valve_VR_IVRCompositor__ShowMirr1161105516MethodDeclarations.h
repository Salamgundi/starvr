﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_ShowMirrorWindow
struct _ShowMirrorWindow_t1161105516;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_ShowMirrorWindow::.ctor(System.Object,System.IntPtr)
extern "C"  void _ShowMirrorWindow__ctor_m1152914629 (_ShowMirrorWindow_t1161105516 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_ShowMirrorWindow::Invoke()
extern "C"  void _ShowMirrorWindow_Invoke_m3442389589 (_ShowMirrorWindow_t1161105516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_ShowMirrorWindow::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ShowMirrorWindow_BeginInvoke_m3857752162 (_ShowMirrorWindow_t1161105516 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_ShowMirrorWindow::EndInvoke(System.IAsyncResult)
extern "C"  void _ShowMirrorWindow_EndInvoke_m474664547 (_ShowMirrorWindow_t1161105516 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
