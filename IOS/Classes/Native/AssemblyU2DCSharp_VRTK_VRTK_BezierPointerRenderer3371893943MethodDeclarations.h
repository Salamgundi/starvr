﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_BezierPointerRenderer
struct VRTK_BezierPointerRenderer_t3371893943;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_BezierPointerRenderer::.ctor()
extern "C"  void VRTK_BezierPointerRenderer__ctor_m2772852171 (VRTK_BezierPointerRenderer_t3371893943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointerRenderer::UpdateRenderer()
extern "C"  void VRTK_BezierPointerRenderer_UpdateRenderer_m3287294211 (VRTK_BezierPointerRenderer_t3371893943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointerRenderer::ToggleRenderer(System.Boolean,System.Boolean)
extern "C"  void VRTK_BezierPointerRenderer_ToggleRenderer_m3510311536 (VRTK_BezierPointerRenderer_t3371893943 * __this, bool ___pointerState0, bool ___actualState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointerRenderer::CreatePointerObjects()
extern "C"  void VRTK_BezierPointerRenderer_CreatePointerObjects_m2889956772 (VRTK_BezierPointerRenderer_t3371893943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointerRenderer::DestroyPointerObjects()
extern "C"  void VRTK_BezierPointerRenderer_DestroyPointerObjects_m763566546 (VRTK_BezierPointerRenderer_t3371893943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointerRenderer::UpdateObjectInteractor()
extern "C"  void VRTK_BezierPointerRenderer_UpdateObjectInteractor_m3970084302 (VRTK_BezierPointerRenderer_t3371893943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointerRenderer::ChangeMaterial(UnityEngine.Color)
extern "C"  void VRTK_BezierPointerRenderer_ChangeMaterial_m3356208054 (VRTK_BezierPointerRenderer_t3371893943 * __this, Color_t2020392075  ___givenColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointerRenderer::CreateTracer()
extern "C"  void VRTK_BezierPointerRenderer_CreateTracer_m72300262 (VRTK_BezierPointerRenderer_t3371893943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_BezierPointerRenderer::CreateCursorObject()
extern "C"  GameObject_t1756533147 * VRTK_BezierPointerRenderer_CreateCursorObject_m4039555827 (VRTK_BezierPointerRenderer_t3371893943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointerRenderer::CreateCursorLocations()
extern "C"  void VRTK_BezierPointerRenderer_CreateCursorLocations_m3127510283 (VRTK_BezierPointerRenderer_t3371893943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointerRenderer::CreateCursor()
extern "C"  void VRTK_BezierPointerRenderer_CreateCursor_m549092225 (VRTK_BezierPointerRenderer_t3371893943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_BezierPointerRenderer::ProjectForwardBeam()
extern "C"  Vector3_t2243707580  VRTK_BezierPointerRenderer_ProjectForwardBeam_m2887760152 (VRTK_BezierPointerRenderer_t3371893943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_BezierPointerRenderer::ProjectDownBeam(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  VRTK_BezierPointerRenderer_ProjectDownBeam_m1287379872 (VRTK_BezierPointerRenderer_t3371893943 * __this, Vector3_t2243707580  ___jointPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointerRenderer::AdjustForEarlyCollisions(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void VRTK_BezierPointerRenderer_AdjustForEarlyCollisions_m3979410195 (VRTK_BezierPointerRenderer_t3371893943 * __this, Vector3_t2243707580  ___jointPosition0, Vector3_t2243707580  ___downPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointerRenderer::DisplayCurvedBeam(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void VRTK_BezierPointerRenderer_DisplayCurvedBeam_m2678095229 (VRTK_BezierPointerRenderer_t3371893943 * __this, Vector3_t2243707580  ___jointPosition0, Vector3_t2243707580  ___downPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointerRenderer::TogglePointerCursor(System.Boolean,System.Boolean)
extern "C"  void VRTK_BezierPointerRenderer_TogglePointerCursor_m2065432784 (VRTK_BezierPointerRenderer_t3371893943 * __this, bool ___pointerState0, bool ___actualState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointerRenderer::TogglePointerTracer(System.Boolean,System.Boolean)
extern "C"  void VRTK_BezierPointerRenderer_TogglePointerTracer_m81641633 (VRTK_BezierPointerRenderer_t3371893943 * __this, bool ___pointerState0, bool ___actualState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BezierPointerRenderer::SetPointerCursor()
extern "C"  void VRTK_BezierPointerRenderer_SetPointerCursor_m567328464 (VRTK_BezierPointerRenderer_t3371893943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
