﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t125053523;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1736152084;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Collections.Generic.List`1<VRTK.VRTK_VRInputModule>
struct List_1_t841621858;

#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_EventSystem
struct  VRTK_EventSystem_t3222336529  : public EventSystem_t3466835263
{
public:
	// System.Collections.Generic.List`1<VRTK.VRTK_VRInputModule> VRTK.VRTK_EventSystem::vrInputModules
	List_1_t841621858 * ___vrInputModules_17;

public:
	inline static int32_t get_offset_of_vrInputModules_17() { return static_cast<int32_t>(offsetof(VRTK_EventSystem_t3222336529, ___vrInputModules_17)); }
	inline List_1_t841621858 * get_vrInputModules_17() const { return ___vrInputModules_17; }
	inline List_1_t841621858 ** get_address_of_vrInputModules_17() { return &___vrInputModules_17; }
	inline void set_vrInputModules_17(List_1_t841621858 * value)
	{
		___vrInputModules_17 = value;
		Il2CppCodeGenWriteBarrier(&___vrInputModules_17, value);
	}
};

struct VRTK_EventSystem_t3222336529_StaticFields
{
public:
	// System.Reflection.FieldInfo[] VRTK.VRTK_EventSystem::EVENT_SYSTEM_FIELD_INFOS
	FieldInfoU5BU5D_t125053523* ___EVENT_SYSTEM_FIELD_INFOS_14;
	// System.Reflection.PropertyInfo[] VRTK.VRTK_EventSystem::EVENT_SYSTEM_PROPERTY_INFOS
	PropertyInfoU5BU5D_t1736152084* ___EVENT_SYSTEM_PROPERTY_INFOS_15;
	// System.Reflection.FieldInfo VRTK.VRTK_EventSystem::BASE_INPUT_MODULE_EVENT_SYSTEM_FIELD_INFO
	FieldInfo_t * ___BASE_INPUT_MODULE_EVENT_SYSTEM_FIELD_INFO_16;

public:
	inline static int32_t get_offset_of_EVENT_SYSTEM_FIELD_INFOS_14() { return static_cast<int32_t>(offsetof(VRTK_EventSystem_t3222336529_StaticFields, ___EVENT_SYSTEM_FIELD_INFOS_14)); }
	inline FieldInfoU5BU5D_t125053523* get_EVENT_SYSTEM_FIELD_INFOS_14() const { return ___EVENT_SYSTEM_FIELD_INFOS_14; }
	inline FieldInfoU5BU5D_t125053523** get_address_of_EVENT_SYSTEM_FIELD_INFOS_14() { return &___EVENT_SYSTEM_FIELD_INFOS_14; }
	inline void set_EVENT_SYSTEM_FIELD_INFOS_14(FieldInfoU5BU5D_t125053523* value)
	{
		___EVENT_SYSTEM_FIELD_INFOS_14 = value;
		Il2CppCodeGenWriteBarrier(&___EVENT_SYSTEM_FIELD_INFOS_14, value);
	}

	inline static int32_t get_offset_of_EVENT_SYSTEM_PROPERTY_INFOS_15() { return static_cast<int32_t>(offsetof(VRTK_EventSystem_t3222336529_StaticFields, ___EVENT_SYSTEM_PROPERTY_INFOS_15)); }
	inline PropertyInfoU5BU5D_t1736152084* get_EVENT_SYSTEM_PROPERTY_INFOS_15() const { return ___EVENT_SYSTEM_PROPERTY_INFOS_15; }
	inline PropertyInfoU5BU5D_t1736152084** get_address_of_EVENT_SYSTEM_PROPERTY_INFOS_15() { return &___EVENT_SYSTEM_PROPERTY_INFOS_15; }
	inline void set_EVENT_SYSTEM_PROPERTY_INFOS_15(PropertyInfoU5BU5D_t1736152084* value)
	{
		___EVENT_SYSTEM_PROPERTY_INFOS_15 = value;
		Il2CppCodeGenWriteBarrier(&___EVENT_SYSTEM_PROPERTY_INFOS_15, value);
	}

	inline static int32_t get_offset_of_BASE_INPUT_MODULE_EVENT_SYSTEM_FIELD_INFO_16() { return static_cast<int32_t>(offsetof(VRTK_EventSystem_t3222336529_StaticFields, ___BASE_INPUT_MODULE_EVENT_SYSTEM_FIELD_INFO_16)); }
	inline FieldInfo_t * get_BASE_INPUT_MODULE_EVENT_SYSTEM_FIELD_INFO_16() const { return ___BASE_INPUT_MODULE_EVENT_SYSTEM_FIELD_INFO_16; }
	inline FieldInfo_t ** get_address_of_BASE_INPUT_MODULE_EVENT_SYSTEM_FIELD_INFO_16() { return &___BASE_INPUT_MODULE_EVENT_SYSTEM_FIELD_INFO_16; }
	inline void set_BASE_INPUT_MODULE_EVENT_SYSTEM_FIELD_INFO_16(FieldInfo_t * value)
	{
		___BASE_INPUT_MODULE_EVENT_SYSTEM_FIELD_INFO_16 = value;
		Il2CppCodeGenWriteBarrier(&___BASE_INPUT_MODULE_EVENT_SYSTEM_FIELD_INFO_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
