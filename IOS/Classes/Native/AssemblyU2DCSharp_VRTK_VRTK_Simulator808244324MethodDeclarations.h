﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_Simulator
struct VRTK_Simulator_t808244324;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_Simulator::.ctor()
extern "C"  void VRTK_Simulator__ctor_m729099940 (VRTK_Simulator_t808244324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Simulator::Start()
extern "C"  void VRTK_Simulator_Start_m2375810292 (VRTK_Simulator_t808244324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Simulator::Update()
extern "C"  void VRTK_Simulator_Update_m3635104593 (VRTK_Simulator_t808244324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_Simulator::overwriteY(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  VRTK_Simulator_overwriteY_m1401231282 (VRTK_Simulator_t808244324 * __this, Vector3_t2243707580  ___vector0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
