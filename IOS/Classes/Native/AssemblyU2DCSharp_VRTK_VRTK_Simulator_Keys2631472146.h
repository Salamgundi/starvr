﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Simulator/Keys
struct  Keys_t2631472146  : public Il2CppObject
{
public:
	// UnityEngine.KeyCode VRTK.VRTK_Simulator/Keys::forward
	int32_t ___forward_0;
	// UnityEngine.KeyCode VRTK.VRTK_Simulator/Keys::backward
	int32_t ___backward_1;
	// UnityEngine.KeyCode VRTK.VRTK_Simulator/Keys::strafeLeft
	int32_t ___strafeLeft_2;
	// UnityEngine.KeyCode VRTK.VRTK_Simulator/Keys::strafeRight
	int32_t ___strafeRight_3;
	// UnityEngine.KeyCode VRTK.VRTK_Simulator/Keys::left
	int32_t ___left_4;
	// UnityEngine.KeyCode VRTK.VRTK_Simulator/Keys::right
	int32_t ___right_5;
	// UnityEngine.KeyCode VRTK.VRTK_Simulator/Keys::up
	int32_t ___up_6;
	// UnityEngine.KeyCode VRTK.VRTK_Simulator/Keys::down
	int32_t ___down_7;
	// UnityEngine.KeyCode VRTK.VRTK_Simulator/Keys::reset
	int32_t ___reset_8;

public:
	inline static int32_t get_offset_of_forward_0() { return static_cast<int32_t>(offsetof(Keys_t2631472146, ___forward_0)); }
	inline int32_t get_forward_0() const { return ___forward_0; }
	inline int32_t* get_address_of_forward_0() { return &___forward_0; }
	inline void set_forward_0(int32_t value)
	{
		___forward_0 = value;
	}

	inline static int32_t get_offset_of_backward_1() { return static_cast<int32_t>(offsetof(Keys_t2631472146, ___backward_1)); }
	inline int32_t get_backward_1() const { return ___backward_1; }
	inline int32_t* get_address_of_backward_1() { return &___backward_1; }
	inline void set_backward_1(int32_t value)
	{
		___backward_1 = value;
	}

	inline static int32_t get_offset_of_strafeLeft_2() { return static_cast<int32_t>(offsetof(Keys_t2631472146, ___strafeLeft_2)); }
	inline int32_t get_strafeLeft_2() const { return ___strafeLeft_2; }
	inline int32_t* get_address_of_strafeLeft_2() { return &___strafeLeft_2; }
	inline void set_strafeLeft_2(int32_t value)
	{
		___strafeLeft_2 = value;
	}

	inline static int32_t get_offset_of_strafeRight_3() { return static_cast<int32_t>(offsetof(Keys_t2631472146, ___strafeRight_3)); }
	inline int32_t get_strafeRight_3() const { return ___strafeRight_3; }
	inline int32_t* get_address_of_strafeRight_3() { return &___strafeRight_3; }
	inline void set_strafeRight_3(int32_t value)
	{
		___strafeRight_3 = value;
	}

	inline static int32_t get_offset_of_left_4() { return static_cast<int32_t>(offsetof(Keys_t2631472146, ___left_4)); }
	inline int32_t get_left_4() const { return ___left_4; }
	inline int32_t* get_address_of_left_4() { return &___left_4; }
	inline void set_left_4(int32_t value)
	{
		___left_4 = value;
	}

	inline static int32_t get_offset_of_right_5() { return static_cast<int32_t>(offsetof(Keys_t2631472146, ___right_5)); }
	inline int32_t get_right_5() const { return ___right_5; }
	inline int32_t* get_address_of_right_5() { return &___right_5; }
	inline void set_right_5(int32_t value)
	{
		___right_5 = value;
	}

	inline static int32_t get_offset_of_up_6() { return static_cast<int32_t>(offsetof(Keys_t2631472146, ___up_6)); }
	inline int32_t get_up_6() const { return ___up_6; }
	inline int32_t* get_address_of_up_6() { return &___up_6; }
	inline void set_up_6(int32_t value)
	{
		___up_6 = value;
	}

	inline static int32_t get_offset_of_down_7() { return static_cast<int32_t>(offsetof(Keys_t2631472146, ___down_7)); }
	inline int32_t get_down_7() const { return ___down_7; }
	inline int32_t* get_address_of_down_7() { return &___down_7; }
	inline void set_down_7(int32_t value)
	{
		___down_7 = value;
	}

	inline static int32_t get_offset_of_reset_8() { return static_cast<int32_t>(offsetof(Keys_t2631472146, ___reset_8)); }
	inline int32_t get_reset_8() const { return ___reset_8; }
	inline int32_t* get_address_of_reset_8() { return &___reset_8; }
	inline void set_reset_8(int32_t value)
	{
		___reset_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
