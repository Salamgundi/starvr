﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>
struct Dictionary_2_t1052574984;
// System.Collections.Generic.IEqualityComparer`1<VRTK.VRTK_SDKManager/SupportedSDKs>
struct IEqualityComparer_1_t551769460;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>[]
struct KeyValuePair_2U5BU5D_t1542894139;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>>
struct IEnumerator_1_t580411329;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>
struct ValueCollection_t4050602123;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23104887502.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SDKManager_SupportedSD1339136682.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2372599686.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m326069777_gshared (Dictionary_2_t1052574984 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m326069777(__this, method) ((  void (*) (Dictionary_2_t1052574984 *, const MethodInfo*))Dictionary_2__ctor_m326069777_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1911883620_gshared (Dictionary_2_t1052574984 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1911883620(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1052574984 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1911883620_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m1530253440_gshared (Dictionary_2_t1052574984 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m1530253440(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1052574984 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1530253440_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2786693530_gshared (Dictionary_2_t1052574984 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2786693530(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1052574984 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m2786693530_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m152097445_gshared (Dictionary_2_t1052574984 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m152097445(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1052574984 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m152097445_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m169412066_gshared (Dictionary_2_t1052574984 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m169412066(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1052574984 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m169412066_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m2828714983_gshared (Dictionary_2_t1052574984 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m2828714983(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1052574984 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2828714983_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3724209814_gshared (Dictionary_2_t1052574984 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3724209814(__this, ___key0, method) ((  void (*) (Dictionary_2_t1052574984 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3724209814_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m5758805_gshared (Dictionary_2_t1052574984 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m5758805(__this, method) ((  bool (*) (Dictionary_2_t1052574984 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m5758805_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1884642069_gshared (Dictionary_2_t1052574984 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1884642069(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1052574984 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1884642069_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3788185739_gshared (Dictionary_2_t1052574984 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3788185739(__this, method) ((  bool (*) (Dictionary_2_t1052574984 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3788185739_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m836871452_gshared (Dictionary_2_t1052574984 * __this, KeyValuePair_2_t3104887502  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m836871452(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1052574984 *, KeyValuePair_2_t3104887502 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m836871452_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m326045424_gshared (Dictionary_2_t1052574984 * __this, KeyValuePair_2_t3104887502  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m326045424(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1052574984 *, KeyValuePair_2_t3104887502 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m326045424_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1107558696_gshared (Dictionary_2_t1052574984 * __this, KeyValuePair_2U5BU5D_t1542894139* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1107558696(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1052574984 *, KeyValuePair_2U5BU5D_t1542894139*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1107558696_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2460220329_gshared (Dictionary_2_t1052574984 * __this, KeyValuePair_2_t3104887502  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2460220329(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1052574984 *, KeyValuePair_2_t3104887502 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2460220329_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3775026809_gshared (Dictionary_2_t1052574984 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3775026809(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1052574984 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3775026809_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3050731018_gshared (Dictionary_2_t1052574984 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3050731018(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1052574984 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3050731018_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1540091879_gshared (Dictionary_2_t1052574984 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1540091879(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1052574984 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1540091879_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3737454332_gshared (Dictionary_2_t1052574984 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3737454332(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1052574984 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3737454332_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1003244241_gshared (Dictionary_2_t1052574984 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1003244241(__this, method) ((  int32_t (*) (Dictionary_2_t1052574984 *, const MethodInfo*))Dictionary_2_get_Count_m1003244241_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m1748638860_gshared (Dictionary_2_t1052574984 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1748638860(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1052574984 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m1748638860_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1525396769_gshared (Dictionary_2_t1052574984 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1525396769(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1052574984 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m1525396769_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m4229651869_gshared (Dictionary_2_t1052574984 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m4229651869(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1052574984 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m4229651869_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2646461128_gshared (Dictionary_2_t1052574984 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2646461128(__this, ___size0, method) ((  void (*) (Dictionary_2_t1052574984 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2646461128_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2615223790_gshared (Dictionary_2_t1052574984 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2615223790(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1052574984 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2615223790_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3104887502  Dictionary_2_make_pair_m3772995368_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3772995368(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3104887502  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m3772995368_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m4001078414_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m4001078414(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m4001078414_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m4142937837_gshared (Dictionary_2_t1052574984 * __this, KeyValuePair_2U5BU5D_t1542894139* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m4142937837(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1052574984 *, KeyValuePair_2U5BU5D_t1542894139*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m4142937837_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m2782306351_gshared (Dictionary_2_t1052574984 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m2782306351(__this, method) ((  void (*) (Dictionary_2_t1052574984 *, const MethodInfo*))Dictionary_2_Resize_m2782306351_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3737545260_gshared (Dictionary_2_t1052574984 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3737545260(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1052574984 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m3737545260_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m3070616856_gshared (Dictionary_2_t1052574984 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3070616856(__this, method) ((  void (*) (Dictionary_2_t1052574984 *, const MethodInfo*))Dictionary_2_Clear_m3070616856_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m4207481556_gshared (Dictionary_2_t1052574984 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m4207481556(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1052574984 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m4207481556_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1664082148_gshared (Dictionary_2_t1052574984 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1664082148(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1052574984 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m1664082148_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3362939573_gshared (Dictionary_2_t1052574984 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3362939573(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1052574984 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m3362939573_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2373203649_gshared (Dictionary_2_t1052574984 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2373203649(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1052574984 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2373203649_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2674595432_gshared (Dictionary_2_t1052574984 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2674595432(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1052574984 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2674595432_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2815582913_gshared (Dictionary_2_t1052574984 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2815582913(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1052574984 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m2815582913_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::get_Values()
extern "C"  ValueCollection_t4050602123 * Dictionary_2_get_Values_m614780488_gshared (Dictionary_2_t1052574984 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m614780488(__this, method) ((  ValueCollection_t4050602123 * (*) (Dictionary_2_t1052574984 *, const MethodInfo*))Dictionary_2_get_Values_m614780488_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m2054370375_gshared (Dictionary_2_t1052574984 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2054370375(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1052574984 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2054370375_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m2634499175_gshared (Dictionary_2_t1052574984 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m2634499175(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t1052574984 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m2634499175_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m3101397241_gshared (Dictionary_2_t1052574984 * __this, KeyValuePair_2_t3104887502  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m3101397241(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1052574984 *, KeyValuePair_2_t3104887502 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3101397241_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2372599686  Dictionary_2_GetEnumerator_m3272550974_gshared (Dictionary_2_t1052574984 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m3272550974(__this, method) ((  Enumerator_t2372599686  (*) (Dictionary_2_t1052574984 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3272550974_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m4105384275_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m4105384275(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m4105384275_gshared)(__this /* static, unused */, ___key0, ___value1, method)
