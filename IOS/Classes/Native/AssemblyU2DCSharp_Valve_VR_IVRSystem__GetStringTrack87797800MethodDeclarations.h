﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetStringTrackedDeviceProperty
struct _GetStringTrackedDeviceProperty_t87797800;
// System.Object
struct Il2CppObject;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedDeviceProperty3226377054.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedPropertyError3340022390.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetStringTrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetStringTrackedDeviceProperty__ctor_m500825725 (_GetStringTrackedDeviceProperty_t87797800 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRSystem/_GetStringTrackedDeviceProperty::Invoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,System.Text.StringBuilder,System.UInt32,Valve.VR.ETrackedPropertyError&)
extern "C"  uint32_t _GetStringTrackedDeviceProperty_Invoke_m2122365806 (_GetStringTrackedDeviceProperty_t87797800 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unBufferSize3, int32_t* ___pError4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetStringTrackedDeviceProperty::BeginInvoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,System.Text.StringBuilder,System.UInt32,Valve.VR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetStringTrackedDeviceProperty_BeginInvoke_m3045677472 (_GetStringTrackedDeviceProperty_t87797800 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unBufferSize3, int32_t* ___pError4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRSystem/_GetStringTrackedDeviceProperty::EndInvoke(Valve.VR.ETrackedPropertyError&,System.IAsyncResult)
extern "C"  uint32_t _GetStringTrackedDeviceProperty_EndInvoke_m1782671678 (_GetStringTrackedDeviceProperty_t87797800 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
