﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0
struct U3CFallDownU3Ec__Iterator0_t3537536025;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0::.ctor()
extern "C"  void U3CFallDownU3Ec__Iterator0__ctor_m3649768356 (U3CFallDownU3Ec__Iterator0_t3537536025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0::MoveNext()
extern "C"  bool U3CFallDownU3Ec__Iterator0_MoveNext_m1805570492 (U3CFallDownU3Ec__Iterator0_t3537536025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFallDownU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1059950826 (U3CFallDownU3Ec__Iterator0_t3537536025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFallDownU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m773835346 (U3CFallDownU3Ec__Iterator0_t3537536025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0::Dispose()
extern "C"  void U3CFallDownU3Ec__Iterator0_Dispose_m538236427 (U3CFallDownU3Ec__Iterator0_t3537536025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArcheryTarget/<FallDown>c__Iterator0::Reset()
extern "C"  void U3CFallDownU3Ec__Iterator0_Reset_m1798514201 (U3CFallDownU3Ec__Iterator0_t3537536025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
