﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRRenderModels/_GetComponentStatePacked
struct _GetComponentStatePacked_t3386937929;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_VRControllerState_t_Pac1296713633.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_ControllerM1298199406.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_ComponentSt2032012879.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.CVRRenderModels/_GetComponentStatePacked::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetComponentStatePacked__ctor_m3924764044 (_GetComponentStatePacked_t3386937929 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRRenderModels/_GetComponentStatePacked::Invoke(System.String,System.String,Valve.VR.VRControllerState_t_Packed&,Valve.VR.RenderModel_ControllerMode_State_t&,Valve.VR.RenderModel_ComponentState_t&)
extern "C"  bool _GetComponentStatePacked_Invoke_m4229072686 (_GetComponentStatePacked_t3386937929 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, VRControllerState_t_Packed_t1296713633 * ___pControllerState2, RenderModel_ControllerMode_State_t_t1298199406 * ___pState3, RenderModel_ComponentState_t_t2032012879 * ___pComponentState4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.CVRRenderModels/_GetComponentStatePacked::BeginInvoke(System.String,System.String,Valve.VR.VRControllerState_t_Packed&,Valve.VR.RenderModel_ControllerMode_State_t&,Valve.VR.RenderModel_ComponentState_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetComponentStatePacked_BeginInvoke_m4263898909 (_GetComponentStatePacked_t3386937929 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, VRControllerState_t_Packed_t1296713633 * ___pControllerState2, RenderModel_ControllerMode_State_t_t1298199406 * ___pState3, RenderModel_ComponentState_t_t2032012879 * ___pComponentState4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRRenderModels/_GetComponentStatePacked::EndInvoke(Valve.VR.VRControllerState_t_Packed&,Valve.VR.RenderModel_ControllerMode_State_t&,Valve.VR.RenderModel_ComponentState_t&,System.IAsyncResult)
extern "C"  bool _GetComponentStatePacked_EndInvoke_m1068425510 (_GetComponentStatePacked_t3386937929 * __this, VRControllerState_t_Packed_t1296713633 * ___pControllerState0, RenderModel_ControllerMode_State_t_t1298199406 * ___pState1, RenderModel_ComponentState_t_t2032012879 * ___pComponentState2, Il2CppObject * ___result3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
