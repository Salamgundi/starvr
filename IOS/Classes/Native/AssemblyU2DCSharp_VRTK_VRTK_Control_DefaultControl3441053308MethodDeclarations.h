﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_Control/DefaultControlEvents
struct DefaultControlEvents_t3441053308;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_Control/DefaultControlEvents::.ctor()
extern "C"  void DefaultControlEvents__ctor_m153664345 (DefaultControlEvents_t3441053308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
