﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_HideMirrorWindow
struct _HideMirrorWindow_t396471547;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_HideMirrorWindow::.ctor(System.Object,System.IntPtr)
extern "C"  void _HideMirrorWindow__ctor_m3247851552 (_HideMirrorWindow_t396471547 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_HideMirrorWindow::Invoke()
extern "C"  void _HideMirrorWindow_Invoke_m1582520474 (_HideMirrorWindow_t396471547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_HideMirrorWindow::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _HideMirrorWindow_BeginInvoke_m240126205 (_HideMirrorWindow_t396471547 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_HideMirrorWindow::EndInvoke(System.IAsyncResult)
extern "C"  void _HideMirrorWindow_EndInvoke_m4290824446 (_HideMirrorWindow_t396471547 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
