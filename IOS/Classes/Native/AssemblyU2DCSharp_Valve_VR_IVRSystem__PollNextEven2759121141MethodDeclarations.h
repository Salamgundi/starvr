﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_PollNextEventWithPose
struct _PollNextEventWithPose_t2759121141;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingUniverseOrigin1464400093.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_t3405266389.h"
#include "AssemblyU2DCSharp_Valve_VR_TrackedDevicePose_t1668551120.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_PollNextEventWithPose::.ctor(System.Object,System.IntPtr)
extern "C"  void _PollNextEventWithPose__ctor_m2657259538 (_PollNextEventWithPose_t2759121141 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_PollNextEventWithPose::Invoke(Valve.VR.ETrackingUniverseOrigin,Valve.VR.VREvent_t&,System.UInt32,Valve.VR.TrackedDevicePose_t&)
extern "C"  bool _PollNextEventWithPose_Invoke_m1823136172 (_PollNextEventWithPose_t2759121141 * __this, int32_t ___eOrigin0, VREvent_t_t3405266389 * ___pEvent1, uint32_t ___uncbVREvent2, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_PollNextEventWithPose::BeginInvoke(Valve.VR.ETrackingUniverseOrigin,Valve.VR.VREvent_t&,System.UInt32,Valve.VR.TrackedDevicePose_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _PollNextEventWithPose_BeginInvoke_m1821337751 (_PollNextEventWithPose_t2759121141 * __this, int32_t ___eOrigin0, VREvent_t_t3405266389 * ___pEvent1, uint32_t ___uncbVREvent2, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_PollNextEventWithPose::EndInvoke(Valve.VR.VREvent_t&,Valve.VR.TrackedDevicePose_t&,System.IAsyncResult)
extern "C"  bool _PollNextEventWithPose_EndInvoke_m4167318909 (_PollNextEventWithPose_t2759121141 * __this, VREvent_t_t3405266389 * ___pEvent0, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
