﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter
struct VRTK_MaterialColorSwapHighlighter_t2305160438;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Material
struct Material_t193706927;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen283458390.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Material193706927.h"

// System.Void VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter::.ctor()
extern "C"  void VRTK_MaterialColorSwapHighlighter__ctor_m538557390 (VRTK_MaterialColorSwapHighlighter_t2305160438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter::Initialise(System.Nullable`1<UnityEngine.Color>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void VRTK_MaterialColorSwapHighlighter_Initialise_m2766811989 (VRTK_MaterialColorSwapHighlighter_t2305160438 * __this, Nullable_1_t283458390  ___color0, Dictionary_2_t309261261 * ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter::ResetHighlighter()
extern "C"  void VRTK_MaterialColorSwapHighlighter_ResetHighlighter_m3030992222 (VRTK_MaterialColorSwapHighlighter_t2305160438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter::Highlight(System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_MaterialColorSwapHighlighter_Highlight_m2307047666 (VRTK_MaterialColorSwapHighlighter_t2305160438 * __this, Nullable_1_t283458390  ___color0, float ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter::Unhighlight(System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_MaterialColorSwapHighlighter_Unhighlight_m2746103241 (VRTK_MaterialColorSwapHighlighter_t2305160438 * __this, Nullable_1_t283458390  ___color0, float ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter::StoreOriginalMaterials()
extern "C"  void VRTK_MaterialColorSwapHighlighter_StoreOriginalMaterials_m3723250968 (VRTK_MaterialColorSwapHighlighter_t2305160438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter::ChangeToHighlightColor(UnityEngine.Color,System.Single)
extern "C"  void VRTK_MaterialColorSwapHighlighter_ChangeToHighlightColor_m4212878655 (VRTK_MaterialColorSwapHighlighter_t2305160438 * __this, Color_t2020392075  ___color0, float ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter::CycleColor(UnityEngine.Material,UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  Il2CppObject * VRTK_MaterialColorSwapHighlighter_CycleColor_m3117656768 (VRTK_MaterialColorSwapHighlighter_t2305160438 * __this, Material_t193706927 * ___material0, Color_t2020392075  ___startColor1, Color_t2020392075  ___endColor2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
