﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_PolicyList
struct VRTK_PolicyList_t2965133344;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PolicyList2965133344.h"

// System.Void VRTK.VRTK_PolicyList::.ctor()
extern "C"  void VRTK_PolicyList__ctor_m3535216790 (VRTK_PolicyList_t2965133344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_PolicyList::Find(UnityEngine.GameObject)
extern "C"  bool VRTK_PolicyList_Find_m2945535965 (VRTK_PolicyList_t2965133344 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_PolicyList::Check(UnityEngine.GameObject,VRTK.VRTK_PolicyList)
extern "C"  bool VRTK_PolicyList_Check_m3179134241 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___obj0, VRTK_PolicyList_t2965133344 * ___list1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_PolicyList::ScriptCheck(UnityEngine.GameObject,System.Boolean)
extern "C"  bool VRTK_PolicyList_ScriptCheck_m365379374 (VRTK_PolicyList_t2965133344 * __this, GameObject_t1756533147 * ___obj0, bool ___returnState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_PolicyList::TagCheck(UnityEngine.GameObject,System.Boolean)
extern "C"  bool VRTK_PolicyList_TagCheck_m1013796235 (VRTK_PolicyList_t2965133344 * __this, GameObject_t1756533147 * ___obj0, bool ___returnState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_PolicyList::LayerCheck(UnityEngine.GameObject,System.Boolean)
extern "C"  bool VRTK_PolicyList_LayerCheck_m1351108722 (VRTK_PolicyList_t2965133344 * __this, GameObject_t1756533147 * ___obj0, bool ___returnState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_PolicyList::TypeCheck(UnityEngine.GameObject,System.Boolean)
extern "C"  bool VRTK_PolicyList_TypeCheck_m1381252853 (VRTK_PolicyList_t2965133344 * __this, GameObject_t1756533147 * ___obj0, bool ___returnState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
