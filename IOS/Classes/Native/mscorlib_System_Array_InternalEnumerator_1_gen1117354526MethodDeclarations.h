﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1117354526.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_258602264.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3741623327_gshared (InternalEnumerator_1_t1117354526 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3741623327(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1117354526 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3741623327_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224452439_gshared (InternalEnumerator_1_t1117354526 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224452439(__this, method) ((  void (*) (InternalEnumerator_1_t1117354526 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224452439_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3915226427_gshared (InternalEnumerator_1_t1117354526 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3915226427(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1117354526 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3915226427_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4109208472_gshared (InternalEnumerator_1_t1117354526 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4109208472(__this, method) ((  void (*) (InternalEnumerator_1_t1117354526 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4109208472_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3428235539_gshared (InternalEnumerator_1_t1117354526 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3428235539(__this, method) ((  bool (*) (InternalEnumerator_1_t1117354526 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3428235539_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::get_Current()
extern "C"  KeyValuePair_2_t258602264  InternalEnumerator_1_get_Current_m2268465014_gshared (InternalEnumerator_1_t1117354526 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2268465014(__this, method) ((  KeyValuePair_2_t258602264  (*) (InternalEnumerator_1_t1117354526 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2268465014_gshared)(__this, method)
