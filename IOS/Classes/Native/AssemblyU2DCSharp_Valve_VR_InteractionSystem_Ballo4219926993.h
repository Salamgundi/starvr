﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// Valve.VR.InteractionSystem.SoundPlayOneshot
struct SoundPlayOneshot_t1703214483;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Ballo1350152013.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.BalloonSpawner
struct  BalloonSpawner_t4219926993  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Valve.VR.InteractionSystem.BalloonSpawner::minSpawnTime
	float ___minSpawnTime_2;
	// System.Single Valve.VR.InteractionSystem.BalloonSpawner::maxSpawnTime
	float ___maxSpawnTime_3;
	// System.Single Valve.VR.InteractionSystem.BalloonSpawner::nextSpawnTime
	float ___nextSpawnTime_4;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.BalloonSpawner::balloonPrefab
	GameObject_t1756533147 * ___balloonPrefab_5;
	// System.Boolean Valve.VR.InteractionSystem.BalloonSpawner::autoSpawn
	bool ___autoSpawn_6;
	// System.Boolean Valve.VR.InteractionSystem.BalloonSpawner::spawnAtStartup
	bool ___spawnAtStartup_7;
	// System.Boolean Valve.VR.InteractionSystem.BalloonSpawner::playSounds
	bool ___playSounds_8;
	// Valve.VR.InteractionSystem.SoundPlayOneshot Valve.VR.InteractionSystem.BalloonSpawner::inflateSound
	SoundPlayOneshot_t1703214483 * ___inflateSound_9;
	// Valve.VR.InteractionSystem.SoundPlayOneshot Valve.VR.InteractionSystem.BalloonSpawner::stretchSound
	SoundPlayOneshot_t1703214483 * ___stretchSound_10;
	// System.Boolean Valve.VR.InteractionSystem.BalloonSpawner::sendSpawnMessageToParent
	bool ___sendSpawnMessageToParent_11;
	// System.Single Valve.VR.InteractionSystem.BalloonSpawner::scale
	float ___scale_12;
	// UnityEngine.Transform Valve.VR.InteractionSystem.BalloonSpawner::spawnDirectionTransform
	Transform_t3275118058 * ___spawnDirectionTransform_13;
	// System.Single Valve.VR.InteractionSystem.BalloonSpawner::spawnForce
	float ___spawnForce_14;
	// System.Boolean Valve.VR.InteractionSystem.BalloonSpawner::attachBalloon
	bool ___attachBalloon_15;
	// Valve.VR.InteractionSystem.Balloon/BalloonColor Valve.VR.InteractionSystem.BalloonSpawner::color
	int32_t ___color_16;

public:
	inline static int32_t get_offset_of_minSpawnTime_2() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___minSpawnTime_2)); }
	inline float get_minSpawnTime_2() const { return ___minSpawnTime_2; }
	inline float* get_address_of_minSpawnTime_2() { return &___minSpawnTime_2; }
	inline void set_minSpawnTime_2(float value)
	{
		___minSpawnTime_2 = value;
	}

	inline static int32_t get_offset_of_maxSpawnTime_3() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___maxSpawnTime_3)); }
	inline float get_maxSpawnTime_3() const { return ___maxSpawnTime_3; }
	inline float* get_address_of_maxSpawnTime_3() { return &___maxSpawnTime_3; }
	inline void set_maxSpawnTime_3(float value)
	{
		___maxSpawnTime_3 = value;
	}

	inline static int32_t get_offset_of_nextSpawnTime_4() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___nextSpawnTime_4)); }
	inline float get_nextSpawnTime_4() const { return ___nextSpawnTime_4; }
	inline float* get_address_of_nextSpawnTime_4() { return &___nextSpawnTime_4; }
	inline void set_nextSpawnTime_4(float value)
	{
		___nextSpawnTime_4 = value;
	}

	inline static int32_t get_offset_of_balloonPrefab_5() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___balloonPrefab_5)); }
	inline GameObject_t1756533147 * get_balloonPrefab_5() const { return ___balloonPrefab_5; }
	inline GameObject_t1756533147 ** get_address_of_balloonPrefab_5() { return &___balloonPrefab_5; }
	inline void set_balloonPrefab_5(GameObject_t1756533147 * value)
	{
		___balloonPrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___balloonPrefab_5, value);
	}

	inline static int32_t get_offset_of_autoSpawn_6() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___autoSpawn_6)); }
	inline bool get_autoSpawn_6() const { return ___autoSpawn_6; }
	inline bool* get_address_of_autoSpawn_6() { return &___autoSpawn_6; }
	inline void set_autoSpawn_6(bool value)
	{
		___autoSpawn_6 = value;
	}

	inline static int32_t get_offset_of_spawnAtStartup_7() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___spawnAtStartup_7)); }
	inline bool get_spawnAtStartup_7() const { return ___spawnAtStartup_7; }
	inline bool* get_address_of_spawnAtStartup_7() { return &___spawnAtStartup_7; }
	inline void set_spawnAtStartup_7(bool value)
	{
		___spawnAtStartup_7 = value;
	}

	inline static int32_t get_offset_of_playSounds_8() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___playSounds_8)); }
	inline bool get_playSounds_8() const { return ___playSounds_8; }
	inline bool* get_address_of_playSounds_8() { return &___playSounds_8; }
	inline void set_playSounds_8(bool value)
	{
		___playSounds_8 = value;
	}

	inline static int32_t get_offset_of_inflateSound_9() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___inflateSound_9)); }
	inline SoundPlayOneshot_t1703214483 * get_inflateSound_9() const { return ___inflateSound_9; }
	inline SoundPlayOneshot_t1703214483 ** get_address_of_inflateSound_9() { return &___inflateSound_9; }
	inline void set_inflateSound_9(SoundPlayOneshot_t1703214483 * value)
	{
		___inflateSound_9 = value;
		Il2CppCodeGenWriteBarrier(&___inflateSound_9, value);
	}

	inline static int32_t get_offset_of_stretchSound_10() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___stretchSound_10)); }
	inline SoundPlayOneshot_t1703214483 * get_stretchSound_10() const { return ___stretchSound_10; }
	inline SoundPlayOneshot_t1703214483 ** get_address_of_stretchSound_10() { return &___stretchSound_10; }
	inline void set_stretchSound_10(SoundPlayOneshot_t1703214483 * value)
	{
		___stretchSound_10 = value;
		Il2CppCodeGenWriteBarrier(&___stretchSound_10, value);
	}

	inline static int32_t get_offset_of_sendSpawnMessageToParent_11() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___sendSpawnMessageToParent_11)); }
	inline bool get_sendSpawnMessageToParent_11() const { return ___sendSpawnMessageToParent_11; }
	inline bool* get_address_of_sendSpawnMessageToParent_11() { return &___sendSpawnMessageToParent_11; }
	inline void set_sendSpawnMessageToParent_11(bool value)
	{
		___sendSpawnMessageToParent_11 = value;
	}

	inline static int32_t get_offset_of_scale_12() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___scale_12)); }
	inline float get_scale_12() const { return ___scale_12; }
	inline float* get_address_of_scale_12() { return &___scale_12; }
	inline void set_scale_12(float value)
	{
		___scale_12 = value;
	}

	inline static int32_t get_offset_of_spawnDirectionTransform_13() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___spawnDirectionTransform_13)); }
	inline Transform_t3275118058 * get_spawnDirectionTransform_13() const { return ___spawnDirectionTransform_13; }
	inline Transform_t3275118058 ** get_address_of_spawnDirectionTransform_13() { return &___spawnDirectionTransform_13; }
	inline void set_spawnDirectionTransform_13(Transform_t3275118058 * value)
	{
		___spawnDirectionTransform_13 = value;
		Il2CppCodeGenWriteBarrier(&___spawnDirectionTransform_13, value);
	}

	inline static int32_t get_offset_of_spawnForce_14() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___spawnForce_14)); }
	inline float get_spawnForce_14() const { return ___spawnForce_14; }
	inline float* get_address_of_spawnForce_14() { return &___spawnForce_14; }
	inline void set_spawnForce_14(float value)
	{
		___spawnForce_14 = value;
	}

	inline static int32_t get_offset_of_attachBalloon_15() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___attachBalloon_15)); }
	inline bool get_attachBalloon_15() const { return ___attachBalloon_15; }
	inline bool* get_address_of_attachBalloon_15() { return &___attachBalloon_15; }
	inline void set_attachBalloon_15(bool value)
	{
		___attachBalloon_15 = value;
	}

	inline static int32_t get_offset_of_color_16() { return static_cast<int32_t>(offsetof(BalloonSpawner_t4219926993, ___color_16)); }
	inline int32_t get_color_16() const { return ___color_16; }
	inline int32_t* get_address_of_color_16() { return &___color_16; }
	inline void set_color_16(int32_t value)
	{
		___color_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
