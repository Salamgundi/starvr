﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.BodyPhysicsEventHandler
struct BodyPhysicsEventHandler_t3963963105;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t720607407;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_BasicTeleport
struct VRTK_BasicTeleport_t3532761337;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;

#include "AssemblyU2DCSharp_VRTK_VRTK_DestinationMarker667613644.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BodyPhysics_FallingRes3718949972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_BodyPhysics
struct  VRTK_BodyPhysics_t3414085265  : public VRTK_DestinationMarker_t667613644
{
public:
	// System.Boolean VRTK.VRTK_BodyPhysics::enableBodyCollisions
	bool ___enableBodyCollisions_9;
	// System.Boolean VRTK.VRTK_BodyPhysics::ignoreGrabbedCollisions
	bool ___ignoreGrabbedCollisions_10;
	// System.Single VRTK.VRTK_BodyPhysics::headsetYOffset
	float ___headsetYOffset_11;
	// System.Single VRTK.VRTK_BodyPhysics::movementThreshold
	float ___movementThreshold_12;
	// System.Int32 VRTK.VRTK_BodyPhysics::standingHistorySamples
	int32_t ___standingHistorySamples_13;
	// System.Single VRTK.VRTK_BodyPhysics::leanYThreshold
	float ___leanYThreshold_14;
	// UnityEngine.LayerMask VRTK.VRTK_BodyPhysics::layersToIgnore
	LayerMask_t3188175821  ___layersToIgnore_15;
	// VRTK.VRTK_BodyPhysics/FallingRestrictors VRTK.VRTK_BodyPhysics::fallRestriction
	int32_t ___fallRestriction_16;
	// System.Single VRTK.VRTK_BodyPhysics::gravityFallYThreshold
	float ___gravityFallYThreshold_17;
	// System.Single VRTK.VRTK_BodyPhysics::blinkYThreshold
	float ___blinkYThreshold_18;
	// System.Single VRTK.VRTK_BodyPhysics::floorHeightTolerance
	float ___floorHeightTolerance_19;
	// VRTK.BodyPhysicsEventHandler VRTK.VRTK_BodyPhysics::StartFalling
	BodyPhysicsEventHandler_t3963963105 * ___StartFalling_20;
	// VRTK.BodyPhysicsEventHandler VRTK.VRTK_BodyPhysics::StopFalling
	BodyPhysicsEventHandler_t3963963105 * ___StopFalling_21;
	// VRTK.BodyPhysicsEventHandler VRTK.VRTK_BodyPhysics::StartMoving
	BodyPhysicsEventHandler_t3963963105 * ___StartMoving_22;
	// VRTK.BodyPhysicsEventHandler VRTK.VRTK_BodyPhysics::StopMoving
	BodyPhysicsEventHandler_t3963963105 * ___StopMoving_23;
	// VRTK.BodyPhysicsEventHandler VRTK.VRTK_BodyPhysics::StartColliding
	BodyPhysicsEventHandler_t3963963105 * ___StartColliding_24;
	// VRTK.BodyPhysicsEventHandler VRTK.VRTK_BodyPhysics::StopColliding
	BodyPhysicsEventHandler_t3963963105 * ___StopColliding_25;
	// UnityEngine.Transform VRTK.VRTK_BodyPhysics::playArea
	Transform_t3275118058 * ___playArea_26;
	// UnityEngine.Transform VRTK.VRTK_BodyPhysics::headset
	Transform_t3275118058 * ___headset_27;
	// UnityEngine.Rigidbody VRTK.VRTK_BodyPhysics::bodyRigidbody
	Rigidbody_t4233889191 * ___bodyRigidbody_28;
	// UnityEngine.CapsuleCollider VRTK.VRTK_BodyPhysics::bodyCollider
	CapsuleCollider_t720607407 * ___bodyCollider_29;
	// System.Boolean VRTK.VRTK_BodyPhysics::currentBodyCollisionsSetting
	bool ___currentBodyCollisionsSetting_30;
	// UnityEngine.GameObject VRTK.VRTK_BodyPhysics::currentCollidingObject
	GameObject_t1756533147 * ___currentCollidingObject_31;
	// UnityEngine.GameObject VRTK.VRTK_BodyPhysics::currentValidFloorObject
	GameObject_t1756533147 * ___currentValidFloorObject_32;
	// VRTK.VRTK_BasicTeleport VRTK.VRTK_BodyPhysics::teleporter
	VRTK_BasicTeleport_t3532761337 * ___teleporter_33;
	// System.Single VRTK.VRTK_BodyPhysics::lastFrameFloorY
	float ___lastFrameFloorY_34;
	// System.Single VRTK.VRTK_BodyPhysics::hitFloorYDelta
	float ___hitFloorYDelta_35;
	// System.Boolean VRTK.VRTK_BodyPhysics::initialFloorDrop
	bool ___initialFloorDrop_36;
	// System.Boolean VRTK.VRTK_BodyPhysics::resetPhysicsAfterTeleport
	bool ___resetPhysicsAfterTeleport_37;
	// System.Boolean VRTK.VRTK_BodyPhysics::storedCurrentPhysics
	bool ___storedCurrentPhysics_38;
	// System.Boolean VRTK.VRTK_BodyPhysics::retogglePhysicsOnCanFall
	bool ___retogglePhysicsOnCanFall_39;
	// System.Boolean VRTK.VRTK_BodyPhysics::storedRetogglePhysics
	bool ___storedRetogglePhysics_40;
	// UnityEngine.Vector3 VRTK.VRTK_BodyPhysics::lastPlayAreaPosition
	Vector3_t2243707580  ___lastPlayAreaPosition_41;
	// UnityEngine.Vector2 VRTK.VRTK_BodyPhysics::currentStandingPosition
	Vector2_t2243707579  ___currentStandingPosition_42;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> VRTK.VRTK_BodyPhysics::standingPositionHistory
	List_1_t1612828711 * ___standingPositionHistory_43;
	// System.Single VRTK.VRTK_BodyPhysics::playAreaHeightAdjustment
	float ___playAreaHeightAdjustment_44;
	// System.Boolean VRTK.VRTK_BodyPhysics::isFalling
	bool ___isFalling_45;
	// System.Boolean VRTK.VRTK_BodyPhysics::isMoving
	bool ___isMoving_46;
	// System.Boolean VRTK.VRTK_BodyPhysics::isLeaning
	bool ___isLeaning_47;
	// System.Boolean VRTK.VRTK_BodyPhysics::onGround
	bool ___onGround_48;
	// System.Boolean VRTK.VRTK_BodyPhysics::preventSnapToFloor
	bool ___preventSnapToFloor_49;
	// System.Boolean VRTK.VRTK_BodyPhysics::generateCollider
	bool ___generateCollider_50;
	// System.Boolean VRTK.VRTK_BodyPhysics::generateRigidbody
	bool ___generateRigidbody_51;
	// UnityEngine.Vector3 VRTK.VRTK_BodyPhysics::playAreaVelocity
	Vector3_t2243707580  ___playAreaVelocity_52;
	// System.Boolean VRTK.VRTK_BodyPhysics::drawDebugGizmo
	bool ___drawDebugGizmo_53;

public:
	inline static int32_t get_offset_of_enableBodyCollisions_9() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___enableBodyCollisions_9)); }
	inline bool get_enableBodyCollisions_9() const { return ___enableBodyCollisions_9; }
	inline bool* get_address_of_enableBodyCollisions_9() { return &___enableBodyCollisions_9; }
	inline void set_enableBodyCollisions_9(bool value)
	{
		___enableBodyCollisions_9 = value;
	}

	inline static int32_t get_offset_of_ignoreGrabbedCollisions_10() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___ignoreGrabbedCollisions_10)); }
	inline bool get_ignoreGrabbedCollisions_10() const { return ___ignoreGrabbedCollisions_10; }
	inline bool* get_address_of_ignoreGrabbedCollisions_10() { return &___ignoreGrabbedCollisions_10; }
	inline void set_ignoreGrabbedCollisions_10(bool value)
	{
		___ignoreGrabbedCollisions_10 = value;
	}

	inline static int32_t get_offset_of_headsetYOffset_11() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___headsetYOffset_11)); }
	inline float get_headsetYOffset_11() const { return ___headsetYOffset_11; }
	inline float* get_address_of_headsetYOffset_11() { return &___headsetYOffset_11; }
	inline void set_headsetYOffset_11(float value)
	{
		___headsetYOffset_11 = value;
	}

	inline static int32_t get_offset_of_movementThreshold_12() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___movementThreshold_12)); }
	inline float get_movementThreshold_12() const { return ___movementThreshold_12; }
	inline float* get_address_of_movementThreshold_12() { return &___movementThreshold_12; }
	inline void set_movementThreshold_12(float value)
	{
		___movementThreshold_12 = value;
	}

	inline static int32_t get_offset_of_standingHistorySamples_13() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___standingHistorySamples_13)); }
	inline int32_t get_standingHistorySamples_13() const { return ___standingHistorySamples_13; }
	inline int32_t* get_address_of_standingHistorySamples_13() { return &___standingHistorySamples_13; }
	inline void set_standingHistorySamples_13(int32_t value)
	{
		___standingHistorySamples_13 = value;
	}

	inline static int32_t get_offset_of_leanYThreshold_14() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___leanYThreshold_14)); }
	inline float get_leanYThreshold_14() const { return ___leanYThreshold_14; }
	inline float* get_address_of_leanYThreshold_14() { return &___leanYThreshold_14; }
	inline void set_leanYThreshold_14(float value)
	{
		___leanYThreshold_14 = value;
	}

	inline static int32_t get_offset_of_layersToIgnore_15() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___layersToIgnore_15)); }
	inline LayerMask_t3188175821  get_layersToIgnore_15() const { return ___layersToIgnore_15; }
	inline LayerMask_t3188175821 * get_address_of_layersToIgnore_15() { return &___layersToIgnore_15; }
	inline void set_layersToIgnore_15(LayerMask_t3188175821  value)
	{
		___layersToIgnore_15 = value;
	}

	inline static int32_t get_offset_of_fallRestriction_16() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___fallRestriction_16)); }
	inline int32_t get_fallRestriction_16() const { return ___fallRestriction_16; }
	inline int32_t* get_address_of_fallRestriction_16() { return &___fallRestriction_16; }
	inline void set_fallRestriction_16(int32_t value)
	{
		___fallRestriction_16 = value;
	}

	inline static int32_t get_offset_of_gravityFallYThreshold_17() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___gravityFallYThreshold_17)); }
	inline float get_gravityFallYThreshold_17() const { return ___gravityFallYThreshold_17; }
	inline float* get_address_of_gravityFallYThreshold_17() { return &___gravityFallYThreshold_17; }
	inline void set_gravityFallYThreshold_17(float value)
	{
		___gravityFallYThreshold_17 = value;
	}

	inline static int32_t get_offset_of_blinkYThreshold_18() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___blinkYThreshold_18)); }
	inline float get_blinkYThreshold_18() const { return ___blinkYThreshold_18; }
	inline float* get_address_of_blinkYThreshold_18() { return &___blinkYThreshold_18; }
	inline void set_blinkYThreshold_18(float value)
	{
		___blinkYThreshold_18 = value;
	}

	inline static int32_t get_offset_of_floorHeightTolerance_19() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___floorHeightTolerance_19)); }
	inline float get_floorHeightTolerance_19() const { return ___floorHeightTolerance_19; }
	inline float* get_address_of_floorHeightTolerance_19() { return &___floorHeightTolerance_19; }
	inline void set_floorHeightTolerance_19(float value)
	{
		___floorHeightTolerance_19 = value;
	}

	inline static int32_t get_offset_of_StartFalling_20() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___StartFalling_20)); }
	inline BodyPhysicsEventHandler_t3963963105 * get_StartFalling_20() const { return ___StartFalling_20; }
	inline BodyPhysicsEventHandler_t3963963105 ** get_address_of_StartFalling_20() { return &___StartFalling_20; }
	inline void set_StartFalling_20(BodyPhysicsEventHandler_t3963963105 * value)
	{
		___StartFalling_20 = value;
		Il2CppCodeGenWriteBarrier(&___StartFalling_20, value);
	}

	inline static int32_t get_offset_of_StopFalling_21() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___StopFalling_21)); }
	inline BodyPhysicsEventHandler_t3963963105 * get_StopFalling_21() const { return ___StopFalling_21; }
	inline BodyPhysicsEventHandler_t3963963105 ** get_address_of_StopFalling_21() { return &___StopFalling_21; }
	inline void set_StopFalling_21(BodyPhysicsEventHandler_t3963963105 * value)
	{
		___StopFalling_21 = value;
		Il2CppCodeGenWriteBarrier(&___StopFalling_21, value);
	}

	inline static int32_t get_offset_of_StartMoving_22() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___StartMoving_22)); }
	inline BodyPhysicsEventHandler_t3963963105 * get_StartMoving_22() const { return ___StartMoving_22; }
	inline BodyPhysicsEventHandler_t3963963105 ** get_address_of_StartMoving_22() { return &___StartMoving_22; }
	inline void set_StartMoving_22(BodyPhysicsEventHandler_t3963963105 * value)
	{
		___StartMoving_22 = value;
		Il2CppCodeGenWriteBarrier(&___StartMoving_22, value);
	}

	inline static int32_t get_offset_of_StopMoving_23() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___StopMoving_23)); }
	inline BodyPhysicsEventHandler_t3963963105 * get_StopMoving_23() const { return ___StopMoving_23; }
	inline BodyPhysicsEventHandler_t3963963105 ** get_address_of_StopMoving_23() { return &___StopMoving_23; }
	inline void set_StopMoving_23(BodyPhysicsEventHandler_t3963963105 * value)
	{
		___StopMoving_23 = value;
		Il2CppCodeGenWriteBarrier(&___StopMoving_23, value);
	}

	inline static int32_t get_offset_of_StartColliding_24() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___StartColliding_24)); }
	inline BodyPhysicsEventHandler_t3963963105 * get_StartColliding_24() const { return ___StartColliding_24; }
	inline BodyPhysicsEventHandler_t3963963105 ** get_address_of_StartColliding_24() { return &___StartColliding_24; }
	inline void set_StartColliding_24(BodyPhysicsEventHandler_t3963963105 * value)
	{
		___StartColliding_24 = value;
		Il2CppCodeGenWriteBarrier(&___StartColliding_24, value);
	}

	inline static int32_t get_offset_of_StopColliding_25() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___StopColliding_25)); }
	inline BodyPhysicsEventHandler_t3963963105 * get_StopColliding_25() const { return ___StopColliding_25; }
	inline BodyPhysicsEventHandler_t3963963105 ** get_address_of_StopColliding_25() { return &___StopColliding_25; }
	inline void set_StopColliding_25(BodyPhysicsEventHandler_t3963963105 * value)
	{
		___StopColliding_25 = value;
		Il2CppCodeGenWriteBarrier(&___StopColliding_25, value);
	}

	inline static int32_t get_offset_of_playArea_26() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___playArea_26)); }
	inline Transform_t3275118058 * get_playArea_26() const { return ___playArea_26; }
	inline Transform_t3275118058 ** get_address_of_playArea_26() { return &___playArea_26; }
	inline void set_playArea_26(Transform_t3275118058 * value)
	{
		___playArea_26 = value;
		Il2CppCodeGenWriteBarrier(&___playArea_26, value);
	}

	inline static int32_t get_offset_of_headset_27() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___headset_27)); }
	inline Transform_t3275118058 * get_headset_27() const { return ___headset_27; }
	inline Transform_t3275118058 ** get_address_of_headset_27() { return &___headset_27; }
	inline void set_headset_27(Transform_t3275118058 * value)
	{
		___headset_27 = value;
		Il2CppCodeGenWriteBarrier(&___headset_27, value);
	}

	inline static int32_t get_offset_of_bodyRigidbody_28() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___bodyRigidbody_28)); }
	inline Rigidbody_t4233889191 * get_bodyRigidbody_28() const { return ___bodyRigidbody_28; }
	inline Rigidbody_t4233889191 ** get_address_of_bodyRigidbody_28() { return &___bodyRigidbody_28; }
	inline void set_bodyRigidbody_28(Rigidbody_t4233889191 * value)
	{
		___bodyRigidbody_28 = value;
		Il2CppCodeGenWriteBarrier(&___bodyRigidbody_28, value);
	}

	inline static int32_t get_offset_of_bodyCollider_29() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___bodyCollider_29)); }
	inline CapsuleCollider_t720607407 * get_bodyCollider_29() const { return ___bodyCollider_29; }
	inline CapsuleCollider_t720607407 ** get_address_of_bodyCollider_29() { return &___bodyCollider_29; }
	inline void set_bodyCollider_29(CapsuleCollider_t720607407 * value)
	{
		___bodyCollider_29 = value;
		Il2CppCodeGenWriteBarrier(&___bodyCollider_29, value);
	}

	inline static int32_t get_offset_of_currentBodyCollisionsSetting_30() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___currentBodyCollisionsSetting_30)); }
	inline bool get_currentBodyCollisionsSetting_30() const { return ___currentBodyCollisionsSetting_30; }
	inline bool* get_address_of_currentBodyCollisionsSetting_30() { return &___currentBodyCollisionsSetting_30; }
	inline void set_currentBodyCollisionsSetting_30(bool value)
	{
		___currentBodyCollisionsSetting_30 = value;
	}

	inline static int32_t get_offset_of_currentCollidingObject_31() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___currentCollidingObject_31)); }
	inline GameObject_t1756533147 * get_currentCollidingObject_31() const { return ___currentCollidingObject_31; }
	inline GameObject_t1756533147 ** get_address_of_currentCollidingObject_31() { return &___currentCollidingObject_31; }
	inline void set_currentCollidingObject_31(GameObject_t1756533147 * value)
	{
		___currentCollidingObject_31 = value;
		Il2CppCodeGenWriteBarrier(&___currentCollidingObject_31, value);
	}

	inline static int32_t get_offset_of_currentValidFloorObject_32() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___currentValidFloorObject_32)); }
	inline GameObject_t1756533147 * get_currentValidFloorObject_32() const { return ___currentValidFloorObject_32; }
	inline GameObject_t1756533147 ** get_address_of_currentValidFloorObject_32() { return &___currentValidFloorObject_32; }
	inline void set_currentValidFloorObject_32(GameObject_t1756533147 * value)
	{
		___currentValidFloorObject_32 = value;
		Il2CppCodeGenWriteBarrier(&___currentValidFloorObject_32, value);
	}

	inline static int32_t get_offset_of_teleporter_33() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___teleporter_33)); }
	inline VRTK_BasicTeleport_t3532761337 * get_teleporter_33() const { return ___teleporter_33; }
	inline VRTK_BasicTeleport_t3532761337 ** get_address_of_teleporter_33() { return &___teleporter_33; }
	inline void set_teleporter_33(VRTK_BasicTeleport_t3532761337 * value)
	{
		___teleporter_33 = value;
		Il2CppCodeGenWriteBarrier(&___teleporter_33, value);
	}

	inline static int32_t get_offset_of_lastFrameFloorY_34() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___lastFrameFloorY_34)); }
	inline float get_lastFrameFloorY_34() const { return ___lastFrameFloorY_34; }
	inline float* get_address_of_lastFrameFloorY_34() { return &___lastFrameFloorY_34; }
	inline void set_lastFrameFloorY_34(float value)
	{
		___lastFrameFloorY_34 = value;
	}

	inline static int32_t get_offset_of_hitFloorYDelta_35() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___hitFloorYDelta_35)); }
	inline float get_hitFloorYDelta_35() const { return ___hitFloorYDelta_35; }
	inline float* get_address_of_hitFloorYDelta_35() { return &___hitFloorYDelta_35; }
	inline void set_hitFloorYDelta_35(float value)
	{
		___hitFloorYDelta_35 = value;
	}

	inline static int32_t get_offset_of_initialFloorDrop_36() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___initialFloorDrop_36)); }
	inline bool get_initialFloorDrop_36() const { return ___initialFloorDrop_36; }
	inline bool* get_address_of_initialFloorDrop_36() { return &___initialFloorDrop_36; }
	inline void set_initialFloorDrop_36(bool value)
	{
		___initialFloorDrop_36 = value;
	}

	inline static int32_t get_offset_of_resetPhysicsAfterTeleport_37() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___resetPhysicsAfterTeleport_37)); }
	inline bool get_resetPhysicsAfterTeleport_37() const { return ___resetPhysicsAfterTeleport_37; }
	inline bool* get_address_of_resetPhysicsAfterTeleport_37() { return &___resetPhysicsAfterTeleport_37; }
	inline void set_resetPhysicsAfterTeleport_37(bool value)
	{
		___resetPhysicsAfterTeleport_37 = value;
	}

	inline static int32_t get_offset_of_storedCurrentPhysics_38() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___storedCurrentPhysics_38)); }
	inline bool get_storedCurrentPhysics_38() const { return ___storedCurrentPhysics_38; }
	inline bool* get_address_of_storedCurrentPhysics_38() { return &___storedCurrentPhysics_38; }
	inline void set_storedCurrentPhysics_38(bool value)
	{
		___storedCurrentPhysics_38 = value;
	}

	inline static int32_t get_offset_of_retogglePhysicsOnCanFall_39() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___retogglePhysicsOnCanFall_39)); }
	inline bool get_retogglePhysicsOnCanFall_39() const { return ___retogglePhysicsOnCanFall_39; }
	inline bool* get_address_of_retogglePhysicsOnCanFall_39() { return &___retogglePhysicsOnCanFall_39; }
	inline void set_retogglePhysicsOnCanFall_39(bool value)
	{
		___retogglePhysicsOnCanFall_39 = value;
	}

	inline static int32_t get_offset_of_storedRetogglePhysics_40() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___storedRetogglePhysics_40)); }
	inline bool get_storedRetogglePhysics_40() const { return ___storedRetogglePhysics_40; }
	inline bool* get_address_of_storedRetogglePhysics_40() { return &___storedRetogglePhysics_40; }
	inline void set_storedRetogglePhysics_40(bool value)
	{
		___storedRetogglePhysics_40 = value;
	}

	inline static int32_t get_offset_of_lastPlayAreaPosition_41() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___lastPlayAreaPosition_41)); }
	inline Vector3_t2243707580  get_lastPlayAreaPosition_41() const { return ___lastPlayAreaPosition_41; }
	inline Vector3_t2243707580 * get_address_of_lastPlayAreaPosition_41() { return &___lastPlayAreaPosition_41; }
	inline void set_lastPlayAreaPosition_41(Vector3_t2243707580  value)
	{
		___lastPlayAreaPosition_41 = value;
	}

	inline static int32_t get_offset_of_currentStandingPosition_42() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___currentStandingPosition_42)); }
	inline Vector2_t2243707579  get_currentStandingPosition_42() const { return ___currentStandingPosition_42; }
	inline Vector2_t2243707579 * get_address_of_currentStandingPosition_42() { return &___currentStandingPosition_42; }
	inline void set_currentStandingPosition_42(Vector2_t2243707579  value)
	{
		___currentStandingPosition_42 = value;
	}

	inline static int32_t get_offset_of_standingPositionHistory_43() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___standingPositionHistory_43)); }
	inline List_1_t1612828711 * get_standingPositionHistory_43() const { return ___standingPositionHistory_43; }
	inline List_1_t1612828711 ** get_address_of_standingPositionHistory_43() { return &___standingPositionHistory_43; }
	inline void set_standingPositionHistory_43(List_1_t1612828711 * value)
	{
		___standingPositionHistory_43 = value;
		Il2CppCodeGenWriteBarrier(&___standingPositionHistory_43, value);
	}

	inline static int32_t get_offset_of_playAreaHeightAdjustment_44() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___playAreaHeightAdjustment_44)); }
	inline float get_playAreaHeightAdjustment_44() const { return ___playAreaHeightAdjustment_44; }
	inline float* get_address_of_playAreaHeightAdjustment_44() { return &___playAreaHeightAdjustment_44; }
	inline void set_playAreaHeightAdjustment_44(float value)
	{
		___playAreaHeightAdjustment_44 = value;
	}

	inline static int32_t get_offset_of_isFalling_45() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___isFalling_45)); }
	inline bool get_isFalling_45() const { return ___isFalling_45; }
	inline bool* get_address_of_isFalling_45() { return &___isFalling_45; }
	inline void set_isFalling_45(bool value)
	{
		___isFalling_45 = value;
	}

	inline static int32_t get_offset_of_isMoving_46() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___isMoving_46)); }
	inline bool get_isMoving_46() const { return ___isMoving_46; }
	inline bool* get_address_of_isMoving_46() { return &___isMoving_46; }
	inline void set_isMoving_46(bool value)
	{
		___isMoving_46 = value;
	}

	inline static int32_t get_offset_of_isLeaning_47() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___isLeaning_47)); }
	inline bool get_isLeaning_47() const { return ___isLeaning_47; }
	inline bool* get_address_of_isLeaning_47() { return &___isLeaning_47; }
	inline void set_isLeaning_47(bool value)
	{
		___isLeaning_47 = value;
	}

	inline static int32_t get_offset_of_onGround_48() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___onGround_48)); }
	inline bool get_onGround_48() const { return ___onGround_48; }
	inline bool* get_address_of_onGround_48() { return &___onGround_48; }
	inline void set_onGround_48(bool value)
	{
		___onGround_48 = value;
	}

	inline static int32_t get_offset_of_preventSnapToFloor_49() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___preventSnapToFloor_49)); }
	inline bool get_preventSnapToFloor_49() const { return ___preventSnapToFloor_49; }
	inline bool* get_address_of_preventSnapToFloor_49() { return &___preventSnapToFloor_49; }
	inline void set_preventSnapToFloor_49(bool value)
	{
		___preventSnapToFloor_49 = value;
	}

	inline static int32_t get_offset_of_generateCollider_50() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___generateCollider_50)); }
	inline bool get_generateCollider_50() const { return ___generateCollider_50; }
	inline bool* get_address_of_generateCollider_50() { return &___generateCollider_50; }
	inline void set_generateCollider_50(bool value)
	{
		___generateCollider_50 = value;
	}

	inline static int32_t get_offset_of_generateRigidbody_51() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___generateRigidbody_51)); }
	inline bool get_generateRigidbody_51() const { return ___generateRigidbody_51; }
	inline bool* get_address_of_generateRigidbody_51() { return &___generateRigidbody_51; }
	inline void set_generateRigidbody_51(bool value)
	{
		___generateRigidbody_51 = value;
	}

	inline static int32_t get_offset_of_playAreaVelocity_52() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___playAreaVelocity_52)); }
	inline Vector3_t2243707580  get_playAreaVelocity_52() const { return ___playAreaVelocity_52; }
	inline Vector3_t2243707580 * get_address_of_playAreaVelocity_52() { return &___playAreaVelocity_52; }
	inline void set_playAreaVelocity_52(Vector3_t2243707580  value)
	{
		___playAreaVelocity_52 = value;
	}

	inline static int32_t get_offset_of_drawDebugGizmo_53() { return static_cast<int32_t>(offsetof(VRTK_BodyPhysics_t3414085265, ___drawDebugGizmo_53)); }
	inline bool get_drawDebugGizmo_53() const { return ___drawDebugGizmo_53; }
	inline bool* get_address_of_drawDebugGizmo_53() { return &___drawDebugGizmo_53; }
	inline void set_drawDebugGizmo_53(bool value)
	{
		___drawDebugGizmo_53 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
