﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_TouchpadWalking
struct VRTK_TouchpadWalking_t2507848959;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"

// System.Void VRTK.VRTK_TouchpadWalking::.ctor()
extern "C"  void VRTK_TouchpadWalking__ctor_m2170475895 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadWalking::Awake()
extern "C"  void VRTK_TouchpadWalking_Awake_m1649679240 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadWalking::OnEnable()
extern "C"  void VRTK_TouchpadWalking_OnEnable_m1583626659 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadWalking::OnDisable()
extern "C"  void VRTK_TouchpadWalking_OnDisable_m2473567300 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadWalking::Update()
extern "C"  void VRTK_TouchpadWalking_Update_m3857399370 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadWalking::FixedUpdate()
extern "C"  void VRTK_TouchpadWalking_FixedUpdate_m8493968 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadWalking::HandleFalling()
extern "C"  void VRTK_TouchpadWalking_HandleFalling_m2567566774 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadWalking::CheckControllerState(UnityEngine.GameObject,System.Boolean,System.Boolean&,System.Boolean&)
extern "C"  void VRTK_TouchpadWalking_CheckControllerState_m2508879645 (VRTK_TouchpadWalking_t2507848959 * __this, GameObject_t1756533147 * ___controller0, bool ___controllerState1, bool* ___subscribedState2, bool* ___previousState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadWalking::DoTouchpadAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_TouchpadWalking_DoTouchpadAxisChanged_m536676883 (VRTK_TouchpadWalking_t2507848959 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadWalking::DoTouchpadTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_TouchpadWalking_DoTouchpadTouchEnd_m847870042 (VRTK_TouchpadWalking_t2507848959 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadWalking::CalculateSpeed(System.Single&,System.Single)
extern "C"  void VRTK_TouchpadWalking_CalculateSpeed_m3501149496 (VRTK_TouchpadWalking_t2507848959 * __this, float* ___speed0, float ___inputValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadWalking::Decelerate(System.Single&)
extern "C"  void VRTK_TouchpadWalking_Decelerate_m1603135194 (VRTK_TouchpadWalking_t2507848959 * __this, float* ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadWalking::Move()
extern "C"  void VRTK_TouchpadWalking_Move_m3469646986 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadWalking::SetControllerListeners(UnityEngine.GameObject,System.Boolean,System.Boolean&,System.Boolean)
extern "C"  void VRTK_TouchpadWalking_SetControllerListeners_m1909848785 (VRTK_TouchpadWalking_t2507848959 * __this, GameObject_t1756533147 * ___controller0, bool ___controllerState1, bool* ___subscribedState2, bool ___forceDisabled3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TouchpadWalking::ToggleControllerListeners(UnityEngine.GameObject,System.Boolean,System.Boolean&)
extern "C"  void VRTK_TouchpadWalking_ToggleControllerListeners_m3493193226 (VRTK_TouchpadWalking_t2507848959 * __this, GameObject_t1756533147 * ___controller0, bool ___toggle1, bool* ___subscribed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
