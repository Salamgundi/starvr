﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ConsoleViewer
struct VRTK_ConsoleViewer_t2446975743;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"

// System.Void VRTK.VRTK_ConsoleViewer::.ctor()
extern "C"  void VRTK_ConsoleViewer__ctor_m3961441795 (VRTK_ConsoleViewer_t2446975743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ConsoleViewer::SetCollapse(System.Boolean)
extern "C"  void VRTK_ConsoleViewer_SetCollapse_m3689577157 (VRTK_ConsoleViewer_t2446975743 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ConsoleViewer::ClearLog()
extern "C"  void VRTK_ConsoleViewer_ClearLog_m1260092722 (VRTK_ConsoleViewer_t2446975743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ConsoleViewer::Awake()
extern "C"  void VRTK_ConsoleViewer_Awake_m844218340 (VRTK_ConsoleViewer_t2446975743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ConsoleViewer::OnEnable()
extern "C"  void VRTK_ConsoleViewer_OnEnable_m1970479951 (VRTK_ConsoleViewer_t2446975743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ConsoleViewer::OnDisable()
extern "C"  void VRTK_ConsoleViewer_OnDisable_m2169880552 (VRTK_ConsoleViewer_t2446975743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VRTK.VRTK_ConsoleViewer::GetMessage(System.String,UnityEngine.LogType)
extern "C"  String_t* VRTK_ConsoleViewer_GetMessage_m383381602 (VRTK_ConsoleViewer_t2446975743 * __this, String_t* ___message0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ConsoleViewer::HandleLog(System.String,System.String,UnityEngine.LogType)
extern "C"  void VRTK_ConsoleViewer_HandleLog_m3981930826 (VRTK_ConsoleViewer_t2446975743 * __this, String_t* ___message0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
