﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2418485124.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.LogType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2506762828_gshared (InternalEnumerator_1_t2418485124 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2506762828(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2418485124 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2506762828_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.LogType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m366406784_gshared (InternalEnumerator_1_t2418485124 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m366406784(__this, method) ((  void (*) (InternalEnumerator_1_t2418485124 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m366406784_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.LogType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3040551830_gshared (InternalEnumerator_1_t2418485124 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3040551830(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2418485124 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3040551830_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.LogType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1439460129_gshared (InternalEnumerator_1_t2418485124 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1439460129(__this, method) ((  void (*) (InternalEnumerator_1_t2418485124 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1439460129_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.LogType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1098390508_gshared (InternalEnumerator_1_t2418485124 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1098390508(__this, method) ((  bool (*) (InternalEnumerator_1_t2418485124 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1098390508_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.LogType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2790740209_gshared (InternalEnumerator_1_t2418485124 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2790740209(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2418485124 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2790740209_gshared)(__this, method)
