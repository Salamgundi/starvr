﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.Hand[]
struct HandU5BU5D_t3984143324;
// Valve.VR.InteractionSystem.Teleport
struct Teleport_t865564691;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0
struct  U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090  : public Il2CppObject
{
public:
	// System.Single Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::<prevBreakTime>__0
	float ___U3CprevBreakTimeU3E__0_0;
	// System.Single Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::<prevHapticPulseTime>__1
	float ___U3CprevHapticPulseTimeU3E__1_1;
	// System.Boolean Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::<pulsed>__2
	bool ___U3CpulsedU3E__2_2;
	// Valve.VR.InteractionSystem.Hand[] Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::$locvar0
	HandU5BU5D_t3984143324* ___U24locvar0_3;
	// System.Int32 Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::$locvar1
	int32_t ___U24locvar1_4;
	// Valve.VR.InteractionSystem.Teleport Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::$this
	Teleport_t865564691 * ___U24this_5;
	// System.Object Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Valve.VR.InteractionSystem.Teleport/<TeleportHintCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CprevBreakTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090, ___U3CprevBreakTimeU3E__0_0)); }
	inline float get_U3CprevBreakTimeU3E__0_0() const { return ___U3CprevBreakTimeU3E__0_0; }
	inline float* get_address_of_U3CprevBreakTimeU3E__0_0() { return &___U3CprevBreakTimeU3E__0_0; }
	inline void set_U3CprevBreakTimeU3E__0_0(float value)
	{
		___U3CprevBreakTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CprevHapticPulseTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090, ___U3CprevHapticPulseTimeU3E__1_1)); }
	inline float get_U3CprevHapticPulseTimeU3E__1_1() const { return ___U3CprevHapticPulseTimeU3E__1_1; }
	inline float* get_address_of_U3CprevHapticPulseTimeU3E__1_1() { return &___U3CprevHapticPulseTimeU3E__1_1; }
	inline void set_U3CprevHapticPulseTimeU3E__1_1(float value)
	{
		___U3CprevHapticPulseTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CpulsedU3E__2_2() { return static_cast<int32_t>(offsetof(U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090, ___U3CpulsedU3E__2_2)); }
	inline bool get_U3CpulsedU3E__2_2() const { return ___U3CpulsedU3E__2_2; }
	inline bool* get_address_of_U3CpulsedU3E__2_2() { return &___U3CpulsedU3E__2_2; }
	inline void set_U3CpulsedU3E__2_2(bool value)
	{
		___U3CpulsedU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_3() { return static_cast<int32_t>(offsetof(U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090, ___U24locvar0_3)); }
	inline HandU5BU5D_t3984143324* get_U24locvar0_3() const { return ___U24locvar0_3; }
	inline HandU5BU5D_t3984143324** get_address_of_U24locvar0_3() { return &___U24locvar0_3; }
	inline void set_U24locvar0_3(HandU5BU5D_t3984143324* value)
	{
		___U24locvar0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_3, value);
	}

	inline static int32_t get_offset_of_U24locvar1_4() { return static_cast<int32_t>(offsetof(U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090, ___U24locvar1_4)); }
	inline int32_t get_U24locvar1_4() const { return ___U24locvar1_4; }
	inline int32_t* get_address_of_U24locvar1_4() { return &___U24locvar1_4; }
	inline void set_U24locvar1_4(int32_t value)
	{
		___U24locvar1_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090, ___U24this_5)); }
	inline Teleport_t865564691 * get_U24this_5() const { return ___U24this_5; }
	inline Teleport_t865564691 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(Teleport_t865564691 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
