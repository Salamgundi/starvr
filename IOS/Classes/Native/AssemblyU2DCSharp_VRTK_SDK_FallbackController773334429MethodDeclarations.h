﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_FallbackController
struct SDK_FallbackController_t773334429;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.SDK_ControllerHapticModifiers
struct SDK_ControllerHapticModifiers_t3871094838;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_SDK_BaseController_Controlle246418309.h"
#include "AssemblyU2DCSharp_VRTK_SDK_BaseController_Controlle447683143.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void VRTK.SDK_FallbackController::.ctor()
extern "C"  void SDK_FallbackController__ctor_m2372506297 (SDK_FallbackController_t773334429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_FallbackController::ProcessUpdate(System.UInt32,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void SDK_FallbackController_ProcessUpdate_m4218575690 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, Dictionary_2_t309261261 * ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VRTK.SDK_FallbackController::GetControllerDefaultColliderPath(VRTK.SDK_BaseController/ControllerHand)
extern "C"  String_t* SDK_FallbackController_GetControllerDefaultColliderPath_m3647649341 (SDK_FallbackController_t773334429 * __this, int32_t ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VRTK.SDK_FallbackController::GetControllerElementPath(VRTK.SDK_BaseController/ControllerElements,VRTK.SDK_BaseController/ControllerHand,System.Boolean)
extern "C"  String_t* SDK_FallbackController_GetControllerElementPath_m2001662574 (SDK_FallbackController_t773334429 * __this, int32_t ___element0, int32_t ___hand1, bool ___fullPath2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 VRTK.SDK_FallbackController::GetControllerIndex(UnityEngine.GameObject)
extern "C"  uint32_t SDK_FallbackController_GetControllerIndex_m4189439300 (SDK_FallbackController_t773334429 * __this, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.SDK_FallbackController::GetControllerByIndex(System.UInt32,System.Boolean)
extern "C"  GameObject_t1756533147 * SDK_FallbackController_GetControllerByIndex_m2292500300 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, bool ___actual1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.SDK_FallbackController::GetControllerOrigin(UnityEngine.GameObject)
extern "C"  Transform_t3275118058 * SDK_FallbackController_GetControllerOrigin_m199731051 (SDK_FallbackController_t773334429 * __this, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.SDK_FallbackController::GenerateControllerPointerOrigin(UnityEngine.GameObject)
extern "C"  Transform_t3275118058 * SDK_FallbackController_GenerateControllerPointerOrigin_m984026665 (SDK_FallbackController_t773334429 * __this, GameObject_t1756533147 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.SDK_FallbackController::GetControllerLeftHand(System.Boolean)
extern "C"  GameObject_t1756533147 * SDK_FallbackController_GetControllerLeftHand_m2814260011 (SDK_FallbackController_t773334429 * __this, bool ___actual0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.SDK_FallbackController::GetControllerRightHand(System.Boolean)
extern "C"  GameObject_t1756533147 * SDK_FallbackController_GetControllerRightHand_m4226363824 (SDK_FallbackController_t773334429 * __this, bool ___actual0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsControllerLeftHand(UnityEngine.GameObject)
extern "C"  bool SDK_FallbackController_IsControllerLeftHand_m1318346467 (SDK_FallbackController_t773334429 * __this, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsControllerRightHand(UnityEngine.GameObject)
extern "C"  bool SDK_FallbackController_IsControllerRightHand_m1063587654 (SDK_FallbackController_t773334429 * __this, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsControllerLeftHand(UnityEngine.GameObject,System.Boolean)
extern "C"  bool SDK_FallbackController_IsControllerLeftHand_m1596503176 (SDK_FallbackController_t773334429 * __this, GameObject_t1756533147 * ___controller0, bool ___actual1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsControllerRightHand(UnityEngine.GameObject,System.Boolean)
extern "C"  bool SDK_FallbackController_IsControllerRightHand_m3531218691 (SDK_FallbackController_t773334429 * __this, GameObject_t1756533147 * ___controller0, bool ___actual1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.SDK_FallbackController::GetControllerModel(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * SDK_FallbackController_GetControllerModel_m4028156603 (SDK_FallbackController_t773334429 * __this, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.SDK_FallbackController::GetControllerModel(VRTK.SDK_BaseController/ControllerHand)
extern "C"  GameObject_t1756533147 * SDK_FallbackController_GetControllerModel_m313118250 (SDK_FallbackController_t773334429 * __this, int32_t ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.SDK_FallbackController::GetControllerRenderModel(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * SDK_FallbackController_GetControllerRenderModel_m2484911905 (SDK_FallbackController_t773334429 * __this, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_FallbackController::SetControllerRenderModelWheel(UnityEngine.GameObject,System.Boolean)
extern "C"  void SDK_FallbackController_SetControllerRenderModelWheel_m2084922794 (SDK_FallbackController_t773334429 * __this, GameObject_t1756533147 * ___renderModel0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_FallbackController::HapticPulseOnIndex(System.UInt32,System.Single)
extern "C"  void SDK_FallbackController_HapticPulseOnIndex_m2050364891 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, float ___strength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.SDK_ControllerHapticModifiers VRTK.SDK_FallbackController::GetHapticModifiers()
extern "C"  SDK_ControllerHapticModifiers_t3871094838 * SDK_FallbackController_GetHapticModifiers_m3039514564 (SDK_FallbackController_t773334429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.SDK_FallbackController::GetVelocityOnIndex(System.UInt32)
extern "C"  Vector3_t2243707580  SDK_FallbackController_GetVelocityOnIndex_m1674602541 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.SDK_FallbackController::GetAngularVelocityOnIndex(System.UInt32)
extern "C"  Vector3_t2243707580  SDK_FallbackController_GetAngularVelocityOnIndex_m625499755 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 VRTK.SDK_FallbackController::GetTouchpadAxisOnIndex(System.UInt32)
extern "C"  Vector2_t2243707579  SDK_FallbackController_GetTouchpadAxisOnIndex_m985714646 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 VRTK.SDK_FallbackController::GetTriggerAxisOnIndex(System.UInt32)
extern "C"  Vector2_t2243707579  SDK_FallbackController_GetTriggerAxisOnIndex_m463417160 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 VRTK.SDK_FallbackController::GetGripAxisOnIndex(System.UInt32)
extern "C"  Vector2_t2243707579  SDK_FallbackController_GetGripAxisOnIndex_m4030325542 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.SDK_FallbackController::GetTriggerHairlineDeltaOnIndex(System.UInt32)
extern "C"  float SDK_FallbackController_GetTriggerHairlineDeltaOnIndex_m3382108418 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.SDK_FallbackController::GetGripHairlineDeltaOnIndex(System.UInt32)
extern "C"  float SDK_FallbackController_GetGripHairlineDeltaOnIndex_m223014334 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsTriggerPressedOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsTriggerPressedOnIndex_m1722051336 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsTriggerPressedDownOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsTriggerPressedDownOnIndex_m71518772 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsTriggerPressedUpOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsTriggerPressedUpOnIndex_m3540461731 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsTriggerTouchedOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsTriggerTouchedOnIndex_m1941193552 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsTriggerTouchedDownOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsTriggerTouchedDownOnIndex_m1125983176 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsTriggerTouchedUpOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsTriggerTouchedUpOnIndex_m2743005301 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsHairTriggerDownOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsHairTriggerDownOnIndex_m1968678182 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsHairTriggerUpOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsHairTriggerUpOnIndex_m848892151 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsGripPressedOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsGripPressedOnIndex_m3103403996 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsGripPressedDownOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsGripPressedDownOnIndex_m1598485328 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsGripPressedUpOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsGripPressedUpOnIndex_m1994145675 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsGripTouchedOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsGripTouchedOnIndex_m380646036 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsGripTouchedDownOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsGripTouchedDownOnIndex_m2562918100 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsGripTouchedUpOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsGripTouchedUpOnIndex_m3165853157 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsHairGripDownOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsHairGripDownOnIndex_m247654734 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsHairGripUpOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsHairGripUpOnIndex_m675499767 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsTouchpadPressedOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsTouchpadPressedOnIndex_m1144781912 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsTouchpadPressedDownOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsTouchpadPressedDownOnIndex_m1041506776 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsTouchpadPressedUpOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsTouchpadPressedUpOnIndex_m3617139879 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsTouchpadTouchedOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsTouchpadTouchedOnIndex_m3545678412 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsTouchpadTouchedDownOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsTouchpadTouchedDownOnIndex_m2455225368 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsTouchpadTouchedUpOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsTouchpadTouchedUpOnIndex_m1323641661 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsButtonOnePressedOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsButtonOnePressedOnIndex_m2611275980 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsButtonOnePressedDownOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsButtonOnePressedDownOnIndex_m1162318752 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsButtonOnePressedUpOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsButtonOnePressedUpOnIndex_m4063444969 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsButtonOneTouchedOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsButtonOneTouchedOnIndex_m4014616464 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsButtonOneTouchedDownOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsButtonOneTouchedDownOnIndex_m474703384 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsButtonOneTouchedUpOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsButtonOneTouchedUpOnIndex_m2356906419 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsButtonTwoPressedOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsButtonTwoPressedOnIndex_m224494906 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsButtonTwoPressedDownOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsButtonTwoPressedDownOnIndex_m1600764130 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsButtonTwoPressedUpOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsButtonTwoPressedUpOnIndex_m43130645 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsButtonTwoTouchedOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsButtonTwoTouchedOnIndex_m3686043426 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsButtonTwoTouchedDownOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsButtonTwoTouchedDownOnIndex_m2018510358 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsButtonTwoTouchedUpOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsButtonTwoTouchedUpOnIndex_m3043090567 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsStartMenuPressedOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsStartMenuPressedOnIndex_m997586901 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsStartMenuPressedDownOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsStartMenuPressedDownOnIndex_m869722087 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsStartMenuPressedUpOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsStartMenuPressedUpOnIndex_m1694864728 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsStartMenuTouchedOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsStartMenuTouchedOnIndex_m2533002887 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsStartMenuTouchedDownOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsStartMenuTouchedDownOnIndex_m3981055185 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackController::IsStartMenuTouchedUpOnIndex(System.UInt32)
extern "C"  bool SDK_FallbackController_IsStartMenuTouchedUpOnIndex_m740136772 (SDK_FallbackController_t773334429 * __this, uint32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_FallbackController::Awake()
extern "C"  void SDK_FallbackController_Awake_m2134279874 (SDK_FallbackController_t773334429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
