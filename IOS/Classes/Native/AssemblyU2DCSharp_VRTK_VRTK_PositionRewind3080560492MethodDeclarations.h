﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_PositionRewind
struct VRTK_PositionRewind_t3080560492;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventArgs1242373387.h"

// System.Void VRTK.VRTK_PositionRewind::.ctor()
extern "C"  void VRTK_PositionRewind__ctor_m1004498988 (VRTK_PositionRewind_t3080560492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PositionRewind::OnEnable()
extern "C"  void VRTK_PositionRewind_OnEnable_m278636340 (VRTK_PositionRewind_t3080560492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PositionRewind::OnDisable()
extern "C"  void VRTK_PositionRewind_OnDisable_m2501627641 (VRTK_PositionRewind_t3080560492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PositionRewind::Update()
extern "C"  void VRTK_PositionRewind_Update_m2107481181 (VRTK_PositionRewind_t3080560492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PositionRewind::FixedUpdate()
extern "C"  void VRTK_PositionRewind_FixedUpdate_m2495915011 (VRTK_PositionRewind_t3080560492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PositionRewind::StartCollision()
extern "C"  void VRTK_PositionRewind_StartCollision_m3729781230 (VRTK_PositionRewind_t3080560492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PositionRewind::EndCollision()
extern "C"  void VRTK_PositionRewind_EndCollision_m1833146629 (VRTK_PositionRewind_t3080560492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PositionRewind::RewindPosition()
extern "C"  void VRTK_PositionRewind_RewindPosition_m1837919626 (VRTK_PositionRewind_t3080560492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PositionRewind::ManageHeadsetListeners(System.Boolean)
extern "C"  void VRTK_PositionRewind_ManageHeadsetListeners_m1444581429 (VRTK_PositionRewind_t3080560492 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PositionRewind::HeadsetCollision_HeadsetCollisionDetect(System.Object,VRTK.HeadsetCollisionEventArgs)
extern "C"  void VRTK_PositionRewind_HeadsetCollision_HeadsetCollisionDetect_m3323639296 (VRTK_PositionRewind_t3080560492 * __this, Il2CppObject * ___sender0, HeadsetCollisionEventArgs_t1242373387  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PositionRewind::HeadsetCollision_HeadsetCollisionEnded(System.Object,VRTK.HeadsetCollisionEventArgs)
extern "C"  void VRTK_PositionRewind_HeadsetCollision_HeadsetCollisionEnded_m1306760273 (VRTK_PositionRewind_t3080560492 * __this, Il2CppObject * ___sender0, HeadsetCollisionEventArgs_t1242373387  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
