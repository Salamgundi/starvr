﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>
struct ShimEnumerator_t1157699805;
// System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>
struct Dictionary_2_t1052574984;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2404882574_gshared (ShimEnumerator_t1157699805 * __this, Dictionary_2_t1052574984 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m2404882574(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1157699805 *, Dictionary_2_t1052574984 *, const MethodInfo*))ShimEnumerator__ctor_m2404882574_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1506759531_gshared (ShimEnumerator_t1157699805 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1506759531(__this, method) ((  bool (*) (ShimEnumerator_t1157699805 *, const MethodInfo*))ShimEnumerator_MoveNext_m1506759531_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2894378391_gshared (ShimEnumerator_t1157699805 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2894378391(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1157699805 *, const MethodInfo*))ShimEnumerator_get_Entry_m2894378391_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m4116147578_gshared (ShimEnumerator_t1157699805 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m4116147578(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1157699805 *, const MethodInfo*))ShimEnumerator_get_Key_m4116147578_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3980156972_gshared (ShimEnumerator_t1157699805 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3980156972(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1157699805 *, const MethodInfo*))ShimEnumerator_get_Value_m3980156972_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1299129360_gshared (ShimEnumerator_t1157699805 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1299129360(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1157699805 *, const MethodInfo*))ShimEnumerator_get_Current_m1299129360_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1432170168_gshared (ShimEnumerator_t1157699805 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1432170168(__this, method) ((  void (*) (ShimEnumerator_t1157699805 *, const MethodInfo*))ShimEnumerator_Reset_m1432170168_gshared)(__this, method)
