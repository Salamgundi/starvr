﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// VRTK.UnityEventHelper.VRTK_Control_UnityEvents
struct VRTK_Control_UnityEvents_t692650506;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.ControlReactor
struct  ControlReactor_t2704852751  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.TextMesh VRTK.Examples.ControlReactor::go
	TextMesh_t1641806576 * ___go_2;
	// VRTK.UnityEventHelper.VRTK_Control_UnityEvents VRTK.Examples.ControlReactor::controlEvents
	VRTK_Control_UnityEvents_t692650506 * ___controlEvents_3;

public:
	inline static int32_t get_offset_of_go_2() { return static_cast<int32_t>(offsetof(ControlReactor_t2704852751, ___go_2)); }
	inline TextMesh_t1641806576 * get_go_2() const { return ___go_2; }
	inline TextMesh_t1641806576 ** get_address_of_go_2() { return &___go_2; }
	inline void set_go_2(TextMesh_t1641806576 * value)
	{
		___go_2 = value;
		Il2CppCodeGenWriteBarrier(&___go_2, value);
	}

	inline static int32_t get_offset_of_controlEvents_3() { return static_cast<int32_t>(offsetof(ControlReactor_t2704852751, ___controlEvents_3)); }
	inline VRTK_Control_UnityEvents_t692650506 * get_controlEvents_3() const { return ___controlEvents_3; }
	inline VRTK_Control_UnityEvents_t692650506 ** get_address_of_controlEvents_3() { return &___controlEvents_3; }
	inline void set_controlEvents_3(VRTK_Control_UnityEvents_t692650506 * value)
	{
		___controlEvents_3 = value;
		Il2CppCodeGenWriteBarrier(&___controlEvents_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
