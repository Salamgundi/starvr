﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ArcheryTarget
struct ArcheryTarget_t3389223157;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.ArcheryTarget::.ctor()
extern "C"  void ArcheryTarget__ctor_m2281354113 (ArcheryTarget_t3389223157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArcheryTarget::ApplyDamage()
extern "C"  void ArcheryTarget_ApplyDamage_m1016998740 (ArcheryTarget_t3389223157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArcheryTarget::FireExposure()
extern "C"  void ArcheryTarget_FireExposure_m2230193048 (ArcheryTarget_t3389223157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArcheryTarget::OnDamageTaken()
extern "C"  void ArcheryTarget_OnDamageTaken_m3366257262 (ArcheryTarget_t3389223157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.ArcheryTarget::FallDown()
extern "C"  Il2CppObject * ArcheryTarget_FallDown_m3969965724 (ArcheryTarget_t3389223157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
