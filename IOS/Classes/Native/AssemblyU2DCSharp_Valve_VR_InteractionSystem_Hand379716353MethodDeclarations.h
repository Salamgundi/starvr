﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct ReadOnlyCollection_1_t1573503628;
// Valve.VR.InteractionSystem.Interactable
struct Interactable_t1274046986;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Inter1274046986.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1719631858.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1543711741.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1387717936.h"

// System.Void Valve.VR.InteractionSystem.Hand::.ctor()
extern "C"  void Hand__ctor_m277208061 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject> Valve.VR.InteractionSystem.Hand::get_AttachedObjects()
extern "C"  ReadOnlyCollection_1_t1573503628 * Hand_get_AttachedObjects_m3434582796 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Hand::get_hoverLocked()
extern "C"  bool Hand_get_hoverLocked_m539024660 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::set_hoverLocked(System.Boolean)
extern "C"  void Hand_set_hoverLocked_m1114938075 (Hand_t379716353 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.InteractionSystem.Interactable Valve.VR.InteractionSystem.Hand::get_hoveringInteractable()
extern "C"  Interactable_t1274046986 * Hand_get_hoveringInteractable_m2988763590 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::set_hoveringInteractable(Valve.VR.InteractionSystem.Interactable)
extern "C"  void Hand_set_hoveringInteractable_m680376679 (Hand_t379716353 * __this, Interactable_t1274046986 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Valve.VR.InteractionSystem.Hand::get_currentAttachedObject()
extern "C"  GameObject_t1756533147 * Hand_get_currentAttachedObject_m2387732129 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Valve.VR.InteractionSystem.Hand::GetAttachmentTransform(System.String)
extern "C"  Transform_t3275118058 * Hand_GetAttachmentTransform_m4135058386 (Hand_t379716353 * __this, String_t* ___attachmentPoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.InteractionSystem.Hand/HandType Valve.VR.InteractionSystem.Hand::GuessCurrentHandType()
extern "C"  int32_t Hand_GuessCurrentHandType_m1896238253 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::AttachObject(UnityEngine.GameObject,Valve.VR.InteractionSystem.Hand/AttachmentFlags,System.String)
extern "C"  void Hand_AttachObject_m1638015214 (Hand_t379716353 * __this, GameObject_t1756533147 * ___objectToAttach0, int32_t ___flags1, String_t* ___attachmentPoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::DetachObject(UnityEngine.GameObject,System.Boolean)
extern "C"  void Hand_DetachObject_m1254624158 (Hand_t379716353 * __this, GameObject_t1756533147 * ___objectToDetach0, bool ___restoreOriginalParent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Valve.VR.InteractionSystem.Hand::GetTrackedObjectVelocity()
extern "C"  Vector3_t2243707580  Hand_GetTrackedObjectVelocity_m4140891981 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Valve.VR.InteractionSystem.Hand::GetTrackedObjectAngularVelocity()
extern "C"  Vector3_t2243707580  Hand_GetTrackedObjectAngularVelocity_m297224883 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::CleanUpAttachedObjectStack()
extern "C"  void Hand_CleanUpAttachedObjectStack_m1922934840 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::Awake()
extern "C"  void Hand_Awake_m2284441334 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.Hand::Start()
extern "C"  Il2CppObject * Hand_Start_m3913891079 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::UpdateHovering()
extern "C"  void Hand_UpdateHovering_m2609941514 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::UpdateNoSteamVRFallback()
extern "C"  void Hand_UpdateNoSteamVRFallback_m4125092855 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::UpdateDebugText()
extern "C"  void Hand_UpdateDebugText_m2901599634 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::OnEnable()
extern "C"  void Hand_OnEnable_m3482071133 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::OnDisable()
extern "C"  void Hand_OnDisable_m3046193198 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::Update()
extern "C"  void Hand_Update_m2298948664 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::LateUpdate()
extern "C"  void Hand_LateUpdate_m4210078828 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::OnInputFocus(System.Boolean)
extern "C"  void Hand_OnInputFocus_m3041257197 (Hand_t379716353 * __this, bool ___hasFocus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::FixedUpdate()
extern "C"  void Hand_FixedUpdate_m2065973530 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::OnDrawGizmos()
extern "C"  void Hand_OnDrawGizmos_m1273465825 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::HandDebugLog(System.String)
extern "C"  void Hand_HandDebugLog_m2480859081 (Hand_t379716353 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::UpdateHandPoses()
extern "C"  void Hand_UpdateHandPoses_m1511947849 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::HoverLock(Valve.VR.InteractionSystem.Interactable)
extern "C"  void Hand_HoverLock_m536782533 (Hand_t379716353 * __this, Interactable_t1274046986 * ___interactable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::HoverUnlock(Valve.VR.InteractionSystem.Interactable)
extern "C"  void Hand_HoverUnlock_m1522862212 (Hand_t379716353 * __this, Interactable_t1274046986 * ___interactable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Hand::GetStandardInteractionButtonDown()
extern "C"  bool Hand_GetStandardInteractionButtonDown_m1339061940 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Hand::GetStandardInteractionButtonUp()
extern "C"  bool Hand_GetStandardInteractionButtonUp_m1309695169 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Hand::GetStandardInteractionButton()
extern "C"  bool Hand_GetStandardInteractionButton_m546658876 (Hand_t379716353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Hand::InitController(System.Int32)
extern "C"  void Hand_InitController_m3981843474 (Hand_t379716353 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Hand::<CleanUpAttachedObjectStack>m__0(Valve.VR.InteractionSystem.Hand/AttachedObject)
extern "C"  bool Hand_U3CCleanUpAttachedObjectStackU3Em__0_m2633004717 (Il2CppObject * __this /* static, unused */, AttachedObject_t1387717936  ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
