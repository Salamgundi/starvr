﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetOverlayRenderingPid
struct _SetOverlayRenderingPid_t1970553664;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetOverlayRenderingPid::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayRenderingPid__ctor_m1493727551 (_SetOverlayRenderingPid_t1970553664 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayRenderingPid::Invoke(System.UInt64,System.UInt32)
extern "C"  int32_t _SetOverlayRenderingPid_Invoke_m2792708852 (_SetOverlayRenderingPid_t1970553664 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unPID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayRenderingPid::BeginInvoke(System.UInt64,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetOverlayRenderingPid_BeginInvoke_m1782692179 (_SetOverlayRenderingPid_t1970553664 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unPID1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayRenderingPid::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayRenderingPid_EndInvoke_m70980545 (_SetOverlayRenderingPid_t1970553664 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
