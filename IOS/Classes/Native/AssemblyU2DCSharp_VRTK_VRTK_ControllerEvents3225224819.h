﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.ControllerInteractionEventHandler
struct ControllerInteractionEventHandler_t343979916;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ControllerEvents
struct  VRTK_ControllerEvents_t3225224819  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_ControllerEvents::pointerToggleButton
	int32_t ___pointerToggleButton_2;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_ControllerEvents::pointerSetButton
	int32_t ___pointerSetButton_3;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_ControllerEvents::grabToggleButton
	int32_t ___grabToggleButton_4;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_ControllerEvents::useToggleButton
	int32_t ___useToggleButton_5;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_ControllerEvents::uiClickButton
	int32_t ___uiClickButton_6;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_ControllerEvents::menuToggleButton
	int32_t ___menuToggleButton_7;
	// System.Int32 VRTK.VRTK_ControllerEvents::axisFidelity
	int32_t ___axisFidelity_8;
	// System.Single VRTK.VRTK_ControllerEvents::triggerClickThreshold
	float ___triggerClickThreshold_9;
	// System.Single VRTK.VRTK_ControllerEvents::gripClickThreshold
	float ___gripClickThreshold_10;
	// System.Boolean VRTK.VRTK_ControllerEvents::triggerPressed
	bool ___triggerPressed_11;
	// System.Boolean VRTK.VRTK_ControllerEvents::triggerTouched
	bool ___triggerTouched_12;
	// System.Boolean VRTK.VRTK_ControllerEvents::triggerHairlinePressed
	bool ___triggerHairlinePressed_13;
	// System.Boolean VRTK.VRTK_ControllerEvents::triggerClicked
	bool ___triggerClicked_14;
	// System.Boolean VRTK.VRTK_ControllerEvents::triggerAxisChanged
	bool ___triggerAxisChanged_15;
	// System.Boolean VRTK.VRTK_ControllerEvents::gripPressed
	bool ___gripPressed_16;
	// System.Boolean VRTK.VRTK_ControllerEvents::gripTouched
	bool ___gripTouched_17;
	// System.Boolean VRTK.VRTK_ControllerEvents::gripHairlinePressed
	bool ___gripHairlinePressed_18;
	// System.Boolean VRTK.VRTK_ControllerEvents::gripClicked
	bool ___gripClicked_19;
	// System.Boolean VRTK.VRTK_ControllerEvents::gripAxisChanged
	bool ___gripAxisChanged_20;
	// System.Boolean VRTK.VRTK_ControllerEvents::touchpadPressed
	bool ___touchpadPressed_21;
	// System.Boolean VRTK.VRTK_ControllerEvents::touchpadTouched
	bool ___touchpadTouched_22;
	// System.Boolean VRTK.VRTK_ControllerEvents::touchpadAxisChanged
	bool ___touchpadAxisChanged_23;
	// System.Boolean VRTK.VRTK_ControllerEvents::buttonOnePressed
	bool ___buttonOnePressed_24;
	// System.Boolean VRTK.VRTK_ControllerEvents::buttonOneTouched
	bool ___buttonOneTouched_25;
	// System.Boolean VRTK.VRTK_ControllerEvents::buttonTwoPressed
	bool ___buttonTwoPressed_26;
	// System.Boolean VRTK.VRTK_ControllerEvents::buttonTwoTouched
	bool ___buttonTwoTouched_27;
	// System.Boolean VRTK.VRTK_ControllerEvents::startMenuPressed
	bool ___startMenuPressed_28;
	// System.Boolean VRTK.VRTK_ControllerEvents::pointerPressed
	bool ___pointerPressed_29;
	// System.Boolean VRTK.VRTK_ControllerEvents::grabPressed
	bool ___grabPressed_30;
	// System.Boolean VRTK.VRTK_ControllerEvents::usePressed
	bool ___usePressed_31;
	// System.Boolean VRTK.VRTK_ControllerEvents::uiClickPressed
	bool ___uiClickPressed_32;
	// System.Boolean VRTK.VRTK_ControllerEvents::menuPressed
	bool ___menuPressed_33;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::TriggerPressed
	ControllerInteractionEventHandler_t343979916 * ___TriggerPressed_34;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::TriggerReleased
	ControllerInteractionEventHandler_t343979916 * ___TriggerReleased_35;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::TriggerTouchStart
	ControllerInteractionEventHandler_t343979916 * ___TriggerTouchStart_36;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::TriggerTouchEnd
	ControllerInteractionEventHandler_t343979916 * ___TriggerTouchEnd_37;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::TriggerHairlineStart
	ControllerInteractionEventHandler_t343979916 * ___TriggerHairlineStart_38;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::TriggerHairlineEnd
	ControllerInteractionEventHandler_t343979916 * ___TriggerHairlineEnd_39;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::TriggerClicked
	ControllerInteractionEventHandler_t343979916 * ___TriggerClicked_40;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::TriggerUnclicked
	ControllerInteractionEventHandler_t343979916 * ___TriggerUnclicked_41;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::TriggerAxisChanged
	ControllerInteractionEventHandler_t343979916 * ___TriggerAxisChanged_42;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::GripPressed
	ControllerInteractionEventHandler_t343979916 * ___GripPressed_43;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::GripReleased
	ControllerInteractionEventHandler_t343979916 * ___GripReleased_44;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::GripTouchStart
	ControllerInteractionEventHandler_t343979916 * ___GripTouchStart_45;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::GripTouchEnd
	ControllerInteractionEventHandler_t343979916 * ___GripTouchEnd_46;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::GripHairlineStart
	ControllerInteractionEventHandler_t343979916 * ___GripHairlineStart_47;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::GripHairlineEnd
	ControllerInteractionEventHandler_t343979916 * ___GripHairlineEnd_48;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::GripClicked
	ControllerInteractionEventHandler_t343979916 * ___GripClicked_49;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::GripUnclicked
	ControllerInteractionEventHandler_t343979916 * ___GripUnclicked_50;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::GripAxisChanged
	ControllerInteractionEventHandler_t343979916 * ___GripAxisChanged_51;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::TouchpadPressed
	ControllerInteractionEventHandler_t343979916 * ___TouchpadPressed_52;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::TouchpadReleased
	ControllerInteractionEventHandler_t343979916 * ___TouchpadReleased_53;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::TouchpadTouchStart
	ControllerInteractionEventHandler_t343979916 * ___TouchpadTouchStart_54;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::TouchpadTouchEnd
	ControllerInteractionEventHandler_t343979916 * ___TouchpadTouchEnd_55;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::TouchpadAxisChanged
	ControllerInteractionEventHandler_t343979916 * ___TouchpadAxisChanged_56;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::ButtonOneTouchStart
	ControllerInteractionEventHandler_t343979916 * ___ButtonOneTouchStart_57;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::ButtonOneTouchEnd
	ControllerInteractionEventHandler_t343979916 * ___ButtonOneTouchEnd_58;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::ButtonOnePressed
	ControllerInteractionEventHandler_t343979916 * ___ButtonOnePressed_59;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::ButtonOneReleased
	ControllerInteractionEventHandler_t343979916 * ___ButtonOneReleased_60;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::ButtonTwoTouchStart
	ControllerInteractionEventHandler_t343979916 * ___ButtonTwoTouchStart_61;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::ButtonTwoTouchEnd
	ControllerInteractionEventHandler_t343979916 * ___ButtonTwoTouchEnd_62;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::ButtonTwoPressed
	ControllerInteractionEventHandler_t343979916 * ___ButtonTwoPressed_63;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::ButtonTwoReleased
	ControllerInteractionEventHandler_t343979916 * ___ButtonTwoReleased_64;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::StartMenuPressed
	ControllerInteractionEventHandler_t343979916 * ___StartMenuPressed_65;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::StartMenuReleased
	ControllerInteractionEventHandler_t343979916 * ___StartMenuReleased_66;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::AliasPointerOn
	ControllerInteractionEventHandler_t343979916 * ___AliasPointerOn_67;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::AliasPointerOff
	ControllerInteractionEventHandler_t343979916 * ___AliasPointerOff_68;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::AliasPointerSet
	ControllerInteractionEventHandler_t343979916 * ___AliasPointerSet_69;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::AliasGrabOn
	ControllerInteractionEventHandler_t343979916 * ___AliasGrabOn_70;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::AliasGrabOff
	ControllerInteractionEventHandler_t343979916 * ___AliasGrabOff_71;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::AliasUseOn
	ControllerInteractionEventHandler_t343979916 * ___AliasUseOn_72;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::AliasUseOff
	ControllerInteractionEventHandler_t343979916 * ___AliasUseOff_73;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::AliasMenuOn
	ControllerInteractionEventHandler_t343979916 * ___AliasMenuOn_74;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::AliasMenuOff
	ControllerInteractionEventHandler_t343979916 * ___AliasMenuOff_75;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::AliasUIClickOn
	ControllerInteractionEventHandler_t343979916 * ___AliasUIClickOn_76;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::AliasUIClickOff
	ControllerInteractionEventHandler_t343979916 * ___AliasUIClickOff_77;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::ControllerEnabled
	ControllerInteractionEventHandler_t343979916 * ___ControllerEnabled_78;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::ControllerDisabled
	ControllerInteractionEventHandler_t343979916 * ___ControllerDisabled_79;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_ControllerEvents::ControllerIndexChanged
	ControllerInteractionEventHandler_t343979916 * ___ControllerIndexChanged_80;
	// UnityEngine.Vector2 VRTK.VRTK_ControllerEvents::touchpadAxis
	Vector2_t2243707579  ___touchpadAxis_81;
	// UnityEngine.Vector2 VRTK.VRTK_ControllerEvents::triggerAxis
	Vector2_t2243707579  ___triggerAxis_82;
	// UnityEngine.Vector2 VRTK.VRTK_ControllerEvents::gripAxis
	Vector2_t2243707579  ___gripAxis_83;
	// System.Single VRTK.VRTK_ControllerEvents::hairTriggerDelta
	float ___hairTriggerDelta_84;
	// System.Single VRTK.VRTK_ControllerEvents::hairGripDelta
	float ___hairGripDelta_85;

public:
	inline static int32_t get_offset_of_pointerToggleButton_2() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___pointerToggleButton_2)); }
	inline int32_t get_pointerToggleButton_2() const { return ___pointerToggleButton_2; }
	inline int32_t* get_address_of_pointerToggleButton_2() { return &___pointerToggleButton_2; }
	inline void set_pointerToggleButton_2(int32_t value)
	{
		___pointerToggleButton_2 = value;
	}

	inline static int32_t get_offset_of_pointerSetButton_3() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___pointerSetButton_3)); }
	inline int32_t get_pointerSetButton_3() const { return ___pointerSetButton_3; }
	inline int32_t* get_address_of_pointerSetButton_3() { return &___pointerSetButton_3; }
	inline void set_pointerSetButton_3(int32_t value)
	{
		___pointerSetButton_3 = value;
	}

	inline static int32_t get_offset_of_grabToggleButton_4() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___grabToggleButton_4)); }
	inline int32_t get_grabToggleButton_4() const { return ___grabToggleButton_4; }
	inline int32_t* get_address_of_grabToggleButton_4() { return &___grabToggleButton_4; }
	inline void set_grabToggleButton_4(int32_t value)
	{
		___grabToggleButton_4 = value;
	}

	inline static int32_t get_offset_of_useToggleButton_5() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___useToggleButton_5)); }
	inline int32_t get_useToggleButton_5() const { return ___useToggleButton_5; }
	inline int32_t* get_address_of_useToggleButton_5() { return &___useToggleButton_5; }
	inline void set_useToggleButton_5(int32_t value)
	{
		___useToggleButton_5 = value;
	}

	inline static int32_t get_offset_of_uiClickButton_6() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___uiClickButton_6)); }
	inline int32_t get_uiClickButton_6() const { return ___uiClickButton_6; }
	inline int32_t* get_address_of_uiClickButton_6() { return &___uiClickButton_6; }
	inline void set_uiClickButton_6(int32_t value)
	{
		___uiClickButton_6 = value;
	}

	inline static int32_t get_offset_of_menuToggleButton_7() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___menuToggleButton_7)); }
	inline int32_t get_menuToggleButton_7() const { return ___menuToggleButton_7; }
	inline int32_t* get_address_of_menuToggleButton_7() { return &___menuToggleButton_7; }
	inline void set_menuToggleButton_7(int32_t value)
	{
		___menuToggleButton_7 = value;
	}

	inline static int32_t get_offset_of_axisFidelity_8() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___axisFidelity_8)); }
	inline int32_t get_axisFidelity_8() const { return ___axisFidelity_8; }
	inline int32_t* get_address_of_axisFidelity_8() { return &___axisFidelity_8; }
	inline void set_axisFidelity_8(int32_t value)
	{
		___axisFidelity_8 = value;
	}

	inline static int32_t get_offset_of_triggerClickThreshold_9() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___triggerClickThreshold_9)); }
	inline float get_triggerClickThreshold_9() const { return ___triggerClickThreshold_9; }
	inline float* get_address_of_triggerClickThreshold_9() { return &___triggerClickThreshold_9; }
	inline void set_triggerClickThreshold_9(float value)
	{
		___triggerClickThreshold_9 = value;
	}

	inline static int32_t get_offset_of_gripClickThreshold_10() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___gripClickThreshold_10)); }
	inline float get_gripClickThreshold_10() const { return ___gripClickThreshold_10; }
	inline float* get_address_of_gripClickThreshold_10() { return &___gripClickThreshold_10; }
	inline void set_gripClickThreshold_10(float value)
	{
		___gripClickThreshold_10 = value;
	}

	inline static int32_t get_offset_of_triggerPressed_11() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___triggerPressed_11)); }
	inline bool get_triggerPressed_11() const { return ___triggerPressed_11; }
	inline bool* get_address_of_triggerPressed_11() { return &___triggerPressed_11; }
	inline void set_triggerPressed_11(bool value)
	{
		___triggerPressed_11 = value;
	}

	inline static int32_t get_offset_of_triggerTouched_12() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___triggerTouched_12)); }
	inline bool get_triggerTouched_12() const { return ___triggerTouched_12; }
	inline bool* get_address_of_triggerTouched_12() { return &___triggerTouched_12; }
	inline void set_triggerTouched_12(bool value)
	{
		___triggerTouched_12 = value;
	}

	inline static int32_t get_offset_of_triggerHairlinePressed_13() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___triggerHairlinePressed_13)); }
	inline bool get_triggerHairlinePressed_13() const { return ___triggerHairlinePressed_13; }
	inline bool* get_address_of_triggerHairlinePressed_13() { return &___triggerHairlinePressed_13; }
	inline void set_triggerHairlinePressed_13(bool value)
	{
		___triggerHairlinePressed_13 = value;
	}

	inline static int32_t get_offset_of_triggerClicked_14() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___triggerClicked_14)); }
	inline bool get_triggerClicked_14() const { return ___triggerClicked_14; }
	inline bool* get_address_of_triggerClicked_14() { return &___triggerClicked_14; }
	inline void set_triggerClicked_14(bool value)
	{
		___triggerClicked_14 = value;
	}

	inline static int32_t get_offset_of_triggerAxisChanged_15() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___triggerAxisChanged_15)); }
	inline bool get_triggerAxisChanged_15() const { return ___triggerAxisChanged_15; }
	inline bool* get_address_of_triggerAxisChanged_15() { return &___triggerAxisChanged_15; }
	inline void set_triggerAxisChanged_15(bool value)
	{
		___triggerAxisChanged_15 = value;
	}

	inline static int32_t get_offset_of_gripPressed_16() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___gripPressed_16)); }
	inline bool get_gripPressed_16() const { return ___gripPressed_16; }
	inline bool* get_address_of_gripPressed_16() { return &___gripPressed_16; }
	inline void set_gripPressed_16(bool value)
	{
		___gripPressed_16 = value;
	}

	inline static int32_t get_offset_of_gripTouched_17() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___gripTouched_17)); }
	inline bool get_gripTouched_17() const { return ___gripTouched_17; }
	inline bool* get_address_of_gripTouched_17() { return &___gripTouched_17; }
	inline void set_gripTouched_17(bool value)
	{
		___gripTouched_17 = value;
	}

	inline static int32_t get_offset_of_gripHairlinePressed_18() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___gripHairlinePressed_18)); }
	inline bool get_gripHairlinePressed_18() const { return ___gripHairlinePressed_18; }
	inline bool* get_address_of_gripHairlinePressed_18() { return &___gripHairlinePressed_18; }
	inline void set_gripHairlinePressed_18(bool value)
	{
		___gripHairlinePressed_18 = value;
	}

	inline static int32_t get_offset_of_gripClicked_19() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___gripClicked_19)); }
	inline bool get_gripClicked_19() const { return ___gripClicked_19; }
	inline bool* get_address_of_gripClicked_19() { return &___gripClicked_19; }
	inline void set_gripClicked_19(bool value)
	{
		___gripClicked_19 = value;
	}

	inline static int32_t get_offset_of_gripAxisChanged_20() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___gripAxisChanged_20)); }
	inline bool get_gripAxisChanged_20() const { return ___gripAxisChanged_20; }
	inline bool* get_address_of_gripAxisChanged_20() { return &___gripAxisChanged_20; }
	inline void set_gripAxisChanged_20(bool value)
	{
		___gripAxisChanged_20 = value;
	}

	inline static int32_t get_offset_of_touchpadPressed_21() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___touchpadPressed_21)); }
	inline bool get_touchpadPressed_21() const { return ___touchpadPressed_21; }
	inline bool* get_address_of_touchpadPressed_21() { return &___touchpadPressed_21; }
	inline void set_touchpadPressed_21(bool value)
	{
		___touchpadPressed_21 = value;
	}

	inline static int32_t get_offset_of_touchpadTouched_22() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___touchpadTouched_22)); }
	inline bool get_touchpadTouched_22() const { return ___touchpadTouched_22; }
	inline bool* get_address_of_touchpadTouched_22() { return &___touchpadTouched_22; }
	inline void set_touchpadTouched_22(bool value)
	{
		___touchpadTouched_22 = value;
	}

	inline static int32_t get_offset_of_touchpadAxisChanged_23() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___touchpadAxisChanged_23)); }
	inline bool get_touchpadAxisChanged_23() const { return ___touchpadAxisChanged_23; }
	inline bool* get_address_of_touchpadAxisChanged_23() { return &___touchpadAxisChanged_23; }
	inline void set_touchpadAxisChanged_23(bool value)
	{
		___touchpadAxisChanged_23 = value;
	}

	inline static int32_t get_offset_of_buttonOnePressed_24() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___buttonOnePressed_24)); }
	inline bool get_buttonOnePressed_24() const { return ___buttonOnePressed_24; }
	inline bool* get_address_of_buttonOnePressed_24() { return &___buttonOnePressed_24; }
	inline void set_buttonOnePressed_24(bool value)
	{
		___buttonOnePressed_24 = value;
	}

	inline static int32_t get_offset_of_buttonOneTouched_25() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___buttonOneTouched_25)); }
	inline bool get_buttonOneTouched_25() const { return ___buttonOneTouched_25; }
	inline bool* get_address_of_buttonOneTouched_25() { return &___buttonOneTouched_25; }
	inline void set_buttonOneTouched_25(bool value)
	{
		___buttonOneTouched_25 = value;
	}

	inline static int32_t get_offset_of_buttonTwoPressed_26() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___buttonTwoPressed_26)); }
	inline bool get_buttonTwoPressed_26() const { return ___buttonTwoPressed_26; }
	inline bool* get_address_of_buttonTwoPressed_26() { return &___buttonTwoPressed_26; }
	inline void set_buttonTwoPressed_26(bool value)
	{
		___buttonTwoPressed_26 = value;
	}

	inline static int32_t get_offset_of_buttonTwoTouched_27() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___buttonTwoTouched_27)); }
	inline bool get_buttonTwoTouched_27() const { return ___buttonTwoTouched_27; }
	inline bool* get_address_of_buttonTwoTouched_27() { return &___buttonTwoTouched_27; }
	inline void set_buttonTwoTouched_27(bool value)
	{
		___buttonTwoTouched_27 = value;
	}

	inline static int32_t get_offset_of_startMenuPressed_28() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___startMenuPressed_28)); }
	inline bool get_startMenuPressed_28() const { return ___startMenuPressed_28; }
	inline bool* get_address_of_startMenuPressed_28() { return &___startMenuPressed_28; }
	inline void set_startMenuPressed_28(bool value)
	{
		___startMenuPressed_28 = value;
	}

	inline static int32_t get_offset_of_pointerPressed_29() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___pointerPressed_29)); }
	inline bool get_pointerPressed_29() const { return ___pointerPressed_29; }
	inline bool* get_address_of_pointerPressed_29() { return &___pointerPressed_29; }
	inline void set_pointerPressed_29(bool value)
	{
		___pointerPressed_29 = value;
	}

	inline static int32_t get_offset_of_grabPressed_30() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___grabPressed_30)); }
	inline bool get_grabPressed_30() const { return ___grabPressed_30; }
	inline bool* get_address_of_grabPressed_30() { return &___grabPressed_30; }
	inline void set_grabPressed_30(bool value)
	{
		___grabPressed_30 = value;
	}

	inline static int32_t get_offset_of_usePressed_31() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___usePressed_31)); }
	inline bool get_usePressed_31() const { return ___usePressed_31; }
	inline bool* get_address_of_usePressed_31() { return &___usePressed_31; }
	inline void set_usePressed_31(bool value)
	{
		___usePressed_31 = value;
	}

	inline static int32_t get_offset_of_uiClickPressed_32() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___uiClickPressed_32)); }
	inline bool get_uiClickPressed_32() const { return ___uiClickPressed_32; }
	inline bool* get_address_of_uiClickPressed_32() { return &___uiClickPressed_32; }
	inline void set_uiClickPressed_32(bool value)
	{
		___uiClickPressed_32 = value;
	}

	inline static int32_t get_offset_of_menuPressed_33() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___menuPressed_33)); }
	inline bool get_menuPressed_33() const { return ___menuPressed_33; }
	inline bool* get_address_of_menuPressed_33() { return &___menuPressed_33; }
	inline void set_menuPressed_33(bool value)
	{
		___menuPressed_33 = value;
	}

	inline static int32_t get_offset_of_TriggerPressed_34() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___TriggerPressed_34)); }
	inline ControllerInteractionEventHandler_t343979916 * get_TriggerPressed_34() const { return ___TriggerPressed_34; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_TriggerPressed_34() { return &___TriggerPressed_34; }
	inline void set_TriggerPressed_34(ControllerInteractionEventHandler_t343979916 * value)
	{
		___TriggerPressed_34 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerPressed_34, value);
	}

	inline static int32_t get_offset_of_TriggerReleased_35() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___TriggerReleased_35)); }
	inline ControllerInteractionEventHandler_t343979916 * get_TriggerReleased_35() const { return ___TriggerReleased_35; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_TriggerReleased_35() { return &___TriggerReleased_35; }
	inline void set_TriggerReleased_35(ControllerInteractionEventHandler_t343979916 * value)
	{
		___TriggerReleased_35 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerReleased_35, value);
	}

	inline static int32_t get_offset_of_TriggerTouchStart_36() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___TriggerTouchStart_36)); }
	inline ControllerInteractionEventHandler_t343979916 * get_TriggerTouchStart_36() const { return ___TriggerTouchStart_36; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_TriggerTouchStart_36() { return &___TriggerTouchStart_36; }
	inline void set_TriggerTouchStart_36(ControllerInteractionEventHandler_t343979916 * value)
	{
		___TriggerTouchStart_36 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerTouchStart_36, value);
	}

	inline static int32_t get_offset_of_TriggerTouchEnd_37() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___TriggerTouchEnd_37)); }
	inline ControllerInteractionEventHandler_t343979916 * get_TriggerTouchEnd_37() const { return ___TriggerTouchEnd_37; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_TriggerTouchEnd_37() { return &___TriggerTouchEnd_37; }
	inline void set_TriggerTouchEnd_37(ControllerInteractionEventHandler_t343979916 * value)
	{
		___TriggerTouchEnd_37 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerTouchEnd_37, value);
	}

	inline static int32_t get_offset_of_TriggerHairlineStart_38() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___TriggerHairlineStart_38)); }
	inline ControllerInteractionEventHandler_t343979916 * get_TriggerHairlineStart_38() const { return ___TriggerHairlineStart_38; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_TriggerHairlineStart_38() { return &___TriggerHairlineStart_38; }
	inline void set_TriggerHairlineStart_38(ControllerInteractionEventHandler_t343979916 * value)
	{
		___TriggerHairlineStart_38 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerHairlineStart_38, value);
	}

	inline static int32_t get_offset_of_TriggerHairlineEnd_39() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___TriggerHairlineEnd_39)); }
	inline ControllerInteractionEventHandler_t343979916 * get_TriggerHairlineEnd_39() const { return ___TriggerHairlineEnd_39; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_TriggerHairlineEnd_39() { return &___TriggerHairlineEnd_39; }
	inline void set_TriggerHairlineEnd_39(ControllerInteractionEventHandler_t343979916 * value)
	{
		___TriggerHairlineEnd_39 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerHairlineEnd_39, value);
	}

	inline static int32_t get_offset_of_TriggerClicked_40() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___TriggerClicked_40)); }
	inline ControllerInteractionEventHandler_t343979916 * get_TriggerClicked_40() const { return ___TriggerClicked_40; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_TriggerClicked_40() { return &___TriggerClicked_40; }
	inline void set_TriggerClicked_40(ControllerInteractionEventHandler_t343979916 * value)
	{
		___TriggerClicked_40 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerClicked_40, value);
	}

	inline static int32_t get_offset_of_TriggerUnclicked_41() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___TriggerUnclicked_41)); }
	inline ControllerInteractionEventHandler_t343979916 * get_TriggerUnclicked_41() const { return ___TriggerUnclicked_41; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_TriggerUnclicked_41() { return &___TriggerUnclicked_41; }
	inline void set_TriggerUnclicked_41(ControllerInteractionEventHandler_t343979916 * value)
	{
		___TriggerUnclicked_41 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerUnclicked_41, value);
	}

	inline static int32_t get_offset_of_TriggerAxisChanged_42() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___TriggerAxisChanged_42)); }
	inline ControllerInteractionEventHandler_t343979916 * get_TriggerAxisChanged_42() const { return ___TriggerAxisChanged_42; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_TriggerAxisChanged_42() { return &___TriggerAxisChanged_42; }
	inline void set_TriggerAxisChanged_42(ControllerInteractionEventHandler_t343979916 * value)
	{
		___TriggerAxisChanged_42 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerAxisChanged_42, value);
	}

	inline static int32_t get_offset_of_GripPressed_43() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___GripPressed_43)); }
	inline ControllerInteractionEventHandler_t343979916 * get_GripPressed_43() const { return ___GripPressed_43; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_GripPressed_43() { return &___GripPressed_43; }
	inline void set_GripPressed_43(ControllerInteractionEventHandler_t343979916 * value)
	{
		___GripPressed_43 = value;
		Il2CppCodeGenWriteBarrier(&___GripPressed_43, value);
	}

	inline static int32_t get_offset_of_GripReleased_44() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___GripReleased_44)); }
	inline ControllerInteractionEventHandler_t343979916 * get_GripReleased_44() const { return ___GripReleased_44; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_GripReleased_44() { return &___GripReleased_44; }
	inline void set_GripReleased_44(ControllerInteractionEventHandler_t343979916 * value)
	{
		___GripReleased_44 = value;
		Il2CppCodeGenWriteBarrier(&___GripReleased_44, value);
	}

	inline static int32_t get_offset_of_GripTouchStart_45() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___GripTouchStart_45)); }
	inline ControllerInteractionEventHandler_t343979916 * get_GripTouchStart_45() const { return ___GripTouchStart_45; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_GripTouchStart_45() { return &___GripTouchStart_45; }
	inline void set_GripTouchStart_45(ControllerInteractionEventHandler_t343979916 * value)
	{
		___GripTouchStart_45 = value;
		Il2CppCodeGenWriteBarrier(&___GripTouchStart_45, value);
	}

	inline static int32_t get_offset_of_GripTouchEnd_46() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___GripTouchEnd_46)); }
	inline ControllerInteractionEventHandler_t343979916 * get_GripTouchEnd_46() const { return ___GripTouchEnd_46; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_GripTouchEnd_46() { return &___GripTouchEnd_46; }
	inline void set_GripTouchEnd_46(ControllerInteractionEventHandler_t343979916 * value)
	{
		___GripTouchEnd_46 = value;
		Il2CppCodeGenWriteBarrier(&___GripTouchEnd_46, value);
	}

	inline static int32_t get_offset_of_GripHairlineStart_47() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___GripHairlineStart_47)); }
	inline ControllerInteractionEventHandler_t343979916 * get_GripHairlineStart_47() const { return ___GripHairlineStart_47; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_GripHairlineStart_47() { return &___GripHairlineStart_47; }
	inline void set_GripHairlineStart_47(ControllerInteractionEventHandler_t343979916 * value)
	{
		___GripHairlineStart_47 = value;
		Il2CppCodeGenWriteBarrier(&___GripHairlineStart_47, value);
	}

	inline static int32_t get_offset_of_GripHairlineEnd_48() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___GripHairlineEnd_48)); }
	inline ControllerInteractionEventHandler_t343979916 * get_GripHairlineEnd_48() const { return ___GripHairlineEnd_48; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_GripHairlineEnd_48() { return &___GripHairlineEnd_48; }
	inline void set_GripHairlineEnd_48(ControllerInteractionEventHandler_t343979916 * value)
	{
		___GripHairlineEnd_48 = value;
		Il2CppCodeGenWriteBarrier(&___GripHairlineEnd_48, value);
	}

	inline static int32_t get_offset_of_GripClicked_49() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___GripClicked_49)); }
	inline ControllerInteractionEventHandler_t343979916 * get_GripClicked_49() const { return ___GripClicked_49; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_GripClicked_49() { return &___GripClicked_49; }
	inline void set_GripClicked_49(ControllerInteractionEventHandler_t343979916 * value)
	{
		___GripClicked_49 = value;
		Il2CppCodeGenWriteBarrier(&___GripClicked_49, value);
	}

	inline static int32_t get_offset_of_GripUnclicked_50() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___GripUnclicked_50)); }
	inline ControllerInteractionEventHandler_t343979916 * get_GripUnclicked_50() const { return ___GripUnclicked_50; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_GripUnclicked_50() { return &___GripUnclicked_50; }
	inline void set_GripUnclicked_50(ControllerInteractionEventHandler_t343979916 * value)
	{
		___GripUnclicked_50 = value;
		Il2CppCodeGenWriteBarrier(&___GripUnclicked_50, value);
	}

	inline static int32_t get_offset_of_GripAxisChanged_51() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___GripAxisChanged_51)); }
	inline ControllerInteractionEventHandler_t343979916 * get_GripAxisChanged_51() const { return ___GripAxisChanged_51; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_GripAxisChanged_51() { return &___GripAxisChanged_51; }
	inline void set_GripAxisChanged_51(ControllerInteractionEventHandler_t343979916 * value)
	{
		___GripAxisChanged_51 = value;
		Il2CppCodeGenWriteBarrier(&___GripAxisChanged_51, value);
	}

	inline static int32_t get_offset_of_TouchpadPressed_52() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___TouchpadPressed_52)); }
	inline ControllerInteractionEventHandler_t343979916 * get_TouchpadPressed_52() const { return ___TouchpadPressed_52; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_TouchpadPressed_52() { return &___TouchpadPressed_52; }
	inline void set_TouchpadPressed_52(ControllerInteractionEventHandler_t343979916 * value)
	{
		___TouchpadPressed_52 = value;
		Il2CppCodeGenWriteBarrier(&___TouchpadPressed_52, value);
	}

	inline static int32_t get_offset_of_TouchpadReleased_53() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___TouchpadReleased_53)); }
	inline ControllerInteractionEventHandler_t343979916 * get_TouchpadReleased_53() const { return ___TouchpadReleased_53; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_TouchpadReleased_53() { return &___TouchpadReleased_53; }
	inline void set_TouchpadReleased_53(ControllerInteractionEventHandler_t343979916 * value)
	{
		___TouchpadReleased_53 = value;
		Il2CppCodeGenWriteBarrier(&___TouchpadReleased_53, value);
	}

	inline static int32_t get_offset_of_TouchpadTouchStart_54() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___TouchpadTouchStart_54)); }
	inline ControllerInteractionEventHandler_t343979916 * get_TouchpadTouchStart_54() const { return ___TouchpadTouchStart_54; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_TouchpadTouchStart_54() { return &___TouchpadTouchStart_54; }
	inline void set_TouchpadTouchStart_54(ControllerInteractionEventHandler_t343979916 * value)
	{
		___TouchpadTouchStart_54 = value;
		Il2CppCodeGenWriteBarrier(&___TouchpadTouchStart_54, value);
	}

	inline static int32_t get_offset_of_TouchpadTouchEnd_55() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___TouchpadTouchEnd_55)); }
	inline ControllerInteractionEventHandler_t343979916 * get_TouchpadTouchEnd_55() const { return ___TouchpadTouchEnd_55; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_TouchpadTouchEnd_55() { return &___TouchpadTouchEnd_55; }
	inline void set_TouchpadTouchEnd_55(ControllerInteractionEventHandler_t343979916 * value)
	{
		___TouchpadTouchEnd_55 = value;
		Il2CppCodeGenWriteBarrier(&___TouchpadTouchEnd_55, value);
	}

	inline static int32_t get_offset_of_TouchpadAxisChanged_56() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___TouchpadAxisChanged_56)); }
	inline ControllerInteractionEventHandler_t343979916 * get_TouchpadAxisChanged_56() const { return ___TouchpadAxisChanged_56; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_TouchpadAxisChanged_56() { return &___TouchpadAxisChanged_56; }
	inline void set_TouchpadAxisChanged_56(ControllerInteractionEventHandler_t343979916 * value)
	{
		___TouchpadAxisChanged_56 = value;
		Il2CppCodeGenWriteBarrier(&___TouchpadAxisChanged_56, value);
	}

	inline static int32_t get_offset_of_ButtonOneTouchStart_57() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___ButtonOneTouchStart_57)); }
	inline ControllerInteractionEventHandler_t343979916 * get_ButtonOneTouchStart_57() const { return ___ButtonOneTouchStart_57; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_ButtonOneTouchStart_57() { return &___ButtonOneTouchStart_57; }
	inline void set_ButtonOneTouchStart_57(ControllerInteractionEventHandler_t343979916 * value)
	{
		___ButtonOneTouchStart_57 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonOneTouchStart_57, value);
	}

	inline static int32_t get_offset_of_ButtonOneTouchEnd_58() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___ButtonOneTouchEnd_58)); }
	inline ControllerInteractionEventHandler_t343979916 * get_ButtonOneTouchEnd_58() const { return ___ButtonOneTouchEnd_58; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_ButtonOneTouchEnd_58() { return &___ButtonOneTouchEnd_58; }
	inline void set_ButtonOneTouchEnd_58(ControllerInteractionEventHandler_t343979916 * value)
	{
		___ButtonOneTouchEnd_58 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonOneTouchEnd_58, value);
	}

	inline static int32_t get_offset_of_ButtonOnePressed_59() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___ButtonOnePressed_59)); }
	inline ControllerInteractionEventHandler_t343979916 * get_ButtonOnePressed_59() const { return ___ButtonOnePressed_59; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_ButtonOnePressed_59() { return &___ButtonOnePressed_59; }
	inline void set_ButtonOnePressed_59(ControllerInteractionEventHandler_t343979916 * value)
	{
		___ButtonOnePressed_59 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonOnePressed_59, value);
	}

	inline static int32_t get_offset_of_ButtonOneReleased_60() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___ButtonOneReleased_60)); }
	inline ControllerInteractionEventHandler_t343979916 * get_ButtonOneReleased_60() const { return ___ButtonOneReleased_60; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_ButtonOneReleased_60() { return &___ButtonOneReleased_60; }
	inline void set_ButtonOneReleased_60(ControllerInteractionEventHandler_t343979916 * value)
	{
		___ButtonOneReleased_60 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonOneReleased_60, value);
	}

	inline static int32_t get_offset_of_ButtonTwoTouchStart_61() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___ButtonTwoTouchStart_61)); }
	inline ControllerInteractionEventHandler_t343979916 * get_ButtonTwoTouchStart_61() const { return ___ButtonTwoTouchStart_61; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_ButtonTwoTouchStart_61() { return &___ButtonTwoTouchStart_61; }
	inline void set_ButtonTwoTouchStart_61(ControllerInteractionEventHandler_t343979916 * value)
	{
		___ButtonTwoTouchStart_61 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonTwoTouchStart_61, value);
	}

	inline static int32_t get_offset_of_ButtonTwoTouchEnd_62() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___ButtonTwoTouchEnd_62)); }
	inline ControllerInteractionEventHandler_t343979916 * get_ButtonTwoTouchEnd_62() const { return ___ButtonTwoTouchEnd_62; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_ButtonTwoTouchEnd_62() { return &___ButtonTwoTouchEnd_62; }
	inline void set_ButtonTwoTouchEnd_62(ControllerInteractionEventHandler_t343979916 * value)
	{
		___ButtonTwoTouchEnd_62 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonTwoTouchEnd_62, value);
	}

	inline static int32_t get_offset_of_ButtonTwoPressed_63() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___ButtonTwoPressed_63)); }
	inline ControllerInteractionEventHandler_t343979916 * get_ButtonTwoPressed_63() const { return ___ButtonTwoPressed_63; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_ButtonTwoPressed_63() { return &___ButtonTwoPressed_63; }
	inline void set_ButtonTwoPressed_63(ControllerInteractionEventHandler_t343979916 * value)
	{
		___ButtonTwoPressed_63 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonTwoPressed_63, value);
	}

	inline static int32_t get_offset_of_ButtonTwoReleased_64() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___ButtonTwoReleased_64)); }
	inline ControllerInteractionEventHandler_t343979916 * get_ButtonTwoReleased_64() const { return ___ButtonTwoReleased_64; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_ButtonTwoReleased_64() { return &___ButtonTwoReleased_64; }
	inline void set_ButtonTwoReleased_64(ControllerInteractionEventHandler_t343979916 * value)
	{
		___ButtonTwoReleased_64 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonTwoReleased_64, value);
	}

	inline static int32_t get_offset_of_StartMenuPressed_65() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___StartMenuPressed_65)); }
	inline ControllerInteractionEventHandler_t343979916 * get_StartMenuPressed_65() const { return ___StartMenuPressed_65; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_StartMenuPressed_65() { return &___StartMenuPressed_65; }
	inline void set_StartMenuPressed_65(ControllerInteractionEventHandler_t343979916 * value)
	{
		___StartMenuPressed_65 = value;
		Il2CppCodeGenWriteBarrier(&___StartMenuPressed_65, value);
	}

	inline static int32_t get_offset_of_StartMenuReleased_66() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___StartMenuReleased_66)); }
	inline ControllerInteractionEventHandler_t343979916 * get_StartMenuReleased_66() const { return ___StartMenuReleased_66; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_StartMenuReleased_66() { return &___StartMenuReleased_66; }
	inline void set_StartMenuReleased_66(ControllerInteractionEventHandler_t343979916 * value)
	{
		___StartMenuReleased_66 = value;
		Il2CppCodeGenWriteBarrier(&___StartMenuReleased_66, value);
	}

	inline static int32_t get_offset_of_AliasPointerOn_67() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___AliasPointerOn_67)); }
	inline ControllerInteractionEventHandler_t343979916 * get_AliasPointerOn_67() const { return ___AliasPointerOn_67; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_AliasPointerOn_67() { return &___AliasPointerOn_67; }
	inline void set_AliasPointerOn_67(ControllerInteractionEventHandler_t343979916 * value)
	{
		___AliasPointerOn_67 = value;
		Il2CppCodeGenWriteBarrier(&___AliasPointerOn_67, value);
	}

	inline static int32_t get_offset_of_AliasPointerOff_68() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___AliasPointerOff_68)); }
	inline ControllerInteractionEventHandler_t343979916 * get_AliasPointerOff_68() const { return ___AliasPointerOff_68; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_AliasPointerOff_68() { return &___AliasPointerOff_68; }
	inline void set_AliasPointerOff_68(ControllerInteractionEventHandler_t343979916 * value)
	{
		___AliasPointerOff_68 = value;
		Il2CppCodeGenWriteBarrier(&___AliasPointerOff_68, value);
	}

	inline static int32_t get_offset_of_AliasPointerSet_69() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___AliasPointerSet_69)); }
	inline ControllerInteractionEventHandler_t343979916 * get_AliasPointerSet_69() const { return ___AliasPointerSet_69; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_AliasPointerSet_69() { return &___AliasPointerSet_69; }
	inline void set_AliasPointerSet_69(ControllerInteractionEventHandler_t343979916 * value)
	{
		___AliasPointerSet_69 = value;
		Il2CppCodeGenWriteBarrier(&___AliasPointerSet_69, value);
	}

	inline static int32_t get_offset_of_AliasGrabOn_70() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___AliasGrabOn_70)); }
	inline ControllerInteractionEventHandler_t343979916 * get_AliasGrabOn_70() const { return ___AliasGrabOn_70; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_AliasGrabOn_70() { return &___AliasGrabOn_70; }
	inline void set_AliasGrabOn_70(ControllerInteractionEventHandler_t343979916 * value)
	{
		___AliasGrabOn_70 = value;
		Il2CppCodeGenWriteBarrier(&___AliasGrabOn_70, value);
	}

	inline static int32_t get_offset_of_AliasGrabOff_71() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___AliasGrabOff_71)); }
	inline ControllerInteractionEventHandler_t343979916 * get_AliasGrabOff_71() const { return ___AliasGrabOff_71; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_AliasGrabOff_71() { return &___AliasGrabOff_71; }
	inline void set_AliasGrabOff_71(ControllerInteractionEventHandler_t343979916 * value)
	{
		___AliasGrabOff_71 = value;
		Il2CppCodeGenWriteBarrier(&___AliasGrabOff_71, value);
	}

	inline static int32_t get_offset_of_AliasUseOn_72() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___AliasUseOn_72)); }
	inline ControllerInteractionEventHandler_t343979916 * get_AliasUseOn_72() const { return ___AliasUseOn_72; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_AliasUseOn_72() { return &___AliasUseOn_72; }
	inline void set_AliasUseOn_72(ControllerInteractionEventHandler_t343979916 * value)
	{
		___AliasUseOn_72 = value;
		Il2CppCodeGenWriteBarrier(&___AliasUseOn_72, value);
	}

	inline static int32_t get_offset_of_AliasUseOff_73() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___AliasUseOff_73)); }
	inline ControllerInteractionEventHandler_t343979916 * get_AliasUseOff_73() const { return ___AliasUseOff_73; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_AliasUseOff_73() { return &___AliasUseOff_73; }
	inline void set_AliasUseOff_73(ControllerInteractionEventHandler_t343979916 * value)
	{
		___AliasUseOff_73 = value;
		Il2CppCodeGenWriteBarrier(&___AliasUseOff_73, value);
	}

	inline static int32_t get_offset_of_AliasMenuOn_74() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___AliasMenuOn_74)); }
	inline ControllerInteractionEventHandler_t343979916 * get_AliasMenuOn_74() const { return ___AliasMenuOn_74; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_AliasMenuOn_74() { return &___AliasMenuOn_74; }
	inline void set_AliasMenuOn_74(ControllerInteractionEventHandler_t343979916 * value)
	{
		___AliasMenuOn_74 = value;
		Il2CppCodeGenWriteBarrier(&___AliasMenuOn_74, value);
	}

	inline static int32_t get_offset_of_AliasMenuOff_75() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___AliasMenuOff_75)); }
	inline ControllerInteractionEventHandler_t343979916 * get_AliasMenuOff_75() const { return ___AliasMenuOff_75; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_AliasMenuOff_75() { return &___AliasMenuOff_75; }
	inline void set_AliasMenuOff_75(ControllerInteractionEventHandler_t343979916 * value)
	{
		___AliasMenuOff_75 = value;
		Il2CppCodeGenWriteBarrier(&___AliasMenuOff_75, value);
	}

	inline static int32_t get_offset_of_AliasUIClickOn_76() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___AliasUIClickOn_76)); }
	inline ControllerInteractionEventHandler_t343979916 * get_AliasUIClickOn_76() const { return ___AliasUIClickOn_76; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_AliasUIClickOn_76() { return &___AliasUIClickOn_76; }
	inline void set_AliasUIClickOn_76(ControllerInteractionEventHandler_t343979916 * value)
	{
		___AliasUIClickOn_76 = value;
		Il2CppCodeGenWriteBarrier(&___AliasUIClickOn_76, value);
	}

	inline static int32_t get_offset_of_AliasUIClickOff_77() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___AliasUIClickOff_77)); }
	inline ControllerInteractionEventHandler_t343979916 * get_AliasUIClickOff_77() const { return ___AliasUIClickOff_77; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_AliasUIClickOff_77() { return &___AliasUIClickOff_77; }
	inline void set_AliasUIClickOff_77(ControllerInteractionEventHandler_t343979916 * value)
	{
		___AliasUIClickOff_77 = value;
		Il2CppCodeGenWriteBarrier(&___AliasUIClickOff_77, value);
	}

	inline static int32_t get_offset_of_ControllerEnabled_78() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___ControllerEnabled_78)); }
	inline ControllerInteractionEventHandler_t343979916 * get_ControllerEnabled_78() const { return ___ControllerEnabled_78; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_ControllerEnabled_78() { return &___ControllerEnabled_78; }
	inline void set_ControllerEnabled_78(ControllerInteractionEventHandler_t343979916 * value)
	{
		___ControllerEnabled_78 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerEnabled_78, value);
	}

	inline static int32_t get_offset_of_ControllerDisabled_79() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___ControllerDisabled_79)); }
	inline ControllerInteractionEventHandler_t343979916 * get_ControllerDisabled_79() const { return ___ControllerDisabled_79; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_ControllerDisabled_79() { return &___ControllerDisabled_79; }
	inline void set_ControllerDisabled_79(ControllerInteractionEventHandler_t343979916 * value)
	{
		___ControllerDisabled_79 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerDisabled_79, value);
	}

	inline static int32_t get_offset_of_ControllerIndexChanged_80() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___ControllerIndexChanged_80)); }
	inline ControllerInteractionEventHandler_t343979916 * get_ControllerIndexChanged_80() const { return ___ControllerIndexChanged_80; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_ControllerIndexChanged_80() { return &___ControllerIndexChanged_80; }
	inline void set_ControllerIndexChanged_80(ControllerInteractionEventHandler_t343979916 * value)
	{
		___ControllerIndexChanged_80 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerIndexChanged_80, value);
	}

	inline static int32_t get_offset_of_touchpadAxis_81() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___touchpadAxis_81)); }
	inline Vector2_t2243707579  get_touchpadAxis_81() const { return ___touchpadAxis_81; }
	inline Vector2_t2243707579 * get_address_of_touchpadAxis_81() { return &___touchpadAxis_81; }
	inline void set_touchpadAxis_81(Vector2_t2243707579  value)
	{
		___touchpadAxis_81 = value;
	}

	inline static int32_t get_offset_of_triggerAxis_82() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___triggerAxis_82)); }
	inline Vector2_t2243707579  get_triggerAxis_82() const { return ___triggerAxis_82; }
	inline Vector2_t2243707579 * get_address_of_triggerAxis_82() { return &___triggerAxis_82; }
	inline void set_triggerAxis_82(Vector2_t2243707579  value)
	{
		___triggerAxis_82 = value;
	}

	inline static int32_t get_offset_of_gripAxis_83() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___gripAxis_83)); }
	inline Vector2_t2243707579  get_gripAxis_83() const { return ___gripAxis_83; }
	inline Vector2_t2243707579 * get_address_of_gripAxis_83() { return &___gripAxis_83; }
	inline void set_gripAxis_83(Vector2_t2243707579  value)
	{
		___gripAxis_83 = value;
	}

	inline static int32_t get_offset_of_hairTriggerDelta_84() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___hairTriggerDelta_84)); }
	inline float get_hairTriggerDelta_84() const { return ___hairTriggerDelta_84; }
	inline float* get_address_of_hairTriggerDelta_84() { return &___hairTriggerDelta_84; }
	inline void set_hairTriggerDelta_84(float value)
	{
		___hairTriggerDelta_84 = value;
	}

	inline static int32_t get_offset_of_hairGripDelta_85() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_t3225224819, ___hairGripDelta_85)); }
	inline float get_hairGripDelta_85() const { return ___hairGripDelta_85; }
	inline float* get_address_of_hairGripDelta_85() { return &___hairGripDelta_85; }
	inline void set_hairGripDelta_85(float value)
	{
		___hairGripDelta_85 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
