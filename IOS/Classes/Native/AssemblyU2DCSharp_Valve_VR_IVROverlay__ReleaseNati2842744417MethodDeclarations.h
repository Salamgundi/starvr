﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_ReleaseNativeOverlayHandle
struct _ReleaseNativeOverlayHandle_t2842744417;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_ReleaseNativeOverlayHandle::.ctor(System.Object,System.IntPtr)
extern "C"  void _ReleaseNativeOverlayHandle__ctor_m1018726112 (_ReleaseNativeOverlayHandle_t2842744417 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_ReleaseNativeOverlayHandle::Invoke(System.UInt64,System.IntPtr)
extern "C"  int32_t _ReleaseNativeOverlayHandle_Invoke_m2634363497 (_ReleaseNativeOverlayHandle_t2842744417 * __this, uint64_t ___ulOverlayHandle0, IntPtr_t ___pNativeTextureHandle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_ReleaseNativeOverlayHandle::BeginInvoke(System.UInt64,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ReleaseNativeOverlayHandle_BeginInvoke_m2494333002 (_ReleaseNativeOverlayHandle_t2842744417 * __this, uint64_t ___ulOverlayHandle0, IntPtr_t ___pNativeTextureHandle1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_ReleaseNativeOverlayHandle::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _ReleaseNativeOverlayHandle_EndInvoke_m2882305768 (_ReleaseNativeOverlayHandle_t2842744417 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
