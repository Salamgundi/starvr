﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Action`3<UnityEngine.Color,System.Single,System.Boolean>
struct Action_3_t557175684;
// SteamVR_Events/Event`3<UnityEngine.Color,System.Single,System.Boolean>
struct Event_3_t29453044;
// UnityEngine.Events.UnityAction`3<UnityEngine.Color,System.Single,System.Boolean>
struct UnityAction_3_t1816056522;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Events/Action`3<UnityEngine.Color,System.Single,System.Boolean>::.ctor(SteamVR_Events/Event`3<T0,T1,T2>,UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void Action_3__ctor_m1084438551_gshared (Action_3_t557175684 * __this, Event_3_t29453044 * ____event0, UnityAction_3_t1816056522 * ___action1, const MethodInfo* method);
#define Action_3__ctor_m1084438551(__this, ____event0, ___action1, method) ((  void (*) (Action_3_t557175684 *, Event_3_t29453044 *, UnityAction_3_t1816056522 *, const MethodInfo*))Action_3__ctor_m1084438551_gshared)(__this, ____event0, ___action1, method)
// System.Void SteamVR_Events/Action`3<UnityEngine.Color,System.Single,System.Boolean>::Enable(System.Boolean)
extern "C"  void Action_3_Enable_m2748940831_gshared (Action_3_t557175684 * __this, bool ___enabled0, const MethodInfo* method);
#define Action_3_Enable_m2748940831(__this, ___enabled0, method) ((  void (*) (Action_3_t557175684 *, bool, const MethodInfo*))Action_3_Enable_m2748940831_gshared)(__this, ___enabled0, method)
