﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Action`2<System.Int32,System.Boolean>
struct Action_2_t2270948822;
// SteamVR_Events/Event`2<System.Int32,System.Boolean>
struct Event_2_t3885983284;
// UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>
struct UnityAction_2_t41828916;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Events/Action`2<System.Int32,System.Boolean>::.ctor(SteamVR_Events/Event`2<T0,T1>,UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Action_2__ctor_m3584128380_gshared (Action_2_t2270948822 * __this, Event_2_t3885983284 * ____event0, UnityAction_2_t41828916 * ___action1, const MethodInfo* method);
#define Action_2__ctor_m3584128380(__this, ____event0, ___action1, method) ((  void (*) (Action_2_t2270948822 *, Event_2_t3885983284 *, UnityAction_2_t41828916 *, const MethodInfo*))Action_2__ctor_m3584128380_gshared)(__this, ____event0, ___action1, method)
// System.Void SteamVR_Events/Action`2<System.Int32,System.Boolean>::Enable(System.Boolean)
extern "C"  void Action_2_Enable_m3258315074_gshared (Action_2_t2270948822 * __this, bool ___enabled0, const MethodInfo* method);
#define Action_2_Enable_m3258315074(__this, ___enabled0, method) ((  void (*) (Action_2_t2270948822 *, bool, const MethodInfo*))Action_2_Enable_m3258315074_gshared)(__this, ___enabled0, method)
