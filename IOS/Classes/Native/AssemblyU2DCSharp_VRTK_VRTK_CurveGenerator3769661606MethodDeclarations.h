﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_CurveGenerator
struct VRTK_CurveGenerator_t3769661606;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Material
struct Material_t193706927;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_CurveGenerator::.ctor()
extern "C"  void VRTK_CurveGenerator__ctor_m3698189496 (VRTK_CurveGenerator_t3769661606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_CurveGenerator::Create(System.Int32,System.Single,UnityEngine.GameObject,System.Boolean)
extern "C"  void VRTK_CurveGenerator_Create_m3561186993 (VRTK_CurveGenerator_t3769661606 * __this, int32_t ___setFrequency0, float ___radius1, GameObject_t1756533147 * ___tracer2, bool ___rescaleTracer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_CurveGenerator::SetPoints(UnityEngine.Vector3[],UnityEngine.Material,UnityEngine.Color)
extern "C"  void VRTK_CurveGenerator_SetPoints_m305427422 (VRTK_CurveGenerator_t3769661606 * __this, Vector3U5BU5D_t1172311765* ___controlPoints0, Material_t193706927 * ___material1, Color_t2020392075  ___color2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] VRTK.VRTK_CurveGenerator::GetPoints(UnityEngine.Vector3[])
extern "C"  Vector3U5BU5D_t1172311765* VRTK_CurveGenerator_GetPoints_m2928089726 (VRTK_CurveGenerator_t3769661606 * __this, Vector3U5BU5D_t1172311765* ___controlPoints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_CurveGenerator::TogglePoints(System.Boolean)
extern "C"  void VRTK_CurveGenerator_TogglePoints_m1645020436 (VRTK_CurveGenerator_t3769661606 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_CurveGenerator::PointsInit(UnityEngine.Vector3[])
extern "C"  void VRTK_CurveGenerator_PointsInit_m4007121008 (VRTK_CurveGenerator_t3769661606 * __this, Vector3U5BU5D_t1172311765* ___controlPoints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_CurveGenerator::CreateSphere()
extern "C"  GameObject_t1756533147 * VRTK_CurveGenerator_CreateSphere_m439241262 (VRTK_CurveGenerator_t3769661606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_CurveGenerator::get_Loop()
extern "C"  bool VRTK_CurveGenerator_get_Loop_m1122861879 (VRTK_CurveGenerator_t3769661606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_CurveGenerator::set_Loop(System.Boolean)
extern "C"  void VRTK_CurveGenerator_set_Loop_m4105822268 (VRTK_CurveGenerator_t3769661606 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VRTK.VRTK_CurveGenerator::get_ControlPointCount()
extern "C"  int32_t VRTK_CurveGenerator_get_ControlPointCount_m1843893945 (VRTK_CurveGenerator_t3769661606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_CurveGenerator::GetControlPoint(System.Int32)
extern "C"  Vector3_t2243707580  VRTK_CurveGenerator_GetControlPoint_m1540102946 (VRTK_CurveGenerator_t3769661606 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_CurveGenerator::SetControlPoint(System.Int32,UnityEngine.Vector3)
extern "C"  void VRTK_CurveGenerator_SetControlPoint_m1317693043 (VRTK_CurveGenerator_t3769661606 * __this, int32_t ___index0, Vector3_t2243707580  ___point1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_CurveGenerator::EnforceMode(System.Int32)
extern "C"  void VRTK_CurveGenerator_EnforceMode_m343225600 (VRTK_CurveGenerator_t3769661606 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VRTK.VRTK_CurveGenerator::get_CurveCount()
extern "C"  int32_t VRTK_CurveGenerator_get_CurveCount_m2128250221 (VRTK_CurveGenerator_t3769661606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_CurveGenerator::GetPoint(System.Single)
extern "C"  Vector3_t2243707580  VRTK_CurveGenerator_GetPoint_m1139332099 (VRTK_CurveGenerator_t3769661606 * __this, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_CurveGenerator::SetObjects(UnityEngine.Material,UnityEngine.Color)
extern "C"  void VRTK_CurveGenerator_SetObjects_m2786143194 (VRTK_CurveGenerator_t3769661606 * __this, Material_t193706927 * ___material0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_CurveGenerator::setMaterial(UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Color)
extern "C"  void VRTK_CurveGenerator_setMaterial_m1446912861 (VRTK_CurveGenerator_t3769661606 * __this, GameObject_t1756533147 * ___item0, Material_t193706927 * ___material1, Color_t2020392075  ___color2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
