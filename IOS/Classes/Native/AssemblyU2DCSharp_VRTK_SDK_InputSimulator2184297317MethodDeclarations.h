﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_InputSimulator
struct SDK_InputSimulator_t2184297317;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_InputSimulator::.ctor()
extern "C"  void SDK_InputSimulator__ctor_m2288518213 (SDK_InputSimulator_t2184297317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.SDK_InputSimulator::FindInScene()
extern "C"  GameObject_t1756533147 * SDK_InputSimulator_FindInScene_m2286590344 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_InputSimulator::Awake()
extern "C"  void SDK_InputSimulator_Awake_m3887329230 (SDK_InputSimulator_t2184297317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_InputSimulator::OnDestroy()
extern "C"  void SDK_InputSimulator_OnDestroy_m618213164 (SDK_InputSimulator_t2184297317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_InputSimulator::Update()
extern "C"  void SDK_InputSimulator_Update_m1647115792 (SDK_InputSimulator_t2184297317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_InputSimulator::UpdateHands()
extern "C"  void SDK_InputSimulator_UpdateHands_m2969844720 (SDK_InputSimulator_t2184297317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_InputSimulator::UpdateRotation()
extern "C"  void SDK_InputSimulator_UpdateRotation_m470564778 (SDK_InputSimulator_t2184297317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_InputSimulator::UpdatePosition()
extern "C"  void SDK_InputSimulator_UpdatePosition_m1945457177 (SDK_InputSimulator_t2184297317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_InputSimulator::SetHand()
extern "C"  void SDK_InputSimulator_SetHand_m916108836 (SDK_InputSimulator_t2184297317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_InputSimulator::SetMove()
extern "C"  void SDK_InputSimulator_SetMove_m2321923620 (SDK_InputSimulator_t2184297317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_InputSimulator::.cctor()
extern "C"  void SDK_InputSimulator__cctor_m294589040 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
