﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_LoadRenderModel_Async
struct _LoadRenderModel_Async_t3622270247;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRRenderModelError21703732.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_LoadRenderModel_Async::.ctor(System.Object,System.IntPtr)
extern "C"  void _LoadRenderModel_Async__ctor_m1496280292 (_LoadRenderModel_Async_t3622270247 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRRenderModelError Valve.VR.IVRRenderModels/_LoadRenderModel_Async::Invoke(System.String,System.IntPtr&)
extern "C"  int32_t _LoadRenderModel_Async_Invoke_m2277295415 (_LoadRenderModel_Async_t3622270247 * __this, String_t* ___pchRenderModelName0, IntPtr_t* ___ppRenderModel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_LoadRenderModel_Async::BeginInvoke(System.String,System.IntPtr&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _LoadRenderModel_Async_BeginInvoke_m1525822855 (_LoadRenderModel_Async_t3622270247 * __this, String_t* ___pchRenderModelName0, IntPtr_t* ___ppRenderModel1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRRenderModelError Valve.VR.IVRRenderModels/_LoadRenderModel_Async::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern "C"  int32_t _LoadRenderModel_Async_EndInvoke_m2925842115 (_LoadRenderModel_Async_t3622270247 * __this, IntPtr_t* ___ppRenderModel0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
