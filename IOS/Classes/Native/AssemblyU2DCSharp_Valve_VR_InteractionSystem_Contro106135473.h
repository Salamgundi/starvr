﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.MeshRenderer>
struct List_1_t637362236;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo
struct  ButtonHintInfo_t106135473  : public Il2CppObject
{
public:
	// System.String Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::componentName
	String_t* ___componentName_0;
	// System.Collections.Generic.List`1<UnityEngine.MeshRenderer> Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::renderers
	List_1_t637362236 * ___renderers_1;
	// UnityEngine.Transform Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::localTransform
	Transform_t3275118058 * ___localTransform_2;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::textHintObject
	GameObject_t1756533147 * ___textHintObject_3;
	// UnityEngine.Transform Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::textStartAnchor
	Transform_t3275118058 * ___textStartAnchor_4;
	// UnityEngine.Transform Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::textEndAnchor
	Transform_t3275118058 * ___textEndAnchor_5;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::textEndOffsetDir
	Vector3_t2243707580  ___textEndOffsetDir_6;
	// UnityEngine.Transform Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::canvasOffset
	Transform_t3275118058 * ___canvasOffset_7;
	// UnityEngine.UI.Text Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::text
	Text_t356221433 * ___text_8;
	// UnityEngine.TextMesh Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::textMesh
	TextMesh_t1641806576 * ___textMesh_9;
	// UnityEngine.Canvas Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::textCanvas
	Canvas_t209405766 * ___textCanvas_10;
	// UnityEngine.LineRenderer Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::line
	LineRenderer_t849157671 * ___line_11;
	// System.Single Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::distanceFromCenter
	float ___distanceFromCenter_12;
	// System.Boolean Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo::textHintActive
	bool ___textHintActive_13;

public:
	inline static int32_t get_offset_of_componentName_0() { return static_cast<int32_t>(offsetof(ButtonHintInfo_t106135473, ___componentName_0)); }
	inline String_t* get_componentName_0() const { return ___componentName_0; }
	inline String_t** get_address_of_componentName_0() { return &___componentName_0; }
	inline void set_componentName_0(String_t* value)
	{
		___componentName_0 = value;
		Il2CppCodeGenWriteBarrier(&___componentName_0, value);
	}

	inline static int32_t get_offset_of_renderers_1() { return static_cast<int32_t>(offsetof(ButtonHintInfo_t106135473, ___renderers_1)); }
	inline List_1_t637362236 * get_renderers_1() const { return ___renderers_1; }
	inline List_1_t637362236 ** get_address_of_renderers_1() { return &___renderers_1; }
	inline void set_renderers_1(List_1_t637362236 * value)
	{
		___renderers_1 = value;
		Il2CppCodeGenWriteBarrier(&___renderers_1, value);
	}

	inline static int32_t get_offset_of_localTransform_2() { return static_cast<int32_t>(offsetof(ButtonHintInfo_t106135473, ___localTransform_2)); }
	inline Transform_t3275118058 * get_localTransform_2() const { return ___localTransform_2; }
	inline Transform_t3275118058 ** get_address_of_localTransform_2() { return &___localTransform_2; }
	inline void set_localTransform_2(Transform_t3275118058 * value)
	{
		___localTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&___localTransform_2, value);
	}

	inline static int32_t get_offset_of_textHintObject_3() { return static_cast<int32_t>(offsetof(ButtonHintInfo_t106135473, ___textHintObject_3)); }
	inline GameObject_t1756533147 * get_textHintObject_3() const { return ___textHintObject_3; }
	inline GameObject_t1756533147 ** get_address_of_textHintObject_3() { return &___textHintObject_3; }
	inline void set_textHintObject_3(GameObject_t1756533147 * value)
	{
		___textHintObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___textHintObject_3, value);
	}

	inline static int32_t get_offset_of_textStartAnchor_4() { return static_cast<int32_t>(offsetof(ButtonHintInfo_t106135473, ___textStartAnchor_4)); }
	inline Transform_t3275118058 * get_textStartAnchor_4() const { return ___textStartAnchor_4; }
	inline Transform_t3275118058 ** get_address_of_textStartAnchor_4() { return &___textStartAnchor_4; }
	inline void set_textStartAnchor_4(Transform_t3275118058 * value)
	{
		___textStartAnchor_4 = value;
		Il2CppCodeGenWriteBarrier(&___textStartAnchor_4, value);
	}

	inline static int32_t get_offset_of_textEndAnchor_5() { return static_cast<int32_t>(offsetof(ButtonHintInfo_t106135473, ___textEndAnchor_5)); }
	inline Transform_t3275118058 * get_textEndAnchor_5() const { return ___textEndAnchor_5; }
	inline Transform_t3275118058 ** get_address_of_textEndAnchor_5() { return &___textEndAnchor_5; }
	inline void set_textEndAnchor_5(Transform_t3275118058 * value)
	{
		___textEndAnchor_5 = value;
		Il2CppCodeGenWriteBarrier(&___textEndAnchor_5, value);
	}

	inline static int32_t get_offset_of_textEndOffsetDir_6() { return static_cast<int32_t>(offsetof(ButtonHintInfo_t106135473, ___textEndOffsetDir_6)); }
	inline Vector3_t2243707580  get_textEndOffsetDir_6() const { return ___textEndOffsetDir_6; }
	inline Vector3_t2243707580 * get_address_of_textEndOffsetDir_6() { return &___textEndOffsetDir_6; }
	inline void set_textEndOffsetDir_6(Vector3_t2243707580  value)
	{
		___textEndOffsetDir_6 = value;
	}

	inline static int32_t get_offset_of_canvasOffset_7() { return static_cast<int32_t>(offsetof(ButtonHintInfo_t106135473, ___canvasOffset_7)); }
	inline Transform_t3275118058 * get_canvasOffset_7() const { return ___canvasOffset_7; }
	inline Transform_t3275118058 ** get_address_of_canvasOffset_7() { return &___canvasOffset_7; }
	inline void set_canvasOffset_7(Transform_t3275118058 * value)
	{
		___canvasOffset_7 = value;
		Il2CppCodeGenWriteBarrier(&___canvasOffset_7, value);
	}

	inline static int32_t get_offset_of_text_8() { return static_cast<int32_t>(offsetof(ButtonHintInfo_t106135473, ___text_8)); }
	inline Text_t356221433 * get_text_8() const { return ___text_8; }
	inline Text_t356221433 ** get_address_of_text_8() { return &___text_8; }
	inline void set_text_8(Text_t356221433 * value)
	{
		___text_8 = value;
		Il2CppCodeGenWriteBarrier(&___text_8, value);
	}

	inline static int32_t get_offset_of_textMesh_9() { return static_cast<int32_t>(offsetof(ButtonHintInfo_t106135473, ___textMesh_9)); }
	inline TextMesh_t1641806576 * get_textMesh_9() const { return ___textMesh_9; }
	inline TextMesh_t1641806576 ** get_address_of_textMesh_9() { return &___textMesh_9; }
	inline void set_textMesh_9(TextMesh_t1641806576 * value)
	{
		___textMesh_9 = value;
		Il2CppCodeGenWriteBarrier(&___textMesh_9, value);
	}

	inline static int32_t get_offset_of_textCanvas_10() { return static_cast<int32_t>(offsetof(ButtonHintInfo_t106135473, ___textCanvas_10)); }
	inline Canvas_t209405766 * get_textCanvas_10() const { return ___textCanvas_10; }
	inline Canvas_t209405766 ** get_address_of_textCanvas_10() { return &___textCanvas_10; }
	inline void set_textCanvas_10(Canvas_t209405766 * value)
	{
		___textCanvas_10 = value;
		Il2CppCodeGenWriteBarrier(&___textCanvas_10, value);
	}

	inline static int32_t get_offset_of_line_11() { return static_cast<int32_t>(offsetof(ButtonHintInfo_t106135473, ___line_11)); }
	inline LineRenderer_t849157671 * get_line_11() const { return ___line_11; }
	inline LineRenderer_t849157671 ** get_address_of_line_11() { return &___line_11; }
	inline void set_line_11(LineRenderer_t849157671 * value)
	{
		___line_11 = value;
		Il2CppCodeGenWriteBarrier(&___line_11, value);
	}

	inline static int32_t get_offset_of_distanceFromCenter_12() { return static_cast<int32_t>(offsetof(ButtonHintInfo_t106135473, ___distanceFromCenter_12)); }
	inline float get_distanceFromCenter_12() const { return ___distanceFromCenter_12; }
	inline float* get_address_of_distanceFromCenter_12() { return &___distanceFromCenter_12; }
	inline void set_distanceFromCenter_12(float value)
	{
		___distanceFromCenter_12 = value;
	}

	inline static int32_t get_offset_of_textHintActive_13() { return static_cast<int32_t>(offsetof(ButtonHintInfo_t106135473, ___textHintActive_13)); }
	inline bool get_textHintActive_13() const { return ___textHintActive_13; }
	inline bool* get_address_of_textHintActive_13() { return &___textHintActive_13; }
	inline void set_textHintActive_13(bool value)
	{
		___textHintActive_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
