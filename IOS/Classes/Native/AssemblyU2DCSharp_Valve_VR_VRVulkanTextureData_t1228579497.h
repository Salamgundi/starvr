﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_IntPtr2504060609.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.VRVulkanTextureData_t
struct  VRVulkanTextureData_t_t1228579497 
{
public:
	// System.UInt64 Valve.VR.VRVulkanTextureData_t::m_nImage
	uint64_t ___m_nImage_0;
	// System.IntPtr Valve.VR.VRVulkanTextureData_t::m_pDevice
	IntPtr_t ___m_pDevice_1;
	// System.IntPtr Valve.VR.VRVulkanTextureData_t::m_pPhysicalDevice
	IntPtr_t ___m_pPhysicalDevice_2;
	// System.IntPtr Valve.VR.VRVulkanTextureData_t::m_pInstance
	IntPtr_t ___m_pInstance_3;
	// System.IntPtr Valve.VR.VRVulkanTextureData_t::m_pQueue
	IntPtr_t ___m_pQueue_4;
	// System.UInt32 Valve.VR.VRVulkanTextureData_t::m_nQueueFamilyIndex
	uint32_t ___m_nQueueFamilyIndex_5;
	// System.UInt32 Valve.VR.VRVulkanTextureData_t::m_nWidth
	uint32_t ___m_nWidth_6;
	// System.UInt32 Valve.VR.VRVulkanTextureData_t::m_nHeight
	uint32_t ___m_nHeight_7;
	// System.UInt32 Valve.VR.VRVulkanTextureData_t::m_nFormat
	uint32_t ___m_nFormat_8;
	// System.UInt32 Valve.VR.VRVulkanTextureData_t::m_nSampleCount
	uint32_t ___m_nSampleCount_9;

public:
	inline static int32_t get_offset_of_m_nImage_0() { return static_cast<int32_t>(offsetof(VRVulkanTextureData_t_t1228579497, ___m_nImage_0)); }
	inline uint64_t get_m_nImage_0() const { return ___m_nImage_0; }
	inline uint64_t* get_address_of_m_nImage_0() { return &___m_nImage_0; }
	inline void set_m_nImage_0(uint64_t value)
	{
		___m_nImage_0 = value;
	}

	inline static int32_t get_offset_of_m_pDevice_1() { return static_cast<int32_t>(offsetof(VRVulkanTextureData_t_t1228579497, ___m_pDevice_1)); }
	inline IntPtr_t get_m_pDevice_1() const { return ___m_pDevice_1; }
	inline IntPtr_t* get_address_of_m_pDevice_1() { return &___m_pDevice_1; }
	inline void set_m_pDevice_1(IntPtr_t value)
	{
		___m_pDevice_1 = value;
	}

	inline static int32_t get_offset_of_m_pPhysicalDevice_2() { return static_cast<int32_t>(offsetof(VRVulkanTextureData_t_t1228579497, ___m_pPhysicalDevice_2)); }
	inline IntPtr_t get_m_pPhysicalDevice_2() const { return ___m_pPhysicalDevice_2; }
	inline IntPtr_t* get_address_of_m_pPhysicalDevice_2() { return &___m_pPhysicalDevice_2; }
	inline void set_m_pPhysicalDevice_2(IntPtr_t value)
	{
		___m_pPhysicalDevice_2 = value;
	}

	inline static int32_t get_offset_of_m_pInstance_3() { return static_cast<int32_t>(offsetof(VRVulkanTextureData_t_t1228579497, ___m_pInstance_3)); }
	inline IntPtr_t get_m_pInstance_3() const { return ___m_pInstance_3; }
	inline IntPtr_t* get_address_of_m_pInstance_3() { return &___m_pInstance_3; }
	inline void set_m_pInstance_3(IntPtr_t value)
	{
		___m_pInstance_3 = value;
	}

	inline static int32_t get_offset_of_m_pQueue_4() { return static_cast<int32_t>(offsetof(VRVulkanTextureData_t_t1228579497, ___m_pQueue_4)); }
	inline IntPtr_t get_m_pQueue_4() const { return ___m_pQueue_4; }
	inline IntPtr_t* get_address_of_m_pQueue_4() { return &___m_pQueue_4; }
	inline void set_m_pQueue_4(IntPtr_t value)
	{
		___m_pQueue_4 = value;
	}

	inline static int32_t get_offset_of_m_nQueueFamilyIndex_5() { return static_cast<int32_t>(offsetof(VRVulkanTextureData_t_t1228579497, ___m_nQueueFamilyIndex_5)); }
	inline uint32_t get_m_nQueueFamilyIndex_5() const { return ___m_nQueueFamilyIndex_5; }
	inline uint32_t* get_address_of_m_nQueueFamilyIndex_5() { return &___m_nQueueFamilyIndex_5; }
	inline void set_m_nQueueFamilyIndex_5(uint32_t value)
	{
		___m_nQueueFamilyIndex_5 = value;
	}

	inline static int32_t get_offset_of_m_nWidth_6() { return static_cast<int32_t>(offsetof(VRVulkanTextureData_t_t1228579497, ___m_nWidth_6)); }
	inline uint32_t get_m_nWidth_6() const { return ___m_nWidth_6; }
	inline uint32_t* get_address_of_m_nWidth_6() { return &___m_nWidth_6; }
	inline void set_m_nWidth_6(uint32_t value)
	{
		___m_nWidth_6 = value;
	}

	inline static int32_t get_offset_of_m_nHeight_7() { return static_cast<int32_t>(offsetof(VRVulkanTextureData_t_t1228579497, ___m_nHeight_7)); }
	inline uint32_t get_m_nHeight_7() const { return ___m_nHeight_7; }
	inline uint32_t* get_address_of_m_nHeight_7() { return &___m_nHeight_7; }
	inline void set_m_nHeight_7(uint32_t value)
	{
		___m_nHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_nFormat_8() { return static_cast<int32_t>(offsetof(VRVulkanTextureData_t_t1228579497, ___m_nFormat_8)); }
	inline uint32_t get_m_nFormat_8() const { return ___m_nFormat_8; }
	inline uint32_t* get_address_of_m_nFormat_8() { return &___m_nFormat_8; }
	inline void set_m_nFormat_8(uint32_t value)
	{
		___m_nFormat_8 = value;
	}

	inline static int32_t get_offset_of_m_nSampleCount_9() { return static_cast<int32_t>(offsetof(VRVulkanTextureData_t_t1228579497, ___m_nSampleCount_9)); }
	inline uint32_t get_m_nSampleCount_9() const { return ___m_nSampleCount_9; }
	inline uint32_t* get_address_of_m_nSampleCount_9() { return &___m_nSampleCount_9; }
	inline void set_m_nSampleCount_9(uint32_t value)
	{
		___m_nSampleCount_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
