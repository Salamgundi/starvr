﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach
struct VRTK_BaseGrabAttach_t3487134318;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::.ctor()
extern "C"  void VRTK_BaseGrabAttach__ctor_m4151241430 (VRTK_BaseGrabAttach_t3487134318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::IsTracked()
extern "C"  bool VRTK_BaseGrabAttach_IsTracked_m4002846166 (VRTK_BaseGrabAttach_t3487134318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::IsClimbable()
extern "C"  bool VRTK_BaseGrabAttach_IsClimbable_m1928275365 (VRTK_BaseGrabAttach_t3487134318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::IsKinematic()
extern "C"  bool VRTK_BaseGrabAttach_IsKinematic_m1731532461 (VRTK_BaseGrabAttach_t3487134318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::ValidGrab(UnityEngine.Rigidbody)
extern "C"  bool VRTK_BaseGrabAttach_ValidGrab_m3989953128 (VRTK_BaseGrabAttach_t3487134318 * __this, Rigidbody_t4233889191 * ___checkAttachPoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::SetTrackPoint(UnityEngine.Transform)
extern "C"  void VRTK_BaseGrabAttach_SetTrackPoint_m2260038454 (VRTK_BaseGrabAttach_t3487134318 * __this, Transform_t3275118058 * ___givenTrackPoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::SetInitialAttachPoint(UnityEngine.Transform)
extern "C"  void VRTK_BaseGrabAttach_SetInitialAttachPoint_m866127476 (VRTK_BaseGrabAttach_t3487134318 * __this, Transform_t3275118058 * ___givenInitialAttachPoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::StartGrab(UnityEngine.GameObject,UnityEngine.GameObject,UnityEngine.Rigidbody)
extern "C"  bool VRTK_BaseGrabAttach_StartGrab_m3692991162 (VRTK_BaseGrabAttach_t3487134318 * __this, GameObject_t1756533147 * ___grabbingObject0, GameObject_t1756533147 * ___givenGrabbedObject1, Rigidbody_t4233889191 * ___givenControllerAttachPoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::StopGrab(System.Boolean)
extern "C"  void VRTK_BaseGrabAttach_StopGrab_m1117917777 (VRTK_BaseGrabAttach_t3487134318 * __this, bool ___applyGrabbingObjectVelocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::CreateTrackPoint(UnityEngine.Transform,UnityEngine.GameObject,UnityEngine.GameObject,System.Boolean&)
extern "C"  Transform_t3275118058 * VRTK_BaseGrabAttach_CreateTrackPoint_m423400865 (VRTK_BaseGrabAttach_t3487134318 * __this, Transform_t3275118058 * ___controllerPoint0, GameObject_t1756533147 * ___currentGrabbedObject1, GameObject_t1756533147 * ___currentGrabbingObject2, bool* ___customTrackPoint3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::ProcessUpdate()
extern "C"  void VRTK_BaseGrabAttach_ProcessUpdate_m2311616238 (VRTK_BaseGrabAttach_t3487134318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::ProcessFixedUpdate()
extern "C"  void VRTK_BaseGrabAttach_ProcessFixedUpdate_m3827300744 (VRTK_BaseGrabAttach_t3487134318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::ReleaseFromController(System.Boolean)
extern "C"  Rigidbody_t4233889191 * VRTK_BaseGrabAttach_ReleaseFromController_m1382381597 (VRTK_BaseGrabAttach_t3487134318 * __this, bool ___applyGrabbingObjectVelocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::ForceReleaseGrab()
extern "C"  void VRTK_BaseGrabAttach_ForceReleaseGrab_m3615903126 (VRTK_BaseGrabAttach_t3487134318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::ReleaseObject(System.Boolean)
extern "C"  void VRTK_BaseGrabAttach_ReleaseObject_m3620379895 (VRTK_BaseGrabAttach_t3487134318 * __this, bool ___applyGrabbingObjectVelocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::FlipSnapHandles()
extern "C"  void VRTK_BaseGrabAttach_FlipSnapHandles_m3282920488 (VRTK_BaseGrabAttach_t3487134318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::Awake()
extern "C"  void VRTK_BaseGrabAttach_Awake_m2268557561 (VRTK_BaseGrabAttach_t3487134318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::ThrowReleasedObject(UnityEngine.Rigidbody)
extern "C"  void VRTK_BaseGrabAttach_ThrowReleasedObject_m2622174594 (VRTK_BaseGrabAttach_t3487134318 * __this, Rigidbody_t4233889191 * ___objectRigidbody0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::GetSnapHandle(UnityEngine.GameObject)
extern "C"  Transform_t3275118058 * VRTK_BaseGrabAttach_GetSnapHandle_m2254189492 (VRTK_BaseGrabAttach_t3487134318 * __this, GameObject_t1756533147 * ___grabbingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach::FlipSnapHandle(UnityEngine.Transform)
extern "C"  void VRTK_BaseGrabAttach_FlipSnapHandle_m3608344634 (VRTK_BaseGrabAttach_t3487134318 * __this, Transform_t3275118058 * ___snapHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
