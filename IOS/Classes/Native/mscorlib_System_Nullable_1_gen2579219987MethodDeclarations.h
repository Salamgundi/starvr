﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2579219987.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Nullable`1<UnityEngine.EventSystems.RaycastResult>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3769688082_gshared (Nullable_1_t2579219987 * __this, RaycastResult_t21186376  ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m3769688082(__this, ___value0, method) ((  void (*) (Nullable_1_t2579219987 *, RaycastResult_t21186376 , const MethodInfo*))Nullable_1__ctor_m3769688082_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<UnityEngine.EventSystems.RaycastResult>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3271298471_gshared (Nullable_1_t2579219987 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m3271298471(__this, method) ((  bool (*) (Nullable_1_t2579219987 *, const MethodInfo*))Nullable_1_get_HasValue_m3271298471_gshared)(__this, method)
// T System.Nullable`1<UnityEngine.EventSystems.RaycastResult>::get_Value()
extern "C"  RaycastResult_t21186376  Nullable_1_get_Value_m2830962711_gshared (Nullable_1_t2579219987 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2830962711(__this, method) ((  RaycastResult_t21186376  (*) (Nullable_1_t2579219987 *, const MethodInfo*))Nullable_1_get_Value_m2830962711_gshared)(__this, method)
// System.Boolean System.Nullable`1<UnityEngine.EventSystems.RaycastResult>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1931672953_gshared (Nullable_1_t2579219987 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1931672953(__this, ___other0, method) ((  bool (*) (Nullable_1_t2579219987 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1931672953_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<UnityEngine.EventSystems.RaycastResult>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3001753250_gshared (Nullable_1_t2579219987 * __this, Nullable_1_t2579219987  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3001753250(__this, ___other0, method) ((  bool (*) (Nullable_1_t2579219987 *, Nullable_1_t2579219987 , const MethodInfo*))Nullable_1_Equals_m3001753250_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<UnityEngine.EventSystems.RaycastResult>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1744192955_gshared (Nullable_1_t2579219987 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1744192955(__this, method) ((  int32_t (*) (Nullable_1_t2579219987 *, const MethodInfo*))Nullable_1_GetHashCode_m1744192955_gshared)(__this, method)
// System.String System.Nullable`1<UnityEngine.EventSystems.RaycastResult>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3325861011_gshared (Nullable_1_t2579219987 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3325861011(__this, method) ((  String_t* (*) (Nullable_1_t2579219987 *, const MethodInfo*))Nullable_1_ToString_m3325861011_gshared)(__this, method)
