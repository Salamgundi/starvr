﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Ba3487134318.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.GrabAttachMechanics.VRTK_ClimbableGrabAttach
struct  VRTK_ClimbableGrabAttach_t3258016228  : public VRTK_BaseGrabAttach_t3487134318
{
public:
	// System.Boolean VRTK.GrabAttachMechanics.VRTK_ClimbableGrabAttach::useObjectRotation
	bool ___useObjectRotation_18;

public:
	inline static int32_t get_offset_of_useObjectRotation_18() { return static_cast<int32_t>(offsetof(VRTK_ClimbableGrabAttach_t3258016228, ___useObjectRotation_18)); }
	inline bool get_useObjectRotation_18() const { return ___useObjectRotation_18; }
	inline bool* get_address_of_useObjectRotation_18() { return &___useObjectRotation_18; }
	inline void set_useObjectRotation_18(bool value)
	{
		___useObjectRotation_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
