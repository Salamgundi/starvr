﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetApplicationProcessId
struct _GetApplicationProcessId_t1419106538;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetApplicationProcessId::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetApplicationProcessId__ctor_m4054132141 (_GetApplicationProcessId_t1419106538 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRApplications/_GetApplicationProcessId::Invoke(System.String)
extern "C"  uint32_t _GetApplicationProcessId_Invoke_m3564208732 (_GetApplicationProcessId_t1419106538 * __this, String_t* ___pchAppKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetApplicationProcessId::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetApplicationProcessId_BeginInvoke_m1148566812 (_GetApplicationProcessId_t1419106538 * __this, String_t* ___pchAppKey0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRApplications/_GetApplicationProcessId::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetApplicationProcessId_EndInvoke_m1317159546 (_GetApplicationProcessId_t1419106538 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
