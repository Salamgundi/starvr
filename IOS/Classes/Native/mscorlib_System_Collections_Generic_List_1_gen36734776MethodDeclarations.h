﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::.ctor()
#define List_1__ctor_m1770226854(__this, method) ((  void (*) (List_1_t36734776 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m2600992478(__this, ___collection0, method) ((  void (*) (List_1_t36734776 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m454375187_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::.ctor(System.Int32)
#define List_1__ctor_m1202286084(__this, ___capacity0, method) ((  void (*) (List_1_t36734776 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::.ctor(T[],System.Int32)
#define List_1__ctor_m3240485004(__this, ___data0, ___size1, method) ((  void (*) (List_1_t36734776 *, VRTK_DestinationMarkerU5BU5D_t3186542981*, int32_t, const MethodInfo*))List_1__ctor_m3410056391_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::.cctor()
#define List_1__cctor_m3453904600(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m639112693(__this, method) ((  Il2CppObject* (*) (List_1_t36734776 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m460977761(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t36734776 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1730162730(__this, method) ((  Il2CppObject * (*) (List_1_t36734776 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m2303022605(__this, ___item0, method) ((  int32_t (*) (List_1_t36734776 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m753290801(__this, ___item0, method) ((  bool (*) (List_1_t36734776 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1590653331(__this, ___item0, method) ((  int32_t (*) (List_1_t36734776 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m782987988(__this, ___index0, ___item1, method) ((  void (*) (List_1_t36734776 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m2065573662(__this, ___item0, method) ((  void (*) (List_1_t36734776 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3539954150(__this, method) ((  bool (*) (List_1_t36734776 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1400879045(__this, method) ((  bool (*) (List_1_t36734776 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m75993793(__this, method) ((  Il2CppObject * (*) (List_1_t36734776 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m2225794916(__this, method) ((  bool (*) (List_1_t36734776 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1502001025(__this, method) ((  bool (*) (List_1_t36734776 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m4277412936(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t36734776 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m998136707(__this, ___index0, ___value1, method) ((  void (*) (List_1_t36734776 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::Add(T)
#define List_1_Add_m3416977666(__this, ___item0, method) ((  void (*) (List_1_t36734776 *, VRTK_DestinationMarker_t667613644 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2078931681(__this, ___newCount0, method) ((  void (*) (List_1_t36734776 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m904511364(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t36734776 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1904903857_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1077003025(__this, ___collection0, method) ((  void (*) (List_1_t36734776 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3055836561(__this, ___enumerable0, method) ((  void (*) (List_1_t36734776 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m88190526(__this, ___collection0, method) ((  void (*) (List_1_t36734776 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::AsReadOnly()
#define List_1_AsReadOnly_m3406813273(__this, method) ((  ReadOnlyCollection_1_t853399336 * (*) (List_1_t36734776 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::Clear()
#define List_1_Clear_m1729552388(__this, method) ((  void (*) (List_1_t36734776 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::Contains(T)
#define List_1_Contains_m3470921186(__this, ___item0, method) ((  bool (*) (List_1_t36734776 *, VRTK_DestinationMarker_t667613644 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1577371104(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t36734776 *, VRTK_DestinationMarkerU5BU5D_t3186542981*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::Find(System.Predicate`1<T>)
#define List_1_Find_m846744396(__this, ___match0, method) ((  VRTK_DestinationMarker_t667613644 * (*) (List_1_t36734776 *, Predicate_1_t3405551055 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2486174489(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3405551055 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m3350829817(__this, ___match0, method) ((  List_1_t36734776 * (*) (List_1_t36734776 *, Predicate_1_t3405551055 *, const MethodInfo*))List_1_FindAll_m1864150752_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m3075225567(__this, ___match0, method) ((  List_1_t36734776 * (*) (List_1_t36734776 *, Predicate_1_t3405551055 *, const MethodInfo*))List_1_FindAllStackBits_m1687626136_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m4169719295(__this, ___match0, method) ((  List_1_t36734776 * (*) (List_1_t36734776 *, Predicate_1_t3405551055 *, const MethodInfo*))List_1_FindAllList_m3013325848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m1125623029(__this, ___match0, method) ((  int32_t (*) (List_1_t36734776 *, Predicate_1_t3405551055 *, const MethodInfo*))List_1_FindIndex_m552623364_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m2823456068(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t36734776 *, int32_t, int32_t, Predicate_1_t3405551055 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::GetEnumerator()
#define List_1_GetEnumerator_m3315896279(__this, method) ((  Enumerator_t3866431746  (*) (List_1_t36734776 *, const MethodInfo*))List_1_GetEnumerator_m3294992758_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::IndexOf(T)
#define List_1_IndexOf_m2370312730(__this, ___item0, method) ((  int32_t (*) (List_1_t36734776 *, VRTK_DestinationMarker_t667613644 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1268311085(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t36734776 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m2633803148(__this, ___index0, method) ((  void (*) (List_1_t36734776 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::Insert(System.Int32,T)
#define List_1_Insert_m492030403(__this, ___index0, ___item1, method) ((  void (*) (List_1_t36734776 *, int32_t, VRTK_DestinationMarker_t667613644 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2745145386(__this, ___collection0, method) ((  void (*) (List_1_t36734776 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::Remove(T)
#define List_1_Remove_m3015146857(__this, ___item0, method) ((  bool (*) (List_1_t36734776 *, VRTK_DestinationMarker_t667613644 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1045732285(__this, ___match0, method) ((  int32_t (*) (List_1_t36734776 *, Predicate_1_t3405551055 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m218154847(__this, ___index0, method) ((  void (*) (List_1_t36734776 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m594939568(__this, ___index0, ___count1, method) ((  void (*) (List_1_t36734776 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3877627853_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::Reverse()
#define List_1_Reverse_m299441149(__this, method) ((  void (*) (List_1_t36734776 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::Sort()
#define List_1_Sort_m81562551(__this, method) ((  void (*) (List_1_t36734776 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3326474688(__this, ___comparison0, method) ((  void (*) (List_1_t36734776 *, Comparison_1_t1929352495 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::ToArray()
#define List_1_ToArray_m1716094312(__this, method) ((  VRTK_DestinationMarkerU5BU5D_t3186542981* (*) (List_1_t36734776 *, const MethodInfo*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::TrimExcess()
#define List_1_TrimExcess_m631114186(__this, method) ((  void (*) (List_1_t36734776 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::get_Capacity()
#define List_1_get_Capacity_m3610943132(__this, method) ((  int32_t (*) (List_1_t36734776 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m81202385(__this, ___value0, method) ((  void (*) (List_1_t36734776 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::get_Count()
#define List_1_get_Count_m261569037(__this, method) ((  int32_t (*) (List_1_t36734776 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// T System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::get_Item(System.Int32)
#define List_1_get_Item_m3903203391(__this, ___index0, method) ((  VRTK_DestinationMarker_t667613644 * (*) (List_1_t36734776 *, int32_t, const MethodInfo*))List_1_get_Item_m1354830498_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VRTK.VRTK_DestinationMarker>::set_Item(System.Int32,T)
#define List_1_set_Item_m3769338442(__this, ___index0, ___value1, method) ((  void (*) (List_1_t36734776 *, int32_t, VRTK_DestinationMarker_t667613644 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
