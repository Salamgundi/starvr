﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_HeadsetCollisionFade
struct VRTK_HeadsetCollisionFade_t3608385924;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventArgs1242373387.h"

// System.Void VRTK.VRTK_HeadsetCollisionFade::.ctor()
extern "C"  void VRTK_HeadsetCollisionFade__ctor_m4257371028 (VRTK_HeadsetCollisionFade_t3608385924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollisionFade::OnEnable()
extern "C"  void VRTK_HeadsetCollisionFade_OnEnable_m238673036 (VRTK_HeadsetCollisionFade_t3608385924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollisionFade::OnDisable()
extern "C"  void VRTK_HeadsetCollisionFade_OnDisable_m2374363617 (VRTK_HeadsetCollisionFade_t3608385924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollisionFade::OnHeadsetCollisionDetect(System.Object,VRTK.HeadsetCollisionEventArgs)
extern "C"  void VRTK_HeadsetCollisionFade_OnHeadsetCollisionDetect_m2970878408 (VRTK_HeadsetCollisionFade_t3608385924 * __this, Il2CppObject * ___sender0, HeadsetCollisionEventArgs_t1242373387  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollisionFade::OnHeadsetCollisionEnded(System.Object,VRTK.HeadsetCollisionEventArgs)
extern "C"  void VRTK_HeadsetCollisionFade_OnHeadsetCollisionEnded_m1836154159 (VRTK_HeadsetCollisionFade_t3608385924 * __this, Il2CppObject * ___sender0, HeadsetCollisionEventArgs_t1242373387  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
