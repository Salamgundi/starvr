﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_SDKDetails
struct  VRTK_SDKDetails_t1748250348  : public Il2CppObject
{
public:
	// System.String VRTK.VRTK_SDKDetails::defineSymbol
	String_t* ___defineSymbol_0;
	// System.String VRTK.VRTK_SDKDetails::prettyName
	String_t* ___prettyName_1;
	// System.String VRTK.VRTK_SDKDetails::checkType
	String_t* ___checkType_2;

public:
	inline static int32_t get_offset_of_defineSymbol_0() { return static_cast<int32_t>(offsetof(VRTK_SDKDetails_t1748250348, ___defineSymbol_0)); }
	inline String_t* get_defineSymbol_0() const { return ___defineSymbol_0; }
	inline String_t** get_address_of_defineSymbol_0() { return &___defineSymbol_0; }
	inline void set_defineSymbol_0(String_t* value)
	{
		___defineSymbol_0 = value;
		Il2CppCodeGenWriteBarrier(&___defineSymbol_0, value);
	}

	inline static int32_t get_offset_of_prettyName_1() { return static_cast<int32_t>(offsetof(VRTK_SDKDetails_t1748250348, ___prettyName_1)); }
	inline String_t* get_prettyName_1() const { return ___prettyName_1; }
	inline String_t** get_address_of_prettyName_1() { return &___prettyName_1; }
	inline void set_prettyName_1(String_t* value)
	{
		___prettyName_1 = value;
		Il2CppCodeGenWriteBarrier(&___prettyName_1, value);
	}

	inline static int32_t get_offset_of_checkType_2() { return static_cast<int32_t>(offsetof(VRTK_SDKDetails_t1748250348, ___checkType_2)); }
	inline String_t* get_checkType_2() const { return ___checkType_2; }
	inline String_t** get_address_of_checkType_2() { return &___checkType_2; }
	inline void set_checkType_2(String_t* value)
	{
		___checkType_2 = value;
		Il2CppCodeGenWriteBarrier(&___checkType_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
