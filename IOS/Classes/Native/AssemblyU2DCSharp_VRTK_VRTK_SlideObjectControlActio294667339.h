﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_VRTK_BaseObjectControlActio3375001897.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_SlideObjectControlAction
struct  VRTK_SlideObjectControlAction_t294667339  : public VRTK_BaseObjectControlAction_t3375001897
{
public:
	// System.Single VRTK.VRTK_SlideObjectControlAction::maximumSpeed
	float ___maximumSpeed_10;
	// System.Single VRTK.VRTK_SlideObjectControlAction::deceleration
	float ___deceleration_11;
	// System.Single VRTK.VRTK_SlideObjectControlAction::fallingDeceleration
	float ___fallingDeceleration_12;
	// System.Single VRTK.VRTK_SlideObjectControlAction::speedMultiplier
	float ___speedMultiplier_13;
	// System.Single VRTK.VRTK_SlideObjectControlAction::currentSpeed
	float ___currentSpeed_14;

public:
	inline static int32_t get_offset_of_maximumSpeed_10() { return static_cast<int32_t>(offsetof(VRTK_SlideObjectControlAction_t294667339, ___maximumSpeed_10)); }
	inline float get_maximumSpeed_10() const { return ___maximumSpeed_10; }
	inline float* get_address_of_maximumSpeed_10() { return &___maximumSpeed_10; }
	inline void set_maximumSpeed_10(float value)
	{
		___maximumSpeed_10 = value;
	}

	inline static int32_t get_offset_of_deceleration_11() { return static_cast<int32_t>(offsetof(VRTK_SlideObjectControlAction_t294667339, ___deceleration_11)); }
	inline float get_deceleration_11() const { return ___deceleration_11; }
	inline float* get_address_of_deceleration_11() { return &___deceleration_11; }
	inline void set_deceleration_11(float value)
	{
		___deceleration_11 = value;
	}

	inline static int32_t get_offset_of_fallingDeceleration_12() { return static_cast<int32_t>(offsetof(VRTK_SlideObjectControlAction_t294667339, ___fallingDeceleration_12)); }
	inline float get_fallingDeceleration_12() const { return ___fallingDeceleration_12; }
	inline float* get_address_of_fallingDeceleration_12() { return &___fallingDeceleration_12; }
	inline void set_fallingDeceleration_12(float value)
	{
		___fallingDeceleration_12 = value;
	}

	inline static int32_t get_offset_of_speedMultiplier_13() { return static_cast<int32_t>(offsetof(VRTK_SlideObjectControlAction_t294667339, ___speedMultiplier_13)); }
	inline float get_speedMultiplier_13() const { return ___speedMultiplier_13; }
	inline float* get_address_of_speedMultiplier_13() { return &___speedMultiplier_13; }
	inline void set_speedMultiplier_13(float value)
	{
		___speedMultiplier_13 = value;
	}

	inline static int32_t get_offset_of_currentSpeed_14() { return static_cast<int32_t>(offsetof(VRTK_SlideObjectControlAction_t294667339, ___currentSpeed_14)); }
	inline float get_currentSpeed_14() const { return ___currentSpeed_14; }
	inline float* get_address_of_currentSpeed_14() { return &___currentSpeed_14; }
	inline void set_currentSpeed_14(float value)
	{
		___currentSpeed_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
