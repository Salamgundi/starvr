﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.InteractableButtonEvents
struct InteractableButtonEvents_t1568724519;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.InteractableButtonEvents::.ctor()
extern "C"  void InteractableButtonEvents__ctor_m1311705123 (InteractableButtonEvents_t1568724519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InteractableButtonEvents::Update()
extern "C"  void InteractableButtonEvents_Update_m113001598 (InteractableButtonEvents_t1568724519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
