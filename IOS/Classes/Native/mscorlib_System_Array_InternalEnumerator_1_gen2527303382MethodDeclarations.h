﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2527303382.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Valve_VR_TrackedDevicePose_t1668551120.h"

// System.Void System.Array/InternalEnumerator`1<Valve.VR.TrackedDevicePose_t>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3323039923_gshared (InternalEnumerator_1_t2527303382 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3323039923(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2527303382 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3323039923_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.TrackedDevicePose_t>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4036684643_gshared (InternalEnumerator_1_t2527303382 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4036684643(__this, method) ((  void (*) (InternalEnumerator_1_t2527303382 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4036684643_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Valve.VR.TrackedDevicePose_t>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3595327175_gshared (InternalEnumerator_1_t2527303382 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3595327175(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2527303382 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3595327175_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.TrackedDevicePose_t>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2287730804_gshared (InternalEnumerator_1_t2527303382 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2287730804(__this, method) ((  void (*) (InternalEnumerator_1_t2527303382 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2287730804_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Valve.VR.TrackedDevicePose_t>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m735745879_gshared (InternalEnumerator_1_t2527303382 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m735745879(__this, method) ((  bool (*) (InternalEnumerator_1_t2527303382 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m735745879_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Valve.VR.TrackedDevicePose_t>::get_Current()
extern "C"  TrackedDevicePose_t_t1668551120  InternalEnumerator_1_get_Current_m3118816290_gshared (InternalEnumerator_1_t2527303382 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3118816290(__this, method) ((  TrackedDevicePose_t_t1668551120  (*) (InternalEnumerator_1_t2527303382 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3118816290_gshared)(__this, method)
