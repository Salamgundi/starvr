﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.KeyCode[]
struct KeyCodeU5BU5D_t3340676209;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_AdaptiveQuality/KeyboardShortcuts
struct  KeyboardShortcuts_t2232892609  : public Il2CppObject
{
public:

public:
};

struct KeyboardShortcuts_t2232892609_StaticFields
{
public:
	// UnityEngine.KeyCode[] VRTK.VRTK_AdaptiveQuality/KeyboardShortcuts::Modifiers
	KeyCodeU5BU5D_t3340676209* ___Modifiers_0;

public:
	inline static int32_t get_offset_of_Modifiers_0() { return static_cast<int32_t>(offsetof(KeyboardShortcuts_t2232892609_StaticFields, ___Modifiers_0)); }
	inline KeyCodeU5BU5D_t3340676209* get_Modifiers_0() const { return ___Modifiers_0; }
	inline KeyCodeU5BU5D_t3340676209** get_address_of_Modifiers_0() { return &___Modifiers_0; }
	inline void set_Modifiers_0(KeyCodeU5BU5D_t3340676209* value)
	{
		___Modifiers_0 = value;
		Il2CppCodeGenWriteBarrier(&___Modifiers_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
