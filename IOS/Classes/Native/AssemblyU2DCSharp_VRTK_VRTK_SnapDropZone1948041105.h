﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_PolicyList
struct VRTK_PolicyList_t2965133344;
// VRTK.SnapDropZoneEventHandler
struct SnapDropZoneEventHandler_t172258073;
// VRTK.Highlighters.VRTK_BaseHighlighter
struct VRTK_BaseHighlighter_t3110203740;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SnapDropZone_SnapTypes2013855124.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_SnapDropZone
struct  VRTK_SnapDropZone_t1948041105  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject VRTK.VRTK_SnapDropZone::highlightObjectPrefab
	GameObject_t1756533147 * ___highlightObjectPrefab_2;
	// VRTK.VRTK_SnapDropZone/SnapTypes VRTK.VRTK_SnapDropZone::snapType
	int32_t ___snapType_3;
	// System.Single VRTK.VRTK_SnapDropZone::snapDuration
	float ___snapDuration_4;
	// System.Boolean VRTK.VRTK_SnapDropZone::applyScalingOnSnap
	bool ___applyScalingOnSnap_5;
	// UnityEngine.Color VRTK.VRTK_SnapDropZone::highlightColor
	Color_t2020392075  ___highlightColor_6;
	// System.Boolean VRTK.VRTK_SnapDropZone::highlightAlwaysActive
	bool ___highlightAlwaysActive_7;
	// VRTK.VRTK_PolicyList VRTK.VRTK_SnapDropZone::validObjectListPolicy
	VRTK_PolicyList_t2965133344 * ___validObjectListPolicy_8;
	// System.Boolean VRTK.VRTK_SnapDropZone::displayDropZoneInEditor
	bool ___displayDropZoneInEditor_9;
	// VRTK.SnapDropZoneEventHandler VRTK.VRTK_SnapDropZone::ObjectEnteredSnapDropZone
	SnapDropZoneEventHandler_t172258073 * ___ObjectEnteredSnapDropZone_10;
	// VRTK.SnapDropZoneEventHandler VRTK.VRTK_SnapDropZone::ObjectExitedSnapDropZone
	SnapDropZoneEventHandler_t172258073 * ___ObjectExitedSnapDropZone_11;
	// VRTK.SnapDropZoneEventHandler VRTK.VRTK_SnapDropZone::ObjectSnappedToDropZone
	SnapDropZoneEventHandler_t172258073 * ___ObjectSnappedToDropZone_12;
	// VRTK.SnapDropZoneEventHandler VRTK.VRTK_SnapDropZone::ObjectUnsnappedFromDropZone
	SnapDropZoneEventHandler_t172258073 * ___ObjectUnsnappedFromDropZone_13;
	// UnityEngine.GameObject VRTK.VRTK_SnapDropZone::previousPrefab
	GameObject_t1756533147 * ___previousPrefab_14;
	// UnityEngine.GameObject VRTK.VRTK_SnapDropZone::highlightContainer
	GameObject_t1756533147 * ___highlightContainer_15;
	// UnityEngine.GameObject VRTK.VRTK_SnapDropZone::highlightObject
	GameObject_t1756533147 * ___highlightObject_16;
	// UnityEngine.GameObject VRTK.VRTK_SnapDropZone::highlightEditorObject
	GameObject_t1756533147 * ___highlightEditorObject_17;
	// UnityEngine.GameObject VRTK.VRTK_SnapDropZone::currentValidSnapObject
	GameObject_t1756533147 * ___currentValidSnapObject_18;
	// UnityEngine.GameObject VRTK.VRTK_SnapDropZone::currentSnappedObject
	GameObject_t1756533147 * ___currentSnappedObject_19;
	// VRTK.Highlighters.VRTK_BaseHighlighter VRTK.VRTK_SnapDropZone::objectHighlighter
	VRTK_BaseHighlighter_t3110203740 * ___objectHighlighter_20;
	// System.Boolean VRTK.VRTK_SnapDropZone::willSnap
	bool ___willSnap_21;
	// System.Boolean VRTK.VRTK_SnapDropZone::isSnapped
	bool ___isSnapped_22;
	// System.Boolean VRTK.VRTK_SnapDropZone::isHighlighted
	bool ___isHighlighted_23;
	// UnityEngine.Coroutine VRTK.VRTK_SnapDropZone::transitionInPlace
	Coroutine_t2299508840 * ___transitionInPlace_24;
	// System.Boolean VRTK.VRTK_SnapDropZone::originalJointCollisionState
	bool ___originalJointCollisionState_25;

public:
	inline static int32_t get_offset_of_highlightObjectPrefab_2() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___highlightObjectPrefab_2)); }
	inline GameObject_t1756533147 * get_highlightObjectPrefab_2() const { return ___highlightObjectPrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_highlightObjectPrefab_2() { return &___highlightObjectPrefab_2; }
	inline void set_highlightObjectPrefab_2(GameObject_t1756533147 * value)
	{
		___highlightObjectPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___highlightObjectPrefab_2, value);
	}

	inline static int32_t get_offset_of_snapType_3() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___snapType_3)); }
	inline int32_t get_snapType_3() const { return ___snapType_3; }
	inline int32_t* get_address_of_snapType_3() { return &___snapType_3; }
	inline void set_snapType_3(int32_t value)
	{
		___snapType_3 = value;
	}

	inline static int32_t get_offset_of_snapDuration_4() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___snapDuration_4)); }
	inline float get_snapDuration_4() const { return ___snapDuration_4; }
	inline float* get_address_of_snapDuration_4() { return &___snapDuration_4; }
	inline void set_snapDuration_4(float value)
	{
		___snapDuration_4 = value;
	}

	inline static int32_t get_offset_of_applyScalingOnSnap_5() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___applyScalingOnSnap_5)); }
	inline bool get_applyScalingOnSnap_5() const { return ___applyScalingOnSnap_5; }
	inline bool* get_address_of_applyScalingOnSnap_5() { return &___applyScalingOnSnap_5; }
	inline void set_applyScalingOnSnap_5(bool value)
	{
		___applyScalingOnSnap_5 = value;
	}

	inline static int32_t get_offset_of_highlightColor_6() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___highlightColor_6)); }
	inline Color_t2020392075  get_highlightColor_6() const { return ___highlightColor_6; }
	inline Color_t2020392075 * get_address_of_highlightColor_6() { return &___highlightColor_6; }
	inline void set_highlightColor_6(Color_t2020392075  value)
	{
		___highlightColor_6 = value;
	}

	inline static int32_t get_offset_of_highlightAlwaysActive_7() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___highlightAlwaysActive_7)); }
	inline bool get_highlightAlwaysActive_7() const { return ___highlightAlwaysActive_7; }
	inline bool* get_address_of_highlightAlwaysActive_7() { return &___highlightAlwaysActive_7; }
	inline void set_highlightAlwaysActive_7(bool value)
	{
		___highlightAlwaysActive_7 = value;
	}

	inline static int32_t get_offset_of_validObjectListPolicy_8() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___validObjectListPolicy_8)); }
	inline VRTK_PolicyList_t2965133344 * get_validObjectListPolicy_8() const { return ___validObjectListPolicy_8; }
	inline VRTK_PolicyList_t2965133344 ** get_address_of_validObjectListPolicy_8() { return &___validObjectListPolicy_8; }
	inline void set_validObjectListPolicy_8(VRTK_PolicyList_t2965133344 * value)
	{
		___validObjectListPolicy_8 = value;
		Il2CppCodeGenWriteBarrier(&___validObjectListPolicy_8, value);
	}

	inline static int32_t get_offset_of_displayDropZoneInEditor_9() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___displayDropZoneInEditor_9)); }
	inline bool get_displayDropZoneInEditor_9() const { return ___displayDropZoneInEditor_9; }
	inline bool* get_address_of_displayDropZoneInEditor_9() { return &___displayDropZoneInEditor_9; }
	inline void set_displayDropZoneInEditor_9(bool value)
	{
		___displayDropZoneInEditor_9 = value;
	}

	inline static int32_t get_offset_of_ObjectEnteredSnapDropZone_10() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___ObjectEnteredSnapDropZone_10)); }
	inline SnapDropZoneEventHandler_t172258073 * get_ObjectEnteredSnapDropZone_10() const { return ___ObjectEnteredSnapDropZone_10; }
	inline SnapDropZoneEventHandler_t172258073 ** get_address_of_ObjectEnteredSnapDropZone_10() { return &___ObjectEnteredSnapDropZone_10; }
	inline void set_ObjectEnteredSnapDropZone_10(SnapDropZoneEventHandler_t172258073 * value)
	{
		___ObjectEnteredSnapDropZone_10 = value;
		Il2CppCodeGenWriteBarrier(&___ObjectEnteredSnapDropZone_10, value);
	}

	inline static int32_t get_offset_of_ObjectExitedSnapDropZone_11() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___ObjectExitedSnapDropZone_11)); }
	inline SnapDropZoneEventHandler_t172258073 * get_ObjectExitedSnapDropZone_11() const { return ___ObjectExitedSnapDropZone_11; }
	inline SnapDropZoneEventHandler_t172258073 ** get_address_of_ObjectExitedSnapDropZone_11() { return &___ObjectExitedSnapDropZone_11; }
	inline void set_ObjectExitedSnapDropZone_11(SnapDropZoneEventHandler_t172258073 * value)
	{
		___ObjectExitedSnapDropZone_11 = value;
		Il2CppCodeGenWriteBarrier(&___ObjectExitedSnapDropZone_11, value);
	}

	inline static int32_t get_offset_of_ObjectSnappedToDropZone_12() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___ObjectSnappedToDropZone_12)); }
	inline SnapDropZoneEventHandler_t172258073 * get_ObjectSnappedToDropZone_12() const { return ___ObjectSnappedToDropZone_12; }
	inline SnapDropZoneEventHandler_t172258073 ** get_address_of_ObjectSnappedToDropZone_12() { return &___ObjectSnappedToDropZone_12; }
	inline void set_ObjectSnappedToDropZone_12(SnapDropZoneEventHandler_t172258073 * value)
	{
		___ObjectSnappedToDropZone_12 = value;
		Il2CppCodeGenWriteBarrier(&___ObjectSnappedToDropZone_12, value);
	}

	inline static int32_t get_offset_of_ObjectUnsnappedFromDropZone_13() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___ObjectUnsnappedFromDropZone_13)); }
	inline SnapDropZoneEventHandler_t172258073 * get_ObjectUnsnappedFromDropZone_13() const { return ___ObjectUnsnappedFromDropZone_13; }
	inline SnapDropZoneEventHandler_t172258073 ** get_address_of_ObjectUnsnappedFromDropZone_13() { return &___ObjectUnsnappedFromDropZone_13; }
	inline void set_ObjectUnsnappedFromDropZone_13(SnapDropZoneEventHandler_t172258073 * value)
	{
		___ObjectUnsnappedFromDropZone_13 = value;
		Il2CppCodeGenWriteBarrier(&___ObjectUnsnappedFromDropZone_13, value);
	}

	inline static int32_t get_offset_of_previousPrefab_14() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___previousPrefab_14)); }
	inline GameObject_t1756533147 * get_previousPrefab_14() const { return ___previousPrefab_14; }
	inline GameObject_t1756533147 ** get_address_of_previousPrefab_14() { return &___previousPrefab_14; }
	inline void set_previousPrefab_14(GameObject_t1756533147 * value)
	{
		___previousPrefab_14 = value;
		Il2CppCodeGenWriteBarrier(&___previousPrefab_14, value);
	}

	inline static int32_t get_offset_of_highlightContainer_15() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___highlightContainer_15)); }
	inline GameObject_t1756533147 * get_highlightContainer_15() const { return ___highlightContainer_15; }
	inline GameObject_t1756533147 ** get_address_of_highlightContainer_15() { return &___highlightContainer_15; }
	inline void set_highlightContainer_15(GameObject_t1756533147 * value)
	{
		___highlightContainer_15 = value;
		Il2CppCodeGenWriteBarrier(&___highlightContainer_15, value);
	}

	inline static int32_t get_offset_of_highlightObject_16() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___highlightObject_16)); }
	inline GameObject_t1756533147 * get_highlightObject_16() const { return ___highlightObject_16; }
	inline GameObject_t1756533147 ** get_address_of_highlightObject_16() { return &___highlightObject_16; }
	inline void set_highlightObject_16(GameObject_t1756533147 * value)
	{
		___highlightObject_16 = value;
		Il2CppCodeGenWriteBarrier(&___highlightObject_16, value);
	}

	inline static int32_t get_offset_of_highlightEditorObject_17() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___highlightEditorObject_17)); }
	inline GameObject_t1756533147 * get_highlightEditorObject_17() const { return ___highlightEditorObject_17; }
	inline GameObject_t1756533147 ** get_address_of_highlightEditorObject_17() { return &___highlightEditorObject_17; }
	inline void set_highlightEditorObject_17(GameObject_t1756533147 * value)
	{
		___highlightEditorObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___highlightEditorObject_17, value);
	}

	inline static int32_t get_offset_of_currentValidSnapObject_18() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___currentValidSnapObject_18)); }
	inline GameObject_t1756533147 * get_currentValidSnapObject_18() const { return ___currentValidSnapObject_18; }
	inline GameObject_t1756533147 ** get_address_of_currentValidSnapObject_18() { return &___currentValidSnapObject_18; }
	inline void set_currentValidSnapObject_18(GameObject_t1756533147 * value)
	{
		___currentValidSnapObject_18 = value;
		Il2CppCodeGenWriteBarrier(&___currentValidSnapObject_18, value);
	}

	inline static int32_t get_offset_of_currentSnappedObject_19() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___currentSnappedObject_19)); }
	inline GameObject_t1756533147 * get_currentSnappedObject_19() const { return ___currentSnappedObject_19; }
	inline GameObject_t1756533147 ** get_address_of_currentSnappedObject_19() { return &___currentSnappedObject_19; }
	inline void set_currentSnappedObject_19(GameObject_t1756533147 * value)
	{
		___currentSnappedObject_19 = value;
		Il2CppCodeGenWriteBarrier(&___currentSnappedObject_19, value);
	}

	inline static int32_t get_offset_of_objectHighlighter_20() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___objectHighlighter_20)); }
	inline VRTK_BaseHighlighter_t3110203740 * get_objectHighlighter_20() const { return ___objectHighlighter_20; }
	inline VRTK_BaseHighlighter_t3110203740 ** get_address_of_objectHighlighter_20() { return &___objectHighlighter_20; }
	inline void set_objectHighlighter_20(VRTK_BaseHighlighter_t3110203740 * value)
	{
		___objectHighlighter_20 = value;
		Il2CppCodeGenWriteBarrier(&___objectHighlighter_20, value);
	}

	inline static int32_t get_offset_of_willSnap_21() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___willSnap_21)); }
	inline bool get_willSnap_21() const { return ___willSnap_21; }
	inline bool* get_address_of_willSnap_21() { return &___willSnap_21; }
	inline void set_willSnap_21(bool value)
	{
		___willSnap_21 = value;
	}

	inline static int32_t get_offset_of_isSnapped_22() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___isSnapped_22)); }
	inline bool get_isSnapped_22() const { return ___isSnapped_22; }
	inline bool* get_address_of_isSnapped_22() { return &___isSnapped_22; }
	inline void set_isSnapped_22(bool value)
	{
		___isSnapped_22 = value;
	}

	inline static int32_t get_offset_of_isHighlighted_23() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___isHighlighted_23)); }
	inline bool get_isHighlighted_23() const { return ___isHighlighted_23; }
	inline bool* get_address_of_isHighlighted_23() { return &___isHighlighted_23; }
	inline void set_isHighlighted_23(bool value)
	{
		___isHighlighted_23 = value;
	}

	inline static int32_t get_offset_of_transitionInPlace_24() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___transitionInPlace_24)); }
	inline Coroutine_t2299508840 * get_transitionInPlace_24() const { return ___transitionInPlace_24; }
	inline Coroutine_t2299508840 ** get_address_of_transitionInPlace_24() { return &___transitionInPlace_24; }
	inline void set_transitionInPlace_24(Coroutine_t2299508840 * value)
	{
		___transitionInPlace_24 = value;
		Il2CppCodeGenWriteBarrier(&___transitionInPlace_24, value);
	}

	inline static int32_t get_offset_of_originalJointCollisionState_25() { return static_cast<int32_t>(offsetof(VRTK_SnapDropZone_t1948041105, ___originalJointCollisionState_25)); }
	inline bool get_originalJointCollisionState_25() const { return ___originalJointCollisionState_25; }
	inline bool* get_address_of_originalJointCollisionState_25() { return &___originalJointCollisionState_25; }
	inline void set_originalJointCollisionState_25(bool value)
	{
		___originalJointCollisionState_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
