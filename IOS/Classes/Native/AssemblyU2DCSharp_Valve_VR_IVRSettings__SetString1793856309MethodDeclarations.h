﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSettings/_SetString
struct _SetString_t1793856309;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRSettingsError4124928198.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSettings/_SetString::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetString__ctor_m1567557314 (_SetString_t1793856309 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSettings/_SetString::Invoke(System.String,System.String,System.String,Valve.VR.EVRSettingsError&)
extern "C"  void _SetString_Invoke_m108682660 (_SetString_t1793856309 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, String_t* ___pchValue2, int32_t* ___peError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSettings/_SetString::BeginInvoke(System.String,System.String,System.String,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetString_BeginInvoke_m2035818935 (_SetString_t1793856309 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, String_t* ___pchValue2, int32_t* ___peError3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSettings/_SetString::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  void _SetString_EndInvoke_m2697589472 (_SetString_t1793856309 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
