﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetControllerAxisTypeNameFromEnum
struct _GetControllerAxisTypeNameFromEnum_t3568402941;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRControllerAxisType1358176136.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetControllerAxisTypeNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetControllerAxisTypeNameFromEnum__ctor_m2699871724 (_GetControllerAxisTypeNameFromEnum_t3568402941 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRSystem/_GetControllerAxisTypeNameFromEnum::Invoke(Valve.VR.EVRControllerAxisType)
extern "C"  IntPtr_t _GetControllerAxisTypeNameFromEnum_Invoke_m1738538447 (_GetControllerAxisTypeNameFromEnum_t3568402941 * __this, int32_t ___eAxisType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetControllerAxisTypeNameFromEnum::BeginInvoke(Valve.VR.EVRControllerAxisType,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetControllerAxisTypeNameFromEnum_BeginInvoke_m3232829541 (_GetControllerAxisTypeNameFromEnum_t3568402941 * __this, int32_t ___eAxisType0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRSystem/_GetControllerAxisTypeNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetControllerAxisTypeNameFromEnum_EndInvoke_m3480416983 (_GetControllerAxisTypeNameFromEnum_t3568402941 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
