﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_BodyPhysics/<RestoreCollisions>c__Iterator0
struct U3CRestoreCollisionsU3Ec__Iterator0_t1574376249;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_BodyPhysics/<RestoreCollisions>c__Iterator0::.ctor()
extern "C"  void U3CRestoreCollisionsU3Ec__Iterator0__ctor_m171322512 (U3CRestoreCollisionsU3Ec__Iterator0_t1574376249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BodyPhysics/<RestoreCollisions>c__Iterator0::MoveNext()
extern "C"  bool U3CRestoreCollisionsU3Ec__Iterator0_MoveNext_m2113829892 (U3CRestoreCollisionsU3Ec__Iterator0_t1574376249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_BodyPhysics/<RestoreCollisions>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRestoreCollisionsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3119568170 (U3CRestoreCollisionsU3Ec__Iterator0_t1574376249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_BodyPhysics/<RestoreCollisions>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRestoreCollisionsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3901447362 (U3CRestoreCollisionsU3Ec__Iterator0_t1574376249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics/<RestoreCollisions>c__Iterator0::Dispose()
extern "C"  void U3CRestoreCollisionsU3Ec__Iterator0_Dispose_m3703091379 (U3CRestoreCollisionsU3Ec__Iterator0_t1574376249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BodyPhysics/<RestoreCollisions>c__Iterator0::Reset()
extern "C"  void U3CRestoreCollisionsU3Ec__Iterator0_Reset_m2413361189 (U3CRestoreCollisionsU3Ec__Iterator0_t1574376249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
