﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.FallbackCameraController
struct  FallbackCameraController_t1563223449  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Valve.VR.InteractionSystem.FallbackCameraController::speed
	float ___speed_2;
	// System.Single Valve.VR.InteractionSystem.FallbackCameraController::shiftSpeed
	float ___shiftSpeed_3;
	// System.Boolean Valve.VR.InteractionSystem.FallbackCameraController::showInstructions
	bool ___showInstructions_4;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.FallbackCameraController::startEulerAngles
	Vector3_t2243707580  ___startEulerAngles_5;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.FallbackCameraController::startMousePosition
	Vector3_t2243707580  ___startMousePosition_6;
	// System.Single Valve.VR.InteractionSystem.FallbackCameraController::realTime
	float ___realTime_7;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(FallbackCameraController_t1563223449, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_shiftSpeed_3() { return static_cast<int32_t>(offsetof(FallbackCameraController_t1563223449, ___shiftSpeed_3)); }
	inline float get_shiftSpeed_3() const { return ___shiftSpeed_3; }
	inline float* get_address_of_shiftSpeed_3() { return &___shiftSpeed_3; }
	inline void set_shiftSpeed_3(float value)
	{
		___shiftSpeed_3 = value;
	}

	inline static int32_t get_offset_of_showInstructions_4() { return static_cast<int32_t>(offsetof(FallbackCameraController_t1563223449, ___showInstructions_4)); }
	inline bool get_showInstructions_4() const { return ___showInstructions_4; }
	inline bool* get_address_of_showInstructions_4() { return &___showInstructions_4; }
	inline void set_showInstructions_4(bool value)
	{
		___showInstructions_4 = value;
	}

	inline static int32_t get_offset_of_startEulerAngles_5() { return static_cast<int32_t>(offsetof(FallbackCameraController_t1563223449, ___startEulerAngles_5)); }
	inline Vector3_t2243707580  get_startEulerAngles_5() const { return ___startEulerAngles_5; }
	inline Vector3_t2243707580 * get_address_of_startEulerAngles_5() { return &___startEulerAngles_5; }
	inline void set_startEulerAngles_5(Vector3_t2243707580  value)
	{
		___startEulerAngles_5 = value;
	}

	inline static int32_t get_offset_of_startMousePosition_6() { return static_cast<int32_t>(offsetof(FallbackCameraController_t1563223449, ___startMousePosition_6)); }
	inline Vector3_t2243707580  get_startMousePosition_6() const { return ___startMousePosition_6; }
	inline Vector3_t2243707580 * get_address_of_startMousePosition_6() { return &___startMousePosition_6; }
	inline void set_startMousePosition_6(Vector3_t2243707580  value)
	{
		___startMousePosition_6 = value;
	}

	inline static int32_t get_offset_of_realTime_7() { return static_cast<int32_t>(offsetof(FallbackCameraController_t1563223449, ___realTime_7)); }
	inline float get_realTime_7() const { return ___realTime_7; }
	inline float* get_address_of_realTime_7() { return &___realTime_7; }
	inline void set_realTime_7(float value)
	{
		___realTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
