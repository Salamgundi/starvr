﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.RenderModel_TextureMap_t
struct RenderModel_TextureMap_t_t1828165156;
struct RenderModel_TextureMap_t_t1828165156_marshaled_pinvoke;
struct RenderModel_TextureMap_t_t1828165156_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct RenderModel_TextureMap_t_t1828165156;
struct RenderModel_TextureMap_t_t1828165156_marshaled_pinvoke;

extern "C" void RenderModel_TextureMap_t_t1828165156_marshal_pinvoke(const RenderModel_TextureMap_t_t1828165156& unmarshaled, RenderModel_TextureMap_t_t1828165156_marshaled_pinvoke& marshaled);
extern "C" void RenderModel_TextureMap_t_t1828165156_marshal_pinvoke_back(const RenderModel_TextureMap_t_t1828165156_marshaled_pinvoke& marshaled, RenderModel_TextureMap_t_t1828165156& unmarshaled);
extern "C" void RenderModel_TextureMap_t_t1828165156_marshal_pinvoke_cleanup(RenderModel_TextureMap_t_t1828165156_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct RenderModel_TextureMap_t_t1828165156;
struct RenderModel_TextureMap_t_t1828165156_marshaled_com;

extern "C" void RenderModel_TextureMap_t_t1828165156_marshal_com(const RenderModel_TextureMap_t_t1828165156& unmarshaled, RenderModel_TextureMap_t_t1828165156_marshaled_com& marshaled);
extern "C" void RenderModel_TextureMap_t_t1828165156_marshal_com_back(const RenderModel_TextureMap_t_t1828165156_marshaled_com& marshaled, RenderModel_TextureMap_t_t1828165156& unmarshaled);
extern "C" void RenderModel_TextureMap_t_t1828165156_marshal_com_cleanup(RenderModel_TextureMap_t_t1828165156_marshaled_com& marshaled);
