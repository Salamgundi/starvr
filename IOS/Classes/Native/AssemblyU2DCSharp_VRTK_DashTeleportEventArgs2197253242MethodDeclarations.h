﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.DashTeleportEventArgs
struct DashTeleportEventArgs_t2197253242;
struct DashTeleportEventArgs_t2197253242_marshaled_pinvoke;
struct DashTeleportEventArgs_t2197253242_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct DashTeleportEventArgs_t2197253242;
struct DashTeleportEventArgs_t2197253242_marshaled_pinvoke;

extern "C" void DashTeleportEventArgs_t2197253242_marshal_pinvoke(const DashTeleportEventArgs_t2197253242& unmarshaled, DashTeleportEventArgs_t2197253242_marshaled_pinvoke& marshaled);
extern "C" void DashTeleportEventArgs_t2197253242_marshal_pinvoke_back(const DashTeleportEventArgs_t2197253242_marshaled_pinvoke& marshaled, DashTeleportEventArgs_t2197253242& unmarshaled);
extern "C" void DashTeleportEventArgs_t2197253242_marshal_pinvoke_cleanup(DashTeleportEventArgs_t2197253242_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct DashTeleportEventArgs_t2197253242;
struct DashTeleportEventArgs_t2197253242_marshaled_com;

extern "C" void DashTeleportEventArgs_t2197253242_marshal_com(const DashTeleportEventArgs_t2197253242& unmarshaled, DashTeleportEventArgs_t2197253242_marshaled_com& marshaled);
extern "C" void DashTeleportEventArgs_t2197253242_marshal_com_back(const DashTeleportEventArgs_t2197253242_marshaled_com& marshaled, DashTeleportEventArgs_t2197253242& unmarshaled);
extern "C" void DashTeleportEventArgs_t2197253242_marshal_com_cleanup(DashTeleportEventArgs_t2197253242_marshaled_com& marshaled);
