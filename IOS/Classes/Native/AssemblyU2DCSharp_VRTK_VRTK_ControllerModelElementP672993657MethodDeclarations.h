﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ControllerModelElementPaths
struct VRTK_ControllerModelElementPaths_t672993657;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_ControllerModelElementPaths::.ctor()
extern "C"  void VRTK_ControllerModelElementPaths__ctor_m2914815829 (VRTK_ControllerModelElementPaths_t672993657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
