﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.IgnoreHovering
struct  IgnoreHovering_t476670048  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.IgnoreHovering::onlyIgnoreHand
	Hand_t379716353 * ___onlyIgnoreHand_2;

public:
	inline static int32_t get_offset_of_onlyIgnoreHand_2() { return static_cast<int32_t>(offsetof(IgnoreHovering_t476670048, ___onlyIgnoreHand_2)); }
	inline Hand_t379716353 * get_onlyIgnoreHand_2() const { return ___onlyIgnoreHand_2; }
	inline Hand_t379716353 ** get_address_of_onlyIgnoreHand_2() { return &___onlyIgnoreHand_2; }
	inline void set_onlyIgnoreHand_2(Hand_t379716353 * value)
	{
		___onlyIgnoreHand_2 = value;
		Il2CppCodeGenWriteBarrier(&___onlyIgnoreHand_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
