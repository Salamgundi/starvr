﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.CombineInstance>
struct List_1_t3728683638;
// System.Collections.Generic.IEnumerable`1<UnityEngine.CombineInstance>
struct IEnumerable_1_t356722255;
// UnityEngine.CombineInstance[]
struct CombineInstanceU5BU5D_t1231324047;
// System.Collections.Generic.IEnumerator`1<UnityEngine.CombineInstance>
struct IEnumerator_1_t1835086333;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.CombineInstance>
struct ICollection_1_t1016670515;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CombineInstance>
struct ReadOnlyCollection_1_t250380902;
// System.Predicate`1<UnityEngine.CombineInstance>
struct Predicate_1_t2802532621;
// System.Comparison`1<UnityEngine.CombineInstance>
struct Comparison_1_t1326334061;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_CombineInstance64595210.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3263413312.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::.ctor()
extern "C"  void List_1__ctor_m1327325092_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1__ctor_m1327325092(__this, method) ((  void (*) (List_1_t3728683638 *, const MethodInfo*))List_1__ctor_m1327325092_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m635014796_gshared (List_1_t3728683638 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m635014796(__this, ___collection0, method) ((  void (*) (List_1_t3728683638 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m635014796_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m2329333458_gshared (List_1_t3728683638 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m2329333458(__this, ___capacity0, method) ((  void (*) (List_1_t3728683638 *, int32_t, const MethodInfo*))List_1__ctor_m2329333458_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m1562761710_gshared (List_1_t3728683638 * __this, CombineInstanceU5BU5D_t1231324047* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m1562761710(__this, ___data0, ___size1, method) ((  void (*) (List_1_t3728683638 *, CombineInstanceU5BU5D_t1231324047*, int32_t, const MethodInfo*))List_1__ctor_m1562761710_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::.cctor()
extern "C"  void List_1__cctor_m1093425274_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1093425274(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1093425274_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4267845353_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4267845353(__this, method) ((  Il2CppObject* (*) (List_1_t3728683638 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4267845353_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1579469245_gshared (List_1_t3728683638 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1579469245(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3728683638 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1579469245_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3769183100_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3769183100(__this, method) ((  Il2CppObject * (*) (List_1_t3728683638 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3769183100_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4003858273_gshared (List_1_t3728683638 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m4003858273(__this, ___item0, method) ((  int32_t (*) (List_1_t3728683638 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m4003858273_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3724439341_gshared (List_1_t3728683638 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3724439341(__this, ___item0, method) ((  bool (*) (List_1_t3728683638 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3724439341_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2411739591_gshared (List_1_t3728683638 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2411739591(__this, ___item0, method) ((  int32_t (*) (List_1_t3728683638 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2411739591_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3671054130_gshared (List_1_t3728683638 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3671054130(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3728683638 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3671054130_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1865915936_gshared (List_1_t3728683638 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1865915936(__this, ___item0, method) ((  void (*) (List_1_t3728683638 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1865915936_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1191164040_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1191164040(__this, method) ((  bool (*) (List_1_t3728683638 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1191164040_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3383848937_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3383848937(__this, method) ((  bool (*) (List_1_t3728683638 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3383848937_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3940676557_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3940676557(__this, method) ((  Il2CppObject * (*) (List_1_t3728683638 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3940676557_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1739299682_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1739299682(__this, method) ((  bool (*) (List_1_t3728683638 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1739299682_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m4255914517_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m4255914517(__this, method) ((  bool (*) (List_1_t3728683638 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m4255914517_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m500902214_gshared (List_1_t3728683638 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m500902214(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3728683638 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m500902214_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3556645919_gshared (List_1_t3728683638 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3556645919(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3728683638 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3556645919_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::Add(T)
extern "C"  void List_1_Add_m1306177680_gshared (List_1_t3728683638 * __this, CombineInstance_t64595210  ___item0, const MethodInfo* method);
#define List_1_Add_m1306177680(__this, ___item0, method) ((  void (*) (List_1_t3728683638 *, CombineInstance_t64595210 , const MethodInfo*))List_1_Add_m1306177680_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1639317541_gshared (List_1_t3728683638 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1639317541(__this, ___newCount0, method) ((  void (*) (List_1_t3728683638 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1639317541_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m1654083298_gshared (List_1_t3728683638 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m1654083298(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3728683638 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1654083298_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2097675973_gshared (List_1_t3728683638 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2097675973(__this, ___collection0, method) ((  void (*) (List_1_t3728683638 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2097675973_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1001886485_gshared (List_1_t3728683638 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1001886485(__this, ___enumerable0, method) ((  void (*) (List_1_t3728683638 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1001886485_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3807371792_gshared (List_1_t3728683638 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3807371792(__this, ___collection0, method) ((  void (*) (List_1_t3728683638 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3807371792_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.CombineInstance>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t250380902 * List_1_AsReadOnly_m3208778157_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3208778157(__this, method) ((  ReadOnlyCollection_1_t250380902 * (*) (List_1_t3728683638 *, const MethodInfo*))List_1_AsReadOnly_m3208778157_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::Clear()
extern "C"  void List_1_Clear_m3656003874_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_Clear_m3656003874(__this, method) ((  void (*) (List_1_t3728683638 *, const MethodInfo*))List_1_Clear_m3656003874_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.CombineInstance>::Contains(T)
extern "C"  bool List_1_Contains_m4168474496_gshared (List_1_t3728683638 * __this, CombineInstance_t64595210  ___item0, const MethodInfo* method);
#define List_1_Contains_m4168474496(__this, ___item0, method) ((  bool (*) (List_1_t3728683638 *, CombineInstance_t64595210 , const MethodInfo*))List_1_Contains_m4168474496_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m180871566_gshared (List_1_t3728683638 * __this, CombineInstanceU5BU5D_t1231324047* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m180871566(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3728683638 *, CombineInstanceU5BU5D_t1231324047*, int32_t, const MethodInfo*))List_1_CopyTo_m180871566_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.CombineInstance>::Find(System.Predicate`1<T>)
extern "C"  CombineInstance_t64595210  List_1_Find_m3461474926_gshared (List_1_t3728683638 * __this, Predicate_1_t2802532621 * ___match0, const MethodInfo* method);
#define List_1_Find_m3461474926(__this, ___match0, method) ((  CombineInstance_t64595210  (*) (List_1_t3728683638 *, Predicate_1_t2802532621 *, const MethodInfo*))List_1_Find_m3461474926_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1898821845_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2802532621 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1898821845(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2802532621 *, const MethodInfo*))List_1_CheckMatch_m1898821845_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.CombineInstance>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t3728683638 * List_1_FindAll_m2134421957_gshared (List_1_t3728683638 * __this, Predicate_1_t2802532621 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m2134421957(__this, ___match0, method) ((  List_1_t3728683638 * (*) (List_1_t3728683638 *, Predicate_1_t2802532621 *, const MethodInfo*))List_1_FindAll_m2134421957_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.CombineInstance>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t3728683638 * List_1_FindAllStackBits_m1587348387_gshared (List_1_t3728683638 * __this, Predicate_1_t2802532621 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m1587348387(__this, ___match0, method) ((  List_1_t3728683638 * (*) (List_1_t3728683638 *, Predicate_1_t2802532621 *, const MethodInfo*))List_1_FindAllStackBits_m1587348387_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.CombineInstance>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t3728683638 * List_1_FindAllList_m150340339_gshared (List_1_t3728683638 * __this, Predicate_1_t2802532621 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m150340339(__this, ___match0, method) ((  List_1_t3728683638 * (*) (List_1_t3728683638 *, Predicate_1_t2802532621 *, const MethodInfo*))List_1_FindAllList_m150340339_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CombineInstance>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m3336142353_gshared (List_1_t3728683638 * __this, Predicate_1_t2802532621 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m3336142353(__this, ___match0, method) ((  int32_t (*) (List_1_t3728683638 *, Predicate_1_t2802532621 *, const MethodInfo*))List_1_FindIndex_m3336142353_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CombineInstance>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2498001686_gshared (List_1_t3728683638 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2802532621 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2498001686(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3728683638 *, int32_t, int32_t, Predicate_1_t2802532621 *, const MethodInfo*))List_1_GetIndex_m2498001686_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.CombineInstance>::GetEnumerator()
extern "C"  Enumerator_t3263413312  List_1_GetEnumerator_m126474469_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m126474469(__this, method) ((  Enumerator_t3263413312  (*) (List_1_t3728683638 *, const MethodInfo*))List_1_GetEnumerator_m126474469_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CombineInstance>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3814337340_gshared (List_1_t3728683638 * __this, CombineInstance_t64595210  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3814337340(__this, ___item0, method) ((  int32_t (*) (List_1_t3728683638 *, CombineInstance_t64595210 , const MethodInfo*))List_1_IndexOf_m3814337340_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3519985929_gshared (List_1_t3728683638 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3519985929(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3728683638 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3519985929_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m222054794_gshared (List_1_t3728683638 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m222054794(__this, ___index0, method) ((  void (*) (List_1_t3728683638 *, int32_t, const MethodInfo*))List_1_CheckIndex_m222054794_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1795601199_gshared (List_1_t3728683638 * __this, int32_t ___index0, CombineInstance_t64595210  ___item1, const MethodInfo* method);
#define List_1_Insert_m1795601199(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3728683638 *, int32_t, CombineInstance_t64595210 , const MethodInfo*))List_1_Insert_m1795601199_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3020828920_gshared (List_1_t3728683638 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3020828920(__this, ___collection0, method) ((  void (*) (List_1_t3728683638 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3020828920_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.CombineInstance>::Remove(T)
extern "C"  bool List_1_Remove_m2999136131_gshared (List_1_t3728683638 * __this, CombineInstance_t64595210  ___item0, const MethodInfo* method);
#define List_1_Remove_m2999136131(__this, ___item0, method) ((  bool (*) (List_1_t3728683638 *, CombineInstance_t64595210 , const MethodInfo*))List_1_Remove_m2999136131_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CombineInstance>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1729659473_gshared (List_1_t3728683638 * __this, Predicate_1_t2802532621 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1729659473(__this, ___match0, method) ((  int32_t (*) (List_1_t3728683638 *, Predicate_1_t2802532621 *, const MethodInfo*))List_1_RemoveAll_m1729659473_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3593393051_gshared (List_1_t3728683638 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3593393051(__this, ___index0, method) ((  void (*) (List_1_t3728683638 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3593393051_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m968926734_gshared (List_1_t3728683638 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m968926734(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3728683638 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m968926734_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::Reverse()
extern "C"  void List_1_Reverse_m1910087265_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_Reverse_m1910087265(__this, method) ((  void (*) (List_1_t3728683638 *, const MethodInfo*))List_1_Reverse_m1910087265_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::Sort()
extern "C"  void List_1_Sort_m587918419_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_Sort_m587918419(__this, method) ((  void (*) (List_1_t3728683638 *, const MethodInfo*))List_1_Sort_m587918419_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1968733794_gshared (List_1_t3728683638 * __this, Comparison_1_t1326334061 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1968733794(__this, ___comparison0, method) ((  void (*) (List_1_t3728683638 *, Comparison_1_t1326334061 *, const MethodInfo*))List_1_Sort_m1968733794_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.CombineInstance>::ToArray()
extern "C"  CombineInstanceU5BU5D_t1231324047* List_1_ToArray_m2131648406_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_ToArray_m2131648406(__this, method) ((  CombineInstanceU5BU5D_t1231324047* (*) (List_1_t3728683638 *, const MethodInfo*))List_1_ToArray_m2131648406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2251522764_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2251522764(__this, method) ((  void (*) (List_1_t3728683638 *, const MethodInfo*))List_1_TrimExcess_m2251522764_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CombineInstance>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2640677374_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2640677374(__this, method) ((  int32_t (*) (List_1_t3728683638 *, const MethodInfo*))List_1_get_Capacity_m2640677374_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m4198853669_gshared (List_1_t3728683638 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m4198853669(__this, ___value0, method) ((  void (*) (List_1_t3728683638 *, int32_t, const MethodInfo*))List_1_set_Capacity_m4198853669_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CombineInstance>::get_Count()
extern "C"  int32_t List_1_get_Count_m2608465201_gshared (List_1_t3728683638 * __this, const MethodInfo* method);
#define List_1_get_Count_m2608465201(__this, method) ((  int32_t (*) (List_1_t3728683638 *, const MethodInfo*))List_1_get_Count_m2608465201_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.CombineInstance>::get_Item(System.Int32)
extern "C"  CombineInstance_t64595210  List_1_get_Item_m3680609075_gshared (List_1_t3728683638 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3680609075(__this, ___index0, method) ((  CombineInstance_t64595210  (*) (List_1_t3728683638 *, int32_t, const MethodInfo*))List_1_get_Item_m3680609075_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CombineInstance>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2699821724_gshared (List_1_t3728683638 * __this, int32_t ___index0, CombineInstance_t64595210  ___value1, const MethodInfo* method);
#define List_1_set_Item_m2699821724(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3728683638 *, int32_t, CombineInstance_t64595210 , const MethodInfo*))List_1_set_Item_m2699821724_gshared)(__this, ___index0, ___value1, method)
