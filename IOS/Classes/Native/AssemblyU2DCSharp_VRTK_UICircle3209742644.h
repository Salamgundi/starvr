﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture
struct Texture_t2243626319;

#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UICircle
struct  UICircle_t3209742644  : public Graphic_t2426225576
{
public:
	// System.Int32 VRTK.UICircle::fillPercent
	int32_t ___fillPercent_19;
	// System.Boolean VRTK.UICircle::fill
	bool ___fill_20;
	// System.Int32 VRTK.UICircle::thickness
	int32_t ___thickness_21;
	// System.Int32 VRTK.UICircle::segments
	int32_t ___segments_22;
	// UnityEngine.Texture VRTK.UICircle::m_Texture
	Texture_t2243626319 * ___m_Texture_23;

public:
	inline static int32_t get_offset_of_fillPercent_19() { return static_cast<int32_t>(offsetof(UICircle_t3209742644, ___fillPercent_19)); }
	inline int32_t get_fillPercent_19() const { return ___fillPercent_19; }
	inline int32_t* get_address_of_fillPercent_19() { return &___fillPercent_19; }
	inline void set_fillPercent_19(int32_t value)
	{
		___fillPercent_19 = value;
	}

	inline static int32_t get_offset_of_fill_20() { return static_cast<int32_t>(offsetof(UICircle_t3209742644, ___fill_20)); }
	inline bool get_fill_20() const { return ___fill_20; }
	inline bool* get_address_of_fill_20() { return &___fill_20; }
	inline void set_fill_20(bool value)
	{
		___fill_20 = value;
	}

	inline static int32_t get_offset_of_thickness_21() { return static_cast<int32_t>(offsetof(UICircle_t3209742644, ___thickness_21)); }
	inline int32_t get_thickness_21() const { return ___thickness_21; }
	inline int32_t* get_address_of_thickness_21() { return &___thickness_21; }
	inline void set_thickness_21(int32_t value)
	{
		___thickness_21 = value;
	}

	inline static int32_t get_offset_of_segments_22() { return static_cast<int32_t>(offsetof(UICircle_t3209742644, ___segments_22)); }
	inline int32_t get_segments_22() const { return ___segments_22; }
	inline int32_t* get_address_of_segments_22() { return &___segments_22; }
	inline void set_segments_22(int32_t value)
	{
		___segments_22 = value;
	}

	inline static int32_t get_offset_of_m_Texture_23() { return static_cast<int32_t>(offsetof(UICircle_t3209742644, ___m_Texture_23)); }
	inline Texture_t2243626319 * get_m_Texture_23() const { return ___m_Texture_23; }
	inline Texture_t2243626319 ** get_address_of_m_Texture_23() { return &___m_Texture_23; }
	inline void set_m_Texture_23(Texture_t2243626319 * value)
	{
		___m_Texture_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_Texture_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
