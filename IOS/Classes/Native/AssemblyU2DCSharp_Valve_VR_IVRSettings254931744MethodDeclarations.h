﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSettings
struct IVRSettings_t254931744;
struct IVRSettings_t254931744_marshaled_pinvoke;
struct IVRSettings_t254931744_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IVRSettings_t254931744;
struct IVRSettings_t254931744_marshaled_pinvoke;

extern "C" void IVRSettings_t254931744_marshal_pinvoke(const IVRSettings_t254931744& unmarshaled, IVRSettings_t254931744_marshaled_pinvoke& marshaled);
extern "C" void IVRSettings_t254931744_marshal_pinvoke_back(const IVRSettings_t254931744_marshaled_pinvoke& marshaled, IVRSettings_t254931744& unmarshaled);
extern "C" void IVRSettings_t254931744_marshal_pinvoke_cleanup(IVRSettings_t254931744_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IVRSettings_t254931744;
struct IVRSettings_t254931744_marshaled_com;

extern "C" void IVRSettings_t254931744_marshal_com(const IVRSettings_t254931744& unmarshaled, IVRSettings_t254931744_marshaled_com& marshaled);
extern "C" void IVRSettings_t254931744_marshal_com_back(const IVRSettings_t254931744_marshaled_com& marshaled, IVRSettings_t254931744& unmarshaled);
extern "C" void IVRSettings_t254931744_marshal_com_cleanup(IVRSettings_t254931744_marshaled_com& marshaled);
