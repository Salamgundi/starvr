﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_OculusVRSystem
struct SDK_OculusVRSystem_t1733635573;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_OculusVRSystem::.ctor()
extern "C"  void SDK_OculusVRSystem__ctor_m2602764735 (SDK_OculusVRSystem_t1733635573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
