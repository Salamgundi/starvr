﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRScreenshots
struct IVRScreenshots_t1006836234;
struct IVRScreenshots_t1006836234_marshaled_pinvoke;
struct IVRScreenshots_t1006836234_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IVRScreenshots_t1006836234;
struct IVRScreenshots_t1006836234_marshaled_pinvoke;

extern "C" void IVRScreenshots_t1006836234_marshal_pinvoke(const IVRScreenshots_t1006836234& unmarshaled, IVRScreenshots_t1006836234_marshaled_pinvoke& marshaled);
extern "C" void IVRScreenshots_t1006836234_marshal_pinvoke_back(const IVRScreenshots_t1006836234_marshaled_pinvoke& marshaled, IVRScreenshots_t1006836234& unmarshaled);
extern "C" void IVRScreenshots_t1006836234_marshal_pinvoke_cleanup(IVRScreenshots_t1006836234_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IVRScreenshots_t1006836234;
struct IVRScreenshots_t1006836234_marshaled_com;

extern "C" void IVRScreenshots_t1006836234_marshal_com(const IVRScreenshots_t1006836234& unmarshaled, IVRScreenshots_t1006836234_marshaled_com& marshaled);
extern "C" void IVRScreenshots_t1006836234_marshal_com_back(const IVRScreenshots_t1006836234_marshaled_com& marshaled, IVRScreenshots_t1006836234& unmarshaled);
extern "C" void IVRScreenshots_t1006836234_marshal_com_cleanup(IVRScreenshots_t1006836234_marshaled_com& marshaled);
