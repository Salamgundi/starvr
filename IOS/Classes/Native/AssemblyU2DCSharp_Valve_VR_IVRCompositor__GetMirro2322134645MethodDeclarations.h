﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_GetMirrorTextureD3D11
struct _GetMirrorTextureD3D11_t2322134645;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRCompositorError3948578210.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_GetMirrorTextureD3D11::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetMirrorTextureD3D11__ctor_m1467530468 (_GetMirrorTextureD3D11_t2322134645 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.IVRCompositor/_GetMirrorTextureD3D11::Invoke(Valve.VR.EVREye,System.IntPtr,System.IntPtr&)
extern "C"  int32_t _GetMirrorTextureD3D11_Invoke_m3719399089 (_GetMirrorTextureD3D11_t2322134645 * __this, int32_t ___eEye0, IntPtr_t ___pD3D11DeviceOrResource1, IntPtr_t* ___ppD3D11ShaderResourceView2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_GetMirrorTextureD3D11::BeginInvoke(Valve.VR.EVREye,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetMirrorTextureD3D11_BeginInvoke_m1925579833 (_GetMirrorTextureD3D11_t2322134645 * __this, int32_t ___eEye0, IntPtr_t ___pD3D11DeviceOrResource1, IntPtr_t* ___ppD3D11ShaderResourceView2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.IVRCompositor/_GetMirrorTextureD3D11::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern "C"  int32_t _GetMirrorTextureD3D11_EndInvoke_m1795005633 (_GetMirrorTextureD3D11_t2322134645 * __this, IntPtr_t* ___ppD3D11ShaderResourceView0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
