﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.VRTK_ControllerPointerEvents_ListenerExample
struct VRTK_ControllerPointerEvents_ListenerExample_t261181245;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventArgs1852451661.h"

// System.Void VRTK.Examples.VRTK_ControllerPointerEvents_ListenerExample::.ctor()
extern "C"  void VRTK_ControllerPointerEvents_ListenerExample__ctor_m3383598466 (VRTK_ControllerPointerEvents_ListenerExample_t261181245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerPointerEvents_ListenerExample::Start()
extern "C"  void VRTK_ControllerPointerEvents_ListenerExample_Start_m3501232550 (VRTK_ControllerPointerEvents_ListenerExample_t261181245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerPointerEvents_ListenerExample::DebugLogger(System.UInt32,System.String,UnityEngine.Transform,UnityEngine.RaycastHit,System.Single,UnityEngine.Vector3)
extern "C"  void VRTK_ControllerPointerEvents_ListenerExample_DebugLogger_m4080402543 (VRTK_ControllerPointerEvents_ListenerExample_t261181245 * __this, uint32_t ___index0, String_t* ___action1, Transform_t3275118058 * ___target2, RaycastHit_t87180320  ___raycastHit3, float ___distance4, Vector3_t2243707580  ___tipPosition5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerPointerEvents_ListenerExample::DoPointerIn(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_ControllerPointerEvents_ListenerExample_DoPointerIn_m1860463787 (VRTK_ControllerPointerEvents_ListenerExample_t261181245 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerPointerEvents_ListenerExample::DoPointerOut(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_ControllerPointerEvents_ListenerExample_DoPointerOut_m4229045432 (VRTK_ControllerPointerEvents_ListenerExample_t261181245 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerPointerEvents_ListenerExample::DoPointerDestinationSet(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_ControllerPointerEvents_ListenerExample_DoPointerDestinationSet_m1058079658 (VRTK_ControllerPointerEvents_ListenerExample_t261181245 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
