﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0
struct U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0::.ctor()
extern "C"  void U3CDelayedSetColliderEnabledU3Ec__Iterator0__ctor_m2550899377 (U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0::MoveNext()
extern "C"  bool U3CDelayedSetColliderEnabledU3Ec__Iterator0_MoveNext_m1347754751 (U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayedSetColliderEnabledU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2770544811 (U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayedSetColliderEnabledU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1111904963 (U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0::Dispose()
extern "C"  void U3CDelayedSetColliderEnabledU3Ec__Iterator0_Dispose_m2560569748 (U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0::Reset()
extern "C"  void U3CDelayedSetColliderEnabledU3Ec__Iterator0_Reset_m3232931878 (U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
