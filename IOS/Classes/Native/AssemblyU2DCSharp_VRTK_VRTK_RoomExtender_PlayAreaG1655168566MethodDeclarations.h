﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_RoomExtender_PlayAreaGizmo
struct VRTK_RoomExtender_PlayAreaGizmo_t1655168566;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_RoomExtender_PlayAreaGizmo::.ctor()
extern "C"  void VRTK_RoomExtender_PlayAreaGizmo__ctor_m1345089544 (VRTK_RoomExtender_PlayAreaGizmo_t1655168566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RoomExtender_PlayAreaGizmo::Awake()
extern "C"  void VRTK_RoomExtender_PlayAreaGizmo_Awake_m3754329211 (VRTK_RoomExtender_PlayAreaGizmo_t1655168566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RoomExtender_PlayAreaGizmo::OnDrawGizmos()
extern "C"  void VRTK_RoomExtender_PlayAreaGizmo_OnDrawGizmos_m3194940642 (VRTK_RoomExtender_PlayAreaGizmo_t1655168566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RoomExtender_PlayAreaGizmo::OnDrawGizmosSelected()
extern "C"  void VRTK_RoomExtender_PlayAreaGizmo_OnDrawGizmosSelected_m3210152427 (VRTK_RoomExtender_PlayAreaGizmo_t1655168566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RoomExtender_PlayAreaGizmo::DrawWireframe()
extern "C"  void VRTK_RoomExtender_PlayAreaGizmo_DrawWireframe_m909987648 (VRTK_RoomExtender_PlayAreaGizmo_t1655168566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
