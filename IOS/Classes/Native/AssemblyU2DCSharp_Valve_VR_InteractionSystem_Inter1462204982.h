﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.TextMesh
struct TextMesh_t1641806576;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1543711741.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.InteractableExample
struct  InteractableExample_t1462204982  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.TextMesh Valve.VR.InteractionSystem.InteractableExample::textMesh
	TextMesh_t1641806576 * ___textMesh_2;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.InteractableExample::oldPosition
	Vector3_t2243707580  ___oldPosition_3;
	// UnityEngine.Quaternion Valve.VR.InteractionSystem.InteractableExample::oldRotation
	Quaternion_t4030073918  ___oldRotation_4;
	// System.Single Valve.VR.InteractionSystem.InteractableExample::attachTime
	float ___attachTime_5;
	// Valve.VR.InteractionSystem.Hand/AttachmentFlags Valve.VR.InteractionSystem.InteractableExample::attachmentFlags
	int32_t ___attachmentFlags_6;

public:
	inline static int32_t get_offset_of_textMesh_2() { return static_cast<int32_t>(offsetof(InteractableExample_t1462204982, ___textMesh_2)); }
	inline TextMesh_t1641806576 * get_textMesh_2() const { return ___textMesh_2; }
	inline TextMesh_t1641806576 ** get_address_of_textMesh_2() { return &___textMesh_2; }
	inline void set_textMesh_2(TextMesh_t1641806576 * value)
	{
		___textMesh_2 = value;
		Il2CppCodeGenWriteBarrier(&___textMesh_2, value);
	}

	inline static int32_t get_offset_of_oldPosition_3() { return static_cast<int32_t>(offsetof(InteractableExample_t1462204982, ___oldPosition_3)); }
	inline Vector3_t2243707580  get_oldPosition_3() const { return ___oldPosition_3; }
	inline Vector3_t2243707580 * get_address_of_oldPosition_3() { return &___oldPosition_3; }
	inline void set_oldPosition_3(Vector3_t2243707580  value)
	{
		___oldPosition_3 = value;
	}

	inline static int32_t get_offset_of_oldRotation_4() { return static_cast<int32_t>(offsetof(InteractableExample_t1462204982, ___oldRotation_4)); }
	inline Quaternion_t4030073918  get_oldRotation_4() const { return ___oldRotation_4; }
	inline Quaternion_t4030073918 * get_address_of_oldRotation_4() { return &___oldRotation_4; }
	inline void set_oldRotation_4(Quaternion_t4030073918  value)
	{
		___oldRotation_4 = value;
	}

	inline static int32_t get_offset_of_attachTime_5() { return static_cast<int32_t>(offsetof(InteractableExample_t1462204982, ___attachTime_5)); }
	inline float get_attachTime_5() const { return ___attachTime_5; }
	inline float* get_address_of_attachTime_5() { return &___attachTime_5; }
	inline void set_attachTime_5(float value)
	{
		___attachTime_5 = value;
	}

	inline static int32_t get_offset_of_attachmentFlags_6() { return static_cast<int32_t>(offsetof(InteractableExample_t1462204982, ___attachmentFlags_6)); }
	inline int32_t get_attachmentFlags_6() const { return ___attachmentFlags_6; }
	inline int32_t* get_address_of_attachmentFlags_6() { return &___attachmentFlags_6; }
	inline void set_attachmentFlags_6(int32_t value)
	{
		___attachmentFlags_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
