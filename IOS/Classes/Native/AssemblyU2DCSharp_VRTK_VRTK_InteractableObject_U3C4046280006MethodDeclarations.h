﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_InteractableObject/<ForceStopInteractingAtEndOfFrame>c__Iterator1
struct U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_t4046280006;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_InteractableObject/<ForceStopInteractingAtEndOfFrame>c__Iterator1::.ctor()
extern "C"  void U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1__ctor_m1436898091 (U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_t4046280006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractableObject/<ForceStopInteractingAtEndOfFrame>c__Iterator1::MoveNext()
extern "C"  bool U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_MoveNext_m3576830753 (U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_t4046280006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_InteractableObject/<ForceStopInteractingAtEndOfFrame>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2068901313 (U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_t4046280006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_InteractableObject/<ForceStopInteractingAtEndOfFrame>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3790832793 (U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_t4046280006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject/<ForceStopInteractingAtEndOfFrame>c__Iterator1::Dispose()
extern "C"  void U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_Dispose_m4171130024 (U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_t4046280006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject/<ForceStopInteractingAtEndOfFrame>c__Iterator1::Reset()
extern "C"  void U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_Reset_m4044103974 (U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_t4046280006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
