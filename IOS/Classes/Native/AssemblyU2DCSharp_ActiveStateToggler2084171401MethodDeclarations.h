﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActiveStateToggler
struct ActiveStateToggler_t2084171401;

#include "codegen/il2cpp-codegen.h"

// System.Void ActiveStateToggler::.ctor()
extern "C"  void ActiveStateToggler__ctor_m1259892638 (ActiveStateToggler_t2084171401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveStateToggler::ToggleActive()
extern "C"  void ActiveStateToggler_ToggleActive_m3282966406 (ActiveStateToggler_t2084171401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
