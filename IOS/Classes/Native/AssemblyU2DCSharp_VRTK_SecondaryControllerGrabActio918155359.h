﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_SecondaryControllerGrabActi4095736311.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.SecondaryControllerGrabActions.VRTK_SwapControllerGrabAction
struct  VRTK_SwapControllerGrabAction_t918155359  : public VRTK_BaseGrabAction_t4095736311
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
