﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Int32>
struct AdaptiveSetting_1_t744429243;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Int32>::.ctor(T,System.Int32,System.Int32)
extern "C"  void AdaptiveSetting_1__ctor_m2088995240_gshared (AdaptiveSetting_1_t744429243 * __this, int32_t ___currentValue0, int32_t ___increaseFrameCost1, int32_t ___decreaseFrameCost2, const MethodInfo* method);
#define AdaptiveSetting_1__ctor_m2088995240(__this, ___currentValue0, ___increaseFrameCost1, ___decreaseFrameCost2, method) ((  void (*) (AdaptiveSetting_1_t744429243 *, int32_t, int32_t, int32_t, const MethodInfo*))AdaptiveSetting_1__ctor_m2088995240_gshared)(__this, ___currentValue0, ___increaseFrameCost1, ___decreaseFrameCost2, method)
// T VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Int32>::get_currentValue()
extern "C"  int32_t AdaptiveSetting_1_get_currentValue_m3150156286_gshared (AdaptiveSetting_1_t744429243 * __this, const MethodInfo* method);
#define AdaptiveSetting_1_get_currentValue_m3150156286(__this, method) ((  int32_t (*) (AdaptiveSetting_1_t744429243 *, const MethodInfo*))AdaptiveSetting_1_get_currentValue_m3150156286_gshared)(__this, method)
// System.Void VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Int32>::set_currentValue(T)
extern "C"  void AdaptiveSetting_1_set_currentValue_m4173575659_gshared (AdaptiveSetting_1_t744429243 * __this, int32_t ___value0, const MethodInfo* method);
#define AdaptiveSetting_1_set_currentValue_m4173575659(__this, ___value0, method) ((  void (*) (AdaptiveSetting_1_t744429243 *, int32_t, const MethodInfo*))AdaptiveSetting_1_set_currentValue_m4173575659_gshared)(__this, ___value0, method)
// T VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Int32>::get_previousValue()
extern "C"  int32_t AdaptiveSetting_1_get_previousValue_m2245131892_gshared (AdaptiveSetting_1_t744429243 * __this, const MethodInfo* method);
#define AdaptiveSetting_1_get_previousValue_m2245131892(__this, method) ((  int32_t (*) (AdaptiveSetting_1_t744429243 *, const MethodInfo*))AdaptiveSetting_1_get_previousValue_m2245131892_gshared)(__this, method)
// System.Void VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Int32>::set_previousValue(T)
extern "C"  void AdaptiveSetting_1_set_previousValue_m793975927_gshared (AdaptiveSetting_1_t744429243 * __this, int32_t ___value0, const MethodInfo* method);
#define AdaptiveSetting_1_set_previousValue_m793975927(__this, ___value0, method) ((  void (*) (AdaptiveSetting_1_t744429243 *, int32_t, const MethodInfo*))AdaptiveSetting_1_set_previousValue_m793975927_gshared)(__this, ___value0, method)
// System.Int32 VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Int32>::get_lastChangeFrameCount()
extern "C"  int32_t AdaptiveSetting_1_get_lastChangeFrameCount_m1079689865_gshared (AdaptiveSetting_1_t744429243 * __this, const MethodInfo* method);
#define AdaptiveSetting_1_get_lastChangeFrameCount_m1079689865(__this, method) ((  int32_t (*) (AdaptiveSetting_1_t744429243 *, const MethodInfo*))AdaptiveSetting_1_get_lastChangeFrameCount_m1079689865_gshared)(__this, method)
// System.Void VRTK.VRTK_AdaptiveQuality/AdaptiveSetting`1<System.Int32>::set_lastChangeFrameCount(System.Int32)
extern "C"  void AdaptiveSetting_1_set_lastChangeFrameCount_m2527549730_gshared (AdaptiveSetting_1_t744429243 * __this, int32_t ___value0, const MethodInfo* method);
#define AdaptiveSetting_1_set_lastChangeFrameCount_m2527549730(__this, ___value0, method) ((  void (*) (AdaptiveSetting_1_t744429243 *, int32_t, const MethodInfo*))AdaptiveSetting_1_set_lastChangeFrameCount_m2527549730_gshared)(__this, ___value0, method)
