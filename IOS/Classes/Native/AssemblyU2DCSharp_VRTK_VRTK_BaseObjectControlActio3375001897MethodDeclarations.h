﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_BaseObjectControlAction
struct VRTK_BaseObjectControlAction_t3375001897;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ObjectControlEventArgs2459490319.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void VRTK.VRTK_BaseObjectControlAction::.ctor()
extern "C"  void VRTK_BaseObjectControlAction__ctor_m2598474563 (VRTK_BaseObjectControlAction_t3375001897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BaseObjectControlAction::OnEnable()
extern "C"  void VRTK_BaseObjectControlAction_OnEnable_m3563341911 (VRTK_BaseObjectControlAction_t3375001897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BaseObjectControlAction::OnDisable()
extern "C"  void VRTK_BaseObjectControlAction_OnDisable_m728346662 (VRTK_BaseObjectControlAction_t3375001897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BaseObjectControlAction::AxisChanged(System.Object,VRTK.ObjectControlEventArgs)
extern "C"  void VRTK_BaseObjectControlAction_AxisChanged_m1888349718 (VRTK_BaseObjectControlAction_t3375001897 * __this, Il2CppObject * ___sender0, ObjectControlEventArgs_t2459490319  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BaseObjectControlAction::RotateAroundPlayer(UnityEngine.GameObject,System.Single)
extern "C"  void VRTK_BaseObjectControlAction_RotateAroundPlayer_m1817613303 (VRTK_BaseObjectControlAction_t3375001897 * __this, GameObject_t1756533147 * ___controlledGameObject0, float ___angle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BaseObjectControlAction::Blink(System.Single)
extern "C"  void VRTK_BaseObjectControlAction_Blink_m3363374256 (VRTK_BaseObjectControlAction_t3375001897 * __this, float ___blinkSpeed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BaseObjectControlAction::ReleaseBlink(System.Single)
extern "C"  void VRTK_BaseObjectControlAction_ReleaseBlink_m3901298905 (VRTK_BaseObjectControlAction_t3375001897 * __this, float ___blinkSpeed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_BaseObjectControlAction::GetObjectCenter(UnityEngine.Transform)
extern "C"  Vector3_t2243707580  VRTK_BaseObjectControlAction_GetObjectCenter_m1722481722 (VRTK_BaseObjectControlAction_t3375001897 * __this, Transform_t3275118058 * ___checkObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VRTK.VRTK_BaseObjectControlAction::GetAxisDirection(System.Single)
extern "C"  int32_t VRTK_BaseObjectControlAction_GetAxisDirection_m897733982 (VRTK_BaseObjectControlAction_t3375001897 * __this, float ___axis0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
