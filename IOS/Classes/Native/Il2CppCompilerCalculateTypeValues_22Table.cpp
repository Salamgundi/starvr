﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Chape1068548853.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Chape3843791664.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Ignore333906550.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Telepo865564691.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Telep1343927090.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Telep2123987351.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Telep1950611660.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Telep1112706968.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Telep3942003985.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Telep4220181178.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem3365196000.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetRecommend4195542627.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetProjectio2621141914.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetProjectio3426995441.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__ComputeDisto3576284924.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetEyeToHead3057184772.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetTimeSince1215702688.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetD3D9Adapt4234979703.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetDXGIOutpu1897151767.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__IsDisplayOnD2551312917.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__SetDisplayVi3986281708.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetDeviceToA1432625068.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__ResetSeatedZ3471614486.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetSeatedZero102610835.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetRawZeroPo1986385273.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetSortedTra3492202929.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetTrackedDev212130385.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__ApplyTransfo1439808290.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetTrackedDe3232960147.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetControlle1728202579.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetTrackedDe1455580370.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__IsTrackedDevi459208129.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetBoolTrack2236257287.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetFloatTrac1406950913.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetInt32Trac2396289227.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetUint64Trac537540785.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetMatrix34T3426445457.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetStringTrack87797800.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetPropError1193025139.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__PollNextEven3908295690.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__PollNextEven2759121141.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetEventType1950138544.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetHiddenAre1813422502.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetControlle3891090487.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetControlle4079915850.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__TriggerHaptic158863722.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetButtonIdNa195009473.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__GetControlle3568402941.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__CaptureInput2994096092.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__ReleaseInputF580725753.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__IsInputFocusCa84136089.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__DriverDebugR4049208724.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__PerformFirmwa673402879.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__AcknowledgeQ1109677234.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRSystem__AcknowledgeQui90686541.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRExtendedDisplay2045258050.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRExtendedDisplay__Get2171929041.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRExtendedDisplay__GetE693377854.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRExtendedDisplay__Get2084284319.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRTrackedCamera2005230018.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRTrackedCamera__GetCa1661571561.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRTrackedCamera__HasCa2352646991.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRTrackedCamera__GetCa1973698407.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRTrackedCamera__GetCa2604844975.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRTrackedCamera__GetCa2867851566.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRTrackedCamera__Acqui1559262216.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRTrackedCamera__Relea1473645041.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRTrackedCamera__GetVid580267038.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRTrackedCamera__GetVi2646805685.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRTrackedCamera__GetVi1327133227.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRTrackedCamera__GetVi1897576655.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRTrackedCamera__Relea3809094800.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications2340401530.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__AddAppl767630098.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__Remove1836596693.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__IsAppl2595723848.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetApp2482336573.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetApp2366899296.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetAppl114599352.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__LaunchA851978817.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__Launch1486019733.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__Launch1201572535.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__Launch3582840745.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__Cancel2074441983.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__Identi1775622238.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetApp1419106538.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetApp3031121943.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetApp2585650982.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetApp3564886007.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetApp3532011289.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__SetApp1014802842.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetAppl253949742.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__SetDef2776386992.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetDef1319680284.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetApp1223733271.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetApp1260006687.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetApp3777004763.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetSta4154880362.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetTran623155336.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__Perfor1375004597.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (ChaperoneInfo_t1068548853), -1, sizeof(ChaperoneInfo_t1068548853_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2200[6] = 
{
	ChaperoneInfo_t1068548853::get_offset_of_U3CinitializedU3Ek__BackingField_2(),
	ChaperoneInfo_t1068548853::get_offset_of_U3CplayAreaSizeXU3Ek__BackingField_3(),
	ChaperoneInfo_t1068548853::get_offset_of_U3CplayAreaSizeZU3Ek__BackingField_4(),
	ChaperoneInfo_t1068548853::get_offset_of_U3CroomscaleU3Ek__BackingField_5(),
	ChaperoneInfo_t1068548853_StaticFields::get_offset_of_Initialized_6(),
	ChaperoneInfo_t1068548853_StaticFields::get_offset_of__instance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (U3CStartU3Ec__Iterator0_t3843791664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[7] = 
{
	U3CStartU3Ec__Iterator0_t3843791664::get_offset_of_U3CchaperoneU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3843791664::get_offset_of_U3CpxU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3843791664::get_offset_of_U3CpzU3E__2_2(),
	U3CStartU3Ec__Iterator0_t3843791664::get_offset_of_U24this_3(),
	U3CStartU3Ec__Iterator0_t3843791664::get_offset_of_U24current_4(),
	U3CStartU3Ec__Iterator0_t3843791664::get_offset_of_U24disposing_5(),
	U3CStartU3Ec__Iterator0_t3843791664::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (IgnoreTeleportTrace_t333906550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (Teleport_t865564691), -1, sizeof(Teleport_t865564691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2203[79] = 
{
	Teleport_t865564691::get_offset_of_traceLayerMask_2(),
	Teleport_t865564691::get_offset_of_floorFixupTraceLayerMask_3(),
	Teleport_t865564691::get_offset_of_floorFixupMaximumTraceDistance_4(),
	Teleport_t865564691::get_offset_of_areaVisibleMaterial_5(),
	Teleport_t865564691::get_offset_of_areaLockedMaterial_6(),
	Teleport_t865564691::get_offset_of_areaHighlightedMaterial_7(),
	Teleport_t865564691::get_offset_of_pointVisibleMaterial_8(),
	Teleport_t865564691::get_offset_of_pointLockedMaterial_9(),
	Teleport_t865564691::get_offset_of_pointHighlightedMaterial_10(),
	Teleport_t865564691::get_offset_of_destinationReticleTransform_11(),
	Teleport_t865564691::get_offset_of_invalidReticleTransform_12(),
	Teleport_t865564691::get_offset_of_playAreaPreviewCorner_13(),
	Teleport_t865564691::get_offset_of_playAreaPreviewSide_14(),
	Teleport_t865564691::get_offset_of_pointerValidColor_15(),
	Teleport_t865564691::get_offset_of_pointerInvalidColor_16(),
	Teleport_t865564691::get_offset_of_pointerLockedColor_17(),
	Teleport_t865564691::get_offset_of_showPlayAreaMarker_18(),
	Teleport_t865564691::get_offset_of_teleportFadeTime_19(),
	Teleport_t865564691::get_offset_of_meshFadeTime_20(),
	Teleport_t865564691::get_offset_of_arcDistance_21(),
	Teleport_t865564691::get_offset_of_onActivateObjectTransform_22(),
	Teleport_t865564691::get_offset_of_onDeactivateObjectTransform_23(),
	Teleport_t865564691::get_offset_of_activateObjectTime_24(),
	Teleport_t865564691::get_offset_of_deactivateObjectTime_25(),
	Teleport_t865564691::get_offset_of_pointerAudioSource_26(),
	Teleport_t865564691::get_offset_of_loopingAudioSource_27(),
	Teleport_t865564691::get_offset_of_headAudioSource_28(),
	Teleport_t865564691::get_offset_of_reticleAudioSource_29(),
	Teleport_t865564691::get_offset_of_teleportSound_30(),
	Teleport_t865564691::get_offset_of_pointerStartSound_31(),
	Teleport_t865564691::get_offset_of_pointerLoopSound_32(),
	Teleport_t865564691::get_offset_of_pointerStopSound_33(),
	Teleport_t865564691::get_offset_of_goodHighlightSound_34(),
	Teleport_t865564691::get_offset_of_badHighlightSound_35(),
	Teleport_t865564691::get_offset_of_debugFloor_36(),
	Teleport_t865564691::get_offset_of_showOffsetReticle_37(),
	Teleport_t865564691::get_offset_of_offsetReticleTransform_38(),
	Teleport_t865564691::get_offset_of_floorDebugSphere_39(),
	Teleport_t865564691::get_offset_of_floorDebugLine_40(),
	Teleport_t865564691::get_offset_of_pointerLineRenderer_41(),
	Teleport_t865564691::get_offset_of_teleportPointerObject_42(),
	Teleport_t865564691::get_offset_of_pointerStartTransform_43(),
	Teleport_t865564691::get_offset_of_pointerHand_44(),
	Teleport_t865564691::get_offset_of_player_45(),
	Teleport_t865564691::get_offset_of_teleportArc_46(),
	Teleport_t865564691::get_offset_of_visible_47(),
	Teleport_t865564691::get_offset_of_teleportMarkers_48(),
	Teleport_t865564691::get_offset_of_pointedAtTeleportMarker_49(),
	Teleport_t865564691::get_offset_of_teleportingToMarker_50(),
	Teleport_t865564691::get_offset_of_pointedAtPosition_51(),
	Teleport_t865564691::get_offset_of_prevPointedAtPosition_52(),
	Teleport_t865564691::get_offset_of_teleporting_53(),
	Teleport_t865564691::get_offset_of_currentFadeTime_54(),
	Teleport_t865564691::get_offset_of_meshAlphaPercent_55(),
	Teleport_t865564691::get_offset_of_pointerShowStartTime_56(),
	Teleport_t865564691::get_offset_of_pointerHideStartTime_57(),
	Teleport_t865564691::get_offset_of_meshFading_58(),
	Teleport_t865564691::get_offset_of_fullTintAlpha_59(),
	Teleport_t865564691::get_offset_of_invalidReticleMinScale_60(),
	Teleport_t865564691::get_offset_of_invalidReticleMaxScale_61(),
	Teleport_t865564691::get_offset_of_invalidReticleMinScaleDistance_62(),
	Teleport_t865564691::get_offset_of_invalidReticleMaxScaleDistance_63(),
	Teleport_t865564691::get_offset_of_invalidReticleScale_64(),
	Teleport_t865564691::get_offset_of_invalidReticleTargetRotation_65(),
	Teleport_t865564691::get_offset_of_playAreaPreviewTransform_66(),
	Teleport_t865564691::get_offset_of_playAreaPreviewCorners_67(),
	Teleport_t865564691::get_offset_of_playAreaPreviewSides_68(),
	Teleport_t865564691::get_offset_of_loopingAudioMaxVolume_69(),
	Teleport_t865564691::get_offset_of_hintCoroutine_70(),
	Teleport_t865564691::get_offset_of_originalHoverLockState_71(),
	Teleport_t865564691::get_offset_of_originalHoveringInteractable_72(),
	Teleport_t865564691::get_offset_of_allowTeleportWhileAttached_73(),
	Teleport_t865564691::get_offset_of_startingFeetOffset_74(),
	Teleport_t865564691::get_offset_of_movedFeetFarEnough_75(),
	Teleport_t865564691::get_offset_of_chaperoneInfoInitializedAction_76(),
	Teleport_t865564691_StaticFields::get_offset_of_ChangeScene_77(),
	Teleport_t865564691_StaticFields::get_offset_of_Player_78(),
	Teleport_t865564691_StaticFields::get_offset_of_PlayerPre_79(),
	Teleport_t865564691_StaticFields::get_offset_of__instance_80(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[9] = 
{
	U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090::get_offset_of_U3CprevBreakTimeU3E__0_0(),
	U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090::get_offset_of_U3CprevHapticPulseTimeU3E__1_1(),
	U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090::get_offset_of_U3CpulsedU3E__2_2(),
	U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090::get_offset_of_U24locvar0_3(),
	U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090::get_offset_of_U24locvar1_4(),
	U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090::get_offset_of_U24this_5(),
	U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090::get_offset_of_U24current_6(),
	U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090::get_offset_of_U24disposing_7(),
	U3CTeleportHintCoroutineU3Ec__Iterator0_t1343927090::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (TeleportArc_t2123987351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[17] = 
{
	TeleportArc_t2123987351::get_offset_of_segmentCount_2(),
	TeleportArc_t2123987351::get_offset_of_thickness_3(),
	TeleportArc_t2123987351::get_offset_of_arcDuration_4(),
	TeleportArc_t2123987351::get_offset_of_segmentBreak_5(),
	TeleportArc_t2123987351::get_offset_of_arcSpeed_6(),
	TeleportArc_t2123987351::get_offset_of_material_7(),
	TeleportArc_t2123987351::get_offset_of_traceLayerMask_8(),
	TeleportArc_t2123987351::get_offset_of_lineRenderers_9(),
	TeleportArc_t2123987351::get_offset_of_arcTimeOffset_10(),
	TeleportArc_t2123987351::get_offset_of_prevThickness_11(),
	TeleportArc_t2123987351::get_offset_of_prevSegmentCount_12(),
	TeleportArc_t2123987351::get_offset_of_showArc_13(),
	TeleportArc_t2123987351::get_offset_of_startPos_14(),
	TeleportArc_t2123987351::get_offset_of_projectileVelocity_15(),
	TeleportArc_t2123987351::get_offset_of_useGravity_16(),
	TeleportArc_t2123987351::get_offset_of_arcObjectsTransfrom_17(),
	TeleportArc_t2123987351::get_offset_of_arcInvalid_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (TeleportArea_t1950611660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[7] = 
{
	TeleportArea_t1950611660::get_offset_of_U3CmeshBoundsU3Ek__BackingField_4(),
	TeleportArea_t1950611660::get_offset_of_areaMesh_5(),
	TeleportArea_t1950611660::get_offset_of_tintColorId_6(),
	TeleportArea_t1950611660::get_offset_of_visibleTintColor_7(),
	TeleportArea_t1950611660::get_offset_of_highlightedTintColor_8(),
	TeleportArea_t1950611660::get_offset_of_lockedTintColor_9(),
	TeleportArea_t1950611660::get_offset_of_highlighted_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (TeleportMarkerBase_t1112706968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[2] = 
{
	TeleportMarkerBase_t1112706968::get_offset_of_locked_2(),
	TeleportMarkerBase_t1112706968::get_offset_of_markerActive_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (TeleportPoint_t3942003985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[25] = 
{
	TeleportPoint_t3942003985::get_offset_of_teleportType_4(),
	TeleportPoint_t3942003985::get_offset_of_title_5(),
	TeleportPoint_t3942003985::get_offset_of_switchToScene_6(),
	TeleportPoint_t3942003985::get_offset_of_titleVisibleColor_7(),
	TeleportPoint_t3942003985::get_offset_of_titleHighlightedColor_8(),
	TeleportPoint_t3942003985::get_offset_of_titleLockedColor_9(),
	TeleportPoint_t3942003985::get_offset_of_playerSpawnPoint_10(),
	TeleportPoint_t3942003985::get_offset_of_gotReleventComponents_11(),
	TeleportPoint_t3942003985::get_offset_of_markerMesh_12(),
	TeleportPoint_t3942003985::get_offset_of_switchSceneIcon_13(),
	TeleportPoint_t3942003985::get_offset_of_moveLocationIcon_14(),
	TeleportPoint_t3942003985::get_offset_of_lockedIcon_15(),
	TeleportPoint_t3942003985::get_offset_of_pointIcon_16(),
	TeleportPoint_t3942003985::get_offset_of_lookAtJointTransform_17(),
	TeleportPoint_t3942003985::get_offset_of_animation_18(),
	TeleportPoint_t3942003985::get_offset_of_titleText_19(),
	TeleportPoint_t3942003985::get_offset_of_player_20(),
	TeleportPoint_t3942003985::get_offset_of_lookAtPosition_21(),
	TeleportPoint_t3942003985::get_offset_of_tintColorID_22(),
	TeleportPoint_t3942003985::get_offset_of_tintColor_23(),
	TeleportPoint_t3942003985::get_offset_of_titleColor_24(),
	TeleportPoint_t3942003985::get_offset_of_fullTitleAlpha_25(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (TeleportPointType_t4220181178)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2209[3] = 
{
	TeleportPointType_t4220181178::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (IVRSystem_t3365196000)+ sizeof (Il2CppObject), sizeof(IVRSystem_t3365196000_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2210[44] = 
{
	IVRSystem_t3365196000::get_offset_of_GetRecommendedRenderTargetSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetProjectionMatrix_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetProjectionRaw_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_ComputeDistortion_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetEyeToHeadTransform_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetTimeSinceLastVsync_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetD3D9AdapterIndex_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetDXGIOutputInfo_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_IsDisplayOnDesktop_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_SetDisplayVisibility_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetDeviceToAbsoluteTrackingPose_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_ResetSeatedZeroPose_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetRawZeroPoseToStandingAbsoluteTrackingPose_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetSortedTrackedDeviceIndicesOfClass_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetTrackedDeviceActivityLevel_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_ApplyTransform_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetTrackedDeviceIndexForControllerRole_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetControllerRoleForTrackedDeviceIndex_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetTrackedDeviceClass_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_IsTrackedDeviceConnected_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetBoolTrackedDeviceProperty_21() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetFloatTrackedDeviceProperty_22() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetInt32TrackedDeviceProperty_23() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetUint64TrackedDeviceProperty_24() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetMatrix34TrackedDeviceProperty_25() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetStringTrackedDeviceProperty_26() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetPropErrorNameFromEnum_27() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_PollNextEvent_28() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_PollNextEventWithPose_29() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetEventTypeNameFromEnum_30() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetHiddenAreaMesh_31() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetControllerState_32() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetControllerStateWithPose_33() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_TriggerHapticPulse_34() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetButtonIdNameFromEnum_35() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_GetControllerAxisTypeNameFromEnum_36() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_CaptureInputFocus_37() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_ReleaseInputFocus_38() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_IsInputFocusCapturedByAnotherProcess_39() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_DriverDebugRequest_40() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_PerformFirmwareUpdate_41() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_AcknowledgeQuit_Exiting_42() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRSystem_t3365196000::get_offset_of_AcknowledgeQuit_UserPrompt_43() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (_GetRecommendedRenderTargetSize_t4195542627), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (_GetProjectionMatrix_t2621141914), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (_GetProjectionRaw_t3426995441), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (_ComputeDistortion_t3576284924), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (_GetEyeToHeadTransform_t3057184772), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (_GetTimeSinceLastVsync_t1215702688), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (_GetD3D9AdapterIndex_t4234979703), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (_GetDXGIOutputInfo_t1897151767), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (_IsDisplayOnDesktop_t2551312917), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (_SetDisplayVisibility_t3986281708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (_GetDeviceToAbsoluteTrackingPose_t1432625068), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (_ResetSeatedZeroPose_t3471614486), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (_GetSeatedZeroPoseToStandingAbsoluteTrackingPose_t102610835), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (_GetRawZeroPoseToStandingAbsoluteTrackingPose_t1986385273), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (_GetSortedTrackedDeviceIndicesOfClass_t3492202929), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (_GetTrackedDeviceActivityLevel_t212130385), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (_ApplyTransform_t1439808290), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (_GetTrackedDeviceIndexForControllerRole_t3232960147), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (_GetControllerRoleForTrackedDeviceIndex_t1728202579), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (_GetTrackedDeviceClass_t1455580370), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (_IsTrackedDeviceConnected_t459208129), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (_GetBoolTrackedDeviceProperty_t2236257287), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (_GetFloatTrackedDeviceProperty_t1406950913), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (_GetInt32TrackedDeviceProperty_t2396289227), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (_GetUint64TrackedDeviceProperty_t537540785), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (_GetMatrix34TrackedDeviceProperty_t3426445457), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (_GetStringTrackedDeviceProperty_t87797800), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (_GetPropErrorNameFromEnum_t1193025139), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (_PollNextEvent_t3908295690), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (_PollNextEventWithPose_t2759121141), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (_GetEventTypeNameFromEnum_t1950138544), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (_GetHiddenAreaMesh_t1813422502), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (_GetControllerState_t3891090487), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (_GetControllerStateWithPose_t4079915850), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (_TriggerHapticPulse_t158863722), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (_GetButtonIdNameFromEnum_t195009473), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (_GetControllerAxisTypeNameFromEnum_t3568402941), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (_CaptureInputFocus_t2994096092), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (_ReleaseInputFocus_t580725753), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (_IsInputFocusCapturedByAnotherProcess_t84136089), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (_DriverDebugRequest_t4049208724), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (_PerformFirmwareUpdate_t673402879), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (_AcknowledgeQuit_Exiting_t1109677234), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (_AcknowledgeQuit_UserPrompt_t90686541), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (IVRExtendedDisplay_t2045258050)+ sizeof (Il2CppObject), sizeof(IVRExtendedDisplay_t2045258050_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2255[3] = 
{
	IVRExtendedDisplay_t2045258050::get_offset_of_GetWindowBounds_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRExtendedDisplay_t2045258050::get_offset_of_GetEyeOutputViewport_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRExtendedDisplay_t2045258050::get_offset_of_GetDXGIOutputInfo_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (_GetWindowBounds_t2171929041), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (_GetEyeOutputViewport_t693377854), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (_GetDXGIOutputInfo_t2084284319), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (IVRTrackedCamera_t2005230018)+ sizeof (Il2CppObject), sizeof(IVRTrackedCamera_t2005230018_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2259[12] = 
{
	IVRTrackedCamera_t2005230018::get_offset_of_GetCameraErrorNameFromEnum_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRTrackedCamera_t2005230018::get_offset_of_HasCamera_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRTrackedCamera_t2005230018::get_offset_of_GetCameraFrameSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRTrackedCamera_t2005230018::get_offset_of_GetCameraIntrinsics_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRTrackedCamera_t2005230018::get_offset_of_GetCameraProjection_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRTrackedCamera_t2005230018::get_offset_of_AcquireVideoStreamingService_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRTrackedCamera_t2005230018::get_offset_of_ReleaseVideoStreamingService_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRTrackedCamera_t2005230018::get_offset_of_GetVideoStreamFrameBuffer_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRTrackedCamera_t2005230018::get_offset_of_GetVideoStreamTextureSize_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRTrackedCamera_t2005230018::get_offset_of_GetVideoStreamTextureD3D11_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRTrackedCamera_t2005230018::get_offset_of_GetVideoStreamTextureGL_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRTrackedCamera_t2005230018::get_offset_of_ReleaseVideoStreamTextureGL_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (_GetCameraErrorNameFromEnum_t1661571561), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (_HasCamera_t2352646991), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (_GetCameraFrameSize_t1973698407), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (_GetCameraIntrinsics_t2604844975), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (_GetCameraProjection_t2867851566), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (_AcquireVideoStreamingService_t1559262216), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (_ReleaseVideoStreamingService_t1473645041), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (_GetVideoStreamFrameBuffer_t580267038), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (_GetVideoStreamTextureSize_t2646805685), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (_GetVideoStreamTextureD3D11_t1327133227), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (_GetVideoStreamTextureGL_t1897576655), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (_ReleaseVideoStreamTextureGL_t3809094800), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (IVRApplications_t2340401530)+ sizeof (Il2CppObject), sizeof(IVRApplications_t2340401530_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2272[31] = 
{
	IVRApplications_t2340401530::get_offset_of_AddApplicationManifest_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_RemoveApplicationManifest_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_IsApplicationInstalled_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetApplicationCount_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetApplicationKeyByIndex_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetApplicationKeyByProcessId_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_LaunchApplication_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_LaunchTemplateApplication_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_LaunchApplicationFromMimeType_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_LaunchDashboardOverlay_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_CancelApplicationLaunch_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_IdentifyApplication_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetApplicationProcessId_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetApplicationsErrorNameFromEnum_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetApplicationPropertyString_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetApplicationPropertyBool_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetApplicationPropertyUint64_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_SetApplicationAutoLaunch_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetApplicationAutoLaunch_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_SetDefaultApplicationForMimeType_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetDefaultApplicationForMimeType_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetApplicationSupportedMimeTypes_21() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetApplicationsThatSupportMimeType_22() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetApplicationLaunchArguments_23() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetStartingApplication_24() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetTransitionState_25() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_PerformApplicationPrelaunchCheck_26() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetApplicationsTransitionStateNameFromEnum_27() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_IsQuitUserPromptRequested_28() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_LaunchInternalProcess_29() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRApplications_t2340401530::get_offset_of_GetCurrentSceneProcessId_30() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (_AddApplicationManifest_t767630098), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (_RemoveApplicationManifest_t1836596693), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (_IsApplicationInstalled_t2595723848), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (_GetApplicationCount_t2482336573), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (_GetApplicationKeyByIndex_t2366899296), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (_GetApplicationKeyByProcessId_t114599352), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (_LaunchApplication_t851978817), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (_LaunchTemplateApplication_t1486019733), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (_LaunchApplicationFromMimeType_t1201572535), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (_LaunchDashboardOverlay_t3582840745), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (_CancelApplicationLaunch_t2074441983), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (_IdentifyApplication_t1775622238), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (_GetApplicationProcessId_t1419106538), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (_GetApplicationsErrorNameFromEnum_t3031121943), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (_GetApplicationPropertyString_t2585650982), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (_GetApplicationPropertyBool_t3564886007), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (_GetApplicationPropertyUint64_t3532011289), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (_SetApplicationAutoLaunch_t1014802842), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (_GetApplicationAutoLaunch_t253949742), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (_SetDefaultApplicationForMimeType_t2776386992), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (_GetDefaultApplicationForMimeType_t1319680284), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (_GetApplicationSupportedMimeTypes_t1223733271), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (_GetApplicationsThatSupportMimeType_t1260006687), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (_GetApplicationLaunchArguments_t3777004763), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (_GetStartingApplication_t4154880362), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (_GetTransitionState_t623155336), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (_PerformApplicationPrelaunchCheck_t1375004597), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
