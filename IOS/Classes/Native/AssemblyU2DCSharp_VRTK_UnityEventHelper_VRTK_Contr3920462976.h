﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;
// VRTK.UnityEventHelper.VRTK_ControllerActions_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t3606750807;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_ControllerActions_UnityEvents
struct  VRTK_ControllerActions_UnityEvents_t3920462976  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_ControllerActions VRTK.UnityEventHelper.VRTK_ControllerActions_UnityEvents::ca
	VRTK_ControllerActions_t3642353851 * ___ca_2;
	// VRTK.UnityEventHelper.VRTK_ControllerActions_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerActions_UnityEvents::OnControllerModelVisible
	UnityObjectEvent_t3606750807 * ___OnControllerModelVisible_3;
	// VRTK.UnityEventHelper.VRTK_ControllerActions_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerActions_UnityEvents::OnControllerModelInvisible
	UnityObjectEvent_t3606750807 * ___OnControllerModelInvisible_4;

public:
	inline static int32_t get_offset_of_ca_2() { return static_cast<int32_t>(offsetof(VRTK_ControllerActions_UnityEvents_t3920462976, ___ca_2)); }
	inline VRTK_ControllerActions_t3642353851 * get_ca_2() const { return ___ca_2; }
	inline VRTK_ControllerActions_t3642353851 ** get_address_of_ca_2() { return &___ca_2; }
	inline void set_ca_2(VRTK_ControllerActions_t3642353851 * value)
	{
		___ca_2 = value;
		Il2CppCodeGenWriteBarrier(&___ca_2, value);
	}

	inline static int32_t get_offset_of_OnControllerModelVisible_3() { return static_cast<int32_t>(offsetof(VRTK_ControllerActions_UnityEvents_t3920462976, ___OnControllerModelVisible_3)); }
	inline UnityObjectEvent_t3606750807 * get_OnControllerModelVisible_3() const { return ___OnControllerModelVisible_3; }
	inline UnityObjectEvent_t3606750807 ** get_address_of_OnControllerModelVisible_3() { return &___OnControllerModelVisible_3; }
	inline void set_OnControllerModelVisible_3(UnityObjectEvent_t3606750807 * value)
	{
		___OnControllerModelVisible_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerModelVisible_3, value);
	}

	inline static int32_t get_offset_of_OnControllerModelInvisible_4() { return static_cast<int32_t>(offsetof(VRTK_ControllerActions_UnityEvents_t3920462976, ___OnControllerModelInvisible_4)); }
	inline UnityObjectEvent_t3606750807 * get_OnControllerModelInvisible_4() const { return ___OnControllerModelInvisible_4; }
	inline UnityObjectEvent_t3606750807 ** get_address_of_OnControllerModelInvisible_4() { return &___OnControllerModelInvisible_4; }
	inline void set_OnControllerModelInvisible_4(UnityObjectEvent_t3606750807 * value)
	{
		___OnControllerModelInvisible_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerModelInvisible_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
