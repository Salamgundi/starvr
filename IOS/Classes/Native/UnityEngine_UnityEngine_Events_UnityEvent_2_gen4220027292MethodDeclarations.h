﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetCollisionEventArgs>
struct UnityEvent_2_t4220027292;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetCollisionEventArgs>
struct UnityAction_2_t2337829374;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventArgs1242373387.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetCollisionEventArgs>::.ctor()
extern "C"  void UnityEvent_2__ctor_m1533904032_gshared (UnityEvent_2_t4220027292 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m1533904032(__this, method) ((  void (*) (UnityEvent_2_t4220027292 *, const MethodInfo*))UnityEvent_2__ctor_m1533904032_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetCollisionEventArgs>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m3386334952_gshared (UnityEvent_2_t4220027292 * __this, UnityAction_2_t2337829374 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m3386334952(__this, ___call0, method) ((  void (*) (UnityEvent_2_t4220027292 *, UnityAction_2_t2337829374 *, const MethodInfo*))UnityEvent_2_AddListener_m3386334952_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetCollisionEventArgs>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m969935461_gshared (UnityEvent_2_t4220027292 * __this, UnityAction_2_t2337829374 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m969935461(__this, ___call0, method) ((  void (*) (UnityEvent_2_t4220027292 *, UnityAction_2_t2337829374 *, const MethodInfo*))UnityEvent_2_RemoveListener_m969935461_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetCollisionEventArgs>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m3435198392_gshared (UnityEvent_2_t4220027292 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m3435198392(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t4220027292 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m3435198392_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetCollisionEventArgs>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m3053876894_gshared (UnityEvent_2_t4220027292 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m3053876894(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t4220027292 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m3053876894_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetCollisionEventArgs>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m3111252873_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t2337829374 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m3111252873(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t2337829374 *, const MethodInfo*))UnityEvent_2_GetDelegate_m3111252873_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetCollisionEventArgs>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m929607979_gshared (UnityEvent_2_t4220027292 * __this, Il2CppObject * ___arg00, HeadsetCollisionEventArgs_t1242373387  ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m929607979(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t4220027292 *, Il2CppObject *, HeadsetCollisionEventArgs_t1242373387 , const MethodInfo*))UnityEvent_2_Invoke_m929607979_gshared)(__this, ___arg00, ___arg11, method)
