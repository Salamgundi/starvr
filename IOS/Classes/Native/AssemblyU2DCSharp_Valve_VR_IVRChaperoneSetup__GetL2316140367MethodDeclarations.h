﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_GetLivePhysicalBoundsInfo
struct _GetLivePhysicalBoundsInfo_t2316140367;
// System.Object
struct Il2CppObject;
// Valve.VR.HmdQuad_t[]
struct HmdQuad_tU5BU5D_t16941492;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_GetLivePhysicalBoundsInfo::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetLivePhysicalBoundsInfo__ctor_m1458391326 (_GetLivePhysicalBoundsInfo_t2316140367 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetLivePhysicalBoundsInfo::Invoke(Valve.VR.HmdQuad_t[],System.UInt32&)
extern "C"  bool _GetLivePhysicalBoundsInfo_Invoke_m1692821233 (_GetLivePhysicalBoundsInfo_t2316140367 * __this, HmdQuad_tU5BU5D_t16941492* ___pQuadsBuffer0, uint32_t* ___punQuadsCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_GetLivePhysicalBoundsInfo::BeginInvoke(Valve.VR.HmdQuad_t[],System.UInt32&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetLivePhysicalBoundsInfo_BeginInvoke_m1960273316 (_GetLivePhysicalBoundsInfo_t2316140367 * __this, HmdQuad_tU5BU5D_t16941492* ___pQuadsBuffer0, uint32_t* ___punQuadsCount1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetLivePhysicalBoundsInfo::EndInvoke(System.UInt32&,System.IAsyncResult)
extern "C"  bool _GetLivePhysicalBoundsInfo_EndInvoke_m3856732720 (_GetLivePhysicalBoundsInfo_t2316140367 * __this, uint32_t* ___punQuadsCount0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
