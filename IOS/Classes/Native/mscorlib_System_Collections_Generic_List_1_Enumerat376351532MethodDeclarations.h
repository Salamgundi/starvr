﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_VRInputModule>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2838734734(__this, ___l0, method) ((  void (*) (Enumerator_t376351532 *, List_1_t841621858 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_VRInputModule>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3120331372(__this, method) ((  void (*) (Enumerator_t376351532 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_VRInputModule>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4290908160(__this, method) ((  Il2CppObject * (*) (Enumerator_t376351532 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_VRInputModule>::Dispose()
#define Enumerator_Dispose_m1951339371(__this, method) ((  void (*) (Enumerator_t376351532 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_VRInputModule>::VerifyState()
#define Enumerator_VerifyState_m4282544600(__this, method) ((  void (*) (Enumerator_t376351532 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_VRInputModule>::MoveNext()
#define Enumerator_MoveNext_m1555652301(__this, method) ((  bool (*) (Enumerator_t376351532 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_VRInputModule>::get_Current()
#define Enumerator_get_Current_m1213949717(__this, method) ((  VRTK_VRInputModule_t1472500726 * (*) (Enumerator_t376351532 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
