﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Valve.VR.InteractionSystem.SoundPlayOneshot
struct SoundPlayOneshot_t1703214483;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.Balloon
struct  Balloon_t3376655393  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.Balloon::hand
	Hand_t379716353 * ___hand_2;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Balloon::popPrefab
	GameObject_t1756533147 * ___popPrefab_3;
	// System.Single Valve.VR.InteractionSystem.Balloon::maxVelocity
	float ___maxVelocity_4;
	// System.Single Valve.VR.InteractionSystem.Balloon::lifetime
	float ___lifetime_5;
	// System.Boolean Valve.VR.InteractionSystem.Balloon::burstOnLifetimeEnd
	bool ___burstOnLifetimeEnd_6;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Balloon::lifetimeEndParticlePrefab
	GameObject_t1756533147 * ___lifetimeEndParticlePrefab_7;
	// Valve.VR.InteractionSystem.SoundPlayOneshot Valve.VR.InteractionSystem.Balloon::lifetimeEndSound
	SoundPlayOneshot_t1703214483 * ___lifetimeEndSound_8;
	// System.Single Valve.VR.InteractionSystem.Balloon::destructTime
	float ___destructTime_9;
	// System.Single Valve.VR.InteractionSystem.Balloon::releaseTime
	float ___releaseTime_10;
	// Valve.VR.InteractionSystem.SoundPlayOneshot Valve.VR.InteractionSystem.Balloon::collisionSound
	SoundPlayOneshot_t1703214483 * ___collisionSound_11;
	// System.Single Valve.VR.InteractionSystem.Balloon::lastSoundTime
	float ___lastSoundTime_12;
	// System.Single Valve.VR.InteractionSystem.Balloon::soundDelay
	float ___soundDelay_13;
	// UnityEngine.Rigidbody Valve.VR.InteractionSystem.Balloon::balloonRigidbody
	Rigidbody_t4233889191 * ___balloonRigidbody_14;
	// System.Boolean Valve.VR.InteractionSystem.Balloon::bParticlesSpawned
	bool ___bParticlesSpawned_15;

public:
	inline static int32_t get_offset_of_hand_2() { return static_cast<int32_t>(offsetof(Balloon_t3376655393, ___hand_2)); }
	inline Hand_t379716353 * get_hand_2() const { return ___hand_2; }
	inline Hand_t379716353 ** get_address_of_hand_2() { return &___hand_2; }
	inline void set_hand_2(Hand_t379716353 * value)
	{
		___hand_2 = value;
		Il2CppCodeGenWriteBarrier(&___hand_2, value);
	}

	inline static int32_t get_offset_of_popPrefab_3() { return static_cast<int32_t>(offsetof(Balloon_t3376655393, ___popPrefab_3)); }
	inline GameObject_t1756533147 * get_popPrefab_3() const { return ___popPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_popPrefab_3() { return &___popPrefab_3; }
	inline void set_popPrefab_3(GameObject_t1756533147 * value)
	{
		___popPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___popPrefab_3, value);
	}

	inline static int32_t get_offset_of_maxVelocity_4() { return static_cast<int32_t>(offsetof(Balloon_t3376655393, ___maxVelocity_4)); }
	inline float get_maxVelocity_4() const { return ___maxVelocity_4; }
	inline float* get_address_of_maxVelocity_4() { return &___maxVelocity_4; }
	inline void set_maxVelocity_4(float value)
	{
		___maxVelocity_4 = value;
	}

	inline static int32_t get_offset_of_lifetime_5() { return static_cast<int32_t>(offsetof(Balloon_t3376655393, ___lifetime_5)); }
	inline float get_lifetime_5() const { return ___lifetime_5; }
	inline float* get_address_of_lifetime_5() { return &___lifetime_5; }
	inline void set_lifetime_5(float value)
	{
		___lifetime_5 = value;
	}

	inline static int32_t get_offset_of_burstOnLifetimeEnd_6() { return static_cast<int32_t>(offsetof(Balloon_t3376655393, ___burstOnLifetimeEnd_6)); }
	inline bool get_burstOnLifetimeEnd_6() const { return ___burstOnLifetimeEnd_6; }
	inline bool* get_address_of_burstOnLifetimeEnd_6() { return &___burstOnLifetimeEnd_6; }
	inline void set_burstOnLifetimeEnd_6(bool value)
	{
		___burstOnLifetimeEnd_6 = value;
	}

	inline static int32_t get_offset_of_lifetimeEndParticlePrefab_7() { return static_cast<int32_t>(offsetof(Balloon_t3376655393, ___lifetimeEndParticlePrefab_7)); }
	inline GameObject_t1756533147 * get_lifetimeEndParticlePrefab_7() const { return ___lifetimeEndParticlePrefab_7; }
	inline GameObject_t1756533147 ** get_address_of_lifetimeEndParticlePrefab_7() { return &___lifetimeEndParticlePrefab_7; }
	inline void set_lifetimeEndParticlePrefab_7(GameObject_t1756533147 * value)
	{
		___lifetimeEndParticlePrefab_7 = value;
		Il2CppCodeGenWriteBarrier(&___lifetimeEndParticlePrefab_7, value);
	}

	inline static int32_t get_offset_of_lifetimeEndSound_8() { return static_cast<int32_t>(offsetof(Balloon_t3376655393, ___lifetimeEndSound_8)); }
	inline SoundPlayOneshot_t1703214483 * get_lifetimeEndSound_8() const { return ___lifetimeEndSound_8; }
	inline SoundPlayOneshot_t1703214483 ** get_address_of_lifetimeEndSound_8() { return &___lifetimeEndSound_8; }
	inline void set_lifetimeEndSound_8(SoundPlayOneshot_t1703214483 * value)
	{
		___lifetimeEndSound_8 = value;
		Il2CppCodeGenWriteBarrier(&___lifetimeEndSound_8, value);
	}

	inline static int32_t get_offset_of_destructTime_9() { return static_cast<int32_t>(offsetof(Balloon_t3376655393, ___destructTime_9)); }
	inline float get_destructTime_9() const { return ___destructTime_9; }
	inline float* get_address_of_destructTime_9() { return &___destructTime_9; }
	inline void set_destructTime_9(float value)
	{
		___destructTime_9 = value;
	}

	inline static int32_t get_offset_of_releaseTime_10() { return static_cast<int32_t>(offsetof(Balloon_t3376655393, ___releaseTime_10)); }
	inline float get_releaseTime_10() const { return ___releaseTime_10; }
	inline float* get_address_of_releaseTime_10() { return &___releaseTime_10; }
	inline void set_releaseTime_10(float value)
	{
		___releaseTime_10 = value;
	}

	inline static int32_t get_offset_of_collisionSound_11() { return static_cast<int32_t>(offsetof(Balloon_t3376655393, ___collisionSound_11)); }
	inline SoundPlayOneshot_t1703214483 * get_collisionSound_11() const { return ___collisionSound_11; }
	inline SoundPlayOneshot_t1703214483 ** get_address_of_collisionSound_11() { return &___collisionSound_11; }
	inline void set_collisionSound_11(SoundPlayOneshot_t1703214483 * value)
	{
		___collisionSound_11 = value;
		Il2CppCodeGenWriteBarrier(&___collisionSound_11, value);
	}

	inline static int32_t get_offset_of_lastSoundTime_12() { return static_cast<int32_t>(offsetof(Balloon_t3376655393, ___lastSoundTime_12)); }
	inline float get_lastSoundTime_12() const { return ___lastSoundTime_12; }
	inline float* get_address_of_lastSoundTime_12() { return &___lastSoundTime_12; }
	inline void set_lastSoundTime_12(float value)
	{
		___lastSoundTime_12 = value;
	}

	inline static int32_t get_offset_of_soundDelay_13() { return static_cast<int32_t>(offsetof(Balloon_t3376655393, ___soundDelay_13)); }
	inline float get_soundDelay_13() const { return ___soundDelay_13; }
	inline float* get_address_of_soundDelay_13() { return &___soundDelay_13; }
	inline void set_soundDelay_13(float value)
	{
		___soundDelay_13 = value;
	}

	inline static int32_t get_offset_of_balloonRigidbody_14() { return static_cast<int32_t>(offsetof(Balloon_t3376655393, ___balloonRigidbody_14)); }
	inline Rigidbody_t4233889191 * get_balloonRigidbody_14() const { return ___balloonRigidbody_14; }
	inline Rigidbody_t4233889191 ** get_address_of_balloonRigidbody_14() { return &___balloonRigidbody_14; }
	inline void set_balloonRigidbody_14(Rigidbody_t4233889191 * value)
	{
		___balloonRigidbody_14 = value;
		Il2CppCodeGenWriteBarrier(&___balloonRigidbody_14, value);
	}

	inline static int32_t get_offset_of_bParticlesSpawned_15() { return static_cast<int32_t>(offsetof(Balloon_t3376655393, ___bParticlesSpawned_15)); }
	inline bool get_bParticlesSpawned_15() const { return ___bParticlesSpawned_15; }
	inline bool* get_address_of_bParticlesSpawned_15() { return &___bParticlesSpawned_15; }
	inline void set_bParticlesSpawned_15(bool value)
	{
		___bParticlesSpawned_15 = value;
	}
};

struct Balloon_t3376655393_StaticFields
{
public:
	// System.Single Valve.VR.InteractionSystem.Balloon::s_flLastDeathSound
	float ___s_flLastDeathSound_16;

public:
	inline static int32_t get_offset_of_s_flLastDeathSound_16() { return static_cast<int32_t>(offsetof(Balloon_t3376655393_StaticFields, ___s_flLastDeathSound_16)); }
	inline float get_s_flLastDeathSound_16() const { return ___s_flLastDeathSound_16; }
	inline float* get_address_of_s_flLastDeathSound_16() { return &___s_flLastDeathSound_16; }
	inline void set_s_flLastDeathSound_16(float value)
	{
		___s_flLastDeathSound_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
