﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t3966406431;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents
struct  VRTK_InteractableObject_UnityEvents_t841223230  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_InteractableObject VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::io
	VRTK_InteractableObject_t2604188111 * ___io_2;
	// VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::OnTouch
	UnityObjectEvent_t3966406431 * ___OnTouch_3;
	// VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::OnUntouch
	UnityObjectEvent_t3966406431 * ___OnUntouch_4;
	// VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::OnGrab
	UnityObjectEvent_t3966406431 * ___OnGrab_5;
	// VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::OnUngrab
	UnityObjectEvent_t3966406431 * ___OnUngrab_6;
	// VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::OnUse
	UnityObjectEvent_t3966406431 * ___OnUse_7;
	// VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::OnUnuse
	UnityObjectEvent_t3966406431 * ___OnUnuse_8;

public:
	inline static int32_t get_offset_of_io_2() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_UnityEvents_t841223230, ___io_2)); }
	inline VRTK_InteractableObject_t2604188111 * get_io_2() const { return ___io_2; }
	inline VRTK_InteractableObject_t2604188111 ** get_address_of_io_2() { return &___io_2; }
	inline void set_io_2(VRTK_InteractableObject_t2604188111 * value)
	{
		___io_2 = value;
		Il2CppCodeGenWriteBarrier(&___io_2, value);
	}

	inline static int32_t get_offset_of_OnTouch_3() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_UnityEvents_t841223230, ___OnTouch_3)); }
	inline UnityObjectEvent_t3966406431 * get_OnTouch_3() const { return ___OnTouch_3; }
	inline UnityObjectEvent_t3966406431 ** get_address_of_OnTouch_3() { return &___OnTouch_3; }
	inline void set_OnTouch_3(UnityObjectEvent_t3966406431 * value)
	{
		___OnTouch_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnTouch_3, value);
	}

	inline static int32_t get_offset_of_OnUntouch_4() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_UnityEvents_t841223230, ___OnUntouch_4)); }
	inline UnityObjectEvent_t3966406431 * get_OnUntouch_4() const { return ___OnUntouch_4; }
	inline UnityObjectEvent_t3966406431 ** get_address_of_OnUntouch_4() { return &___OnUntouch_4; }
	inline void set_OnUntouch_4(UnityObjectEvent_t3966406431 * value)
	{
		___OnUntouch_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnUntouch_4, value);
	}

	inline static int32_t get_offset_of_OnGrab_5() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_UnityEvents_t841223230, ___OnGrab_5)); }
	inline UnityObjectEvent_t3966406431 * get_OnGrab_5() const { return ___OnGrab_5; }
	inline UnityObjectEvent_t3966406431 ** get_address_of_OnGrab_5() { return &___OnGrab_5; }
	inline void set_OnGrab_5(UnityObjectEvent_t3966406431 * value)
	{
		___OnGrab_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnGrab_5, value);
	}

	inline static int32_t get_offset_of_OnUngrab_6() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_UnityEvents_t841223230, ___OnUngrab_6)); }
	inline UnityObjectEvent_t3966406431 * get_OnUngrab_6() const { return ___OnUngrab_6; }
	inline UnityObjectEvent_t3966406431 ** get_address_of_OnUngrab_6() { return &___OnUngrab_6; }
	inline void set_OnUngrab_6(UnityObjectEvent_t3966406431 * value)
	{
		___OnUngrab_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnUngrab_6, value);
	}

	inline static int32_t get_offset_of_OnUse_7() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_UnityEvents_t841223230, ___OnUse_7)); }
	inline UnityObjectEvent_t3966406431 * get_OnUse_7() const { return ___OnUse_7; }
	inline UnityObjectEvent_t3966406431 ** get_address_of_OnUse_7() { return &___OnUse_7; }
	inline void set_OnUse_7(UnityObjectEvent_t3966406431 * value)
	{
		___OnUse_7 = value;
		Il2CppCodeGenWriteBarrier(&___OnUse_7, value);
	}

	inline static int32_t get_offset_of_OnUnuse_8() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_UnityEvents_t841223230, ___OnUnuse_8)); }
	inline UnityObjectEvent_t3966406431 * get_OnUnuse_8() const { return ___OnUnuse_8; }
	inline UnityObjectEvent_t3966406431 ** get_address_of_OnUnuse_8() { return &___OnUnuse_8; }
	inline void set_OnUnuse_8(UnityObjectEvent_t3966406431 * value)
	{
		___OnUnuse_8 = value;
		Il2CppCodeGenWriteBarrier(&___OnUnuse_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
