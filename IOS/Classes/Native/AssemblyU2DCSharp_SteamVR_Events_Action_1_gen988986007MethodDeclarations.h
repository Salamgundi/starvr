﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Action`1<System.Boolean>
struct Action_1_t988986007;
// SteamVR_Events/Event`1<System.Boolean>
struct Event_1_t1706029839;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t897193173;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Events/Action`1<System.Boolean>::.ctor(SteamVR_Events/Event`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Action_1__ctor_m3254789872_gshared (Action_1_t988986007 * __this, Event_1_t1706029839 * ____event0, UnityAction_1_t897193173 * ___action1, const MethodInfo* method);
#define Action_1__ctor_m3254789872(__this, ____event0, ___action1, method) ((  void (*) (Action_1_t988986007 *, Event_1_t1706029839 *, UnityAction_1_t897193173 *, const MethodInfo*))Action_1__ctor_m3254789872_gshared)(__this, ____event0, ___action1, method)
// System.Void SteamVR_Events/Action`1<System.Boolean>::Enable(System.Boolean)
extern "C"  void Action_1_Enable_m2891429450_gshared (Action_1_t988986007 * __this, bool ___enabled0, const MethodInfo* method);
#define Action_1_Enable_m2891429450(__this, ___enabled0, method) ((  void (*) (Action_1_t988986007 *, bool, const MethodInfo*))Action_1_Enable_m2891429450_gshared)(__this, ___enabled0, method)
