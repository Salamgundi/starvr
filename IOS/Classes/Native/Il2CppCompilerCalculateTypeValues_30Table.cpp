﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Heads3010802322.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Heads2825175431.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Headse642803444.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Heads2247800009.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Headse515017992.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Inter1100655609.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Inter2245989332.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Inter2444271850.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Intera217234877.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Inter2357120922.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Inter2739379367.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Intera841223230.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Inter3966406431.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Objec2571398247.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Objec4282528608.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Playe3249633635.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_Playe1820762702.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_SnapD2045993404.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_SnapDr447141393.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_UIPoin980224384.h"
#include "AssemblyU2DCSharp_VRTK_UnityEventHelper_VRTK_UIPoi2013676129.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_AdaptiveQuality2723434039.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_AdaptiveQuality_Comman2311551420.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_AdaptiveQuality_Keyboa2232892609.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_AdaptiveQuality_ShaderP445255555.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_AdaptiveQuality_Timing3878883323.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DeviceFinder2083124544.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DeviceFinder_Devices2408891389.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DeviceFinder_Headsets2701247739.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PolicyList2965133344.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PolicyList_OperationTy2997627994.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PolicyList_CheckTypes3385731591.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SDKManager2629434797.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SDKManager_SupportedSD1339136682.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SDKDetails1748250348.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SharedMethods185961009.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Simulator808244324.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Simulator_Keys2631472146.h"
#include "AssemblyU2DCSharp_ViveControllerInput1577066400.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437132.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2375206772.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3384550474.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236537.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3762068664.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1254291669.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3706046914.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3259153605.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2543710313.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4077469257.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1274006168.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3629569411.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1989801646.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3790432936.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I496834990.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3850268328.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I681392339.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2913820361.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I579860294.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2174076389.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2318278682.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3313275655.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2905677972.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2925629392.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3591885494.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3964716834.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3523901841.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3694918262.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2385315007.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1672908095.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I163796146.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3133742431.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3095868303.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3961919654.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2459464934.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3059663215.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3904103385.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3902263335.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I457390305.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1921949650.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1282715729.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I785857862.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I859433922.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I863619287.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3948012467.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4036041399.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I517806655.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2907318477.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1887427701.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2899312312.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3375869057.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2152133263.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1845967802.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I761286994.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3972324734.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I962057283.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (UnityObjectEvent_t3010802322), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (VRTK_HeadsetControllerAware_UnityEvents_t2825175431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3001[5] = 
{
	VRTK_HeadsetControllerAware_UnityEvents_t2825175431::get_offset_of_hca_2(),
	VRTK_HeadsetControllerAware_UnityEvents_t2825175431::get_offset_of_OnControllerObscured_3(),
	VRTK_HeadsetControllerAware_UnityEvents_t2825175431::get_offset_of_OnControllerUnobscured_4(),
	VRTK_HeadsetControllerAware_UnityEvents_t2825175431::get_offset_of_OnControllerGlanceEnter_5(),
	VRTK_HeadsetControllerAware_UnityEvents_t2825175431::get_offset_of_OnControllerGlanceExit_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (UnityObjectEvent_t642803444), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (VRTK_HeadsetFade_UnityEvents_t2247800009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3003[5] = 
{
	VRTK_HeadsetFade_UnityEvents_t2247800009::get_offset_of_hf_2(),
	VRTK_HeadsetFade_UnityEvents_t2247800009::get_offset_of_OnHeadsetFadeStart_3(),
	VRTK_HeadsetFade_UnityEvents_t2247800009::get_offset_of_OnHeadsetFadeComplete_4(),
	VRTK_HeadsetFade_UnityEvents_t2247800009::get_offset_of_OnHeadsetUnfadeStart_5(),
	VRTK_HeadsetFade_UnityEvents_t2247800009::get_offset_of_OnHeadsetUnfadeComplete_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (UnityObjectEvent_t515017992), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (VRTK_InteractGrab_UnityEvents_t1100655609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3005[3] = 
{
	VRTK_InteractGrab_UnityEvents_t1100655609::get_offset_of_ig_2(),
	VRTK_InteractGrab_UnityEvents_t1100655609::get_offset_of_OnControllerGrabInteractableObject_3(),
	VRTK_InteractGrab_UnityEvents_t1100655609::get_offset_of_OnControllerUngrabInteractableObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (UnityObjectEvent_t2245989332), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (VRTK_InteractTouch_UnityEvents_t2444271850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3007[3] = 
{
	VRTK_InteractTouch_UnityEvents_t2444271850::get_offset_of_it_2(),
	VRTK_InteractTouch_UnityEvents_t2444271850::get_offset_of_OnControllerTouchInteractableObject_3(),
	VRTK_InteractTouch_UnityEvents_t2444271850::get_offset_of_OnControllerUntouchInteractableObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (UnityObjectEvent_t217234877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (VRTK_InteractUse_UnityEvents_t2357120922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3009[3] = 
{
	VRTK_InteractUse_UnityEvents_t2357120922::get_offset_of_iu_2(),
	VRTK_InteractUse_UnityEvents_t2357120922::get_offset_of_OnControllerUseInteractableObject_3(),
	VRTK_InteractUse_UnityEvents_t2357120922::get_offset_of_OnControllerUnuseInteractableObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (UnityObjectEvent_t2739379367), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (VRTK_InteractableObject_UnityEvents_t841223230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3011[7] = 
{
	VRTK_InteractableObject_UnityEvents_t841223230::get_offset_of_io_2(),
	VRTK_InteractableObject_UnityEvents_t841223230::get_offset_of_OnTouch_3(),
	VRTK_InteractableObject_UnityEvents_t841223230::get_offset_of_OnUntouch_4(),
	VRTK_InteractableObject_UnityEvents_t841223230::get_offset_of_OnGrab_5(),
	VRTK_InteractableObject_UnityEvents_t841223230::get_offset_of_OnUngrab_6(),
	VRTK_InteractableObject_UnityEvents_t841223230::get_offset_of_OnUse_7(),
	VRTK_InteractableObject_UnityEvents_t841223230::get_offset_of_OnUnuse_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (UnityObjectEvent_t3966406431), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (VRTK_ObjectControl_UnityEvents_t2571398247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3013[3] = 
{
	VRTK_ObjectControl_UnityEvents_t2571398247::get_offset_of_oc_2(),
	VRTK_ObjectControl_UnityEvents_t2571398247::get_offset_of_OnXAxisChanged_3(),
	VRTK_ObjectControl_UnityEvents_t2571398247::get_offset_of_OnYAxisChanged_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (UnityObjectEvent_t4282528608), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (VRTK_PlayerClimb_UnityEvents_t3249633635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3015[3] = 
{
	VRTK_PlayerClimb_UnityEvents_t3249633635::get_offset_of_pc_2(),
	VRTK_PlayerClimb_UnityEvents_t3249633635::get_offset_of_OnPlayerClimbStarted_3(),
	VRTK_PlayerClimb_UnityEvents_t3249633635::get_offset_of_OnPlayerClimbEnded_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (UnityObjectEvent_t1820762702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (VRTK_SnapDropZone_UnityEvents_t2045993404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3017[5] = 
{
	VRTK_SnapDropZone_UnityEvents_t2045993404::get_offset_of_sdz_2(),
	VRTK_SnapDropZone_UnityEvents_t2045993404::get_offset_of_OnObjectEnteredSnapDropZone_3(),
	VRTK_SnapDropZone_UnityEvents_t2045993404::get_offset_of_OnObjectExitedSnapDropZone_4(),
	VRTK_SnapDropZone_UnityEvents_t2045993404::get_offset_of_OnObjectSnappedToDropZone_5(),
	VRTK_SnapDropZone_UnityEvents_t2045993404::get_offset_of_OnObjectUnsnappedFromDropZone_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (UnityObjectEvent_t447141393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (VRTK_UIPointer_UnityEvents_t980224384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3019[6] = 
{
	VRTK_UIPointer_UnityEvents_t980224384::get_offset_of_uip_2(),
	VRTK_UIPointer_UnityEvents_t980224384::get_offset_of_OnUIPointerElementEnter_3(),
	VRTK_UIPointer_UnityEvents_t980224384::get_offset_of_OnUIPointerElementExit_4(),
	VRTK_UIPointer_UnityEvents_t980224384::get_offset_of_OnUIPointerElementClick_5(),
	VRTK_UIPointer_UnityEvents_t980224384::get_offset_of_OnUIPointerElementDragStart_6(),
	VRTK_UIPointer_UnityEvents_t980224384::get_offset_of_OnUIPointerElementDragEnd_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (UnityObjectEvent_t2013676129), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (VRTK_AdaptiveQuality_t2723434039), -1, sizeof(VRTK_AdaptiveQuality_t2723434039_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3021[31] = 
{
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_drawDebugVisualization_2(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_allowKeyboardShortcuts_3(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_allowCommandLineArguments_4(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_msaaLevel_5(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_scaleRenderViewport_6(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_minimumRenderScale_7(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_maximumRenderScale_8(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_maximumRenderTargetDimension_9(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_renderScaleFillRateStepSizeInPercent_10(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_scaleRenderTargetResolution_11(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_overrideRenderViewportScale_12(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_overrideRenderViewportScaleLevel_13(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_renderScales_14(),
	0,
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_renderViewportScaleSetting_16(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_renderScaleSetting_17(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_allRenderScales_18(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_defaultRenderViewportScaleLevel_19(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_previousMinimumRenderScale_20(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_previousMaximumRenderScale_21(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_previousRenderScaleFillRateStepSizeInPercent_22(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_timing_23(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_lastRenderViewportScaleLevelBelowRenderScaleLevelFrameCount_24(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_interleavedReprojectionEnabled_25(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_hmdDisplayIsOnDesktop_26(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_singleFrameDurationInMilliseconds_27(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_debugVisualizationQuad_28(),
	VRTK_AdaptiveQuality_t2723434039::get_offset_of_debugVisualizationQuadMaterial_29(),
	VRTK_AdaptiveQuality_t2723434039_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_30(),
	VRTK_AdaptiveQuality_t2723434039_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_31(),
	VRTK_AdaptiveQuality_t2723434039_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3022[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (CommandLineArguments_t2311551420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3023[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (KeyboardShortcuts_t2232892609), -1, sizeof(KeyboardShortcuts_t2232892609_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3024[5] = 
{
	KeyboardShortcuts_t2232892609_StaticFields::get_offset_of_Modifiers_0(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (ShaderPropertyIDs_t445255555), -1, sizeof(ShaderPropertyIDs_t445255555_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3025[5] = 
{
	ShaderPropertyIDs_t445255555_StaticFields::get_offset_of_RenderScaleLevelsCount_0(),
	ShaderPropertyIDs_t445255555_StaticFields::get_offset_of_DefaultRenderViewportScaleLevel_1(),
	ShaderPropertyIDs_t445255555_StaticFields::get_offset_of_CurrentRenderViewportScaleLevel_2(),
	ShaderPropertyIDs_t445255555_StaticFields::get_offset_of_CurrentRenderScaleLevel_3(),
	ShaderPropertyIDs_t445255555_StaticFields::get_offset_of_LastFrameIsInBudget_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (Timing_t3878883323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3026[2] = 
{
	Timing_t3878883323::get_offset_of_buffer_0(),
	Timing_t3878883323::get_offset_of_bufferIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (VRTK_DeviceFinder_t2083124544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (Devices_t2408891389)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3028[4] = 
{
	Devices_t2408891389::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (Headsets_t2701247739)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3029[6] = 
{
	Headsets_t2701247739::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (VRTK_PolicyList_t2965133344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3030[3] = 
{
	VRTK_PolicyList_t2965133344::get_offset_of_operation_2(),
	VRTK_PolicyList_t2965133344::get_offset_of_checkType_3(),
	VRTK_PolicyList_t2965133344::get_offset_of_identifiers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (OperationTypes_t2997627994)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3031[3] = 
{
	OperationTypes_t2997627994::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (CheckTypes_t3385731591)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3032[4] = 
{
	CheckTypes_t3385731591::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (VRTK_SDKManager_t2629434797), -1, sizeof(VRTK_SDKManager_t2629434797_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3033[16] = 
{
	VRTK_SDKManager_t2629434797::get_offset_of_sdkDetails_2(),
	VRTK_SDKManager_t2629434797_StaticFields::get_offset_of_instance_3(),
	VRTK_SDKManager_t2629434797::get_offset_of_persistOnLoad_4(),
	VRTK_SDKManager_t2629434797::get_offset_of_systemSDK_5(),
	VRTK_SDKManager_t2629434797::get_offset_of_boundariesSDK_6(),
	VRTK_SDKManager_t2629434797::get_offset_of_headsetSDK_7(),
	VRTK_SDKManager_t2629434797::get_offset_of_controllerSDK_8(),
	VRTK_SDKManager_t2629434797::get_offset_of_autoManageScriptDefines_9(),
	VRTK_SDKManager_t2629434797::get_offset_of_actualBoundaries_10(),
	VRTK_SDKManager_t2629434797::get_offset_of_actualHeadset_11(),
	VRTK_SDKManager_t2629434797::get_offset_of_actualLeftController_12(),
	VRTK_SDKManager_t2629434797::get_offset_of_actualRightController_13(),
	VRTK_SDKManager_t2629434797::get_offset_of_modelAliasLeftController_14(),
	VRTK_SDKManager_t2629434797::get_offset_of_modelAliasRightController_15(),
	VRTK_SDKManager_t2629434797::get_offset_of_scriptAliasLeftController_16(),
	VRTK_SDKManager_t2629434797::get_offset_of_scriptAliasRightController_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (SupportedSDKs_t1339136682)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3034[6] = 
{
	SupportedSDKs_t1339136682::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (VRTK_SDKDetails_t1748250348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3035[3] = 
{
	VRTK_SDKDetails_t1748250348::get_offset_of_defineSymbol_0(),
	VRTK_SDKDetails_t1748250348::get_offset_of_prettyName_1(),
	VRTK_SDKDetails_t1748250348::get_offset_of_checkType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (VRTK_SharedMethods_t185961009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (VRTK_Simulator_t808244324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3037[8] = 
{
	VRTK_Simulator_t808244324::get_offset_of_keys_2(),
	VRTK_Simulator_t808244324::get_offset_of_onlyInEditor_3(),
	VRTK_Simulator_t808244324::get_offset_of_stepSize_4(),
	VRTK_Simulator_t808244324::get_offset_of_camStart_5(),
	VRTK_Simulator_t808244324::get_offset_of_headset_6(),
	VRTK_Simulator_t808244324::get_offset_of_playArea_7(),
	VRTK_Simulator_t808244324::get_offset_of_initialPosition_8(),
	VRTK_Simulator_t808244324::get_offset_of_initialRotation_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (Keys_t2631472146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3038[9] = 
{
	Keys_t2631472146::get_offset_of_forward_0(),
	Keys_t2631472146::get_offset_of_backward_1(),
	Keys_t2631472146::get_offset_of_strafeLeft_2(),
	Keys_t2631472146::get_offset_of_strafeRight_3(),
	Keys_t2631472146::get_offset_of_left_4(),
	Keys_t2631472146::get_offset_of_right_5(),
	Keys_t2631472146::get_offset_of_up_6(),
	Keys_t2631472146::get_offset_of_down_7(),
	Keys_t2631472146::get_offset_of_reset_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (ViveControllerInput_t1577066400), -1, sizeof(ViveControllerInput_t1577066400_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3039[16] = 
{
	ViveControllerInput_t1577066400_StaticFields::get_offset_of_Instance_8(),
	ViveControllerInput_t1577066400::get_offset_of_CursorSprite_9(),
	ViveControllerInput_t1577066400::get_offset_of_CursorMaterial_10(),
	ViveControllerInput_t1577066400::get_offset_of_NormalCursorScale_11(),
	ViveControllerInput_t1577066400::get_offset_of_GuiHit_12(),
	ViveControllerInput_t1577066400::get_offset_of_ButtonUsed_13(),
	ViveControllerInput_t1577066400::get_offset_of_Cursors_14(),
	ViveControllerInput_t1577066400::get_offset_of_CurrentPoint_15(),
	ViveControllerInput_t1577066400::get_offset_of_CurrentPressed_16(),
	ViveControllerInput_t1577066400::get_offset_of_CurrentDragging_17(),
	ViveControllerInput_t1577066400::get_offset_of_PointEvents_18(),
	ViveControllerInput_t1577066400::get_offset_of_Initialized_19(),
	ViveControllerInput_t1577066400::get_offset_of_ControllerCamera_20(),
	ViveControllerInput_t1577066400::get_offset_of_ControllerManager_21(),
	ViveControllerInput_t1577066400::get_offset_of_Controllers_22(),
	ViveControllerInput_t1577066400::get_offset_of_ControllerDevices_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3040[12] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D51B2AA051AFFF21EBC28102EA2F57BEF007038AE_0(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_1(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_2(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_3(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D25B4B83D2A43393F4E18624598DDA694217A6622_4(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_5(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DC34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_6(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DFF31D7C7A32DBAFAA3A95A8E0769B3226747B5B3_7(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DECCE630F2BA605DA3A785F4D2DDD0C59EC9376F1_8(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D8236C0CC36A2E3B682228FAF16E4A74BE79F3DE1_9(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D50567A6578C37E24118E2B7EE8F5C7930666F62F_10(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D9365FDAD4BD75AEF9446E0B22483D217BE36E309_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (U24ArrayTypeU3D20_t2731437132)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D20_t2731437132 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (U24ArrayTypeU3D12_t1568637718)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637718 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (U24ArrayTypeU3D28_t2375206772)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D28_t2375206772 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (U24ArrayTypeU3D192_t384550474)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D192_t384550474 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (U24ArrayTypeU3D96_t3894236537)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D96_t3894236537 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (U24ArrayTypeU3D24_t762068664)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D24_t762068664 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (AAMode_t1254291669)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3049[8] = 
{
	AAMode_t1254291669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (Antialiasing_t3706046914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3050[22] = 
{
	Antialiasing_t3706046914::get_offset_of_mode_6(),
	Antialiasing_t3706046914::get_offset_of_showGeneratedNormals_7(),
	Antialiasing_t3706046914::get_offset_of_offsetScale_8(),
	Antialiasing_t3706046914::get_offset_of_blurRadius_9(),
	Antialiasing_t3706046914::get_offset_of_edgeThresholdMin_10(),
	Antialiasing_t3706046914::get_offset_of_edgeThreshold_11(),
	Antialiasing_t3706046914::get_offset_of_edgeSharpness_12(),
	Antialiasing_t3706046914::get_offset_of_dlaaSharp_13(),
	Antialiasing_t3706046914::get_offset_of_ssaaShader_14(),
	Antialiasing_t3706046914::get_offset_of_ssaa_15(),
	Antialiasing_t3706046914::get_offset_of_dlaaShader_16(),
	Antialiasing_t3706046914::get_offset_of_dlaa_17(),
	Antialiasing_t3706046914::get_offset_of_nfaaShader_18(),
	Antialiasing_t3706046914::get_offset_of_nfaa_19(),
	Antialiasing_t3706046914::get_offset_of_shaderFXAAPreset2_20(),
	Antialiasing_t3706046914::get_offset_of_materialFXAAPreset2_21(),
	Antialiasing_t3706046914::get_offset_of_shaderFXAAPreset3_22(),
	Antialiasing_t3706046914::get_offset_of_materialFXAAPreset3_23(),
	Antialiasing_t3706046914::get_offset_of_shaderFXAAII_24(),
	Antialiasing_t3706046914::get_offset_of_materialFXAAII_25(),
	Antialiasing_t3706046914::get_offset_of_shaderFXAAIII_26(),
	Antialiasing_t3706046914::get_offset_of_materialFXAAIII_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (Bloom_t3259153605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3051[30] = 
{
	Bloom_t3259153605::get_offset_of_tweakMode_6(),
	Bloom_t3259153605::get_offset_of_screenBlendMode_7(),
	Bloom_t3259153605::get_offset_of_hdr_8(),
	Bloom_t3259153605::get_offset_of_doHdr_9(),
	Bloom_t3259153605::get_offset_of_sepBlurSpread_10(),
	Bloom_t3259153605::get_offset_of_quality_11(),
	Bloom_t3259153605::get_offset_of_bloomIntensity_12(),
	Bloom_t3259153605::get_offset_of_bloomThreshold_13(),
	Bloom_t3259153605::get_offset_of_bloomThresholdColor_14(),
	Bloom_t3259153605::get_offset_of_bloomBlurIterations_15(),
	Bloom_t3259153605::get_offset_of_hollywoodFlareBlurIterations_16(),
	Bloom_t3259153605::get_offset_of_flareRotation_17(),
	Bloom_t3259153605::get_offset_of_lensflareMode_18(),
	Bloom_t3259153605::get_offset_of_hollyStretchWidth_19(),
	Bloom_t3259153605::get_offset_of_lensflareIntensity_20(),
	Bloom_t3259153605::get_offset_of_lensflareThreshold_21(),
	Bloom_t3259153605::get_offset_of_lensFlareSaturation_22(),
	Bloom_t3259153605::get_offset_of_flareColorA_23(),
	Bloom_t3259153605::get_offset_of_flareColorB_24(),
	Bloom_t3259153605::get_offset_of_flareColorC_25(),
	Bloom_t3259153605::get_offset_of_flareColorD_26(),
	Bloom_t3259153605::get_offset_of_lensFlareVignetteMask_27(),
	Bloom_t3259153605::get_offset_of_lensFlareShader_28(),
	Bloom_t3259153605::get_offset_of_lensFlareMaterial_29(),
	Bloom_t3259153605::get_offset_of_screenBlendShader_30(),
	Bloom_t3259153605::get_offset_of_screenBlend_31(),
	Bloom_t3259153605::get_offset_of_blurAndFlaresShader_32(),
	Bloom_t3259153605::get_offset_of_blurAndFlaresMaterial_33(),
	Bloom_t3259153605::get_offset_of_brightPassFilterShader_34(),
	Bloom_t3259153605::get_offset_of_brightPassFilterMaterial_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (LensFlareStyle_t2543710313)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3052[4] = 
{
	LensFlareStyle_t2543710313::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (TweakMode_t4077469257)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3053[3] = 
{
	TweakMode_t4077469257::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (HDRBloomMode_t1274006168)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3054[4] = 
{
	HDRBloomMode_t1274006168::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (BloomScreenBlendMode_t3629569411)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3055[3] = 
{
	BloomScreenBlendMode_t3629569411::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (BloomQuality_t1989801646)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3056[3] = 
{
	BloomQuality_t1989801646::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (LensflareStyle34_t3790432936)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3057[4] = 
{
	LensflareStyle34_t3790432936::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (TweakMode34_t496834990)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3058[3] = 
{
	TweakMode34_t496834990::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (HDRBloomMode_t3850268328)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3059[4] = 
{
	HDRBloomMode_t3850268328::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { sizeof (BloomScreenBlendMode_t681392339)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3060[3] = 
{
	BloomScreenBlendMode_t681392339::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (BloomAndFlares_t2913820361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3061[34] = 
{
	BloomAndFlares_t2913820361::get_offset_of_tweakMode_6(),
	BloomAndFlares_t2913820361::get_offset_of_screenBlendMode_7(),
	BloomAndFlares_t2913820361::get_offset_of_hdr_8(),
	BloomAndFlares_t2913820361::get_offset_of_doHdr_9(),
	BloomAndFlares_t2913820361::get_offset_of_sepBlurSpread_10(),
	BloomAndFlares_t2913820361::get_offset_of_useSrcAlphaAsMask_11(),
	BloomAndFlares_t2913820361::get_offset_of_bloomIntensity_12(),
	BloomAndFlares_t2913820361::get_offset_of_bloomThreshold_13(),
	BloomAndFlares_t2913820361::get_offset_of_bloomBlurIterations_14(),
	BloomAndFlares_t2913820361::get_offset_of_lensflares_15(),
	BloomAndFlares_t2913820361::get_offset_of_hollywoodFlareBlurIterations_16(),
	BloomAndFlares_t2913820361::get_offset_of_lensflareMode_17(),
	BloomAndFlares_t2913820361::get_offset_of_hollyStretchWidth_18(),
	BloomAndFlares_t2913820361::get_offset_of_lensflareIntensity_19(),
	BloomAndFlares_t2913820361::get_offset_of_lensflareThreshold_20(),
	BloomAndFlares_t2913820361::get_offset_of_flareColorA_21(),
	BloomAndFlares_t2913820361::get_offset_of_flareColorB_22(),
	BloomAndFlares_t2913820361::get_offset_of_flareColorC_23(),
	BloomAndFlares_t2913820361::get_offset_of_flareColorD_24(),
	BloomAndFlares_t2913820361::get_offset_of_lensFlareVignetteMask_25(),
	BloomAndFlares_t2913820361::get_offset_of_lensFlareShader_26(),
	BloomAndFlares_t2913820361::get_offset_of_lensFlareMaterial_27(),
	BloomAndFlares_t2913820361::get_offset_of_vignetteShader_28(),
	BloomAndFlares_t2913820361::get_offset_of_vignetteMaterial_29(),
	BloomAndFlares_t2913820361::get_offset_of_separableBlurShader_30(),
	BloomAndFlares_t2913820361::get_offset_of_separableBlurMaterial_31(),
	BloomAndFlares_t2913820361::get_offset_of_addBrightStuffOneOneShader_32(),
	BloomAndFlares_t2913820361::get_offset_of_addBrightStuffBlendOneOneMaterial_33(),
	BloomAndFlares_t2913820361::get_offset_of_screenBlendShader_34(),
	BloomAndFlares_t2913820361::get_offset_of_screenBlend_35(),
	BloomAndFlares_t2913820361::get_offset_of_hollywoodFlaresShader_36(),
	BloomAndFlares_t2913820361::get_offset_of_hollywoodFlaresMaterial_37(),
	BloomAndFlares_t2913820361::get_offset_of_brightPassFilterShader_38(),
	BloomAndFlares_t2913820361::get_offset_of_brightPassFilterMaterial_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (BloomOptimized_t579860294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3062[8] = 
{
	BloomOptimized_t579860294::get_offset_of_threshold_6(),
	BloomOptimized_t579860294::get_offset_of_intensity_7(),
	BloomOptimized_t579860294::get_offset_of_blurSize_8(),
	BloomOptimized_t579860294::get_offset_of_resolution_9(),
	BloomOptimized_t579860294::get_offset_of_blurIterations_10(),
	BloomOptimized_t579860294::get_offset_of_blurType_11(),
	BloomOptimized_t579860294::get_offset_of_fastBloomShader_12(),
	BloomOptimized_t579860294::get_offset_of_fastBloomMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (Resolution_t2174076389)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3063[3] = 
{
	Resolution_t2174076389::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (BlurType_t2318278682)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3064[3] = 
{
	BlurType_t2318278682::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (Blur_t3313275655), -1, sizeof(Blur_t3313275655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3065[4] = 
{
	Blur_t3313275655::get_offset_of_iterations_2(),
	Blur_t3313275655::get_offset_of_blurSpread_3(),
	Blur_t3313275655::get_offset_of_blurShader_4(),
	Blur_t3313275655_StaticFields::get_offset_of_m_Material_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (BlurOptimized_t2905677972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3066[6] = 
{
	BlurOptimized_t2905677972::get_offset_of_downsample_6(),
	BlurOptimized_t2905677972::get_offset_of_blurSize_7(),
	BlurOptimized_t2905677972::get_offset_of_blurIterations_8(),
	BlurOptimized_t2905677972::get_offset_of_blurType_9(),
	BlurOptimized_t2905677972::get_offset_of_blurShader_10(),
	BlurOptimized_t2905677972::get_offset_of_blurMaterial_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { sizeof (BlurType_t2925629392)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3067[3] = 
{
	BlurType_t2925629392::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (CameraMotionBlur_t3591885494), -1, sizeof(CameraMotionBlur_t3591885494_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3068[32] = 
{
	CameraMotionBlur_t3591885494_StaticFields::get_offset_of_MAX_RADIUS_6(),
	CameraMotionBlur_t3591885494::get_offset_of_filterType_7(),
	CameraMotionBlur_t3591885494::get_offset_of_preview_8(),
	CameraMotionBlur_t3591885494::get_offset_of_previewScale_9(),
	CameraMotionBlur_t3591885494::get_offset_of_movementScale_10(),
	CameraMotionBlur_t3591885494::get_offset_of_rotationScale_11(),
	CameraMotionBlur_t3591885494::get_offset_of_maxVelocity_12(),
	CameraMotionBlur_t3591885494::get_offset_of_minVelocity_13(),
	CameraMotionBlur_t3591885494::get_offset_of_velocityScale_14(),
	CameraMotionBlur_t3591885494::get_offset_of_softZDistance_15(),
	CameraMotionBlur_t3591885494::get_offset_of_velocityDownsample_16(),
	CameraMotionBlur_t3591885494::get_offset_of_excludeLayers_17(),
	CameraMotionBlur_t3591885494::get_offset_of_tmpCam_18(),
	CameraMotionBlur_t3591885494::get_offset_of_shader_19(),
	CameraMotionBlur_t3591885494::get_offset_of_dx11MotionBlurShader_20(),
	CameraMotionBlur_t3591885494::get_offset_of_replacementClear_21(),
	CameraMotionBlur_t3591885494::get_offset_of_motionBlurMaterial_22(),
	CameraMotionBlur_t3591885494::get_offset_of_dx11MotionBlurMaterial_23(),
	CameraMotionBlur_t3591885494::get_offset_of_noiseTexture_24(),
	CameraMotionBlur_t3591885494::get_offset_of_jitter_25(),
	CameraMotionBlur_t3591885494::get_offset_of_showVelocity_26(),
	CameraMotionBlur_t3591885494::get_offset_of_showVelocityScale_27(),
	CameraMotionBlur_t3591885494::get_offset_of_currentViewProjMat_28(),
	CameraMotionBlur_t3591885494::get_offset_of_currentStereoViewProjMat_29(),
	CameraMotionBlur_t3591885494::get_offset_of_prevViewProjMat_30(),
	CameraMotionBlur_t3591885494::get_offset_of_prevStereoViewProjMat_31(),
	CameraMotionBlur_t3591885494::get_offset_of_prevFrameCount_32(),
	CameraMotionBlur_t3591885494::get_offset_of_wasActive_33(),
	CameraMotionBlur_t3591885494::get_offset_of_prevFrameForward_34(),
	CameraMotionBlur_t3591885494::get_offset_of_prevFrameUp_35(),
	CameraMotionBlur_t3591885494::get_offset_of_prevFramePos_36(),
	CameraMotionBlur_t3591885494::get_offset_of__camera_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { sizeof (MotionBlurFilter_t3964716834)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3069[6] = 
{
	MotionBlurFilter_t3964716834::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (ColorCorrectionCurves_t3523901841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3070[24] = 
{
	ColorCorrectionCurves_t3523901841::get_offset_of_redChannel_6(),
	ColorCorrectionCurves_t3523901841::get_offset_of_greenChannel_7(),
	ColorCorrectionCurves_t3523901841::get_offset_of_blueChannel_8(),
	ColorCorrectionCurves_t3523901841::get_offset_of_useDepthCorrection_9(),
	ColorCorrectionCurves_t3523901841::get_offset_of_zCurve_10(),
	ColorCorrectionCurves_t3523901841::get_offset_of_depthRedChannel_11(),
	ColorCorrectionCurves_t3523901841::get_offset_of_depthGreenChannel_12(),
	ColorCorrectionCurves_t3523901841::get_offset_of_depthBlueChannel_13(),
	ColorCorrectionCurves_t3523901841::get_offset_of_ccMaterial_14(),
	ColorCorrectionCurves_t3523901841::get_offset_of_ccDepthMaterial_15(),
	ColorCorrectionCurves_t3523901841::get_offset_of_selectiveCcMaterial_16(),
	ColorCorrectionCurves_t3523901841::get_offset_of_rgbChannelTex_17(),
	ColorCorrectionCurves_t3523901841::get_offset_of_rgbDepthChannelTex_18(),
	ColorCorrectionCurves_t3523901841::get_offset_of_zCurveTex_19(),
	ColorCorrectionCurves_t3523901841::get_offset_of_saturation_20(),
	ColorCorrectionCurves_t3523901841::get_offset_of_selectiveCc_21(),
	ColorCorrectionCurves_t3523901841::get_offset_of_selectiveFromColor_22(),
	ColorCorrectionCurves_t3523901841::get_offset_of_selectiveToColor_23(),
	ColorCorrectionCurves_t3523901841::get_offset_of_mode_24(),
	ColorCorrectionCurves_t3523901841::get_offset_of_updateTextures_25(),
	ColorCorrectionCurves_t3523901841::get_offset_of_colorCorrectionCurvesShader_26(),
	ColorCorrectionCurves_t3523901841::get_offset_of_simpleColorCorrectionCurvesShader_27(),
	ColorCorrectionCurves_t3523901841::get_offset_of_colorCorrectionSelectiveShader_28(),
	ColorCorrectionCurves_t3523901841::get_offset_of_updateTexturesOnStartup_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (ColorCorrectionMode_t3694918262)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3071[3] = 
{
	ColorCorrectionMode_t3694918262::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (ColorCorrectionLookup_t2385315007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3072[4] = 
{
	ColorCorrectionLookup_t2385315007::get_offset_of_shader_6(),
	ColorCorrectionLookup_t2385315007::get_offset_of_material_7(),
	ColorCorrectionLookup_t2385315007::get_offset_of_converted3DLut_8(),
	ColorCorrectionLookup_t2385315007::get_offset_of_basedOnTempTex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (ColorCorrectionRamp_t1672908095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3073[1] = 
{
	ColorCorrectionRamp_t1672908095::get_offset_of_textureRamp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (ContrastEnhance_t163796146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3074[7] = 
{
	ContrastEnhance_t163796146::get_offset_of_intensity_6(),
	ContrastEnhance_t163796146::get_offset_of_threshold_7(),
	ContrastEnhance_t163796146::get_offset_of_separableBlurMaterial_8(),
	ContrastEnhance_t163796146::get_offset_of_contrastCompositeMaterial_9(),
	ContrastEnhance_t163796146::get_offset_of_blurSpread_10(),
	ContrastEnhance_t163796146::get_offset_of_separableBlurShader_11(),
	ContrastEnhance_t163796146::get_offset_of_contrastCompositeShader_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (ContrastStretch_t3133742431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3075[13] = 
{
	ContrastStretch_t3133742431::get_offset_of_adaptationSpeed_2(),
	ContrastStretch_t3133742431::get_offset_of_limitMinimum_3(),
	ContrastStretch_t3133742431::get_offset_of_limitMaximum_4(),
	ContrastStretch_t3133742431::get_offset_of_adaptRenderTex_5(),
	ContrastStretch_t3133742431::get_offset_of_curAdaptIndex_6(),
	ContrastStretch_t3133742431::get_offset_of_shaderLum_7(),
	ContrastStretch_t3133742431::get_offset_of_m_materialLum_8(),
	ContrastStretch_t3133742431::get_offset_of_shaderReduce_9(),
	ContrastStretch_t3133742431::get_offset_of_m_materialReduce_10(),
	ContrastStretch_t3133742431::get_offset_of_shaderAdapt_11(),
	ContrastStretch_t3133742431::get_offset_of_m_materialAdapt_12(),
	ContrastStretch_t3133742431::get_offset_of_shaderApply_13(),
	ContrastStretch_t3133742431::get_offset_of_m_materialApply_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { sizeof (CreaseShading_t3095868303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3076[9] = 
{
	CreaseShading_t3095868303::get_offset_of_intensity_6(),
	CreaseShading_t3095868303::get_offset_of_softness_7(),
	CreaseShading_t3095868303::get_offset_of_spread_8(),
	CreaseShading_t3095868303::get_offset_of_blurShader_9(),
	CreaseShading_t3095868303::get_offset_of_blurMaterial_10(),
	CreaseShading_t3095868303::get_offset_of_depthFetchShader_11(),
	CreaseShading_t3095868303::get_offset_of_depthFetchMaterial_12(),
	CreaseShading_t3095868303::get_offset_of_creaseApplyShader_13(),
	CreaseShading_t3095868303::get_offset_of_creaseApplyMaterial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (DepthOfField_t3961919654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3077[25] = 
{
	DepthOfField_t3961919654::get_offset_of_visualizeFocus_6(),
	DepthOfField_t3961919654::get_offset_of_focalLength_7(),
	DepthOfField_t3961919654::get_offset_of_focalSize_8(),
	DepthOfField_t3961919654::get_offset_of_aperture_9(),
	DepthOfField_t3961919654::get_offset_of_focalTransform_10(),
	DepthOfField_t3961919654::get_offset_of_maxBlurSize_11(),
	DepthOfField_t3961919654::get_offset_of_highResolution_12(),
	DepthOfField_t3961919654::get_offset_of_blurType_13(),
	DepthOfField_t3961919654::get_offset_of_blurSampleCount_14(),
	DepthOfField_t3961919654::get_offset_of_nearBlur_15(),
	DepthOfField_t3961919654::get_offset_of_foregroundOverlap_16(),
	DepthOfField_t3961919654::get_offset_of_dofHdrShader_17(),
	DepthOfField_t3961919654::get_offset_of_dofHdrMaterial_18(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehShader_19(),
	DepthOfField_t3961919654::get_offset_of_dx11bokehMaterial_20(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehThreshold_21(),
	DepthOfField_t3961919654::get_offset_of_dx11SpawnHeuristic_22(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehTexture_23(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehScale_24(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehIntensity_25(),
	DepthOfField_t3961919654::get_offset_of_focalDistance01_26(),
	DepthOfField_t3961919654::get_offset_of_cbDrawArgs_27(),
	DepthOfField_t3961919654::get_offset_of_cbPoints_28(),
	DepthOfField_t3961919654::get_offset_of_internalBlurWidth_29(),
	DepthOfField_t3961919654::get_offset_of_cachedCamera_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (BlurType_t2459464934)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3078[3] = 
{
	BlurType_t2459464934::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (BlurSampleCount_t3059663215)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3079[4] = 
{
	BlurSampleCount_t3059663215::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (DepthOfFieldDeprecated_t3904103385), -1, sizeof(DepthOfFieldDeprecated_t3904103385_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3080[43] = 
{
	DepthOfFieldDeprecated_t3904103385_StaticFields::get_offset_of_SMOOTH_DOWNSAMPLE_PASS_6(),
	DepthOfFieldDeprecated_t3904103385_StaticFields::get_offset_of_BOKEH_EXTRA_BLUR_7(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_quality_8(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_resolution_9(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_simpleTweakMode_10(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalPoint_11(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_smoothness_12(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalZDistance_13(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalZStartCurve_14(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalZEndCurve_15(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalStartCurve_16(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalEndCurve_17(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalDistance01_18(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_objectFocus_19(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalSize_20(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bluriness_21(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_maxBlurSpread_22(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_foregroundBlurExtrude_23(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_dofBlurShader_24(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_dofBlurMaterial_25(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_dofShader_26(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_dofMaterial_27(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_visualize_28(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehDestination_29(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_widthOverHeight_30(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_oneOverBaseSize_31(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokeh_32(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehSupport_33(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehShader_34(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehTexture_35(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehScale_36(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehIntensity_37(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehThresholdContrast_38(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehThresholdLuminance_39(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehDownsample_40(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehMaterial_41(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of__camera_42(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_foregroundTexture_43(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_mediumRezWorkTexture_44(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_finalDefocus_45(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_lowRezWorkTexture_46(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehSource_47(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehSource2_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (Dof34QualitySetting_t3902263335)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3081[3] = 
{
	Dof34QualitySetting_t3902263335::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (DofResolution_t457390305)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3082[4] = 
{
	DofResolution_t457390305::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (DofBlurriness_t1921949650)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3083[4] = 
{
	DofBlurriness_t1921949650::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (BokehDestination_t1282715729)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3084[4] = 
{
	BokehDestination_t1282715729::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (EdgeDetection_t785857862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3085[11] = 
{
	EdgeDetection_t785857862::get_offset_of_mode_6(),
	EdgeDetection_t785857862::get_offset_of_sensitivityDepth_7(),
	EdgeDetection_t785857862::get_offset_of_sensitivityNormals_8(),
	EdgeDetection_t785857862::get_offset_of_lumThreshold_9(),
	EdgeDetection_t785857862::get_offset_of_edgeExp_10(),
	EdgeDetection_t785857862::get_offset_of_sampleDist_11(),
	EdgeDetection_t785857862::get_offset_of_edgesOnly_12(),
	EdgeDetection_t785857862::get_offset_of_edgesOnlyBgColor_13(),
	EdgeDetection_t785857862::get_offset_of_edgeDetectShader_14(),
	EdgeDetection_t785857862::get_offset_of_edgeDetectMaterial_15(),
	EdgeDetection_t785857862::get_offset_of_oldMode_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (EdgeDetectMode_t859433922)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3086[6] = 
{
	EdgeDetectMode_t859433922::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (Fisheye_t863619287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3087[4] = 
{
	Fisheye_t863619287::get_offset_of_strengthX_6(),
	Fisheye_t863619287::get_offset_of_strengthY_7(),
	Fisheye_t863619287::get_offset_of_fishEyeShader_8(),
	Fisheye_t863619287::get_offset_of_fisheyeMaterial_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (GlobalFog_t3948012467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3088[9] = 
{
	GlobalFog_t3948012467::get_offset_of_distanceFog_6(),
	GlobalFog_t3948012467::get_offset_of_excludeFarPixels_7(),
	GlobalFog_t3948012467::get_offset_of_useRadialDistance_8(),
	GlobalFog_t3948012467::get_offset_of_heightFog_9(),
	GlobalFog_t3948012467::get_offset_of_height_10(),
	GlobalFog_t3948012467::get_offset_of_heightDensity_11(),
	GlobalFog_t3948012467::get_offset_of_startDistance_12(),
	GlobalFog_t3948012467::get_offset_of_fogShader_13(),
	GlobalFog_t3948012467::get_offset_of_fogMaterial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (Grayscale_t4036041399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3089[2] = 
{
	Grayscale_t4036041399::get_offset_of_textureRamp_4(),
	Grayscale_t4036041399::get_offset_of_rampOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (ImageEffectBase_t517806655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3090[2] = 
{
	ImageEffectBase_t517806655::get_offset_of_shader_2(),
	ImageEffectBase_t517806655::get_offset_of_m_Material_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (ImageEffects_t2907318477), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (MotionBlur_t1887427701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3092[3] = 
{
	MotionBlur_t1887427701::get_offset_of_blurAmount_4(),
	MotionBlur_t1887427701::get_offset_of_extraBlur_5(),
	MotionBlur_t1887427701::get_offset_of_accumTexture_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (NoiseAndGrain_t2899312312), -1, sizeof(NoiseAndGrain_t2899312312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3093[18] = 
{
	NoiseAndGrain_t2899312312::get_offset_of_intensityMultiplier_6(),
	NoiseAndGrain_t2899312312::get_offset_of_generalIntensity_7(),
	NoiseAndGrain_t2899312312::get_offset_of_blackIntensity_8(),
	NoiseAndGrain_t2899312312::get_offset_of_whiteIntensity_9(),
	NoiseAndGrain_t2899312312::get_offset_of_midGrey_10(),
	NoiseAndGrain_t2899312312::get_offset_of_dx11Grain_11(),
	NoiseAndGrain_t2899312312::get_offset_of_softness_12(),
	NoiseAndGrain_t2899312312::get_offset_of_monochrome_13(),
	NoiseAndGrain_t2899312312::get_offset_of_intensities_14(),
	NoiseAndGrain_t2899312312::get_offset_of_tiling_15(),
	NoiseAndGrain_t2899312312::get_offset_of_monochromeTiling_16(),
	NoiseAndGrain_t2899312312::get_offset_of_filterMode_17(),
	NoiseAndGrain_t2899312312::get_offset_of_noiseTexture_18(),
	NoiseAndGrain_t2899312312::get_offset_of_noiseShader_19(),
	NoiseAndGrain_t2899312312::get_offset_of_noiseMaterial_20(),
	NoiseAndGrain_t2899312312::get_offset_of_dx11NoiseShader_21(),
	NoiseAndGrain_t2899312312::get_offset_of_dx11NoiseMaterial_22(),
	NoiseAndGrain_t2899312312_StaticFields::get_offset_of_TILE_AMOUNT_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (NoiseAndScratches_t3375869057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3094[18] = 
{
	NoiseAndScratches_t3375869057::get_offset_of_monochrome_2(),
	NoiseAndScratches_t3375869057::get_offset_of_rgbFallback_3(),
	NoiseAndScratches_t3375869057::get_offset_of_grainIntensityMin_4(),
	NoiseAndScratches_t3375869057::get_offset_of_grainIntensityMax_5(),
	NoiseAndScratches_t3375869057::get_offset_of_grainSize_6(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchIntensityMin_7(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchIntensityMax_8(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchFPS_9(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchJitter_10(),
	NoiseAndScratches_t3375869057::get_offset_of_grainTexture_11(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchTexture_12(),
	NoiseAndScratches_t3375869057::get_offset_of_shaderRGB_13(),
	NoiseAndScratches_t3375869057::get_offset_of_shaderYUV_14(),
	NoiseAndScratches_t3375869057::get_offset_of_m_MaterialRGB_15(),
	NoiseAndScratches_t3375869057::get_offset_of_m_MaterialYUV_16(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchTimeLeft_17(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchX_18(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchY_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (PostEffectsBase_t2152133263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3095[4] = 
{
	PostEffectsBase_t2152133263::get_offset_of_supportHDRTextures_2(),
	PostEffectsBase_t2152133263::get_offset_of_supportDX11_3(),
	PostEffectsBase_t2152133263::get_offset_of_isSupported_4(),
	PostEffectsBase_t2152133263::get_offset_of_createdMaterials_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (PostEffectsHelper_t1845967802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (Quads_t761286994), -1, sizeof(Quads_t761286994_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3097[2] = 
{
	Quads_t761286994_StaticFields::get_offset_of_meshes_0(),
	Quads_t761286994_StaticFields::get_offset_of_currentQuads_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (ScreenOverlay_t3972324734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3098[5] = 
{
	ScreenOverlay_t3972324734::get_offset_of_blendMode_6(),
	ScreenOverlay_t3972324734::get_offset_of_intensity_7(),
	ScreenOverlay_t3972324734::get_offset_of_texture_8(),
	ScreenOverlay_t3972324734::get_offset_of_overlayShader_9(),
	ScreenOverlay_t3972324734::get_offset_of_overlayMaterial_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (OverlayBlendMode_t962057283)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3099[6] = 
{
	OverlayBlendMode_t962057283::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
