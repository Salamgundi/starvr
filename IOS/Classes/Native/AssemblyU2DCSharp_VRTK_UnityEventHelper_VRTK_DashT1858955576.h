﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_DashTeleport
struct VRTK_DashTeleport_t1206199485;
// VRTK.UnityEventHelper.VRTK_DashTeleport_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t1688415125;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_DashTeleport_UnityEvents
struct  VRTK_DashTeleport_UnityEvents_t1858955576  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_DashTeleport VRTK.UnityEventHelper.VRTK_DashTeleport_UnityEvents::dt
	VRTK_DashTeleport_t1206199485 * ___dt_2;
	// VRTK.UnityEventHelper.VRTK_DashTeleport_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_DashTeleport_UnityEvents::OnWillDashThruObjects
	UnityObjectEvent_t1688415125 * ___OnWillDashThruObjects_3;
	// VRTK.UnityEventHelper.VRTK_DashTeleport_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_DashTeleport_UnityEvents::OnDashedThruObjects
	UnityObjectEvent_t1688415125 * ___OnDashedThruObjects_4;

public:
	inline static int32_t get_offset_of_dt_2() { return static_cast<int32_t>(offsetof(VRTK_DashTeleport_UnityEvents_t1858955576, ___dt_2)); }
	inline VRTK_DashTeleport_t1206199485 * get_dt_2() const { return ___dt_2; }
	inline VRTK_DashTeleport_t1206199485 ** get_address_of_dt_2() { return &___dt_2; }
	inline void set_dt_2(VRTK_DashTeleport_t1206199485 * value)
	{
		___dt_2 = value;
		Il2CppCodeGenWriteBarrier(&___dt_2, value);
	}

	inline static int32_t get_offset_of_OnWillDashThruObjects_3() { return static_cast<int32_t>(offsetof(VRTK_DashTeleport_UnityEvents_t1858955576, ___OnWillDashThruObjects_3)); }
	inline UnityObjectEvent_t1688415125 * get_OnWillDashThruObjects_3() const { return ___OnWillDashThruObjects_3; }
	inline UnityObjectEvent_t1688415125 ** get_address_of_OnWillDashThruObjects_3() { return &___OnWillDashThruObjects_3; }
	inline void set_OnWillDashThruObjects_3(UnityObjectEvent_t1688415125 * value)
	{
		___OnWillDashThruObjects_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnWillDashThruObjects_3, value);
	}

	inline static int32_t get_offset_of_OnDashedThruObjects_4() { return static_cast<int32_t>(offsetof(VRTK_DashTeleport_UnityEvents_t1858955576, ___OnDashedThruObjects_4)); }
	inline UnityObjectEvent_t1688415125 * get_OnDashedThruObjects_4() const { return ___OnDashedThruObjects_4; }
	inline UnityObjectEvent_t1688415125 ** get_address_of_OnDashedThruObjects_4() { return &___OnDashedThruObjects_4; }
	inline void set_OnDashedThruObjects_4(UnityObjectEvent_t1688415125 * value)
	{
		___OnDashedThruObjects_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnDashedThruObjects_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
