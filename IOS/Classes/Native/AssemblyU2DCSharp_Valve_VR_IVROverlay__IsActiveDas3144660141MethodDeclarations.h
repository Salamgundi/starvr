﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_IsActiveDashboardOverlay
struct _IsActiveDashboardOverlay_t3144660141;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_IsActiveDashboardOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _IsActiveDashboardOverlay__ctor_m279968522 (_IsActiveDashboardOverlay_t3144660141 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVROverlay/_IsActiveDashboardOverlay::Invoke(System.UInt64)
extern "C"  bool _IsActiveDashboardOverlay_Invoke_m3692294895 (_IsActiveDashboardOverlay_t3144660141 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_IsActiveDashboardOverlay::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _IsActiveDashboardOverlay_BeginInvoke_m2018969752 (_IsActiveDashboardOverlay_t3144660141 * __this, uint64_t ___ulOverlayHandle0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVROverlay/_IsActiveDashboardOverlay::EndInvoke(System.IAsyncResult)
extern "C"  bool _IsActiveDashboardOverlay_EndInvoke_m2341904986 (_IsActiveDashboardOverlay_t3144660141 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
