﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.TeleportArc
struct TeleportArc_t2123987351;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void Valve.VR.InteractionSystem.TeleportArc::.ctor()
extern "C"  void TeleportArc__ctor_m3131566183 (TeleportArc_t2123987351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArc::Start()
extern "C"  void TeleportArc_Start_m195935659 (TeleportArc_t2123987351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArc::Update()
extern "C"  void TeleportArc_Update_m2804725070 (TeleportArc_t2123987351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArc::CreateLineRendererObjects()
extern "C"  void TeleportArc_CreateLineRendererObjects_m1619753188 (TeleportArc_t2123987351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArc::SetArcData(UnityEngine.Vector3,UnityEngine.Vector3,System.Boolean,System.Boolean)
extern "C"  void TeleportArc_SetArcData_m1947693085 (TeleportArc_t2123987351 * __this, Vector3_t2243707580  ___position0, Vector3_t2243707580  ___velocity1, bool ___gravity2, bool ___pointerAtBadAngle3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArc::Show()
extern "C"  void TeleportArc_Show_m1054113678 (TeleportArc_t2123987351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArc::Hide()
extern "C"  void TeleportArc_Hide_m261621791 (TeleportArc_t2123987351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.TeleportArc::DrawArc(UnityEngine.RaycastHit&)
extern "C"  bool TeleportArc_DrawArc_m1116279522 (TeleportArc_t2123987351 * __this, RaycastHit_t87180320 * ___hitInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArc::DrawArcSegment(System.Int32,System.Single,System.Single)
extern "C"  void TeleportArc_DrawArcSegment_m802279333 (TeleportArc_t2123987351 * __this, int32_t ___index0, float ___startTime1, float ___endTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArc::SetColor(UnityEngine.Color)
extern "C"  void TeleportArc_SetColor_m1171504352 (TeleportArc_t2123987351 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.InteractionSystem.TeleportArc::FindProjectileCollision(UnityEngine.RaycastHit&)
extern "C"  float TeleportArc_FindProjectileCollision_m1684590408 (TeleportArc_t2123987351 * __this, RaycastHit_t87180320 * ___hitInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Valve.VR.InteractionSystem.TeleportArc::GetArcPositionAtTime(System.Single)
extern "C"  Vector3_t2243707580  TeleportArc_GetArcPositionAtTime_m735766327 (TeleportArc_t2123987351 * __this, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArc::HideLineSegments(System.Int32,System.Int32)
extern "C"  void TeleportArc_HideLineSegments_m1028184865 (TeleportArc_t2123987351 * __this, int32_t ___startSegment0, int32_t ___endSegment1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
