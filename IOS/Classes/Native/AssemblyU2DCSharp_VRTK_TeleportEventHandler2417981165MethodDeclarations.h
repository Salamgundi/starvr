﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.TeleportEventHandler
struct TeleportEventHandler_t2417981165;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventArgs1852451661.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.TeleportEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void TeleportEventHandler__ctor_m1422155295 (TeleportEventHandler_t2417981165 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.TeleportEventHandler::Invoke(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void TeleportEventHandler_Invoke_m3244848869 (TeleportEventHandler_t2417981165 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.TeleportEventHandler::BeginInvoke(System.Object,VRTK.DestinationMarkerEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TeleportEventHandler_BeginInvoke_m1378318876 (TeleportEventHandler_t2417981165 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.TeleportEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void TeleportEventHandler_EndInvoke_m69629417 (TeleportEventHandler_t2417981165 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
