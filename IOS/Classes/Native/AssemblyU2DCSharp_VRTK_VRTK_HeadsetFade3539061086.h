﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.HeadsetFadeEventHandler
struct HeadsetFadeEventHandler_t2012619754;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_HeadsetFade
struct  VRTK_HeadsetFade_t3539061086  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.HeadsetFadeEventHandler VRTK.VRTK_HeadsetFade::HeadsetFadeStart
	HeadsetFadeEventHandler_t2012619754 * ___HeadsetFadeStart_2;
	// VRTK.HeadsetFadeEventHandler VRTK.VRTK_HeadsetFade::HeadsetFadeComplete
	HeadsetFadeEventHandler_t2012619754 * ___HeadsetFadeComplete_3;
	// VRTK.HeadsetFadeEventHandler VRTK.VRTK_HeadsetFade::HeadsetUnfadeStart
	HeadsetFadeEventHandler_t2012619754 * ___HeadsetUnfadeStart_4;
	// VRTK.HeadsetFadeEventHandler VRTK.VRTK_HeadsetFade::HeadsetUnfadeComplete
	HeadsetFadeEventHandler_t2012619754 * ___HeadsetUnfadeComplete_5;
	// UnityEngine.Transform VRTK.VRTK_HeadsetFade::headset
	Transform_t3275118058 * ___headset_6;
	// System.Boolean VRTK.VRTK_HeadsetFade::isTransitioning
	bool ___isTransitioning_7;
	// System.Boolean VRTK.VRTK_HeadsetFade::isFaded
	bool ___isFaded_8;

public:
	inline static int32_t get_offset_of_HeadsetFadeStart_2() { return static_cast<int32_t>(offsetof(VRTK_HeadsetFade_t3539061086, ___HeadsetFadeStart_2)); }
	inline HeadsetFadeEventHandler_t2012619754 * get_HeadsetFadeStart_2() const { return ___HeadsetFadeStart_2; }
	inline HeadsetFadeEventHandler_t2012619754 ** get_address_of_HeadsetFadeStart_2() { return &___HeadsetFadeStart_2; }
	inline void set_HeadsetFadeStart_2(HeadsetFadeEventHandler_t2012619754 * value)
	{
		___HeadsetFadeStart_2 = value;
		Il2CppCodeGenWriteBarrier(&___HeadsetFadeStart_2, value);
	}

	inline static int32_t get_offset_of_HeadsetFadeComplete_3() { return static_cast<int32_t>(offsetof(VRTK_HeadsetFade_t3539061086, ___HeadsetFadeComplete_3)); }
	inline HeadsetFadeEventHandler_t2012619754 * get_HeadsetFadeComplete_3() const { return ___HeadsetFadeComplete_3; }
	inline HeadsetFadeEventHandler_t2012619754 ** get_address_of_HeadsetFadeComplete_3() { return &___HeadsetFadeComplete_3; }
	inline void set_HeadsetFadeComplete_3(HeadsetFadeEventHandler_t2012619754 * value)
	{
		___HeadsetFadeComplete_3 = value;
		Il2CppCodeGenWriteBarrier(&___HeadsetFadeComplete_3, value);
	}

	inline static int32_t get_offset_of_HeadsetUnfadeStart_4() { return static_cast<int32_t>(offsetof(VRTK_HeadsetFade_t3539061086, ___HeadsetUnfadeStart_4)); }
	inline HeadsetFadeEventHandler_t2012619754 * get_HeadsetUnfadeStart_4() const { return ___HeadsetUnfadeStart_4; }
	inline HeadsetFadeEventHandler_t2012619754 ** get_address_of_HeadsetUnfadeStart_4() { return &___HeadsetUnfadeStart_4; }
	inline void set_HeadsetUnfadeStart_4(HeadsetFadeEventHandler_t2012619754 * value)
	{
		___HeadsetUnfadeStart_4 = value;
		Il2CppCodeGenWriteBarrier(&___HeadsetUnfadeStart_4, value);
	}

	inline static int32_t get_offset_of_HeadsetUnfadeComplete_5() { return static_cast<int32_t>(offsetof(VRTK_HeadsetFade_t3539061086, ___HeadsetUnfadeComplete_5)); }
	inline HeadsetFadeEventHandler_t2012619754 * get_HeadsetUnfadeComplete_5() const { return ___HeadsetUnfadeComplete_5; }
	inline HeadsetFadeEventHandler_t2012619754 ** get_address_of_HeadsetUnfadeComplete_5() { return &___HeadsetUnfadeComplete_5; }
	inline void set_HeadsetUnfadeComplete_5(HeadsetFadeEventHandler_t2012619754 * value)
	{
		___HeadsetUnfadeComplete_5 = value;
		Il2CppCodeGenWriteBarrier(&___HeadsetUnfadeComplete_5, value);
	}

	inline static int32_t get_offset_of_headset_6() { return static_cast<int32_t>(offsetof(VRTK_HeadsetFade_t3539061086, ___headset_6)); }
	inline Transform_t3275118058 * get_headset_6() const { return ___headset_6; }
	inline Transform_t3275118058 ** get_address_of_headset_6() { return &___headset_6; }
	inline void set_headset_6(Transform_t3275118058 * value)
	{
		___headset_6 = value;
		Il2CppCodeGenWriteBarrier(&___headset_6, value);
	}

	inline static int32_t get_offset_of_isTransitioning_7() { return static_cast<int32_t>(offsetof(VRTK_HeadsetFade_t3539061086, ___isTransitioning_7)); }
	inline bool get_isTransitioning_7() const { return ___isTransitioning_7; }
	inline bool* get_address_of_isTransitioning_7() { return &___isTransitioning_7; }
	inline void set_isTransitioning_7(bool value)
	{
		___isTransitioning_7 = value;
	}

	inline static int32_t get_offset_of_isFaded_8() { return static_cast<int32_t>(offsetof(VRTK_HeadsetFade_t3539061086, ___isFaded_8)); }
	inline bool get_isFaded_8() const { return ___isFaded_8; }
	inline bool* get_address_of_isFaded_8() { return &___isFaded_8; }
	inline void set_isFaded_8(bool value)
	{
		___isFaded_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
