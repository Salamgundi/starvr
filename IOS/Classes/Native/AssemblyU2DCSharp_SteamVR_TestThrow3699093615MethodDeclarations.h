﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_TestThrow
struct SteamVR_TestThrow_t3699093615;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_TestThrow::.ctor()
extern "C"  void SteamVR_TestThrow__ctor_m1156623770 (SteamVR_TestThrow_t3699093615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TestThrow::Awake()
extern "C"  void SteamVR_TestThrow_Awake_m2310289355 (SteamVR_TestThrow_t3699093615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TestThrow::FixedUpdate()
extern "C"  void SteamVR_TestThrow_FixedUpdate_m2474464649 (SteamVR_TestThrow_t3699093615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
