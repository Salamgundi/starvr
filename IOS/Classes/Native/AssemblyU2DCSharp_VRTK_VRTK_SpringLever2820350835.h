﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_VRTK_Lever736126558.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_SpringLever
struct  VRTK_SpringLever_t2820350835  : public VRTK_Lever_t736126558
{
public:
	// System.Single VRTK.VRTK_SpringLever::springStrength
	float ___springStrength_25;
	// System.Single VRTK.VRTK_SpringLever::springDamper
	float ___springDamper_26;
	// System.Boolean VRTK.VRTK_SpringLever::snapToNearestLimit
	bool ___snapToNearestLimit_27;
	// System.Boolean VRTK.VRTK_SpringLever::alwaysActive
	bool ___alwaysActive_28;
	// System.Boolean VRTK.VRTK_SpringLever::wasTowardZero
	bool ___wasTowardZero_29;
	// System.Boolean VRTK.VRTK_SpringLever::isGrabbed
	bool ___isGrabbed_30;

public:
	inline static int32_t get_offset_of_springStrength_25() { return static_cast<int32_t>(offsetof(VRTK_SpringLever_t2820350835, ___springStrength_25)); }
	inline float get_springStrength_25() const { return ___springStrength_25; }
	inline float* get_address_of_springStrength_25() { return &___springStrength_25; }
	inline void set_springStrength_25(float value)
	{
		___springStrength_25 = value;
	}

	inline static int32_t get_offset_of_springDamper_26() { return static_cast<int32_t>(offsetof(VRTK_SpringLever_t2820350835, ___springDamper_26)); }
	inline float get_springDamper_26() const { return ___springDamper_26; }
	inline float* get_address_of_springDamper_26() { return &___springDamper_26; }
	inline void set_springDamper_26(float value)
	{
		___springDamper_26 = value;
	}

	inline static int32_t get_offset_of_snapToNearestLimit_27() { return static_cast<int32_t>(offsetof(VRTK_SpringLever_t2820350835, ___snapToNearestLimit_27)); }
	inline bool get_snapToNearestLimit_27() const { return ___snapToNearestLimit_27; }
	inline bool* get_address_of_snapToNearestLimit_27() { return &___snapToNearestLimit_27; }
	inline void set_snapToNearestLimit_27(bool value)
	{
		___snapToNearestLimit_27 = value;
	}

	inline static int32_t get_offset_of_alwaysActive_28() { return static_cast<int32_t>(offsetof(VRTK_SpringLever_t2820350835, ___alwaysActive_28)); }
	inline bool get_alwaysActive_28() const { return ___alwaysActive_28; }
	inline bool* get_address_of_alwaysActive_28() { return &___alwaysActive_28; }
	inline void set_alwaysActive_28(bool value)
	{
		___alwaysActive_28 = value;
	}

	inline static int32_t get_offset_of_wasTowardZero_29() { return static_cast<int32_t>(offsetof(VRTK_SpringLever_t2820350835, ___wasTowardZero_29)); }
	inline bool get_wasTowardZero_29() const { return ___wasTowardZero_29; }
	inline bool* get_address_of_wasTowardZero_29() { return &___wasTowardZero_29; }
	inline void set_wasTowardZero_29(bool value)
	{
		___wasTowardZero_29 = value;
	}

	inline static int32_t get_offset_of_isGrabbed_30() { return static_cast<int32_t>(offsetof(VRTK_SpringLever_t2820350835, ___isGrabbed_30)); }
	inline bool get_isGrabbed_30() const { return ___isGrabbed_30; }
	inline bool* get_address_of_isGrabbed_30() { return &___isGrabbed_30; }
	inline void set_isGrabbed_30(bool value)
	{
		___isGrabbed_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
