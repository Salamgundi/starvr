﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_PlayerClimb
struct VRTK_PlayerClimb_t322130288;
// VRTK.PlayerClimbEventHandler
struct PlayerClimbEventHandler_t2826600604;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_PlayerClimbEventHandler2826600604.h"
#include "AssemblyU2DCSharp_VRTK_PlayerClimbEventArgs2537585745.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventArgs1852451661.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventArgs771291242.h"

// System.Void VRTK.VRTK_PlayerClimb::.ctor()
extern "C"  void VRTK_PlayerClimb__ctor_m393589752 (VRTK_PlayerClimb_t322130288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::add_PlayerClimbStarted(VRTK.PlayerClimbEventHandler)
extern "C"  void VRTK_PlayerClimb_add_PlayerClimbStarted_m2189372694 (VRTK_PlayerClimb_t322130288 * __this, PlayerClimbEventHandler_t2826600604 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::remove_PlayerClimbStarted(VRTK.PlayerClimbEventHandler)
extern "C"  void VRTK_PlayerClimb_remove_PlayerClimbStarted_m3731437509 (VRTK_PlayerClimb_t322130288 * __this, PlayerClimbEventHandler_t2826600604 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::add_PlayerClimbEnded(VRTK.PlayerClimbEventHandler)
extern "C"  void VRTK_PlayerClimb_add_PlayerClimbEnded_m568622417 (VRTK_PlayerClimb_t322130288 * __this, PlayerClimbEventHandler_t2826600604 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::remove_PlayerClimbEnded(VRTK.PlayerClimbEventHandler)
extern "C"  void VRTK_PlayerClimb_remove_PlayerClimbEnded_m1942394052 (VRTK_PlayerClimb_t322130288 * __this, PlayerClimbEventHandler_t2826600604 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::Awake()
extern "C"  void VRTK_PlayerClimb_Awake_m1789433465 (VRTK_PlayerClimb_t322130288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::OnEnable()
extern "C"  void VRTK_PlayerClimb_OnEnable_m737040064 (VRTK_PlayerClimb_t322130288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::OnDisable()
extern "C"  void VRTK_PlayerClimb_OnDisable_m494430289 (VRTK_PlayerClimb_t322130288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::Update()
extern "C"  void VRTK_PlayerClimb_Update_m2844593373 (VRTK_PlayerClimb_t322130288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::OnPlayerClimbStarted(VRTK.PlayerClimbEventArgs)
extern "C"  void VRTK_PlayerClimb_OnPlayerClimbStarted_m933619964 (VRTK_PlayerClimb_t322130288 * __this, PlayerClimbEventArgs_t2537585745  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::OnPlayerClimbEnded(VRTK.PlayerClimbEventArgs)
extern "C"  void VRTK_PlayerClimb_OnPlayerClimbEnded_m947097053 (VRTK_PlayerClimb_t322130288 * __this, PlayerClimbEventArgs_t2537585745  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.PlayerClimbEventArgs VRTK.VRTK_PlayerClimb::SetPlayerClimbEvent(System.UInt32,UnityEngine.GameObject)
extern "C"  PlayerClimbEventArgs_t2537585745  VRTK_PlayerClimb_SetPlayerClimbEvent_m3164510155 (VRTK_PlayerClimb_t322130288 * __this, uint32_t ___controllerIndex0, GameObject_t1756533147 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::InitListeners(System.Boolean)
extern "C"  void VRTK_PlayerClimb_InitListeners_m3257764986 (VRTK_PlayerClimb_t322130288 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::InitTeleportListener(System.Boolean)
extern "C"  void VRTK_PlayerClimb_InitTeleportListener_m2260955106 (VRTK_PlayerClimb_t322130288 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::OnTeleport(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_PlayerClimb_OnTeleport_m4177394466 (VRTK_PlayerClimb_t322130288 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_PlayerClimb::GetScaledLocalPosition(UnityEngine.Transform)
extern "C"  Vector3_t2243707580  VRTK_PlayerClimb_GetScaledLocalPosition_m4002154959 (VRTK_PlayerClimb_t322130288 * __this, Transform_t3275118058 * ___objTransform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::OnGrabObject(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_PlayerClimb_OnGrabObject_m1134285953 (VRTK_PlayerClimb_t322130288 * __this, Il2CppObject * ___sender0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::OnUngrabObject(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_PlayerClimb_OnUngrabObject_m4007609208 (VRTK_PlayerClimb_t322130288 * __this, Il2CppObject * ___sender0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::Grab(UnityEngine.GameObject,System.UInt32,UnityEngine.GameObject)
extern "C"  void VRTK_PlayerClimb_Grab_m2977309242 (VRTK_PlayerClimb_t322130288 * __this, GameObject_t1756533147 * ___currentGrabbingController0, uint32_t ___controllerIndex1, GameObject_t1756533147 * ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::Ungrab(System.Boolean,System.UInt32,UnityEngine.GameObject)
extern "C"  void VRTK_PlayerClimb_Ungrab_m2615354466 (VRTK_PlayerClimb_t322130288 * __this, bool ___carryMomentum0, uint32_t ___controllerIndex1, GameObject_t1756533147 * ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_PlayerClimb::IsActiveClimbingController(UnityEngine.GameObject)
extern "C"  bool VRTK_PlayerClimb_IsActiveClimbingController_m1624024519 (VRTK_PlayerClimb_t322130288 * __this, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_PlayerClimb::IsClimbableObject(UnityEngine.GameObject)
extern "C"  bool VRTK_PlayerClimb_IsClimbableObject_m550397498 (VRTK_PlayerClimb_t322130288 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_PlayerClimb::InitControllerListeners(UnityEngine.GameObject,System.Boolean)
extern "C"  void VRTK_PlayerClimb_InitControllerListeners_m1927372676 (VRTK_PlayerClimb_t322130288 * __this, GameObject_t1756533147 * ___controller0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
