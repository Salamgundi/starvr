﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_Slider
struct VRTK_Slider_t2095192727;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_ControlValueRa2976216666.h"

// System.Void VRTK.VRTK_Slider::.ctor()
extern "C"  void VRTK_Slider__ctor_m2445644735 (VRTK_Slider_t2095192727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Slider::OnDrawGizmos()
extern "C"  void VRTK_Slider_OnDrawGizmos_m363304219 (VRTK_Slider_t2095192727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Slider::InitRequiredComponents()
extern "C"  void VRTK_Slider_InitRequiredComponents_m919618940 (VRTK_Slider_t2095192727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_Slider::CalculateDiff(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Boolean)
extern "C"  Vector3_t2243707580  VRTK_Slider_CalculateDiff_m1806443831 (VRTK_Slider_t2095192727 * __this, Vector3_t2243707580  ___initialPosition0, Vector3_t2243707580  ___givenDirection1, float ___scaleValue2, float ___diffMultiplier3, bool ___addition4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Slider::DetectSetup()
extern "C"  bool VRTK_Slider_DetectSetup_m99958275 (VRTK_Slider_t2095192727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_Control/ControlValueRange VRTK.VRTK_Slider::RegisterValueRange()
extern "C"  ControlValueRange_t2976216666  VRTK_Slider_RegisterValueRange_m1327078895 (VRTK_Slider_t2095192727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Slider::HandleUpdate()
extern "C"  void VRTK_Slider_HandleUpdate_m471143768 (VRTK_Slider_t2095192727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Slider::InitRigidbody()
extern "C"  void VRTK_Slider_InitRigidbody_m1255411006 (VRTK_Slider_t2095192727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Slider::InitInteractableObject()
extern "C"  void VRTK_Slider_InitInteractableObject_m983168084 (VRTK_Slider_t2095192727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Slider::InitJoint()
extern "C"  void VRTK_Slider_InitJoint_m3389830003 (VRTK_Slider_t2095192727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Slider::CalculateValue()
extern "C"  void VRTK_Slider_CalculateValue_m579127626 (VRTK_Slider_t2095192727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Slider::ToggleSpring(System.Boolean)
extern "C"  void VRTK_Slider_ToggleSpring_m2395001491 (VRTK_Slider_t2095192727 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Slider::SnapToValue()
extern "C"  void VRTK_Slider_SnapToValue_m3620039251 (VRTK_Slider_t2095192727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
