﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ExplosionWobble
struct ExplosionWobble_t605436430;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Valve.VR.InteractionSystem.ExplosionWobble::.ctor()
extern "C"  void ExplosionWobble__ctor_m1886409630 (ExplosionWobble_t605436430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ExplosionWobble::ExplosionEvent(UnityEngine.Vector3)
extern "C"  void ExplosionWobble_ExplosionEvent_m3955347646 (ExplosionWobble_t605436430 * __this, Vector3_t2243707580  ___explosionPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
