﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_258602264.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m814106894_gshared (KeyValuePair_2_t258602264 * __this, Il2CppObject * ___key0, uint64_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m814106894(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t258602264 *, Il2CppObject *, uint64_t, const MethodInfo*))KeyValuePair_2__ctor_m814106894_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m311169592_gshared (KeyValuePair_2_t258602264 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m311169592(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t258602264 *, const MethodInfo*))KeyValuePair_2_get_Key_m311169592_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3307037513_gshared (KeyValuePair_2_t258602264 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3307037513(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t258602264 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m3307037513_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>::get_Value()
extern "C"  uint64_t KeyValuePair_2_get_Value_m689648760_gshared (KeyValuePair_2_t258602264 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m689648760(__this, method) ((  uint64_t (*) (KeyValuePair_2_t258602264 *, const MethodInfo*))KeyValuePair_2_get_Value_m689648760_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2707677985_gshared (KeyValuePair_2_t258602264 * __this, uint64_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2707677985(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t258602264 *, uint64_t, const MethodInfo*))KeyValuePair_2_set_Value_m2707677985_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2421585715_gshared (KeyValuePair_2_t258602264 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2421585715(__this, method) ((  String_t* (*) (KeyValuePair_2_t258602264 *, const MethodInfo*))KeyValuePair_2_ToString_m2421585715_gshared)(__this, method)
