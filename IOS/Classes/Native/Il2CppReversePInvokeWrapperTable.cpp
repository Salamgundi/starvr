﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture673526704MethodDeclarations.h"

extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[2] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_GvrVideoPlayerTexture_InternalOnVideoEventCallback_m846984960),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_GvrVideoPlayerTexture_InternalOnExceptionCallback_m3417655275),
};
