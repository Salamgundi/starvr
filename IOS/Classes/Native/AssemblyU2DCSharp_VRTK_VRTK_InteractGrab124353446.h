﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// VRTK.ObjectInteractEventHandler
struct ObjectInteractEventHandler_t1701902511;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_InteractTouch
struct VRTK_InteractTouch_t4022091061;
// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;
// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_InteractGrab
struct  VRTK_InteractGrab_t124353446  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Rigidbody VRTK.VRTK_InteractGrab::controllerAttachPoint
	Rigidbody_t4233889191 * ___controllerAttachPoint_2;
	// System.Single VRTK.VRTK_InteractGrab::grabPrecognition
	float ___grabPrecognition_3;
	// System.Single VRTK.VRTK_InteractGrab::throwMultiplier
	float ___throwMultiplier_4;
	// System.Boolean VRTK.VRTK_InteractGrab::createRigidBodyWhenNotTouching
	bool ___createRigidBodyWhenNotTouching_5;
	// VRTK.ObjectInteractEventHandler VRTK.VRTK_InteractGrab::ControllerGrabInteractableObject
	ObjectInteractEventHandler_t1701902511 * ___ControllerGrabInteractableObject_6;
	// VRTK.ObjectInteractEventHandler VRTK.VRTK_InteractGrab::ControllerUngrabInteractableObject
	ObjectInteractEventHandler_t1701902511 * ___ControllerUngrabInteractableObject_7;
	// UnityEngine.GameObject VRTK.VRTK_InteractGrab::grabbedObject
	GameObject_t1756533147 * ___grabbedObject_8;
	// System.Boolean VRTK.VRTK_InteractGrab::influencingGrabbedObject
	bool ___influencingGrabbedObject_9;
	// VRTK.VRTK_InteractTouch VRTK.VRTK_InteractGrab::interactTouch
	VRTK_InteractTouch_t4022091061 * ___interactTouch_10;
	// VRTK.VRTK_ControllerActions VRTK.VRTK_InteractGrab::controllerActions
	VRTK_ControllerActions_t3642353851 * ___controllerActions_11;
	// VRTK.VRTK_ControllerEvents VRTK.VRTK_InteractGrab::controllerEvents
	VRTK_ControllerEvents_t3225224819 * ___controllerEvents_12;
	// System.Int32 VRTK.VRTK_InteractGrab::grabEnabledState
	int32_t ___grabEnabledState_13;
	// System.Single VRTK.VRTK_InteractGrab::grabPrecognitionTimer
	float ___grabPrecognitionTimer_14;
	// UnityEngine.GameObject VRTK.VRTK_InteractGrab::undroppableGrabbedObject
	GameObject_t1756533147 * ___undroppableGrabbedObject_15;

public:
	inline static int32_t get_offset_of_controllerAttachPoint_2() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_t124353446, ___controllerAttachPoint_2)); }
	inline Rigidbody_t4233889191 * get_controllerAttachPoint_2() const { return ___controllerAttachPoint_2; }
	inline Rigidbody_t4233889191 ** get_address_of_controllerAttachPoint_2() { return &___controllerAttachPoint_2; }
	inline void set_controllerAttachPoint_2(Rigidbody_t4233889191 * value)
	{
		___controllerAttachPoint_2 = value;
		Il2CppCodeGenWriteBarrier(&___controllerAttachPoint_2, value);
	}

	inline static int32_t get_offset_of_grabPrecognition_3() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_t124353446, ___grabPrecognition_3)); }
	inline float get_grabPrecognition_3() const { return ___grabPrecognition_3; }
	inline float* get_address_of_grabPrecognition_3() { return &___grabPrecognition_3; }
	inline void set_grabPrecognition_3(float value)
	{
		___grabPrecognition_3 = value;
	}

	inline static int32_t get_offset_of_throwMultiplier_4() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_t124353446, ___throwMultiplier_4)); }
	inline float get_throwMultiplier_4() const { return ___throwMultiplier_4; }
	inline float* get_address_of_throwMultiplier_4() { return &___throwMultiplier_4; }
	inline void set_throwMultiplier_4(float value)
	{
		___throwMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_createRigidBodyWhenNotTouching_5() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_t124353446, ___createRigidBodyWhenNotTouching_5)); }
	inline bool get_createRigidBodyWhenNotTouching_5() const { return ___createRigidBodyWhenNotTouching_5; }
	inline bool* get_address_of_createRigidBodyWhenNotTouching_5() { return &___createRigidBodyWhenNotTouching_5; }
	inline void set_createRigidBodyWhenNotTouching_5(bool value)
	{
		___createRigidBodyWhenNotTouching_5 = value;
	}

	inline static int32_t get_offset_of_ControllerGrabInteractableObject_6() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_t124353446, ___ControllerGrabInteractableObject_6)); }
	inline ObjectInteractEventHandler_t1701902511 * get_ControllerGrabInteractableObject_6() const { return ___ControllerGrabInteractableObject_6; }
	inline ObjectInteractEventHandler_t1701902511 ** get_address_of_ControllerGrabInteractableObject_6() { return &___ControllerGrabInteractableObject_6; }
	inline void set_ControllerGrabInteractableObject_6(ObjectInteractEventHandler_t1701902511 * value)
	{
		___ControllerGrabInteractableObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerGrabInteractableObject_6, value);
	}

	inline static int32_t get_offset_of_ControllerUngrabInteractableObject_7() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_t124353446, ___ControllerUngrabInteractableObject_7)); }
	inline ObjectInteractEventHandler_t1701902511 * get_ControllerUngrabInteractableObject_7() const { return ___ControllerUngrabInteractableObject_7; }
	inline ObjectInteractEventHandler_t1701902511 ** get_address_of_ControllerUngrabInteractableObject_7() { return &___ControllerUngrabInteractableObject_7; }
	inline void set_ControllerUngrabInteractableObject_7(ObjectInteractEventHandler_t1701902511 * value)
	{
		___ControllerUngrabInteractableObject_7 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerUngrabInteractableObject_7, value);
	}

	inline static int32_t get_offset_of_grabbedObject_8() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_t124353446, ___grabbedObject_8)); }
	inline GameObject_t1756533147 * get_grabbedObject_8() const { return ___grabbedObject_8; }
	inline GameObject_t1756533147 ** get_address_of_grabbedObject_8() { return &___grabbedObject_8; }
	inline void set_grabbedObject_8(GameObject_t1756533147 * value)
	{
		___grabbedObject_8 = value;
		Il2CppCodeGenWriteBarrier(&___grabbedObject_8, value);
	}

	inline static int32_t get_offset_of_influencingGrabbedObject_9() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_t124353446, ___influencingGrabbedObject_9)); }
	inline bool get_influencingGrabbedObject_9() const { return ___influencingGrabbedObject_9; }
	inline bool* get_address_of_influencingGrabbedObject_9() { return &___influencingGrabbedObject_9; }
	inline void set_influencingGrabbedObject_9(bool value)
	{
		___influencingGrabbedObject_9 = value;
	}

	inline static int32_t get_offset_of_interactTouch_10() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_t124353446, ___interactTouch_10)); }
	inline VRTK_InteractTouch_t4022091061 * get_interactTouch_10() const { return ___interactTouch_10; }
	inline VRTK_InteractTouch_t4022091061 ** get_address_of_interactTouch_10() { return &___interactTouch_10; }
	inline void set_interactTouch_10(VRTK_InteractTouch_t4022091061 * value)
	{
		___interactTouch_10 = value;
		Il2CppCodeGenWriteBarrier(&___interactTouch_10, value);
	}

	inline static int32_t get_offset_of_controllerActions_11() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_t124353446, ___controllerActions_11)); }
	inline VRTK_ControllerActions_t3642353851 * get_controllerActions_11() const { return ___controllerActions_11; }
	inline VRTK_ControllerActions_t3642353851 ** get_address_of_controllerActions_11() { return &___controllerActions_11; }
	inline void set_controllerActions_11(VRTK_ControllerActions_t3642353851 * value)
	{
		___controllerActions_11 = value;
		Il2CppCodeGenWriteBarrier(&___controllerActions_11, value);
	}

	inline static int32_t get_offset_of_controllerEvents_12() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_t124353446, ___controllerEvents_12)); }
	inline VRTK_ControllerEvents_t3225224819 * get_controllerEvents_12() const { return ___controllerEvents_12; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_controllerEvents_12() { return &___controllerEvents_12; }
	inline void set_controllerEvents_12(VRTK_ControllerEvents_t3225224819 * value)
	{
		___controllerEvents_12 = value;
		Il2CppCodeGenWriteBarrier(&___controllerEvents_12, value);
	}

	inline static int32_t get_offset_of_grabEnabledState_13() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_t124353446, ___grabEnabledState_13)); }
	inline int32_t get_grabEnabledState_13() const { return ___grabEnabledState_13; }
	inline int32_t* get_address_of_grabEnabledState_13() { return &___grabEnabledState_13; }
	inline void set_grabEnabledState_13(int32_t value)
	{
		___grabEnabledState_13 = value;
	}

	inline static int32_t get_offset_of_grabPrecognitionTimer_14() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_t124353446, ___grabPrecognitionTimer_14)); }
	inline float get_grabPrecognitionTimer_14() const { return ___grabPrecognitionTimer_14; }
	inline float* get_address_of_grabPrecognitionTimer_14() { return &___grabPrecognitionTimer_14; }
	inline void set_grabPrecognitionTimer_14(float value)
	{
		___grabPrecognitionTimer_14 = value;
	}

	inline static int32_t get_offset_of_undroppableGrabbedObject_15() { return static_cast<int32_t>(offsetof(VRTK_InteractGrab_t124353446, ___undroppableGrabbedObject_15)); }
	inline GameObject_t1756533147 * get_undroppableGrabbedObject_15() const { return ___undroppableGrabbedObject_15; }
	inline GameObject_t1756533147 ** get_address_of_undroppableGrabbedObject_15() { return &___undroppableGrabbedObject_15; }
	inline void set_undroppableGrabbedObject_15(GameObject_t1756533147 * value)
	{
		___undroppableGrabbedObject_15 = value;
		Il2CppCodeGenWriteBarrier(&___undroppableGrabbedObject_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
