﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_SimHeadset
struct SDK_SimHeadset_t4149936844;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_SimHeadset::.ctor()
extern "C"  void SDK_SimHeadset__ctor_m2489299646 (SDK_SimHeadset_t4149936844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
