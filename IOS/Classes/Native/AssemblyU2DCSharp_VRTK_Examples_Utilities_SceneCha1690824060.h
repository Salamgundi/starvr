﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Utilities.SceneChanger
struct  SceneChanger_t1690824060  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.Examples.Utilities.SceneChanger::canPress
	bool ___canPress_2;
	// System.UInt32 VRTK.Examples.Utilities.SceneChanger::controllerIndex
	uint32_t ___controllerIndex_3;

public:
	inline static int32_t get_offset_of_canPress_2() { return static_cast<int32_t>(offsetof(SceneChanger_t1690824060, ___canPress_2)); }
	inline bool get_canPress_2() const { return ___canPress_2; }
	inline bool* get_address_of_canPress_2() { return &___canPress_2; }
	inline void set_canPress_2(bool value)
	{
		___canPress_2 = value;
	}

	inline static int32_t get_offset_of_controllerIndex_3() { return static_cast<int32_t>(offsetof(SceneChanger_t1690824060, ___controllerIndex_3)); }
	inline uint32_t get_controllerIndex_3() const { return ___controllerIndex_3; }
	inline uint32_t* get_address_of_controllerIndex_3() { return &___controllerIndex_3; }
	inline void set_controllerIndex_3(uint32_t value)
	{
		___controllerIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
