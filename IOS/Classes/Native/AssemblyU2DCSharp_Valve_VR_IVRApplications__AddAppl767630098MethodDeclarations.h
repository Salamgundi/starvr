﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_AddApplicationManifest
struct _AddApplicationManifest_t767630098;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_AddApplicationManifest::.ctor(System.Object,System.IntPtr)
extern "C"  void _AddApplicationManifest__ctor_m3448070071 (_AddApplicationManifest_t767630098 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_AddApplicationManifest::Invoke(System.String,System.Boolean)
extern "C"  int32_t _AddApplicationManifest_Invoke_m563100176 (_AddApplicationManifest_t767630098 * __this, String_t* ___pchApplicationManifestFullPath0, bool ___bTemporary1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_AddApplicationManifest::BeginInvoke(System.String,System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _AddApplicationManifest_BeginInvoke_m3164746009 (_AddApplicationManifest_t767630098 * __this, String_t* ___pchApplicationManifestFullPath0, bool ___bTemporary1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_AddApplicationManifest::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _AddApplicationManifest_EndInvoke_m965498529 (_AddApplicationManifest_t767630098 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
