﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.PanelMenu.PanelMenuDemoFlyingSaucer
struct  PanelMenuDemoFlyingSaucer_t3577797216  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color[] VRTK.Examples.PanelMenu.PanelMenuDemoFlyingSaucer::colors
	ColorU5BU5D_t672350442* ___colors_2;

public:
	inline static int32_t get_offset_of_colors_2() { return static_cast<int32_t>(offsetof(PanelMenuDemoFlyingSaucer_t3577797216, ___colors_2)); }
	inline ColorU5BU5D_t672350442* get_colors_2() const { return ___colors_2; }
	inline ColorU5BU5D_t672350442** get_address_of_colors_2() { return &___colors_2; }
	inline void set_colors_2(ColorU5BU5D_t672350442* value)
	{
		___colors_2 = value;
		Il2CppCodeGenWriteBarrier(&___colors_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
