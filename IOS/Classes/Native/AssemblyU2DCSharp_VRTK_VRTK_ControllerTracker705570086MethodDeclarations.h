﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ControllerTracker
struct VRTK_ControllerTracker_t705570086;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_ControllerTracker::.ctor()
extern "C"  void VRTK_ControllerTracker__ctor_m733568538 (VRTK_ControllerTracker_t705570086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerTracker::OnEnable()
extern "C"  void VRTK_ControllerTracker_OnEnable_m2191374390 (VRTK_ControllerTracker_t705570086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerTracker::Update()
extern "C"  void VRTK_ControllerTracker_Update_m3827546639 (VRTK_ControllerTracker_t705570086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
