﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Action`3<System.Object,System.Object,System.Object>
struct Action_3_t2223553130;
// SteamVR_Events/Event`3<System.Object,System.Object,System.Object>
struct Event_3_t1695830490;
// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct UnityAction_3_t3482433968;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Events/Action`3<System.Object,System.Object,System.Object>::.ctor(SteamVR_Events/Event`3<T0,T1,T2>,UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void Action_3__ctor_m363946389_gshared (Action_3_t2223553130 * __this, Event_3_t1695830490 * ____event0, UnityAction_3_t3482433968 * ___action1, const MethodInfo* method);
#define Action_3__ctor_m363946389(__this, ____event0, ___action1, method) ((  void (*) (Action_3_t2223553130 *, Event_3_t1695830490 *, UnityAction_3_t3482433968 *, const MethodInfo*))Action_3__ctor_m363946389_gshared)(__this, ____event0, ___action1, method)
// System.Void SteamVR_Events/Action`3<System.Object,System.Object,System.Object>::Enable(System.Boolean)
extern "C"  void Action_3_Enable_m3822748381_gshared (Action_3_t2223553130 * __this, bool ___enabled0, const MethodInfo* method);
#define Action_3_Enable_m3822748381(__this, ___enabled0, method) ((  void (*) (Action_3_t2223553130 *, bool, const MethodInfo*))Action_3_Enable_m3822748381_gshared)(__this, ___enabled0, method)
