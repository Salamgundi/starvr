﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reset
struct Reset_t866863539;
// SteamVR_Controller/Device
struct Device_t2885069456;

#include "codegen/il2cpp-codegen.h"

// System.Void Reset::.ctor()
extern "C"  void Reset__ctor_m2481109506 (Reset_t866863539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Controller/Device Reset::get_Controller()
extern "C"  Device_t2885069456 * Reset_get_Controller_m2841466826 (Reset_t866863539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reset::Awake()
extern "C"  void Reset_Awake_m883161443 (Reset_t866863539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reset::Update()
extern "C"  void Reset_Update_m2066158135 (Reset_t866863539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
