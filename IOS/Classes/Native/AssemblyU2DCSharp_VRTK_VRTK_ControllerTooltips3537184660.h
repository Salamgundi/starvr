﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.VRTK_ControllerTooltips/TooltipButtons[]
struct TooltipButtonsU5BU5D_t4290115087;
// VRTK.VRTK_ObjectTooltip[]
struct VRTK_ObjectTooltipU5BU5D_t560630551;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;
// VRTK.VRTK_HeadsetControllerAware
struct VRTK_HeadsetControllerAware_t1678000416;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ControllerTooltips
struct  VRTK_ControllerTooltips_t3537184660  : public MonoBehaviour_t1158329972
{
public:
	// System.String VRTK.VRTK_ControllerTooltips::triggerText
	String_t* ___triggerText_2;
	// System.String VRTK.VRTK_ControllerTooltips::gripText
	String_t* ___gripText_3;
	// System.String VRTK.VRTK_ControllerTooltips::touchpadText
	String_t* ___touchpadText_4;
	// System.String VRTK.VRTK_ControllerTooltips::buttonOneText
	String_t* ___buttonOneText_5;
	// System.String VRTK.VRTK_ControllerTooltips::buttonTwoText
	String_t* ___buttonTwoText_6;
	// System.String VRTK.VRTK_ControllerTooltips::startMenuText
	String_t* ___startMenuText_7;
	// UnityEngine.Color VRTK.VRTK_ControllerTooltips::tipBackgroundColor
	Color_t2020392075  ___tipBackgroundColor_8;
	// UnityEngine.Color VRTK.VRTK_ControllerTooltips::tipTextColor
	Color_t2020392075  ___tipTextColor_9;
	// UnityEngine.Color VRTK.VRTK_ControllerTooltips::tipLineColor
	Color_t2020392075  ___tipLineColor_10;
	// UnityEngine.Transform VRTK.VRTK_ControllerTooltips::trigger
	Transform_t3275118058 * ___trigger_11;
	// UnityEngine.Transform VRTK.VRTK_ControllerTooltips::grip
	Transform_t3275118058 * ___grip_12;
	// UnityEngine.Transform VRTK.VRTK_ControllerTooltips::touchpad
	Transform_t3275118058 * ___touchpad_13;
	// UnityEngine.Transform VRTK.VRTK_ControllerTooltips::buttonOne
	Transform_t3275118058 * ___buttonOne_14;
	// UnityEngine.Transform VRTK.VRTK_ControllerTooltips::buttonTwo
	Transform_t3275118058 * ___buttonTwo_15;
	// UnityEngine.Transform VRTK.VRTK_ControllerTooltips::startMenu
	Transform_t3275118058 * ___startMenu_16;
	// System.Boolean VRTK.VRTK_ControllerTooltips::triggerInitialised
	bool ___triggerInitialised_17;
	// System.Boolean VRTK.VRTK_ControllerTooltips::gripInitialised
	bool ___gripInitialised_18;
	// System.Boolean VRTK.VRTK_ControllerTooltips::touchpadInitialised
	bool ___touchpadInitialised_19;
	// System.Boolean VRTK.VRTK_ControllerTooltips::buttonOneInitialised
	bool ___buttonOneInitialised_20;
	// System.Boolean VRTK.VRTK_ControllerTooltips::buttonTwoInitialised
	bool ___buttonTwoInitialised_21;
	// System.Boolean VRTK.VRTK_ControllerTooltips::startMenuInitialised
	bool ___startMenuInitialised_22;
	// VRTK.VRTK_ControllerTooltips/TooltipButtons[] VRTK.VRTK_ControllerTooltips::availableButtons
	TooltipButtonsU5BU5D_t4290115087* ___availableButtons_23;
	// VRTK.VRTK_ObjectTooltip[] VRTK.VRTK_ControllerTooltips::buttonTooltips
	VRTK_ObjectTooltipU5BU5D_t560630551* ___buttonTooltips_24;
	// System.Boolean[] VRTK.VRTK_ControllerTooltips::tooltipStates
	BooleanU5BU5D_t3568034315* ___tooltipStates_25;
	// VRTK.VRTK_ControllerActions VRTK.VRTK_ControllerTooltips::controllerActions
	VRTK_ControllerActions_t3642353851 * ___controllerActions_26;
	// VRTK.VRTK_HeadsetControllerAware VRTK.VRTK_ControllerTooltips::headsetControllerAware
	VRTK_HeadsetControllerAware_t1678000416 * ___headsetControllerAware_27;

public:
	inline static int32_t get_offset_of_triggerText_2() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___triggerText_2)); }
	inline String_t* get_triggerText_2() const { return ___triggerText_2; }
	inline String_t** get_address_of_triggerText_2() { return &___triggerText_2; }
	inline void set_triggerText_2(String_t* value)
	{
		___triggerText_2 = value;
		Il2CppCodeGenWriteBarrier(&___triggerText_2, value);
	}

	inline static int32_t get_offset_of_gripText_3() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___gripText_3)); }
	inline String_t* get_gripText_3() const { return ___gripText_3; }
	inline String_t** get_address_of_gripText_3() { return &___gripText_3; }
	inline void set_gripText_3(String_t* value)
	{
		___gripText_3 = value;
		Il2CppCodeGenWriteBarrier(&___gripText_3, value);
	}

	inline static int32_t get_offset_of_touchpadText_4() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___touchpadText_4)); }
	inline String_t* get_touchpadText_4() const { return ___touchpadText_4; }
	inline String_t** get_address_of_touchpadText_4() { return &___touchpadText_4; }
	inline void set_touchpadText_4(String_t* value)
	{
		___touchpadText_4 = value;
		Il2CppCodeGenWriteBarrier(&___touchpadText_4, value);
	}

	inline static int32_t get_offset_of_buttonOneText_5() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___buttonOneText_5)); }
	inline String_t* get_buttonOneText_5() const { return ___buttonOneText_5; }
	inline String_t** get_address_of_buttonOneText_5() { return &___buttonOneText_5; }
	inline void set_buttonOneText_5(String_t* value)
	{
		___buttonOneText_5 = value;
		Il2CppCodeGenWriteBarrier(&___buttonOneText_5, value);
	}

	inline static int32_t get_offset_of_buttonTwoText_6() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___buttonTwoText_6)); }
	inline String_t* get_buttonTwoText_6() const { return ___buttonTwoText_6; }
	inline String_t** get_address_of_buttonTwoText_6() { return &___buttonTwoText_6; }
	inline void set_buttonTwoText_6(String_t* value)
	{
		___buttonTwoText_6 = value;
		Il2CppCodeGenWriteBarrier(&___buttonTwoText_6, value);
	}

	inline static int32_t get_offset_of_startMenuText_7() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___startMenuText_7)); }
	inline String_t* get_startMenuText_7() const { return ___startMenuText_7; }
	inline String_t** get_address_of_startMenuText_7() { return &___startMenuText_7; }
	inline void set_startMenuText_7(String_t* value)
	{
		___startMenuText_7 = value;
		Il2CppCodeGenWriteBarrier(&___startMenuText_7, value);
	}

	inline static int32_t get_offset_of_tipBackgroundColor_8() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___tipBackgroundColor_8)); }
	inline Color_t2020392075  get_tipBackgroundColor_8() const { return ___tipBackgroundColor_8; }
	inline Color_t2020392075 * get_address_of_tipBackgroundColor_8() { return &___tipBackgroundColor_8; }
	inline void set_tipBackgroundColor_8(Color_t2020392075  value)
	{
		___tipBackgroundColor_8 = value;
	}

	inline static int32_t get_offset_of_tipTextColor_9() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___tipTextColor_9)); }
	inline Color_t2020392075  get_tipTextColor_9() const { return ___tipTextColor_9; }
	inline Color_t2020392075 * get_address_of_tipTextColor_9() { return &___tipTextColor_9; }
	inline void set_tipTextColor_9(Color_t2020392075  value)
	{
		___tipTextColor_9 = value;
	}

	inline static int32_t get_offset_of_tipLineColor_10() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___tipLineColor_10)); }
	inline Color_t2020392075  get_tipLineColor_10() const { return ___tipLineColor_10; }
	inline Color_t2020392075 * get_address_of_tipLineColor_10() { return &___tipLineColor_10; }
	inline void set_tipLineColor_10(Color_t2020392075  value)
	{
		___tipLineColor_10 = value;
	}

	inline static int32_t get_offset_of_trigger_11() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___trigger_11)); }
	inline Transform_t3275118058 * get_trigger_11() const { return ___trigger_11; }
	inline Transform_t3275118058 ** get_address_of_trigger_11() { return &___trigger_11; }
	inline void set_trigger_11(Transform_t3275118058 * value)
	{
		___trigger_11 = value;
		Il2CppCodeGenWriteBarrier(&___trigger_11, value);
	}

	inline static int32_t get_offset_of_grip_12() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___grip_12)); }
	inline Transform_t3275118058 * get_grip_12() const { return ___grip_12; }
	inline Transform_t3275118058 ** get_address_of_grip_12() { return &___grip_12; }
	inline void set_grip_12(Transform_t3275118058 * value)
	{
		___grip_12 = value;
		Il2CppCodeGenWriteBarrier(&___grip_12, value);
	}

	inline static int32_t get_offset_of_touchpad_13() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___touchpad_13)); }
	inline Transform_t3275118058 * get_touchpad_13() const { return ___touchpad_13; }
	inline Transform_t3275118058 ** get_address_of_touchpad_13() { return &___touchpad_13; }
	inline void set_touchpad_13(Transform_t3275118058 * value)
	{
		___touchpad_13 = value;
		Il2CppCodeGenWriteBarrier(&___touchpad_13, value);
	}

	inline static int32_t get_offset_of_buttonOne_14() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___buttonOne_14)); }
	inline Transform_t3275118058 * get_buttonOne_14() const { return ___buttonOne_14; }
	inline Transform_t3275118058 ** get_address_of_buttonOne_14() { return &___buttonOne_14; }
	inline void set_buttonOne_14(Transform_t3275118058 * value)
	{
		___buttonOne_14 = value;
		Il2CppCodeGenWriteBarrier(&___buttonOne_14, value);
	}

	inline static int32_t get_offset_of_buttonTwo_15() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___buttonTwo_15)); }
	inline Transform_t3275118058 * get_buttonTwo_15() const { return ___buttonTwo_15; }
	inline Transform_t3275118058 ** get_address_of_buttonTwo_15() { return &___buttonTwo_15; }
	inline void set_buttonTwo_15(Transform_t3275118058 * value)
	{
		___buttonTwo_15 = value;
		Il2CppCodeGenWriteBarrier(&___buttonTwo_15, value);
	}

	inline static int32_t get_offset_of_startMenu_16() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___startMenu_16)); }
	inline Transform_t3275118058 * get_startMenu_16() const { return ___startMenu_16; }
	inline Transform_t3275118058 ** get_address_of_startMenu_16() { return &___startMenu_16; }
	inline void set_startMenu_16(Transform_t3275118058 * value)
	{
		___startMenu_16 = value;
		Il2CppCodeGenWriteBarrier(&___startMenu_16, value);
	}

	inline static int32_t get_offset_of_triggerInitialised_17() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___triggerInitialised_17)); }
	inline bool get_triggerInitialised_17() const { return ___triggerInitialised_17; }
	inline bool* get_address_of_triggerInitialised_17() { return &___triggerInitialised_17; }
	inline void set_triggerInitialised_17(bool value)
	{
		___triggerInitialised_17 = value;
	}

	inline static int32_t get_offset_of_gripInitialised_18() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___gripInitialised_18)); }
	inline bool get_gripInitialised_18() const { return ___gripInitialised_18; }
	inline bool* get_address_of_gripInitialised_18() { return &___gripInitialised_18; }
	inline void set_gripInitialised_18(bool value)
	{
		___gripInitialised_18 = value;
	}

	inline static int32_t get_offset_of_touchpadInitialised_19() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___touchpadInitialised_19)); }
	inline bool get_touchpadInitialised_19() const { return ___touchpadInitialised_19; }
	inline bool* get_address_of_touchpadInitialised_19() { return &___touchpadInitialised_19; }
	inline void set_touchpadInitialised_19(bool value)
	{
		___touchpadInitialised_19 = value;
	}

	inline static int32_t get_offset_of_buttonOneInitialised_20() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___buttonOneInitialised_20)); }
	inline bool get_buttonOneInitialised_20() const { return ___buttonOneInitialised_20; }
	inline bool* get_address_of_buttonOneInitialised_20() { return &___buttonOneInitialised_20; }
	inline void set_buttonOneInitialised_20(bool value)
	{
		___buttonOneInitialised_20 = value;
	}

	inline static int32_t get_offset_of_buttonTwoInitialised_21() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___buttonTwoInitialised_21)); }
	inline bool get_buttonTwoInitialised_21() const { return ___buttonTwoInitialised_21; }
	inline bool* get_address_of_buttonTwoInitialised_21() { return &___buttonTwoInitialised_21; }
	inline void set_buttonTwoInitialised_21(bool value)
	{
		___buttonTwoInitialised_21 = value;
	}

	inline static int32_t get_offset_of_startMenuInitialised_22() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___startMenuInitialised_22)); }
	inline bool get_startMenuInitialised_22() const { return ___startMenuInitialised_22; }
	inline bool* get_address_of_startMenuInitialised_22() { return &___startMenuInitialised_22; }
	inline void set_startMenuInitialised_22(bool value)
	{
		___startMenuInitialised_22 = value;
	}

	inline static int32_t get_offset_of_availableButtons_23() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___availableButtons_23)); }
	inline TooltipButtonsU5BU5D_t4290115087* get_availableButtons_23() const { return ___availableButtons_23; }
	inline TooltipButtonsU5BU5D_t4290115087** get_address_of_availableButtons_23() { return &___availableButtons_23; }
	inline void set_availableButtons_23(TooltipButtonsU5BU5D_t4290115087* value)
	{
		___availableButtons_23 = value;
		Il2CppCodeGenWriteBarrier(&___availableButtons_23, value);
	}

	inline static int32_t get_offset_of_buttonTooltips_24() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___buttonTooltips_24)); }
	inline VRTK_ObjectTooltipU5BU5D_t560630551* get_buttonTooltips_24() const { return ___buttonTooltips_24; }
	inline VRTK_ObjectTooltipU5BU5D_t560630551** get_address_of_buttonTooltips_24() { return &___buttonTooltips_24; }
	inline void set_buttonTooltips_24(VRTK_ObjectTooltipU5BU5D_t560630551* value)
	{
		___buttonTooltips_24 = value;
		Il2CppCodeGenWriteBarrier(&___buttonTooltips_24, value);
	}

	inline static int32_t get_offset_of_tooltipStates_25() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___tooltipStates_25)); }
	inline BooleanU5BU5D_t3568034315* get_tooltipStates_25() const { return ___tooltipStates_25; }
	inline BooleanU5BU5D_t3568034315** get_address_of_tooltipStates_25() { return &___tooltipStates_25; }
	inline void set_tooltipStates_25(BooleanU5BU5D_t3568034315* value)
	{
		___tooltipStates_25 = value;
		Il2CppCodeGenWriteBarrier(&___tooltipStates_25, value);
	}

	inline static int32_t get_offset_of_controllerActions_26() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___controllerActions_26)); }
	inline VRTK_ControllerActions_t3642353851 * get_controllerActions_26() const { return ___controllerActions_26; }
	inline VRTK_ControllerActions_t3642353851 ** get_address_of_controllerActions_26() { return &___controllerActions_26; }
	inline void set_controllerActions_26(VRTK_ControllerActions_t3642353851 * value)
	{
		___controllerActions_26 = value;
		Il2CppCodeGenWriteBarrier(&___controllerActions_26, value);
	}

	inline static int32_t get_offset_of_headsetControllerAware_27() { return static_cast<int32_t>(offsetof(VRTK_ControllerTooltips_t3537184660, ___headsetControllerAware_27)); }
	inline VRTK_HeadsetControllerAware_t1678000416 * get_headsetControllerAware_27() const { return ___headsetControllerAware_27; }
	inline VRTK_HeadsetControllerAware_t1678000416 ** get_address_of_headsetControllerAware_27() { return &___headsetControllerAware_27; }
	inline void set_headsetControllerAware_27(VRTK_HeadsetControllerAware_t1678000416 * value)
	{
		___headsetControllerAware_27 = value;
		Il2CppCodeGenWriteBarrier(&___headsetControllerAware_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
