﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UICircle
struct UICircle_t3209742644;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t3048644023;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.Mesh
struct Mesh_t1356156583;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"

// System.Void VRTK.UICircle::.ctor()
extern "C"  void UICircle__ctor_m4239750178 (UICircle_t3209742644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture VRTK.UICircle::get_mainTexture()
extern "C"  Texture_t2243626319 * UICircle_get_mainTexture_m1091190362 (UICircle_t3209742644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture VRTK.UICircle::get_texture()
extern "C"  Texture_t2243626319 * UICircle_get_texture_m2659479277 (UICircle_t3209742644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UICircle::set_texture(UnityEngine.Texture)
extern "C"  void UICircle_set_texture_m2652881562 (UICircle_t3209742644 * __this, Texture_t2243626319 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UIVertex[] VRTK.UICircle::SetVbo(UnityEngine.Vector2[],UnityEngine.Vector2[])
extern "C"  UIVertexU5BU5D_t3048644023* UICircle_SetVbo_m2971207539 (UICircle_t3209742644 * __this, Vector2U5BU5D_t686124026* ___vertices0, Vector2U5BU5D_t686124026* ___uvs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UICircle::OnPopulateMesh(UnityEngine.Mesh)
extern "C"  void UICircle_OnPopulateMesh_m865074824 (UICircle_t3209742644 * __this, Mesh_t1356156583 * ___toFill0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UICircle::Update()
extern "C"  void UICircle_Update_m1668737317 (UICircle_t3209742644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
