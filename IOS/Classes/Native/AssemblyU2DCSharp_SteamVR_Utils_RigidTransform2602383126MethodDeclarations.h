﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Transform
struct Transform_t3275118058;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SteamVR_Utils_RigidTransform2602383126.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix44_t664273159.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void SteamVR_Utils/RigidTransform::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void RigidTransform__ctor_m2467621573 (RigidTransform_t2602383126 * __this, Vector3_t2243707580  ___pos0, Quaternion_t4030073918  ___rot1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Utils/RigidTransform::.ctor(UnityEngine.Transform)
extern "C"  void RigidTransform__ctor_m2076224422 (RigidTransform_t2602383126 * __this, Transform_t3275118058 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Utils/RigidTransform::.ctor(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  void RigidTransform__ctor_m2494024479 (RigidTransform_t2602383126 * __this, Transform_t3275118058 * ___from0, Transform_t3275118058 * ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Utils/RigidTransform::.ctor(Valve.VR.HmdMatrix34_t)
extern "C"  void RigidTransform__ctor_m1440021001 (RigidTransform_t2602383126 * __this, HmdMatrix34_t_t664273062  ___pose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Utils/RigidTransform::.ctor(Valve.VR.HmdMatrix44_t)
extern "C"  void RigidTransform__ctor_m1440017930 (RigidTransform_t2602383126 * __this, HmdMatrix44_t_t664273159  ___pose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Utils/RigidTransform SteamVR_Utils/RigidTransform::get_identity()
extern "C"  RigidTransform_t2602383126  RigidTransform_get_identity_m4243295783 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Utils/RigidTransform SteamVR_Utils/RigidTransform::FromLocal(UnityEngine.Transform)
extern "C"  RigidTransform_t2602383126  RigidTransform_FromLocal_m3888206310 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix44_t SteamVR_Utils/RigidTransform::ToHmdMatrix44()
extern "C"  HmdMatrix44_t_t664273159  RigidTransform_ToHmdMatrix44_m1768006482 (RigidTransform_t2602383126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix34_t SteamVR_Utils/RigidTransform::ToHmdMatrix34()
extern "C"  HmdMatrix34_t_t664273062  RigidTransform_ToHmdMatrix34_m3851857522 (RigidTransform_t2602383126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Utils/RigidTransform::Equals(System.Object)
extern "C"  bool RigidTransform_Equals_m741082682 (RigidTransform_t2602383126 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SteamVR_Utils/RigidTransform::GetHashCode()
extern "C"  int32_t RigidTransform_GetHashCode_m2265241566 (RigidTransform_t2602383126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Utils/RigidTransform::op_Equality(SteamVR_Utils/RigidTransform,SteamVR_Utils/RigidTransform)
extern "C"  bool RigidTransform_op_Equality_m3306526099 (Il2CppObject * __this /* static, unused */, RigidTransform_t2602383126  ___a0, RigidTransform_t2602383126  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Utils/RigidTransform::op_Inequality(SteamVR_Utils/RigidTransform,SteamVR_Utils/RigidTransform)
extern "C"  bool RigidTransform_op_Inequality_m149998286 (Il2CppObject * __this /* static, unused */, RigidTransform_t2602383126  ___a0, RigidTransform_t2602383126  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Utils/RigidTransform SteamVR_Utils/RigidTransform::op_Multiply(SteamVR_Utils/RigidTransform,SteamVR_Utils/RigidTransform)
extern "C"  RigidTransform_t2602383126  RigidTransform_op_Multiply_m590842408 (Il2CppObject * __this /* static, unused */, RigidTransform_t2602383126  ___a0, RigidTransform_t2602383126  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Utils/RigidTransform::Inverse()
extern "C"  void RigidTransform_Inverse_m2547541379 (RigidTransform_t2602383126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Utils/RigidTransform SteamVR_Utils/RigidTransform::GetInverse()
extern "C"  RigidTransform_t2602383126  RigidTransform_GetInverse_m3726610136 (RigidTransform_t2602383126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Utils/RigidTransform::Multiply(SteamVR_Utils/RigidTransform,SteamVR_Utils/RigidTransform)
extern "C"  void RigidTransform_Multiply_m4074058907 (RigidTransform_t2602383126 * __this, RigidTransform_t2602383126  ___a0, RigidTransform_t2602383126  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SteamVR_Utils/RigidTransform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  RigidTransform_InverseTransformPoint_m3908392210 (RigidTransform_t2602383126 * __this, Vector3_t2243707580  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SteamVR_Utils/RigidTransform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  RigidTransform_TransformPoint_m3205357202 (RigidTransform_t2602383126 * __this, Vector3_t2243707580  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SteamVR_Utils/RigidTransform::op_Multiply(SteamVR_Utils/RigidTransform,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  RigidTransform_op_Multiply_m2473661886 (Il2CppObject * __this /* static, unused */, RigidTransform_t2602383126  ___t0, Vector3_t2243707580  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Utils/RigidTransform SteamVR_Utils/RigidTransform::Interpolate(SteamVR_Utils/RigidTransform,SteamVR_Utils/RigidTransform,System.Single)
extern "C"  RigidTransform_t2602383126  RigidTransform_Interpolate_m3433561418 (Il2CppObject * __this /* static, unused */, RigidTransform_t2602383126  ___a0, RigidTransform_t2602383126  ___b1, float ___t2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Utils/RigidTransform::Interpolate(SteamVR_Utils/RigidTransform,System.Single)
extern "C"  void RigidTransform_Interpolate_m444255429 (RigidTransform_t2602383126 * __this, RigidTransform_t2602383126  ___to0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
