﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Menu
struct  Menu_t4261767481  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Menu::Menu1
	GameObject_t1756533147 * ___Menu1_2;
	// System.Boolean Menu::MenuOn
	bool ___MenuOn_3;

public:
	inline static int32_t get_offset_of_Menu1_2() { return static_cast<int32_t>(offsetof(Menu_t4261767481, ___Menu1_2)); }
	inline GameObject_t1756533147 * get_Menu1_2() const { return ___Menu1_2; }
	inline GameObject_t1756533147 ** get_address_of_Menu1_2() { return &___Menu1_2; }
	inline void set_Menu1_2(GameObject_t1756533147 * value)
	{
		___Menu1_2 = value;
		Il2CppCodeGenWriteBarrier(&___Menu1_2, value);
	}

	inline static int32_t get_offset_of_MenuOn_3() { return static_cast<int32_t>(offsetof(Menu_t4261767481, ___MenuOn_3)); }
	inline bool get_MenuOn_3() const { return ___MenuOn_3; }
	inline bool* get_address_of_MenuOn_3() { return &___MenuOn_3; }
	inline void set_MenuOn_3(bool value)
	{
		___MenuOn_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
