﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.SpawnAndAttachAfterControllerIsTracking
struct SpawnAndAttachAfterControllerIsTracking_t115944288;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.SpawnAndAttachAfterControllerIsTracking::.ctor()
extern "C"  void SpawnAndAttachAfterControllerIsTracking__ctor_m642134124 (SpawnAndAttachAfterControllerIsTracking_t115944288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SpawnAndAttachAfterControllerIsTracking::Start()
extern "C"  void SpawnAndAttachAfterControllerIsTracking_Start_m2878249140 (SpawnAndAttachAfterControllerIsTracking_t115944288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SpawnAndAttachAfterControllerIsTracking::Update()
extern "C"  void SpawnAndAttachAfterControllerIsTracking_Update_m1521989277 (SpawnAndAttachAfterControllerIsTracking_t115944288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
