﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRTrackedCamera/_AcquireVideoStreamingService
struct _AcquireVideoStreamingService_t1559262216;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraError3529690400.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRTrackedCamera/_AcquireVideoStreamingService::.ctor(System.Object,System.IntPtr)
extern "C"  void _AcquireVideoStreamingService__ctor_m2541041653 (_AcquireVideoStreamingService_t1559262216 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_AcquireVideoStreamingService::Invoke(System.UInt32,System.UInt64&)
extern "C"  int32_t _AcquireVideoStreamingService_Invoke_m328571545 (_AcquireVideoStreamingService_t1559262216 * __this, uint32_t ___nDeviceIndex0, uint64_t* ___pHandle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRTrackedCamera/_AcquireVideoStreamingService::BeginInvoke(System.UInt32,System.UInt64&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _AcquireVideoStreamingService_BeginInvoke_m1961607711 (_AcquireVideoStreamingService_t1559262216 * __this, uint32_t ___nDeviceIndex0, uint64_t* ___pHandle1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_AcquireVideoStreamingService::EndInvoke(System.UInt64&,System.IAsyncResult)
extern "C"  int32_t _AcquireVideoStreamingService_EndInvoke_m3075144523 (_AcquireVideoStreamingService_t1559262216 * __this, uint64_t* ___pHandle0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
