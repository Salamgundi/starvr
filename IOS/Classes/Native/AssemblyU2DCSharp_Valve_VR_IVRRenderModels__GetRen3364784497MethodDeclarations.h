﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_GetRenderModelCount
struct _GetRenderModelCount_t3364784497;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_GetRenderModelCount::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetRenderModelCount__ctor_m3843656478 (_GetRenderModelCount_t3364784497 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelCount::Invoke()
extern "C"  uint32_t _GetRenderModelCount_Invoke_m2026972611 (_GetRenderModelCount_t3364784497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetRenderModelCount::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetRenderModelCount_BeginInvoke_m2693889567 (_GetRenderModelCount_t3364784497 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelCount::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetRenderModelCount_EndInvoke_m2333466433 (_GetRenderModelCount_t3364784497 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
