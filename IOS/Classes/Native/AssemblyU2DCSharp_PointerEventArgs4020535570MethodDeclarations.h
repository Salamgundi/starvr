﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PointerEventArgs
struct PointerEventArgs_t4020535570;
struct PointerEventArgs_t4020535570_marshaled_pinvoke;
struct PointerEventArgs_t4020535570_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct PointerEventArgs_t4020535570;
struct PointerEventArgs_t4020535570_marshaled_pinvoke;

extern "C" void PointerEventArgs_t4020535570_marshal_pinvoke(const PointerEventArgs_t4020535570& unmarshaled, PointerEventArgs_t4020535570_marshaled_pinvoke& marshaled);
extern "C" void PointerEventArgs_t4020535570_marshal_pinvoke_back(const PointerEventArgs_t4020535570_marshaled_pinvoke& marshaled, PointerEventArgs_t4020535570& unmarshaled);
extern "C" void PointerEventArgs_t4020535570_marshal_pinvoke_cleanup(PointerEventArgs_t4020535570_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct PointerEventArgs_t4020535570;
struct PointerEventArgs_t4020535570_marshaled_com;

extern "C" void PointerEventArgs_t4020535570_marshal_com(const PointerEventArgs_t4020535570& unmarshaled, PointerEventArgs_t4020535570_marshaled_com& marshaled);
extern "C" void PointerEventArgs_t4020535570_marshal_com_back(const PointerEventArgs_t4020535570_marshaled_com& marshaled, PointerEventArgs_t4020535570& unmarshaled);
extern "C" void PointerEventArgs_t4020535570_marshal_com_cleanup(PointerEventArgs_t4020535570_marshaled_com& marshaled);
