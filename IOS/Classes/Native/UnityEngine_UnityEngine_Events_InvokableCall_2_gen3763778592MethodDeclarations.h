﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>
struct InvokableCall_2_t3763778592;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>
struct UnityAction_2_t3748987708;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m2494562780_gshared (InvokableCall_2_t3763778592 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_2__ctor_m2494562780(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_2_t3763778592 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2__ctor_m2494562780_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m3838738679_gshared (InvokableCall_2_t3763778592 * __this, UnityAction_2_t3748987708 * ___action0, const MethodInfo* method);
#define InvokableCall_2__ctor_m3838738679(__this, ___action0, method) ((  void (*) (InvokableCall_2_t3763778592 *, UnityAction_2_t3748987708 *, const MethodInfo*))InvokableCall_2__ctor_m3838738679_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m3250696026_gshared (InvokableCall_2_t3763778592 * __this, UnityAction_2_t3748987708 * ___value0, const MethodInfo* method);
#define InvokableCall_2_add_Delegate_m3250696026(__this, ___value0, method) ((  void (*) (InvokableCall_2_t3763778592 *, UnityAction_2_t3748987708 *, const MethodInfo*))InvokableCall_2_add_Delegate_m3250696026_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m2939077381_gshared (InvokableCall_2_t3763778592 * __this, UnityAction_2_t3748987708 * ___value0, const MethodInfo* method);
#define InvokableCall_2_remove_Delegate_m2939077381(__this, ___value0, method) ((  void (*) (InvokableCall_2_t3763778592 *, UnityAction_2_t3748987708 *, const MethodInfo*))InvokableCall_2_remove_Delegate_m2939077381_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m414081419_gshared (InvokableCall_2_t3763778592 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_2_Invoke_m414081419(__this, ___args0, method) ((  void (*) (InvokableCall_2_t3763778592 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_2_Invoke_m414081419_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.HeadsetControllerAwareEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m736191899_gshared (InvokableCall_2_t3763778592 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_2_Find_m736191899(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_2_t3763778592 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2_Find_m736191899_gshared)(__this, ___targetObj0, ___method1, method)
