﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_IndependentRadialMenuController
struct VRTK_IndependentRadialMenuController_t4268715152;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_IndependentRadialMenuController::.ctor()
extern "C"  void VRTK_IndependentRadialMenuController__ctor_m664159000 (VRTK_IndependentRadialMenuController_t4268715152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::UpdateEventsManager()
extern "C"  void VRTK_IndependentRadialMenuController_UpdateEventsManager_m3635452233 (VRTK_IndependentRadialMenuController_t4268715152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::Initialize()
extern "C"  void VRTK_IndependentRadialMenuController_Initialize_m3683813544 (VRTK_IndependentRadialMenuController_t4268715152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::OnEnable()
extern "C"  void VRTK_IndependentRadialMenuController_OnEnable_m1662076128 (VRTK_IndependentRadialMenuController_t4268715152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::OnDisable()
extern "C"  void VRTK_IndependentRadialMenuController_OnDisable_m3077502577 (VRTK_IndependentRadialMenuController_t4268715152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::ObjectClicked(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_IndependentRadialMenuController_ObjectClicked_m1122997079 (VRTK_IndependentRadialMenuController_t4268715152 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::ObjectUnClicked(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_IndependentRadialMenuController_ObjectUnClicked_m2230517568 (VRTK_IndependentRadialMenuController_t4268715152 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::ObjectTouched(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_IndependentRadialMenuController_ObjectTouched_m767198142 (VRTK_IndependentRadialMenuController_t4268715152 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::ObjectUntouched(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_IndependentRadialMenuController_ObjectUntouched_m2873638731 (VRTK_IndependentRadialMenuController_t4268715152 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::AttemptHapticPulse(System.Single)
extern "C"  void VRTK_IndependentRadialMenuController_AttemptHapticPulse_m1637494830 (VRTK_IndependentRadialMenuController_t4268715152 * __this, float ___strength0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_IndependentRadialMenuController::CalculateAngle(UnityEngine.GameObject)
extern "C"  float VRTK_IndependentRadialMenuController_CalculateAngle_m1107068847 (VRTK_IndependentRadialMenuController_t4268715152 * __this, GameObject_t1756533147 * ___interactingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_IndependentRadialMenuController::AngleSigned(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float VRTK_IndependentRadialMenuController_AngleSigned_m911254242 (VRTK_IndependentRadialMenuController_t4268715152 * __this, Vector3_t2243707580  ___v10, Vector3_t2243707580  ___v21, Vector3_t2243707580  ___n2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::ImmediatelyHideMenu(VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_IndependentRadialMenuController_ImmediatelyHideMenu_m3983548798 (VRTK_IndependentRadialMenuController_t4268715152 * __this, InteractableObjectEventArgs_t473175556  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::SetColliderState(System.Boolean,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_IndependentRadialMenuController_SetColliderState_m1863245595 (VRTK_IndependentRadialMenuController_t4268715152 * __this, bool ___state0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_IndependentRadialMenuController::DelayedSetColliderEnabled(System.Boolean,System.Single,VRTK.InteractableObjectEventArgs)
extern "C"  Il2CppObject * VRTK_IndependentRadialMenuController_DelayedSetColliderEnabled_m291315098 (VRTK_IndependentRadialMenuController_t4268715152 * __this, bool ___enabled0, float ___delay1, InteractableObjectEventArgs_t473175556  ___e2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::Awake()
extern "C"  void VRTK_IndependentRadialMenuController_Awake_m2060002713 (VRTK_IndependentRadialMenuController_t4268715152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::Start()
extern "C"  void VRTK_IndependentRadialMenuController_Start_m3665412064 (VRTK_IndependentRadialMenuController_t4268715152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::Update()
extern "C"  void VRTK_IndependentRadialMenuController_Update_m920367037 (VRTK_IndependentRadialMenuController_t4268715152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_IndependentRadialMenuController::FixedUpdate()
extern "C"  void VRTK_IndependentRadialMenuController_FixedUpdate_m3245574719 (VRTK_IndependentRadialMenuController_t4268715152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
