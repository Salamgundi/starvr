﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_ControllerManager
struct SteamVR_ControllerManager_t3520649604;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_t3405266389.h"

// System.Void SteamVR_ControllerManager::.ctor()
extern "C"  void SteamVR_ControllerManager__ctor_m178506221 (SteamVR_ControllerManager_t3520649604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ControllerManager::UpdateTargets()
extern "C"  void SteamVR_ControllerManager_UpdateTargets_m503145802 (SteamVR_ControllerManager_t3520649604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ControllerManager::Awake()
extern "C"  void SteamVR_ControllerManager_Awake_m1244489508 (SteamVR_ControllerManager_t3520649604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ControllerManager::OnEnable()
extern "C"  void SteamVR_ControllerManager_OnEnable_m3800765589 (SteamVR_ControllerManager_t3520649604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ControllerManager::OnDisable()
extern "C"  void SteamVR_ControllerManager_OnDisable_m1218488284 (SteamVR_ControllerManager_t3520649604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ControllerManager::OnInputFocus(System.Boolean)
extern "C"  void SteamVR_ControllerManager_OnInputFocus_m1648290365 (SteamVR_ControllerManager_t3520649604 * __this, bool ___hasFocus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ControllerManager::HideObject(UnityEngine.Transform,System.String)
extern "C"  void SteamVR_ControllerManager_HideObject_m1962958265 (SteamVR_ControllerManager_t3520649604 * __this, Transform_t3275118058 * ___t0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ControllerManager::ShowObject(UnityEngine.Transform,System.String)
extern "C"  void SteamVR_ControllerManager_ShowObject_m750956022 (SteamVR_ControllerManager_t3520649604 * __this, Transform_t3275118058 * ___t0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ControllerManager::SetTrackedDeviceIndex(System.Int32,System.UInt32)
extern "C"  void SteamVR_ControllerManager_SetTrackedDeviceIndex_m404037226 (SteamVR_ControllerManager_t3520649604 * __this, int32_t ___objectIndex0, uint32_t ___trackedDeviceIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ControllerManager::OnTrackedDeviceRoleChanged(Valve.VR.VREvent_t)
extern "C"  void SteamVR_ControllerManager_OnTrackedDeviceRoleChanged_m3260814863 (SteamVR_ControllerManager_t3520649604 * __this, VREvent_t_t3405266389  ___vrEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ControllerManager::OnDeviceConnected(System.Int32,System.Boolean)
extern "C"  void SteamVR_ControllerManager_OnDeviceConnected_m2774588997 (SteamVR_ControllerManager_t3520649604 * __this, int32_t ___index0, bool ___connected1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ControllerManager::Refresh()
extern "C"  void SteamVR_ControllerManager_Refresh_m823751552 (SteamVR_ControllerManager_t3520649604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_ControllerManager::.cctor()
extern "C"  void SteamVR_ControllerManager__cctor_m4124303902 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
