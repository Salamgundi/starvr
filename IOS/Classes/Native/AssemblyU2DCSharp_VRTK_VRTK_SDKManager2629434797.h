﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails>
struct Dictionary_2_t111376037;
// VRTK.VRTK_SDKManager
struct VRTK_SDKManager_t2629434797;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SDKManager_SupportedSD1339136682.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_SDKManager
struct  VRTK_SDKManager_t2629434797  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,VRTK.VRTK_SDKDetails> VRTK.VRTK_SDKManager::sdkDetails
	Dictionary_2_t111376037 * ___sdkDetails_2;
	// System.Boolean VRTK.VRTK_SDKManager::persistOnLoad
	bool ___persistOnLoad_4;
	// VRTK.VRTK_SDKManager/SupportedSDKs VRTK.VRTK_SDKManager::systemSDK
	int32_t ___systemSDK_5;
	// VRTK.VRTK_SDKManager/SupportedSDKs VRTK.VRTK_SDKManager::boundariesSDK
	int32_t ___boundariesSDK_6;
	// VRTK.VRTK_SDKManager/SupportedSDKs VRTK.VRTK_SDKManager::headsetSDK
	int32_t ___headsetSDK_7;
	// VRTK.VRTK_SDKManager/SupportedSDKs VRTK.VRTK_SDKManager::controllerSDK
	int32_t ___controllerSDK_8;
	// System.Boolean VRTK.VRTK_SDKManager::autoManageScriptDefines
	bool ___autoManageScriptDefines_9;
	// UnityEngine.GameObject VRTK.VRTK_SDKManager::actualBoundaries
	GameObject_t1756533147 * ___actualBoundaries_10;
	// UnityEngine.GameObject VRTK.VRTK_SDKManager::actualHeadset
	GameObject_t1756533147 * ___actualHeadset_11;
	// UnityEngine.GameObject VRTK.VRTK_SDKManager::actualLeftController
	GameObject_t1756533147 * ___actualLeftController_12;
	// UnityEngine.GameObject VRTK.VRTK_SDKManager::actualRightController
	GameObject_t1756533147 * ___actualRightController_13;
	// UnityEngine.GameObject VRTK.VRTK_SDKManager::modelAliasLeftController
	GameObject_t1756533147 * ___modelAliasLeftController_14;
	// UnityEngine.GameObject VRTK.VRTK_SDKManager::modelAliasRightController
	GameObject_t1756533147 * ___modelAliasRightController_15;
	// UnityEngine.GameObject VRTK.VRTK_SDKManager::scriptAliasLeftController
	GameObject_t1756533147 * ___scriptAliasLeftController_16;
	// UnityEngine.GameObject VRTK.VRTK_SDKManager::scriptAliasRightController
	GameObject_t1756533147 * ___scriptAliasRightController_17;

public:
	inline static int32_t get_offset_of_sdkDetails_2() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___sdkDetails_2)); }
	inline Dictionary_2_t111376037 * get_sdkDetails_2() const { return ___sdkDetails_2; }
	inline Dictionary_2_t111376037 ** get_address_of_sdkDetails_2() { return &___sdkDetails_2; }
	inline void set_sdkDetails_2(Dictionary_2_t111376037 * value)
	{
		___sdkDetails_2 = value;
		Il2CppCodeGenWriteBarrier(&___sdkDetails_2, value);
	}

	inline static int32_t get_offset_of_persistOnLoad_4() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___persistOnLoad_4)); }
	inline bool get_persistOnLoad_4() const { return ___persistOnLoad_4; }
	inline bool* get_address_of_persistOnLoad_4() { return &___persistOnLoad_4; }
	inline void set_persistOnLoad_4(bool value)
	{
		___persistOnLoad_4 = value;
	}

	inline static int32_t get_offset_of_systemSDK_5() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___systemSDK_5)); }
	inline int32_t get_systemSDK_5() const { return ___systemSDK_5; }
	inline int32_t* get_address_of_systemSDK_5() { return &___systemSDK_5; }
	inline void set_systemSDK_5(int32_t value)
	{
		___systemSDK_5 = value;
	}

	inline static int32_t get_offset_of_boundariesSDK_6() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___boundariesSDK_6)); }
	inline int32_t get_boundariesSDK_6() const { return ___boundariesSDK_6; }
	inline int32_t* get_address_of_boundariesSDK_6() { return &___boundariesSDK_6; }
	inline void set_boundariesSDK_6(int32_t value)
	{
		___boundariesSDK_6 = value;
	}

	inline static int32_t get_offset_of_headsetSDK_7() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___headsetSDK_7)); }
	inline int32_t get_headsetSDK_7() const { return ___headsetSDK_7; }
	inline int32_t* get_address_of_headsetSDK_7() { return &___headsetSDK_7; }
	inline void set_headsetSDK_7(int32_t value)
	{
		___headsetSDK_7 = value;
	}

	inline static int32_t get_offset_of_controllerSDK_8() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___controllerSDK_8)); }
	inline int32_t get_controllerSDK_8() const { return ___controllerSDK_8; }
	inline int32_t* get_address_of_controllerSDK_8() { return &___controllerSDK_8; }
	inline void set_controllerSDK_8(int32_t value)
	{
		___controllerSDK_8 = value;
	}

	inline static int32_t get_offset_of_autoManageScriptDefines_9() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___autoManageScriptDefines_9)); }
	inline bool get_autoManageScriptDefines_9() const { return ___autoManageScriptDefines_9; }
	inline bool* get_address_of_autoManageScriptDefines_9() { return &___autoManageScriptDefines_9; }
	inline void set_autoManageScriptDefines_9(bool value)
	{
		___autoManageScriptDefines_9 = value;
	}

	inline static int32_t get_offset_of_actualBoundaries_10() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___actualBoundaries_10)); }
	inline GameObject_t1756533147 * get_actualBoundaries_10() const { return ___actualBoundaries_10; }
	inline GameObject_t1756533147 ** get_address_of_actualBoundaries_10() { return &___actualBoundaries_10; }
	inline void set_actualBoundaries_10(GameObject_t1756533147 * value)
	{
		___actualBoundaries_10 = value;
		Il2CppCodeGenWriteBarrier(&___actualBoundaries_10, value);
	}

	inline static int32_t get_offset_of_actualHeadset_11() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___actualHeadset_11)); }
	inline GameObject_t1756533147 * get_actualHeadset_11() const { return ___actualHeadset_11; }
	inline GameObject_t1756533147 ** get_address_of_actualHeadset_11() { return &___actualHeadset_11; }
	inline void set_actualHeadset_11(GameObject_t1756533147 * value)
	{
		___actualHeadset_11 = value;
		Il2CppCodeGenWriteBarrier(&___actualHeadset_11, value);
	}

	inline static int32_t get_offset_of_actualLeftController_12() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___actualLeftController_12)); }
	inline GameObject_t1756533147 * get_actualLeftController_12() const { return ___actualLeftController_12; }
	inline GameObject_t1756533147 ** get_address_of_actualLeftController_12() { return &___actualLeftController_12; }
	inline void set_actualLeftController_12(GameObject_t1756533147 * value)
	{
		___actualLeftController_12 = value;
		Il2CppCodeGenWriteBarrier(&___actualLeftController_12, value);
	}

	inline static int32_t get_offset_of_actualRightController_13() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___actualRightController_13)); }
	inline GameObject_t1756533147 * get_actualRightController_13() const { return ___actualRightController_13; }
	inline GameObject_t1756533147 ** get_address_of_actualRightController_13() { return &___actualRightController_13; }
	inline void set_actualRightController_13(GameObject_t1756533147 * value)
	{
		___actualRightController_13 = value;
		Il2CppCodeGenWriteBarrier(&___actualRightController_13, value);
	}

	inline static int32_t get_offset_of_modelAliasLeftController_14() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___modelAliasLeftController_14)); }
	inline GameObject_t1756533147 * get_modelAliasLeftController_14() const { return ___modelAliasLeftController_14; }
	inline GameObject_t1756533147 ** get_address_of_modelAliasLeftController_14() { return &___modelAliasLeftController_14; }
	inline void set_modelAliasLeftController_14(GameObject_t1756533147 * value)
	{
		___modelAliasLeftController_14 = value;
		Il2CppCodeGenWriteBarrier(&___modelAliasLeftController_14, value);
	}

	inline static int32_t get_offset_of_modelAliasRightController_15() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___modelAliasRightController_15)); }
	inline GameObject_t1756533147 * get_modelAliasRightController_15() const { return ___modelAliasRightController_15; }
	inline GameObject_t1756533147 ** get_address_of_modelAliasRightController_15() { return &___modelAliasRightController_15; }
	inline void set_modelAliasRightController_15(GameObject_t1756533147 * value)
	{
		___modelAliasRightController_15 = value;
		Il2CppCodeGenWriteBarrier(&___modelAliasRightController_15, value);
	}

	inline static int32_t get_offset_of_scriptAliasLeftController_16() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___scriptAliasLeftController_16)); }
	inline GameObject_t1756533147 * get_scriptAliasLeftController_16() const { return ___scriptAliasLeftController_16; }
	inline GameObject_t1756533147 ** get_address_of_scriptAliasLeftController_16() { return &___scriptAliasLeftController_16; }
	inline void set_scriptAliasLeftController_16(GameObject_t1756533147 * value)
	{
		___scriptAliasLeftController_16 = value;
		Il2CppCodeGenWriteBarrier(&___scriptAliasLeftController_16, value);
	}

	inline static int32_t get_offset_of_scriptAliasRightController_17() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797, ___scriptAliasRightController_17)); }
	inline GameObject_t1756533147 * get_scriptAliasRightController_17() const { return ___scriptAliasRightController_17; }
	inline GameObject_t1756533147 ** get_address_of_scriptAliasRightController_17() { return &___scriptAliasRightController_17; }
	inline void set_scriptAliasRightController_17(GameObject_t1756533147 * value)
	{
		___scriptAliasRightController_17 = value;
		Il2CppCodeGenWriteBarrier(&___scriptAliasRightController_17, value);
	}
};

struct VRTK_SDKManager_t2629434797_StaticFields
{
public:
	// VRTK.VRTK_SDKManager VRTK.VRTK_SDKManager::instance
	VRTK_SDKManager_t2629434797 * ___instance_3;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(VRTK_SDKManager_t2629434797_StaticFields, ___instance_3)); }
	inline VRTK_SDKManager_t2629434797 * get_instance_3() const { return ___instance_3; }
	inline VRTK_SDKManager_t2629434797 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(VRTK_SDKManager_t2629434797 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
