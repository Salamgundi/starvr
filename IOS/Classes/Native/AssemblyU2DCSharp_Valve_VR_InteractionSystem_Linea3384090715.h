﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.LinearMapping
struct LinearMapping_t810676855;
// UnityEngine.Animation
struct Animation_t2068071072;
// UnityEngine.AnimationState
struct AnimationState_t1303741697;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.LinearAnimation
struct  LinearAnimation_t3384090715  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.LinearMapping Valve.VR.InteractionSystem.LinearAnimation::linearMapping
	LinearMapping_t810676855 * ___linearMapping_2;
	// UnityEngine.Animation Valve.VR.InteractionSystem.LinearAnimation::animation
	Animation_t2068071072 * ___animation_3;
	// UnityEngine.AnimationState Valve.VR.InteractionSystem.LinearAnimation::animState
	AnimationState_t1303741697 * ___animState_4;
	// System.Single Valve.VR.InteractionSystem.LinearAnimation::animLength
	float ___animLength_5;
	// System.Single Valve.VR.InteractionSystem.LinearAnimation::lastValue
	float ___lastValue_6;

public:
	inline static int32_t get_offset_of_linearMapping_2() { return static_cast<int32_t>(offsetof(LinearAnimation_t3384090715, ___linearMapping_2)); }
	inline LinearMapping_t810676855 * get_linearMapping_2() const { return ___linearMapping_2; }
	inline LinearMapping_t810676855 ** get_address_of_linearMapping_2() { return &___linearMapping_2; }
	inline void set_linearMapping_2(LinearMapping_t810676855 * value)
	{
		___linearMapping_2 = value;
		Il2CppCodeGenWriteBarrier(&___linearMapping_2, value);
	}

	inline static int32_t get_offset_of_animation_3() { return static_cast<int32_t>(offsetof(LinearAnimation_t3384090715, ___animation_3)); }
	inline Animation_t2068071072 * get_animation_3() const { return ___animation_3; }
	inline Animation_t2068071072 ** get_address_of_animation_3() { return &___animation_3; }
	inline void set_animation_3(Animation_t2068071072 * value)
	{
		___animation_3 = value;
		Il2CppCodeGenWriteBarrier(&___animation_3, value);
	}

	inline static int32_t get_offset_of_animState_4() { return static_cast<int32_t>(offsetof(LinearAnimation_t3384090715, ___animState_4)); }
	inline AnimationState_t1303741697 * get_animState_4() const { return ___animState_4; }
	inline AnimationState_t1303741697 ** get_address_of_animState_4() { return &___animState_4; }
	inline void set_animState_4(AnimationState_t1303741697 * value)
	{
		___animState_4 = value;
		Il2CppCodeGenWriteBarrier(&___animState_4, value);
	}

	inline static int32_t get_offset_of_animLength_5() { return static_cast<int32_t>(offsetof(LinearAnimation_t3384090715, ___animLength_5)); }
	inline float get_animLength_5() const { return ___animLength_5; }
	inline float* get_address_of_animLength_5() { return &___animLength_5; }
	inline void set_animLength_5(float value)
	{
		___animLength_5 = value;
	}

	inline static int32_t get_offset_of_lastValue_6() { return static_cast<int32_t>(offsetof(LinearAnimation_t3384090715, ___lastValue_6)); }
	inline float get_lastValue_6() const { return ___lastValue_6; }
	inline float* get_address_of_lastValue_6() { return &___lastValue_6; }
	inline void set_lastValue_6(float value)
	{
		___lastValue_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
