﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_SDKManager
struct VRTK_SDKManager_t2629434797;
// VRTK.SDK_BaseSystem
struct SDK_BaseSystem_t244469351;
// VRTK.SDK_BaseHeadset
struct SDK_BaseHeadset_t3046873914;
// VRTK.SDK_BaseController
struct SDK_BaseController_t197168236;
// VRTK.SDK_BaseBoundaries
struct SDK_BaseBoundaries_t1766380066;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_SDKManager::.ctor()
extern "C"  void VRTK_SDKManager__ctor_m3425088233 (VRTK_SDKManager_t2629434797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.SDK_BaseSystem VRTK.VRTK_SDKManager::GetSystemSDK()
extern "C"  SDK_BaseSystem_t244469351 * VRTK_SDKManager_GetSystemSDK_m1113381987 (VRTK_SDKManager_t2629434797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.SDK_BaseHeadset VRTK.VRTK_SDKManager::GetHeadsetSDK()
extern "C"  SDK_BaseHeadset_t3046873914 * VRTK_SDKManager_GetHeadsetSDK_m1925074571 (VRTK_SDKManager_t2629434797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.SDK_BaseController VRTK.VRTK_SDKManager::GetControllerSDK()
extern "C"  SDK_BaseController_t197168236 * VRTK_SDKManager_GetControllerSDK_m274578403 (VRTK_SDKManager_t2629434797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.SDK_BaseBoundaries VRTK.VRTK_SDKManager::GetBoundariesSDK()
extern "C"  SDK_BaseBoundaries_t1766380066 * VRTK_SDKManager_GetBoundariesSDK_m3294236963 (VRTK_SDKManager_t2629434797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SDKManager::Awake()
extern "C"  void VRTK_SDKManager_Awake_m1017683398 (VRTK_SDKManager_t2629434797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SDKManager::SetupHeadset()
extern "C"  void VRTK_SDKManager_SetupHeadset_m3226377248 (VRTK_SDKManager_t2629434797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SDKManager::SetupControllers()
extern "C"  void VRTK_SDKManager_SetupControllers_m1627032421 (VRTK_SDKManager_t2629434797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SDKManager::CreateInstance()
extern "C"  void VRTK_SDKManager_CreateInstance_m674894198 (VRTK_SDKManager_t2629434797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SDKManager::.cctor()
extern "C"  void VRTK_SDKManager__cctor_m3853050536 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
