﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Highlighters.VRTK_BaseHighlighter
struct VRTK_BaseHighlighter_t3110203740;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.Highlighters.VRTK_BaseHighlighter::.ctor()
extern "C"  void VRTK_BaseHighlighter__ctor_m1677849478 (VRTK_BaseHighlighter_t3110203740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.Highlighters.VRTK_BaseHighlighter::UsesClonedObject()
extern "C"  bool VRTK_BaseHighlighter_UsesClonedObject_m1583337702 (VRTK_BaseHighlighter_t3110203740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.Highlighters.VRTK_BaseHighlighter VRTK.Highlighters.VRTK_BaseHighlighter::GetActiveHighlighter(UnityEngine.GameObject)
extern "C"  VRTK_BaseHighlighter_t3110203740 * VRTK_BaseHighlighter_GetActiveHighlighter_m258623881 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_BaseHighlighter::OnDisable()
extern "C"  void VRTK_BaseHighlighter_OnDisable_m2996393919 (VRTK_BaseHighlighter_t3110203740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
