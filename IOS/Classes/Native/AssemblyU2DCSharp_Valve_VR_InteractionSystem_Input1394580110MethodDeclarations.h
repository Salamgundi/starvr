﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.InputModule
struct InputModule_t1394580110;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void Valve.VR.InteractionSystem.InputModule::.ctor()
extern "C"  void InputModule__ctor_m2132934084 (InputModule_t1394580110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.InteractionSystem.InputModule Valve.VR.InteractionSystem.InputModule::get_instance()
extern "C"  InputModule_t1394580110 * InputModule_get_instance_m757368332 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.InputModule::ShouldActivateModule()
extern "C"  bool InputModule_ShouldActivateModule_m187261454 (InputModule_t1394580110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InputModule::HoverBegin(UnityEngine.GameObject)
extern "C"  void InputModule_HoverBegin_m3347134599 (InputModule_t1394580110 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InputModule::HoverEnd(UnityEngine.GameObject)
extern "C"  void InputModule_HoverEnd_m118290535 (InputModule_t1394580110 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InputModule::Submit(UnityEngine.GameObject)
extern "C"  void InputModule_Submit_m138378048 (InputModule_t1394580110 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.InputModule::Process()
extern "C"  void InputModule_Process_m1616014909 (InputModule_t1394580110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
