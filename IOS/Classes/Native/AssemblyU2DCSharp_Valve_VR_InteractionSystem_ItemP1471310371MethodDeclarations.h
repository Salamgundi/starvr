﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ItemPackageSpawner
struct ItemPackageSpawner_t1471310371;
// Valve.VR.InteractionSystem.ItemPackage
struct ItemPackage_t3423754743;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_ItemP3423754743.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_ItemP4052302202.h"

// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::.ctor()
extern "C"  void ItemPackageSpawner__ctor_m624960029 (ItemPackageSpawner_t1471310371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.InteractionSystem.ItemPackage Valve.VR.InteractionSystem.ItemPackageSpawner::get_itemPackage()
extern "C"  ItemPackage_t3423754743 * ItemPackageSpawner_get_itemPackage_m2804144246 (ItemPackageSpawner_t1471310371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::set_itemPackage(Valve.VR.InteractionSystem.ItemPackage)
extern "C"  void ItemPackageSpawner_set_itemPackage_m286839117 (ItemPackageSpawner_t1471310371 * __this, ItemPackage_t3423754743 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::CreatePreviewObject()
extern "C"  void ItemPackageSpawner_CreatePreviewObject_m2707225556 (ItemPackageSpawner_t1471310371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::Start()
extern "C"  void ItemPackageSpawner_Start_m1362667133 (ItemPackageSpawner_t1471310371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::VerifyItemPackage()
extern "C"  void ItemPackageSpawner_VerifyItemPackage_m3202695397 (ItemPackageSpawner_t1471310371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::ItemPackageNotValid()
extern "C"  void ItemPackageSpawner_ItemPackageNotValid_m3952011201 (ItemPackageSpawner_t1471310371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::ClearPreview()
extern "C"  void ItemPackageSpawner_ClearPreview_m3490560036 (ItemPackageSpawner_t1471310371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::Update()
extern "C"  void ItemPackageSpawner_Update_m3537170242 (ItemPackageSpawner_t1471310371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::OnHandHoverBegin(Valve.VR.InteractionSystem.Hand)
extern "C"  void ItemPackageSpawner_OnHandHoverBegin_m2723147412 (ItemPackageSpawner_t1471310371 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::TakeBackItem(Valve.VR.InteractionSystem.Hand)
extern "C"  void ItemPackageSpawner_TakeBackItem_m1432484860 (ItemPackageSpawner_t1471310371 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.InteractionSystem.ItemPackage Valve.VR.InteractionSystem.ItemPackageSpawner::GetAttachedItemPackage(Valve.VR.InteractionSystem.Hand)
extern "C"  ItemPackage_t3423754743 * ItemPackageSpawner_GetAttachedItemPackage_m2261696797 (ItemPackageSpawner_t1471310371 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::HandHoverUpdate(Valve.VR.InteractionSystem.Hand)
extern "C"  void ItemPackageSpawner_HandHoverUpdate_m920278063 (ItemPackageSpawner_t1471310371 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::OnHandHoverEnd(Valve.VR.InteractionSystem.Hand)
extern "C"  void ItemPackageSpawner_OnHandHoverEnd_m3657857376 (ItemPackageSpawner_t1471310371 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::RemoveMatchingItemsFromHandStack(Valve.VR.InteractionSystem.ItemPackage,Valve.VR.InteractionSystem.Hand)
extern "C"  void ItemPackageSpawner_RemoveMatchingItemsFromHandStack_m3368639749 (ItemPackageSpawner_t1471310371 * __this, ItemPackage_t3423754743 * ___package0, Hand_t379716353 * ___hand1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::RemoveMatchingItemTypesFromHand(Valve.VR.InteractionSystem.ItemPackage/ItemPackageType,Valve.VR.InteractionSystem.Hand)
extern "C"  void ItemPackageSpawner_RemoveMatchingItemTypesFromHand_m3419095779 (ItemPackageSpawner_t1471310371 * __this, int32_t ___packageType0, Hand_t379716353 * ___hand1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ItemPackageSpawner::SpawnAndAttachObject(Valve.VR.InteractionSystem.Hand)
extern "C"  void ItemPackageSpawner_SpawnAndAttachObject_m1963592833 (ItemPackageSpawner_t1471310371 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
