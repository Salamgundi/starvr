﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_InteractUse
struct VRTK_InteractUse_t4015307561;
// VRTK.UnityEventHelper.VRTK_InteractUse_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t2739379367;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_InteractUse_UnityEvents
struct  VRTK_InteractUse_UnityEvents_t2357120922  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_InteractUse VRTK.UnityEventHelper.VRTK_InteractUse_UnityEvents::iu
	VRTK_InteractUse_t4015307561 * ___iu_2;
	// VRTK.UnityEventHelper.VRTK_InteractUse_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_InteractUse_UnityEvents::OnControllerUseInteractableObject
	UnityObjectEvent_t2739379367 * ___OnControllerUseInteractableObject_3;
	// VRTK.UnityEventHelper.VRTK_InteractUse_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_InteractUse_UnityEvents::OnControllerUnuseInteractableObject
	UnityObjectEvent_t2739379367 * ___OnControllerUnuseInteractableObject_4;

public:
	inline static int32_t get_offset_of_iu_2() { return static_cast<int32_t>(offsetof(VRTK_InteractUse_UnityEvents_t2357120922, ___iu_2)); }
	inline VRTK_InteractUse_t4015307561 * get_iu_2() const { return ___iu_2; }
	inline VRTK_InteractUse_t4015307561 ** get_address_of_iu_2() { return &___iu_2; }
	inline void set_iu_2(VRTK_InteractUse_t4015307561 * value)
	{
		___iu_2 = value;
		Il2CppCodeGenWriteBarrier(&___iu_2, value);
	}

	inline static int32_t get_offset_of_OnControllerUseInteractableObject_3() { return static_cast<int32_t>(offsetof(VRTK_InteractUse_UnityEvents_t2357120922, ___OnControllerUseInteractableObject_3)); }
	inline UnityObjectEvent_t2739379367 * get_OnControllerUseInteractableObject_3() const { return ___OnControllerUseInteractableObject_3; }
	inline UnityObjectEvent_t2739379367 ** get_address_of_OnControllerUseInteractableObject_3() { return &___OnControllerUseInteractableObject_3; }
	inline void set_OnControllerUseInteractableObject_3(UnityObjectEvent_t2739379367 * value)
	{
		___OnControllerUseInteractableObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerUseInteractableObject_3, value);
	}

	inline static int32_t get_offset_of_OnControllerUnuseInteractableObject_4() { return static_cast<int32_t>(offsetof(VRTK_InteractUse_UnityEvents_t2357120922, ___OnControllerUnuseInteractableObject_4)); }
	inline UnityObjectEvent_t2739379367 * get_OnControllerUnuseInteractableObject_4() const { return ___OnControllerUnuseInteractableObject_4; }
	inline UnityObjectEvent_t2739379367 ** get_address_of_OnControllerUnuseInteractableObject_4() { return &___OnControllerUnuseInteractableObject_4; }
	inline void set_OnControllerUnuseInteractableObject_4(UnityObjectEvent_t2739379367 * value)
	{
		___OnControllerUnuseInteractableObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerUnuseInteractableObject_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
