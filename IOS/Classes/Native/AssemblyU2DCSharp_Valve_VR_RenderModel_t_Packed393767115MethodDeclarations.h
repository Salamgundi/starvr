﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_t_Packed393767115.h"
#include "AssemblyU2DCSharp_Valve_VR_RenderModel_t1723493896.h"

// System.Void Valve.VR.RenderModel_t_Packed::Unpack(Valve.VR.RenderModel_t&)
extern "C"  void RenderModel_t_Packed_Unpack_m562006704 (RenderModel_t_Packed_t393767115 * __this, RenderModel_t_t1723493896 * ___unpacked0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
