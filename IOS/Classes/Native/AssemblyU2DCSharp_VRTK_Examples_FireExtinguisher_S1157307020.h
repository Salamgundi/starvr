﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.Examples.FireExtinguisher_Base
struct FireExtinguisher_Base_t3980302883;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;

#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.FireExtinguisher_Sprayer
struct  FireExtinguisher_Sprayer_t1157307020  : public VRTK_InteractableObject_t2604188111
{
public:
	// VRTK.Examples.FireExtinguisher_Base VRTK.Examples.FireExtinguisher_Sprayer::baseCan
	FireExtinguisher_Base_t3980302883 * ___baseCan_45;
	// System.Single VRTK.Examples.FireExtinguisher_Sprayer::breakDistance
	float ___breakDistance_46;
	// System.Single VRTK.Examples.FireExtinguisher_Sprayer::maxSprayPower
	float ___maxSprayPower_47;
	// UnityEngine.GameObject VRTK.Examples.FireExtinguisher_Sprayer::waterSpray
	GameObject_t1756533147 * ___waterSpray_48;
	// UnityEngine.ParticleSystem VRTK.Examples.FireExtinguisher_Sprayer::particles
	ParticleSystem_t3394631041 * ___particles_49;

public:
	inline static int32_t get_offset_of_baseCan_45() { return static_cast<int32_t>(offsetof(FireExtinguisher_Sprayer_t1157307020, ___baseCan_45)); }
	inline FireExtinguisher_Base_t3980302883 * get_baseCan_45() const { return ___baseCan_45; }
	inline FireExtinguisher_Base_t3980302883 ** get_address_of_baseCan_45() { return &___baseCan_45; }
	inline void set_baseCan_45(FireExtinguisher_Base_t3980302883 * value)
	{
		___baseCan_45 = value;
		Il2CppCodeGenWriteBarrier(&___baseCan_45, value);
	}

	inline static int32_t get_offset_of_breakDistance_46() { return static_cast<int32_t>(offsetof(FireExtinguisher_Sprayer_t1157307020, ___breakDistance_46)); }
	inline float get_breakDistance_46() const { return ___breakDistance_46; }
	inline float* get_address_of_breakDistance_46() { return &___breakDistance_46; }
	inline void set_breakDistance_46(float value)
	{
		___breakDistance_46 = value;
	}

	inline static int32_t get_offset_of_maxSprayPower_47() { return static_cast<int32_t>(offsetof(FireExtinguisher_Sprayer_t1157307020, ___maxSprayPower_47)); }
	inline float get_maxSprayPower_47() const { return ___maxSprayPower_47; }
	inline float* get_address_of_maxSprayPower_47() { return &___maxSprayPower_47; }
	inline void set_maxSprayPower_47(float value)
	{
		___maxSprayPower_47 = value;
	}

	inline static int32_t get_offset_of_waterSpray_48() { return static_cast<int32_t>(offsetof(FireExtinguisher_Sprayer_t1157307020, ___waterSpray_48)); }
	inline GameObject_t1756533147 * get_waterSpray_48() const { return ___waterSpray_48; }
	inline GameObject_t1756533147 ** get_address_of_waterSpray_48() { return &___waterSpray_48; }
	inline void set_waterSpray_48(GameObject_t1756533147 * value)
	{
		___waterSpray_48 = value;
		Il2CppCodeGenWriteBarrier(&___waterSpray_48, value);
	}

	inline static int32_t get_offset_of_particles_49() { return static_cast<int32_t>(offsetof(FireExtinguisher_Sprayer_t1157307020, ___particles_49)); }
	inline ParticleSystem_t3394631041 * get_particles_49() const { return ___particles_49; }
	inline ParticleSystem_t3394631041 ** get_address_of_particles_49() { return &___particles_49; }
	inline void set_particles_49(ParticleSystem_t3394631041 * value)
	{
		___particles_49 = value;
		Il2CppCodeGenWriteBarrier(&___particles_49, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
