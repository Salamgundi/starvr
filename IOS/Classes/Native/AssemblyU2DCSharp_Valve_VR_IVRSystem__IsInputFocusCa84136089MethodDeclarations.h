﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_IsInputFocusCapturedByAnotherProcess
struct _IsInputFocusCapturedByAnotherProcess_t84136089;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_IsInputFocusCapturedByAnotherProcess::.ctor(System.Object,System.IntPtr)
extern "C"  void _IsInputFocusCapturedByAnotherProcess__ctor_m3692242704 (_IsInputFocusCapturedByAnotherProcess_t84136089 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_IsInputFocusCapturedByAnotherProcess::Invoke()
extern "C"  bool _IsInputFocusCapturedByAnotherProcess_Invoke_m492171912 (_IsInputFocusCapturedByAnotherProcess_t84136089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_IsInputFocusCapturedByAnotherProcess::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _IsInputFocusCapturedByAnotherProcess_BeginInvoke_m3465614967 (_IsInputFocusCapturedByAnotherProcess_t84136089 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_IsInputFocusCapturedByAnotherProcess::EndInvoke(System.IAsyncResult)
extern "C"  bool _IsInputFocusCapturedByAnotherProcess_EndInvoke_m1299763196 (_IsInputFocusCapturedByAnotherProcess_t84136089 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
