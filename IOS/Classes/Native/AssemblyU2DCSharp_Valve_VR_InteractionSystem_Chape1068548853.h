﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SteamVR_Events/Event
struct Event_t1855872343;
// Valve.VR.InteractionSystem.ChaperoneInfo
struct ChaperoneInfo_t1068548853;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.ChaperoneInfo
struct  ChaperoneInfo_t1068548853  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Valve.VR.InteractionSystem.ChaperoneInfo::<initialized>k__BackingField
	bool ___U3CinitializedU3Ek__BackingField_2;
	// System.Single Valve.VR.InteractionSystem.ChaperoneInfo::<playAreaSizeX>k__BackingField
	float ___U3CplayAreaSizeXU3Ek__BackingField_3;
	// System.Single Valve.VR.InteractionSystem.ChaperoneInfo::<playAreaSizeZ>k__BackingField
	float ___U3CplayAreaSizeZU3Ek__BackingField_4;
	// System.Boolean Valve.VR.InteractionSystem.ChaperoneInfo::<roomscale>k__BackingField
	bool ___U3CroomscaleU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CinitializedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ChaperoneInfo_t1068548853, ___U3CinitializedU3Ek__BackingField_2)); }
	inline bool get_U3CinitializedU3Ek__BackingField_2() const { return ___U3CinitializedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CinitializedU3Ek__BackingField_2() { return &___U3CinitializedU3Ek__BackingField_2; }
	inline void set_U3CinitializedU3Ek__BackingField_2(bool value)
	{
		___U3CinitializedU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CplayAreaSizeXU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ChaperoneInfo_t1068548853, ___U3CplayAreaSizeXU3Ek__BackingField_3)); }
	inline float get_U3CplayAreaSizeXU3Ek__BackingField_3() const { return ___U3CplayAreaSizeXU3Ek__BackingField_3; }
	inline float* get_address_of_U3CplayAreaSizeXU3Ek__BackingField_3() { return &___U3CplayAreaSizeXU3Ek__BackingField_3; }
	inline void set_U3CplayAreaSizeXU3Ek__BackingField_3(float value)
	{
		___U3CplayAreaSizeXU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CplayAreaSizeZU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ChaperoneInfo_t1068548853, ___U3CplayAreaSizeZU3Ek__BackingField_4)); }
	inline float get_U3CplayAreaSizeZU3Ek__BackingField_4() const { return ___U3CplayAreaSizeZU3Ek__BackingField_4; }
	inline float* get_address_of_U3CplayAreaSizeZU3Ek__BackingField_4() { return &___U3CplayAreaSizeZU3Ek__BackingField_4; }
	inline void set_U3CplayAreaSizeZU3Ek__BackingField_4(float value)
	{
		___U3CplayAreaSizeZU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CroomscaleU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ChaperoneInfo_t1068548853, ___U3CroomscaleU3Ek__BackingField_5)); }
	inline bool get_U3CroomscaleU3Ek__BackingField_5() const { return ___U3CroomscaleU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CroomscaleU3Ek__BackingField_5() { return &___U3CroomscaleU3Ek__BackingField_5; }
	inline void set_U3CroomscaleU3Ek__BackingField_5(bool value)
	{
		___U3CroomscaleU3Ek__BackingField_5 = value;
	}
};

struct ChaperoneInfo_t1068548853_StaticFields
{
public:
	// SteamVR_Events/Event Valve.VR.InteractionSystem.ChaperoneInfo::Initialized
	Event_t1855872343 * ___Initialized_6;
	// Valve.VR.InteractionSystem.ChaperoneInfo Valve.VR.InteractionSystem.ChaperoneInfo::_instance
	ChaperoneInfo_t1068548853 * ____instance_7;

public:
	inline static int32_t get_offset_of_Initialized_6() { return static_cast<int32_t>(offsetof(ChaperoneInfo_t1068548853_StaticFields, ___Initialized_6)); }
	inline Event_t1855872343 * get_Initialized_6() const { return ___Initialized_6; }
	inline Event_t1855872343 ** get_address_of_Initialized_6() { return &___Initialized_6; }
	inline void set_Initialized_6(Event_t1855872343 * value)
	{
		___Initialized_6 = value;
		Il2CppCodeGenWriteBarrier(&___Initialized_6, value);
	}

	inline static int32_t get_offset_of__instance_7() { return static_cast<int32_t>(offsetof(ChaperoneInfo_t1068548853_StaticFields, ____instance_7)); }
	inline ChaperoneInfo_t1068548853 * get__instance_7() const { return ____instance_7; }
	inline ChaperoneInfo_t1068548853 ** get_address_of__instance_7() { return &____instance_7; }
	inline void set__instance_7(ChaperoneInfo_t1068548853 * value)
	{
		____instance_7 = value;
		Il2CppCodeGenWriteBarrier(&____instance_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
