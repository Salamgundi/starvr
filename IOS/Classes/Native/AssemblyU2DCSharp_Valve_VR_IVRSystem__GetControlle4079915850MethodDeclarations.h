﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetControllerStateWithPose
struct _GetControllerStateWithPose_t4079915850;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingUniverseOrigin1464400093.h"
#include "AssemblyU2DCSharp_Valve_VR_VRControllerState_t2504874220.h"
#include "AssemblyU2DCSharp_Valve_VR_TrackedDevicePose_t1668551120.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetControllerStateWithPose::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetControllerStateWithPose__ctor_m978017505 (_GetControllerStateWithPose_t4079915850 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_GetControllerStateWithPose::Invoke(Valve.VR.ETrackingUniverseOrigin,System.UInt32,Valve.VR.VRControllerState_t&,System.UInt32,Valve.VR.TrackedDevicePose_t&)
extern "C"  bool _GetControllerStateWithPose_Invoke_m3300024348 (_GetControllerStateWithPose_t4079915850 * __this, int32_t ___eOrigin0, uint32_t ___unControllerDeviceIndex1, VRControllerState_t_t2504874220 * ___pControllerState2, uint32_t ___unControllerStateSize3, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetControllerStateWithPose::BeginInvoke(Valve.VR.ETrackingUniverseOrigin,System.UInt32,Valve.VR.VRControllerState_t&,System.UInt32,Valve.VR.TrackedDevicePose_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetControllerStateWithPose_BeginInvoke_m1372453569 (_GetControllerStateWithPose_t4079915850 * __this, int32_t ___eOrigin0, uint32_t ___unControllerDeviceIndex1, VRControllerState_t_t2504874220 * ___pControllerState2, uint32_t ___unControllerStateSize3, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_GetControllerStateWithPose::EndInvoke(Valve.VR.VRControllerState_t&,Valve.VR.TrackedDevicePose_t&,System.IAsyncResult)
extern "C"  bool _GetControllerStateWithPose_EndInvoke_m451703135 (_GetControllerStateWithPose_t4079915850 * __this, VRControllerState_t_t2504874220 * ___pControllerState0, TrackedDevicePose_t_t1668551120 * ___pTrackedDevicePose1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
