﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSettings/_RemoveKeyInSection
struct _RemoveKeyInSection_t1836506429;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRSettingsError4124928198.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSettings/_RemoveKeyInSection::.ctor(System.Object,System.IntPtr)
extern "C"  void _RemoveKeyInSection__ctor_m3387857040 (_RemoveKeyInSection_t1836506429 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSettings/_RemoveKeyInSection::Invoke(System.String,System.String,Valve.VR.EVRSettingsError&)
extern "C"  void _RemoveKeyInSection_Invoke_m548050310 (_RemoveKeyInSection_t1836506429 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSettings/_RemoveKeyInSection::BeginInvoke(System.String,System.String,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _RemoveKeyInSection_BeginInvoke_m2025829649 (_RemoveKeyInSection_t1836506429 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSettings/_RemoveKeyInSection::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  void _RemoveKeyInSection_EndInvoke_m4281034706 (_RemoveKeyInSection_t1836506429 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
