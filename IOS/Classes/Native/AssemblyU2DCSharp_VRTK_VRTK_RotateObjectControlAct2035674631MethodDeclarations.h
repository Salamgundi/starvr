﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_RotateObjectControlAction
struct VRTK_RotateObjectControlAction_t2035674631;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_RotateObjectControlAction::.ctor()
extern "C"  void VRTK_RotateObjectControlAction__ctor_m2482059099 (VRTK_RotateObjectControlAction_t2035674631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RotateObjectControlAction::Process(UnityEngine.GameObject,UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Single,System.Boolean,System.Boolean)
extern "C"  void VRTK_RotateObjectControlAction_Process_m2075800846 (VRTK_RotateObjectControlAction_t2035674631 * __this, GameObject_t1756533147 * ___controlledGameObject0, Transform_t3275118058 * ___directionDevice1, Vector3_t2243707580  ___axisDirection2, float ___axis3, float ___deadzone4, bool ___currentlyFalling5, bool ___modifierActive6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_RotateObjectControlAction::Rotate(System.Single,System.Boolean)
extern "C"  float VRTK_RotateObjectControlAction_Rotate_m945671258 (VRTK_RotateObjectControlAction_t2035674631 * __this, float ___axis0, bool ___modifierActive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
