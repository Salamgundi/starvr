﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,VRTK.PlayerClimbEventArgs>
struct UnityEvent_2_t1220272354;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.PlayerClimbEventArgs>
struct UnityAction_2_t3633041732;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_VRTK_PlayerClimbEventArgs2537585745.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.PlayerClimbEventArgs>::.ctor()
extern "C"  void UnityEvent_2__ctor_m3740577308_gshared (UnityEvent_2_t1220272354 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m3740577308(__this, method) ((  void (*) (UnityEvent_2_t1220272354 *, const MethodInfo*))UnityEvent_2__ctor_m3740577308_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.PlayerClimbEventArgs>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m1586940098_gshared (UnityEvent_2_t1220272354 * __this, UnityAction_2_t3633041732 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m1586940098(__this, ___call0, method) ((  void (*) (UnityEvent_2_t1220272354 *, UnityAction_2_t3633041732 *, const MethodInfo*))UnityEvent_2_AddListener_m1586940098_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.PlayerClimbEventArgs>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m3308373143_gshared (UnityEvent_2_t1220272354 * __this, UnityAction_2_t3633041732 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m3308373143(__this, ___call0, method) ((  void (*) (UnityEvent_2_t1220272354 *, UnityAction_2_t3633041732 *, const MethodInfo*))UnityEvent_2_RemoveListener_m3308373143_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,VRTK.PlayerClimbEventArgs>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m4279617790_gshared (UnityEvent_2_t1220272354 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m4279617790(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t1220272354 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m4279617790_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.PlayerClimbEventArgs>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m4292890170_gshared (UnityEvent_2_t1220272354 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m4292890170(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t1220272354 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m4292890170_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.PlayerClimbEventArgs>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m660984939_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t3633041732 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m660984939(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t3633041732 *, const MethodInfo*))UnityEvent_2_GetDelegate_m660984939_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.PlayerClimbEventArgs>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m1178526181_gshared (UnityEvent_2_t1220272354 * __this, Il2CppObject * ___arg00, PlayerClimbEventArgs_t2537585745  ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m1178526181(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t1220272354 *, Il2CppObject *, PlayerClimbEventArgs_t2537585745 , const MethodInfo*))UnityEvent_2_Invoke_m1178526181_gshared)(__this, ___arg00, ___arg11, method)
