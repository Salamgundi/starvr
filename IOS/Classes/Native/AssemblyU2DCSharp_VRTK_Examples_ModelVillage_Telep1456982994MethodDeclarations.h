﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.ModelVillage_TeleportLocation
struct ModelVillage_TeleportLocation_t1456982994;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void VRTK.Examples.ModelVillage_TeleportLocation::.ctor()
extern "C"  void ModelVillage_TeleportLocation__ctor_m67035175 (ModelVillage_TeleportLocation_t1456982994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.ModelVillage_TeleportLocation::OnTriggerStay(UnityEngine.Collider)
extern "C"  void ModelVillage_TeleportLocation_OnTriggerStay_m2917973960 (ModelVillage_TeleportLocation_t1456982994 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
