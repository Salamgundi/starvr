﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRChaperoneSetup
struct CVRChaperoneSetup_t1611144107;
// Valve.VR.HmdQuad_t[]
struct HmdQuad_tU5BU5D_t16941492;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EChaperoneConfigFile30305944.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdQuad_t2172573705.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Valve.VR.CVRChaperoneSetup::.ctor(System.IntPtr)
extern "C"  void CVRChaperoneSetup__ctor_m756616412 (CVRChaperoneSetup_t1611144107 * __this, IntPtr_t ___pInterface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperoneSetup::CommitWorkingCopy(Valve.VR.EChaperoneConfigFile)
extern "C"  bool CVRChaperoneSetup_CommitWorkingCopy_m3024062631 (CVRChaperoneSetup_t1611144107 * __this, int32_t ___configFile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRChaperoneSetup::RevertWorkingCopy()
extern "C"  void CVRChaperoneSetup_RevertWorkingCopy_m1541375806 (CVRChaperoneSetup_t1611144107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperoneSetup::GetWorkingPlayAreaSize(System.Single&,System.Single&)
extern "C"  bool CVRChaperoneSetup_GetWorkingPlayAreaSize_m3680586495 (CVRChaperoneSetup_t1611144107 * __this, float* ___pSizeX0, float* ___pSizeZ1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperoneSetup::GetWorkingPlayAreaRect(Valve.VR.HmdQuad_t&)
extern "C"  bool CVRChaperoneSetup_GetWorkingPlayAreaRect_m1209388059 (CVRChaperoneSetup_t1611144107 * __this, HmdQuad_t_t2172573705 * ___rect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperoneSetup::GetWorkingCollisionBoundsInfo(Valve.VR.HmdQuad_t[]&)
extern "C"  bool CVRChaperoneSetup_GetWorkingCollisionBoundsInfo_m4242526467 (CVRChaperoneSetup_t1611144107 * __this, HmdQuad_tU5BU5D_t16941492** ___pQuadsBuffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperoneSetup::GetLiveCollisionBoundsInfo(Valve.VR.HmdQuad_t[]&)
extern "C"  bool CVRChaperoneSetup_GetLiveCollisionBoundsInfo_m1725914196 (CVRChaperoneSetup_t1611144107 * __this, HmdQuad_tU5BU5D_t16941492** ___pQuadsBuffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperoneSetup::GetWorkingSeatedZeroPoseToRawTrackingPose(Valve.VR.HmdMatrix34_t&)
extern "C"  bool CVRChaperoneSetup_GetWorkingSeatedZeroPoseToRawTrackingPose_m3132160673 (CVRChaperoneSetup_t1611144107 * __this, HmdMatrix34_t_t664273062 * ___pmatSeatedZeroPoseToRawTrackingPose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperoneSetup::GetWorkingStandingZeroPoseToRawTrackingPose(Valve.VR.HmdMatrix34_t&)
extern "C"  bool CVRChaperoneSetup_GetWorkingStandingZeroPoseToRawTrackingPose_m2036141739 (CVRChaperoneSetup_t1611144107 * __this, HmdMatrix34_t_t664273062 * ___pmatStandingZeroPoseToRawTrackingPose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRChaperoneSetup::SetWorkingPlayAreaSize(System.Single,System.Single)
extern "C"  void CVRChaperoneSetup_SetWorkingPlayAreaSize_m1809337967 (CVRChaperoneSetup_t1611144107 * __this, float ___sizeX0, float ___sizeZ1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRChaperoneSetup::SetWorkingCollisionBoundsInfo(Valve.VR.HmdQuad_t[])
extern "C"  void CVRChaperoneSetup_SetWorkingCollisionBoundsInfo_m3638169265 (CVRChaperoneSetup_t1611144107 * __this, HmdQuad_tU5BU5D_t16941492* ___pQuadsBuffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRChaperoneSetup::SetWorkingSeatedZeroPoseToRawTrackingPose(Valve.VR.HmdMatrix34_t&)
extern "C"  void CVRChaperoneSetup_SetWorkingSeatedZeroPoseToRawTrackingPose_m2492594649 (CVRChaperoneSetup_t1611144107 * __this, HmdMatrix34_t_t664273062 * ___pMatSeatedZeroPoseToRawTrackingPose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRChaperoneSetup::SetWorkingStandingZeroPoseToRawTrackingPose(Valve.VR.HmdMatrix34_t&)
extern "C"  void CVRChaperoneSetup_SetWorkingStandingZeroPoseToRawTrackingPose_m3236434015 (CVRChaperoneSetup_t1611144107 * __this, HmdMatrix34_t_t664273062 * ___pMatStandingZeroPoseToRawTrackingPose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRChaperoneSetup::ReloadFromDisk(Valve.VR.EChaperoneConfigFile)
extern "C"  void CVRChaperoneSetup_ReloadFromDisk_m2921883658 (CVRChaperoneSetup_t1611144107 * __this, int32_t ___configFile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperoneSetup::GetLiveSeatedZeroPoseToRawTrackingPose(Valve.VR.HmdMatrix34_t&)
extern "C"  bool CVRChaperoneSetup_GetLiveSeatedZeroPoseToRawTrackingPose_m2729372986 (CVRChaperoneSetup_t1611144107 * __this, HmdMatrix34_t_t664273062 * ___pmatSeatedZeroPoseToRawTrackingPose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.CVRChaperoneSetup::SetWorkingCollisionBoundsTagsInfo(System.Byte[])
extern "C"  void CVRChaperoneSetup_SetWorkingCollisionBoundsTagsInfo_m2477193786 (CVRChaperoneSetup_t1611144107 * __this, ByteU5BU5D_t3397334013* ___pTagsBuffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperoneSetup::GetLiveCollisionBoundsTagsInfo(System.Byte[]&)
extern "C"  bool CVRChaperoneSetup_GetLiveCollisionBoundsTagsInfo_m584809799 (CVRChaperoneSetup_t1611144107 * __this, ByteU5BU5D_t3397334013** ___pTagsBuffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperoneSetup::SetWorkingPhysicalBoundsInfo(Valve.VR.HmdQuad_t[])
extern "C"  bool CVRChaperoneSetup_SetWorkingPhysicalBoundsInfo_m1501433884 (CVRChaperoneSetup_t1611144107 * __this, HmdQuad_tU5BU5D_t16941492* ___pQuadsBuffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperoneSetup::GetLivePhysicalBoundsInfo(Valve.VR.HmdQuad_t[]&)
extern "C"  bool CVRChaperoneSetup_GetLivePhysicalBoundsInfo_m1174534947 (CVRChaperoneSetup_t1611144107 * __this, HmdQuad_tU5BU5D_t16941492** ___pQuadsBuffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperoneSetup::ExportLiveToBuffer(System.Text.StringBuilder,System.UInt32&)
extern "C"  bool CVRChaperoneSetup_ExportLiveToBuffer_m2135194513 (CVRChaperoneSetup_t1611144107 * __this, StringBuilder_t1221177846 * ___pBuffer0, uint32_t* ___pnBufferLength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.CVRChaperoneSetup::ImportFromBufferToWorking(System.String,System.UInt32)
extern "C"  bool CVRChaperoneSetup_ImportFromBufferToWorking_m318456181 (CVRChaperoneSetup_t1611144107 * __this, String_t* ___pBuffer0, uint32_t ___nImportFlags1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
