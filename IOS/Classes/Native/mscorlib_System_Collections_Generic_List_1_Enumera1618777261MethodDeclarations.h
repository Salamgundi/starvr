﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_UIPointer>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m525660337(__this, ___l0, method) ((  void (*) (Enumerator_t1618777261 *, List_1_t2084047587 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_UIPointer>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2081461633(__this, method) ((  void (*) (Enumerator_t1618777261 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_UIPointer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1588057545(__this, method) ((  Il2CppObject * (*) (Enumerator_t1618777261 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_UIPointer>::Dispose()
#define Enumerator_Dispose_m2288739124(__this, method) ((  void (*) (Enumerator_t1618777261 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_UIPointer>::VerifyState()
#define Enumerator_VerifyState_m3910561883(__this, method) ((  void (*) (Enumerator_t1618777261 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_UIPointer>::MoveNext()
#define Enumerator_MoveNext_m1093170033(__this, method) ((  bool (*) (Enumerator_t1618777261 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_UIPointer>::get_Current()
#define Enumerator_get_Current_m844663310(__this, method) ((  VRTK_UIPointer_t2714926455 * (*) (Enumerator_t1618777261 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
