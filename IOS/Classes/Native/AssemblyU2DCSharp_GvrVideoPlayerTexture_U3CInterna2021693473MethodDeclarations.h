﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey3
struct U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey3::.ctor()
extern "C"  void U3CInternalOnExceptionCallbackU3Ec__AnonStorey3__ctor_m1780225876 (U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey3::<>m__0()
extern "C"  void U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_U3CU3Em__0_m480643675 (U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
