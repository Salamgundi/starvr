﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetTimeSinceLastVsync
struct _GetTimeSinceLastVsync_t1215702688;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetTimeSinceLastVsync::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetTimeSinceLastVsync__ctor_m514120617 (_GetTimeSinceLastVsync_t1215702688 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_GetTimeSinceLastVsync::Invoke(System.Single&,System.UInt64&)
extern "C"  bool _GetTimeSinceLastVsync_Invoke_m149520711 (_GetTimeSinceLastVsync_t1215702688 * __this, float* ___pfSecondsSinceLastVsync0, uint64_t* ___pulFrameCounter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetTimeSinceLastVsync::BeginInvoke(System.Single&,System.UInt64&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetTimeSinceLastVsync_BeginInvoke_m2443950666 (_GetTimeSinceLastVsync_t1215702688 * __this, float* ___pfSecondsSinceLastVsync0, uint64_t* ___pulFrameCounter1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_GetTimeSinceLastVsync::EndInvoke(System.Single&,System.UInt64&,System.IAsyncResult)
extern "C"  bool _GetTimeSinceLastVsync_EndInvoke_m3244267461 (_GetTimeSinceLastVsync_t1215702688 * __this, float* ___pfSecondsSinceLastVsync0, uint64_t* ___pulFrameCounter1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
