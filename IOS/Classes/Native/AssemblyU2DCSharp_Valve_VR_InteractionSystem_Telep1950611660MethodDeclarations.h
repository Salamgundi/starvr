﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.TeleportArea
struct TeleportArea_t1950611660;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void Valve.VR.InteractionSystem.TeleportArea::.ctor()
extern "C"  void TeleportArea__ctor_m3532932040 (TeleportArea_t1950611660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Valve.VR.InteractionSystem.TeleportArea::get_meshBounds()
extern "C"  Bounds_t3033363703  TeleportArea_get_meshBounds_m1105906672 (TeleportArea_t1950611660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArea::set_meshBounds(UnityEngine.Bounds)
extern "C"  void TeleportArea_set_meshBounds_m3661869601 (TeleportArea_t1950611660 * __this, Bounds_t3033363703  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArea::Awake()
extern "C"  void TeleportArea_Awake_m4037591569 (TeleportArea_t1950611660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArea::Start()
extern "C"  void TeleportArea_Start_m1481705448 (TeleportArea_t1950611660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.TeleportArea::ShouldActivate(UnityEngine.Vector3)
extern "C"  bool TeleportArea_ShouldActivate_m4233603533 (TeleportArea_t1950611660 * __this, Vector3_t2243707580  ___playerPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.TeleportArea::ShouldMovePlayer()
extern "C"  bool TeleportArea_ShouldMovePlayer_m2159823619 (TeleportArea_t1950611660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArea::Highlight(System.Boolean)
extern "C"  void TeleportArea_Highlight_m191971283 (TeleportArea_t1950611660 * __this, bool ___highlight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArea::SetAlpha(System.Single,System.Single)
extern "C"  void TeleportArea_SetAlpha_m1544863464 (TeleportArea_t1950611660 * __this, float ___tintAlpha0, float ___alphaPercent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArea::UpdateVisuals()
extern "C"  void TeleportArea_UpdateVisuals_m1518746062 (TeleportArea_t1950611660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.TeleportArea::UpdateVisualsInEditor()
extern "C"  void TeleportArea_UpdateVisualsInEditor_m2818421272 (TeleportArea_t1950611660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.TeleportArea::CalculateBounds()
extern "C"  bool TeleportArea_CalculateBounds_m4032006829 (TeleportArea_t1950611660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Valve.VR.InteractionSystem.TeleportArea::GetTintColor()
extern "C"  Color_t2020392075  TeleportArea_GetTintColor_m2283979869 (TeleportArea_t1950611660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
