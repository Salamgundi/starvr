﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.BalloonSpawner
struct BalloonSpawner_t4219926993;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Ballo1350152013.h"

// System.Void Valve.VR.InteractionSystem.BalloonSpawner::.ctor()
extern "C"  void BalloonSpawner__ctor_m1669642743 (BalloonSpawner_t4219926993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.BalloonSpawner::Start()
extern "C"  void BalloonSpawner_Start_m2567455531 (BalloonSpawner_t4219926993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.BalloonSpawner::Update()
extern "C"  void BalloonSpawner_Update_m3039965204 (BalloonSpawner_t4219926993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Valve.VR.InteractionSystem.BalloonSpawner::SpawnBalloon(Valve.VR.InteractionSystem.Balloon/BalloonColor)
extern "C"  GameObject_t1756533147 * BalloonSpawner_SpawnBalloon_m2140961661 (BalloonSpawner_t4219926993 * __this, int32_t ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.BalloonSpawner::SpawnBalloonFromEvent(System.Int32)
extern "C"  void BalloonSpawner_SpawnBalloonFromEvent_m2309959626 (BalloonSpawner_t4219926993 * __this, int32_t ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
