﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_SimBoundaries
struct SDK_SimBoundaries_t3645911520;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_SimBoundaries::.ctor()
extern "C"  void SDK_SimBoundaries__ctor_m2913356376 (SDK_SimBoundaries_t3645911520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
