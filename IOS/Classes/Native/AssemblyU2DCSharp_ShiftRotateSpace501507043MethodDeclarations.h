﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShiftRotateSpace
struct ShiftRotateSpace_t501507043;
// SteamVR_Controller/Device
struct Device_t2885069456;

#include "codegen/il2cpp-codegen.h"

// System.Void ShiftRotateSpace::.ctor()
extern "C"  void ShiftRotateSpace__ctor_m2286191450 (ShiftRotateSpace_t501507043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Controller/Device ShiftRotateSpace::get_Controller()
extern "C"  Device_t2885069456 * ShiftRotateSpace_get_Controller_m1819332760 (ShiftRotateSpace_t501507043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShiftRotateSpace::Awake()
extern "C"  void ShiftRotateSpace_Awake_m396537919 (ShiftRotateSpace_t501507043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShiftRotateSpace::Start()
extern "C"  void ShiftRotateSpace_Start_m1531069262 (ShiftRotateSpace_t501507043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShiftRotateSpace::Update()
extern "C"  void ShiftRotateSpace_Update_m1528955083 (ShiftRotateSpace_t501507043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
