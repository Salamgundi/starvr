﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_WaitGetPoses
struct _WaitGetPoses_t4192584901;
// System.Object
struct Il2CppObject;
// Valve.VR.TrackedDevicePose_t[]
struct TrackedDevicePose_tU5BU5D_t2897272049;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRCompositorError3948578210.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_WaitGetPoses::.ctor(System.Object,System.IntPtr)
extern "C"  void _WaitGetPoses__ctor_m1820459628 (_WaitGetPoses_t4192584901 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.IVRCompositor/_WaitGetPoses::Invoke(Valve.VR.TrackedDevicePose_t[],System.UInt32,Valve.VR.TrackedDevicePose_t[],System.UInt32)
extern "C"  int32_t _WaitGetPoses_Invoke_m1847857363 (_WaitGetPoses_t4192584901 * __this, TrackedDevicePose_tU5BU5D_t2897272049* ___pRenderPoseArray0, uint32_t ___unRenderPoseArrayCount1, TrackedDevicePose_tU5BU5D_t2897272049* ___pGamePoseArray2, uint32_t ___unGamePoseArrayCount3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_WaitGetPoses::BeginInvoke(Valve.VR.TrackedDevicePose_t[],System.UInt32,Valve.VR.TrackedDevicePose_t[],System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _WaitGetPoses_BeginInvoke_m2583823683 (_WaitGetPoses_t4192584901 * __this, TrackedDevicePose_tU5BU5D_t2897272049* ___pRenderPoseArray0, uint32_t ___unRenderPoseArrayCount1, TrackedDevicePose_tU5BU5D_t2897272049* ___pGamePoseArray2, uint32_t ___unGamePoseArrayCount3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.IVRCompositor/_WaitGetPoses::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _WaitGetPoses_EndInvoke_m3910670969 (_WaitGetPoses_t4192584901 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
