﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animation
struct Animation_t2068071072;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Archery.BowAnimation
struct  BowAnimation_t4022033600  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Animation VRTK.Examples.Archery.BowAnimation::animationTimeline
	Animation_t2068071072 * ___animationTimeline_2;

public:
	inline static int32_t get_offset_of_animationTimeline_2() { return static_cast<int32_t>(offsetof(BowAnimation_t4022033600, ___animationTimeline_2)); }
	inline Animation_t2068071072 * get_animationTimeline_2() const { return ___animationTimeline_2; }
	inline Animation_t2068071072 ** get_address_of_animationTimeline_2() { return &___animationTimeline_2; }
	inline void set_animationTimeline_2(Animation_t2068071072 * value)
	{
		___animationTimeline_2 = value;
		Il2CppCodeGenWriteBarrier(&___animationTimeline_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
