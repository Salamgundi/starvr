﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveBlock
struct  MoveBlock_t1905298658  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MoveBlock::moveYAmount
	float ___moveYAmount_2;
	// System.Single MoveBlock::moveSpeed
	float ___moveSpeed_3;
	// System.Single MoveBlock::waitTime
	float ___waitTime_4;
	// System.Single MoveBlock::rotateSpeed
	float ___rotateSpeed_5;
	// System.Single MoveBlock::startY
	float ___startY_6;
	// System.Boolean MoveBlock::goingUp
	bool ___goingUp_7;
	// System.Single MoveBlock::stoppedUntilTime
	float ___stoppedUntilTime_8;
	// System.Single MoveBlock::moveUpAmount
	float ___moveUpAmount_9;

public:
	inline static int32_t get_offset_of_moveYAmount_2() { return static_cast<int32_t>(offsetof(MoveBlock_t1905298658, ___moveYAmount_2)); }
	inline float get_moveYAmount_2() const { return ___moveYAmount_2; }
	inline float* get_address_of_moveYAmount_2() { return &___moveYAmount_2; }
	inline void set_moveYAmount_2(float value)
	{
		___moveYAmount_2 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_3() { return static_cast<int32_t>(offsetof(MoveBlock_t1905298658, ___moveSpeed_3)); }
	inline float get_moveSpeed_3() const { return ___moveSpeed_3; }
	inline float* get_address_of_moveSpeed_3() { return &___moveSpeed_3; }
	inline void set_moveSpeed_3(float value)
	{
		___moveSpeed_3 = value;
	}

	inline static int32_t get_offset_of_waitTime_4() { return static_cast<int32_t>(offsetof(MoveBlock_t1905298658, ___waitTime_4)); }
	inline float get_waitTime_4() const { return ___waitTime_4; }
	inline float* get_address_of_waitTime_4() { return &___waitTime_4; }
	inline void set_waitTime_4(float value)
	{
		___waitTime_4 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_5() { return static_cast<int32_t>(offsetof(MoveBlock_t1905298658, ___rotateSpeed_5)); }
	inline float get_rotateSpeed_5() const { return ___rotateSpeed_5; }
	inline float* get_address_of_rotateSpeed_5() { return &___rotateSpeed_5; }
	inline void set_rotateSpeed_5(float value)
	{
		___rotateSpeed_5 = value;
	}

	inline static int32_t get_offset_of_startY_6() { return static_cast<int32_t>(offsetof(MoveBlock_t1905298658, ___startY_6)); }
	inline float get_startY_6() const { return ___startY_6; }
	inline float* get_address_of_startY_6() { return &___startY_6; }
	inline void set_startY_6(float value)
	{
		___startY_6 = value;
	}

	inline static int32_t get_offset_of_goingUp_7() { return static_cast<int32_t>(offsetof(MoveBlock_t1905298658, ___goingUp_7)); }
	inline bool get_goingUp_7() const { return ___goingUp_7; }
	inline bool* get_address_of_goingUp_7() { return &___goingUp_7; }
	inline void set_goingUp_7(bool value)
	{
		___goingUp_7 = value;
	}

	inline static int32_t get_offset_of_stoppedUntilTime_8() { return static_cast<int32_t>(offsetof(MoveBlock_t1905298658, ___stoppedUntilTime_8)); }
	inline float get_stoppedUntilTime_8() const { return ___stoppedUntilTime_8; }
	inline float* get_address_of_stoppedUntilTime_8() { return &___stoppedUntilTime_8; }
	inline void set_stoppedUntilTime_8(float value)
	{
		___stoppedUntilTime_8 = value;
	}

	inline static int32_t get_offset_of_moveUpAmount_9() { return static_cast<int32_t>(offsetof(MoveBlock_t1905298658, ___moveUpAmount_9)); }
	inline float get_moveUpAmount_9() const { return ___moveUpAmount_9; }
	inline float* get_address_of_moveUpAmount_9() { return &___moveUpAmount_9; }
	inline void set_moveUpAmount_9(float value)
	{
		___moveUpAmount_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
