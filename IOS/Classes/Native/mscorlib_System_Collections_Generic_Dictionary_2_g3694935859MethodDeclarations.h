﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>
struct Dictionary_2_t3694935859;
// System.Collections.Generic.IEqualityComparer`1<Valve.VR.EVREventType>
struct IEqualityComparer_1_t3514446949;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>[]
struct KeyValuePair_2U5BU5D_t479898372;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>>
struct IEnumerator_1_t3222772204;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>
struct ValueCollection_t2397995702;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21452281081.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREventType6846875.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En719993265.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3209577220_gshared (Dictionary_2_t3694935859 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3209577220(__this, method) ((  void (*) (Dictionary_2_t3694935859 *, const MethodInfo*))Dictionary_2__ctor_m3209577220_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3700101715_gshared (Dictionary_2_t3694935859 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3700101715(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3694935859 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3700101715_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m586060425_gshared (Dictionary_2_t3694935859 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m586060425(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3694935859 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m586060425_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2508538971_gshared (Dictionary_2_t3694935859 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2508538971(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3694935859 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m2508538971_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m426400252_gshared (Dictionary_2_t3694935859 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m426400252(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3694935859 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m426400252_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3721125571_gshared (Dictionary_2_t3694935859 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3721125571(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3694935859 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3721125571_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3813929522_gshared (Dictionary_2_t3694935859 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3813929522(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3694935859 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3813929522_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m589886775_gshared (Dictionary_2_t3694935859 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m589886775(__this, ___key0, method) ((  void (*) (Dictionary_2_t3694935859 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m589886775_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1354451932_gshared (Dictionary_2_t3694935859 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1354451932(__this, method) ((  bool (*) (Dictionary_2_t3694935859 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1354451932_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1152107276_gshared (Dictionary_2_t3694935859 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1152107276(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3694935859 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1152107276_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2884791962_gshared (Dictionary_2_t3694935859 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2884791962(__this, method) ((  bool (*) (Dictionary_2_t3694935859 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2884791962_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1963331899_gshared (Dictionary_2_t3694935859 * __this, KeyValuePair_2_t1452281081  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1963331899(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3694935859 *, KeyValuePair_2_t1452281081 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1963331899_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3485341149_gshared (Dictionary_2_t3694935859 * __this, KeyValuePair_2_t1452281081  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3485341149(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3694935859 *, KeyValuePair_2_t1452281081 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3485341149_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2101635143_gshared (Dictionary_2_t3694935859 * __this, KeyValuePair_2U5BU5D_t479898372* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2101635143(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3694935859 *, KeyValuePair_2U5BU5D_t479898372*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2101635143_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2804976272_gshared (Dictionary_2_t3694935859 * __this, KeyValuePair_2_t1452281081  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2804976272(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3694935859 *, KeyValuePair_2_t1452281081 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2804976272_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m744521660_gshared (Dictionary_2_t3694935859 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m744521660(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3694935859 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m744521660_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1175468817_gshared (Dictionary_2_t3694935859 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1175468817(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3694935859 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1175468817_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m801427490_gshared (Dictionary_2_t3694935859 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m801427490(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3694935859 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m801427490_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m793621695_gshared (Dictionary_2_t3694935859 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m793621695(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3694935859 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m793621695_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1728514812_gshared (Dictionary_2_t3694935859 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1728514812(__this, method) ((  int32_t (*) (Dictionary_2_t3694935859 *, const MethodInfo*))Dictionary_2_get_Count_m1728514812_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m666136007_gshared (Dictionary_2_t3694935859 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m666136007(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3694935859 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m666136007_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1384242606_gshared (Dictionary_2_t3694935859 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1384242606(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3694935859 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m1384242606_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3308873456_gshared (Dictionary_2_t3694935859 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3308873456(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3694935859 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3308873456_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3932968177_gshared (Dictionary_2_t3694935859 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3932968177(__this, ___size0, method) ((  void (*) (Dictionary_2_t3694935859 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3932968177_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3731155663_gshared (Dictionary_2_t3694935859 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3731155663(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3694935859 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3731155663_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1452281081  Dictionary_2_make_pair_m4282695529_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m4282695529(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1452281081  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m4282695529_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3627468737_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3627468737(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m3627468737_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3649852218_gshared (Dictionary_2_t3694935859 * __this, KeyValuePair_2U5BU5D_t479898372* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3649852218(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3694935859 *, KeyValuePair_2U5BU5D_t479898372*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3649852218_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m856576922_gshared (Dictionary_2_t3694935859 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m856576922(__this, method) ((  void (*) (Dictionary_2_t3694935859 *, const MethodInfo*))Dictionary_2_Resize_m856576922_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1729967029_gshared (Dictionary_2_t3694935859 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1729967029(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3694935859 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m1729967029_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m1331289377_gshared (Dictionary_2_t3694935859 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1331289377(__this, method) ((  void (*) (Dictionary_2_t3694935859 *, const MethodInfo*))Dictionary_2_Clear_m1331289377_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3448906111_gshared (Dictionary_2_t3694935859 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3448906111(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3694935859 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m3448906111_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m635254207_gshared (Dictionary_2_t3694935859 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m635254207(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3694935859 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m635254207_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2889147944_gshared (Dictionary_2_t3694935859 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2889147944(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3694935859 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2889147944_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2526577438_gshared (Dictionary_2_t3694935859 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2526577438(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3694935859 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2526577438_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m285380869_gshared (Dictionary_2_t3694935859 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m285380869(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3694935859 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m285380869_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2179097578_gshared (Dictionary_2_t3694935859 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2179097578(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3694935859 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m2179097578_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::get_Values()
extern "C"  ValueCollection_t2397995702 * Dictionary_2_get_Values_m907854475_gshared (Dictionary_2_t3694935859 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m907854475(__this, method) ((  ValueCollection_t2397995702 * (*) (Dictionary_2_t3694935859 *, const MethodInfo*))Dictionary_2_get_Values_m907854475_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m2894495408_gshared (Dictionary_2_t3694935859 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2894495408(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3694935859 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2894495408_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1644330088_gshared (Dictionary_2_t3694935859 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1644330088(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t3694935859 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1644330088_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2471966754_gshared (Dictionary_2_t3694935859 * __this, KeyValuePair_2_t1452281081  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2471966754(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3694935859 *, KeyValuePair_2_t1452281081 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2471966754_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t719993265  Dictionary_2_GetEnumerator_m3724962557_gshared (Dictionary_2_t3694935859 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m3724962557(__this, method) ((  Enumerator_t719993265  (*) (Dictionary_2_t3694935859 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3724962557_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m1192149872_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1192149872(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1192149872_gshared)(__this /* static, unused */, ___key0, ___value1, method)
