﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRNotifications
struct IVRNotifications_t3935579733;
struct IVRNotifications_t3935579733_marshaled_pinvoke;
struct IVRNotifications_t3935579733_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IVRNotifications_t3935579733;
struct IVRNotifications_t3935579733_marshaled_pinvoke;

extern "C" void IVRNotifications_t3935579733_marshal_pinvoke(const IVRNotifications_t3935579733& unmarshaled, IVRNotifications_t3935579733_marshaled_pinvoke& marshaled);
extern "C" void IVRNotifications_t3935579733_marshal_pinvoke_back(const IVRNotifications_t3935579733_marshaled_pinvoke& marshaled, IVRNotifications_t3935579733& unmarshaled);
extern "C" void IVRNotifications_t3935579733_marshal_pinvoke_cleanup(IVRNotifications_t3935579733_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IVRNotifications_t3935579733;
struct IVRNotifications_t3935579733_marshaled_com;

extern "C" void IVRNotifications_t3935579733_marshal_com(const IVRNotifications_t3935579733& unmarshaled, IVRNotifications_t3935579733_marshaled_com& marshaled);
extern "C" void IVRNotifications_t3935579733_marshal_com_back(const IVRNotifications_t3935579733_marshaled_com& marshaled, IVRNotifications_t3935579733& unmarshaled);
extern "C" void IVRNotifications_t3935579733_marshal_com_cleanup(IVRNotifications_t3935579733_marshaled_com& marshaled);
