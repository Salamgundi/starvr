﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t2200943093;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents
struct  VRTK_ControllerEvents_UnityEvents_t1877972230  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_ControllerEvents VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::ce
	VRTK_ControllerEvents_t3225224819 * ___ce_2;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnTriggerPressed
	UnityObjectEvent_t2200943093 * ___OnTriggerPressed_3;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnTriggerReleased
	UnityObjectEvent_t2200943093 * ___OnTriggerReleased_4;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnTriggerTouchStart
	UnityObjectEvent_t2200943093 * ___OnTriggerTouchStart_5;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnTriggerTouchEnd
	UnityObjectEvent_t2200943093 * ___OnTriggerTouchEnd_6;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnTriggerHairlineStart
	UnityObjectEvent_t2200943093 * ___OnTriggerHairlineStart_7;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnTriggerHairlineEnd
	UnityObjectEvent_t2200943093 * ___OnTriggerHairlineEnd_8;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnTriggerClicked
	UnityObjectEvent_t2200943093 * ___OnTriggerClicked_9;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnTriggerUnclicked
	UnityObjectEvent_t2200943093 * ___OnTriggerUnclicked_10;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnTriggerAxisChanged
	UnityObjectEvent_t2200943093 * ___OnTriggerAxisChanged_11;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnGripPressed
	UnityObjectEvent_t2200943093 * ___OnGripPressed_12;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnGripReleased
	UnityObjectEvent_t2200943093 * ___OnGripReleased_13;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnGripTouchStart
	UnityObjectEvent_t2200943093 * ___OnGripTouchStart_14;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnGripTouchEnd
	UnityObjectEvent_t2200943093 * ___OnGripTouchEnd_15;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnGripHairlineStart
	UnityObjectEvent_t2200943093 * ___OnGripHairlineStart_16;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnGripHairlineEnd
	UnityObjectEvent_t2200943093 * ___OnGripHairlineEnd_17;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnGripClicked
	UnityObjectEvent_t2200943093 * ___OnGripClicked_18;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnGripUnclicked
	UnityObjectEvent_t2200943093 * ___OnGripUnclicked_19;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnGripAxisChanged
	UnityObjectEvent_t2200943093 * ___OnGripAxisChanged_20;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnTouchpadPressed
	UnityObjectEvent_t2200943093 * ___OnTouchpadPressed_21;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnTouchpadReleased
	UnityObjectEvent_t2200943093 * ___OnTouchpadReleased_22;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnTouchpadTouchStart
	UnityObjectEvent_t2200943093 * ___OnTouchpadTouchStart_23;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnTouchpadTouchEnd
	UnityObjectEvent_t2200943093 * ___OnTouchpadTouchEnd_24;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnTouchpadAxisChanged
	UnityObjectEvent_t2200943093 * ___OnTouchpadAxisChanged_25;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnButtonOnePressed
	UnityObjectEvent_t2200943093 * ___OnButtonOnePressed_26;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnButtonOneReleased
	UnityObjectEvent_t2200943093 * ___OnButtonOneReleased_27;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnButtonOneTouchStart
	UnityObjectEvent_t2200943093 * ___OnButtonOneTouchStart_28;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnButtonOneTouchEnd
	UnityObjectEvent_t2200943093 * ___OnButtonOneTouchEnd_29;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnButtonTwoPressed
	UnityObjectEvent_t2200943093 * ___OnButtonTwoPressed_30;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnButtonTwoReleased
	UnityObjectEvent_t2200943093 * ___OnButtonTwoReleased_31;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnButtonTwoTouchStart
	UnityObjectEvent_t2200943093 * ___OnButtonTwoTouchStart_32;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnButtonTwoTouchEnd
	UnityObjectEvent_t2200943093 * ___OnButtonTwoTouchEnd_33;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnStartMenuPressed
	UnityObjectEvent_t2200943093 * ___OnStartMenuPressed_34;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnStartMenuReleased
	UnityObjectEvent_t2200943093 * ___OnStartMenuReleased_35;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnAliasPointerOn
	UnityObjectEvent_t2200943093 * ___OnAliasPointerOn_36;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnAliasPointerOff
	UnityObjectEvent_t2200943093 * ___OnAliasPointerOff_37;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnAliasPointerSet
	UnityObjectEvent_t2200943093 * ___OnAliasPointerSet_38;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnAliasGrabOn
	UnityObjectEvent_t2200943093 * ___OnAliasGrabOn_39;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnAliasGrabOff
	UnityObjectEvent_t2200943093 * ___OnAliasGrabOff_40;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnAliasUseOn
	UnityObjectEvent_t2200943093 * ___OnAliasUseOn_41;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnAliasUseOff
	UnityObjectEvent_t2200943093 * ___OnAliasUseOff_42;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnAliasUIClickOn
	UnityObjectEvent_t2200943093 * ___OnAliasUIClickOn_43;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnAliasUIClickOff
	UnityObjectEvent_t2200943093 * ___OnAliasUIClickOff_44;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnAliasMenuOn
	UnityObjectEvent_t2200943093 * ___OnAliasMenuOn_45;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnAliasMenuOff
	UnityObjectEvent_t2200943093 * ___OnAliasMenuOff_46;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnControllerEnabled
	UnityObjectEvent_t2200943093 * ___OnControllerEnabled_47;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnControllerDisabled
	UnityObjectEvent_t2200943093 * ___OnControllerDisabled_48;
	// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnControllerIndexChanged
	UnityObjectEvent_t2200943093 * ___OnControllerIndexChanged_49;

public:
	inline static int32_t get_offset_of_ce_2() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___ce_2)); }
	inline VRTK_ControllerEvents_t3225224819 * get_ce_2() const { return ___ce_2; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_ce_2() { return &___ce_2; }
	inline void set_ce_2(VRTK_ControllerEvents_t3225224819 * value)
	{
		___ce_2 = value;
		Il2CppCodeGenWriteBarrier(&___ce_2, value);
	}

	inline static int32_t get_offset_of_OnTriggerPressed_3() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnTriggerPressed_3)); }
	inline UnityObjectEvent_t2200943093 * get_OnTriggerPressed_3() const { return ___OnTriggerPressed_3; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnTriggerPressed_3() { return &___OnTriggerPressed_3; }
	inline void set_OnTriggerPressed_3(UnityObjectEvent_t2200943093 * value)
	{
		___OnTriggerPressed_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnTriggerPressed_3, value);
	}

	inline static int32_t get_offset_of_OnTriggerReleased_4() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnTriggerReleased_4)); }
	inline UnityObjectEvent_t2200943093 * get_OnTriggerReleased_4() const { return ___OnTriggerReleased_4; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnTriggerReleased_4() { return &___OnTriggerReleased_4; }
	inline void set_OnTriggerReleased_4(UnityObjectEvent_t2200943093 * value)
	{
		___OnTriggerReleased_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnTriggerReleased_4, value);
	}

	inline static int32_t get_offset_of_OnTriggerTouchStart_5() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnTriggerTouchStart_5)); }
	inline UnityObjectEvent_t2200943093 * get_OnTriggerTouchStart_5() const { return ___OnTriggerTouchStart_5; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnTriggerTouchStart_5() { return &___OnTriggerTouchStart_5; }
	inline void set_OnTriggerTouchStart_5(UnityObjectEvent_t2200943093 * value)
	{
		___OnTriggerTouchStart_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnTriggerTouchStart_5, value);
	}

	inline static int32_t get_offset_of_OnTriggerTouchEnd_6() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnTriggerTouchEnd_6)); }
	inline UnityObjectEvent_t2200943093 * get_OnTriggerTouchEnd_6() const { return ___OnTriggerTouchEnd_6; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnTriggerTouchEnd_6() { return &___OnTriggerTouchEnd_6; }
	inline void set_OnTriggerTouchEnd_6(UnityObjectEvent_t2200943093 * value)
	{
		___OnTriggerTouchEnd_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnTriggerTouchEnd_6, value);
	}

	inline static int32_t get_offset_of_OnTriggerHairlineStart_7() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnTriggerHairlineStart_7)); }
	inline UnityObjectEvent_t2200943093 * get_OnTriggerHairlineStart_7() const { return ___OnTriggerHairlineStart_7; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnTriggerHairlineStart_7() { return &___OnTriggerHairlineStart_7; }
	inline void set_OnTriggerHairlineStart_7(UnityObjectEvent_t2200943093 * value)
	{
		___OnTriggerHairlineStart_7 = value;
		Il2CppCodeGenWriteBarrier(&___OnTriggerHairlineStart_7, value);
	}

	inline static int32_t get_offset_of_OnTriggerHairlineEnd_8() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnTriggerHairlineEnd_8)); }
	inline UnityObjectEvent_t2200943093 * get_OnTriggerHairlineEnd_8() const { return ___OnTriggerHairlineEnd_8; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnTriggerHairlineEnd_8() { return &___OnTriggerHairlineEnd_8; }
	inline void set_OnTriggerHairlineEnd_8(UnityObjectEvent_t2200943093 * value)
	{
		___OnTriggerHairlineEnd_8 = value;
		Il2CppCodeGenWriteBarrier(&___OnTriggerHairlineEnd_8, value);
	}

	inline static int32_t get_offset_of_OnTriggerClicked_9() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnTriggerClicked_9)); }
	inline UnityObjectEvent_t2200943093 * get_OnTriggerClicked_9() const { return ___OnTriggerClicked_9; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnTriggerClicked_9() { return &___OnTriggerClicked_9; }
	inline void set_OnTriggerClicked_9(UnityObjectEvent_t2200943093 * value)
	{
		___OnTriggerClicked_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnTriggerClicked_9, value);
	}

	inline static int32_t get_offset_of_OnTriggerUnclicked_10() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnTriggerUnclicked_10)); }
	inline UnityObjectEvent_t2200943093 * get_OnTriggerUnclicked_10() const { return ___OnTriggerUnclicked_10; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnTriggerUnclicked_10() { return &___OnTriggerUnclicked_10; }
	inline void set_OnTriggerUnclicked_10(UnityObjectEvent_t2200943093 * value)
	{
		___OnTriggerUnclicked_10 = value;
		Il2CppCodeGenWriteBarrier(&___OnTriggerUnclicked_10, value);
	}

	inline static int32_t get_offset_of_OnTriggerAxisChanged_11() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnTriggerAxisChanged_11)); }
	inline UnityObjectEvent_t2200943093 * get_OnTriggerAxisChanged_11() const { return ___OnTriggerAxisChanged_11; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnTriggerAxisChanged_11() { return &___OnTriggerAxisChanged_11; }
	inline void set_OnTriggerAxisChanged_11(UnityObjectEvent_t2200943093 * value)
	{
		___OnTriggerAxisChanged_11 = value;
		Il2CppCodeGenWriteBarrier(&___OnTriggerAxisChanged_11, value);
	}

	inline static int32_t get_offset_of_OnGripPressed_12() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnGripPressed_12)); }
	inline UnityObjectEvent_t2200943093 * get_OnGripPressed_12() const { return ___OnGripPressed_12; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnGripPressed_12() { return &___OnGripPressed_12; }
	inline void set_OnGripPressed_12(UnityObjectEvent_t2200943093 * value)
	{
		___OnGripPressed_12 = value;
		Il2CppCodeGenWriteBarrier(&___OnGripPressed_12, value);
	}

	inline static int32_t get_offset_of_OnGripReleased_13() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnGripReleased_13)); }
	inline UnityObjectEvent_t2200943093 * get_OnGripReleased_13() const { return ___OnGripReleased_13; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnGripReleased_13() { return &___OnGripReleased_13; }
	inline void set_OnGripReleased_13(UnityObjectEvent_t2200943093 * value)
	{
		___OnGripReleased_13 = value;
		Il2CppCodeGenWriteBarrier(&___OnGripReleased_13, value);
	}

	inline static int32_t get_offset_of_OnGripTouchStart_14() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnGripTouchStart_14)); }
	inline UnityObjectEvent_t2200943093 * get_OnGripTouchStart_14() const { return ___OnGripTouchStart_14; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnGripTouchStart_14() { return &___OnGripTouchStart_14; }
	inline void set_OnGripTouchStart_14(UnityObjectEvent_t2200943093 * value)
	{
		___OnGripTouchStart_14 = value;
		Il2CppCodeGenWriteBarrier(&___OnGripTouchStart_14, value);
	}

	inline static int32_t get_offset_of_OnGripTouchEnd_15() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnGripTouchEnd_15)); }
	inline UnityObjectEvent_t2200943093 * get_OnGripTouchEnd_15() const { return ___OnGripTouchEnd_15; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnGripTouchEnd_15() { return &___OnGripTouchEnd_15; }
	inline void set_OnGripTouchEnd_15(UnityObjectEvent_t2200943093 * value)
	{
		___OnGripTouchEnd_15 = value;
		Il2CppCodeGenWriteBarrier(&___OnGripTouchEnd_15, value);
	}

	inline static int32_t get_offset_of_OnGripHairlineStart_16() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnGripHairlineStart_16)); }
	inline UnityObjectEvent_t2200943093 * get_OnGripHairlineStart_16() const { return ___OnGripHairlineStart_16; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnGripHairlineStart_16() { return &___OnGripHairlineStart_16; }
	inline void set_OnGripHairlineStart_16(UnityObjectEvent_t2200943093 * value)
	{
		___OnGripHairlineStart_16 = value;
		Il2CppCodeGenWriteBarrier(&___OnGripHairlineStart_16, value);
	}

	inline static int32_t get_offset_of_OnGripHairlineEnd_17() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnGripHairlineEnd_17)); }
	inline UnityObjectEvent_t2200943093 * get_OnGripHairlineEnd_17() const { return ___OnGripHairlineEnd_17; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnGripHairlineEnd_17() { return &___OnGripHairlineEnd_17; }
	inline void set_OnGripHairlineEnd_17(UnityObjectEvent_t2200943093 * value)
	{
		___OnGripHairlineEnd_17 = value;
		Il2CppCodeGenWriteBarrier(&___OnGripHairlineEnd_17, value);
	}

	inline static int32_t get_offset_of_OnGripClicked_18() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnGripClicked_18)); }
	inline UnityObjectEvent_t2200943093 * get_OnGripClicked_18() const { return ___OnGripClicked_18; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnGripClicked_18() { return &___OnGripClicked_18; }
	inline void set_OnGripClicked_18(UnityObjectEvent_t2200943093 * value)
	{
		___OnGripClicked_18 = value;
		Il2CppCodeGenWriteBarrier(&___OnGripClicked_18, value);
	}

	inline static int32_t get_offset_of_OnGripUnclicked_19() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnGripUnclicked_19)); }
	inline UnityObjectEvent_t2200943093 * get_OnGripUnclicked_19() const { return ___OnGripUnclicked_19; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnGripUnclicked_19() { return &___OnGripUnclicked_19; }
	inline void set_OnGripUnclicked_19(UnityObjectEvent_t2200943093 * value)
	{
		___OnGripUnclicked_19 = value;
		Il2CppCodeGenWriteBarrier(&___OnGripUnclicked_19, value);
	}

	inline static int32_t get_offset_of_OnGripAxisChanged_20() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnGripAxisChanged_20)); }
	inline UnityObjectEvent_t2200943093 * get_OnGripAxisChanged_20() const { return ___OnGripAxisChanged_20; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnGripAxisChanged_20() { return &___OnGripAxisChanged_20; }
	inline void set_OnGripAxisChanged_20(UnityObjectEvent_t2200943093 * value)
	{
		___OnGripAxisChanged_20 = value;
		Il2CppCodeGenWriteBarrier(&___OnGripAxisChanged_20, value);
	}

	inline static int32_t get_offset_of_OnTouchpadPressed_21() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnTouchpadPressed_21)); }
	inline UnityObjectEvent_t2200943093 * get_OnTouchpadPressed_21() const { return ___OnTouchpadPressed_21; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnTouchpadPressed_21() { return &___OnTouchpadPressed_21; }
	inline void set_OnTouchpadPressed_21(UnityObjectEvent_t2200943093 * value)
	{
		___OnTouchpadPressed_21 = value;
		Il2CppCodeGenWriteBarrier(&___OnTouchpadPressed_21, value);
	}

	inline static int32_t get_offset_of_OnTouchpadReleased_22() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnTouchpadReleased_22)); }
	inline UnityObjectEvent_t2200943093 * get_OnTouchpadReleased_22() const { return ___OnTouchpadReleased_22; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnTouchpadReleased_22() { return &___OnTouchpadReleased_22; }
	inline void set_OnTouchpadReleased_22(UnityObjectEvent_t2200943093 * value)
	{
		___OnTouchpadReleased_22 = value;
		Il2CppCodeGenWriteBarrier(&___OnTouchpadReleased_22, value);
	}

	inline static int32_t get_offset_of_OnTouchpadTouchStart_23() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnTouchpadTouchStart_23)); }
	inline UnityObjectEvent_t2200943093 * get_OnTouchpadTouchStart_23() const { return ___OnTouchpadTouchStart_23; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnTouchpadTouchStart_23() { return &___OnTouchpadTouchStart_23; }
	inline void set_OnTouchpadTouchStart_23(UnityObjectEvent_t2200943093 * value)
	{
		___OnTouchpadTouchStart_23 = value;
		Il2CppCodeGenWriteBarrier(&___OnTouchpadTouchStart_23, value);
	}

	inline static int32_t get_offset_of_OnTouchpadTouchEnd_24() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnTouchpadTouchEnd_24)); }
	inline UnityObjectEvent_t2200943093 * get_OnTouchpadTouchEnd_24() const { return ___OnTouchpadTouchEnd_24; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnTouchpadTouchEnd_24() { return &___OnTouchpadTouchEnd_24; }
	inline void set_OnTouchpadTouchEnd_24(UnityObjectEvent_t2200943093 * value)
	{
		___OnTouchpadTouchEnd_24 = value;
		Il2CppCodeGenWriteBarrier(&___OnTouchpadTouchEnd_24, value);
	}

	inline static int32_t get_offset_of_OnTouchpadAxisChanged_25() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnTouchpadAxisChanged_25)); }
	inline UnityObjectEvent_t2200943093 * get_OnTouchpadAxisChanged_25() const { return ___OnTouchpadAxisChanged_25; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnTouchpadAxisChanged_25() { return &___OnTouchpadAxisChanged_25; }
	inline void set_OnTouchpadAxisChanged_25(UnityObjectEvent_t2200943093 * value)
	{
		___OnTouchpadAxisChanged_25 = value;
		Il2CppCodeGenWriteBarrier(&___OnTouchpadAxisChanged_25, value);
	}

	inline static int32_t get_offset_of_OnButtonOnePressed_26() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnButtonOnePressed_26)); }
	inline UnityObjectEvent_t2200943093 * get_OnButtonOnePressed_26() const { return ___OnButtonOnePressed_26; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnButtonOnePressed_26() { return &___OnButtonOnePressed_26; }
	inline void set_OnButtonOnePressed_26(UnityObjectEvent_t2200943093 * value)
	{
		___OnButtonOnePressed_26 = value;
		Il2CppCodeGenWriteBarrier(&___OnButtonOnePressed_26, value);
	}

	inline static int32_t get_offset_of_OnButtonOneReleased_27() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnButtonOneReleased_27)); }
	inline UnityObjectEvent_t2200943093 * get_OnButtonOneReleased_27() const { return ___OnButtonOneReleased_27; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnButtonOneReleased_27() { return &___OnButtonOneReleased_27; }
	inline void set_OnButtonOneReleased_27(UnityObjectEvent_t2200943093 * value)
	{
		___OnButtonOneReleased_27 = value;
		Il2CppCodeGenWriteBarrier(&___OnButtonOneReleased_27, value);
	}

	inline static int32_t get_offset_of_OnButtonOneTouchStart_28() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnButtonOneTouchStart_28)); }
	inline UnityObjectEvent_t2200943093 * get_OnButtonOneTouchStart_28() const { return ___OnButtonOneTouchStart_28; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnButtonOneTouchStart_28() { return &___OnButtonOneTouchStart_28; }
	inline void set_OnButtonOneTouchStart_28(UnityObjectEvent_t2200943093 * value)
	{
		___OnButtonOneTouchStart_28 = value;
		Il2CppCodeGenWriteBarrier(&___OnButtonOneTouchStart_28, value);
	}

	inline static int32_t get_offset_of_OnButtonOneTouchEnd_29() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnButtonOneTouchEnd_29)); }
	inline UnityObjectEvent_t2200943093 * get_OnButtonOneTouchEnd_29() const { return ___OnButtonOneTouchEnd_29; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnButtonOneTouchEnd_29() { return &___OnButtonOneTouchEnd_29; }
	inline void set_OnButtonOneTouchEnd_29(UnityObjectEvent_t2200943093 * value)
	{
		___OnButtonOneTouchEnd_29 = value;
		Il2CppCodeGenWriteBarrier(&___OnButtonOneTouchEnd_29, value);
	}

	inline static int32_t get_offset_of_OnButtonTwoPressed_30() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnButtonTwoPressed_30)); }
	inline UnityObjectEvent_t2200943093 * get_OnButtonTwoPressed_30() const { return ___OnButtonTwoPressed_30; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnButtonTwoPressed_30() { return &___OnButtonTwoPressed_30; }
	inline void set_OnButtonTwoPressed_30(UnityObjectEvent_t2200943093 * value)
	{
		___OnButtonTwoPressed_30 = value;
		Il2CppCodeGenWriteBarrier(&___OnButtonTwoPressed_30, value);
	}

	inline static int32_t get_offset_of_OnButtonTwoReleased_31() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnButtonTwoReleased_31)); }
	inline UnityObjectEvent_t2200943093 * get_OnButtonTwoReleased_31() const { return ___OnButtonTwoReleased_31; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnButtonTwoReleased_31() { return &___OnButtonTwoReleased_31; }
	inline void set_OnButtonTwoReleased_31(UnityObjectEvent_t2200943093 * value)
	{
		___OnButtonTwoReleased_31 = value;
		Il2CppCodeGenWriteBarrier(&___OnButtonTwoReleased_31, value);
	}

	inline static int32_t get_offset_of_OnButtonTwoTouchStart_32() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnButtonTwoTouchStart_32)); }
	inline UnityObjectEvent_t2200943093 * get_OnButtonTwoTouchStart_32() const { return ___OnButtonTwoTouchStart_32; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnButtonTwoTouchStart_32() { return &___OnButtonTwoTouchStart_32; }
	inline void set_OnButtonTwoTouchStart_32(UnityObjectEvent_t2200943093 * value)
	{
		___OnButtonTwoTouchStart_32 = value;
		Il2CppCodeGenWriteBarrier(&___OnButtonTwoTouchStart_32, value);
	}

	inline static int32_t get_offset_of_OnButtonTwoTouchEnd_33() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnButtonTwoTouchEnd_33)); }
	inline UnityObjectEvent_t2200943093 * get_OnButtonTwoTouchEnd_33() const { return ___OnButtonTwoTouchEnd_33; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnButtonTwoTouchEnd_33() { return &___OnButtonTwoTouchEnd_33; }
	inline void set_OnButtonTwoTouchEnd_33(UnityObjectEvent_t2200943093 * value)
	{
		___OnButtonTwoTouchEnd_33 = value;
		Il2CppCodeGenWriteBarrier(&___OnButtonTwoTouchEnd_33, value);
	}

	inline static int32_t get_offset_of_OnStartMenuPressed_34() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnStartMenuPressed_34)); }
	inline UnityObjectEvent_t2200943093 * get_OnStartMenuPressed_34() const { return ___OnStartMenuPressed_34; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnStartMenuPressed_34() { return &___OnStartMenuPressed_34; }
	inline void set_OnStartMenuPressed_34(UnityObjectEvent_t2200943093 * value)
	{
		___OnStartMenuPressed_34 = value;
		Il2CppCodeGenWriteBarrier(&___OnStartMenuPressed_34, value);
	}

	inline static int32_t get_offset_of_OnStartMenuReleased_35() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnStartMenuReleased_35)); }
	inline UnityObjectEvent_t2200943093 * get_OnStartMenuReleased_35() const { return ___OnStartMenuReleased_35; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnStartMenuReleased_35() { return &___OnStartMenuReleased_35; }
	inline void set_OnStartMenuReleased_35(UnityObjectEvent_t2200943093 * value)
	{
		___OnStartMenuReleased_35 = value;
		Il2CppCodeGenWriteBarrier(&___OnStartMenuReleased_35, value);
	}

	inline static int32_t get_offset_of_OnAliasPointerOn_36() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnAliasPointerOn_36)); }
	inline UnityObjectEvent_t2200943093 * get_OnAliasPointerOn_36() const { return ___OnAliasPointerOn_36; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnAliasPointerOn_36() { return &___OnAliasPointerOn_36; }
	inline void set_OnAliasPointerOn_36(UnityObjectEvent_t2200943093 * value)
	{
		___OnAliasPointerOn_36 = value;
		Il2CppCodeGenWriteBarrier(&___OnAliasPointerOn_36, value);
	}

	inline static int32_t get_offset_of_OnAliasPointerOff_37() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnAliasPointerOff_37)); }
	inline UnityObjectEvent_t2200943093 * get_OnAliasPointerOff_37() const { return ___OnAliasPointerOff_37; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnAliasPointerOff_37() { return &___OnAliasPointerOff_37; }
	inline void set_OnAliasPointerOff_37(UnityObjectEvent_t2200943093 * value)
	{
		___OnAliasPointerOff_37 = value;
		Il2CppCodeGenWriteBarrier(&___OnAliasPointerOff_37, value);
	}

	inline static int32_t get_offset_of_OnAliasPointerSet_38() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnAliasPointerSet_38)); }
	inline UnityObjectEvent_t2200943093 * get_OnAliasPointerSet_38() const { return ___OnAliasPointerSet_38; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnAliasPointerSet_38() { return &___OnAliasPointerSet_38; }
	inline void set_OnAliasPointerSet_38(UnityObjectEvent_t2200943093 * value)
	{
		___OnAliasPointerSet_38 = value;
		Il2CppCodeGenWriteBarrier(&___OnAliasPointerSet_38, value);
	}

	inline static int32_t get_offset_of_OnAliasGrabOn_39() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnAliasGrabOn_39)); }
	inline UnityObjectEvent_t2200943093 * get_OnAliasGrabOn_39() const { return ___OnAliasGrabOn_39; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnAliasGrabOn_39() { return &___OnAliasGrabOn_39; }
	inline void set_OnAliasGrabOn_39(UnityObjectEvent_t2200943093 * value)
	{
		___OnAliasGrabOn_39 = value;
		Il2CppCodeGenWriteBarrier(&___OnAliasGrabOn_39, value);
	}

	inline static int32_t get_offset_of_OnAliasGrabOff_40() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnAliasGrabOff_40)); }
	inline UnityObjectEvent_t2200943093 * get_OnAliasGrabOff_40() const { return ___OnAliasGrabOff_40; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnAliasGrabOff_40() { return &___OnAliasGrabOff_40; }
	inline void set_OnAliasGrabOff_40(UnityObjectEvent_t2200943093 * value)
	{
		___OnAliasGrabOff_40 = value;
		Il2CppCodeGenWriteBarrier(&___OnAliasGrabOff_40, value);
	}

	inline static int32_t get_offset_of_OnAliasUseOn_41() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnAliasUseOn_41)); }
	inline UnityObjectEvent_t2200943093 * get_OnAliasUseOn_41() const { return ___OnAliasUseOn_41; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnAliasUseOn_41() { return &___OnAliasUseOn_41; }
	inline void set_OnAliasUseOn_41(UnityObjectEvent_t2200943093 * value)
	{
		___OnAliasUseOn_41 = value;
		Il2CppCodeGenWriteBarrier(&___OnAliasUseOn_41, value);
	}

	inline static int32_t get_offset_of_OnAliasUseOff_42() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnAliasUseOff_42)); }
	inline UnityObjectEvent_t2200943093 * get_OnAliasUseOff_42() const { return ___OnAliasUseOff_42; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnAliasUseOff_42() { return &___OnAliasUseOff_42; }
	inline void set_OnAliasUseOff_42(UnityObjectEvent_t2200943093 * value)
	{
		___OnAliasUseOff_42 = value;
		Il2CppCodeGenWriteBarrier(&___OnAliasUseOff_42, value);
	}

	inline static int32_t get_offset_of_OnAliasUIClickOn_43() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnAliasUIClickOn_43)); }
	inline UnityObjectEvent_t2200943093 * get_OnAliasUIClickOn_43() const { return ___OnAliasUIClickOn_43; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnAliasUIClickOn_43() { return &___OnAliasUIClickOn_43; }
	inline void set_OnAliasUIClickOn_43(UnityObjectEvent_t2200943093 * value)
	{
		___OnAliasUIClickOn_43 = value;
		Il2CppCodeGenWriteBarrier(&___OnAliasUIClickOn_43, value);
	}

	inline static int32_t get_offset_of_OnAliasUIClickOff_44() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnAliasUIClickOff_44)); }
	inline UnityObjectEvent_t2200943093 * get_OnAliasUIClickOff_44() const { return ___OnAliasUIClickOff_44; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnAliasUIClickOff_44() { return &___OnAliasUIClickOff_44; }
	inline void set_OnAliasUIClickOff_44(UnityObjectEvent_t2200943093 * value)
	{
		___OnAliasUIClickOff_44 = value;
		Il2CppCodeGenWriteBarrier(&___OnAliasUIClickOff_44, value);
	}

	inline static int32_t get_offset_of_OnAliasMenuOn_45() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnAliasMenuOn_45)); }
	inline UnityObjectEvent_t2200943093 * get_OnAliasMenuOn_45() const { return ___OnAliasMenuOn_45; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnAliasMenuOn_45() { return &___OnAliasMenuOn_45; }
	inline void set_OnAliasMenuOn_45(UnityObjectEvent_t2200943093 * value)
	{
		___OnAliasMenuOn_45 = value;
		Il2CppCodeGenWriteBarrier(&___OnAliasMenuOn_45, value);
	}

	inline static int32_t get_offset_of_OnAliasMenuOff_46() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnAliasMenuOff_46)); }
	inline UnityObjectEvent_t2200943093 * get_OnAliasMenuOff_46() const { return ___OnAliasMenuOff_46; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnAliasMenuOff_46() { return &___OnAliasMenuOff_46; }
	inline void set_OnAliasMenuOff_46(UnityObjectEvent_t2200943093 * value)
	{
		___OnAliasMenuOff_46 = value;
		Il2CppCodeGenWriteBarrier(&___OnAliasMenuOff_46, value);
	}

	inline static int32_t get_offset_of_OnControllerEnabled_47() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnControllerEnabled_47)); }
	inline UnityObjectEvent_t2200943093 * get_OnControllerEnabled_47() const { return ___OnControllerEnabled_47; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnControllerEnabled_47() { return &___OnControllerEnabled_47; }
	inline void set_OnControllerEnabled_47(UnityObjectEvent_t2200943093 * value)
	{
		___OnControllerEnabled_47 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerEnabled_47, value);
	}

	inline static int32_t get_offset_of_OnControllerDisabled_48() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnControllerDisabled_48)); }
	inline UnityObjectEvent_t2200943093 * get_OnControllerDisabled_48() const { return ___OnControllerDisabled_48; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnControllerDisabled_48() { return &___OnControllerDisabled_48; }
	inline void set_OnControllerDisabled_48(UnityObjectEvent_t2200943093 * value)
	{
		___OnControllerDisabled_48 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerDisabled_48, value);
	}

	inline static int32_t get_offset_of_OnControllerIndexChanged_49() { return static_cast<int32_t>(offsetof(VRTK_ControllerEvents_UnityEvents_t1877972230, ___OnControllerIndexChanged_49)); }
	inline UnityObjectEvent_t2200943093 * get_OnControllerIndexChanged_49() const { return ___OnControllerIndexChanged_49; }
	inline UnityObjectEvent_t2200943093 ** get_address_of_OnControllerIndexChanged_49() { return &___OnControllerIndexChanged_49; }
	inline void set_OnControllerIndexChanged_49(UnityObjectEvent_t2200943093 * value)
	{
		___OnControllerIndexChanged_49 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerIndexChanged_49, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
