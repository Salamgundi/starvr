﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_GetComponentName
struct _GetComponentName_t3462998887;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_GetComponentName::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetComponentName__ctor_m3486105392 (_GetComponentName_t3462998887 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRRenderModels/_GetComponentName::Invoke(System.String,System.UInt32,System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t _GetComponentName_Invoke_m2092916931 (_GetComponentName_t3462998887 * __this, String_t* ___pchRenderModelName0, uint32_t ___unComponentIndex1, StringBuilder_t1221177846 * ___pchComponentName2, uint32_t ___unComponentNameLen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetComponentName::BeginInvoke(System.String,System.UInt32,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetComponentName_BeginInvoke_m1213578803 (_GetComponentName_t3462998887 * __this, String_t* ___pchRenderModelName0, uint32_t ___unComponentIndex1, StringBuilder_t1221177846 * ___pchComponentName2, uint32_t ___unComponentNameLen3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRRenderModels/_GetComponentName::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetComponentName_EndInvoke_m2526683259 (_GetComponentName_t3462998887 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
