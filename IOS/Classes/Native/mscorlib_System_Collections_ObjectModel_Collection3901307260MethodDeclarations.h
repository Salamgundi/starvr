﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>
struct Collection_1_t3901307260;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.CombineInstance[]
struct CombineInstanceU5BU5D_t1231324047;
// System.Collections.Generic.IEnumerator`1<UnityEngine.CombineInstance>
struct IEnumerator_1_t1835086333;
// System.Collections.Generic.IList`1<UnityEngine.CombineInstance>
struct IList_1_t605535811;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_CombineInstance64595210.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::.ctor()
extern "C"  void Collection_1__ctor_m3052975292_gshared (Collection_1_t3901307260 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3052975292(__this, method) ((  void (*) (Collection_1_t3901307260 *, const MethodInfo*))Collection_1__ctor_m3052975292_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2423262571_gshared (Collection_1_t3901307260 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2423262571(__this, method) ((  bool (*) (Collection_1_t3901307260 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2423262571_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m512532016_gshared (Collection_1_t3901307260 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m512532016(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3901307260 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m512532016_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m4034546447_gshared (Collection_1_t3901307260 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m4034546447(__this, method) ((  Il2CppObject * (*) (Collection_1_t3901307260 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m4034546447_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2480273516_gshared (Collection_1_t3901307260 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2480273516(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3901307260 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2480273516_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m977500210_gshared (Collection_1_t3901307260 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m977500210(__this, ___value0, method) ((  bool (*) (Collection_1_t3901307260 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m977500210_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3569925082_gshared (Collection_1_t3901307260 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3569925082(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3901307260 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3569925082_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2552795823_gshared (Collection_1_t3901307260 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2552795823(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3901307260 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2552795823_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2815090991_gshared (Collection_1_t3901307260 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2815090991(__this, ___value0, method) ((  void (*) (Collection_1_t3901307260 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2815090991_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m539029612_gshared (Collection_1_t3901307260 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m539029612(__this, method) ((  bool (*) (Collection_1_t3901307260 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m539029612_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1781047920_gshared (Collection_1_t3901307260 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1781047920(__this, method) ((  Il2CppObject * (*) (Collection_1_t3901307260 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1781047920_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m3857570103_gshared (Collection_1_t3901307260 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3857570103(__this, method) ((  bool (*) (Collection_1_t3901307260 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m3857570103_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3927805104_gshared (Collection_1_t3901307260 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3927805104(__this, method) ((  bool (*) (Collection_1_t3901307260 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3927805104_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m4198565991_gshared (Collection_1_t3901307260 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m4198565991(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3901307260 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m4198565991_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1386829230_gshared (Collection_1_t3901307260 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1386829230(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3901307260 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1386829230_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::Add(T)
extern "C"  void Collection_1_Add_m1372211307_gshared (Collection_1_t3901307260 * __this, CombineInstance_t64595210  ___item0, const MethodInfo* method);
#define Collection_1_Add_m1372211307(__this, ___item0, method) ((  void (*) (Collection_1_t3901307260 *, CombineInstance_t64595210 , const MethodInfo*))Collection_1_Add_m1372211307_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::Clear()
extern "C"  void Collection_1_Clear_m3302825127_gshared (Collection_1_t3901307260 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3302825127(__this, method) ((  void (*) (Collection_1_t3901307260 *, const MethodInfo*))Collection_1_Clear_m3302825127_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::ClearItems()
extern "C"  void Collection_1_ClearItems_m414913109_gshared (Collection_1_t3901307260 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m414913109(__this, method) ((  void (*) (Collection_1_t3901307260 *, const MethodInfo*))Collection_1_ClearItems_m414913109_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::Contains(T)
extern "C"  bool Collection_1_Contains_m2095371821_gshared (Collection_1_t3901307260 * __this, CombineInstance_t64595210  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2095371821(__this, ___item0, method) ((  bool (*) (Collection_1_t3901307260 *, CombineInstance_t64595210 , const MethodInfo*))Collection_1_Contains_m2095371821_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1392141839_gshared (Collection_1_t3901307260 * __this, CombineInstanceU5BU5D_t1231324047* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1392141839(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3901307260 *, CombineInstanceU5BU5D_t1231324047*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1392141839_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1788664416_gshared (Collection_1_t3901307260 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1788664416(__this, method) ((  Il2CppObject* (*) (Collection_1_t3901307260 *, const MethodInfo*))Collection_1_GetEnumerator_m1788664416_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3821350187_gshared (Collection_1_t3901307260 * __this, CombineInstance_t64595210  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3821350187(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3901307260 *, CombineInstance_t64595210 , const MethodInfo*))Collection_1_IndexOf_m3821350187_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m398507554_gshared (Collection_1_t3901307260 * __this, int32_t ___index0, CombineInstance_t64595210  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m398507554(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3901307260 *, int32_t, CombineInstance_t64595210 , const MethodInfo*))Collection_1_Insert_m398507554_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3850747333_gshared (Collection_1_t3901307260 * __this, int32_t ___index0, CombineInstance_t64595210  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3850747333(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3901307260 *, int32_t, CombineInstance_t64595210 , const MethodInfo*))Collection_1_InsertItem_m3850747333_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::Remove(T)
extern "C"  bool Collection_1_Remove_m1431621516_gshared (Collection_1_t3901307260 * __this, CombineInstance_t64595210  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1431621516(__this, ___item0, method) ((  bool (*) (Collection_1_t3901307260 *, CombineInstance_t64595210 , const MethodInfo*))Collection_1_Remove_m1431621516_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2826144590_gshared (Collection_1_t3901307260 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2826144590(__this, ___index0, method) ((  void (*) (Collection_1_t3901307260 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2826144590_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m163963168_gshared (Collection_1_t3901307260 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m163963168(__this, ___index0, method) ((  void (*) (Collection_1_t3901307260 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m163963168_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3193259148_gshared (Collection_1_t3901307260 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3193259148(__this, method) ((  int32_t (*) (Collection_1_t3901307260 *, const MethodInfo*))Collection_1_get_Count_m3193259148_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::get_Item(System.Int32)
extern "C"  CombineInstance_t64595210  Collection_1_get_Item_m4177110736_gshared (Collection_1_t3901307260 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m4177110736(__this, ___index0, method) ((  CombineInstance_t64595210  (*) (Collection_1_t3901307260 *, int32_t, const MethodInfo*))Collection_1_get_Item_m4177110736_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2550059291_gshared (Collection_1_t3901307260 * __this, int32_t ___index0, CombineInstance_t64595210  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2550059291(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3901307260 *, int32_t, CombineInstance_t64595210 , const MethodInfo*))Collection_1_set_Item_m2550059291_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3390868542_gshared (Collection_1_t3901307260 * __this, int32_t ___index0, CombineInstance_t64595210  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3390868542(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3901307260 *, int32_t, CombineInstance_t64595210 , const MethodInfo*))Collection_1_SetItem_m3390868542_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3668962481_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3668962481(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3668962481_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::ConvertItem(System.Object)
extern "C"  CombineInstance_t64595210  Collection_1_ConvertItem_m2576485549_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2576485549(__this /* static, unused */, ___item0, method) ((  CombineInstance_t64595210  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2576485549_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2818764905_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2818764905(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2818764905_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1676208185_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1676208185(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1676208185_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.CombineInstance>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1614658246_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1614658246(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1614658246_gshared)(__this /* static, unused */, ___list0, method)
