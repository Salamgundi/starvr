﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_CurveGenerator
struct VRTK_CurveGenerator_t3769661606;

#include "AssemblyU2DCSharp_VRTK_VRTK_BasePointer3466805540.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_BezierPointer
struct  VRTK_BezierPointer_t2668864506  : public VRTK_BasePointer_t3466805540
{
public:
	// System.Single VRTK.VRTK_BezierPointer::pointerLength
	float ___pointerLength_41;
	// System.Int32 VRTK.VRTK_BezierPointer::pointerDensity
	int32_t ___pointerDensity_42;
	// System.Int32 VRTK.VRTK_BezierPointer::collisionCheckFrequency
	int32_t ___collisionCheckFrequency_43;
	// System.Single VRTK.VRTK_BezierPointer::beamCurveOffset
	float ___beamCurveOffset_44;
	// System.Single VRTK.VRTK_BezierPointer::beamHeightLimitAngle
	float ___beamHeightLimitAngle_45;
	// System.Boolean VRTK.VRTK_BezierPointer::rescalePointerTracer
	bool ___rescalePointerTracer_46;
	// System.Boolean VRTK.VRTK_BezierPointer::showPointerCursor
	bool ___showPointerCursor_47;
	// System.Single VRTK.VRTK_BezierPointer::pointerCursorRadius
	float ___pointerCursorRadius_48;
	// System.Boolean VRTK.VRTK_BezierPointer::pointerCursorMatchTargetRotation
	bool ___pointerCursorMatchTargetRotation_49;
	// UnityEngine.GameObject VRTK.VRTK_BezierPointer::customPointerTracer
	GameObject_t1756533147 * ___customPointerTracer_50;
	// UnityEngine.GameObject VRTK.VRTK_BezierPointer::customPointerCursor
	GameObject_t1756533147 * ___customPointerCursor_51;
	// UnityEngine.GameObject VRTK.VRTK_BezierPointer::validTeleportLocationObject
	GameObject_t1756533147 * ___validTeleportLocationObject_52;
	// UnityEngine.GameObject VRTK.VRTK_BezierPointer::pointerCursor
	GameObject_t1756533147 * ___pointerCursor_53;
	// UnityEngine.GameObject VRTK.VRTK_BezierPointer::curvedBeamContainer
	GameObject_t1756533147 * ___curvedBeamContainer_54;
	// VRTK.VRTK_CurveGenerator VRTK.VRTK_BezierPointer::curvedBeam
	VRTK_CurveGenerator_t3769661606 * ___curvedBeam_55;
	// UnityEngine.GameObject VRTK.VRTK_BezierPointer::validTeleportLocationInstance
	GameObject_t1756533147 * ___validTeleportLocationInstance_56;
	// System.Boolean VRTK.VRTK_BezierPointer::beamActive
	bool ___beamActive_57;
	// UnityEngine.Vector3 VRTK.VRTK_BezierPointer::fixedForwardBeamForward
	Vector3_t2243707580  ___fixedForwardBeamForward_58;
	// UnityEngine.Vector3 VRTK.VRTK_BezierPointer::contactNormal
	Vector3_t2243707580  ___contactNormal_59;

public:
	inline static int32_t get_offset_of_pointerLength_41() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___pointerLength_41)); }
	inline float get_pointerLength_41() const { return ___pointerLength_41; }
	inline float* get_address_of_pointerLength_41() { return &___pointerLength_41; }
	inline void set_pointerLength_41(float value)
	{
		___pointerLength_41 = value;
	}

	inline static int32_t get_offset_of_pointerDensity_42() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___pointerDensity_42)); }
	inline int32_t get_pointerDensity_42() const { return ___pointerDensity_42; }
	inline int32_t* get_address_of_pointerDensity_42() { return &___pointerDensity_42; }
	inline void set_pointerDensity_42(int32_t value)
	{
		___pointerDensity_42 = value;
	}

	inline static int32_t get_offset_of_collisionCheckFrequency_43() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___collisionCheckFrequency_43)); }
	inline int32_t get_collisionCheckFrequency_43() const { return ___collisionCheckFrequency_43; }
	inline int32_t* get_address_of_collisionCheckFrequency_43() { return &___collisionCheckFrequency_43; }
	inline void set_collisionCheckFrequency_43(int32_t value)
	{
		___collisionCheckFrequency_43 = value;
	}

	inline static int32_t get_offset_of_beamCurveOffset_44() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___beamCurveOffset_44)); }
	inline float get_beamCurveOffset_44() const { return ___beamCurveOffset_44; }
	inline float* get_address_of_beamCurveOffset_44() { return &___beamCurveOffset_44; }
	inline void set_beamCurveOffset_44(float value)
	{
		___beamCurveOffset_44 = value;
	}

	inline static int32_t get_offset_of_beamHeightLimitAngle_45() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___beamHeightLimitAngle_45)); }
	inline float get_beamHeightLimitAngle_45() const { return ___beamHeightLimitAngle_45; }
	inline float* get_address_of_beamHeightLimitAngle_45() { return &___beamHeightLimitAngle_45; }
	inline void set_beamHeightLimitAngle_45(float value)
	{
		___beamHeightLimitAngle_45 = value;
	}

	inline static int32_t get_offset_of_rescalePointerTracer_46() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___rescalePointerTracer_46)); }
	inline bool get_rescalePointerTracer_46() const { return ___rescalePointerTracer_46; }
	inline bool* get_address_of_rescalePointerTracer_46() { return &___rescalePointerTracer_46; }
	inline void set_rescalePointerTracer_46(bool value)
	{
		___rescalePointerTracer_46 = value;
	}

	inline static int32_t get_offset_of_showPointerCursor_47() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___showPointerCursor_47)); }
	inline bool get_showPointerCursor_47() const { return ___showPointerCursor_47; }
	inline bool* get_address_of_showPointerCursor_47() { return &___showPointerCursor_47; }
	inline void set_showPointerCursor_47(bool value)
	{
		___showPointerCursor_47 = value;
	}

	inline static int32_t get_offset_of_pointerCursorRadius_48() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___pointerCursorRadius_48)); }
	inline float get_pointerCursorRadius_48() const { return ___pointerCursorRadius_48; }
	inline float* get_address_of_pointerCursorRadius_48() { return &___pointerCursorRadius_48; }
	inline void set_pointerCursorRadius_48(float value)
	{
		___pointerCursorRadius_48 = value;
	}

	inline static int32_t get_offset_of_pointerCursorMatchTargetRotation_49() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___pointerCursorMatchTargetRotation_49)); }
	inline bool get_pointerCursorMatchTargetRotation_49() const { return ___pointerCursorMatchTargetRotation_49; }
	inline bool* get_address_of_pointerCursorMatchTargetRotation_49() { return &___pointerCursorMatchTargetRotation_49; }
	inline void set_pointerCursorMatchTargetRotation_49(bool value)
	{
		___pointerCursorMatchTargetRotation_49 = value;
	}

	inline static int32_t get_offset_of_customPointerTracer_50() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___customPointerTracer_50)); }
	inline GameObject_t1756533147 * get_customPointerTracer_50() const { return ___customPointerTracer_50; }
	inline GameObject_t1756533147 ** get_address_of_customPointerTracer_50() { return &___customPointerTracer_50; }
	inline void set_customPointerTracer_50(GameObject_t1756533147 * value)
	{
		___customPointerTracer_50 = value;
		Il2CppCodeGenWriteBarrier(&___customPointerTracer_50, value);
	}

	inline static int32_t get_offset_of_customPointerCursor_51() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___customPointerCursor_51)); }
	inline GameObject_t1756533147 * get_customPointerCursor_51() const { return ___customPointerCursor_51; }
	inline GameObject_t1756533147 ** get_address_of_customPointerCursor_51() { return &___customPointerCursor_51; }
	inline void set_customPointerCursor_51(GameObject_t1756533147 * value)
	{
		___customPointerCursor_51 = value;
		Il2CppCodeGenWriteBarrier(&___customPointerCursor_51, value);
	}

	inline static int32_t get_offset_of_validTeleportLocationObject_52() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___validTeleportLocationObject_52)); }
	inline GameObject_t1756533147 * get_validTeleportLocationObject_52() const { return ___validTeleportLocationObject_52; }
	inline GameObject_t1756533147 ** get_address_of_validTeleportLocationObject_52() { return &___validTeleportLocationObject_52; }
	inline void set_validTeleportLocationObject_52(GameObject_t1756533147 * value)
	{
		___validTeleportLocationObject_52 = value;
		Il2CppCodeGenWriteBarrier(&___validTeleportLocationObject_52, value);
	}

	inline static int32_t get_offset_of_pointerCursor_53() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___pointerCursor_53)); }
	inline GameObject_t1756533147 * get_pointerCursor_53() const { return ___pointerCursor_53; }
	inline GameObject_t1756533147 ** get_address_of_pointerCursor_53() { return &___pointerCursor_53; }
	inline void set_pointerCursor_53(GameObject_t1756533147 * value)
	{
		___pointerCursor_53 = value;
		Il2CppCodeGenWriteBarrier(&___pointerCursor_53, value);
	}

	inline static int32_t get_offset_of_curvedBeamContainer_54() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___curvedBeamContainer_54)); }
	inline GameObject_t1756533147 * get_curvedBeamContainer_54() const { return ___curvedBeamContainer_54; }
	inline GameObject_t1756533147 ** get_address_of_curvedBeamContainer_54() { return &___curvedBeamContainer_54; }
	inline void set_curvedBeamContainer_54(GameObject_t1756533147 * value)
	{
		___curvedBeamContainer_54 = value;
		Il2CppCodeGenWriteBarrier(&___curvedBeamContainer_54, value);
	}

	inline static int32_t get_offset_of_curvedBeam_55() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___curvedBeam_55)); }
	inline VRTK_CurveGenerator_t3769661606 * get_curvedBeam_55() const { return ___curvedBeam_55; }
	inline VRTK_CurveGenerator_t3769661606 ** get_address_of_curvedBeam_55() { return &___curvedBeam_55; }
	inline void set_curvedBeam_55(VRTK_CurveGenerator_t3769661606 * value)
	{
		___curvedBeam_55 = value;
		Il2CppCodeGenWriteBarrier(&___curvedBeam_55, value);
	}

	inline static int32_t get_offset_of_validTeleportLocationInstance_56() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___validTeleportLocationInstance_56)); }
	inline GameObject_t1756533147 * get_validTeleportLocationInstance_56() const { return ___validTeleportLocationInstance_56; }
	inline GameObject_t1756533147 ** get_address_of_validTeleportLocationInstance_56() { return &___validTeleportLocationInstance_56; }
	inline void set_validTeleportLocationInstance_56(GameObject_t1756533147 * value)
	{
		___validTeleportLocationInstance_56 = value;
		Il2CppCodeGenWriteBarrier(&___validTeleportLocationInstance_56, value);
	}

	inline static int32_t get_offset_of_beamActive_57() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___beamActive_57)); }
	inline bool get_beamActive_57() const { return ___beamActive_57; }
	inline bool* get_address_of_beamActive_57() { return &___beamActive_57; }
	inline void set_beamActive_57(bool value)
	{
		___beamActive_57 = value;
	}

	inline static int32_t get_offset_of_fixedForwardBeamForward_58() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___fixedForwardBeamForward_58)); }
	inline Vector3_t2243707580  get_fixedForwardBeamForward_58() const { return ___fixedForwardBeamForward_58; }
	inline Vector3_t2243707580 * get_address_of_fixedForwardBeamForward_58() { return &___fixedForwardBeamForward_58; }
	inline void set_fixedForwardBeamForward_58(Vector3_t2243707580  value)
	{
		___fixedForwardBeamForward_58 = value;
	}

	inline static int32_t get_offset_of_contactNormal_59() { return static_cast<int32_t>(offsetof(VRTK_BezierPointer_t2668864506, ___contactNormal_59)); }
	inline Vector3_t2243707580  get_contactNormal_59() const { return ___contactNormal_59; }
	inline Vector3_t2243707580 * get_address_of_contactNormal_59() { return &___contactNormal_59; }
	inline void set_contactNormal_59(Vector3_t2243707580  value)
	{
		___contactNormal_59 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
