﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Balloon
struct Balloon_t3376655393;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Valve.VR.InteractionSystem.SoundPlayOneshot
struct SoundPlayOneshot_t1703214483;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Sound1703214483.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Ballo1350152013.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void Valve.VR.InteractionSystem.Balloon::.ctor()
extern "C"  void Balloon__ctor_m2787398079 (Balloon_t3376655393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Balloon::Start()
extern "C"  void Balloon_Start_m256208707 (Balloon_t3376655393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Balloon::Update()
extern "C"  void Balloon_Update_m1204201152 (Balloon_t3376655393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Balloon::SpawnParticles(UnityEngine.GameObject,Valve.VR.InteractionSystem.SoundPlayOneshot)
extern "C"  void Balloon_SpawnParticles_m1167144509 (Balloon_t3376655393 * __this, GameObject_t1756533147 * ___particlePrefab0, SoundPlayOneshot_t1703214483 * ___sound1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Balloon::FixedUpdate()
extern "C"  void Balloon_FixedUpdate_m2602502006 (Balloon_t3376655393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Balloon::ApplyDamage()
extern "C"  void Balloon_ApplyDamage_m3350313660 (Balloon_t3376655393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Balloon::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void Balloon_OnCollisionEnter_m2040120177 (Balloon_t3376655393 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Balloon::SetColor(Valve.VR.InteractionSystem.Balloon/BalloonColor)
extern "C"  void Balloon_SetColor_m1680856135 (Balloon_t3376655393 * __this, int32_t ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Valve.VR.InteractionSystem.Balloon::BalloonColorToRGB(Valve.VR.InteractionSystem.Balloon/BalloonColor)
extern "C"  Color_t2020392075  Balloon_BalloonColorToRGB_m2445448445 (Balloon_t3376655393 * __this, int32_t ___balloonColorVar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Balloon::.cctor()
extern "C"  void Balloon__cctor_m1026763504 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
