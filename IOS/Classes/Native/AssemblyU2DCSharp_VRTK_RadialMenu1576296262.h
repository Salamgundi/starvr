﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<VRTK.RadialMenuButton>
struct List_1_t3795244468;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.HapticPulseEventHandler
struct HapticPulseEventHandler_t422205650;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.RadialMenu
struct  RadialMenu_t1576296262  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<VRTK.RadialMenuButton> VRTK.RadialMenu::buttons
	List_1_t3795244468 * ___buttons_2;
	// UnityEngine.GameObject VRTK.RadialMenu::buttonPrefab
	GameObject_t1756533147 * ___buttonPrefab_3;
	// System.Boolean VRTK.RadialMenu::generateOnAwake
	bool ___generateOnAwake_4;
	// System.Single VRTK.RadialMenu::buttonThickness
	float ___buttonThickness_5;
	// UnityEngine.Color VRTK.RadialMenu::buttonColor
	Color_t2020392075  ___buttonColor_6;
	// System.Single VRTK.RadialMenu::offsetDistance
	float ___offsetDistance_7;
	// System.Single VRTK.RadialMenu::offsetRotation
	float ___offsetRotation_8;
	// System.Boolean VRTK.RadialMenu::rotateIcons
	bool ___rotateIcons_9;
	// System.Single VRTK.RadialMenu::iconMargin
	float ___iconMargin_10;
	// System.Boolean VRTK.RadialMenu::isShown
	bool ___isShown_11;
	// System.Boolean VRTK.RadialMenu::hideOnRelease
	bool ___hideOnRelease_12;
	// System.Boolean VRTK.RadialMenu::executeOnUnclick
	bool ___executeOnUnclick_13;
	// System.Single VRTK.RadialMenu::baseHapticStrength
	float ___baseHapticStrength_14;
	// VRTK.HapticPulseEventHandler VRTK.RadialMenu::FireHapticPulse
	HapticPulseEventHandler_t422205650 * ___FireHapticPulse_15;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> VRTK.RadialMenu::menuButtons
	List_1_t1125654279 * ___menuButtons_16;
	// System.Int32 VRTK.RadialMenu::currentHover
	int32_t ___currentHover_17;
	// System.Int32 VRTK.RadialMenu::currentPress
	int32_t ___currentPress_18;

public:
	inline static int32_t get_offset_of_buttons_2() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___buttons_2)); }
	inline List_1_t3795244468 * get_buttons_2() const { return ___buttons_2; }
	inline List_1_t3795244468 ** get_address_of_buttons_2() { return &___buttons_2; }
	inline void set_buttons_2(List_1_t3795244468 * value)
	{
		___buttons_2 = value;
		Il2CppCodeGenWriteBarrier(&___buttons_2, value);
	}

	inline static int32_t get_offset_of_buttonPrefab_3() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___buttonPrefab_3)); }
	inline GameObject_t1756533147 * get_buttonPrefab_3() const { return ___buttonPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_buttonPrefab_3() { return &___buttonPrefab_3; }
	inline void set_buttonPrefab_3(GameObject_t1756533147 * value)
	{
		___buttonPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___buttonPrefab_3, value);
	}

	inline static int32_t get_offset_of_generateOnAwake_4() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___generateOnAwake_4)); }
	inline bool get_generateOnAwake_4() const { return ___generateOnAwake_4; }
	inline bool* get_address_of_generateOnAwake_4() { return &___generateOnAwake_4; }
	inline void set_generateOnAwake_4(bool value)
	{
		___generateOnAwake_4 = value;
	}

	inline static int32_t get_offset_of_buttonThickness_5() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___buttonThickness_5)); }
	inline float get_buttonThickness_5() const { return ___buttonThickness_5; }
	inline float* get_address_of_buttonThickness_5() { return &___buttonThickness_5; }
	inline void set_buttonThickness_5(float value)
	{
		___buttonThickness_5 = value;
	}

	inline static int32_t get_offset_of_buttonColor_6() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___buttonColor_6)); }
	inline Color_t2020392075  get_buttonColor_6() const { return ___buttonColor_6; }
	inline Color_t2020392075 * get_address_of_buttonColor_6() { return &___buttonColor_6; }
	inline void set_buttonColor_6(Color_t2020392075  value)
	{
		___buttonColor_6 = value;
	}

	inline static int32_t get_offset_of_offsetDistance_7() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___offsetDistance_7)); }
	inline float get_offsetDistance_7() const { return ___offsetDistance_7; }
	inline float* get_address_of_offsetDistance_7() { return &___offsetDistance_7; }
	inline void set_offsetDistance_7(float value)
	{
		___offsetDistance_7 = value;
	}

	inline static int32_t get_offset_of_offsetRotation_8() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___offsetRotation_8)); }
	inline float get_offsetRotation_8() const { return ___offsetRotation_8; }
	inline float* get_address_of_offsetRotation_8() { return &___offsetRotation_8; }
	inline void set_offsetRotation_8(float value)
	{
		___offsetRotation_8 = value;
	}

	inline static int32_t get_offset_of_rotateIcons_9() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___rotateIcons_9)); }
	inline bool get_rotateIcons_9() const { return ___rotateIcons_9; }
	inline bool* get_address_of_rotateIcons_9() { return &___rotateIcons_9; }
	inline void set_rotateIcons_9(bool value)
	{
		___rotateIcons_9 = value;
	}

	inline static int32_t get_offset_of_iconMargin_10() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___iconMargin_10)); }
	inline float get_iconMargin_10() const { return ___iconMargin_10; }
	inline float* get_address_of_iconMargin_10() { return &___iconMargin_10; }
	inline void set_iconMargin_10(float value)
	{
		___iconMargin_10 = value;
	}

	inline static int32_t get_offset_of_isShown_11() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___isShown_11)); }
	inline bool get_isShown_11() const { return ___isShown_11; }
	inline bool* get_address_of_isShown_11() { return &___isShown_11; }
	inline void set_isShown_11(bool value)
	{
		___isShown_11 = value;
	}

	inline static int32_t get_offset_of_hideOnRelease_12() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___hideOnRelease_12)); }
	inline bool get_hideOnRelease_12() const { return ___hideOnRelease_12; }
	inline bool* get_address_of_hideOnRelease_12() { return &___hideOnRelease_12; }
	inline void set_hideOnRelease_12(bool value)
	{
		___hideOnRelease_12 = value;
	}

	inline static int32_t get_offset_of_executeOnUnclick_13() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___executeOnUnclick_13)); }
	inline bool get_executeOnUnclick_13() const { return ___executeOnUnclick_13; }
	inline bool* get_address_of_executeOnUnclick_13() { return &___executeOnUnclick_13; }
	inline void set_executeOnUnclick_13(bool value)
	{
		___executeOnUnclick_13 = value;
	}

	inline static int32_t get_offset_of_baseHapticStrength_14() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___baseHapticStrength_14)); }
	inline float get_baseHapticStrength_14() const { return ___baseHapticStrength_14; }
	inline float* get_address_of_baseHapticStrength_14() { return &___baseHapticStrength_14; }
	inline void set_baseHapticStrength_14(float value)
	{
		___baseHapticStrength_14 = value;
	}

	inline static int32_t get_offset_of_FireHapticPulse_15() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___FireHapticPulse_15)); }
	inline HapticPulseEventHandler_t422205650 * get_FireHapticPulse_15() const { return ___FireHapticPulse_15; }
	inline HapticPulseEventHandler_t422205650 ** get_address_of_FireHapticPulse_15() { return &___FireHapticPulse_15; }
	inline void set_FireHapticPulse_15(HapticPulseEventHandler_t422205650 * value)
	{
		___FireHapticPulse_15 = value;
		Il2CppCodeGenWriteBarrier(&___FireHapticPulse_15, value);
	}

	inline static int32_t get_offset_of_menuButtons_16() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___menuButtons_16)); }
	inline List_1_t1125654279 * get_menuButtons_16() const { return ___menuButtons_16; }
	inline List_1_t1125654279 ** get_address_of_menuButtons_16() { return &___menuButtons_16; }
	inline void set_menuButtons_16(List_1_t1125654279 * value)
	{
		___menuButtons_16 = value;
		Il2CppCodeGenWriteBarrier(&___menuButtons_16, value);
	}

	inline static int32_t get_offset_of_currentHover_17() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___currentHover_17)); }
	inline int32_t get_currentHover_17() const { return ___currentHover_17; }
	inline int32_t* get_address_of_currentHover_17() { return &___currentHover_17; }
	inline void set_currentHover_17(int32_t value)
	{
		___currentHover_17 = value;
	}

	inline static int32_t get_offset_of_currentPress_18() { return static_cast<int32_t>(offsetof(RadialMenu_t1576296262, ___currentPress_18)); }
	inline int32_t get_currentPress_18() const { return ___currentPress_18; }
	inline int32_t* get_address_of_currentPress_18() { return &___currentPress_18; }
	inline void set_currentPress_18(int32_t value)
	{
		___currentPress_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
