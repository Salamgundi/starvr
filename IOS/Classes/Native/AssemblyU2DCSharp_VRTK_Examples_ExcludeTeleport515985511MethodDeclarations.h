﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.ExcludeTeleport
struct ExcludeTeleport_t515985511;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.Examples.ExcludeTeleport::.ctor()
extern "C"  void ExcludeTeleport__ctor_m2045297592 (ExcludeTeleport_t515985511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
