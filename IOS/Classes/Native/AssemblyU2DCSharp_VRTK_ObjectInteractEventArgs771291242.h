﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.ObjectInteractEventArgs
struct  ObjectInteractEventArgs_t771291242 
{
public:
	// System.UInt32 VRTK.ObjectInteractEventArgs::controllerIndex
	uint32_t ___controllerIndex_0;
	// UnityEngine.GameObject VRTK.ObjectInteractEventArgs::target
	GameObject_t1756533147 * ___target_1;

public:
	inline static int32_t get_offset_of_controllerIndex_0() { return static_cast<int32_t>(offsetof(ObjectInteractEventArgs_t771291242, ___controllerIndex_0)); }
	inline uint32_t get_controllerIndex_0() const { return ___controllerIndex_0; }
	inline uint32_t* get_address_of_controllerIndex_0() { return &___controllerIndex_0; }
	inline void set_controllerIndex_0(uint32_t value)
	{
		___controllerIndex_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(ObjectInteractEventArgs_t771291242, ___target_1)); }
	inline GameObject_t1756533147 * get_target_1() const { return ___target_1; }
	inline GameObject_t1756533147 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(GameObject_t1756533147 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier(&___target_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VRTK.ObjectInteractEventArgs
struct ObjectInteractEventArgs_t771291242_marshaled_pinvoke
{
	uint32_t ___controllerIndex_0;
	GameObject_t1756533147 * ___target_1;
};
// Native definition for COM marshalling of VRTK.ObjectInteractEventArgs
struct ObjectInteractEventArgs_t771291242_marshaled_com
{
	uint32_t ___controllerIndex_0;
	GameObject_t1756533147 * ___target_1;
};
