﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.LinearAnimation
struct LinearAnimation_t3384090715;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.LinearAnimation::.ctor()
extern "C"  void LinearAnimation__ctor_m105452081 (LinearAnimation_t3384090715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearAnimation::Awake()
extern "C"  void LinearAnimation_Awake_m736585132 (LinearAnimation_t3384090715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearAnimation::Update()
extern "C"  void LinearAnimation_Update_m2717943162 (LinearAnimation_t3384090715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
