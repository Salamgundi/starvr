﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Render
struct SteamVR_Render_t595857297;
// SteamVR_Camera
struct SteamVR_Camera_t3632348390;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"
#include "AssemblyU2DCSharp_SteamVR_Camera3632348390.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_t3405266389.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotPropertyFile29427162.h"

// System.Void SteamVR_Render::.ctor()
extern "C"  void SteamVR_Render__ctor_m3396417732 (SteamVR_Render_t595857297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVREye SteamVR_Render::get_eye()
extern "C"  int32_t SteamVR_Render_get_eye_m2885568099 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::set_eye(Valve.VR.EVREye)
extern "C"  void SteamVR_Render_set_eye_m1358594162 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Render SteamVR_Render::get_instance()
extern "C"  SteamVR_Render_t595857297 * SteamVR_Render_get_instance_m3172241352 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::OnDestroy()
extern "C"  void SteamVR_Render_OnDestroy_m2328840183 (SteamVR_Render_t595857297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::OnApplicationQuit()
extern "C"  void SteamVR_Render_OnApplicationQuit_m2515683598 (SteamVR_Render_t595857297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::Add(SteamVR_Camera)
extern "C"  void SteamVR_Render_Add_m1168296161 (Il2CppObject * __this /* static, unused */, SteamVR_Camera_t3632348390 * ___vrcam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::Remove(SteamVR_Camera)
extern "C"  void SteamVR_Render_Remove_m3247626700 (Il2CppObject * __this /* static, unused */, SteamVR_Camera_t3632348390 * ___vrcam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Camera SteamVR_Render::Top()
extern "C"  SteamVR_Camera_t3632348390 * SteamVR_Render_Top_m1741751696 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::AddInternal(SteamVR_Camera)
extern "C"  void SteamVR_Render_AddInternal_m276752988 (SteamVR_Render_t595857297 * __this, SteamVR_Camera_t3632348390 * ___vrcam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::RemoveInternal(SteamVR_Camera)
extern "C"  void SteamVR_Render_RemoveInternal_m812086681 (SteamVR_Render_t595857297 * __this, SteamVR_Camera_t3632348390 * ___vrcam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Camera SteamVR_Render::TopInternal()
extern "C"  SteamVR_Camera_t3632348390 * SteamVR_Render_TopInternal_m4170070143 (SteamVR_Render_t595857297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Render::get_pauseRendering()
extern "C"  bool SteamVR_Render_get_pauseRendering_m4130893145 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::set_pauseRendering(System.Boolean)
extern "C"  void SteamVR_Render_set_pauseRendering_m3477763756 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SteamVR_Render::RenderLoop()
extern "C"  Il2CppObject * SteamVR_Render_RenderLoop_m3701554336 (SteamVR_Render_t595857297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::RenderExternalCamera()
extern "C"  void SteamVR_Render_RenderExternalCamera_m649473618 (SteamVR_Render_t595857297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::OnInputFocus(System.Boolean)
extern "C"  void SteamVR_Render_OnInputFocus_m3320465190 (SteamVR_Render_t595857297 * __this, bool ___hasFocus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::OnQuit(Valve.VR.VREvent_t)
extern "C"  void SteamVR_Render_OnQuit_m2317321267 (SteamVR_Render_t595857297 * __this, VREvent_t_t3405266389  ___vrEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SteamVR_Render::GetScreenshotFilename(System.UInt32,Valve.VR.EVRScreenshotPropertyFilenames)
extern "C"  String_t* SteamVR_Render_GetScreenshotFilename_m1953453570 (SteamVR_Render_t595857297 * __this, uint32_t ___screenshotHandle0, int32_t ___screenshotPropertyFilename1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::OnRequestScreenshot(Valve.VR.VREvent_t)
extern "C"  void SteamVR_Render_OnRequestScreenshot_m2225608391 (SteamVR_Render_t595857297 * __this, VREvent_t_t3405266389  ___vrEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::OnEnable()
extern "C"  void SteamVR_Render_OnEnable_m484463228 (SteamVR_Render_t595857297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::OnDisable()
extern "C"  void SteamVR_Render_OnDisable_m2670685537 (SteamVR_Render_t595857297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::Awake()
extern "C"  void SteamVR_Render_Awake_m4030499569 (SteamVR_Render_t595857297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render::Update()
extern "C"  void SteamVR_Render_Update_m3694930365 (SteamVR_Render_t595857297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
