﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;

#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Sword
struct  Sword_t3542381029  : public VRTK_InteractableObject_t2604188111
{
public:
	// VRTK.VRTK_ControllerActions VRTK.Examples.Sword::controllerActions
	VRTK_ControllerActions_t3642353851 * ___controllerActions_45;
	// System.Single VRTK.Examples.Sword::impactMagnifier
	float ___impactMagnifier_46;
	// System.Single VRTK.Examples.Sword::collisionForce
	float ___collisionForce_47;
	// System.Single VRTK.Examples.Sword::maxCollisionForce
	float ___maxCollisionForce_48;

public:
	inline static int32_t get_offset_of_controllerActions_45() { return static_cast<int32_t>(offsetof(Sword_t3542381029, ___controllerActions_45)); }
	inline VRTK_ControllerActions_t3642353851 * get_controllerActions_45() const { return ___controllerActions_45; }
	inline VRTK_ControllerActions_t3642353851 ** get_address_of_controllerActions_45() { return &___controllerActions_45; }
	inline void set_controllerActions_45(VRTK_ControllerActions_t3642353851 * value)
	{
		___controllerActions_45 = value;
		Il2CppCodeGenWriteBarrier(&___controllerActions_45, value);
	}

	inline static int32_t get_offset_of_impactMagnifier_46() { return static_cast<int32_t>(offsetof(Sword_t3542381029, ___impactMagnifier_46)); }
	inline float get_impactMagnifier_46() const { return ___impactMagnifier_46; }
	inline float* get_address_of_impactMagnifier_46() { return &___impactMagnifier_46; }
	inline void set_impactMagnifier_46(float value)
	{
		___impactMagnifier_46 = value;
	}

	inline static int32_t get_offset_of_collisionForce_47() { return static_cast<int32_t>(offsetof(Sword_t3542381029, ___collisionForce_47)); }
	inline float get_collisionForce_47() const { return ___collisionForce_47; }
	inline float* get_address_of_collisionForce_47() { return &___collisionForce_47; }
	inline void set_collisionForce_47(float value)
	{
		___collisionForce_47 = value;
	}

	inline static int32_t get_offset_of_maxCollisionForce_48() { return static_cast<int32_t>(offsetof(Sword_t3542381029, ___maxCollisionForce_48)); }
	inline float get_maxCollisionForce_48() const { return ___maxCollisionForce_48; }
	inline float* get_address_of_maxCollisionForce_48() { return &___maxCollisionForce_48; }
	inline void set_maxCollisionForce_48(float value)
	{
		___maxCollisionForce_48 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
