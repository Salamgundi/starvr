﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_GetWorkingPlayAreaSize
struct _GetWorkingPlayAreaSize_t1540358174;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_GetWorkingPlayAreaSize::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetWorkingPlayAreaSize__ctor_m1682697333 (_GetWorkingPlayAreaSize_t1540358174 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetWorkingPlayAreaSize::Invoke(System.Single&,System.Single&)
extern "C"  bool _GetWorkingPlayAreaSize_Invoke_m3324064105 (_GetWorkingPlayAreaSize_t1540358174 * __this, float* ___pSizeX0, float* ___pSizeZ1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_GetWorkingPlayAreaSize::BeginInvoke(System.Single&,System.Single&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetWorkingPlayAreaSize_BeginInvoke_m2562586030 (_GetWorkingPlayAreaSize_t1540358174 * __this, float* ___pSizeX0, float* ___pSizeZ1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetWorkingPlayAreaSize::EndInvoke(System.Single&,System.Single&,System.IAsyncResult)
extern "C"  bool _GetWorkingPlayAreaSize_EndInvoke_m3097564719 (_GetWorkingPlayAreaSize_t1540358174 * __this, float* ___pSizeX0, float* ___pSizeZ1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
