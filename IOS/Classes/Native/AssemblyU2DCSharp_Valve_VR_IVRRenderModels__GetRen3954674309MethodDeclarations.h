﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_GetRenderModelThumbnailURL
struct _GetRenderModelThumbnailURL_t3954674309;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRRenderModelError21703732.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_GetRenderModelThumbnailURL::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetRenderModelThumbnailURL__ctor_m4077953000 (_GetRenderModelThumbnailURL_t3954674309 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelThumbnailURL::Invoke(System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRRenderModelError&)
extern "C"  uint32_t _GetRenderModelThumbnailURL_Invoke_m560263279 (_GetRenderModelThumbnailURL_t3954674309 * __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchThumbnailURL1, uint32_t ___unThumbnailURLLen2, int32_t* ___peError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetRenderModelThumbnailURL::BeginInvoke(System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRRenderModelError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetRenderModelThumbnailURL_BeginInvoke_m4092992911 (_GetRenderModelThumbnailURL_t3954674309 * __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchThumbnailURL1, uint32_t ___unThumbnailURLLen2, int32_t* ___peError3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelThumbnailURL::EndInvoke(Valve.VR.EVRRenderModelError&,System.IAsyncResult)
extern "C"  uint32_t _GetRenderModelThumbnailURL_EndInvoke_m1079584463 (_GetRenderModelThumbnailURL_t3954674309 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
