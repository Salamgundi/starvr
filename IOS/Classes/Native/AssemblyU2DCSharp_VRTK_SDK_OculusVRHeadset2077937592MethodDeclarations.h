﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_OculusVRHeadset
struct SDK_OculusVRHeadset_t2077937592;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_OculusVRHeadset::.ctor()
extern "C"  void SDK_OculusVRHeadset__ctor_m702920670 (SDK_OculusVRHeadset_t2077937592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
