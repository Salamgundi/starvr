﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Single[]
struct SingleU5BU5D_t577127397;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_AdaptiveQuality/Timing
struct  Timing_t3878883323  : public Il2CppObject
{
public:
	// System.Single[] VRTK.VRTK_AdaptiveQuality/Timing::buffer
	SingleU5BU5D_t577127397* ___buffer_0;
	// System.Int32 VRTK.VRTK_AdaptiveQuality/Timing::bufferIndex
	int32_t ___bufferIndex_1;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(Timing_t3878883323, ___buffer_0)); }
	inline SingleU5BU5D_t577127397* get_buffer_0() const { return ___buffer_0; }
	inline SingleU5BU5D_t577127397** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(SingleU5BU5D_t577127397* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_0, value);
	}

	inline static int32_t get_offset_of_bufferIndex_1() { return static_cast<int32_t>(offsetof(Timing_t3878883323, ___bufferIndex_1)); }
	inline int32_t get_bufferIndex_1() const { return ___bufferIndex_1; }
	inline int32_t* get_address_of_bufferIndex_1() { return &___bufferIndex_1; }
	inline void set_bufferIndex_1(int32_t value)
	{
		___bufferIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
