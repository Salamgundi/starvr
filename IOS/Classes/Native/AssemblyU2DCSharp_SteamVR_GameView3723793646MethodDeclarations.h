﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_GameView
struct SteamVR_GameView_t3723793646;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_GameView::.ctor()
extern "C"  void SteamVR_GameView__ctor_m2487502897 (SteamVR_GameView_t3723793646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_GameView::Awake()
extern "C"  void SteamVR_GameView_Awake_m3119419806 (SteamVR_GameView_t3723793646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
