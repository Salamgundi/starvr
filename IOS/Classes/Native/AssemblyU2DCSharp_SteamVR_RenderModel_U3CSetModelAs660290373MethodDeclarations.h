﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_RenderModel/<SetModelAsync>c__Iterator0
struct U3CSetModelAsyncU3Ec__Iterator0_t660290373;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_RenderModel/<SetModelAsync>c__Iterator0::.ctor()
extern "C"  void U3CSetModelAsyncU3Ec__Iterator0__ctor_m678481168 (U3CSetModelAsyncU3Ec__Iterator0_t660290373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_RenderModel/<SetModelAsync>c__Iterator0::MoveNext()
extern "C"  bool U3CSetModelAsyncU3Ec__Iterator0_MoveNext_m3364346752 (U3CSetModelAsyncU3Ec__Iterator0_t660290373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SteamVR_RenderModel/<SetModelAsync>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSetModelAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m375774518 (U3CSetModelAsyncU3Ec__Iterator0_t660290373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SteamVR_RenderModel/<SetModelAsync>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSetModelAsyncU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m764284766 (U3CSetModelAsyncU3Ec__Iterator0_t660290373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel/<SetModelAsync>c__Iterator0::Dispose()
extern "C"  void U3CSetModelAsyncU3Ec__Iterator0_Dispose_m2091229887 (U3CSetModelAsyncU3Ec__Iterator0_t660290373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel/<SetModelAsync>c__Iterator0::Reset()
extern "C"  void U3CSetModelAsyncU3Ec__Iterator0_Reset_m2246099661 (U3CSetModelAsyncU3Ec__Iterator0_t660290373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel/<SetModelAsync>c__Iterator0::<>__Finally0()
extern "C"  void U3CSetModelAsyncU3Ec__Iterator0_U3CU3E__Finally0_m3868843563 (U3CSetModelAsyncU3Ec__Iterator0_t660290373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
