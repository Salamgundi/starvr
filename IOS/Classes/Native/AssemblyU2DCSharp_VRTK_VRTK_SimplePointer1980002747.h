﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_VRTK_VRTK_BasePointer3466805540.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_SimplePointer
struct  VRTK_SimplePointer_t1980002747  : public VRTK_BasePointer_t3466805540
{
public:
	// System.Single VRTK.VRTK_SimplePointer::pointerThickness
	float ___pointerThickness_41;
	// System.Single VRTK.VRTK_SimplePointer::pointerLength
	float ___pointerLength_42;
	// System.Boolean VRTK.VRTK_SimplePointer::showPointerTip
	bool ___showPointerTip_43;
	// UnityEngine.GameObject VRTK.VRTK_SimplePointer::customPointerCursor
	GameObject_t1756533147 * ___customPointerCursor_44;
	// System.Boolean VRTK.VRTK_SimplePointer::pointerCursorMatchTargetNormal
	bool ___pointerCursorMatchTargetNormal_45;
	// System.Boolean VRTK.VRTK_SimplePointer::pointerCursorRescaledAlongDistance
	bool ___pointerCursorRescaledAlongDistance_46;
	// UnityEngine.GameObject VRTK.VRTK_SimplePointer::pointerHolder
	GameObject_t1756533147 * ___pointerHolder_47;
	// UnityEngine.GameObject VRTK.VRTK_SimplePointer::pointerBeam
	GameObject_t1756533147 * ___pointerBeam_48;
	// UnityEngine.GameObject VRTK.VRTK_SimplePointer::pointerTip
	GameObject_t1756533147 * ___pointerTip_49;
	// UnityEngine.Vector3 VRTK.VRTK_SimplePointer::pointerTipScale
	Vector3_t2243707580  ___pointerTipScale_50;
	// UnityEngine.Vector3 VRTK.VRTK_SimplePointer::pointerCursorOriginalScale
	Vector3_t2243707580  ___pointerCursorOriginalScale_51;
	// System.Boolean VRTK.VRTK_SimplePointer::activeEnabled
	bool ___activeEnabled_52;
	// System.Boolean VRTK.VRTK_SimplePointer::storedBeamState
	bool ___storedBeamState_53;
	// System.Boolean VRTK.VRTK_SimplePointer::storedTipState
	bool ___storedTipState_54;

public:
	inline static int32_t get_offset_of_pointerThickness_41() { return static_cast<int32_t>(offsetof(VRTK_SimplePointer_t1980002747, ___pointerThickness_41)); }
	inline float get_pointerThickness_41() const { return ___pointerThickness_41; }
	inline float* get_address_of_pointerThickness_41() { return &___pointerThickness_41; }
	inline void set_pointerThickness_41(float value)
	{
		___pointerThickness_41 = value;
	}

	inline static int32_t get_offset_of_pointerLength_42() { return static_cast<int32_t>(offsetof(VRTK_SimplePointer_t1980002747, ___pointerLength_42)); }
	inline float get_pointerLength_42() const { return ___pointerLength_42; }
	inline float* get_address_of_pointerLength_42() { return &___pointerLength_42; }
	inline void set_pointerLength_42(float value)
	{
		___pointerLength_42 = value;
	}

	inline static int32_t get_offset_of_showPointerTip_43() { return static_cast<int32_t>(offsetof(VRTK_SimplePointer_t1980002747, ___showPointerTip_43)); }
	inline bool get_showPointerTip_43() const { return ___showPointerTip_43; }
	inline bool* get_address_of_showPointerTip_43() { return &___showPointerTip_43; }
	inline void set_showPointerTip_43(bool value)
	{
		___showPointerTip_43 = value;
	}

	inline static int32_t get_offset_of_customPointerCursor_44() { return static_cast<int32_t>(offsetof(VRTK_SimplePointer_t1980002747, ___customPointerCursor_44)); }
	inline GameObject_t1756533147 * get_customPointerCursor_44() const { return ___customPointerCursor_44; }
	inline GameObject_t1756533147 ** get_address_of_customPointerCursor_44() { return &___customPointerCursor_44; }
	inline void set_customPointerCursor_44(GameObject_t1756533147 * value)
	{
		___customPointerCursor_44 = value;
		Il2CppCodeGenWriteBarrier(&___customPointerCursor_44, value);
	}

	inline static int32_t get_offset_of_pointerCursorMatchTargetNormal_45() { return static_cast<int32_t>(offsetof(VRTK_SimplePointer_t1980002747, ___pointerCursorMatchTargetNormal_45)); }
	inline bool get_pointerCursorMatchTargetNormal_45() const { return ___pointerCursorMatchTargetNormal_45; }
	inline bool* get_address_of_pointerCursorMatchTargetNormal_45() { return &___pointerCursorMatchTargetNormal_45; }
	inline void set_pointerCursorMatchTargetNormal_45(bool value)
	{
		___pointerCursorMatchTargetNormal_45 = value;
	}

	inline static int32_t get_offset_of_pointerCursorRescaledAlongDistance_46() { return static_cast<int32_t>(offsetof(VRTK_SimplePointer_t1980002747, ___pointerCursorRescaledAlongDistance_46)); }
	inline bool get_pointerCursorRescaledAlongDistance_46() const { return ___pointerCursorRescaledAlongDistance_46; }
	inline bool* get_address_of_pointerCursorRescaledAlongDistance_46() { return &___pointerCursorRescaledAlongDistance_46; }
	inline void set_pointerCursorRescaledAlongDistance_46(bool value)
	{
		___pointerCursorRescaledAlongDistance_46 = value;
	}

	inline static int32_t get_offset_of_pointerHolder_47() { return static_cast<int32_t>(offsetof(VRTK_SimplePointer_t1980002747, ___pointerHolder_47)); }
	inline GameObject_t1756533147 * get_pointerHolder_47() const { return ___pointerHolder_47; }
	inline GameObject_t1756533147 ** get_address_of_pointerHolder_47() { return &___pointerHolder_47; }
	inline void set_pointerHolder_47(GameObject_t1756533147 * value)
	{
		___pointerHolder_47 = value;
		Il2CppCodeGenWriteBarrier(&___pointerHolder_47, value);
	}

	inline static int32_t get_offset_of_pointerBeam_48() { return static_cast<int32_t>(offsetof(VRTK_SimplePointer_t1980002747, ___pointerBeam_48)); }
	inline GameObject_t1756533147 * get_pointerBeam_48() const { return ___pointerBeam_48; }
	inline GameObject_t1756533147 ** get_address_of_pointerBeam_48() { return &___pointerBeam_48; }
	inline void set_pointerBeam_48(GameObject_t1756533147 * value)
	{
		___pointerBeam_48 = value;
		Il2CppCodeGenWriteBarrier(&___pointerBeam_48, value);
	}

	inline static int32_t get_offset_of_pointerTip_49() { return static_cast<int32_t>(offsetof(VRTK_SimplePointer_t1980002747, ___pointerTip_49)); }
	inline GameObject_t1756533147 * get_pointerTip_49() const { return ___pointerTip_49; }
	inline GameObject_t1756533147 ** get_address_of_pointerTip_49() { return &___pointerTip_49; }
	inline void set_pointerTip_49(GameObject_t1756533147 * value)
	{
		___pointerTip_49 = value;
		Il2CppCodeGenWriteBarrier(&___pointerTip_49, value);
	}

	inline static int32_t get_offset_of_pointerTipScale_50() { return static_cast<int32_t>(offsetof(VRTK_SimplePointer_t1980002747, ___pointerTipScale_50)); }
	inline Vector3_t2243707580  get_pointerTipScale_50() const { return ___pointerTipScale_50; }
	inline Vector3_t2243707580 * get_address_of_pointerTipScale_50() { return &___pointerTipScale_50; }
	inline void set_pointerTipScale_50(Vector3_t2243707580  value)
	{
		___pointerTipScale_50 = value;
	}

	inline static int32_t get_offset_of_pointerCursorOriginalScale_51() { return static_cast<int32_t>(offsetof(VRTK_SimplePointer_t1980002747, ___pointerCursorOriginalScale_51)); }
	inline Vector3_t2243707580  get_pointerCursorOriginalScale_51() const { return ___pointerCursorOriginalScale_51; }
	inline Vector3_t2243707580 * get_address_of_pointerCursorOriginalScale_51() { return &___pointerCursorOriginalScale_51; }
	inline void set_pointerCursorOriginalScale_51(Vector3_t2243707580  value)
	{
		___pointerCursorOriginalScale_51 = value;
	}

	inline static int32_t get_offset_of_activeEnabled_52() { return static_cast<int32_t>(offsetof(VRTK_SimplePointer_t1980002747, ___activeEnabled_52)); }
	inline bool get_activeEnabled_52() const { return ___activeEnabled_52; }
	inline bool* get_address_of_activeEnabled_52() { return &___activeEnabled_52; }
	inline void set_activeEnabled_52(bool value)
	{
		___activeEnabled_52 = value;
	}

	inline static int32_t get_offset_of_storedBeamState_53() { return static_cast<int32_t>(offsetof(VRTK_SimplePointer_t1980002747, ___storedBeamState_53)); }
	inline bool get_storedBeamState_53() const { return ___storedBeamState_53; }
	inline bool* get_address_of_storedBeamState_53() { return &___storedBeamState_53; }
	inline void set_storedBeamState_53(bool value)
	{
		___storedBeamState_53 = value;
	}

	inline static int32_t get_offset_of_storedTipState_54() { return static_cast<int32_t>(offsetof(VRTK_SimplePointer_t1980002747, ___storedTipState_54)); }
	inline bool get_storedTipState_54() const { return ___storedTipState_54; }
	inline bool* get_address_of_storedTipState_54() { return &___storedTipState_54; }
	inline void set_storedTipState_54(bool value)
	{
		___storedTipState_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
