﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_ItemP4052302202.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.ItemPackage
struct  ItemPackage_t3423754743  : public MonoBehaviour_t1158329972
{
public:
	// System.String Valve.VR.InteractionSystem.ItemPackage::name
	String_t* ___name_2;
	// Valve.VR.InteractionSystem.ItemPackage/ItemPackageType Valve.VR.InteractionSystem.ItemPackage::packageType
	int32_t ___packageType_3;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.ItemPackage::itemPrefab
	GameObject_t1756533147 * ___itemPrefab_4;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.ItemPackage::otherHandItemPrefab
	GameObject_t1756533147 * ___otherHandItemPrefab_5;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.ItemPackage::previewPrefab
	GameObject_t1756533147 * ___previewPrefab_6;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.ItemPackage::fadedPreviewPrefab
	GameObject_t1756533147 * ___fadedPreviewPrefab_7;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(ItemPackage_t3423754743, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_packageType_3() { return static_cast<int32_t>(offsetof(ItemPackage_t3423754743, ___packageType_3)); }
	inline int32_t get_packageType_3() const { return ___packageType_3; }
	inline int32_t* get_address_of_packageType_3() { return &___packageType_3; }
	inline void set_packageType_3(int32_t value)
	{
		___packageType_3 = value;
	}

	inline static int32_t get_offset_of_itemPrefab_4() { return static_cast<int32_t>(offsetof(ItemPackage_t3423754743, ___itemPrefab_4)); }
	inline GameObject_t1756533147 * get_itemPrefab_4() const { return ___itemPrefab_4; }
	inline GameObject_t1756533147 ** get_address_of_itemPrefab_4() { return &___itemPrefab_4; }
	inline void set_itemPrefab_4(GameObject_t1756533147 * value)
	{
		___itemPrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_4, value);
	}

	inline static int32_t get_offset_of_otherHandItemPrefab_5() { return static_cast<int32_t>(offsetof(ItemPackage_t3423754743, ___otherHandItemPrefab_5)); }
	inline GameObject_t1756533147 * get_otherHandItemPrefab_5() const { return ___otherHandItemPrefab_5; }
	inline GameObject_t1756533147 ** get_address_of_otherHandItemPrefab_5() { return &___otherHandItemPrefab_5; }
	inline void set_otherHandItemPrefab_5(GameObject_t1756533147 * value)
	{
		___otherHandItemPrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___otherHandItemPrefab_5, value);
	}

	inline static int32_t get_offset_of_previewPrefab_6() { return static_cast<int32_t>(offsetof(ItemPackage_t3423754743, ___previewPrefab_6)); }
	inline GameObject_t1756533147 * get_previewPrefab_6() const { return ___previewPrefab_6; }
	inline GameObject_t1756533147 ** get_address_of_previewPrefab_6() { return &___previewPrefab_6; }
	inline void set_previewPrefab_6(GameObject_t1756533147 * value)
	{
		___previewPrefab_6 = value;
		Il2CppCodeGenWriteBarrier(&___previewPrefab_6, value);
	}

	inline static int32_t get_offset_of_fadedPreviewPrefab_7() { return static_cast<int32_t>(offsetof(ItemPackage_t3423754743, ___fadedPreviewPrefab_7)); }
	inline GameObject_t1756533147 * get_fadedPreviewPrefab_7() const { return ___fadedPreviewPrefab_7; }
	inline GameObject_t1756533147 ** get_address_of_fadedPreviewPrefab_7() { return &___fadedPreviewPrefab_7; }
	inline void set_fadedPreviewPrefab_7(GameObject_t1756533147 * value)
	{
		___fadedPreviewPrefab_7 = value;
		Il2CppCodeGenWriteBarrier(&___fadedPreviewPrefab_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
