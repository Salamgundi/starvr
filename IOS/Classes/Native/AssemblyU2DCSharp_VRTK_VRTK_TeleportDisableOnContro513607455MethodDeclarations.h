﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_TeleportDisableOnControllerObscured/<EnableAtEndOfFrame>c__Iterator0
struct U3CEnableAtEndOfFrameU3Ec__Iterator0_t513607455;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_TeleportDisableOnControllerObscured/<EnableAtEndOfFrame>c__Iterator0::.ctor()
extern "C"  void U3CEnableAtEndOfFrameU3Ec__Iterator0__ctor_m2251044958 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t513607455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_TeleportDisableOnControllerObscured/<EnableAtEndOfFrame>c__Iterator0::MoveNext()
extern "C"  bool U3CEnableAtEndOfFrameU3Ec__Iterator0_MoveNext_m1054597846 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t513607455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_TeleportDisableOnControllerObscured/<EnableAtEndOfFrame>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEnableAtEndOfFrameU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1434515950 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t513607455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_TeleportDisableOnControllerObscured/<EnableAtEndOfFrame>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEnableAtEndOfFrameU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2018204342 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t513607455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TeleportDisableOnControllerObscured/<EnableAtEndOfFrame>c__Iterator0::Dispose()
extern "C"  void U3CEnableAtEndOfFrameU3Ec__Iterator0_Dispose_m3784932309 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t513607455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TeleportDisableOnControllerObscured/<EnableAtEndOfFrame>c__Iterator0::Reset()
extern "C"  void U3CEnableAtEndOfFrameU3Ec__Iterator0_Reset_m3971140527 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t513607455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
