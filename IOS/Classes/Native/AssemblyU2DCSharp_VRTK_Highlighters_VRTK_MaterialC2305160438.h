﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]>
struct Dictionary_2_t743801652;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Coroutine>
struct Dictionary_2_t4214288102;

#include "AssemblyU2DCSharp_VRTK_Highlighters_VRTK_BaseHighl3110203740.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter
struct  VRTK_MaterialColorSwapHighlighter_t2305160438  : public VRTK_BaseHighlighter_t3110203740
{
public:
	// System.Single VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter::emissionDarken
	float ___emissionDarken_5;
	// UnityEngine.Material VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter::customMaterial
	Material_t193706927 * ___customMaterial_6;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]> VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter::originalSharedRendererMaterials
	Dictionary_2_t743801652 * ___originalSharedRendererMaterials_7;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material[]> VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter::originalRendererMaterials
	Dictionary_2_t743801652 * ___originalRendererMaterials_8;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Coroutine> VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter::faderRoutines
	Dictionary_2_t4214288102 * ___faderRoutines_9;
	// System.Boolean VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter::resetMainTexture
	bool ___resetMainTexture_10;

public:
	inline static int32_t get_offset_of_emissionDarken_5() { return static_cast<int32_t>(offsetof(VRTK_MaterialColorSwapHighlighter_t2305160438, ___emissionDarken_5)); }
	inline float get_emissionDarken_5() const { return ___emissionDarken_5; }
	inline float* get_address_of_emissionDarken_5() { return &___emissionDarken_5; }
	inline void set_emissionDarken_5(float value)
	{
		___emissionDarken_5 = value;
	}

	inline static int32_t get_offset_of_customMaterial_6() { return static_cast<int32_t>(offsetof(VRTK_MaterialColorSwapHighlighter_t2305160438, ___customMaterial_6)); }
	inline Material_t193706927 * get_customMaterial_6() const { return ___customMaterial_6; }
	inline Material_t193706927 ** get_address_of_customMaterial_6() { return &___customMaterial_6; }
	inline void set_customMaterial_6(Material_t193706927 * value)
	{
		___customMaterial_6 = value;
		Il2CppCodeGenWriteBarrier(&___customMaterial_6, value);
	}

	inline static int32_t get_offset_of_originalSharedRendererMaterials_7() { return static_cast<int32_t>(offsetof(VRTK_MaterialColorSwapHighlighter_t2305160438, ___originalSharedRendererMaterials_7)); }
	inline Dictionary_2_t743801652 * get_originalSharedRendererMaterials_7() const { return ___originalSharedRendererMaterials_7; }
	inline Dictionary_2_t743801652 ** get_address_of_originalSharedRendererMaterials_7() { return &___originalSharedRendererMaterials_7; }
	inline void set_originalSharedRendererMaterials_7(Dictionary_2_t743801652 * value)
	{
		___originalSharedRendererMaterials_7 = value;
		Il2CppCodeGenWriteBarrier(&___originalSharedRendererMaterials_7, value);
	}

	inline static int32_t get_offset_of_originalRendererMaterials_8() { return static_cast<int32_t>(offsetof(VRTK_MaterialColorSwapHighlighter_t2305160438, ___originalRendererMaterials_8)); }
	inline Dictionary_2_t743801652 * get_originalRendererMaterials_8() const { return ___originalRendererMaterials_8; }
	inline Dictionary_2_t743801652 ** get_address_of_originalRendererMaterials_8() { return &___originalRendererMaterials_8; }
	inline void set_originalRendererMaterials_8(Dictionary_2_t743801652 * value)
	{
		___originalRendererMaterials_8 = value;
		Il2CppCodeGenWriteBarrier(&___originalRendererMaterials_8, value);
	}

	inline static int32_t get_offset_of_faderRoutines_9() { return static_cast<int32_t>(offsetof(VRTK_MaterialColorSwapHighlighter_t2305160438, ___faderRoutines_9)); }
	inline Dictionary_2_t4214288102 * get_faderRoutines_9() const { return ___faderRoutines_9; }
	inline Dictionary_2_t4214288102 ** get_address_of_faderRoutines_9() { return &___faderRoutines_9; }
	inline void set_faderRoutines_9(Dictionary_2_t4214288102 * value)
	{
		___faderRoutines_9 = value;
		Il2CppCodeGenWriteBarrier(&___faderRoutines_9, value);
	}

	inline static int32_t get_offset_of_resetMainTexture_10() { return static_cast<int32_t>(offsetof(VRTK_MaterialColorSwapHighlighter_t2305160438, ___resetMainTexture_10)); }
	inline bool get_resetMainTexture_10() const { return ___resetMainTexture_10; }
	inline bool* get_address_of_resetMainTexture_10() { return &___resetMainTexture_10; }
	inline void set_resetMainTexture_10(bool value)
	{
		___resetMainTexture_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
