﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Flarez
struct Flarez_t1047003190;

#include "codegen/il2cpp-codegen.h"

// System.Void Flarez::.ctor()
extern "C"  void Flarez__ctor_m3894567733 (Flarez_t1047003190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Flarez::Start()
extern "C"  void Flarez_Start_m3166964725 (Flarez_t1047003190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Flarez::addFlarez()
extern "C"  void Flarez_addFlarez_m1274387664 (Flarez_t1047003190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
