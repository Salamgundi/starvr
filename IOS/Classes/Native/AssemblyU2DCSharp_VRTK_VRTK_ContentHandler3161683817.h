﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_Control
struct VRTK_Control_t651619021;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ContentHandler
struct  VRTK_ContentHandler_t3161683817  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_Control VRTK.VRTK_ContentHandler::control
	VRTK_Control_t651619021 * ___control_2;
	// UnityEngine.Transform VRTK.VRTK_ContentHandler::inside
	Transform_t3275118058 * ___inside_3;
	// UnityEngine.Transform VRTK.VRTK_ContentHandler::outside
	Transform_t3275118058 * ___outside_4;

public:
	inline static int32_t get_offset_of_control_2() { return static_cast<int32_t>(offsetof(VRTK_ContentHandler_t3161683817, ___control_2)); }
	inline VRTK_Control_t651619021 * get_control_2() const { return ___control_2; }
	inline VRTK_Control_t651619021 ** get_address_of_control_2() { return &___control_2; }
	inline void set_control_2(VRTK_Control_t651619021 * value)
	{
		___control_2 = value;
		Il2CppCodeGenWriteBarrier(&___control_2, value);
	}

	inline static int32_t get_offset_of_inside_3() { return static_cast<int32_t>(offsetof(VRTK_ContentHandler_t3161683817, ___inside_3)); }
	inline Transform_t3275118058 * get_inside_3() const { return ___inside_3; }
	inline Transform_t3275118058 ** get_address_of_inside_3() { return &___inside_3; }
	inline void set_inside_3(Transform_t3275118058 * value)
	{
		___inside_3 = value;
		Il2CppCodeGenWriteBarrier(&___inside_3, value);
	}

	inline static int32_t get_offset_of_outside_4() { return static_cast<int32_t>(offsetof(VRTK_ContentHandler_t3161683817, ___outside_4)); }
	inline Transform_t3275118058 * get_outside_4() const { return ___outside_4; }
	inline Transform_t3275118058 ** get_address_of_outside_4() { return &___outside_4; }
	inline void set_outside_4(Transform_t3275118058 * value)
	{
		___outside_4 = value;
		Il2CppCodeGenWriteBarrier(&___outside_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
