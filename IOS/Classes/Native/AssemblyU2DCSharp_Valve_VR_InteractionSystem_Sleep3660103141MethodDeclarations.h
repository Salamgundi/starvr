﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.SleepOnAwake
struct SleepOnAwake_t3660103141;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.SleepOnAwake::.ctor()
extern "C"  void SleepOnAwake__ctor_m4012046365 (SleepOnAwake_t3660103141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SleepOnAwake::Awake()
extern "C"  void SleepOnAwake_Awake_m925884822 (SleepOnAwake_t3660103141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
