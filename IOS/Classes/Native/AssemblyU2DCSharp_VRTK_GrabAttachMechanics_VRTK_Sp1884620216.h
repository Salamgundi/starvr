﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Ba1229653768.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.GrabAttachMechanics.VRTK_SpringJointGrabAttach
struct  VRTK_SpringJointGrabAttach_t1884620216  : public VRTK_BaseJointGrabAttach_t1229653768
{
public:
	// System.Single VRTK.GrabAttachMechanics.VRTK_SpringJointGrabAttach::breakForce
	float ___breakForce_21;
	// System.Single VRTK.GrabAttachMechanics.VRTK_SpringJointGrabAttach::strength
	float ___strength_22;
	// System.Single VRTK.GrabAttachMechanics.VRTK_SpringJointGrabAttach::damper
	float ___damper_23;

public:
	inline static int32_t get_offset_of_breakForce_21() { return static_cast<int32_t>(offsetof(VRTK_SpringJointGrabAttach_t1884620216, ___breakForce_21)); }
	inline float get_breakForce_21() const { return ___breakForce_21; }
	inline float* get_address_of_breakForce_21() { return &___breakForce_21; }
	inline void set_breakForce_21(float value)
	{
		___breakForce_21 = value;
	}

	inline static int32_t get_offset_of_strength_22() { return static_cast<int32_t>(offsetof(VRTK_SpringJointGrabAttach_t1884620216, ___strength_22)); }
	inline float get_strength_22() const { return ___strength_22; }
	inline float* get_address_of_strength_22() { return &___strength_22; }
	inline void set_strength_22(float value)
	{
		___strength_22 = value;
	}

	inline static int32_t get_offset_of_damper_23() { return static_cast<int32_t>(offsetof(VRTK_SpringJointGrabAttach_t1884620216, ___damper_23)); }
	inline float get_damper_23() const { return ___damper_23; }
	inline float* get_address_of_damper_23() { return &___damper_23; }
	inline void set_damper_23(float value)
	{
		___damper_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
