﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// VRTK.VRTK_HeadsetCollision
struct VRTK_HeadsetCollision_t2015187094;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_PositionRewind
struct  VRTK_PositionRewind_t3080560492  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VRTK.VRTK_PositionRewind::rewindDelay
	float ___rewindDelay_2;
	// System.Single VRTK.VRTK_PositionRewind::pushbackDistance
	float ___pushbackDistance_3;
	// System.Single VRTK.VRTK_PositionRewind::crouchThreshold
	float ___crouchThreshold_4;
	// UnityEngine.Transform VRTK.VRTK_PositionRewind::headset
	Transform_t3275118058 * ___headset_5;
	// UnityEngine.Transform VRTK.VRTK_PositionRewind::playArea
	Transform_t3275118058 * ___playArea_6;
	// UnityEngine.Rigidbody VRTK.VRTK_PositionRewind::playareaRigidbody
	Rigidbody_t4233889191 * ___playareaRigidbody_7;
	// VRTK.VRTK_HeadsetCollision VRTK.VRTK_PositionRewind::headsetCollision
	VRTK_HeadsetCollision_t2015187094 * ___headsetCollision_8;
	// UnityEngine.Vector3 VRTK.VRTK_PositionRewind::lastGoodStandingPosition
	Vector3_t2243707580  ___lastGoodStandingPosition_9;
	// UnityEngine.Vector3 VRTK.VRTK_PositionRewind::lastGoodHeadsetPosition
	Vector3_t2243707580  ___lastGoodHeadsetPosition_10;
	// System.Single VRTK.VRTK_PositionRewind::highestHeadsetY
	float ___highestHeadsetY_11;
	// System.Single VRTK.VRTK_PositionRewind::lastPlayAreaY
	float ___lastPlayAreaY_12;
	// System.Boolean VRTK.VRTK_PositionRewind::lastGoodPositionSet
	bool ___lastGoodPositionSet_13;
	// System.Boolean VRTK.VRTK_PositionRewind::hasCollided
	bool ___hasCollided_14;
	// System.Boolean VRTK.VRTK_PositionRewind::isColliding
	bool ___isColliding_15;
	// System.Single VRTK.VRTK_PositionRewind::collideTimer
	float ___collideTimer_16;

public:
	inline static int32_t get_offset_of_rewindDelay_2() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___rewindDelay_2)); }
	inline float get_rewindDelay_2() const { return ___rewindDelay_2; }
	inline float* get_address_of_rewindDelay_2() { return &___rewindDelay_2; }
	inline void set_rewindDelay_2(float value)
	{
		___rewindDelay_2 = value;
	}

	inline static int32_t get_offset_of_pushbackDistance_3() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___pushbackDistance_3)); }
	inline float get_pushbackDistance_3() const { return ___pushbackDistance_3; }
	inline float* get_address_of_pushbackDistance_3() { return &___pushbackDistance_3; }
	inline void set_pushbackDistance_3(float value)
	{
		___pushbackDistance_3 = value;
	}

	inline static int32_t get_offset_of_crouchThreshold_4() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___crouchThreshold_4)); }
	inline float get_crouchThreshold_4() const { return ___crouchThreshold_4; }
	inline float* get_address_of_crouchThreshold_4() { return &___crouchThreshold_4; }
	inline void set_crouchThreshold_4(float value)
	{
		___crouchThreshold_4 = value;
	}

	inline static int32_t get_offset_of_headset_5() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___headset_5)); }
	inline Transform_t3275118058 * get_headset_5() const { return ___headset_5; }
	inline Transform_t3275118058 ** get_address_of_headset_5() { return &___headset_5; }
	inline void set_headset_5(Transform_t3275118058 * value)
	{
		___headset_5 = value;
		Il2CppCodeGenWriteBarrier(&___headset_5, value);
	}

	inline static int32_t get_offset_of_playArea_6() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___playArea_6)); }
	inline Transform_t3275118058 * get_playArea_6() const { return ___playArea_6; }
	inline Transform_t3275118058 ** get_address_of_playArea_6() { return &___playArea_6; }
	inline void set_playArea_6(Transform_t3275118058 * value)
	{
		___playArea_6 = value;
		Il2CppCodeGenWriteBarrier(&___playArea_6, value);
	}

	inline static int32_t get_offset_of_playareaRigidbody_7() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___playareaRigidbody_7)); }
	inline Rigidbody_t4233889191 * get_playareaRigidbody_7() const { return ___playareaRigidbody_7; }
	inline Rigidbody_t4233889191 ** get_address_of_playareaRigidbody_7() { return &___playareaRigidbody_7; }
	inline void set_playareaRigidbody_7(Rigidbody_t4233889191 * value)
	{
		___playareaRigidbody_7 = value;
		Il2CppCodeGenWriteBarrier(&___playareaRigidbody_7, value);
	}

	inline static int32_t get_offset_of_headsetCollision_8() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___headsetCollision_8)); }
	inline VRTK_HeadsetCollision_t2015187094 * get_headsetCollision_8() const { return ___headsetCollision_8; }
	inline VRTK_HeadsetCollision_t2015187094 ** get_address_of_headsetCollision_8() { return &___headsetCollision_8; }
	inline void set_headsetCollision_8(VRTK_HeadsetCollision_t2015187094 * value)
	{
		___headsetCollision_8 = value;
		Il2CppCodeGenWriteBarrier(&___headsetCollision_8, value);
	}

	inline static int32_t get_offset_of_lastGoodStandingPosition_9() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___lastGoodStandingPosition_9)); }
	inline Vector3_t2243707580  get_lastGoodStandingPosition_9() const { return ___lastGoodStandingPosition_9; }
	inline Vector3_t2243707580 * get_address_of_lastGoodStandingPosition_9() { return &___lastGoodStandingPosition_9; }
	inline void set_lastGoodStandingPosition_9(Vector3_t2243707580  value)
	{
		___lastGoodStandingPosition_9 = value;
	}

	inline static int32_t get_offset_of_lastGoodHeadsetPosition_10() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___lastGoodHeadsetPosition_10)); }
	inline Vector3_t2243707580  get_lastGoodHeadsetPosition_10() const { return ___lastGoodHeadsetPosition_10; }
	inline Vector3_t2243707580 * get_address_of_lastGoodHeadsetPosition_10() { return &___lastGoodHeadsetPosition_10; }
	inline void set_lastGoodHeadsetPosition_10(Vector3_t2243707580  value)
	{
		___lastGoodHeadsetPosition_10 = value;
	}

	inline static int32_t get_offset_of_highestHeadsetY_11() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___highestHeadsetY_11)); }
	inline float get_highestHeadsetY_11() const { return ___highestHeadsetY_11; }
	inline float* get_address_of_highestHeadsetY_11() { return &___highestHeadsetY_11; }
	inline void set_highestHeadsetY_11(float value)
	{
		___highestHeadsetY_11 = value;
	}

	inline static int32_t get_offset_of_lastPlayAreaY_12() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___lastPlayAreaY_12)); }
	inline float get_lastPlayAreaY_12() const { return ___lastPlayAreaY_12; }
	inline float* get_address_of_lastPlayAreaY_12() { return &___lastPlayAreaY_12; }
	inline void set_lastPlayAreaY_12(float value)
	{
		___lastPlayAreaY_12 = value;
	}

	inline static int32_t get_offset_of_lastGoodPositionSet_13() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___lastGoodPositionSet_13)); }
	inline bool get_lastGoodPositionSet_13() const { return ___lastGoodPositionSet_13; }
	inline bool* get_address_of_lastGoodPositionSet_13() { return &___lastGoodPositionSet_13; }
	inline void set_lastGoodPositionSet_13(bool value)
	{
		___lastGoodPositionSet_13 = value;
	}

	inline static int32_t get_offset_of_hasCollided_14() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___hasCollided_14)); }
	inline bool get_hasCollided_14() const { return ___hasCollided_14; }
	inline bool* get_address_of_hasCollided_14() { return &___hasCollided_14; }
	inline void set_hasCollided_14(bool value)
	{
		___hasCollided_14 = value;
	}

	inline static int32_t get_offset_of_isColliding_15() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___isColliding_15)); }
	inline bool get_isColliding_15() const { return ___isColliding_15; }
	inline bool* get_address_of_isColliding_15() { return &___isColliding_15; }
	inline void set_isColliding_15(bool value)
	{
		___isColliding_15 = value;
	}

	inline static int32_t get_offset_of_collideTimer_16() { return static_cast<int32_t>(offsetof(VRTK_PositionRewind_t3080560492, ___collideTimer_16)); }
	inline float get_collideTimer_16() const { return ___collideTimer_16; }
	inline float* get_address_of_collideTimer_16() { return &___collideTimer_16; }
	inline void set_collideTimer_16(float value)
	{
		___collideTimer_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
