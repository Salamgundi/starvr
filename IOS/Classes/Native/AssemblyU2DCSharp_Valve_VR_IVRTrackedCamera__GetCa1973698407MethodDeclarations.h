﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRTrackedCamera/_GetCameraFrameSize
struct _GetCameraFrameSize_t1973698407;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraError3529690400.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraFrameTy2003723073.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRTrackedCamera/_GetCameraFrameSize::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetCameraFrameSize__ctor_m2980325328 (_GetCameraFrameSize_t1973698407 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_GetCameraFrameSize::Invoke(System.UInt32,Valve.VR.EVRTrackedCameraFrameType,System.UInt32&,System.UInt32&,System.UInt32&)
extern "C"  int32_t _GetCameraFrameSize_Invoke_m3916470438 (_GetCameraFrameSize_t1973698407 * __this, uint32_t ___nDeviceIndex0, int32_t ___eFrameType1, uint32_t* ___pnWidth2, uint32_t* ___pnHeight3, uint32_t* ___pnFrameBufferSize4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRTrackedCamera/_GetCameraFrameSize::BeginInvoke(System.UInt32,Valve.VR.EVRTrackedCameraFrameType,System.UInt32&,System.UInt32&,System.UInt32&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetCameraFrameSize_BeginInvoke_m2325196906 (_GetCameraFrameSize_t1973698407 * __this, uint32_t ___nDeviceIndex0, int32_t ___eFrameType1, uint32_t* ___pnWidth2, uint32_t* ___pnHeight3, uint32_t* ___pnFrameBufferSize4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_GetCameraFrameSize::EndInvoke(System.UInt32&,System.UInt32&,System.UInt32&,System.IAsyncResult)
extern "C"  int32_t _GetCameraFrameSize_EndInvoke_m2520796063 (_GetCameraFrameSize_t1973698407 * __this, uint32_t* ___pnWidth0, uint32_t* ___pnHeight1, uint32_t* ___pnFrameBufferSize2, Il2CppObject * ___result3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
