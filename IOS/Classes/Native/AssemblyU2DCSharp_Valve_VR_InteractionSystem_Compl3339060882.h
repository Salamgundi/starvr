﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand>
struct List_1_t4043804781;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody>
struct List_1_t3603010323;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Compl1851484950.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1543711741.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.ComplexThrowable
struct  ComplexThrowable_t3339060882  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Valve.VR.InteractionSystem.ComplexThrowable::attachForce
	float ___attachForce_2;
	// System.Single Valve.VR.InteractionSystem.ComplexThrowable::attachForceDamper
	float ___attachForceDamper_3;
	// Valve.VR.InteractionSystem.ComplexThrowable/AttachMode Valve.VR.InteractionSystem.ComplexThrowable::attachMode
	int32_t ___attachMode_4;
	// Valve.VR.InteractionSystem.Hand/AttachmentFlags Valve.VR.InteractionSystem.ComplexThrowable::attachmentFlags
	int32_t ___attachmentFlags_5;
	// System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand> Valve.VR.InteractionSystem.ComplexThrowable::holdingHands
	List_1_t4043804781 * ___holdingHands_6;
	// System.Collections.Generic.List`1<UnityEngine.Rigidbody> Valve.VR.InteractionSystem.ComplexThrowable::holdingBodies
	List_1_t3603010323 * ___holdingBodies_7;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Valve.VR.InteractionSystem.ComplexThrowable::holdingPoints
	List_1_t1612828712 * ___holdingPoints_8;
	// System.Collections.Generic.List`1<UnityEngine.Rigidbody> Valve.VR.InteractionSystem.ComplexThrowable::rigidBodies
	List_1_t3603010323 * ___rigidBodies_9;

public:
	inline static int32_t get_offset_of_attachForce_2() { return static_cast<int32_t>(offsetof(ComplexThrowable_t3339060882, ___attachForce_2)); }
	inline float get_attachForce_2() const { return ___attachForce_2; }
	inline float* get_address_of_attachForce_2() { return &___attachForce_2; }
	inline void set_attachForce_2(float value)
	{
		___attachForce_2 = value;
	}

	inline static int32_t get_offset_of_attachForceDamper_3() { return static_cast<int32_t>(offsetof(ComplexThrowable_t3339060882, ___attachForceDamper_3)); }
	inline float get_attachForceDamper_3() const { return ___attachForceDamper_3; }
	inline float* get_address_of_attachForceDamper_3() { return &___attachForceDamper_3; }
	inline void set_attachForceDamper_3(float value)
	{
		___attachForceDamper_3 = value;
	}

	inline static int32_t get_offset_of_attachMode_4() { return static_cast<int32_t>(offsetof(ComplexThrowable_t3339060882, ___attachMode_4)); }
	inline int32_t get_attachMode_4() const { return ___attachMode_4; }
	inline int32_t* get_address_of_attachMode_4() { return &___attachMode_4; }
	inline void set_attachMode_4(int32_t value)
	{
		___attachMode_4 = value;
	}

	inline static int32_t get_offset_of_attachmentFlags_5() { return static_cast<int32_t>(offsetof(ComplexThrowable_t3339060882, ___attachmentFlags_5)); }
	inline int32_t get_attachmentFlags_5() const { return ___attachmentFlags_5; }
	inline int32_t* get_address_of_attachmentFlags_5() { return &___attachmentFlags_5; }
	inline void set_attachmentFlags_5(int32_t value)
	{
		___attachmentFlags_5 = value;
	}

	inline static int32_t get_offset_of_holdingHands_6() { return static_cast<int32_t>(offsetof(ComplexThrowable_t3339060882, ___holdingHands_6)); }
	inline List_1_t4043804781 * get_holdingHands_6() const { return ___holdingHands_6; }
	inline List_1_t4043804781 ** get_address_of_holdingHands_6() { return &___holdingHands_6; }
	inline void set_holdingHands_6(List_1_t4043804781 * value)
	{
		___holdingHands_6 = value;
		Il2CppCodeGenWriteBarrier(&___holdingHands_6, value);
	}

	inline static int32_t get_offset_of_holdingBodies_7() { return static_cast<int32_t>(offsetof(ComplexThrowable_t3339060882, ___holdingBodies_7)); }
	inline List_1_t3603010323 * get_holdingBodies_7() const { return ___holdingBodies_7; }
	inline List_1_t3603010323 ** get_address_of_holdingBodies_7() { return &___holdingBodies_7; }
	inline void set_holdingBodies_7(List_1_t3603010323 * value)
	{
		___holdingBodies_7 = value;
		Il2CppCodeGenWriteBarrier(&___holdingBodies_7, value);
	}

	inline static int32_t get_offset_of_holdingPoints_8() { return static_cast<int32_t>(offsetof(ComplexThrowable_t3339060882, ___holdingPoints_8)); }
	inline List_1_t1612828712 * get_holdingPoints_8() const { return ___holdingPoints_8; }
	inline List_1_t1612828712 ** get_address_of_holdingPoints_8() { return &___holdingPoints_8; }
	inline void set_holdingPoints_8(List_1_t1612828712 * value)
	{
		___holdingPoints_8 = value;
		Il2CppCodeGenWriteBarrier(&___holdingPoints_8, value);
	}

	inline static int32_t get_offset_of_rigidBodies_9() { return static_cast<int32_t>(offsetof(ComplexThrowable_t3339060882, ___rigidBodies_9)); }
	inline List_1_t3603010323 * get_rigidBodies_9() const { return ___rigidBodies_9; }
	inline List_1_t3603010323 ** get_address_of_rigidBodies_9() { return &___rigidBodies_9; }
	inline void set_rigidBodies_9(List_1_t3603010323 * value)
	{
		___rigidBodies_9 = value;
		Il2CppCodeGenWriteBarrier(&___rigidBodies_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
