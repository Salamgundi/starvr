﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.ConfigurableJoint
struct ConfigurableJoint_t454307495;

#include "AssemblyU2DCSharp_VRTK_VRTK_Control651619021.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_Direction3775008092.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Slider
struct  VRTK_Slider_t2095192727  : public VRTK_Control_t651619021
{
public:
	// UnityEngine.GameObject VRTK.VRTK_Slider::connectedTo
	GameObject_t1756533147 * ___connectedTo_15;
	// VRTK.VRTK_Control/Direction VRTK.VRTK_Slider::direction
	int32_t ___direction_16;
	// UnityEngine.Collider VRTK.VRTK_Slider::minimumLimit
	Collider_t3497673348 * ___minimumLimit_17;
	// UnityEngine.Collider VRTK.VRTK_Slider::maximumLimit
	Collider_t3497673348 * ___maximumLimit_18;
	// System.Single VRTK.VRTK_Slider::minimumValue
	float ___minimumValue_19;
	// System.Single VRTK.VRTK_Slider::maximumValue
	float ___maximumValue_20;
	// System.Single VRTK.VRTK_Slider::stepSize
	float ___stepSize_21;
	// System.Boolean VRTK.VRTK_Slider::snapToStep
	bool ___snapToStep_22;
	// System.Single VRTK.VRTK_Slider::releasedFriction
	float ___releasedFriction_23;
	// VRTK.VRTK_Control/Direction VRTK.VRTK_Slider::finalDirection
	int32_t ___finalDirection_24;
	// UnityEngine.Rigidbody VRTK.VRTK_Slider::sliderRigidbody
	Rigidbody_t4233889191 * ___sliderRigidbody_25;
	// UnityEngine.ConfigurableJoint VRTK.VRTK_Slider::sliderJoint
	ConfigurableJoint_t454307495 * ___sliderJoint_26;
	// System.Boolean VRTK.VRTK_Slider::sliderJointCreated
	bool ___sliderJointCreated_27;
	// UnityEngine.Vector3 VRTK.VRTK_Slider::minimumLimitDiff
	Vector3_t2243707580  ___minimumLimitDiff_28;
	// UnityEngine.Vector3 VRTK.VRTK_Slider::maximumLimitDiff
	Vector3_t2243707580  ___maximumLimitDiff_29;
	// UnityEngine.Vector3 VRTK.VRTK_Slider::snapPosition
	Vector3_t2243707580  ___snapPosition_30;

public:
	inline static int32_t get_offset_of_connectedTo_15() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___connectedTo_15)); }
	inline GameObject_t1756533147 * get_connectedTo_15() const { return ___connectedTo_15; }
	inline GameObject_t1756533147 ** get_address_of_connectedTo_15() { return &___connectedTo_15; }
	inline void set_connectedTo_15(GameObject_t1756533147 * value)
	{
		___connectedTo_15 = value;
		Il2CppCodeGenWriteBarrier(&___connectedTo_15, value);
	}

	inline static int32_t get_offset_of_direction_16() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___direction_16)); }
	inline int32_t get_direction_16() const { return ___direction_16; }
	inline int32_t* get_address_of_direction_16() { return &___direction_16; }
	inline void set_direction_16(int32_t value)
	{
		___direction_16 = value;
	}

	inline static int32_t get_offset_of_minimumLimit_17() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___minimumLimit_17)); }
	inline Collider_t3497673348 * get_minimumLimit_17() const { return ___minimumLimit_17; }
	inline Collider_t3497673348 ** get_address_of_minimumLimit_17() { return &___minimumLimit_17; }
	inline void set_minimumLimit_17(Collider_t3497673348 * value)
	{
		___minimumLimit_17 = value;
		Il2CppCodeGenWriteBarrier(&___minimumLimit_17, value);
	}

	inline static int32_t get_offset_of_maximumLimit_18() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___maximumLimit_18)); }
	inline Collider_t3497673348 * get_maximumLimit_18() const { return ___maximumLimit_18; }
	inline Collider_t3497673348 ** get_address_of_maximumLimit_18() { return &___maximumLimit_18; }
	inline void set_maximumLimit_18(Collider_t3497673348 * value)
	{
		___maximumLimit_18 = value;
		Il2CppCodeGenWriteBarrier(&___maximumLimit_18, value);
	}

	inline static int32_t get_offset_of_minimumValue_19() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___minimumValue_19)); }
	inline float get_minimumValue_19() const { return ___minimumValue_19; }
	inline float* get_address_of_minimumValue_19() { return &___minimumValue_19; }
	inline void set_minimumValue_19(float value)
	{
		___minimumValue_19 = value;
	}

	inline static int32_t get_offset_of_maximumValue_20() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___maximumValue_20)); }
	inline float get_maximumValue_20() const { return ___maximumValue_20; }
	inline float* get_address_of_maximumValue_20() { return &___maximumValue_20; }
	inline void set_maximumValue_20(float value)
	{
		___maximumValue_20 = value;
	}

	inline static int32_t get_offset_of_stepSize_21() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___stepSize_21)); }
	inline float get_stepSize_21() const { return ___stepSize_21; }
	inline float* get_address_of_stepSize_21() { return &___stepSize_21; }
	inline void set_stepSize_21(float value)
	{
		___stepSize_21 = value;
	}

	inline static int32_t get_offset_of_snapToStep_22() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___snapToStep_22)); }
	inline bool get_snapToStep_22() const { return ___snapToStep_22; }
	inline bool* get_address_of_snapToStep_22() { return &___snapToStep_22; }
	inline void set_snapToStep_22(bool value)
	{
		___snapToStep_22 = value;
	}

	inline static int32_t get_offset_of_releasedFriction_23() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___releasedFriction_23)); }
	inline float get_releasedFriction_23() const { return ___releasedFriction_23; }
	inline float* get_address_of_releasedFriction_23() { return &___releasedFriction_23; }
	inline void set_releasedFriction_23(float value)
	{
		___releasedFriction_23 = value;
	}

	inline static int32_t get_offset_of_finalDirection_24() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___finalDirection_24)); }
	inline int32_t get_finalDirection_24() const { return ___finalDirection_24; }
	inline int32_t* get_address_of_finalDirection_24() { return &___finalDirection_24; }
	inline void set_finalDirection_24(int32_t value)
	{
		___finalDirection_24 = value;
	}

	inline static int32_t get_offset_of_sliderRigidbody_25() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___sliderRigidbody_25)); }
	inline Rigidbody_t4233889191 * get_sliderRigidbody_25() const { return ___sliderRigidbody_25; }
	inline Rigidbody_t4233889191 ** get_address_of_sliderRigidbody_25() { return &___sliderRigidbody_25; }
	inline void set_sliderRigidbody_25(Rigidbody_t4233889191 * value)
	{
		___sliderRigidbody_25 = value;
		Il2CppCodeGenWriteBarrier(&___sliderRigidbody_25, value);
	}

	inline static int32_t get_offset_of_sliderJoint_26() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___sliderJoint_26)); }
	inline ConfigurableJoint_t454307495 * get_sliderJoint_26() const { return ___sliderJoint_26; }
	inline ConfigurableJoint_t454307495 ** get_address_of_sliderJoint_26() { return &___sliderJoint_26; }
	inline void set_sliderJoint_26(ConfigurableJoint_t454307495 * value)
	{
		___sliderJoint_26 = value;
		Il2CppCodeGenWriteBarrier(&___sliderJoint_26, value);
	}

	inline static int32_t get_offset_of_sliderJointCreated_27() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___sliderJointCreated_27)); }
	inline bool get_sliderJointCreated_27() const { return ___sliderJointCreated_27; }
	inline bool* get_address_of_sliderJointCreated_27() { return &___sliderJointCreated_27; }
	inline void set_sliderJointCreated_27(bool value)
	{
		___sliderJointCreated_27 = value;
	}

	inline static int32_t get_offset_of_minimumLimitDiff_28() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___minimumLimitDiff_28)); }
	inline Vector3_t2243707580  get_minimumLimitDiff_28() const { return ___minimumLimitDiff_28; }
	inline Vector3_t2243707580 * get_address_of_minimumLimitDiff_28() { return &___minimumLimitDiff_28; }
	inline void set_minimumLimitDiff_28(Vector3_t2243707580  value)
	{
		___minimumLimitDiff_28 = value;
	}

	inline static int32_t get_offset_of_maximumLimitDiff_29() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___maximumLimitDiff_29)); }
	inline Vector3_t2243707580  get_maximumLimitDiff_29() const { return ___maximumLimitDiff_29; }
	inline Vector3_t2243707580 * get_address_of_maximumLimitDiff_29() { return &___maximumLimitDiff_29; }
	inline void set_maximumLimitDiff_29(Vector3_t2243707580  value)
	{
		___maximumLimitDiff_29 = value;
	}

	inline static int32_t get_offset_of_snapPosition_30() { return static_cast<int32_t>(offsetof(VRTK_Slider_t2095192727, ___snapPosition_30)); }
	inline Vector3_t2243707580  get_snapPosition_30() const { return ___snapPosition_30; }
	inline Vector3_t2243707580 * get_address_of_snapPosition_30() { return &___snapPosition_30; }
	inline void set_snapPosition_30(Vector3_t2243707580  value)
	{
		___snapPosition_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
