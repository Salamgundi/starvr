﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ComplexThrowable
struct ComplexThrowable_t3339060882;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"

// System.Void Valve.VR.InteractionSystem.ComplexThrowable::.ctor()
extern "C"  void ComplexThrowable__ctor_m4117057782 (ComplexThrowable_t3339060882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ComplexThrowable::Awake()
extern "C"  void ComplexThrowable_Awake_m3620304535 (ComplexThrowable_t3339060882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ComplexThrowable::Update()
extern "C"  void ComplexThrowable_Update_m2331454187 (ComplexThrowable_t3339060882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ComplexThrowable::OnHandHoverBegin(Valve.VR.InteractionSystem.Hand)
extern "C"  void ComplexThrowable_OnHandHoverBegin_m3799497499 (ComplexThrowable_t3339060882 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ComplexThrowable::OnHandHoverEnd(Valve.VR.InteractionSystem.Hand)
extern "C"  void ComplexThrowable_OnHandHoverEnd_m3150999947 (ComplexThrowable_t3339060882 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ComplexThrowable::HandHoverUpdate(Valve.VR.InteractionSystem.Hand)
extern "C"  void ComplexThrowable_HandHoverUpdate_m3776942226 (ComplexThrowable_t3339060882 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ComplexThrowable::PhysicsAttach(Valve.VR.InteractionSystem.Hand)
extern "C"  void ComplexThrowable_PhysicsAttach_m4226177086 (ComplexThrowable_t3339060882 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.ComplexThrowable::PhysicsDetach(Valve.VR.InteractionSystem.Hand)
extern "C"  bool ComplexThrowable_PhysicsDetach_m3303933686 (ComplexThrowable_t3339060882 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ComplexThrowable::FixedUpdate()
extern "C"  void ComplexThrowable_FixedUpdate_m2279153009 (ComplexThrowable_t3339060882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
