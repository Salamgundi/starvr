﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.RC_Car
struct  RC_Car_t3139601232  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VRTK.Examples.RC_Car::maxAcceleration
	float ___maxAcceleration_2;
	// System.Single VRTK.Examples.RC_Car::jumpPower
	float ___jumpPower_3;
	// System.Single VRTK.Examples.RC_Car::acceleration
	float ___acceleration_4;
	// System.Single VRTK.Examples.RC_Car::movementSpeed
	float ___movementSpeed_5;
	// System.Single VRTK.Examples.RC_Car::rotationSpeed
	float ___rotationSpeed_6;
	// System.Boolean VRTK.Examples.RC_Car::isJumping
	bool ___isJumping_7;
	// UnityEngine.Vector2 VRTK.Examples.RC_Car::touchAxis
	Vector2_t2243707579  ___touchAxis_8;
	// System.Single VRTK.Examples.RC_Car::triggerAxis
	float ___triggerAxis_9;
	// UnityEngine.Rigidbody VRTK.Examples.RC_Car::rb
	Rigidbody_t4233889191 * ___rb_10;
	// UnityEngine.Vector3 VRTK.Examples.RC_Car::defaultPosition
	Vector3_t2243707580  ___defaultPosition_11;
	// UnityEngine.Quaternion VRTK.Examples.RC_Car::defaultRotation
	Quaternion_t4030073918  ___defaultRotation_12;

public:
	inline static int32_t get_offset_of_maxAcceleration_2() { return static_cast<int32_t>(offsetof(RC_Car_t3139601232, ___maxAcceleration_2)); }
	inline float get_maxAcceleration_2() const { return ___maxAcceleration_2; }
	inline float* get_address_of_maxAcceleration_2() { return &___maxAcceleration_2; }
	inline void set_maxAcceleration_2(float value)
	{
		___maxAcceleration_2 = value;
	}

	inline static int32_t get_offset_of_jumpPower_3() { return static_cast<int32_t>(offsetof(RC_Car_t3139601232, ___jumpPower_3)); }
	inline float get_jumpPower_3() const { return ___jumpPower_3; }
	inline float* get_address_of_jumpPower_3() { return &___jumpPower_3; }
	inline void set_jumpPower_3(float value)
	{
		___jumpPower_3 = value;
	}

	inline static int32_t get_offset_of_acceleration_4() { return static_cast<int32_t>(offsetof(RC_Car_t3139601232, ___acceleration_4)); }
	inline float get_acceleration_4() const { return ___acceleration_4; }
	inline float* get_address_of_acceleration_4() { return &___acceleration_4; }
	inline void set_acceleration_4(float value)
	{
		___acceleration_4 = value;
	}

	inline static int32_t get_offset_of_movementSpeed_5() { return static_cast<int32_t>(offsetof(RC_Car_t3139601232, ___movementSpeed_5)); }
	inline float get_movementSpeed_5() const { return ___movementSpeed_5; }
	inline float* get_address_of_movementSpeed_5() { return &___movementSpeed_5; }
	inline void set_movementSpeed_5(float value)
	{
		___movementSpeed_5 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_6() { return static_cast<int32_t>(offsetof(RC_Car_t3139601232, ___rotationSpeed_6)); }
	inline float get_rotationSpeed_6() const { return ___rotationSpeed_6; }
	inline float* get_address_of_rotationSpeed_6() { return &___rotationSpeed_6; }
	inline void set_rotationSpeed_6(float value)
	{
		___rotationSpeed_6 = value;
	}

	inline static int32_t get_offset_of_isJumping_7() { return static_cast<int32_t>(offsetof(RC_Car_t3139601232, ___isJumping_7)); }
	inline bool get_isJumping_7() const { return ___isJumping_7; }
	inline bool* get_address_of_isJumping_7() { return &___isJumping_7; }
	inline void set_isJumping_7(bool value)
	{
		___isJumping_7 = value;
	}

	inline static int32_t get_offset_of_touchAxis_8() { return static_cast<int32_t>(offsetof(RC_Car_t3139601232, ___touchAxis_8)); }
	inline Vector2_t2243707579  get_touchAxis_8() const { return ___touchAxis_8; }
	inline Vector2_t2243707579 * get_address_of_touchAxis_8() { return &___touchAxis_8; }
	inline void set_touchAxis_8(Vector2_t2243707579  value)
	{
		___touchAxis_8 = value;
	}

	inline static int32_t get_offset_of_triggerAxis_9() { return static_cast<int32_t>(offsetof(RC_Car_t3139601232, ___triggerAxis_9)); }
	inline float get_triggerAxis_9() const { return ___triggerAxis_9; }
	inline float* get_address_of_triggerAxis_9() { return &___triggerAxis_9; }
	inline void set_triggerAxis_9(float value)
	{
		___triggerAxis_9 = value;
	}

	inline static int32_t get_offset_of_rb_10() { return static_cast<int32_t>(offsetof(RC_Car_t3139601232, ___rb_10)); }
	inline Rigidbody_t4233889191 * get_rb_10() const { return ___rb_10; }
	inline Rigidbody_t4233889191 ** get_address_of_rb_10() { return &___rb_10; }
	inline void set_rb_10(Rigidbody_t4233889191 * value)
	{
		___rb_10 = value;
		Il2CppCodeGenWriteBarrier(&___rb_10, value);
	}

	inline static int32_t get_offset_of_defaultPosition_11() { return static_cast<int32_t>(offsetof(RC_Car_t3139601232, ___defaultPosition_11)); }
	inline Vector3_t2243707580  get_defaultPosition_11() const { return ___defaultPosition_11; }
	inline Vector3_t2243707580 * get_address_of_defaultPosition_11() { return &___defaultPosition_11; }
	inline void set_defaultPosition_11(Vector3_t2243707580  value)
	{
		___defaultPosition_11 = value;
	}

	inline static int32_t get_offset_of_defaultRotation_12() { return static_cast<int32_t>(offsetof(RC_Car_t3139601232, ___defaultRotation_12)); }
	inline Quaternion_t4030073918  get_defaultRotation_12() const { return ___defaultRotation_12; }
	inline Quaternion_t4030073918 * get_address_of_defaultRotation_12() { return &___defaultRotation_12; }
	inline void set_defaultRotation_12(Quaternion_t4030073918  value)
	{
		___defaultRotation_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
