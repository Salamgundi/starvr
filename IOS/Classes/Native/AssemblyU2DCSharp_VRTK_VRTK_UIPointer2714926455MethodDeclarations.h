﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_UIPointer
struct VRTK_UIPointer_t2714926455;
// VRTK.UIPointerEventHandler
struct UIPointerEventHandler_t988663103;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_VRInputModule
struct VRTK_VRInputModule_t1472500726;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_UIPointerEventHandler988663103.h"
#include "AssemblyU2DCSharp_VRTK_UIPointerEventArgs1171985978.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_UIPointer::.ctor()
extern "C"  void VRTK_UIPointer__ctor_m951207381 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::add_UIPointerElementEnter(VRTK.UIPointerEventHandler)
extern "C"  void VRTK_UIPointer_add_UIPointerElementEnter_m2528476926 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::remove_UIPointerElementEnter(VRTK.UIPointerEventHandler)
extern "C"  void VRTK_UIPointer_remove_UIPointerElementEnter_m4179690723 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::add_UIPointerElementExit(VRTK.UIPointerEventHandler)
extern "C"  void VRTK_UIPointer_add_UIPointerElementExit_m3973785302 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::remove_UIPointerElementExit(VRTK.UIPointerEventHandler)
extern "C"  void VRTK_UIPointer_remove_UIPointerElementExit_m2698589931 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::add_UIPointerElementClick(VRTK.UIPointerEventHandler)
extern "C"  void VRTK_UIPointer_add_UIPointerElementClick_m3261397528 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::remove_UIPointerElementClick(VRTK.UIPointerEventHandler)
extern "C"  void VRTK_UIPointer_remove_UIPointerElementClick_m3345897689 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::add_UIPointerElementDragStart(VRTK.UIPointerEventHandler)
extern "C"  void VRTK_UIPointer_add_UIPointerElementDragStart_m1439174518 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::remove_UIPointerElementDragStart(VRTK.UIPointerEventHandler)
extern "C"  void VRTK_UIPointer_remove_UIPointerElementDragStart_m145289803 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::add_UIPointerElementDragEnd(VRTK.UIPointerEventHandler)
extern "C"  void VRTK_UIPointer_add_UIPointerElementDragEnd_m1805642955 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::remove_UIPointerElementDragEnd(VRTK.UIPointerEventHandler)
extern "C"  void VRTK_UIPointer_remove_UIPointerElementDragEnd_m3976071480 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::OnUIPointerElementEnter(VRTK.UIPointerEventArgs)
extern "C"  void VRTK_UIPointer_OnUIPointerElementEnter_m988000146 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventArgs_t1171985978  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::OnUIPointerElementExit(VRTK.UIPointerEventArgs)
extern "C"  void VRTK_UIPointer_OnUIPointerElementExit_m2620195152 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventArgs_t1171985978  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::OnUIPointerElementClick(VRTK.UIPointerEventArgs)
extern "C"  void VRTK_UIPointer_OnUIPointerElementClick_m3395580096 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventArgs_t1171985978  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::OnUIPointerElementDragStart(VRTK.UIPointerEventArgs)
extern "C"  void VRTK_UIPointer_OnUIPointerElementDragStart_m3799511114 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventArgs_t1171985978  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::OnUIPointerElementDragEnd(VRTK.UIPointerEventArgs)
extern "C"  void VRTK_UIPointer_OnUIPointerElementDragEnd_m248063443 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventArgs_t1171985978  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.UIPointerEventArgs VRTK.VRTK_UIPointer::SetUIPointerEvent(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  UIPointerEventArgs_t1171985978  VRTK_UIPointer_SetUIPointerEvent_m4163559974 (VRTK_UIPointer_t2714926455 * __this, GameObject_t1756533147 * ___currentTarget0, GameObject_t1756533147 * ___lastTarget1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_VRInputModule VRTK.VRTK_UIPointer::SetEventSystem(UnityEngine.EventSystems.EventSystem)
extern "C"  VRTK_VRInputModule_t1472500726 * VRTK_UIPointer_SetEventSystem_m2788055360 (VRTK_UIPointer_t2714926455 * __this, EventSystem_t3466835263 * ___eventSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::RemoveEventSystem()
extern "C"  void VRTK_UIPointer_RemoveEventSystem_m2648293778 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_UIPointer::PointerActive()
extern "C"  bool VRTK_UIPointer_PointerActive_m1525123954 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_UIPointer::SelectionButtonActive()
extern "C"  bool VRTK_UIPointer_SelectionButtonActive_m1429660791 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_UIPointer::ValidClick(System.Boolean,System.Boolean)
extern "C"  bool VRTK_UIPointer_ValidClick_m4095485069 (VRTK_UIPointer_t2714926455 * __this, bool ___checkLastClick0, bool ___lastClickState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_UIPointer::GetOriginPosition()
extern "C"  Vector3_t2243707580  VRTK_UIPointer_GetOriginPosition_m1573724116 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_UIPointer::GetOriginForward()
extern "C"  Vector3_t2243707580  VRTK_UIPointer_GetOriginForward_m878658030 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::OnEnable()
extern "C"  void VRTK_UIPointer_OnEnable_m2039416421 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::OnDisable()
extern "C"  void VRTK_UIPointer_OnDisable_m1810105220 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::ResetHoverTimer()
extern "C"  void VRTK_UIPointer_ResetHoverTimer_m1372327469 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointer::ConfigureEventSystem()
extern "C"  void VRTK_UIPointer_ConfigureEventSystem_m3931862010 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_UIPointer::WaitForPointerId()
extern "C"  Il2CppObject * VRTK_UIPointer_WaitForPointerId_m1975289061 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
