﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_HipTracking
struct  VRTK_HipTracking_t3141756816  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VRTK.VRTK_HipTracking::HeadOffset
	float ___HeadOffset_2;
	// UnityEngine.Transform VRTK.VRTK_HipTracking::headOverride
	Transform_t3275118058 * ___headOverride_3;
	// UnityEngine.Transform VRTK.VRTK_HipTracking::ReferenceUp
	Transform_t3275118058 * ___ReferenceUp_4;
	// UnityEngine.Transform VRTK.VRTK_HipTracking::playerHead
	Transform_t3275118058 * ___playerHead_5;

public:
	inline static int32_t get_offset_of_HeadOffset_2() { return static_cast<int32_t>(offsetof(VRTK_HipTracking_t3141756816, ___HeadOffset_2)); }
	inline float get_HeadOffset_2() const { return ___HeadOffset_2; }
	inline float* get_address_of_HeadOffset_2() { return &___HeadOffset_2; }
	inline void set_HeadOffset_2(float value)
	{
		___HeadOffset_2 = value;
	}

	inline static int32_t get_offset_of_headOverride_3() { return static_cast<int32_t>(offsetof(VRTK_HipTracking_t3141756816, ___headOverride_3)); }
	inline Transform_t3275118058 * get_headOverride_3() const { return ___headOverride_3; }
	inline Transform_t3275118058 ** get_address_of_headOverride_3() { return &___headOverride_3; }
	inline void set_headOverride_3(Transform_t3275118058 * value)
	{
		___headOverride_3 = value;
		Il2CppCodeGenWriteBarrier(&___headOverride_3, value);
	}

	inline static int32_t get_offset_of_ReferenceUp_4() { return static_cast<int32_t>(offsetof(VRTK_HipTracking_t3141756816, ___ReferenceUp_4)); }
	inline Transform_t3275118058 * get_ReferenceUp_4() const { return ___ReferenceUp_4; }
	inline Transform_t3275118058 ** get_address_of_ReferenceUp_4() { return &___ReferenceUp_4; }
	inline void set_ReferenceUp_4(Transform_t3275118058 * value)
	{
		___ReferenceUp_4 = value;
		Il2CppCodeGenWriteBarrier(&___ReferenceUp_4, value);
	}

	inline static int32_t get_offset_of_playerHead_5() { return static_cast<int32_t>(offsetof(VRTK_HipTracking_t3141756816, ___playerHead_5)); }
	inline Transform_t3275118058 * get_playerHead_5() const { return ___playerHead_5; }
	inline Transform_t3275118058 ** get_address_of_playerHead_5() { return &___playerHead_5; }
	inline void set_playerHead_5(Transform_t3275118058 * value)
	{
		___playerHead_5 = value;
		Il2CppCodeGenWriteBarrier(&___playerHead_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
