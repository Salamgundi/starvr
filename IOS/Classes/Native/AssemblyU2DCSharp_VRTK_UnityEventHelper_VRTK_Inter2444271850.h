﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_InteractTouch
struct VRTK_InteractTouch_t4022091061;
// VRTK.UnityEventHelper.VRTK_InteractTouch_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t217234877;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_InteractTouch_UnityEvents
struct  VRTK_InteractTouch_UnityEvents_t2444271850  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_InteractTouch VRTK.UnityEventHelper.VRTK_InteractTouch_UnityEvents::it
	VRTK_InteractTouch_t4022091061 * ___it_2;
	// VRTK.UnityEventHelper.VRTK_InteractTouch_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_InteractTouch_UnityEvents::OnControllerTouchInteractableObject
	UnityObjectEvent_t217234877 * ___OnControllerTouchInteractableObject_3;
	// VRTK.UnityEventHelper.VRTK_InteractTouch_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_InteractTouch_UnityEvents::OnControllerUntouchInteractableObject
	UnityObjectEvent_t217234877 * ___OnControllerUntouchInteractableObject_4;

public:
	inline static int32_t get_offset_of_it_2() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_UnityEvents_t2444271850, ___it_2)); }
	inline VRTK_InteractTouch_t4022091061 * get_it_2() const { return ___it_2; }
	inline VRTK_InteractTouch_t4022091061 ** get_address_of_it_2() { return &___it_2; }
	inline void set_it_2(VRTK_InteractTouch_t4022091061 * value)
	{
		___it_2 = value;
		Il2CppCodeGenWriteBarrier(&___it_2, value);
	}

	inline static int32_t get_offset_of_OnControllerTouchInteractableObject_3() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_UnityEvents_t2444271850, ___OnControllerTouchInteractableObject_3)); }
	inline UnityObjectEvent_t217234877 * get_OnControllerTouchInteractableObject_3() const { return ___OnControllerTouchInteractableObject_3; }
	inline UnityObjectEvent_t217234877 ** get_address_of_OnControllerTouchInteractableObject_3() { return &___OnControllerTouchInteractableObject_3; }
	inline void set_OnControllerTouchInteractableObject_3(UnityObjectEvent_t217234877 * value)
	{
		___OnControllerTouchInteractableObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerTouchInteractableObject_3, value);
	}

	inline static int32_t get_offset_of_OnControllerUntouchInteractableObject_4() { return static_cast<int32_t>(offsetof(VRTK_InteractTouch_UnityEvents_t2444271850, ___OnControllerUntouchInteractableObject_4)); }
	inline UnityObjectEvent_t217234877 * get_OnControllerUntouchInteractableObject_4() const { return ___OnControllerUntouchInteractableObject_4; }
	inline UnityObjectEvent_t217234877 ** get_address_of_OnControllerUntouchInteractableObject_4() { return &___OnControllerUntouchInteractableObject_4; }
	inline void set_OnControllerUntouchInteractableObject_4(UnityObjectEvent_t217234877 * value)
	{
		___OnControllerUntouchInteractableObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerUntouchInteractableObject_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
