﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRScreenshots/_SubmitScreenshot
struct _SubmitScreenshot_t3156929320;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotError1400268927.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotType611740195.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRScreenshots/_SubmitScreenshot::.ctor(System.Object,System.IntPtr)
extern "C"  void _SubmitScreenshot__ctor_m1729493409 (_SubmitScreenshot_t3156929320 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_SubmitScreenshot::Invoke(System.UInt32,Valve.VR.EVRScreenshotType,System.String,System.String)
extern "C"  int32_t _SubmitScreenshot_Invoke_m26953202 (_SubmitScreenshot_t3156929320 * __this, uint32_t ___screenshotHandle0, int32_t ___type1, String_t* ___pchSourcePreviewFilename2, String_t* ___pchSourceVRFilename3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRScreenshots/_SubmitScreenshot::BeginInvoke(System.UInt32,Valve.VR.EVRScreenshotType,System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SubmitScreenshot_BeginInvoke_m2995271979 (_SubmitScreenshot_t3156929320 * __this, uint32_t ___screenshotHandle0, int32_t ___type1, String_t* ___pchSourcePreviewFilename2, String_t* ___pchSourceVRFilename3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotError Valve.VR.IVRScreenshots/_SubmitScreenshot::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SubmitScreenshot_EndInvoke_m3607649749 (_SubmitScreenshot_t3156929320 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
