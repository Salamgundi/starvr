﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0
struct U3CAutoGrabU3Ec__Iterator0_t1000106702;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0::.ctor()
extern "C"  void U3CAutoGrabU3Ec__Iterator0__ctor_m2061947611 (U3CAutoGrabU3Ec__Iterator0_t1000106702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0::MoveNext()
extern "C"  bool U3CAutoGrabU3Ec__Iterator0_MoveNext_m2216434961 (U3CAutoGrabU3Ec__Iterator0_t1000106702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAutoGrabU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3293945993 (U3CAutoGrabU3Ec__Iterator0_t1000106702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAutoGrabU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2515428081 (U3CAutoGrabU3Ec__Iterator0_t1000106702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0::Dispose()
extern "C"  void U3CAutoGrabU3Ec__Iterator0_Dispose_m1135405784 (U3CAutoGrabU3Ec__Iterator0_t1000106702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectAutoGrab/<AutoGrab>c__Iterator0::Reset()
extern "C"  void U3CAutoGrabU3Ec__Iterator0_Reset_m3663837846 (U3CAutoGrabU3Ec__Iterator0_t1000106702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
