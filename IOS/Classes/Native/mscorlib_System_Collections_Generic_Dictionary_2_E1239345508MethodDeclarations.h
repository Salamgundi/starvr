﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Coroutine>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m310512644(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1239345508 *, Dictionary_2_t4214288102 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Coroutine>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1244958185(__this, method) ((  Il2CppObject * (*) (Enumerator_t1239345508 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Coroutine>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1223667897(__this, method) ((  void (*) (Enumerator_t1239345508 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Coroutine>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1641296626(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1239345508 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Coroutine>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3893266603(__this, method) ((  Il2CppObject * (*) (Enumerator_t1239345508 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Coroutine>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3923637779(__this, method) ((  Il2CppObject * (*) (Enumerator_t1239345508 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Coroutine>::MoveNext()
#define Enumerator_MoveNext_m265328684(__this, method) ((  bool (*) (Enumerator_t1239345508 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Coroutine>::get_Current()
#define Enumerator_get_Current_m1747233363(__this, method) ((  KeyValuePair_2_t1971633324  (*) (Enumerator_t1239345508 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Coroutine>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m880852508(__this, method) ((  String_t* (*) (Enumerator_t1239345508 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Coroutine>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2558169180(__this, method) ((  Coroutine_t2299508840 * (*) (Enumerator_t1239345508 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Coroutine>::Reset()
#define Enumerator_Reset_m2739138338(__this, method) ((  void (*) (Enumerator_t1239345508 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Coroutine>::VerifyState()
#define Enumerator_VerifyState_m234880299(__this, method) ((  void (*) (Enumerator_t1239345508 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Coroutine>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m749096465(__this, method) ((  void (*) (Enumerator_t1239345508 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Coroutine>::Dispose()
#define Enumerator_Dispose_m2775790236(__this, method) ((  void (*) (Enumerator_t1239345508 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
