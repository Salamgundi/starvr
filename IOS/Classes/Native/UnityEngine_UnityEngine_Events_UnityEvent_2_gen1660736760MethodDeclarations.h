﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen2508261327MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityEvent`2<SteamVR_RenderModel,System.Boolean>::.ctor()
#define UnityEvent_2__ctor_m2355170122(__this, method) ((  void (*) (UnityEvent_2_t1660736760 *, const MethodInfo*))UnityEvent_2__ctor_m4064166462_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<SteamVR_RenderModel,System.Boolean>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
#define UnityEvent_2_AddListener_m1573112973(__this, ___call0, method) ((  void (*) (UnityEvent_2_t1660736760 *, UnityAction_2_t4073506138 *, const MethodInfo*))UnityEvent_2_AddListener_m3913511359_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<SteamVR_RenderModel,System.Boolean>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
#define UnityEvent_2_RemoveListener_m3384497070(__this, ___call0, method) ((  void (*) (UnityEvent_2_t1660736760 *, UnityAction_2_t4073506138 *, const MethodInfo*))UnityEvent_2_RemoveListener_m87176962_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<SteamVR_RenderModel,System.Boolean>::FindMethod_Impl(System.String,System.Object)
#define UnityEvent_2_FindMethod_Impl_m2233648453(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t1660736760 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m3929411811_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<SteamVR_RenderModel,System.Boolean>::GetDelegate(System.Object,System.Reflection.MethodInfo)
#define UnityEvent_2_GetDelegate_m617869173(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t1660736760 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m4060621883_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<SteamVR_RenderModel,System.Boolean>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
#define UnityEvent_2_GetDelegate_m428117324(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t4073506138 *, const MethodInfo*))UnityEvent_2_GetDelegate_m1561906888_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<SteamVR_RenderModel,System.Boolean>::Invoke(T0,T1)
#define UnityEvent_2_Invoke_m2581524801(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t1660736760 *, SteamVR_RenderModel_t2905485978 *, bool, const MethodInfo*))UnityEvent_2_Invoke_m202592039_gshared)(__this, ___arg00, ___arg11, method)
