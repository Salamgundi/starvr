﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetHighQualityOverlay
struct _GetHighQualityOverlay_t362587296;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetHighQualityOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetHighQualityOverlay__ctor_m3799215923 (_GetHighQualityOverlay_t362587296 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.IVROverlay/_GetHighQualityOverlay::Invoke()
extern "C"  uint64_t _GetHighQualityOverlay_Invoke_m1799682823 (_GetHighQualityOverlay_t362587296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetHighQualityOverlay::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetHighQualityOverlay_BeginInvoke_m2433886906 (_GetHighQualityOverlay_t362587296 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.IVROverlay/_GetHighQualityOverlay::EndInvoke(System.IAsyncResult)
extern "C"  uint64_t _GetHighQualityOverlay_EndInvoke_m3175502549 (_GetHighQualityOverlay_t362587296 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
