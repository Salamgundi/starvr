﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrReticlePointer
struct GvrReticlePointer_t2836438988;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrReticlePointer::.ctor()
extern "C"  void GvrReticlePointer__ctor_m2416667207 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::Awake()
extern "C"  void GvrReticlePointer_Awake_m1239880840 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::Start()
extern "C"  void GvrReticlePointer_Start_m2978962707 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::Update()
extern "C"  void GvrReticlePointer_Update_m4104086346 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::SetAsMainPointer()
extern "C"  void GvrReticlePointer_SetAsMainPointer_m266935859 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::CreateReticleVertices()
extern "C"  void GvrReticlePointer_CreateReticleVertices_m2589246972 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::UpdateReticleProperties()
extern "C"  void GvrReticlePointer_UpdateReticleProperties_m4145231559 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
