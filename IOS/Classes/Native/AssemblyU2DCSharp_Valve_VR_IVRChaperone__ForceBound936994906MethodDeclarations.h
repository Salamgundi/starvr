﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperone/_ForceBoundsVisible
struct _ForceBoundsVisible_t936994906;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperone/_ForceBoundsVisible::.ctor(System.Object,System.IntPtr)
extern "C"  void _ForceBoundsVisible__ctor_m2871360013 (_ForceBoundsVisible_t936994906 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperone/_ForceBoundsVisible::Invoke(System.Boolean)
extern "C"  void _ForceBoundsVisible_Invoke_m3507002484 (_ForceBoundsVisible_t936994906 * __this, bool ___bForce0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperone/_ForceBoundsVisible::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ForceBoundsVisible_BeginInvoke_m146847221 (_ForceBoundsVisible_t936994906 * __this, bool ___bForce0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperone/_ForceBoundsVisible::EndInvoke(System.IAsyncResult)
extern "C"  void _ForceBoundsVisible_EndInvoke_m3164039859 (_ForceBoundsVisible_t936994906 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
