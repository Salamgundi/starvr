﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ObjectTooltip
struct VRTK_ObjectTooltip_t333831714;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void VRTK.VRTK_ObjectTooltip::.ctor()
extern "C"  void VRTK_ObjectTooltip__ctor_m1460009836 (VRTK_ObjectTooltip_t333831714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectTooltip::ResetTooltip()
extern "C"  void VRTK_ObjectTooltip_ResetTooltip_m618492416 (VRTK_ObjectTooltip_t333831714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectTooltip::UpdateText(System.String)
extern "C"  void VRTK_ObjectTooltip_UpdateText_m188977522 (VRTK_ObjectTooltip_t333831714 * __this, String_t* ___newText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectTooltip::Start()
extern "C"  void VRTK_ObjectTooltip_Start_m2753761584 (VRTK_ObjectTooltip_t333831714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectTooltip::Update()
extern "C"  void VRTK_ObjectTooltip_Update_m1174606487 (VRTK_ObjectTooltip_t333831714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectTooltip::SetContainer()
extern "C"  void VRTK_ObjectTooltip_SetContainer_m264733143 (VRTK_ObjectTooltip_t333831714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectTooltip::SetText(System.String)
extern "C"  void VRTK_ObjectTooltip_SetText_m3787763519 (VRTK_ObjectTooltip_t333831714 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectTooltip::SetLine()
extern "C"  void VRTK_ObjectTooltip_SetLine_m1414098006 (VRTK_ObjectTooltip_t333831714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectTooltip::DrawLine()
extern "C"  void VRTK_ObjectTooltip_DrawLine_m2680288494 (VRTK_ObjectTooltip_t333831714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
