﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceComponent
struct _SetOverlayTransformTrackedDeviceComponent_t1749665136;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceComponent::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayTransformTrackedDeviceComponent__ctor_m404352569 (_SetOverlayTransformTrackedDeviceComponent_t1749665136 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceComponent::Invoke(System.UInt64,System.UInt32,System.String)
extern "C"  int32_t _SetOverlayTransformTrackedDeviceComponent_Invoke_m2742715876 (_SetOverlayTransformTrackedDeviceComponent_t1749665136 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unDeviceIndex1, String_t* ___pchComponentName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceComponent::BeginInvoke(System.UInt64,System.UInt32,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetOverlayTransformTrackedDeviceComponent_BeginInvoke_m1125179223 (_SetOverlayTransformTrackedDeviceComponent_t1749665136 * __this, uint64_t ___ulOverlayHandle0, uint32_t ___unDeviceIndex1, String_t* ___pchComponentName2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTransformTrackedDeviceComponent::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayTransformTrackedDeviceComponent_EndInvoke_m3757268983 (_SetOverlayTransformTrackedDeviceComponent_t1749665136 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
