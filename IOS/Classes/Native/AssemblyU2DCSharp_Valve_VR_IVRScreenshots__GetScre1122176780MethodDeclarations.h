﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRScreenshots/_GetScreenshotPropertyFilename
struct _GetScreenshotPropertyFilename_t1122176780;
// System.Object
struct Il2CppObject;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotPropertyFile29427162.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotError1400268927.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRScreenshots/_GetScreenshotPropertyFilename::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetScreenshotPropertyFilename__ctor_m1609003829 (_GetScreenshotPropertyFilename_t1122176780 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRScreenshots/_GetScreenshotPropertyFilename::Invoke(System.UInt32,Valve.VR.EVRScreenshotPropertyFilenames,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRScreenshotError&)
extern "C"  uint32_t _GetScreenshotPropertyFilename_Invoke_m3641906837 (_GetScreenshotPropertyFilename_t1122176780 * __this, uint32_t ___screenshotHandle0, int32_t ___filenameType1, StringBuilder_t1221177846 * ___pchFilename2, uint32_t ___cchFilename3, int32_t* ___pError4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRScreenshots/_GetScreenshotPropertyFilename::BeginInvoke(System.UInt32,Valve.VR.EVRScreenshotPropertyFilenames,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRScreenshotError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetScreenshotPropertyFilename_BeginInvoke_m2793546033 (_GetScreenshotPropertyFilename_t1122176780 * __this, uint32_t ___screenshotHandle0, int32_t ___filenameType1, StringBuilder_t1221177846 * ___pchFilename2, uint32_t ___cchFilename3, int32_t* ___pError4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRScreenshots/_GetScreenshotPropertyFilename::EndInvoke(Valve.VR.EVRScreenshotError&,System.IAsyncResult)
extern "C"  uint32_t _GetScreenshotPropertyFilename_EndInvoke_m872640387 (_GetScreenshotPropertyFilename_t1122176780 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
