﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction
struct VRTK_ControlDirectionGrabAction_t1241164474;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// VRTK.VRTK_InteractGrab
struct VRTK_InteractGrab_t124353446;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractGrab124353446.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::.ctor()
extern "C"  void VRTK_ControlDirectionGrabAction__ctor_m4148521799 (VRTK_ControlDirectionGrabAction_t1241164474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::Initialise(VRTK.VRTK_InteractableObject,VRTK.VRTK_InteractGrab,VRTK.VRTK_InteractGrab,UnityEngine.Transform,UnityEngine.Transform)
extern "C"  void VRTK_ControlDirectionGrabAction_Initialise_m1028772254 (VRTK_ControlDirectionGrabAction_t1241164474 * __this, VRTK_InteractableObject_t2604188111 * ___currentGrabbdObject0, VRTK_InteractGrab_t124353446 * ___currentPrimaryGrabbingObject1, VRTK_InteractGrab_t124353446 * ___currentSecondaryGrabbingObject2, Transform_t3275118058 * ___primaryGrabPoint3, Transform_t3275118058 * ___secondaryGrabPoint4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::ResetAction()
extern "C"  void VRTK_ControlDirectionGrabAction_ResetAction_m2386072058 (VRTK_ControlDirectionGrabAction_t1241164474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::OnDropAction()
extern "C"  void VRTK_ControlDirectionGrabAction_OnDropAction_m2701542593 (VRTK_ControlDirectionGrabAction_t1241164474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::ProcessUpdate()
extern "C"  void VRTK_ControlDirectionGrabAction_ProcessUpdate_m2767707521 (VRTK_ControlDirectionGrabAction_t1241164474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::ProcessFixedUpdate()
extern "C"  void VRTK_ControlDirectionGrabAction_ProcessFixedUpdate_m398207795 (VRTK_ControlDirectionGrabAction_t1241164474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::StopRealignOnRelease()
extern "C"  void VRTK_ControlDirectionGrabAction_StopRealignOnRelease_m1455806877 (VRTK_ControlDirectionGrabAction_t1241164474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::RealignOnRelease()
extern "C"  Il2CppObject * VRTK_ControlDirectionGrabAction_RealignOnRelease_m2717075059 (VRTK_ControlDirectionGrabAction_t1241164474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::AimObject()
extern "C"  void VRTK_ControlDirectionGrabAction_AimObject_m4172646303 (VRTK_ControlDirectionGrabAction_t1241164474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_ControlDirectionGrabAction::ZLockedAim()
extern "C"  void VRTK_ControlDirectionGrabAction_ZLockedAim_m2514793198 (VRTK_ControlDirectionGrabAction_t1241164474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
