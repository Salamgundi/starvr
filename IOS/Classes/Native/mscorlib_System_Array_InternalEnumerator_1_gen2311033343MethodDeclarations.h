﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2311033343.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21452281081.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4273781177_gshared (InternalEnumerator_1_t2311033343 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4273781177(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2311033343 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4273781177_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2900799557_gshared (InternalEnumerator_1_t2311033343 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2900799557(__this, method) ((  void (*) (InternalEnumerator_1_t2311033343 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2900799557_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m850308093_gshared (InternalEnumerator_1_t2311033343 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m850308093(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2311033343 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m850308093_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1810311080_gshared (InternalEnumerator_1_t2311033343 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1810311080(__this, method) ((  void (*) (InternalEnumerator_1_t2311033343 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1810311080_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m186401949_gshared (InternalEnumerator_1_t2311033343 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m186401949(__this, method) ((  bool (*) (InternalEnumerator_1_t2311033343 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m186401949_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Valve.VR.EVREventType,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1452281081  InternalEnumerator_1_get_Current_m4201080002_gshared (InternalEnumerator_1_t2311033343 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4201080002(__this, method) ((  KeyValuePair_2_t1452281081  (*) (InternalEnumerator_1_t2311033343 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4201080002_gshared)(__this, method)
