﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_TestController
struct SteamVR_TestController_t1860367601;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_TestController::.ctor()
extern "C"  void SteamVR_TestController__ctor_m572197988 (SteamVR_TestController_t1860367601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TestController::OnDeviceConnected(System.Int32,System.Boolean)
extern "C"  void SteamVR_TestController_OnDeviceConnected_m4100079190 (SteamVR_TestController_t1860367601 * __this, int32_t ___index0, bool ___connected1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TestController::OnEnable()
extern "C"  void SteamVR_TestController_OnEnable_m2154162396 (SteamVR_TestController_t1860367601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TestController::OnDisable()
extern "C"  void SteamVR_TestController_OnDisable_m2573001473 (SteamVR_TestController_t1860367601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TestController::PrintControllerStatus(System.Int32)
extern "C"  void SteamVR_TestController_PrintControllerStatus_m339964524 (SteamVR_TestController_t1860367601 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TestController::Update()
extern "C"  void SteamVR_TestController_Update_m214118429 (SteamVR_TestController_t1860367601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
