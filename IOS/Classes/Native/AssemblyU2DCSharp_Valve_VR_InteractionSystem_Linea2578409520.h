﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.LinearMapping
struct LinearMapping_t810676855;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.LinearDisplacement
struct  LinearDisplacement_t2578409520  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.LinearDisplacement::displacement
	Vector3_t2243707580  ___displacement_2;
	// Valve.VR.InteractionSystem.LinearMapping Valve.VR.InteractionSystem.LinearDisplacement::linearMapping
	LinearMapping_t810676855 * ___linearMapping_3;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.LinearDisplacement::initialPosition
	Vector3_t2243707580  ___initialPosition_4;

public:
	inline static int32_t get_offset_of_displacement_2() { return static_cast<int32_t>(offsetof(LinearDisplacement_t2578409520, ___displacement_2)); }
	inline Vector3_t2243707580  get_displacement_2() const { return ___displacement_2; }
	inline Vector3_t2243707580 * get_address_of_displacement_2() { return &___displacement_2; }
	inline void set_displacement_2(Vector3_t2243707580  value)
	{
		___displacement_2 = value;
	}

	inline static int32_t get_offset_of_linearMapping_3() { return static_cast<int32_t>(offsetof(LinearDisplacement_t2578409520, ___linearMapping_3)); }
	inline LinearMapping_t810676855 * get_linearMapping_3() const { return ___linearMapping_3; }
	inline LinearMapping_t810676855 ** get_address_of_linearMapping_3() { return &___linearMapping_3; }
	inline void set_linearMapping_3(LinearMapping_t810676855 * value)
	{
		___linearMapping_3 = value;
		Il2CppCodeGenWriteBarrier(&___linearMapping_3, value);
	}

	inline static int32_t get_offset_of_initialPosition_4() { return static_cast<int32_t>(offsetof(LinearDisplacement_t2578409520, ___initialPosition_4)); }
	inline Vector3_t2243707580  get_initialPosition_4() const { return ___initialPosition_4; }
	inline Vector3_t2243707580 * get_address_of_initialPosition_4() { return &___initialPosition_4; }
	inline void set_initialPosition_4(Vector3_t2243707580  value)
	{
		___initialPosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
