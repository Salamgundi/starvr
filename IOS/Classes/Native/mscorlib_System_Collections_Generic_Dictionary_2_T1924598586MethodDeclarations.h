﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3517915223MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Transform,System.Single,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m2667794791(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1924598586 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3974524280_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Transform,System.Single,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3440964307(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t1924598586 *, Transform_t3275118058 *, float, const MethodInfo*))Transform_1_Invoke_m2819829432_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Transform,System.Single,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m2645128852(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1924598586 *, Transform_t3275118058 *, float, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1736815753_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Transform,System.Single,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m3382963933(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t1924598586 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m2144263142_gshared)(__this, ___result0, method)
