﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ArrowHand
struct ArrowHand_t565341420;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"

// System.Void Valve.VR.InteractionSystem.ArrowHand::.ctor()
extern "C"  void ArrowHand__ctor_m2420412808 (ArrowHand_t565341420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArrowHand::Awake()
extern "C"  void ArrowHand_Awake_m537490349 (ArrowHand_t565341420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArrowHand::OnAttachedToHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void ArrowHand_OnAttachedToHand_m2198639973 (ArrowHand_t565341420 * __this, Hand_t379716353 * ___attachedHand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Valve.VR.InteractionSystem.ArrowHand::InstantiateArrow()
extern "C"  GameObject_t1756533147 * ArrowHand_InstantiateArrow_m2413476874 (ArrowHand_t565341420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArrowHand::HandAttachedUpdate(Valve.VR.InteractionSystem.Hand)
extern "C"  void ArrowHand_HandAttachedUpdate_m3826511602 (ArrowHand_t565341420 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArrowHand::OnDetachedFromHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void ArrowHand_OnDetachedFromHand_m1796790296 (ArrowHand_t565341420 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArrowHand::FireArrow()
extern "C"  void ArrowHand_FireArrow_m277246121 (ArrowHand_t565341420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArrowHand::EnableArrowSpawn()
extern "C"  void ArrowHand_EnableArrowSpawn_m3408571067 (ArrowHand_t565341420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.ArrowHand::ArrowReleaseHaptics()
extern "C"  Il2CppObject * ArrowHand_ArrowReleaseHaptics_m3560855482 (ArrowHand_t565341420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArrowHand::OnHandFocusLost(Valve.VR.InteractionSystem.Hand)
extern "C"  void ArrowHand_OnHandFocusLost_m1368616982 (ArrowHand_t565341420 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArrowHand::OnHandFocusAcquired(Valve.VR.InteractionSystem.Hand)
extern "C"  void ArrowHand_OnHandFocusAcquired_m3858745620 (ArrowHand_t565341420 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ArrowHand::FindBow()
extern "C"  void ArrowHand_FindBow_m522750659 (ArrowHand_t565341420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
