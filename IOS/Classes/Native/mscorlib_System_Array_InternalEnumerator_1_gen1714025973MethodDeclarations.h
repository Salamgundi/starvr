﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1714025973.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_CurveGenerator_BezierCo855273711.h"

// System.Void System.Array/InternalEnumerator`1<VRTK.VRTK_CurveGenerator/BezierControlPointMode>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m936015828_gshared (InternalEnumerator_1_t1714025973 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m936015828(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1714025973 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m936015828_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<VRTK.VRTK_CurveGenerator/BezierControlPointMode>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2332365524_gshared (InternalEnumerator_1_t1714025973 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2332365524(__this, method) ((  void (*) (InternalEnumerator_1_t1714025973 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2332365524_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<VRTK.VRTK_CurveGenerator/BezierControlPointMode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1153692170_gshared (InternalEnumerator_1_t1714025973 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1153692170(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1714025973 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1153692170_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<VRTK.VRTK_CurveGenerator/BezierControlPointMode>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3216873599_gshared (InternalEnumerator_1_t1714025973 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3216873599(__this, method) ((  void (*) (InternalEnumerator_1_t1714025973 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3216873599_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<VRTK.VRTK_CurveGenerator/BezierControlPointMode>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2401863228_gshared (InternalEnumerator_1_t1714025973 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2401863228(__this, method) ((  bool (*) (InternalEnumerator_1_t1714025973 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2401863228_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<VRTK.VRTK_CurveGenerator/BezierControlPointMode>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1561421699_gshared (InternalEnumerator_1_t1714025973 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1561421699(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1714025973 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1561421699_gshared)(__this, method)
