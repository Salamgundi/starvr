﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.InteractableObjectEventArgs>
struct InvokableCall_2_t1583422427;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.InteractableObjectEventArgs>
struct UnityAction_2_t1568631543;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.InteractableObjectEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m2929970813_gshared (InvokableCall_2_t1583422427 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_2__ctor_m2929970813(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_2_t1583422427 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2__ctor_m2929970813_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.InteractableObjectEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m4044741794_gshared (InvokableCall_2_t1583422427 * __this, UnityAction_2_t1568631543 * ___action0, const MethodInfo* method);
#define InvokableCall_2__ctor_m4044741794(__this, ___action0, method) ((  void (*) (InvokableCall_2_t1583422427 *, UnityAction_2_t1568631543 *, const MethodInfo*))InvokableCall_2__ctor_m4044741794_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.InteractableObjectEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m951544561_gshared (InvokableCall_2_t1583422427 * __this, UnityAction_2_t1568631543 * ___value0, const MethodInfo* method);
#define InvokableCall_2_add_Delegate_m951544561(__this, ___value0, method) ((  void (*) (InvokableCall_2_t1583422427 *, UnityAction_2_t1568631543 *, const MethodInfo*))InvokableCall_2_add_Delegate_m951544561_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.InteractableObjectEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m2157615652_gshared (InvokableCall_2_t1583422427 * __this, UnityAction_2_t1568631543 * ___value0, const MethodInfo* method);
#define InvokableCall_2_remove_Delegate_m2157615652(__this, ___value0, method) ((  void (*) (InvokableCall_2_t1583422427 *, UnityAction_2_t1568631543 *, const MethodInfo*))InvokableCall_2_remove_Delegate_m2157615652_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.InteractableObjectEventArgs>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m2098480210_gshared (InvokableCall_2_t1583422427 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_2_Invoke_m2098480210(__this, ___args0, method) ((  void (*) (InvokableCall_2_t1583422427 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_2_Invoke_m2098480210_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.InteractableObjectEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m4083968974_gshared (InvokableCall_2_t1583422427 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_2_Find_m4083968974(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_2_t1583422427 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2_Find_m4083968974_gshared)(__this, ___targetObj0, ___method1, method)
