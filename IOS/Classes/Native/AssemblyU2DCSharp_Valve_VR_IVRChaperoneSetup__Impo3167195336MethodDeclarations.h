﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_ImportFromBufferToWorking
struct _ImportFromBufferToWorking_t3167195336;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_ImportFromBufferToWorking::.ctor(System.Object,System.IntPtr)
extern "C"  void _ImportFromBufferToWorking__ctor_m2059311941 (_ImportFromBufferToWorking_t3167195336 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_ImportFromBufferToWorking::Invoke(System.String,System.UInt32)
extern "C"  bool _ImportFromBufferToWorking_Invoke_m3640380195 (_ImportFromBufferToWorking_t3167195336 * __this, String_t* ___pBuffer0, uint32_t ___nImportFlags1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_ImportFromBufferToWorking::BeginInvoke(System.String,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ImportFromBufferToWorking_BeginInvoke_m2960674862 (_ImportFromBufferToWorking_t3167195336 * __this, String_t* ___pBuffer0, uint32_t ___nImportFlags1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_ImportFromBufferToWorking::EndInvoke(System.IAsyncResult)
extern "C"  bool _ImportFromBufferToWorking_EndInvoke_m2487585763 (_ImportFromBufferToWorking_t3167195336 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
