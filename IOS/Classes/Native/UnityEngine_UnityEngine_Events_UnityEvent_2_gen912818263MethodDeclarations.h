﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,VRTK.BodyPhysicsEventArgs>
struct UnityEvent_2_t912818263;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.BodyPhysicsEventArgs>
struct UnityAction_2_t3325587641;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_VRTK_BodyPhysicsEventArgs2230131654.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.BodyPhysicsEventArgs>::.ctor()
extern "C"  void UnityEvent_2__ctor_m2284025281_gshared (UnityEvent_2_t912818263 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m2284025281(__this, method) ((  void (*) (UnityEvent_2_t912818263 *, const MethodInfo*))UnityEvent_2__ctor_m2284025281_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.BodyPhysicsEventArgs>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m1746363901_gshared (UnityEvent_2_t912818263 * __this, UnityAction_2_t3325587641 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m1746363901(__this, ___call0, method) ((  void (*) (UnityEvent_2_t912818263 *, UnityAction_2_t3325587641 *, const MethodInfo*))UnityEvent_2_AddListener_m1746363901_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.BodyPhysicsEventArgs>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m2990562354_gshared (UnityEvent_2_t912818263 * __this, UnityAction_2_t3325587641 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m2990562354(__this, ___call0, method) ((  void (*) (UnityEvent_2_t912818263 *, UnityAction_2_t3325587641 *, const MethodInfo*))UnityEvent_2_RemoveListener_m2990562354_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,VRTK.BodyPhysicsEventArgs>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m3904257629_gshared (UnityEvent_2_t912818263 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m3904257629(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t912818263 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m3904257629_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.BodyPhysicsEventArgs>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m2263809225_gshared (UnityEvent_2_t912818263 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m2263809225(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t912818263 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m2263809225_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.BodyPhysicsEventArgs>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m1675390554_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t3325587641 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m1675390554(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t3325587641 *, const MethodInfo*))UnityEvent_2_GetDelegate_m1675390554_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.BodyPhysicsEventArgs>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m1779473178_gshared (UnityEvent_2_t912818263 * __this, Il2CppObject * ___arg00, BodyPhysicsEventArgs_t2230131654  ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m1779473178(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t912818263 *, Il2CppObject *, BodyPhysicsEventArgs_t2230131654 , const MethodInfo*))UnityEvent_2_Invoke_m1779473178_gshared)(__this, ___arg00, ___arg11, method)
