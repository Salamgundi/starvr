﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.CustomEvents/UnityEventSingleFloat
struct UnityEventSingleFloat_t3482038085;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.CustomEvents/UnityEventSingleFloat::.ctor()
extern "C"  void UnityEventSingleFloat__ctor_m445437720 (UnityEventSingleFloat_t3482038085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
