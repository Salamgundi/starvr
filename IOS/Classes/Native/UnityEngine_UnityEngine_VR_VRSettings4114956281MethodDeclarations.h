﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Boolean UnityEngine.VR.VRSettings::get_enabled()
extern "C"  bool VRSettings_get_enabled_m1122229986 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.VR.VRSettings::get_renderScale()
extern "C"  float VRSettings_get_renderScale_m2233090995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.VRSettings::set_renderScale(System.Single)
extern "C"  void VRSettings_set_renderScale_m2344392922 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.VR.VRSettings::get_eyeTextureWidth()
extern "C"  int32_t VRSettings_get_eyeTextureWidth_m3959004321 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.VR.VRSettings::get_eyeTextureHeight()
extern "C"  int32_t VRSettings_get_eyeTextureHeight_m2634538628 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.VR.VRSettings::get_renderViewportScale()
extern "C"  float VRSettings_get_renderViewportScale_m1939087459 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.VRSettings::set_renderViewportScale(System.Single)
extern "C"  void VRSettings_set_renderViewportScale_m3851603072 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.VR.VRSettings::get_renderViewportScaleInternal()
extern "C"  float VRSettings_get_renderViewportScaleInternal_m2953886208 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.VRSettings::set_renderViewportScaleInternal(System.Single)
extern "C"  void VRSettings_set_renderViewportScaleInternal_m2786194167 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
