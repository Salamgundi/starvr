﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetTrackedDeviceActivityLevel
struct _GetTrackedDeviceActivityLevel_t212130385;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EDeviceActivityLevel886867856.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetTrackedDeviceActivityLevel::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetTrackedDeviceActivityLevel__ctor_m2869817562 (_GetTrackedDeviceActivityLevel_t212130385 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EDeviceActivityLevel Valve.VR.IVRSystem/_GetTrackedDeviceActivityLevel::Invoke(System.UInt32)
extern "C"  int32_t _GetTrackedDeviceActivityLevel_Invoke_m1284377401 (_GetTrackedDeviceActivityLevel_t212130385 * __this, uint32_t ___unDeviceId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetTrackedDeviceActivityLevel::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetTrackedDeviceActivityLevel_BeginInvoke_m111391887 (_GetTrackedDeviceActivityLevel_t212130385 * __this, uint32_t ___unDeviceId0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EDeviceActivityLevel Valve.VR.IVRSystem/_GetTrackedDeviceActivityLevel::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _GetTrackedDeviceActivityLevel_EndInvoke_m293185487 (_GetTrackedDeviceActivityLevel_t212130385 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
