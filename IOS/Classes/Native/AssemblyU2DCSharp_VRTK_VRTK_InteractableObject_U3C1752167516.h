﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2436612143.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_InteractableObject/<RegisterTeleportersAtEndOfFrame>c__Iterator0
struct  U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<VRTK.VRTK_BasicTeleport> VRTK.VRTK_InteractableObject/<RegisterTeleportersAtEndOfFrame>c__Iterator0::$locvar0
	Enumerator_t2436612143  ___U24locvar0_0;
	// VRTK.VRTK_InteractableObject VRTK.VRTK_InteractableObject/<RegisterTeleportersAtEndOfFrame>c__Iterator0::$this
	VRTK_InteractableObject_t2604188111 * ___U24this_1;
	// System.Object VRTK.VRTK_InteractableObject/<RegisterTeleportersAtEndOfFrame>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean VRTK.VRTK_InteractableObject/<RegisterTeleportersAtEndOfFrame>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 VRTK.VRTK_InteractableObject/<RegisterTeleportersAtEndOfFrame>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516, ___U24locvar0_0)); }
	inline Enumerator_t2436612143  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t2436612143 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t2436612143  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516, ___U24this_1)); }
	inline VRTK_InteractableObject_t2604188111 * get_U24this_1() const { return ___U24this_1; }
	inline VRTK_InteractableObject_t2604188111 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(VRTK_InteractableObject_t2604188111 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
