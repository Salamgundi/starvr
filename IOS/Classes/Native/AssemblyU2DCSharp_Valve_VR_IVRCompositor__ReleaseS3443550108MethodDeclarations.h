﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_ReleaseSharedGLTexture
struct _ReleaseSharedGLTexture_t3443550108;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_ReleaseSharedGLTexture::.ctor(System.Object,System.IntPtr)
extern "C"  void _ReleaseSharedGLTexture__ctor_m206437887 (_ReleaseSharedGLTexture_t3443550108 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRCompositor/_ReleaseSharedGLTexture::Invoke(System.UInt32,System.IntPtr)
extern "C"  bool _ReleaseSharedGLTexture_Invoke_m2480353953 (_ReleaseSharedGLTexture_t3443550108 * __this, uint32_t ___glTextureId0, IntPtr_t ___glSharedTextureHandle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_ReleaseSharedGLTexture::BeginInvoke(System.UInt32,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ReleaseSharedGLTexture_BeginInvoke_m2703308826 (_ReleaseSharedGLTexture_t3443550108 * __this, uint32_t ___glTextureId0, IntPtr_t ___glSharedTextureHandle1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRCompositor/_ReleaseSharedGLTexture::EndInvoke(System.IAsyncResult)
extern "C"  bool _ReleaseSharedGLTexture_EndInvoke_m2308413305 (_ReleaseSharedGLTexture_t3443550108 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
