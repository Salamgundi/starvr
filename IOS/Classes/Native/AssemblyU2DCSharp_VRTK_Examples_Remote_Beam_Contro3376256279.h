﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.Examples.Remote_Beam
struct Remote_Beam_t3772907446;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Remote_Beam_Controller
struct  Remote_Beam_Controller_t3376256279  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject VRTK.Examples.Remote_Beam_Controller::remoteBeam
	GameObject_t1756533147 * ___remoteBeam_2;
	// VRTK.Examples.Remote_Beam VRTK.Examples.Remote_Beam_Controller::remoteBeamScript
	Remote_Beam_t3772907446 * ___remoteBeamScript_3;

public:
	inline static int32_t get_offset_of_remoteBeam_2() { return static_cast<int32_t>(offsetof(Remote_Beam_Controller_t3376256279, ___remoteBeam_2)); }
	inline GameObject_t1756533147 * get_remoteBeam_2() const { return ___remoteBeam_2; }
	inline GameObject_t1756533147 ** get_address_of_remoteBeam_2() { return &___remoteBeam_2; }
	inline void set_remoteBeam_2(GameObject_t1756533147 * value)
	{
		___remoteBeam_2 = value;
		Il2CppCodeGenWriteBarrier(&___remoteBeam_2, value);
	}

	inline static int32_t get_offset_of_remoteBeamScript_3() { return static_cast<int32_t>(offsetof(Remote_Beam_Controller_t3376256279, ___remoteBeamScript_3)); }
	inline Remote_Beam_t3772907446 * get_remoteBeamScript_3() const { return ___remoteBeamScript_3; }
	inline Remote_Beam_t3772907446 ** get_address_of_remoteBeamScript_3() { return &___remoteBeamScript_3; }
	inline void set_remoteBeamScript_3(Remote_Beam_t3772907446 * value)
	{
		___remoteBeamScript_3 = value;
		Il2CppCodeGenWriteBarrier(&___remoteBeamScript_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
