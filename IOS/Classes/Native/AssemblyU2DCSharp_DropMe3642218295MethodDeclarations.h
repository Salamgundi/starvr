﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DropMe
struct DropMe_t3642218295;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void DropMe::.ctor()
extern "C"  void DropMe__ctor_m1838141246 (DropMe_t3642218295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DropMe::OnEnable()
extern "C"  void DropMe_OnEnable_m1124426402 (DropMe_t3642218295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DropMe::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DropMe_OnDrop_m2874514420 (DropMe_t3642218295 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DropMe::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DropMe_OnPointerEnter_m1374481992 (DropMe_t3642218295 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DropMe::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DropMe_OnPointerExit_m4066393732 (DropMe_t3642218295 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite DropMe::GetDropSprite(UnityEngine.EventSystems.PointerEventData)
extern "C"  Sprite_t309593783 * DropMe_GetDropSprite_m285790665 (DropMe_t3642218295 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
