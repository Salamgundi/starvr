﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t2203355011;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.PlaySound
struct  PlaySound_t165629647  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioClip[] Valve.VR.InteractionSystem.PlaySound::waveFile
	AudioClipU5BU5D_t2203355011* ___waveFile_2;
	// System.Boolean Valve.VR.InteractionSystem.PlaySound::stopOnPlay
	bool ___stopOnPlay_3;
	// System.Boolean Valve.VR.InteractionSystem.PlaySound::disableOnEnd
	bool ___disableOnEnd_4;
	// System.Boolean Valve.VR.InteractionSystem.PlaySound::looping
	bool ___looping_5;
	// System.Boolean Valve.VR.InteractionSystem.PlaySound::stopOnEnd
	bool ___stopOnEnd_6;
	// System.Boolean Valve.VR.InteractionSystem.PlaySound::playOnAwakeWithDelay
	bool ___playOnAwakeWithDelay_7;
	// System.Boolean Valve.VR.InteractionSystem.PlaySound::useRandomVolume
	bool ___useRandomVolume_8;
	// System.Single Valve.VR.InteractionSystem.PlaySound::volMin
	float ___volMin_9;
	// System.Single Valve.VR.InteractionSystem.PlaySound::volMax
	float ___volMax_10;
	// System.Boolean Valve.VR.InteractionSystem.PlaySound::useRandomPitch
	bool ___useRandomPitch_11;
	// System.Single Valve.VR.InteractionSystem.PlaySound::pitchMin
	float ___pitchMin_12;
	// System.Single Valve.VR.InteractionSystem.PlaySound::pitchMax
	float ___pitchMax_13;
	// System.Boolean Valve.VR.InteractionSystem.PlaySound::useRetriggerTime
	bool ___useRetriggerTime_14;
	// System.Single Valve.VR.InteractionSystem.PlaySound::timeInitial
	float ___timeInitial_15;
	// System.Single Valve.VR.InteractionSystem.PlaySound::timeMin
	float ___timeMin_16;
	// System.Single Valve.VR.InteractionSystem.PlaySound::timeMax
	float ___timeMax_17;
	// System.Boolean Valve.VR.InteractionSystem.PlaySound::useRandomSilence
	bool ___useRandomSilence_18;
	// System.Single Valve.VR.InteractionSystem.PlaySound::percentToNotPlay
	float ___percentToNotPlay_19;
	// System.Single Valve.VR.InteractionSystem.PlaySound::delayOffsetTime
	float ___delayOffsetTime_20;
	// UnityEngine.AudioSource Valve.VR.InteractionSystem.PlaySound::audioSource
	AudioSource_t1135106623 * ___audioSource_21;
	// UnityEngine.AudioClip Valve.VR.InteractionSystem.PlaySound::clip
	AudioClip_t1932558630 * ___clip_22;

public:
	inline static int32_t get_offset_of_waveFile_2() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___waveFile_2)); }
	inline AudioClipU5BU5D_t2203355011* get_waveFile_2() const { return ___waveFile_2; }
	inline AudioClipU5BU5D_t2203355011** get_address_of_waveFile_2() { return &___waveFile_2; }
	inline void set_waveFile_2(AudioClipU5BU5D_t2203355011* value)
	{
		___waveFile_2 = value;
		Il2CppCodeGenWriteBarrier(&___waveFile_2, value);
	}

	inline static int32_t get_offset_of_stopOnPlay_3() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___stopOnPlay_3)); }
	inline bool get_stopOnPlay_3() const { return ___stopOnPlay_3; }
	inline bool* get_address_of_stopOnPlay_3() { return &___stopOnPlay_3; }
	inline void set_stopOnPlay_3(bool value)
	{
		___stopOnPlay_3 = value;
	}

	inline static int32_t get_offset_of_disableOnEnd_4() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___disableOnEnd_4)); }
	inline bool get_disableOnEnd_4() const { return ___disableOnEnd_4; }
	inline bool* get_address_of_disableOnEnd_4() { return &___disableOnEnd_4; }
	inline void set_disableOnEnd_4(bool value)
	{
		___disableOnEnd_4 = value;
	}

	inline static int32_t get_offset_of_looping_5() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___looping_5)); }
	inline bool get_looping_5() const { return ___looping_5; }
	inline bool* get_address_of_looping_5() { return &___looping_5; }
	inline void set_looping_5(bool value)
	{
		___looping_5 = value;
	}

	inline static int32_t get_offset_of_stopOnEnd_6() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___stopOnEnd_6)); }
	inline bool get_stopOnEnd_6() const { return ___stopOnEnd_6; }
	inline bool* get_address_of_stopOnEnd_6() { return &___stopOnEnd_6; }
	inline void set_stopOnEnd_6(bool value)
	{
		___stopOnEnd_6 = value;
	}

	inline static int32_t get_offset_of_playOnAwakeWithDelay_7() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___playOnAwakeWithDelay_7)); }
	inline bool get_playOnAwakeWithDelay_7() const { return ___playOnAwakeWithDelay_7; }
	inline bool* get_address_of_playOnAwakeWithDelay_7() { return &___playOnAwakeWithDelay_7; }
	inline void set_playOnAwakeWithDelay_7(bool value)
	{
		___playOnAwakeWithDelay_7 = value;
	}

	inline static int32_t get_offset_of_useRandomVolume_8() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___useRandomVolume_8)); }
	inline bool get_useRandomVolume_8() const { return ___useRandomVolume_8; }
	inline bool* get_address_of_useRandomVolume_8() { return &___useRandomVolume_8; }
	inline void set_useRandomVolume_8(bool value)
	{
		___useRandomVolume_8 = value;
	}

	inline static int32_t get_offset_of_volMin_9() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___volMin_9)); }
	inline float get_volMin_9() const { return ___volMin_9; }
	inline float* get_address_of_volMin_9() { return &___volMin_9; }
	inline void set_volMin_9(float value)
	{
		___volMin_9 = value;
	}

	inline static int32_t get_offset_of_volMax_10() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___volMax_10)); }
	inline float get_volMax_10() const { return ___volMax_10; }
	inline float* get_address_of_volMax_10() { return &___volMax_10; }
	inline void set_volMax_10(float value)
	{
		___volMax_10 = value;
	}

	inline static int32_t get_offset_of_useRandomPitch_11() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___useRandomPitch_11)); }
	inline bool get_useRandomPitch_11() const { return ___useRandomPitch_11; }
	inline bool* get_address_of_useRandomPitch_11() { return &___useRandomPitch_11; }
	inline void set_useRandomPitch_11(bool value)
	{
		___useRandomPitch_11 = value;
	}

	inline static int32_t get_offset_of_pitchMin_12() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___pitchMin_12)); }
	inline float get_pitchMin_12() const { return ___pitchMin_12; }
	inline float* get_address_of_pitchMin_12() { return &___pitchMin_12; }
	inline void set_pitchMin_12(float value)
	{
		___pitchMin_12 = value;
	}

	inline static int32_t get_offset_of_pitchMax_13() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___pitchMax_13)); }
	inline float get_pitchMax_13() const { return ___pitchMax_13; }
	inline float* get_address_of_pitchMax_13() { return &___pitchMax_13; }
	inline void set_pitchMax_13(float value)
	{
		___pitchMax_13 = value;
	}

	inline static int32_t get_offset_of_useRetriggerTime_14() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___useRetriggerTime_14)); }
	inline bool get_useRetriggerTime_14() const { return ___useRetriggerTime_14; }
	inline bool* get_address_of_useRetriggerTime_14() { return &___useRetriggerTime_14; }
	inline void set_useRetriggerTime_14(bool value)
	{
		___useRetriggerTime_14 = value;
	}

	inline static int32_t get_offset_of_timeInitial_15() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___timeInitial_15)); }
	inline float get_timeInitial_15() const { return ___timeInitial_15; }
	inline float* get_address_of_timeInitial_15() { return &___timeInitial_15; }
	inline void set_timeInitial_15(float value)
	{
		___timeInitial_15 = value;
	}

	inline static int32_t get_offset_of_timeMin_16() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___timeMin_16)); }
	inline float get_timeMin_16() const { return ___timeMin_16; }
	inline float* get_address_of_timeMin_16() { return &___timeMin_16; }
	inline void set_timeMin_16(float value)
	{
		___timeMin_16 = value;
	}

	inline static int32_t get_offset_of_timeMax_17() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___timeMax_17)); }
	inline float get_timeMax_17() const { return ___timeMax_17; }
	inline float* get_address_of_timeMax_17() { return &___timeMax_17; }
	inline void set_timeMax_17(float value)
	{
		___timeMax_17 = value;
	}

	inline static int32_t get_offset_of_useRandomSilence_18() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___useRandomSilence_18)); }
	inline bool get_useRandomSilence_18() const { return ___useRandomSilence_18; }
	inline bool* get_address_of_useRandomSilence_18() { return &___useRandomSilence_18; }
	inline void set_useRandomSilence_18(bool value)
	{
		___useRandomSilence_18 = value;
	}

	inline static int32_t get_offset_of_percentToNotPlay_19() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___percentToNotPlay_19)); }
	inline float get_percentToNotPlay_19() const { return ___percentToNotPlay_19; }
	inline float* get_address_of_percentToNotPlay_19() { return &___percentToNotPlay_19; }
	inline void set_percentToNotPlay_19(float value)
	{
		___percentToNotPlay_19 = value;
	}

	inline static int32_t get_offset_of_delayOffsetTime_20() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___delayOffsetTime_20)); }
	inline float get_delayOffsetTime_20() const { return ___delayOffsetTime_20; }
	inline float* get_address_of_delayOffsetTime_20() { return &___delayOffsetTime_20; }
	inline void set_delayOffsetTime_20(float value)
	{
		___delayOffsetTime_20 = value;
	}

	inline static int32_t get_offset_of_audioSource_21() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___audioSource_21)); }
	inline AudioSource_t1135106623 * get_audioSource_21() const { return ___audioSource_21; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_21() { return &___audioSource_21; }
	inline void set_audioSource_21(AudioSource_t1135106623 * value)
	{
		___audioSource_21 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_21, value);
	}

	inline static int32_t get_offset_of_clip_22() { return static_cast<int32_t>(offsetof(PlaySound_t165629647, ___clip_22)); }
	inline AudioClip_t1932558630 * get_clip_22() const { return ___clip_22; }
	inline AudioClip_t1932558630 ** get_address_of_clip_22() { return &___clip_22; }
	inline void set_clip_22(AudioClip_t1932558630 * value)
	{
		___clip_22 = value;
		Il2CppCodeGenWriteBarrier(&___clip_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
