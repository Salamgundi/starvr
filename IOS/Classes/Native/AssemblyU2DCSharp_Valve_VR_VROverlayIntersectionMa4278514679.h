﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayIntersectionM2680024489.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayIntersectionMa1248365778.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.VROverlayIntersectionMaskPrimitive_t
struct  VROverlayIntersectionMaskPrimitive_t_t4278514679 
{
public:
	// Valve.VR.EVROverlayIntersectionMaskPrimitiveType Valve.VR.VROverlayIntersectionMaskPrimitive_t::m_nPrimitiveType
	int32_t ___m_nPrimitiveType_0;
	// Valve.VR.VROverlayIntersectionMaskPrimitive_Data_t Valve.VR.VROverlayIntersectionMaskPrimitive_t::m_Primitive
	VROverlayIntersectionMaskPrimitive_Data_t_t1248365778  ___m_Primitive_1;

public:
	inline static int32_t get_offset_of_m_nPrimitiveType_0() { return static_cast<int32_t>(offsetof(VROverlayIntersectionMaskPrimitive_t_t4278514679, ___m_nPrimitiveType_0)); }
	inline int32_t get_m_nPrimitiveType_0() const { return ___m_nPrimitiveType_0; }
	inline int32_t* get_address_of_m_nPrimitiveType_0() { return &___m_nPrimitiveType_0; }
	inline void set_m_nPrimitiveType_0(int32_t value)
	{
		___m_nPrimitiveType_0 = value;
	}

	inline static int32_t get_offset_of_m_Primitive_1() { return static_cast<int32_t>(offsetof(VROverlayIntersectionMaskPrimitive_t_t4278514679, ___m_Primitive_1)); }
	inline VROverlayIntersectionMaskPrimitive_Data_t_t1248365778  get_m_Primitive_1() const { return ___m_Primitive_1; }
	inline VROverlayIntersectionMaskPrimitive_Data_t_t1248365778 * get_address_of_m_Primitive_1() { return &___m_Primitive_1; }
	inline void set_m_Primitive_1(VROverlayIntersectionMaskPrimitive_Data_t_t1248365778  value)
	{
		___m_Primitive_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
