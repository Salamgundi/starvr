﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Skybox
struct SteamVR_Skybox_t52115419;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"

// System.Void SteamVR_Skybox::.ctor()
extern "C"  void SteamVR_Skybox__ctor_m4000697926 (SteamVR_Skybox_t52115419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Skybox::SetTextureByIndex(System.Int32,UnityEngine.Texture)
extern "C"  void SteamVR_Skybox_SetTextureByIndex_m478727067 (SteamVR_Skybox_t52115419 * __this, int32_t ___i0, Texture_t2243626319 * ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture SteamVR_Skybox::GetTextureByIndex(System.Int32)
extern "C"  Texture_t2243626319 * SteamVR_Skybox_GetTextureByIndex_m1416899136 (SteamVR_Skybox_t52115419 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Skybox::SetOverride(UnityEngine.Texture,UnityEngine.Texture,UnityEngine.Texture,UnityEngine.Texture,UnityEngine.Texture,UnityEngine.Texture)
extern "C"  void SteamVR_Skybox_SetOverride_m1812733784 (Il2CppObject * __this /* static, unused */, Texture_t2243626319 * ___front0, Texture_t2243626319 * ___back1, Texture_t2243626319 * ___left2, Texture_t2243626319 * ___right3, Texture_t2243626319 * ___top4, Texture_t2243626319 * ___bottom5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Skybox::ClearOverride()
extern "C"  void SteamVR_Skybox_ClearOverride_m2476981923 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Skybox::OnEnable()
extern "C"  void SteamVR_Skybox_OnEnable_m611938394 (SteamVR_Skybox_t52115419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Skybox::OnDisable()
extern "C"  void SteamVR_Skybox_OnDisable_m1867446287 (SteamVR_Skybox_t52115419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
