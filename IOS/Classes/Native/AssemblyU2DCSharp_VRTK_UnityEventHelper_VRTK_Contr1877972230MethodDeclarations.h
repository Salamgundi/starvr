﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents
struct VRTK_ControllerEvents_UnityEvents_t1877972230;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"

// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::.ctor()
extern "C"  void VRTK_ControllerEvents_UnityEvents__ctor_m1643560837 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::SetControllerEvents()
extern "C"  void VRTK_ControllerEvents_UnityEvents_SetControllerEvents_m1085106966 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnEnable()
extern "C"  void VRTK_ControllerEvents_UnityEvents_OnEnable_m1941221557 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::TriggerPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_TriggerPressed_m3889456573 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::TriggerReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_TriggerReleased_m1705088040 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::TriggerTouchStart(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_TriggerTouchStart_m162418722 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::TriggerTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_TriggerTouchEnd_m840532589 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::TriggerHairlineStart(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_TriggerHairlineStart_m679578779 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::TriggerHairlineEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_TriggerHairlineEnd_m1816115668 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::TriggerClicked(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_TriggerClicked_m3424723162 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::TriggerUnclicked(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_TriggerUnclicked_m3436751247 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::TriggerAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_TriggerAxisChanged_m522624604 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::GripPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_GripPressed_m865088129 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::GripReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_GripReleased_m77963636 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::GripTouchStart(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_GripTouchStart_m2008719174 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::GripTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_GripTouchEnd_m1211885465 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::GripHairlineStart(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_GripHairlineStart_m163371347 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::GripHairlineEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_GripHairlineEnd_m4204061492 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::GripClicked(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_GripClicked_m3030001602 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::GripUnclicked(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_GripUnclicked_m670981959 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::GripAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_GripAxisChanged_m3865468976 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::TouchpadPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_TouchpadPressed_m3345157417 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::TouchpadReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_TouchpadReleased_m3983017888 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::TouchpadTouchStart(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_TouchpadTouchStart_m2051461994 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::TouchpadTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_TouchpadTouchEnd_m1970884929 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::TouchpadAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_TouchpadAxisChanged_m1186959932 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::ButtonOnePressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_ButtonOnePressed_m65410955 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::ButtonOneReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_ButtonOneReleased_m2553256644 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::ButtonOneTouchStart(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_ButtonOneTouchStart_m221983642 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::ButtonOneTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_ButtonOneTouchEnd_m3682607751 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::ButtonTwoPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_ButtonTwoPressed_m2003531767 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::ButtonTwoReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_ButtonTwoReleased_m3127606610 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::ButtonTwoTouchStart(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_ButtonTwoTouchStart_m2903965852 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::ButtonTwoTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_ButtonTwoTouchEnd_m3567578499 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::StartMenuPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_StartMenuPressed_m1015625048 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::StartMenuReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_StartMenuReleased_m4165910801 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::AliasPointerOn(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_AliasPointerOn_m1481167047 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::AliasPointerOff(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_AliasPointerOff_m518921389 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::AliasPointerSet(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_AliasPointerSet_m893923212 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::AliasGrabOn(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_AliasGrabOn_m1892392988 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::AliasGrabOff(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_AliasGrabOff_m1724751912 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::AliasUseOn(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_AliasUseOn_m993883595 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::AliasUseOff(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_AliasUseOff_m2009329077 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::AliasUIClickOn(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_AliasUIClickOn_m3262550792 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::AliasUIClickOff(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_AliasUIClickOff_m3118378128 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::AliasMenuOn(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_AliasMenuOn_m3475515359 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::AliasMenuOff(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_AliasMenuOff_m3300701669 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::ControllerEnabled(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_ControllerEnabled_m798045142 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::ControllerDisabled(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_ControllerDisabled_m3648508935 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::ControllerIndexChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_UnityEvents_ControllerIndexChanged_m3031385475 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, Il2CppObject * ___o0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerEvents_UnityEvents::OnDisable()
extern "C"  void VRTK_ControllerEvents_UnityEvents_OnDisable_m1914082100 (VRTK_ControllerEvents_UnityEvents_t1877972230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
