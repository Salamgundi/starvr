﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;

#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.LightSaber
struct  LightSaber_t2639943817  : public VRTK_InteractableObject_t2604188111
{
public:
	// System.Boolean VRTK.Examples.LightSaber::beamActive
	bool ___beamActive_45;
	// UnityEngine.Vector2 VRTK.Examples.LightSaber::beamLimits
	Vector2_t2243707579  ___beamLimits_46;
	// System.Single VRTK.Examples.LightSaber::currentBeamSize
	float ___currentBeamSize_47;
	// System.Single VRTK.Examples.LightSaber::beamExtendSpeed
	float ___beamExtendSpeed_48;
	// UnityEngine.GameObject VRTK.Examples.LightSaber::blade
	GameObject_t1756533147 * ___blade_49;
	// UnityEngine.Color VRTK.Examples.LightSaber::activeColor
	Color_t2020392075  ___activeColor_50;
	// UnityEngine.Color VRTK.Examples.LightSaber::targetColor
	Color_t2020392075  ___targetColor_51;
	// UnityEngine.Color[] VRTK.Examples.LightSaber::bladePhaseColors
	ColorU5BU5D_t672350442* ___bladePhaseColors_52;

public:
	inline static int32_t get_offset_of_beamActive_45() { return static_cast<int32_t>(offsetof(LightSaber_t2639943817, ___beamActive_45)); }
	inline bool get_beamActive_45() const { return ___beamActive_45; }
	inline bool* get_address_of_beamActive_45() { return &___beamActive_45; }
	inline void set_beamActive_45(bool value)
	{
		___beamActive_45 = value;
	}

	inline static int32_t get_offset_of_beamLimits_46() { return static_cast<int32_t>(offsetof(LightSaber_t2639943817, ___beamLimits_46)); }
	inline Vector2_t2243707579  get_beamLimits_46() const { return ___beamLimits_46; }
	inline Vector2_t2243707579 * get_address_of_beamLimits_46() { return &___beamLimits_46; }
	inline void set_beamLimits_46(Vector2_t2243707579  value)
	{
		___beamLimits_46 = value;
	}

	inline static int32_t get_offset_of_currentBeamSize_47() { return static_cast<int32_t>(offsetof(LightSaber_t2639943817, ___currentBeamSize_47)); }
	inline float get_currentBeamSize_47() const { return ___currentBeamSize_47; }
	inline float* get_address_of_currentBeamSize_47() { return &___currentBeamSize_47; }
	inline void set_currentBeamSize_47(float value)
	{
		___currentBeamSize_47 = value;
	}

	inline static int32_t get_offset_of_beamExtendSpeed_48() { return static_cast<int32_t>(offsetof(LightSaber_t2639943817, ___beamExtendSpeed_48)); }
	inline float get_beamExtendSpeed_48() const { return ___beamExtendSpeed_48; }
	inline float* get_address_of_beamExtendSpeed_48() { return &___beamExtendSpeed_48; }
	inline void set_beamExtendSpeed_48(float value)
	{
		___beamExtendSpeed_48 = value;
	}

	inline static int32_t get_offset_of_blade_49() { return static_cast<int32_t>(offsetof(LightSaber_t2639943817, ___blade_49)); }
	inline GameObject_t1756533147 * get_blade_49() const { return ___blade_49; }
	inline GameObject_t1756533147 ** get_address_of_blade_49() { return &___blade_49; }
	inline void set_blade_49(GameObject_t1756533147 * value)
	{
		___blade_49 = value;
		Il2CppCodeGenWriteBarrier(&___blade_49, value);
	}

	inline static int32_t get_offset_of_activeColor_50() { return static_cast<int32_t>(offsetof(LightSaber_t2639943817, ___activeColor_50)); }
	inline Color_t2020392075  get_activeColor_50() const { return ___activeColor_50; }
	inline Color_t2020392075 * get_address_of_activeColor_50() { return &___activeColor_50; }
	inline void set_activeColor_50(Color_t2020392075  value)
	{
		___activeColor_50 = value;
	}

	inline static int32_t get_offset_of_targetColor_51() { return static_cast<int32_t>(offsetof(LightSaber_t2639943817, ___targetColor_51)); }
	inline Color_t2020392075  get_targetColor_51() const { return ___targetColor_51; }
	inline Color_t2020392075 * get_address_of_targetColor_51() { return &___targetColor_51; }
	inline void set_targetColor_51(Color_t2020392075  value)
	{
		___targetColor_51 = value;
	}

	inline static int32_t get_offset_of_bladePhaseColors_52() { return static_cast<int32_t>(offsetof(LightSaber_t2639943817, ___bladePhaseColors_52)); }
	inline ColorU5BU5D_t672350442* get_bladePhaseColors_52() const { return ___bladePhaseColors_52; }
	inline ColorU5BU5D_t672350442** get_address_of_bladePhaseColors_52() { return &___bladePhaseColors_52; }
	inline void set_bladePhaseColors_52(ColorU5BU5D_t672350442* value)
	{
		___bladePhaseColors_52 = value;
		Il2CppCodeGenWriteBarrier(&___bladePhaseColors_52, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
