﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperone/_GetBoundsColor
struct _GetBoundsColor_t3705523218;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdColor_t1780554589.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperone/_GetBoundsColor::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetBoundsColor__ctor_m2847399777 (_GetBoundsColor_t3705523218 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperone/_GetBoundsColor::Invoke(Valve.VR.HmdColor_t&,System.Int32,System.Single,Valve.VR.HmdColor_t&)
extern "C"  void _GetBoundsColor_Invoke_m1482625045 (_GetBoundsColor_t3705523218 * __this, HmdColor_t_t1780554589 * ___pOutputColorArray0, int32_t ___nNumOutputColors1, float ___flCollisionBoundsFadeDistance2, HmdColor_t_t1780554589 * ___pOutputCameraColor3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperone/_GetBoundsColor::BeginInvoke(Valve.VR.HmdColor_t&,System.Int32,System.Single,Valve.VR.HmdColor_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetBoundsColor_BeginInvoke_m2430177424 (_GetBoundsColor_t3705523218 * __this, HmdColor_t_t1780554589 * ___pOutputColorArray0, int32_t ___nNumOutputColors1, float ___flCollisionBoundsFadeDistance2, HmdColor_t_t1780554589 * ___pOutputCameraColor3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperone/_GetBoundsColor::EndInvoke(Valve.VR.HmdColor_t&,Valve.VR.HmdColor_t&,System.IAsyncResult)
extern "C"  void _GetBoundsColor_EndInvoke_m2195501647 (_GetBoundsColor_t3705523218 * __this, HmdColor_t_t1780554589 * ___pOutputColorArray0, HmdColor_t_t1780554589 * ___pOutputCameraColor1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
