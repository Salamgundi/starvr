﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_CompositorBringToFront
struct _CompositorBringToFront_t2294069769;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_CompositorBringToFront::.ctor(System.Object,System.IntPtr)
extern "C"  void _CompositorBringToFront__ctor_m3984427214 (_CompositorBringToFront_t2294069769 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_CompositorBringToFront::Invoke()
extern "C"  void _CompositorBringToFront_Invoke_m3888462412 (_CompositorBringToFront_t2294069769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_CompositorBringToFront::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _CompositorBringToFront_BeginInvoke_m1213184079 (_CompositorBringToFront_t2294069769 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_CompositorBringToFront::EndInvoke(System.IAsyncResult)
extern "C"  void _CompositorBringToFront_EndInvoke_m3766958000 (_CompositorBringToFront_t2294069769 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
