﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1214023521;
// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.VRTK_DashTeleport
struct VRTK_DashTeleport_t1206199485;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0
struct  U3ClerpToPositionU3Ec__Iterator0_t2809459013  : public Il2CppObject
{
public:
	// System.Boolean VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::<gameObjectInTheWay>__0
	bool ___U3CgameObjectInTheWayU3E__0_0;
	// UnityEngine.Vector3 VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::<eyeCameraPosition>__1
	Vector3_t2243707580  ___U3CeyeCameraPositionU3E__1_1;
	// UnityEngine.Vector3 VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::<eyeCameraPositionOnGround>__2
	Vector3_t2243707580  ___U3CeyeCameraPositionOnGroundU3E__2_2;
	// UnityEngine.Vector3 VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::<eyeCameraRelativeToRig>__3
	Vector3_t2243707580  ___U3CeyeCameraRelativeToRigU3E__3_3;
	// UnityEngine.Vector3 VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::targetPosition
	Vector3_t2243707580  ___targetPosition_4;
	// UnityEngine.Vector3 VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::<targetEyeCameraPosition>__4
	Vector3_t2243707580  ___U3CtargetEyeCameraPositionU3E__4_5;
	// UnityEngine.Vector3 VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::<direction>__5
	Vector3_t2243707580  ___U3CdirectionU3E__5_6;
	// UnityEngine.Vector3 VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::<bottomPoint>__6
	Vector3_t2243707580  ___U3CbottomPointU3E__6_7;
	// UnityEngine.Vector3 VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::<topPoint>__7
	Vector3_t2243707580  ___U3CtopPointU3E__7_8;
	// System.Single VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::<maxDistance>__8
	float ___U3CmaxDistanceU3E__8_9;
	// UnityEngine.RaycastHit[] VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::<allHits>__9
	RaycastHitU5BU5D_t1214023521* ___U3CallHitsU3E__9_10;
	// UnityEngine.RaycastHit[] VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::$locvar0
	RaycastHitU5BU5D_t1214023521* ___U24locvar0_11;
	// System.Int32 VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::$locvar1
	int32_t ___U24locvar1_12;
	// UnityEngine.Transform VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::target
	Transform_t3275118058 * ___target_13;
	// UnityEngine.Vector3 VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::<startPosition>__A
	Vector3_t2243707580  ___U3CstartPositionU3E__A_14;
	// System.Single VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::<elapsedTime>__B
	float ___U3CelapsedTimeU3E__B_15;
	// System.Single VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::<t>__C
	float ___U3CtU3E__C_16;
	// VRTK.VRTK_DashTeleport VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::$this
	VRTK_DashTeleport_t1206199485 * ___U24this_17;
	// System.Object VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::$current
	Il2CppObject * ___U24current_18;
	// System.Boolean VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::$disposing
	bool ___U24disposing_19;
	// System.Int32 VRTK.VRTK_DashTeleport/<lerpToPosition>c__Iterator0::$PC
	int32_t ___U24PC_20;

public:
	inline static int32_t get_offset_of_U3CgameObjectInTheWayU3E__0_0() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U3CgameObjectInTheWayU3E__0_0)); }
	inline bool get_U3CgameObjectInTheWayU3E__0_0() const { return ___U3CgameObjectInTheWayU3E__0_0; }
	inline bool* get_address_of_U3CgameObjectInTheWayU3E__0_0() { return &___U3CgameObjectInTheWayU3E__0_0; }
	inline void set_U3CgameObjectInTheWayU3E__0_0(bool value)
	{
		___U3CgameObjectInTheWayU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CeyeCameraPositionU3E__1_1() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U3CeyeCameraPositionU3E__1_1)); }
	inline Vector3_t2243707580  get_U3CeyeCameraPositionU3E__1_1() const { return ___U3CeyeCameraPositionU3E__1_1; }
	inline Vector3_t2243707580 * get_address_of_U3CeyeCameraPositionU3E__1_1() { return &___U3CeyeCameraPositionU3E__1_1; }
	inline void set_U3CeyeCameraPositionU3E__1_1(Vector3_t2243707580  value)
	{
		___U3CeyeCameraPositionU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CeyeCameraPositionOnGroundU3E__2_2() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U3CeyeCameraPositionOnGroundU3E__2_2)); }
	inline Vector3_t2243707580  get_U3CeyeCameraPositionOnGroundU3E__2_2() const { return ___U3CeyeCameraPositionOnGroundU3E__2_2; }
	inline Vector3_t2243707580 * get_address_of_U3CeyeCameraPositionOnGroundU3E__2_2() { return &___U3CeyeCameraPositionOnGroundU3E__2_2; }
	inline void set_U3CeyeCameraPositionOnGroundU3E__2_2(Vector3_t2243707580  value)
	{
		___U3CeyeCameraPositionOnGroundU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CeyeCameraRelativeToRigU3E__3_3() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U3CeyeCameraRelativeToRigU3E__3_3)); }
	inline Vector3_t2243707580  get_U3CeyeCameraRelativeToRigU3E__3_3() const { return ___U3CeyeCameraRelativeToRigU3E__3_3; }
	inline Vector3_t2243707580 * get_address_of_U3CeyeCameraRelativeToRigU3E__3_3() { return &___U3CeyeCameraRelativeToRigU3E__3_3; }
	inline void set_U3CeyeCameraRelativeToRigU3E__3_3(Vector3_t2243707580  value)
	{
		___U3CeyeCameraRelativeToRigU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_targetPosition_4() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___targetPosition_4)); }
	inline Vector3_t2243707580  get_targetPosition_4() const { return ___targetPosition_4; }
	inline Vector3_t2243707580 * get_address_of_targetPosition_4() { return &___targetPosition_4; }
	inline void set_targetPosition_4(Vector3_t2243707580  value)
	{
		___targetPosition_4 = value;
	}

	inline static int32_t get_offset_of_U3CtargetEyeCameraPositionU3E__4_5() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U3CtargetEyeCameraPositionU3E__4_5)); }
	inline Vector3_t2243707580  get_U3CtargetEyeCameraPositionU3E__4_5() const { return ___U3CtargetEyeCameraPositionU3E__4_5; }
	inline Vector3_t2243707580 * get_address_of_U3CtargetEyeCameraPositionU3E__4_5() { return &___U3CtargetEyeCameraPositionU3E__4_5; }
	inline void set_U3CtargetEyeCameraPositionU3E__4_5(Vector3_t2243707580  value)
	{
		___U3CtargetEyeCameraPositionU3E__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CdirectionU3E__5_6() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U3CdirectionU3E__5_6)); }
	inline Vector3_t2243707580  get_U3CdirectionU3E__5_6() const { return ___U3CdirectionU3E__5_6; }
	inline Vector3_t2243707580 * get_address_of_U3CdirectionU3E__5_6() { return &___U3CdirectionU3E__5_6; }
	inline void set_U3CdirectionU3E__5_6(Vector3_t2243707580  value)
	{
		___U3CdirectionU3E__5_6 = value;
	}

	inline static int32_t get_offset_of_U3CbottomPointU3E__6_7() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U3CbottomPointU3E__6_7)); }
	inline Vector3_t2243707580  get_U3CbottomPointU3E__6_7() const { return ___U3CbottomPointU3E__6_7; }
	inline Vector3_t2243707580 * get_address_of_U3CbottomPointU3E__6_7() { return &___U3CbottomPointU3E__6_7; }
	inline void set_U3CbottomPointU3E__6_7(Vector3_t2243707580  value)
	{
		___U3CbottomPointU3E__6_7 = value;
	}

	inline static int32_t get_offset_of_U3CtopPointU3E__7_8() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U3CtopPointU3E__7_8)); }
	inline Vector3_t2243707580  get_U3CtopPointU3E__7_8() const { return ___U3CtopPointU3E__7_8; }
	inline Vector3_t2243707580 * get_address_of_U3CtopPointU3E__7_8() { return &___U3CtopPointU3E__7_8; }
	inline void set_U3CtopPointU3E__7_8(Vector3_t2243707580  value)
	{
		___U3CtopPointU3E__7_8 = value;
	}

	inline static int32_t get_offset_of_U3CmaxDistanceU3E__8_9() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U3CmaxDistanceU3E__8_9)); }
	inline float get_U3CmaxDistanceU3E__8_9() const { return ___U3CmaxDistanceU3E__8_9; }
	inline float* get_address_of_U3CmaxDistanceU3E__8_9() { return &___U3CmaxDistanceU3E__8_9; }
	inline void set_U3CmaxDistanceU3E__8_9(float value)
	{
		___U3CmaxDistanceU3E__8_9 = value;
	}

	inline static int32_t get_offset_of_U3CallHitsU3E__9_10() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U3CallHitsU3E__9_10)); }
	inline RaycastHitU5BU5D_t1214023521* get_U3CallHitsU3E__9_10() const { return ___U3CallHitsU3E__9_10; }
	inline RaycastHitU5BU5D_t1214023521** get_address_of_U3CallHitsU3E__9_10() { return &___U3CallHitsU3E__9_10; }
	inline void set_U3CallHitsU3E__9_10(RaycastHitU5BU5D_t1214023521* value)
	{
		___U3CallHitsU3E__9_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CallHitsU3E__9_10, value);
	}

	inline static int32_t get_offset_of_U24locvar0_11() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U24locvar0_11)); }
	inline RaycastHitU5BU5D_t1214023521* get_U24locvar0_11() const { return ___U24locvar0_11; }
	inline RaycastHitU5BU5D_t1214023521** get_address_of_U24locvar0_11() { return &___U24locvar0_11; }
	inline void set_U24locvar0_11(RaycastHitU5BU5D_t1214023521* value)
	{
		___U24locvar0_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_11, value);
	}

	inline static int32_t get_offset_of_U24locvar1_12() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U24locvar1_12)); }
	inline int32_t get_U24locvar1_12() const { return ___U24locvar1_12; }
	inline int32_t* get_address_of_U24locvar1_12() { return &___U24locvar1_12; }
	inline void set_U24locvar1_12(int32_t value)
	{
		___U24locvar1_12 = value;
	}

	inline static int32_t get_offset_of_target_13() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___target_13)); }
	inline Transform_t3275118058 * get_target_13() const { return ___target_13; }
	inline Transform_t3275118058 ** get_address_of_target_13() { return &___target_13; }
	inline void set_target_13(Transform_t3275118058 * value)
	{
		___target_13 = value;
		Il2CppCodeGenWriteBarrier(&___target_13, value);
	}

	inline static int32_t get_offset_of_U3CstartPositionU3E__A_14() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U3CstartPositionU3E__A_14)); }
	inline Vector3_t2243707580  get_U3CstartPositionU3E__A_14() const { return ___U3CstartPositionU3E__A_14; }
	inline Vector3_t2243707580 * get_address_of_U3CstartPositionU3E__A_14() { return &___U3CstartPositionU3E__A_14; }
	inline void set_U3CstartPositionU3E__A_14(Vector3_t2243707580  value)
	{
		___U3CstartPositionU3E__A_14 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E__B_15() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U3CelapsedTimeU3E__B_15)); }
	inline float get_U3CelapsedTimeU3E__B_15() const { return ___U3CelapsedTimeU3E__B_15; }
	inline float* get_address_of_U3CelapsedTimeU3E__B_15() { return &___U3CelapsedTimeU3E__B_15; }
	inline void set_U3CelapsedTimeU3E__B_15(float value)
	{
		___U3CelapsedTimeU3E__B_15 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__C_16() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U3CtU3E__C_16)); }
	inline float get_U3CtU3E__C_16() const { return ___U3CtU3E__C_16; }
	inline float* get_address_of_U3CtU3E__C_16() { return &___U3CtU3E__C_16; }
	inline void set_U3CtU3E__C_16(float value)
	{
		___U3CtU3E__C_16 = value;
	}

	inline static int32_t get_offset_of_U24this_17() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U24this_17)); }
	inline VRTK_DashTeleport_t1206199485 * get_U24this_17() const { return ___U24this_17; }
	inline VRTK_DashTeleport_t1206199485 ** get_address_of_U24this_17() { return &___U24this_17; }
	inline void set_U24this_17(VRTK_DashTeleport_t1206199485 * value)
	{
		___U24this_17 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_17, value);
	}

	inline static int32_t get_offset_of_U24current_18() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U24current_18)); }
	inline Il2CppObject * get_U24current_18() const { return ___U24current_18; }
	inline Il2CppObject ** get_address_of_U24current_18() { return &___U24current_18; }
	inline void set_U24current_18(Il2CppObject * value)
	{
		___U24current_18 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_18, value);
	}

	inline static int32_t get_offset_of_U24disposing_19() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U24disposing_19)); }
	inline bool get_U24disposing_19() const { return ___U24disposing_19; }
	inline bool* get_address_of_U24disposing_19() { return &___U24disposing_19; }
	inline void set_U24disposing_19(bool value)
	{
		___U24disposing_19 = value;
	}

	inline static int32_t get_offset_of_U24PC_20() { return static_cast<int32_t>(offsetof(U3ClerpToPositionU3Ec__Iterator0_t2809459013, ___U24PC_20)); }
	inline int32_t get_U24PC_20() const { return ___U24PC_20; }
	inline int32_t* get_address_of_U24PC_20() { return &___U24PC_20; }
	inline void set_U24PC_20(int32_t value)
	{
		___U24PC_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
