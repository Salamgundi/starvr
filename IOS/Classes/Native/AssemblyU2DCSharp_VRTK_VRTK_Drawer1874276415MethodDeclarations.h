﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_Drawer
struct VRTK_Drawer_t1874276415;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_ControlValueRa2976216666.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_Direction3775008092.h"

// System.Void VRTK.VRTK_Drawer::.ctor()
extern "C"  void VRTK_Drawer__ctor_m3913697397 (VRTK_Drawer_t1874276415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Drawer::OnDrawGizmos()
extern "C"  void VRTK_Drawer_OnDrawGizmos_m1701921241 (VRTK_Drawer_t1874276415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Drawer::InitRequiredComponents()
extern "C"  void VRTK_Drawer_InitRequiredComponents_m542453688 (VRTK_Drawer_t1874276415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Drawer::DetectSetup()
extern "C"  bool VRTK_Drawer_DetectSetup_m2054775461 (VRTK_Drawer_t1874276415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_Control/ControlValueRange VRTK.VRTK_Drawer::RegisterValueRange()
extern "C"  ControlValueRange_t2976216666  VRTK_Drawer_RegisterValueRange_m1052335249 (VRTK_Drawer_t1874276415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Drawer::HandleUpdate()
extern "C"  void VRTK_Drawer_HandleUpdate_m525790876 (VRTK_Drawer_t1874276415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Drawer::InitBody()
extern "C"  void VRTK_Drawer_InitBody_m3703512267 (VRTK_Drawer_t1874276415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Drawer::InitHandle()
extern "C"  void VRTK_Drawer_InitHandle_m1870238153 (VRTK_Drawer_t1874276415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_Control/Direction VRTK.VRTK_Drawer::DetectDirection()
extern "C"  int32_t VRTK_Drawer_DetectDirection_m2507260992 (VRTK_Drawer_t1874276415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_Drawer::CalculateValue()
extern "C"  float VRTK_Drawer_CalculateValue_m3959865492 (VRTK_Drawer_t1874276415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_Drawer::GetBody()
extern "C"  GameObject_t1756533147 * VRTK_Drawer_GetBody_m1721063064 (VRTK_Drawer_t1874276415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_Drawer::GetHandle()
extern "C"  GameObject_t1756533147 * VRTK_Drawer_GetHandle_m1936433858 (VRTK_Drawer_t1874276415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
