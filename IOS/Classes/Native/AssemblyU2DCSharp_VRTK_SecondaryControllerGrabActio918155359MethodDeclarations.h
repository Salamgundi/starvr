﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SecondaryControllerGrabActions.VRTK_SwapControllerGrabAction
struct VRTK_SwapControllerGrabAction_t918155359;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SecondaryControllerGrabActions.VRTK_SwapControllerGrabAction::.ctor()
extern "C"  void VRTK_SwapControllerGrabAction__ctor_m2830750304 (VRTK_SwapControllerGrabAction_t918155359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SecondaryControllerGrabActions.VRTK_SwapControllerGrabAction::Awake()
extern "C"  void VRTK_SwapControllerGrabAction_Awake_m518964425 (VRTK_SwapControllerGrabAction_t918155359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
