﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Flarez
struct  Flarez_t1047003190  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Flarez::rightEye
	GameObject_t1756533147 * ___rightEye_2;
	// UnityEngine.GameObject Flarez::leftEye
	GameObject_t1756533147 * ___leftEye_3;

public:
	inline static int32_t get_offset_of_rightEye_2() { return static_cast<int32_t>(offsetof(Flarez_t1047003190, ___rightEye_2)); }
	inline GameObject_t1756533147 * get_rightEye_2() const { return ___rightEye_2; }
	inline GameObject_t1756533147 ** get_address_of_rightEye_2() { return &___rightEye_2; }
	inline void set_rightEye_2(GameObject_t1756533147 * value)
	{
		___rightEye_2 = value;
		Il2CppCodeGenWriteBarrier(&___rightEye_2, value);
	}

	inline static int32_t get_offset_of_leftEye_3() { return static_cast<int32_t>(offsetof(Flarez_t1047003190, ___leftEye_3)); }
	inline GameObject_t1756533147 * get_leftEye_3() const { return ___leftEye_3; }
	inline GameObject_t1756533147 ** get_address_of_leftEye_3() { return &___leftEye_3; }
	inline void set_leftEye_3(GameObject_t1756533147 * value)
	{
		___leftEye_3 = value;
		Il2CppCodeGenWriteBarrier(&___leftEye_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
