﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetControllerState
struct _GetControllerState_t3891090487;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_VRControllerState_t2504874220.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetControllerState::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetControllerState__ctor_m1598880442 (_GetControllerState_t3891090487 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_GetControllerState::Invoke(System.UInt32,Valve.VR.VRControllerState_t&,System.UInt32)
extern "C"  bool _GetControllerState_Invoke_m3024192214 (_GetControllerState_t3891090487 * __this, uint32_t ___unControllerDeviceIndex0, VRControllerState_t_t2504874220 * ___pControllerState1, uint32_t ___unControllerStateSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetControllerState::BeginInvoke(System.UInt32,Valve.VR.VRControllerState_t&,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetControllerState_BeginInvoke_m1607478547 (_GetControllerState_t3891090487 * __this, uint32_t ___unControllerDeviceIndex0, VRControllerState_t_t2504874220 * ___pControllerState1, uint32_t ___unControllerStateSize2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_GetControllerState::EndInvoke(Valve.VR.VRControllerState_t&,System.IAsyncResult)
extern "C"  bool _GetControllerState_EndInvoke_m2488851528 (_GetControllerState_t3891090487 * __this, VRControllerState_t_t2504874220 * ___pControllerState0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
