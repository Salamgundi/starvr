﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetTransformForOverlayCoordinates
struct _GetTransformForOverlayCoordinates_t2483359939;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingUniverseOrigin1464400093.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdVector2_t2255225135.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetTransformForOverlayCoordinates::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetTransformForOverlayCoordinates__ctor_m797442426 (_GetTransformForOverlayCoordinates_t2483359939 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetTransformForOverlayCoordinates::Invoke(System.UInt64,Valve.VR.ETrackingUniverseOrigin,Valve.VR.HmdVector2_t,Valve.VR.HmdMatrix34_t&)
extern "C"  int32_t _GetTransformForOverlayCoordinates_Invoke_m2333370423 (_GetTransformForOverlayCoordinates_t2483359939 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTrackingOrigin1, HmdVector2_t_t2255225135  ___coordinatesInOverlay2, HmdMatrix34_t_t664273062 * ___pmatTransform3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetTransformForOverlayCoordinates::BeginInvoke(System.UInt64,Valve.VR.ETrackingUniverseOrigin,Valve.VR.HmdVector2_t,Valve.VR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetTransformForOverlayCoordinates_BeginInvoke_m2513318306 (_GetTransformForOverlayCoordinates_t2483359939 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTrackingOrigin1, HmdVector2_t_t2255225135  ___coordinatesInOverlay2, HmdMatrix34_t_t664273062 * ___pmatTransform3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetTransformForOverlayCoordinates::EndInvoke(Valve.VR.HmdMatrix34_t&,System.IAsyncResult)
extern "C"  int32_t _GetTransformForOverlayCoordinates_EndInvoke_m2484066194 (_GetTransformForOverlayCoordinates_t2483359939 * __this, HmdMatrix34_t_t664273062 * ___pmatTransform0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
