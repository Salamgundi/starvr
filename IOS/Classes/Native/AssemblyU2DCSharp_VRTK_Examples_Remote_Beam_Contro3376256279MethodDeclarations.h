﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Remote_Beam_Controller
struct Remote_Beam_Controller_t3376256279;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"

// System.Void VRTK.Examples.Remote_Beam_Controller::.ctor()
extern "C"  void Remote_Beam_Controller__ctor_m1033144384 (Remote_Beam_Controller_t3376256279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Remote_Beam_Controller::Start()
extern "C"  void Remote_Beam_Controller_Start_m3168532480 (Remote_Beam_Controller_t3376256279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Remote_Beam_Controller::DoTouchpadAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void Remote_Beam_Controller_DoTouchpadAxisChanged_m3207144688 (Remote_Beam_Controller_t3376256279 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Remote_Beam_Controller::DoTouchpadTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void Remote_Beam_Controller_DoTouchpadTouchEnd_m3068419307 (Remote_Beam_Controller_t3376256279 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
