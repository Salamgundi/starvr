﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_AxisM2300405083.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_AxisM1724819570.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.TouchpadMovementAxisEventArgs
struct  TouchpadMovementAxisEventArgs_t1237298133 
{
public:
	// VRTK.VRTK_TouchpadMovement/AxisMovementType VRTK.TouchpadMovementAxisEventArgs::movementType
	int32_t ___movementType_0;
	// VRTK.VRTK_TouchpadMovement/AxisMovementDirection VRTK.TouchpadMovementAxisEventArgs::direction
	int32_t ___direction_1;

public:
	inline static int32_t get_offset_of_movementType_0() { return static_cast<int32_t>(offsetof(TouchpadMovementAxisEventArgs_t1237298133, ___movementType_0)); }
	inline int32_t get_movementType_0() const { return ___movementType_0; }
	inline int32_t* get_address_of_movementType_0() { return &___movementType_0; }
	inline void set_movementType_0(int32_t value)
	{
		___movementType_0 = value;
	}

	inline static int32_t get_offset_of_direction_1() { return static_cast<int32_t>(offsetof(TouchpadMovementAxisEventArgs_t1237298133, ___direction_1)); }
	inline int32_t get_direction_1() const { return ___direction_1; }
	inline int32_t* get_address_of_direction_1() { return &___direction_1; }
	inline void set_direction_1(int32_t value)
	{
		___direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
