﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_IsDisplayOnDesktop
struct _IsDisplayOnDesktop_t2551312917;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_IsDisplayOnDesktop::.ctor(System.Object,System.IntPtr)
extern "C"  void _IsDisplayOnDesktop__ctor_m3532348030 (_IsDisplayOnDesktop_t2551312917 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_IsDisplayOnDesktop::Invoke()
extern "C"  bool _IsDisplayOnDesktop_Invoke_m2991096310 (_IsDisplayOnDesktop_t2551312917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_IsDisplayOnDesktop::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _IsDisplayOnDesktop_BeginInvoke_m4231313827 (_IsDisplayOnDesktop_t2551312917 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_IsDisplayOnDesktop::EndInvoke(System.IAsyncResult)
extern "C"  bool _IsDisplayOnDesktop_EndInvoke_m579493134 (_IsDisplayOnDesktop_t2551312917 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
