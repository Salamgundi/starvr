﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.BalloonColliders
struct BalloonColliders_t2580220890;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.BalloonColliders::.ctor()
extern "C"  void BalloonColliders__ctor_m3705728794 (BalloonColliders_t2580220890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.BalloonColliders::Awake()
extern "C"  void BalloonColliders_Awake_m3343804715 (BalloonColliders_t2580220890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.BalloonColliders::OnEnable()
extern "C"  void BalloonColliders_OnEnable_m1068316918 (BalloonColliders_t2580220890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.BalloonColliders::OnDisable()
extern "C"  void BalloonColliders_OnDisable_m1980796255 (BalloonColliders_t2580220890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.BalloonColliders::OnDestroy()
extern "C"  void BalloonColliders_OnDestroy_m3130552041 (BalloonColliders_t2580220890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
