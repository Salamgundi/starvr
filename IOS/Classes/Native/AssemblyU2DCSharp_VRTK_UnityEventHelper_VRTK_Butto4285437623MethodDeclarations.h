﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_Button_UnityEvents
struct VRTK_Button_UnityEvents_t4285437623;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_Control3DEventArgs4095025701.h"

// System.Void VRTK.UnityEventHelper.VRTK_Button_UnityEvents::.ctor()
extern "C"  void VRTK_Button_UnityEvents__ctor_m44212848 (VRTK_Button_UnityEvents_t4285437623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_Button_UnityEvents::SetButton3D()
extern "C"  void VRTK_Button_UnityEvents_SetButton3D_m1873693731 (VRTK_Button_UnityEvents_t4285437623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_Button_UnityEvents::OnEnable()
extern "C"  void VRTK_Button_UnityEvents_OnEnable_m3026487368 (VRTK_Button_UnityEvents_t4285437623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_Button_UnityEvents::Pushed(System.Object,VRTK.Control3DEventArgs)
extern "C"  void VRTK_Button_UnityEvents_Pushed_m1285697587 (VRTK_Button_UnityEvents_t4285437623 * __this, Il2CppObject * ___o0, Control3DEventArgs_t4095025701  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_Button_UnityEvents::OnDisable()
extern "C"  void VRTK_Button_UnityEvents_OnDisable_m328498217 (VRTK_Button_UnityEvents_t4285437623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
