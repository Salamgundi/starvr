﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct Comparison_1_t1520341115;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_258602264.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m716614833_gshared (Comparison_1_t1520341115 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m716614833(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t1520341115 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m716614833_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m1419390023_gshared (Comparison_1_t1520341115 * __this, KeyValuePair_2_t258602264  ___x0, KeyValuePair_2_t258602264  ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m1419390023(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t1520341115 *, KeyValuePair_2_t258602264 , KeyValuePair_2_t258602264 , const MethodInfo*))Comparison_1_Invoke_m1419390023_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m390525400_gshared (Comparison_1_t1520341115 * __this, KeyValuePair_2_t258602264  ___x0, KeyValuePair_2_t258602264  ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m390525400(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t1520341115 *, KeyValuePair_2_t258602264 , KeyValuePair_2_t258602264 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m390525400_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m760782173_gshared (Comparison_1_t1520341115 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m760782173(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t1520341115 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m760782173_gshared)(__this, ___result0, method)
