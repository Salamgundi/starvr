﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.Control3DEventArgs>
struct InvokableCall_2_t910305276;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.Control3DEventArgs>
struct UnityAction_2_t895514392;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.Control3DEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m4055870136_gshared (InvokableCall_2_t910305276 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_2__ctor_m4055870136(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_2_t910305276 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2__ctor_m4055870136_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.Control3DEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m357326609_gshared (InvokableCall_2_t910305276 * __this, UnityAction_2_t895514392 * ___action0, const MethodInfo* method);
#define InvokableCall_2__ctor_m357326609(__this, ___action0, method) ((  void (*) (InvokableCall_2_t910305276 *, UnityAction_2_t895514392 *, const MethodInfo*))InvokableCall_2__ctor_m357326609_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.Control3DEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m3732820374_gshared (InvokableCall_2_t910305276 * __this, UnityAction_2_t895514392 * ___value0, const MethodInfo* method);
#define InvokableCall_2_add_Delegate_m3732820374(__this, ___value0, method) ((  void (*) (InvokableCall_2_t910305276 *, UnityAction_2_t895514392 *, const MethodInfo*))InvokableCall_2_add_Delegate_m3732820374_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.Control3DEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m3912846059_gshared (InvokableCall_2_t910305276 * __this, UnityAction_2_t895514392 * ___value0, const MethodInfo* method);
#define InvokableCall_2_remove_Delegate_m3912846059(__this, ___value0, method) ((  void (*) (InvokableCall_2_t910305276 *, UnityAction_2_t895514392 *, const MethodInfo*))InvokableCall_2_remove_Delegate_m3912846059_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.Control3DEventArgs>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m2887987353_gshared (InvokableCall_2_t910305276 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_2_Invoke_m2887987353(__this, ___args0, method) ((  void (*) (InvokableCall_2_t910305276 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_2_Invoke_m2887987353_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.Control3DEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m2991183833_gshared (InvokableCall_2_t910305276 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_2_Find_m2991183833(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_2_t910305276 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2_Find_m2991183833_gshared)(__this, ___targetObj0, ___method1, method)
