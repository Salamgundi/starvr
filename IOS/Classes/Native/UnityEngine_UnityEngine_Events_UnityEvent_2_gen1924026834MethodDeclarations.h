﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Int32,System.Boolean>
struct UnityEvent_2_t1924026834;
// UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>
struct UnityAction_2_t41828916;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Int32,System.Boolean>::.ctor()
extern "C"  void UnityEvent_2__ctor_m164723781_gshared (UnityEvent_2_t1924026834 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m164723781(__this, method) ((  void (*) (UnityEvent_2_t1924026834 *, const MethodInfo*))UnityEvent_2__ctor_m164723781_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Int32,System.Boolean>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m2986942348_gshared (UnityEvent_2_t1924026834 * __this, UnityAction_2_t41828916 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m2986942348(__this, ___call0, method) ((  void (*) (UnityEvent_2_t1924026834 *, UnityAction_2_t41828916 *, const MethodInfo*))UnityEvent_2_AddListener_m2986942348_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Int32,System.Boolean>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m578770775_gshared (UnityEvent_2_t1924026834 * __this, UnityAction_2_t41828916 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m578770775(__this, ___call0, method) ((  void (*) (UnityEvent_2_t1924026834 *, UnityAction_2_t41828916 *, const MethodInfo*))UnityEvent_2_RemoveListener_m578770775_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Int32,System.Boolean>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m1584835480_gshared (UnityEvent_2_t1924026834 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m1584835480(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t1924026834 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m1584835480_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Int32,System.Boolean>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m2833300204_gshared (UnityEvent_2_t1924026834 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m2833300204(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t1924026834 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m2833300204_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Int32,System.Boolean>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m3407745715_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t41828916 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m3407745715(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t41828916 *, const MethodInfo*))UnityEvent_2_GetDelegate_m3407745715_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Int32,System.Boolean>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m3805637944_gshared (UnityEvent_2_t1924026834 * __this, int32_t ___arg00, bool ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m3805637944(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t1924026834 *, int32_t, bool, const MethodInfo*))UnityEvent_2_Invoke_m3805637944_gshared)(__this, ___arg00, ___arg11, method)
