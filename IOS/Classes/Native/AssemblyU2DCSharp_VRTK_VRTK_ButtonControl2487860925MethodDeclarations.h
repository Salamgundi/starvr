﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ButtonControl
struct VRTK_ButtonControl_t2487860925;
// VRTK.VRTK_ObjectControl
struct VRTK_ObjectControl_t724022372;
// VRTK.ControllerInteractionEventHandler
struct ControllerInteractionEventHandler_t343979916;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventHa343979916.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"

// System.Void VRTK.VRTK_ButtonControl::.ctor()
extern "C"  void VRTK_ButtonControl__ctor_m918351939 (VRTK_ButtonControl_t2487860925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ButtonControl::Update()
extern "C"  void VRTK_ButtonControl_Update_m3843814704 (VRTK_ButtonControl_t2487860925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ButtonControl::ControlFixedUpdate()
extern "C"  void VRTK_ButtonControl_ControlFixedUpdate_m1640749147 (VRTK_ButtonControl_t2487860925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_ObjectControl VRTK.VRTK_ButtonControl::GetOtherControl()
extern "C"  VRTK_ObjectControl_t724022372 * VRTK_ButtonControl_GetOtherControl_m2098288178 (VRTK_ButtonControl_t2487860925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ButtonControl::SetListeners(System.Boolean)
extern "C"  void VRTK_ButtonControl_SetListeners_m3302560595 (VRTK_ButtonControl_t2487860925 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_ButtonControl::IsInAction()
extern "C"  bool VRTK_ButtonControl_IsInAction_m2337483850 (VRTK_ButtonControl_t2487860925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ButtonControl::SetDirectionListener(System.Boolean,VRTK.VRTK_ControllerEvents/ButtonAlias,VRTK.VRTK_ControllerEvents/ButtonAlias&,VRTK.ControllerInteractionEventHandler,VRTK.ControllerInteractionEventHandler)
extern "C"  void VRTK_ButtonControl_SetDirectionListener_m1294539723 (VRTK_ButtonControl_t2487860925 * __this, bool ___state0, int32_t ___directionButton1, int32_t* ___subscribedDirectionButton2, ControllerInteractionEventHandler_t343979916 * ___pressCallback3, ControllerInteractionEventHandler_t343979916 * ___releaseCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ButtonControl::ForwardButtonPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ButtonControl_ForwardButtonPressed_m4231839056 (VRTK_ButtonControl_t2487860925 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ButtonControl::ForwardButtonReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ButtonControl_ForwardButtonReleased_m1125344709 (VRTK_ButtonControl_t2487860925 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ButtonControl::BackwardButtonPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ButtonControl_BackwardButtonPressed_m2310091904 (VRTK_ButtonControl_t2487860925 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ButtonControl::BackwardButtonReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ButtonControl_BackwardButtonReleased_m456160217 (VRTK_ButtonControl_t2487860925 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ButtonControl::LeftButtonPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ButtonControl_LeftButtonPressed_m3190519056 (VRTK_ButtonControl_t2487860925 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ButtonControl::LeftButtonReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ButtonControl_LeftButtonReleased_m415801549 (VRTK_ButtonControl_t2487860925 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ButtonControl::RightButtonPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ButtonControl_RightButtonPressed_m2653598325 (VRTK_ButtonControl_t2487860925 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ButtonControl::RightButtonReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ButtonControl_RightButtonReleased_m2282089456 (VRTK_ButtonControl_t2487860925 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
