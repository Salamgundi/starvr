﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_IsOverlayVisible
struct _IsOverlayVisible_t1564696099;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_IsOverlayVisible::.ctor(System.Object,System.IntPtr)
extern "C"  void _IsOverlayVisible__ctor_m3224570166 (_IsOverlayVisible_t1564696099 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVROverlay/_IsOverlayVisible::Invoke(System.UInt64)
extern "C"  bool _IsOverlayVisible_Invoke_m2382339705 (_IsOverlayVisible_t1564696099 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_IsOverlayVisible::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _IsOverlayVisible_BeginInvoke_m3766352140 (_IsOverlayVisible_t1564696099 * __this, uint64_t ___ulOverlayHandle0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVROverlay/_IsOverlayVisible::EndInvoke(System.IAsyncResult)
extern "C"  bool _IsOverlayVisible_EndInvoke_m815443206 (_IsOverlayVisible_t1564696099 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
