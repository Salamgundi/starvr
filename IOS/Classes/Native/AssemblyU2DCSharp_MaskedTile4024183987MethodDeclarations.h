﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MaskedTile
struct MaskedTile_t4024183987;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void MaskedTile::.ctor()
extern "C"  void MaskedTile__ctor_m3345174548 (MaskedTile_t4024183987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MaskedTile::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void MaskedTile_OnPointerEnter_m2963555742 (MaskedTile_t4024183987 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MaskedTile::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void MaskedTile_OnPointerExit_m397974906 (MaskedTile_t4024183987 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MaskedTile::OnGvrPointerHover(UnityEngine.EventSystems.PointerEventData)
extern "C"  void MaskedTile_OnGvrPointerHover_m1818417205 (MaskedTile_t4024183987 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
