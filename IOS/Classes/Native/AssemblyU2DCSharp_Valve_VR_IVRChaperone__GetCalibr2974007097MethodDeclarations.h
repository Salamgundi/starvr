﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperone/_GetCalibrationState
struct _GetCalibrationState_t2974007097;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ChaperoneCalibrationState49870780.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperone/_GetCalibrationState::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetCalibrationState__ctor_m1392193966 (_GetCalibrationState_t2974007097 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.ChaperoneCalibrationState Valve.VR.IVRChaperone/_GetCalibrationState::Invoke()
extern "C"  int32_t _GetCalibrationState_Invoke_m2663499587 (_GetCalibrationState_t2974007097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperone/_GetCalibrationState::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetCalibrationState_BeginInvoke_m847520607 (_GetCalibrationState_t2974007097 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.ChaperoneCalibrationState Valve.VR.IVRChaperone/_GetCalibrationState::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _GetCalibrationState_EndInvoke_m593867297 (_GetCalibrationState_t2974007097 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
