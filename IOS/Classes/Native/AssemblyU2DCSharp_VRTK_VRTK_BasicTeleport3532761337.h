﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_PolicyList
struct VRTK_PolicyList_t2965133344;
// VRTK.TeleportEventHandler
struct TeleportEventHandler_t2417981165;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_BasicTeleport
struct  VRTK_BasicTeleport_t3532761337  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VRTK.VRTK_BasicTeleport::blinkTransitionSpeed
	float ___blinkTransitionSpeed_2;
	// System.Single VRTK.VRTK_BasicTeleport::distanceBlinkDelay
	float ___distanceBlinkDelay_3;
	// System.Boolean VRTK.VRTK_BasicTeleport::headsetPositionCompensation
	bool ___headsetPositionCompensation_4;
	// VRTK.VRTK_PolicyList VRTK.VRTK_BasicTeleport::targetListPolicy
	VRTK_PolicyList_t2965133344 * ___targetListPolicy_5;
	// System.Single VRTK.VRTK_BasicTeleport::navMeshLimitDistance
	float ___navMeshLimitDistance_6;
	// VRTK.TeleportEventHandler VRTK.VRTK_BasicTeleport::Teleporting
	TeleportEventHandler_t2417981165 * ___Teleporting_7;
	// VRTK.TeleportEventHandler VRTK.VRTK_BasicTeleport::Teleported
	TeleportEventHandler_t2417981165 * ___Teleported_8;
	// UnityEngine.Transform VRTK.VRTK_BasicTeleport::headset
	Transform_t3275118058 * ___headset_9;
	// UnityEngine.Transform VRTK.VRTK_BasicTeleport::playArea
	Transform_t3275118058 * ___playArea_10;
	// System.Boolean VRTK.VRTK_BasicTeleport::adjustYForTerrain
	bool ___adjustYForTerrain_11;
	// System.Boolean VRTK.VRTK_BasicTeleport::enableTeleport
	bool ___enableTeleport_12;
	// System.Single VRTK.VRTK_BasicTeleport::blinkPause
	float ___blinkPause_13;
	// System.Single VRTK.VRTK_BasicTeleport::fadeInTime
	float ___fadeInTime_14;
	// System.Single VRTK.VRTK_BasicTeleport::maxBlinkTransitionSpeed
	float ___maxBlinkTransitionSpeed_15;
	// System.Single VRTK.VRTK_BasicTeleport::maxBlinkDistance
	float ___maxBlinkDistance_16;
	// UnityEngine.Coroutine VRTK.VRTK_BasicTeleport::initaliseListeners
	Coroutine_t2299508840 * ___initaliseListeners_17;

public:
	inline static int32_t get_offset_of_blinkTransitionSpeed_2() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___blinkTransitionSpeed_2)); }
	inline float get_blinkTransitionSpeed_2() const { return ___blinkTransitionSpeed_2; }
	inline float* get_address_of_blinkTransitionSpeed_2() { return &___blinkTransitionSpeed_2; }
	inline void set_blinkTransitionSpeed_2(float value)
	{
		___blinkTransitionSpeed_2 = value;
	}

	inline static int32_t get_offset_of_distanceBlinkDelay_3() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___distanceBlinkDelay_3)); }
	inline float get_distanceBlinkDelay_3() const { return ___distanceBlinkDelay_3; }
	inline float* get_address_of_distanceBlinkDelay_3() { return &___distanceBlinkDelay_3; }
	inline void set_distanceBlinkDelay_3(float value)
	{
		___distanceBlinkDelay_3 = value;
	}

	inline static int32_t get_offset_of_headsetPositionCompensation_4() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___headsetPositionCompensation_4)); }
	inline bool get_headsetPositionCompensation_4() const { return ___headsetPositionCompensation_4; }
	inline bool* get_address_of_headsetPositionCompensation_4() { return &___headsetPositionCompensation_4; }
	inline void set_headsetPositionCompensation_4(bool value)
	{
		___headsetPositionCompensation_4 = value;
	}

	inline static int32_t get_offset_of_targetListPolicy_5() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___targetListPolicy_5)); }
	inline VRTK_PolicyList_t2965133344 * get_targetListPolicy_5() const { return ___targetListPolicy_5; }
	inline VRTK_PolicyList_t2965133344 ** get_address_of_targetListPolicy_5() { return &___targetListPolicy_5; }
	inline void set_targetListPolicy_5(VRTK_PolicyList_t2965133344 * value)
	{
		___targetListPolicy_5 = value;
		Il2CppCodeGenWriteBarrier(&___targetListPolicy_5, value);
	}

	inline static int32_t get_offset_of_navMeshLimitDistance_6() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___navMeshLimitDistance_6)); }
	inline float get_navMeshLimitDistance_6() const { return ___navMeshLimitDistance_6; }
	inline float* get_address_of_navMeshLimitDistance_6() { return &___navMeshLimitDistance_6; }
	inline void set_navMeshLimitDistance_6(float value)
	{
		___navMeshLimitDistance_6 = value;
	}

	inline static int32_t get_offset_of_Teleporting_7() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___Teleporting_7)); }
	inline TeleportEventHandler_t2417981165 * get_Teleporting_7() const { return ___Teleporting_7; }
	inline TeleportEventHandler_t2417981165 ** get_address_of_Teleporting_7() { return &___Teleporting_7; }
	inline void set_Teleporting_7(TeleportEventHandler_t2417981165 * value)
	{
		___Teleporting_7 = value;
		Il2CppCodeGenWriteBarrier(&___Teleporting_7, value);
	}

	inline static int32_t get_offset_of_Teleported_8() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___Teleported_8)); }
	inline TeleportEventHandler_t2417981165 * get_Teleported_8() const { return ___Teleported_8; }
	inline TeleportEventHandler_t2417981165 ** get_address_of_Teleported_8() { return &___Teleported_8; }
	inline void set_Teleported_8(TeleportEventHandler_t2417981165 * value)
	{
		___Teleported_8 = value;
		Il2CppCodeGenWriteBarrier(&___Teleported_8, value);
	}

	inline static int32_t get_offset_of_headset_9() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___headset_9)); }
	inline Transform_t3275118058 * get_headset_9() const { return ___headset_9; }
	inline Transform_t3275118058 ** get_address_of_headset_9() { return &___headset_9; }
	inline void set_headset_9(Transform_t3275118058 * value)
	{
		___headset_9 = value;
		Il2CppCodeGenWriteBarrier(&___headset_9, value);
	}

	inline static int32_t get_offset_of_playArea_10() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___playArea_10)); }
	inline Transform_t3275118058 * get_playArea_10() const { return ___playArea_10; }
	inline Transform_t3275118058 ** get_address_of_playArea_10() { return &___playArea_10; }
	inline void set_playArea_10(Transform_t3275118058 * value)
	{
		___playArea_10 = value;
		Il2CppCodeGenWriteBarrier(&___playArea_10, value);
	}

	inline static int32_t get_offset_of_adjustYForTerrain_11() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___adjustYForTerrain_11)); }
	inline bool get_adjustYForTerrain_11() const { return ___adjustYForTerrain_11; }
	inline bool* get_address_of_adjustYForTerrain_11() { return &___adjustYForTerrain_11; }
	inline void set_adjustYForTerrain_11(bool value)
	{
		___adjustYForTerrain_11 = value;
	}

	inline static int32_t get_offset_of_enableTeleport_12() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___enableTeleport_12)); }
	inline bool get_enableTeleport_12() const { return ___enableTeleport_12; }
	inline bool* get_address_of_enableTeleport_12() { return &___enableTeleport_12; }
	inline void set_enableTeleport_12(bool value)
	{
		___enableTeleport_12 = value;
	}

	inline static int32_t get_offset_of_blinkPause_13() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___blinkPause_13)); }
	inline float get_blinkPause_13() const { return ___blinkPause_13; }
	inline float* get_address_of_blinkPause_13() { return &___blinkPause_13; }
	inline void set_blinkPause_13(float value)
	{
		___blinkPause_13 = value;
	}

	inline static int32_t get_offset_of_fadeInTime_14() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___fadeInTime_14)); }
	inline float get_fadeInTime_14() const { return ___fadeInTime_14; }
	inline float* get_address_of_fadeInTime_14() { return &___fadeInTime_14; }
	inline void set_fadeInTime_14(float value)
	{
		___fadeInTime_14 = value;
	}

	inline static int32_t get_offset_of_maxBlinkTransitionSpeed_15() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___maxBlinkTransitionSpeed_15)); }
	inline float get_maxBlinkTransitionSpeed_15() const { return ___maxBlinkTransitionSpeed_15; }
	inline float* get_address_of_maxBlinkTransitionSpeed_15() { return &___maxBlinkTransitionSpeed_15; }
	inline void set_maxBlinkTransitionSpeed_15(float value)
	{
		___maxBlinkTransitionSpeed_15 = value;
	}

	inline static int32_t get_offset_of_maxBlinkDistance_16() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___maxBlinkDistance_16)); }
	inline float get_maxBlinkDistance_16() const { return ___maxBlinkDistance_16; }
	inline float* get_address_of_maxBlinkDistance_16() { return &___maxBlinkDistance_16; }
	inline void set_maxBlinkDistance_16(float value)
	{
		___maxBlinkDistance_16 = value;
	}

	inline static int32_t get_offset_of_initaliseListeners_17() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_t3532761337, ___initaliseListeners_17)); }
	inline Coroutine_t2299508840 * get_initaliseListeners_17() const { return ___initaliseListeners_17; }
	inline Coroutine_t2299508840 ** get_address_of_initaliseListeners_17() { return &___initaliseListeners_17; }
	inline void set_initaliseListeners_17(Coroutine_t2299508840 * value)
	{
		___initaliseListeners_17 = value;
		Il2CppCodeGenWriteBarrier(&___initaliseListeners_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
