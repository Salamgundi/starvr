﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayRenderingPid
struct _GetOverlayRenderingPid_t1470043428;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayRenderingPid::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayRenderingPid__ctor_m1991923587 (_GetOverlayRenderingPid_t1470043428 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVROverlay/_GetOverlayRenderingPid::Invoke(System.UInt64)
extern "C"  uint32_t _GetOverlayRenderingPid_Invoke_m343767503 (_GetOverlayRenderingPid_t1470043428 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayRenderingPid::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayRenderingPid_BeginInvoke_m527442431 (_GetOverlayRenderingPid_t1470043428 * __this, uint64_t ___ulOverlayHandle0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVROverlay/_GetOverlayRenderingPid::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetOverlayRenderingPid_EndInvoke_m2693384288 (_GetOverlayRenderingPid_t1470043428 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
