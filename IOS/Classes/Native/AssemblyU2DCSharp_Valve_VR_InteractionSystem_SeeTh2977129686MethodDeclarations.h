﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.SeeThru
struct SeeThru_t2977129686;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"

// System.Void Valve.VR.InteractionSystem.SeeThru::.ctor()
extern "C"  void SeeThru__ctor_m1599359634 (SeeThru_t2977129686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SeeThru::Awake()
extern "C"  void SeeThru_Awake_m4008419591 (SeeThru_t2977129686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SeeThru::OnEnable()
extern "C"  void SeeThru_OnEnable_m3108630358 (SeeThru_t2977129686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SeeThru::OnDisable()
extern "C"  void SeeThru_OnDisable_m301128579 (SeeThru_t2977129686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SeeThru::AttachedToHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void SeeThru_AttachedToHand_m912254144 (SeeThru_t2977129686 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SeeThru::DetachedFromHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void SeeThru_DetachedFromHand_m3954775987 (SeeThru_t2977129686 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SeeThru::Update()
extern "C"  void SeeThru_Update_m3059757715 (SeeThru_t2977129686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
