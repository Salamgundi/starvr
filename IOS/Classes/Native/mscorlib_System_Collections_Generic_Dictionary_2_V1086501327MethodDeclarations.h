﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>
struct Dictionary_2_t3694935859;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1086501327.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Valve.VR.EVREventType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3698069496_gshared (Enumerator_t1086501327 * __this, Dictionary_2_t3694935859 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3698069496(__this, ___host0, method) ((  void (*) (Enumerator_t1086501327 *, Dictionary_2_t3694935859 *, const MethodInfo*))Enumerator__ctor_m3698069496_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Valve.VR.EVREventType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m487755943_gshared (Enumerator_t1086501327 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m487755943(__this, method) ((  Il2CppObject * (*) (Enumerator_t1086501327 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m487755943_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Valve.VR.EVREventType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m202848299_gshared (Enumerator_t1086501327 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m202848299(__this, method) ((  void (*) (Enumerator_t1086501327 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m202848299_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Valve.VR.EVREventType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m176294364_gshared (Enumerator_t1086501327 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m176294364(__this, method) ((  void (*) (Enumerator_t1086501327 *, const MethodInfo*))Enumerator_Dispose_m176294364_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Valve.VR.EVREventType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3243427695_gshared (Enumerator_t1086501327 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3243427695(__this, method) ((  bool (*) (Enumerator_t1086501327 *, const MethodInfo*))Enumerator_MoveNext_m3243427695_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Valve.VR.EVREventType,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m385249465_gshared (Enumerator_t1086501327 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m385249465(__this, method) ((  Il2CppObject * (*) (Enumerator_t1086501327 *, const MethodInfo*))Enumerator_get_Current_m385249465_gshared)(__this, method)
