﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ControllerActions/<WaitForModel>c__Iterator0
struct U3CWaitForModelU3Ec__Iterator0_t2574180537;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_ControllerActions/<WaitForModel>c__Iterator0::.ctor()
extern "C"  void U3CWaitForModelU3Ec__Iterator0__ctor_m1006193446 (U3CWaitForModelU3Ec__Iterator0_t2574180537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_ControllerActions/<WaitForModel>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitForModelU3Ec__Iterator0_MoveNext_m3585067206 (U3CWaitForModelU3Ec__Iterator0_t2574180537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_ControllerActions/<WaitForModel>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForModelU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3235491362 (U3CWaitForModelU3Ec__Iterator0_t2574180537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_ControllerActions/<WaitForModel>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForModelU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m913010026 (U3CWaitForModelU3Ec__Iterator0_t2574180537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions/<WaitForModel>c__Iterator0::Dispose()
extern "C"  void U3CWaitForModelU3Ec__Iterator0_Dispose_m1718271355 (U3CWaitForModelU3Ec__Iterator0_t2574180537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions/<WaitForModel>c__Iterator0::Reset()
extern "C"  void U3CWaitForModelU3Ec__Iterator0_Reset_m2082337301 (U3CWaitForModelU3Ec__Iterator0_t2574180537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
