﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GVR.Input.Vector3Event
struct Vector3Event_t2806921088;

#include "codegen/il2cpp-codegen.h"

// System.Void GVR.Input.Vector3Event::.ctor()
extern "C"  void Vector3Event__ctor_m4093400854 (Vector3Event_t2806921088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
