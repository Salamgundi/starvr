﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ItemPackageReference
struct ItemPackageReference_t1778794894;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.ItemPackageReference::.ctor()
extern "C"  void ItemPackageReference__ctor_m2392675380 (ItemPackageReference_t1778794894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
