﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_RenderModelHasComponent
struct _RenderModelHasComponent_t1969881317;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_RenderModelHasComponent::.ctor(System.Object,System.IntPtr)
extern "C"  void _RenderModelHasComponent__ctor_m3003626504 (_RenderModelHasComponent_t1969881317 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRRenderModels/_RenderModelHasComponent::Invoke(System.String,System.String)
extern "C"  bool _RenderModelHasComponent_Invoke_m1753530378 (_RenderModelHasComponent_t1969881317 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_RenderModelHasComponent::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _RenderModelHasComponent_BeginInvoke_m911620243 (_RenderModelHasComponent_t1969881317 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRRenderModels/_RenderModelHasComponent::EndInvoke(System.IAsyncResult)
extern "C"  bool _RenderModelHasComponent_EndInvoke_m500587676 (_RenderModelHasComponent_t1969881317 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
