﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_IsDashboardVisible
struct _IsDashboardVisible_t2020287181;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_IsDashboardVisible::.ctor(System.Object,System.IntPtr)
extern "C"  void _IsDashboardVisible__ctor_m320924486 (_IsDashboardVisible_t2020287181 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVROverlay/_IsDashboardVisible::Invoke()
extern "C"  bool _IsDashboardVisible_Invoke_m469361822 (_IsDashboardVisible_t2020287181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_IsDashboardVisible::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _IsDashboardVisible_BeginInvoke_m1260410659 (_IsDashboardVisible_t2020287181 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVROverlay/_IsDashboardVisible::EndInvoke(System.IAsyncResult)
extern "C"  bool _IsDashboardVisible_EndInvoke_m3344365882 (_IsDashboardVisible_t2020287181 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
