﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.HandLift
struct HandLift_t3915206274;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"

// System.Void VRTK.Examples.HandLift::.ctor()
extern "C"  void HandLift__ctor_m493589343 (HandLift_t3915206274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.HandLift::OnInteractableObjectGrabbed(VRTK.InteractableObjectEventArgs)
extern "C"  void HandLift_OnInteractableObjectGrabbed_m3277406801 (HandLift_t3915206274 * __this, InteractableObjectEventArgs_t473175556  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.HandLift::Update()
extern "C"  void HandLift_Update_m2419330270 (HandLift_t3915206274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
