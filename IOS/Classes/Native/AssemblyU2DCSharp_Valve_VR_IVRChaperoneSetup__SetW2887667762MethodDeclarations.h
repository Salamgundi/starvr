﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_SetWorkingPlayAreaSize
struct _SetWorkingPlayAreaSize_t2887667762;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingPlayAreaSize::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetWorkingPlayAreaSize__ctor_m2768588137 (_SetWorkingPlayAreaSize_t2887667762 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingPlayAreaSize::Invoke(System.Single,System.Single)
extern "C"  void _SetWorkingPlayAreaSize_Invoke_m3049288265 (_SetWorkingPlayAreaSize_t2887667762 * __this, float ___sizeX0, float ___sizeZ1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_SetWorkingPlayAreaSize::BeginInvoke(System.Single,System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetWorkingPlayAreaSize_BeginInvoke_m157746760 (_SetWorkingPlayAreaSize_t2887667762 * __this, float ___sizeX0, float ___sizeZ1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingPlayAreaSize::EndInvoke(System.IAsyncResult)
extern "C"  void _SetWorkingPlayAreaSize_EndInvoke_m2977701639 (_SetWorkingPlayAreaSize_t2887667762 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
