﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_Control/DefaultControlEvents
struct DefaultControlEvents_t3441053308;
// VRTK.Control3DEventHandler
struct Control3DEventHandler_t2392187186;
// VRTK.VRTK_ControllerRigidbodyActivator
struct VRTK_ControllerRigidbodyActivator_t3039396590;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_ControlValueRa2976216666.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Control
struct  VRTK_Control_t651619021  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_Control/DefaultControlEvents VRTK.VRTK_Control::defaultEvents
	DefaultControlEvents_t3441053308 * ___defaultEvents_2;
	// System.Boolean VRTK.VRTK_Control::interactWithoutGrab
	bool ___interactWithoutGrab_3;
	// VRTK.Control3DEventHandler VRTK.VRTK_Control::ValueChanged
	Control3DEventHandler_t2392187186 * ___ValueChanged_4;
	// UnityEngine.Bounds VRTK.VRTK_Control::bounds
	Bounds_t3033363703  ___bounds_5;
	// System.Boolean VRTK.VRTK_Control::setupSuccessful
	bool ___setupSuccessful_6;
	// VRTK.VRTK_ControllerRigidbodyActivator VRTK.VRTK_Control::autoTriggerVolume
	VRTK_ControllerRigidbodyActivator_t3039396590 * ___autoTriggerVolume_7;
	// System.Single VRTK.VRTK_Control::value
	float ___value_8;
	// VRTK.VRTK_Control/ControlValueRange VRTK.VRTK_Control::valueRange
	ControlValueRange_t2976216666  ___valueRange_12;
	// UnityEngine.GameObject VRTK.VRTK_Control::controlContent
	GameObject_t1756533147 * ___controlContent_13;
	// System.Boolean VRTK.VRTK_Control::hideControlContent
	bool ___hideControlContent_14;

public:
	inline static int32_t get_offset_of_defaultEvents_2() { return static_cast<int32_t>(offsetof(VRTK_Control_t651619021, ___defaultEvents_2)); }
	inline DefaultControlEvents_t3441053308 * get_defaultEvents_2() const { return ___defaultEvents_2; }
	inline DefaultControlEvents_t3441053308 ** get_address_of_defaultEvents_2() { return &___defaultEvents_2; }
	inline void set_defaultEvents_2(DefaultControlEvents_t3441053308 * value)
	{
		___defaultEvents_2 = value;
		Il2CppCodeGenWriteBarrier(&___defaultEvents_2, value);
	}

	inline static int32_t get_offset_of_interactWithoutGrab_3() { return static_cast<int32_t>(offsetof(VRTK_Control_t651619021, ___interactWithoutGrab_3)); }
	inline bool get_interactWithoutGrab_3() const { return ___interactWithoutGrab_3; }
	inline bool* get_address_of_interactWithoutGrab_3() { return &___interactWithoutGrab_3; }
	inline void set_interactWithoutGrab_3(bool value)
	{
		___interactWithoutGrab_3 = value;
	}

	inline static int32_t get_offset_of_ValueChanged_4() { return static_cast<int32_t>(offsetof(VRTK_Control_t651619021, ___ValueChanged_4)); }
	inline Control3DEventHandler_t2392187186 * get_ValueChanged_4() const { return ___ValueChanged_4; }
	inline Control3DEventHandler_t2392187186 ** get_address_of_ValueChanged_4() { return &___ValueChanged_4; }
	inline void set_ValueChanged_4(Control3DEventHandler_t2392187186 * value)
	{
		___ValueChanged_4 = value;
		Il2CppCodeGenWriteBarrier(&___ValueChanged_4, value);
	}

	inline static int32_t get_offset_of_bounds_5() { return static_cast<int32_t>(offsetof(VRTK_Control_t651619021, ___bounds_5)); }
	inline Bounds_t3033363703  get_bounds_5() const { return ___bounds_5; }
	inline Bounds_t3033363703 * get_address_of_bounds_5() { return &___bounds_5; }
	inline void set_bounds_5(Bounds_t3033363703  value)
	{
		___bounds_5 = value;
	}

	inline static int32_t get_offset_of_setupSuccessful_6() { return static_cast<int32_t>(offsetof(VRTK_Control_t651619021, ___setupSuccessful_6)); }
	inline bool get_setupSuccessful_6() const { return ___setupSuccessful_6; }
	inline bool* get_address_of_setupSuccessful_6() { return &___setupSuccessful_6; }
	inline void set_setupSuccessful_6(bool value)
	{
		___setupSuccessful_6 = value;
	}

	inline static int32_t get_offset_of_autoTriggerVolume_7() { return static_cast<int32_t>(offsetof(VRTK_Control_t651619021, ___autoTriggerVolume_7)); }
	inline VRTK_ControllerRigidbodyActivator_t3039396590 * get_autoTriggerVolume_7() const { return ___autoTriggerVolume_7; }
	inline VRTK_ControllerRigidbodyActivator_t3039396590 ** get_address_of_autoTriggerVolume_7() { return &___autoTriggerVolume_7; }
	inline void set_autoTriggerVolume_7(VRTK_ControllerRigidbodyActivator_t3039396590 * value)
	{
		___autoTriggerVolume_7 = value;
		Il2CppCodeGenWriteBarrier(&___autoTriggerVolume_7, value);
	}

	inline static int32_t get_offset_of_value_8() { return static_cast<int32_t>(offsetof(VRTK_Control_t651619021, ___value_8)); }
	inline float get_value_8() const { return ___value_8; }
	inline float* get_address_of_value_8() { return &___value_8; }
	inline void set_value_8(float value)
	{
		___value_8 = value;
	}

	inline static int32_t get_offset_of_valueRange_12() { return static_cast<int32_t>(offsetof(VRTK_Control_t651619021, ___valueRange_12)); }
	inline ControlValueRange_t2976216666  get_valueRange_12() const { return ___valueRange_12; }
	inline ControlValueRange_t2976216666 * get_address_of_valueRange_12() { return &___valueRange_12; }
	inline void set_valueRange_12(ControlValueRange_t2976216666  value)
	{
		___valueRange_12 = value;
	}

	inline static int32_t get_offset_of_controlContent_13() { return static_cast<int32_t>(offsetof(VRTK_Control_t651619021, ___controlContent_13)); }
	inline GameObject_t1756533147 * get_controlContent_13() const { return ___controlContent_13; }
	inline GameObject_t1756533147 ** get_address_of_controlContent_13() { return &___controlContent_13; }
	inline void set_controlContent_13(GameObject_t1756533147 * value)
	{
		___controlContent_13 = value;
		Il2CppCodeGenWriteBarrier(&___controlContent_13, value);
	}

	inline static int32_t get_offset_of_hideControlContent_14() { return static_cast<int32_t>(offsetof(VRTK_Control_t651619021, ___hideControlContent_14)); }
	inline bool get_hideControlContent_14() const { return ___hideControlContent_14; }
	inline bool* get_address_of_hideControlContent_14() { return &___hideControlContent_14; }
	inline void set_hideControlContent_14(bool value)
	{
		___hideControlContent_14 = value;
	}
};

struct VRTK_Control_t651619021_StaticFields
{
public:
	// UnityEngine.Color VRTK.VRTK_Control::COLOR_OK
	Color_t2020392075  ___COLOR_OK_9;
	// UnityEngine.Color VRTK.VRTK_Control::COLOR_ERROR
	Color_t2020392075  ___COLOR_ERROR_10;

public:
	inline static int32_t get_offset_of_COLOR_OK_9() { return static_cast<int32_t>(offsetof(VRTK_Control_t651619021_StaticFields, ___COLOR_OK_9)); }
	inline Color_t2020392075  get_COLOR_OK_9() const { return ___COLOR_OK_9; }
	inline Color_t2020392075 * get_address_of_COLOR_OK_9() { return &___COLOR_OK_9; }
	inline void set_COLOR_OK_9(Color_t2020392075  value)
	{
		___COLOR_OK_9 = value;
	}

	inline static int32_t get_offset_of_COLOR_ERROR_10() { return static_cast<int32_t>(offsetof(VRTK_Control_t651619021_StaticFields, ___COLOR_ERROR_10)); }
	inline Color_t2020392075  get_COLOR_ERROR_10() const { return ___COLOR_ERROR_10; }
	inline Color_t2020392075 * get_address_of_COLOR_ERROR_10() { return &___COLOR_ERROR_10; }
	inline void set_COLOR_ERROR_10(Color_t2020392075  value)
	{
		___COLOR_ERROR_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
