﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>
struct ValueCollection_t2397995702;
// System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>
struct Dictionary_2_t3694935859;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1086501327.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2564002873_gshared (ValueCollection_t2397995702 * __this, Dictionary_2_t3694935859 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2564002873(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2397995702 *, Dictionary_2_t3694935859 *, const MethodInfo*))ValueCollection__ctor_m2564002873_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m817171771_gshared (ValueCollection_t2397995702 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m817171771(__this, ___item0, method) ((  void (*) (ValueCollection_t2397995702 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m817171771_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2138940134_gshared (ValueCollection_t2397995702 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2138940134(__this, method) ((  void (*) (ValueCollection_t2397995702 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2138940134_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m132305249_gshared (ValueCollection_t2397995702 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m132305249(__this, ___item0, method) ((  bool (*) (ValueCollection_t2397995702 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m132305249_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2885333860_gshared (ValueCollection_t2397995702 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2885333860(__this, ___item0, method) ((  bool (*) (ValueCollection_t2397995702 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2885333860_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m167065842_gshared (ValueCollection_t2397995702 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m167065842(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2397995702 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m167065842_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m4026706282_gshared (ValueCollection_t2397995702 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m4026706282(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2397995702 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m4026706282_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4273237419_gshared (ValueCollection_t2397995702 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4273237419(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2397995702 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4273237419_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2030574432_gshared (ValueCollection_t2397995702 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2030574432(__this, method) ((  bool (*) (ValueCollection_t2397995702 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2030574432_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2420584342_gshared (ValueCollection_t2397995702 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2420584342(__this, method) ((  bool (*) (ValueCollection_t2397995702 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2420584342_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2275224122_gshared (ValueCollection_t2397995702 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2275224122(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2397995702 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2275224122_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m793632866_gshared (ValueCollection_t2397995702 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m793632866(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2397995702 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m793632866_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1086501327  ValueCollection_GetEnumerator_m1801917245_gshared (ValueCollection_t2397995702 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1801917245(__this, method) ((  Enumerator_t1086501327  (*) (ValueCollection_t2397995702 *, const MethodInfo*))ValueCollection_GetEnumerator_m1801917245_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Valve.VR.EVREventType,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1258902038_gshared (ValueCollection_t2397995702 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1258902038(__this, method) ((  int32_t (*) (ValueCollection_t2397995702 *, const MethodInfo*))ValueCollection_get_Count_m1258902038_gshared)(__this, method)
