﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSettings/_GetString
struct _GetString_t1816180801;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRSettingsError4124928198.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSettings/_GetString::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetString__ctor_m3751110094 (_GetString_t1816180801 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSettings/_GetString::Invoke(System.String,System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRSettingsError&)
extern "C"  void _GetString_Invoke_m707189712 (_GetString_t1816180801 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unValueLen3, int32_t* ___peError4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSettings/_GetString::BeginInvoke(System.String,System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetString_BeginInvoke_m3142172733 (_GetString_t1816180801 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, StringBuilder_t1221177846 * ___pchValue2, uint32_t ___unValueLen3, int32_t* ___peError4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSettings/_GetString::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  void _GetString_EndInvoke_m145041772 (_GetString_t1816180801 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
