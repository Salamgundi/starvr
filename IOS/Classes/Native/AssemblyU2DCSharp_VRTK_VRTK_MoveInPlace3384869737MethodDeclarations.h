﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_MoveInPlace
struct VRTK_MoveInPlace_t3384869737;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_MoveInPlace_ControlOpt4045712330.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// System.Void VRTK.VRTK_MoveInPlace::.ctor()
extern "C"  void VRTK_MoveInPlace__ctor_m3134208319 (VRTK_MoveInPlace_t3384869737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_MoveInPlace::SetControlOptions(VRTK.VRTK_MoveInPlace/ControlOptions)
extern "C"  void VRTK_MoveInPlace_SetControlOptions_m2933802398 (VRTK_MoveInPlace_t3384869737 * __this, int32_t ___givenControlOptions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_MoveInPlace::GetMovementDirection()
extern "C"  Vector3_t2243707580  VRTK_MoveInPlace_GetMovementDirection_m2537722809 (VRTK_MoveInPlace_t3384869737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_MoveInPlace::GetSpeed()
extern "C"  float VRTK_MoveInPlace_GetSpeed_m372608836 (VRTK_MoveInPlace_t3384869737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_MoveInPlace::OnEnable()
extern "C"  void VRTK_MoveInPlace_OnEnable_m78407555 (VRTK_MoveInPlace_t3384869737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_MoveInPlace::OnDisable()
extern "C"  void VRTK_MoveInPlace_OnDisable_m790344490 (VRTK_MoveInPlace_t3384869737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_MoveInPlace::Update()
extern "C"  void VRTK_MoveInPlace_Update_m3513183004 (VRTK_MoveInPlace_t3384869737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_MoveInPlace::FixedUpdate()
extern "C"  void VRTK_MoveInPlace_FixedUpdate_m3588616094 (VRTK_MoveInPlace_t3384869737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_MoveInPlace::CheckControllerState(UnityEngine.GameObject,System.Boolean,System.Boolean&,System.Boolean&)
extern "C"  void VRTK_MoveInPlace_CheckControllerState_m761178449 (VRTK_MoveInPlace_t3384869737 * __this, GameObject_t1756533147 * ___controller0, bool ___controllerState1, bool* ___subscribedState2, bool* ___previousState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_MoveInPlace::HandleFalling()
extern "C"  void VRTK_MoveInPlace_HandleFalling_m2473591864 (VRTK_MoveInPlace_t3384869737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_MoveInPlace::EngageButtonPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_MoveInPlace_EngageButtonPressed_m4215940210 (VRTK_MoveInPlace_t3384869737 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_MoveInPlace::EngageButtonReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_MoveInPlace_EngageButtonReleased_m162855813 (VRTK_MoveInPlace_t3384869737 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion VRTK.VRTK_MoveInPlace::DetermineAverageControllerRotation()
extern "C"  Quaternion_t4030073918  VRTK_MoveInPlace_DetermineAverageControllerRotation_m1448470137 (VRTK_MoveInPlace_t3384869737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion VRTK.VRTK_MoveInPlace::AverageRotation(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  VRTK_MoveInPlace_AverageRotation_m1040755418 (VRTK_MoveInPlace_t3384869737 * __this, Quaternion_t4030073918  ___rot10, Quaternion_t4030073918  ___rot21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_MoveInPlace::Vector3XZOnly(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  VRTK_MoveInPlace_Vector3XZOnly_m590001052 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___vec0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_MoveInPlace::SetControllerListeners(UnityEngine.GameObject,System.Boolean,System.Boolean&,System.Boolean)
extern "C"  void VRTK_MoveInPlace_SetControllerListeners_m490484093 (VRTK_MoveInPlace_t3384869737 * __this, GameObject_t1756533147 * ___controller0, bool ___controllerState1, bool* ___subscribedState2, bool ___forceDisabled3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_MoveInPlace::ToggleControllerListeners(UnityEngine.GameObject,System.Boolean,System.Boolean&)
extern "C"  void VRTK_MoveInPlace_ToggleControllerListeners_m3453655828 (VRTK_MoveInPlace_t3384869737 * __this, GameObject_t1756533147 * ___controller0, bool ___toggle1, bool* ___subscribed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
