﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.LinearMapping
struct LinearMapping_t810676855;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.LinearMapping::.ctor()
extern "C"  void LinearMapping__ctor_m2866329863 (LinearMapping_t810676855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
