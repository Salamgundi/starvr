﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Hand/<UpdateHovering>c__AnonStorey2
struct U3CUpdateHoveringU3Ec__AnonStorey2_t2275351047;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1387717936.h"

// System.Void Valve.VR.InteractionSystem.Hand/<UpdateHovering>c__AnonStorey2::.ctor()
extern "C"  void U3CUpdateHoveringU3Ec__AnonStorey2__ctor_m1415390496 (U3CUpdateHoveringU3Ec__AnonStorey2_t2275351047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Hand/<UpdateHovering>c__AnonStorey2::<>m__0(Valve.VR.InteractionSystem.Hand/AttachedObject)
extern "C"  bool U3CUpdateHoveringU3Ec__AnonStorey2_U3CU3Em__0_m929372103 (U3CUpdateHoveringU3Ec__AnonStorey2_t2275351047 * __this, AttachedObject_t1387717936  ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
