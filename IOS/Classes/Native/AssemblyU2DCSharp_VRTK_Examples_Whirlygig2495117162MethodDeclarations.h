﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Whirlygig
struct Whirlygig_t2495117162;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.Examples.Whirlygig::.ctor()
extern "C"  void Whirlygig__ctor_m2592306895 (Whirlygig_t2495117162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Whirlygig::StartUsing(UnityEngine.GameObject)
extern "C"  void Whirlygig_StartUsing_m3737952285 (Whirlygig_t2495117162 * __this, GameObject_t1756533147 * ___usingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Whirlygig::StopUsing(UnityEngine.GameObject)
extern "C"  void Whirlygig_StopUsing_m1085684737 (Whirlygig_t2495117162 * __this, GameObject_t1756533147 * ___usingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Whirlygig::Start()
extern "C"  void Whirlygig_Start_m1866540035 (Whirlygig_t2495117162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Whirlygig::Update()
extern "C"  void Whirlygig_Update_m1495953610 (Whirlygig_t2495117162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
