﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.UseRotate
struct UseRotate_t241092046;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.Examples.UseRotate::.ctor()
extern "C"  void UseRotate__ctor_m811956253 (UseRotate_t241092046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.UseRotate::StartUsing(UnityEngine.GameObject)
extern "C"  void UseRotate_StartUsing_m3564562883 (UseRotate_t241092046 * __this, GameObject_t1756533147 * ___usingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.UseRotate::StopUsing(UnityEngine.GameObject)
extern "C"  void UseRotate_StopUsing_m1082974687 (UseRotate_t241092046 * __this, GameObject_t1756533147 * ___usingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.UseRotate::Start()
extern "C"  void UseRotate_Start_m1697820477 (UseRotate_t241092046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.UseRotate::Update()
extern "C"  void UseRotate_Update_m3877156090 (UseRotate_t241092046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
