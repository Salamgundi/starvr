﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayTextureBounds
struct _GetOverlayTextureBounds_t3680071147;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_VRTextureBounds_t1897807375.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayTextureBounds::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayTextureBounds__ctor_m2639099136 (_GetOverlayTextureBounds_t3680071147 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTextureBounds::Invoke(System.UInt64,Valve.VR.VRTextureBounds_t&)
extern "C"  int32_t _GetOverlayTextureBounds_Invoke_m59764618 (_GetOverlayTextureBounds_t3680071147 * __this, uint64_t ___ulOverlayHandle0, VRTextureBounds_t_t1897807375 * ___pOverlayTextureBounds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayTextureBounds::BeginInvoke(System.UInt64,Valve.VR.VRTextureBounds_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayTextureBounds_BeginInvoke_m1723875469 (_GetOverlayTextureBounds_t3680071147 * __this, uint64_t ___ulOverlayHandle0, VRTextureBounds_t_t1897807375 * ___pOverlayTextureBounds1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTextureBounds::EndInvoke(Valve.VR.VRTextureBounds_t&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayTextureBounds_EndInvoke_m2619394367 (_GetOverlayTextureBounds_t3680071147 * __this, VRTextureBounds_t_t1897807375 * ___pOverlayTextureBounds0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
