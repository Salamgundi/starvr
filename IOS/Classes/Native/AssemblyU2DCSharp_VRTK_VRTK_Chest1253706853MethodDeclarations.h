﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_Chest
struct VRTK_Chest_t1253706853;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_ControlValueRa2976216666.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_Direction3775008092.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.VRTK_Chest::.ctor()
extern "C"  void VRTK_Chest__ctor_m945737019 (VRTK_Chest_t1253706853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Chest::OnDrawGizmos()
extern "C"  void VRTK_Chest_OnDrawGizmos_m3206762871 (VRTK_Chest_t1253706853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Chest::InitRequiredComponents()
extern "C"  void VRTK_Chest_InitRequiredComponents_m2230731930 (VRTK_Chest_t1253706853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Chest::DetectSetup()
extern "C"  bool VRTK_Chest_DetectSetup_m584747543 (VRTK_Chest_t1253706853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_Control/ControlValueRange VRTK.VRTK_Chest::RegisterValueRange()
extern "C"  ControlValueRange_t2976216666  VRTK_Chest_RegisterValueRange_m1247354759 (VRTK_Chest_t1253706853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Chest::HandleUpdate()
extern "C"  void VRTK_Chest_HandleUpdate_m3037790694 (VRTK_Chest_t1253706853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_Control/Direction VRTK.VRTK_Chest::DetectDirection()
extern "C"  int32_t VRTK_Chest_DetectDirection_m2662212064 (VRTK_Chest_t1253706853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Chest::InitBody()
extern "C"  void VRTK_Chest_InitBody_m598750945 (VRTK_Chest_t1253706853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Chest::InitLid()
extern "C"  void VRTK_Chest_InitLid_m1908484778 (VRTK_Chest_t1253706853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Chest::InitHandle()
extern "C"  void VRTK_Chest_InitHandle_m2126710915 (VRTK_Chest_t1253706853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Chest::CreateInteractableObject(UnityEngine.GameObject)
extern "C"  void VRTK_Chest_CreateInteractableObject_m1119766086 (VRTK_Chest_t1253706853 * __this, GameObject_t1756533147 * ___targetGameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_Chest::CalculateValue()
extern "C"  float VRTK_Chest_CalculateValue_m1921929058 (VRTK_Chest_t1253706853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
