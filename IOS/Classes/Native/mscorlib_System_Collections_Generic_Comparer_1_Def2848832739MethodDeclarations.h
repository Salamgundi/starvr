﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.CombineInstance>
struct DefaultComparer_t2848832739;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_CombineInstance64595210.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.CombineInstance>::.ctor()
extern "C"  void DefaultComparer__ctor_m3443528019_gshared (DefaultComparer_t2848832739 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3443528019(__this, method) ((  void (*) (DefaultComparer_t2848832739 *, const MethodInfo*))DefaultComparer__ctor_m3443528019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.CombineInstance>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3604912316_gshared (DefaultComparer_t2848832739 * __this, CombineInstance_t64595210  ___x0, CombineInstance_t64595210  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3604912316(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2848832739 *, CombineInstance_t64595210 , CombineInstance_t64595210 , const MethodInfo*))DefaultComparer_Compare_m3604912316_gshared)(__this, ___x0, ___y1, method)
