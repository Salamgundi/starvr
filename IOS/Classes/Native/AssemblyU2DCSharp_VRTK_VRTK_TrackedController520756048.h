﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTKTrackedControllerEventHandler
struct VRTKTrackedControllerEventHandler_t2437916365;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_TrackedController
struct  VRTK_TrackedController_t520756048  : public MonoBehaviour_t1158329972
{
public:
	// System.UInt32 VRTK.VRTK_TrackedController::index
	uint32_t ___index_2;
	// VRTK.VRTKTrackedControllerEventHandler VRTK.VRTK_TrackedController::ControllerEnabled
	VRTKTrackedControllerEventHandler_t2437916365 * ___ControllerEnabled_3;
	// VRTK.VRTKTrackedControllerEventHandler VRTK.VRTK_TrackedController::ControllerDisabled
	VRTKTrackedControllerEventHandler_t2437916365 * ___ControllerDisabled_4;
	// VRTK.VRTKTrackedControllerEventHandler VRTK.VRTK_TrackedController::ControllerIndexChanged
	VRTKTrackedControllerEventHandler_t2437916365 * ___ControllerIndexChanged_5;
	// System.UInt32 VRTK.VRTK_TrackedController::currentIndex
	uint32_t ___currentIndex_6;
	// UnityEngine.Coroutine VRTK.VRTK_TrackedController::enableControllerCoroutine
	Coroutine_t2299508840 * ___enableControllerCoroutine_7;
	// UnityEngine.GameObject VRTK.VRTK_TrackedController::aliasController
	GameObject_t1756533147 * ___aliasController_8;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(VRTK_TrackedController_t520756048, ___index_2)); }
	inline uint32_t get_index_2() const { return ___index_2; }
	inline uint32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(uint32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_ControllerEnabled_3() { return static_cast<int32_t>(offsetof(VRTK_TrackedController_t520756048, ___ControllerEnabled_3)); }
	inline VRTKTrackedControllerEventHandler_t2437916365 * get_ControllerEnabled_3() const { return ___ControllerEnabled_3; }
	inline VRTKTrackedControllerEventHandler_t2437916365 ** get_address_of_ControllerEnabled_3() { return &___ControllerEnabled_3; }
	inline void set_ControllerEnabled_3(VRTKTrackedControllerEventHandler_t2437916365 * value)
	{
		___ControllerEnabled_3 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerEnabled_3, value);
	}

	inline static int32_t get_offset_of_ControllerDisabled_4() { return static_cast<int32_t>(offsetof(VRTK_TrackedController_t520756048, ___ControllerDisabled_4)); }
	inline VRTKTrackedControllerEventHandler_t2437916365 * get_ControllerDisabled_4() const { return ___ControllerDisabled_4; }
	inline VRTKTrackedControllerEventHandler_t2437916365 ** get_address_of_ControllerDisabled_4() { return &___ControllerDisabled_4; }
	inline void set_ControllerDisabled_4(VRTKTrackedControllerEventHandler_t2437916365 * value)
	{
		___ControllerDisabled_4 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerDisabled_4, value);
	}

	inline static int32_t get_offset_of_ControllerIndexChanged_5() { return static_cast<int32_t>(offsetof(VRTK_TrackedController_t520756048, ___ControllerIndexChanged_5)); }
	inline VRTKTrackedControllerEventHandler_t2437916365 * get_ControllerIndexChanged_5() const { return ___ControllerIndexChanged_5; }
	inline VRTKTrackedControllerEventHandler_t2437916365 ** get_address_of_ControllerIndexChanged_5() { return &___ControllerIndexChanged_5; }
	inline void set_ControllerIndexChanged_5(VRTKTrackedControllerEventHandler_t2437916365 * value)
	{
		___ControllerIndexChanged_5 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerIndexChanged_5, value);
	}

	inline static int32_t get_offset_of_currentIndex_6() { return static_cast<int32_t>(offsetof(VRTK_TrackedController_t520756048, ___currentIndex_6)); }
	inline uint32_t get_currentIndex_6() const { return ___currentIndex_6; }
	inline uint32_t* get_address_of_currentIndex_6() { return &___currentIndex_6; }
	inline void set_currentIndex_6(uint32_t value)
	{
		___currentIndex_6 = value;
	}

	inline static int32_t get_offset_of_enableControllerCoroutine_7() { return static_cast<int32_t>(offsetof(VRTK_TrackedController_t520756048, ___enableControllerCoroutine_7)); }
	inline Coroutine_t2299508840 * get_enableControllerCoroutine_7() const { return ___enableControllerCoroutine_7; }
	inline Coroutine_t2299508840 ** get_address_of_enableControllerCoroutine_7() { return &___enableControllerCoroutine_7; }
	inline void set_enableControllerCoroutine_7(Coroutine_t2299508840 * value)
	{
		___enableControllerCoroutine_7 = value;
		Il2CppCodeGenWriteBarrier(&___enableControllerCoroutine_7, value);
	}

	inline static int32_t get_offset_of_aliasController_8() { return static_cast<int32_t>(offsetof(VRTK_TrackedController_t520756048, ___aliasController_8)); }
	inline GameObject_t1756533147 * get_aliasController_8() const { return ___aliasController_8; }
	inline GameObject_t1756533147 ** get_address_of_aliasController_8() { return &___aliasController_8; }
	inline void set_aliasController_8(GameObject_t1756533147 * value)
	{
		___aliasController_8 = value;
		Il2CppCodeGenWriteBarrier(&___aliasController_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
