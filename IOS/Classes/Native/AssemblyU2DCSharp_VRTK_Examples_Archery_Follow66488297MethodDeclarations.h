﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Archery.Follow
struct Follow_t66488297;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.Examples.Archery.Follow::.ctor()
extern "C"  void Follow__ctor_m2351561844 (Follow_t66488297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.Follow::Update()
extern "C"  void Follow_Update_m3040149915 (Follow_t66488297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
