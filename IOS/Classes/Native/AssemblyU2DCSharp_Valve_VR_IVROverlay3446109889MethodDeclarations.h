﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay
struct IVROverlay_t3446109889;
struct IVROverlay_t3446109889_marshaled_pinvoke;
struct IVROverlay_t3446109889_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IVROverlay_t3446109889;
struct IVROverlay_t3446109889_marshaled_pinvoke;

extern "C" void IVROverlay_t3446109889_marshal_pinvoke(const IVROverlay_t3446109889& unmarshaled, IVROverlay_t3446109889_marshaled_pinvoke& marshaled);
extern "C" void IVROverlay_t3446109889_marshal_pinvoke_back(const IVROverlay_t3446109889_marshaled_pinvoke& marshaled, IVROverlay_t3446109889& unmarshaled);
extern "C" void IVROverlay_t3446109889_marshal_pinvoke_cleanup(IVROverlay_t3446109889_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IVROverlay_t3446109889;
struct IVROverlay_t3446109889_marshaled_com;

extern "C" void IVROverlay_t3446109889_marshal_com(const IVROverlay_t3446109889& unmarshaled, IVROverlay_t3446109889_marshaled_com& marshaled);
extern "C" void IVROverlay_t3446109889_marshal_com_back(const IVROverlay_t3446109889_marshaled_com& marshaled, IVROverlay_t3446109889& unmarshaled);
extern "C" void IVROverlay_t3446109889_marshal_com_cleanup(IVROverlay_t3446109889_marshaled_com& marshaled);
