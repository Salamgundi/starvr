﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetApplicationLaunchArguments
struct _GetApplicationLaunchArguments_t3777004763;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetApplicationLaunchArguments::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetApplicationLaunchArguments__ctor_m1824081338 (_GetApplicationLaunchArguments_t3777004763 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRApplications/_GetApplicationLaunchArguments::Invoke(System.UInt32,System.String,System.UInt32)
extern "C"  uint32_t _GetApplicationLaunchArguments_Invoke_m1030675471 (_GetApplicationLaunchArguments_t3777004763 * __this, uint32_t ___unHandle0, String_t* ___pchArgs1, uint32_t ___unArgs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetApplicationLaunchArguments::BeginInvoke(System.UInt32,System.String,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetApplicationLaunchArguments_BeginInvoke_m1660235179 (_GetApplicationLaunchArguments_t3777004763 * __this, uint32_t ___unHandle0, String_t* ___pchArgs1, uint32_t ___unArgs2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRApplications/_GetApplicationLaunchArguments::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetApplicationLaunchArguments_EndInvoke_m2221191431 (_GetApplicationLaunchArguments_t3777004763 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
