﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_BasePointerRenderer/PointerOriginSmoothingSettings
struct PointerOriginSmoothingSettings_t2399923839;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_BasePointerRenderer/PointerOriginSmoothingSettings::.ctor()
extern "C"  void PointerOriginSmoothingSettings__ctor_m2293757536 (PointerOriginSmoothingSettings_t2399923839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
