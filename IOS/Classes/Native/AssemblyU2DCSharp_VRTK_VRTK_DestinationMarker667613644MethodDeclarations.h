﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_DestinationMarker
struct VRTK_DestinationMarker_t667613644;
// VRTK.DestinationMarkerEventHandler
struct DestinationMarkerEventHandler_t1148830384;
// VRTK.VRTK_PolicyList
struct VRTK_PolicyList_t2965133344;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventHandl1148830384.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventArgs1852451661.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PolicyList2965133344.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_DestinationMarker::.ctor()
extern "C"  void VRTK_DestinationMarker__ctor_m47221588 (VRTK_DestinationMarker_t667613644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationMarker::add_DestinationMarkerEnter(VRTK.DestinationMarkerEventHandler)
extern "C"  void VRTK_DestinationMarker_add_DestinationMarkerEnter_m3116141619 (VRTK_DestinationMarker_t667613644 * __this, DestinationMarkerEventHandler_t1148830384 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationMarker::remove_DestinationMarkerEnter(VRTK.DestinationMarkerEventHandler)
extern "C"  void VRTK_DestinationMarker_remove_DestinationMarkerEnter_m2155806846 (VRTK_DestinationMarker_t667613644 * __this, DestinationMarkerEventHandler_t1148830384 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationMarker::add_DestinationMarkerExit(VRTK.DestinationMarkerEventHandler)
extern "C"  void VRTK_DestinationMarker_add_DestinationMarkerExit_m1445675215 (VRTK_DestinationMarker_t667613644 * __this, DestinationMarkerEventHandler_t1148830384 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationMarker::remove_DestinationMarkerExit(VRTK.DestinationMarkerEventHandler)
extern "C"  void VRTK_DestinationMarker_remove_DestinationMarkerExit_m539697008 (VRTK_DestinationMarker_t667613644 * __this, DestinationMarkerEventHandler_t1148830384 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationMarker::add_DestinationMarkerSet(VRTK.DestinationMarkerEventHandler)
extern "C"  void VRTK_DestinationMarker_add_DestinationMarkerSet_m155753725 (VRTK_DestinationMarker_t667613644 * __this, DestinationMarkerEventHandler_t1148830384 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationMarker::remove_DestinationMarkerSet(VRTK.DestinationMarkerEventHandler)
extern "C"  void VRTK_DestinationMarker_remove_DestinationMarkerSet_m1889050564 (VRTK_DestinationMarker_t667613644 * __this, DestinationMarkerEventHandler_t1148830384 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationMarker::OnDestinationMarkerEnter(VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_DestinationMarker_OnDestinationMarkerEnter_m715710335 (VRTK_DestinationMarker_t667613644 * __this, DestinationMarkerEventArgs_t1852451661  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationMarker::OnDestinationMarkerExit(VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_DestinationMarker_OnDestinationMarkerExit_m42226999 (VRTK_DestinationMarker_t667613644 * __this, DestinationMarkerEventArgs_t1852451661  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationMarker::OnDestinationMarkerSet(VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_DestinationMarker_OnDestinationMarkerSet_m3969209769 (VRTK_DestinationMarker_t667613644 * __this, DestinationMarkerEventArgs_t1852451661  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationMarker::SetInvalidTarget(VRTK.VRTK_PolicyList)
extern "C"  void VRTK_DestinationMarker_SetInvalidTarget_m4229255837 (VRTK_DestinationMarker_t667613644 * __this, VRTK_PolicyList_t2965133344 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationMarker::SetNavMeshCheckDistance(System.Single)
extern "C"  void VRTK_DestinationMarker_SetNavMeshCheckDistance_m3891381072 (VRTK_DestinationMarker_t667613644 * __this, float ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationMarker::SetHeadsetPositionCompensation(System.Boolean)
extern "C"  void VRTK_DestinationMarker_SetHeadsetPositionCompensation_m3791048380 (VRTK_DestinationMarker_t667613644 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationMarker::OnEnable()
extern "C"  void VRTK_DestinationMarker_OnEnable_m1956865628 (VRTK_DestinationMarker_t667613644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationMarker::OnDisable()
extern "C"  void VRTK_DestinationMarker_OnDisable_m1454766485 (VRTK_DestinationMarker_t667613644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.DestinationMarkerEventArgs VRTK.VRTK_DestinationMarker::SetDestinationMarkerEvent(System.Single,UnityEngine.Transform,UnityEngine.RaycastHit,UnityEngine.Vector3,System.UInt32,System.Boolean)
extern "C"  DestinationMarkerEventArgs_t1852451661  VRTK_DestinationMarker_SetDestinationMarkerEvent_m2498198830 (VRTK_DestinationMarker_t667613644 * __this, float ___distance0, Transform_t3275118058 * ___target1, RaycastHit_t87180320  ___raycastHit2, Vector3_t2243707580  ___position3, uint32_t ___controllerIndex4, bool ___forceDestinationPosition5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
