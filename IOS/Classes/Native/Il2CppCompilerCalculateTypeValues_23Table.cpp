﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetApp1807962071.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__IsQuit1816118422.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__Launch1067354359.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRApplications__GetCurr911045961.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperone1398180532.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperone__GetCalibr2974007097.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperone__GetPlayAr2872887692.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperone__GetPlayAr2644523155.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperone__ReloadInf2817167257.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperone__SetSceneCo792692487.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperone__GetBounds3705523218.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperone__AreBounds1424518499.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperone__ForceBound936994906.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup32382285.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__Commi206435796.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__Reve3249028635.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__GetW1540358174.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__GetWo478315365.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__GetW1852222363.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__GetL1921386472.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__GetWo459969004.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__GetW2605213384.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__SetW2887667762.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__SetW3486256295.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__SetW1261944056.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__SetW1528237404.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__Relo2889659875.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__GetL1524066641.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__SetW2916654968.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__GetLi229051089.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__SetW2516501904.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__GetL2316140367.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__Expor945638768.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRChaperoneSetup__Impo3167195336.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor4206274356.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__SetTrack3662949163.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetTrackin10051991.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__WaitGetP4192584901.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetLastP2717021868.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetLastP3158332482.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__Submit938257482.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__ClearLastS32198265.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__PostPres4129629097.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetFrame1729400753.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetFrameT711480574.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetFrame2433513766.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetCumul1488691712.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__FadeToCo4129572280.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetCurre2258064488.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__FadeGrid1389933364.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetCurre2030734261.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__SetSkybo2590693784.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__ClearSkyb745528287.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__Composit2294069769.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__Composit2760466547.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__Composit2273085604.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__IsFullscr988890383.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetCurre1910270748.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetLastF1990293024.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__CanRender808393456.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__ShowMirr1161105516.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__HideMirro396471547.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__IsMirrorW410903031.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__Composit3462826167.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__ShouldAp1834060663.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__ForceInter78695371.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__ForceRec3057442189.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__SuspendR3440691638.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetMirro2322134645.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__ReleaseM2100129624.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetMirro3071699813.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__ReleaseS3443550108.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__LockGLSh2180462479.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__UnlockGLS320290112.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetVulka2249042297.h"
#include "AssemblyU2DCSharp_Valve_VR_IVRCompositor__GetVulka3459249220.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay3446109889.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__FindOverlay1862706542.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__CreateOverl4211228331.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__DestroyOver1020329117.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetHighQual4225758140.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetHighQuali362587296.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayK1341267772.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayNa221395366.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayIm836541042.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayE3535846231.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayR1970553664.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayR1470043428.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayF1900166033.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayF3676293589.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayC2804573176.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayC2379424348.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayA2092887413.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayA2185592753.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayTe460846625.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayT1835744573.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayS3099711365.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlaySo376620481.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__SetOverlayW3047220066.h"
#include "AssemblyU2DCSharp_Valve_VR_IVROverlay__GetOverlayW2815542566.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (_GetApplicationsTransitionStateNameFromEnum_t1807962071), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (_IsQuitUserPromptRequested_t1816118422), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (_LaunchInternalProcess_t1067354359), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (_GetCurrentSceneProcessId_t911045961), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (IVRChaperone_t1398180532)+ sizeof (Il2CppObject), sizeof(IVRChaperone_t1398180532_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2304[8] = 
{
	IVRChaperone_t1398180532::get_offset_of_GetCalibrationState_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperone_t1398180532::get_offset_of_GetPlayAreaSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperone_t1398180532::get_offset_of_GetPlayAreaRect_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperone_t1398180532::get_offset_of_ReloadInfo_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperone_t1398180532::get_offset_of_SetSceneColor_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperone_t1398180532::get_offset_of_GetBoundsColor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperone_t1398180532::get_offset_of_AreBoundsVisible_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperone_t1398180532::get_offset_of_ForceBoundsVisible_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (_GetCalibrationState_t2974007097), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (_GetPlayAreaSize_t2872887692), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (_GetPlayAreaRect_t2644523155), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (_ReloadInfo_t2817167257), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (_SetSceneColor_t792692487), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (_GetBoundsColor_t3705523218), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (_AreBoundsVisible_t1424518499), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (_ForceBoundsVisible_t936994906), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (IVRChaperoneSetup_t32382285)+ sizeof (Il2CppObject), sizeof(IVRChaperoneSetup_t32382285_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2313[20] = 
{
	IVRChaperoneSetup_t32382285::get_offset_of_CommitWorkingCopy_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_RevertWorkingCopy_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_GetWorkingPlayAreaSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_GetWorkingPlayAreaRect_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_GetWorkingCollisionBoundsInfo_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_GetLiveCollisionBoundsInfo_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_GetWorkingSeatedZeroPoseToRawTrackingPose_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_GetWorkingStandingZeroPoseToRawTrackingPose_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_SetWorkingPlayAreaSize_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_SetWorkingCollisionBoundsInfo_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_SetWorkingSeatedZeroPoseToRawTrackingPose_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_SetWorkingStandingZeroPoseToRawTrackingPose_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_ReloadFromDisk_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_GetLiveSeatedZeroPoseToRawTrackingPose_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_SetWorkingCollisionBoundsTagsInfo_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_GetLiveCollisionBoundsTagsInfo_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_SetWorkingPhysicalBoundsInfo_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_GetLivePhysicalBoundsInfo_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_ExportLiveToBuffer_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRChaperoneSetup_t32382285::get_offset_of_ImportFromBufferToWorking_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (_CommitWorkingCopy_t206435796), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (_RevertWorkingCopy_t3249028635), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (_GetWorkingPlayAreaSize_t1540358174), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (_GetWorkingPlayAreaRect_t478315365), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (_GetWorkingCollisionBoundsInfo_t1852222363), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (_GetLiveCollisionBoundsInfo_t1921386472), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (_GetWorkingSeatedZeroPoseToRawTrackingPose_t459969004), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (_GetWorkingStandingZeroPoseToRawTrackingPose_t2605213384), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (_SetWorkingPlayAreaSize_t2887667762), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (_SetWorkingCollisionBoundsInfo_t3486256295), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (_SetWorkingSeatedZeroPoseToRawTrackingPose_t1261944056), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (_SetWorkingStandingZeroPoseToRawTrackingPose_t1528237404), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (_ReloadFromDisk_t2889659875), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (_GetLiveSeatedZeroPoseToRawTrackingPose_t1524066641), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (_SetWorkingCollisionBoundsTagsInfo_t2916654968), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (_GetLiveCollisionBoundsTagsInfo_t229051089), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (_SetWorkingPhysicalBoundsInfo_t2516501904), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (_GetLivePhysicalBoundsInfo_t2316140367), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (_ExportLiveToBuffer_t945638768), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (_ImportFromBufferToWorking_t3167195336), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (IVRCompositor_t4206274356)+ sizeof (Il2CppObject), sizeof(IVRCompositor_t4206274356_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2334[41] = 
{
	IVRCompositor_t4206274356::get_offset_of_SetTrackingSpace_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetTrackingSpace_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_WaitGetPoses_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetLastPoses_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetLastPoseForTrackedDeviceIndex_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_Submit_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_ClearLastSubmittedFrame_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_PostPresentHandoff_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetFrameTiming_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetFrameTimings_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetFrameTimeRemaining_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetCumulativeStats_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_FadeToColor_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetCurrentFadeColor_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_FadeGrid_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetCurrentGridAlpha_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_SetSkyboxOverride_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_ClearSkyboxOverride_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_CompositorBringToFront_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_CompositorGoToBack_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_CompositorQuit_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_IsFullscreen_21() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetCurrentSceneFocusProcess_22() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetLastFrameRenderer_23() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_CanRenderScene_24() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_ShowMirrorWindow_25() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_HideMirrorWindow_26() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_IsMirrorWindowVisible_27() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_CompositorDumpImages_28() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_ShouldAppRenderWithLowResources_29() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_ForceInterleavedReprojectionOn_30() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_ForceReconnectProcess_31() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_SuspendRendering_32() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetMirrorTextureD3D11_33() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_ReleaseMirrorTextureD3D11_34() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetMirrorTextureGL_35() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_ReleaseSharedGLTexture_36() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_LockGLSharedTextureForAccess_37() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_UnlockGLSharedTextureForAccess_38() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetVulkanInstanceExtensionsRequired_39() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVRCompositor_t4206274356::get_offset_of_GetVulkanDeviceExtensionsRequired_40() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (_SetTrackingSpace_t3662949163), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (_GetTrackingSpace_t10051991), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (_WaitGetPoses_t4192584901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (_GetLastPoses_t2717021868), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (_GetLastPoseForTrackedDeviceIndex_t3158332482), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (_Submit_t938257482), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (_ClearLastSubmittedFrame_t32198265), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (_PostPresentHandoff_t4129629097), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (_GetFrameTiming_t1729400753), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (_GetFrameTimings_t711480574), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (_GetFrameTimeRemaining_t2433513766), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (_GetCumulativeStats_t1488691712), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (_FadeToColor_t4129572280), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (_GetCurrentFadeColor_t2258064488), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (_FadeGrid_t1389933364), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (_GetCurrentGridAlpha_t2030734261), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (_SetSkyboxOverride_t2590693784), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (_ClearSkyboxOverride_t745528287), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (_CompositorBringToFront_t2294069769), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (_CompositorGoToBack_t2760466547), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (_CompositorQuit_t2273085604), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (_IsFullscreen_t988890383), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (_GetCurrentSceneFocusProcess_t1910270748), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (_GetLastFrameRenderer_t1990293024), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (_CanRenderScene_t808393456), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (_ShowMirrorWindow_t1161105516), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (_HideMirrorWindow_t396471547), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (_IsMirrorWindowVisible_t410903031), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (_CompositorDumpImages_t3462826167), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (_ShouldAppRenderWithLowResources_t1834060663), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (_ForceInterleavedReprojectionOn_t78695371), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (_ForceReconnectProcess_t3057442189), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (_SuspendRendering_t3440691638), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (_GetMirrorTextureD3D11_t2322134645), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (_ReleaseMirrorTextureD3D11_t2100129624), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (_GetMirrorTextureGL_t3071699813), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (_ReleaseSharedGLTexture_t3443550108), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (_LockGLSharedTextureForAccess_t2180462479), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (_UnlockGLSharedTextureForAccess_t320290112), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (_GetVulkanInstanceExtensionsRequired_t2249042297), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (_GetVulkanDeviceExtensionsRequired_t3459249220), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (IVROverlay_t3446109889)+ sizeof (Il2CppObject), sizeof(IVROverlay_t3446109889_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2376[75] = 
{
	IVROverlay_t3446109889::get_offset_of_FindOverlay_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_CreateOverlay_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_DestroyOverlay_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetHighQualityOverlay_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetHighQualityOverlay_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayKey_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayName_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayImageData_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayErrorNameFromEnum_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayRenderingPid_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayRenderingPid_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayFlag_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayFlag_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayColor_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayColor_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayAlpha_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayAlpha_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayTexelAspect_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayTexelAspect_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlaySortOrder_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlaySortOrder_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayWidthInMeters_21() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayWidthInMeters_22() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayAutoCurveDistanceRangeInMeters_23() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayAutoCurveDistanceRangeInMeters_24() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayTextureColorSpace_25() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayTextureColorSpace_26() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayTextureBounds_27() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayTextureBounds_28() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayTransformType_29() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayTransformAbsolute_30() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayTransformAbsolute_31() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayTransformTrackedDeviceRelative_32() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayTransformTrackedDeviceRelative_33() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayTransformTrackedDeviceComponent_34() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayTransformTrackedDeviceComponent_35() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_ShowOverlay_36() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_HideOverlay_37() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_IsOverlayVisible_38() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetTransformForOverlayCoordinates_39() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_PollNextOverlayEvent_40() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayInputMethod_41() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayInputMethod_42() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayMouseScale_43() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayMouseScale_44() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_ComputeOverlayIntersection_45() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_HandleControllerOverlayInteractionAsMouse_46() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_IsHoverTargetOverlay_47() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetGamepadFocusOverlay_48() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetGamepadFocusOverlay_49() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayNeighbor_50() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_MoveGamepadFocusToNeighbor_51() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayTexture_52() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_ClearOverlayTexture_53() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayRaw_54() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayFromFile_55() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayTexture_56() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_ReleaseNativeOverlayHandle_57() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayTextureSize_58() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_CreateDashboardOverlay_59() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_IsDashboardVisible_60() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_IsActiveDashboardOverlay_61() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetDashboardOverlaySceneProcess_62() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetDashboardOverlaySceneProcess_63() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_ShowDashboard_64() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetPrimaryDashboardDevice_65() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_ShowKeyboard_66() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_ShowKeyboardForOverlay_67() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetKeyboardText_68() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_HideKeyboard_69() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetKeyboardTransformAbsolute_70() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetKeyboardPositionForOverlay_71() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_SetOverlayIntersectionMask_72() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_GetOverlayFlags_73() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IVROverlay_t3446109889::get_offset_of_ShowMessageOverlay_74() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (_FindOverlay_t1862706542), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (_CreateOverlay_t4211228331), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (_DestroyOverlay_t1020329117), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (_SetHighQualityOverlay_t4225758140), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (_GetHighQualityOverlay_t362587296), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (_GetOverlayKey_t1341267772), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (_GetOverlayName_t221395366), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (_GetOverlayImageData_t836541042), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (_GetOverlayErrorNameFromEnum_t3535846231), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (_SetOverlayRenderingPid_t1970553664), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (_GetOverlayRenderingPid_t1470043428), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (_SetOverlayFlag_t1900166033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (_GetOverlayFlag_t3676293589), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (_SetOverlayColor_t2804573176), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (_GetOverlayColor_t2379424348), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (_SetOverlayAlpha_t2092887413), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (_GetOverlayAlpha_t2185592753), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (_SetOverlayTexelAspect_t460846625), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (_GetOverlayTexelAspect_t1835744573), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (_SetOverlaySortOrder_t3099711365), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (_GetOverlaySortOrder_t376620481), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (_SetOverlayWidthInMeters_t3047220066), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (_GetOverlayWidthInMeters_t2815542566), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
