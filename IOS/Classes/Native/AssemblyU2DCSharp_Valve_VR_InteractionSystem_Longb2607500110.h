﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// Valve.VR.InteractionSystem.ArrowHand
struct ArrowHand_t565341420;
// Valve.VR.InteractionSystem.ItemPackage
struct ItemPackage_t3423754743;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Valve.VR.InteractionSystem.LinearMapping
struct LinearMapping_t810676855;
// Valve.VR.InteractionSystem.SoundBowClick
struct SoundBowClick_t88828517;
// Valve.VR.InteractionSystem.SoundPlayOneshot
struct SoundPlayOneshot_t1703214483;
// SteamVR_Events/Action
struct Action_t1836998693;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Longbo840502957.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.Longbow
struct  Longbow_t2607500110  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.Longbow/Handedness Valve.VR.InteractionSystem.Longbow::currentHandGuess
	int32_t ___currentHandGuess_2;
	// System.Single Valve.VR.InteractionSystem.Longbow::timeOfPossibleHandSwitch
	float ___timeOfPossibleHandSwitch_3;
	// System.Single Valve.VR.InteractionSystem.Longbow::timeBeforeConfirmingHandSwitch
	float ___timeBeforeConfirmingHandSwitch_4;
	// System.Boolean Valve.VR.InteractionSystem.Longbow::possibleHandSwitch
	bool ___possibleHandSwitch_5;
	// UnityEngine.Transform Valve.VR.InteractionSystem.Longbow::pivotTransform
	Transform_t3275118058 * ___pivotTransform_6;
	// UnityEngine.Transform Valve.VR.InteractionSystem.Longbow::handleTransform
	Transform_t3275118058 * ___handleTransform_7;
	// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.Longbow::hand
	Hand_t379716353 * ___hand_8;
	// Valve.VR.InteractionSystem.ArrowHand Valve.VR.InteractionSystem.Longbow::arrowHand
	ArrowHand_t565341420 * ___arrowHand_9;
	// UnityEngine.Transform Valve.VR.InteractionSystem.Longbow::nockTransform
	Transform_t3275118058 * ___nockTransform_10;
	// UnityEngine.Transform Valve.VR.InteractionSystem.Longbow::nockRestTransform
	Transform_t3275118058 * ___nockRestTransform_11;
	// System.Boolean Valve.VR.InteractionSystem.Longbow::autoSpawnArrowHand
	bool ___autoSpawnArrowHand_12;
	// Valve.VR.InteractionSystem.ItemPackage Valve.VR.InteractionSystem.Longbow::arrowHandItemPackage
	ItemPackage_t3423754743 * ___arrowHandItemPackage_13;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.Longbow::arrowHandPrefab
	GameObject_t1756533147 * ___arrowHandPrefab_14;
	// System.Boolean Valve.VR.InteractionSystem.Longbow::nocked
	bool ___nocked_15;
	// System.Boolean Valve.VR.InteractionSystem.Longbow::pulled
	bool ___pulled_16;
	// System.Single Valve.VR.InteractionSystem.Longbow::nockDistanceTravelled
	float ___nockDistanceTravelled_19;
	// System.Single Valve.VR.InteractionSystem.Longbow::hapticDistanceThreshold
	float ___hapticDistanceThreshold_20;
	// System.Single Valve.VR.InteractionSystem.Longbow::lastTickDistance
	float ___lastTickDistance_21;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.Longbow::bowLeftVector
	Vector3_t2243707580  ___bowLeftVector_24;
	// System.Single Valve.VR.InteractionSystem.Longbow::arrowMinVelocity
	float ___arrowMinVelocity_25;
	// System.Single Valve.VR.InteractionSystem.Longbow::arrowMaxVelocity
	float ___arrowMaxVelocity_26;
	// System.Single Valve.VR.InteractionSystem.Longbow::arrowVelocity
	float ___arrowVelocity_27;
	// System.Single Valve.VR.InteractionSystem.Longbow::minStrainTickTime
	float ___minStrainTickTime_28;
	// System.Single Valve.VR.InteractionSystem.Longbow::maxStrainTickTime
	float ___maxStrainTickTime_29;
	// System.Single Valve.VR.InteractionSystem.Longbow::nextStrainTick
	float ___nextStrainTick_30;
	// System.Boolean Valve.VR.InteractionSystem.Longbow::lerpBackToZeroRotation
	bool ___lerpBackToZeroRotation_31;
	// System.Single Valve.VR.InteractionSystem.Longbow::lerpStartTime
	float ___lerpStartTime_32;
	// System.Single Valve.VR.InteractionSystem.Longbow::lerpDuration
	float ___lerpDuration_33;
	// UnityEngine.Quaternion Valve.VR.InteractionSystem.Longbow::lerpStartRotation
	Quaternion_t4030073918  ___lerpStartRotation_34;
	// System.Single Valve.VR.InteractionSystem.Longbow::nockLerpStartTime
	float ___nockLerpStartTime_35;
	// UnityEngine.Quaternion Valve.VR.InteractionSystem.Longbow::nockLerpStartRotation
	Quaternion_t4030073918  ___nockLerpStartRotation_36;
	// System.Single Valve.VR.InteractionSystem.Longbow::drawOffset
	float ___drawOffset_37;
	// Valve.VR.InteractionSystem.LinearMapping Valve.VR.InteractionSystem.Longbow::bowDrawLinearMapping
	LinearMapping_t810676855 * ___bowDrawLinearMapping_38;
	// System.Boolean Valve.VR.InteractionSystem.Longbow::deferNewPoses
	bool ___deferNewPoses_39;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.Longbow::lateUpdatePos
	Vector3_t2243707580  ___lateUpdatePos_40;
	// UnityEngine.Quaternion Valve.VR.InteractionSystem.Longbow::lateUpdateRot
	Quaternion_t4030073918  ___lateUpdateRot_41;
	// Valve.VR.InteractionSystem.SoundBowClick Valve.VR.InteractionSystem.Longbow::drawSound
	SoundBowClick_t88828517 * ___drawSound_42;
	// System.Single Valve.VR.InteractionSystem.Longbow::drawTension
	float ___drawTension_43;
	// Valve.VR.InteractionSystem.SoundPlayOneshot Valve.VR.InteractionSystem.Longbow::arrowSlideSound
	SoundPlayOneshot_t1703214483 * ___arrowSlideSound_44;
	// Valve.VR.InteractionSystem.SoundPlayOneshot Valve.VR.InteractionSystem.Longbow::releaseSound
	SoundPlayOneshot_t1703214483 * ___releaseSound_45;
	// Valve.VR.InteractionSystem.SoundPlayOneshot Valve.VR.InteractionSystem.Longbow::nockSound
	SoundPlayOneshot_t1703214483 * ___nockSound_46;
	// SteamVR_Events/Action Valve.VR.InteractionSystem.Longbow::newPosesAppliedAction
	Action_t1836998693 * ___newPosesAppliedAction_47;

public:
	inline static int32_t get_offset_of_currentHandGuess_2() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___currentHandGuess_2)); }
	inline int32_t get_currentHandGuess_2() const { return ___currentHandGuess_2; }
	inline int32_t* get_address_of_currentHandGuess_2() { return &___currentHandGuess_2; }
	inline void set_currentHandGuess_2(int32_t value)
	{
		___currentHandGuess_2 = value;
	}

	inline static int32_t get_offset_of_timeOfPossibleHandSwitch_3() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___timeOfPossibleHandSwitch_3)); }
	inline float get_timeOfPossibleHandSwitch_3() const { return ___timeOfPossibleHandSwitch_3; }
	inline float* get_address_of_timeOfPossibleHandSwitch_3() { return &___timeOfPossibleHandSwitch_3; }
	inline void set_timeOfPossibleHandSwitch_3(float value)
	{
		___timeOfPossibleHandSwitch_3 = value;
	}

	inline static int32_t get_offset_of_timeBeforeConfirmingHandSwitch_4() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___timeBeforeConfirmingHandSwitch_4)); }
	inline float get_timeBeforeConfirmingHandSwitch_4() const { return ___timeBeforeConfirmingHandSwitch_4; }
	inline float* get_address_of_timeBeforeConfirmingHandSwitch_4() { return &___timeBeforeConfirmingHandSwitch_4; }
	inline void set_timeBeforeConfirmingHandSwitch_4(float value)
	{
		___timeBeforeConfirmingHandSwitch_4 = value;
	}

	inline static int32_t get_offset_of_possibleHandSwitch_5() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___possibleHandSwitch_5)); }
	inline bool get_possibleHandSwitch_5() const { return ___possibleHandSwitch_5; }
	inline bool* get_address_of_possibleHandSwitch_5() { return &___possibleHandSwitch_5; }
	inline void set_possibleHandSwitch_5(bool value)
	{
		___possibleHandSwitch_5 = value;
	}

	inline static int32_t get_offset_of_pivotTransform_6() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___pivotTransform_6)); }
	inline Transform_t3275118058 * get_pivotTransform_6() const { return ___pivotTransform_6; }
	inline Transform_t3275118058 ** get_address_of_pivotTransform_6() { return &___pivotTransform_6; }
	inline void set_pivotTransform_6(Transform_t3275118058 * value)
	{
		___pivotTransform_6 = value;
		Il2CppCodeGenWriteBarrier(&___pivotTransform_6, value);
	}

	inline static int32_t get_offset_of_handleTransform_7() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___handleTransform_7)); }
	inline Transform_t3275118058 * get_handleTransform_7() const { return ___handleTransform_7; }
	inline Transform_t3275118058 ** get_address_of_handleTransform_7() { return &___handleTransform_7; }
	inline void set_handleTransform_7(Transform_t3275118058 * value)
	{
		___handleTransform_7 = value;
		Il2CppCodeGenWriteBarrier(&___handleTransform_7, value);
	}

	inline static int32_t get_offset_of_hand_8() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___hand_8)); }
	inline Hand_t379716353 * get_hand_8() const { return ___hand_8; }
	inline Hand_t379716353 ** get_address_of_hand_8() { return &___hand_8; }
	inline void set_hand_8(Hand_t379716353 * value)
	{
		___hand_8 = value;
		Il2CppCodeGenWriteBarrier(&___hand_8, value);
	}

	inline static int32_t get_offset_of_arrowHand_9() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___arrowHand_9)); }
	inline ArrowHand_t565341420 * get_arrowHand_9() const { return ___arrowHand_9; }
	inline ArrowHand_t565341420 ** get_address_of_arrowHand_9() { return &___arrowHand_9; }
	inline void set_arrowHand_9(ArrowHand_t565341420 * value)
	{
		___arrowHand_9 = value;
		Il2CppCodeGenWriteBarrier(&___arrowHand_9, value);
	}

	inline static int32_t get_offset_of_nockTransform_10() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___nockTransform_10)); }
	inline Transform_t3275118058 * get_nockTransform_10() const { return ___nockTransform_10; }
	inline Transform_t3275118058 ** get_address_of_nockTransform_10() { return &___nockTransform_10; }
	inline void set_nockTransform_10(Transform_t3275118058 * value)
	{
		___nockTransform_10 = value;
		Il2CppCodeGenWriteBarrier(&___nockTransform_10, value);
	}

	inline static int32_t get_offset_of_nockRestTransform_11() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___nockRestTransform_11)); }
	inline Transform_t3275118058 * get_nockRestTransform_11() const { return ___nockRestTransform_11; }
	inline Transform_t3275118058 ** get_address_of_nockRestTransform_11() { return &___nockRestTransform_11; }
	inline void set_nockRestTransform_11(Transform_t3275118058 * value)
	{
		___nockRestTransform_11 = value;
		Il2CppCodeGenWriteBarrier(&___nockRestTransform_11, value);
	}

	inline static int32_t get_offset_of_autoSpawnArrowHand_12() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___autoSpawnArrowHand_12)); }
	inline bool get_autoSpawnArrowHand_12() const { return ___autoSpawnArrowHand_12; }
	inline bool* get_address_of_autoSpawnArrowHand_12() { return &___autoSpawnArrowHand_12; }
	inline void set_autoSpawnArrowHand_12(bool value)
	{
		___autoSpawnArrowHand_12 = value;
	}

	inline static int32_t get_offset_of_arrowHandItemPackage_13() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___arrowHandItemPackage_13)); }
	inline ItemPackage_t3423754743 * get_arrowHandItemPackage_13() const { return ___arrowHandItemPackage_13; }
	inline ItemPackage_t3423754743 ** get_address_of_arrowHandItemPackage_13() { return &___arrowHandItemPackage_13; }
	inline void set_arrowHandItemPackage_13(ItemPackage_t3423754743 * value)
	{
		___arrowHandItemPackage_13 = value;
		Il2CppCodeGenWriteBarrier(&___arrowHandItemPackage_13, value);
	}

	inline static int32_t get_offset_of_arrowHandPrefab_14() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___arrowHandPrefab_14)); }
	inline GameObject_t1756533147 * get_arrowHandPrefab_14() const { return ___arrowHandPrefab_14; }
	inline GameObject_t1756533147 ** get_address_of_arrowHandPrefab_14() { return &___arrowHandPrefab_14; }
	inline void set_arrowHandPrefab_14(GameObject_t1756533147 * value)
	{
		___arrowHandPrefab_14 = value;
		Il2CppCodeGenWriteBarrier(&___arrowHandPrefab_14, value);
	}

	inline static int32_t get_offset_of_nocked_15() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___nocked_15)); }
	inline bool get_nocked_15() const { return ___nocked_15; }
	inline bool* get_address_of_nocked_15() { return &___nocked_15; }
	inline void set_nocked_15(bool value)
	{
		___nocked_15 = value;
	}

	inline static int32_t get_offset_of_pulled_16() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___pulled_16)); }
	inline bool get_pulled_16() const { return ___pulled_16; }
	inline bool* get_address_of_pulled_16() { return &___pulled_16; }
	inline void set_pulled_16(bool value)
	{
		___pulled_16 = value;
	}

	inline static int32_t get_offset_of_nockDistanceTravelled_19() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___nockDistanceTravelled_19)); }
	inline float get_nockDistanceTravelled_19() const { return ___nockDistanceTravelled_19; }
	inline float* get_address_of_nockDistanceTravelled_19() { return &___nockDistanceTravelled_19; }
	inline void set_nockDistanceTravelled_19(float value)
	{
		___nockDistanceTravelled_19 = value;
	}

	inline static int32_t get_offset_of_hapticDistanceThreshold_20() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___hapticDistanceThreshold_20)); }
	inline float get_hapticDistanceThreshold_20() const { return ___hapticDistanceThreshold_20; }
	inline float* get_address_of_hapticDistanceThreshold_20() { return &___hapticDistanceThreshold_20; }
	inline void set_hapticDistanceThreshold_20(float value)
	{
		___hapticDistanceThreshold_20 = value;
	}

	inline static int32_t get_offset_of_lastTickDistance_21() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___lastTickDistance_21)); }
	inline float get_lastTickDistance_21() const { return ___lastTickDistance_21; }
	inline float* get_address_of_lastTickDistance_21() { return &___lastTickDistance_21; }
	inline void set_lastTickDistance_21(float value)
	{
		___lastTickDistance_21 = value;
	}

	inline static int32_t get_offset_of_bowLeftVector_24() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___bowLeftVector_24)); }
	inline Vector3_t2243707580  get_bowLeftVector_24() const { return ___bowLeftVector_24; }
	inline Vector3_t2243707580 * get_address_of_bowLeftVector_24() { return &___bowLeftVector_24; }
	inline void set_bowLeftVector_24(Vector3_t2243707580  value)
	{
		___bowLeftVector_24 = value;
	}

	inline static int32_t get_offset_of_arrowMinVelocity_25() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___arrowMinVelocity_25)); }
	inline float get_arrowMinVelocity_25() const { return ___arrowMinVelocity_25; }
	inline float* get_address_of_arrowMinVelocity_25() { return &___arrowMinVelocity_25; }
	inline void set_arrowMinVelocity_25(float value)
	{
		___arrowMinVelocity_25 = value;
	}

	inline static int32_t get_offset_of_arrowMaxVelocity_26() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___arrowMaxVelocity_26)); }
	inline float get_arrowMaxVelocity_26() const { return ___arrowMaxVelocity_26; }
	inline float* get_address_of_arrowMaxVelocity_26() { return &___arrowMaxVelocity_26; }
	inline void set_arrowMaxVelocity_26(float value)
	{
		___arrowMaxVelocity_26 = value;
	}

	inline static int32_t get_offset_of_arrowVelocity_27() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___arrowVelocity_27)); }
	inline float get_arrowVelocity_27() const { return ___arrowVelocity_27; }
	inline float* get_address_of_arrowVelocity_27() { return &___arrowVelocity_27; }
	inline void set_arrowVelocity_27(float value)
	{
		___arrowVelocity_27 = value;
	}

	inline static int32_t get_offset_of_minStrainTickTime_28() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___minStrainTickTime_28)); }
	inline float get_minStrainTickTime_28() const { return ___minStrainTickTime_28; }
	inline float* get_address_of_minStrainTickTime_28() { return &___minStrainTickTime_28; }
	inline void set_minStrainTickTime_28(float value)
	{
		___minStrainTickTime_28 = value;
	}

	inline static int32_t get_offset_of_maxStrainTickTime_29() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___maxStrainTickTime_29)); }
	inline float get_maxStrainTickTime_29() const { return ___maxStrainTickTime_29; }
	inline float* get_address_of_maxStrainTickTime_29() { return &___maxStrainTickTime_29; }
	inline void set_maxStrainTickTime_29(float value)
	{
		___maxStrainTickTime_29 = value;
	}

	inline static int32_t get_offset_of_nextStrainTick_30() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___nextStrainTick_30)); }
	inline float get_nextStrainTick_30() const { return ___nextStrainTick_30; }
	inline float* get_address_of_nextStrainTick_30() { return &___nextStrainTick_30; }
	inline void set_nextStrainTick_30(float value)
	{
		___nextStrainTick_30 = value;
	}

	inline static int32_t get_offset_of_lerpBackToZeroRotation_31() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___lerpBackToZeroRotation_31)); }
	inline bool get_lerpBackToZeroRotation_31() const { return ___lerpBackToZeroRotation_31; }
	inline bool* get_address_of_lerpBackToZeroRotation_31() { return &___lerpBackToZeroRotation_31; }
	inline void set_lerpBackToZeroRotation_31(bool value)
	{
		___lerpBackToZeroRotation_31 = value;
	}

	inline static int32_t get_offset_of_lerpStartTime_32() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___lerpStartTime_32)); }
	inline float get_lerpStartTime_32() const { return ___lerpStartTime_32; }
	inline float* get_address_of_lerpStartTime_32() { return &___lerpStartTime_32; }
	inline void set_lerpStartTime_32(float value)
	{
		___lerpStartTime_32 = value;
	}

	inline static int32_t get_offset_of_lerpDuration_33() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___lerpDuration_33)); }
	inline float get_lerpDuration_33() const { return ___lerpDuration_33; }
	inline float* get_address_of_lerpDuration_33() { return &___lerpDuration_33; }
	inline void set_lerpDuration_33(float value)
	{
		___lerpDuration_33 = value;
	}

	inline static int32_t get_offset_of_lerpStartRotation_34() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___lerpStartRotation_34)); }
	inline Quaternion_t4030073918  get_lerpStartRotation_34() const { return ___lerpStartRotation_34; }
	inline Quaternion_t4030073918 * get_address_of_lerpStartRotation_34() { return &___lerpStartRotation_34; }
	inline void set_lerpStartRotation_34(Quaternion_t4030073918  value)
	{
		___lerpStartRotation_34 = value;
	}

	inline static int32_t get_offset_of_nockLerpStartTime_35() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___nockLerpStartTime_35)); }
	inline float get_nockLerpStartTime_35() const { return ___nockLerpStartTime_35; }
	inline float* get_address_of_nockLerpStartTime_35() { return &___nockLerpStartTime_35; }
	inline void set_nockLerpStartTime_35(float value)
	{
		___nockLerpStartTime_35 = value;
	}

	inline static int32_t get_offset_of_nockLerpStartRotation_36() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___nockLerpStartRotation_36)); }
	inline Quaternion_t4030073918  get_nockLerpStartRotation_36() const { return ___nockLerpStartRotation_36; }
	inline Quaternion_t4030073918 * get_address_of_nockLerpStartRotation_36() { return &___nockLerpStartRotation_36; }
	inline void set_nockLerpStartRotation_36(Quaternion_t4030073918  value)
	{
		___nockLerpStartRotation_36 = value;
	}

	inline static int32_t get_offset_of_drawOffset_37() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___drawOffset_37)); }
	inline float get_drawOffset_37() const { return ___drawOffset_37; }
	inline float* get_address_of_drawOffset_37() { return &___drawOffset_37; }
	inline void set_drawOffset_37(float value)
	{
		___drawOffset_37 = value;
	}

	inline static int32_t get_offset_of_bowDrawLinearMapping_38() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___bowDrawLinearMapping_38)); }
	inline LinearMapping_t810676855 * get_bowDrawLinearMapping_38() const { return ___bowDrawLinearMapping_38; }
	inline LinearMapping_t810676855 ** get_address_of_bowDrawLinearMapping_38() { return &___bowDrawLinearMapping_38; }
	inline void set_bowDrawLinearMapping_38(LinearMapping_t810676855 * value)
	{
		___bowDrawLinearMapping_38 = value;
		Il2CppCodeGenWriteBarrier(&___bowDrawLinearMapping_38, value);
	}

	inline static int32_t get_offset_of_deferNewPoses_39() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___deferNewPoses_39)); }
	inline bool get_deferNewPoses_39() const { return ___deferNewPoses_39; }
	inline bool* get_address_of_deferNewPoses_39() { return &___deferNewPoses_39; }
	inline void set_deferNewPoses_39(bool value)
	{
		___deferNewPoses_39 = value;
	}

	inline static int32_t get_offset_of_lateUpdatePos_40() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___lateUpdatePos_40)); }
	inline Vector3_t2243707580  get_lateUpdatePos_40() const { return ___lateUpdatePos_40; }
	inline Vector3_t2243707580 * get_address_of_lateUpdatePos_40() { return &___lateUpdatePos_40; }
	inline void set_lateUpdatePos_40(Vector3_t2243707580  value)
	{
		___lateUpdatePos_40 = value;
	}

	inline static int32_t get_offset_of_lateUpdateRot_41() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___lateUpdateRot_41)); }
	inline Quaternion_t4030073918  get_lateUpdateRot_41() const { return ___lateUpdateRot_41; }
	inline Quaternion_t4030073918 * get_address_of_lateUpdateRot_41() { return &___lateUpdateRot_41; }
	inline void set_lateUpdateRot_41(Quaternion_t4030073918  value)
	{
		___lateUpdateRot_41 = value;
	}

	inline static int32_t get_offset_of_drawSound_42() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___drawSound_42)); }
	inline SoundBowClick_t88828517 * get_drawSound_42() const { return ___drawSound_42; }
	inline SoundBowClick_t88828517 ** get_address_of_drawSound_42() { return &___drawSound_42; }
	inline void set_drawSound_42(SoundBowClick_t88828517 * value)
	{
		___drawSound_42 = value;
		Il2CppCodeGenWriteBarrier(&___drawSound_42, value);
	}

	inline static int32_t get_offset_of_drawTension_43() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___drawTension_43)); }
	inline float get_drawTension_43() const { return ___drawTension_43; }
	inline float* get_address_of_drawTension_43() { return &___drawTension_43; }
	inline void set_drawTension_43(float value)
	{
		___drawTension_43 = value;
	}

	inline static int32_t get_offset_of_arrowSlideSound_44() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___arrowSlideSound_44)); }
	inline SoundPlayOneshot_t1703214483 * get_arrowSlideSound_44() const { return ___arrowSlideSound_44; }
	inline SoundPlayOneshot_t1703214483 ** get_address_of_arrowSlideSound_44() { return &___arrowSlideSound_44; }
	inline void set_arrowSlideSound_44(SoundPlayOneshot_t1703214483 * value)
	{
		___arrowSlideSound_44 = value;
		Il2CppCodeGenWriteBarrier(&___arrowSlideSound_44, value);
	}

	inline static int32_t get_offset_of_releaseSound_45() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___releaseSound_45)); }
	inline SoundPlayOneshot_t1703214483 * get_releaseSound_45() const { return ___releaseSound_45; }
	inline SoundPlayOneshot_t1703214483 ** get_address_of_releaseSound_45() { return &___releaseSound_45; }
	inline void set_releaseSound_45(SoundPlayOneshot_t1703214483 * value)
	{
		___releaseSound_45 = value;
		Il2CppCodeGenWriteBarrier(&___releaseSound_45, value);
	}

	inline static int32_t get_offset_of_nockSound_46() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___nockSound_46)); }
	inline SoundPlayOneshot_t1703214483 * get_nockSound_46() const { return ___nockSound_46; }
	inline SoundPlayOneshot_t1703214483 ** get_address_of_nockSound_46() { return &___nockSound_46; }
	inline void set_nockSound_46(SoundPlayOneshot_t1703214483 * value)
	{
		___nockSound_46 = value;
		Il2CppCodeGenWriteBarrier(&___nockSound_46, value);
	}

	inline static int32_t get_offset_of_newPosesAppliedAction_47() { return static_cast<int32_t>(offsetof(Longbow_t2607500110, ___newPosesAppliedAction_47)); }
	inline Action_t1836998693 * get_newPosesAppliedAction_47() const { return ___newPosesAppliedAction_47; }
	inline Action_t1836998693 ** get_address_of_newPosesAppliedAction_47() { return &___newPosesAppliedAction_47; }
	inline void set_newPosesAppliedAction_47(Action_t1836998693 * value)
	{
		___newPosesAppliedAction_47 = value;
		Il2CppCodeGenWriteBarrier(&___newPosesAppliedAction_47, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
