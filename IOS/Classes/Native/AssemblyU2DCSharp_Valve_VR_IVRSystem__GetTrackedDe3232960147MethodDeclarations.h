﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetTrackedDeviceIndexForControllerRole
struct _GetTrackedDeviceIndexForControllerRole_t3232960147;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedControllerRole361251409.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetTrackedDeviceIndexForControllerRole::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetTrackedDeviceIndexForControllerRole__ctor_m650788904 (_GetTrackedDeviceIndexForControllerRole_t3232960147 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRSystem/_GetTrackedDeviceIndexForControllerRole::Invoke(Valve.VR.ETrackedControllerRole)
extern "C"  uint32_t _GetTrackedDeviceIndexForControllerRole_Invoke_m989873404 (_GetTrackedDeviceIndexForControllerRole_t3232960147 * __this, int32_t ___unDeviceType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetTrackedDeviceIndexForControllerRole::BeginInvoke(Valve.VR.ETrackedControllerRole,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetTrackedDeviceIndexForControllerRole_BeginInvoke_m1997822968 (_GetTrackedDeviceIndexForControllerRole_t3232960147 * __this, int32_t ___unDeviceType0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRSystem/_GetTrackedDeviceIndexForControllerRole::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetTrackedDeviceIndexForControllerRole_EndInvoke_m326872227 (_GetTrackedDeviceIndexForControllerRole_t3232960147 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
