﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SteamVR_Events/Event`3<System.Object,System.Object,System.Object>
struct Event_3_t1695830490;
// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct UnityAction_3_t3482433968;

#include "AssemblyU2DCSharp_SteamVR_Events_Action1836998693.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SteamVR_Events/Action`3<System.Object,System.Object,System.Object>
struct  Action_3_t2223553130  : public Action_t1836998693
{
public:
	// SteamVR_Events/Event`3<T0,T1,T2> SteamVR_Events/Action`3::_event
	Event_3_t1695830490 * ____event_0;
	// UnityEngine.Events.UnityAction`3<T0,T1,T2> SteamVR_Events/Action`3::action
	UnityAction_3_t3482433968 * ___action_1;

public:
	inline static int32_t get_offset_of__event_0() { return static_cast<int32_t>(offsetof(Action_3_t2223553130, ____event_0)); }
	inline Event_3_t1695830490 * get__event_0() const { return ____event_0; }
	inline Event_3_t1695830490 ** get_address_of__event_0() { return &____event_0; }
	inline void set__event_0(Event_3_t1695830490 * value)
	{
		____event_0 = value;
		Il2CppCodeGenWriteBarrier(&____event_0, value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(Action_3_t2223553130, ___action_1)); }
	inline UnityAction_3_t3482433968 * get_action_1() const { return ___action_1; }
	inline UnityAction_3_t3482433968 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(UnityAction_3_t3482433968 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
