﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_RenderModel/RenderModel
struct RenderModel_t582033572;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.Material
struct Material_t193706927;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_Material193706927.h"

// System.Void SteamVR_RenderModel/RenderModel::.ctor(UnityEngine.Mesh,UnityEngine.Material)
extern "C"  void RenderModel__ctor_m3016389549 (RenderModel_t582033572 * __this, Mesh_t1356156583 * ___mesh0, Material_t193706927 * ___material1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh SteamVR_RenderModel/RenderModel::get_mesh()
extern "C"  Mesh_t1356156583 * RenderModel_get_mesh_m1436407112 (RenderModel_t582033572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel/RenderModel::set_mesh(UnityEngine.Mesh)
extern "C"  void RenderModel_set_mesh_m2986987355 (RenderModel_t582033572 * __this, Mesh_t1356156583 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material SteamVR_RenderModel/RenderModel::get_material()
extern "C"  Material_t193706927 * RenderModel_get_material_m2100597384 (RenderModel_t582033572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_RenderModel/RenderModel::set_material(UnityEngine.Material)
extern "C"  void RenderModel_set_material_m3370273851 (RenderModel_t582033572 * __this, Material_t193706927 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
