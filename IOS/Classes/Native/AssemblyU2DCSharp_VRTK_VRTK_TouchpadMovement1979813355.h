﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.TouchpadMovementAxisEventHandler
struct TouchpadMovementAxisEventHandler_t2208405138;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t720607407;
// VRTK.ControllerInteractionEventHandler
struct ControllerInteractionEventHandler_t343979916;
// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// VRTK.VRTK_BodyPhysics
struct VRTK_BodyPhysics_t3414085265;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_Verti2238062081.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DeviceFinder_Devices2408891389.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_Horiz3153229385.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_TouchpadMovement
struct  VRTK_TouchpadMovement_t1979813355  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.TouchpadMovementAxisEventHandler VRTK.VRTK_TouchpadMovement::AxisMovement
	TouchpadMovementAxisEventHandler_t2208405138 * ___AxisMovement_2;
	// System.Boolean VRTK.VRTK_TouchpadMovement::leftController
	bool ___leftController_3;
	// System.Boolean VRTK.VRTK_TouchpadMovement::rightController
	bool ___rightController_4;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_TouchpadMovement::moveOnButtonPress
	int32_t ___moveOnButtonPress_5;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_TouchpadMovement::movementMultiplierButton
	int32_t ___movementMultiplierButton_6;
	// VRTK.VRTK_TouchpadMovement/VerticalAxisMovement VRTK.VRTK_TouchpadMovement::verticalAxisMovement
	int32_t ___verticalAxisMovement_7;
	// System.Single VRTK.VRTK_TouchpadMovement::verticalDeadzone
	float ___verticalDeadzone_8;
	// System.Single VRTK.VRTK_TouchpadMovement::verticalMultiplier
	float ___verticalMultiplier_9;
	// VRTK.VRTK_DeviceFinder/Devices VRTK.VRTK_TouchpadMovement::deviceForDirection
	int32_t ___deviceForDirection_10;
	// System.Boolean VRTK.VRTK_TouchpadMovement::flipDirectionEnabled
	bool ___flipDirectionEnabled_11;
	// System.Single VRTK.VRTK_TouchpadMovement::flipDeadzone
	float ___flipDeadzone_12;
	// System.Single VRTK.VRTK_TouchpadMovement::flipDelay
	float ___flipDelay_13;
	// System.Boolean VRTK.VRTK_TouchpadMovement::flipBlink
	bool ___flipBlink_14;
	// VRTK.VRTK_TouchpadMovement/HorizontalAxisMovement VRTK.VRTK_TouchpadMovement::horizontalAxisMovement
	int32_t ___horizontalAxisMovement_15;
	// System.Single VRTK.VRTK_TouchpadMovement::horizontalDeadzone
	float ___horizontalDeadzone_16;
	// System.Single VRTK.VRTK_TouchpadMovement::horizontalMultiplier
	float ___horizontalMultiplier_17;
	// System.Single VRTK.VRTK_TouchpadMovement::snapRotateDelay
	float ___snapRotateDelay_18;
	// System.Single VRTK.VRTK_TouchpadMovement::snapRotateAngle
	float ___snapRotateAngle_19;
	// System.Single VRTK.VRTK_TouchpadMovement::rotateMaxSpeed
	float ___rotateMaxSpeed_20;
	// System.Single VRTK.VRTK_TouchpadMovement::blinkDurationMultiplier
	float ___blinkDurationMultiplier_21;
	// System.Single VRTK.VRTK_TouchpadMovement::slideMaxSpeed
	float ___slideMaxSpeed_22;
	// System.Single VRTK.VRTK_TouchpadMovement::slideDeceleration
	float ___slideDeceleration_23;
	// System.Single VRTK.VRTK_TouchpadMovement::warpDelay
	float ___warpDelay_24;
	// System.Single VRTK.VRTK_TouchpadMovement::warpRange
	float ___warpRange_25;
	// System.Single VRTK.VRTK_TouchpadMovement::warpMaxAltitudeChange
	float ___warpMaxAltitudeChange_26;
	// UnityEngine.GameObject VRTK.VRTK_TouchpadMovement::controllerLeftHand
	GameObject_t1756533147 * ___controllerLeftHand_27;
	// UnityEngine.GameObject VRTK.VRTK_TouchpadMovement::controllerRightHand
	GameObject_t1756533147 * ___controllerRightHand_28;
	// UnityEngine.Transform VRTK.VRTK_TouchpadMovement::playArea
	Transform_t3275118058 * ___playArea_29;
	// UnityEngine.Vector2 VRTK.VRTK_TouchpadMovement::touchAxis
	Vector2_t2243707579  ___touchAxis_30;
	// System.Single VRTK.VRTK_TouchpadMovement::movementSpeed
	float ___movementSpeed_31;
	// System.Single VRTK.VRTK_TouchpadMovement::strafeSpeed
	float ___strafeSpeed_32;
	// System.Single VRTK.VRTK_TouchpadMovement::blinkFadeInTime
	float ___blinkFadeInTime_33;
	// System.Single VRTK.VRTK_TouchpadMovement::lastWarp
	float ___lastWarp_34;
	// System.Single VRTK.VRTK_TouchpadMovement::lastFlip
	float ___lastFlip_35;
	// System.Single VRTK.VRTK_TouchpadMovement::lastSnapRotate
	float ___lastSnapRotate_36;
	// System.Boolean VRTK.VRTK_TouchpadMovement::multiplyMovement
	bool ___multiplyMovement_37;
	// UnityEngine.CapsuleCollider VRTK.VRTK_TouchpadMovement::bodyCollider
	CapsuleCollider_t720607407 * ___bodyCollider_38;
	// UnityEngine.Transform VRTK.VRTK_TouchpadMovement::headset
	Transform_t3275118058 * ___headset_39;
	// System.Boolean VRTK.VRTK_TouchpadMovement::leftSubscribed
	bool ___leftSubscribed_40;
	// System.Boolean VRTK.VRTK_TouchpadMovement::rightSubscribed
	bool ___rightSubscribed_41;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_TouchpadMovement::touchpadAxisChanged
	ControllerInteractionEventHandler_t343979916 * ___touchpadAxisChanged_42;
	// VRTK.ControllerInteractionEventHandler VRTK.VRTK_TouchpadMovement::touchpadUntouched
	ControllerInteractionEventHandler_t343979916 * ___touchpadUntouched_43;
	// VRTK.VRTK_ControllerEvents VRTK.VRTK_TouchpadMovement::controllerEvents
	VRTK_ControllerEvents_t3225224819 * ___controllerEvents_44;
	// VRTK.VRTK_BodyPhysics VRTK.VRTK_TouchpadMovement::bodyPhysics
	VRTK_BodyPhysics_t3414085265 * ___bodyPhysics_45;
	// System.Boolean VRTK.VRTK_TouchpadMovement::wasFalling
	bool ___wasFalling_46;
	// System.Boolean VRTK.VRTK_TouchpadMovement::previousLeftControllerState
	bool ___previousLeftControllerState_47;
	// System.Boolean VRTK.VRTK_TouchpadMovement::previousRightControllerState
	bool ___previousRightControllerState_48;

public:
	inline static int32_t get_offset_of_AxisMovement_2() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___AxisMovement_2)); }
	inline TouchpadMovementAxisEventHandler_t2208405138 * get_AxisMovement_2() const { return ___AxisMovement_2; }
	inline TouchpadMovementAxisEventHandler_t2208405138 ** get_address_of_AxisMovement_2() { return &___AxisMovement_2; }
	inline void set_AxisMovement_2(TouchpadMovementAxisEventHandler_t2208405138 * value)
	{
		___AxisMovement_2 = value;
		Il2CppCodeGenWriteBarrier(&___AxisMovement_2, value);
	}

	inline static int32_t get_offset_of_leftController_3() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___leftController_3)); }
	inline bool get_leftController_3() const { return ___leftController_3; }
	inline bool* get_address_of_leftController_3() { return &___leftController_3; }
	inline void set_leftController_3(bool value)
	{
		___leftController_3 = value;
	}

	inline static int32_t get_offset_of_rightController_4() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___rightController_4)); }
	inline bool get_rightController_4() const { return ___rightController_4; }
	inline bool* get_address_of_rightController_4() { return &___rightController_4; }
	inline void set_rightController_4(bool value)
	{
		___rightController_4 = value;
	}

	inline static int32_t get_offset_of_moveOnButtonPress_5() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___moveOnButtonPress_5)); }
	inline int32_t get_moveOnButtonPress_5() const { return ___moveOnButtonPress_5; }
	inline int32_t* get_address_of_moveOnButtonPress_5() { return &___moveOnButtonPress_5; }
	inline void set_moveOnButtonPress_5(int32_t value)
	{
		___moveOnButtonPress_5 = value;
	}

	inline static int32_t get_offset_of_movementMultiplierButton_6() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___movementMultiplierButton_6)); }
	inline int32_t get_movementMultiplierButton_6() const { return ___movementMultiplierButton_6; }
	inline int32_t* get_address_of_movementMultiplierButton_6() { return &___movementMultiplierButton_6; }
	inline void set_movementMultiplierButton_6(int32_t value)
	{
		___movementMultiplierButton_6 = value;
	}

	inline static int32_t get_offset_of_verticalAxisMovement_7() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___verticalAxisMovement_7)); }
	inline int32_t get_verticalAxisMovement_7() const { return ___verticalAxisMovement_7; }
	inline int32_t* get_address_of_verticalAxisMovement_7() { return &___verticalAxisMovement_7; }
	inline void set_verticalAxisMovement_7(int32_t value)
	{
		___verticalAxisMovement_7 = value;
	}

	inline static int32_t get_offset_of_verticalDeadzone_8() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___verticalDeadzone_8)); }
	inline float get_verticalDeadzone_8() const { return ___verticalDeadzone_8; }
	inline float* get_address_of_verticalDeadzone_8() { return &___verticalDeadzone_8; }
	inline void set_verticalDeadzone_8(float value)
	{
		___verticalDeadzone_8 = value;
	}

	inline static int32_t get_offset_of_verticalMultiplier_9() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___verticalMultiplier_9)); }
	inline float get_verticalMultiplier_9() const { return ___verticalMultiplier_9; }
	inline float* get_address_of_verticalMultiplier_9() { return &___verticalMultiplier_9; }
	inline void set_verticalMultiplier_9(float value)
	{
		___verticalMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_deviceForDirection_10() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___deviceForDirection_10)); }
	inline int32_t get_deviceForDirection_10() const { return ___deviceForDirection_10; }
	inline int32_t* get_address_of_deviceForDirection_10() { return &___deviceForDirection_10; }
	inline void set_deviceForDirection_10(int32_t value)
	{
		___deviceForDirection_10 = value;
	}

	inline static int32_t get_offset_of_flipDirectionEnabled_11() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___flipDirectionEnabled_11)); }
	inline bool get_flipDirectionEnabled_11() const { return ___flipDirectionEnabled_11; }
	inline bool* get_address_of_flipDirectionEnabled_11() { return &___flipDirectionEnabled_11; }
	inline void set_flipDirectionEnabled_11(bool value)
	{
		___flipDirectionEnabled_11 = value;
	}

	inline static int32_t get_offset_of_flipDeadzone_12() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___flipDeadzone_12)); }
	inline float get_flipDeadzone_12() const { return ___flipDeadzone_12; }
	inline float* get_address_of_flipDeadzone_12() { return &___flipDeadzone_12; }
	inline void set_flipDeadzone_12(float value)
	{
		___flipDeadzone_12 = value;
	}

	inline static int32_t get_offset_of_flipDelay_13() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___flipDelay_13)); }
	inline float get_flipDelay_13() const { return ___flipDelay_13; }
	inline float* get_address_of_flipDelay_13() { return &___flipDelay_13; }
	inline void set_flipDelay_13(float value)
	{
		___flipDelay_13 = value;
	}

	inline static int32_t get_offset_of_flipBlink_14() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___flipBlink_14)); }
	inline bool get_flipBlink_14() const { return ___flipBlink_14; }
	inline bool* get_address_of_flipBlink_14() { return &___flipBlink_14; }
	inline void set_flipBlink_14(bool value)
	{
		___flipBlink_14 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisMovement_15() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___horizontalAxisMovement_15)); }
	inline int32_t get_horizontalAxisMovement_15() const { return ___horizontalAxisMovement_15; }
	inline int32_t* get_address_of_horizontalAxisMovement_15() { return &___horizontalAxisMovement_15; }
	inline void set_horizontalAxisMovement_15(int32_t value)
	{
		___horizontalAxisMovement_15 = value;
	}

	inline static int32_t get_offset_of_horizontalDeadzone_16() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___horizontalDeadzone_16)); }
	inline float get_horizontalDeadzone_16() const { return ___horizontalDeadzone_16; }
	inline float* get_address_of_horizontalDeadzone_16() { return &___horizontalDeadzone_16; }
	inline void set_horizontalDeadzone_16(float value)
	{
		___horizontalDeadzone_16 = value;
	}

	inline static int32_t get_offset_of_horizontalMultiplier_17() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___horizontalMultiplier_17)); }
	inline float get_horizontalMultiplier_17() const { return ___horizontalMultiplier_17; }
	inline float* get_address_of_horizontalMultiplier_17() { return &___horizontalMultiplier_17; }
	inline void set_horizontalMultiplier_17(float value)
	{
		___horizontalMultiplier_17 = value;
	}

	inline static int32_t get_offset_of_snapRotateDelay_18() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___snapRotateDelay_18)); }
	inline float get_snapRotateDelay_18() const { return ___snapRotateDelay_18; }
	inline float* get_address_of_snapRotateDelay_18() { return &___snapRotateDelay_18; }
	inline void set_snapRotateDelay_18(float value)
	{
		___snapRotateDelay_18 = value;
	}

	inline static int32_t get_offset_of_snapRotateAngle_19() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___snapRotateAngle_19)); }
	inline float get_snapRotateAngle_19() const { return ___snapRotateAngle_19; }
	inline float* get_address_of_snapRotateAngle_19() { return &___snapRotateAngle_19; }
	inline void set_snapRotateAngle_19(float value)
	{
		___snapRotateAngle_19 = value;
	}

	inline static int32_t get_offset_of_rotateMaxSpeed_20() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___rotateMaxSpeed_20)); }
	inline float get_rotateMaxSpeed_20() const { return ___rotateMaxSpeed_20; }
	inline float* get_address_of_rotateMaxSpeed_20() { return &___rotateMaxSpeed_20; }
	inline void set_rotateMaxSpeed_20(float value)
	{
		___rotateMaxSpeed_20 = value;
	}

	inline static int32_t get_offset_of_blinkDurationMultiplier_21() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___blinkDurationMultiplier_21)); }
	inline float get_blinkDurationMultiplier_21() const { return ___blinkDurationMultiplier_21; }
	inline float* get_address_of_blinkDurationMultiplier_21() { return &___blinkDurationMultiplier_21; }
	inline void set_blinkDurationMultiplier_21(float value)
	{
		___blinkDurationMultiplier_21 = value;
	}

	inline static int32_t get_offset_of_slideMaxSpeed_22() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___slideMaxSpeed_22)); }
	inline float get_slideMaxSpeed_22() const { return ___slideMaxSpeed_22; }
	inline float* get_address_of_slideMaxSpeed_22() { return &___slideMaxSpeed_22; }
	inline void set_slideMaxSpeed_22(float value)
	{
		___slideMaxSpeed_22 = value;
	}

	inline static int32_t get_offset_of_slideDeceleration_23() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___slideDeceleration_23)); }
	inline float get_slideDeceleration_23() const { return ___slideDeceleration_23; }
	inline float* get_address_of_slideDeceleration_23() { return &___slideDeceleration_23; }
	inline void set_slideDeceleration_23(float value)
	{
		___slideDeceleration_23 = value;
	}

	inline static int32_t get_offset_of_warpDelay_24() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___warpDelay_24)); }
	inline float get_warpDelay_24() const { return ___warpDelay_24; }
	inline float* get_address_of_warpDelay_24() { return &___warpDelay_24; }
	inline void set_warpDelay_24(float value)
	{
		___warpDelay_24 = value;
	}

	inline static int32_t get_offset_of_warpRange_25() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___warpRange_25)); }
	inline float get_warpRange_25() const { return ___warpRange_25; }
	inline float* get_address_of_warpRange_25() { return &___warpRange_25; }
	inline void set_warpRange_25(float value)
	{
		___warpRange_25 = value;
	}

	inline static int32_t get_offset_of_warpMaxAltitudeChange_26() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___warpMaxAltitudeChange_26)); }
	inline float get_warpMaxAltitudeChange_26() const { return ___warpMaxAltitudeChange_26; }
	inline float* get_address_of_warpMaxAltitudeChange_26() { return &___warpMaxAltitudeChange_26; }
	inline void set_warpMaxAltitudeChange_26(float value)
	{
		___warpMaxAltitudeChange_26 = value;
	}

	inline static int32_t get_offset_of_controllerLeftHand_27() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___controllerLeftHand_27)); }
	inline GameObject_t1756533147 * get_controllerLeftHand_27() const { return ___controllerLeftHand_27; }
	inline GameObject_t1756533147 ** get_address_of_controllerLeftHand_27() { return &___controllerLeftHand_27; }
	inline void set_controllerLeftHand_27(GameObject_t1756533147 * value)
	{
		___controllerLeftHand_27 = value;
		Il2CppCodeGenWriteBarrier(&___controllerLeftHand_27, value);
	}

	inline static int32_t get_offset_of_controllerRightHand_28() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___controllerRightHand_28)); }
	inline GameObject_t1756533147 * get_controllerRightHand_28() const { return ___controllerRightHand_28; }
	inline GameObject_t1756533147 ** get_address_of_controllerRightHand_28() { return &___controllerRightHand_28; }
	inline void set_controllerRightHand_28(GameObject_t1756533147 * value)
	{
		___controllerRightHand_28 = value;
		Il2CppCodeGenWriteBarrier(&___controllerRightHand_28, value);
	}

	inline static int32_t get_offset_of_playArea_29() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___playArea_29)); }
	inline Transform_t3275118058 * get_playArea_29() const { return ___playArea_29; }
	inline Transform_t3275118058 ** get_address_of_playArea_29() { return &___playArea_29; }
	inline void set_playArea_29(Transform_t3275118058 * value)
	{
		___playArea_29 = value;
		Il2CppCodeGenWriteBarrier(&___playArea_29, value);
	}

	inline static int32_t get_offset_of_touchAxis_30() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___touchAxis_30)); }
	inline Vector2_t2243707579  get_touchAxis_30() const { return ___touchAxis_30; }
	inline Vector2_t2243707579 * get_address_of_touchAxis_30() { return &___touchAxis_30; }
	inline void set_touchAxis_30(Vector2_t2243707579  value)
	{
		___touchAxis_30 = value;
	}

	inline static int32_t get_offset_of_movementSpeed_31() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___movementSpeed_31)); }
	inline float get_movementSpeed_31() const { return ___movementSpeed_31; }
	inline float* get_address_of_movementSpeed_31() { return &___movementSpeed_31; }
	inline void set_movementSpeed_31(float value)
	{
		___movementSpeed_31 = value;
	}

	inline static int32_t get_offset_of_strafeSpeed_32() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___strafeSpeed_32)); }
	inline float get_strafeSpeed_32() const { return ___strafeSpeed_32; }
	inline float* get_address_of_strafeSpeed_32() { return &___strafeSpeed_32; }
	inline void set_strafeSpeed_32(float value)
	{
		___strafeSpeed_32 = value;
	}

	inline static int32_t get_offset_of_blinkFadeInTime_33() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___blinkFadeInTime_33)); }
	inline float get_blinkFadeInTime_33() const { return ___blinkFadeInTime_33; }
	inline float* get_address_of_blinkFadeInTime_33() { return &___blinkFadeInTime_33; }
	inline void set_blinkFadeInTime_33(float value)
	{
		___blinkFadeInTime_33 = value;
	}

	inline static int32_t get_offset_of_lastWarp_34() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___lastWarp_34)); }
	inline float get_lastWarp_34() const { return ___lastWarp_34; }
	inline float* get_address_of_lastWarp_34() { return &___lastWarp_34; }
	inline void set_lastWarp_34(float value)
	{
		___lastWarp_34 = value;
	}

	inline static int32_t get_offset_of_lastFlip_35() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___lastFlip_35)); }
	inline float get_lastFlip_35() const { return ___lastFlip_35; }
	inline float* get_address_of_lastFlip_35() { return &___lastFlip_35; }
	inline void set_lastFlip_35(float value)
	{
		___lastFlip_35 = value;
	}

	inline static int32_t get_offset_of_lastSnapRotate_36() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___lastSnapRotate_36)); }
	inline float get_lastSnapRotate_36() const { return ___lastSnapRotate_36; }
	inline float* get_address_of_lastSnapRotate_36() { return &___lastSnapRotate_36; }
	inline void set_lastSnapRotate_36(float value)
	{
		___lastSnapRotate_36 = value;
	}

	inline static int32_t get_offset_of_multiplyMovement_37() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___multiplyMovement_37)); }
	inline bool get_multiplyMovement_37() const { return ___multiplyMovement_37; }
	inline bool* get_address_of_multiplyMovement_37() { return &___multiplyMovement_37; }
	inline void set_multiplyMovement_37(bool value)
	{
		___multiplyMovement_37 = value;
	}

	inline static int32_t get_offset_of_bodyCollider_38() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___bodyCollider_38)); }
	inline CapsuleCollider_t720607407 * get_bodyCollider_38() const { return ___bodyCollider_38; }
	inline CapsuleCollider_t720607407 ** get_address_of_bodyCollider_38() { return &___bodyCollider_38; }
	inline void set_bodyCollider_38(CapsuleCollider_t720607407 * value)
	{
		___bodyCollider_38 = value;
		Il2CppCodeGenWriteBarrier(&___bodyCollider_38, value);
	}

	inline static int32_t get_offset_of_headset_39() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___headset_39)); }
	inline Transform_t3275118058 * get_headset_39() const { return ___headset_39; }
	inline Transform_t3275118058 ** get_address_of_headset_39() { return &___headset_39; }
	inline void set_headset_39(Transform_t3275118058 * value)
	{
		___headset_39 = value;
		Il2CppCodeGenWriteBarrier(&___headset_39, value);
	}

	inline static int32_t get_offset_of_leftSubscribed_40() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___leftSubscribed_40)); }
	inline bool get_leftSubscribed_40() const { return ___leftSubscribed_40; }
	inline bool* get_address_of_leftSubscribed_40() { return &___leftSubscribed_40; }
	inline void set_leftSubscribed_40(bool value)
	{
		___leftSubscribed_40 = value;
	}

	inline static int32_t get_offset_of_rightSubscribed_41() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___rightSubscribed_41)); }
	inline bool get_rightSubscribed_41() const { return ___rightSubscribed_41; }
	inline bool* get_address_of_rightSubscribed_41() { return &___rightSubscribed_41; }
	inline void set_rightSubscribed_41(bool value)
	{
		___rightSubscribed_41 = value;
	}

	inline static int32_t get_offset_of_touchpadAxisChanged_42() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___touchpadAxisChanged_42)); }
	inline ControllerInteractionEventHandler_t343979916 * get_touchpadAxisChanged_42() const { return ___touchpadAxisChanged_42; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_touchpadAxisChanged_42() { return &___touchpadAxisChanged_42; }
	inline void set_touchpadAxisChanged_42(ControllerInteractionEventHandler_t343979916 * value)
	{
		___touchpadAxisChanged_42 = value;
		Il2CppCodeGenWriteBarrier(&___touchpadAxisChanged_42, value);
	}

	inline static int32_t get_offset_of_touchpadUntouched_43() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___touchpadUntouched_43)); }
	inline ControllerInteractionEventHandler_t343979916 * get_touchpadUntouched_43() const { return ___touchpadUntouched_43; }
	inline ControllerInteractionEventHandler_t343979916 ** get_address_of_touchpadUntouched_43() { return &___touchpadUntouched_43; }
	inline void set_touchpadUntouched_43(ControllerInteractionEventHandler_t343979916 * value)
	{
		___touchpadUntouched_43 = value;
		Il2CppCodeGenWriteBarrier(&___touchpadUntouched_43, value);
	}

	inline static int32_t get_offset_of_controllerEvents_44() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___controllerEvents_44)); }
	inline VRTK_ControllerEvents_t3225224819 * get_controllerEvents_44() const { return ___controllerEvents_44; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_controllerEvents_44() { return &___controllerEvents_44; }
	inline void set_controllerEvents_44(VRTK_ControllerEvents_t3225224819 * value)
	{
		___controllerEvents_44 = value;
		Il2CppCodeGenWriteBarrier(&___controllerEvents_44, value);
	}

	inline static int32_t get_offset_of_bodyPhysics_45() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___bodyPhysics_45)); }
	inline VRTK_BodyPhysics_t3414085265 * get_bodyPhysics_45() const { return ___bodyPhysics_45; }
	inline VRTK_BodyPhysics_t3414085265 ** get_address_of_bodyPhysics_45() { return &___bodyPhysics_45; }
	inline void set_bodyPhysics_45(VRTK_BodyPhysics_t3414085265 * value)
	{
		___bodyPhysics_45 = value;
		Il2CppCodeGenWriteBarrier(&___bodyPhysics_45, value);
	}

	inline static int32_t get_offset_of_wasFalling_46() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___wasFalling_46)); }
	inline bool get_wasFalling_46() const { return ___wasFalling_46; }
	inline bool* get_address_of_wasFalling_46() { return &___wasFalling_46; }
	inline void set_wasFalling_46(bool value)
	{
		___wasFalling_46 = value;
	}

	inline static int32_t get_offset_of_previousLeftControllerState_47() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___previousLeftControllerState_47)); }
	inline bool get_previousLeftControllerState_47() const { return ___previousLeftControllerState_47; }
	inline bool* get_address_of_previousLeftControllerState_47() { return &___previousLeftControllerState_47; }
	inline void set_previousLeftControllerState_47(bool value)
	{
		___previousLeftControllerState_47 = value;
	}

	inline static int32_t get_offset_of_previousRightControllerState_48() { return static_cast<int32_t>(offsetof(VRTK_TouchpadMovement_t1979813355, ___previousRightControllerState_48)); }
	inline bool get_previousRightControllerState_48() const { return ___previousRightControllerState_48; }
	inline bool* get_address_of_previousRightControllerState_48() { return &___previousRightControllerState_48; }
	inline void set_previousRightControllerState_48(bool value)
	{
		___previousRightControllerState_48 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
