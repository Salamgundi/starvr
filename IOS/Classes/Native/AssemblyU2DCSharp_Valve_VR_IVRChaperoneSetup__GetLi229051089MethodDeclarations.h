﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_GetLiveCollisionBoundsTagsInfo
struct _GetLiveCollisionBoundsTagsInfo_t229051089;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_GetLiveCollisionBoundsTagsInfo::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetLiveCollisionBoundsTagsInfo__ctor_m3365597572 (_GetLiveCollisionBoundsTagsInfo_t229051089 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetLiveCollisionBoundsTagsInfo::Invoke(System.Byte[],System.UInt32&)
extern "C"  bool _GetLiveCollisionBoundsTagsInfo_Invoke_m745362925 (_GetLiveCollisionBoundsTagsInfo_t229051089 * __this, ByteU5BU5D_t3397334013* ___pTagsBuffer0, uint32_t* ___punTagCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_GetLiveCollisionBoundsTagsInfo::BeginInvoke(System.Byte[],System.UInt32&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetLiveCollisionBoundsTagsInfo_BeginInvoke_m892037698 (_GetLiveCollisionBoundsTagsInfo_t229051089 * __this, ByteU5BU5D_t3397334013* ___pTagsBuffer0, uint32_t* ___punTagCount1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetLiveCollisionBoundsTagsInfo::EndInvoke(System.UInt32&,System.IAsyncResult)
extern "C"  bool _GetLiveCollisionBoundsTagsInfo_EndInvoke_m1421118382 (_GetLiveCollisionBoundsTagsInfo_t229051089 * __this, uint32_t* ___punTagCount0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
