﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>
struct ValueCollection_t4050602123;
// System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>
struct Dictionary_2_t1052574984;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2739107748.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2971809126_gshared (ValueCollection_t4050602123 * __this, Dictionary_2_t1052574984 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2971809126(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4050602123 *, Dictionary_2_t1052574984 *, const MethodInfo*))ValueCollection__ctor_m2971809126_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4081813812_gshared (ValueCollection_t4050602123 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4081813812(__this, ___item0, method) ((  void (*) (ValueCollection_t4050602123 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4081813812_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1797909237_gshared (ValueCollection_t4050602123 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1797909237(__this, method) ((  void (*) (ValueCollection_t4050602123 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1797909237_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4122873072_gshared (ValueCollection_t4050602123 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4122873072(__this, ___item0, method) ((  bool (*) (ValueCollection_t4050602123 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4122873072_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2354728161_gshared (ValueCollection_t4050602123 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2354728161(__this, ___item0, method) ((  bool (*) (ValueCollection_t4050602123 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2354728161_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m633958511_gshared (ValueCollection_t4050602123 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m633958511(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4050602123 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m633958511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3720259095_gshared (ValueCollection_t4050602123 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3720259095(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4050602123 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3720259095_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1617558016_gshared (ValueCollection_t4050602123 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1617558016(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4050602123 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1617558016_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1991166525_gshared (ValueCollection_t4050602123 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1991166525(__this, method) ((  bool (*) (ValueCollection_t4050602123 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1991166525_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1308370015_gshared (ValueCollection_t4050602123 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1308370015(__this, method) ((  bool (*) (ValueCollection_t4050602123 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1308370015_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2674763763_gshared (ValueCollection_t4050602123 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2674763763(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4050602123 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2674763763_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m4115803321_gshared (ValueCollection_t4050602123 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m4115803321(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4050602123 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m4115803321_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2739107748  ValueCollection_GetEnumerator_m436904190_gshared (ValueCollection_t4050602123 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m436904190(__this, method) ((  Enumerator_t2739107748  (*) (ValueCollection_t4050602123 *, const MethodInfo*))ValueCollection_GetEnumerator_m436904190_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3834064743_gshared (ValueCollection_t4050602123 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3834064743(__this, method) ((  int32_t (*) (ValueCollection_t4050602123 *, const MethodInfo*))ValueCollection_get_Count_m3834064743_gshared)(__this, method)
