﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Hand/AttachedObject
struct AttachedObject_t1387717936;
struct AttachedObject_t1387717936_marshaled_pinvoke;
struct AttachedObject_t1387717936_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct AttachedObject_t1387717936;
struct AttachedObject_t1387717936_marshaled_pinvoke;

extern "C" void AttachedObject_t1387717936_marshal_pinvoke(const AttachedObject_t1387717936& unmarshaled, AttachedObject_t1387717936_marshaled_pinvoke& marshaled);
extern "C" void AttachedObject_t1387717936_marshal_pinvoke_back(const AttachedObject_t1387717936_marshaled_pinvoke& marshaled, AttachedObject_t1387717936& unmarshaled);
extern "C" void AttachedObject_t1387717936_marshal_pinvoke_cleanup(AttachedObject_t1387717936_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct AttachedObject_t1387717936;
struct AttachedObject_t1387717936_marshaled_com;

extern "C" void AttachedObject_t1387717936_marshal_com(const AttachedObject_t1387717936& unmarshaled, AttachedObject_t1387717936_marshaled_com& marshaled);
extern "C" void AttachedObject_t1387717936_marshal_com_back(const AttachedObject_t1387717936_marshaled_com& marshaled, AttachedObject_t1387717936& unmarshaled);
extern "C" void AttachedObject_t1387717936_marshal_com_cleanup(AttachedObject_t1387717936_marshaled_com& marshaled);
