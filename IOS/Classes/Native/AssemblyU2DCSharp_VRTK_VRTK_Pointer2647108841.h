﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_BasePointerRenderer
struct VRTK_BasePointerRenderer_t1270536273;
// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;

#include "AssemblyU2DCSharp_VRTK_VRTK_DestinationMarker667613644.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Pointer
struct  VRTK_Pointer_t2647108841  : public VRTK_DestinationMarker_t667613644
{
public:
	// VRTK.VRTK_BasePointerRenderer VRTK.VRTK_Pointer::pointerRenderer
	VRTK_BasePointerRenderer_t1270536273 * ___pointerRenderer_9;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_Pointer::activationButton
	int32_t ___activationButton_10;
	// System.Boolean VRTK.VRTK_Pointer::holdButtonToActivate
	bool ___holdButtonToActivate_11;
	// System.Single VRTK.VRTK_Pointer::activationDelay
	float ___activationDelay_12;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_Pointer::selectionButton
	int32_t ___selectionButton_13;
	// System.Boolean VRTK.VRTK_Pointer::selectOnPress
	bool ___selectOnPress_14;
	// System.Boolean VRTK.VRTK_Pointer::interactWithObjects
	bool ___interactWithObjects_15;
	// System.Boolean VRTK.VRTK_Pointer::grabToPointerTip
	bool ___grabToPointerTip_16;
	// VRTK.VRTK_ControllerEvents VRTK.VRTK_Pointer::controller
	VRTK_ControllerEvents_t3225224819 * ___controller_17;
	// UnityEngine.Transform VRTK.VRTK_Pointer::customOrigin
	Transform_t3275118058 * ___customOrigin_18;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_Pointer::subscribedActivationButton
	int32_t ___subscribedActivationButton_19;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_Pointer::subscribedSelectionButton
	int32_t ___subscribedSelectionButton_20;
	// System.Boolean VRTK.VRTK_Pointer::currentSelectOnPress
	bool ___currentSelectOnPress_21;
	// System.Single VRTK.VRTK_Pointer::activateDelayTimer
	float ___activateDelayTimer_22;
	// System.Int32 VRTK.VRTK_Pointer::currentActivationState
	int32_t ___currentActivationState_23;
	// System.Boolean VRTK.VRTK_Pointer::willDeactivate
	bool ___willDeactivate_24;
	// System.Boolean VRTK.VRTK_Pointer::wasActivated
	bool ___wasActivated_25;
	// System.UInt32 VRTK.VRTK_Pointer::controllerIndex
	uint32_t ___controllerIndex_26;
	// VRTK.VRTK_InteractableObject VRTK.VRTK_Pointer::pointerInteractableObject
	VRTK_InteractableObject_t2604188111 * ___pointerInteractableObject_27;

public:
	inline static int32_t get_offset_of_pointerRenderer_9() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___pointerRenderer_9)); }
	inline VRTK_BasePointerRenderer_t1270536273 * get_pointerRenderer_9() const { return ___pointerRenderer_9; }
	inline VRTK_BasePointerRenderer_t1270536273 ** get_address_of_pointerRenderer_9() { return &___pointerRenderer_9; }
	inline void set_pointerRenderer_9(VRTK_BasePointerRenderer_t1270536273 * value)
	{
		___pointerRenderer_9 = value;
		Il2CppCodeGenWriteBarrier(&___pointerRenderer_9, value);
	}

	inline static int32_t get_offset_of_activationButton_10() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___activationButton_10)); }
	inline int32_t get_activationButton_10() const { return ___activationButton_10; }
	inline int32_t* get_address_of_activationButton_10() { return &___activationButton_10; }
	inline void set_activationButton_10(int32_t value)
	{
		___activationButton_10 = value;
	}

	inline static int32_t get_offset_of_holdButtonToActivate_11() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___holdButtonToActivate_11)); }
	inline bool get_holdButtonToActivate_11() const { return ___holdButtonToActivate_11; }
	inline bool* get_address_of_holdButtonToActivate_11() { return &___holdButtonToActivate_11; }
	inline void set_holdButtonToActivate_11(bool value)
	{
		___holdButtonToActivate_11 = value;
	}

	inline static int32_t get_offset_of_activationDelay_12() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___activationDelay_12)); }
	inline float get_activationDelay_12() const { return ___activationDelay_12; }
	inline float* get_address_of_activationDelay_12() { return &___activationDelay_12; }
	inline void set_activationDelay_12(float value)
	{
		___activationDelay_12 = value;
	}

	inline static int32_t get_offset_of_selectionButton_13() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___selectionButton_13)); }
	inline int32_t get_selectionButton_13() const { return ___selectionButton_13; }
	inline int32_t* get_address_of_selectionButton_13() { return &___selectionButton_13; }
	inline void set_selectionButton_13(int32_t value)
	{
		___selectionButton_13 = value;
	}

	inline static int32_t get_offset_of_selectOnPress_14() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___selectOnPress_14)); }
	inline bool get_selectOnPress_14() const { return ___selectOnPress_14; }
	inline bool* get_address_of_selectOnPress_14() { return &___selectOnPress_14; }
	inline void set_selectOnPress_14(bool value)
	{
		___selectOnPress_14 = value;
	}

	inline static int32_t get_offset_of_interactWithObjects_15() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___interactWithObjects_15)); }
	inline bool get_interactWithObjects_15() const { return ___interactWithObjects_15; }
	inline bool* get_address_of_interactWithObjects_15() { return &___interactWithObjects_15; }
	inline void set_interactWithObjects_15(bool value)
	{
		___interactWithObjects_15 = value;
	}

	inline static int32_t get_offset_of_grabToPointerTip_16() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___grabToPointerTip_16)); }
	inline bool get_grabToPointerTip_16() const { return ___grabToPointerTip_16; }
	inline bool* get_address_of_grabToPointerTip_16() { return &___grabToPointerTip_16; }
	inline void set_grabToPointerTip_16(bool value)
	{
		___grabToPointerTip_16 = value;
	}

	inline static int32_t get_offset_of_controller_17() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___controller_17)); }
	inline VRTK_ControllerEvents_t3225224819 * get_controller_17() const { return ___controller_17; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_controller_17() { return &___controller_17; }
	inline void set_controller_17(VRTK_ControllerEvents_t3225224819 * value)
	{
		___controller_17 = value;
		Il2CppCodeGenWriteBarrier(&___controller_17, value);
	}

	inline static int32_t get_offset_of_customOrigin_18() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___customOrigin_18)); }
	inline Transform_t3275118058 * get_customOrigin_18() const { return ___customOrigin_18; }
	inline Transform_t3275118058 ** get_address_of_customOrigin_18() { return &___customOrigin_18; }
	inline void set_customOrigin_18(Transform_t3275118058 * value)
	{
		___customOrigin_18 = value;
		Il2CppCodeGenWriteBarrier(&___customOrigin_18, value);
	}

	inline static int32_t get_offset_of_subscribedActivationButton_19() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___subscribedActivationButton_19)); }
	inline int32_t get_subscribedActivationButton_19() const { return ___subscribedActivationButton_19; }
	inline int32_t* get_address_of_subscribedActivationButton_19() { return &___subscribedActivationButton_19; }
	inline void set_subscribedActivationButton_19(int32_t value)
	{
		___subscribedActivationButton_19 = value;
	}

	inline static int32_t get_offset_of_subscribedSelectionButton_20() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___subscribedSelectionButton_20)); }
	inline int32_t get_subscribedSelectionButton_20() const { return ___subscribedSelectionButton_20; }
	inline int32_t* get_address_of_subscribedSelectionButton_20() { return &___subscribedSelectionButton_20; }
	inline void set_subscribedSelectionButton_20(int32_t value)
	{
		___subscribedSelectionButton_20 = value;
	}

	inline static int32_t get_offset_of_currentSelectOnPress_21() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___currentSelectOnPress_21)); }
	inline bool get_currentSelectOnPress_21() const { return ___currentSelectOnPress_21; }
	inline bool* get_address_of_currentSelectOnPress_21() { return &___currentSelectOnPress_21; }
	inline void set_currentSelectOnPress_21(bool value)
	{
		___currentSelectOnPress_21 = value;
	}

	inline static int32_t get_offset_of_activateDelayTimer_22() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___activateDelayTimer_22)); }
	inline float get_activateDelayTimer_22() const { return ___activateDelayTimer_22; }
	inline float* get_address_of_activateDelayTimer_22() { return &___activateDelayTimer_22; }
	inline void set_activateDelayTimer_22(float value)
	{
		___activateDelayTimer_22 = value;
	}

	inline static int32_t get_offset_of_currentActivationState_23() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___currentActivationState_23)); }
	inline int32_t get_currentActivationState_23() const { return ___currentActivationState_23; }
	inline int32_t* get_address_of_currentActivationState_23() { return &___currentActivationState_23; }
	inline void set_currentActivationState_23(int32_t value)
	{
		___currentActivationState_23 = value;
	}

	inline static int32_t get_offset_of_willDeactivate_24() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___willDeactivate_24)); }
	inline bool get_willDeactivate_24() const { return ___willDeactivate_24; }
	inline bool* get_address_of_willDeactivate_24() { return &___willDeactivate_24; }
	inline void set_willDeactivate_24(bool value)
	{
		___willDeactivate_24 = value;
	}

	inline static int32_t get_offset_of_wasActivated_25() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___wasActivated_25)); }
	inline bool get_wasActivated_25() const { return ___wasActivated_25; }
	inline bool* get_address_of_wasActivated_25() { return &___wasActivated_25; }
	inline void set_wasActivated_25(bool value)
	{
		___wasActivated_25 = value;
	}

	inline static int32_t get_offset_of_controllerIndex_26() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___controllerIndex_26)); }
	inline uint32_t get_controllerIndex_26() const { return ___controllerIndex_26; }
	inline uint32_t* get_address_of_controllerIndex_26() { return &___controllerIndex_26; }
	inline void set_controllerIndex_26(uint32_t value)
	{
		___controllerIndex_26 = value;
	}

	inline static int32_t get_offset_of_pointerInteractableObject_27() { return static_cast<int32_t>(offsetof(VRTK_Pointer_t2647108841, ___pointerInteractableObject_27)); }
	inline VRTK_InteractableObject_t2604188111 * get_pointerInteractableObject_27() const { return ___pointerInteractableObject_27; }
	inline VRTK_InteractableObject_t2604188111 ** get_address_of_pointerInteractableObject_27() { return &___pointerInteractableObject_27; }
	inline void set_pointerInteractableObject_27(VRTK_InteractableObject_t2604188111 * value)
	{
		___pointerInteractableObject_27 = value;
		Il2CppCodeGenWriteBarrier(&___pointerInteractableObject_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
