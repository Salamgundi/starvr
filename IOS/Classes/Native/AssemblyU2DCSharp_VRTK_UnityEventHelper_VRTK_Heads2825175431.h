﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_HeadsetControllerAware
struct VRTK_HeadsetControllerAware_t1678000416;
// VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t642803444;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents
struct  VRTK_HeadsetControllerAware_UnityEvents_t2825175431  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_HeadsetControllerAware VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents::hca
	VRTK_HeadsetControllerAware_t1678000416 * ___hca_2;
	// VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents::OnControllerObscured
	UnityObjectEvent_t642803444 * ___OnControllerObscured_3;
	// VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents::OnControllerUnobscured
	UnityObjectEvent_t642803444 * ___OnControllerUnobscured_4;
	// VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents::OnControllerGlanceEnter
	UnityObjectEvent_t642803444 * ___OnControllerGlanceEnter_5;
	// VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_HeadsetControllerAware_UnityEvents::OnControllerGlanceExit
	UnityObjectEvent_t642803444 * ___OnControllerGlanceExit_6;

public:
	inline static int32_t get_offset_of_hca_2() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_UnityEvents_t2825175431, ___hca_2)); }
	inline VRTK_HeadsetControllerAware_t1678000416 * get_hca_2() const { return ___hca_2; }
	inline VRTK_HeadsetControllerAware_t1678000416 ** get_address_of_hca_2() { return &___hca_2; }
	inline void set_hca_2(VRTK_HeadsetControllerAware_t1678000416 * value)
	{
		___hca_2 = value;
		Il2CppCodeGenWriteBarrier(&___hca_2, value);
	}

	inline static int32_t get_offset_of_OnControllerObscured_3() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_UnityEvents_t2825175431, ___OnControllerObscured_3)); }
	inline UnityObjectEvent_t642803444 * get_OnControllerObscured_3() const { return ___OnControllerObscured_3; }
	inline UnityObjectEvent_t642803444 ** get_address_of_OnControllerObscured_3() { return &___OnControllerObscured_3; }
	inline void set_OnControllerObscured_3(UnityObjectEvent_t642803444 * value)
	{
		___OnControllerObscured_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerObscured_3, value);
	}

	inline static int32_t get_offset_of_OnControllerUnobscured_4() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_UnityEvents_t2825175431, ___OnControllerUnobscured_4)); }
	inline UnityObjectEvent_t642803444 * get_OnControllerUnobscured_4() const { return ___OnControllerUnobscured_4; }
	inline UnityObjectEvent_t642803444 ** get_address_of_OnControllerUnobscured_4() { return &___OnControllerUnobscured_4; }
	inline void set_OnControllerUnobscured_4(UnityObjectEvent_t642803444 * value)
	{
		___OnControllerUnobscured_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerUnobscured_4, value);
	}

	inline static int32_t get_offset_of_OnControllerGlanceEnter_5() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_UnityEvents_t2825175431, ___OnControllerGlanceEnter_5)); }
	inline UnityObjectEvent_t642803444 * get_OnControllerGlanceEnter_5() const { return ___OnControllerGlanceEnter_5; }
	inline UnityObjectEvent_t642803444 ** get_address_of_OnControllerGlanceEnter_5() { return &___OnControllerGlanceEnter_5; }
	inline void set_OnControllerGlanceEnter_5(UnityObjectEvent_t642803444 * value)
	{
		___OnControllerGlanceEnter_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerGlanceEnter_5, value);
	}

	inline static int32_t get_offset_of_OnControllerGlanceExit_6() { return static_cast<int32_t>(offsetof(VRTK_HeadsetControllerAware_UnityEvents_t2825175431, ___OnControllerGlanceExit_6)); }
	inline UnityObjectEvent_t642803444 * get_OnControllerGlanceExit_6() const { return ___OnControllerGlanceExit_6; }
	inline UnityObjectEvent_t642803444 ** get_address_of_OnControllerGlanceExit_6() { return &___OnControllerGlanceExit_6; }
	inline void set_OnControllerGlanceExit_6(UnityObjectEvent_t642803444 * value)
	{
		___OnControllerGlanceExit_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnControllerGlanceExit_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
