﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.Examples.Archery.BowAim
struct BowAim_t1262532443;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Archery.ArrowSpawner
struct  ArrowSpawner_t3691485253  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject VRTK.Examples.Archery.ArrowSpawner::arrowPrefab
	GameObject_t1756533147 * ___arrowPrefab_2;
	// System.Single VRTK.Examples.Archery.ArrowSpawner::spawnDelay
	float ___spawnDelay_3;
	// System.Single VRTK.Examples.Archery.ArrowSpawner::spawnDelayTimer
	float ___spawnDelayTimer_4;
	// VRTK.Examples.Archery.BowAim VRTK.Examples.Archery.ArrowSpawner::bow
	BowAim_t1262532443 * ___bow_5;

public:
	inline static int32_t get_offset_of_arrowPrefab_2() { return static_cast<int32_t>(offsetof(ArrowSpawner_t3691485253, ___arrowPrefab_2)); }
	inline GameObject_t1756533147 * get_arrowPrefab_2() const { return ___arrowPrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_arrowPrefab_2() { return &___arrowPrefab_2; }
	inline void set_arrowPrefab_2(GameObject_t1756533147 * value)
	{
		___arrowPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___arrowPrefab_2, value);
	}

	inline static int32_t get_offset_of_spawnDelay_3() { return static_cast<int32_t>(offsetof(ArrowSpawner_t3691485253, ___spawnDelay_3)); }
	inline float get_spawnDelay_3() const { return ___spawnDelay_3; }
	inline float* get_address_of_spawnDelay_3() { return &___spawnDelay_3; }
	inline void set_spawnDelay_3(float value)
	{
		___spawnDelay_3 = value;
	}

	inline static int32_t get_offset_of_spawnDelayTimer_4() { return static_cast<int32_t>(offsetof(ArrowSpawner_t3691485253, ___spawnDelayTimer_4)); }
	inline float get_spawnDelayTimer_4() const { return ___spawnDelayTimer_4; }
	inline float* get_address_of_spawnDelayTimer_4() { return &___spawnDelayTimer_4; }
	inline void set_spawnDelayTimer_4(float value)
	{
		___spawnDelayTimer_4 = value;
	}

	inline static int32_t get_offset_of_bow_5() { return static_cast<int32_t>(offsetof(ArrowSpawner_t3691485253, ___bow_5)); }
	inline BowAim_t1262532443 * get_bow_5() const { return ___bow_5; }
	inline BowAim_t1262532443 ** get_address_of_bow_5() { return &___bow_5; }
	inline void set_bow_5(BowAim_t1262532443 * value)
	{
		___bow_5 = value;
		Il2CppCodeGenWriteBarrier(&___bow_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
