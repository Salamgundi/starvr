﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.RadialMenu
struct RadialMenu_t1576296262;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.RadialMenu/<TweenMenuScale>c__Iterator0
struct  U3CTweenMenuScaleU3Ec__Iterator0_t701686089  : public Il2CppObject
{
public:
	// System.Single VRTK.RadialMenu/<TweenMenuScale>c__Iterator0::<targetScale>__0
	float ___U3CtargetScaleU3E__0_0;
	// UnityEngine.Vector3 VRTK.RadialMenu/<TweenMenuScale>c__Iterator0::<Dir>__1
	Vector3_t2243707580  ___U3CDirU3E__1_1;
	// System.Boolean VRTK.RadialMenu/<TweenMenuScale>c__Iterator0::show
	bool ___show_2;
	// System.Int32 VRTK.RadialMenu/<TweenMenuScale>c__Iterator0::<i>__2
	int32_t ___U3CiU3E__2_3;
	// VRTK.RadialMenu VRTK.RadialMenu/<TweenMenuScale>c__Iterator0::$this
	RadialMenu_t1576296262 * ___U24this_4;
	// System.Object VRTK.RadialMenu/<TweenMenuScale>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean VRTK.RadialMenu/<TweenMenuScale>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 VRTK.RadialMenu/<TweenMenuScale>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CtargetScaleU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTweenMenuScaleU3Ec__Iterator0_t701686089, ___U3CtargetScaleU3E__0_0)); }
	inline float get_U3CtargetScaleU3E__0_0() const { return ___U3CtargetScaleU3E__0_0; }
	inline float* get_address_of_U3CtargetScaleU3E__0_0() { return &___U3CtargetScaleU3E__0_0; }
	inline void set_U3CtargetScaleU3E__0_0(float value)
	{
		___U3CtargetScaleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CDirU3E__1_1() { return static_cast<int32_t>(offsetof(U3CTweenMenuScaleU3Ec__Iterator0_t701686089, ___U3CDirU3E__1_1)); }
	inline Vector3_t2243707580  get_U3CDirU3E__1_1() const { return ___U3CDirU3E__1_1; }
	inline Vector3_t2243707580 * get_address_of_U3CDirU3E__1_1() { return &___U3CDirU3E__1_1; }
	inline void set_U3CDirU3E__1_1(Vector3_t2243707580  value)
	{
		___U3CDirU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_show_2() { return static_cast<int32_t>(offsetof(U3CTweenMenuScaleU3Ec__Iterator0_t701686089, ___show_2)); }
	inline bool get_show_2() const { return ___show_2; }
	inline bool* get_address_of_show_2() { return &___show_2; }
	inline void set_show_2(bool value)
	{
		___show_2 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__2_3() { return static_cast<int32_t>(offsetof(U3CTweenMenuScaleU3Ec__Iterator0_t701686089, ___U3CiU3E__2_3)); }
	inline int32_t get_U3CiU3E__2_3() const { return ___U3CiU3E__2_3; }
	inline int32_t* get_address_of_U3CiU3E__2_3() { return &___U3CiU3E__2_3; }
	inline void set_U3CiU3E__2_3(int32_t value)
	{
		___U3CiU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CTweenMenuScaleU3Ec__Iterator0_t701686089, ___U24this_4)); }
	inline RadialMenu_t1576296262 * get_U24this_4() const { return ___U24this_4; }
	inline RadialMenu_t1576296262 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(RadialMenu_t1576296262 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CTweenMenuScaleU3Ec__Iterator0_t701686089, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CTweenMenuScaleU3Ec__Iterator0_t701686089, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CTweenMenuScaleU3Ec__Iterator0_t701686089, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
