﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.ControllerButtonHints
struct ControllerButtonHints_t4025449936;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.ControllerButtonHints/<TestTextHints>c__Iterator1
struct  U3CTestTextHintsU3Ec__Iterator1_t1805603699  : public Il2CppObject
{
public:
	// Valve.VR.InteractionSystem.ControllerButtonHints Valve.VR.InteractionSystem.ControllerButtonHints/<TestTextHints>c__Iterator1::$this
	ControllerButtonHints_t4025449936 * ___U24this_0;
	// System.Object Valve.VR.InteractionSystem.ControllerButtonHints/<TestTextHints>c__Iterator1::$current
	Il2CppObject * ___U24current_1;
	// System.Boolean Valve.VR.InteractionSystem.ControllerButtonHints/<TestTextHints>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Valve.VR.InteractionSystem.ControllerButtonHints/<TestTextHints>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTestTextHintsU3Ec__Iterator1_t1805603699, ___U24this_0)); }
	inline ControllerButtonHints_t4025449936 * get_U24this_0() const { return ___U24this_0; }
	inline ControllerButtonHints_t4025449936 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ControllerButtonHints_t4025449936 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_0, value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTestTextHintsU3Ec__Iterator1_t1805603699, ___U24current_1)); }
	inline Il2CppObject * get_U24current_1() const { return ___U24current_1; }
	inline Il2CppObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(Il2CppObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_1, value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTestTextHintsU3Ec__Iterator1_t1805603699, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTestTextHintsU3Ec__Iterator1_t1805603699, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
