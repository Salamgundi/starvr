﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ControllerButtonHints
struct ControllerButtonHints_t4025449936;
// System.String
struct String_t;
// SteamVR_RenderModel
struct SteamVR_RenderModel_t2905485978;
// Valve.VR.EVRButtonId[]
struct EVRButtonIdU5BU5D_t985660653;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo
struct ButtonHintInfo_t106135473;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_SteamVR_RenderModel2905485978.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRButtonId66145412.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Contro106135473.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"

// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::.ctor()
extern "C"  void ControllerButtonHints__ctor_m3566862146 (ControllerButtonHints_t4025449936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.ControllerButtonHints::get_initialized()
extern "C"  bool ControllerButtonHints_get_initialized_m1690574881 (ControllerButtonHints_t4025449936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::set_initialized(System.Boolean)
extern "C"  void ControllerButtonHints_set_initialized_m1170473794 (ControllerButtonHints_t4025449936 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::Awake()
extern "C"  void ControllerButtonHints_Awake_m2940263621 (ControllerButtonHints_t4025449936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::Start()
extern "C"  void ControllerButtonHints_Start_m1336740722 (ControllerButtonHints_t4025449936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::HintDebugLog(System.String)
extern "C"  void ControllerButtonHints_HintDebugLog_m4082251644 (ControllerButtonHints_t4025449936 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::OnEnable()
extern "C"  void ControllerButtonHints_OnEnable_m1038539090 (ControllerButtonHints_t4025449936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::OnDisable()
extern "C"  void ControllerButtonHints_OnDisable_m407963725 (ControllerButtonHints_t4025449936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::OnParentHandInputFocusLost()
extern "C"  void ControllerButtonHints_OnParentHandInputFocusLost_m3249345086 (ControllerButtonHints_t4025449936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::OnHandInitialized(System.Int32)
extern "C"  void ControllerButtonHints_OnHandInitialized_m3797855787 (ControllerButtonHints_t4025449936 * __this, int32_t ___deviceIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::OnRenderModelLoaded(SteamVR_RenderModel,System.Boolean)
extern "C"  void ControllerButtonHints_OnRenderModelLoaded_m2654523294 (ControllerButtonHints_t4025449936 * __this, SteamVR_RenderModel_t2905485978 * ___renderModel0, bool ___succeess1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::CreateAndAddButtonInfo(Valve.VR.EVRButtonId)
extern "C"  void ControllerButtonHints_CreateAndAddButtonInfo_m1077064092 (ControllerButtonHints_t4025449936 * __this, int32_t ___buttonID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::ComputeTextEndTransforms()
extern "C"  void ControllerButtonHints_ComputeTextEndTransforms_m2007054758 (ControllerButtonHints_t4025449936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::ShowButtonHint(Valve.VR.EVRButtonId[])
extern "C"  void ControllerButtonHints_ShowButtonHint_m1396074672 (ControllerButtonHints_t4025449936 * __this, EVRButtonIdU5BU5D_t985660653* ___buttons0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::HideAllButtonHints()
extern "C"  void ControllerButtonHints_HideAllButtonHints_m1456199619 (ControllerButtonHints_t4025449936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::HideButtonHint(Valve.VR.EVRButtonId[])
extern "C"  void ControllerButtonHints_HideButtonHint_m429335125 (ControllerButtonHints_t4025449936 * __this, EVRButtonIdU5BU5D_t985660653* ___buttons0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.ControllerButtonHints::IsButtonHintActive(Valve.VR.EVRButtonId)
extern "C"  bool ControllerButtonHints_IsButtonHintActive_m2591612825 (ControllerButtonHints_t4025449936 * __this, int32_t ___button0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.ControllerButtonHints::TestButtonHints()
extern "C"  Il2CppObject * ControllerButtonHints_TestButtonHints_m1513754404 (ControllerButtonHints_t4025449936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.ControllerButtonHints::TestTextHints()
extern "C"  Il2CppObject * ControllerButtonHints_TestTextHints_m1624369365 (ControllerButtonHints_t4025449936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::Update()
extern "C"  void ControllerButtonHints_Update_m3390041129 (ControllerButtonHints_t4025449936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::UpdateTextHint(Valve.VR.InteractionSystem.ControllerButtonHints/ButtonHintInfo)
extern "C"  void ControllerButtonHints_UpdateTextHint_m1330877860 (ControllerButtonHints_t4025449936 * __this, ButtonHintInfo_t106135473 * ___hintInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::Clear()
extern "C"  void ControllerButtonHints_Clear_m2912579809 (ControllerButtonHints_t4025449936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::ShowText(Valve.VR.EVRButtonId,System.String,System.Boolean)
extern "C"  void ControllerButtonHints_ShowText_m3425131801 (ControllerButtonHints_t4025449936 * __this, int32_t ___button0, String_t* ___text1, bool ___highlightButton2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::HideText(Valve.VR.EVRButtonId)
extern "C"  void ControllerButtonHints_HideText_m2641257135 (ControllerButtonHints_t4025449936 * __this, int32_t ___button0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::HideAllText()
extern "C"  void ControllerButtonHints_HideAllText_m3153212490 (ControllerButtonHints_t4025449936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.InteractionSystem.ControllerButtonHints::GetActiveHintText(Valve.VR.EVRButtonId)
extern "C"  String_t* ControllerButtonHints_GetActiveHintText_m1287075127 (ControllerButtonHints_t4025449936 * __this, int32_t ___button0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.InteractionSystem.ControllerButtonHints Valve.VR.InteractionSystem.ControllerButtonHints::GetControllerButtonHints(Valve.VR.InteractionSystem.Hand)
extern "C"  ControllerButtonHints_t4025449936 * ControllerButtonHints_GetControllerButtonHints_m1010644652 (Il2CppObject * __this /* static, unused */, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::ShowButtonHint(Valve.VR.InteractionSystem.Hand,Valve.VR.EVRButtonId[])
extern "C"  void ControllerButtonHints_ShowButtonHint_m1498121550 (Il2CppObject * __this /* static, unused */, Hand_t379716353 * ___hand0, EVRButtonIdU5BU5D_t985660653* ___buttons1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::HideButtonHint(Valve.VR.InteractionSystem.Hand,Valve.VR.EVRButtonId[])
extern "C"  void ControllerButtonHints_HideButtonHint_m432886355 (Il2CppObject * __this /* static, unused */, Hand_t379716353 * ___hand0, EVRButtonIdU5BU5D_t985660653* ___buttons1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::HideAllButtonHints(Valve.VR.InteractionSystem.Hand)
extern "C"  void ControllerButtonHints_HideAllButtonHints_m1172818845 (Il2CppObject * __this /* static, unused */, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.ControllerButtonHints::IsButtonHintActive(Valve.VR.InteractionSystem.Hand,Valve.VR.EVRButtonId)
extern "C"  bool ControllerButtonHints_IsButtonHintActive_m1746263887 (Il2CppObject * __this /* static, unused */, Hand_t379716353 * ___hand0, int32_t ___button1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::ShowTextHint(Valve.VR.InteractionSystem.Hand,Valve.VR.EVRButtonId,System.String,System.Boolean)
extern "C"  void ControllerButtonHints_ShowTextHint_m2524928140 (Il2CppObject * __this /* static, unused */, Hand_t379716353 * ___hand0, int32_t ___button1, String_t* ___text2, bool ___highlightButton3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::HideTextHint(Valve.VR.InteractionSystem.Hand,Valve.VR.EVRButtonId)
extern "C"  void ControllerButtonHints_HideTextHint_m744084396 (Il2CppObject * __this /* static, unused */, Hand_t379716353 * ___hand0, int32_t ___button1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints::HideAllTextHints(Valve.VR.InteractionSystem.Hand)
extern "C"  void ControllerButtonHints_HideAllTextHints_m746325662 (Il2CppObject * __this /* static, unused */, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.InteractionSystem.ControllerButtonHints::GetActiveHintText(Valve.VR.InteractionSystem.Hand,Valve.VR.EVRButtonId)
extern "C"  String_t* ControllerButtonHints_GetActiveHintText_m142647361 (Il2CppObject * __this /* static, unused */, Hand_t379716353 * ___hand0, int32_t ___button1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
