﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_IsQuitUserPromptRequested
struct _IsQuitUserPromptRequested_t1816118422;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_IsQuitUserPromptRequested::.ctor(System.Object,System.IntPtr)
extern "C"  void _IsQuitUserPromptRequested__ctor_m1650408789 (_IsQuitUserPromptRequested_t1816118422 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRApplications/_IsQuitUserPromptRequested::Invoke()
extern "C"  bool _IsQuitUserPromptRequested_Invoke_m460685809 (_IsQuitUserPromptRequested_t1816118422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_IsQuitUserPromptRequested::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _IsQuitUserPromptRequested_BeginInvoke_m1733066820 (_IsQuitUserPromptRequested_t1816118422 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRApplications/_IsQuitUserPromptRequested::EndInvoke(System.IAsyncResult)
extern "C"  bool _IsQuitUserPromptRequested_EndInvoke_m2344725383 (_IsQuitUserPromptRequested_t1816118422 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
