﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t1515633077;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.PanelMenu.PanelMenuUIGrid
struct  PanelMenuUIGrid_t729840129  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color VRTK.Examples.PanelMenu.PanelMenuUIGrid::colorDefault
	Color_t2020392075  ___colorDefault_2;
	// UnityEngine.Color VRTK.Examples.PanelMenu.PanelMenuUIGrid::colorSelected
	Color_t2020392075  ___colorSelected_3;
	// System.Single VRTK.Examples.PanelMenu.PanelMenuUIGrid::colorAlpha
	float ___colorAlpha_4;
	// UnityEngine.UI.GridLayoutGroup VRTK.Examples.PanelMenu.PanelMenuUIGrid::gridLayoutGroup
	GridLayoutGroup_t1515633077 * ___gridLayoutGroup_5;
	// System.Int32 VRTK.Examples.PanelMenu.PanelMenuUIGrid::selectedIndex
	int32_t ___selectedIndex_6;

public:
	inline static int32_t get_offset_of_colorDefault_2() { return static_cast<int32_t>(offsetof(PanelMenuUIGrid_t729840129, ___colorDefault_2)); }
	inline Color_t2020392075  get_colorDefault_2() const { return ___colorDefault_2; }
	inline Color_t2020392075 * get_address_of_colorDefault_2() { return &___colorDefault_2; }
	inline void set_colorDefault_2(Color_t2020392075  value)
	{
		___colorDefault_2 = value;
	}

	inline static int32_t get_offset_of_colorSelected_3() { return static_cast<int32_t>(offsetof(PanelMenuUIGrid_t729840129, ___colorSelected_3)); }
	inline Color_t2020392075  get_colorSelected_3() const { return ___colorSelected_3; }
	inline Color_t2020392075 * get_address_of_colorSelected_3() { return &___colorSelected_3; }
	inline void set_colorSelected_3(Color_t2020392075  value)
	{
		___colorSelected_3 = value;
	}

	inline static int32_t get_offset_of_colorAlpha_4() { return static_cast<int32_t>(offsetof(PanelMenuUIGrid_t729840129, ___colorAlpha_4)); }
	inline float get_colorAlpha_4() const { return ___colorAlpha_4; }
	inline float* get_address_of_colorAlpha_4() { return &___colorAlpha_4; }
	inline void set_colorAlpha_4(float value)
	{
		___colorAlpha_4 = value;
	}

	inline static int32_t get_offset_of_gridLayoutGroup_5() { return static_cast<int32_t>(offsetof(PanelMenuUIGrid_t729840129, ___gridLayoutGroup_5)); }
	inline GridLayoutGroup_t1515633077 * get_gridLayoutGroup_5() const { return ___gridLayoutGroup_5; }
	inline GridLayoutGroup_t1515633077 ** get_address_of_gridLayoutGroup_5() { return &___gridLayoutGroup_5; }
	inline void set_gridLayoutGroup_5(GridLayoutGroup_t1515633077 * value)
	{
		___gridLayoutGroup_5 = value;
		Il2CppCodeGenWriteBarrier(&___gridLayoutGroup_5, value);
	}

	inline static int32_t get_offset_of_selectedIndex_6() { return static_cast<int32_t>(offsetof(PanelMenuUIGrid_t729840129, ___selectedIndex_6)); }
	inline int32_t get_selectedIndex_6() const { return ___selectedIndex_6; }
	inline int32_t* get_address_of_selectedIndex_6() { return &___selectedIndex_6; }
	inline void set_selectedIndex_6(int32_t value)
	{
		___selectedIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
