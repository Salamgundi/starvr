﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_InteractControllerAppearance
struct  VRTK_InteractControllerAppearance_t3909124886  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.VRTK_InteractControllerAppearance::hideControllerOnTouch
	bool ___hideControllerOnTouch_2;
	// System.Single VRTK.VRTK_InteractControllerAppearance::hideDelayOnTouch
	float ___hideDelayOnTouch_3;
	// System.Boolean VRTK.VRTK_InteractControllerAppearance::hideControllerOnGrab
	bool ___hideControllerOnGrab_4;
	// System.Single VRTK.VRTK_InteractControllerAppearance::hideDelayOnGrab
	float ___hideDelayOnGrab_5;
	// System.Boolean VRTK.VRTK_InteractControllerAppearance::hideControllerOnUse
	bool ___hideControllerOnUse_6;
	// System.Single VRTK.VRTK_InteractControllerAppearance::hideDelayOnUse
	float ___hideDelayOnUse_7;
	// VRTK.VRTK_ControllerActions VRTK.VRTK_InteractControllerAppearance::storedControllerActions
	VRTK_ControllerActions_t3642353851 * ___storedControllerActions_8;
	// UnityEngine.GameObject VRTK.VRTK_InteractControllerAppearance::storedCurrentObject
	GameObject_t1756533147 * ___storedCurrentObject_9;
	// System.Boolean VRTK.VRTK_InteractControllerAppearance::touchControllerShow
	bool ___touchControllerShow_10;
	// System.Boolean VRTK.VRTK_InteractControllerAppearance::grabControllerShow
	bool ___grabControllerShow_11;

public:
	inline static int32_t get_offset_of_hideControllerOnTouch_2() { return static_cast<int32_t>(offsetof(VRTK_InteractControllerAppearance_t3909124886, ___hideControllerOnTouch_2)); }
	inline bool get_hideControllerOnTouch_2() const { return ___hideControllerOnTouch_2; }
	inline bool* get_address_of_hideControllerOnTouch_2() { return &___hideControllerOnTouch_2; }
	inline void set_hideControllerOnTouch_2(bool value)
	{
		___hideControllerOnTouch_2 = value;
	}

	inline static int32_t get_offset_of_hideDelayOnTouch_3() { return static_cast<int32_t>(offsetof(VRTK_InteractControllerAppearance_t3909124886, ___hideDelayOnTouch_3)); }
	inline float get_hideDelayOnTouch_3() const { return ___hideDelayOnTouch_3; }
	inline float* get_address_of_hideDelayOnTouch_3() { return &___hideDelayOnTouch_3; }
	inline void set_hideDelayOnTouch_3(float value)
	{
		___hideDelayOnTouch_3 = value;
	}

	inline static int32_t get_offset_of_hideControllerOnGrab_4() { return static_cast<int32_t>(offsetof(VRTK_InteractControllerAppearance_t3909124886, ___hideControllerOnGrab_4)); }
	inline bool get_hideControllerOnGrab_4() const { return ___hideControllerOnGrab_4; }
	inline bool* get_address_of_hideControllerOnGrab_4() { return &___hideControllerOnGrab_4; }
	inline void set_hideControllerOnGrab_4(bool value)
	{
		___hideControllerOnGrab_4 = value;
	}

	inline static int32_t get_offset_of_hideDelayOnGrab_5() { return static_cast<int32_t>(offsetof(VRTK_InteractControllerAppearance_t3909124886, ___hideDelayOnGrab_5)); }
	inline float get_hideDelayOnGrab_5() const { return ___hideDelayOnGrab_5; }
	inline float* get_address_of_hideDelayOnGrab_5() { return &___hideDelayOnGrab_5; }
	inline void set_hideDelayOnGrab_5(float value)
	{
		___hideDelayOnGrab_5 = value;
	}

	inline static int32_t get_offset_of_hideControllerOnUse_6() { return static_cast<int32_t>(offsetof(VRTK_InteractControllerAppearance_t3909124886, ___hideControllerOnUse_6)); }
	inline bool get_hideControllerOnUse_6() const { return ___hideControllerOnUse_6; }
	inline bool* get_address_of_hideControllerOnUse_6() { return &___hideControllerOnUse_6; }
	inline void set_hideControllerOnUse_6(bool value)
	{
		___hideControllerOnUse_6 = value;
	}

	inline static int32_t get_offset_of_hideDelayOnUse_7() { return static_cast<int32_t>(offsetof(VRTK_InteractControllerAppearance_t3909124886, ___hideDelayOnUse_7)); }
	inline float get_hideDelayOnUse_7() const { return ___hideDelayOnUse_7; }
	inline float* get_address_of_hideDelayOnUse_7() { return &___hideDelayOnUse_7; }
	inline void set_hideDelayOnUse_7(float value)
	{
		___hideDelayOnUse_7 = value;
	}

	inline static int32_t get_offset_of_storedControllerActions_8() { return static_cast<int32_t>(offsetof(VRTK_InteractControllerAppearance_t3909124886, ___storedControllerActions_8)); }
	inline VRTK_ControllerActions_t3642353851 * get_storedControllerActions_8() const { return ___storedControllerActions_8; }
	inline VRTK_ControllerActions_t3642353851 ** get_address_of_storedControllerActions_8() { return &___storedControllerActions_8; }
	inline void set_storedControllerActions_8(VRTK_ControllerActions_t3642353851 * value)
	{
		___storedControllerActions_8 = value;
		Il2CppCodeGenWriteBarrier(&___storedControllerActions_8, value);
	}

	inline static int32_t get_offset_of_storedCurrentObject_9() { return static_cast<int32_t>(offsetof(VRTK_InteractControllerAppearance_t3909124886, ___storedCurrentObject_9)); }
	inline GameObject_t1756533147 * get_storedCurrentObject_9() const { return ___storedCurrentObject_9; }
	inline GameObject_t1756533147 ** get_address_of_storedCurrentObject_9() { return &___storedCurrentObject_9; }
	inline void set_storedCurrentObject_9(GameObject_t1756533147 * value)
	{
		___storedCurrentObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___storedCurrentObject_9, value);
	}

	inline static int32_t get_offset_of_touchControllerShow_10() { return static_cast<int32_t>(offsetof(VRTK_InteractControllerAppearance_t3909124886, ___touchControllerShow_10)); }
	inline bool get_touchControllerShow_10() const { return ___touchControllerShow_10; }
	inline bool* get_address_of_touchControllerShow_10() { return &___touchControllerShow_10; }
	inline void set_touchControllerShow_10(bool value)
	{
		___touchControllerShow_10 = value;
	}

	inline static int32_t get_offset_of_grabControllerShow_11() { return static_cast<int32_t>(offsetof(VRTK_InteractControllerAppearance_t3909124886, ___grabControllerShow_11)); }
	inline bool get_grabControllerShow_11() const { return ___grabControllerShow_11; }
	inline bool* get_address_of_grabControllerShow_11() { return &___grabControllerShow_11; }
	inline void set_grabControllerShow_11(bool value)
	{
		___grabControllerShow_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
