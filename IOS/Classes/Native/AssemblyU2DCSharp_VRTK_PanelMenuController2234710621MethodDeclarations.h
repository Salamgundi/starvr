﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.PanelMenuController
struct PanelMenuController_t2234710621;
// VRTK.PanelMenuItemController
struct PanelMenuItemController_t3837844790;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_PanelMenuItemController3837844790.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"
#include "AssemblyU2DCSharp_VRTK_PanelMenuController_Touchpa1242655431.h"

// System.Void VRTK.PanelMenuController::.ctor()
extern "C"  void PanelMenuController__ctor_m1112741379 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::Awake()
extern "C"  void PanelMenuController_Awake_m483445462 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::Start()
extern "C"  void PanelMenuController_Start_m4086919175 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::Update()
extern "C"  void PanelMenuController_Update_m20562132 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::Initialize()
extern "C"  void PanelMenuController_Initialize_m3523200543 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::ToggleMenu()
extern "C"  void PanelMenuController_ToggleMenu_m1774905854 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::ShowMenu()
extern "C"  void PanelMenuController_ShowMenu_m2356878359 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::HideMenu(System.Boolean)
extern "C"  void PanelMenuController_HideMenu_m2001410827 (PanelMenuController_t2234710621 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::HideMenuImmediate()
extern "C"  void PanelMenuController_HideMenuImmediate_m83830417 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::BindControllerEvents()
extern "C"  void PanelMenuController_BindControllerEvents_m3176002753 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::UnbindControllerEvents()
extern "C"  void PanelMenuController_UnbindControllerEvents_m3155965796 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::HandlePanelMenuItemControllerVisibility(VRTK.PanelMenuItemController)
extern "C"  void PanelMenuController_HandlePanelMenuItemControllerVisibility_m4083895310 (PanelMenuController_t2234710621 * __this, PanelMenuItemController_t3837844790 * ___targetPanelItemController0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.PanelMenuController::TweenMenuScale(System.Boolean)
extern "C"  Il2CppObject * PanelMenuController_TweenMenuScale_m776862594 (PanelMenuController_t2234710621 * __this, bool ___show0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::DoInteractableObjectIsGrabbed(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void PanelMenuController_DoInteractableObjectIsGrabbed_m2172468399 (PanelMenuController_t2234710621 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::DoInteractableObjectIsUngrabbed(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void PanelMenuController_DoInteractableObjectIsUngrabbed_m131890956 (PanelMenuController_t2234710621 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::DoTouchpadPress(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void PanelMenuController_DoTouchpadPress_m601873889 (PanelMenuController_t2234710621 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::DoTouchpadTouched(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void PanelMenuController_DoTouchpadTouched_m1269914112 (PanelMenuController_t2234710621 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::DoTouchpadUntouched(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void PanelMenuController_DoTouchpadUntouched_m4031657685 (PanelMenuController_t2234710621 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::DoTouchpadAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void PanelMenuController_DoTouchpadAxisChanged_m2048573015 (PanelMenuController_t2234710621 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::DoTriggerPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void PanelMenuController_DoTriggerPressed_m3647555018 (PanelMenuController_t2234710621 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::ChangeAngle(System.Single,System.Object)
extern "C"  void PanelMenuController_ChangeAngle_m1277861783 (PanelMenuController_t2234710621 * __this, float ___angle0, Il2CppObject * ___sender1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::CalculateSwipeAction()
extern "C"  void PanelMenuController_CalculateSwipeAction_m2034283341 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.PanelMenuController/TouchpadPressPosition VRTK.PanelMenuController::CalculateTouchpadPressPosition()
extern "C"  int32_t PanelMenuController_CalculateTouchpadPressPosition_m726721407 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::OnSwipeLeft()
extern "C"  void PanelMenuController_OnSwipeLeft_m559062269 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::OnSwipeRight()
extern "C"  void PanelMenuController_OnSwipeRight_m327968652 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::OnSwipeTop()
extern "C"  void PanelMenuController_OnSwipeTop_m1011919405 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::OnSwipeBottom()
extern "C"  void PanelMenuController_OnSwipeBottom_m2756286009 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuController::OnTriggerPressed()
extern "C"  void PanelMenuController_OnTriggerPressed_m800641470 (PanelMenuController_t2234710621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.PanelMenuController::CalculateAngle(VRTK.ControllerInteractionEventArgs)
extern "C"  float PanelMenuController_CalculateAngle_m3144528306 (PanelMenuController_t2234710621 * __this, ControllerInteractionEventArgs_t287637539  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.PanelMenuController::NormAngle(System.Single,System.Single)
extern "C"  float PanelMenuController_NormAngle_m3214164720 (PanelMenuController_t2234710621 * __this, float ___currentDegree0, float ___maxAngle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.PanelMenuController::CheckAnglePosition(System.Single,System.Single,System.Single)
extern "C"  bool PanelMenuController_CheckAnglePosition_m2229919180 (PanelMenuController_t2234710621 * __this, float ___currentDegree0, float ___tolerance1, float ___targetDegree2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
