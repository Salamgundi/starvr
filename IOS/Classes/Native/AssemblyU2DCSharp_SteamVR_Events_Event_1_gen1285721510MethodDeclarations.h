﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Event`1<Valve.VR.VREvent_t>
struct Event_1_t1285721510;
// UnityEngine.Events.UnityAction`1<Valve.VR.VREvent_t>
struct UnityAction_1_t476884844;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_VREvent_t3405266389.h"

// System.Void SteamVR_Events/Event`1<Valve.VR.VREvent_t>::.ctor()
extern "C"  void Event_1__ctor_m2157519104_gshared (Event_1_t1285721510 * __this, const MethodInfo* method);
#define Event_1__ctor_m2157519104(__this, method) ((  void (*) (Event_1_t1285721510 *, const MethodInfo*))Event_1__ctor_m2157519104_gshared)(__this, method)
// System.Void SteamVR_Events/Event`1<Valve.VR.VREvent_t>::Listen(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Listen_m2330954003_gshared (Event_1_t1285721510 * __this, UnityAction_1_t476884844 * ___action0, const MethodInfo* method);
#define Event_1_Listen_m2330954003(__this, ___action0, method) ((  void (*) (Event_1_t1285721510 *, UnityAction_1_t476884844 *, const MethodInfo*))Event_1_Listen_m2330954003_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`1<Valve.VR.VREvent_t>::Remove(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void Event_1_Remove_m2607860930_gshared (Event_1_t1285721510 * __this, UnityAction_1_t476884844 * ___action0, const MethodInfo* method);
#define Event_1_Remove_m2607860930(__this, ___action0, method) ((  void (*) (Event_1_t1285721510 *, UnityAction_1_t476884844 *, const MethodInfo*))Event_1_Remove_m2607860930_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`1<Valve.VR.VREvent_t>::Send(T)
extern "C"  void Event_1_Send_m2783568422_gshared (Event_1_t1285721510 * __this, VREvent_t_t3405266389  ___arg00, const MethodInfo* method);
#define Event_1_Send_m2783568422(__this, ___arg00, method) ((  void (*) (Event_1_t1285721510 *, VREvent_t_t3405266389 , const MethodInfo*))Event_1_Send_m2783568422_gshared)(__this, ___arg00, method)
