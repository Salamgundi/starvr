﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Mesh
struct Mesh_t1356156583;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_CombineInstance64595210.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

// System.Void UnityEngine.CombineInstance::set_mesh(UnityEngine.Mesh)
extern "C"  void CombineInstance_set_mesh_m1001542527 (CombineInstance_t64595210 * __this, Mesh_t1356156583 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CombineInstance::set_subMeshIndex(System.Int32)
extern "C"  void CombineInstance_set_subMeshIndex_m3633535514 (CombineInstance_t64595210 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CombineInstance::set_transform(UnityEngine.Matrix4x4)
extern "C"  void CombineInstance_set_transform_m3327542376 (CombineInstance_t64595210 * __this, Matrix4x4_t2933234003  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
