﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DemoInputManager
struct DemoInputManager_t2776755480;

#include "codegen/il2cpp-codegen.h"

// System.Void DemoInputManager::.ctor()
extern "C"  void DemoInputManager__ctor_m9619453 (DemoInputManager_t2776755480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoInputManager::Start()
extern "C"  void DemoInputManager_Start_m50976037 (DemoInputManager_t2776755480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
