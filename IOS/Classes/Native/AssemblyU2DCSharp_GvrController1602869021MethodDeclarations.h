﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrController
struct GvrController_t1602869021;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrController::.ctor()
extern "C"  void GvrController__ctor_m2877647192 (GvrController_t1602869021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
