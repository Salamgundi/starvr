﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_OculusVRBoundaries
struct SDK_OculusVRBoundaries_t3279427364;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_OculusVRBoundaries::.ctor()
extern "C"  void SDK_OculusVRBoundaries__ctor_m4088516496 (SDK_OculusVRBoundaries_t3279427364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
