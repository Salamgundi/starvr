﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveBlock
struct MoveBlock_t1905298658;

#include "codegen/il2cpp-codegen.h"

// System.Void MoveBlock::.ctor()
extern "C"  void MoveBlock__ctor_m1296222771 (MoveBlock_t1905298658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveBlock::Start()
extern "C"  void MoveBlock_Start_m2218104999 (MoveBlock_t1905298658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveBlock::Update()
extern "C"  void MoveBlock_Update_m518191440 (MoveBlock_t1905298658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
