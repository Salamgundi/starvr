﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Ba1229653768.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.GrabAttachMechanics.VRTK_FixedJointGrabAttach
struct  VRTK_FixedJointGrabAttach_t3523806925  : public VRTK_BaseJointGrabAttach_t1229653768
{
public:
	// System.Single VRTK.GrabAttachMechanics.VRTK_FixedJointGrabAttach::breakForce
	float ___breakForce_21;

public:
	inline static int32_t get_offset_of_breakForce_21() { return static_cast<int32_t>(offsetof(VRTK_FixedJointGrabAttach_t3523806925, ___breakForce_21)); }
	inline float get_breakForce_21() const { return ___breakForce_21; }
	inline float* get_address_of_breakForce_21() { return &___breakForce_21; }
	inline void set_breakForce_21(float value)
	{
		___breakForce_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
