﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_DestinationPoint/<DoDestinationMarkerSetAtEndOfFrame>c__Iterator1
struct U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_DestinationPoint/<DoDestinationMarkerSetAtEndOfFrame>c__Iterator1::.ctor()
extern "C"  void U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1__ctor_m3895291578 (U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_DestinationPoint/<DoDestinationMarkerSetAtEndOfFrame>c__Iterator1::MoveNext()
extern "C"  bool U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_MoveNext_m3148861538 (U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_DestinationPoint/<DoDestinationMarkerSetAtEndOfFrame>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1136099028 (U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_DestinationPoint/<DoDestinationMarkerSetAtEndOfFrame>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1852791020 (U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint/<DoDestinationMarkerSetAtEndOfFrame>c__Iterator1::Dispose()
extern "C"  void U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_Dispose_m3855705397 (U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint/<DoDestinationMarkerSetAtEndOfFrame>c__Iterator1::Reset()
extern "C"  void U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_Reset_m3725530343 (U3CDoDestinationMarkerSetAtEndOfFrameU3Ec__Iterator1_t3301794915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
