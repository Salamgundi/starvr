﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// Valve.VR.InteractionSystem.Longbow
struct Longbow_t2607500110;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// Valve.VR.InteractionSystem.SoundPlayOneshot
struct SoundPlayOneshot_t1703214483;
// Valve.VR.InteractionSystem.AllowTeleportWhileAttachedToHand
struct AllowTeleportWhileAttachedToHand_t416616227;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.ArrowHand
struct  ArrowHand_t565341420  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.ArrowHand::hand
	Hand_t379716353 * ___hand_2;
	// Valve.VR.InteractionSystem.Longbow Valve.VR.InteractionSystem.ArrowHand::bow
	Longbow_t2607500110 * ___bow_3;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.ArrowHand::currentArrow
	GameObject_t1756533147 * ___currentArrow_4;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.ArrowHand::arrowPrefab
	GameObject_t1756533147 * ___arrowPrefab_5;
	// UnityEngine.Transform Valve.VR.InteractionSystem.ArrowHand::arrowNockTransform
	Transform_t3275118058 * ___arrowNockTransform_6;
	// System.Single Valve.VR.InteractionSystem.ArrowHand::nockDistance
	float ___nockDistance_7;
	// System.Single Valve.VR.InteractionSystem.ArrowHand::lerpCompleteDistance
	float ___lerpCompleteDistance_8;
	// System.Single Valve.VR.InteractionSystem.ArrowHand::rotationLerpThreshold
	float ___rotationLerpThreshold_9;
	// System.Single Valve.VR.InteractionSystem.ArrowHand::positionLerpThreshold
	float ___positionLerpThreshold_10;
	// System.Boolean Valve.VR.InteractionSystem.ArrowHand::allowArrowSpawn
	bool ___allowArrowSpawn_11;
	// System.Boolean Valve.VR.InteractionSystem.ArrowHand::nocked
	bool ___nocked_12;
	// System.Boolean Valve.VR.InteractionSystem.ArrowHand::inNockRange
	bool ___inNockRange_13;
	// System.Boolean Valve.VR.InteractionSystem.ArrowHand::arrowLerpComplete
	bool ___arrowLerpComplete_14;
	// Valve.VR.InteractionSystem.SoundPlayOneshot Valve.VR.InteractionSystem.ArrowHand::arrowSpawnSound
	SoundPlayOneshot_t1703214483 * ___arrowSpawnSound_15;
	// Valve.VR.InteractionSystem.AllowTeleportWhileAttachedToHand Valve.VR.InteractionSystem.ArrowHand::allowTeleport
	AllowTeleportWhileAttachedToHand_t416616227 * ___allowTeleport_16;
	// System.Int32 Valve.VR.InteractionSystem.ArrowHand::maxArrowCount
	int32_t ___maxArrowCount_17;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Valve.VR.InteractionSystem.ArrowHand::arrowList
	List_1_t1125654279 * ___arrowList_18;

public:
	inline static int32_t get_offset_of_hand_2() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___hand_2)); }
	inline Hand_t379716353 * get_hand_2() const { return ___hand_2; }
	inline Hand_t379716353 ** get_address_of_hand_2() { return &___hand_2; }
	inline void set_hand_2(Hand_t379716353 * value)
	{
		___hand_2 = value;
		Il2CppCodeGenWriteBarrier(&___hand_2, value);
	}

	inline static int32_t get_offset_of_bow_3() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___bow_3)); }
	inline Longbow_t2607500110 * get_bow_3() const { return ___bow_3; }
	inline Longbow_t2607500110 ** get_address_of_bow_3() { return &___bow_3; }
	inline void set_bow_3(Longbow_t2607500110 * value)
	{
		___bow_3 = value;
		Il2CppCodeGenWriteBarrier(&___bow_3, value);
	}

	inline static int32_t get_offset_of_currentArrow_4() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___currentArrow_4)); }
	inline GameObject_t1756533147 * get_currentArrow_4() const { return ___currentArrow_4; }
	inline GameObject_t1756533147 ** get_address_of_currentArrow_4() { return &___currentArrow_4; }
	inline void set_currentArrow_4(GameObject_t1756533147 * value)
	{
		___currentArrow_4 = value;
		Il2CppCodeGenWriteBarrier(&___currentArrow_4, value);
	}

	inline static int32_t get_offset_of_arrowPrefab_5() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___arrowPrefab_5)); }
	inline GameObject_t1756533147 * get_arrowPrefab_5() const { return ___arrowPrefab_5; }
	inline GameObject_t1756533147 ** get_address_of_arrowPrefab_5() { return &___arrowPrefab_5; }
	inline void set_arrowPrefab_5(GameObject_t1756533147 * value)
	{
		___arrowPrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___arrowPrefab_5, value);
	}

	inline static int32_t get_offset_of_arrowNockTransform_6() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___arrowNockTransform_6)); }
	inline Transform_t3275118058 * get_arrowNockTransform_6() const { return ___arrowNockTransform_6; }
	inline Transform_t3275118058 ** get_address_of_arrowNockTransform_6() { return &___arrowNockTransform_6; }
	inline void set_arrowNockTransform_6(Transform_t3275118058 * value)
	{
		___arrowNockTransform_6 = value;
		Il2CppCodeGenWriteBarrier(&___arrowNockTransform_6, value);
	}

	inline static int32_t get_offset_of_nockDistance_7() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___nockDistance_7)); }
	inline float get_nockDistance_7() const { return ___nockDistance_7; }
	inline float* get_address_of_nockDistance_7() { return &___nockDistance_7; }
	inline void set_nockDistance_7(float value)
	{
		___nockDistance_7 = value;
	}

	inline static int32_t get_offset_of_lerpCompleteDistance_8() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___lerpCompleteDistance_8)); }
	inline float get_lerpCompleteDistance_8() const { return ___lerpCompleteDistance_8; }
	inline float* get_address_of_lerpCompleteDistance_8() { return &___lerpCompleteDistance_8; }
	inline void set_lerpCompleteDistance_8(float value)
	{
		___lerpCompleteDistance_8 = value;
	}

	inline static int32_t get_offset_of_rotationLerpThreshold_9() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___rotationLerpThreshold_9)); }
	inline float get_rotationLerpThreshold_9() const { return ___rotationLerpThreshold_9; }
	inline float* get_address_of_rotationLerpThreshold_9() { return &___rotationLerpThreshold_9; }
	inline void set_rotationLerpThreshold_9(float value)
	{
		___rotationLerpThreshold_9 = value;
	}

	inline static int32_t get_offset_of_positionLerpThreshold_10() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___positionLerpThreshold_10)); }
	inline float get_positionLerpThreshold_10() const { return ___positionLerpThreshold_10; }
	inline float* get_address_of_positionLerpThreshold_10() { return &___positionLerpThreshold_10; }
	inline void set_positionLerpThreshold_10(float value)
	{
		___positionLerpThreshold_10 = value;
	}

	inline static int32_t get_offset_of_allowArrowSpawn_11() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___allowArrowSpawn_11)); }
	inline bool get_allowArrowSpawn_11() const { return ___allowArrowSpawn_11; }
	inline bool* get_address_of_allowArrowSpawn_11() { return &___allowArrowSpawn_11; }
	inline void set_allowArrowSpawn_11(bool value)
	{
		___allowArrowSpawn_11 = value;
	}

	inline static int32_t get_offset_of_nocked_12() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___nocked_12)); }
	inline bool get_nocked_12() const { return ___nocked_12; }
	inline bool* get_address_of_nocked_12() { return &___nocked_12; }
	inline void set_nocked_12(bool value)
	{
		___nocked_12 = value;
	}

	inline static int32_t get_offset_of_inNockRange_13() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___inNockRange_13)); }
	inline bool get_inNockRange_13() const { return ___inNockRange_13; }
	inline bool* get_address_of_inNockRange_13() { return &___inNockRange_13; }
	inline void set_inNockRange_13(bool value)
	{
		___inNockRange_13 = value;
	}

	inline static int32_t get_offset_of_arrowLerpComplete_14() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___arrowLerpComplete_14)); }
	inline bool get_arrowLerpComplete_14() const { return ___arrowLerpComplete_14; }
	inline bool* get_address_of_arrowLerpComplete_14() { return &___arrowLerpComplete_14; }
	inline void set_arrowLerpComplete_14(bool value)
	{
		___arrowLerpComplete_14 = value;
	}

	inline static int32_t get_offset_of_arrowSpawnSound_15() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___arrowSpawnSound_15)); }
	inline SoundPlayOneshot_t1703214483 * get_arrowSpawnSound_15() const { return ___arrowSpawnSound_15; }
	inline SoundPlayOneshot_t1703214483 ** get_address_of_arrowSpawnSound_15() { return &___arrowSpawnSound_15; }
	inline void set_arrowSpawnSound_15(SoundPlayOneshot_t1703214483 * value)
	{
		___arrowSpawnSound_15 = value;
		Il2CppCodeGenWriteBarrier(&___arrowSpawnSound_15, value);
	}

	inline static int32_t get_offset_of_allowTeleport_16() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___allowTeleport_16)); }
	inline AllowTeleportWhileAttachedToHand_t416616227 * get_allowTeleport_16() const { return ___allowTeleport_16; }
	inline AllowTeleportWhileAttachedToHand_t416616227 ** get_address_of_allowTeleport_16() { return &___allowTeleport_16; }
	inline void set_allowTeleport_16(AllowTeleportWhileAttachedToHand_t416616227 * value)
	{
		___allowTeleport_16 = value;
		Il2CppCodeGenWriteBarrier(&___allowTeleport_16, value);
	}

	inline static int32_t get_offset_of_maxArrowCount_17() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___maxArrowCount_17)); }
	inline int32_t get_maxArrowCount_17() const { return ___maxArrowCount_17; }
	inline int32_t* get_address_of_maxArrowCount_17() { return &___maxArrowCount_17; }
	inline void set_maxArrowCount_17(int32_t value)
	{
		___maxArrowCount_17 = value;
	}

	inline static int32_t get_offset_of_arrowList_18() { return static_cast<int32_t>(offsetof(ArrowHand_t565341420, ___arrowList_18)); }
	inline List_1_t1125654279 * get_arrowList_18() const { return ___arrowList_18; }
	inline List_1_t1125654279 ** get_address_of_arrowList_18() { return &___arrowList_18; }
	inline void set_arrowList_18(List_1_t1125654279 * value)
	{
		___arrowList_18 = value;
		Il2CppCodeGenWriteBarrier(&___arrowList_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
