﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.DestinationMarkerEventArgs
struct DestinationMarkerEventArgs_t1852451661;
struct DestinationMarkerEventArgs_t1852451661_marshaled_pinvoke;
struct DestinationMarkerEventArgs_t1852451661_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct DestinationMarkerEventArgs_t1852451661;
struct DestinationMarkerEventArgs_t1852451661_marshaled_pinvoke;

extern "C" void DestinationMarkerEventArgs_t1852451661_marshal_pinvoke(const DestinationMarkerEventArgs_t1852451661& unmarshaled, DestinationMarkerEventArgs_t1852451661_marshaled_pinvoke& marshaled);
extern "C" void DestinationMarkerEventArgs_t1852451661_marshal_pinvoke_back(const DestinationMarkerEventArgs_t1852451661_marshaled_pinvoke& marshaled, DestinationMarkerEventArgs_t1852451661& unmarshaled);
extern "C" void DestinationMarkerEventArgs_t1852451661_marshal_pinvoke_cleanup(DestinationMarkerEventArgs_t1852451661_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct DestinationMarkerEventArgs_t1852451661;
struct DestinationMarkerEventArgs_t1852451661_marshaled_com;

extern "C" void DestinationMarkerEventArgs_t1852451661_marshal_com(const DestinationMarkerEventArgs_t1852451661& unmarshaled, DestinationMarkerEventArgs_t1852451661_marshaled_com& marshaled);
extern "C" void DestinationMarkerEventArgs_t1852451661_marshal_com_back(const DestinationMarkerEventArgs_t1852451661_marshaled_com& marshaled, DestinationMarkerEventArgs_t1852451661& unmarshaled);
extern "C" void DestinationMarkerEventArgs_t1852451661_marshal_com_cleanup(DestinationMarkerEventArgs_t1852451661_marshaled_com& marshaled);
