﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.IgnoreHovering
struct IgnoreHovering_t476670048;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.IgnoreHovering::.ctor()
extern "C"  void IgnoreHovering__ctor_m3558180438 (IgnoreHovering_t476670048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
