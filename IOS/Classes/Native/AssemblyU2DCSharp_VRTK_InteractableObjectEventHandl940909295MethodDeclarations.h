﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.InteractableObjectEventHandler
struct InteractableObjectEventHandler_t940909295;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.InteractableObjectEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void InteractableObjectEventHandler__ctor_m1866744359 (InteractableObjectEventHandler_t940909295 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.InteractableObjectEventHandler::Invoke(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void InteractableObjectEventHandler_Invoke_m2205757494 (InteractableObjectEventHandler_t940909295 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.InteractableObjectEventHandler::BeginInvoke(System.Object,VRTK.InteractableObjectEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * InteractableObjectEventHandler_BeginInvoke_m2044505309 (InteractableObjectEventHandler_t940909295 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.InteractableObjectEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void InteractableObjectEventHandler_EndInvoke_m3647590729 (InteractableObjectEventHandler_t940909295 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
