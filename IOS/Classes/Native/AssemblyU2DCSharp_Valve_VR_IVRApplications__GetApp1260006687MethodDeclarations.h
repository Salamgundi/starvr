﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetApplicationsThatSupportMimeType
struct _GetApplicationsThatSupportMimeType_t1260006687;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetApplicationsThatSupportMimeType::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetApplicationsThatSupportMimeType__ctor_m1919410580 (_GetApplicationsThatSupportMimeType_t1260006687 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRApplications/_GetApplicationsThatSupportMimeType::Invoke(System.String,System.String,System.UInt32)
extern "C"  uint32_t _GetApplicationsThatSupportMimeType_Invoke_m3938362745 (_GetApplicationsThatSupportMimeType_t1260006687 * __this, String_t* ___pchMimeType0, String_t* ___pchAppKeysThatSupportBuffer1, uint32_t ___unAppKeysThatSupportBuffer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetApplicationsThatSupportMimeType::BeginInvoke(System.String,System.String,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetApplicationsThatSupportMimeType_BeginInvoke_m4076395113 (_GetApplicationsThatSupportMimeType_t1260006687 * __this, String_t* ___pchMimeType0, String_t* ___pchAppKeysThatSupportBuffer1, uint32_t ___unAppKeysThatSupportBuffer2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRApplications/_GetApplicationsThatSupportMimeType::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetApplicationsThatSupportMimeType_EndInvoke_m2360039487 (_GetApplicationsThatSupportMimeType_t1260006687 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
