﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Event`2<System.Object,System.Boolean>
struct Event_2_t175250481;
// UnityEngine.Events.UnityAction`2<System.Object,System.Boolean>
struct UnityAction_2_t626063409;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void SteamVR_Events/Event`2<System.Object,System.Boolean>::.ctor()
extern "C"  void Event_2__ctor_m1718189359_gshared (Event_2_t175250481 * __this, const MethodInfo* method);
#define Event_2__ctor_m1718189359(__this, method) ((  void (*) (Event_2_t175250481 *, const MethodInfo*))Event_2__ctor_m1718189359_gshared)(__this, method)
// System.Void SteamVR_Events/Event`2<System.Object,System.Boolean>::Listen(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Event_2_Listen_m1492392574_gshared (Event_2_t175250481 * __this, UnityAction_2_t626063409 * ___action0, const MethodInfo* method);
#define Event_2_Listen_m1492392574(__this, ___action0, method) ((  void (*) (Event_2_t175250481 *, UnityAction_2_t626063409 *, const MethodInfo*))Event_2_Listen_m1492392574_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`2<System.Object,System.Boolean>::Remove(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void Event_2_Remove_m760931903_gshared (Event_2_t175250481 * __this, UnityAction_2_t626063409 * ___action0, const MethodInfo* method);
#define Event_2_Remove_m760931903(__this, ___action0, method) ((  void (*) (Event_2_t175250481 *, UnityAction_2_t626063409 *, const MethodInfo*))Event_2_Remove_m760931903_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`2<System.Object,System.Boolean>::Send(T0,T1)
extern "C"  void Event_2_Send_m2170767364_gshared (Event_2_t175250481 * __this, Il2CppObject * ___arg00, bool ___arg11, const MethodInfo* method);
#define Event_2_Send_m2170767364(__this, ___arg00, ___arg11, method) ((  void (*) (Event_2_t175250481 *, Il2CppObject *, bool, const MethodInfo*))Event_2_Send_m2170767364_gshared)(__this, ___arg00, ___arg11, method)
