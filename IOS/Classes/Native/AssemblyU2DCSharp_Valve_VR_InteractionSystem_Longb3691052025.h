﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.Longbow
struct Longbow_t2607500110;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.Longbow/<ResetDrawAnim>c__Iterator0
struct  U3CResetDrawAnimU3Ec__Iterator0_t3691052025  : public Il2CppObject
{
public:
	// System.Single Valve.VR.InteractionSystem.Longbow/<ResetDrawAnim>c__Iterator0::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Single Valve.VR.InteractionSystem.Longbow/<ResetDrawAnim>c__Iterator0::<startLerp>__1
	float ___U3CstartLerpU3E__1_1;
	// System.Single Valve.VR.InteractionSystem.Longbow/<ResetDrawAnim>c__Iterator0::<lerp>__2
	float ___U3ClerpU3E__2_2;
	// Valve.VR.InteractionSystem.Longbow Valve.VR.InteractionSystem.Longbow/<ResetDrawAnim>c__Iterator0::$this
	Longbow_t2607500110 * ___U24this_3;
	// System.Object Valve.VR.InteractionSystem.Longbow/<ResetDrawAnim>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean Valve.VR.InteractionSystem.Longbow/<ResetDrawAnim>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Valve.VR.InteractionSystem.Longbow/<ResetDrawAnim>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CResetDrawAnimU3Ec__Iterator0_t3691052025, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartLerpU3E__1_1() { return static_cast<int32_t>(offsetof(U3CResetDrawAnimU3Ec__Iterator0_t3691052025, ___U3CstartLerpU3E__1_1)); }
	inline float get_U3CstartLerpU3E__1_1() const { return ___U3CstartLerpU3E__1_1; }
	inline float* get_address_of_U3CstartLerpU3E__1_1() { return &___U3CstartLerpU3E__1_1; }
	inline void set_U3CstartLerpU3E__1_1(float value)
	{
		___U3CstartLerpU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3ClerpU3E__2_2() { return static_cast<int32_t>(offsetof(U3CResetDrawAnimU3Ec__Iterator0_t3691052025, ___U3ClerpU3E__2_2)); }
	inline float get_U3ClerpU3E__2_2() const { return ___U3ClerpU3E__2_2; }
	inline float* get_address_of_U3ClerpU3E__2_2() { return &___U3ClerpU3E__2_2; }
	inline void set_U3ClerpU3E__2_2(float value)
	{
		___U3ClerpU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CResetDrawAnimU3Ec__Iterator0_t3691052025, ___U24this_3)); }
	inline Longbow_t2607500110 * get_U24this_3() const { return ___U24this_3; }
	inline Longbow_t2607500110 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(Longbow_t2607500110 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CResetDrawAnimU3Ec__Iterator0_t3691052025, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CResetDrawAnimU3Ec__Iterator0_t3691052025, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CResetDrawAnimU3Ec__Iterator0_t3691052025, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
