﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_UIGraphicRaycaster
struct VRTK_UIGraphicRaycaster_t2816944648;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Camera
struct Camera_t189460977;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"

// System.Void VRTK.VRTK_UIGraphicRaycaster::.ctor()
extern "C"  void VRTK_UIGraphicRaycaster__ctor_m2535130454 (VRTK_UIGraphicRaycaster_t2816944648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIGraphicRaycaster::Raycast(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  void VRTK_UIGraphicRaycaster_Raycast_m1162287854 (VRTK_UIGraphicRaycaster_t2816944648 * __this, PointerEventData_t1599784723 * ___eventData0, List_1_t3685274804 * ___resultAppendList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIGraphicRaycaster::SetNearestRaycast(UnityEngine.EventSystems.PointerEventData&,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>&,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>&)
extern "C"  void VRTK_UIGraphicRaycaster_SetNearestRaycast_m4292847853 (VRTK_UIGraphicRaycaster_t2816944648 * __this, PointerEventData_t1599784723 ** ___eventData0, List_1_t3685274804 ** ___resultAppendList1, List_1_t3685274804 ** ___raycastResults2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_UIGraphicRaycaster::GetHitDistance(UnityEngine.Ray)
extern "C"  float VRTK_UIGraphicRaycaster_GetHitDistance_m874130475 (VRTK_UIGraphicRaycaster_t2816944648 * __this, Ray_t2469606224  ___ray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIGraphicRaycaster::Raycast(UnityEngine.Canvas,UnityEngine.Camera,UnityEngine.Ray,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>&)
extern "C"  void VRTK_UIGraphicRaycaster_Raycast_m2870623256 (VRTK_UIGraphicRaycaster_t2816944648 * __this, Canvas_t209405766 * ___canvas0, Camera_t189460977 * ___eventCamera1, Ray_t2469606224  ___ray2, List_1_t3685274804 ** ___results3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Canvas VRTK.VRTK_UIGraphicRaycaster::get_canvas()
extern "C"  Canvas_t209405766 * VRTK_UIGraphicRaycaster_get_canvas_m1893935089 (VRTK_UIGraphicRaycaster_t2816944648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIGraphicRaycaster::.cctor()
extern "C"  void VRTK_UIGraphicRaycaster__cctor_m2721502627 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VRTK.VRTK_UIGraphicRaycaster::<Raycast>m__0(UnityEngine.EventSystems.RaycastResult,UnityEngine.EventSystems.RaycastResult)
extern "C"  int32_t VRTK_UIGraphicRaycaster_U3CRaycastU3Em__0_m2793431176 (Il2CppObject * __this /* static, unused */, RaycastResult_t21186376  ___g10, RaycastResult_t21186376  ___g21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
