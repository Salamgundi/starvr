﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_DriverDebugRequest
struct _DriverDebugRequest_t4049208724;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_DriverDebugRequest::.ctor(System.Object,System.IntPtr)
extern "C"  void _DriverDebugRequest__ctor_m1763855545 (_DriverDebugRequest_t4049208724 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRSystem/_DriverDebugRequest::Invoke(System.UInt32,System.String,System.String,System.UInt32)
extern "C"  uint32_t _DriverDebugRequest_Invoke_m1499682732 (_DriverDebugRequest_t4049208724 * __this, uint32_t ___unDeviceIndex0, String_t* ___pchRequest1, String_t* ___pchResponseBuffer2, uint32_t ___unResponseBufferSize3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_DriverDebugRequest::BeginInvoke(System.UInt32,System.String,System.String,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _DriverDebugRequest_BeginInvoke_m1239116954 (_DriverDebugRequest_t4049208724 * __this, uint32_t ___unDeviceIndex0, String_t* ___pchRequest1, String_t* ___pchResponseBuffer2, uint32_t ___unResponseBufferSize3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRSystem/_DriverDebugRequest::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _DriverDebugRequest_EndInvoke_m1383865042 (_DriverDebugRequest_t4049208724 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
