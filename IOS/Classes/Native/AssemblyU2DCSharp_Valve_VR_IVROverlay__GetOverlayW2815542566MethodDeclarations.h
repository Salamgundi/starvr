﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayWidthInMeters
struct _GetOverlayWidthInMeters_t2815542566;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayWidthInMeters::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayWidthInMeters__ctor_m1403353801 (_GetOverlayWidthInMeters_t2815542566 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayWidthInMeters::Invoke(System.UInt64,System.Single&)
extern "C"  int32_t _GetOverlayWidthInMeters_Invoke_m917396673 (_GetOverlayWidthInMeters_t2815542566 * __this, uint64_t ___ulOverlayHandle0, float* ___pfWidthInMeters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayWidthInMeters::BeginInvoke(System.UInt64,System.Single&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayWidthInMeters_BeginInvoke_m4094678342 (_GetOverlayWidthInMeters_t2815542566 * __this, uint64_t ___ulOverlayHandle0, float* ___pfWidthInMeters1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayWidthInMeters::EndInvoke(System.Single&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayWidthInMeters_EndInvoke_m2514085574 (_GetOverlayWidthInMeters_t2815542566 * __this, float* ___pfWidthInMeters0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
