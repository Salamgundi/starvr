﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraSunController
struct CameraSunController_t3090970055;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraSunController::.ctor()
extern "C"  void CameraSunController__ctor_m612063658 (CameraSunController_t3090970055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSunController::PlanetOn()
extern "C"  void CameraSunController_PlanetOn_m3443176157 (CameraSunController_t3090970055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
