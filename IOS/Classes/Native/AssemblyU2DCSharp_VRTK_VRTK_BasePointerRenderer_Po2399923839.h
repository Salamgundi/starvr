﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_BasePointerRenderer/PointerOriginSmoothingSettings
struct  PointerOriginSmoothingSettings_t2399923839  : public Il2CppObject
{
public:
	// System.Boolean VRTK.VRTK_BasePointerRenderer/PointerOriginSmoothingSettings::smoothsPosition
	bool ___smoothsPosition_0;
	// System.Single VRTK.VRTK_BasePointerRenderer/PointerOriginSmoothingSettings::maxAllowedPerFrameDistanceDifference
	float ___maxAllowedPerFrameDistanceDifference_1;
	// System.Boolean VRTK.VRTK_BasePointerRenderer/PointerOriginSmoothingSettings::smoothsRotation
	bool ___smoothsRotation_2;
	// System.Single VRTK.VRTK_BasePointerRenderer/PointerOriginSmoothingSettings::maxAllowedPerFrameAngleDifference
	float ___maxAllowedPerFrameAngleDifference_3;

public:
	inline static int32_t get_offset_of_smoothsPosition_0() { return static_cast<int32_t>(offsetof(PointerOriginSmoothingSettings_t2399923839, ___smoothsPosition_0)); }
	inline bool get_smoothsPosition_0() const { return ___smoothsPosition_0; }
	inline bool* get_address_of_smoothsPosition_0() { return &___smoothsPosition_0; }
	inline void set_smoothsPosition_0(bool value)
	{
		___smoothsPosition_0 = value;
	}

	inline static int32_t get_offset_of_maxAllowedPerFrameDistanceDifference_1() { return static_cast<int32_t>(offsetof(PointerOriginSmoothingSettings_t2399923839, ___maxAllowedPerFrameDistanceDifference_1)); }
	inline float get_maxAllowedPerFrameDistanceDifference_1() const { return ___maxAllowedPerFrameDistanceDifference_1; }
	inline float* get_address_of_maxAllowedPerFrameDistanceDifference_1() { return &___maxAllowedPerFrameDistanceDifference_1; }
	inline void set_maxAllowedPerFrameDistanceDifference_1(float value)
	{
		___maxAllowedPerFrameDistanceDifference_1 = value;
	}

	inline static int32_t get_offset_of_smoothsRotation_2() { return static_cast<int32_t>(offsetof(PointerOriginSmoothingSettings_t2399923839, ___smoothsRotation_2)); }
	inline bool get_smoothsRotation_2() const { return ___smoothsRotation_2; }
	inline bool* get_address_of_smoothsRotation_2() { return &___smoothsRotation_2; }
	inline void set_smoothsRotation_2(bool value)
	{
		___smoothsRotation_2 = value;
	}

	inline static int32_t get_offset_of_maxAllowedPerFrameAngleDifference_3() { return static_cast<int32_t>(offsetof(PointerOriginSmoothingSettings_t2399923839, ___maxAllowedPerFrameAngleDifference_3)); }
	inline float get_maxAllowedPerFrameAngleDifference_3() const { return ___maxAllowedPerFrameAngleDifference_3; }
	inline float* get_address_of_maxAllowedPerFrameAngleDifference_3() { return &___maxAllowedPerFrameAngleDifference_3; }
	inline void set_maxAllowedPerFrameAngleDifference_3(float value)
	{
		___maxAllowedPerFrameAngleDifference_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
