﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.CombineInstance>
struct DefaultComparer_t1686525371;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_CombineInstance64595210.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.CombineInstance>::.ctor()
extern "C"  void DefaultComparer__ctor_m4148352283_gshared (DefaultComparer_t1686525371 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4148352283(__this, method) ((  void (*) (DefaultComparer_t1686525371 *, const MethodInfo*))DefaultComparer__ctor_m4148352283_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.CombineInstance>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m129638490_gshared (DefaultComparer_t1686525371 * __this, CombineInstance_t64595210  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m129638490(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1686525371 *, CombineInstance_t64595210 , const MethodInfo*))DefaultComparer_GetHashCode_m129638490_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.CombineInstance>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1114221642_gshared (DefaultComparer_t1686525371 * __this, CombineInstance_t64595210  ___x0, CombineInstance_t64595210  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1114221642(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1686525371 *, CombineInstance_t64595210 , CombineInstance_t64595210 , const MethodInfo*))DefaultComparer_Equals_m1114221642_gshared)(__this, ___x0, ___y1, method)
