﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVRButtonId,System.Object>
struct ShimEnumerator_t4052628059;
// System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>
struct Dictionary_2_t3947503238;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVRButtonId,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1491705048_gshared (ShimEnumerator_t4052628059 * __this, Dictionary_2_t3947503238 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1491705048(__this, ___host0, method) ((  void (*) (ShimEnumerator_t4052628059 *, Dictionary_2_t3947503238 *, const MethodInfo*))ShimEnumerator__ctor_m1491705048_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVRButtonId,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m551340849_gshared (ShimEnumerator_t4052628059 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m551340849(__this, method) ((  bool (*) (ShimEnumerator_t4052628059 *, const MethodInfo*))ShimEnumerator_MoveNext_m551340849_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVRButtonId,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2868315565_gshared (ShimEnumerator_t4052628059 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2868315565(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t4052628059 *, const MethodInfo*))ShimEnumerator_get_Entry_m2868315565_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVRButtonId,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m182481544_gshared (ShimEnumerator_t4052628059 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m182481544(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4052628059 *, const MethodInfo*))ShimEnumerator_get_Key_m182481544_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVRButtonId,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m727707370_gshared (ShimEnumerator_t4052628059 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m727707370(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4052628059 *, const MethodInfo*))ShimEnumerator_get_Value_m727707370_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVRButtonId,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2106109202_gshared (ShimEnumerator_t4052628059 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2106109202(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4052628059 *, const MethodInfo*))ShimEnumerator_get_Current_m2106109202_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Valve.VR.EVRButtonId,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1793120398_gshared (ShimEnumerator_t4052628059 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1793120398(__this, method) ((  void (*) (ShimEnumerator_t4052628059 *, const MethodInfo*))ShimEnumerator_Reset_m1793120398_gshared)(__this, method)
