﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Throwable
struct Throwable_t3270371398;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"

// System.Void Valve.VR.InteractionSystem.Throwable::.ctor()
extern "C"  void Throwable__ctor_m1962979524 (Throwable_t3270371398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Throwable::Awake()
extern "C"  void Throwable_Awake_m3848776647 (Throwable_t3270371398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Throwable::OnHandHoverBegin(Valve.VR.InteractionSystem.Hand)
extern "C"  void Throwable_OnHandHoverBegin_m4228596051 (Throwable_t3270371398 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Throwable::OnHandHoverEnd(Valve.VR.InteractionSystem.Hand)
extern "C"  void Throwable_OnHandHoverEnd_m994422835 (Throwable_t3270371398 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Throwable::HandHoverUpdate(Valve.VR.InteractionSystem.Hand)
extern "C"  void Throwable_HandHoverUpdate_m2441904756 (Throwable_t3270371398 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Throwable::OnAttachedToHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void Throwable_OnAttachedToHand_m3416476567 (Throwable_t3270371398 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Throwable::OnDetachedFromHand(Valve.VR.InteractionSystem.Hand)
extern "C"  void Throwable_OnDetachedFromHand_m33801652 (Throwable_t3270371398 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Throwable::HandAttachedUpdate(Valve.VR.InteractionSystem.Hand)
extern "C"  void Throwable_HandAttachedUpdate_m4182968406 (Throwable_t3270371398 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.Throwable::LateDetach(Valve.VR.InteractionSystem.Hand)
extern "C"  Il2CppObject * Throwable_LateDetach_m2933851721 (Throwable_t3270371398 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Throwable::OnHandFocusAcquired(Valve.VR.InteractionSystem.Hand)
extern "C"  void Throwable_OnHandFocusAcquired_m27147140 (Throwable_t3270371398 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Throwable::OnHandFocusLost(Valve.VR.InteractionSystem.Hand)
extern "C"  void Throwable_OnHandFocusLost_m4189261422 (Throwable_t3270371398 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
