﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Render/<RenderLoop>c__Iterator0
struct U3CRenderLoopU3Ec__Iterator0_t3103906043;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_Render/<RenderLoop>c__Iterator0::.ctor()
extern "C"  void U3CRenderLoopU3Ec__Iterator0__ctor_m4225640688 (U3CRenderLoopU3Ec__Iterator0_t3103906043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Render/<RenderLoop>c__Iterator0::MoveNext()
extern "C"  bool U3CRenderLoopU3Ec__Iterator0_MoveNext_m2138927592 (U3CRenderLoopU3Ec__Iterator0_t3103906043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SteamVR_Render/<RenderLoop>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRenderLoopU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4141729174 (U3CRenderLoopU3Ec__Iterator0_t3103906043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SteamVR_Render/<RenderLoop>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRenderLoopU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2332019582 (U3CRenderLoopU3Ec__Iterator0_t3103906043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render/<RenderLoop>c__Iterator0::Dispose()
extern "C"  void U3CRenderLoopU3Ec__Iterator0_Dispose_m1505149269 (U3CRenderLoopU3Ec__Iterator0_t3103906043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Render/<RenderLoop>c__Iterator0::Reset()
extern "C"  void U3CRenderLoopU3Ec__Iterator0_Reset_m3183100195 (U3CRenderLoopU3Ec__Iterator0_t3103906043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
