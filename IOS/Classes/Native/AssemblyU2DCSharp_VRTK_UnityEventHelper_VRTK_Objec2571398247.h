﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_ObjectControl
struct VRTK_ObjectControl_t724022372;
// VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t4282528608;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents
struct  VRTK_ObjectControl_UnityEvents_t2571398247  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_ObjectControl VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents::oc
	VRTK_ObjectControl_t724022372 * ___oc_2;
	// VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents::OnXAxisChanged
	UnityObjectEvent_t4282528608 * ___OnXAxisChanged_3;
	// VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_ObjectControl_UnityEvents::OnYAxisChanged
	UnityObjectEvent_t4282528608 * ___OnYAxisChanged_4;

public:
	inline static int32_t get_offset_of_oc_2() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_UnityEvents_t2571398247, ___oc_2)); }
	inline VRTK_ObjectControl_t724022372 * get_oc_2() const { return ___oc_2; }
	inline VRTK_ObjectControl_t724022372 ** get_address_of_oc_2() { return &___oc_2; }
	inline void set_oc_2(VRTK_ObjectControl_t724022372 * value)
	{
		___oc_2 = value;
		Il2CppCodeGenWriteBarrier(&___oc_2, value);
	}

	inline static int32_t get_offset_of_OnXAxisChanged_3() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_UnityEvents_t2571398247, ___OnXAxisChanged_3)); }
	inline UnityObjectEvent_t4282528608 * get_OnXAxisChanged_3() const { return ___OnXAxisChanged_3; }
	inline UnityObjectEvent_t4282528608 ** get_address_of_OnXAxisChanged_3() { return &___OnXAxisChanged_3; }
	inline void set_OnXAxisChanged_3(UnityObjectEvent_t4282528608 * value)
	{
		___OnXAxisChanged_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnXAxisChanged_3, value);
	}

	inline static int32_t get_offset_of_OnYAxisChanged_4() { return static_cast<int32_t>(offsetof(VRTK_ObjectControl_UnityEvents_t2571398247, ___OnYAxisChanged_4)); }
	inline UnityObjectEvent_t4282528608 * get_OnYAxisChanged_4() const { return ___OnYAxisChanged_4; }
	inline UnityObjectEvent_t4282528608 ** get_address_of_OnYAxisChanged_4() { return &___OnYAxisChanged_4; }
	inline void set_OnYAxisChanged_4(UnityObjectEvent_t4282528608 * value)
	{
		___OnYAxisChanged_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnYAxisChanged_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
