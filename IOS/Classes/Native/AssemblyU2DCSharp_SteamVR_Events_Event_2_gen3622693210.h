﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen1660736760.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SteamVR_Events/Event`2<SteamVR_RenderModel,System.Boolean>
struct  Event_2_t3622693210  : public UnityEvent_2_t1660736760
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
