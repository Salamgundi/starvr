﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetRecommendedRenderTargetSize
struct _GetRecommendedRenderTargetSize_t4195542627;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetRecommendedRenderTargetSize::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetRecommendedRenderTargetSize__ctor_m2113485512 (_GetRecommendedRenderTargetSize_t4195542627 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_GetRecommendedRenderTargetSize::Invoke(System.UInt32&,System.UInt32&)
extern "C"  void _GetRecommendedRenderTargetSize_Invoke_m4106040300 (_GetRecommendedRenderTargetSize_t4195542627 * __this, uint32_t* ___pnWidth0, uint32_t* ___pnHeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetRecommendedRenderTargetSize::BeginInvoke(System.UInt32&,System.UInt32&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetRecommendedRenderTargetSize_BeginInvoke_m894391113 (_GetRecommendedRenderTargetSize_t4195542627 * __this, uint32_t* ___pnWidth0, uint32_t* ___pnHeight1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRSystem/_GetRecommendedRenderTargetSize::EndInvoke(System.UInt32&,System.UInt32&,System.IAsyncResult)
extern "C"  void _GetRecommendedRenderTargetSize_EndInvoke_m2432261258 (_GetRecommendedRenderTargetSize_t4195542627 * __this, uint32_t* ___pnWidth0, uint32_t* ___pnHeight1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
