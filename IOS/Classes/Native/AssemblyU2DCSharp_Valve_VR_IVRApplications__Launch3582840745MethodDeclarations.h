﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_LaunchDashboardOverlay
struct _LaunchDashboardOverlay_t3582840745;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_LaunchDashboardOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _LaunchDashboardOverlay__ctor_m2114403762 (_LaunchDashboardOverlay_t3582840745 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_LaunchDashboardOverlay::Invoke(System.String)
extern "C"  int32_t _LaunchDashboardOverlay_Invoke_m4254404398 (_LaunchDashboardOverlay_t3582840745 * __this, String_t* ___pchAppKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_LaunchDashboardOverlay::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _LaunchDashboardOverlay_BeginInvoke_m4230811601 (_LaunchDashboardOverlay_t3582840745 * __this, String_t* ___pchAppKey0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_LaunchDashboardOverlay::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _LaunchDashboardOverlay_EndInvoke_m1025135704 (_LaunchDashboardOverlay_t3582840745 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
