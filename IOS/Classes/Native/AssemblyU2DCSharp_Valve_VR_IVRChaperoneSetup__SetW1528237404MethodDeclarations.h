﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_SetWorkingStandingZeroPoseToRawTrackingPose
struct _SetWorkingStandingZeroPoseToRawTrackingPose_t1528237404;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingStandingZeroPoseToRawTrackingPose::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetWorkingStandingZeroPoseToRawTrackingPose__ctor_m1058824629 (_SetWorkingStandingZeroPoseToRawTrackingPose_t1528237404 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingStandingZeroPoseToRawTrackingPose::Invoke(Valve.VR.HmdMatrix34_t&)
extern "C"  void _SetWorkingStandingZeroPoseToRawTrackingPose_Invoke_m166490205 (_SetWorkingStandingZeroPoseToRawTrackingPose_t1528237404 * __this, HmdMatrix34_t_t664273062 * ___pMatStandingZeroPoseToRawTrackingPose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_SetWorkingStandingZeroPoseToRawTrackingPose::BeginInvoke(Valve.VR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetWorkingStandingZeroPoseToRawTrackingPose_BeginInvoke_m696706530 (_SetWorkingStandingZeroPoseToRawTrackingPose_t1528237404 * __this, HmdMatrix34_t_t664273062 * ___pMatStandingZeroPoseToRawTrackingPose0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingStandingZeroPoseToRawTrackingPose::EndInvoke(Valve.VR.HmdMatrix34_t&,System.IAsyncResult)
extern "C"  void _SetWorkingStandingZeroPoseToRawTrackingPose_EndInvoke_m2431654415 (_SetWorkingStandingZeroPoseToRawTrackingPose_t1528237404 * __this, HmdMatrix34_t_t664273062 * ___pMatStandingZeroPoseToRawTrackingPose0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
