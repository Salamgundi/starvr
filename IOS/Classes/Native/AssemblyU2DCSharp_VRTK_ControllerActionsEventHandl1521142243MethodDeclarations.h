﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.ControllerActionsEventHandler
struct ControllerActionsEventHandler_t1521142243;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_ControllerActionsEventArgs344001476.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.ControllerActionsEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ControllerActionsEventHandler__ctor_m1744438799 (ControllerActionsEventHandler_t1521142243 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.ControllerActionsEventHandler::Invoke(System.Object,VRTK.ControllerActionsEventArgs)
extern "C"  void ControllerActionsEventHandler_Invoke_m2276859790 (ControllerActionsEventHandler_t1521142243 * __this, Il2CppObject * ___sender0, ControllerActionsEventArgs_t344001476  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.ControllerActionsEventHandler::BeginInvoke(System.Object,VRTK.ControllerActionsEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ControllerActionsEventHandler_BeginInvoke_m3192889153 (ControllerActionsEventHandler_t1521142243 * __this, Il2CppObject * ___sender0, ControllerActionsEventArgs_t344001476  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.ControllerActionsEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ControllerActionsEventHandler_EndInvoke_m3732816429 (ControllerActionsEventHandler_t1521142243 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
