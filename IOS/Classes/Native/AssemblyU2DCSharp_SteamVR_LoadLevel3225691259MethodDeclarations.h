﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_LoadLevel
struct SteamVR_LoadLevel_t3225691259;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void SteamVR_LoadLevel::.ctor()
extern "C"  void SteamVR_LoadLevel__ctor_m2807315736 (SteamVR_LoadLevel_t3225691259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_LoadLevel::get_loading()
extern "C"  bool SteamVR_LoadLevel_get_loading_m480302837 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SteamVR_LoadLevel::get_progress()
extern "C"  float SteamVR_LoadLevel_get_progress_m1657697042 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture SteamVR_LoadLevel::get_progressTexture()
extern "C"  Texture_t2243626319 * SteamVR_LoadLevel_get_progressTexture_m4177178490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LoadLevel::OnEnable()
extern "C"  void SteamVR_LoadLevel_OnEnable_m1849773132 (SteamVR_LoadLevel_t3225691259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LoadLevel::Trigger()
extern "C"  void SteamVR_LoadLevel_Trigger_m2636946076 (SteamVR_LoadLevel_t3225691259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LoadLevel::Begin(System.String,System.Boolean,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void SteamVR_LoadLevel_Begin_m1567419445 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, bool ___showGrid1, float ___fadeOutTime2, float ___r3, float ___g4, float ___b5, float ___a6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LoadLevel::OnGUI()
extern "C"  void SteamVR_LoadLevel_OnGUI_m1214202872 (SteamVR_LoadLevel_t3225691259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LoadLevel::Update()
extern "C"  void SteamVR_LoadLevel_Update_m1439044675 (SteamVR_LoadLevel_t3225691259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SteamVR_LoadLevel::LoadLevel()
extern "C"  Il2CppObject * SteamVR_LoadLevel_LoadLevel_m1648243422 (SteamVR_LoadLevel_t3225691259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 SteamVR_LoadLevel::GetOverlayHandle(System.String,UnityEngine.Transform,System.Single)
extern "C"  uint64_t SteamVR_LoadLevel_GetOverlayHandle_m4294694094 (SteamVR_LoadLevel_t3225691259 * __this, String_t* ___overlayName0, Transform_t3275118058 * ___transform1, float ___widthInMeters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LoadLevel::.cctor()
extern "C"  void SteamVR_LoadLevel__cctor_m1529347865 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
