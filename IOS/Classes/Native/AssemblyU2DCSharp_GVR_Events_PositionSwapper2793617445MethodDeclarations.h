﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GVR.Events.PositionSwapper
struct PositionSwapper_t2793617445;

#include "codegen/il2cpp-codegen.h"

// System.Void GVR.Events.PositionSwapper::.ctor()
extern "C"  void PositionSwapper__ctor_m983052788 (PositionSwapper_t2793617445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GVR.Events.PositionSwapper::SetConstraint(System.Int32)
extern "C"  void PositionSwapper_SetConstraint_m520123626 (PositionSwapper_t2793617445 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GVR.Events.PositionSwapper::SetPosition(System.Int32)
extern "C"  void PositionSwapper_SetPosition_m1298722950 (PositionSwapper_t2793617445 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
