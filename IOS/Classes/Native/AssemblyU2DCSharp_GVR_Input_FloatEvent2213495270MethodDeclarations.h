﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GVR.Input.FloatEvent
struct FloatEvent_t2213495270;

#include "codegen/il2cpp-codegen.h"

// System.Void GVR.Input.FloatEvent::.ctor()
extern "C"  void FloatEvent__ctor_m920471060 (FloatEvent_t2213495270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
