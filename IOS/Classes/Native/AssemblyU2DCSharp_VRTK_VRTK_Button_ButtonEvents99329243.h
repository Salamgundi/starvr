﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Button/ButtonEvents
struct  ButtonEvents_t99329243  : public Il2CppObject
{
public:
	// UnityEngine.Events.UnityEvent VRTK.VRTK_Button/ButtonEvents::OnPush
	UnityEvent_t408735097 * ___OnPush_0;

public:
	inline static int32_t get_offset_of_OnPush_0() { return static_cast<int32_t>(offsetof(ButtonEvents_t99329243, ___OnPush_0)); }
	inline UnityEvent_t408735097 * get_OnPush_0() const { return ___OnPush_0; }
	inline UnityEvent_t408735097 ** get_address_of_OnPush_0() { return &___OnPush_0; }
	inline void set_OnPush_0(UnityEvent_t408735097 * value)
	{
		___OnPush_0 = value;
		Il2CppCodeGenWriteBarrier(&___OnPush_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
