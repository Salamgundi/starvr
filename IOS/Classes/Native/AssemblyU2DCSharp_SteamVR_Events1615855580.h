﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SteamVR_Events/Event`1<System.Boolean>
struct Event_1_t1706029839;
// SteamVR_Events/Event`2<System.Int32,System.Boolean>
struct Event_2_t3885983284;
// SteamVR_Events/Event`3<UnityEngine.Color,System.Single,System.Boolean>
struct Event_3_t29453044;
// SteamVR_Events/Event
struct Event_t1855872343;
// SteamVR_Events/Event`1<System.Single>
struct Event_1_t4251932349;
// SteamVR_Events/Event`1<Valve.VR.TrackedDevicePose_t[]>
struct Event_1_t777727170;
// SteamVR_Events/Event`2<SteamVR_RenderModel,System.Boolean>
struct Event_2_t3622693210;
// System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>
struct Dictionary_2_t2291208074;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SteamVR_Events
struct  SteamVR_Events_t1615855580  : public Il2CppObject
{
public:

public:
};

struct SteamVR_Events_t1615855580_StaticFields
{
public:
	// SteamVR_Events/Event`1<System.Boolean> SteamVR_Events::Calibrating
	Event_1_t1706029839 * ___Calibrating_0;
	// SteamVR_Events/Event`2<System.Int32,System.Boolean> SteamVR_Events::DeviceConnected
	Event_2_t3885983284 * ___DeviceConnected_1;
	// SteamVR_Events/Event`3<UnityEngine.Color,System.Single,System.Boolean> SteamVR_Events::Fade
	Event_3_t29453044 * ___Fade_2;
	// SteamVR_Events/Event SteamVR_Events::FadeReady
	Event_t1855872343 * ___FadeReady_3;
	// SteamVR_Events/Event`1<System.Boolean> SteamVR_Events::HideRenderModels
	Event_1_t1706029839 * ___HideRenderModels_4;
	// SteamVR_Events/Event`1<System.Boolean> SteamVR_Events::Initializing
	Event_1_t1706029839 * ___Initializing_5;
	// SteamVR_Events/Event`1<System.Boolean> SteamVR_Events::InputFocus
	Event_1_t1706029839 * ___InputFocus_6;
	// SteamVR_Events/Event`1<System.Boolean> SteamVR_Events::Loading
	Event_1_t1706029839 * ___Loading_7;
	// SteamVR_Events/Event`1<System.Single> SteamVR_Events::LoadingFadeIn
	Event_1_t4251932349 * ___LoadingFadeIn_8;
	// SteamVR_Events/Event`1<System.Single> SteamVR_Events::LoadingFadeOut
	Event_1_t4251932349 * ___LoadingFadeOut_9;
	// SteamVR_Events/Event`1<Valve.VR.TrackedDevicePose_t[]> SteamVR_Events::NewPoses
	Event_1_t777727170 * ___NewPoses_10;
	// SteamVR_Events/Event SteamVR_Events::NewPosesApplied
	Event_t1855872343 * ___NewPosesApplied_11;
	// SteamVR_Events/Event`1<System.Boolean> SteamVR_Events::OutOfRange
	Event_1_t1706029839 * ___OutOfRange_12;
	// SteamVR_Events/Event`2<SteamVR_RenderModel,System.Boolean> SteamVR_Events::RenderModelLoaded
	Event_2_t3622693210 * ___RenderModelLoaded_13;
	// System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>> SteamVR_Events::systemEvents
	Dictionary_2_t2291208074 * ___systemEvents_14;

public:
	inline static int32_t get_offset_of_Calibrating_0() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___Calibrating_0)); }
	inline Event_1_t1706029839 * get_Calibrating_0() const { return ___Calibrating_0; }
	inline Event_1_t1706029839 ** get_address_of_Calibrating_0() { return &___Calibrating_0; }
	inline void set_Calibrating_0(Event_1_t1706029839 * value)
	{
		___Calibrating_0 = value;
		Il2CppCodeGenWriteBarrier(&___Calibrating_0, value);
	}

	inline static int32_t get_offset_of_DeviceConnected_1() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___DeviceConnected_1)); }
	inline Event_2_t3885983284 * get_DeviceConnected_1() const { return ___DeviceConnected_1; }
	inline Event_2_t3885983284 ** get_address_of_DeviceConnected_1() { return &___DeviceConnected_1; }
	inline void set_DeviceConnected_1(Event_2_t3885983284 * value)
	{
		___DeviceConnected_1 = value;
		Il2CppCodeGenWriteBarrier(&___DeviceConnected_1, value);
	}

	inline static int32_t get_offset_of_Fade_2() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___Fade_2)); }
	inline Event_3_t29453044 * get_Fade_2() const { return ___Fade_2; }
	inline Event_3_t29453044 ** get_address_of_Fade_2() { return &___Fade_2; }
	inline void set_Fade_2(Event_3_t29453044 * value)
	{
		___Fade_2 = value;
		Il2CppCodeGenWriteBarrier(&___Fade_2, value);
	}

	inline static int32_t get_offset_of_FadeReady_3() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___FadeReady_3)); }
	inline Event_t1855872343 * get_FadeReady_3() const { return ___FadeReady_3; }
	inline Event_t1855872343 ** get_address_of_FadeReady_3() { return &___FadeReady_3; }
	inline void set_FadeReady_3(Event_t1855872343 * value)
	{
		___FadeReady_3 = value;
		Il2CppCodeGenWriteBarrier(&___FadeReady_3, value);
	}

	inline static int32_t get_offset_of_HideRenderModels_4() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___HideRenderModels_4)); }
	inline Event_1_t1706029839 * get_HideRenderModels_4() const { return ___HideRenderModels_4; }
	inline Event_1_t1706029839 ** get_address_of_HideRenderModels_4() { return &___HideRenderModels_4; }
	inline void set_HideRenderModels_4(Event_1_t1706029839 * value)
	{
		___HideRenderModels_4 = value;
		Il2CppCodeGenWriteBarrier(&___HideRenderModels_4, value);
	}

	inline static int32_t get_offset_of_Initializing_5() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___Initializing_5)); }
	inline Event_1_t1706029839 * get_Initializing_5() const { return ___Initializing_5; }
	inline Event_1_t1706029839 ** get_address_of_Initializing_5() { return &___Initializing_5; }
	inline void set_Initializing_5(Event_1_t1706029839 * value)
	{
		___Initializing_5 = value;
		Il2CppCodeGenWriteBarrier(&___Initializing_5, value);
	}

	inline static int32_t get_offset_of_InputFocus_6() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___InputFocus_6)); }
	inline Event_1_t1706029839 * get_InputFocus_6() const { return ___InputFocus_6; }
	inline Event_1_t1706029839 ** get_address_of_InputFocus_6() { return &___InputFocus_6; }
	inline void set_InputFocus_6(Event_1_t1706029839 * value)
	{
		___InputFocus_6 = value;
		Il2CppCodeGenWriteBarrier(&___InputFocus_6, value);
	}

	inline static int32_t get_offset_of_Loading_7() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___Loading_7)); }
	inline Event_1_t1706029839 * get_Loading_7() const { return ___Loading_7; }
	inline Event_1_t1706029839 ** get_address_of_Loading_7() { return &___Loading_7; }
	inline void set_Loading_7(Event_1_t1706029839 * value)
	{
		___Loading_7 = value;
		Il2CppCodeGenWriteBarrier(&___Loading_7, value);
	}

	inline static int32_t get_offset_of_LoadingFadeIn_8() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___LoadingFadeIn_8)); }
	inline Event_1_t4251932349 * get_LoadingFadeIn_8() const { return ___LoadingFadeIn_8; }
	inline Event_1_t4251932349 ** get_address_of_LoadingFadeIn_8() { return &___LoadingFadeIn_8; }
	inline void set_LoadingFadeIn_8(Event_1_t4251932349 * value)
	{
		___LoadingFadeIn_8 = value;
		Il2CppCodeGenWriteBarrier(&___LoadingFadeIn_8, value);
	}

	inline static int32_t get_offset_of_LoadingFadeOut_9() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___LoadingFadeOut_9)); }
	inline Event_1_t4251932349 * get_LoadingFadeOut_9() const { return ___LoadingFadeOut_9; }
	inline Event_1_t4251932349 ** get_address_of_LoadingFadeOut_9() { return &___LoadingFadeOut_9; }
	inline void set_LoadingFadeOut_9(Event_1_t4251932349 * value)
	{
		___LoadingFadeOut_9 = value;
		Il2CppCodeGenWriteBarrier(&___LoadingFadeOut_9, value);
	}

	inline static int32_t get_offset_of_NewPoses_10() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___NewPoses_10)); }
	inline Event_1_t777727170 * get_NewPoses_10() const { return ___NewPoses_10; }
	inline Event_1_t777727170 ** get_address_of_NewPoses_10() { return &___NewPoses_10; }
	inline void set_NewPoses_10(Event_1_t777727170 * value)
	{
		___NewPoses_10 = value;
		Il2CppCodeGenWriteBarrier(&___NewPoses_10, value);
	}

	inline static int32_t get_offset_of_NewPosesApplied_11() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___NewPosesApplied_11)); }
	inline Event_t1855872343 * get_NewPosesApplied_11() const { return ___NewPosesApplied_11; }
	inline Event_t1855872343 ** get_address_of_NewPosesApplied_11() { return &___NewPosesApplied_11; }
	inline void set_NewPosesApplied_11(Event_t1855872343 * value)
	{
		___NewPosesApplied_11 = value;
		Il2CppCodeGenWriteBarrier(&___NewPosesApplied_11, value);
	}

	inline static int32_t get_offset_of_OutOfRange_12() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___OutOfRange_12)); }
	inline Event_1_t1706029839 * get_OutOfRange_12() const { return ___OutOfRange_12; }
	inline Event_1_t1706029839 ** get_address_of_OutOfRange_12() { return &___OutOfRange_12; }
	inline void set_OutOfRange_12(Event_1_t1706029839 * value)
	{
		___OutOfRange_12 = value;
		Il2CppCodeGenWriteBarrier(&___OutOfRange_12, value);
	}

	inline static int32_t get_offset_of_RenderModelLoaded_13() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___RenderModelLoaded_13)); }
	inline Event_2_t3622693210 * get_RenderModelLoaded_13() const { return ___RenderModelLoaded_13; }
	inline Event_2_t3622693210 ** get_address_of_RenderModelLoaded_13() { return &___RenderModelLoaded_13; }
	inline void set_RenderModelLoaded_13(Event_2_t3622693210 * value)
	{
		___RenderModelLoaded_13 = value;
		Il2CppCodeGenWriteBarrier(&___RenderModelLoaded_13, value);
	}

	inline static int32_t get_offset_of_systemEvents_14() { return static_cast<int32_t>(offsetof(SteamVR_Events_t1615855580_StaticFields, ___systemEvents_14)); }
	inline Dictionary_2_t2291208074 * get_systemEvents_14() const { return ___systemEvents_14; }
	inline Dictionary_2_t2291208074 ** get_address_of_systemEvents_14() { return &___systemEvents_14; }
	inline void set_systemEvents_14(Dictionary_2_t2291208074 * value)
	{
		___systemEvents_14 = value;
		Il2CppCodeGenWriteBarrier(&___systemEvents_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
