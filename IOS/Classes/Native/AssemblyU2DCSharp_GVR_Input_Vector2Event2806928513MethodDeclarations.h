﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GVR.Input.Vector2Event
struct Vector2Event_t2806928513;

#include "codegen/il2cpp-codegen.h"

// System.Void GVR.Input.Vector2Event::.ctor()
extern "C"  void Vector2Event__ctor_m2692025845 (Vector2Event_t2806928513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
