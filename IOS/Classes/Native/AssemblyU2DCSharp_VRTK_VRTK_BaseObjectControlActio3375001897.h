﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_ObjectControl
struct VRTK_ObjectControl_t724022372;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BaseObjectControlAction727620975.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_BaseObjectControlAction
struct  VRTK_BaseObjectControlAction_t3375001897  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_ObjectControl VRTK.VRTK_BaseObjectControlAction::objectControlScript
	VRTK_ObjectControl_t724022372 * ___objectControlScript_2;
	// VRTK.VRTK_BaseObjectControlAction/AxisListeners VRTK.VRTK_BaseObjectControlAction::listenOnAxisChange
	int32_t ___listenOnAxisChange_3;
	// UnityEngine.Collider VRTK.VRTK_BaseObjectControlAction::centerCollider
	Collider_t3497673348 * ___centerCollider_4;
	// UnityEngine.Vector3 VRTK.VRTK_BaseObjectControlAction::colliderCenter
	Vector3_t2243707580  ___colliderCenter_5;
	// System.Single VRTK.VRTK_BaseObjectControlAction::colliderRadius
	float ___colliderRadius_6;
	// System.Single VRTK.VRTK_BaseObjectControlAction::colliderHeight
	float ___colliderHeight_7;
	// UnityEngine.Transform VRTK.VRTK_BaseObjectControlAction::controlledTransform
	Transform_t3275118058 * ___controlledTransform_8;
	// UnityEngine.Transform VRTK.VRTK_BaseObjectControlAction::playArea
	Transform_t3275118058 * ___playArea_9;

public:
	inline static int32_t get_offset_of_objectControlScript_2() { return static_cast<int32_t>(offsetof(VRTK_BaseObjectControlAction_t3375001897, ___objectControlScript_2)); }
	inline VRTK_ObjectControl_t724022372 * get_objectControlScript_2() const { return ___objectControlScript_2; }
	inline VRTK_ObjectControl_t724022372 ** get_address_of_objectControlScript_2() { return &___objectControlScript_2; }
	inline void set_objectControlScript_2(VRTK_ObjectControl_t724022372 * value)
	{
		___objectControlScript_2 = value;
		Il2CppCodeGenWriteBarrier(&___objectControlScript_2, value);
	}

	inline static int32_t get_offset_of_listenOnAxisChange_3() { return static_cast<int32_t>(offsetof(VRTK_BaseObjectControlAction_t3375001897, ___listenOnAxisChange_3)); }
	inline int32_t get_listenOnAxisChange_3() const { return ___listenOnAxisChange_3; }
	inline int32_t* get_address_of_listenOnAxisChange_3() { return &___listenOnAxisChange_3; }
	inline void set_listenOnAxisChange_3(int32_t value)
	{
		___listenOnAxisChange_3 = value;
	}

	inline static int32_t get_offset_of_centerCollider_4() { return static_cast<int32_t>(offsetof(VRTK_BaseObjectControlAction_t3375001897, ___centerCollider_4)); }
	inline Collider_t3497673348 * get_centerCollider_4() const { return ___centerCollider_4; }
	inline Collider_t3497673348 ** get_address_of_centerCollider_4() { return &___centerCollider_4; }
	inline void set_centerCollider_4(Collider_t3497673348 * value)
	{
		___centerCollider_4 = value;
		Il2CppCodeGenWriteBarrier(&___centerCollider_4, value);
	}

	inline static int32_t get_offset_of_colliderCenter_5() { return static_cast<int32_t>(offsetof(VRTK_BaseObjectControlAction_t3375001897, ___colliderCenter_5)); }
	inline Vector3_t2243707580  get_colliderCenter_5() const { return ___colliderCenter_5; }
	inline Vector3_t2243707580 * get_address_of_colliderCenter_5() { return &___colliderCenter_5; }
	inline void set_colliderCenter_5(Vector3_t2243707580  value)
	{
		___colliderCenter_5 = value;
	}

	inline static int32_t get_offset_of_colliderRadius_6() { return static_cast<int32_t>(offsetof(VRTK_BaseObjectControlAction_t3375001897, ___colliderRadius_6)); }
	inline float get_colliderRadius_6() const { return ___colliderRadius_6; }
	inline float* get_address_of_colliderRadius_6() { return &___colliderRadius_6; }
	inline void set_colliderRadius_6(float value)
	{
		___colliderRadius_6 = value;
	}

	inline static int32_t get_offset_of_colliderHeight_7() { return static_cast<int32_t>(offsetof(VRTK_BaseObjectControlAction_t3375001897, ___colliderHeight_7)); }
	inline float get_colliderHeight_7() const { return ___colliderHeight_7; }
	inline float* get_address_of_colliderHeight_7() { return &___colliderHeight_7; }
	inline void set_colliderHeight_7(float value)
	{
		___colliderHeight_7 = value;
	}

	inline static int32_t get_offset_of_controlledTransform_8() { return static_cast<int32_t>(offsetof(VRTK_BaseObjectControlAction_t3375001897, ___controlledTransform_8)); }
	inline Transform_t3275118058 * get_controlledTransform_8() const { return ___controlledTransform_8; }
	inline Transform_t3275118058 ** get_address_of_controlledTransform_8() { return &___controlledTransform_8; }
	inline void set_controlledTransform_8(Transform_t3275118058 * value)
	{
		___controlledTransform_8 = value;
		Il2CppCodeGenWriteBarrier(&___controlledTransform_8, value);
	}

	inline static int32_t get_offset_of_playArea_9() { return static_cast<int32_t>(offsetof(VRTK_BaseObjectControlAction_t3375001897, ___playArea_9)); }
	inline Transform_t3275118058 * get_playArea_9() const { return ___playArea_9; }
	inline Transform_t3275118058 ** get_address_of_playArea_9() { return &___playArea_9; }
	inline void set_playArea_9(Transform_t3275118058 * value)
	{
		___playArea_9 = value;
		Il2CppCodeGenWriteBarrier(&___playArea_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
