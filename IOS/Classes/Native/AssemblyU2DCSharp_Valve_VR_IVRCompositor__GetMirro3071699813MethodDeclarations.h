﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_GetMirrorTextureGL
struct _GetMirrorTextureGL_t3071699813;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRCompositorError3948578210.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_GetMirrorTextureGL::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetMirrorTextureGL__ctor_m1848071870 (_GetMirrorTextureGL_t3071699813 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.IVRCompositor/_GetMirrorTextureGL::Invoke(Valve.VR.EVREye,System.UInt32&,System.IntPtr)
extern "C"  int32_t _GetMirrorTextureGL_Invoke_m3299990563 (_GetMirrorTextureGL_t3071699813 * __this, int32_t ___eEye0, uint32_t* ___pglTextureId1, IntPtr_t ___pglSharedTextureHandle2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_GetMirrorTextureGL::BeginInvoke(Valve.VR.EVREye,System.UInt32&,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetMirrorTextureGL_BeginInvoke_m3366173475 (_GetMirrorTextureGL_t3071699813 * __this, int32_t ___eEye0, uint32_t* ___pglTextureId1, IntPtr_t ___pglSharedTextureHandle2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.IVRCompositor/_GetMirrorTextureGL::EndInvoke(System.UInt32&,System.IAsyncResult)
extern "C"  int32_t _GetMirrorTextureGL_EndInvoke_m2477659019 (_GetMirrorTextureGL_t3071699813 * __this, uint32_t* ___pglTextureId0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
