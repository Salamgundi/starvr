﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_RoomExtender
struct VRTK_RoomExtender_t3041247552;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.VRTK_RoomExtender_ControllerExample
struct  VRTK_RoomExtender_ControllerExample_t3110057233  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_RoomExtender VRTK.Examples.VRTK_RoomExtender_ControllerExample::roomExtender
	VRTK_RoomExtender_t3041247552 * ___roomExtender_2;

public:
	inline static int32_t get_offset_of_roomExtender_2() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_ControllerExample_t3110057233, ___roomExtender_2)); }
	inline VRTK_RoomExtender_t3041247552 * get_roomExtender_2() const { return ___roomExtender_2; }
	inline VRTK_RoomExtender_t3041247552 ** get_address_of_roomExtender_2() { return &___roomExtender_2; }
	inline void set_roomExtender_2(VRTK_RoomExtender_t3041247552 * value)
	{
		___roomExtender_2 = value;
		Il2CppCodeGenWriteBarrier(&___roomExtender_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
