﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.RC_Car
struct RC_Car_t3139601232;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void VRTK.Examples.RC_Car::.ctor()
extern "C"  void RC_Car__ctor_m927265149 (RC_Car_t3139601232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car::SetTouchAxis(UnityEngine.Vector2)
extern "C"  void RC_Car_SetTouchAxis_m185898401 (RC_Car_t3139601232 * __this, Vector2_t2243707579  ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car::SetTriggerAxis(System.Single)
extern "C"  void RC_Car_SetTriggerAxis_m1899004949 (RC_Car_t3139601232 * __this, float ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car::ResetCar()
extern "C"  void RC_Car_ResetCar_m732003638 (RC_Car_t3139601232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car::Awake()
extern "C"  void RC_Car_Awake_m2814405210 (RC_Car_t3139601232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car::FixedUpdate()
extern "C"  void RC_Car_FixedUpdate_m3391963698 (RC_Car_t3139601232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car::CalculateSpeed()
extern "C"  void RC_Car_CalculateSpeed_m894667022 (RC_Car_t3139601232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car::Decelerate()
extern "C"  void RC_Car_Decelerate_m2420029581 (RC_Car_t3139601232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car::Move()
extern "C"  void RC_Car_Move_m1524454568 (RC_Car_t3139601232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car::Turn()
extern "C"  void RC_Car_Turn_m590732826 (RC_Car_t3139601232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car::Jump()
extern "C"  void RC_Car_Jump_m3844112259 (RC_Car_t3139601232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car::OnTriggerStay(UnityEngine.Collider)
extern "C"  void RC_Car_OnTriggerStay_m271815786 (RC_Car_t3139601232 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RC_Car::OnTriggerExit(UnityEngine.Collider)
extern "C"  void RC_Car_OnTriggerExit_m4003712729 (RC_Car_t3139601232 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
