﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>
struct Dictionary_2_t3947503238;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1339068706.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Valve.VR.EVRButtonId,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3022090505_gshared (Enumerator_t1339068706 * __this, Dictionary_2_t3947503238 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3022090505(__this, ___host0, method) ((  void (*) (Enumerator_t1339068706 *, Dictionary_2_t3947503238 *, const MethodInfo*))Enumerator__ctor_m3022090505_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Valve.VR.EVRButtonId,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1188554340_gshared (Enumerator_t1339068706 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1188554340(__this, method) ((  Il2CppObject * (*) (Enumerator_t1339068706 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1188554340_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Valve.VR.EVRButtonId,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m353061978_gshared (Enumerator_t1339068706 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m353061978(__this, method) ((  void (*) (Enumerator_t1339068706 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m353061978_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Valve.VR.EVRButtonId,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3730565457_gshared (Enumerator_t1339068706 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3730565457(__this, method) ((  void (*) (Enumerator_t1339068706 *, const MethodInfo*))Enumerator_Dispose_m3730565457_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Valve.VR.EVRButtonId,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3484935214_gshared (Enumerator_t1339068706 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3484935214(__this, method) ((  bool (*) (Enumerator_t1339068706 *, const MethodInfo*))Enumerator_MoveNext_m3484935214_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Valve.VR.EVRButtonId,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3332600564_gshared (Enumerator_t1339068706 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3332600564(__this, method) ((  Il2CppObject * (*) (Enumerator_t1339068706 *, const MethodInfo*))Enumerator_get_Current_m3332600564_gshared)(__this, method)
