﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Events/Event
struct Event_t1855872343;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"

// System.Void SteamVR_Events/Event::.ctor()
extern "C"  void Event__ctor_m1939075782 (Event_t1855872343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Events/Event::Listen(UnityEngine.Events.UnityAction)
extern "C"  void Event_Listen_m163577570 (Event_t1855872343 * __this, UnityAction_t4025899511 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Events/Event::Remove(UnityEngine.Events.UnityAction)
extern "C"  void Event_Remove_m3971628327 (Event_t1855872343 * __this, UnityAction_t4025899511 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Events/Event::Send()
extern "C"  void Event_Send_m2776595870 (Event_t1855872343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
