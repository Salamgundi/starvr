﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RotateSpace
struct RotateSpace_t2533461947;

#include "codegen/il2cpp-codegen.h"

// System.Void RotateSpace::.ctor()
extern "C"  void RotateSpace__ctor_m3337035642 (RotateSpace_t2533461947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateSpace::Start()
extern "C"  void RotateSpace_Start_m2451160854 (RotateSpace_t2533461947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateSpace::Update()
extern "C"  void RotateSpace_Update_m60041055 (RotateSpace_t2533461947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
