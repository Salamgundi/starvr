﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Archery.Arrow
struct Arrow_t1847108333;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void VRTK.Examples.Archery.Arrow::.ctor()
extern "C"  void Arrow__ctor_m147264924 (Arrow_t1847108333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.Arrow::SetArrowHolder(UnityEngine.GameObject)
extern "C"  void Arrow_SetArrowHolder_m2319916629 (Arrow_t1847108333 * __this, GameObject_t1756533147 * ___holder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.Arrow::OnNock()
extern "C"  void Arrow_OnNock_m1088046442 (Arrow_t1847108333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.Arrow::Fired()
extern "C"  void Arrow_Fired_m3734305300 (Arrow_t1847108333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.Arrow::ResetArrow()
extern "C"  void Arrow_ResetArrow_m647515870 (Arrow_t1847108333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.Arrow::Start()
extern "C"  void Arrow_Start_m1440974304 (Arrow_t1847108333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.Arrow::SetOrigns()
extern "C"  void Arrow_SetOrigns_m834065374 (Arrow_t1847108333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.Arrow::FixedUpdate()
extern "C"  void Arrow_FixedUpdate_m194802101 (Arrow_t1847108333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.Arrow::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void Arrow_OnCollisionEnter_m312563574 (Arrow_t1847108333 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.Arrow::RecreateNotch()
extern "C"  void Arrow_RecreateNotch_m4239672043 (Arrow_t1847108333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.Arrow::ResetTransform()
extern "C"  void Arrow_ResetTransform_m727853503 (Arrow_t1847108333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.Arrow::DestroyArrow(System.Single)
extern "C"  void Arrow_DestroyArrow_m4137188102 (Arrow_t1847108333 * __this, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
