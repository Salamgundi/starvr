﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetApplicationPropertyString
struct _GetApplicationPropertyString_t2585650982;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationProperty1959780520.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetApplicationPropertyString::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetApplicationPropertyString__ctor_m1206136221 (_GetApplicationPropertyString_t2585650982 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRApplications/_GetApplicationPropertyString::Invoke(System.String,Valve.VR.EVRApplicationProperty,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRApplicationError&)
extern "C"  uint32_t _GetApplicationPropertyString_Invoke_m1600415261 (_GetApplicationPropertyString_t2585650982 * __this, String_t* ___pchAppKey0, int32_t ___eProperty1, StringBuilder_t1221177846 * ___pchPropertyValueBuffer2, uint32_t ___unPropertyValueBufferLen3, int32_t* ___peError4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetApplicationPropertyString::BeginInvoke(System.String,Valve.VR.EVRApplicationProperty,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRApplicationError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetApplicationPropertyString_BeginInvoke_m2413905949 (_GetApplicationPropertyString_t2585650982 * __this, String_t* ___pchAppKey0, int32_t ___eProperty1, StringBuilder_t1221177846 * ___pchPropertyValueBuffer2, uint32_t ___unPropertyValueBufferLen3, int32_t* ___peError4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRApplications/_GetApplicationPropertyString::EndInvoke(Valve.VR.EVRApplicationError&,System.IAsyncResult)
extern "C"  uint32_t _GetApplicationPropertyString_EndInvoke_m1144640333 (_GetApplicationPropertyString_t2585650982 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
