﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangeColor
struct ChangeColor_t2618434533;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void ChangeColor::.ctor()
extern "C"  void ChangeColor__ctor_m2790372686 (ChangeColor_t2618434533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeColor::OnEnable()
extern "C"  void ChangeColor_OnEnable_m4050073974 (ChangeColor_t2618434533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeColor::SetRed(System.Single)
extern "C"  void ChangeColor_SetRed_m2156477528 (ChangeColor_t2618434533 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeColor::SetGreen(System.Single)
extern "C"  void ChangeColor_SetGreen_m2923697004 (ChangeColor_t2618434533 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeColor::SetBlue(System.Single)
extern "C"  void ChangeColor_SetBlue_m3283249285 (ChangeColor_t2618434533 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeColor::OnValueChanged(System.Single,System.Int32)
extern "C"  void ChangeColor_OnValueChanged_m3031140646 (ChangeColor_t2618434533 * __this, float ___value0, int32_t ___channel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeColor::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ChangeColor_OnPointerClick_m668921054 (ChangeColor_t2618434533 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
