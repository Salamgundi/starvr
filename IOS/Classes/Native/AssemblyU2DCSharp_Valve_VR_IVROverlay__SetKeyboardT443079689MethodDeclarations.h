﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetKeyboardTransformAbsolute
struct _SetKeyboardTransformAbsolute_t443079689;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackingUniverseOrigin1464400093.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix34_t664273062.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetKeyboardTransformAbsolute::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetKeyboardTransformAbsolute__ctor_m1421119862 (_SetKeyboardTransformAbsolute_t443079689 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVROverlay/_SetKeyboardTransformAbsolute::Invoke(Valve.VR.ETrackingUniverseOrigin,Valve.VR.HmdMatrix34_t&)
extern "C"  void _SetKeyboardTransformAbsolute_Invoke_m3438338983 (_SetKeyboardTransformAbsolute_t443079689 * __this, int32_t ___eTrackingOrigin0, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToKeyboardTransform1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetKeyboardTransformAbsolute::BeginInvoke(Valve.VR.ETrackingUniverseOrigin,Valve.VR.HmdMatrix34_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetKeyboardTransformAbsolute_BeginInvoke_m2869166502 (_SetKeyboardTransformAbsolute_t443079689 * __this, int32_t ___eTrackingOrigin0, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToKeyboardTransform1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVROverlay/_SetKeyboardTransformAbsolute::EndInvoke(Valve.VR.HmdMatrix34_t&,System.IAsyncResult)
extern "C"  void _SetKeyboardTransformAbsolute_EndInvoke_m4061758452 (_SetKeyboardTransformAbsolute_t443079689 * __this, HmdMatrix34_t_t664273062 * ___pmatTrackingOriginToKeyboardTransform0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
