﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DragMe
struct DragMe_t1948526048;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void DragMe::.ctor()
extern "C"  void DragMe__ctor_m3390742995 (DragMe_t1948526048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragMe::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DragMe_OnBeginDrag_m2400362709 (DragMe_t1948526048 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragMe::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DragMe_OnDrag_m2701567764 (DragMe_t1948526048 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragMe::SetDraggedPosition(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DragMe_SetDraggedPosition_m2380895642 (DragMe_t1948526048 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragMe::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DragMe_OnEndDrag_m2837555393 (DragMe_t1948526048 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
