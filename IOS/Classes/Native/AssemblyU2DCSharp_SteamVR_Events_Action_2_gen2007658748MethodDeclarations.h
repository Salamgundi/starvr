﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_2_gen2855183315MethodDeclarations.h"

// System.Void SteamVR_Events/Action`2<SteamVR_RenderModel,System.Boolean>::.ctor(SteamVR_Events/Event`2<T0,T1>,UnityEngine.Events.UnityAction`2<T0,T1>)
#define Action_2__ctor_m2859387301(__this, ____event0, ___action1, method) ((  void (*) (Action_2_t2007658748 *, Event_2_t3622693210 *, UnityAction_2_t4073506138 *, const MethodInfo*))Action_2__ctor_m980819815_gshared)(__this, ____event0, ___action1, method)
// System.Void SteamVR_Events/Action`2<SteamVR_RenderModel,System.Boolean>::Enable(System.Boolean)
#define Action_2_Enable_m978244153(__this, ___enabled0, method) ((  void (*) (Action_2_t2007658748 *, bool, const MethodInfo*))Action_2_Enable_m822486595_gshared)(__this, ___enabled0, method)
