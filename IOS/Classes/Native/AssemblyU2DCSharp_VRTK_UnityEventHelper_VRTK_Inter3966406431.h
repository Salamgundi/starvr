﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen3450829461.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents/UnityObjectEvent
struct  UnityObjectEvent_t3966406431  : public UnityEvent_2_t3450829461
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
