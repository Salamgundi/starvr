﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Controller/Device
struct Device_t2885069456;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SteamVR_Utils_RigidTransform2602383126.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_Valve_VR_VRControllerState_t2504874220.h"
#include "AssemblyU2DCSharp_Valve_VR_TrackedDevicePose_t1668551120.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRButtonId66145412.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void SteamVR_Controller/Device::.ctor(System.UInt32)
extern "C"  void Device__ctor_m2058017889 (Device_t2885069456 * __this, uint32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SteamVR_Controller/Device::get_index()
extern "C"  uint32_t Device_get_index_m1246351645 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Controller/Device::set_index(System.UInt32)
extern "C"  void Device_set_index_m400374434 (Device_t2885069456 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::get_valid()
extern "C"  bool Device_get_valid_m978625664 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Controller/Device::set_valid(System.Boolean)
extern "C"  void Device_set_valid_m2639682273 (Device_t2885069456 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::get_connected()
extern "C"  bool Device_get_connected_m702149907 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::get_hasTracking()
extern "C"  bool Device_get_hasTracking_m35620941 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::get_outOfRange()
extern "C"  bool Device_get_outOfRange_m994424578 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::get_calibrating()
extern "C"  bool Device_get_calibrating_m222283684 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::get_uninitialized()
extern "C"  bool Device_get_uninitialized_m1056960251 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Utils/RigidTransform SteamVR_Controller/Device::get_transform()
extern "C"  RigidTransform_t2602383126  Device_get_transform_m792528943 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SteamVR_Controller/Device::get_velocity()
extern "C"  Vector3_t2243707580  Device_get_velocity_m513529121 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SteamVR_Controller/Device::get_angularVelocity()
extern "C"  Vector3_t2243707580  Device_get_angularVelocity_m2791070131 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.VRControllerState_t SteamVR_Controller/Device::GetState()
extern "C"  VRControllerState_t_t2504874220  Device_GetState_m634997987 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.VRControllerState_t SteamVR_Controller/Device::GetPrevState()
extern "C"  VRControllerState_t_t2504874220  Device_GetPrevState_m268347072 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.TrackedDevicePose_t SteamVR_Controller/Device::GetPose()
extern "C"  TrackedDevicePose_t_t1668551120  Device_GetPose_m2129370149 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Controller/Device::Update()
extern "C"  void Device_Update_m4028598910 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetPress(System.UInt64)
extern "C"  bool Device_GetPress_m826175971 (Device_t2885069456 * __this, uint64_t ___buttonMask0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetPressDown(System.UInt64)
extern "C"  bool Device_GetPressDown_m3963090645 (Device_t2885069456 * __this, uint64_t ___buttonMask0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetPressUp(System.UInt64)
extern "C"  bool Device_GetPressUp_m1707789094 (Device_t2885069456 * __this, uint64_t ___buttonMask0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetPress(Valve.VR.EVRButtonId)
extern "C"  bool Device_GetPress_m1795303762 (Device_t2885069456 * __this, int32_t ___buttonId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetPressDown(Valve.VR.EVRButtonId)
extern "C"  bool Device_GetPressDown_m3691321226 (Device_t2885069456 * __this, int32_t ___buttonId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetPressUp(Valve.VR.EVRButtonId)
extern "C"  bool Device_GetPressUp_m2110224535 (Device_t2885069456 * __this, int32_t ___buttonId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetTouch(System.UInt64)
extern "C"  bool Device_GetTouch_m3690271609 (Device_t2885069456 * __this, uint64_t ___buttonMask0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetTouchDown(System.UInt64)
extern "C"  bool Device_GetTouchDown_m1008347171 (Device_t2885069456 * __this, uint64_t ___buttonMask0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetTouchUp(System.UInt64)
extern "C"  bool Device_GetTouchUp_m371776694 (Device_t2885069456 * __this, uint64_t ___buttonMask0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetTouch(Valve.VR.EVRButtonId)
extern "C"  bool Device_GetTouch_m3085278406 (Device_t2885069456 * __this, int32_t ___buttonId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetTouchDown(Valve.VR.EVRButtonId)
extern "C"  bool Device_GetTouchDown_m1109214962 (Device_t2885069456 * __this, int32_t ___buttonId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetTouchUp(Valve.VR.EVRButtonId)
extern "C"  bool Device_GetTouchUp_m1219428545 (Device_t2885069456 * __this, int32_t ___buttonId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 SteamVR_Controller/Device::GetAxis(Valve.VR.EVRButtonId)
extern "C"  Vector2_t2243707579  Device_GetAxis_m35048721 (Device_t2885069456 * __this, int32_t ___buttonId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Controller/Device::TriggerHapticPulse(System.UInt16,Valve.VR.EVRButtonId)
extern "C"  void Device_TriggerHapticPulse_m2212079533 (Device_t2885069456 * __this, uint16_t ___durationMicroSec0, int32_t ___buttonId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Controller/Device::UpdateHairTrigger()
extern "C"  void Device_UpdateHairTrigger_m1143442884 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetHairTrigger()
extern "C"  bool Device_GetHairTrigger_m3876017081 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetHairTriggerDown()
extern "C"  bool Device_GetHairTriggerDown_m3381787855 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_Controller/Device::GetHairTriggerUp()
extern "C"  bool Device_GetHairTriggerUp_m833275572 (Device_t2885069456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
