﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen1520341115MethodDeclarations.h"

// System.Void System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m883641565(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t3843060249 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m716614833_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::Invoke(T,T)
#define Comparison_1_Invoke_m3719708371(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3843060249 *, KeyValuePair_2_t2581321398 , KeyValuePair_2_t2581321398 , const MethodInfo*))Comparison_1_Invoke_m1419390023_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m3768511026(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t3843060249 *, KeyValuePair_2_t2581321398 , KeyValuePair_2_t2581321398 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m390525400_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m157643673(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t3843060249 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m760782173_gshared)(__this, ___result0, method)
