﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_CurveGenerator
struct VRTK_CurveGenerator_t3769661606;

#include "AssemblyU2DCSharp_VRTK_VRTK_BasePointerRenderer1270536273.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_BezierPointerRenderer
struct  VRTK_BezierPointerRenderer_t3371893943  : public VRTK_BasePointerRenderer_t1270536273
{
public:
	// System.Single VRTK.VRTK_BezierPointerRenderer::maximumLength
	float ___maximumLength_28;
	// System.Int32 VRTK.VRTK_BezierPointerRenderer::tracerDensity
	int32_t ___tracerDensity_29;
	// System.Single VRTK.VRTK_BezierPointerRenderer::cursorRadius
	float ___cursorRadius_30;
	// System.Single VRTK.VRTK_BezierPointerRenderer::heightLimitAngle
	float ___heightLimitAngle_31;
	// System.Single VRTK.VRTK_BezierPointerRenderer::curveOffset
	float ___curveOffset_32;
	// System.Boolean VRTK.VRTK_BezierPointerRenderer::rescaleTracer
	bool ___rescaleTracer_33;
	// System.Boolean VRTK.VRTK_BezierPointerRenderer::cursorMatchTargetRotation
	bool ___cursorMatchTargetRotation_34;
	// System.Int32 VRTK.VRTK_BezierPointerRenderer::collisionCheckFrequency
	int32_t ___collisionCheckFrequency_35;
	// UnityEngine.GameObject VRTK.VRTK_BezierPointerRenderer::customTracer
	GameObject_t1756533147 * ___customTracer_36;
	// UnityEngine.GameObject VRTK.VRTK_BezierPointerRenderer::customCursor
	GameObject_t1756533147 * ___customCursor_37;
	// UnityEngine.GameObject VRTK.VRTK_BezierPointerRenderer::validLocationObject
	GameObject_t1756533147 * ___validLocationObject_38;
	// UnityEngine.GameObject VRTK.VRTK_BezierPointerRenderer::invalidLocationObject
	GameObject_t1756533147 * ___invalidLocationObject_39;
	// VRTK.VRTK_CurveGenerator VRTK.VRTK_BezierPointerRenderer::actualTracer
	VRTK_CurveGenerator_t3769661606 * ___actualTracer_40;
	// UnityEngine.GameObject VRTK.VRTK_BezierPointerRenderer::actualContainer
	GameObject_t1756533147 * ___actualContainer_41;
	// UnityEngine.GameObject VRTK.VRTK_BezierPointerRenderer::actualCursor
	GameObject_t1756533147 * ___actualCursor_42;
	// UnityEngine.GameObject VRTK.VRTK_BezierPointerRenderer::actualValidLocationObject
	GameObject_t1756533147 * ___actualValidLocationObject_43;
	// UnityEngine.GameObject VRTK.VRTK_BezierPointerRenderer::actualInvalidLocationObject
	GameObject_t1756533147 * ___actualInvalidLocationObject_44;
	// UnityEngine.Vector3 VRTK.VRTK_BezierPointerRenderer::fixedForwardBeamForward
	Vector3_t2243707580  ___fixedForwardBeamForward_45;

public:
	inline static int32_t get_offset_of_maximumLength_28() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___maximumLength_28)); }
	inline float get_maximumLength_28() const { return ___maximumLength_28; }
	inline float* get_address_of_maximumLength_28() { return &___maximumLength_28; }
	inline void set_maximumLength_28(float value)
	{
		___maximumLength_28 = value;
	}

	inline static int32_t get_offset_of_tracerDensity_29() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___tracerDensity_29)); }
	inline int32_t get_tracerDensity_29() const { return ___tracerDensity_29; }
	inline int32_t* get_address_of_tracerDensity_29() { return &___tracerDensity_29; }
	inline void set_tracerDensity_29(int32_t value)
	{
		___tracerDensity_29 = value;
	}

	inline static int32_t get_offset_of_cursorRadius_30() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___cursorRadius_30)); }
	inline float get_cursorRadius_30() const { return ___cursorRadius_30; }
	inline float* get_address_of_cursorRadius_30() { return &___cursorRadius_30; }
	inline void set_cursorRadius_30(float value)
	{
		___cursorRadius_30 = value;
	}

	inline static int32_t get_offset_of_heightLimitAngle_31() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___heightLimitAngle_31)); }
	inline float get_heightLimitAngle_31() const { return ___heightLimitAngle_31; }
	inline float* get_address_of_heightLimitAngle_31() { return &___heightLimitAngle_31; }
	inline void set_heightLimitAngle_31(float value)
	{
		___heightLimitAngle_31 = value;
	}

	inline static int32_t get_offset_of_curveOffset_32() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___curveOffset_32)); }
	inline float get_curveOffset_32() const { return ___curveOffset_32; }
	inline float* get_address_of_curveOffset_32() { return &___curveOffset_32; }
	inline void set_curveOffset_32(float value)
	{
		___curveOffset_32 = value;
	}

	inline static int32_t get_offset_of_rescaleTracer_33() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___rescaleTracer_33)); }
	inline bool get_rescaleTracer_33() const { return ___rescaleTracer_33; }
	inline bool* get_address_of_rescaleTracer_33() { return &___rescaleTracer_33; }
	inline void set_rescaleTracer_33(bool value)
	{
		___rescaleTracer_33 = value;
	}

	inline static int32_t get_offset_of_cursorMatchTargetRotation_34() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___cursorMatchTargetRotation_34)); }
	inline bool get_cursorMatchTargetRotation_34() const { return ___cursorMatchTargetRotation_34; }
	inline bool* get_address_of_cursorMatchTargetRotation_34() { return &___cursorMatchTargetRotation_34; }
	inline void set_cursorMatchTargetRotation_34(bool value)
	{
		___cursorMatchTargetRotation_34 = value;
	}

	inline static int32_t get_offset_of_collisionCheckFrequency_35() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___collisionCheckFrequency_35)); }
	inline int32_t get_collisionCheckFrequency_35() const { return ___collisionCheckFrequency_35; }
	inline int32_t* get_address_of_collisionCheckFrequency_35() { return &___collisionCheckFrequency_35; }
	inline void set_collisionCheckFrequency_35(int32_t value)
	{
		___collisionCheckFrequency_35 = value;
	}

	inline static int32_t get_offset_of_customTracer_36() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___customTracer_36)); }
	inline GameObject_t1756533147 * get_customTracer_36() const { return ___customTracer_36; }
	inline GameObject_t1756533147 ** get_address_of_customTracer_36() { return &___customTracer_36; }
	inline void set_customTracer_36(GameObject_t1756533147 * value)
	{
		___customTracer_36 = value;
		Il2CppCodeGenWriteBarrier(&___customTracer_36, value);
	}

	inline static int32_t get_offset_of_customCursor_37() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___customCursor_37)); }
	inline GameObject_t1756533147 * get_customCursor_37() const { return ___customCursor_37; }
	inline GameObject_t1756533147 ** get_address_of_customCursor_37() { return &___customCursor_37; }
	inline void set_customCursor_37(GameObject_t1756533147 * value)
	{
		___customCursor_37 = value;
		Il2CppCodeGenWriteBarrier(&___customCursor_37, value);
	}

	inline static int32_t get_offset_of_validLocationObject_38() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___validLocationObject_38)); }
	inline GameObject_t1756533147 * get_validLocationObject_38() const { return ___validLocationObject_38; }
	inline GameObject_t1756533147 ** get_address_of_validLocationObject_38() { return &___validLocationObject_38; }
	inline void set_validLocationObject_38(GameObject_t1756533147 * value)
	{
		___validLocationObject_38 = value;
		Il2CppCodeGenWriteBarrier(&___validLocationObject_38, value);
	}

	inline static int32_t get_offset_of_invalidLocationObject_39() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___invalidLocationObject_39)); }
	inline GameObject_t1756533147 * get_invalidLocationObject_39() const { return ___invalidLocationObject_39; }
	inline GameObject_t1756533147 ** get_address_of_invalidLocationObject_39() { return &___invalidLocationObject_39; }
	inline void set_invalidLocationObject_39(GameObject_t1756533147 * value)
	{
		___invalidLocationObject_39 = value;
		Il2CppCodeGenWriteBarrier(&___invalidLocationObject_39, value);
	}

	inline static int32_t get_offset_of_actualTracer_40() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___actualTracer_40)); }
	inline VRTK_CurveGenerator_t3769661606 * get_actualTracer_40() const { return ___actualTracer_40; }
	inline VRTK_CurveGenerator_t3769661606 ** get_address_of_actualTracer_40() { return &___actualTracer_40; }
	inline void set_actualTracer_40(VRTK_CurveGenerator_t3769661606 * value)
	{
		___actualTracer_40 = value;
		Il2CppCodeGenWriteBarrier(&___actualTracer_40, value);
	}

	inline static int32_t get_offset_of_actualContainer_41() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___actualContainer_41)); }
	inline GameObject_t1756533147 * get_actualContainer_41() const { return ___actualContainer_41; }
	inline GameObject_t1756533147 ** get_address_of_actualContainer_41() { return &___actualContainer_41; }
	inline void set_actualContainer_41(GameObject_t1756533147 * value)
	{
		___actualContainer_41 = value;
		Il2CppCodeGenWriteBarrier(&___actualContainer_41, value);
	}

	inline static int32_t get_offset_of_actualCursor_42() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___actualCursor_42)); }
	inline GameObject_t1756533147 * get_actualCursor_42() const { return ___actualCursor_42; }
	inline GameObject_t1756533147 ** get_address_of_actualCursor_42() { return &___actualCursor_42; }
	inline void set_actualCursor_42(GameObject_t1756533147 * value)
	{
		___actualCursor_42 = value;
		Il2CppCodeGenWriteBarrier(&___actualCursor_42, value);
	}

	inline static int32_t get_offset_of_actualValidLocationObject_43() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___actualValidLocationObject_43)); }
	inline GameObject_t1756533147 * get_actualValidLocationObject_43() const { return ___actualValidLocationObject_43; }
	inline GameObject_t1756533147 ** get_address_of_actualValidLocationObject_43() { return &___actualValidLocationObject_43; }
	inline void set_actualValidLocationObject_43(GameObject_t1756533147 * value)
	{
		___actualValidLocationObject_43 = value;
		Il2CppCodeGenWriteBarrier(&___actualValidLocationObject_43, value);
	}

	inline static int32_t get_offset_of_actualInvalidLocationObject_44() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___actualInvalidLocationObject_44)); }
	inline GameObject_t1756533147 * get_actualInvalidLocationObject_44() const { return ___actualInvalidLocationObject_44; }
	inline GameObject_t1756533147 ** get_address_of_actualInvalidLocationObject_44() { return &___actualInvalidLocationObject_44; }
	inline void set_actualInvalidLocationObject_44(GameObject_t1756533147 * value)
	{
		___actualInvalidLocationObject_44 = value;
		Il2CppCodeGenWriteBarrier(&___actualInvalidLocationObject_44, value);
	}

	inline static int32_t get_offset_of_fixedForwardBeamForward_45() { return static_cast<int32_t>(offsetof(VRTK_BezierPointerRenderer_t3371893943, ___fixedForwardBeamForward_45)); }
	inline Vector3_t2243707580  get_fixedForwardBeamForward_45() const { return ___fixedForwardBeamForward_45; }
	inline Vector3_t2243707580 * get_address_of_fixedForwardBeamForward_45() { return &___fixedForwardBeamForward_45; }
	inline void set_fixedForwardBeamForward_45(Vector3_t2243707580  value)
	{
		___fixedForwardBeamForward_45 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
