﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayTextureColorSpace
struct _GetOverlayTextureColorSpace_t2119049239;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_EColorSpace2848861630.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayTextureColorSpace::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayTextureColorSpace__ctor_m212052634 (_GetOverlayTextureColorSpace_t2119049239 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTextureColorSpace::Invoke(System.UInt64,Valve.VR.EColorSpace&)
extern "C"  int32_t _GetOverlayTextureColorSpace_Invoke_m533305851 (_GetOverlayTextureColorSpace_t2119049239 * __this, uint64_t ___ulOverlayHandle0, int32_t* ___peTextureColorSpace1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayTextureColorSpace::BeginInvoke(System.UInt64,Valve.VR.EColorSpace&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayTextureColorSpace_BeginInvoke_m486489864 (_GetOverlayTextureColorSpace_t2119049239 * __this, uint64_t ___ulOverlayHandle0, int32_t* ___peTextureColorSpace1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTextureColorSpace::EndInvoke(Valve.VR.EColorSpace&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayTextureColorSpace_EndInvoke_m2224989986 (_GetOverlayTextureColorSpace_t2119049239 * __this, int32_t* ___peTextureColorSpace0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
