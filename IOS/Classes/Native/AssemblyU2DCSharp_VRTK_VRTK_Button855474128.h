﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_Button/ButtonEvents
struct ButtonEvents_t99329243;
// VRTK.Button3DEventHandler
struct Button3DEventHandler_t1715584557;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.ConfigurableJoint
struct ConfigurableJoint_t454307495;
// UnityEngine.ConstantForce
struct ConstantForce_t3796310167;

#include "AssemblyU2DCSharp_VRTK_VRTK_Control651619021.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Button_ButtonDirection1346461537.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Button
struct  VRTK_Button_t855474128  : public VRTK_Control_t651619021
{
public:
	// UnityEngine.GameObject VRTK.VRTK_Button::connectedTo
	GameObject_t1756533147 * ___connectedTo_15;
	// VRTK.VRTK_Button/ButtonDirection VRTK.VRTK_Button::direction
	int32_t ___direction_16;
	// System.Single VRTK.VRTK_Button::activationDistance
	float ___activationDistance_17;
	// System.Single VRTK.VRTK_Button::buttonStrength
	float ___buttonStrength_18;
	// VRTK.VRTK_Button/ButtonEvents VRTK.VRTK_Button::events
	ButtonEvents_t99329243 * ___events_19;
	// VRTK.Button3DEventHandler VRTK.VRTK_Button::Pushed
	Button3DEventHandler_t1715584557 * ___Pushed_20;
	// VRTK.VRTK_Button/ButtonDirection VRTK.VRTK_Button::finalDirection
	int32_t ___finalDirection_22;
	// UnityEngine.Vector3 VRTK.VRTK_Button::restingPosition
	Vector3_t2243707580  ___restingPosition_23;
	// UnityEngine.Vector3 VRTK.VRTK_Button::activationDir
	Vector3_t2243707580  ___activationDir_24;
	// UnityEngine.Rigidbody VRTK.VRTK_Button::buttonRigidbody
	Rigidbody_t4233889191 * ___buttonRigidbody_25;
	// UnityEngine.ConfigurableJoint VRTK.VRTK_Button::buttonJoint
	ConfigurableJoint_t454307495 * ___buttonJoint_26;
	// UnityEngine.ConstantForce VRTK.VRTK_Button::buttonForce
	ConstantForce_t3796310167 * ___buttonForce_27;
	// System.Int32 VRTK.VRTK_Button::forceCount
	int32_t ___forceCount_28;

public:
	inline static int32_t get_offset_of_connectedTo_15() { return static_cast<int32_t>(offsetof(VRTK_Button_t855474128, ___connectedTo_15)); }
	inline GameObject_t1756533147 * get_connectedTo_15() const { return ___connectedTo_15; }
	inline GameObject_t1756533147 ** get_address_of_connectedTo_15() { return &___connectedTo_15; }
	inline void set_connectedTo_15(GameObject_t1756533147 * value)
	{
		___connectedTo_15 = value;
		Il2CppCodeGenWriteBarrier(&___connectedTo_15, value);
	}

	inline static int32_t get_offset_of_direction_16() { return static_cast<int32_t>(offsetof(VRTK_Button_t855474128, ___direction_16)); }
	inline int32_t get_direction_16() const { return ___direction_16; }
	inline int32_t* get_address_of_direction_16() { return &___direction_16; }
	inline void set_direction_16(int32_t value)
	{
		___direction_16 = value;
	}

	inline static int32_t get_offset_of_activationDistance_17() { return static_cast<int32_t>(offsetof(VRTK_Button_t855474128, ___activationDistance_17)); }
	inline float get_activationDistance_17() const { return ___activationDistance_17; }
	inline float* get_address_of_activationDistance_17() { return &___activationDistance_17; }
	inline void set_activationDistance_17(float value)
	{
		___activationDistance_17 = value;
	}

	inline static int32_t get_offset_of_buttonStrength_18() { return static_cast<int32_t>(offsetof(VRTK_Button_t855474128, ___buttonStrength_18)); }
	inline float get_buttonStrength_18() const { return ___buttonStrength_18; }
	inline float* get_address_of_buttonStrength_18() { return &___buttonStrength_18; }
	inline void set_buttonStrength_18(float value)
	{
		___buttonStrength_18 = value;
	}

	inline static int32_t get_offset_of_events_19() { return static_cast<int32_t>(offsetof(VRTK_Button_t855474128, ___events_19)); }
	inline ButtonEvents_t99329243 * get_events_19() const { return ___events_19; }
	inline ButtonEvents_t99329243 ** get_address_of_events_19() { return &___events_19; }
	inline void set_events_19(ButtonEvents_t99329243 * value)
	{
		___events_19 = value;
		Il2CppCodeGenWriteBarrier(&___events_19, value);
	}

	inline static int32_t get_offset_of_Pushed_20() { return static_cast<int32_t>(offsetof(VRTK_Button_t855474128, ___Pushed_20)); }
	inline Button3DEventHandler_t1715584557 * get_Pushed_20() const { return ___Pushed_20; }
	inline Button3DEventHandler_t1715584557 ** get_address_of_Pushed_20() { return &___Pushed_20; }
	inline void set_Pushed_20(Button3DEventHandler_t1715584557 * value)
	{
		___Pushed_20 = value;
		Il2CppCodeGenWriteBarrier(&___Pushed_20, value);
	}

	inline static int32_t get_offset_of_finalDirection_22() { return static_cast<int32_t>(offsetof(VRTK_Button_t855474128, ___finalDirection_22)); }
	inline int32_t get_finalDirection_22() const { return ___finalDirection_22; }
	inline int32_t* get_address_of_finalDirection_22() { return &___finalDirection_22; }
	inline void set_finalDirection_22(int32_t value)
	{
		___finalDirection_22 = value;
	}

	inline static int32_t get_offset_of_restingPosition_23() { return static_cast<int32_t>(offsetof(VRTK_Button_t855474128, ___restingPosition_23)); }
	inline Vector3_t2243707580  get_restingPosition_23() const { return ___restingPosition_23; }
	inline Vector3_t2243707580 * get_address_of_restingPosition_23() { return &___restingPosition_23; }
	inline void set_restingPosition_23(Vector3_t2243707580  value)
	{
		___restingPosition_23 = value;
	}

	inline static int32_t get_offset_of_activationDir_24() { return static_cast<int32_t>(offsetof(VRTK_Button_t855474128, ___activationDir_24)); }
	inline Vector3_t2243707580  get_activationDir_24() const { return ___activationDir_24; }
	inline Vector3_t2243707580 * get_address_of_activationDir_24() { return &___activationDir_24; }
	inline void set_activationDir_24(Vector3_t2243707580  value)
	{
		___activationDir_24 = value;
	}

	inline static int32_t get_offset_of_buttonRigidbody_25() { return static_cast<int32_t>(offsetof(VRTK_Button_t855474128, ___buttonRigidbody_25)); }
	inline Rigidbody_t4233889191 * get_buttonRigidbody_25() const { return ___buttonRigidbody_25; }
	inline Rigidbody_t4233889191 ** get_address_of_buttonRigidbody_25() { return &___buttonRigidbody_25; }
	inline void set_buttonRigidbody_25(Rigidbody_t4233889191 * value)
	{
		___buttonRigidbody_25 = value;
		Il2CppCodeGenWriteBarrier(&___buttonRigidbody_25, value);
	}

	inline static int32_t get_offset_of_buttonJoint_26() { return static_cast<int32_t>(offsetof(VRTK_Button_t855474128, ___buttonJoint_26)); }
	inline ConfigurableJoint_t454307495 * get_buttonJoint_26() const { return ___buttonJoint_26; }
	inline ConfigurableJoint_t454307495 ** get_address_of_buttonJoint_26() { return &___buttonJoint_26; }
	inline void set_buttonJoint_26(ConfigurableJoint_t454307495 * value)
	{
		___buttonJoint_26 = value;
		Il2CppCodeGenWriteBarrier(&___buttonJoint_26, value);
	}

	inline static int32_t get_offset_of_buttonForce_27() { return static_cast<int32_t>(offsetof(VRTK_Button_t855474128, ___buttonForce_27)); }
	inline ConstantForce_t3796310167 * get_buttonForce_27() const { return ___buttonForce_27; }
	inline ConstantForce_t3796310167 ** get_address_of_buttonForce_27() { return &___buttonForce_27; }
	inline void set_buttonForce_27(ConstantForce_t3796310167 * value)
	{
		___buttonForce_27 = value;
		Il2CppCodeGenWriteBarrier(&___buttonForce_27, value);
	}

	inline static int32_t get_offset_of_forceCount_28() { return static_cast<int32_t>(offsetof(VRTK_Button_t855474128, ___forceCount_28)); }
	inline int32_t get_forceCount_28() const { return ___forceCount_28; }
	inline int32_t* get_address_of_forceCount_28() { return &___forceCount_28; }
	inline void set_forceCount_28(int32_t value)
	{
		___forceCount_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
