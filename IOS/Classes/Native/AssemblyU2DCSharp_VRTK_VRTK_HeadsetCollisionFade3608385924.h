﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_HeadsetCollision
struct VRTK_HeadsetCollision_t2015187094;
// VRTK.VRTK_HeadsetFade
struct VRTK_HeadsetFade_t3539061086;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_HeadsetCollisionFade
struct  VRTK_HeadsetCollisionFade_t3608385924  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VRTK.VRTK_HeadsetCollisionFade::blinkTransitionSpeed
	float ___blinkTransitionSpeed_2;
	// UnityEngine.Color VRTK.VRTK_HeadsetCollisionFade::fadeColor
	Color_t2020392075  ___fadeColor_3;
	// VRTK.VRTK_HeadsetCollision VRTK.VRTK_HeadsetCollisionFade::headsetCollision
	VRTK_HeadsetCollision_t2015187094 * ___headsetCollision_4;
	// VRTK.VRTK_HeadsetFade VRTK.VRTK_HeadsetCollisionFade::headsetFade
	VRTK_HeadsetFade_t3539061086 * ___headsetFade_5;

public:
	inline static int32_t get_offset_of_blinkTransitionSpeed_2() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollisionFade_t3608385924, ___blinkTransitionSpeed_2)); }
	inline float get_blinkTransitionSpeed_2() const { return ___blinkTransitionSpeed_2; }
	inline float* get_address_of_blinkTransitionSpeed_2() { return &___blinkTransitionSpeed_2; }
	inline void set_blinkTransitionSpeed_2(float value)
	{
		___blinkTransitionSpeed_2 = value;
	}

	inline static int32_t get_offset_of_fadeColor_3() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollisionFade_t3608385924, ___fadeColor_3)); }
	inline Color_t2020392075  get_fadeColor_3() const { return ___fadeColor_3; }
	inline Color_t2020392075 * get_address_of_fadeColor_3() { return &___fadeColor_3; }
	inline void set_fadeColor_3(Color_t2020392075  value)
	{
		___fadeColor_3 = value;
	}

	inline static int32_t get_offset_of_headsetCollision_4() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollisionFade_t3608385924, ___headsetCollision_4)); }
	inline VRTK_HeadsetCollision_t2015187094 * get_headsetCollision_4() const { return ___headsetCollision_4; }
	inline VRTK_HeadsetCollision_t2015187094 ** get_address_of_headsetCollision_4() { return &___headsetCollision_4; }
	inline void set_headsetCollision_4(VRTK_HeadsetCollision_t2015187094 * value)
	{
		___headsetCollision_4 = value;
		Il2CppCodeGenWriteBarrier(&___headsetCollision_4, value);
	}

	inline static int32_t get_offset_of_headsetFade_5() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollisionFade_t3608385924, ___headsetFade_5)); }
	inline VRTK_HeadsetFade_t3539061086 * get_headsetFade_5() const { return ___headsetFade_5; }
	inline VRTK_HeadsetFade_t3539061086 ** get_address_of_headsetFade_5() { return &___headsetFade_5; }
	inline void set_headsetFade_5(VRTK_HeadsetFade_t3539061086 * value)
	{
		___headsetFade_5 = value;
		Il2CppCodeGenWriteBarrier(&___headsetFade_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
