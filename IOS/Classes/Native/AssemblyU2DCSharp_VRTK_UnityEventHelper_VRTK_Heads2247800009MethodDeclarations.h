﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents
struct VRTK_HeadsetFade_UnityEvents_t2247800009;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetFadeEventArgs2892542019.h"

// System.Void VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents::.ctor()
extern "C"  void VRTK_HeadsetFade_UnityEvents__ctor_m2285515244 (VRTK_HeadsetFade_UnityEvents_t2247800009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents::SetHeadsetFade()
extern "C"  void VRTK_HeadsetFade_UnityEvents_SetHeadsetFade_m3831984682 (VRTK_HeadsetFade_UnityEvents_t2247800009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents::OnEnable()
extern "C"  void VRTK_HeadsetFade_UnityEvents_OnEnable_m3566350656 (VRTK_HeadsetFade_UnityEvents_t2247800009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents::HeadsetFadeStart(System.Object,VRTK.HeadsetFadeEventArgs)
extern "C"  void VRTK_HeadsetFade_UnityEvents_HeadsetFadeStart_m510419622 (VRTK_HeadsetFade_UnityEvents_t2247800009 * __this, Il2CppObject * ___o0, HeadsetFadeEventArgs_t2892542019  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents::HeadsetFadeComplete(System.Object,VRTK.HeadsetFadeEventArgs)
extern "C"  void VRTK_HeadsetFade_UnityEvents_HeadsetFadeComplete_m3000119267 (VRTK_HeadsetFade_UnityEvents_t2247800009 * __this, Il2CppObject * ___o0, HeadsetFadeEventArgs_t2892542019  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents::HeadsetUnfadeStart(System.Object,VRTK.HeadsetFadeEventArgs)
extern "C"  void VRTK_HeadsetFade_UnityEvents_HeadsetUnfadeStart_m3592874817 (VRTK_HeadsetFade_UnityEvents_t2247800009 * __this, Il2CppObject * ___o0, HeadsetFadeEventArgs_t2892542019  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents::HeadsetUnfadeComplete(System.Object,VRTK.HeadsetFadeEventArgs)
extern "C"  void VRTK_HeadsetFade_UnityEvents_HeadsetUnfadeComplete_m560293988 (VRTK_HeadsetFade_UnityEvents_t2247800009 * __this, Il2CppObject * ___o0, HeadsetFadeEventArgs_t2892542019  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_HeadsetFade_UnityEvents::OnDisable()
extern "C"  void VRTK_HeadsetFade_UnityEvents_OnDisable_m3554262435 (VRTK_HeadsetFade_UnityEvents_t2247800009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
