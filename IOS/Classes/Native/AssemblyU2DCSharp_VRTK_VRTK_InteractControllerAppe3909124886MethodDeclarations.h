﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_InteractControllerAppearance
struct VRTK_InteractControllerAppearance_t3909124886;
// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerActions3642353851.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.VRTK_InteractControllerAppearance::.ctor()
extern "C"  void VRTK_InteractControllerAppearance__ctor_m2412692844 (VRTK_InteractControllerAppearance_t3909124886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractControllerAppearance::ToggleControllerOnTouch(System.Boolean,VRTK.VRTK_ControllerActions,UnityEngine.GameObject)
extern "C"  void VRTK_InteractControllerAppearance_ToggleControllerOnTouch_m897603683 (VRTK_InteractControllerAppearance_t3909124886 * __this, bool ___showController0, VRTK_ControllerActions_t3642353851 * ___controllerActions1, GameObject_t1756533147 * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractControllerAppearance::ToggleControllerOnGrab(System.Boolean,VRTK.VRTK_ControllerActions,UnityEngine.GameObject)
extern "C"  void VRTK_InteractControllerAppearance_ToggleControllerOnGrab_m1110076432 (VRTK_InteractControllerAppearance_t3909124886 * __this, bool ___showController0, VRTK_ControllerActions_t3642353851 * ___controllerActions1, GameObject_t1756533147 * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractControllerAppearance::ToggleControllerOnUse(System.Boolean,VRTK.VRTK_ControllerActions,UnityEngine.GameObject)
extern "C"  void VRTK_InteractControllerAppearance_ToggleControllerOnUse_m2064808953 (VRTK_InteractControllerAppearance_t3909124886 * __this, bool ___showController0, VRTK_ControllerActions_t3642353851 * ___controllerActions1, GameObject_t1756533147 * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractControllerAppearance::OnEnable()
extern "C"  void VRTK_InteractControllerAppearance_OnEnable_m2034261344 (VRTK_InteractControllerAppearance_t3909124886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractControllerAppearance::ToggleController(System.Boolean,VRTK.VRTK_ControllerActions,UnityEngine.GameObject,System.Single)
extern "C"  void VRTK_InteractControllerAppearance_ToggleController_m2866532346 (VRTK_InteractControllerAppearance_t3909124886 * __this, bool ___showController0, VRTK_ControllerActions_t3642353851 * ___controllerActions1, GameObject_t1756533147 * ___obj2, float ___touchDelay3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractControllerAppearance::ShowController(VRTK.VRTK_ControllerActions,UnityEngine.GameObject)
extern "C"  void VRTK_InteractControllerAppearance_ShowController_m1347769797 (VRTK_InteractControllerAppearance_t3909124886 * __this, VRTK_ControllerActions_t3642353851 * ___controllerActions0, GameObject_t1756533147 * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractControllerAppearance::HideController()
extern "C"  void VRTK_InteractControllerAppearance_HideController_m3919572646 (VRTK_InteractControllerAppearance_t3909124886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
