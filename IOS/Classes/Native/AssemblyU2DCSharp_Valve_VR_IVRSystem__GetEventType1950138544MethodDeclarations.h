﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetEventTypeNameFromEnum
struct _GetEventTypeNameFromEnum_t1950138544;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREventType6846875.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetEventTypeNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetEventTypeNameFromEnum__ctor_m219289187 (_GetEventTypeNameFromEnum_t1950138544 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRSystem/_GetEventTypeNameFromEnum::Invoke(Valve.VR.EVREventType)
extern "C"  IntPtr_t _GetEventTypeNameFromEnum_Invoke_m2044523695 (_GetEventTypeNameFromEnum_t1950138544 * __this, int32_t ___eType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetEventTypeNameFromEnum::BeginInvoke(Valve.VR.EVREventType,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetEventTypeNameFromEnum_BeginInvoke_m297886973 (_GetEventTypeNameFromEnum_t1950138544 * __this, int32_t ___eType0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRSystem/_GetEventTypeNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetEventTypeNameFromEnum_EndInvoke_m3635183318 (_GetEventTypeNameFromEnum_t1950138544 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
