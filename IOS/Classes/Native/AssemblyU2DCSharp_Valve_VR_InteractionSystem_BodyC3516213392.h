﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t720607407;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.BodyCollider
struct  BodyCollider_t3516213392  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Valve.VR.InteractionSystem.BodyCollider::head
	Transform_t3275118058 * ___head_2;
	// UnityEngine.CapsuleCollider Valve.VR.InteractionSystem.BodyCollider::capsuleCollider
	CapsuleCollider_t720607407 * ___capsuleCollider_3;

public:
	inline static int32_t get_offset_of_head_2() { return static_cast<int32_t>(offsetof(BodyCollider_t3516213392, ___head_2)); }
	inline Transform_t3275118058 * get_head_2() const { return ___head_2; }
	inline Transform_t3275118058 ** get_address_of_head_2() { return &___head_2; }
	inline void set_head_2(Transform_t3275118058 * value)
	{
		___head_2 = value;
		Il2CppCodeGenWriteBarrier(&___head_2, value);
	}

	inline static int32_t get_offset_of_capsuleCollider_3() { return static_cast<int32_t>(offsetof(BodyCollider_t3516213392, ___capsuleCollider_3)); }
	inline CapsuleCollider_t720607407 * get_capsuleCollider_3() const { return ___capsuleCollider_3; }
	inline CapsuleCollider_t720607407 ** get_address_of_capsuleCollider_3() { return &___capsuleCollider_3; }
	inline void set_capsuleCollider_3(CapsuleCollider_t720607407 * value)
	{
		___capsuleCollider_3 = value;
		Il2CppCodeGenWriteBarrier(&___capsuleCollider_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
