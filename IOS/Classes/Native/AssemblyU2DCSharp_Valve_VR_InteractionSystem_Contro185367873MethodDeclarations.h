﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ControllerButtonHints/<TestButtonHints>c__Iterator0
struct U3CTestButtonHintsU3Ec__Iterator0_t185367873;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.ControllerButtonHints/<TestButtonHints>c__Iterator0::.ctor()
extern "C"  void U3CTestButtonHintsU3Ec__Iterator0__ctor_m487065822 (U3CTestButtonHintsU3Ec__Iterator0_t185367873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.ControllerButtonHints/<TestButtonHints>c__Iterator0::MoveNext()
extern "C"  bool U3CTestButtonHintsU3Ec__Iterator0_MoveNext_m1947719790 (U3CTestButtonHintsU3Ec__Iterator0_t185367873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.ControllerButtonHints/<TestButtonHints>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTestButtonHintsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m738063402 (U3CTestButtonHintsU3Ec__Iterator0_t185367873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.ControllerButtonHints/<TestButtonHints>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTestButtonHintsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3824415650 (U3CTestButtonHintsU3Ec__Iterator0_t185367873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints/<TestButtonHints>c__Iterator0::Dispose()
extern "C"  void U3CTestButtonHintsU3Ec__Iterator0_Dispose_m2914478835 (U3CTestButtonHintsU3Ec__Iterator0_t185367873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerButtonHints/<TestButtonHints>c__Iterator0::Reset()
extern "C"  void U3CTestButtonHintsU3Ec__Iterator0_Reset_m3334010781 (U3CTestButtonHintsU3Ec__Iterator0_t185367873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
