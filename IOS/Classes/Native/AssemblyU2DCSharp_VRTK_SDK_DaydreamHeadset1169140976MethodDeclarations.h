﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_DaydreamHeadset
struct SDK_DaydreamHeadset_t1169140976;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_DaydreamHeadset::.ctor()
extern "C"  void SDK_DaydreamHeadset__ctor_m4153166064 (SDK_DaydreamHeadset_t1169140976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
