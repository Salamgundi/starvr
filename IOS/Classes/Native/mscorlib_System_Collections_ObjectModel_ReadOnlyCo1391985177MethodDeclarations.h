﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2875234987MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m912129762(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1391985177 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3671019970_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m944015126(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1391985177 *, VRTK_DashTeleport_t1206199485 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2989589458_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3650889754(__this, method) ((  void (*) (ReadOnlyCollection_1_t1391985177 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m454937302_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1392671495(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1391985177 *, int32_t, VRTK_DashTeleport_t1206199485 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4272763307_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3748024463(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1391985177 *, VRTK_DashTeleport_t1206199485 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3199809075_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1829088499(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1391985177 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m962041751_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3661223951(__this, ___index0, method) ((  VRTK_DashTeleport_t1206199485 * (*) (ReadOnlyCollection_1_t1391985177 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m70085287_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m315268380(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1391985177 *, int32_t, VRTK_DashTeleport_t1206199485 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1547026160_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1157688820(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1391985177 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4041967064_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2814693661(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1391985177 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3664791405_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1437824972(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1391985177 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m531171980_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1291505997(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1391985177 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3780136817_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2094242597(__this, method) ((  void (*) (ReadOnlyCollection_1_t1391985177 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3983677501_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1084753197(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1391985177 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1990607517_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3689076451(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1391985177 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m606942423_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m704843470(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1391985177 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m691705570_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1335187564(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1391985177 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3182494192_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2296170240(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1391985177 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m572840272_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1914539321(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1391985177 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2871048729_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m263443793(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1391985177 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m769863805_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2110746310(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1391985177 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m942145650_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m431871781(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1391985177 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1367736517_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1622140500(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1391985177 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3336878134_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2335436459(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1391985177 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1799572719_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::Contains(T)
#define ReadOnlyCollection_1_Contains_m1771912368(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1391985177 *, VRTK_DashTeleport_t1206199485 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m1227826160_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m646303090(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1391985177 *, VRTK_DashTeleportU5BU5D_t249038768*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m4257276542_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m2519556125(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1391985177 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1627519329_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m375853188(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1391985177 *, VRTK_DashTeleport_t1206199485 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1981423404_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::get_Count()
#define ReadOnlyCollection_1_get_Count_m3073981565(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1391985177 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2562379905_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<VRTK.VRTK_DashTeleport>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m3449100347(__this, ___index0, method) ((  VRTK_DashTeleport_t1206199485 * (*) (ReadOnlyCollection_1_t1391985177 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m191392387_gshared)(__this, ___index0, method)
