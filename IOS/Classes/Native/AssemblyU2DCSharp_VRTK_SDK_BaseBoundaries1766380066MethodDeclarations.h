﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_BaseBoundaries
struct SDK_BaseBoundaries_t1766380066;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_BaseBoundaries::.ctor()
extern "C"  void SDK_BaseBoundaries__ctor_m122228114 (SDK_BaseBoundaries_t1766380066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.SDK_BaseBoundaries::GetSDKManagerPlayArea()
extern "C"  Transform_t3275118058 * SDK_BaseBoundaries_GetSDKManagerPlayArea_m2513561328 (SDK_BaseBoundaries_t1766380066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
