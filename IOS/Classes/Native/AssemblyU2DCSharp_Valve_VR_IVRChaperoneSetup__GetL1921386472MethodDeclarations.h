﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_GetLiveCollisionBoundsInfo
struct _GetLiveCollisionBoundsInfo_t1921386472;
// System.Object
struct Il2CppObject;
// Valve.VR.HmdQuad_t[]
struct HmdQuad_tU5BU5D_t16941492;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_GetLiveCollisionBoundsInfo::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetLiveCollisionBoundsInfo__ctor_m4227103505 (_GetLiveCollisionBoundsInfo_t1921386472 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetLiveCollisionBoundsInfo::Invoke(Valve.VR.HmdQuad_t[],System.UInt32&)
extern "C"  bool _GetLiveCollisionBoundsInfo_Invoke_m1340516592 (_GetLiveCollisionBoundsInfo_t1921386472 * __this, HmdQuad_tU5BU5D_t16941492* ___pQuadsBuffer0, uint32_t* ___punQuadsCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_GetLiveCollisionBoundsInfo::BeginInvoke(Valve.VR.HmdQuad_t[],System.UInt32&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetLiveCollisionBoundsInfo_BeginInvoke_m4240407417 (_GetLiveCollisionBoundsInfo_t1921386472 * __this, HmdQuad_tU5BU5D_t16941492* ___pQuadsBuffer0, uint32_t* ___punQuadsCount1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetLiveCollisionBoundsInfo::EndInvoke(System.UInt32&,System.IAsyncResult)
extern "C"  bool _GetLiveCollisionBoundsInfo_EndInvoke_m3066079541 (_GetLiveCollisionBoundsInfo_t1921386472 * __this, uint32_t* ___punQuadsCount0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
