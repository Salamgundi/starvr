﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_TeleportDisableOnHeadsetCollision
struct VRTK_TeleportDisableOnHeadsetCollision_t56384818;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventArgs1242373387.h"

// System.Void VRTK.VRTK_TeleportDisableOnHeadsetCollision::.ctor()
extern "C"  void VRTK_TeleportDisableOnHeadsetCollision__ctor_m2606520156 (VRTK_TeleportDisableOnHeadsetCollision_t56384818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TeleportDisableOnHeadsetCollision::OnEnable()
extern "C"  void VRTK_TeleportDisableOnHeadsetCollision_OnEnable_m3883689824 (VRTK_TeleportDisableOnHeadsetCollision_t56384818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TeleportDisableOnHeadsetCollision::OnDisable()
extern "C"  void VRTK_TeleportDisableOnHeadsetCollision_OnDisable_m1482005215 (VRTK_TeleportDisableOnHeadsetCollision_t56384818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_TeleportDisableOnHeadsetCollision::EnableAtEndOfFrame()
extern "C"  Il2CppObject * VRTK_TeleportDisableOnHeadsetCollision_EnableAtEndOfFrame_m3908512027 (VRTK_TeleportDisableOnHeadsetCollision_t56384818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TeleportDisableOnHeadsetCollision::DisableTeleport(System.Object,VRTK.HeadsetCollisionEventArgs)
extern "C"  void VRTK_TeleportDisableOnHeadsetCollision_DisableTeleport_m2051254723 (VRTK_TeleportDisableOnHeadsetCollision_t56384818 * __this, Il2CppObject * ___sender0, HeadsetCollisionEventArgs_t1242373387  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TeleportDisableOnHeadsetCollision::EnableTeleport(System.Object,VRTK.HeadsetCollisionEventArgs)
extern "C"  void VRTK_TeleportDisableOnHeadsetCollision_EnableTeleport_m2564996272 (VRTK_TeleportDisableOnHeadsetCollision_t56384818 * __this, Il2CppObject * ___sender0, HeadsetCollisionEventArgs_t1242373387  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
