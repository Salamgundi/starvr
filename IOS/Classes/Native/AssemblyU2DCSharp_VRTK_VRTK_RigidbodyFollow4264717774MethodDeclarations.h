﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_RigidbodyFollow
struct VRTK_RigidbodyFollow_t4264717774;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// System.Void VRTK.VRTK_RigidbodyFollow::.ctor()
extern "C"  void VRTK_RigidbodyFollow__ctor_m2423983988 (VRTK_RigidbodyFollow_t4264717774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RigidbodyFollow::OnEnable()
extern "C"  void VRTK_RigidbodyFollow_OnEnable_m1015202984 (VRTK_RigidbodyFollow_t4264717774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RigidbodyFollow::OnDisable()
extern "C"  void VRTK_RigidbodyFollow_OnDisable_m2370896359 (VRTK_RigidbodyFollow_t4264717774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RigidbodyFollow::FixedUpdate()
extern "C"  void VRTK_RigidbodyFollow_FixedUpdate_m2999609849 (VRTK_RigidbodyFollow_t4264717774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_RigidbodyFollow::GetPositionToFollow()
extern "C"  Vector3_t2243707580  VRTK_RigidbodyFollow_GetPositionToFollow_m1435089593 (VRTK_RigidbodyFollow_t4264717774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RigidbodyFollow::SetPositionOnGameObject(UnityEngine.Vector3)
extern "C"  void VRTK_RigidbodyFollow_SetPositionOnGameObject_m2255093188 (VRTK_RigidbodyFollow_t4264717774 * __this, Vector3_t2243707580  ___newPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion VRTK.VRTK_RigidbodyFollow::GetRotationToFollow()
extern "C"  Quaternion_t4030073918  VRTK_RigidbodyFollow_GetRotationToFollow_m2395458184 (VRTK_RigidbodyFollow_t4264717774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RigidbodyFollow::SetRotationOnGameObject(UnityEngine.Quaternion)
extern "C"  void VRTK_RigidbodyFollow_SetRotationOnGameObject_m3732069889 (VRTK_RigidbodyFollow_t4264717774 * __this, Quaternion_t4030073918  ___newRotation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_RigidbodyFollow::GetScaleToFollow()
extern "C"  Vector3_t2243707580  VRTK_RigidbodyFollow_GetScaleToFollow_m2410737966 (VRTK_RigidbodyFollow_t4264717774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
