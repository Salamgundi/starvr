﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_SimplePointer
struct VRTK_SimplePointer_t1980002747;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"

// System.Void VRTK.VRTK_SimplePointer::.ctor()
extern "C"  void VRTK_SimplePointer__ctor_m4210864539 (VRTK_SimplePointer_t1980002747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SimplePointer::OnEnable()
extern "C"  void VRTK_SimplePointer_OnEnable_m268971039 (VRTK_SimplePointer_t1980002747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SimplePointer::OnDisable()
extern "C"  void VRTK_SimplePointer_OnDisable_m2829408576 (VRTK_SimplePointer_t1980002747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SimplePointer::Update()
extern "C"  void VRTK_SimplePointer_Update_m3502235686 (VRTK_SimplePointer_t1980002747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SimplePointer::UpdateObjectInteractor()
extern "C"  void VRTK_SimplePointer_UpdateObjectInteractor_m4011910114 (VRTK_SimplePointer_t1980002747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SimplePointer::InitPointer()
extern "C"  void VRTK_SimplePointer_InitPointer_m451186740 (VRTK_SimplePointer_t1980002747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SimplePointer::SetPointerMaterial(UnityEngine.Color)
extern "C"  void VRTK_SimplePointer_SetPointerMaterial_m3602887115 (VRTK_SimplePointer_t1980002747 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SimplePointer::TogglePointer(System.Boolean)
extern "C"  void VRTK_SimplePointer_TogglePointer_m1190121949 (VRTK_SimplePointer_t1980002747 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SimplePointer::ResizeObjectInteractor()
extern "C"  void VRTK_SimplePointer_ResizeObjectInteractor_m2179417819 (VRTK_SimplePointer_t1980002747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SimplePointer::SetPointerTransform(System.Single,System.Single)
extern "C"  void VRTK_SimplePointer_SetPointerTransform_m4251954758 (VRTK_SimplePointer_t1980002747 * __this, float ___setLength0, float ___setThicknes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_SimplePointer::GetPointerBeamLength(System.Boolean,UnityEngine.RaycastHit)
extern "C"  float VRTK_SimplePointer_GetPointerBeamLength_m2649734465 (VRTK_SimplePointer_t1980002747 * __this, bool ___hasRayHit0, RaycastHit_t87180320  ___collidedWith1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
