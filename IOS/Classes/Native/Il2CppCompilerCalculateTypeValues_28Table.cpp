﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_RadialMenuButton131156040.h"
#include "AssemblyU2DCSharp_VRTK_ButtonEvent2865057236.h"
#include "AssemblyU2DCSharp_VRTK_UICircle3209742644.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_IndependentRadialMenuC4268715152.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_IndependentRadialMenuCon20279770.h"
#include "AssemblyU2DCSharp_VRTK_RadialMenuController89584558.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ContentHandler3161683817.h"
#include "AssemblyU2DCSharp_VRTK_Button3DEventHandler1715584557.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Button855474128.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Button_ButtonEvents99329243.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Button_ButtonDirection1346461537.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Chest1253706853.h"
#include "AssemblyU2DCSharp_VRTK_Control3DEventArgs4095025701.h"
#include "AssemblyU2DCSharp_VRTK_Control3DEventHandler2392187186.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control651619021.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_ValueChangedEv4219324264.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_DefaultControl3441053308.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_ControlValueRa2976216666.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_Direction3775008092.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Door1992640360.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Drawer1874276415.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Knob1401139190.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Knob_KnobDirection4203016241.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Lever736126558.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Lever_LeverDirection1799098973.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Slider2095192727.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SpringLever2820350835.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Wheel803354859.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Wheel_GrabTypes504515700.h"
#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Ba3487134318.h"
#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Ba1229653768.h"
#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Ch1018385220.h"
#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Cl3258016228.h"
#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Cu1230827278.h"
#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Fi3523806925.h"
#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Ro3062106299.h"
#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Sp1884620216.h"
#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Tr1133162717.h"
#include "AssemblyU2DCSharp_VRTK_Highlighters_VRTK_BaseHighl3110203740.h"
#include "AssemblyU2DCSharp_VRTK_Highlighters_VRTK_MaterialC2305160438.h"
#include "AssemblyU2DCSharp_VRTK_Highlighters_VRTK_MaterialCo125975806.h"
#include "AssemblyU2DCSharp_VRTK_Highlighters_VRTK_MaterialP2327852568.h"
#include "AssemblyU2DCSharp_VRTK_Highlighters_VRTK_MaterialP1494384812.h"
#include "AssemblyU2DCSharp_VRTK_Highlighters_VRTK_OutlineObj668900239.h"
#include "AssemblyU2DCSharp_VRTK_SecondaryControllerGrabActi2817064065.h"
#include "AssemblyU2DCSharp_VRTK_SecondaryControllerGrabActi4095736311.h"
#include "AssemblyU2DCSharp_VRTK_SecondaryControllerGrabActi1241164474.h"
#include "AssemblyU2DCSharp_VRTK_SecondaryControllerGrabActio560927000.h"
#include "AssemblyU2DCSharp_VRTK_SecondaryControllerGrabActio918155359.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerModelElementP672993657.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerElementHighl1898231604.h"
#include "AssemblyU2DCSharp_VRTK_ControllerActionsEventArgs344001476.h"
#include "AssemblyU2DCSharp_VRTK_ControllerActionsEventHandl1521142243.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerActions3642353851.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerActions_U3CW2574180537.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerActions_U3CHa288649631.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerActions_U3CC3567685877.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventHa343979916.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents3225224819.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractControllerAppe3909124886.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractGrab124353446.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractHaptics1219060492.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventArgs771291242.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventHandler1701902511.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractTouch4022091061.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractUse4015307561.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventHandl940909295.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject_All2794712781.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject_Vali960740973.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject_U3C1752167516.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject_U3C4046280006.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectAutoGrab2748125822.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectAutoGrab_U3CAuto1000106702.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerTracker705570086.h"
#include "AssemblyU2DCSharp_VRTK_Bezier3037471005.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_CurveGenerator3769661606.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_CurveGenerator_BezierCo855273711.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_EventSystem3222336529.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectCache513154459.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PlayerObject502441292.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PlayerObject_ObjectTyp2764484032.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_RoomExtender_PlayAreaG1655168566.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ScreenFade2079719832.h"
#include "AssemblyU2DCSharp_VRTK_VRTKTrackedControllerEventA2407378264.h"
#include "AssemblyU2DCSharp_VRTK_VRTKTrackedControllerEventH2437916365.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TrackedController520756048.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TrackedController_U3CE1493054694.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TrackedHeadset3483597430.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIGraphicRaycaster2816944648.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_VRInputModule1472500726.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BaseObjectControlActio3375001897.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BaseObjectControlAction727620975.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_RotateObjectControlAct2035674631.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SlideObjectControlActio294667339.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SnapRotateObjectContro1299063727.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_WarpObjectControlActio3039272518.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (RadialMenuButton_t131156040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2800[5] = 
{
	RadialMenuButton_t131156040::get_offset_of_ButtonIcon_0(),
	RadialMenuButton_t131156040::get_offset_of_OnClick_1(),
	RadialMenuButton_t131156040::get_offset_of_OnHold_2(),
	RadialMenuButton_t131156040::get_offset_of_OnHoverEnter_3(),
	RadialMenuButton_t131156040::get_offset_of_OnHoverExit_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (ButtonEvent_t2865057236)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2801[5] = 
{
	ButtonEvent_t2865057236::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (UICircle_t3209742644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2802[5] = 
{
	UICircle_t3209742644::get_offset_of_fillPercent_19(),
	UICircle_t3209742644::get_offset_of_fill_20(),
	UICircle_t3209742644::get_offset_of_thickness_21(),
	UICircle_t3209742644::get_offset_of_segments_22(),
	UICircle_t3209742644::get_offset_of_m_Texture_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (VRTK_IndependentRadialMenuController_t4268715152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2803[15] = 
{
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_eventsManager_6(),
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_addMenuCollider_7(),
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_colliderRadiusMultiplier_8(),
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_hideAfterExecution_9(),
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_offsetMultiplier_10(),
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_rotateTowards_11(),
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_interactingObjects_12(),
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_collidingObjects_13(),
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_menuCollider_14(),
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_disableCoroutine_15(),
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_desiredColliderCenter_16(),
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_initialRotation_17(),
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_isClicked_18(),
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_waitingToDisableCollider_19(),
	VRTK_IndependentRadialMenuController_t4268715152::get_offset_of_counter_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2804[7] = 
{
	U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770::get_offset_of_delay_0(),
	U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770::get_offset_of_enabled_1(),
	U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770::get_offset_of_e_2(),
	U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770::get_offset_of_U24this_3(),
	U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770::get_offset_of_U24current_4(),
	U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770::get_offset_of_U24disposing_5(),
	U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (RadialMenuController_t89584558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2805[4] = 
{
	RadialMenuController_t89584558::get_offset_of_events_2(),
	RadialMenuController_t89584558::get_offset_of_menu_3(),
	RadialMenuController_t89584558::get_offset_of_currentAngle_4(),
	RadialMenuController_t89584558::get_offset_of_touchpadTouched_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (VRTK_ContentHandler_t3161683817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2806[3] = 
{
	VRTK_ContentHandler_t3161683817::get_offset_of_control_2(),
	VRTK_ContentHandler_t3161683817::get_offset_of_inside_3(),
	VRTK_ContentHandler_t3161683817::get_offset_of_outside_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (Button3DEventHandler_t1715584557), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (VRTK_Button_t855474128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2808[14] = 
{
	VRTK_Button_t855474128::get_offset_of_connectedTo_15(),
	VRTK_Button_t855474128::get_offset_of_direction_16(),
	VRTK_Button_t855474128::get_offset_of_activationDistance_17(),
	VRTK_Button_t855474128::get_offset_of_buttonStrength_18(),
	VRTK_Button_t855474128::get_offset_of_events_19(),
	VRTK_Button_t855474128::get_offset_of_Pushed_20(),
	0,
	VRTK_Button_t855474128::get_offset_of_finalDirection_22(),
	VRTK_Button_t855474128::get_offset_of_restingPosition_23(),
	VRTK_Button_t855474128::get_offset_of_activationDir_24(),
	VRTK_Button_t855474128::get_offset_of_buttonRigidbody_25(),
	VRTK_Button_t855474128::get_offset_of_buttonJoint_26(),
	VRTK_Button_t855474128::get_offset_of_buttonForce_27(),
	VRTK_Button_t855474128::get_offset_of_forceCount_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (ButtonEvents_t99329243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[1] = 
{
	ButtonEvents_t99329243::get_offset_of_OnPush_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (ButtonDirection_t1346461537)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2810[8] = 
{
	ButtonDirection_t1346461537::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (VRTK_Chest_t1253706853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2811[17] = 
{
	VRTK_Chest_t1253706853::get_offset_of_direction_15(),
	VRTK_Chest_t1253706853::get_offset_of_lid_16(),
	VRTK_Chest_t1253706853::get_offset_of_body_17(),
	VRTK_Chest_t1253706853::get_offset_of_handle_18(),
	VRTK_Chest_t1253706853::get_offset_of_content_19(),
	VRTK_Chest_t1253706853::get_offset_of_hideContent_20(),
	VRTK_Chest_t1253706853::get_offset_of_maxAngle_21(),
	VRTK_Chest_t1253706853::get_offset_of_minAngle_22(),
	VRTK_Chest_t1253706853::get_offset_of_stepSize_23(),
	VRTK_Chest_t1253706853::get_offset_of_bodyRigidbody_24(),
	VRTK_Chest_t1253706853::get_offset_of_handleRigidbody_25(),
	VRTK_Chest_t1253706853::get_offset_of_handleJoint_26(),
	VRTK_Chest_t1253706853::get_offset_of_lidRigidbody_27(),
	VRTK_Chest_t1253706853::get_offset_of_lidJoint_28(),
	VRTK_Chest_t1253706853::get_offset_of_lidJointCreated_29(),
	VRTK_Chest_t1253706853::get_offset_of_finalDirection_30(),
	VRTK_Chest_t1253706853::get_offset_of_subDirection_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (Control3DEventArgs_t4095025701)+ sizeof (Il2CppObject), sizeof(Control3DEventArgs_t4095025701 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2812[2] = 
{
	Control3DEventArgs_t4095025701::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Control3DEventArgs_t4095025701::get_offset_of_normalizedValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (Control3DEventHandler_t2392187186), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (VRTK_Control_t651619021), -1, sizeof(VRTK_Control_t651619021_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2814[13] = 
{
	VRTK_Control_t651619021::get_offset_of_defaultEvents_2(),
	VRTK_Control_t651619021::get_offset_of_interactWithoutGrab_3(),
	VRTK_Control_t651619021::get_offset_of_ValueChanged_4(),
	VRTK_Control_t651619021::get_offset_of_bounds_5(),
	VRTK_Control_t651619021::get_offset_of_setupSuccessful_6(),
	VRTK_Control_t651619021::get_offset_of_autoTriggerVolume_7(),
	VRTK_Control_t651619021::get_offset_of_value_8(),
	VRTK_Control_t651619021_StaticFields::get_offset_of_COLOR_OK_9(),
	VRTK_Control_t651619021_StaticFields::get_offset_of_COLOR_ERROR_10(),
	0,
	VRTK_Control_t651619021::get_offset_of_valueRange_12(),
	VRTK_Control_t651619021::get_offset_of_controlContent_13(),
	VRTK_Control_t651619021::get_offset_of_hideControlContent_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (ValueChangedEvent_t4219324264), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (DefaultControlEvents_t3441053308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2816[1] = 
{
	DefaultControlEvents_t3441053308::get_offset_of_OnValueChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (ControlValueRange_t2976216666)+ sizeof (Il2CppObject), sizeof(ControlValueRange_t2976216666 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2817[2] = 
{
	ControlValueRange_t2976216666::get_offset_of_controlMin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControlValueRange_t2976216666::get_offset_of_controlMax_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (Direction_t3775008092)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2818[5] = 
{
	Direction_t3775008092::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (VRTK_Door_t1992640360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2819[23] = 
{
	VRTK_Door_t1992640360::get_offset_of_direction_15(),
	VRTK_Door_t1992640360::get_offset_of_door_16(),
	VRTK_Door_t1992640360::get_offset_of_handles_17(),
	VRTK_Door_t1992640360::get_offset_of_frame_18(),
	VRTK_Door_t1992640360::get_offset_of_content_19(),
	VRTK_Door_t1992640360::get_offset_of_hideContent_20(),
	VRTK_Door_t1992640360::get_offset_of_maxAngle_21(),
	VRTK_Door_t1992640360::get_offset_of_openInward_22(),
	VRTK_Door_t1992640360::get_offset_of_openOutward_23(),
	VRTK_Door_t1992640360::get_offset_of_minSnapClose_24(),
	VRTK_Door_t1992640360::get_offset_of_releasedFriction_25(),
	VRTK_Door_t1992640360::get_offset_of_grabbedFriction_26(),
	VRTK_Door_t1992640360::get_offset_of_handleInteractableOnly_27(),
	VRTK_Door_t1992640360::get_offset_of_stepSize_28(),
	VRTK_Door_t1992640360::get_offset_of_doorRigidbody_29(),
	VRTK_Door_t1992640360::get_offset_of_doorHinge_30(),
	VRTK_Door_t1992640360::get_offset_of_doorSnapForce_31(),
	VRTK_Door_t1992640360::get_offset_of_frameRigidbody_32(),
	VRTK_Door_t1992640360::get_offset_of_finalDirection_33(),
	VRTK_Door_t1992640360::get_offset_of_subDirection_34(),
	VRTK_Door_t1992640360::get_offset_of_secondaryDirection_35(),
	VRTK_Door_t1992640360::get_offset_of_doorHingeCreated_36(),
	VRTK_Door_t1992640360::get_offset_of_doorSnapForceCreated_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (VRTK_Drawer_t1874276415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2820[20] = 
{
	VRTK_Drawer_t1874276415::get_offset_of_connectedTo_15(),
	VRTK_Drawer_t1874276415::get_offset_of_direction_16(),
	VRTK_Drawer_t1874276415::get_offset_of_body_17(),
	VRTK_Drawer_t1874276415::get_offset_of_handle_18(),
	VRTK_Drawer_t1874276415::get_offset_of_content_19(),
	VRTK_Drawer_t1874276415::get_offset_of_hideContent_20(),
	VRTK_Drawer_t1874276415::get_offset_of_minSnapClose_21(),
	VRTK_Drawer_t1874276415::get_offset_of_maxExtend_22(),
	VRTK_Drawer_t1874276415::get_offset_of_drawerRigidbody_23(),
	VRTK_Drawer_t1874276415::get_offset_of_handleRigidbody_24(),
	VRTK_Drawer_t1874276415::get_offset_of_handleFixedJoint_25(),
	VRTK_Drawer_t1874276415::get_offset_of_drawerJoint_26(),
	VRTK_Drawer_t1874276415::get_offset_of_drawerInteractableObject_27(),
	VRTK_Drawer_t1874276415::get_offset_of_drawerSnapForce_28(),
	VRTK_Drawer_t1874276415::get_offset_of_finalDirection_29(),
	VRTK_Drawer_t1874276415::get_offset_of_subDirection_30(),
	VRTK_Drawer_t1874276415::get_offset_of_pullDistance_31(),
	VRTK_Drawer_t1874276415::get_offset_of_initialPosition_32(),
	VRTK_Drawer_t1874276415::get_offset_of_drawerJointCreated_33(),
	VRTK_Drawer_t1874276415::get_offset_of_drawerSnapForceCreated_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (VRTK_Knob_t1401139190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2821[13] = 
{
	VRTK_Knob_t1401139190::get_offset_of_connectedTo_15(),
	VRTK_Knob_t1401139190::get_offset_of_direction_16(),
	VRTK_Knob_t1401139190::get_offset_of_min_17(),
	VRTK_Knob_t1401139190::get_offset_of_max_18(),
	VRTK_Knob_t1401139190::get_offset_of_stepSize_19(),
	0,
	VRTK_Knob_t1401139190::get_offset_of_finalDirection_21(),
	VRTK_Knob_t1401139190::get_offset_of_subDirection_22(),
	VRTK_Knob_t1401139190::get_offset_of_subDirectionFound_23(),
	VRTK_Knob_t1401139190::get_offset_of_initialRotation_24(),
	VRTK_Knob_t1401139190::get_offset_of_initialLocalRotation_25(),
	VRTK_Knob_t1401139190::get_offset_of_knobJoint_26(),
	VRTK_Knob_t1401139190::get_offset_of_knobJointCreated_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (KnobDirection_t4203016241)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2822[4] = 
{
	KnobDirection_t4203016241::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (VRTK_Lever_t736126558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2823[10] = 
{
	VRTK_Lever_t736126558::get_offset_of_connectedTo_15(),
	VRTK_Lever_t736126558::get_offset_of_direction_16(),
	VRTK_Lever_t736126558::get_offset_of_minAngle_17(),
	VRTK_Lever_t736126558::get_offset_of_maxAngle_18(),
	VRTK_Lever_t736126558::get_offset_of_stepSize_19(),
	VRTK_Lever_t736126558::get_offset_of_releasedFriction_20(),
	VRTK_Lever_t736126558::get_offset_of_grabbedFriction_21(),
	VRTK_Lever_t736126558::get_offset_of_leverHingeJoint_22(),
	VRTK_Lever_t736126558::get_offset_of_leverHingeJointCreated_23(),
	VRTK_Lever_t736126558::get_offset_of_leverRigidbody_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (LeverDirection_t1799098973)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2824[4] = 
{
	LeverDirection_t1799098973::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (VRTK_Slider_t2095192727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2825[16] = 
{
	VRTK_Slider_t2095192727::get_offset_of_connectedTo_15(),
	VRTK_Slider_t2095192727::get_offset_of_direction_16(),
	VRTK_Slider_t2095192727::get_offset_of_minimumLimit_17(),
	VRTK_Slider_t2095192727::get_offset_of_maximumLimit_18(),
	VRTK_Slider_t2095192727::get_offset_of_minimumValue_19(),
	VRTK_Slider_t2095192727::get_offset_of_maximumValue_20(),
	VRTK_Slider_t2095192727::get_offset_of_stepSize_21(),
	VRTK_Slider_t2095192727::get_offset_of_snapToStep_22(),
	VRTK_Slider_t2095192727::get_offset_of_releasedFriction_23(),
	VRTK_Slider_t2095192727::get_offset_of_finalDirection_24(),
	VRTK_Slider_t2095192727::get_offset_of_sliderRigidbody_25(),
	VRTK_Slider_t2095192727::get_offset_of_sliderJoint_26(),
	VRTK_Slider_t2095192727::get_offset_of_sliderJointCreated_27(),
	VRTK_Slider_t2095192727::get_offset_of_minimumLimitDiff_28(),
	VRTK_Slider_t2095192727::get_offset_of_maximumLimitDiff_29(),
	VRTK_Slider_t2095192727::get_offset_of_snapPosition_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (VRTK_SpringLever_t2820350835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2826[6] = 
{
	VRTK_SpringLever_t2820350835::get_offset_of_springStrength_25(),
	VRTK_SpringLever_t2820350835::get_offset_of_springDamper_26(),
	VRTK_SpringLever_t2820350835::get_offset_of_snapToNearestLimit_27(),
	VRTK_SpringLever_t2820350835::get_offset_of_alwaysActive_28(),
	VRTK_SpringLever_t2820350835::get_offset_of_wasTowardZero_29(),
	VRTK_SpringLever_t2820350835::get_offset_of_isGrabbed_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (VRTK_Wheel_t803354859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2827[20] = 
{
	VRTK_Wheel_t803354859::get_offset_of_connectedTo_15(),
	VRTK_Wheel_t803354859::get_offset_of_grabType_16(),
	VRTK_Wheel_t803354859::get_offset_of_detatchDistance_17(),
	VRTK_Wheel_t803354859::get_offset_of_minimumValue_18(),
	VRTK_Wheel_t803354859::get_offset_of_maximumValue_19(),
	VRTK_Wheel_t803354859::get_offset_of_stepSize_20(),
	VRTK_Wheel_t803354859::get_offset_of_snapToStep_21(),
	VRTK_Wheel_t803354859::get_offset_of_grabbedFriction_22(),
	VRTK_Wheel_t803354859::get_offset_of_releasedFriction_23(),
	VRTK_Wheel_t803354859::get_offset_of_maxAngle_24(),
	VRTK_Wheel_t803354859::get_offset_of_lockAtLimits_25(),
	VRTK_Wheel_t803354859::get_offset_of_angularVelocityLimit_26(),
	VRTK_Wheel_t803354859::get_offset_of_springStrengthValue_27(),
	VRTK_Wheel_t803354859::get_offset_of_springDamperValue_28(),
	VRTK_Wheel_t803354859::get_offset_of_initialLocalRotation_29(),
	VRTK_Wheel_t803354859::get_offset_of_wheelRigidbody_30(),
	VRTK_Wheel_t803354859::get_offset_of_wheelHinge_31(),
	VRTK_Wheel_t803354859::get_offset_of_wheelHingeCreated_32(),
	VRTK_Wheel_t803354859::get_offset_of_initialValueCalculated_33(),
	VRTK_Wheel_t803354859::get_offset_of_springAngle_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (GrabTypes_t504515700)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2828[3] = 
{
	GrabTypes_t504515700::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (VRTK_BaseGrabAttach_t3487134318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2829[16] = 
{
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_precisionGrab_2(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_rightSnapHandle_3(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_leftSnapHandle_4(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_throwVelocityWithAttachDistance_5(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_throwMultiplier_6(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_onGrabCollisionDelay_7(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_tracked_8(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_climbable_9(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_kinematic_10(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_grabbedObject_11(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_grabbedObjectRigidBody_12(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_grabbedObjectScript_13(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_trackPoint_14(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_grabbedSnapHandle_15(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_initialAttachPoint_16(),
	VRTK_BaseGrabAttach_t3487134318::get_offset_of_controllerAttachPoint_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (VRTK_BaseJointGrabAttach_t1229653768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[3] = 
{
	VRTK_BaseJointGrabAttach_t1229653768::get_offset_of_destroyImmediatelyOnThrow_18(),
	VRTK_BaseJointGrabAttach_t1229653768::get_offset_of_givenJoint_19(),
	VRTK_BaseJointGrabAttach_t1229653768::get_offset_of_controllerAttachJoint_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (VRTK_ChildOfControllerGrabAttach_t1018385220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (VRTK_ClimbableGrabAttach_t3258016228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2832[1] = 
{
	VRTK_ClimbableGrabAttach_t3258016228::get_offset_of_useObjectRotation_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (VRTK_CustomJointGrabAttach_t1230827278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2833[2] = 
{
	VRTK_CustomJointGrabAttach_t1230827278::get_offset_of_customJoint_21(),
	VRTK_CustomJointGrabAttach_t1230827278::get_offset_of_jointHolder_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (VRTK_FixedJointGrabAttach_t3523806925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2834[1] = 
{
	VRTK_FixedJointGrabAttach_t3523806925::get_offset_of_breakForce_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (VRTK_RotatorTrackGrabAttach_t3062106299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (VRTK_SpringJointGrabAttach_t1884620216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2836[3] = 
{
	VRTK_SpringJointGrabAttach_t1884620216::get_offset_of_breakForce_21(),
	VRTK_SpringJointGrabAttach_t1884620216::get_offset_of_strength_22(),
	VRTK_SpringJointGrabAttach_t1884620216::get_offset_of_damper_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (VRTK_TrackObjectGrabAttach_t1133162717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[4] = 
{
	VRTK_TrackObjectGrabAttach_t1133162717::get_offset_of_detachDistance_18(),
	VRTK_TrackObjectGrabAttach_t1133162717::get_offset_of_velocityLimit_19(),
	VRTK_TrackObjectGrabAttach_t1133162717::get_offset_of_angularVelocityLimit_20(),
	VRTK_TrackObjectGrabAttach_t1133162717::get_offset_of_isReleasable_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (VRTK_BaseHighlighter_t3110203740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2838[3] = 
{
	VRTK_BaseHighlighter_t3110203740::get_offset_of_active_2(),
	VRTK_BaseHighlighter_t3110203740::get_offset_of_unhighlightOnDisable_3(),
	VRTK_BaseHighlighter_t3110203740::get_offset_of_usesClonedObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (VRTK_MaterialColorSwapHighlighter_t2305160438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2839[6] = 
{
	VRTK_MaterialColorSwapHighlighter_t2305160438::get_offset_of_emissionDarken_5(),
	VRTK_MaterialColorSwapHighlighter_t2305160438::get_offset_of_customMaterial_6(),
	VRTK_MaterialColorSwapHighlighter_t2305160438::get_offset_of_originalSharedRendererMaterials_7(),
	VRTK_MaterialColorSwapHighlighter_t2305160438::get_offset_of_originalRendererMaterials_8(),
	VRTK_MaterialColorSwapHighlighter_t2305160438::get_offset_of_faderRoutines_9(),
	VRTK_MaterialColorSwapHighlighter_t2305160438::get_offset_of_resetMainTexture_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (U3CCycleColorU3Ec__Iterator0_t125975806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2840[9] = 
{
	U3CCycleColorU3Ec__Iterator0_t125975806::get_offset_of_U3CelapsedTimeU3E__0_0(),
	U3CCycleColorU3Ec__Iterator0_t125975806::get_offset_of_duration_1(),
	U3CCycleColorU3Ec__Iterator0_t125975806::get_offset_of_material_2(),
	U3CCycleColorU3Ec__Iterator0_t125975806::get_offset_of_startColor_3(),
	U3CCycleColorU3Ec__Iterator0_t125975806::get_offset_of_endColor_4(),
	U3CCycleColorU3Ec__Iterator0_t125975806::get_offset_of_U24this_5(),
	U3CCycleColorU3Ec__Iterator0_t125975806::get_offset_of_U24current_6(),
	U3CCycleColorU3Ec__Iterator0_t125975806::get_offset_of_U24disposing_7(),
	U3CCycleColorU3Ec__Iterator0_t125975806::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (VRTK_MaterialPropertyBlockColorSwapHighlighter_t2327852568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2841[2] = 
{
	VRTK_MaterialPropertyBlockColorSwapHighlighter_t2327852568::get_offset_of_originalMaterialPropertyBlocks_11(),
	VRTK_MaterialPropertyBlockColorSwapHighlighter_t2327852568::get_offset_of_highlightMaterialPropertyBlocks_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (U3CCycleColorU3Ec__Iterator0_t1494384812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2842[9] = 
{
	U3CCycleColorU3Ec__Iterator0_t1494384812::get_offset_of_U3CelapsedTimeU3E__0_0(),
	U3CCycleColorU3Ec__Iterator0_t1494384812::get_offset_of_duration_1(),
	U3CCycleColorU3Ec__Iterator0_t1494384812::get_offset_of_highlightMaterialPropertyBlock_2(),
	U3CCycleColorU3Ec__Iterator0_t1494384812::get_offset_of_U3CstartColorU3E__1_3(),
	U3CCycleColorU3Ec__Iterator0_t1494384812::get_offset_of_endColor_4(),
	U3CCycleColorU3Ec__Iterator0_t1494384812::get_offset_of_renderer_5(),
	U3CCycleColorU3Ec__Iterator0_t1494384812::get_offset_of_U24current_6(),
	U3CCycleColorU3Ec__Iterator0_t1494384812::get_offset_of_U24disposing_7(),
	U3CCycleColorU3Ec__Iterator0_t1494384812::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (VRTK_OutlineObjectCopyHighlighter_t668900239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2843[7] = 
{
	VRTK_OutlineObjectCopyHighlighter_t668900239::get_offset_of_thickness_5(),
	VRTK_OutlineObjectCopyHighlighter_t668900239::get_offset_of_customOutlineModels_6(),
	VRTK_OutlineObjectCopyHighlighter_t668900239::get_offset_of_customOutlineModelPaths_7(),
	VRTK_OutlineObjectCopyHighlighter_t668900239::get_offset_of_enableSubmeshHighlight_8(),
	VRTK_OutlineObjectCopyHighlighter_t668900239::get_offset_of_stencilOutline_9(),
	VRTK_OutlineObjectCopyHighlighter_t668900239::get_offset_of_highlightModels_10(),
	VRTK_OutlineObjectCopyHighlighter_t668900239::get_offset_of_copyComponents_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (VRTK_AxisScaleGrabAction_t2817064065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2844[8] = 
{
	VRTK_AxisScaleGrabAction_t2817064065::get_offset_of_ungrabDistance_10(),
	VRTK_AxisScaleGrabAction_t2817064065::get_offset_of_lockXAxis_11(),
	VRTK_AxisScaleGrabAction_t2817064065::get_offset_of_lockYAxis_12(),
	VRTK_AxisScaleGrabAction_t2817064065::get_offset_of_lockZAxis_13(),
	VRTK_AxisScaleGrabAction_t2817064065::get_offset_of_uniformScaling_14(),
	VRTK_AxisScaleGrabAction_t2817064065::get_offset_of_initialScale_15(),
	VRTK_AxisScaleGrabAction_t2817064065::get_offset_of_initalLength_16(),
	VRTK_AxisScaleGrabAction_t2817064065::get_offset_of_initialScaleFactor_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (VRTK_BaseGrabAction_t4095736311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[8] = 
{
	VRTK_BaseGrabAction_t4095736311::get_offset_of_grabbedObject_2(),
	VRTK_BaseGrabAction_t4095736311::get_offset_of_primaryGrabbingObject_3(),
	VRTK_BaseGrabAction_t4095736311::get_offset_of_secondaryGrabbingObject_4(),
	VRTK_BaseGrabAction_t4095736311::get_offset_of_primaryInitialGrabPoint_5(),
	VRTK_BaseGrabAction_t4095736311::get_offset_of_secondaryInitialGrabPoint_6(),
	VRTK_BaseGrabAction_t4095736311::get_offset_of_initialised_7(),
	VRTK_BaseGrabAction_t4095736311::get_offset_of_isActionable_8(),
	VRTK_BaseGrabAction_t4095736311::get_offset_of_isSwappable_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (VRTK_ControlDirectionGrabAction_t1241164474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[7] = 
{
	VRTK_ControlDirectionGrabAction_t1241164474::get_offset_of_ungrabDistance_10(),
	VRTK_ControlDirectionGrabAction_t1241164474::get_offset_of_releaseSnapSpeed_11(),
	VRTK_ControlDirectionGrabAction_t1241164474::get_offset_of_lockZRotation_12(),
	VRTK_ControlDirectionGrabAction_t1241164474::get_offset_of_initialPosition_13(),
	VRTK_ControlDirectionGrabAction_t1241164474::get_offset_of_initialRotation_14(),
	VRTK_ControlDirectionGrabAction_t1241164474::get_offset_of_releaseRotation_15(),
	VRTK_ControlDirectionGrabAction_t1241164474::get_offset_of_snappingOnRelease_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (U3CRealignOnReleaseU3Ec__Iterator0_t560927000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2847[5] = 
{
	U3CRealignOnReleaseU3Ec__Iterator0_t560927000::get_offset_of_U3CelapsedTimeU3E__0_0(),
	U3CRealignOnReleaseU3Ec__Iterator0_t560927000::get_offset_of_U24this_1(),
	U3CRealignOnReleaseU3Ec__Iterator0_t560927000::get_offset_of_U24current_2(),
	U3CRealignOnReleaseU3Ec__Iterator0_t560927000::get_offset_of_U24disposing_3(),
	U3CRealignOnReleaseU3Ec__Iterator0_t560927000::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (VRTK_SwapControllerGrabAction_t918155359), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (VRTK_ControllerModelElementPaths_t672993657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2849[9] = 
{
	VRTK_ControllerModelElementPaths_t672993657::get_offset_of_bodyModelPath_0(),
	VRTK_ControllerModelElementPaths_t672993657::get_offset_of_triggerModelPath_1(),
	VRTK_ControllerModelElementPaths_t672993657::get_offset_of_leftGripModelPath_2(),
	VRTK_ControllerModelElementPaths_t672993657::get_offset_of_rightGripModelPath_3(),
	VRTK_ControllerModelElementPaths_t672993657::get_offset_of_touchpadModelPath_4(),
	VRTK_ControllerModelElementPaths_t672993657::get_offset_of_buttonOneModelPath_5(),
	VRTK_ControllerModelElementPaths_t672993657::get_offset_of_buttonTwoModelPath_6(),
	VRTK_ControllerModelElementPaths_t672993657::get_offset_of_systemMenuModelPath_7(),
	VRTK_ControllerModelElementPaths_t672993657::get_offset_of_startMenuModelPath_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (VRTK_ControllerElementHighlighers_t1898231604)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2850[9] = 
{
	VRTK_ControllerElementHighlighers_t1898231604::get_offset_of_body_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRTK_ControllerElementHighlighers_t1898231604::get_offset_of_trigger_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRTK_ControllerElementHighlighers_t1898231604::get_offset_of_gripLeft_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRTK_ControllerElementHighlighers_t1898231604::get_offset_of_gripRight_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRTK_ControllerElementHighlighers_t1898231604::get_offset_of_touchpad_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRTK_ControllerElementHighlighers_t1898231604::get_offset_of_buttonOne_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRTK_ControllerElementHighlighers_t1898231604::get_offset_of_buttonTwo_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRTK_ControllerElementHighlighers_t1898231604::get_offset_of_systemMenu_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRTK_ControllerElementHighlighers_t1898231604::get_offset_of_startMenu_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (ControllerActionsEventArgs_t344001476)+ sizeof (Il2CppObject), sizeof(ControllerActionsEventArgs_t344001476 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2851[1] = 
{
	ControllerActionsEventArgs_t344001476::get_offset_of_controllerIndex_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (ControllerActionsEventHandler_t1521142243), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (VRTK_ControllerActions_t3642353851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2853[10] = 
{
	VRTK_ControllerActions_t3642353851::get_offset_of_modelElementPaths_2(),
	VRTK_ControllerActions_t3642353851::get_offset_of_elementHighlighterOverrides_3(),
	VRTK_ControllerActions_t3642353851::get_offset_of_ControllerModelVisible_4(),
	VRTK_ControllerActions_t3642353851::get_offset_of_ControllerModelInvisible_5(),
	VRTK_ControllerActions_t3642353851::get_offset_of_modelContainer_6(),
	VRTK_ControllerActions_t3642353851::get_offset_of_controllerVisible_7(),
	VRTK_ControllerActions_t3642353851::get_offset_of_controllerHighlighted_8(),
	VRTK_ControllerActions_t3642353851::get_offset_of_cachedElements_9(),
	VRTK_ControllerActions_t3642353851::get_offset_of_highlighterOptions_10(),
	VRTK_ControllerActions_t3642353851::get_offset_of_hapticLoop_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (U3CWaitForModelU3Ec__Iterator0_t2574180537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2854[4] = 
{
	U3CWaitForModelU3Ec__Iterator0_t2574180537::get_offset_of_U24this_0(),
	U3CWaitForModelU3Ec__Iterator0_t2574180537::get_offset_of_U24current_1(),
	U3CWaitForModelU3Ec__Iterator0_t2574180537::get_offset_of_U24disposing_2(),
	U3CWaitForModelU3Ec__Iterator0_t2574180537::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (U3CHapticPulseU3Ec__Iterator1_t288649631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2855[7] = 
{
	U3CHapticPulseU3Ec__Iterator1_t288649631::get_offset_of_pulseInterval_0(),
	U3CHapticPulseU3Ec__Iterator1_t288649631::get_offset_of_duration_1(),
	U3CHapticPulseU3Ec__Iterator1_t288649631::get_offset_of_hapticPulseStrength_2(),
	U3CHapticPulseU3Ec__Iterator1_t288649631::get_offset_of_U24this_3(),
	U3CHapticPulseU3Ec__Iterator1_t288649631::get_offset_of_U24current_4(),
	U3CHapticPulseU3Ec__Iterator1_t288649631::get_offset_of_U24disposing_5(),
	U3CHapticPulseU3Ec__Iterator1_t288649631::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (U3CCycleColorU3Ec__Iterator2_t3567685877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[8] = 
{
	U3CCycleColorU3Ec__Iterator2_t3567685877::get_offset_of_U3CelapsedTimeU3E__0_0(),
	U3CCycleColorU3Ec__Iterator2_t3567685877::get_offset_of_duration_1(),
	U3CCycleColorU3Ec__Iterator2_t3567685877::get_offset_of_material_2(),
	U3CCycleColorU3Ec__Iterator2_t3567685877::get_offset_of_startColor_3(),
	U3CCycleColorU3Ec__Iterator2_t3567685877::get_offset_of_endColor_4(),
	U3CCycleColorU3Ec__Iterator2_t3567685877::get_offset_of_U24current_5(),
	U3CCycleColorU3Ec__Iterator2_t3567685877::get_offset_of_U24disposing_6(),
	U3CCycleColorU3Ec__Iterator2_t3567685877::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (ControllerInteractionEventArgs_t287637539)+ sizeof (Il2CppObject), sizeof(ControllerInteractionEventArgs_t287637539 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2857[4] = 
{
	ControllerInteractionEventArgs_t287637539::get_offset_of_controllerIndex_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControllerInteractionEventArgs_t287637539::get_offset_of_buttonPressure_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControllerInteractionEventArgs_t287637539::get_offset_of_touchpadAxis_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControllerInteractionEventArgs_t287637539::get_offset_of_touchpadAngle_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (ControllerInteractionEventHandler_t343979916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (VRTK_ControllerEvents_t3225224819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2859[84] = 
{
	VRTK_ControllerEvents_t3225224819::get_offset_of_pointerToggleButton_2(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_pointerSetButton_3(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_grabToggleButton_4(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_useToggleButton_5(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_uiClickButton_6(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_menuToggleButton_7(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_axisFidelity_8(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_triggerClickThreshold_9(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_gripClickThreshold_10(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_triggerPressed_11(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_triggerTouched_12(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_triggerHairlinePressed_13(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_triggerClicked_14(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_triggerAxisChanged_15(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_gripPressed_16(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_gripTouched_17(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_gripHairlinePressed_18(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_gripClicked_19(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_gripAxisChanged_20(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_touchpadPressed_21(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_touchpadTouched_22(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_touchpadAxisChanged_23(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_buttonOnePressed_24(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_buttonOneTouched_25(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_buttonTwoPressed_26(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_buttonTwoTouched_27(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_startMenuPressed_28(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_pointerPressed_29(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_grabPressed_30(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_usePressed_31(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_uiClickPressed_32(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_menuPressed_33(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_TriggerPressed_34(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_TriggerReleased_35(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_TriggerTouchStart_36(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_TriggerTouchEnd_37(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_TriggerHairlineStart_38(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_TriggerHairlineEnd_39(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_TriggerClicked_40(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_TriggerUnclicked_41(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_TriggerAxisChanged_42(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_GripPressed_43(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_GripReleased_44(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_GripTouchStart_45(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_GripTouchEnd_46(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_GripHairlineStart_47(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_GripHairlineEnd_48(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_GripClicked_49(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_GripUnclicked_50(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_GripAxisChanged_51(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_TouchpadPressed_52(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_TouchpadReleased_53(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_TouchpadTouchStart_54(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_TouchpadTouchEnd_55(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_TouchpadAxisChanged_56(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_ButtonOneTouchStart_57(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_ButtonOneTouchEnd_58(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_ButtonOnePressed_59(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_ButtonOneReleased_60(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_ButtonTwoTouchStart_61(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_ButtonTwoTouchEnd_62(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_ButtonTwoPressed_63(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_ButtonTwoReleased_64(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_StartMenuPressed_65(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_StartMenuReleased_66(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_AliasPointerOn_67(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_AliasPointerOff_68(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_AliasPointerSet_69(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_AliasGrabOn_70(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_AliasGrabOff_71(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_AliasUseOn_72(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_AliasUseOff_73(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_AliasMenuOn_74(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_AliasMenuOff_75(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_AliasUIClickOn_76(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_AliasUIClickOff_77(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_ControllerEnabled_78(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_ControllerDisabled_79(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_ControllerIndexChanged_80(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_touchpadAxis_81(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_triggerAxis_82(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_gripAxis_83(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_hairTriggerDelta_84(),
	VRTK_ControllerEvents_t3225224819::get_offset_of_hairGripDelta_85(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (ButtonAlias_t1389393715)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2860[17] = 
{
	ButtonAlias_t1389393715::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (VRTK_InteractControllerAppearance_t3909124886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2861[10] = 
{
	VRTK_InteractControllerAppearance_t3909124886::get_offset_of_hideControllerOnTouch_2(),
	VRTK_InteractControllerAppearance_t3909124886::get_offset_of_hideDelayOnTouch_3(),
	VRTK_InteractControllerAppearance_t3909124886::get_offset_of_hideControllerOnGrab_4(),
	VRTK_InteractControllerAppearance_t3909124886::get_offset_of_hideDelayOnGrab_5(),
	VRTK_InteractControllerAppearance_t3909124886::get_offset_of_hideControllerOnUse_6(),
	VRTK_InteractControllerAppearance_t3909124886::get_offset_of_hideDelayOnUse_7(),
	VRTK_InteractControllerAppearance_t3909124886::get_offset_of_storedControllerActions_8(),
	VRTK_InteractControllerAppearance_t3909124886::get_offset_of_storedCurrentObject_9(),
	VRTK_InteractControllerAppearance_t3909124886::get_offset_of_touchControllerShow_10(),
	VRTK_InteractControllerAppearance_t3909124886::get_offset_of_grabControllerShow_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (VRTK_InteractGrab_t124353446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[14] = 
{
	VRTK_InteractGrab_t124353446::get_offset_of_controllerAttachPoint_2(),
	VRTK_InteractGrab_t124353446::get_offset_of_grabPrecognition_3(),
	VRTK_InteractGrab_t124353446::get_offset_of_throwMultiplier_4(),
	VRTK_InteractGrab_t124353446::get_offset_of_createRigidBodyWhenNotTouching_5(),
	VRTK_InteractGrab_t124353446::get_offset_of_ControllerGrabInteractableObject_6(),
	VRTK_InteractGrab_t124353446::get_offset_of_ControllerUngrabInteractableObject_7(),
	VRTK_InteractGrab_t124353446::get_offset_of_grabbedObject_8(),
	VRTK_InteractGrab_t124353446::get_offset_of_influencingGrabbedObject_9(),
	VRTK_InteractGrab_t124353446::get_offset_of_interactTouch_10(),
	VRTK_InteractGrab_t124353446::get_offset_of_controllerActions_11(),
	VRTK_InteractGrab_t124353446::get_offset_of_controllerEvents_12(),
	VRTK_InteractGrab_t124353446::get_offset_of_grabEnabledState_13(),
	VRTK_InteractGrab_t124353446::get_offset_of_grabPrecognitionTimer_14(),
	VRTK_InteractGrab_t124353446::get_offset_of_undroppableGrabbedObject_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (VRTK_InteractHaptics_t1219060492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2863[10] = 
{
	VRTK_InteractHaptics_t1219060492::get_offset_of_strengthOnTouch_2(),
	VRTK_InteractHaptics_t1219060492::get_offset_of_durationOnTouch_3(),
	VRTK_InteractHaptics_t1219060492::get_offset_of_intervalOnTouch_4(),
	VRTK_InteractHaptics_t1219060492::get_offset_of_strengthOnGrab_5(),
	VRTK_InteractHaptics_t1219060492::get_offset_of_durationOnGrab_6(),
	VRTK_InteractHaptics_t1219060492::get_offset_of_intervalOnGrab_7(),
	VRTK_InteractHaptics_t1219060492::get_offset_of_strengthOnUse_8(),
	VRTK_InteractHaptics_t1219060492::get_offset_of_durationOnUse_9(),
	VRTK_InteractHaptics_t1219060492::get_offset_of_intervalOnUse_10(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (ObjectInteractEventArgs_t771291242)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[2] = 
{
	ObjectInteractEventArgs_t771291242::get_offset_of_controllerIndex_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectInteractEventArgs_t771291242::get_offset_of_target_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (ObjectInteractEventHandler_t1701902511), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (VRTK_InteractTouch_t4022091061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2866[18] = 
{
	VRTK_InteractTouch_t4022091061::get_offset_of_customRigidbodyObject_2(),
	VRTK_InteractTouch_t4022091061::get_offset_of_ControllerTouchInteractableObject_3(),
	VRTK_InteractTouch_t4022091061::get_offset_of_ControllerUntouchInteractableObject_4(),
	VRTK_InteractTouch_t4022091061::get_offset_of_touchedObject_5(),
	VRTK_InteractTouch_t4022091061::get_offset_of_touchedObjectColliders_6(),
	VRTK_InteractTouch_t4022091061::get_offset_of_touchedObjectActiveColliders_7(),
	VRTK_InteractTouch_t4022091061::get_offset_of_controllerEvents_8(),
	VRTK_InteractTouch_t4022091061::get_offset_of_controllerActions_9(),
	VRTK_InteractTouch_t4022091061::get_offset_of_controllerCollisionDetector_10(),
	VRTK_InteractTouch_t4022091061::get_offset_of_triggerRumble_11(),
	VRTK_InteractTouch_t4022091061::get_offset_of_destroyColliderOnDisable_12(),
	VRTK_InteractTouch_t4022091061::get_offset_of_triggerIsColliding_13(),
	VRTK_InteractTouch_t4022091061::get_offset_of_triggerWasColliding_14(),
	VRTK_InteractTouch_t4022091061::get_offset_of_touchRigidBody_15(),
	VRTK_InteractTouch_t4022091061::get_offset_of_rigidBodyForcedActive_16(),
	VRTK_InteractTouch_t4022091061::get_offset_of_defaultColliderPrefab_17(),
	VRTK_InteractTouch_t4022091061::get_offset_of_originalGrabAlias_18(),
	VRTK_InteractTouch_t4022091061::get_offset_of_originalUseAlias_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (VRTK_InteractUse_t4015307561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[6] = 
{
	VRTK_InteractUse_t4015307561::get_offset_of_ControllerUseInteractableObject_2(),
	VRTK_InteractUse_t4015307561::get_offset_of_ControllerUnuseInteractableObject_3(),
	VRTK_InteractUse_t4015307561::get_offset_of_usingObject_4(),
	VRTK_InteractUse_t4015307561::get_offset_of_interactTouch_5(),
	VRTK_InteractUse_t4015307561::get_offset_of_controllerActions_6(),
	VRTK_InteractUse_t4015307561::get_offset_of_controllerEvents_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (InteractableObjectEventArgs_t473175556)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2868[1] = 
{
	InteractableObjectEventArgs_t473175556::get_offset_of_interactingObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (InteractableObjectEventHandler_t940909295), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (VRTK_InteractableObject_t2604188111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2870[43] = 
{
	VRTK_InteractableObject_t2604188111::get_offset_of_disableWhenIdle_2(),
	VRTK_InteractableObject_t2604188111::get_offset_of_touchHighlightColor_3(),
	VRTK_InteractableObject_t2604188111::get_offset_of_allowedTouchControllers_4(),
	VRTK_InteractableObject_t2604188111::get_offset_of_isGrabbable_5(),
	VRTK_InteractableObject_t2604188111::get_offset_of_holdButtonToGrab_6(),
	VRTK_InteractableObject_t2604188111::get_offset_of_stayGrabbedOnTeleport_7(),
	VRTK_InteractableObject_t2604188111::get_offset_of_validDrop_8(),
	VRTK_InteractableObject_t2604188111::get_offset_of_grabOverrideButton_9(),
	VRTK_InteractableObject_t2604188111::get_offset_of_allowedGrabControllers_10(),
	VRTK_InteractableObject_t2604188111::get_offset_of_grabAttachMechanicScript_11(),
	VRTK_InteractableObject_t2604188111::get_offset_of_secondaryGrabActionScript_12(),
	VRTK_InteractableObject_t2604188111::get_offset_of_isUsable_13(),
	VRTK_InteractableObject_t2604188111::get_offset_of_holdButtonToUse_14(),
	VRTK_InteractableObject_t2604188111::get_offset_of_useOnlyIfGrabbed_15(),
	VRTK_InteractableObject_t2604188111::get_offset_of_pointerActivatesUseAction_16(),
	VRTK_InteractableObject_t2604188111::get_offset_of_useOverrideButton_17(),
	VRTK_InteractableObject_t2604188111::get_offset_of_allowedUseControllers_18(),
	VRTK_InteractableObject_t2604188111::get_offset_of_InteractableObjectTouched_19(),
	VRTK_InteractableObject_t2604188111::get_offset_of_InteractableObjectUntouched_20(),
	VRTK_InteractableObject_t2604188111::get_offset_of_InteractableObjectGrabbed_21(),
	VRTK_InteractableObject_t2604188111::get_offset_of_InteractableObjectUngrabbed_22(),
	VRTK_InteractableObject_t2604188111::get_offset_of_InteractableObjectUsed_23(),
	VRTK_InteractableObject_t2604188111::get_offset_of_InteractableObjectUnused_24(),
	VRTK_InteractableObject_t2604188111::get_offset_of_usingState_25(),
	VRTK_InteractableObject_t2604188111::get_offset_of_interactableRigidbody_26(),
	VRTK_InteractableObject_t2604188111::get_offset_of_touchingObjects_27(),
	VRTK_InteractableObject_t2604188111::get_offset_of_grabbingObjects_28(),
	VRTK_InteractableObject_t2604188111::get_offset_of_usingObject_29(),
	VRTK_InteractableObject_t2604188111::get_offset_of_trackPoint_30(),
	VRTK_InteractableObject_t2604188111::get_offset_of_customTrackPoint_31(),
	VRTK_InteractableObject_t2604188111::get_offset_of_primaryControllerAttachPoint_32(),
	VRTK_InteractableObject_t2604188111::get_offset_of_secondaryControllerAttachPoint_33(),
	VRTK_InteractableObject_t2604188111::get_offset_of_previousParent_34(),
	VRTK_InteractableObject_t2604188111::get_offset_of_previousKinematicState_35(),
	VRTK_InteractableObject_t2604188111::get_offset_of_previousIsGrabbable_36(),
	VRTK_InteractableObject_t2604188111::get_offset_of_forcedDropped_37(),
	VRTK_InteractableObject_t2604188111::get_offset_of_forceDisabled_38(),
	VRTK_InteractableObject_t2604188111::get_offset_of_objectHighlighter_39(),
	VRTK_InteractableObject_t2604188111::get_offset_of_autoHighlighter_40(),
	VRTK_InteractableObject_t2604188111::get_offset_of_hoveredOverSnapDropZone_41(),
	VRTK_InteractableObject_t2604188111::get_offset_of_snappedInSnapDropZone_42(),
	VRTK_InteractableObject_t2604188111::get_offset_of_storedSnapDropZone_43(),
	VRTK_InteractableObject_t2604188111::get_offset_of_previousLocalScale_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (AllowedController_t2794712781)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2871[4] = 
{
	AllowedController_t2794712781::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (ValidDropTypes_t960740973)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2872[4] = 
{
	ValidDropTypes_t960740973::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2873[5] = 
{
	U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516::get_offset_of_U24locvar0_0(),
	U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516::get_offset_of_U24this_1(),
	U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516::get_offset_of_U24current_2(),
	U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516::get_offset_of_U24disposing_3(),
	U3CRegisterTeleportersAtEndOfFrameU3Ec__Iterator0_t1752167516::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_t4046280006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[4] = 
{
	U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_t4046280006::get_offset_of_U24this_0(),
	U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_t4046280006::get_offset_of_U24current_1(),
	U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_t4046280006::get_offset_of_U24disposing_2(),
	U3CForceStopInteractingAtEndOfFrameU3Ec__Iterator1_t4046280006::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (VRTK_ObjectAutoGrab_t2748125822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2875[5] = 
{
	VRTK_ObjectAutoGrab_t2748125822::get_offset_of_objectToGrab_2(),
	VRTK_ObjectAutoGrab_t2748125822::get_offset_of_objectIsPrefab_3(),
	VRTK_ObjectAutoGrab_t2748125822::get_offset_of_cloneGrabbedObject_4(),
	VRTK_ObjectAutoGrab_t2748125822::get_offset_of_alwaysCloneOnEnable_5(),
	VRTK_ObjectAutoGrab_t2748125822::get_offset_of_previousClonedObject_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (U3CAutoGrabU3Ec__Iterator0_t1000106702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2876[8] = 
{
	U3CAutoGrabU3Ec__Iterator0_t1000106702::get_offset_of_U3CcontrollerGrabU3E__0_0(),
	U3CAutoGrabU3Ec__Iterator0_t1000106702::get_offset_of_U3CcontrollerTouchU3E__1_1(),
	U3CAutoGrabU3Ec__Iterator0_t1000106702::get_offset_of_U3CgrabbableObjectDisableStateU3E__2_2(),
	U3CAutoGrabU3Ec__Iterator0_t1000106702::get_offset_of_U3CgrabbableObjectU3E__3_3(),
	U3CAutoGrabU3Ec__Iterator0_t1000106702::get_offset_of_U24this_4(),
	U3CAutoGrabU3Ec__Iterator0_t1000106702::get_offset_of_U24current_5(),
	U3CAutoGrabU3Ec__Iterator0_t1000106702::get_offset_of_U24disposing_6(),
	U3CAutoGrabU3Ec__Iterator0_t1000106702::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (VRTK_ControllerTracker_t705570086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2877[1] = 
{
	VRTK_ControllerTracker_t705570086::get_offset_of_trackedController_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (Bezier_t3037471005), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (VRTK_CurveGenerator_t3769661606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2879[7] = 
{
	VRTK_CurveGenerator_t3769661606::get_offset_of_points_2(),
	VRTK_CurveGenerator_t3769661606::get_offset_of_items_3(),
	VRTK_CurveGenerator_t3769661606::get_offset_of_modes_4(),
	VRTK_CurveGenerator_t3769661606::get_offset_of_loop_5(),
	VRTK_CurveGenerator_t3769661606::get_offset_of_frequency_6(),
	VRTK_CurveGenerator_t3769661606::get_offset_of_customTracer_7(),
	VRTK_CurveGenerator_t3769661606::get_offset_of_rescalePointerTracer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (BezierControlPointMode_t855273711)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2880[4] = 
{
	BezierControlPointMode_t855273711::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (VRTK_EventSystem_t3222336529), -1, sizeof(VRTK_EventSystem_t3222336529_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2881[4] = 
{
	VRTK_EventSystem_t3222336529_StaticFields::get_offset_of_EVENT_SYSTEM_FIELD_INFOS_14(),
	VRTK_EventSystem_t3222336529_StaticFields::get_offset_of_EVENT_SYSTEM_PROPERTY_INFOS_15(),
	VRTK_EventSystem_t3222336529_StaticFields::get_offset_of_BASE_INPUT_MODULE_EVENT_SYSTEM_FIELD_INFO_16(),
	VRTK_EventSystem_t3222336529::get_offset_of_vrInputModules_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (VRTK_ObjectCache_t513154459), -1, sizeof(VRTK_ObjectCache_t513154459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2882[4] = 
{
	VRTK_ObjectCache_t513154459_StaticFields::get_offset_of_registeredTeleporters_2(),
	VRTK_ObjectCache_t513154459_StaticFields::get_offset_of_registeredDestinationMarkers_3(),
	VRTK_ObjectCache_t513154459_StaticFields::get_offset_of_registeredHeadsetCollider_4(),
	VRTK_ObjectCache_t513154459_StaticFields::get_offset_of_registeredHeadsetControllerAwareness_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (VRTK_PlayerObject_t502441292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2883[1] = 
{
	VRTK_PlayerObject_t502441292::get_offset_of_objectType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (ObjectTypes_t2764484032)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2884[8] = 
{
	ObjectTypes_t2764484032::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (VRTK_RoomExtender_PlayAreaGizmo_t1655168566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2885[5] = 
{
	VRTK_RoomExtender_PlayAreaGizmo_t1655168566::get_offset_of_color_2(),
	VRTK_RoomExtender_PlayAreaGizmo_t1655168566::get_offset_of_wireframeHeight_3(),
	VRTK_RoomExtender_PlayAreaGizmo_t1655168566::get_offset_of_drawWireframeWhenSelectedOnly_4(),
	VRTK_RoomExtender_PlayAreaGizmo_t1655168566::get_offset_of_playArea_5(),
	VRTK_RoomExtender_PlayAreaGizmo_t1655168566::get_offset_of_roomExtender_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (VRTK_ScreenFade_t2079719832), -1, sizeof(VRTK_ScreenFade_t2079719832_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2886[5] = 
{
	VRTK_ScreenFade_t2079719832_StaticFields::get_offset_of_instance_2(),
	VRTK_ScreenFade_t2079719832::get_offset_of_fadeMaterial_3(),
	VRTK_ScreenFade_t2079719832::get_offset_of_currentColor_4(),
	VRTK_ScreenFade_t2079719832::get_offset_of_targetColor_5(),
	VRTK_ScreenFade_t2079719832::get_offset_of_deltaColor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (VRTKTrackedControllerEventArgs_t2407378264)+ sizeof (Il2CppObject), sizeof(VRTKTrackedControllerEventArgs_t2407378264 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2887[2] = 
{
	VRTKTrackedControllerEventArgs_t2407378264::get_offset_of_currentIndex_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VRTKTrackedControllerEventArgs_t2407378264::get_offset_of_previousIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (VRTKTrackedControllerEventHandler_t2437916365), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (VRTK_TrackedController_t520756048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2889[7] = 
{
	VRTK_TrackedController_t520756048::get_offset_of_index_2(),
	VRTK_TrackedController_t520756048::get_offset_of_ControllerEnabled_3(),
	VRTK_TrackedController_t520756048::get_offset_of_ControllerDisabled_4(),
	VRTK_TrackedController_t520756048::get_offset_of_ControllerIndexChanged_5(),
	VRTK_TrackedController_t520756048::get_offset_of_currentIndex_6(),
	VRTK_TrackedController_t520756048::get_offset_of_enableControllerCoroutine_7(),
	VRTK_TrackedController_t520756048::get_offset_of_aliasController_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (U3CEnableU3Ec__Iterator0_t1493054694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[4] = 
{
	U3CEnableU3Ec__Iterator0_t1493054694::get_offset_of_U24this_0(),
	U3CEnableU3Ec__Iterator0_t1493054694::get_offset_of_U24current_1(),
	U3CEnableU3Ec__Iterator0_t1493054694::get_offset_of_U24disposing_2(),
	U3CEnableU3Ec__Iterator0_t1493054694::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (VRTK_TrackedHeadset_t3483597430), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (VRTK_UIGraphicRaycaster_t2816944648), -1, sizeof(VRTK_UIGraphicRaycaster_t2816944648_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2892[5] = 
{
	VRTK_UIGraphicRaycaster_t2816944648::get_offset_of_m_Canvas_10(),
	VRTK_UIGraphicRaycaster_t2816944648::get_offset_of_lastKnownPosition_11(),
	0,
	VRTK_UIGraphicRaycaster_t2816944648_StaticFields::get_offset_of_s_RaycastResults_13(),
	VRTK_UIGraphicRaycaster_t2816944648_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (VRTK_VRInputModule_t1472500726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2893[1] = 
{
	VRTK_VRInputModule_t1472500726::get_offset_of_pointers_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (VRTK_BaseObjectControlAction_t3375001897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2894[8] = 
{
	VRTK_BaseObjectControlAction_t3375001897::get_offset_of_objectControlScript_2(),
	VRTK_BaseObjectControlAction_t3375001897::get_offset_of_listenOnAxisChange_3(),
	VRTK_BaseObjectControlAction_t3375001897::get_offset_of_centerCollider_4(),
	VRTK_BaseObjectControlAction_t3375001897::get_offset_of_colliderCenter_5(),
	VRTK_BaseObjectControlAction_t3375001897::get_offset_of_colliderRadius_6(),
	VRTK_BaseObjectControlAction_t3375001897::get_offset_of_colliderHeight_7(),
	VRTK_BaseObjectControlAction_t3375001897::get_offset_of_controlledTransform_8(),
	VRTK_BaseObjectControlAction_t3375001897::get_offset_of_playArea_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (AxisListeners_t727620975)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2895[3] = 
{
	AxisListeners_t727620975::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (VRTK_RotateObjectControlAction_t2035674631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2896[2] = 
{
	VRTK_RotateObjectControlAction_t2035674631::get_offset_of_maximumRotationSpeed_10(),
	VRTK_RotateObjectControlAction_t2035674631::get_offset_of_rotationMultiplier_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (VRTK_SlideObjectControlAction_t294667339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2897[5] = 
{
	VRTK_SlideObjectControlAction_t294667339::get_offset_of_maximumSpeed_10(),
	VRTK_SlideObjectControlAction_t294667339::get_offset_of_deceleration_11(),
	VRTK_SlideObjectControlAction_t294667339::get_offset_of_fallingDeceleration_12(),
	VRTK_SlideObjectControlAction_t294667339::get_offset_of_speedMultiplier_13(),
	VRTK_SlideObjectControlAction_t294667339::get_offset_of_currentSpeed_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (VRTK_SnapRotateObjectControlAction_t1299063727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2898[6] = 
{
	VRTK_SnapRotateObjectControlAction_t1299063727::get_offset_of_anglePerSnap_10(),
	VRTK_SnapRotateObjectControlAction_t1299063727::get_offset_of_angleMultiplier_11(),
	VRTK_SnapRotateObjectControlAction_t1299063727::get_offset_of_snapDelay_12(),
	VRTK_SnapRotateObjectControlAction_t1299063727::get_offset_of_blinkTransitionSpeed_13(),
	VRTK_SnapRotateObjectControlAction_t1299063727::get_offset_of_axisThreshold_14(),
	VRTK_SnapRotateObjectControlAction_t1299063727::get_offset_of_snapDelayTimer_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (VRTK_WarpObjectControlAction_t3039272518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2899[7] = 
{
	VRTK_WarpObjectControlAction_t3039272518::get_offset_of_warpDistance_10(),
	VRTK_WarpObjectControlAction_t3039272518::get_offset_of_warpMultiplier_11(),
	VRTK_WarpObjectControlAction_t3039272518::get_offset_of_warpDelay_12(),
	VRTK_WarpObjectControlAction_t3039272518::get_offset_of_floorHeightTolerance_13(),
	VRTK_WarpObjectControlAction_t3039272518::get_offset_of_blinkTransitionSpeed_14(),
	VRTK_WarpObjectControlAction_t3039272518::get_offset_of_warpDelayTimer_15(),
	VRTK_WarpObjectControlAction_t3039272518::get_offset_of_headset_16(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
