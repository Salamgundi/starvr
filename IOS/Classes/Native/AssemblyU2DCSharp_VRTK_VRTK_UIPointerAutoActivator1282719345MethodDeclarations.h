﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_UIPointerAutoActivator
struct VRTK_UIPointerAutoActivator_t1282719345;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void VRTK.VRTK_UIPointerAutoActivator::.ctor()
extern "C"  void VRTK_UIPointerAutoActivator__ctor_m2947574927 (VRTK_UIPointerAutoActivator_t1282719345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointerAutoActivator::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void VRTK_UIPointerAutoActivator_OnTriggerEnter_m2629940323 (VRTK_UIPointerAutoActivator_t1282719345 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIPointerAutoActivator::OnTriggerExit(UnityEngine.Collider)
extern "C"  void VRTK_UIPointerAutoActivator_OnTriggerExit_m3837674671 (VRTK_UIPointerAutoActivator_t1282719345 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
