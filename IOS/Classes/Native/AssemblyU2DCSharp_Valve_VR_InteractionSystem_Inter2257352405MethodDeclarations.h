﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Interactable/OnDetachedFromHandDelegate
struct OnDetachedFromHandDelegate_t2257352405;
// System.Object
struct Il2CppObject;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.InteractionSystem.Interactable/OnDetachedFromHandDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDetachedFromHandDelegate__ctor_m1006531530 (OnDetachedFromHandDelegate_t2257352405 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Interactable/OnDetachedFromHandDelegate::Invoke(Valve.VR.InteractionSystem.Hand)
extern "C"  void OnDetachedFromHandDelegate_Invoke_m2620915744 (OnDetachedFromHandDelegate_t2257352405 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.InteractionSystem.Interactable/OnDetachedFromHandDelegate::BeginInvoke(Valve.VR.InteractionSystem.Hand,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnDetachedFromHandDelegate_BeginInvoke_m3398122557 (OnDetachedFromHandDelegate_t2257352405 * __this, Hand_t379716353 * ___hand0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Interactable/OnDetachedFromHandDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void OnDetachedFromHandDelegate_EndInvoke_m3777188068 (OnDetachedFromHandDelegate_t2257352405 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
