﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.LogType,UnityEngine.Color,System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>
struct Transform_1_t47168147;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23065293926.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.LogType,UnityEngine.Color,System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2656888054_gshared (Transform_1_t47168147 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m2656888054(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t47168147 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2656888054_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.LogType,UnityEngine.Color,System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3065293926  Transform_1_Invoke_m3161743990_gshared (Transform_1_t47168147 * __this, int32_t ___key0, Color_t2020392075  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m3161743990(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t3065293926  (*) (Transform_1_t47168147 *, int32_t, Color_t2020392075 , const MethodInfo*))Transform_1_Invoke_m3161743990_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.LogType,UnityEngine.Color,System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m604900127_gshared (Transform_1_t47168147 * __this, int32_t ___key0, Color_t2020392075  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m604900127(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t47168147 *, int32_t, Color_t2020392075 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m604900127_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.LogType,UnityEngine.Color,System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3065293926  Transform_1_EndInvoke_m674367564_gshared (Transform_1_t47168147 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m674367564(__this, ___result0, method) ((  KeyValuePair_2_t3065293926  (*) (Transform_1_t47168147 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m674367564_gshared)(__this, ___result0, method)
