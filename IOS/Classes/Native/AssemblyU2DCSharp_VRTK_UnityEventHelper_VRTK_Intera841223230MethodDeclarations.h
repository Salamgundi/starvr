﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents
struct VRTK_InteractableObject_UnityEvents_t841223230;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"

// System.Void VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::.ctor()
extern "C"  void VRTK_InteractableObject_UnityEvents__ctor_m2713423983 (VRTK_InteractableObject_UnityEvents_t841223230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::SetInteractableObject()
extern "C"  void VRTK_InteractableObject_UnityEvents_SetInteractableObject_m4244890186 (VRTK_InteractableObject_UnityEvents_t841223230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::OnEnable()
extern "C"  void VRTK_InteractableObject_UnityEvents_OnEnable_m936003747 (VRTK_InteractableObject_UnityEvents_t841223230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::Touch(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_InteractableObject_UnityEvents_Touch_m2085745203 (VRTK_InteractableObject_UnityEvents_t841223230 * __this, Il2CppObject * ___o0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::UnTouch(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_InteractableObject_UnityEvents_UnTouch_m788011668 (VRTK_InteractableObject_UnityEvents_t841223230 * __this, Il2CppObject * ___o0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::Grab(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_InteractableObject_UnityEvents_Grab_m1787227640 (VRTK_InteractableObject_UnityEvents_t841223230 * __this, Il2CppObject * ___o0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::UnGrab(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_InteractableObject_UnityEvents_UnGrab_m2829106067 (VRTK_InteractableObject_UnityEvents_t841223230 * __this, Il2CppObject * ___o0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::Use(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_InteractableObject_UnityEvents_Use_m3698372561 (VRTK_InteractableObject_UnityEvents_t841223230 * __this, Il2CppObject * ___o0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::Unuse(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_InteractableObject_UnityEvents_Unuse_m4123188296 (VRTK_InteractableObject_UnityEvents_t841223230 * __this, Il2CppObject * ___o0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractableObject_UnityEvents::OnDisable()
extern "C"  void VRTK_InteractableObject_UnityEvents_OnDisable_m3059148348 (VRTK_InteractableObject_UnityEvents_t841223230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
