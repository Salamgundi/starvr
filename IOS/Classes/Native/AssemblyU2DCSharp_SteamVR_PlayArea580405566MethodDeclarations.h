﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_PlayArea
struct SteamVR_PlayArea_t580405566;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SteamVR_PlayArea_Size1963019026.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdQuad_t2172573705.h"

// System.Void SteamVR_PlayArea::.ctor()
extern "C"  void SteamVR_PlayArea__ctor_m4198016297 (SteamVR_PlayArea_t580405566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_PlayArea::GetBounds(SteamVR_PlayArea/Size,Valve.VR.HmdQuad_t&)
extern "C"  bool SteamVR_PlayArea_GetBounds_m3855398087 (Il2CppObject * __this /* static, unused */, int32_t ___size0, HmdQuad_t_t2172573705 * ___pRect1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_PlayArea::BuildMesh()
extern "C"  void SteamVR_PlayArea_BuildMesh_m2264972968 (SteamVR_PlayArea_t580405566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_PlayArea::OnDrawGizmos()
extern "C"  void SteamVR_PlayArea_OnDrawGizmos_m3615280293 (SteamVR_PlayArea_t580405566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_PlayArea::OnDrawGizmosSelected()
extern "C"  void SteamVR_PlayArea_OnDrawGizmosSelected_m2298377644 (SteamVR_PlayArea_t580405566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_PlayArea::DrawWireframe()
extern "C"  void SteamVR_PlayArea_DrawWireframe_m3017845949 (SteamVR_PlayArea_t580405566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_PlayArea::OnEnable()
extern "C"  void SteamVR_PlayArea_OnEnable_m2468613417 (SteamVR_PlayArea_t580405566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SteamVR_PlayArea::UpdateBounds()
extern "C"  Il2CppObject * SteamVR_PlayArea_UpdateBounds_m1620050127 (SteamVR_PlayArea_t580405566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
