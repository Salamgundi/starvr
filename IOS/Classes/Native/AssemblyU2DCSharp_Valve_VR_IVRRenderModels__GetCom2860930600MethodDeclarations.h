﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_GetComponentRenderModelName
struct _GetComponentRenderModelName_t2860930600;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_GetComponentRenderModelName::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetComponentRenderModelName__ctor_m1767760407 (_GetComponentRenderModelName_t2860930600 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRRenderModels/_GetComponentRenderModelName::Invoke(System.String,System.String,System.Text.StringBuilder,System.UInt32)
extern "C"  uint32_t _GetComponentRenderModelName_Invoke_m1715179606 (_GetComponentRenderModelName_t2860930600 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, StringBuilder_t1221177846 * ___pchComponentRenderModelName2, uint32_t ___unComponentRenderModelNameLen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetComponentRenderModelName::BeginInvoke(System.String,System.String,System.Text.StringBuilder,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetComponentRenderModelName_BeginInvoke_m3352067234 (_GetComponentRenderModelName_t2860930600 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, StringBuilder_t1221177846 * ___pchComponentRenderModelName2, uint32_t ___unComponentRenderModelNameLen3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRRenderModels/_GetComponentRenderModelName::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetComponentRenderModelName_EndInvoke_m78849076 (_GetComponentRenderModelName_t2860930600 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
