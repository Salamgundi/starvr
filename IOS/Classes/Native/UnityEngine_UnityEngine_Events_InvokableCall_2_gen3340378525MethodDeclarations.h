﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`2<System.Object,VRTK.BodyPhysicsEventArgs>
struct InvokableCall_2_t3340378525;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.BodyPhysicsEventArgs>
struct UnityAction_2_t3325587641;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.BodyPhysicsEventArgs>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m3199703015_gshared (InvokableCall_2_t3340378525 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_2__ctor_m3199703015(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_2_t3340378525 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2__ctor_m3199703015_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.BodyPhysicsEventArgs>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m2987865216_gshared (InvokableCall_2_t3340378525 * __this, UnityAction_2_t3325587641 * ___action0, const MethodInfo* method);
#define InvokableCall_2__ctor_m2987865216(__this, ___action0, method) ((  void (*) (InvokableCall_2_t3340378525 *, UnityAction_2_t3325587641 *, const MethodInfo*))InvokableCall_2__ctor_m2987865216_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.BodyPhysicsEventArgs>::add_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_add_Delegate_m578261047_gshared (InvokableCall_2_t3340378525 * __this, UnityAction_2_t3325587641 * ___value0, const MethodInfo* method);
#define InvokableCall_2_add_Delegate_m578261047(__this, ___value0, method) ((  void (*) (InvokableCall_2_t3340378525 *, UnityAction_2_t3325587641 *, const MethodInfo*))InvokableCall_2_add_Delegate_m578261047_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.BodyPhysicsEventArgs>::remove_Delegate(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2_remove_Delegate_m3739457386_gshared (InvokableCall_2_t3340378525 * __this, UnityAction_2_t3325587641 * ___value0, const MethodInfo* method);
#define InvokableCall_2_remove_Delegate_m3739457386(__this, ___value0, method) ((  void (*) (InvokableCall_2_t3340378525 *, UnityAction_2_t3325587641 *, const MethodInfo*))InvokableCall_2_remove_Delegate_m3739457386_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,VRTK.BodyPhysicsEventArgs>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m568552404_gshared (InvokableCall_2_t3340378525 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_2_Invoke_m568552404(__this, ___args0, method) ((  void (*) (InvokableCall_2_t3340378525 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_2_Invoke_m568552404_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,VRTK.BodyPhysicsEventArgs>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m2626551512_gshared (InvokableCall_2_t3340378525 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_2_Find_m2626551512(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_2_t3340378525 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2_Find_m2626551512_gshared)(__this, ___targetObj0, ___method1, method)
