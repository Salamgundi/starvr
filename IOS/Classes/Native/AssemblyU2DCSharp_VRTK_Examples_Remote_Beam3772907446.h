﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Remote_Beam
struct  Remote_Beam_t3772907446  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector2 VRTK.Examples.Remote_Beam::touchAxis
	Vector2_t2243707579  ___touchAxis_2;
	// System.Single VRTK.Examples.Remote_Beam::rotationSpeed
	float ___rotationSpeed_3;
	// System.Single VRTK.Examples.Remote_Beam::currentYaw
	float ___currentYaw_4;
	// System.Single VRTK.Examples.Remote_Beam::currentPitch
	float ___currentPitch_5;

public:
	inline static int32_t get_offset_of_touchAxis_2() { return static_cast<int32_t>(offsetof(Remote_Beam_t3772907446, ___touchAxis_2)); }
	inline Vector2_t2243707579  get_touchAxis_2() const { return ___touchAxis_2; }
	inline Vector2_t2243707579 * get_address_of_touchAxis_2() { return &___touchAxis_2; }
	inline void set_touchAxis_2(Vector2_t2243707579  value)
	{
		___touchAxis_2 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_3() { return static_cast<int32_t>(offsetof(Remote_Beam_t3772907446, ___rotationSpeed_3)); }
	inline float get_rotationSpeed_3() const { return ___rotationSpeed_3; }
	inline float* get_address_of_rotationSpeed_3() { return &___rotationSpeed_3; }
	inline void set_rotationSpeed_3(float value)
	{
		___rotationSpeed_3 = value;
	}

	inline static int32_t get_offset_of_currentYaw_4() { return static_cast<int32_t>(offsetof(Remote_Beam_t3772907446, ___currentYaw_4)); }
	inline float get_currentYaw_4() const { return ___currentYaw_4; }
	inline float* get_address_of_currentYaw_4() { return &___currentYaw_4; }
	inline void set_currentYaw_4(float value)
	{
		___currentYaw_4 = value;
	}

	inline static int32_t get_offset_of_currentPitch_5() { return static_cast<int32_t>(offsetof(Remote_Beam_t3772907446, ___currentPitch_5)); }
	inline float get_currentPitch_5() const { return ___currentPitch_5; }
	inline float* get_address_of_currentPitch_5() { return &___currentPitch_5; }
	inline void set_currentPitch_5(float value)
	{
		___currentPitch_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
