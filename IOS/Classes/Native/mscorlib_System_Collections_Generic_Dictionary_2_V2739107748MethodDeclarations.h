﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>
struct Dictionary_2_t1052574984;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2739107748.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m603142083_gshared (Enumerator_t2739107748 * __this, Dictionary_2_t1052574984 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m603142083(__this, ___host0, method) ((  void (*) (Enumerator_t2739107748 *, Dictionary_2_t1052574984 *, const MethodInfo*))Enumerator__ctor_m603142083_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3372083266_gshared (Enumerator_t2739107748 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3372083266(__this, method) ((  Il2CppObject * (*) (Enumerator_t2739107748 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3372083266_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1832852956_gshared (Enumerator_t2739107748 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1832852956(__this, method) ((  void (*) (Enumerator_t2739107748 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1832852956_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m4010725143_gshared (Enumerator_t2739107748 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4010725143(__this, method) ((  void (*) (Enumerator_t2739107748 *, const MethodInfo*))Enumerator_Dispose_m4010725143_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m495612356_gshared (Enumerator_t2739107748 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m495612356(__this, method) ((  bool (*) (Enumerator_t2739107748 *, const MethodInfo*))Enumerator_MoveNext_m495612356_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2204834694_gshared (Enumerator_t2739107748 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2204834694(__this, method) ((  Il2CppObject * (*) (Enumerator_t2739107748 *, const MethodInfo*))Enumerator_get_Current_m2204834694_gshared)(__this, method)
