﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.VRTK_ControllerAppearance_Example
struct VRTK_ControllerAppearance_Example_t4148578061;
// System.Object
struct Il2CppObject;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::.ctor()
extern "C"  void VRTK_ControllerAppearance_Example__ctor_m2467069964 (VRTK_ControllerAppearance_Example_t4148578061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::Start()
extern "C"  void VRTK_ControllerAppearance_Example_Start_m3760813136 (VRTK_ControllerAppearance_Example_t4148578061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::DoTriggerPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerAppearance_Example_DoTriggerPressed_m93422017 (VRTK_ControllerAppearance_Example_t4148578061 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::DoTriggerReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerAppearance_Example_DoTriggerReleased_m1202208156 (VRTK_ControllerAppearance_Example_t4148578061 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::DoButtonOnePressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerAppearance_Example_DoButtonOnePressed_m3459526911 (VRTK_ControllerAppearance_Example_t4148578061 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::DoButtonOneReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerAppearance_Example_DoButtonOneReleased_m164331976 (VRTK_ControllerAppearance_Example_t4148578061 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::DoButtonTwoPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerAppearance_Example_DoButtonTwoPressed_m918785707 (VRTK_ControllerAppearance_Example_t4148578061 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::DoButtonTwoReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerAppearance_Example_DoButtonTwoReleased_m3977783062 (VRTK_ControllerAppearance_Example_t4148578061 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::DoStartMenuPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerAppearance_Example_DoStartMenuPressed_m1868086740 (VRTK_ControllerAppearance_Example_t4148578061 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::DoStartMenuReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerAppearance_Example_DoStartMenuReleased_m1900938525 (VRTK_ControllerAppearance_Example_t4148578061 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::DoGripPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerAppearance_Example_DoGripPressed_m389989213 (VRTK_ControllerAppearance_Example_t4148578061 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::DoGripReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerAppearance_Example_DoGripReleased_m1180232144 (VRTK_ControllerAppearance_Example_t4148578061 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::DoTouchpadPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerAppearance_Example_DoTouchpadPressed_m876951781 (VRTK_ControllerAppearance_Example_t4148578061 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::DoTouchpadReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerAppearance_Example_DoTouchpadReleased_m1102421020 (VRTK_ControllerAppearance_Example_t4148578061 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void VRTK_ControllerAppearance_Example_OnTriggerEnter_m3420360104 (VRTK_ControllerAppearance_Example_t4148578061 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerAppearance_Example::OnTriggerExit(UnityEngine.Collider)
extern "C"  void VRTK_ControllerAppearance_Example_OnTriggerExit_m11930666 (VRTK_ControllerAppearance_Example_t4148578061 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
