﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SnapDropZoneEventHandler
struct SnapDropZoneEventHandler_t172258073;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_SnapDropZoneEventArgs418702774.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.SnapDropZoneEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SnapDropZoneEventHandler__ctor_m1449381749 (SnapDropZoneEventHandler_t172258073 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SnapDropZoneEventHandler::Invoke(System.Object,VRTK.SnapDropZoneEventArgs)
extern "C"  void SnapDropZoneEventHandler_Invoke_m1204667382 (SnapDropZoneEventHandler_t172258073 * __this, Il2CppObject * ___sender0, SnapDropZoneEventArgs_t418702774  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.SnapDropZoneEventHandler::BeginInvoke(System.Object,VRTK.SnapDropZoneEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SnapDropZoneEventHandler_BeginInvoke_m1674174409 (SnapDropZoneEventHandler_t172258073 * __this, Il2CppObject * ___sender0, SnapDropZoneEventArgs_t418702774  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SnapDropZoneEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void SnapDropZoneEventHandler_EndInvoke_m1700107243 (SnapDropZoneEventHandler_t172258073 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
