﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRTrackedCamera
struct CVRTrackedCamera_t2050215972;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraError3529690400.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraFrameTy2003723073.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdVector2_t2255225135.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix44_t664273159.h"
#include "AssemblyU2DCSharp_Valve_VR_CameraVideoStreamFrameHe968213647.h"
#include "AssemblyU2DCSharp_Valve_VR_VRTextureBounds_t1897807375.h"

// System.Void Valve.VR.CVRTrackedCamera::.ctor(System.IntPtr)
extern "C"  void CVRTrackedCamera__ctor_m2583231269 (CVRTrackedCamera_t2050215972 * __this, IntPtr_t ___pInterface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Valve.VR.CVRTrackedCamera::GetCameraErrorNameFromEnum(Valve.VR.EVRTrackedCameraError)
extern "C"  String_t* CVRTrackedCamera_GetCameraErrorNameFromEnum_m752632409 (CVRTrackedCamera_t2050215972 * __this, int32_t ___eCameraError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.CVRTrackedCamera::HasCamera(System.UInt32,System.Boolean&)
extern "C"  int32_t CVRTrackedCamera_HasCamera_m3092079058 (CVRTrackedCamera_t2050215972 * __this, uint32_t ___nDeviceIndex0, bool* ___pHasCamera1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.CVRTrackedCamera::GetCameraFrameSize(System.UInt32,Valve.VR.EVRTrackedCameraFrameType,System.UInt32&,System.UInt32&,System.UInt32&)
extern "C"  int32_t CVRTrackedCamera_GetCameraFrameSize_m3305163358 (CVRTrackedCamera_t2050215972 * __this, uint32_t ___nDeviceIndex0, int32_t ___eFrameType1, uint32_t* ___pnWidth2, uint32_t* ___pnHeight3, uint32_t* ___pnFrameBufferSize4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.CVRTrackedCamera::GetCameraIntrinsics(System.UInt32,Valve.VR.EVRTrackedCameraFrameType,Valve.VR.HmdVector2_t&,Valve.VR.HmdVector2_t&)
extern "C"  int32_t CVRTrackedCamera_GetCameraIntrinsics_m1334924404 (CVRTrackedCamera_t2050215972 * __this, uint32_t ___nDeviceIndex0, int32_t ___eFrameType1, HmdVector2_t_t2255225135 * ___pFocalLength2, HmdVector2_t_t2255225135 * ___pCenter3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.CVRTrackedCamera::GetCameraProjection(System.UInt32,Valve.VR.EVRTrackedCameraFrameType,System.Single,System.Single,Valve.VR.HmdMatrix44_t&)
extern "C"  int32_t CVRTrackedCamera_GetCameraProjection_m392256408 (CVRTrackedCamera_t2050215972 * __this, uint32_t ___nDeviceIndex0, int32_t ___eFrameType1, float ___flZNear2, float ___flZFar3, HmdMatrix44_t_t664273159 * ___pProjection4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.CVRTrackedCamera::AcquireVideoStreamingService(System.UInt32,System.UInt64&)
extern "C"  int32_t CVRTrackedCamera_AcquireVideoStreamingService_m2475827139 (CVRTrackedCamera_t2050215972 * __this, uint32_t ___nDeviceIndex0, uint64_t* ___pHandle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.CVRTrackedCamera::ReleaseVideoStreamingService(System.UInt64)
extern "C"  int32_t CVRTrackedCamera_ReleaseVideoStreamingService_m2364426576 (CVRTrackedCamera_t2050215972 * __this, uint64_t ___hTrackedCamera0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.CVRTrackedCamera::GetVideoStreamFrameBuffer(System.UInt64,Valve.VR.EVRTrackedCameraFrameType,System.IntPtr,System.UInt32,Valve.VR.CameraVideoStreamFrameHeader_t&,System.UInt32)
extern "C"  int32_t CVRTrackedCamera_GetVideoStreamFrameBuffer_m104151229 (CVRTrackedCamera_t2050215972 * __this, uint64_t ___hTrackedCamera0, int32_t ___eFrameType1, IntPtr_t ___pFrameBuffer2, uint32_t ___nFrameBufferSize3, CameraVideoStreamFrameHeader_t_t968213647 * ___pFrameHeader4, uint32_t ___nFrameHeaderSize5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.CVRTrackedCamera::GetVideoStreamTextureSize(System.UInt32,Valve.VR.EVRTrackedCameraFrameType,Valve.VR.VRTextureBounds_t&,System.UInt32&,System.UInt32&)
extern "C"  int32_t CVRTrackedCamera_GetVideoStreamTextureSize_m554916695 (CVRTrackedCamera_t2050215972 * __this, uint32_t ___nDeviceIndex0, int32_t ___eFrameType1, VRTextureBounds_t_t1897807375 * ___pTextureBounds2, uint32_t* ___pnWidth3, uint32_t* ___pnHeight4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.CVRTrackedCamera::GetVideoStreamTextureD3D11(System.UInt64,Valve.VR.EVRTrackedCameraFrameType,System.IntPtr,System.IntPtr&,Valve.VR.CameraVideoStreamFrameHeader_t&,System.UInt32)
extern "C"  int32_t CVRTrackedCamera_GetVideoStreamTextureD3D11_m6956356 (CVRTrackedCamera_t2050215972 * __this, uint64_t ___hTrackedCamera0, int32_t ___eFrameType1, IntPtr_t ___pD3D11DeviceOrResource2, IntPtr_t* ___ppD3D11ShaderResourceView3, CameraVideoStreamFrameHeader_t_t968213647 * ___pFrameHeader4, uint32_t ___nFrameHeaderSize5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.CVRTrackedCamera::GetVideoStreamTextureGL(System.UInt64,Valve.VR.EVRTrackedCameraFrameType,System.UInt32&,Valve.VR.CameraVideoStreamFrameHeader_t&,System.UInt32)
extern "C"  int32_t CVRTrackedCamera_GetVideoStreamTextureGL_m4056849656 (CVRTrackedCamera_t2050215972 * __this, uint64_t ___hTrackedCamera0, int32_t ___eFrameType1, uint32_t* ___pglTextureId2, CameraVideoStreamFrameHeader_t_t968213647 * ___pFrameHeader3, uint32_t ___nFrameHeaderSize4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.CVRTrackedCamera::ReleaseVideoStreamTextureGL(System.UInt64,System.UInt32)
extern "C"  int32_t CVRTrackedCamera_ReleaseVideoStreamTextureGL_m4289433789 (CVRTrackedCamera_t2050215972 * __this, uint64_t ___hTrackedCamera0, uint32_t ___glTextureId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
