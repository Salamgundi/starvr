﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.HandLift
struct  HandLift_t3915206274  : public VRTK_InteractableObject_t2604188111
{
public:
	// System.Single VRTK.Examples.HandLift::speed
	float ___speed_45;
	// UnityEngine.Transform VRTK.Examples.HandLift::handleTop
	Transform_t3275118058 * ___handleTop_46;
	// UnityEngine.Transform VRTK.Examples.HandLift::ropeTop
	Transform_t3275118058 * ___ropeTop_47;
	// UnityEngine.Transform VRTK.Examples.HandLift::ropeBottom
	Transform_t3275118058 * ___ropeBottom_48;
	// UnityEngine.GameObject VRTK.Examples.HandLift::rope
	GameObject_t1756533147 * ___rope_49;
	// UnityEngine.GameObject VRTK.Examples.HandLift::handle
	GameObject_t1756533147 * ___handle_50;
	// System.Boolean VRTK.Examples.HandLift::isMoving
	bool ___isMoving_51;
	// System.Boolean VRTK.Examples.HandLift::isMovingUp
	bool ___isMovingUp_52;

public:
	inline static int32_t get_offset_of_speed_45() { return static_cast<int32_t>(offsetof(HandLift_t3915206274, ___speed_45)); }
	inline float get_speed_45() const { return ___speed_45; }
	inline float* get_address_of_speed_45() { return &___speed_45; }
	inline void set_speed_45(float value)
	{
		___speed_45 = value;
	}

	inline static int32_t get_offset_of_handleTop_46() { return static_cast<int32_t>(offsetof(HandLift_t3915206274, ___handleTop_46)); }
	inline Transform_t3275118058 * get_handleTop_46() const { return ___handleTop_46; }
	inline Transform_t3275118058 ** get_address_of_handleTop_46() { return &___handleTop_46; }
	inline void set_handleTop_46(Transform_t3275118058 * value)
	{
		___handleTop_46 = value;
		Il2CppCodeGenWriteBarrier(&___handleTop_46, value);
	}

	inline static int32_t get_offset_of_ropeTop_47() { return static_cast<int32_t>(offsetof(HandLift_t3915206274, ___ropeTop_47)); }
	inline Transform_t3275118058 * get_ropeTop_47() const { return ___ropeTop_47; }
	inline Transform_t3275118058 ** get_address_of_ropeTop_47() { return &___ropeTop_47; }
	inline void set_ropeTop_47(Transform_t3275118058 * value)
	{
		___ropeTop_47 = value;
		Il2CppCodeGenWriteBarrier(&___ropeTop_47, value);
	}

	inline static int32_t get_offset_of_ropeBottom_48() { return static_cast<int32_t>(offsetof(HandLift_t3915206274, ___ropeBottom_48)); }
	inline Transform_t3275118058 * get_ropeBottom_48() const { return ___ropeBottom_48; }
	inline Transform_t3275118058 ** get_address_of_ropeBottom_48() { return &___ropeBottom_48; }
	inline void set_ropeBottom_48(Transform_t3275118058 * value)
	{
		___ropeBottom_48 = value;
		Il2CppCodeGenWriteBarrier(&___ropeBottom_48, value);
	}

	inline static int32_t get_offset_of_rope_49() { return static_cast<int32_t>(offsetof(HandLift_t3915206274, ___rope_49)); }
	inline GameObject_t1756533147 * get_rope_49() const { return ___rope_49; }
	inline GameObject_t1756533147 ** get_address_of_rope_49() { return &___rope_49; }
	inline void set_rope_49(GameObject_t1756533147 * value)
	{
		___rope_49 = value;
		Il2CppCodeGenWriteBarrier(&___rope_49, value);
	}

	inline static int32_t get_offset_of_handle_50() { return static_cast<int32_t>(offsetof(HandLift_t3915206274, ___handle_50)); }
	inline GameObject_t1756533147 * get_handle_50() const { return ___handle_50; }
	inline GameObject_t1756533147 ** get_address_of_handle_50() { return &___handle_50; }
	inline void set_handle_50(GameObject_t1756533147 * value)
	{
		___handle_50 = value;
		Il2CppCodeGenWriteBarrier(&___handle_50, value);
	}

	inline static int32_t get_offset_of_isMoving_51() { return static_cast<int32_t>(offsetof(HandLift_t3915206274, ___isMoving_51)); }
	inline bool get_isMoving_51() const { return ___isMoving_51; }
	inline bool* get_address_of_isMoving_51() { return &___isMoving_51; }
	inline void set_isMoving_51(bool value)
	{
		___isMoving_51 = value;
	}

	inline static int32_t get_offset_of_isMovingUp_52() { return static_cast<int32_t>(offsetof(HandLift_t3915206274, ___isMovingUp_52)); }
	inline bool get_isMovingUp_52() const { return ___isMovingUp_52; }
	inline bool* get_address_of_isMovingUp_52() { return &___isMovingUp_52; }
	inline void set_isMovingUp_52(bool value)
	{
		___isMovingUp_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
