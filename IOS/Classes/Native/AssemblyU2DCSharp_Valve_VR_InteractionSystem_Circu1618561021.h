﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SteamVR_Controller/Device
struct Device_t2885069456;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0
struct  U3CHapticPulsesU3Ec__Iterator0_t1618561021  : public Il2CppObject
{
public:
	// SteamVR_Controller/Device Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::controller
	Device_t2885069456 * ___controller_0;
	// System.Single Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::flMagnitude
	float ___flMagnitude_1;
	// System.Int32 Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::<nRangeMax>__0
	int32_t ___U3CnRangeMaxU3E__0_2;
	// System.Int32 Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::nCount
	int32_t ___nCount_3;
	// System.UInt16 Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::<i>__1
	uint16_t ___U3CiU3E__1_4;
	// System.UInt16 Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::<duration>__2
	uint16_t ___U3CdurationU3E__2_5;
	// System.Object Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Valve.VR.InteractionSystem.CircularDrive/<HapticPulses>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_controller_0() { return static_cast<int32_t>(offsetof(U3CHapticPulsesU3Ec__Iterator0_t1618561021, ___controller_0)); }
	inline Device_t2885069456 * get_controller_0() const { return ___controller_0; }
	inline Device_t2885069456 ** get_address_of_controller_0() { return &___controller_0; }
	inline void set_controller_0(Device_t2885069456 * value)
	{
		___controller_0 = value;
		Il2CppCodeGenWriteBarrier(&___controller_0, value);
	}

	inline static int32_t get_offset_of_flMagnitude_1() { return static_cast<int32_t>(offsetof(U3CHapticPulsesU3Ec__Iterator0_t1618561021, ___flMagnitude_1)); }
	inline float get_flMagnitude_1() const { return ___flMagnitude_1; }
	inline float* get_address_of_flMagnitude_1() { return &___flMagnitude_1; }
	inline void set_flMagnitude_1(float value)
	{
		___flMagnitude_1 = value;
	}

	inline static int32_t get_offset_of_U3CnRangeMaxU3E__0_2() { return static_cast<int32_t>(offsetof(U3CHapticPulsesU3Ec__Iterator0_t1618561021, ___U3CnRangeMaxU3E__0_2)); }
	inline int32_t get_U3CnRangeMaxU3E__0_2() const { return ___U3CnRangeMaxU3E__0_2; }
	inline int32_t* get_address_of_U3CnRangeMaxU3E__0_2() { return &___U3CnRangeMaxU3E__0_2; }
	inline void set_U3CnRangeMaxU3E__0_2(int32_t value)
	{
		___U3CnRangeMaxU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_nCount_3() { return static_cast<int32_t>(offsetof(U3CHapticPulsesU3Ec__Iterator0_t1618561021, ___nCount_3)); }
	inline int32_t get_nCount_3() const { return ___nCount_3; }
	inline int32_t* get_address_of_nCount_3() { return &___nCount_3; }
	inline void set_nCount_3(int32_t value)
	{
		___nCount_3 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_4() { return static_cast<int32_t>(offsetof(U3CHapticPulsesU3Ec__Iterator0_t1618561021, ___U3CiU3E__1_4)); }
	inline uint16_t get_U3CiU3E__1_4() const { return ___U3CiU3E__1_4; }
	inline uint16_t* get_address_of_U3CiU3E__1_4() { return &___U3CiU3E__1_4; }
	inline void set_U3CiU3E__1_4(uint16_t value)
	{
		___U3CiU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CdurationU3E__2_5() { return static_cast<int32_t>(offsetof(U3CHapticPulsesU3Ec__Iterator0_t1618561021, ___U3CdurationU3E__2_5)); }
	inline uint16_t get_U3CdurationU3E__2_5() const { return ___U3CdurationU3E__2_5; }
	inline uint16_t* get_address_of_U3CdurationU3E__2_5() { return &___U3CdurationU3E__2_5; }
	inline void set_U3CdurationU3E__2_5(uint16_t value)
	{
		___U3CdurationU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CHapticPulsesU3Ec__Iterator0_t1618561021, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CHapticPulsesU3Ec__Iterator0_t1618561021, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CHapticPulsesU3Ec__Iterator0_t1618561021, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
