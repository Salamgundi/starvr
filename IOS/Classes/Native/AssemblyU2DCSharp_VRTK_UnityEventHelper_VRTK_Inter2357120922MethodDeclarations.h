﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_InteractUse_UnityEvents
struct VRTK_InteractUse_UnityEvents_t2357120922;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventArgs771291242.h"

// System.Void VRTK.UnityEventHelper.VRTK_InteractUse_UnityEvents::.ctor()
extern "C"  void VRTK_InteractUse_UnityEvents__ctor_m1892592239 (VRTK_InteractUse_UnityEvents_t2357120922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractUse_UnityEvents::SetInteractUse()
extern "C"  void VRTK_InteractUse_UnityEvents_SetInteractUse_m42542826 (VRTK_InteractUse_UnityEvents_t2357120922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractUse_UnityEvents::OnEnable()
extern "C"  void VRTK_InteractUse_UnityEvents_OnEnable_m27114403 (VRTK_InteractUse_UnityEvents_t2357120922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractUse_UnityEvents::ControllerUseInteractableObject(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_InteractUse_UnityEvents_ControllerUseInteractableObject_m2933668268 (VRTK_InteractUse_UnityEvents_t2357120922 * __this, Il2CppObject * ___o0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractUse_UnityEvents::ControllerUnuseInteractableObject(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_InteractUse_UnityEvents_ControllerUnuseInteractableObject_m769206345 (VRTK_InteractUse_UnityEvents_t2357120922 * __this, Il2CppObject * ___o0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_InteractUse_UnityEvents::OnDisable()
extern "C"  void VRTK_InteractUse_UnityEvents_OnDisable_m2124682000 (VRTK_InteractUse_UnityEvents_t2357120922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
