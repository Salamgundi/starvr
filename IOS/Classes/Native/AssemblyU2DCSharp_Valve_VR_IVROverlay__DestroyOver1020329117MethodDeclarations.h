﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_DestroyOverlay
struct _DestroyOverlay_t1020329117;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_DestroyOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _DestroyOverlay__ctor_m863371366 (_DestroyOverlay_t1020329117 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_DestroyOverlay::Invoke(System.UInt64)
extern "C"  int32_t _DestroyOverlay_Invoke_m940991567 (_DestroyOverlay_t1020329117 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_DestroyOverlay::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _DestroyOverlay_BeginInvoke_m1179468508 (_DestroyOverlay_t1020329117 * __this, uint64_t ___ulOverlayHandle0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_DestroyOverlay::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _DestroyOverlay_EndInvoke_m4001013154 (_DestroyOverlay_t1020329117 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
