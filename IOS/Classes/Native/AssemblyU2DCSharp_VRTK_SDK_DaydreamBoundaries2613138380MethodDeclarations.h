﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_DaydreamBoundaries
struct SDK_DaydreamBoundaries_t2613138380;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_DaydreamBoundaries::.ctor()
extern "C"  void SDK_DaydreamBoundaries__ctor_m2702392318 (SDK_DaydreamBoundaries_t2613138380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
