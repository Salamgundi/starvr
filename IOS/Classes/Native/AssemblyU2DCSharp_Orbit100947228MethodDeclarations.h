﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Orbit
struct Orbit_t100947228;

#include "codegen/il2cpp-codegen.h"

// System.Void Orbit::.ctor()
extern "C"  void Orbit__ctor_m3201962997 (Orbit_t100947228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Orbit::Update()
extern "C"  void Orbit_Update_m348480074 (Orbit_t100947228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
