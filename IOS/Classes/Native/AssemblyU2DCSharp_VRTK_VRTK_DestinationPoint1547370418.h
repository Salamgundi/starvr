﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_DestinationPoint
struct VRTK_DestinationPoint_t1547370418;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "AssemblyU2DCSharp_VRTK_VRTK_DestinationMarker667613644.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BasePointerRenderer_Vis517960985.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_DestinationPoint
struct  VRTK_DestinationPoint_t1547370418  : public VRTK_DestinationMarker_t667613644
{
public:
	// UnityEngine.GameObject VRTK.VRTK_DestinationPoint::defaultCursorObject
	GameObject_t1756533147 * ___defaultCursorObject_9;
	// UnityEngine.GameObject VRTK.VRTK_DestinationPoint::hoverCursorObject
	GameObject_t1756533147 * ___hoverCursorObject_10;
	// UnityEngine.GameObject VRTK.VRTK_DestinationPoint::lockedCursorObject
	GameObject_t1756533147 * ___lockedCursorObject_11;
	// System.Boolean VRTK.VRTK_DestinationPoint::snapToPoint
	bool ___snapToPoint_12;
	// System.Boolean VRTK.VRTK_DestinationPoint::hidePointerCursorOnHover
	bool ___hidePointerCursorOnHover_13;
	// UnityEngine.Collider VRTK.VRTK_DestinationPoint::pointCollider
	Collider_t3497673348 * ___pointCollider_15;
	// System.Boolean VRTK.VRTK_DestinationPoint::createdCollider
	bool ___createdCollider_16;
	// UnityEngine.Rigidbody VRTK.VRTK_DestinationPoint::pointRigidbody
	Rigidbody_t4233889191 * ___pointRigidbody_17;
	// System.Boolean VRTK.VRTK_DestinationPoint::createdRigidbody
	bool ___createdRigidbody_18;
	// UnityEngine.Coroutine VRTK.VRTK_DestinationPoint::initaliseListeners
	Coroutine_t2299508840 * ___initaliseListeners_19;
	// System.Boolean VRTK.VRTK_DestinationPoint::isActive
	bool ___isActive_20;
	// VRTK.VRTK_BasePointerRenderer/VisibilityStates VRTK.VRTK_DestinationPoint::storedCursorState
	int32_t ___storedCursorState_21;
	// UnityEngine.Coroutine VRTK.VRTK_DestinationPoint::setDestination
	Coroutine_t2299508840 * ___setDestination_22;
	// System.Boolean VRTK.VRTK_DestinationPoint::currentTeleportState
	bool ___currentTeleportState_23;

public:
	inline static int32_t get_offset_of_defaultCursorObject_9() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418, ___defaultCursorObject_9)); }
	inline GameObject_t1756533147 * get_defaultCursorObject_9() const { return ___defaultCursorObject_9; }
	inline GameObject_t1756533147 ** get_address_of_defaultCursorObject_9() { return &___defaultCursorObject_9; }
	inline void set_defaultCursorObject_9(GameObject_t1756533147 * value)
	{
		___defaultCursorObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___defaultCursorObject_9, value);
	}

	inline static int32_t get_offset_of_hoverCursorObject_10() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418, ___hoverCursorObject_10)); }
	inline GameObject_t1756533147 * get_hoverCursorObject_10() const { return ___hoverCursorObject_10; }
	inline GameObject_t1756533147 ** get_address_of_hoverCursorObject_10() { return &___hoverCursorObject_10; }
	inline void set_hoverCursorObject_10(GameObject_t1756533147 * value)
	{
		___hoverCursorObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___hoverCursorObject_10, value);
	}

	inline static int32_t get_offset_of_lockedCursorObject_11() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418, ___lockedCursorObject_11)); }
	inline GameObject_t1756533147 * get_lockedCursorObject_11() const { return ___lockedCursorObject_11; }
	inline GameObject_t1756533147 ** get_address_of_lockedCursorObject_11() { return &___lockedCursorObject_11; }
	inline void set_lockedCursorObject_11(GameObject_t1756533147 * value)
	{
		___lockedCursorObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___lockedCursorObject_11, value);
	}

	inline static int32_t get_offset_of_snapToPoint_12() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418, ___snapToPoint_12)); }
	inline bool get_snapToPoint_12() const { return ___snapToPoint_12; }
	inline bool* get_address_of_snapToPoint_12() { return &___snapToPoint_12; }
	inline void set_snapToPoint_12(bool value)
	{
		___snapToPoint_12 = value;
	}

	inline static int32_t get_offset_of_hidePointerCursorOnHover_13() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418, ___hidePointerCursorOnHover_13)); }
	inline bool get_hidePointerCursorOnHover_13() const { return ___hidePointerCursorOnHover_13; }
	inline bool* get_address_of_hidePointerCursorOnHover_13() { return &___hidePointerCursorOnHover_13; }
	inline void set_hidePointerCursorOnHover_13(bool value)
	{
		___hidePointerCursorOnHover_13 = value;
	}

	inline static int32_t get_offset_of_pointCollider_15() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418, ___pointCollider_15)); }
	inline Collider_t3497673348 * get_pointCollider_15() const { return ___pointCollider_15; }
	inline Collider_t3497673348 ** get_address_of_pointCollider_15() { return &___pointCollider_15; }
	inline void set_pointCollider_15(Collider_t3497673348 * value)
	{
		___pointCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___pointCollider_15, value);
	}

	inline static int32_t get_offset_of_createdCollider_16() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418, ___createdCollider_16)); }
	inline bool get_createdCollider_16() const { return ___createdCollider_16; }
	inline bool* get_address_of_createdCollider_16() { return &___createdCollider_16; }
	inline void set_createdCollider_16(bool value)
	{
		___createdCollider_16 = value;
	}

	inline static int32_t get_offset_of_pointRigidbody_17() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418, ___pointRigidbody_17)); }
	inline Rigidbody_t4233889191 * get_pointRigidbody_17() const { return ___pointRigidbody_17; }
	inline Rigidbody_t4233889191 ** get_address_of_pointRigidbody_17() { return &___pointRigidbody_17; }
	inline void set_pointRigidbody_17(Rigidbody_t4233889191 * value)
	{
		___pointRigidbody_17 = value;
		Il2CppCodeGenWriteBarrier(&___pointRigidbody_17, value);
	}

	inline static int32_t get_offset_of_createdRigidbody_18() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418, ___createdRigidbody_18)); }
	inline bool get_createdRigidbody_18() const { return ___createdRigidbody_18; }
	inline bool* get_address_of_createdRigidbody_18() { return &___createdRigidbody_18; }
	inline void set_createdRigidbody_18(bool value)
	{
		___createdRigidbody_18 = value;
	}

	inline static int32_t get_offset_of_initaliseListeners_19() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418, ___initaliseListeners_19)); }
	inline Coroutine_t2299508840 * get_initaliseListeners_19() const { return ___initaliseListeners_19; }
	inline Coroutine_t2299508840 ** get_address_of_initaliseListeners_19() { return &___initaliseListeners_19; }
	inline void set_initaliseListeners_19(Coroutine_t2299508840 * value)
	{
		___initaliseListeners_19 = value;
		Il2CppCodeGenWriteBarrier(&___initaliseListeners_19, value);
	}

	inline static int32_t get_offset_of_isActive_20() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418, ___isActive_20)); }
	inline bool get_isActive_20() const { return ___isActive_20; }
	inline bool* get_address_of_isActive_20() { return &___isActive_20; }
	inline void set_isActive_20(bool value)
	{
		___isActive_20 = value;
	}

	inline static int32_t get_offset_of_storedCursorState_21() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418, ___storedCursorState_21)); }
	inline int32_t get_storedCursorState_21() const { return ___storedCursorState_21; }
	inline int32_t* get_address_of_storedCursorState_21() { return &___storedCursorState_21; }
	inline void set_storedCursorState_21(int32_t value)
	{
		___storedCursorState_21 = value;
	}

	inline static int32_t get_offset_of_setDestination_22() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418, ___setDestination_22)); }
	inline Coroutine_t2299508840 * get_setDestination_22() const { return ___setDestination_22; }
	inline Coroutine_t2299508840 ** get_address_of_setDestination_22() { return &___setDestination_22; }
	inline void set_setDestination_22(Coroutine_t2299508840 * value)
	{
		___setDestination_22 = value;
		Il2CppCodeGenWriteBarrier(&___setDestination_22, value);
	}

	inline static int32_t get_offset_of_currentTeleportState_23() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418, ___currentTeleportState_23)); }
	inline bool get_currentTeleportState_23() const { return ___currentTeleportState_23; }
	inline bool* get_address_of_currentTeleportState_23() { return &___currentTeleportState_23; }
	inline void set_currentTeleportState_23(bool value)
	{
		___currentTeleportState_23 = value;
	}
};

struct VRTK_DestinationPoint_t1547370418_StaticFields
{
public:
	// VRTK.VRTK_DestinationPoint VRTK.VRTK_DestinationPoint::currentDestinationPoint
	VRTK_DestinationPoint_t1547370418 * ___currentDestinationPoint_14;

public:
	inline static int32_t get_offset_of_currentDestinationPoint_14() { return static_cast<int32_t>(offsetof(VRTK_DestinationPoint_t1547370418_StaticFields, ___currentDestinationPoint_14)); }
	inline VRTK_DestinationPoint_t1547370418 * get_currentDestinationPoint_14() const { return ___currentDestinationPoint_14; }
	inline VRTK_DestinationPoint_t1547370418 ** get_address_of_currentDestinationPoint_14() { return &___currentDestinationPoint_14; }
	inline void set_currentDestinationPoint_14(VRTK_DestinationPoint_t1547370418 * value)
	{
		___currentDestinationPoint_14 = value;
		Il2CppCodeGenWriteBarrier(&___currentDestinationPoint_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
