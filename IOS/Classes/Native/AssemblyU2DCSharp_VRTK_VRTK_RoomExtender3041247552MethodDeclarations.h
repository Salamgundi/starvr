﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_RoomExtender
struct VRTK_RoomExtender_t3041247552;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_RoomExtender::.ctor()
extern "C"  void VRTK_RoomExtender__ctor_m3259894520 (VRTK_RoomExtender_t3041247552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RoomExtender::Start()
extern "C"  void VRTK_RoomExtender_Start_m402350984 (VRTK_RoomExtender_t3041247552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RoomExtender::Update()
extern "C"  void VRTK_RoomExtender_Update_m384569393 (VRTK_RoomExtender_t3041247552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RoomExtender::Move(UnityEngine.Vector3)
extern "C"  void VRTK_RoomExtender_Move_m435657624 (VRTK_RoomExtender_t3041247552 * __this, Vector3_t2243707580  ___movement0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RoomExtender::MoveHeadCircle()
extern "C"  void VRTK_RoomExtender_MoveHeadCircle_m597962539 (VRTK_RoomExtender_t3041247552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RoomExtender::MoveHeadCircleNonLinearDrift()
extern "C"  void VRTK_RoomExtender_MoveHeadCircleNonLinearDrift_m443783296 (VRTK_RoomExtender_t3041247552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_RoomExtender::UpdateLastMovement()
extern "C"  void VRTK_RoomExtender_UpdateLastMovement_m750923942 (VRTK_RoomExtender_t3041247552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
