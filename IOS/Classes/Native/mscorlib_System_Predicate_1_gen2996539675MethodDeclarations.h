﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct Predicate_1_t2996539675;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_258602264.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2898931145_gshared (Predicate_1_t2996539675 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Predicate_1__ctor_m2898931145(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t2996539675 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2898931145_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4054137757_gshared (Predicate_1_t2996539675 * __this, KeyValuePair_2_t258602264  ___obj0, const MethodInfo* method);
#define Predicate_1_Invoke_m4054137757(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2996539675 *, KeyValuePair_2_t258602264 , const MethodInfo*))Predicate_1_Invoke_m4054137757_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2924426472_gshared (Predicate_1_t2996539675 * __this, KeyValuePair_2_t258602264  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m2924426472(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t2996539675 *, KeyValuePair_2_t258602264 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2924426472_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2875564199_gshared (Predicate_1_t2996539675 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Predicate_1_EndInvoke_m2875564199(__this, ___result0, method) ((  bool (*) (Predicate_1_t2996539675 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m2875564199_gshared)(__this, ___result0, method)
