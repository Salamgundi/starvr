﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIFadeTransition
struct UIFadeTransition_t1996000123;

#include "codegen/il2cpp-codegen.h"

// System.Void UIFadeTransition::.ctor()
extern "C"  void UIFadeTransition__ctor_m277026778 (UIFadeTransition_t1996000123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
