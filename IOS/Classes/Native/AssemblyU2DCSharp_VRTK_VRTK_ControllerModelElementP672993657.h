﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ControllerModelElementPaths
struct  VRTK_ControllerModelElementPaths_t672993657  : public Il2CppObject
{
public:
	// System.String VRTK.VRTK_ControllerModelElementPaths::bodyModelPath
	String_t* ___bodyModelPath_0;
	// System.String VRTK.VRTK_ControllerModelElementPaths::triggerModelPath
	String_t* ___triggerModelPath_1;
	// System.String VRTK.VRTK_ControllerModelElementPaths::leftGripModelPath
	String_t* ___leftGripModelPath_2;
	// System.String VRTK.VRTK_ControllerModelElementPaths::rightGripModelPath
	String_t* ___rightGripModelPath_3;
	// System.String VRTK.VRTK_ControllerModelElementPaths::touchpadModelPath
	String_t* ___touchpadModelPath_4;
	// System.String VRTK.VRTK_ControllerModelElementPaths::buttonOneModelPath
	String_t* ___buttonOneModelPath_5;
	// System.String VRTK.VRTK_ControllerModelElementPaths::buttonTwoModelPath
	String_t* ___buttonTwoModelPath_6;
	// System.String VRTK.VRTK_ControllerModelElementPaths::systemMenuModelPath
	String_t* ___systemMenuModelPath_7;
	// System.String VRTK.VRTK_ControllerModelElementPaths::startMenuModelPath
	String_t* ___startMenuModelPath_8;

public:
	inline static int32_t get_offset_of_bodyModelPath_0() { return static_cast<int32_t>(offsetof(VRTK_ControllerModelElementPaths_t672993657, ___bodyModelPath_0)); }
	inline String_t* get_bodyModelPath_0() const { return ___bodyModelPath_0; }
	inline String_t** get_address_of_bodyModelPath_0() { return &___bodyModelPath_0; }
	inline void set_bodyModelPath_0(String_t* value)
	{
		___bodyModelPath_0 = value;
		Il2CppCodeGenWriteBarrier(&___bodyModelPath_0, value);
	}

	inline static int32_t get_offset_of_triggerModelPath_1() { return static_cast<int32_t>(offsetof(VRTK_ControllerModelElementPaths_t672993657, ___triggerModelPath_1)); }
	inline String_t* get_triggerModelPath_1() const { return ___triggerModelPath_1; }
	inline String_t** get_address_of_triggerModelPath_1() { return &___triggerModelPath_1; }
	inline void set_triggerModelPath_1(String_t* value)
	{
		___triggerModelPath_1 = value;
		Il2CppCodeGenWriteBarrier(&___triggerModelPath_1, value);
	}

	inline static int32_t get_offset_of_leftGripModelPath_2() { return static_cast<int32_t>(offsetof(VRTK_ControllerModelElementPaths_t672993657, ___leftGripModelPath_2)); }
	inline String_t* get_leftGripModelPath_2() const { return ___leftGripModelPath_2; }
	inline String_t** get_address_of_leftGripModelPath_2() { return &___leftGripModelPath_2; }
	inline void set_leftGripModelPath_2(String_t* value)
	{
		___leftGripModelPath_2 = value;
		Il2CppCodeGenWriteBarrier(&___leftGripModelPath_2, value);
	}

	inline static int32_t get_offset_of_rightGripModelPath_3() { return static_cast<int32_t>(offsetof(VRTK_ControllerModelElementPaths_t672993657, ___rightGripModelPath_3)); }
	inline String_t* get_rightGripModelPath_3() const { return ___rightGripModelPath_3; }
	inline String_t** get_address_of_rightGripModelPath_3() { return &___rightGripModelPath_3; }
	inline void set_rightGripModelPath_3(String_t* value)
	{
		___rightGripModelPath_3 = value;
		Il2CppCodeGenWriteBarrier(&___rightGripModelPath_3, value);
	}

	inline static int32_t get_offset_of_touchpadModelPath_4() { return static_cast<int32_t>(offsetof(VRTK_ControllerModelElementPaths_t672993657, ___touchpadModelPath_4)); }
	inline String_t* get_touchpadModelPath_4() const { return ___touchpadModelPath_4; }
	inline String_t** get_address_of_touchpadModelPath_4() { return &___touchpadModelPath_4; }
	inline void set_touchpadModelPath_4(String_t* value)
	{
		___touchpadModelPath_4 = value;
		Il2CppCodeGenWriteBarrier(&___touchpadModelPath_4, value);
	}

	inline static int32_t get_offset_of_buttonOneModelPath_5() { return static_cast<int32_t>(offsetof(VRTK_ControllerModelElementPaths_t672993657, ___buttonOneModelPath_5)); }
	inline String_t* get_buttonOneModelPath_5() const { return ___buttonOneModelPath_5; }
	inline String_t** get_address_of_buttonOneModelPath_5() { return &___buttonOneModelPath_5; }
	inline void set_buttonOneModelPath_5(String_t* value)
	{
		___buttonOneModelPath_5 = value;
		Il2CppCodeGenWriteBarrier(&___buttonOneModelPath_5, value);
	}

	inline static int32_t get_offset_of_buttonTwoModelPath_6() { return static_cast<int32_t>(offsetof(VRTK_ControllerModelElementPaths_t672993657, ___buttonTwoModelPath_6)); }
	inline String_t* get_buttonTwoModelPath_6() const { return ___buttonTwoModelPath_6; }
	inline String_t** get_address_of_buttonTwoModelPath_6() { return &___buttonTwoModelPath_6; }
	inline void set_buttonTwoModelPath_6(String_t* value)
	{
		___buttonTwoModelPath_6 = value;
		Il2CppCodeGenWriteBarrier(&___buttonTwoModelPath_6, value);
	}

	inline static int32_t get_offset_of_systemMenuModelPath_7() { return static_cast<int32_t>(offsetof(VRTK_ControllerModelElementPaths_t672993657, ___systemMenuModelPath_7)); }
	inline String_t* get_systemMenuModelPath_7() const { return ___systemMenuModelPath_7; }
	inline String_t** get_address_of_systemMenuModelPath_7() { return &___systemMenuModelPath_7; }
	inline void set_systemMenuModelPath_7(String_t* value)
	{
		___systemMenuModelPath_7 = value;
		Il2CppCodeGenWriteBarrier(&___systemMenuModelPath_7, value);
	}

	inline static int32_t get_offset_of_startMenuModelPath_8() { return static_cast<int32_t>(offsetof(VRTK_ControllerModelElementPaths_t672993657, ___startMenuModelPath_8)); }
	inline String_t* get_startMenuModelPath_8() const { return ___startMenuModelPath_8; }
	inline String_t** get_address_of_startMenuModelPath_8() { return &___startMenuModelPath_8; }
	inline void set_startMenuModelPath_8(String_t* value)
	{
		___startMenuModelPath_8 = value;
		Il2CppCodeGenWriteBarrier(&___startMenuModelPath_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
