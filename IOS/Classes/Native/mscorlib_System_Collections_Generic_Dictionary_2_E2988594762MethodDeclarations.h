﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t1668570060;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2988594762.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23720882578.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1050411420_gshared (Enumerator_t2988594762 * __this, Dictionary_2_t1668570060 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1050411420(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2988594762 *, Dictionary_2_t1668570060 *, const MethodInfo*))Enumerator__ctor_m1050411420_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1686586217_gshared (Enumerator_t2988594762 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1686586217(__this, method) ((  Il2CppObject * (*) (Enumerator_t2988594762 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1686586217_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4227218433_gshared (Enumerator_t2988594762 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4227218433(__this, method) ((  void (*) (Enumerator_t2988594762 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4227218433_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m542906350_gshared (Enumerator_t2988594762 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m542906350(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2988594762 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m542906350_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3398460119_gshared (Enumerator_t2988594762 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3398460119(__this, method) ((  Il2CppObject * (*) (Enumerator_t2988594762 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3398460119_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m700552431_gshared (Enumerator_t2988594762 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m700552431(__this, method) ((  Il2CppObject * (*) (Enumerator_t2988594762 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m700552431_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1671601793_gshared (Enumerator_t2988594762 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1671601793(__this, method) ((  bool (*) (Enumerator_t2988594762 *, const MethodInfo*))Enumerator_MoveNext_m1671601793_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::get_Current()
extern "C"  KeyValuePair_2_t3720882578  Enumerator_get_Current_m2163799293_gshared (Enumerator_t2988594762 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2163799293(__this, method) ((  KeyValuePair_2_t3720882578  (*) (Enumerator_t2988594762 *, const MethodInfo*))Enumerator_get_Current_m2163799293_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m2539943900_gshared (Enumerator_t2988594762 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2539943900(__this, method) ((  Il2CppObject * (*) (Enumerator_t2988594762 *, const MethodInfo*))Enumerator_get_CurrentKey_m2539943900_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::get_CurrentValue()
extern "C"  float Enumerator_get_CurrentValue_m3141163996_gshared (Enumerator_t2988594762 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3141163996(__this, method) ((  float (*) (Enumerator_t2988594762 *, const MethodInfo*))Enumerator_get_CurrentValue_m3141163996_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::Reset()
extern "C"  void Enumerator_Reset_m308315030_gshared (Enumerator_t2988594762 * __this, const MethodInfo* method);
#define Enumerator_Reset_m308315030(__this, method) ((  void (*) (Enumerator_t2988594762 *, const MethodInfo*))Enumerator_Reset_m308315030_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3000094783_gshared (Enumerator_t2988594762 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3000094783(__this, method) ((  void (*) (Enumerator_t2988594762 *, const MethodInfo*))Enumerator_VerifyState_m3000094783_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m220863089_gshared (Enumerator_t2988594762 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m220863089(__this, method) ((  void (*) (Enumerator_t2988594762 *, const MethodInfo*))Enumerator_VerifyCurrent_m220863089_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m884018764_gshared (Enumerator_t2988594762 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m884018764(__this, method) ((  void (*) (Enumerator_t2988594762 *, const MethodInfo*))Enumerator_Dispose_m884018764_gshared)(__this, method)
