﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// VRTK.RadialMenu
struct RadialMenu_t1576296262;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.RadialMenuController
struct  RadialMenuController_t89584558  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_ControllerEvents VRTK.RadialMenuController::events
	VRTK_ControllerEvents_t3225224819 * ___events_2;
	// VRTK.RadialMenu VRTK.RadialMenuController::menu
	RadialMenu_t1576296262 * ___menu_3;
	// System.Single VRTK.RadialMenuController::currentAngle
	float ___currentAngle_4;
	// System.Boolean VRTK.RadialMenuController::touchpadTouched
	bool ___touchpadTouched_5;

public:
	inline static int32_t get_offset_of_events_2() { return static_cast<int32_t>(offsetof(RadialMenuController_t89584558, ___events_2)); }
	inline VRTK_ControllerEvents_t3225224819 * get_events_2() const { return ___events_2; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_events_2() { return &___events_2; }
	inline void set_events_2(VRTK_ControllerEvents_t3225224819 * value)
	{
		___events_2 = value;
		Il2CppCodeGenWriteBarrier(&___events_2, value);
	}

	inline static int32_t get_offset_of_menu_3() { return static_cast<int32_t>(offsetof(RadialMenuController_t89584558, ___menu_3)); }
	inline RadialMenu_t1576296262 * get_menu_3() const { return ___menu_3; }
	inline RadialMenu_t1576296262 ** get_address_of_menu_3() { return &___menu_3; }
	inline void set_menu_3(RadialMenu_t1576296262 * value)
	{
		___menu_3 = value;
		Il2CppCodeGenWriteBarrier(&___menu_3, value);
	}

	inline static int32_t get_offset_of_currentAngle_4() { return static_cast<int32_t>(offsetof(RadialMenuController_t89584558, ___currentAngle_4)); }
	inline float get_currentAngle_4() const { return ___currentAngle_4; }
	inline float* get_address_of_currentAngle_4() { return &___currentAngle_4; }
	inline void set_currentAngle_4(float value)
	{
		___currentAngle_4 = value;
	}

	inline static int32_t get_offset_of_touchpadTouched_5() { return static_cast<int32_t>(offsetof(RadialMenuController_t89584558, ___touchpadTouched_5)); }
	inline bool get_touchpadTouched_5() const { return ___touchpadTouched_5; }
	inline bool* get_address_of_touchpadTouched_5() { return &___touchpadTouched_5; }
	inline void set_touchpadTouched_5(bool value)
	{
		___touchpadTouched_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
