﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FadeScrollEffect
struct FadeScrollEffect_t2128935010;

#include "codegen/il2cpp-codegen.h"

// System.Void FadeScrollEffect::.ctor()
extern "C"  void FadeScrollEffect__ctor_m3546210939 (FadeScrollEffect_t2128935010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
