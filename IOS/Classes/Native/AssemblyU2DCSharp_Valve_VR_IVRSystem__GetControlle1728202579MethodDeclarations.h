﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetControllerRoleForTrackedDeviceIndex
struct _GetControllerRoleForTrackedDeviceIndex_t1728202579;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedControllerRole361251409.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetControllerRoleForTrackedDeviceIndex::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetControllerRoleForTrackedDeviceIndex__ctor_m1818992616 (_GetControllerRoleForTrackedDeviceIndex_t1728202579 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.ETrackedControllerRole Valve.VR.IVRSystem/_GetControllerRoleForTrackedDeviceIndex::Invoke(System.UInt32)
extern "C"  int32_t _GetControllerRoleForTrackedDeviceIndex_Invoke_m4099929182 (_GetControllerRoleForTrackedDeviceIndex_t1728202579 * __this, uint32_t ___unDeviceIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetControllerRoleForTrackedDeviceIndex::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetControllerRoleForTrackedDeviceIndex_BeginInvoke_m976477837 (_GetControllerRoleForTrackedDeviceIndex_t1728202579 * __this, uint32_t ___unDeviceIndex0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.ETrackedControllerRole Valve.VR.IVRSystem/_GetControllerRoleForTrackedDeviceIndex::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _GetControllerRoleForTrackedDeviceIndex_EndInvoke_m4028217496 (_GetControllerRoleForTrackedDeviceIndex_t1728202579 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
