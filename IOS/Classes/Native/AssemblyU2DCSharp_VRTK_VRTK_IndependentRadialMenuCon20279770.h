﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_IndependentRadialMenuController
struct VRTK_IndependentRadialMenuController_t4268715152;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0
struct  U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770  : public Il2CppObject
{
public:
	// System.Single VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0::delay
	float ___delay_0;
	// System.Boolean VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0::enabled
	bool ___enabled_1;
	// VRTK.InteractableObjectEventArgs VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0::e
	InteractableObjectEventArgs_t473175556  ___e_2;
	// VRTK.VRTK_IndependentRadialMenuController VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0::$this
	VRTK_IndependentRadialMenuController_t4268715152 * ___U24this_3;
	// System.Object VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 VRTK.VRTK_IndependentRadialMenuController/<DelayedSetColliderEnabled>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_enabled_1() { return static_cast<int32_t>(offsetof(U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770, ___enabled_1)); }
	inline bool get_enabled_1() const { return ___enabled_1; }
	inline bool* get_address_of_enabled_1() { return &___enabled_1; }
	inline void set_enabled_1(bool value)
	{
		___enabled_1 = value;
	}

	inline static int32_t get_offset_of_e_2() { return static_cast<int32_t>(offsetof(U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770, ___e_2)); }
	inline InteractableObjectEventArgs_t473175556  get_e_2() const { return ___e_2; }
	inline InteractableObjectEventArgs_t473175556 * get_address_of_e_2() { return &___e_2; }
	inline void set_e_2(InteractableObjectEventArgs_t473175556  value)
	{
		___e_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770, ___U24this_3)); }
	inline VRTK_IndependentRadialMenuController_t4268715152 * get_U24this_3() const { return ___U24this_3; }
	inline VRTK_IndependentRadialMenuController_t4268715152 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(VRTK_IndependentRadialMenuController_t4268715152 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDelayedSetColliderEnabledU3Ec__Iterator0_t20279770, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
