﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.SoundPlayOneshot
struct SoundPlayOneshot_t1703214483;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.SoundPlayOneshot::.ctor()
extern "C"  void SoundPlayOneshot__ctor_m2284972597 (SoundPlayOneshot_t1703214483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SoundPlayOneshot::Awake()
extern "C"  void SoundPlayOneshot_Awake_m1242178212 (SoundPlayOneshot_t1703214483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SoundPlayOneshot::Play()
extern "C"  void SoundPlayOneshot_Play_m259358005 (SoundPlayOneshot_t1703214483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SoundPlayOneshot::Pause()
extern "C"  void SoundPlayOneshot_Pause_m1915607811 (SoundPlayOneshot_t1703214483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.SoundPlayOneshot::UnPause()
extern "C"  void SoundPlayOneshot_UnPause_m4151315188 (SoundPlayOneshot_t1703214483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
