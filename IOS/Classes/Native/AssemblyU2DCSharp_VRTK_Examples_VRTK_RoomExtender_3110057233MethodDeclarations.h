﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.VRTK_RoomExtender_ControllerExample
struct VRTK_RoomExtender_ControllerExample_t3110057233;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"

// System.Void VRTK.Examples.VRTK_RoomExtender_ControllerExample::.ctor()
extern "C"  void VRTK_RoomExtender_ControllerExample__ctor_m3900213958 (VRTK_RoomExtender_ControllerExample_t3110057233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_RoomExtender_ControllerExample::Start()
extern "C"  void VRTK_RoomExtender_ControllerExample_Start_m448983786 (VRTK_RoomExtender_ControllerExample_t3110057233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_RoomExtender_ControllerExample::DoTouchpadPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_RoomExtender_ControllerExample_DoTouchpadPressed_m136492425 (VRTK_RoomExtender_ControllerExample_t3110057233 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_RoomExtender_ControllerExample::DoTouchpadReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_RoomExtender_ControllerExample_DoTouchpadReleased_m2732320678 (VRTK_RoomExtender_ControllerExample_t3110057233 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_RoomExtender_ControllerExample::DoSwitchMovementFunction(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_RoomExtender_ControllerExample_DoSwitchMovementFunction_m4156028332 (VRTK_RoomExtender_ControllerExample_t3110057233 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_RoomExtender_ControllerExample::EnableAdditionalMovement()
extern "C"  void VRTK_RoomExtender_ControllerExample_EnableAdditionalMovement_m3394508293 (VRTK_RoomExtender_ControllerExample_t3110057233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_RoomExtender_ControllerExample::DisableAdditionalMovement()
extern "C"  void VRTK_RoomExtender_ControllerExample_DisableAdditionalMovement_m816463730 (VRTK_RoomExtender_ControllerExample_t3110057233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
