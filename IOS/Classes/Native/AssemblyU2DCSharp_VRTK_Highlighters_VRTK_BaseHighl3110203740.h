﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Highlighters.VRTK_BaseHighlighter
struct  VRTK_BaseHighlighter_t3110203740  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.Highlighters.VRTK_BaseHighlighter::active
	bool ___active_2;
	// System.Boolean VRTK.Highlighters.VRTK_BaseHighlighter::unhighlightOnDisable
	bool ___unhighlightOnDisable_3;
	// System.Boolean VRTK.Highlighters.VRTK_BaseHighlighter::usesClonedObject
	bool ___usesClonedObject_4;

public:
	inline static int32_t get_offset_of_active_2() { return static_cast<int32_t>(offsetof(VRTK_BaseHighlighter_t3110203740, ___active_2)); }
	inline bool get_active_2() const { return ___active_2; }
	inline bool* get_address_of_active_2() { return &___active_2; }
	inline void set_active_2(bool value)
	{
		___active_2 = value;
	}

	inline static int32_t get_offset_of_unhighlightOnDisable_3() { return static_cast<int32_t>(offsetof(VRTK_BaseHighlighter_t3110203740, ___unhighlightOnDisable_3)); }
	inline bool get_unhighlightOnDisable_3() const { return ___unhighlightOnDisable_3; }
	inline bool* get_address_of_unhighlightOnDisable_3() { return &___unhighlightOnDisable_3; }
	inline void set_unhighlightOnDisable_3(bool value)
	{
		___unhighlightOnDisable_3 = value;
	}

	inline static int32_t get_offset_of_usesClonedObject_4() { return static_cast<int32_t>(offsetof(VRTK_BaseHighlighter_t3110203740, ___usesClonedObject_4)); }
	inline bool get_usesClonedObject_4() const { return ___usesClonedObject_4; }
	inline bool* get_address_of_usesClonedObject_4() { return &___usesClonedObject_4; }
	inline void set_usesClonedObject_4(bool value)
	{
		___usesClonedObject_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
