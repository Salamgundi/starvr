﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRExtendedDisplay
struct IVRExtendedDisplay_t2045258050;
struct IVRExtendedDisplay_t2045258050_marshaled_pinvoke;
struct IVRExtendedDisplay_t2045258050_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IVRExtendedDisplay_t2045258050;
struct IVRExtendedDisplay_t2045258050_marshaled_pinvoke;

extern "C" void IVRExtendedDisplay_t2045258050_marshal_pinvoke(const IVRExtendedDisplay_t2045258050& unmarshaled, IVRExtendedDisplay_t2045258050_marshaled_pinvoke& marshaled);
extern "C" void IVRExtendedDisplay_t2045258050_marshal_pinvoke_back(const IVRExtendedDisplay_t2045258050_marshaled_pinvoke& marshaled, IVRExtendedDisplay_t2045258050& unmarshaled);
extern "C" void IVRExtendedDisplay_t2045258050_marshal_pinvoke_cleanup(IVRExtendedDisplay_t2045258050_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IVRExtendedDisplay_t2045258050;
struct IVRExtendedDisplay_t2045258050_marshaled_com;

extern "C" void IVRExtendedDisplay_t2045258050_marshal_com(const IVRExtendedDisplay_t2045258050& unmarshaled, IVRExtendedDisplay_t2045258050_marshaled_com& marshaled);
extern "C" void IVRExtendedDisplay_t2045258050_marshal_com_back(const IVRExtendedDisplay_t2045258050_marshaled_com& marshaled, IVRExtendedDisplay_t2045258050& unmarshaled);
extern "C" void IVRExtendedDisplay_t2045258050_marshal_com_cleanup(IVRExtendedDisplay_t2045258050_marshaled_com& marshaled);
