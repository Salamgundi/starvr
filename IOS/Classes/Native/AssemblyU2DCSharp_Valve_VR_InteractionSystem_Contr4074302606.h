﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.ControllerHintsExample
struct  ControllerHintsExample_t4074302606  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Coroutine Valve.VR.InteractionSystem.ControllerHintsExample::buttonHintCoroutine
	Coroutine_t2299508840 * ___buttonHintCoroutine_2;
	// UnityEngine.Coroutine Valve.VR.InteractionSystem.ControllerHintsExample::textHintCoroutine
	Coroutine_t2299508840 * ___textHintCoroutine_3;

public:
	inline static int32_t get_offset_of_buttonHintCoroutine_2() { return static_cast<int32_t>(offsetof(ControllerHintsExample_t4074302606, ___buttonHintCoroutine_2)); }
	inline Coroutine_t2299508840 * get_buttonHintCoroutine_2() const { return ___buttonHintCoroutine_2; }
	inline Coroutine_t2299508840 ** get_address_of_buttonHintCoroutine_2() { return &___buttonHintCoroutine_2; }
	inline void set_buttonHintCoroutine_2(Coroutine_t2299508840 * value)
	{
		___buttonHintCoroutine_2 = value;
		Il2CppCodeGenWriteBarrier(&___buttonHintCoroutine_2, value);
	}

	inline static int32_t get_offset_of_textHintCoroutine_3() { return static_cast<int32_t>(offsetof(ControllerHintsExample_t4074302606, ___textHintCoroutine_3)); }
	inline Coroutine_t2299508840 * get_textHintCoroutine_3() const { return ___textHintCoroutine_3; }
	inline Coroutine_t2299508840 ** get_address_of_textHintCoroutine_3() { return &___textHintCoroutine_3; }
	inline void set_textHintCoroutine_3(Coroutine_t2299508840 * value)
	{
		___textHintCoroutine_3 = value;
		Il2CppCodeGenWriteBarrier(&___textHintCoroutine_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
