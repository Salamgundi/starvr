﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_UIDraggableItem
struct VRTK_UIDraggableItem_t2269178406;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_UIDropZone
struct  VRTK_UIDropZone_t1661569883  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_UIDraggableItem VRTK.VRTK_UIDropZone::droppableItem
	VRTK_UIDraggableItem_t2269178406 * ___droppableItem_2;

public:
	inline static int32_t get_offset_of_droppableItem_2() { return static_cast<int32_t>(offsetof(VRTK_UIDropZone_t1661569883, ___droppableItem_2)); }
	inline VRTK_UIDraggableItem_t2269178406 * get_droppableItem_2() const { return ___droppableItem_2; }
	inline VRTK_UIDraggableItem_t2269178406 ** get_address_of_droppableItem_2() { return &___droppableItem_2; }
	inline void set_droppableItem_2(VRTK_UIDraggableItem_t2269178406 * value)
	{
		___droppableItem_2 = value;
		Il2CppCodeGenWriteBarrier(&___droppableItem_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
