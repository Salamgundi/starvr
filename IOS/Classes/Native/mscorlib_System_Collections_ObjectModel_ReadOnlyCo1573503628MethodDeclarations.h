﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct ReadOnlyCollection_1_t1573503628;
// System.Collections.Generic.IList`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct IList_1_t1928658537;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Valve.VR.InteractionSystem.Hand/AttachedObject[]
struct AttachedObjectU5BU5D_t2422108177;
// System.Collections.Generic.IEnumerator`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct IEnumerator_1_t3158209059;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1387717936.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2816348928_gshared (ReadOnlyCollection_1_t1573503628 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2816348928(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1573503628 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2816348928_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1185479704_gshared (ReadOnlyCollection_1_t1573503628 * __this, AttachedObject_t1387717936  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1185479704(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1573503628 *, AttachedObject_t1387717936 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1185479704_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2944879108_gshared (ReadOnlyCollection_1_t1573503628 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2944879108(__this, method) ((  void (*) (ReadOnlyCollection_1_t1573503628 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2944879108_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1621455931_gshared (ReadOnlyCollection_1_t1573503628 * __this, int32_t ___index0, AttachedObject_t1387717936  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1621455931(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1573503628 *, int32_t, AttachedObject_t1387717936 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1621455931_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m54291075_gshared (ReadOnlyCollection_1_t1573503628 * __this, AttachedObject_t1387717936  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m54291075(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1573503628 *, AttachedObject_t1387717936 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m54291075_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3235897919_gshared (ReadOnlyCollection_1_t1573503628 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3235897919(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1573503628 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3235897919_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  AttachedObject_t1387717936  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2228797379_gshared (ReadOnlyCollection_1_t1573503628 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2228797379(__this, ___index0, method) ((  AttachedObject_t1387717936  (*) (ReadOnlyCollection_1_t1573503628 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2228797379_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2351092826_gshared (ReadOnlyCollection_1_t1573503628 * __this, int32_t ___index0, AttachedObject_t1387717936  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2351092826(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1573503628 *, int32_t, AttachedObject_t1387717936 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2351092826_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3858078034_gshared (ReadOnlyCollection_1_t1573503628 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3858078034(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1573503628 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3858078034_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1366768529_gshared (ReadOnlyCollection_1_t1573503628 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1366768529(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1573503628 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1366768529_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2275961762_gshared (ReadOnlyCollection_1_t1573503628 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2275961762(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1573503628 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2275961762_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1326729793_gshared (ReadOnlyCollection_1_t1573503628 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1326729793(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1573503628 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1326729793_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m967876041_gshared (ReadOnlyCollection_1_t1573503628 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m967876041(__this, method) ((  void (*) (ReadOnlyCollection_1_t1573503628 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m967876041_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3364203073_gshared (ReadOnlyCollection_1_t1573503628 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3364203073(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1573503628 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3364203073_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2875155503_gshared (ReadOnlyCollection_1_t1573503628 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2875155503(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1573503628 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2875155503_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3720645528_gshared (ReadOnlyCollection_1_t1573503628 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3720645528(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1573503628 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3720645528_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m83751834_gshared (ReadOnlyCollection_1_t1573503628 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m83751834(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1573503628 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m83751834_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2188645794_gshared (ReadOnlyCollection_1_t1573503628 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2188645794(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1573503628 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2188645794_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3957315885_gshared (ReadOnlyCollection_1_t1573503628 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3957315885(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1573503628 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3957315885_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1721551037_gshared (ReadOnlyCollection_1_t1573503628 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1721551037(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1573503628 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1721551037_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1053826648_gshared (ReadOnlyCollection_1_t1573503628 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1053826648(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1573503628 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1053826648_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m4007156593_gshared (ReadOnlyCollection_1_t1573503628 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m4007156593(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1573503628 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m4007156593_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3553509034_gshared (ReadOnlyCollection_1_t1573503628 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3553509034(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1573503628 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3553509034_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1568925015_gshared (ReadOnlyCollection_1_t1573503628 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1568925015(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1573503628 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1568925015_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2574736594_gshared (ReadOnlyCollection_1_t1573503628 * __this, AttachedObject_t1387717936  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2574736594(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1573503628 *, AttachedObject_t1387717936 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2574736594_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2549991868_gshared (ReadOnlyCollection_1_t1573503628 * __this, AttachedObjectU5BU5D_t2422108177* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2549991868(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1573503628 *, AttachedObjectU5BU5D_t2422108177*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2549991868_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2511519153_gshared (ReadOnlyCollection_1_t1573503628 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2511519153(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1573503628 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2511519153_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m503339794_gshared (ReadOnlyCollection_1_t1573503628 * __this, AttachedObject_t1387717936  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m503339794(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1573503628 *, AttachedObject_t1387717936 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m503339794_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1946804726_gshared (ReadOnlyCollection_1_t1573503628 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1946804726(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1573503628 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1946804726_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Valve.VR.InteractionSystem.Hand/AttachedObject>::get_Item(System.Int32)
extern "C"  AttachedObject_t1387717936  ReadOnlyCollection_1_get_Item_m3798891705_gshared (ReadOnlyCollection_1_t1573503628 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3798891705(__this, ___index0, method) ((  AttachedObject_t1387717936  (*) (ReadOnlyCollection_1_t1573503628 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3798891705_gshared)(__this, ___index0, method)
