﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SteamVR_Events/Event`1<System.Boolean>
struct Event_1_t1706029839;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t897193173;

#include "AssemblyU2DCSharp_SteamVR_Events_Action1836998693.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SteamVR_Events/Action`1<System.Boolean>
struct  Action_1_t988986007  : public Action_t1836998693
{
public:
	// SteamVR_Events/Event`1<T> SteamVR_Events/Action`1::_event
	Event_1_t1706029839 * ____event_0;
	// UnityEngine.Events.UnityAction`1<T> SteamVR_Events/Action`1::action
	UnityAction_1_t897193173 * ___action_1;

public:
	inline static int32_t get_offset_of__event_0() { return static_cast<int32_t>(offsetof(Action_1_t988986007, ____event_0)); }
	inline Event_1_t1706029839 * get__event_0() const { return ____event_0; }
	inline Event_1_t1706029839 ** get_address_of__event_0() { return &____event_0; }
	inline void set__event_0(Event_1_t1706029839 * value)
	{
		____event_0 = value;
		Il2CppCodeGenWriteBarrier(&____event_0, value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(Action_1_t988986007, ___action_1)); }
	inline UnityAction_1_t897193173 * get_action_1() const { return ___action_1; }
	inline UnityAction_1_t897193173 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(UnityAction_1_t897193173 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
