﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_VRTK_BaseObjectControlActio3375001897.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_SnapRotateObjectControlAction
struct  VRTK_SnapRotateObjectControlAction_t1299063727  : public VRTK_BaseObjectControlAction_t3375001897
{
public:
	// System.Single VRTK.VRTK_SnapRotateObjectControlAction::anglePerSnap
	float ___anglePerSnap_10;
	// System.Single VRTK.VRTK_SnapRotateObjectControlAction::angleMultiplier
	float ___angleMultiplier_11;
	// System.Single VRTK.VRTK_SnapRotateObjectControlAction::snapDelay
	float ___snapDelay_12;
	// System.Single VRTK.VRTK_SnapRotateObjectControlAction::blinkTransitionSpeed
	float ___blinkTransitionSpeed_13;
	// System.Single VRTK.VRTK_SnapRotateObjectControlAction::axisThreshold
	float ___axisThreshold_14;
	// System.Single VRTK.VRTK_SnapRotateObjectControlAction::snapDelayTimer
	float ___snapDelayTimer_15;

public:
	inline static int32_t get_offset_of_anglePerSnap_10() { return static_cast<int32_t>(offsetof(VRTK_SnapRotateObjectControlAction_t1299063727, ___anglePerSnap_10)); }
	inline float get_anglePerSnap_10() const { return ___anglePerSnap_10; }
	inline float* get_address_of_anglePerSnap_10() { return &___anglePerSnap_10; }
	inline void set_anglePerSnap_10(float value)
	{
		___anglePerSnap_10 = value;
	}

	inline static int32_t get_offset_of_angleMultiplier_11() { return static_cast<int32_t>(offsetof(VRTK_SnapRotateObjectControlAction_t1299063727, ___angleMultiplier_11)); }
	inline float get_angleMultiplier_11() const { return ___angleMultiplier_11; }
	inline float* get_address_of_angleMultiplier_11() { return &___angleMultiplier_11; }
	inline void set_angleMultiplier_11(float value)
	{
		___angleMultiplier_11 = value;
	}

	inline static int32_t get_offset_of_snapDelay_12() { return static_cast<int32_t>(offsetof(VRTK_SnapRotateObjectControlAction_t1299063727, ___snapDelay_12)); }
	inline float get_snapDelay_12() const { return ___snapDelay_12; }
	inline float* get_address_of_snapDelay_12() { return &___snapDelay_12; }
	inline void set_snapDelay_12(float value)
	{
		___snapDelay_12 = value;
	}

	inline static int32_t get_offset_of_blinkTransitionSpeed_13() { return static_cast<int32_t>(offsetof(VRTK_SnapRotateObjectControlAction_t1299063727, ___blinkTransitionSpeed_13)); }
	inline float get_blinkTransitionSpeed_13() const { return ___blinkTransitionSpeed_13; }
	inline float* get_address_of_blinkTransitionSpeed_13() { return &___blinkTransitionSpeed_13; }
	inline void set_blinkTransitionSpeed_13(float value)
	{
		___blinkTransitionSpeed_13 = value;
	}

	inline static int32_t get_offset_of_axisThreshold_14() { return static_cast<int32_t>(offsetof(VRTK_SnapRotateObjectControlAction_t1299063727, ___axisThreshold_14)); }
	inline float get_axisThreshold_14() const { return ___axisThreshold_14; }
	inline float* get_address_of_axisThreshold_14() { return &___axisThreshold_14; }
	inline void set_axisThreshold_14(float value)
	{
		___axisThreshold_14 = value;
	}

	inline static int32_t get_offset_of_snapDelayTimer_15() { return static_cast<int32_t>(offsetof(VRTK_SnapRotateObjectControlAction_t1299063727, ___snapDelayTimer_15)); }
	inline float get_snapDelayTimer_15() const { return ___snapDelayTimer_15; }
	inline float* get_address_of_snapDelayTimer_15() { return &___snapDelayTimer_15; }
	inline void set_snapDelayTimer_15(float value)
	{
		___snapDelayTimer_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
