﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_Burst208217445.h"

// System.Void UnityEngine.ParticleSystem/Burst::set_time(System.Single)
extern "C"  void Burst_set_time_m2249016428 (Burst_t208217445 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Burst::set_minCount(System.Int16)
extern "C"  void Burst_set_minCount_m2527778498 (Burst_t208217445 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Burst::set_maxCount(System.Int16)
extern "C"  void Burst_set_maxCount_m3745844044 (Burst_t208217445 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
