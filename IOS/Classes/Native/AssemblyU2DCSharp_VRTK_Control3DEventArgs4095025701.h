﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Control3DEventArgs
struct  Control3DEventArgs_t4095025701 
{
public:
	// System.Single VRTK.Control3DEventArgs::value
	float ___value_0;
	// System.Single VRTK.Control3DEventArgs::normalizedValue
	float ___normalizedValue_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Control3DEventArgs_t4095025701, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_normalizedValue_1() { return static_cast<int32_t>(offsetof(Control3DEventArgs_t4095025701, ___normalizedValue_1)); }
	inline float get_normalizedValue_1() const { return ___normalizedValue_1; }
	inline float* get_address_of_normalizedValue_1() { return &___normalizedValue_1; }
	inline void set_normalizedValue_1(float value)
	{
		___normalizedValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
