﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.GrabAttachMechanics.VRTK_RotatorTrackGrabAttach
struct VRTK_RotatorTrackGrabAttach_t3062106299;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void VRTK.GrabAttachMechanics.VRTK_RotatorTrackGrabAttach::.ctor()
extern "C"  void VRTK_RotatorTrackGrabAttach__ctor_m820894771 (VRTK_RotatorTrackGrabAttach_t3062106299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_RotatorTrackGrabAttach::StopGrab(System.Boolean)
extern "C"  void VRTK_RotatorTrackGrabAttach_StopGrab_m3033711790 (VRTK_RotatorTrackGrabAttach_t3062106299 * __this, bool ___applyGrabbingObjectVelocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_RotatorTrackGrabAttach::ProcessFixedUpdate()
extern "C"  void VRTK_RotatorTrackGrabAttach_ProcessFixedUpdate_m1524451215 (VRTK_RotatorTrackGrabAttach_t3062106299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.GrabAttachMechanics.VRTK_RotatorTrackGrabAttach::SetTrackPointOrientation(UnityEngine.Transform&,UnityEngine.Transform,UnityEngine.Transform)
extern "C"  void VRTK_RotatorTrackGrabAttach_SetTrackPointOrientation_m1090324597 (VRTK_RotatorTrackGrabAttach_t3062106299 * __this, Transform_t3275118058 ** ___trackPoint0, Transform_t3275118058 * ___currentGrabbedObject1, Transform_t3275118058 * ___controllerPoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
