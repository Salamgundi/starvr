﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResizePanel
struct ResizePanel_t244327678;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void ResizePanel::.ctor()
extern "C"  void ResizePanel__ctor_m3428709759 (ResizePanel_t244327678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResizePanel::Awake()
extern "C"  void ResizePanel_Awake_m621147550 (ResizePanel_t244327678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResizePanel::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ResizePanel_OnPointerDown_m1215799603 (ResizePanel_t244327678 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResizePanel::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ResizePanel_OnDrag_m3426370394 (ResizePanel_t244327678 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
