﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Teleport
struct Teleport_t865564691;
// SteamVR_Events/Action`1<System.Single>
struct Action_1_t3534888517;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t3443095683;
// SteamVR_Events/Action`1<Valve.VR.InteractionSystem.TeleportMarkerBase>
struct Action_1_t2571085553;
// UnityEngine.Events.UnityAction`1<Valve.VR.InteractionSystem.TeleportMarkerBase>
struct UnityAction_1_t2479292719;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// Valve.VR.InteractionSystem.TeleportMarkerBase
struct TeleportMarkerBase_t1112706968;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Telep1112706968.h"

// System.Void Valve.VR.InteractionSystem.Teleport::.ctor()
extern "C"  void Teleport__ctor_m3753345699 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action`1<System.Single> Valve.VR.InteractionSystem.Teleport::ChangeSceneAction(UnityEngine.Events.UnityAction`1<System.Single>)
extern "C"  Action_1_t3534888517 * Teleport_ChangeSceneAction_m558225538 (Il2CppObject * __this /* static, unused */, UnityAction_1_t3443095683 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action`1<Valve.VR.InteractionSystem.TeleportMarkerBase> Valve.VR.InteractionSystem.Teleport::PlayerAction(UnityEngine.Events.UnityAction`1<Valve.VR.InteractionSystem.TeleportMarkerBase>)
extern "C"  Action_1_t2571085553 * Teleport_PlayerAction_m183208809 (Il2CppObject * __this /* static, unused */, UnityAction_1_t2479292719 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Events/Action`1<Valve.VR.InteractionSystem.TeleportMarkerBase> Valve.VR.InteractionSystem.Teleport::PlayerPreAction(UnityEngine.Events.UnityAction`1<Valve.VR.InteractionSystem.TeleportMarkerBase>)
extern "C"  Action_1_t2571085553 * Teleport_PlayerPreAction_m1968218916 (Il2CppObject * __this /* static, unused */, UnityAction_1_t2479292719 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.InteractionSystem.Teleport Valve.VR.InteractionSystem.Teleport::get_instance()
extern "C"  Teleport_t865564691 * Teleport_get_instance_m300815954 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::Awake()
extern "C"  void Teleport_Awake_m2552472628 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::Start()
extern "C"  void Teleport_Start_m4291554991 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::OnEnable()
extern "C"  void Teleport_OnEnable_m1086151407 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::OnDisable()
extern "C"  void Teleport_OnDisable_m2780067304 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::CheckForSpawnPoint()
extern "C"  void Teleport_CheckForSpawnPoint_m945456613 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::HideTeleportPointer()
extern "C"  void Teleport_HideTeleportPointer_m72811349 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::Update()
extern "C"  void Teleport_Update_m3351230310 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::UpdatePointer()
extern "C"  void Teleport_UpdatePointer_m3487805909 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::FixedUpdate()
extern "C"  void Teleport_FixedUpdate_m2820302000 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::OnChaperoneInfoInitialized()
extern "C"  void Teleport_OnChaperoneInfoInitialized_m3742491301 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::HidePointer()
extern "C"  void Teleport_HidePointer_m3210562810 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::ShowPointer(Valve.VR.InteractionSystem.Hand,Valve.VR.InteractionSystem.Hand)
extern "C"  void Teleport_ShowPointer_m1302308523 (Teleport_t865564691 * __this, Hand_t379716353 * ___newPointerHand0, Hand_t379716353 * ___oldPointerHand1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::UpdateTeleportColors()
extern "C"  void Teleport_UpdateTeleportColors_m2712614005 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::PlayAudioClip(UnityEngine.AudioSource,UnityEngine.AudioClip)
extern "C"  void Teleport_PlayAudioClip_m3385717276 (Teleport_t865564691 * __this, AudioSource_t1135106623 * ___source0, AudioClip_t1932558630 * ___clip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::PlayPointerHaptic(System.Boolean)
extern "C"  void Teleport_PlayPointerHaptic_m819825530 (Teleport_t865564691 * __this, bool ___validLocation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::TryTeleportPlayer()
extern "C"  void Teleport_TryTeleportPlayer_m2233295760 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::InitiateTeleportFade()
extern "C"  void Teleport_InitiateTeleportFade_m4163063579 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::TeleportPlayer()
extern "C"  void Teleport_TeleportPlayer_m1042096289 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::HighlightSelected(Valve.VR.InteractionSystem.TeleportMarkerBase)
extern "C"  void Teleport_HighlightSelected_m671266193 (Teleport_t865564691 * __this, TeleportMarkerBase_t1112706968 * ___hitTeleportMarker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::ShowTeleportHint()
extern "C"  void Teleport_ShowTeleportHint_m3676085796 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::CancelTeleportHint()
extern "C"  void Teleport_CancelTeleportHint_m2978887735 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.Teleport::TeleportHintCoroutine()
extern "C"  Il2CppObject * Teleport_TeleportHintCoroutine_m1802965261 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Teleport::IsEligibleForTeleport(Valve.VR.InteractionSystem.Hand)
extern "C"  bool Teleport_IsEligibleForTeleport_m4200584854 (Teleport_t865564691 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Teleport::ShouldOverrideHoverLock()
extern "C"  bool Teleport_ShouldOverrideHoverLock_m4193772711 (Teleport_t865564691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Teleport::WasTeleportButtonReleased(Valve.VR.InteractionSystem.Hand)
extern "C"  bool Teleport_WasTeleportButtonReleased_m3785016404 (Teleport_t865564691 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Teleport::IsTeleportButtonDown(Valve.VR.InteractionSystem.Hand)
extern "C"  bool Teleport_IsTeleportButtonDown_m1782705390 (Teleport_t865564691 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Teleport::WasTeleportButtonPressed(Valve.VR.InteractionSystem.Hand)
extern "C"  bool Teleport_WasTeleportButtonPressed_m3309880849 (Teleport_t865564691 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Valve.VR.InteractionSystem.Teleport::GetPointerStartTransform(Valve.VR.InteractionSystem.Hand)
extern "C"  Transform_t3275118058 * Teleport_GetPointerStartTransform_m2213439544 (Teleport_t865564691 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Teleport::.cctor()
extern "C"  void Teleport__cctor_m2708582078 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
