﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_RemoveApplicationManifest
struct _RemoveApplicationManifest_t1836596693;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_RemoveApplicationManifest::.ctor(System.Object,System.IntPtr)
extern "C"  void _RemoveApplicationManifest__ctor_m1459631154 (_RemoveApplicationManifest_t1836596693 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_RemoveApplicationManifest::Invoke(System.String)
extern "C"  int32_t _RemoveApplicationManifest_Invoke_m853921718 (_RemoveApplicationManifest_t1836596693 * __this, String_t* ___pchApplicationManifestFullPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_RemoveApplicationManifest::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _RemoveApplicationManifest_BeginInvoke_m2011575825 (_RemoveApplicationManifest_t1836596693 * __this, String_t* ___pchApplicationManifestFullPath0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_RemoveApplicationManifest::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _RemoveApplicationManifest_EndInvoke_m1329162928 (_RemoveApplicationManifest_t1836596693 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
