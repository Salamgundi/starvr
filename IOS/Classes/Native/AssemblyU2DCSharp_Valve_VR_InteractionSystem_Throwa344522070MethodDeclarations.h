﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Throwable/<LateDetach>c__Iterator0
struct U3CLateDetachU3Ec__Iterator0_t344522070;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.Throwable/<LateDetach>c__Iterator0::.ctor()
extern "C"  void U3CLateDetachU3Ec__Iterator0__ctor_m1339383357 (U3CLateDetachU3Ec__Iterator0_t344522070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Throwable/<LateDetach>c__Iterator0::MoveNext()
extern "C"  bool U3CLateDetachU3Ec__Iterator0_MoveNext_m2109899523 (U3CLateDetachU3Ec__Iterator0_t344522070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.Throwable/<LateDetach>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLateDetachU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1640946343 (U3CLateDetachU3Ec__Iterator0_t344522070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.Throwable/<LateDetach>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLateDetachU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3419545023 (U3CLateDetachU3Ec__Iterator0_t344522070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Throwable/<LateDetach>c__Iterator0::Dispose()
extern "C"  void U3CLateDetachU3Ec__Iterator0_Dispose_m2490685048 (U3CLateDetachU3Ec__Iterator0_t344522070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.Throwable/<LateDetach>c__Iterator0::Reset()
extern "C"  void U3CLateDetachU3Ec__Iterator0_Reset_m1866128378 (U3CLateDetachU3Ec__Iterator0_t344522070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
