﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_HeadsetControllerAware
struct VRTK_HeadsetControllerAware_t1678000416;
// VRTK.HeadsetControllerAwareEventHandler
struct HeadsetControllerAwareEventHandler_t1106036588;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetControllerAwareEvent1106036588.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetControllerAwareEvent2653531721.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void VRTK.VRTK_HeadsetControllerAware::.ctor()
extern "C"  void VRTK_HeadsetControllerAware__ctor_m631773172 (VRTK_HeadsetControllerAware_t1678000416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::add_ControllerObscured(VRTK.HeadsetControllerAwareEventHandler)
extern "C"  void VRTK_HeadsetControllerAware_add_ControllerObscured_m1120295376 (VRTK_HeadsetControllerAware_t1678000416 * __this, HeadsetControllerAwareEventHandler_t1106036588 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::remove_ControllerObscured(VRTK.HeadsetControllerAwareEventHandler)
extern "C"  void VRTK_HeadsetControllerAware_remove_ControllerObscured_m2158383225 (VRTK_HeadsetControllerAware_t1678000416 * __this, HeadsetControllerAwareEventHandler_t1106036588 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::add_ControllerUnobscured(VRTK.HeadsetControllerAwareEventHandler)
extern "C"  void VRTK_HeadsetControllerAware_add_ControllerUnobscured_m4179867621 (VRTK_HeadsetControllerAware_t1678000416 * __this, HeadsetControllerAwareEventHandler_t1106036588 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::remove_ControllerUnobscured(VRTK.HeadsetControllerAwareEventHandler)
extern "C"  void VRTK_HeadsetControllerAware_remove_ControllerUnobscured_m134197396 (VRTK_HeadsetControllerAware_t1678000416 * __this, HeadsetControllerAwareEventHandler_t1106036588 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::add_ControllerGlanceEnter(VRTK.HeadsetControllerAwareEventHandler)
extern "C"  void VRTK_HeadsetControllerAware_add_ControllerGlanceEnter_m3525306371 (VRTK_HeadsetControllerAware_t1678000416 * __this, HeadsetControllerAwareEventHandler_t1106036588 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::remove_ControllerGlanceEnter(VRTK.HeadsetControllerAwareEventHandler)
extern "C"  void VRTK_HeadsetControllerAware_remove_ControllerGlanceEnter_m3583782744 (VRTK_HeadsetControllerAware_t1678000416 * __this, HeadsetControllerAwareEventHandler_t1106036588 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::add_ControllerGlanceExit(VRTK.HeadsetControllerAwareEventHandler)
extern "C"  void VRTK_HeadsetControllerAware_add_ControllerGlanceExit_m3024023055 (VRTK_HeadsetControllerAware_t1678000416 * __this, HeadsetControllerAwareEventHandler_t1106036588 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::remove_ControllerGlanceExit(VRTK.HeadsetControllerAwareEventHandler)
extern "C"  void VRTK_HeadsetControllerAware_remove_ControllerGlanceExit_m2427140982 (VRTK_HeadsetControllerAware_t1678000416 * __this, HeadsetControllerAwareEventHandler_t1106036588 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::OnControllerObscured(VRTK.HeadsetControllerAwareEventArgs)
extern "C"  void VRTK_HeadsetControllerAware_OnControllerObscured_m1828381736 (VRTK_HeadsetControllerAware_t1678000416 * __this, HeadsetControllerAwareEventArgs_t2653531721  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::OnControllerUnobscured(VRTK.HeadsetControllerAwareEventArgs)
extern "C"  void VRTK_HeadsetControllerAware_OnControllerUnobscured_m2294225713 (VRTK_HeadsetControllerAware_t1678000416 * __this, HeadsetControllerAwareEventArgs_t2653531721  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::OnControllerGlanceEnter(VRTK.HeadsetControllerAwareEventArgs)
extern "C"  void VRTK_HeadsetControllerAware_OnControllerGlanceEnter_m2718231839 (VRTK_HeadsetControllerAware_t1678000416 * __this, HeadsetControllerAwareEventArgs_t2653531721  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::OnControllerGlanceExit(VRTK.HeadsetControllerAwareEventArgs)
extern "C"  void VRTK_HeadsetControllerAware_OnControllerGlanceExit_m89986823 (VRTK_HeadsetControllerAware_t1678000416 * __this, HeadsetControllerAwareEventArgs_t2653531721  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_HeadsetControllerAware::LeftControllerObscured()
extern "C"  bool VRTK_HeadsetControllerAware_LeftControllerObscured_m3590369280 (VRTK_HeadsetControllerAware_t1678000416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_HeadsetControllerAware::RightControllerObscured()
extern "C"  bool VRTK_HeadsetControllerAware_RightControllerObscured_m1206879895 (VRTK_HeadsetControllerAware_t1678000416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_HeadsetControllerAware::LeftControllerGlanced()
extern "C"  bool VRTK_HeadsetControllerAware_LeftControllerGlanced_m3951280373 (VRTK_HeadsetControllerAware_t1678000416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_HeadsetControllerAware::RightControllerGlanced()
extern "C"  bool VRTK_HeadsetControllerAware_RightControllerGlanced_m366688782 (VRTK_HeadsetControllerAware_t1678000416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::OnEnable()
extern "C"  void VRTK_HeadsetControllerAware_OnEnable_m3857529812 (VRTK_HeadsetControllerAware_t1678000416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::OnDisable()
extern "C"  void VRTK_HeadsetControllerAware_OnDisable_m475788513 (VRTK_HeadsetControllerAware_t1678000416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::Update()
extern "C"  void VRTK_HeadsetControllerAware_Update_m432214837 (VRTK_HeadsetControllerAware_t1678000416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.HeadsetControllerAwareEventArgs VRTK.VRTK_HeadsetControllerAware::SetHeadsetControllerAwareEvent(UnityEngine.RaycastHit,System.UInt32)
extern "C"  HeadsetControllerAwareEventArgs_t2653531721  VRTK_HeadsetControllerAware_SetHeadsetControllerAwareEvent_m567297798 (VRTK_HeadsetControllerAware_t1678000416 * __this, RaycastHit_t87180320  ___raycastHit0, uint32_t ___controllerIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::RayCastToController(UnityEngine.GameObject,UnityEngine.Transform,System.Boolean&,System.Boolean&)
extern "C"  void VRTK_HeadsetControllerAware_RayCastToController_m1151826107 (VRTK_HeadsetControllerAware_t1678000416 * __this, GameObject_t1756533147 * ___controller0, Transform_t3275118058 * ___customDestination1, bool* ___obscured2, bool* ___lastState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::ObscuredStateChanged(UnityEngine.GameObject,System.Boolean,UnityEngine.RaycastHit)
extern "C"  void VRTK_HeadsetControllerAware_ObscuredStateChanged_m864814196 (VRTK_HeadsetControllerAware_t1678000416 * __this, GameObject_t1756533147 * ___controller0, bool ___obscured1, RaycastHit_t87180320  ___hitInfo2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::CheckHeadsetView(UnityEngine.GameObject,UnityEngine.Transform,System.Boolean&,System.Boolean&)
extern "C"  void VRTK_HeadsetControllerAware_CheckHeadsetView_m2920891900 (VRTK_HeadsetControllerAware_t1678000416 * __this, GameObject_t1756533147 * ___controller0, Transform_t3275118058 * ___customDestination1, bool* ___controllerGlance2, bool* ___controllerGlanceLastState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetControllerAware::GlanceStateChanged(UnityEngine.GameObject,System.Boolean)
extern "C"  void VRTK_HeadsetControllerAware_GlanceStateChanged_m3278502426 (VRTK_HeadsetControllerAware_t1678000416 * __this, GameObject_t1756533147 * ___controller0, bool ___glance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
