﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayAutoCurveDistanceRangeInMeters
struct _GetOverlayAutoCurveDistanceRangeInMeters_t4193138162;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayAutoCurveDistanceRangeInMeters::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayAutoCurveDistanceRangeInMeters__ctor_m2647896203 (_GetOverlayAutoCurveDistanceRangeInMeters_t4193138162 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayAutoCurveDistanceRangeInMeters::Invoke(System.UInt64,System.Single&,System.Single&)
extern "C"  int32_t _GetOverlayAutoCurveDistanceRangeInMeters_Invoke_m739910632 (_GetOverlayAutoCurveDistanceRangeInMeters_t4193138162 * __this, uint64_t ___ulOverlayHandle0, float* ___pfMinDistanceInMeters1, float* ___pfMaxDistanceInMeters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayAutoCurveDistanceRangeInMeters::BeginInvoke(System.UInt64,System.Single&,System.Single&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayAutoCurveDistanceRangeInMeters_BeginInvoke_m1416780803 (_GetOverlayAutoCurveDistanceRangeInMeters_t4193138162 * __this, uint64_t ___ulOverlayHandle0, float* ___pfMinDistanceInMeters1, float* ___pfMaxDistanceInMeters2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayAutoCurveDistanceRangeInMeters::EndInvoke(System.Single&,System.Single&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayAutoCurveDistanceRangeInMeters_EndInvoke_m1370619485 (_GetOverlayAutoCurveDistanceRangeInMeters_t4193138162 * __this, float* ___pfMinDistanceInMeters0, float* ___pfMaxDistanceInMeters1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
