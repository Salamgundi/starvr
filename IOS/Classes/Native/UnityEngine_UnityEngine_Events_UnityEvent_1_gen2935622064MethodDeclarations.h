﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2727799310MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.TrackedDevicePose_t[]>::.ctor()
#define UnityEvent_1__ctor_m80577820(__this, method) ((  void (*) (UnityEvent_1_t2935622064 *, const MethodInfo*))UnityEvent_1__ctor_m2073978020_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.TrackedDevicePose_t[]>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_AddListener_m2155259369(__this, ___call0, method) ((  void (*) (UnityEvent_1_t2935622064 *, UnityAction_1_t4263857800 *, const MethodInfo*))UnityEvent_1_AddListener_m22503421_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.TrackedDevicePose_t[]>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_RemoveListener_m532370504(__this, ___call0, method) ((  void (*) (UnityEvent_1_t2935622064 *, UnityAction_1_t4263857800 *, const MethodInfo*))UnityEvent_1_RemoveListener_m4278264272_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<Valve.VR.TrackedDevicePose_t[]>::FindMethod_Impl(System.String,System.Object)
#define UnityEvent_1_FindMethod_Impl_m2496051879(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_1_t2935622064 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m2223850067_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<Valve.VR.TrackedDevicePose_t[]>::GetDelegate(System.Object,System.Reflection.MethodInfo)
#define UnityEvent_1_GetDelegate_m2538774655(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_1_t2935622064 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m669290055_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<Valve.VR.TrackedDevicePose_t[]>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_GetDelegate_m587490770(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t4263857800 *, const MethodInfo*))UnityEvent_1_GetDelegate_m3098147632_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.TrackedDevicePose_t[]>::Invoke(T0)
#define UnityEvent_1_Invoke_m2274215126(__this, ___arg00, method) ((  void (*) (UnityEvent_1_t2935622064 *, TrackedDevicePose_tU5BU5D_t2897272049*, const MethodInfo*))UnityEvent_1_Invoke_m838874366_gshared)(__this, ___arg00, method)
