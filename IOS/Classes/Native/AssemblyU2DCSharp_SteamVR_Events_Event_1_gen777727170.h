﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2935622064.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SteamVR_Events/Event`1<Valve.VR.TrackedDevicePose_t[]>
struct  Event_1_t777727170  : public UnityEvent_1_t2935622064
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
