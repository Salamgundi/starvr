﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.Interactable
struct Interactable_t1274046986;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.Hand/<UpdateHovering>c__AnonStorey2
struct  U3CUpdateHoveringU3Ec__AnonStorey2_t2275351047  : public Il2CppObject
{
public:
	// Valve.VR.InteractionSystem.Interactable Valve.VR.InteractionSystem.Hand/<UpdateHovering>c__AnonStorey2::contacting
	Interactable_t1274046986 * ___contacting_0;

public:
	inline static int32_t get_offset_of_contacting_0() { return static_cast<int32_t>(offsetof(U3CUpdateHoveringU3Ec__AnonStorey2_t2275351047, ___contacting_0)); }
	inline Interactable_t1274046986 * get_contacting_0() const { return ___contacting_0; }
	inline Interactable_t1274046986 ** get_address_of_contacting_0() { return &___contacting_0; }
	inline void set_contacting_0(Interactable_t1274046986 * value)
	{
		___contacting_0 = value;
		Il2CppCodeGenWriteBarrier(&___contacting_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
