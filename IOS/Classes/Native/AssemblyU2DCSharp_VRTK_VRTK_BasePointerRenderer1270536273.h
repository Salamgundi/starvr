﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_PlayAreaCursor
struct VRTK_PlayAreaCursor_t3566057915;
// VRTK.VRTK_BasePointerRenderer/PointerOriginSmoothingSettings
struct PointerOriginSmoothingSettings_t2399923839;
// VRTK.VRTK_Pointer
struct VRTK_Pointer_t2647108841;
// UnityEngine.Material
struct Material_t193706927;
// VRTK.VRTK_PolicyList
struct VRTK_PolicyList_t2965133344;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_TransformFollow
struct VRTK_TransformFollow_t3532748285;
// VRTK.VRTK_InteractGrab
struct VRTK_InteractGrab_t124353446;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BasePointerRenderer_Vis517960985.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_BasePointerRenderer
struct  VRTK_BasePointerRenderer_t1270536273  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_PlayAreaCursor VRTK.VRTK_BasePointerRenderer::playareaCursor
	VRTK_PlayAreaCursor_t3566057915 * ___playareaCursor_2;
	// UnityEngine.LayerMask VRTK.VRTK_BasePointerRenderer::layersToIgnore
	LayerMask_t3188175821  ___layersToIgnore_3;
	// VRTK.VRTK_BasePointerRenderer/PointerOriginSmoothingSettings VRTK.VRTK_BasePointerRenderer::pointerOriginSmoothingSettings
	PointerOriginSmoothingSettings_t2399923839 * ___pointerOriginSmoothingSettings_4;
	// UnityEngine.Color VRTK.VRTK_BasePointerRenderer::validCollisionColor
	Color_t2020392075  ___validCollisionColor_5;
	// UnityEngine.Color VRTK.VRTK_BasePointerRenderer::invalidCollisionColor
	Color_t2020392075  ___invalidCollisionColor_6;
	// VRTK.VRTK_BasePointerRenderer/VisibilityStates VRTK.VRTK_BasePointerRenderer::tracerVisibility
	int32_t ___tracerVisibility_7;
	// VRTK.VRTK_BasePointerRenderer/VisibilityStates VRTK.VRTK_BasePointerRenderer::cursorVisibility
	int32_t ___cursorVisibility_8;
	// VRTK.VRTK_Pointer VRTK.VRTK_BasePointerRenderer::controllingPointer
	VRTK_Pointer_t2647108841 * ___controllingPointer_10;
	// UnityEngine.RaycastHit VRTK.VRTK_BasePointerRenderer::destinationHit
	RaycastHit_t87180320  ___destinationHit_11;
	// UnityEngine.Material VRTK.VRTK_BasePointerRenderer::defaultMaterial
	Material_t193706927 * ___defaultMaterial_12;
	// UnityEngine.Color VRTK.VRTK_BasePointerRenderer::currentColor
	Color_t2020392075  ___currentColor_13;
	// VRTK.VRTK_PolicyList VRTK.VRTK_BasePointerRenderer::invalidListPolicy
	VRTK_PolicyList_t2965133344 * ___invalidListPolicy_14;
	// System.Single VRTK.VRTK_BasePointerRenderer::navMeshCheckDistance
	float ___navMeshCheckDistance_15;
	// System.Boolean VRTK.VRTK_BasePointerRenderer::headsetPositionCompensation
	bool ___headsetPositionCompensation_16;
	// UnityEngine.GameObject VRTK.VRTK_BasePointerRenderer::objectInteractor
	GameObject_t1756533147 * ___objectInteractor_17;
	// UnityEngine.GameObject VRTK.VRTK_BasePointerRenderer::objectInteractorAttachPoint
	GameObject_t1756533147 * ___objectInteractorAttachPoint_18;
	// UnityEngine.GameObject VRTK.VRTK_BasePointerRenderer::pointerOriginTransformFollowGameObject
	GameObject_t1756533147 * ___pointerOriginTransformFollowGameObject_19;
	// VRTK.VRTK_TransformFollow VRTK.VRTK_BasePointerRenderer::pointerOriginTransformFollow
	VRTK_TransformFollow_t3532748285 * ___pointerOriginTransformFollow_20;
	// VRTK.VRTK_InteractGrab VRTK.VRTK_BasePointerRenderer::controllerGrabScript
	VRTK_InteractGrab_t124353446 * ___controllerGrabScript_21;
	// UnityEngine.Rigidbody VRTK.VRTK_BasePointerRenderer::savedAttachPoint
	Rigidbody_t4233889191 * ___savedAttachPoint_22;
	// System.Boolean VRTK.VRTK_BasePointerRenderer::attachedToInteractorAttachPoint
	bool ___attachedToInteractorAttachPoint_23;
	// System.Single VRTK.VRTK_BasePointerRenderer::savedBeamLength
	float ___savedBeamLength_24;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> VRTK.VRTK_BasePointerRenderer::makeRendererVisible
	List_1_t1125654279 * ___makeRendererVisible_25;
	// System.Boolean VRTK.VRTK_BasePointerRenderer::tracerVisible
	bool ___tracerVisible_26;
	// System.Boolean VRTK.VRTK_BasePointerRenderer::cursorVisible
	bool ___cursorVisible_27;

public:
	inline static int32_t get_offset_of_playareaCursor_2() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___playareaCursor_2)); }
	inline VRTK_PlayAreaCursor_t3566057915 * get_playareaCursor_2() const { return ___playareaCursor_2; }
	inline VRTK_PlayAreaCursor_t3566057915 ** get_address_of_playareaCursor_2() { return &___playareaCursor_2; }
	inline void set_playareaCursor_2(VRTK_PlayAreaCursor_t3566057915 * value)
	{
		___playareaCursor_2 = value;
		Il2CppCodeGenWriteBarrier(&___playareaCursor_2, value);
	}

	inline static int32_t get_offset_of_layersToIgnore_3() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___layersToIgnore_3)); }
	inline LayerMask_t3188175821  get_layersToIgnore_3() const { return ___layersToIgnore_3; }
	inline LayerMask_t3188175821 * get_address_of_layersToIgnore_3() { return &___layersToIgnore_3; }
	inline void set_layersToIgnore_3(LayerMask_t3188175821  value)
	{
		___layersToIgnore_3 = value;
	}

	inline static int32_t get_offset_of_pointerOriginSmoothingSettings_4() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___pointerOriginSmoothingSettings_4)); }
	inline PointerOriginSmoothingSettings_t2399923839 * get_pointerOriginSmoothingSettings_4() const { return ___pointerOriginSmoothingSettings_4; }
	inline PointerOriginSmoothingSettings_t2399923839 ** get_address_of_pointerOriginSmoothingSettings_4() { return &___pointerOriginSmoothingSettings_4; }
	inline void set_pointerOriginSmoothingSettings_4(PointerOriginSmoothingSettings_t2399923839 * value)
	{
		___pointerOriginSmoothingSettings_4 = value;
		Il2CppCodeGenWriteBarrier(&___pointerOriginSmoothingSettings_4, value);
	}

	inline static int32_t get_offset_of_validCollisionColor_5() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___validCollisionColor_5)); }
	inline Color_t2020392075  get_validCollisionColor_5() const { return ___validCollisionColor_5; }
	inline Color_t2020392075 * get_address_of_validCollisionColor_5() { return &___validCollisionColor_5; }
	inline void set_validCollisionColor_5(Color_t2020392075  value)
	{
		___validCollisionColor_5 = value;
	}

	inline static int32_t get_offset_of_invalidCollisionColor_6() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___invalidCollisionColor_6)); }
	inline Color_t2020392075  get_invalidCollisionColor_6() const { return ___invalidCollisionColor_6; }
	inline Color_t2020392075 * get_address_of_invalidCollisionColor_6() { return &___invalidCollisionColor_6; }
	inline void set_invalidCollisionColor_6(Color_t2020392075  value)
	{
		___invalidCollisionColor_6 = value;
	}

	inline static int32_t get_offset_of_tracerVisibility_7() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___tracerVisibility_7)); }
	inline int32_t get_tracerVisibility_7() const { return ___tracerVisibility_7; }
	inline int32_t* get_address_of_tracerVisibility_7() { return &___tracerVisibility_7; }
	inline void set_tracerVisibility_7(int32_t value)
	{
		___tracerVisibility_7 = value;
	}

	inline static int32_t get_offset_of_cursorVisibility_8() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___cursorVisibility_8)); }
	inline int32_t get_cursorVisibility_8() const { return ___cursorVisibility_8; }
	inline int32_t* get_address_of_cursorVisibility_8() { return &___cursorVisibility_8; }
	inline void set_cursorVisibility_8(int32_t value)
	{
		___cursorVisibility_8 = value;
	}

	inline static int32_t get_offset_of_controllingPointer_10() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___controllingPointer_10)); }
	inline VRTK_Pointer_t2647108841 * get_controllingPointer_10() const { return ___controllingPointer_10; }
	inline VRTK_Pointer_t2647108841 ** get_address_of_controllingPointer_10() { return &___controllingPointer_10; }
	inline void set_controllingPointer_10(VRTK_Pointer_t2647108841 * value)
	{
		___controllingPointer_10 = value;
		Il2CppCodeGenWriteBarrier(&___controllingPointer_10, value);
	}

	inline static int32_t get_offset_of_destinationHit_11() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___destinationHit_11)); }
	inline RaycastHit_t87180320  get_destinationHit_11() const { return ___destinationHit_11; }
	inline RaycastHit_t87180320 * get_address_of_destinationHit_11() { return &___destinationHit_11; }
	inline void set_destinationHit_11(RaycastHit_t87180320  value)
	{
		___destinationHit_11 = value;
	}

	inline static int32_t get_offset_of_defaultMaterial_12() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___defaultMaterial_12)); }
	inline Material_t193706927 * get_defaultMaterial_12() const { return ___defaultMaterial_12; }
	inline Material_t193706927 ** get_address_of_defaultMaterial_12() { return &___defaultMaterial_12; }
	inline void set_defaultMaterial_12(Material_t193706927 * value)
	{
		___defaultMaterial_12 = value;
		Il2CppCodeGenWriteBarrier(&___defaultMaterial_12, value);
	}

	inline static int32_t get_offset_of_currentColor_13() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___currentColor_13)); }
	inline Color_t2020392075  get_currentColor_13() const { return ___currentColor_13; }
	inline Color_t2020392075 * get_address_of_currentColor_13() { return &___currentColor_13; }
	inline void set_currentColor_13(Color_t2020392075  value)
	{
		___currentColor_13 = value;
	}

	inline static int32_t get_offset_of_invalidListPolicy_14() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___invalidListPolicy_14)); }
	inline VRTK_PolicyList_t2965133344 * get_invalidListPolicy_14() const { return ___invalidListPolicy_14; }
	inline VRTK_PolicyList_t2965133344 ** get_address_of_invalidListPolicy_14() { return &___invalidListPolicy_14; }
	inline void set_invalidListPolicy_14(VRTK_PolicyList_t2965133344 * value)
	{
		___invalidListPolicy_14 = value;
		Il2CppCodeGenWriteBarrier(&___invalidListPolicy_14, value);
	}

	inline static int32_t get_offset_of_navMeshCheckDistance_15() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___navMeshCheckDistance_15)); }
	inline float get_navMeshCheckDistance_15() const { return ___navMeshCheckDistance_15; }
	inline float* get_address_of_navMeshCheckDistance_15() { return &___navMeshCheckDistance_15; }
	inline void set_navMeshCheckDistance_15(float value)
	{
		___navMeshCheckDistance_15 = value;
	}

	inline static int32_t get_offset_of_headsetPositionCompensation_16() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___headsetPositionCompensation_16)); }
	inline bool get_headsetPositionCompensation_16() const { return ___headsetPositionCompensation_16; }
	inline bool* get_address_of_headsetPositionCompensation_16() { return &___headsetPositionCompensation_16; }
	inline void set_headsetPositionCompensation_16(bool value)
	{
		___headsetPositionCompensation_16 = value;
	}

	inline static int32_t get_offset_of_objectInteractor_17() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___objectInteractor_17)); }
	inline GameObject_t1756533147 * get_objectInteractor_17() const { return ___objectInteractor_17; }
	inline GameObject_t1756533147 ** get_address_of_objectInteractor_17() { return &___objectInteractor_17; }
	inline void set_objectInteractor_17(GameObject_t1756533147 * value)
	{
		___objectInteractor_17 = value;
		Il2CppCodeGenWriteBarrier(&___objectInteractor_17, value);
	}

	inline static int32_t get_offset_of_objectInteractorAttachPoint_18() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___objectInteractorAttachPoint_18)); }
	inline GameObject_t1756533147 * get_objectInteractorAttachPoint_18() const { return ___objectInteractorAttachPoint_18; }
	inline GameObject_t1756533147 ** get_address_of_objectInteractorAttachPoint_18() { return &___objectInteractorAttachPoint_18; }
	inline void set_objectInteractorAttachPoint_18(GameObject_t1756533147 * value)
	{
		___objectInteractorAttachPoint_18 = value;
		Il2CppCodeGenWriteBarrier(&___objectInteractorAttachPoint_18, value);
	}

	inline static int32_t get_offset_of_pointerOriginTransformFollowGameObject_19() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___pointerOriginTransformFollowGameObject_19)); }
	inline GameObject_t1756533147 * get_pointerOriginTransformFollowGameObject_19() const { return ___pointerOriginTransformFollowGameObject_19; }
	inline GameObject_t1756533147 ** get_address_of_pointerOriginTransformFollowGameObject_19() { return &___pointerOriginTransformFollowGameObject_19; }
	inline void set_pointerOriginTransformFollowGameObject_19(GameObject_t1756533147 * value)
	{
		___pointerOriginTransformFollowGameObject_19 = value;
		Il2CppCodeGenWriteBarrier(&___pointerOriginTransformFollowGameObject_19, value);
	}

	inline static int32_t get_offset_of_pointerOriginTransformFollow_20() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___pointerOriginTransformFollow_20)); }
	inline VRTK_TransformFollow_t3532748285 * get_pointerOriginTransformFollow_20() const { return ___pointerOriginTransformFollow_20; }
	inline VRTK_TransformFollow_t3532748285 ** get_address_of_pointerOriginTransformFollow_20() { return &___pointerOriginTransformFollow_20; }
	inline void set_pointerOriginTransformFollow_20(VRTK_TransformFollow_t3532748285 * value)
	{
		___pointerOriginTransformFollow_20 = value;
		Il2CppCodeGenWriteBarrier(&___pointerOriginTransformFollow_20, value);
	}

	inline static int32_t get_offset_of_controllerGrabScript_21() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___controllerGrabScript_21)); }
	inline VRTK_InteractGrab_t124353446 * get_controllerGrabScript_21() const { return ___controllerGrabScript_21; }
	inline VRTK_InteractGrab_t124353446 ** get_address_of_controllerGrabScript_21() { return &___controllerGrabScript_21; }
	inline void set_controllerGrabScript_21(VRTK_InteractGrab_t124353446 * value)
	{
		___controllerGrabScript_21 = value;
		Il2CppCodeGenWriteBarrier(&___controllerGrabScript_21, value);
	}

	inline static int32_t get_offset_of_savedAttachPoint_22() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___savedAttachPoint_22)); }
	inline Rigidbody_t4233889191 * get_savedAttachPoint_22() const { return ___savedAttachPoint_22; }
	inline Rigidbody_t4233889191 ** get_address_of_savedAttachPoint_22() { return &___savedAttachPoint_22; }
	inline void set_savedAttachPoint_22(Rigidbody_t4233889191 * value)
	{
		___savedAttachPoint_22 = value;
		Il2CppCodeGenWriteBarrier(&___savedAttachPoint_22, value);
	}

	inline static int32_t get_offset_of_attachedToInteractorAttachPoint_23() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___attachedToInteractorAttachPoint_23)); }
	inline bool get_attachedToInteractorAttachPoint_23() const { return ___attachedToInteractorAttachPoint_23; }
	inline bool* get_address_of_attachedToInteractorAttachPoint_23() { return &___attachedToInteractorAttachPoint_23; }
	inline void set_attachedToInteractorAttachPoint_23(bool value)
	{
		___attachedToInteractorAttachPoint_23 = value;
	}

	inline static int32_t get_offset_of_savedBeamLength_24() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___savedBeamLength_24)); }
	inline float get_savedBeamLength_24() const { return ___savedBeamLength_24; }
	inline float* get_address_of_savedBeamLength_24() { return &___savedBeamLength_24; }
	inline void set_savedBeamLength_24(float value)
	{
		___savedBeamLength_24 = value;
	}

	inline static int32_t get_offset_of_makeRendererVisible_25() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___makeRendererVisible_25)); }
	inline List_1_t1125654279 * get_makeRendererVisible_25() const { return ___makeRendererVisible_25; }
	inline List_1_t1125654279 ** get_address_of_makeRendererVisible_25() { return &___makeRendererVisible_25; }
	inline void set_makeRendererVisible_25(List_1_t1125654279 * value)
	{
		___makeRendererVisible_25 = value;
		Il2CppCodeGenWriteBarrier(&___makeRendererVisible_25, value);
	}

	inline static int32_t get_offset_of_tracerVisible_26() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___tracerVisible_26)); }
	inline bool get_tracerVisible_26() const { return ___tracerVisible_26; }
	inline bool* get_address_of_tracerVisible_26() { return &___tracerVisible_26; }
	inline void set_tracerVisible_26(bool value)
	{
		___tracerVisible_26 = value;
	}

	inline static int32_t get_offset_of_cursorVisible_27() { return static_cast<int32_t>(offsetof(VRTK_BasePointerRenderer_t1270536273, ___cursorVisible_27)); }
	inline bool get_cursorVisible_27() const { return ___cursorVisible_27; }
	inline bool* get_address_of_cursorVisible_27() { return &___cursorVisible_27; }
	inline void set_cursorVisible_27(bool value)
	{
		___cursorVisible_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
