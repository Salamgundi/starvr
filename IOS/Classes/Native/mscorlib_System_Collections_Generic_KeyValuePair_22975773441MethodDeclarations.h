﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.MaterialPropertyBlock>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2151304165(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2975773441 *, String_t*, MaterialPropertyBlock_t3303648957 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.MaterialPropertyBlock>::get_Key()
#define KeyValuePair_2_get_Key_m3471079071(__this, method) ((  String_t* (*) (KeyValuePair_2_t2975773441 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.MaterialPropertyBlock>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m794022080(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2975773441 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.MaterialPropertyBlock>::get_Value()
#define KeyValuePair_2_get_Value_m57433663(__this, method) ((  MaterialPropertyBlock_t3303648957 * (*) (KeyValuePair_2_t2975773441 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.MaterialPropertyBlock>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1232115840(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2975773441 *, MaterialPropertyBlock_t3303648957 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.MaterialPropertyBlock>::ToString()
#define KeyValuePair_2_ToString_m2501329472(__this, method) ((  String_t* (*) (KeyValuePair_2_t2975773441 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
