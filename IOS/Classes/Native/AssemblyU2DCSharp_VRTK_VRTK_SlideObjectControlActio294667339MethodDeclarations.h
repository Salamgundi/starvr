﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_SlideObjectControlAction
struct VRTK_SlideObjectControlAction_t294667339;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_SlideObjectControlAction::.ctor()
extern "C"  void VRTK_SlideObjectControlAction__ctor_m3098511533 (VRTK_SlideObjectControlAction_t294667339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SlideObjectControlAction::Process(UnityEngine.GameObject,UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Single,System.Boolean,System.Boolean)
extern "C"  void VRTK_SlideObjectControlAction_Process_m3292365530 (VRTK_SlideObjectControlAction_t294667339 * __this, GameObject_t1756533147 * ___controlledGameObject0, Transform_t3275118058 * ___directionDevice1, Vector3_t2243707580  ___axisDirection2, float ___axis3, float ___deadzone4, bool ___currentlyFalling5, bool ___modifierActive6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_SlideObjectControlAction::CalculateSpeed(System.Single,System.Boolean,System.Boolean)
extern "C"  float VRTK_SlideObjectControlAction_CalculateSpeed_m3631864057 (VRTK_SlideObjectControlAction_t294667339 * __this, float ___inputValue0, bool ___currentlyFalling1, bool ___modifierActive2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_SlideObjectControlAction::Decelerate(System.Single,System.Boolean)
extern "C"  float VRTK_SlideObjectControlAction_Decelerate_m2150010313 (VRTK_SlideObjectControlAction_t294667339 * __this, float ___speed0, bool ___currentlyFalling1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_SlideObjectControlAction::Move(UnityEngine.GameObject,UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  void VRTK_SlideObjectControlAction_Move_m1738812896 (VRTK_SlideObjectControlAction_t294667339 * __this, GameObject_t1756533147 * ___controlledGameObject0, Transform_t3275118058 * ___directionDevice1, Vector3_t2243707580  ___axisDirection2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
