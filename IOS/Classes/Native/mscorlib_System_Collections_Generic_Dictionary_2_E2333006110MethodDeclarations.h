﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>
struct Dictionary_2_t1012981408;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2333006110.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23065293926.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2344959086_gshared (Enumerator_t2333006110 * __this, Dictionary_2_t1012981408 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2344959086(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2333006110 *, Dictionary_2_t1012981408 *, const MethodInfo*))Enumerator__ctor_m2344959086_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1684541727_gshared (Enumerator_t2333006110 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1684541727(__this, method) ((  Il2CppObject * (*) (Enumerator_t2333006110 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1684541727_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2701779915_gshared (Enumerator_t2333006110 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2701779915(__this, method) ((  void (*) (Enumerator_t2333006110 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2701779915_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3813164584_gshared (Enumerator_t2333006110 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3813164584(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2333006110 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3813164584_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1359871929_gshared (Enumerator_t2333006110 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1359871929(__this, method) ((  Il2CppObject * (*) (Enumerator_t2333006110 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1359871929_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1595199785_gshared (Enumerator_t2333006110 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1595199785(__this, method) ((  Il2CppObject * (*) (Enumerator_t2333006110 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1595199785_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m356799_gshared (Enumerator_t2333006110 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m356799(__this, method) ((  bool (*) (Enumerator_t2333006110 *, const MethodInfo*))Enumerator_MoveNext_m356799_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::get_Current()
extern "C"  KeyValuePair_2_t3065293926  Enumerator_get_Current_m156217879_gshared (Enumerator_t2333006110 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m156217879(__this, method) ((  KeyValuePair_2_t3065293926  (*) (Enumerator_t2333006110 *, const MethodInfo*))Enumerator_get_Current_m156217879_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3388063838_gshared (Enumerator_t2333006110 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3388063838(__this, method) ((  int32_t (*) (Enumerator_t2333006110 *, const MethodInfo*))Enumerator_get_CurrentKey_m3388063838_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::get_CurrentValue()
extern "C"  Color_t2020392075  Enumerator_get_CurrentValue_m2876718750_gshared (Enumerator_t2333006110 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2876718750(__this, method) ((  Color_t2020392075  (*) (Enumerator_t2333006110 *, const MethodInfo*))Enumerator_get_CurrentValue_m2876718750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::Reset()
extern "C"  void Enumerator_Reset_m1460843828_gshared (Enumerator_t2333006110 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1460843828(__this, method) ((  void (*) (Enumerator_t2333006110 *, const MethodInfo*))Enumerator_Reset_m1460843828_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3733163221_gshared (Enumerator_t2333006110 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3733163221(__this, method) ((  void (*) (Enumerator_t2333006110 *, const MethodInfo*))Enumerator_VerifyState_m3733163221_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m265390739_gshared (Enumerator_t2333006110 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m265390739(__this, method) ((  void (*) (Enumerator_t2333006110 *, const MethodInfo*))Enumerator_VerifyCurrent_m265390739_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::Dispose()
extern "C"  void Enumerator_Dispose_m1552944986_gshared (Enumerator_t2333006110 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1552944986(__this, method) ((  void (*) (Enumerator_t2333006110 *, const MethodInfo*))Enumerator_Dispose_m1552944986_gshared)(__this, method)
