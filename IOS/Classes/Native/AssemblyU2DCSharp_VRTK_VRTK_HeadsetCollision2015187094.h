﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_PolicyList
struct VRTK_PolicyList_t2965133344;
// VRTK.HeadsetCollisionEventHandler
struct HeadsetCollisionEventHandler_t3119610762;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.VRTK_HeadsetCollider
struct VRTK_HeadsetCollider_t4272516266;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_HeadsetCollision
struct  VRTK_HeadsetCollision_t2015187094  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VRTK.VRTK_HeadsetCollision::colliderRadius
	float ___colliderRadius_2;
	// VRTK.VRTK_PolicyList VRTK.VRTK_HeadsetCollision::targetListPolicy
	VRTK_PolicyList_t2965133344 * ___targetListPolicy_3;
	// VRTK.HeadsetCollisionEventHandler VRTK.VRTK_HeadsetCollision::HeadsetCollisionDetect
	HeadsetCollisionEventHandler_t3119610762 * ___HeadsetCollisionDetect_4;
	// VRTK.HeadsetCollisionEventHandler VRTK.VRTK_HeadsetCollision::HeadsetCollisionEnded
	HeadsetCollisionEventHandler_t3119610762 * ___HeadsetCollisionEnded_5;
	// System.Boolean VRTK.VRTK_HeadsetCollision::headsetColliding
	bool ___headsetColliding_6;
	// UnityEngine.Collider VRTK.VRTK_HeadsetCollision::collidingWith
	Collider_t3497673348 * ___collidingWith_7;
	// UnityEngine.Transform VRTK.VRTK_HeadsetCollision::headset
	Transform_t3275118058 * ___headset_8;
	// VRTK.VRTK_HeadsetCollider VRTK.VRTK_HeadsetCollision::headsetColliderScript
	VRTK_HeadsetCollider_t4272516266 * ___headsetColliderScript_9;
	// UnityEngine.GameObject VRTK.VRTK_HeadsetCollision::headsetColliderContainer
	GameObject_t1756533147 * ___headsetColliderContainer_10;
	// System.Boolean VRTK.VRTK_HeadsetCollision::generateCollider
	bool ___generateCollider_11;
	// System.Boolean VRTK.VRTK_HeadsetCollision::generateRigidbody
	bool ___generateRigidbody_12;

public:
	inline static int32_t get_offset_of_colliderRadius_2() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollision_t2015187094, ___colliderRadius_2)); }
	inline float get_colliderRadius_2() const { return ___colliderRadius_2; }
	inline float* get_address_of_colliderRadius_2() { return &___colliderRadius_2; }
	inline void set_colliderRadius_2(float value)
	{
		___colliderRadius_2 = value;
	}

	inline static int32_t get_offset_of_targetListPolicy_3() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollision_t2015187094, ___targetListPolicy_3)); }
	inline VRTK_PolicyList_t2965133344 * get_targetListPolicy_3() const { return ___targetListPolicy_3; }
	inline VRTK_PolicyList_t2965133344 ** get_address_of_targetListPolicy_3() { return &___targetListPolicy_3; }
	inline void set_targetListPolicy_3(VRTK_PolicyList_t2965133344 * value)
	{
		___targetListPolicy_3 = value;
		Il2CppCodeGenWriteBarrier(&___targetListPolicy_3, value);
	}

	inline static int32_t get_offset_of_HeadsetCollisionDetect_4() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollision_t2015187094, ___HeadsetCollisionDetect_4)); }
	inline HeadsetCollisionEventHandler_t3119610762 * get_HeadsetCollisionDetect_4() const { return ___HeadsetCollisionDetect_4; }
	inline HeadsetCollisionEventHandler_t3119610762 ** get_address_of_HeadsetCollisionDetect_4() { return &___HeadsetCollisionDetect_4; }
	inline void set_HeadsetCollisionDetect_4(HeadsetCollisionEventHandler_t3119610762 * value)
	{
		___HeadsetCollisionDetect_4 = value;
		Il2CppCodeGenWriteBarrier(&___HeadsetCollisionDetect_4, value);
	}

	inline static int32_t get_offset_of_HeadsetCollisionEnded_5() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollision_t2015187094, ___HeadsetCollisionEnded_5)); }
	inline HeadsetCollisionEventHandler_t3119610762 * get_HeadsetCollisionEnded_5() const { return ___HeadsetCollisionEnded_5; }
	inline HeadsetCollisionEventHandler_t3119610762 ** get_address_of_HeadsetCollisionEnded_5() { return &___HeadsetCollisionEnded_5; }
	inline void set_HeadsetCollisionEnded_5(HeadsetCollisionEventHandler_t3119610762 * value)
	{
		___HeadsetCollisionEnded_5 = value;
		Il2CppCodeGenWriteBarrier(&___HeadsetCollisionEnded_5, value);
	}

	inline static int32_t get_offset_of_headsetColliding_6() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollision_t2015187094, ___headsetColliding_6)); }
	inline bool get_headsetColliding_6() const { return ___headsetColliding_6; }
	inline bool* get_address_of_headsetColliding_6() { return &___headsetColliding_6; }
	inline void set_headsetColliding_6(bool value)
	{
		___headsetColliding_6 = value;
	}

	inline static int32_t get_offset_of_collidingWith_7() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollision_t2015187094, ___collidingWith_7)); }
	inline Collider_t3497673348 * get_collidingWith_7() const { return ___collidingWith_7; }
	inline Collider_t3497673348 ** get_address_of_collidingWith_7() { return &___collidingWith_7; }
	inline void set_collidingWith_7(Collider_t3497673348 * value)
	{
		___collidingWith_7 = value;
		Il2CppCodeGenWriteBarrier(&___collidingWith_7, value);
	}

	inline static int32_t get_offset_of_headset_8() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollision_t2015187094, ___headset_8)); }
	inline Transform_t3275118058 * get_headset_8() const { return ___headset_8; }
	inline Transform_t3275118058 ** get_address_of_headset_8() { return &___headset_8; }
	inline void set_headset_8(Transform_t3275118058 * value)
	{
		___headset_8 = value;
		Il2CppCodeGenWriteBarrier(&___headset_8, value);
	}

	inline static int32_t get_offset_of_headsetColliderScript_9() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollision_t2015187094, ___headsetColliderScript_9)); }
	inline VRTK_HeadsetCollider_t4272516266 * get_headsetColliderScript_9() const { return ___headsetColliderScript_9; }
	inline VRTK_HeadsetCollider_t4272516266 ** get_address_of_headsetColliderScript_9() { return &___headsetColliderScript_9; }
	inline void set_headsetColliderScript_9(VRTK_HeadsetCollider_t4272516266 * value)
	{
		___headsetColliderScript_9 = value;
		Il2CppCodeGenWriteBarrier(&___headsetColliderScript_9, value);
	}

	inline static int32_t get_offset_of_headsetColliderContainer_10() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollision_t2015187094, ___headsetColliderContainer_10)); }
	inline GameObject_t1756533147 * get_headsetColliderContainer_10() const { return ___headsetColliderContainer_10; }
	inline GameObject_t1756533147 ** get_address_of_headsetColliderContainer_10() { return &___headsetColliderContainer_10; }
	inline void set_headsetColliderContainer_10(GameObject_t1756533147 * value)
	{
		___headsetColliderContainer_10 = value;
		Il2CppCodeGenWriteBarrier(&___headsetColliderContainer_10, value);
	}

	inline static int32_t get_offset_of_generateCollider_11() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollision_t2015187094, ___generateCollider_11)); }
	inline bool get_generateCollider_11() const { return ___generateCollider_11; }
	inline bool* get_address_of_generateCollider_11() { return &___generateCollider_11; }
	inline void set_generateCollider_11(bool value)
	{
		___generateCollider_11 = value;
	}

	inline static int32_t get_offset_of_generateRigidbody_12() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollision_t2015187094, ___generateRigidbody_12)); }
	inline bool get_generateRigidbody_12() const { return ___generateRigidbody_12; }
	inline bool* get_address_of_generateRigidbody_12() { return &___generateRigidbody_12; }
	inline void set_generateRigidbody_12(bool value)
	{
		___generateRigidbody_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
