﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.VRTK_ControllerEvents_ListenerExample
struct VRTK_ControllerEvents_ListenerExample_t190763384;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::.ctor()
extern "C"  void VRTK_ControllerEvents_ListenerExample__ctor_m3758773813 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::Start()
extern "C"  void VRTK_ControllerEvents_ListenerExample_Start_m1986109 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DebugLogger(System.UInt32,System.String,System.String,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DebugLogger_m4016824708 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, uint32_t ___index0, String_t* ___button1, String_t* ___action2, ControllerInteractionEventArgs_t287637539  ___e3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoTriggerPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoTriggerPressed_m37822762 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoTriggerReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoTriggerReleased_m3951365989 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoTriggerTouchStart(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoTriggerTouchStart_m2698209577 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoTriggerTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoTriggerTouchEnd_m2277770110 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoTriggerHairlineStart(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoTriggerHairlineStart_m2415920626 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoTriggerHairlineEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoTriggerHairlineEnd_m815558035 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoTriggerClicked(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoTriggerClicked_m1468279315 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoTriggerUnclicked(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoTriggerUnclicked_m215416476 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoTriggerAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoTriggerAxisChanged_m2201498577 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoGripPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoGripPressed_m775092044 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoGripReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoGripReleased_m2377908057 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoGripTouchStart(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoGripTouchStart_m1814789125 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoGripTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoGripTouchEnd_m3145033744 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoGripHairlineStart(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoGripHairlineStart_m198726904 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoGripHairlineEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoGripHairlineEnd_m2640782519 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoGripClicked(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoGripClicked_m266446727 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoGripUnclicked(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoGripUnclicked_m3309080998 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoGripAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoGripAxisChanged_m2591790749 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoTouchpadPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoTouchpadPressed_m1841211520 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoTouchpadReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoTouchpadReleased_m3055847241 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoTouchpadTouchStart(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoTouchpadTouchStart_m3300580797 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoTouchpadTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoTouchpadTouchEnd_m4222909900 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoTouchpadAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoTouchpadAxisChanged_m2691280749 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoButtonOnePressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoButtonOnePressed_m3383843166 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoButtonOneReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoButtonOneReleased_m737002695 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoButtonOneTouchStart(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoButtonOneTouchStart_m3825770803 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoButtonOneTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoButtonOneTouchEnd_m1610156774 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoButtonTwoPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoButtonTwoPressed_m3926580816 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoButtonTwoReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoButtonTwoReleased_m1993801499 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoButtonTwoTouchStart(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoButtonTwoTouchStart_m3227448079 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoButtonTwoTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoButtonTwoTouchEnd_m567067272 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoStartMenuPressed(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoStartMenuPressed_m3121875537 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoStartMenuReleased(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoStartMenuReleased_m2397491898 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoControllerEnabled(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoControllerEnabled_m1570072853 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoControllerDisabled(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoControllerDisabled_m249675840 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerEvents_ListenerExample::DoControllerIndexChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_ControllerEvents_ListenerExample_DoControllerIndexChanged_m3293682440 (VRTK_ControllerEvents_ListenerExample_t190763384 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
