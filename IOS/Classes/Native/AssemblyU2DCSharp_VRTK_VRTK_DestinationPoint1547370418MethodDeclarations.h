﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_DestinationPoint
struct VRTK_DestinationPoint_t1547370418;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventArgs1852451661.h"

// System.Void VRTK.VRTK_DestinationPoint::.ctor()
extern "C"  void VRTK_DestinationPoint__ctor_m4081159014 (VRTK_DestinationPoint_t1547370418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::ResetDestinationPoint()
extern "C"  void VRTK_DestinationPoint_ResetDestinationPoint_m905620763 (VRTK_DestinationPoint_t1547370418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::OnEnable()
extern "C"  void VRTK_DestinationPoint_OnEnable_m403961914 (VRTK_DestinationPoint_t1547370418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::OnDisable()
extern "C"  void VRTK_DestinationPoint_OnDisable_m2606461743 (VRTK_DestinationPoint_t1547370418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::Update()
extern "C"  void VRTK_DestinationPoint_Update_m4063384231 (VRTK_DestinationPoint_t1547370418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::CreateColliderIfRequired()
extern "C"  void VRTK_DestinationPoint_CreateColliderIfRequired_m1889632382 (VRTK_DestinationPoint_t1547370418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::SetupRigidbody()
extern "C"  void VRTK_DestinationPoint_SetupRigidbody_m2010874240 (VRTK_DestinationPoint_t1547370418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_DestinationPoint::ManageDestinationMarkersAtEndOfFrame()
extern "C"  Il2CppObject * VRTK_DestinationPoint_ManageDestinationMarkersAtEndOfFrame_m1957062600 (VRTK_DestinationPoint_t1547370418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::ManageDestinationMarkers(System.Boolean)
extern "C"  void VRTK_DestinationPoint_ManageDestinationMarkers_m8079881 (VRTK_DestinationPoint_t1547370418 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::ManageDestinationMarkerListeners(UnityEngine.GameObject,System.Boolean)
extern "C"  void VRTK_DestinationPoint_ManageDestinationMarkerListeners_m3476916959 (VRTK_DestinationPoint_t1547370418 * __this, GameObject_t1756533147 * ___markerMaker0, bool ___register1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::DoDestinationMarkerEnter(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_DestinationPoint_DoDestinationMarkerEnter_m2858838257 (VRTK_DestinationPoint_t1547370418 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::DoDestinationMarkerExit(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_DestinationPoint_DoDestinationMarkerExit_m2246027513 (VRTK_DestinationPoint_t1547370418 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::DoDestinationMarkerSet(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_DestinationPoint_DoDestinationMarkerSet_m3710475563 (VRTK_DestinationPoint_t1547370418 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_DestinationPoint::DoDestinationMarkerSetAtEndOfFrame(VRTK.DestinationMarkerEventArgs)
extern "C"  Il2CppObject * VRTK_DestinationPoint_DoDestinationMarkerSetAtEndOfFrame_m558020113 (VRTK_DestinationPoint_t1547370418 * __this, DestinationMarkerEventArgs_t1852451661  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::ToggleCursor(System.Object,System.Boolean)
extern "C"  void VRTK_DestinationPoint_ToggleCursor_m2604186427 (VRTK_DestinationPoint_t1547370418 * __this, Il2CppObject * ___sender0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::EnablePoint()
extern "C"  void VRTK_DestinationPoint_EnablePoint_m4086064511 (VRTK_DestinationPoint_t1547370418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::DisablePoint()
extern "C"  void VRTK_DestinationPoint_DisablePoint_m786529682 (VRTK_DestinationPoint_t1547370418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::ResetPoint()
extern "C"  void VRTK_DestinationPoint_ResetPoint_m2626995611 (VRTK_DestinationPoint_t1547370418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_DestinationPoint::ToggleObject(UnityEngine.GameObject,System.Boolean)
extern "C"  void VRTK_DestinationPoint_ToggleObject_m3658607488 (VRTK_DestinationPoint_t1547370418 * __this, GameObject_t1756533147 * ___givenObject0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
