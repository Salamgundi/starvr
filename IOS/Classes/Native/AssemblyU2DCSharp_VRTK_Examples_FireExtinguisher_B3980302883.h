﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animation
struct Animation_t2068071072;
// VRTK.Examples.FireExtinguisher_Sprayer
struct FireExtinguisher_Sprayer_t1157307020;
// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// VRTK.VRTK_ControllerActions
struct VRTK_ControllerActions_t3642353851;

#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.FireExtinguisher_Base
struct  FireExtinguisher_Base_t3980302883  : public VRTK_InteractableObject_t2604188111
{
public:
	// UnityEngine.Animation VRTK.Examples.FireExtinguisher_Base::leverAnimation
	Animation_t2068071072 * ___leverAnimation_45;
	// VRTK.Examples.FireExtinguisher_Sprayer VRTK.Examples.FireExtinguisher_Base::sprayer
	FireExtinguisher_Sprayer_t1157307020 * ___sprayer_46;
	// VRTK.VRTK_ControllerEvents VRTK.Examples.FireExtinguisher_Base::controllerEvents
	VRTK_ControllerEvents_t3225224819 * ___controllerEvents_47;
	// VRTK.VRTK_ControllerActions VRTK.Examples.FireExtinguisher_Base::controllerActions
	VRTK_ControllerActions_t3642353851 * ___controllerActions_48;

public:
	inline static int32_t get_offset_of_leverAnimation_45() { return static_cast<int32_t>(offsetof(FireExtinguisher_Base_t3980302883, ___leverAnimation_45)); }
	inline Animation_t2068071072 * get_leverAnimation_45() const { return ___leverAnimation_45; }
	inline Animation_t2068071072 ** get_address_of_leverAnimation_45() { return &___leverAnimation_45; }
	inline void set_leverAnimation_45(Animation_t2068071072 * value)
	{
		___leverAnimation_45 = value;
		Il2CppCodeGenWriteBarrier(&___leverAnimation_45, value);
	}

	inline static int32_t get_offset_of_sprayer_46() { return static_cast<int32_t>(offsetof(FireExtinguisher_Base_t3980302883, ___sprayer_46)); }
	inline FireExtinguisher_Sprayer_t1157307020 * get_sprayer_46() const { return ___sprayer_46; }
	inline FireExtinguisher_Sprayer_t1157307020 ** get_address_of_sprayer_46() { return &___sprayer_46; }
	inline void set_sprayer_46(FireExtinguisher_Sprayer_t1157307020 * value)
	{
		___sprayer_46 = value;
		Il2CppCodeGenWriteBarrier(&___sprayer_46, value);
	}

	inline static int32_t get_offset_of_controllerEvents_47() { return static_cast<int32_t>(offsetof(FireExtinguisher_Base_t3980302883, ___controllerEvents_47)); }
	inline VRTK_ControllerEvents_t3225224819 * get_controllerEvents_47() const { return ___controllerEvents_47; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_controllerEvents_47() { return &___controllerEvents_47; }
	inline void set_controllerEvents_47(VRTK_ControllerEvents_t3225224819 * value)
	{
		___controllerEvents_47 = value;
		Il2CppCodeGenWriteBarrier(&___controllerEvents_47, value);
	}

	inline static int32_t get_offset_of_controllerActions_48() { return static_cast<int32_t>(offsetof(FireExtinguisher_Base_t3980302883, ___controllerActions_48)); }
	inline VRTK_ControllerActions_t3642353851 * get_controllerActions_48() const { return ___controllerActions_48; }
	inline VRTK_ControllerActions_t3642353851 ** get_address_of_controllerActions_48() { return &___controllerActions_48; }
	inline void set_controllerActions_48(VRTK_ControllerActions_t3642353851 * value)
	{
		___controllerActions_48 = value;
		Il2CppCodeGenWriteBarrier(&___controllerActions_48, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
