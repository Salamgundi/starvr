﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Breakable_Cube
struct Breakable_Cube_t459142415;
// UnityEngine.Collision
struct Collision_t2876846408;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void VRTK.Examples.Breakable_Cube::.ctor()
extern "C"  void Breakable_Cube__ctor_m3139173256 (Breakable_Cube_t459142415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Breakable_Cube::Start()
extern "C"  void Breakable_Cube_Start_m1476205560 (Breakable_Cube_t459142415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Breakable_Cube::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void Breakable_Cube_OnCollisionEnter_m3660768662 (Breakable_Cube_t459142415 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.Examples.Breakable_Cube::GetCollisionForce(UnityEngine.Collision)
extern "C"  float Breakable_Cube_GetCollisionForce_m1619072434 (Breakable_Cube_t459142415 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Breakable_Cube::ExplodeCube(System.Single)
extern "C"  void Breakable_Cube_ExplodeCube_m2967704867 (Breakable_Cube_t459142415 * __this, float ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Breakable_Cube::ExplodeFace(UnityEngine.Transform,System.Single)
extern "C"  void Breakable_Cube_ExplodeFace_m2914021410 (Breakable_Cube_t459142415 * __this, Transform_t3275118058 * ___face0, float ___force1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
