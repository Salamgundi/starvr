﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1214023521;
struct RaycastHit_t87180320_marshaled_pinvoke;
struct RaycastHit_t87180320_marshaled_com;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.DashTeleportEventArgs
struct  DashTeleportEventArgs_t2197253242 
{
public:
	// UnityEngine.RaycastHit[] VRTK.DashTeleportEventArgs::hits
	RaycastHitU5BU5D_t1214023521* ___hits_0;

public:
	inline static int32_t get_offset_of_hits_0() { return static_cast<int32_t>(offsetof(DashTeleportEventArgs_t2197253242, ___hits_0)); }
	inline RaycastHitU5BU5D_t1214023521* get_hits_0() const { return ___hits_0; }
	inline RaycastHitU5BU5D_t1214023521** get_address_of_hits_0() { return &___hits_0; }
	inline void set_hits_0(RaycastHitU5BU5D_t1214023521* value)
	{
		___hits_0 = value;
		Il2CppCodeGenWriteBarrier(&___hits_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VRTK.DashTeleportEventArgs
struct DashTeleportEventArgs_t2197253242_marshaled_pinvoke
{
	RaycastHit_t87180320_marshaled_pinvoke* ___hits_0;
};
// Native definition for COM marshalling of VRTK.DashTeleportEventArgs
struct DashTeleportEventArgs_t2197253242_marshaled_com
{
	RaycastHit_t87180320_marshaled_com* ___hits_0;
};
