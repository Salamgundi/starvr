﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_IsHoverTargetOverlay
struct _IsHoverTargetOverlay_t4101716668;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_IsHoverTargetOverlay::.ctor(System.Object,System.IntPtr)
extern "C"  void _IsHoverTargetOverlay__ctor_m970165695 (_IsHoverTargetOverlay_t4101716668 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVROverlay/_IsHoverTargetOverlay::Invoke(System.UInt64)
extern "C"  bool _IsHoverTargetOverlay_Invoke_m1295590524 (_IsHoverTargetOverlay_t4101716668 * __this, uint64_t ___ulOverlayHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_IsHoverTargetOverlay::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _IsHoverTargetOverlay_BeginInvoke_m13632179 (_IsHoverTargetOverlay_t4101716668 * __this, uint64_t ___ulOverlayHandle0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVROverlay/_IsHoverTargetOverlay::EndInvoke(System.IAsyncResult)
extern "C"  bool _IsHoverTargetOverlay_EndInvoke_m2346494265 (_IsHoverTargetOverlay_t4101716668 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
