﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.SpawnAndAttachAfterControllerIsTracking
struct  SpawnAndAttachAfterControllerIsTracking_t115944288  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.SpawnAndAttachAfterControllerIsTracking::hand
	Hand_t379716353 * ___hand_2;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.SpawnAndAttachAfterControllerIsTracking::itemPrefab
	GameObject_t1756533147 * ___itemPrefab_3;

public:
	inline static int32_t get_offset_of_hand_2() { return static_cast<int32_t>(offsetof(SpawnAndAttachAfterControllerIsTracking_t115944288, ___hand_2)); }
	inline Hand_t379716353 * get_hand_2() const { return ___hand_2; }
	inline Hand_t379716353 ** get_address_of_hand_2() { return &___hand_2; }
	inline void set_hand_2(Hand_t379716353 * value)
	{
		___hand_2 = value;
		Il2CppCodeGenWriteBarrier(&___hand_2, value);
	}

	inline static int32_t get_offset_of_itemPrefab_3() { return static_cast<int32_t>(offsetof(SpawnAndAttachAfterControllerIsTracking_t115944288, ___itemPrefab_3)); }
	inline GameObject_t1756533147 * get_itemPrefab_3() const { return ___itemPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_itemPrefab_3() { return &___itemPrefab_3; }
	inline void set_itemPrefab_3(GameObject_t1756533147 * value)
	{
		___itemPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
