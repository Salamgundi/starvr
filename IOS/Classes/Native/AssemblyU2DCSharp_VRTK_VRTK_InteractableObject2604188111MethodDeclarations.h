﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// VRTK.InteractableObjectEventHandler
struct InteractableObjectEventHandler_t940909295;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// VRTK.VRTK_SnapDropZone
struct VRTK_SnapDropZone_t1948041105;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventHandl940909295.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject_All2794712781.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SnapDropZone1948041105.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_DestinationMarkerEventArgs1852451661.h"

// System.Void VRTK.VRTK_InteractableObject::.ctor()
extern "C"  void VRTK_InteractableObject__ctor_m434600263 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::add_InteractableObjectTouched(VRTK.InteractableObjectEventHandler)
extern "C"  void VRTK_InteractableObject_add_InteractableObjectTouched_m3633503924 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventHandler_t940909295 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::remove_InteractableObjectTouched(VRTK.InteractableObjectEventHandler)
extern "C"  void VRTK_InteractableObject_remove_InteractableObjectTouched_m1961600827 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventHandler_t940909295 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::add_InteractableObjectUntouched(VRTK.InteractableObjectEventHandler)
extern "C"  void VRTK_InteractableObject_add_InteractableObjectUntouched_m2625738023 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventHandler_t940909295 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::remove_InteractableObjectUntouched(VRTK.InteractableObjectEventHandler)
extern "C"  void VRTK_InteractableObject_remove_InteractableObjectUntouched_m748698654 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventHandler_t940909295 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::add_InteractableObjectGrabbed(VRTK.InteractableObjectEventHandler)
extern "C"  void VRTK_InteractableObject_add_InteractableObjectGrabbed_m2695870213 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventHandler_t940909295 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::remove_InteractableObjectGrabbed(VRTK.InteractableObjectEventHandler)
extern "C"  void VRTK_InteractableObject_remove_InteractableObjectGrabbed_m1540416264 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventHandler_t940909295 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::add_InteractableObjectUngrabbed(VRTK.InteractableObjectEventHandler)
extern "C"  void VRTK_InteractableObject_add_InteractableObjectUngrabbed_m569745414 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventHandler_t940909295 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::remove_InteractableObjectUngrabbed(VRTK.InteractableObjectEventHandler)
extern "C"  void VRTK_InteractableObject_remove_InteractableObjectUngrabbed_m1763623297 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventHandler_t940909295 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::add_InteractableObjectUsed(VRTK.InteractableObjectEventHandler)
extern "C"  void VRTK_InteractableObject_add_InteractableObjectUsed_m4142747459 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventHandler_t940909295 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::remove_InteractableObjectUsed(VRTK.InteractableObjectEventHandler)
extern "C"  void VRTK_InteractableObject_remove_InteractableObjectUsed_m2147079576 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventHandler_t940909295 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::add_InteractableObjectUnused(VRTK.InteractableObjectEventHandler)
extern "C"  void VRTK_InteractableObject_add_InteractableObjectUnused_m226488610 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventHandler_t940909295 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::remove_InteractableObjectUnused(VRTK.InteractableObjectEventHandler)
extern "C"  void VRTK_InteractableObject_remove_InteractableObjectUnused_m736257471 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventHandler_t940909295 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractableObject::get_isKinematic()
extern "C"  bool VRTK_InteractableObject_get_isKinematic_m2341181999 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::set_isKinematic(System.Boolean)
extern "C"  void VRTK_InteractableObject_set_isKinematic_m1079904828 (VRTK_InteractableObject_t2604188111 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::OnInteractableObjectTouched(VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_InteractableObject_OnInteractableObjectTouched_m1600698842 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventArgs_t473175556  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::OnInteractableObjectUntouched(VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_InteractableObject_OnInteractableObjectUntouched_m1006546107 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventArgs_t473175556  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::OnInteractableObjectGrabbed(VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_InteractableObject_OnInteractableObjectGrabbed_m530635689 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventArgs_t473175556  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::OnInteractableObjectUngrabbed(VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_InteractableObject_OnInteractableObjectUngrabbed_m1405179964 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventArgs_t473175556  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::OnInteractableObjectUsed(VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_InteractableObject_OnInteractableObjectUsed_m1659850875 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventArgs_t473175556  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::OnInteractableObjectUnused(VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_InteractableObject_OnInteractableObjectUnused_m1672653582 (VRTK_InteractableObject_t2604188111 * __this, InteractableObjectEventArgs_t473175556  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.InteractableObjectEventArgs VRTK.VRTK_InteractableObject::SetInteractableObjectEvent(UnityEngine.GameObject)
extern "C"  InteractableObjectEventArgs_t473175556  VRTK_InteractableObject_SetInteractableObjectEvent_m3727834458 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___interactingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractableObject::IsTouched()
extern "C"  bool VRTK_InteractableObject_IsTouched_m2186867785 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractableObject::IsGrabbed(UnityEngine.GameObject)
extern "C"  bool VRTK_InteractableObject_IsGrabbed_m2352200634 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___grabbedBy0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractableObject::IsUsing(UnityEngine.GameObject)
extern "C"  bool VRTK_InteractableObject_IsUsing_m845499359 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___usedBy0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::StartTouching(UnityEngine.GameObject)
extern "C"  void VRTK_InteractableObject_StartTouching_m2587501092 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___currentTouchingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::StopTouching(UnityEngine.GameObject)
extern "C"  void VRTK_InteractableObject_StopTouching_m2578657684 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___previousTouchingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::Grabbed(UnityEngine.GameObject)
extern "C"  void VRTK_InteractableObject_Grabbed_m4186915044 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___currentGrabbingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::Ungrabbed(UnityEngine.GameObject)
extern "C"  void VRTK_InteractableObject_Ungrabbed_m3054527669 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___previousGrabbingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::StartUsing(UnityEngine.GameObject)
extern "C"  void VRTK_InteractableObject_StartUsing_m837058141 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___currentUsingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::StopUsing(UnityEngine.GameObject)
extern "C"  void VRTK_InteractableObject_StopUsing_m3269840985 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___previousUsingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::ToggleHighlight(System.Boolean)
extern "C"  void VRTK_InteractableObject_ToggleHighlight_m1404261194 (VRTK_InteractableObject_t2604188111 * __this, bool ___toggle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::ResetHighlighter()
extern "C"  void VRTK_InteractableObject_ResetHighlighter_m764249277 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::PauseCollisions(System.Single)
extern "C"  void VRTK_InteractableObject_PauseCollisions_m286332905 (VRTK_InteractableObject_t2604188111 * __this, float ___delay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::ZeroVelocity()
extern "C"  void VRTK_InteractableObject_ZeroVelocity_m4220509610 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::SaveCurrentState()
extern "C"  void VRTK_InteractableObject_SaveCurrentState_m1292236568 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.GameObject> VRTK.VRTK_InteractableObject::GetTouchingObjects()
extern "C"  List_1_t1125654279 * VRTK_InteractableObject_GetTouchingObjects_m692873919 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_InteractableObject::GetGrabbingObject()
extern "C"  GameObject_t1756533147 * VRTK_InteractableObject_GetGrabbingObject_m396453179 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_InteractableObject::GetSecondaryGrabbingObject()
extern "C"  GameObject_t1756533147 * VRTK_InteractableObject_GetSecondaryGrabbingObject_m1267840361 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_InteractableObject::GetUsingObject()
extern "C"  GameObject_t1756533147 * VRTK_InteractableObject_GetUsingObject_m3609225217 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractableObject::IsValidInteractableController(UnityEngine.GameObject,VRTK.VRTK_InteractableObject/AllowedController)
extern "C"  bool VRTK_InteractableObject_IsValidInteractableController_m2703252170 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___actualController0, int32_t ___controllerCheck1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::ForceStopInteracting()
extern "C"  void VRTK_InteractableObject_ForceStopInteracting_m4043691456 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::ForceStopSecondaryGrabInteraction()
extern "C"  void VRTK_InteractableObject_ForceStopSecondaryGrabInteraction_m595034892 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::RegisterTeleporters()
extern "C"  void VRTK_InteractableObject_RegisterTeleporters_m3665917819 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::UnregisterTeleporters()
extern "C"  void VRTK_InteractableObject_UnregisterTeleporters_m87604948 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::StoreLocalScale()
extern "C"  void VRTK_InteractableObject_StoreLocalScale_m1449831817 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::ToggleSnapDropZone(VRTK.VRTK_SnapDropZone,System.Boolean)
extern "C"  void VRTK_InteractableObject_ToggleSnapDropZone_m1979379095 (VRTK_InteractableObject_t2604188111 * __this, VRTK_SnapDropZone_t1948041105 * ___snapDropZone0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractableObject::IsInSnapDropZone()
extern "C"  bool VRTK_InteractableObject_IsInSnapDropZone_m508222417 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::SetSnapDropZoneHover(System.Boolean)
extern "C"  void VRTK_InteractableObject_SetSnapDropZoneHover_m3211358235 (VRTK_InteractableObject_t2604188111 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_SnapDropZone VRTK.VRTK_InteractableObject::GetStoredSnapDropZone()
extern "C"  VRTK_SnapDropZone_t1948041105 * VRTK_InteractableObject_GetStoredSnapDropZone_m2811218204 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractableObject::IsDroppable()
extern "C"  bool VRTK_InteractableObject_IsDroppable_m577794862 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractableObject::IsSwappable()
extern "C"  bool VRTK_InteractableObject_IsSwappable_m1487041984 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractableObject::PerformSecondaryAction()
extern "C"  bool VRTK_InteractableObject_PerformSecondaryAction_m482875794 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::Awake()
extern "C"  void VRTK_InteractableObject_Awake_m1063930100 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::OnEnable()
extern "C"  void VRTK_InteractableObject_OnEnable_m1735313859 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::OnDisable()
extern "C"  void VRTK_InteractableObject_OnDisable_m809495528 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::FixedUpdate()
extern "C"  void VRTK_InteractableObject_FixedUpdate_m1351468732 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::Update()
extern "C"  void VRTK_InteractableObject_Update_m4170271126 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::LateUpdate()
extern "C"  void VRTK_InteractableObject_LateUpdate_m1732600674 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::LoadPreviousState()
extern "C"  void VRTK_InteractableObject_LoadPreviousState_m1659315023 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::InitialiseHighlighter()
extern "C"  void VRTK_InteractableObject_InitialiseHighlighter_m2543725813 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::ToggleEnableState(System.Boolean)
extern "C"  void VRTK_InteractableObject_ToggleEnableState_m2416836986 (VRTK_InteractableObject_t2604188111 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::AttemptSetGrabMechanic()
extern "C"  void VRTK_InteractableObject_AttemptSetGrabMechanic_m3551612944 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::AttemptSetSecondaryGrabAction()
extern "C"  void VRTK_InteractableObject_AttemptSetSecondaryGrabAction_m1609661592 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::ForceReleaseGrab()
extern "C"  void VRTK_InteractableObject_ForceReleaseGrab_m1474391069 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::PrimaryControllerGrab(UnityEngine.GameObject)
extern "C"  void VRTK_InteractableObject_PrimaryControllerGrab_m361142129 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___currentGrabbingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::SecondaryControllerGrab(UnityEngine.GameObject)
extern "C"  void VRTK_InteractableObject_SecondaryControllerGrab_m1433247075 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___currentGrabbingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::PrimaryControllerUngrab(UnityEngine.GameObject)
extern "C"  void VRTK_InteractableObject_PrimaryControllerUngrab_m1499343286 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___previousGrabbingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::SecondaryControllerUngrab(UnityEngine.GameObject)
extern "C"  void VRTK_InteractableObject_SecondaryControllerUngrab_m3584120262 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___previousGrabbingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::UnpauseCollisions()
extern "C"  void VRTK_InteractableObject_UnpauseCollisions_m2951654659 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::SetTrackPoint(UnityEngine.GameObject)
extern "C"  void VRTK_InteractableObject_SetTrackPoint_m2312973250 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___currentGrabbingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_InteractableObject::CreateAttachPoint(System.String,System.String,UnityEngine.Transform)
extern "C"  Transform_t3275118058 * VRTK_InteractableObject_CreateAttachPoint_m218115375 (VRTK_InteractableObject_t2604188111 * __this, String_t* ___namePrefix0, String_t* ___nameSuffix1, Transform_t3275118058 * ___origin2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::AddTrackPoint(UnityEngine.GameObject)
extern "C"  void VRTK_InteractableObject_AddTrackPoint_m3781012041 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___currentGrabbingObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::RemoveTrackPoint()
extern "C"  void VRTK_InteractableObject_RemoveTrackPoint_m4074207128 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::OnTeleporting(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_InteractableObject_OnTeleporting_m2039003469 (VRTK_InteractableObject_t2604188111 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::OnTeleported(System.Object,VRTK.DestinationMarkerEventArgs)
extern "C"  void VRTK_InteractableObject_OnTeleported_m457178866 (VRTK_InteractableObject_t2604188111 * __this, Il2CppObject * ___sender0, DestinationMarkerEventArgs_t1852451661  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_InteractableObject::RegisterTeleportersAtEndOfFrame()
extern "C"  Il2CppObject * VRTK_InteractableObject_RegisterTeleportersAtEndOfFrame_m4267139547 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::ResetUseState(UnityEngine.GameObject)
extern "C"  void VRTK_InteractableObject_ResetUseState_m1886792602 (VRTK_InteractableObject_t2604188111 * __this, GameObject_t1756533147 * ___checkObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_InteractableObject::ForceStopInteractingAtEndOfFrame()
extern "C"  Il2CppObject * VRTK_InteractableObject_ForceStopInteractingAtEndOfFrame_m381262508 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::ForceStopAllInteractions()
extern "C"  void VRTK_InteractableObject_ForceStopAllInteractions_m1073895480 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::StopTouchingInteractions()
extern "C"  void VRTK_InteractableObject_StopTouchingInteractions_m1193599827 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::StopGrabbingInteractions()
extern "C"  void VRTK_InteractableObject_StopGrabbingInteractions_m3359028282 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::StopUsingInteractions()
extern "C"  void VRTK_InteractableObject_StopUsingInteractions_m1284981702 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::ResetDropSnapType()
extern "C"  void VRTK_InteractableObject_ResetDropSnapType_m185953215 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractableObject::ResetUsingObject()
extern "C"  void VRTK_InteractableObject_ResetUsingObject_m4115697213 (VRTK_InteractableObject_t2604188111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
