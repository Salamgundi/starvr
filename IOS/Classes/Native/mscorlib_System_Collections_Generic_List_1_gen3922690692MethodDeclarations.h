﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct List_1_t3922690692;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct IEnumerable_1_t550729309;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>[]
struct KeyValuePair_2U5BU5D_t908974985;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct IEnumerator_1_t2029093387;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct ICollection_1_t1210677569;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct ReadOnlyCollection_1_t444387956;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct Predicate_1_t2996539675;
// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct Comparison_1_t1520341115;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_258602264.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3457420366.h"

// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::.ctor()
extern "C"  void List_1__ctor_m3285292010_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1__ctor_m3285292010(__this, method) ((  void (*) (List_1_t3922690692 *, const MethodInfo*))List_1__ctor_m3285292010_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2187392909_gshared (List_1_t3922690692 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m2187392909(__this, ___collection0, method) ((  void (*) (List_1_t3922690692 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2187392909_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m187318711_gshared (List_1_t3922690692 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m187318711(__this, ___capacity0, method) ((  void (*) (List_1_t3922690692 *, int32_t, const MethodInfo*))List_1__ctor_m187318711_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m4032496353_gshared (List_1_t3922690692 * __this, KeyValuePair_2U5BU5D_t908974985* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m4032496353(__this, ___data0, ___size1, method) ((  void (*) (List_1_t3922690692 *, KeyValuePair_2U5BU5D_t908974985*, int32_t, const MethodInfo*))List_1__ctor_m4032496353_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::.cctor()
extern "C"  void List_1__cctor_m439393245_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m439393245(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m439393245_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4218872492_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4218872492(__this, method) ((  Il2CppObject* (*) (List_1_t3922690692 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4218872492_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m737228814_gshared (List_1_t3922690692 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m737228814(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3922690692 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m737228814_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3771602507_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3771602507(__this, method) ((  Il2CppObject * (*) (List_1_t3922690692 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3771602507_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m175753090_gshared (List_1_t3922690692 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m175753090(__this, ___item0, method) ((  int32_t (*) (List_1_t3922690692 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m175753090_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3663643692_gshared (List_1_t3922690692 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3663643692(__this, ___item0, method) ((  bool (*) (List_1_t3922690692 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3663643692_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3644396376_gshared (List_1_t3922690692 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3644396376(__this, ___item0, method) ((  int32_t (*) (List_1_t3922690692 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3644396376_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m677322555_gshared (List_1_t3922690692 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m677322555(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3922690692 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m677322555_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m4156515483_gshared (List_1_t3922690692 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m4156515483(__this, ___item0, method) ((  void (*) (List_1_t3922690692 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m4156515483_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2443303903_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2443303903(__this, method) ((  bool (*) (List_1_t3922690692 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2443303903_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1988943450_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1988943450(__this, method) ((  bool (*) (List_1_t3922690692 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1988943450_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2805736772_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2805736772(__this, method) ((  Il2CppObject * (*) (List_1_t3922690692 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2805736772_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1491163899_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1491163899(__this, method) ((  bool (*) (List_1_t3922690692 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1491163899_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m710753254_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m710753254(__this, method) ((  bool (*) (List_1_t3922690692 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m710753254_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m839249255_gshared (List_1_t3922690692 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m839249255(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3922690692 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m839249255_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2220775960_gshared (List_1_t3922690692 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2220775960(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3922690692 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2220775960_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Add(T)
extern "C"  void List_1_Add_m2618471391_gshared (List_1_t3922690692 * __this, KeyValuePair_2_t258602264  ___item0, const MethodInfo* method);
#define List_1_Add_m2618471391(__this, ___item0, method) ((  void (*) (List_1_t3922690692 *, KeyValuePair_2_t258602264 , const MethodInfo*))List_1_Add_m2618471391_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2082116036_gshared (List_1_t3922690692 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2082116036(__this, ___newCount0, method) ((  void (*) (List_1_t3922690692 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2082116036_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m3478515895_gshared (List_1_t3922690692 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m3478515895(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3922690692 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m3478515895_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m506292804_gshared (List_1_t3922690692 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m506292804(__this, ___collection0, method) ((  void (*) (List_1_t3922690692 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m506292804_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1772672148_gshared (List_1_t3922690692 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1772672148(__this, ___enumerable0, method) ((  void (*) (List_1_t3922690692 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1772672148_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m4214833527_gshared (List_1_t3922690692 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m4214833527(__this, ___collection0, method) ((  void (*) (List_1_t3922690692 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m4214833527_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t444387956 * List_1_AsReadOnly_m47978846_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m47978846(__this, method) ((  ReadOnlyCollection_1_t444387956 * (*) (List_1_t3922690692 *, const MethodInfo*))List_1_AsReadOnly_m47978846_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Clear()
extern "C"  void List_1_Clear_m4091418123_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_Clear_m4091418123(__this, method) ((  void (*) (List_1_t3922690692 *, const MethodInfo*))List_1_Clear_m4091418123_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Contains(T)
extern "C"  bool List_1_Contains_m274162405_gshared (List_1_t3922690692 * __this, KeyValuePair_2_t258602264  ___item0, const MethodInfo* method);
#define List_1_Contains_m274162405(__this, ___item0, method) ((  bool (*) (List_1_t3922690692 *, KeyValuePair_2_t258602264 , const MethodInfo*))List_1_Contains_m274162405_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1017395235_gshared (List_1_t3922690692 * __this, KeyValuePair_2U5BU5D_t908974985* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1017395235(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3922690692 *, KeyValuePair_2U5BU5D_t908974985*, int32_t, const MethodInfo*))List_1_CopyTo_m1017395235_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Find(System.Predicate`1<T>)
extern "C"  KeyValuePair_2_t258602264  List_1_Find_m2540784669_gshared (List_1_t3922690692 * __this, Predicate_1_t2996539675 * ___match0, const MethodInfo* method);
#define List_1_Find_m2540784669(__this, ___match0, method) ((  KeyValuePair_2_t258602264  (*) (List_1_t3922690692 *, Predicate_1_t2996539675 *, const MethodInfo*))List_1_Find_m2540784669_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3085649910_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2996539675 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3085649910(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2996539675 *, const MethodInfo*))List_1_CheckMatch_m3085649910_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t3922690692 * List_1_FindAll_m360859322_gshared (List_1_t3922690692 * __this, Predicate_1_t2996539675 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m360859322(__this, ___match0, method) ((  List_1_t3922690692 * (*) (List_1_t3922690692 *, Predicate_1_t2996539675 *, const MethodInfo*))List_1_FindAll_m360859322_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t3922690692 * List_1_FindAllStackBits_m3478682374_gshared (List_1_t3922690692 * __this, Predicate_1_t2996539675 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m3478682374(__this, ___match0, method) ((  List_1_t3922690692 * (*) (List_1_t3922690692 *, Predicate_1_t2996539675 *, const MethodInfo*))List_1_FindAllStackBits_m3478682374_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t3922690692 * List_1_FindAllList_m2479684342_gshared (List_1_t3922690692 * __this, Predicate_1_t2996539675 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m2479684342(__this, ___match0, method) ((  List_1_t3922690692 * (*) (List_1_t3922690692 *, Predicate_1_t2996539675 *, const MethodInfo*))List_1_FindAllList_m2479684342_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m300688364_gshared (List_1_t3922690692 * __this, Predicate_1_t2996539675 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m300688364(__this, ___match0, method) ((  int32_t (*) (List_1_t3922690692 *, Predicate_1_t2996539675 *, const MethodInfo*))List_1_FindIndex_m300688364_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3336578637_gshared (List_1_t3922690692 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2996539675 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3336578637(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3922690692 *, int32_t, int32_t, Predicate_1_t2996539675 *, const MethodInfo*))List_1_GetIndex_m3336578637_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::GetEnumerator()
extern "C"  Enumerator_t3457420366  List_1_GetEnumerator_m1875384090_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1875384090(__this, method) ((  Enumerator_t3457420366  (*) (List_1_t3922690692 *, const MethodInfo*))List_1_GetEnumerator_m1875384090_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1209075951_gshared (List_1_t3922690692 * __this, KeyValuePair_2_t258602264  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1209075951(__this, ___item0, method) ((  int32_t (*) (List_1_t3922690692 *, KeyValuePair_2_t258602264 , const MethodInfo*))List_1_IndexOf_m1209075951_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m831403114_gshared (List_1_t3922690692 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m831403114(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3922690692 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m831403114_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2759871267_gshared (List_1_t3922690692 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2759871267(__this, ___index0, method) ((  void (*) (List_1_t3922690692 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2759871267_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2155430732_gshared (List_1_t3922690692 * __this, int32_t ___index0, KeyValuePair_2_t258602264  ___item1, const MethodInfo* method);
#define List_1_Insert_m2155430732(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3922690692 *, int32_t, KeyValuePair_2_t258602264 , const MethodInfo*))List_1_Insert_m2155430732_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2650034053_gshared (List_1_t3922690692 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2650034053(__this, ___collection0, method) ((  void (*) (List_1_t3922690692 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2650034053_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Remove(T)
extern "C"  bool List_1_Remove_m1141718218_gshared (List_1_t3922690692 * __this, KeyValuePair_2_t258602264  ___item0, const MethodInfo* method);
#define List_1_Remove_m1141718218(__this, ___item0, method) ((  bool (*) (List_1_t3922690692 *, KeyValuePair_2_t258602264 , const MethodInfo*))List_1_Remove_m1141718218_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m4195050962_gshared (List_1_t3922690692 * __this, Predicate_1_t2996539675 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m4195050962(__this, ___match0, method) ((  int32_t (*) (List_1_t3922690692 *, Predicate_1_t2996539675 *, const MethodInfo*))List_1_RemoveAll_m4195050962_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m23500600_gshared (List_1_t3922690692 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m23500600(__this, ___index0, method) ((  void (*) (List_1_t3922690692 *, int32_t, const MethodInfo*))List_1_RemoveAt_m23500600_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m1010469847_gshared (List_1_t3922690692 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m1010469847(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3922690692 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1010469847_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Reverse()
extern "C"  void List_1_Reverse_m2115744236_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_Reverse_m2115744236(__this, method) ((  void (*) (List_1_t3922690692 *, const MethodInfo*))List_1_Reverse_m2115744236_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Sort()
extern "C"  void List_1_Sort_m95732076_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_Sort_m95732076(__this, method) ((  void (*) (List_1_t3922690692 *, const MethodInfo*))List_1_Sort_m95732076_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m48300501_gshared (List_1_t3922690692 * __this, Comparison_1_t1520341115 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m48300501(__this, ___comparison0, method) ((  void (*) (List_1_t3922690692 *, Comparison_1_t1520341115 *, const MethodInfo*))List_1_Sort_m48300501_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t908974985* List_1_ToArray_m1113946329_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_ToArray_m1113946329(__this, method) ((  KeyValuePair_2U5BU5D_t908974985* (*) (List_1_t3922690692 *, const MethodInfo*))List_1_ToArray_m1113946329_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1110107703_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1110107703(__this, method) ((  void (*) (List_1_t3922690692 *, const MethodInfo*))List_1_TrimExcess_m1110107703_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3196960945_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3196960945(__this, method) ((  int32_t (*) (List_1_t3922690692 *, const MethodInfo*))List_1_get_Capacity_m3196960945_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2355038340_gshared (List_1_t3922690692 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2355038340(__this, ___value0, method) ((  void (*) (List_1_t3922690692 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2355038340_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::get_Count()
extern "C"  int32_t List_1_get_Count_m2481920306_gshared (List_1_t3922690692 * __this, const MethodInfo* method);
#define List_1_get_Count_m2481920306(__this, method) ((  int32_t (*) (List_1_t3922690692 *, const MethodInfo*))List_1_get_Count_m2481920306_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t258602264  List_1_get_Item_m2883294796_gshared (List_1_t3922690692 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2883294796(__this, ___index0, method) ((  KeyValuePair_2_t258602264  (*) (List_1_t3922690692 *, int32_t, const MethodInfo*))List_1_get_Item_m2883294796_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3461864135_gshared (List_1_t3922690692 * __this, int32_t ___index0, KeyValuePair_2_t258602264  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3461864135(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3922690692 *, int32_t, KeyValuePair_2_t258602264 , const MethodInfo*))List_1_set_Item_m3461864135_gshared)(__this, ___index0, ___value1, method)
