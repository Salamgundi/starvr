﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_CameraMask
struct SteamVR_CameraMask_t1809821990;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_CameraMask::.ctor()
extern "C"  void SteamVR_CameraMask__ctor_m4248495159 (SteamVR_CameraMask_t1809821990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_CameraMask::Awake()
extern "C"  void SteamVR_CameraMask_Awake_m3618743314 (SteamVR_CameraMask_t1809821990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
