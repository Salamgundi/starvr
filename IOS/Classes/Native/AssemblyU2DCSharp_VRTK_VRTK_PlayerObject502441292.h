﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PlayerObject_ObjectTyp2764484032.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_PlayerObject
struct  VRTK_PlayerObject_t502441292  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_PlayerObject/ObjectTypes VRTK.VRTK_PlayerObject::objectType
	int32_t ___objectType_2;

public:
	inline static int32_t get_offset_of_objectType_2() { return static_cast<int32_t>(offsetof(VRTK_PlayerObject_t502441292, ___objectType_2)); }
	inline int32_t get_objectType_2() const { return ___objectType_2; }
	inline int32_t* get_address_of_objectType_2() { return &___objectType_2; }
	inline void set_objectType_2(int32_t value)
	{
		___objectType_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
