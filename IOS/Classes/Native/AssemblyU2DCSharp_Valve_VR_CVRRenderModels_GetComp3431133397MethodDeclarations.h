﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.CVRRenderModels/GetComponentStateUnion
struct GetComponentStateUnion_t3431133397;
struct GetComponentStateUnion_t3431133397_marshaled_pinvoke;
struct GetComponentStateUnion_t3431133397_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct GetComponentStateUnion_t3431133397;
struct GetComponentStateUnion_t3431133397_marshaled_pinvoke;

extern "C" void GetComponentStateUnion_t3431133397_marshal_pinvoke(const GetComponentStateUnion_t3431133397& unmarshaled, GetComponentStateUnion_t3431133397_marshaled_pinvoke& marshaled);
extern "C" void GetComponentStateUnion_t3431133397_marshal_pinvoke_back(const GetComponentStateUnion_t3431133397_marshaled_pinvoke& marshaled, GetComponentStateUnion_t3431133397& unmarshaled);
extern "C" void GetComponentStateUnion_t3431133397_marshal_pinvoke_cleanup(GetComponentStateUnion_t3431133397_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct GetComponentStateUnion_t3431133397;
struct GetComponentStateUnion_t3431133397_marshaled_com;

extern "C" void GetComponentStateUnion_t3431133397_marshal_com(const GetComponentStateUnion_t3431133397& unmarshaled, GetComponentStateUnion_t3431133397_marshaled_com& marshaled);
extern "C" void GetComponentStateUnion_t3431133397_marshal_com_back(const GetComponentStateUnion_t3431133397_marshaled_com& marshaled, GetComponentStateUnion_t3431133397& unmarshaled);
extern "C" void GetComponentStateUnion_t3431133397_marshal_com_cleanup(GetComponentStateUnion_t3431133397_marshaled_com& marshaled);
