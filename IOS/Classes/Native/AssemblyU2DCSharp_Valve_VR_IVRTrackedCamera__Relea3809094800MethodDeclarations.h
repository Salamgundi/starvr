﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRTrackedCamera/_ReleaseVideoStreamTextureGL
struct _ReleaseVideoStreamTextureGL_t3809094800;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraError3529690400.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRTrackedCamera/_ReleaseVideoStreamTextureGL::.ctor(System.Object,System.IntPtr)
extern "C"  void _ReleaseVideoStreamTextureGL__ctor_m4164129449 (_ReleaseVideoStreamTextureGL_t3809094800 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_ReleaseVideoStreamTextureGL::Invoke(System.UInt64,System.UInt32)
extern "C"  int32_t _ReleaseVideoStreamTextureGL_Invoke_m3431574551 (_ReleaseVideoStreamTextureGL_t3809094800 * __this, uint64_t ___hTrackedCamera0, uint32_t ___glTextureId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRTrackedCamera/_ReleaseVideoStreamTextureGL::BeginInvoke(System.UInt64,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ReleaseVideoStreamTextureGL_BeginInvoke_m3451547541 (_ReleaseVideoStreamTextureGL_t3809094800 * __this, uint64_t ___hTrackedCamera0, uint32_t ___glTextureId1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_ReleaseVideoStreamTextureGL::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _ReleaseVideoStreamTextureGL_EndInvoke_m3126888000 (_ReleaseVideoStreamTextureGL_t3809094800 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
