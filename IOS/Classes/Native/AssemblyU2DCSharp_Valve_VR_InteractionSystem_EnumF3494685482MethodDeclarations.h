﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.EnumFlags
struct EnumFlags_t3494685482;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.EnumFlags::.ctor()
extern "C"  void EnumFlags__ctor_m3398578220 (EnumFlags_t3494685482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
