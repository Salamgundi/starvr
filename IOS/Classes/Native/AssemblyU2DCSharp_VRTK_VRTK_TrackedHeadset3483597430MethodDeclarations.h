﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_TrackedHeadset
struct VRTK_TrackedHeadset_t3483597430;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_TrackedHeadset::.ctor()
extern "C"  void VRTK_TrackedHeadset__ctor_m762156736 (VRTK_TrackedHeadset_t3483597430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TrackedHeadset::Update()
extern "C"  void VRTK_TrackedHeadset_Update_m708495359 (VRTK_TrackedHeadset_t3483597430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
