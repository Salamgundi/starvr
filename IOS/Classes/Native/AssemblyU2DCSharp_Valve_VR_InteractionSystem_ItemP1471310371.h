﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.ItemPackage
struct ItemPackage_t3423754743;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1543711741.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.ItemPackageSpawner
struct  ItemPackageSpawner_t1471310371  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.ItemPackage Valve.VR.InteractionSystem.ItemPackageSpawner::_itemPackage
	ItemPackage_t3423754743 * ____itemPackage_2;
	// System.Boolean Valve.VR.InteractionSystem.ItemPackageSpawner::useItemPackagePreview
	bool ___useItemPackagePreview_3;
	// System.Boolean Valve.VR.InteractionSystem.ItemPackageSpawner::useFadedPreview
	bool ___useFadedPreview_4;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.ItemPackageSpawner::previewObject
	GameObject_t1756533147 * ___previewObject_5;
	// System.Boolean Valve.VR.InteractionSystem.ItemPackageSpawner::requireTriggerPressToTake
	bool ___requireTriggerPressToTake_6;
	// System.Boolean Valve.VR.InteractionSystem.ItemPackageSpawner::requireTriggerPressToReturn
	bool ___requireTriggerPressToReturn_7;
	// System.Boolean Valve.VR.InteractionSystem.ItemPackageSpawner::showTriggerHint
	bool ___showTriggerHint_8;
	// Valve.VR.InteractionSystem.Hand/AttachmentFlags Valve.VR.InteractionSystem.ItemPackageSpawner::attachmentFlags
	int32_t ___attachmentFlags_9;
	// System.String Valve.VR.InteractionSystem.ItemPackageSpawner::attachmentPoint
	String_t* ___attachmentPoint_10;
	// System.Boolean Valve.VR.InteractionSystem.ItemPackageSpawner::takeBackItem
	bool ___takeBackItem_11;
	// System.Boolean Valve.VR.InteractionSystem.ItemPackageSpawner::acceptDifferentItems
	bool ___acceptDifferentItems_12;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.ItemPackageSpawner::spawnedItem
	GameObject_t1756533147 * ___spawnedItem_13;
	// System.Boolean Valve.VR.InteractionSystem.ItemPackageSpawner::itemIsSpawned
	bool ___itemIsSpawned_14;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.ItemPackageSpawner::pickupEvent
	UnityEvent_t408735097 * ___pickupEvent_15;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.ItemPackageSpawner::dropEvent
	UnityEvent_t408735097 * ___dropEvent_16;
	// System.Boolean Valve.VR.InteractionSystem.ItemPackageSpawner::justPickedUpItem
	bool ___justPickedUpItem_17;

public:
	inline static int32_t get_offset_of__itemPackage_2() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ____itemPackage_2)); }
	inline ItemPackage_t3423754743 * get__itemPackage_2() const { return ____itemPackage_2; }
	inline ItemPackage_t3423754743 ** get_address_of__itemPackage_2() { return &____itemPackage_2; }
	inline void set__itemPackage_2(ItemPackage_t3423754743 * value)
	{
		____itemPackage_2 = value;
		Il2CppCodeGenWriteBarrier(&____itemPackage_2, value);
	}

	inline static int32_t get_offset_of_useItemPackagePreview_3() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___useItemPackagePreview_3)); }
	inline bool get_useItemPackagePreview_3() const { return ___useItemPackagePreview_3; }
	inline bool* get_address_of_useItemPackagePreview_3() { return &___useItemPackagePreview_3; }
	inline void set_useItemPackagePreview_3(bool value)
	{
		___useItemPackagePreview_3 = value;
	}

	inline static int32_t get_offset_of_useFadedPreview_4() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___useFadedPreview_4)); }
	inline bool get_useFadedPreview_4() const { return ___useFadedPreview_4; }
	inline bool* get_address_of_useFadedPreview_4() { return &___useFadedPreview_4; }
	inline void set_useFadedPreview_4(bool value)
	{
		___useFadedPreview_4 = value;
	}

	inline static int32_t get_offset_of_previewObject_5() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___previewObject_5)); }
	inline GameObject_t1756533147 * get_previewObject_5() const { return ___previewObject_5; }
	inline GameObject_t1756533147 ** get_address_of_previewObject_5() { return &___previewObject_5; }
	inline void set_previewObject_5(GameObject_t1756533147 * value)
	{
		___previewObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___previewObject_5, value);
	}

	inline static int32_t get_offset_of_requireTriggerPressToTake_6() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___requireTriggerPressToTake_6)); }
	inline bool get_requireTriggerPressToTake_6() const { return ___requireTriggerPressToTake_6; }
	inline bool* get_address_of_requireTriggerPressToTake_6() { return &___requireTriggerPressToTake_6; }
	inline void set_requireTriggerPressToTake_6(bool value)
	{
		___requireTriggerPressToTake_6 = value;
	}

	inline static int32_t get_offset_of_requireTriggerPressToReturn_7() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___requireTriggerPressToReturn_7)); }
	inline bool get_requireTriggerPressToReturn_7() const { return ___requireTriggerPressToReturn_7; }
	inline bool* get_address_of_requireTriggerPressToReturn_7() { return &___requireTriggerPressToReturn_7; }
	inline void set_requireTriggerPressToReturn_7(bool value)
	{
		___requireTriggerPressToReturn_7 = value;
	}

	inline static int32_t get_offset_of_showTriggerHint_8() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___showTriggerHint_8)); }
	inline bool get_showTriggerHint_8() const { return ___showTriggerHint_8; }
	inline bool* get_address_of_showTriggerHint_8() { return &___showTriggerHint_8; }
	inline void set_showTriggerHint_8(bool value)
	{
		___showTriggerHint_8 = value;
	}

	inline static int32_t get_offset_of_attachmentFlags_9() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___attachmentFlags_9)); }
	inline int32_t get_attachmentFlags_9() const { return ___attachmentFlags_9; }
	inline int32_t* get_address_of_attachmentFlags_9() { return &___attachmentFlags_9; }
	inline void set_attachmentFlags_9(int32_t value)
	{
		___attachmentFlags_9 = value;
	}

	inline static int32_t get_offset_of_attachmentPoint_10() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___attachmentPoint_10)); }
	inline String_t* get_attachmentPoint_10() const { return ___attachmentPoint_10; }
	inline String_t** get_address_of_attachmentPoint_10() { return &___attachmentPoint_10; }
	inline void set_attachmentPoint_10(String_t* value)
	{
		___attachmentPoint_10 = value;
		Il2CppCodeGenWriteBarrier(&___attachmentPoint_10, value);
	}

	inline static int32_t get_offset_of_takeBackItem_11() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___takeBackItem_11)); }
	inline bool get_takeBackItem_11() const { return ___takeBackItem_11; }
	inline bool* get_address_of_takeBackItem_11() { return &___takeBackItem_11; }
	inline void set_takeBackItem_11(bool value)
	{
		___takeBackItem_11 = value;
	}

	inline static int32_t get_offset_of_acceptDifferentItems_12() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___acceptDifferentItems_12)); }
	inline bool get_acceptDifferentItems_12() const { return ___acceptDifferentItems_12; }
	inline bool* get_address_of_acceptDifferentItems_12() { return &___acceptDifferentItems_12; }
	inline void set_acceptDifferentItems_12(bool value)
	{
		___acceptDifferentItems_12 = value;
	}

	inline static int32_t get_offset_of_spawnedItem_13() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___spawnedItem_13)); }
	inline GameObject_t1756533147 * get_spawnedItem_13() const { return ___spawnedItem_13; }
	inline GameObject_t1756533147 ** get_address_of_spawnedItem_13() { return &___spawnedItem_13; }
	inline void set_spawnedItem_13(GameObject_t1756533147 * value)
	{
		___spawnedItem_13 = value;
		Il2CppCodeGenWriteBarrier(&___spawnedItem_13, value);
	}

	inline static int32_t get_offset_of_itemIsSpawned_14() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___itemIsSpawned_14)); }
	inline bool get_itemIsSpawned_14() const { return ___itemIsSpawned_14; }
	inline bool* get_address_of_itemIsSpawned_14() { return &___itemIsSpawned_14; }
	inline void set_itemIsSpawned_14(bool value)
	{
		___itemIsSpawned_14 = value;
	}

	inline static int32_t get_offset_of_pickupEvent_15() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___pickupEvent_15)); }
	inline UnityEvent_t408735097 * get_pickupEvent_15() const { return ___pickupEvent_15; }
	inline UnityEvent_t408735097 ** get_address_of_pickupEvent_15() { return &___pickupEvent_15; }
	inline void set_pickupEvent_15(UnityEvent_t408735097 * value)
	{
		___pickupEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___pickupEvent_15, value);
	}

	inline static int32_t get_offset_of_dropEvent_16() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___dropEvent_16)); }
	inline UnityEvent_t408735097 * get_dropEvent_16() const { return ___dropEvent_16; }
	inline UnityEvent_t408735097 ** get_address_of_dropEvent_16() { return &___dropEvent_16; }
	inline void set_dropEvent_16(UnityEvent_t408735097 * value)
	{
		___dropEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___dropEvent_16, value);
	}

	inline static int32_t get_offset_of_justPickedUpItem_17() { return static_cast<int32_t>(offsetof(ItemPackageSpawner_t1471310371, ___justPickedUpItem_17)); }
	inline bool get_justPickedUpItem_17() const { return ___justPickedUpItem_17; }
	inline bool* get_address_of_justPickedUpItem_17() { return &___justPickedUpItem_17; }
	inline void set_justPickedUpItem_17(bool value)
	{
		___justPickedUpItem_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
