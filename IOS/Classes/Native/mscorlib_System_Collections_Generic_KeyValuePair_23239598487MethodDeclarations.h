﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2162604193(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3239598487 *, Transform_t3275118058 *, List_1_t1445631064 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::get_Key()
#define KeyValuePair_2_get_Key_m3833677059(__this, method) ((  Transform_t3275118058 * (*) (KeyValuePair_2_t3239598487 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1490536968(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3239598487 *, Transform_t3275118058 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::get_Value()
#define KeyValuePair_2_get_Value_m2347692163(__this, method) ((  List_1_t1445631064 * (*) (KeyValuePair_2_t3239598487 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1101871096(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3239598487 *, List_1_t1445631064 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Transform,System.Collections.Generic.List`1<System.Single>>::ToString()
#define KeyValuePair_2_ToString_m42451330(__this, method) ((  String_t* (*) (KeyValuePair_2_t3239598487 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
