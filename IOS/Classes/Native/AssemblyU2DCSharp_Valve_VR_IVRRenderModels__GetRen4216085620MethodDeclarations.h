﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_GetRenderModelOriginalPath
struct _GetRenderModelOriginalPath_t4216085620;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRRenderModelError21703732.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_GetRenderModelOriginalPath::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetRenderModelOriginalPath__ctor_m4287276727 (_GetRenderModelOriginalPath_t4216085620 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelOriginalPath::Invoke(System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRRenderModelError&)
extern "C"  uint32_t _GetRenderModelOriginalPath_Invoke_m218742038 (_GetRenderModelOriginalPath_t4216085620 * __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchOriginalPath1, uint32_t ___unOriginalPathLen2, int32_t* ___peError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetRenderModelOriginalPath::BeginInvoke(System.String,System.Text.StringBuilder,System.UInt32,Valve.VR.EVRRenderModelError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetRenderModelOriginalPath_BeginInvoke_m3084562884 (_GetRenderModelOriginalPath_t4216085620 * __this, String_t* ___pchRenderModelName0, StringBuilder_t1221177846 * ___pchOriginalPath1, uint32_t ___unOriginalPathLen2, int32_t* ___peError3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRRenderModels/_GetRenderModelOriginalPath::EndInvoke(Valve.VR.EVRRenderModelError&,System.IAsyncResult)
extern "C"  uint32_t _GetRenderModelOriginalPath_EndInvoke_m215431012 (_GetRenderModelOriginalPath_t4216085620 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
