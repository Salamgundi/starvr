﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointLimits4282861422.h"

// System.Single UnityEngine.JointLimits::get_min()
extern "C"  float JointLimits_get_min_m2921097272 (JointLimits_t4282861422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointLimits::set_min(System.Single)
extern "C"  void JointLimits_set_min_m3403685567 (JointLimits_t4282861422 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.JointLimits::get_max()
extern "C"  float JointLimits_get_max_m1509472510 (JointLimits_t4282861422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointLimits::set_max(System.Single)
extern "C"  void JointLimits_set_max_m2042039841 (JointLimits_t4282861422 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointLimits::set_bounciness(System.Single)
extern "C"  void JointLimits_set_bounciness_m2541245684 (JointLimits_t4282861422 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
