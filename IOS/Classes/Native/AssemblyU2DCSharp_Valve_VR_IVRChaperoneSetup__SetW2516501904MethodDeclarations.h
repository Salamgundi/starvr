﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_SetWorkingPhysicalBoundsInfo
struct _SetWorkingPhysicalBoundsInfo_t2516501904;
// System.Object
struct Il2CppObject;
// Valve.VR.HmdQuad_t[]
struct HmdQuad_tU5BU5D_t16941492;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_SetWorkingPhysicalBoundsInfo::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetWorkingPhysicalBoundsInfo__ctor_m2742512905 (_SetWorkingPhysicalBoundsInfo_t2516501904 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_SetWorkingPhysicalBoundsInfo::Invoke(Valve.VR.HmdQuad_t[],System.UInt32)
extern "C"  bool _SetWorkingPhysicalBoundsInfo_Invoke_m2966046964 (_SetWorkingPhysicalBoundsInfo_t2516501904 * __this, HmdQuad_tU5BU5D_t16941492* ___pQuadsBuffer0, uint32_t ___unQuadsCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_SetWorkingPhysicalBoundsInfo::BeginInvoke(Valve.VR.HmdQuad_t[],System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetWorkingPhysicalBoundsInfo_BeginInvoke_m637330115 (_SetWorkingPhysicalBoundsInfo_t2516501904 * __this, HmdQuad_tU5BU5D_t16941492* ___pQuadsBuffer0, uint32_t ___unQuadsCount1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_SetWorkingPhysicalBoundsInfo::EndInvoke(System.IAsyncResult)
extern "C"  bool _SetWorkingPhysicalBoundsInfo_EndInvoke_m3119760111 (_SetWorkingPhysicalBoundsInfo_t2516501904 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
