﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,VRTK.Control3DEventArgs>
struct UnityEvent_2_t2777712310;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.Control3DEventArgs>
struct UnityAction_2_t895514392;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_VRTK_Control3DEventArgs4095025701.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.Control3DEventArgs>::.ctor()
extern "C"  void UnityEvent_2__ctor_m4039041972_gshared (UnityEvent_2_t2777712310 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m4039041972(__this, method) ((  void (*) (UnityEvent_2_t2777712310 *, const MethodInfo*))UnityEvent_2__ctor_m4039041972_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.Control3DEventArgs>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m3652911555_gshared (UnityEvent_2_t2777712310 * __this, UnityAction_2_t895514392 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m3652911555(__this, ___call0, method) ((  void (*) (UnityEvent_2_t2777712310 *, UnityAction_2_t895514392 *, const MethodInfo*))UnityEvent_2_AddListener_m3652911555_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.Control3DEventArgs>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m1572936881_gshared (UnityEvent_2_t2777712310 * __this, UnityAction_2_t895514392 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m1572936881(__this, ___call0, method) ((  void (*) (UnityEvent_2_t2777712310 *, UnityAction_2_t895514392 *, const MethodInfo*))UnityEvent_2_RemoveListener_m1572936881_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,VRTK.Control3DEventArgs>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m3345535182_gshared (UnityEvent_2_t2777712310 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m3345535182(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t2777712310 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m3345535182_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.Control3DEventArgs>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m986906266_gshared (UnityEvent_2_t2777712310 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m986906266(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t2777712310 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m986906266_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.Control3DEventArgs>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m3025684665_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t895514392 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m3025684665(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t895514392 *, const MethodInfo*))UnityEvent_2_GetDelegate_m3025684665_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.Control3DEventArgs>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m3115851627_gshared (UnityEvent_2_t2777712310 * __this, Il2CppObject * ___arg00, Control3DEventArgs_t4095025701  ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m3115851627(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t2777712310 *, Il2CppObject *, Control3DEventArgs_t4095025701 , const MethodInfo*))UnityEvent_2_Invoke_m3115851627_gshared)(__this, ___arg00, ___arg11, method)
