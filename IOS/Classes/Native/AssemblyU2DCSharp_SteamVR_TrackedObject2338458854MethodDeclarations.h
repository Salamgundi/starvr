﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_TrackedObject
struct SteamVR_TrackedObject_t2338458854;
// Valve.VR.TrackedDevicePose_t[]
struct TrackedDevicePose_tU5BU5D_t2897272049;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_TrackedObject::.ctor()
extern "C"  void SteamVR_TrackedObject__ctor_m2412325325 (SteamVR_TrackedObject_t2338458854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedObject::OnNewPoses(Valve.VR.TrackedDevicePose_t[])
extern "C"  void SteamVR_TrackedObject_OnNewPoses_m1425649700 (SteamVR_TrackedObject_t2338458854 * __this, TrackedDevicePose_tU5BU5D_t2897272049* ___poses0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedObject::Awake()
extern "C"  void SteamVR_TrackedObject_Awake_m100547894 (SteamVR_TrackedObject_t2338458854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedObject::OnEnable()
extern "C"  void SteamVR_TrackedObject_OnEnable_m3098537325 (SteamVR_TrackedObject_t2338458854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedObject::OnDisable()
extern "C"  void SteamVR_TrackedObject_OnDisable_m2530593790 (SteamVR_TrackedObject_t2338458854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedObject::SetDeviceIndex(System.Int32)
extern "C"  void SteamVR_TrackedObject_SetDeviceIndex_m179999150 (SteamVR_TrackedObject_t2338458854 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
