﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// SteamVR_RenderModel
struct SteamVR_RenderModel_t2905485978;
// SteamVR_Events/Action
struct Action_t1836998693;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.ControllerHoverHighlight
struct  ControllerHoverHighlight_t2644472  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material Valve.VR.InteractionSystem.ControllerHoverHighlight::highLightMaterial
	Material_t193706927 * ___highLightMaterial_2;
	// System.Boolean Valve.VR.InteractionSystem.ControllerHoverHighlight::fireHapticsOnHightlight
	bool ___fireHapticsOnHightlight_3;
	// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.ControllerHoverHighlight::hand
	Hand_t379716353 * ___hand_4;
	// UnityEngine.MeshRenderer Valve.VR.InteractionSystem.ControllerHoverHighlight::bodyMeshRenderer
	MeshRenderer_t1268241104 * ___bodyMeshRenderer_5;
	// UnityEngine.MeshRenderer Valve.VR.InteractionSystem.ControllerHoverHighlight::trackingHatMeshRenderer
	MeshRenderer_t1268241104 * ___trackingHatMeshRenderer_6;
	// SteamVR_RenderModel Valve.VR.InteractionSystem.ControllerHoverHighlight::renderModel
	SteamVR_RenderModel_t2905485978 * ___renderModel_7;
	// System.Boolean Valve.VR.InteractionSystem.ControllerHoverHighlight::renderModelLoaded
	bool ___renderModelLoaded_8;
	// SteamVR_Events/Action Valve.VR.InteractionSystem.ControllerHoverHighlight::renderModelLoadedAction
	Action_t1836998693 * ___renderModelLoadedAction_9;

public:
	inline static int32_t get_offset_of_highLightMaterial_2() { return static_cast<int32_t>(offsetof(ControllerHoverHighlight_t2644472, ___highLightMaterial_2)); }
	inline Material_t193706927 * get_highLightMaterial_2() const { return ___highLightMaterial_2; }
	inline Material_t193706927 ** get_address_of_highLightMaterial_2() { return &___highLightMaterial_2; }
	inline void set_highLightMaterial_2(Material_t193706927 * value)
	{
		___highLightMaterial_2 = value;
		Il2CppCodeGenWriteBarrier(&___highLightMaterial_2, value);
	}

	inline static int32_t get_offset_of_fireHapticsOnHightlight_3() { return static_cast<int32_t>(offsetof(ControllerHoverHighlight_t2644472, ___fireHapticsOnHightlight_3)); }
	inline bool get_fireHapticsOnHightlight_3() const { return ___fireHapticsOnHightlight_3; }
	inline bool* get_address_of_fireHapticsOnHightlight_3() { return &___fireHapticsOnHightlight_3; }
	inline void set_fireHapticsOnHightlight_3(bool value)
	{
		___fireHapticsOnHightlight_3 = value;
	}

	inline static int32_t get_offset_of_hand_4() { return static_cast<int32_t>(offsetof(ControllerHoverHighlight_t2644472, ___hand_4)); }
	inline Hand_t379716353 * get_hand_4() const { return ___hand_4; }
	inline Hand_t379716353 ** get_address_of_hand_4() { return &___hand_4; }
	inline void set_hand_4(Hand_t379716353 * value)
	{
		___hand_4 = value;
		Il2CppCodeGenWriteBarrier(&___hand_4, value);
	}

	inline static int32_t get_offset_of_bodyMeshRenderer_5() { return static_cast<int32_t>(offsetof(ControllerHoverHighlight_t2644472, ___bodyMeshRenderer_5)); }
	inline MeshRenderer_t1268241104 * get_bodyMeshRenderer_5() const { return ___bodyMeshRenderer_5; }
	inline MeshRenderer_t1268241104 ** get_address_of_bodyMeshRenderer_5() { return &___bodyMeshRenderer_5; }
	inline void set_bodyMeshRenderer_5(MeshRenderer_t1268241104 * value)
	{
		___bodyMeshRenderer_5 = value;
		Il2CppCodeGenWriteBarrier(&___bodyMeshRenderer_5, value);
	}

	inline static int32_t get_offset_of_trackingHatMeshRenderer_6() { return static_cast<int32_t>(offsetof(ControllerHoverHighlight_t2644472, ___trackingHatMeshRenderer_6)); }
	inline MeshRenderer_t1268241104 * get_trackingHatMeshRenderer_6() const { return ___trackingHatMeshRenderer_6; }
	inline MeshRenderer_t1268241104 ** get_address_of_trackingHatMeshRenderer_6() { return &___trackingHatMeshRenderer_6; }
	inline void set_trackingHatMeshRenderer_6(MeshRenderer_t1268241104 * value)
	{
		___trackingHatMeshRenderer_6 = value;
		Il2CppCodeGenWriteBarrier(&___trackingHatMeshRenderer_6, value);
	}

	inline static int32_t get_offset_of_renderModel_7() { return static_cast<int32_t>(offsetof(ControllerHoverHighlight_t2644472, ___renderModel_7)); }
	inline SteamVR_RenderModel_t2905485978 * get_renderModel_7() const { return ___renderModel_7; }
	inline SteamVR_RenderModel_t2905485978 ** get_address_of_renderModel_7() { return &___renderModel_7; }
	inline void set_renderModel_7(SteamVR_RenderModel_t2905485978 * value)
	{
		___renderModel_7 = value;
		Il2CppCodeGenWriteBarrier(&___renderModel_7, value);
	}

	inline static int32_t get_offset_of_renderModelLoaded_8() { return static_cast<int32_t>(offsetof(ControllerHoverHighlight_t2644472, ___renderModelLoaded_8)); }
	inline bool get_renderModelLoaded_8() const { return ___renderModelLoaded_8; }
	inline bool* get_address_of_renderModelLoaded_8() { return &___renderModelLoaded_8; }
	inline void set_renderModelLoaded_8(bool value)
	{
		___renderModelLoaded_8 = value;
	}

	inline static int32_t get_offset_of_renderModelLoadedAction_9() { return static_cast<int32_t>(offsetof(ControllerHoverHighlight_t2644472, ___renderModelLoadedAction_9)); }
	inline Action_t1836998693 * get_renderModelLoadedAction_9() const { return ___renderModelLoadedAction_9; }
	inline Action_t1836998693 ** get_address_of_renderModelLoadedAction_9() { return &___renderModelLoadedAction_9; }
	inline void set_renderModelLoadedAction_9(Action_t1836998693 * value)
	{
		___renderModelLoadedAction_9 = value;
		Il2CppCodeGenWriteBarrier(&___renderModelLoadedAction_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
