﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter
struct VRTK_MaterialColorSwapHighlighter_t2305160438;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter/<CycleColor>c__Iterator0
struct  U3CCycleColorU3Ec__Iterator0_t125975806  : public Il2CppObject
{
public:
	// System.Single VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter/<CycleColor>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_0;
	// System.Single VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter/<CycleColor>c__Iterator0::duration
	float ___duration_1;
	// UnityEngine.Material VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter/<CycleColor>c__Iterator0::material
	Material_t193706927 * ___material_2;
	// UnityEngine.Color VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter/<CycleColor>c__Iterator0::startColor
	Color_t2020392075  ___startColor_3;
	// UnityEngine.Color VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter/<CycleColor>c__Iterator0::endColor
	Color_t2020392075  ___endColor_4;
	// VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter/<CycleColor>c__Iterator0::$this
	VRTK_MaterialColorSwapHighlighter_t2305160438 * ___U24this_5;
	// System.Object VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter/<CycleColor>c__Iterator0::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter/<CycleColor>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 VRTK.Highlighters.VRTK_MaterialColorSwapHighlighter/<CycleColor>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCycleColorU3Ec__Iterator0_t125975806, ___U3CelapsedTimeU3E__0_0)); }
	inline float get_U3CelapsedTimeU3E__0_0() const { return ___U3CelapsedTimeU3E__0_0; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_0() { return &___U3CelapsedTimeU3E__0_0; }
	inline void set_U3CelapsedTimeU3E__0_0(float value)
	{
		___U3CelapsedTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_duration_1() { return static_cast<int32_t>(offsetof(U3CCycleColorU3Ec__Iterator0_t125975806, ___duration_1)); }
	inline float get_duration_1() const { return ___duration_1; }
	inline float* get_address_of_duration_1() { return &___duration_1; }
	inline void set_duration_1(float value)
	{
		___duration_1 = value;
	}

	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(U3CCycleColorU3Ec__Iterator0_t125975806, ___material_2)); }
	inline Material_t193706927 * get_material_2() const { return ___material_2; }
	inline Material_t193706927 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_t193706927 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier(&___material_2, value);
	}

	inline static int32_t get_offset_of_startColor_3() { return static_cast<int32_t>(offsetof(U3CCycleColorU3Ec__Iterator0_t125975806, ___startColor_3)); }
	inline Color_t2020392075  get_startColor_3() const { return ___startColor_3; }
	inline Color_t2020392075 * get_address_of_startColor_3() { return &___startColor_3; }
	inline void set_startColor_3(Color_t2020392075  value)
	{
		___startColor_3 = value;
	}

	inline static int32_t get_offset_of_endColor_4() { return static_cast<int32_t>(offsetof(U3CCycleColorU3Ec__Iterator0_t125975806, ___endColor_4)); }
	inline Color_t2020392075  get_endColor_4() const { return ___endColor_4; }
	inline Color_t2020392075 * get_address_of_endColor_4() { return &___endColor_4; }
	inline void set_endColor_4(Color_t2020392075  value)
	{
		___endColor_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CCycleColorU3Ec__Iterator0_t125975806, ___U24this_5)); }
	inline VRTK_MaterialColorSwapHighlighter_t2305160438 * get_U24this_5() const { return ___U24this_5; }
	inline VRTK_MaterialColorSwapHighlighter_t2305160438 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(VRTK_MaterialColorSwapHighlighter_t2305160438 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CCycleColorU3Ec__Iterator0_t125975806, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CCycleColorU3Ec__Iterator0_t125975806, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CCycleColorU3Ec__Iterator0_t125975806, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
