﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRTrackedCamera
struct IVRTrackedCamera_t2005230018;
struct IVRTrackedCamera_t2005230018_marshaled_pinvoke;
struct IVRTrackedCamera_t2005230018_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IVRTrackedCamera_t2005230018;
struct IVRTrackedCamera_t2005230018_marshaled_pinvoke;

extern "C" void IVRTrackedCamera_t2005230018_marshal_pinvoke(const IVRTrackedCamera_t2005230018& unmarshaled, IVRTrackedCamera_t2005230018_marshaled_pinvoke& marshaled);
extern "C" void IVRTrackedCamera_t2005230018_marshal_pinvoke_back(const IVRTrackedCamera_t2005230018_marshaled_pinvoke& marshaled, IVRTrackedCamera_t2005230018& unmarshaled);
extern "C" void IVRTrackedCamera_t2005230018_marshal_pinvoke_cleanup(IVRTrackedCamera_t2005230018_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IVRTrackedCamera_t2005230018;
struct IVRTrackedCamera_t2005230018_marshaled_com;

extern "C" void IVRTrackedCamera_t2005230018_marshal_com(const IVRTrackedCamera_t2005230018& unmarshaled, IVRTrackedCamera_t2005230018_marshaled_com& marshaled);
extern "C" void IVRTrackedCamera_t2005230018_marshal_com_back(const IVRTrackedCamera_t2005230018_marshaled_com& marshaled, IVRTrackedCamera_t2005230018& unmarshaled);
extern "C" void IVRTrackedCamera_t2005230018_marshal_com_cleanup(IVRTrackedCamera_t2005230018_marshaled_com& marshaled);
