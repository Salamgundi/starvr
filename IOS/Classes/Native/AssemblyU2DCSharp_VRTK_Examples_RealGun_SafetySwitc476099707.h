﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.RealGun_SafetySwitch
struct  RealGun_SafetySwitch_t476099707  : public VRTK_InteractableObject_t2604188111
{
public:
	// System.Boolean VRTK.Examples.RealGun_SafetySwitch::safetyOff
	bool ___safetyOff_45;
	// System.Single VRTK.Examples.RealGun_SafetySwitch::offAngle
	float ___offAngle_46;
	// UnityEngine.Vector3 VRTK.Examples.RealGun_SafetySwitch::fixedPosition
	Vector3_t2243707580  ___fixedPosition_47;

public:
	inline static int32_t get_offset_of_safetyOff_45() { return static_cast<int32_t>(offsetof(RealGun_SafetySwitch_t476099707, ___safetyOff_45)); }
	inline bool get_safetyOff_45() const { return ___safetyOff_45; }
	inline bool* get_address_of_safetyOff_45() { return &___safetyOff_45; }
	inline void set_safetyOff_45(bool value)
	{
		___safetyOff_45 = value;
	}

	inline static int32_t get_offset_of_offAngle_46() { return static_cast<int32_t>(offsetof(RealGun_SafetySwitch_t476099707, ___offAngle_46)); }
	inline float get_offAngle_46() const { return ___offAngle_46; }
	inline float* get_address_of_offAngle_46() { return &___offAngle_46; }
	inline void set_offAngle_46(float value)
	{
		___offAngle_46 = value;
	}

	inline static int32_t get_offset_of_fixedPosition_47() { return static_cast<int32_t>(offsetof(RealGun_SafetySwitch_t476099707, ___fixedPosition_47)); }
	inline Vector3_t2243707580  get_fixedPosition_47() const { return ___fixedPosition_47; }
	inline Vector3_t2243707580 * get_address_of_fixedPosition_47() { return &___fixedPosition_47; }
	inline void set_fixedPosition_47(Vector3_t2243707580  value)
	{
		___fixedPosition_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
