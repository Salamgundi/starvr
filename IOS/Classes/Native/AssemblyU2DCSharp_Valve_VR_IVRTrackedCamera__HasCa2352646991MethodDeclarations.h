﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRTrackedCamera/_HasCamera
struct _HasCamera_t2352646991;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraError3529690400.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRTrackedCamera/_HasCamera::.ctor(System.Object,System.IntPtr)
extern "C"  void _HasCamera__ctor_m3715055958 (_HasCamera_t2352646991 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_HasCamera::Invoke(System.UInt32,System.Boolean&)
extern "C"  int32_t _HasCamera_Invoke_m1995214122 (_HasCamera_t2352646991 * __this, uint32_t ___nDeviceIndex0, bool* ___pHasCamera1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRTrackedCamera/_HasCamera::BeginInvoke(System.UInt32,System.Boolean&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _HasCamera_BeginInvoke_m950163256 (_HasCamera_t2352646991 * __this, uint32_t ___nDeviceIndex0, bool* ___pHasCamera1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_HasCamera::EndInvoke(System.Boolean&,System.IAsyncResult)
extern "C"  int32_t _HasCamera_EndInvoke_m3171483266 (_HasCamera_t2352646991 * __this, bool* ___pHasCamera0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
