﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Action_1_gen4147827880MethodDeclarations.h"

// System.Void SteamVR_Events/Action`1<Valve.VR.TrackedDevicePose_t[]>::.ctor(SteamVR_Events/Event`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define Action_1__ctor_m3578992051(__this, ____event0, ___action1, method) ((  void (*) (Action_1_t60683338 *, Event_1_t777727170 *, UnityAction_1_t4263857800 *, const MethodInfo*))Action_1__ctor_m3060490555_gshared)(__this, ____event0, ___action1, method)
// System.Void SteamVR_Events/Action`1<Valve.VR.TrackedDevicePose_t[]>::Enable(System.Boolean)
#define Action_1_Enable_m3913871383(__this, ___enabled0, method) ((  void (*) (Action_1_t60683338 *, bool, const MethodInfo*))Action_1_Enable_m3823038047_gshared)(__this, ___enabled0, method)
