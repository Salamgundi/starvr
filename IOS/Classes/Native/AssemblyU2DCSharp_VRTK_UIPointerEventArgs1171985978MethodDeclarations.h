﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UIPointerEventArgs
struct UIPointerEventArgs_t1171985978;
struct UIPointerEventArgs_t1171985978_marshaled_pinvoke;
struct UIPointerEventArgs_t1171985978_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct UIPointerEventArgs_t1171985978;
struct UIPointerEventArgs_t1171985978_marshaled_pinvoke;

extern "C" void UIPointerEventArgs_t1171985978_marshal_pinvoke(const UIPointerEventArgs_t1171985978& unmarshaled, UIPointerEventArgs_t1171985978_marshaled_pinvoke& marshaled);
extern "C" void UIPointerEventArgs_t1171985978_marshal_pinvoke_back(const UIPointerEventArgs_t1171985978_marshaled_pinvoke& marshaled, UIPointerEventArgs_t1171985978& unmarshaled);
extern "C" void UIPointerEventArgs_t1171985978_marshal_pinvoke_cleanup(UIPointerEventArgs_t1171985978_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct UIPointerEventArgs_t1171985978;
struct UIPointerEventArgs_t1171985978_marshaled_com;

extern "C" void UIPointerEventArgs_t1171985978_marshal_com(const UIPointerEventArgs_t1171985978& unmarshaled, UIPointerEventArgs_t1171985978_marshaled_com& marshaled);
extern "C" void UIPointerEventArgs_t1171985978_marshal_com_back(const UIPointerEventArgs_t1171985978_marshaled_com& marshaled, UIPointerEventArgs_t1171985978& unmarshaled);
extern "C" void UIPointerEventArgs_t1171985978_marshal_com_cleanup(UIPointerEventArgs_t1171985978_marshaled_com& marshaled);
