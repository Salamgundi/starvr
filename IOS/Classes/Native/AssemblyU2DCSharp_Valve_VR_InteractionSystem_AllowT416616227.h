﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.AllowTeleportWhileAttachedToHand
struct  AllowTeleportWhileAttachedToHand_t416616227  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Valve.VR.InteractionSystem.AllowTeleportWhileAttachedToHand::teleportAllowed
	bool ___teleportAllowed_2;
	// System.Boolean Valve.VR.InteractionSystem.AllowTeleportWhileAttachedToHand::overrideHoverLock
	bool ___overrideHoverLock_3;

public:
	inline static int32_t get_offset_of_teleportAllowed_2() { return static_cast<int32_t>(offsetof(AllowTeleportWhileAttachedToHand_t416616227, ___teleportAllowed_2)); }
	inline bool get_teleportAllowed_2() const { return ___teleportAllowed_2; }
	inline bool* get_address_of_teleportAllowed_2() { return &___teleportAllowed_2; }
	inline void set_teleportAllowed_2(bool value)
	{
		___teleportAllowed_2 = value;
	}

	inline static int32_t get_offset_of_overrideHoverLock_3() { return static_cast<int32_t>(offsetof(AllowTeleportWhileAttachedToHand_t416616227, ___overrideHoverLock_3)); }
	inline bool get_overrideHoverLock_3() const { return ___overrideHoverLock_3; }
	inline bool* get_address_of_overrideHoverLock_3() { return &___overrideHoverLock_3; }
	inline void set_overrideHoverLock_3(bool value)
	{
		___overrideHoverLock_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
