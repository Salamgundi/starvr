﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.IVRSystem/_GetControllerState
struct _GetControllerState_t3891090487;
// Valve.VR.CVRSystem/_GetControllerStatePacked
struct _GetControllerStatePacked_t385670509;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.CVRSystem/GetControllerStateUnion
struct  GetControllerStateUnion_t1135918473 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// Valve.VR.IVRSystem/_GetControllerState Valve.VR.CVRSystem/GetControllerStateUnion::pGetControllerState
			_GetControllerState_t3891090487 * ___pGetControllerState_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			_GetControllerState_t3891090487 * ___pGetControllerState_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// Valve.VR.CVRSystem/_GetControllerStatePacked Valve.VR.CVRSystem/GetControllerStateUnion::pGetControllerStatePacked
			_GetControllerStatePacked_t385670509 * ___pGetControllerStatePacked_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			_GetControllerStatePacked_t385670509 * ___pGetControllerStatePacked_1_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_pGetControllerState_0() { return static_cast<int32_t>(offsetof(GetControllerStateUnion_t1135918473, ___pGetControllerState_0)); }
	inline _GetControllerState_t3891090487 * get_pGetControllerState_0() const { return ___pGetControllerState_0; }
	inline _GetControllerState_t3891090487 ** get_address_of_pGetControllerState_0() { return &___pGetControllerState_0; }
	inline void set_pGetControllerState_0(_GetControllerState_t3891090487 * value)
	{
		___pGetControllerState_0 = value;
		Il2CppCodeGenWriteBarrier(&___pGetControllerState_0, value);
	}

	inline static int32_t get_offset_of_pGetControllerStatePacked_1() { return static_cast<int32_t>(offsetof(GetControllerStateUnion_t1135918473, ___pGetControllerStatePacked_1)); }
	inline _GetControllerStatePacked_t385670509 * get_pGetControllerStatePacked_1() const { return ___pGetControllerStatePacked_1; }
	inline _GetControllerStatePacked_t385670509 ** get_address_of_pGetControllerStatePacked_1() { return &___pGetControllerStatePacked_1; }
	inline void set_pGetControllerStatePacked_1(_GetControllerStatePacked_t385670509 * value)
	{
		___pGetControllerStatePacked_1 = value;
		Il2CppCodeGenWriteBarrier(&___pGetControllerStatePacked_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Valve.VR.CVRSystem/GetControllerStateUnion
struct GetControllerStateUnion_t1135918473_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			Il2CppMethodPointer ___pGetControllerState_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppMethodPointer ___pGetControllerState_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Il2CppMethodPointer ___pGetControllerStatePacked_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppMethodPointer ___pGetControllerStatePacked_1_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of Valve.VR.CVRSystem/GetControllerStateUnion
struct GetControllerStateUnion_t1135918473_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			Il2CppMethodPointer ___pGetControllerState_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppMethodPointer ___pGetControllerState_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Il2CppMethodPointer ___pGetControllerStatePacked_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppMethodPointer ___pGetControllerStatePacked_1_forAlignmentOnly;
		};
	};
};
