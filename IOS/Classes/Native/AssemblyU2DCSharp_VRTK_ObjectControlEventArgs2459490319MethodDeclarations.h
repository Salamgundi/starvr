﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.ObjectControlEventArgs
struct ObjectControlEventArgs_t2459490319;
struct ObjectControlEventArgs_t2459490319_marshaled_pinvoke;
struct ObjectControlEventArgs_t2459490319_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ObjectControlEventArgs_t2459490319;
struct ObjectControlEventArgs_t2459490319_marshaled_pinvoke;

extern "C" void ObjectControlEventArgs_t2459490319_marshal_pinvoke(const ObjectControlEventArgs_t2459490319& unmarshaled, ObjectControlEventArgs_t2459490319_marshaled_pinvoke& marshaled);
extern "C" void ObjectControlEventArgs_t2459490319_marshal_pinvoke_back(const ObjectControlEventArgs_t2459490319_marshaled_pinvoke& marshaled, ObjectControlEventArgs_t2459490319& unmarshaled);
extern "C" void ObjectControlEventArgs_t2459490319_marshal_pinvoke_cleanup(ObjectControlEventArgs_t2459490319_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ObjectControlEventArgs_t2459490319;
struct ObjectControlEventArgs_t2459490319_marshaled_com;

extern "C" void ObjectControlEventArgs_t2459490319_marshal_com(const ObjectControlEventArgs_t2459490319& unmarshaled, ObjectControlEventArgs_t2459490319_marshaled_com& marshaled);
extern "C" void ObjectControlEventArgs_t2459490319_marshal_com_back(const ObjectControlEventArgs_t2459490319_marshaled_com& marshaled, ObjectControlEventArgs_t2459490319& unmarshaled);
extern "C" void ObjectControlEventArgs_t2459490319_marshal_com_cleanup(ObjectControlEventArgs_t2459490319_marshaled_com& marshaled);
