﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.ConfigurableJoint
struct ConfigurableJoint_t454307495;

#include "AssemblyU2DCSharp_VRTK_VRTK_Control651619021.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Knob_KnobDirection4203016241.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Knob
struct  VRTK_Knob_t1401139190  : public VRTK_Control_t651619021
{
public:
	// UnityEngine.GameObject VRTK.VRTK_Knob::connectedTo
	GameObject_t1756533147 * ___connectedTo_15;
	// VRTK.VRTK_Knob/KnobDirection VRTK.VRTK_Knob::direction
	int32_t ___direction_16;
	// System.Single VRTK.VRTK_Knob::min
	float ___min_17;
	// System.Single VRTK.VRTK_Knob::max
	float ___max_18;
	// System.Single VRTK.VRTK_Knob::stepSize
	float ___stepSize_19;
	// VRTK.VRTK_Knob/KnobDirection VRTK.VRTK_Knob::finalDirection
	int32_t ___finalDirection_21;
	// VRTK.VRTK_Knob/KnobDirection VRTK.VRTK_Knob::subDirection
	int32_t ___subDirection_22;
	// System.Boolean VRTK.VRTK_Knob::subDirectionFound
	bool ___subDirectionFound_23;
	// UnityEngine.Quaternion VRTK.VRTK_Knob::initialRotation
	Quaternion_t4030073918  ___initialRotation_24;
	// UnityEngine.Vector3 VRTK.VRTK_Knob::initialLocalRotation
	Vector3_t2243707580  ___initialLocalRotation_25;
	// UnityEngine.ConfigurableJoint VRTK.VRTK_Knob::knobJoint
	ConfigurableJoint_t454307495 * ___knobJoint_26;
	// System.Boolean VRTK.VRTK_Knob::knobJointCreated
	bool ___knobJointCreated_27;

public:
	inline static int32_t get_offset_of_connectedTo_15() { return static_cast<int32_t>(offsetof(VRTK_Knob_t1401139190, ___connectedTo_15)); }
	inline GameObject_t1756533147 * get_connectedTo_15() const { return ___connectedTo_15; }
	inline GameObject_t1756533147 ** get_address_of_connectedTo_15() { return &___connectedTo_15; }
	inline void set_connectedTo_15(GameObject_t1756533147 * value)
	{
		___connectedTo_15 = value;
		Il2CppCodeGenWriteBarrier(&___connectedTo_15, value);
	}

	inline static int32_t get_offset_of_direction_16() { return static_cast<int32_t>(offsetof(VRTK_Knob_t1401139190, ___direction_16)); }
	inline int32_t get_direction_16() const { return ___direction_16; }
	inline int32_t* get_address_of_direction_16() { return &___direction_16; }
	inline void set_direction_16(int32_t value)
	{
		___direction_16 = value;
	}

	inline static int32_t get_offset_of_min_17() { return static_cast<int32_t>(offsetof(VRTK_Knob_t1401139190, ___min_17)); }
	inline float get_min_17() const { return ___min_17; }
	inline float* get_address_of_min_17() { return &___min_17; }
	inline void set_min_17(float value)
	{
		___min_17 = value;
	}

	inline static int32_t get_offset_of_max_18() { return static_cast<int32_t>(offsetof(VRTK_Knob_t1401139190, ___max_18)); }
	inline float get_max_18() const { return ___max_18; }
	inline float* get_address_of_max_18() { return &___max_18; }
	inline void set_max_18(float value)
	{
		___max_18 = value;
	}

	inline static int32_t get_offset_of_stepSize_19() { return static_cast<int32_t>(offsetof(VRTK_Knob_t1401139190, ___stepSize_19)); }
	inline float get_stepSize_19() const { return ___stepSize_19; }
	inline float* get_address_of_stepSize_19() { return &___stepSize_19; }
	inline void set_stepSize_19(float value)
	{
		___stepSize_19 = value;
	}

	inline static int32_t get_offset_of_finalDirection_21() { return static_cast<int32_t>(offsetof(VRTK_Knob_t1401139190, ___finalDirection_21)); }
	inline int32_t get_finalDirection_21() const { return ___finalDirection_21; }
	inline int32_t* get_address_of_finalDirection_21() { return &___finalDirection_21; }
	inline void set_finalDirection_21(int32_t value)
	{
		___finalDirection_21 = value;
	}

	inline static int32_t get_offset_of_subDirection_22() { return static_cast<int32_t>(offsetof(VRTK_Knob_t1401139190, ___subDirection_22)); }
	inline int32_t get_subDirection_22() const { return ___subDirection_22; }
	inline int32_t* get_address_of_subDirection_22() { return &___subDirection_22; }
	inline void set_subDirection_22(int32_t value)
	{
		___subDirection_22 = value;
	}

	inline static int32_t get_offset_of_subDirectionFound_23() { return static_cast<int32_t>(offsetof(VRTK_Knob_t1401139190, ___subDirectionFound_23)); }
	inline bool get_subDirectionFound_23() const { return ___subDirectionFound_23; }
	inline bool* get_address_of_subDirectionFound_23() { return &___subDirectionFound_23; }
	inline void set_subDirectionFound_23(bool value)
	{
		___subDirectionFound_23 = value;
	}

	inline static int32_t get_offset_of_initialRotation_24() { return static_cast<int32_t>(offsetof(VRTK_Knob_t1401139190, ___initialRotation_24)); }
	inline Quaternion_t4030073918  get_initialRotation_24() const { return ___initialRotation_24; }
	inline Quaternion_t4030073918 * get_address_of_initialRotation_24() { return &___initialRotation_24; }
	inline void set_initialRotation_24(Quaternion_t4030073918  value)
	{
		___initialRotation_24 = value;
	}

	inline static int32_t get_offset_of_initialLocalRotation_25() { return static_cast<int32_t>(offsetof(VRTK_Knob_t1401139190, ___initialLocalRotation_25)); }
	inline Vector3_t2243707580  get_initialLocalRotation_25() const { return ___initialLocalRotation_25; }
	inline Vector3_t2243707580 * get_address_of_initialLocalRotation_25() { return &___initialLocalRotation_25; }
	inline void set_initialLocalRotation_25(Vector3_t2243707580  value)
	{
		___initialLocalRotation_25 = value;
	}

	inline static int32_t get_offset_of_knobJoint_26() { return static_cast<int32_t>(offsetof(VRTK_Knob_t1401139190, ___knobJoint_26)); }
	inline ConfigurableJoint_t454307495 * get_knobJoint_26() const { return ___knobJoint_26; }
	inline ConfigurableJoint_t454307495 ** get_address_of_knobJoint_26() { return &___knobJoint_26; }
	inline void set_knobJoint_26(ConfigurableJoint_t454307495 * value)
	{
		___knobJoint_26 = value;
		Il2CppCodeGenWriteBarrier(&___knobJoint_26, value);
	}

	inline static int32_t get_offset_of_knobJointCreated_27() { return static_cast<int32_t>(offsetof(VRTK_Knob_t1401139190, ___knobJointCreated_27)); }
	inline bool get_knobJointCreated_27() const { return ___knobJointCreated_27; }
	inline bool* get_address_of_knobJointCreated_27() { return &___knobJointCreated_27; }
	inline void set_knobJointCreated_27(bool value)
	{
		___knobJointCreated_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
