﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSettings/_GetFloat
struct _GetFloat_t3725422668;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRSettingsError4124928198.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSettings/_GetFloat::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetFloat__ctor_m511259393 (_GetFloat_t3725422668 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.IVRSettings/_GetFloat::Invoke(System.String,System.String,Valve.VR.EVRSettingsError&)
extern "C"  float _GetFloat_Invoke_m2540038257 (_GetFloat_t3725422668 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSettings/_GetFloat::BeginInvoke(System.String,System.String,Valve.VR.EVRSettingsError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetFloat_BeginInvoke_m1143133072 (_GetFloat_t3725422668 * __this, String_t* ___pchSection0, String_t* ___pchSettingsKey1, int32_t* ___peError2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.IVRSettings/_GetFloat::EndInvoke(Valve.VR.EVRSettingsError&,System.IAsyncResult)
extern "C"  float _GetFloat_EndInvoke_m379496283 (_GetFloat_t3725422668 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
