﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.Examples.Archery.BowAim
struct BowAim_t1262532443;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Archery.BowHandle
struct  BowHandle_t1058184516  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform VRTK.Examples.Archery.BowHandle::arrowNockingPoint
	Transform_t3275118058 * ___arrowNockingPoint_2;
	// VRTK.Examples.Archery.BowAim VRTK.Examples.Archery.BowHandle::aim
	BowAim_t1262532443 * ___aim_3;
	// UnityEngine.Transform VRTK.Examples.Archery.BowHandle::nockSide
	Transform_t3275118058 * ___nockSide_4;

public:
	inline static int32_t get_offset_of_arrowNockingPoint_2() { return static_cast<int32_t>(offsetof(BowHandle_t1058184516, ___arrowNockingPoint_2)); }
	inline Transform_t3275118058 * get_arrowNockingPoint_2() const { return ___arrowNockingPoint_2; }
	inline Transform_t3275118058 ** get_address_of_arrowNockingPoint_2() { return &___arrowNockingPoint_2; }
	inline void set_arrowNockingPoint_2(Transform_t3275118058 * value)
	{
		___arrowNockingPoint_2 = value;
		Il2CppCodeGenWriteBarrier(&___arrowNockingPoint_2, value);
	}

	inline static int32_t get_offset_of_aim_3() { return static_cast<int32_t>(offsetof(BowHandle_t1058184516, ___aim_3)); }
	inline BowAim_t1262532443 * get_aim_3() const { return ___aim_3; }
	inline BowAim_t1262532443 ** get_address_of_aim_3() { return &___aim_3; }
	inline void set_aim_3(BowAim_t1262532443 * value)
	{
		___aim_3 = value;
		Il2CppCodeGenWriteBarrier(&___aim_3, value);
	}

	inline static int32_t get_offset_of_nockSide_4() { return static_cast<int32_t>(offsetof(BowHandle_t1058184516, ___nockSide_4)); }
	inline Transform_t3275118058 * get_nockSide_4() const { return ___nockSide_4; }
	inline Transform_t3275118058 ** get_address_of_nockSide_4() { return &___nockSide_4; }
	inline void set_nockSide_4(Transform_t3275118058 * value)
	{
		___nockSide_4 = value;
		Il2CppCodeGenWriteBarrier(&___nockSide_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
