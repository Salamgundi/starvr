﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter
struct VRTK_OutlineObjectCopyHighlighter_t668900239;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen283458390.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"

// System.Void VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::.ctor()
extern "C"  void VRTK_OutlineObjectCopyHighlighter__ctor_m952800359 (VRTK_OutlineObjectCopyHighlighter_t668900239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::Initialise(System.Nullable`1<UnityEngine.Color>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void VRTK_OutlineObjectCopyHighlighter_Initialise_m1941750586 (VRTK_OutlineObjectCopyHighlighter_t668900239 * __this, Nullable_1_t283458390  ___color0, Dictionary_2_t309261261 * ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::ResetHighlighter()
extern "C"  void VRTK_OutlineObjectCopyHighlighter_ResetHighlighter_m646805369 (VRTK_OutlineObjectCopyHighlighter_t668900239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::Highlight(System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_OutlineObjectCopyHighlighter_Highlight_m3171397561 (VRTK_OutlineObjectCopyHighlighter_t668900239 * __this, Nullable_1_t283458390  ___color0, float ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::Unhighlight(System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_OutlineObjectCopyHighlighter_Unhighlight_m646073264 (VRTK_OutlineObjectCopyHighlighter_t668900239 * __this, Nullable_1_t283458390  ___color0, float ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::OnEnable()
extern "C"  void VRTK_OutlineObjectCopyHighlighter_OnEnable_m3620829715 (VRTK_OutlineObjectCopyHighlighter_t668900239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::OnDestroy()
extern "C"  void VRTK_OutlineObjectCopyHighlighter_OnDestroy_m1014884828 (VRTK_OutlineObjectCopyHighlighter_t668900239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::ResetHighlighterWithCustomModels()
extern "C"  void VRTK_OutlineObjectCopyHighlighter_ResetHighlighterWithCustomModels_m4094121586 (VRTK_OutlineObjectCopyHighlighter_t668900239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::ResetHighlighterWithCustomModelPaths()
extern "C"  void VRTK_OutlineObjectCopyHighlighter_ResetHighlighterWithCustomModelPaths_m960739609 (VRTK_OutlineObjectCopyHighlighter_t668900239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::ResetHighlightersWithCurrentGameObject()
extern "C"  void VRTK_OutlineObjectCopyHighlighter_ResetHighlightersWithCurrentGameObject_m3768782212 (VRTK_OutlineObjectCopyHighlighter_t668900239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::SetOptions(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void VRTK_OutlineObjectCopyHighlighter_SetOptions_m1164261164 (VRTK_OutlineObjectCopyHighlighter_t668900239 * __this, Dictionary_2_t309261261 * ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::DeleteExistingHighlightModels()
extern "C"  void VRTK_OutlineObjectCopyHighlighter_DeleteExistingHighlightModels_m3572707001 (VRTK_OutlineObjectCopyHighlighter_t668900239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.Highlighters.VRTK_OutlineObjectCopyHighlighter::CreateHighlightModel(UnityEngine.GameObject,System.String)
extern "C"  GameObject_t1756533147 * VRTK_OutlineObjectCopyHighlighter_CreateHighlightModel_m1307247201 (VRTK_OutlineObjectCopyHighlighter_t668900239 * __this, GameObject_t1756533147 * ___givenOutlineModel0, String_t* ___givenOutlineModelPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
