﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_Control/ValueChangedEvent
struct ValueChangedEvent_t4219324264;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Control/DefaultControlEvents
struct  DefaultControlEvents_t3441053308  : public Il2CppObject
{
public:
	// VRTK.VRTK_Control/ValueChangedEvent VRTK.VRTK_Control/DefaultControlEvents::OnValueChanged
	ValueChangedEvent_t4219324264 * ___OnValueChanged_0;

public:
	inline static int32_t get_offset_of_OnValueChanged_0() { return static_cast<int32_t>(offsetof(DefaultControlEvents_t3441053308, ___OnValueChanged_0)); }
	inline ValueChangedEvent_t4219324264 * get_OnValueChanged_0() const { return ___OnValueChanged_0; }
	inline ValueChangedEvent_t4219324264 ** get_address_of_OnValueChanged_0() { return &___OnValueChanged_0; }
	inline void set_OnValueChanged_0(ValueChangedEvent_t4219324264 * value)
	{
		___OnValueChanged_0 = value;
		Il2CppCodeGenWriteBarrier(&___OnValueChanged_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
