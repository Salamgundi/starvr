﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.IVRRenderModels/_GetComponentState
struct _GetComponentState_t742926735;
// Valve.VR.CVRRenderModels/_GetComponentStatePacked
struct _GetComponentStatePacked_t3386937929;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.CVRRenderModels/GetComponentStateUnion
struct  GetComponentStateUnion_t3431133397 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// Valve.VR.IVRRenderModels/_GetComponentState Valve.VR.CVRRenderModels/GetComponentStateUnion::pGetComponentState
			_GetComponentState_t742926735 * ___pGetComponentState_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			_GetComponentState_t742926735 * ___pGetComponentState_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// Valve.VR.CVRRenderModels/_GetComponentStatePacked Valve.VR.CVRRenderModels/GetComponentStateUnion::pGetComponentStatePacked
			_GetComponentStatePacked_t3386937929 * ___pGetComponentStatePacked_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			_GetComponentStatePacked_t3386937929 * ___pGetComponentStatePacked_1_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_pGetComponentState_0() { return static_cast<int32_t>(offsetof(GetComponentStateUnion_t3431133397, ___pGetComponentState_0)); }
	inline _GetComponentState_t742926735 * get_pGetComponentState_0() const { return ___pGetComponentState_0; }
	inline _GetComponentState_t742926735 ** get_address_of_pGetComponentState_0() { return &___pGetComponentState_0; }
	inline void set_pGetComponentState_0(_GetComponentState_t742926735 * value)
	{
		___pGetComponentState_0 = value;
		Il2CppCodeGenWriteBarrier(&___pGetComponentState_0, value);
	}

	inline static int32_t get_offset_of_pGetComponentStatePacked_1() { return static_cast<int32_t>(offsetof(GetComponentStateUnion_t3431133397, ___pGetComponentStatePacked_1)); }
	inline _GetComponentStatePacked_t3386937929 * get_pGetComponentStatePacked_1() const { return ___pGetComponentStatePacked_1; }
	inline _GetComponentStatePacked_t3386937929 ** get_address_of_pGetComponentStatePacked_1() { return &___pGetComponentStatePacked_1; }
	inline void set_pGetComponentStatePacked_1(_GetComponentStatePacked_t3386937929 * value)
	{
		___pGetComponentStatePacked_1 = value;
		Il2CppCodeGenWriteBarrier(&___pGetComponentStatePacked_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Valve.VR.CVRRenderModels/GetComponentStateUnion
struct GetComponentStateUnion_t3431133397_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			Il2CppMethodPointer ___pGetComponentState_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppMethodPointer ___pGetComponentState_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Il2CppMethodPointer ___pGetComponentStatePacked_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppMethodPointer ___pGetComponentStatePacked_1_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of Valve.VR.CVRRenderModels/GetComponentStateUnion
struct GetComponentStateUnion_t3431133397_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			Il2CppMethodPointer ___pGetComponentState_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppMethodPointer ___pGetComponentState_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			Il2CppMethodPointer ___pGetComponentStatePacked_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppMethodPointer ___pGetComponentStatePacked_1_forAlignmentOnly;
		};
	};
};
