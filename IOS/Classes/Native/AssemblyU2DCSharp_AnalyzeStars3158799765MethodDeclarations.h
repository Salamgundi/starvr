﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnalyzeStars
struct AnalyzeStars_t3158799765;

#include "codegen/il2cpp-codegen.h"

// System.Void AnalyzeStars::.ctor()
extern "C"  void AnalyzeStars__ctor_m95506420 (AnalyzeStars_t3158799765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyzeStars::Awake()
extern "C"  void AnalyzeStars_Awake_m3756221009 (AnalyzeStars_t3158799765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyzeStars::Start()
extern "C"  void AnalyzeStars_Start_m1898455316 (AnalyzeStars_t3158799765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
