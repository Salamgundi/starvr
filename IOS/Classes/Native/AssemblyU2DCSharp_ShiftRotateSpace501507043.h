﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// SteamVR_TrackedObject
struct SteamVR_TrackedObject_t2338458854;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShiftRotateSpace
struct  ShiftRotateSpace_t501507043  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ShiftRotateSpace::degrees
	float ___degrees_2;
	// System.Single ShiftRotateSpace::rotatespeed
	float ___rotatespeed_3;
	// System.Boolean ShiftRotateSpace::Paused
	bool ___Paused_4;
	// System.Boolean ShiftRotateSpace::North
	bool ___North_5;
	// UnityEngine.GameObject ShiftRotateSpace::Stars
	GameObject_t1756533147 * ___Stars_6;
	// SteamVR_TrackedObject ShiftRotateSpace::trackedObj
	SteamVR_TrackedObject_t2338458854 * ___trackedObj_7;

public:
	inline static int32_t get_offset_of_degrees_2() { return static_cast<int32_t>(offsetof(ShiftRotateSpace_t501507043, ___degrees_2)); }
	inline float get_degrees_2() const { return ___degrees_2; }
	inline float* get_address_of_degrees_2() { return &___degrees_2; }
	inline void set_degrees_2(float value)
	{
		___degrees_2 = value;
	}

	inline static int32_t get_offset_of_rotatespeed_3() { return static_cast<int32_t>(offsetof(ShiftRotateSpace_t501507043, ___rotatespeed_3)); }
	inline float get_rotatespeed_3() const { return ___rotatespeed_3; }
	inline float* get_address_of_rotatespeed_3() { return &___rotatespeed_3; }
	inline void set_rotatespeed_3(float value)
	{
		___rotatespeed_3 = value;
	}

	inline static int32_t get_offset_of_Paused_4() { return static_cast<int32_t>(offsetof(ShiftRotateSpace_t501507043, ___Paused_4)); }
	inline bool get_Paused_4() const { return ___Paused_4; }
	inline bool* get_address_of_Paused_4() { return &___Paused_4; }
	inline void set_Paused_4(bool value)
	{
		___Paused_4 = value;
	}

	inline static int32_t get_offset_of_North_5() { return static_cast<int32_t>(offsetof(ShiftRotateSpace_t501507043, ___North_5)); }
	inline bool get_North_5() const { return ___North_5; }
	inline bool* get_address_of_North_5() { return &___North_5; }
	inline void set_North_5(bool value)
	{
		___North_5 = value;
	}

	inline static int32_t get_offset_of_Stars_6() { return static_cast<int32_t>(offsetof(ShiftRotateSpace_t501507043, ___Stars_6)); }
	inline GameObject_t1756533147 * get_Stars_6() const { return ___Stars_6; }
	inline GameObject_t1756533147 ** get_address_of_Stars_6() { return &___Stars_6; }
	inline void set_Stars_6(GameObject_t1756533147 * value)
	{
		___Stars_6 = value;
		Il2CppCodeGenWriteBarrier(&___Stars_6, value);
	}

	inline static int32_t get_offset_of_trackedObj_7() { return static_cast<int32_t>(offsetof(ShiftRotateSpace_t501507043, ___trackedObj_7)); }
	inline SteamVR_TrackedObject_t2338458854 * get_trackedObj_7() const { return ___trackedObj_7; }
	inline SteamVR_TrackedObject_t2338458854 ** get_address_of_trackedObj_7() { return &___trackedObj_7; }
	inline void set_trackedObj_7(SteamVR_TrackedObject_t2338458854 * value)
	{
		___trackedObj_7 = value;
		Il2CppCodeGenWriteBarrier(&___trackedObj_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
