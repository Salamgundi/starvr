﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SwitchVideos/<SetActiveDelayed>c__Iterator0
struct U3CSetActiveDelayedU3Ec__Iterator0_t531920552;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SwitchVideos/<SetActiveDelayed>c__Iterator0::.ctor()
extern "C"  void U3CSetActiveDelayedU3Ec__Iterator0__ctor_m1846534601 (U3CSetActiveDelayedU3Ec__Iterator0_t531920552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SwitchVideos/<SetActiveDelayed>c__Iterator0::MoveNext()
extern "C"  bool U3CSetActiveDelayedU3Ec__Iterator0_MoveNext_m2100400895 (U3CSetActiveDelayedU3Ec__Iterator0_t531920552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SwitchVideos/<SetActiveDelayed>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSetActiveDelayedU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m300012835 (U3CSetActiveDelayedU3Ec__Iterator0_t531920552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SwitchVideos/<SetActiveDelayed>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSetActiveDelayedU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4213937355 (U3CSetActiveDelayedU3Ec__Iterator0_t531920552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchVideos/<SetActiveDelayed>c__Iterator0::Dispose()
extern "C"  void U3CSetActiveDelayedU3Ec__Iterator0_Dispose_m1359708250 (U3CSetActiveDelayedU3Ec__Iterator0_t531920552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchVideos/<SetActiveDelayed>c__Iterator0::Reset()
extern "C"  void U3CSetActiveDelayedU3Ec__Iterator0_Reset_m3603717004 (U3CSetActiveDelayedU3Ec__Iterator0_t531920552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
