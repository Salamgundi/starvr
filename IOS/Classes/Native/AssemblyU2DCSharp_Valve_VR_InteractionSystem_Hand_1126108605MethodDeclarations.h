﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.Hand/<DetachObject>c__AnonStorey1
struct U3CDetachObjectU3Ec__AnonStorey1_t1126108605;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1387717936.h"

// System.Void Valve.VR.InteractionSystem.Hand/<DetachObject>c__AnonStorey1::.ctor()
extern "C"  void U3CDetachObjectU3Ec__AnonStorey1__ctor_m2983854816 (U3CDetachObjectU3Ec__AnonStorey1_t1126108605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.Hand/<DetachObject>c__AnonStorey1::<>m__0(Valve.VR.InteractionSystem.Hand/AttachedObject)
extern "C"  bool U3CDetachObjectU3Ec__AnonStorey1_U3CU3Em__0_m4003199565 (U3CDetachObjectU3Ec__AnonStorey1_t1126108605 * __this, AttachedObject_t1387717936  ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
