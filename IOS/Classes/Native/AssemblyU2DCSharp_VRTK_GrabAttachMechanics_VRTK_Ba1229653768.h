﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Joint
struct Joint_t454317436;

#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Ba3487134318.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach
struct  VRTK_BaseJointGrabAttach_t1229653768  : public VRTK_BaseGrabAttach_t3487134318
{
public:
	// System.Boolean VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach::destroyImmediatelyOnThrow
	bool ___destroyImmediatelyOnThrow_18;
	// UnityEngine.Joint VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach::givenJoint
	Joint_t454317436 * ___givenJoint_19;
	// UnityEngine.Joint VRTK.GrabAttachMechanics.VRTK_BaseJointGrabAttach::controllerAttachJoint
	Joint_t454317436 * ___controllerAttachJoint_20;

public:
	inline static int32_t get_offset_of_destroyImmediatelyOnThrow_18() { return static_cast<int32_t>(offsetof(VRTK_BaseJointGrabAttach_t1229653768, ___destroyImmediatelyOnThrow_18)); }
	inline bool get_destroyImmediatelyOnThrow_18() const { return ___destroyImmediatelyOnThrow_18; }
	inline bool* get_address_of_destroyImmediatelyOnThrow_18() { return &___destroyImmediatelyOnThrow_18; }
	inline void set_destroyImmediatelyOnThrow_18(bool value)
	{
		___destroyImmediatelyOnThrow_18 = value;
	}

	inline static int32_t get_offset_of_givenJoint_19() { return static_cast<int32_t>(offsetof(VRTK_BaseJointGrabAttach_t1229653768, ___givenJoint_19)); }
	inline Joint_t454317436 * get_givenJoint_19() const { return ___givenJoint_19; }
	inline Joint_t454317436 ** get_address_of_givenJoint_19() { return &___givenJoint_19; }
	inline void set_givenJoint_19(Joint_t454317436 * value)
	{
		___givenJoint_19 = value;
		Il2CppCodeGenWriteBarrier(&___givenJoint_19, value);
	}

	inline static int32_t get_offset_of_controllerAttachJoint_20() { return static_cast<int32_t>(offsetof(VRTK_BaseJointGrabAttach_t1229653768, ___controllerAttachJoint_20)); }
	inline Joint_t454317436 * get_controllerAttachJoint_20() const { return ___controllerAttachJoint_20; }
	inline Joint_t454317436 ** get_address_of_controllerAttachJoint_20() { return &___controllerAttachJoint_20; }
	inline void set_controllerAttachJoint_20(Joint_t454317436 * value)
	{
		___controllerAttachJoint_20 = value;
		Il2CppCodeGenWriteBarrier(&___controllerAttachJoint_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
