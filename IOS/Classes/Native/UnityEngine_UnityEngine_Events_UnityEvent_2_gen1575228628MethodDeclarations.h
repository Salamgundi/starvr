﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetFadeEventArgs>
struct UnityEvent_2_t1575228628;
// UnityEngine.Events.UnityAction`2<System.Object,VRTK.HeadsetFadeEventArgs>
struct UnityAction_2_t3987998006;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetFadeEventArgs2892542019.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetFadeEventArgs>::.ctor()
extern "C"  void UnityEvent_2__ctor_m164610890_gshared (UnityEvent_2_t1575228628 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m164610890(__this, method) ((  void (*) (UnityEvent_2_t1575228628 *, const MethodInfo*))UnityEvent_2__ctor_m164610890_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetFadeEventArgs>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m655229032_gshared (UnityEvent_2_t1575228628 * __this, UnityAction_2_t3987998006 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m655229032(__this, ___call0, method) ((  void (*) (UnityEvent_2_t1575228628 *, UnityAction_2_t3987998006 *, const MethodInfo*))UnityEvent_2_AddListener_m655229032_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetFadeEventArgs>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m2940384593_gshared (UnityEvent_2_t1575228628 * __this, UnityAction_2_t3987998006 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m2940384593(__this, ___call0, method) ((  void (*) (UnityEvent_2_t1575228628 *, UnityAction_2_t3987998006 *, const MethodInfo*))UnityEvent_2_RemoveListener_m2940384593_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetFadeEventArgs>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m3965079088_gshared (UnityEvent_2_t1575228628 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m3965079088(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t1575228628 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m3965079088_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetFadeEventArgs>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m1217803492_gshared (UnityEvent_2_t1575228628 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m1217803492(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t1575228628 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m1217803492_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetFadeEventArgs>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m2000414049_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t3987998006 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m2000414049(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t3987998006 *, const MethodInfo*))UnityEvent_2_GetDelegate_m2000414049_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,VRTK.HeadsetFadeEventArgs>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m3781318923_gshared (UnityEvent_2_t1575228628 * __this, Il2CppObject * ___arg00, HeadsetFadeEventArgs_t2892542019  ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m3781318923(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t1575228628 *, Il2CppObject *, HeadsetFadeEventArgs_t2892542019 , const MethodInfo*))UnityEvent_2_Invoke_m3781318923_gshared)(__this, ___arg00, ___arg11, method)
