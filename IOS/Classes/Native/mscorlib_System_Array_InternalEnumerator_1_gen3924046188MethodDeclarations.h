﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3924046188.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23065293926.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1903821871_gshared (InternalEnumerator_1_t3924046188 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1903821871(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3924046188 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1903821871_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1072714343_gshared (InternalEnumerator_1_t3924046188 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1072714343(__this, method) ((  void (*) (InternalEnumerator_1_t3924046188 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1072714343_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3490397643_gshared (InternalEnumerator_1_t3924046188 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3490397643(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3924046188 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3490397643_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4135396136_gshared (InternalEnumerator_1_t3924046188 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4135396136(__this, method) ((  void (*) (InternalEnumerator_1_t3924046188 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4135396136_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4138226531_gshared (InternalEnumerator_1_t3924046188 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4138226531(__this, method) ((  bool (*) (InternalEnumerator_1_t3924046188 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4138226531_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::get_Current()
extern "C"  KeyValuePair_2_t3065293926  InternalEnumerator_1_get_Current_m2800610598_gshared (InternalEnumerator_1_t3924046188 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2800610598(__this, method) ((  KeyValuePair_2_t3065293926  (*) (InternalEnumerator_1_t3924046188 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2800610598_gshared)(__this, method)
