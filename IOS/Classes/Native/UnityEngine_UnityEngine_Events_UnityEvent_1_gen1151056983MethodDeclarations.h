﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2727799310MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::.ctor()
#define UnityEvent_1__ctor_m1503788483(__this, method) ((  void (*) (UnityEvent_1_t1151056983 *, const MethodInfo*))UnityEvent_1__ctor_m2073978020_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_AddListener_m1007951984(__this, ___call0, method) ((  void (*) (UnityEvent_1_t1151056983 *, UnityAction_1_t2479292719 *, const MethodInfo*))UnityEvent_1_AddListener_m22503421_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_RemoveListener_m584803935(__this, ___call0, method) ((  void (*) (UnityEvent_1_t1151056983 *, UnityAction_1_t2479292719 *, const MethodInfo*))UnityEvent_1_RemoveListener_m4278264272_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::FindMethod_Impl(System.String,System.Object)
#define UnityEvent_1_FindMethod_Impl_m305370264(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_1_t1151056983 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m2223850067_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::GetDelegate(System.Object,System.Reflection.MethodInfo)
#define UnityEvent_1_GetDelegate_m810101620(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_1_t1151056983 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m669290055_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_GetDelegate_m3254027075(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t2479292719 *, const MethodInfo*))UnityEvent_1_GetDelegate_m3098147632_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`1<Valve.VR.InteractionSystem.TeleportMarkerBase>::Invoke(T0)
#define UnityEvent_1_Invoke_m4248149103(__this, ___arg00, method) ((  void (*) (UnityEvent_1_t1151056983 *, TeleportMarkerBase_t1112706968 *, const MethodInfo*))UnityEvent_1_Invoke_m838874366_gshared)(__this, ___arg00, method)
