﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ObjectFollow
struct VRTK_ObjectFollow_t3175963762;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// System.Void VRTK.VRTK_ObjectFollow::.ctor()
extern "C"  void VRTK_ObjectFollow__ctor_m2665693762 (VRTK_ObjectFollow_t3175963762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_ObjectFollow::get_targetPosition()
extern "C"  Vector3_t2243707580  VRTK_ObjectFollow_get_targetPosition_m153006201 (VRTK_ObjectFollow_t3175963762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectFollow::set_targetPosition(UnityEngine.Vector3)
extern "C"  void VRTK_ObjectFollow_set_targetPosition_m2439163036 (VRTK_ObjectFollow_t3175963762 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion VRTK.VRTK_ObjectFollow::get_targetRotation()
extern "C"  Quaternion_t4030073918  VRTK_ObjectFollow_get_targetRotation_m3769853440 (VRTK_ObjectFollow_t3175963762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectFollow::set_targetRotation(UnityEngine.Quaternion)
extern "C"  void VRTK_ObjectFollow_set_targetRotation_m3427495957 (VRTK_ObjectFollow_t3175963762 * __this, Quaternion_t4030073918  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_ObjectFollow::get_targetScale()
extern "C"  Vector3_t2243707580  VRTK_ObjectFollow_get_targetScale_m3586456002 (VRTK_ObjectFollow_t3175963762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectFollow::set_targetScale(UnityEngine.Vector3)
extern "C"  void VRTK_ObjectFollow_set_targetScale_m4186557043 (VRTK_ObjectFollow_t3175963762 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectFollow::Follow()
extern "C"  void VRTK_ObjectFollow_Follow_m541450691 (VRTK_ObjectFollow_t3175963762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectFollow::OnEnable()
extern "C"  void VRTK_ObjectFollow_OnEnable_m3692853350 (VRTK_ObjectFollow_t3175963762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectFollow::OnValidate()
extern "C"  void VRTK_ObjectFollow_OnValidate_m4240255093 (VRTK_ObjectFollow_t3175963762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_ObjectFollow::GetScaleToFollow()
extern "C"  Vector3_t2243707580  VRTK_ObjectFollow_GetScaleToFollow_m1867633116 (VRTK_ObjectFollow_t3175963762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectFollow::SetScaleOnGameObject(UnityEngine.Vector3)
extern "C"  void VRTK_ObjectFollow_SetScaleOnGameObject_m830514551 (VRTK_ObjectFollow_t3175963762 * __this, Vector3_t2243707580  ___newScale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectFollow::FollowPosition()
extern "C"  void VRTK_ObjectFollow_FollowPosition_m2400125142 (VRTK_ObjectFollow_t3175963762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectFollow::FollowRotation()
extern "C"  void VRTK_ObjectFollow_FollowRotation_m3794051619 (VRTK_ObjectFollow_t3175963762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ObjectFollow::FollowScale()
extern "C"  void VRTK_ObjectFollow_FollowScale_m1039915203 (VRTK_ObjectFollow_t3175963762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
