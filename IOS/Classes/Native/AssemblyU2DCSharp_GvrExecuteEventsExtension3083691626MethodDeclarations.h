﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IGvrPointerHoverHandler
struct IGvrPointerHoverHandler_t1683868601;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<IGvrPointerHoverHandler>
struct EventFunction_1_t181019251;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"

// System.Void GvrExecuteEventsExtension::Execute(IGvrPointerHoverHandler,UnityEngine.EventSystems.BaseEventData)
extern "C"  void GvrExecuteEventsExtension_Execute_m751839475 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___handler0, BaseEventData_t2681005625 * ___eventData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<IGvrPointerHoverHandler> GvrExecuteEventsExtension::get_pointerHoverHandler()
extern "C"  EventFunction_1_t181019251 * GvrExecuteEventsExtension_get_pointerHoverHandler_m3800604216 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrExecuteEventsExtension::.cctor()
extern "C"  void GvrExecuteEventsExtension__cctor_m3810135292 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
