﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.HapticPulseEventHandler
struct HapticPulseEventHandler_t422205650;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.HapticPulseEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void HapticPulseEventHandler__ctor_m4029371170 (HapticPulseEventHandler_t422205650 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.HapticPulseEventHandler::Invoke(System.Single)
extern "C"  void HapticPulseEventHandler_Invoke_m738220595 (HapticPulseEventHandler_t422205650 * __this, float ___strength0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.HapticPulseEventHandler::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * HapticPulseEventHandler_BeginInvoke_m2826062838 (HapticPulseEventHandler_t422205650 * __this, float ___strength0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.HapticPulseEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void HapticPulseEventHandler_EndInvoke_m1225553428 (HapticPulseEventHandler_t422205650 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
