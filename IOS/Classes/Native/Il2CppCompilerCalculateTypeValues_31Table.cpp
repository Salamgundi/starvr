﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3563669597.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2806915419.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3465217523.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3901407784.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2916142869.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1836015611.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4146040027.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3401316463.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I430511954.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2215595694.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1171761296.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3310062628.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1008153775.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1046072227.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1381705816.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3322560050.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3949418959.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4170634026.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (ScreenSpaceAmbientObscurance_t3563669597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3100[8] = 
{
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_intensity_6(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_radius_7(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_blurIterations_8(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_blurFilterDistance_9(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_downsample_10(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_rand_11(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_aoShader_12(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_aoMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (ScreenSpaceAmbientOcclusion_t2806915419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3101[11] = 
{
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_Radius_2(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_SampleCount_3(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_OcclusionIntensity_4(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_Blur_5(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_Downsampling_6(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_OcclusionAttenuation_7(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_MinZ_8(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_SSAOShader_9(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_SSAOMaterial_10(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_RandomTexture_11(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_Supported_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (SSAOSamples_t3465217523)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3102[4] = 
{
	SSAOSamples_t3465217523::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (SepiaTone_t3901407784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (SunShafts_t2916142869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3104[14] = 
{
	SunShafts_t2916142869::get_offset_of_resolution_6(),
	SunShafts_t2916142869::get_offset_of_screenBlendMode_7(),
	SunShafts_t2916142869::get_offset_of_sunTransform_8(),
	SunShafts_t2916142869::get_offset_of_radialBlurIterations_9(),
	SunShafts_t2916142869::get_offset_of_sunColor_10(),
	SunShafts_t2916142869::get_offset_of_sunThreshold_11(),
	SunShafts_t2916142869::get_offset_of_sunShaftBlurRadius_12(),
	SunShafts_t2916142869::get_offset_of_sunShaftIntensity_13(),
	SunShafts_t2916142869::get_offset_of_maxRadius_14(),
	SunShafts_t2916142869::get_offset_of_useDepthTexture_15(),
	SunShafts_t2916142869::get_offset_of_sunShaftsShader_16(),
	SunShafts_t2916142869::get_offset_of_sunShaftsMaterial_17(),
	SunShafts_t2916142869::get_offset_of_simpleClearShader_18(),
	SunShafts_t2916142869::get_offset_of_simpleClearMaterial_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (SunShaftsResolution_t1836015611)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3105[4] = 
{
	SunShaftsResolution_t1836015611::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (ShaftsScreenBlendMode_t4146040027)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3106[3] = 
{
	ShaftsScreenBlendMode_t4146040027::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (TiltShift_t3401316463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3107[7] = 
{
	TiltShift_t3401316463::get_offset_of_mode_6(),
	TiltShift_t3401316463::get_offset_of_quality_7(),
	TiltShift_t3401316463::get_offset_of_blurArea_8(),
	TiltShift_t3401316463::get_offset_of_maxBlurSize_9(),
	TiltShift_t3401316463::get_offset_of_downsample_10(),
	TiltShift_t3401316463::get_offset_of_tiltShiftShader_11(),
	TiltShift_t3401316463::get_offset_of_tiltShiftMaterial_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (TiltShiftMode_t430511954)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3108[3] = 
{
	TiltShiftMode_t430511954::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (TiltShiftQuality_t2215595694)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3109[5] = 
{
	TiltShiftQuality_t2215595694::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (Tonemapping_t1171761296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3110[13] = 
{
	Tonemapping_t1171761296::get_offset_of_type_6(),
	Tonemapping_t1171761296::get_offset_of_adaptiveTextureSize_7(),
	Tonemapping_t1171761296::get_offset_of_remapCurve_8(),
	Tonemapping_t1171761296::get_offset_of_curveTex_9(),
	Tonemapping_t1171761296::get_offset_of_exposureAdjustment_10(),
	Tonemapping_t1171761296::get_offset_of_middleGrey_11(),
	Tonemapping_t1171761296::get_offset_of_white_12(),
	Tonemapping_t1171761296::get_offset_of_adaptionSpeed_13(),
	Tonemapping_t1171761296::get_offset_of_tonemapper_14(),
	Tonemapping_t1171761296::get_offset_of_validRenderTextureFormat_15(),
	Tonemapping_t1171761296::get_offset_of_tonemapMaterial_16(),
	Tonemapping_t1171761296::get_offset_of_rt_17(),
	Tonemapping_t1171761296::get_offset_of_rtFormat_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (TonemapperType_t3310062628)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3111[8] = 
{
	TonemapperType_t3310062628::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (AdaptiveTexSize_t1008153775)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3112[8] = 
{
	AdaptiveTexSize_t1008153775::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (Triangles_t1046072227), -1, sizeof(Triangles_t1046072227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3113[2] = 
{
	Triangles_t1046072227_StaticFields::get_offset_of_meshes_0(),
	Triangles_t1046072227_StaticFields::get_offset_of_currentTris_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (Twirl_t1381705816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3114[3] = 
{
	Twirl_t1381705816::get_offset_of_radius_4(),
	Twirl_t1381705816::get_offset_of_angle_5(),
	Twirl_t1381705816::get_offset_of_center_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (VignetteAndChromaticAberration_t3322560050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3115[14] = 
{
	VignetteAndChromaticAberration_t3322560050::get_offset_of_mode_6(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_intensity_7(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_chromaticAberration_8(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_axialAberration_9(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_blur_10(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_blurSpread_11(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_luminanceDependency_12(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_blurDistance_13(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_vignetteShader_14(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_separableBlurShader_15(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_chromAberrationShader_16(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_m_VignetteMaterial_17(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_m_SeparableBlurMaterial_18(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_m_ChromAberrationMaterial_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (AberrationMode_t3949418959)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3116[3] = 
{
	AberrationMode_t3949418959::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (Vortex_t4170634026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3117[3] = 
{
	Vortex_t4170634026::get_offset_of_radius_4(),
	Vortex_t4170634026::get_offset_of_angle_5(),
	Vortex_t4170634026::get_offset_of_center_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
