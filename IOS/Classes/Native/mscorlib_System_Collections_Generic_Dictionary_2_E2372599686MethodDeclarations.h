﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>
struct Dictionary_2_t1052574984;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2372599686.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23104887502.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SDKManager_SupportedSD1339136682.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1222508305_gshared (Enumerator_t2372599686 * __this, Dictionary_2_t1052574984 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1222508305(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2372599686 *, Dictionary_2_t1052574984 *, const MethodInfo*))Enumerator__ctor_m1222508305_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m588288164_gshared (Enumerator_t2372599686 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m588288164(__this, method) ((  Il2CppObject * (*) (Enumerator_t2372599686 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m588288164_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3676260506_gshared (Enumerator_t2372599686 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3676260506(__this, method) ((  void (*) (Enumerator_t2372599686 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3676260506_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1435760167_gshared (Enumerator_t2372599686 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1435760167(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2372599686 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1435760167_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2067661622_gshared (Enumerator_t2372599686 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2067661622(__this, method) ((  Il2CppObject * (*) (Enumerator_t2372599686 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2067661622_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1639305936_gshared (Enumerator_t2372599686 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1639305936(__this, method) ((  Il2CppObject * (*) (Enumerator_t2372599686 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1639305936_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m291900446_gshared (Enumerator_t2372599686 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m291900446(__this, method) ((  bool (*) (Enumerator_t2372599686 *, const MethodInfo*))Enumerator_MoveNext_m291900446_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3104887502  Enumerator_get_Current_m2601154750_gshared (Enumerator_t2372599686 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2601154750(__this, method) ((  KeyValuePair_2_t3104887502  (*) (Enumerator_t2372599686 *, const MethodInfo*))Enumerator_get_Current_m2601154750_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m1942024941_gshared (Enumerator_t2372599686 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1942024941(__this, method) ((  int32_t (*) (Enumerator_t2372599686 *, const MethodInfo*))Enumerator_get_CurrentKey_m1942024941_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m253268421_gshared (Enumerator_t2372599686 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m253268421(__this, method) ((  Il2CppObject * (*) (Enumerator_t2372599686 *, const MethodInfo*))Enumerator_get_CurrentValue_m253268421_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m256676999_gshared (Enumerator_t2372599686 * __this, const MethodInfo* method);
#define Enumerator_Reset_m256676999(__this, method) ((  void (*) (Enumerator_t2372599686 *, const MethodInfo*))Enumerator_Reset_m256676999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2260481914_gshared (Enumerator_t2372599686 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2260481914(__this, method) ((  void (*) (Enumerator_t2372599686 *, const MethodInfo*))Enumerator_VerifyState_m2260481914_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1279197290_gshared (Enumerator_t2372599686 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1279197290(__this, method) ((  void (*) (Enumerator_t2372599686 *, const MethodInfo*))Enumerator_VerifyCurrent_m1279197290_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<VRTK.VRTK_SDKManager/SupportedSDKs,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1508571049_gshared (Enumerator_t2372599686 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1508571049(__this, method) ((  void (*) (Enumerator_t2372599686 *, const MethodInfo*))Enumerator_Dispose_m1508571049_gshared)(__this, method)
