﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter
struct VRTK_MaterialPropertyBlockColorSwapHighlighter_t2327852568;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3303648957;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen283458390.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_MaterialPropertyBlock3303648957.h"

// System.Void VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter::.ctor()
extern "C"  void VRTK_MaterialPropertyBlockColorSwapHighlighter__ctor_m3179597268 (VRTK_MaterialPropertyBlockColorSwapHighlighter_t2327852568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter::Initialise(System.Nullable`1<UnityEngine.Color>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void VRTK_MaterialPropertyBlockColorSwapHighlighter_Initialise_m3690591059 (VRTK_MaterialPropertyBlockColorSwapHighlighter_t2327852568 * __this, Nullable_1_t283458390  ___color0, Dictionary_2_t309261261 * ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter::Unhighlight(System.Nullable`1<UnityEngine.Color>,System.Single)
extern "C"  void VRTK_MaterialPropertyBlockColorSwapHighlighter_Unhighlight_m566323311 (VRTK_MaterialPropertyBlockColorSwapHighlighter_t2327852568 * __this, Nullable_1_t283458390  ___color0, float ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter::StoreOriginalMaterials()
extern "C"  void VRTK_MaterialPropertyBlockColorSwapHighlighter_StoreOriginalMaterials_m128103166 (VRTK_MaterialPropertyBlockColorSwapHighlighter_t2327852568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter::ChangeToHighlightColor(UnityEngine.Color,System.Single)
extern "C"  void VRTK_MaterialPropertyBlockColorSwapHighlighter_ChangeToHighlightColor_m4250516285 (VRTK_MaterialPropertyBlockColorSwapHighlighter_t2327852568 * __this, Color_t2020392075  ___color0, float ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter::CycleColor(UnityEngine.Renderer,UnityEngine.MaterialPropertyBlock,UnityEngine.Color,System.Single)
extern "C"  Il2CppObject * VRTK_MaterialPropertyBlockColorSwapHighlighter_CycleColor_m1559525378 (VRTK_MaterialPropertyBlockColorSwapHighlighter_t2327852568 * __this, Renderer_t257310565 * ___renderer0, MaterialPropertyBlock_t3303648957 * ___highlightMaterialPropertyBlock1, Color_t2020392075  ___endColor2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
