﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_DaydreamController
struct SDK_DaydreamController_t2403190306;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_DaydreamController::.ctor()
extern "C"  void SDK_DaydreamController__ctor_m797072724 (SDK_DaydreamController_t2403190306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
