﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_UIDraggableItem
struct VRTK_UIDraggableItem_t2269178406;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// VRTK.VRTK_UIPointer
struct VRTK_UIPointer_t2714926455;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void VRTK.VRTK_UIDraggableItem::.ctor()
extern "C"  void VRTK_UIDraggableItem__ctor_m3029610376 (VRTK_UIDraggableItem_t2269178406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIDraggableItem::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void VRTK_UIDraggableItem_OnBeginDrag_m407964972 (VRTK_UIDraggableItem_t2269178406 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIDraggableItem::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void VRTK_UIDraggableItem_OnDrag_m3631955875 (VRTK_UIDraggableItem_t2269178406 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIDraggableItem::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void VRTK_UIDraggableItem_OnEndDrag_m2148315312 (VRTK_UIDraggableItem_t2269178406 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIDraggableItem::OnEnable()
extern "C"  void VRTK_UIDraggableItem_OnEnable_m2200266388 (VRTK_UIDraggableItem_t2269178406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_UIPointer VRTK.VRTK_UIDraggableItem::GetPointer(UnityEngine.EventSystems.PointerEventData)
extern "C"  VRTK_UIPointer_t2714926455 * VRTK_UIDraggableItem_GetPointer_m1874048532 (VRTK_UIDraggableItem_t2269178406 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIDraggableItem::SetDragPosition(UnityEngine.EventSystems.PointerEventData)
extern "C"  void VRTK_UIDraggableItem_SetDragPosition_m533012469 (VRTK_UIDraggableItem_t2269178406 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_UIDraggableItem::ResetElement()
extern "C"  void VRTK_UIDraggableItem_ResetElement_m2312080505 (VRTK_UIDraggableItem_t2269178406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
