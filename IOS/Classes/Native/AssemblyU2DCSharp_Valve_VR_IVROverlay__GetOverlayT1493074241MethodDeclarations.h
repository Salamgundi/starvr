﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayTransformType
struct _GetOverlayTransformType_t1493074241;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayTransformType3148689642.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayTransformType::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayTransformType__ctor_m3567642010 (_GetOverlayTransformType_t1493074241 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTransformType::Invoke(System.UInt64,Valve.VR.VROverlayTransformType&)
extern "C"  int32_t _GetOverlayTransformType_Invoke_m223622883 (_GetOverlayTransformType_t1493074241 * __this, uint64_t ___ulOverlayHandle0, int32_t* ___peTransformType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayTransformType::BeginInvoke(System.UInt64,Valve.VR.VROverlayTransformType&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayTransformType_BeginInvoke_m906438514 (_GetOverlayTransformType_t1493074241 * __this, uint64_t ___ulOverlayHandle0, int32_t* ___peTransformType1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayTransformType::EndInvoke(Valve.VR.VROverlayTransformType&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayTransformType_EndInvoke_m555317326 (_GetOverlayTransformType_t1493074241 * __this, int32_t* ___peTransformType0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
