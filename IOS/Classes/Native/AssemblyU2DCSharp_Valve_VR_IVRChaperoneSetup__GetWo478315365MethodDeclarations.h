﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_GetWorkingPlayAreaRect
struct _GetWorkingPlayAreaRect_t478315365;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdQuad_t2172573705.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_GetWorkingPlayAreaRect::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetWorkingPlayAreaRect__ctor_m2938480012 (_GetWorkingPlayAreaRect_t478315365 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetWorkingPlayAreaRect::Invoke(Valve.VR.HmdQuad_t&)
extern "C"  bool _GetWorkingPlayAreaRect_Invoke_m2534769501 (_GetWorkingPlayAreaRect_t478315365 * __this, HmdQuad_t_t2172573705 * ___rect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_GetWorkingPlayAreaRect::BeginInvoke(Valve.VR.HmdQuad_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetWorkingPlayAreaRect_BeginInvoke_m538061318 (_GetWorkingPlayAreaRect_t478315365 * __this, HmdQuad_t_t2172573705 * ___rect0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperoneSetup/_GetWorkingPlayAreaRect::EndInvoke(Valve.VR.HmdQuad_t&,System.IAsyncResult)
extern "C"  bool _GetWorkingPlayAreaRect_EndInvoke_m2327068695 (_GetWorkingPlayAreaRect_t478315365 * __this, HmdQuad_t_t2172573705 * ___rect0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
