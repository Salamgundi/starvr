﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.ControlReactor
struct ControlReactor_t2704852751;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_Control3DEventArgs4095025701.h"

// System.Void VRTK.Examples.ControlReactor::.ctor()
extern "C"  void ControlReactor__ctor_m1344034056 (ControlReactor_t2704852751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.ControlReactor::Start()
extern "C"  void ControlReactor_Start_m2496431480 (ControlReactor_t2704852751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.ControlReactor::HandleChange(System.Object,VRTK.Control3DEventArgs)
extern "C"  void ControlReactor_HandleChange_m3283826554 (ControlReactor_t2704852751 * __this, Il2CppObject * ___sender0, Control3DEventArgs_t4095025701  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
