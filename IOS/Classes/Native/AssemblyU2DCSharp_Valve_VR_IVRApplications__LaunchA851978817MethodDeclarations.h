﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_LaunchApplication
struct _LaunchApplication_t851978817;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_LaunchApplication::.ctor(System.Object,System.IntPtr)
extern "C"  void _LaunchApplication__ctor_m2797212378 (_LaunchApplication_t851978817 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_LaunchApplication::Invoke(System.String)
extern "C"  int32_t _LaunchApplication_Invoke_m2362523546 (_LaunchApplication_t851978817 * __this, String_t* ___pchAppKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_LaunchApplication::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _LaunchApplication_BeginInvoke_m1903085121 (_LaunchApplication_t851978817 * __this, String_t* ___pchAppKey0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRApplicationError Valve.VR.IVRApplications/_LaunchApplication::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _LaunchApplication_EndInvoke_m2919347060 (_LaunchApplication_t851978817 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
