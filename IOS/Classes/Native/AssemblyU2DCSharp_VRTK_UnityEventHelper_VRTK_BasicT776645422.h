﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_BasicTeleport
struct VRTK_BasicTeleport_t3532761337;
// VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t4156405881;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents
struct  VRTK_BasicTeleport_UnityEvents_t776645422  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_BasicTeleport VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents::bt
	VRTK_BasicTeleport_t3532761337 * ___bt_2;
	// VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents::OnTeleporting
	UnityObjectEvent_t4156405881 * ___OnTeleporting_3;
	// VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_BasicTeleport_UnityEvents::OnTeleported
	UnityObjectEvent_t4156405881 * ___OnTeleported_4;

public:
	inline static int32_t get_offset_of_bt_2() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_UnityEvents_t776645422, ___bt_2)); }
	inline VRTK_BasicTeleport_t3532761337 * get_bt_2() const { return ___bt_2; }
	inline VRTK_BasicTeleport_t3532761337 ** get_address_of_bt_2() { return &___bt_2; }
	inline void set_bt_2(VRTK_BasicTeleport_t3532761337 * value)
	{
		___bt_2 = value;
		Il2CppCodeGenWriteBarrier(&___bt_2, value);
	}

	inline static int32_t get_offset_of_OnTeleporting_3() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_UnityEvents_t776645422, ___OnTeleporting_3)); }
	inline UnityObjectEvent_t4156405881 * get_OnTeleporting_3() const { return ___OnTeleporting_3; }
	inline UnityObjectEvent_t4156405881 ** get_address_of_OnTeleporting_3() { return &___OnTeleporting_3; }
	inline void set_OnTeleporting_3(UnityObjectEvent_t4156405881 * value)
	{
		___OnTeleporting_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnTeleporting_3, value);
	}

	inline static int32_t get_offset_of_OnTeleported_4() { return static_cast<int32_t>(offsetof(VRTK_BasicTeleport_UnityEvents_t776645422, ___OnTeleported_4)); }
	inline UnityObjectEvent_t4156405881 * get_OnTeleported_4() const { return ___OnTeleported_4; }
	inline UnityObjectEvent_t4156405881 ** get_address_of_OnTeleported_4() { return &___OnTeleported_4; }
	inline void set_OnTeleported_4(UnityObjectEvent_t4156405881 * value)
	{
		___OnTeleported_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnTeleported_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
