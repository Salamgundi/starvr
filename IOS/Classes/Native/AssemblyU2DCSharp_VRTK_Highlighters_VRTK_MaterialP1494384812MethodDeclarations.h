﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter/<CycleColor>c__Iterator0
struct U3CCycleColorU3Ec__Iterator0_t1494384812;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter/<CycleColor>c__Iterator0::.ctor()
extern "C"  void U3CCycleColorU3Ec__Iterator0__ctor_m3100086943 (U3CCycleColorU3Ec__Iterator0_t1494384812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter/<CycleColor>c__Iterator0::MoveNext()
extern "C"  bool U3CCycleColorU3Ec__Iterator0_MoveNext_m4067448709 (U3CCycleColorU3Ec__Iterator0_t1494384812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter/<CycleColor>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCycleColorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2528366269 (U3CCycleColorU3Ec__Iterator0_t1494384812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter/<CycleColor>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCycleColorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3988453861 (U3CCycleColorU3Ec__Iterator0_t1494384812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter/<CycleColor>c__Iterator0::Dispose()
extern "C"  void U3CCycleColorU3Ec__Iterator0_Dispose_m2388456806 (U3CCycleColorU3Ec__Iterator0_t1494384812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Highlighters.VRTK_MaterialPropertyBlockColorSwapHighlighter/<CycleColor>c__Iterator0::Reset()
extern "C"  void U3CCycleColorU3Ec__Iterator0_Reset_m1071224404 (U3CCycleColorU3Ec__Iterator0_t1494384812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
