﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>
struct Dictionary_2_t1012981408;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2699514172.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.LogType,UnityEngine.Color>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m877222448_gshared (Enumerator_t2699514172 * __this, Dictionary_2_t1012981408 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m877222448(__this, ___host0, method) ((  void (*) (Enumerator_t2699514172 *, Dictionary_2_t1012981408 *, const MethodInfo*))Enumerator__ctor_m877222448_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m679343341_gshared (Enumerator_t2699514172 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m679343341(__this, method) ((  Il2CppObject * (*) (Enumerator_t2699514172 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m679343341_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1104995005_gshared (Enumerator_t2699514172 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1104995005(__this, method) ((  void (*) (Enumerator_t2699514172 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1104995005_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.LogType,UnityEngine.Color>::Dispose()
extern "C"  void Enumerator_Dispose_m4051076888_gshared (Enumerator_t2699514172 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4051076888(__this, method) ((  void (*) (Enumerator_t2699514172 *, const MethodInfo*))Enumerator_Dispose_m4051076888_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.LogType,UnityEngine.Color>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4103789493_gshared (Enumerator_t2699514172 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4103789493(__this, method) ((  bool (*) (Enumerator_t2699514172 *, const MethodInfo*))Enumerator_MoveNext_m4103789493_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.LogType,UnityEngine.Color>::get_Current()
extern "C"  Color_t2020392075  Enumerator_get_Current_m2606686707_gshared (Enumerator_t2699514172 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2606686707(__this, method) ((  Color_t2020392075  (*) (Enumerator_t2699514172 *, const MethodInfo*))Enumerator_get_Current_m2606686707_gshared)(__this, method)
