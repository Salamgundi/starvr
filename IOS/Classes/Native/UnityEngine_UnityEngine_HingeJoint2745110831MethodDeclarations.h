﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.HingeJoint
struct HingeJoint_t2745110831;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointLimits4282861422.h"
#include "UnityEngine_UnityEngine_JointSpring1540921605.h"

// UnityEngine.JointLimits UnityEngine.HingeJoint::get_limits()
extern "C"  JointLimits_t4282861422  HingeJoint_get_limits_m4283751733 (HingeJoint_t2745110831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::set_limits(UnityEngine.JointLimits)
extern "C"  void HingeJoint_set_limits_m249910410 (HingeJoint_t2745110831 * __this, JointLimits_t4282861422  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::INTERNAL_get_limits(UnityEngine.JointLimits&)
extern "C"  void HingeJoint_INTERNAL_get_limits_m4155226410 (HingeJoint_t2745110831 * __this, JointLimits_t4282861422 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::INTERNAL_set_limits(UnityEngine.JointLimits&)
extern "C"  void HingeJoint_INTERNAL_set_limits_m439830886 (HingeJoint_t2745110831 * __this, JointLimits_t4282861422 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointSpring UnityEngine.HingeJoint::get_spring()
extern "C"  JointSpring_t1540921605  HingeJoint_get_spring_m53828405 (HingeJoint_t2745110831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::set_spring(UnityEngine.JointSpring)
extern "C"  void HingeJoint_set_spring_m3669466314 (HingeJoint_t2745110831 * __this, JointSpring_t1540921605  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::INTERNAL_get_spring(UnityEngine.JointSpring&)
extern "C"  void HingeJoint_INTERNAL_get_spring_m3293750890 (HingeJoint_t2745110831 * __this, JointSpring_t1540921605 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::INTERNAL_set_spring(UnityEngine.JointSpring&)
extern "C"  void HingeJoint_INTERNAL_set_spring_m2039164582 (HingeJoint_t2745110831 * __this, JointSpring_t1540921605 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::set_useLimits(System.Boolean)
extern "C"  void HingeJoint_set_useLimits_m1140625641 (HingeJoint_t2745110831 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.HingeJoint::get_useSpring()
extern "C"  bool HingeJoint_get_useSpring_m281717861 (HingeJoint_t2745110831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::set_useSpring(System.Boolean)
extern "C"  void HingeJoint_set_useSpring_m3450562342 (HingeJoint_t2745110831 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.HingeJoint::get_angle()
extern "C"  float HingeJoint_get_angle_m3634425392 (HingeJoint_t2745110831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
