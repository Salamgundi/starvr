﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup/_ReloadFromDisk
struct _ReloadFromDisk_t2889659875;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EChaperoneConfigFile30305944.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperoneSetup/_ReloadFromDisk::.ctor(System.Object,System.IntPtr)
extern "C"  void _ReloadFromDisk__ctor_m3536682214 (_ReloadFromDisk_t2889659875 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperoneSetup/_ReloadFromDisk::Invoke(Valve.VR.EChaperoneConfigFile)
extern "C"  void _ReloadFromDisk_Invoke_m2115309994 (_ReloadFromDisk_t2889659875 * __this, int32_t ___configFile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperoneSetup/_ReloadFromDisk::BeginInvoke(Valve.VR.EChaperoneConfigFile,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ReloadFromDisk_BeginInvoke_m1613179021 (_ReloadFromDisk_t2889659875 * __this, int32_t ___configFile0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperoneSetup/_ReloadFromDisk::EndInvoke(System.IAsyncResult)
extern "C"  void _ReloadFromDisk_EndInvoke_m3250648108 (_ReloadFromDisk_t2889659875 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
