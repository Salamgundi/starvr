﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.FixedJoint
struct FixedJoint_t3848069458;
// UnityEngine.HingeJoint
struct HingeJoint_t2745110831;

#include "AssemblyU2DCSharp_VRTK_VRTK_Control651619021.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_Direction3775008092.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_Chest
struct  VRTK_Chest_t1253706853  : public VRTK_Control_t651619021
{
public:
	// VRTK.VRTK_Control/Direction VRTK.VRTK_Chest::direction
	int32_t ___direction_15;
	// UnityEngine.GameObject VRTK.VRTK_Chest::lid
	GameObject_t1756533147 * ___lid_16;
	// UnityEngine.GameObject VRTK.VRTK_Chest::body
	GameObject_t1756533147 * ___body_17;
	// UnityEngine.GameObject VRTK.VRTK_Chest::handle
	GameObject_t1756533147 * ___handle_18;
	// UnityEngine.GameObject VRTK.VRTK_Chest::content
	GameObject_t1756533147 * ___content_19;
	// System.Boolean VRTK.VRTK_Chest::hideContent
	bool ___hideContent_20;
	// System.Single VRTK.VRTK_Chest::maxAngle
	float ___maxAngle_21;
	// System.Single VRTK.VRTK_Chest::minAngle
	float ___minAngle_22;
	// System.Single VRTK.VRTK_Chest::stepSize
	float ___stepSize_23;
	// UnityEngine.Rigidbody VRTK.VRTK_Chest::bodyRigidbody
	Rigidbody_t4233889191 * ___bodyRigidbody_24;
	// UnityEngine.Rigidbody VRTK.VRTK_Chest::handleRigidbody
	Rigidbody_t4233889191 * ___handleRigidbody_25;
	// UnityEngine.FixedJoint VRTK.VRTK_Chest::handleJoint
	FixedJoint_t3848069458 * ___handleJoint_26;
	// UnityEngine.Rigidbody VRTK.VRTK_Chest::lidRigidbody
	Rigidbody_t4233889191 * ___lidRigidbody_27;
	// UnityEngine.HingeJoint VRTK.VRTK_Chest::lidJoint
	HingeJoint_t2745110831 * ___lidJoint_28;
	// System.Boolean VRTK.VRTK_Chest::lidJointCreated
	bool ___lidJointCreated_29;
	// VRTK.VRTK_Control/Direction VRTK.VRTK_Chest::finalDirection
	int32_t ___finalDirection_30;
	// System.Single VRTK.VRTK_Chest::subDirection
	float ___subDirection_31;

public:
	inline static int32_t get_offset_of_direction_15() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___direction_15)); }
	inline int32_t get_direction_15() const { return ___direction_15; }
	inline int32_t* get_address_of_direction_15() { return &___direction_15; }
	inline void set_direction_15(int32_t value)
	{
		___direction_15 = value;
	}

	inline static int32_t get_offset_of_lid_16() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___lid_16)); }
	inline GameObject_t1756533147 * get_lid_16() const { return ___lid_16; }
	inline GameObject_t1756533147 ** get_address_of_lid_16() { return &___lid_16; }
	inline void set_lid_16(GameObject_t1756533147 * value)
	{
		___lid_16 = value;
		Il2CppCodeGenWriteBarrier(&___lid_16, value);
	}

	inline static int32_t get_offset_of_body_17() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___body_17)); }
	inline GameObject_t1756533147 * get_body_17() const { return ___body_17; }
	inline GameObject_t1756533147 ** get_address_of_body_17() { return &___body_17; }
	inline void set_body_17(GameObject_t1756533147 * value)
	{
		___body_17 = value;
		Il2CppCodeGenWriteBarrier(&___body_17, value);
	}

	inline static int32_t get_offset_of_handle_18() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___handle_18)); }
	inline GameObject_t1756533147 * get_handle_18() const { return ___handle_18; }
	inline GameObject_t1756533147 ** get_address_of_handle_18() { return &___handle_18; }
	inline void set_handle_18(GameObject_t1756533147 * value)
	{
		___handle_18 = value;
		Il2CppCodeGenWriteBarrier(&___handle_18, value);
	}

	inline static int32_t get_offset_of_content_19() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___content_19)); }
	inline GameObject_t1756533147 * get_content_19() const { return ___content_19; }
	inline GameObject_t1756533147 ** get_address_of_content_19() { return &___content_19; }
	inline void set_content_19(GameObject_t1756533147 * value)
	{
		___content_19 = value;
		Il2CppCodeGenWriteBarrier(&___content_19, value);
	}

	inline static int32_t get_offset_of_hideContent_20() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___hideContent_20)); }
	inline bool get_hideContent_20() const { return ___hideContent_20; }
	inline bool* get_address_of_hideContent_20() { return &___hideContent_20; }
	inline void set_hideContent_20(bool value)
	{
		___hideContent_20 = value;
	}

	inline static int32_t get_offset_of_maxAngle_21() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___maxAngle_21)); }
	inline float get_maxAngle_21() const { return ___maxAngle_21; }
	inline float* get_address_of_maxAngle_21() { return &___maxAngle_21; }
	inline void set_maxAngle_21(float value)
	{
		___maxAngle_21 = value;
	}

	inline static int32_t get_offset_of_minAngle_22() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___minAngle_22)); }
	inline float get_minAngle_22() const { return ___minAngle_22; }
	inline float* get_address_of_minAngle_22() { return &___minAngle_22; }
	inline void set_minAngle_22(float value)
	{
		___minAngle_22 = value;
	}

	inline static int32_t get_offset_of_stepSize_23() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___stepSize_23)); }
	inline float get_stepSize_23() const { return ___stepSize_23; }
	inline float* get_address_of_stepSize_23() { return &___stepSize_23; }
	inline void set_stepSize_23(float value)
	{
		___stepSize_23 = value;
	}

	inline static int32_t get_offset_of_bodyRigidbody_24() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___bodyRigidbody_24)); }
	inline Rigidbody_t4233889191 * get_bodyRigidbody_24() const { return ___bodyRigidbody_24; }
	inline Rigidbody_t4233889191 ** get_address_of_bodyRigidbody_24() { return &___bodyRigidbody_24; }
	inline void set_bodyRigidbody_24(Rigidbody_t4233889191 * value)
	{
		___bodyRigidbody_24 = value;
		Il2CppCodeGenWriteBarrier(&___bodyRigidbody_24, value);
	}

	inline static int32_t get_offset_of_handleRigidbody_25() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___handleRigidbody_25)); }
	inline Rigidbody_t4233889191 * get_handleRigidbody_25() const { return ___handleRigidbody_25; }
	inline Rigidbody_t4233889191 ** get_address_of_handleRigidbody_25() { return &___handleRigidbody_25; }
	inline void set_handleRigidbody_25(Rigidbody_t4233889191 * value)
	{
		___handleRigidbody_25 = value;
		Il2CppCodeGenWriteBarrier(&___handleRigidbody_25, value);
	}

	inline static int32_t get_offset_of_handleJoint_26() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___handleJoint_26)); }
	inline FixedJoint_t3848069458 * get_handleJoint_26() const { return ___handleJoint_26; }
	inline FixedJoint_t3848069458 ** get_address_of_handleJoint_26() { return &___handleJoint_26; }
	inline void set_handleJoint_26(FixedJoint_t3848069458 * value)
	{
		___handleJoint_26 = value;
		Il2CppCodeGenWriteBarrier(&___handleJoint_26, value);
	}

	inline static int32_t get_offset_of_lidRigidbody_27() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___lidRigidbody_27)); }
	inline Rigidbody_t4233889191 * get_lidRigidbody_27() const { return ___lidRigidbody_27; }
	inline Rigidbody_t4233889191 ** get_address_of_lidRigidbody_27() { return &___lidRigidbody_27; }
	inline void set_lidRigidbody_27(Rigidbody_t4233889191 * value)
	{
		___lidRigidbody_27 = value;
		Il2CppCodeGenWriteBarrier(&___lidRigidbody_27, value);
	}

	inline static int32_t get_offset_of_lidJoint_28() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___lidJoint_28)); }
	inline HingeJoint_t2745110831 * get_lidJoint_28() const { return ___lidJoint_28; }
	inline HingeJoint_t2745110831 ** get_address_of_lidJoint_28() { return &___lidJoint_28; }
	inline void set_lidJoint_28(HingeJoint_t2745110831 * value)
	{
		___lidJoint_28 = value;
		Il2CppCodeGenWriteBarrier(&___lidJoint_28, value);
	}

	inline static int32_t get_offset_of_lidJointCreated_29() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___lidJointCreated_29)); }
	inline bool get_lidJointCreated_29() const { return ___lidJointCreated_29; }
	inline bool* get_address_of_lidJointCreated_29() { return &___lidJointCreated_29; }
	inline void set_lidJointCreated_29(bool value)
	{
		___lidJointCreated_29 = value;
	}

	inline static int32_t get_offset_of_finalDirection_30() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___finalDirection_30)); }
	inline int32_t get_finalDirection_30() const { return ___finalDirection_30; }
	inline int32_t* get_address_of_finalDirection_30() { return &___finalDirection_30; }
	inline void set_finalDirection_30(int32_t value)
	{
		___finalDirection_30 = value;
	}

	inline static int32_t get_offset_of_subDirection_31() { return static_cast<int32_t>(offsetof(VRTK_Chest_t1253706853, ___subDirection_31)); }
	inline float get_subDirection_31() const { return ___subDirection_31; }
	inline float* get_address_of_subDirection_31() { return &___subDirection_31; }
	inline void set_subDirection_31(float value)
	{
		___subDirection_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
