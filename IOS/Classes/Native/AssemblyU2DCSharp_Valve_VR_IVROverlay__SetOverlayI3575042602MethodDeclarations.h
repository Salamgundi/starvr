﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetOverlayInputMethod
struct _SetOverlayInputMethod_t3575042602;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayInputMethod3830649193.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetOverlayInputMethod::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayInputMethod__ctor_m3092122113 (_SetOverlayInputMethod_t3575042602 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayInputMethod::Invoke(System.UInt64,Valve.VR.VROverlayInputMethod)
extern "C"  int32_t _SetOverlayInputMethod_Invoke_m8169801 (_SetOverlayInputMethod_t3575042602 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eInputMethod1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayInputMethod::BeginInvoke(System.UInt64,Valve.VR.VROverlayInputMethod,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetOverlayInputMethod_BeginInvoke_m605303188 (_SetOverlayInputMethod_t3575042602 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eInputMethod1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayInputMethod::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayInputMethod_EndInvoke_m1225885463 (_SetOverlayInputMethod_t3575042602 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
