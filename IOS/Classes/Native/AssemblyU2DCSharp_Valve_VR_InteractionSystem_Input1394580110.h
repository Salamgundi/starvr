﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// Valve.VR.InteractionSystem.InputModule
struct InputModule_t1394580110;

#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM1295781545.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.InputModule
struct  InputModule_t1394580110  : public BaseInputModule_t1295781545
{
public:
	// UnityEngine.GameObject Valve.VR.InteractionSystem.InputModule::submitObject
	GameObject_t1756533147 * ___submitObject_8;

public:
	inline static int32_t get_offset_of_submitObject_8() { return static_cast<int32_t>(offsetof(InputModule_t1394580110, ___submitObject_8)); }
	inline GameObject_t1756533147 * get_submitObject_8() const { return ___submitObject_8; }
	inline GameObject_t1756533147 ** get_address_of_submitObject_8() { return &___submitObject_8; }
	inline void set_submitObject_8(GameObject_t1756533147 * value)
	{
		___submitObject_8 = value;
		Il2CppCodeGenWriteBarrier(&___submitObject_8, value);
	}
};

struct InputModule_t1394580110_StaticFields
{
public:
	// Valve.VR.InteractionSystem.InputModule Valve.VR.InteractionSystem.InputModule::_instance
	InputModule_t1394580110 * ____instance_9;

public:
	inline static int32_t get_offset_of__instance_9() { return static_cast<int32_t>(offsetof(InputModule_t1394580110_StaticFields, ____instance_9)); }
	inline InputModule_t1394580110 * get__instance_9() const { return ____instance_9; }
	inline InputModule_t1394580110 ** get_address_of__instance_9() { return &____instance_9; }
	inline void set__instance_9(InputModule_t1394580110 * value)
	{
		____instance_9 = value;
		Il2CppCodeGenWriteBarrier(&____instance_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
