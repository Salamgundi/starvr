﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.LinearDrive
struct LinearDrive_t1068763581;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void Valve.VR.InteractionSystem.LinearDrive::.ctor()
extern "C"  void LinearDrive__ctor_m679362859 (LinearDrive_t1068763581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearDrive::Awake()
extern "C"  void LinearDrive_Awake_m3089177486 (LinearDrive_t1068763581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearDrive::Start()
extern "C"  void LinearDrive_Start_m4225136351 (LinearDrive_t1068763581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearDrive::HandHoverUpdate(Valve.VR.InteractionSystem.Hand)
extern "C"  void LinearDrive_HandHoverUpdate_m743301205 (LinearDrive_t1068763581 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearDrive::CalculateMappingChangeRate()
extern "C"  void LinearDrive_CalculateMappingChangeRate_m217683967 (LinearDrive_t1068763581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearDrive::UpdateLinearMapping(UnityEngine.Transform)
extern "C"  void LinearDrive_UpdateLinearMapping_m2343504344 (LinearDrive_t1068763581 * __this, Transform_t3275118058 * ___tr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Valve.VR.InteractionSystem.LinearDrive::CalculateLinearMapping(UnityEngine.Transform)
extern "C"  float LinearDrive_CalculateLinearMapping_m2703898011 (LinearDrive_t1068763581 * __this, Transform_t3275118058 * ___tr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearDrive::Update()
extern "C"  void LinearDrive_Update_m736242780 (LinearDrive_t1068763581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
