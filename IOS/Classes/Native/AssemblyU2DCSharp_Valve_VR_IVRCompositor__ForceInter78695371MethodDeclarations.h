﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_ForceInterleavedReprojectionOn
struct _ForceInterleavedReprojectionOn_t78695371;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_ForceInterleavedReprojectionOn::.ctor(System.Object,System.IntPtr)
extern "C"  void _ForceInterleavedReprojectionOn__ctor_m3952215088 (_ForceInterleavedReprojectionOn_t78695371 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_ForceInterleavedReprojectionOn::Invoke(System.Boolean)
extern "C"  void _ForceInterleavedReprojectionOn_Invoke_m871517821 (_ForceInterleavedReprojectionOn_t78695371 * __this, bool ___bOverride0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_ForceInterleavedReprojectionOn::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ForceInterleavedReprojectionOn_BeginInvoke_m3507812282 (_ForceInterleavedReprojectionOn_t78695371 * __this, bool ___bOverride0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRCompositor/_ForceInterleavedReprojectionOn::EndInvoke(System.IAsyncResult)
extern "C"  void _ForceInterleavedReprojectionOn_EndInvoke_m2982696270 (_ForceInterleavedReprojectionOn_t78695371 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
