﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct List_1_t3922690692;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3457420366.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_258602264.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3360409919_gshared (Enumerator_t3457420366 * __this, List_1_t3922690692 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3360409919(__this, ___l0, method) ((  void (*) (Enumerator_t3457420366 *, List_1_t3922690692 *, const MethodInfo*))Enumerator__ctor_m3360409919_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1421715187_gshared (Enumerator_t3457420366 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1421715187(__this, method) ((  void (*) (Enumerator_t3457420366 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1421715187_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3461842571_gshared (Enumerator_t3457420366 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3461842571(__this, method) ((  Il2CppObject * (*) (Enumerator_t3457420366 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3461842571_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Dispose()
extern "C"  void Enumerator_Dispose_m28399880_gshared (Enumerator_t3457420366 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m28399880(__this, method) ((  void (*) (Enumerator_t3457420366 *, const MethodInfo*))Enumerator_Dispose_m28399880_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::VerifyState()
extern "C"  void Enumerator_VerifyState_m699500485_gshared (Enumerator_t3457420366 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m699500485(__this, method) ((  void (*) (Enumerator_t3457420366 *, const MethodInfo*))Enumerator_VerifyState_m699500485_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1551428375_gshared (Enumerator_t3457420366 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1551428375(__this, method) ((  bool (*) (Enumerator_t3457420366 *, const MethodInfo*))Enumerator_MoveNext_m1551428375_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::get_Current()
extern "C"  KeyValuePair_2_t258602264  Enumerator_get_Current_m1781954980_gshared (Enumerator_t3457420366 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1781954980(__this, method) ((  KeyValuePair_2_t258602264  (*) (Enumerator_t3457420366 *, const MethodInfo*))Enumerator_get_Current_m1781954980_gshared)(__this, method)
