﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_TeleportDisableOnControllerObscured
struct VRTK_TeleportDisableOnControllerObscured_t3503067553;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetControllerAwareEvent2653531721.h"

// System.Void VRTK.VRTK_TeleportDisableOnControllerObscured::.ctor()
extern "C"  void VRTK_TeleportDisableOnControllerObscured__ctor_m2391417985 (VRTK_TeleportDisableOnControllerObscured_t3503067553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TeleportDisableOnControllerObscured::OnEnable()
extern "C"  void VRTK_TeleportDisableOnControllerObscured_OnEnable_m2426854961 (VRTK_TeleportDisableOnControllerObscured_t3503067553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TeleportDisableOnControllerObscured::OnDisable()
extern "C"  void VRTK_TeleportDisableOnControllerObscured_OnDisable_m741552954 (VRTK_TeleportDisableOnControllerObscured_t3503067553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTK.VRTK_TeleportDisableOnControllerObscured::EnableAtEndOfFrame()
extern "C"  Il2CppObject * VRTK_TeleportDisableOnControllerObscured_EnableAtEndOfFrame_m2320319930 (VRTK_TeleportDisableOnControllerObscured_t3503067553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TeleportDisableOnControllerObscured::DisableTeleport(System.Object,VRTK.HeadsetControllerAwareEventArgs)
extern "C"  void VRTK_TeleportDisableOnControllerObscured_DisableTeleport_m4086778990 (VRTK_TeleportDisableOnControllerObscured_t3503067553 * __this, Il2CppObject * ___sender0, HeadsetControllerAwareEventArgs_t2653531721  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_TeleportDisableOnControllerObscured::EnableTeleport(System.Object,VRTK.HeadsetControllerAwareEventArgs)
extern "C"  void VRTK_TeleportDisableOnControllerObscured_EnableTeleport_m3791876067 (VRTK_TeleportDisableOnControllerObscured_t3503067553 * __this, Il2CppObject * ___sender0, HeadsetControllerAwareEventArgs_t2653531721  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
