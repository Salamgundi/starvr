﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetProjectionMatrix
struct _GetProjectionMatrix_t2621141914;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix44_t664273159.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetProjectionMatrix::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetProjectionMatrix__ctor_m1740759091 (_GetProjectionMatrix_t2621141914 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix44_t Valve.VR.IVRSystem/_GetProjectionMatrix::Invoke(Valve.VR.EVREye,System.Single,System.Single)
extern "C"  HmdMatrix44_t_t664273159  _GetProjectionMatrix_Invoke_m3714650273 (_GetProjectionMatrix_t2621141914 * __this, int32_t ___eEye0, float ___fNearZ1, float ___fFarZ2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetProjectionMatrix::BeginInvoke(Valve.VR.EVREye,System.Single,System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetProjectionMatrix_BeginInvoke_m359430866 (_GetProjectionMatrix_t2621141914 * __this, int32_t ___eEye0, float ___fNearZ1, float ___fFarZ2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.HmdMatrix44_t Valve.VR.IVRSystem/_GetProjectionMatrix::EndInvoke(System.IAsyncResult)
extern "C"  HmdMatrix44_t_t664273159  _GetProjectionMatrix_EndInvoke_m722289619 (_GetProjectionMatrix_t2621141914 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
