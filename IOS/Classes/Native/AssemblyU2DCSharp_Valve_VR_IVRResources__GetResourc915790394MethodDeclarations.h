﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRResources/_GetResourceFullPath
struct _GetResourceFullPath_t915790394;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRResources/_GetResourceFullPath::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetResourceFullPath__ctor_m2248991025 (_GetResourceFullPath_t915790394 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRResources/_GetResourceFullPath::Invoke(System.String,System.String,System.String,System.UInt32)
extern "C"  uint32_t _GetResourceFullPath_Invoke_m3238287078 (_GetResourceFullPath_t915790394 * __this, String_t* ___pchResourceName0, String_t* ___pchResourceTypeDirectory1, String_t* ___pchPathBuffer2, uint32_t ___unBufferLen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRResources/_GetResourceFullPath::BeginInvoke(System.String,System.String,System.String,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetResourceFullPath_BeginInvoke_m3374072904 (_GetResourceFullPath_t915790394 * __this, String_t* ___pchResourceName0, String_t* ___pchResourceTypeDirectory1, String_t* ___pchPathBuffer2, uint32_t ___unBufferLen3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRResources/_GetResourceFullPath::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetResourceFullPath_EndInvoke_m1714703720 (_GetResourceFullPath_t915790394 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
