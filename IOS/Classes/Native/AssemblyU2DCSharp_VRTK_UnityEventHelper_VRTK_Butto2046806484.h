﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen2777712310.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_Button_UnityEvents/UnityObjectEvent
struct  UnityObjectEvent_t2046806484  : public UnityEvent_2_t2777712310
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
