﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.CircularDrive
struct CircularDrive_t2658775813;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// SteamVR_Controller/Device
struct Device_t2885069456;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"
#include "AssemblyU2DCSharp_SteamVR_Controller_Device2885069456.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void Valve.VR.InteractionSystem.CircularDrive::.ctor()
extern "C"  void CircularDrive__ctor_m1690438055 (CircularDrive_t2658775813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive::Freeze(Valve.VR.InteractionSystem.Hand)
extern "C"  void CircularDrive_Freeze_m4092733470 (CircularDrive_t2658775813 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive::UnFreeze()
extern "C"  void CircularDrive_UnFreeze_m2611071543 (CircularDrive_t2658775813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive::Start()
extern "C"  void CircularDrive_Start_m3131050043 (CircularDrive_t2658775813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive::OnDisable()
extern "C"  void CircularDrive_OnDisable_m295992558 (CircularDrive_t2658775813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Valve.VR.InteractionSystem.CircularDrive::HapticPulses(SteamVR_Controller/Device,System.Single,System.Int32)
extern "C"  Il2CppObject * CircularDrive_HapticPulses_m2999354832 (CircularDrive_t2658775813 * __this, Device_t2885069456 * ___controller0, float ___flMagnitude1, int32_t ___nCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive::OnHandHoverBegin(Valve.VR.InteractionSystem.Hand)
extern "C"  void CircularDrive_OnHandHoverBegin_m3003846870 (CircularDrive_t2658775813 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive::OnHandHoverEnd(Valve.VR.InteractionSystem.Hand)
extern "C"  void CircularDrive_OnHandHoverEnd_m2136038934 (CircularDrive_t2658775813 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive::HandHoverUpdate(Valve.VR.InteractionSystem.Hand)
extern "C"  void CircularDrive_HandHoverUpdate_m1749171789 (CircularDrive_t2658775813 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Valve.VR.InteractionSystem.CircularDrive::ComputeToTransformProjected(UnityEngine.Transform)
extern "C"  Vector3_t2243707580  CircularDrive_ComputeToTransformProjected_m1177166716 (CircularDrive_t2658775813 * __this, Transform_t3275118058 * ___xForm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive::DrawDebugPath(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  void CircularDrive_DrawDebugPath_m4035714341 (CircularDrive_t2658775813 * __this, Transform_t3275118058 * ___xForm0, Vector3_t2243707580  ___toTransformProjected1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive::UpdateLinearMapping()
extern "C"  void CircularDrive_UpdateLinearMapping_m1476477111 (CircularDrive_t2658775813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive::UpdateGameObject()
extern "C"  void CircularDrive_UpdateGameObject_m3411482285 (CircularDrive_t2658775813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive::UpdateDebugText()
extern "C"  void CircularDrive_UpdateDebugText_m780769838 (CircularDrive_t2658775813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive::UpdateAll()
extern "C"  void CircularDrive_UpdateAll_m9082347 (CircularDrive_t2658775813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.CircularDrive::ComputeAngle(Valve.VR.InteractionSystem.Hand)
extern "C"  void CircularDrive_ComputeAngle_m3475236033 (CircularDrive_t2658775813 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
