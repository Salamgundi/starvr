﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_Fade
struct SteamVR_Fade_t3745867721;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void SteamVR_Fade::.ctor()
extern "C"  void SteamVR_Fade__ctor_m2442321418 (SteamVR_Fade_t3745867721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Fade::Start(UnityEngine.Color,System.Single,System.Boolean)
extern "C"  void SteamVR_Fade_Start_m1232161388 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___newColor0, float ___duration1, bool ___fadeOverlay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Fade::View(UnityEngine.Color,System.Single)
extern "C"  void SteamVR_Fade_View_m1604859008 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___newColor0, float ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Fade::OnStartFade(UnityEngine.Color,System.Single,System.Boolean)
extern "C"  void SteamVR_Fade_OnStartFade_m2757824643 (SteamVR_Fade_t3745867721 * __this, Color_t2020392075  ___newColor0, float ___duration1, bool ___fadeOverlay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Fade::OnEnable()
extern "C"  void SteamVR_Fade_OnEnable_m3867337930 (SteamVR_Fade_t3745867721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Fade::OnDisable()
extern "C"  void SteamVR_Fade_OnDisable_m842331573 (SteamVR_Fade_t3745867721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Fade::OnPostRender()
extern "C"  void SteamVR_Fade_OnPostRender_m1777528563 (SteamVR_Fade_t3745867721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_Fade::.cctor()
extern "C"  void SteamVR_Fade__cctor_m1220463579 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
