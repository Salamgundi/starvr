﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material[]
struct MaterialU5BU5D_t3123989686;
// SteamVR_RenderModel[]
struct SteamVR_RenderModelU5BU5D_t2767109567;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;
// System.Collections.Generic.List`1<UnityEngine.MeshRenderer>
struct List_1_t637362236;
// System.Collections.Generic.List`1<Valve.VR.InteractionSystem.SpawnRenderModel>
struct List_1_t585697636;
// SteamVR_Events/Action
struct Action_t1836998693;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.SpawnRenderModel
struct  SpawnRenderModel_t1216576504  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material[] Valve.VR.InteractionSystem.SpawnRenderModel::materials
	MaterialU5BU5D_t3123989686* ___materials_2;
	// SteamVR_RenderModel[] Valve.VR.InteractionSystem.SpawnRenderModel::renderModels
	SteamVR_RenderModelU5BU5D_t2767109567* ___renderModels_3;
	// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.SpawnRenderModel::hand
	Hand_t379716353 * ___hand_4;
	// System.Collections.Generic.List`1<UnityEngine.MeshRenderer> Valve.VR.InteractionSystem.SpawnRenderModel::renderers
	List_1_t637362236 * ___renderers_5;
	// SteamVR_Events/Action Valve.VR.InteractionSystem.SpawnRenderModel::renderModelLoadedAction
	Action_t1836998693 * ___renderModelLoadedAction_9;

public:
	inline static int32_t get_offset_of_materials_2() { return static_cast<int32_t>(offsetof(SpawnRenderModel_t1216576504, ___materials_2)); }
	inline MaterialU5BU5D_t3123989686* get_materials_2() const { return ___materials_2; }
	inline MaterialU5BU5D_t3123989686** get_address_of_materials_2() { return &___materials_2; }
	inline void set_materials_2(MaterialU5BU5D_t3123989686* value)
	{
		___materials_2 = value;
		Il2CppCodeGenWriteBarrier(&___materials_2, value);
	}

	inline static int32_t get_offset_of_renderModels_3() { return static_cast<int32_t>(offsetof(SpawnRenderModel_t1216576504, ___renderModels_3)); }
	inline SteamVR_RenderModelU5BU5D_t2767109567* get_renderModels_3() const { return ___renderModels_3; }
	inline SteamVR_RenderModelU5BU5D_t2767109567** get_address_of_renderModels_3() { return &___renderModels_3; }
	inline void set_renderModels_3(SteamVR_RenderModelU5BU5D_t2767109567* value)
	{
		___renderModels_3 = value;
		Il2CppCodeGenWriteBarrier(&___renderModels_3, value);
	}

	inline static int32_t get_offset_of_hand_4() { return static_cast<int32_t>(offsetof(SpawnRenderModel_t1216576504, ___hand_4)); }
	inline Hand_t379716353 * get_hand_4() const { return ___hand_4; }
	inline Hand_t379716353 ** get_address_of_hand_4() { return &___hand_4; }
	inline void set_hand_4(Hand_t379716353 * value)
	{
		___hand_4 = value;
		Il2CppCodeGenWriteBarrier(&___hand_4, value);
	}

	inline static int32_t get_offset_of_renderers_5() { return static_cast<int32_t>(offsetof(SpawnRenderModel_t1216576504, ___renderers_5)); }
	inline List_1_t637362236 * get_renderers_5() const { return ___renderers_5; }
	inline List_1_t637362236 ** get_address_of_renderers_5() { return &___renderers_5; }
	inline void set_renderers_5(List_1_t637362236 * value)
	{
		___renderers_5 = value;
		Il2CppCodeGenWriteBarrier(&___renderers_5, value);
	}

	inline static int32_t get_offset_of_renderModelLoadedAction_9() { return static_cast<int32_t>(offsetof(SpawnRenderModel_t1216576504, ___renderModelLoadedAction_9)); }
	inline Action_t1836998693 * get_renderModelLoadedAction_9() const { return ___renderModelLoadedAction_9; }
	inline Action_t1836998693 ** get_address_of_renderModelLoadedAction_9() { return &___renderModelLoadedAction_9; }
	inline void set_renderModelLoadedAction_9(Action_t1836998693 * value)
	{
		___renderModelLoadedAction_9 = value;
		Il2CppCodeGenWriteBarrier(&___renderModelLoadedAction_9, value);
	}
};

struct SpawnRenderModel_t1216576504_StaticFields
{
public:
	// System.Collections.Generic.List`1<Valve.VR.InteractionSystem.SpawnRenderModel> Valve.VR.InteractionSystem.SpawnRenderModel::spawnRenderModels
	List_1_t585697636 * ___spawnRenderModels_6;
	// System.Int32 Valve.VR.InteractionSystem.SpawnRenderModel::lastFrameUpdated
	int32_t ___lastFrameUpdated_7;
	// System.Int32 Valve.VR.InteractionSystem.SpawnRenderModel::spawnRenderModelUpdateIndex
	int32_t ___spawnRenderModelUpdateIndex_8;

public:
	inline static int32_t get_offset_of_spawnRenderModels_6() { return static_cast<int32_t>(offsetof(SpawnRenderModel_t1216576504_StaticFields, ___spawnRenderModels_6)); }
	inline List_1_t585697636 * get_spawnRenderModels_6() const { return ___spawnRenderModels_6; }
	inline List_1_t585697636 ** get_address_of_spawnRenderModels_6() { return &___spawnRenderModels_6; }
	inline void set_spawnRenderModels_6(List_1_t585697636 * value)
	{
		___spawnRenderModels_6 = value;
		Il2CppCodeGenWriteBarrier(&___spawnRenderModels_6, value);
	}

	inline static int32_t get_offset_of_lastFrameUpdated_7() { return static_cast<int32_t>(offsetof(SpawnRenderModel_t1216576504_StaticFields, ___lastFrameUpdated_7)); }
	inline int32_t get_lastFrameUpdated_7() const { return ___lastFrameUpdated_7; }
	inline int32_t* get_address_of_lastFrameUpdated_7() { return &___lastFrameUpdated_7; }
	inline void set_lastFrameUpdated_7(int32_t value)
	{
		___lastFrameUpdated_7 = value;
	}

	inline static int32_t get_offset_of_spawnRenderModelUpdateIndex_8() { return static_cast<int32_t>(offsetof(SpawnRenderModel_t1216576504_StaticFields, ___spawnRenderModelUpdateIndex_8)); }
	inline int32_t get_spawnRenderModelUpdateIndex_8() const { return ___spawnRenderModelUpdateIndex_8; }
	inline int32_t* get_address_of_spawnRenderModelUpdateIndex_8() { return &___spawnRenderModelUpdateIndex_8; }
	inline void set_spawnRenderModelUpdateIndex_8(int32_t value)
	{
		___spawnRenderModelUpdateIndex_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
