﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragMe
struct  DragMe_t1948526048  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean DragMe::dragOnSurfaces
	bool ___dragOnSurfaces_2;
	// UnityEngine.GameObject DragMe::m_DraggingIcon
	GameObject_t1756533147 * ___m_DraggingIcon_3;
	// UnityEngine.RectTransform DragMe::m_DraggingPlane
	RectTransform_t3349966182 * ___m_DraggingPlane_4;

public:
	inline static int32_t get_offset_of_dragOnSurfaces_2() { return static_cast<int32_t>(offsetof(DragMe_t1948526048, ___dragOnSurfaces_2)); }
	inline bool get_dragOnSurfaces_2() const { return ___dragOnSurfaces_2; }
	inline bool* get_address_of_dragOnSurfaces_2() { return &___dragOnSurfaces_2; }
	inline void set_dragOnSurfaces_2(bool value)
	{
		___dragOnSurfaces_2 = value;
	}

	inline static int32_t get_offset_of_m_DraggingIcon_3() { return static_cast<int32_t>(offsetof(DragMe_t1948526048, ___m_DraggingIcon_3)); }
	inline GameObject_t1756533147 * get_m_DraggingIcon_3() const { return ___m_DraggingIcon_3; }
	inline GameObject_t1756533147 ** get_address_of_m_DraggingIcon_3() { return &___m_DraggingIcon_3; }
	inline void set_m_DraggingIcon_3(GameObject_t1756533147 * value)
	{
		___m_DraggingIcon_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_DraggingIcon_3, value);
	}

	inline static int32_t get_offset_of_m_DraggingPlane_4() { return static_cast<int32_t>(offsetof(DragMe_t1948526048, ___m_DraggingPlane_4)); }
	inline RectTransform_t3349966182 * get_m_DraggingPlane_4() const { return ___m_DraggingPlane_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_DraggingPlane_4() { return &___m_DraggingPlane_4; }
	inline void set_m_DraggingPlane_4(RectTransform_t3349966182 * value)
	{
		___m_DraggingPlane_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_DraggingPlane_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
