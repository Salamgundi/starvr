﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.LinearDisplacement
struct LinearDisplacement_t2578409520;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.LinearDisplacement::.ctor()
extern "C"  void LinearDisplacement__ctor_m886609008 (LinearDisplacement_t2578409520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearDisplacement::Start()
extern "C"  void LinearDisplacement_Start_m2828410336 (LinearDisplacement_t2578409520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.LinearDisplacement::Update()
extern "C"  void LinearDisplacement_Update_m252532005 (LinearDisplacement_t2578409520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
