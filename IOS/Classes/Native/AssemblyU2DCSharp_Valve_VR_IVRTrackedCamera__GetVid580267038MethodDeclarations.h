﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRTrackedCamera/_GetVideoStreamFrameBuffer
struct _GetVideoStreamFrameBuffer_t580267038;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraError3529690400.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraFrameTy2003723073.h"
#include "AssemblyU2DCSharp_Valve_VR_CameraVideoStreamFrameHe968213647.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRTrackedCamera/_GetVideoStreamFrameBuffer::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetVideoStreamFrameBuffer__ctor_m2620925139 (_GetVideoStreamFrameBuffer_t580267038 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_GetVideoStreamFrameBuffer::Invoke(System.UInt64,Valve.VR.EVRTrackedCameraFrameType,System.IntPtr,System.UInt32,Valve.VR.CameraVideoStreamFrameHeader_t&,System.UInt32)
extern "C"  int32_t _GetVideoStreamFrameBuffer_Invoke_m2929458219 (_GetVideoStreamFrameBuffer_t580267038 * __this, uint64_t ___hTrackedCamera0, int32_t ___eFrameType1, IntPtr_t ___pFrameBuffer2, uint32_t ___nFrameBufferSize3, CameraVideoStreamFrameHeader_t_t968213647 * ___pFrameHeader4, uint32_t ___nFrameHeaderSize5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRTrackedCamera/_GetVideoStreamFrameBuffer::BeginInvoke(System.UInt64,Valve.VR.EVRTrackedCameraFrameType,System.IntPtr,System.UInt32,Valve.VR.CameraVideoStreamFrameHeader_t&,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetVideoStreamFrameBuffer_BeginInvoke_m1602675561 (_GetVideoStreamFrameBuffer_t580267038 * __this, uint64_t ___hTrackedCamera0, int32_t ___eFrameType1, IntPtr_t ___pFrameBuffer2, uint32_t ___nFrameBufferSize3, CameraVideoStreamFrameHeader_t_t968213647 * ___pFrameHeader4, uint32_t ___nFrameHeaderSize5, AsyncCallback_t163412349 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_GetVideoStreamFrameBuffer::EndInvoke(Valve.VR.CameraVideoStreamFrameHeader_t&,System.IAsyncResult)
extern "C"  int32_t _GetVideoStreamFrameBuffer_EndInvoke_m3107335807 (_GetVideoStreamFrameBuffer_t580267038 * __this, CameraVideoStreamFrameHeader_t_t968213647 * ___pFrameHeader0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
