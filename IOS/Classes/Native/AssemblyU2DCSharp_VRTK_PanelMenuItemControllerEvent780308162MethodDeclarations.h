﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.PanelMenuItemControllerEventHandler
struct PanelMenuItemControllerEventHandler_t780308162;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_PanelMenuItemControllerEven2917504033.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.PanelMenuItemControllerEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void PanelMenuItemControllerEventHandler__ctor_m237879972 (PanelMenuItemControllerEventHandler_t780308162 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemControllerEventHandler::Invoke(System.Object,VRTK.PanelMenuItemControllerEventArgs)
extern "C"  void PanelMenuItemControllerEventHandler_Invoke_m1761430084 (PanelMenuItemControllerEventHandler_t780308162 * __this, Il2CppObject * ___sender0, PanelMenuItemControllerEventArgs_t2917504033  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.PanelMenuItemControllerEventHandler::BeginInvoke(System.Object,VRTK.PanelMenuItemControllerEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PanelMenuItemControllerEventHandler_BeginInvoke_m973201121 (PanelMenuItemControllerEventHandler_t780308162 * __this, Il2CppObject * ___sender0, PanelMenuItemControllerEventArgs_t2917504033  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.PanelMenuItemControllerEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void PanelMenuItemControllerEventHandler_EndInvoke_m2125890318 (PanelMenuItemControllerEventHandler_t780308162 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
