﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_LoadTextureD3D11_Async
struct _LoadTextureD3D11_Async_t2681806282;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRRenderModelError21703732.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_LoadTextureD3D11_Async::.ctor(System.Object,System.IntPtr)
extern "C"  void _LoadTextureD3D11_Async__ctor_m2012365743 (_LoadTextureD3D11_Async_t2681806282 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRRenderModelError Valve.VR.IVRRenderModels/_LoadTextureD3D11_Async::Invoke(System.Int32,System.IntPtr,System.IntPtr&)
extern "C"  int32_t _LoadTextureD3D11_Async_Invoke_m285591107 (_LoadTextureD3D11_Async_t2681806282 * __this, int32_t ___textureId0, IntPtr_t ___pD3D11Device1, IntPtr_t* ___ppD3D11Texture2D2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_LoadTextureD3D11_Async::BeginInvoke(System.Int32,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _LoadTextureD3D11_Async_BeginInvoke_m766226979 (_LoadTextureD3D11_Async_t2681806282 * __this, int32_t ___textureId0, IntPtr_t ___pD3D11Device1, IntPtr_t* ___ppD3D11Texture2D2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRRenderModelError Valve.VR.IVRRenderModels/_LoadTextureD3D11_Async::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern "C"  int32_t _LoadTextureD3D11_Async_EndInvoke_m1875304892 (_LoadTextureD3D11_Async_t2681806282 * __this, IntPtr_t* ___ppD3D11Texture2D0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
