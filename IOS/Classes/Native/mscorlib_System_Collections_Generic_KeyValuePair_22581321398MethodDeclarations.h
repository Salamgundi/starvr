﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_258602264MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2180775198(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2581321398 *, String_t*, uint64_t, const MethodInfo*))KeyValuePair_2__ctor_m814106894_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>::get_Key()
#define KeyValuePair_2_get_Key_m1795993507(__this, method) ((  String_t* (*) (KeyValuePair_2_t2581321398 *, const MethodInfo*))KeyValuePair_2_get_Key_m311169592_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2138029757(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2581321398 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m3307037513_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>::get_Value()
#define KeyValuePair_2_get_Value_m2331664688(__this, method) ((  uint64_t (*) (KeyValuePair_2_t2581321398 *, const MethodInfo*))KeyValuePair_2_get_Value_m689648760_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1774418421(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2581321398 *, uint64_t, const MethodInfo*))KeyValuePair_2_set_Value_m2707677985_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.UInt64>::ToString()
#define KeyValuePair_2_ToString_m1098098143(__this, method) ((  String_t* (*) (KeyValuePair_2_t2581321398 *, const MethodInfo*))KeyValuePair_2_ToString_m2421585715_gshared)(__this, method)
