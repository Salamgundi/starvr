﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClickedEventHandler
struct ClickedEventHandler_t1112331409;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_ClickedEventArgs2917034410.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ClickedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ClickedEventHandler__ctor_m3671415200 (ClickedEventHandler_t1112331409 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickedEventHandler::Invoke(System.Object,ClickedEventArgs)
extern "C"  void ClickedEventHandler_Invoke_m3883410454 (ClickedEventHandler_t1112331409 * __this, Il2CppObject * ___sender0, ClickedEventArgs_t2917034410  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ClickedEventHandler::BeginInvoke(System.Object,ClickedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ClickedEventHandler_BeginInvoke_m439103693 (ClickedEventHandler_t1112331409 * __this, Il2CppObject * ___sender0, ClickedEventArgs_t2917034410  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ClickedEventHandler_EndInvoke_m3311229034 (ClickedEventHandler_t1112331409 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
