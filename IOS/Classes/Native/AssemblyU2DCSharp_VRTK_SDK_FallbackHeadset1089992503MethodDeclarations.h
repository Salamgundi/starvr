﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_FallbackHeadset
struct SDK_FallbackHeadset_t1089992503;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void VRTK.SDK_FallbackHeadset::.ctor()
extern "C"  void SDK_FallbackHeadset__ctor_m1271950923 (SDK_FallbackHeadset_t1089992503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_FallbackHeadset::ProcessUpdate(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void SDK_FallbackHeadset_ProcessUpdate_m397505376 (SDK_FallbackHeadset_t1089992503 * __this, Dictionary_2_t309261261 * ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.SDK_FallbackHeadset::GetHeadset()
extern "C"  Transform_t3275118058 * SDK_FallbackHeadset_GetHeadset_m2715322947 (SDK_FallbackHeadset_t1089992503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.SDK_FallbackHeadset::GetHeadsetCamera()
extern "C"  Transform_t3275118058 * SDK_FallbackHeadset_GetHeadsetCamera_m912963864 (SDK_FallbackHeadset_t1089992503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.SDK_FallbackHeadset::GetHeadsetVelocity()
extern "C"  Vector3_t2243707580  SDK_FallbackHeadset_GetHeadsetVelocity_m2628716376 (SDK_FallbackHeadset_t1089992503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.SDK_FallbackHeadset::GetHeadsetAngularVelocity()
extern "C"  Vector3_t2243707580  SDK_FallbackHeadset_GetHeadsetAngularVelocity_m1554790622 (SDK_FallbackHeadset_t1089992503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_FallbackHeadset::HeadsetFade(UnityEngine.Color,System.Single,System.Boolean)
extern "C"  void SDK_FallbackHeadset_HeadsetFade_m3637898315 (SDK_FallbackHeadset_t1089992503 * __this, Color_t2020392075  ___color0, float ___duration1, bool ___fadeOverlay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.SDK_FallbackHeadset::HasHeadsetFade(UnityEngine.Transform)
extern "C"  bool SDK_FallbackHeadset_HasHeadsetFade_m140700726 (SDK_FallbackHeadset_t1089992503 * __this, Transform_t3275118058 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_FallbackHeadset::AddHeadsetFade(UnityEngine.Transform)
extern "C"  void SDK_FallbackHeadset_AddHeadsetFade_m1531522349 (SDK_FallbackHeadset_t1089992503 * __this, Transform_t3275118058 * ___camera0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.SDK_FallbackHeadset::Awake()
extern "C"  void SDK_FallbackHeadset_Awake_m1906311280 (SDK_FallbackHeadset_t1089992503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
