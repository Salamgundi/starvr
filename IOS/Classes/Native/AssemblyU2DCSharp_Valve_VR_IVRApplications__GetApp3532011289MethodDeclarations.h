﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetApplicationPropertyUint64
struct _GetApplicationPropertyUint64_t3532011289;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationProperty1959780520.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationError862086677.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetApplicationPropertyUint64::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetApplicationPropertyUint64__ctor_m164503228 (_GetApplicationPropertyUint64_t3532011289 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.IVRApplications/_GetApplicationPropertyUint64::Invoke(System.String,Valve.VR.EVRApplicationProperty,Valve.VR.EVRApplicationError&)
extern "C"  uint64_t _GetApplicationPropertyUint64_Invoke_m1879722055 (_GetApplicationPropertyUint64_t3532011289 * __this, String_t* ___pchAppKey0, int32_t ___eProperty1, int32_t* ___peError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetApplicationPropertyUint64::BeginInvoke(System.String,Valve.VR.EVRApplicationProperty,Valve.VR.EVRApplicationError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetApplicationPropertyUint64_BeginInvoke_m1817225132 (_GetApplicationPropertyUint64_t3532011289 * __this, String_t* ___pchAppKey0, int32_t ___eProperty1, int32_t* ___peError2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.IVRApplications/_GetApplicationPropertyUint64::EndInvoke(Valve.VR.EVRApplicationError&,System.IAsyncResult)
extern "C"  uint64_t _GetApplicationPropertyUint64_EndInvoke_m1010930767 (_GetApplicationPropertyUint64_t3532011289 * __this, int32_t* ___peError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
