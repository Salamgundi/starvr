﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.RealGun_Slide
struct RealGun_Slide_t2998553338;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.Examples.RealGun_Slide::.ctor()
extern "C"  void RealGun_Slide__ctor_m2888574085 (RealGun_Slide_t2998553338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun_Slide::Fire()
extern "C"  void RealGun_Slide_Fire_m2005673221 (RealGun_Slide_t2998553338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun_Slide::Awake()
extern "C"  void RealGun_Slide_Awake_m2660685284 (RealGun_Slide_t2998553338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.RealGun_Slide::Update()
extern "C"  void RealGun_Slide_Update_m370763714 (RealGun_Slide_t2998553338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
