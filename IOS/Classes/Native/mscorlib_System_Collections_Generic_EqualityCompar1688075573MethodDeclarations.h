﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Valve.VR.EVRButtonId>
struct DefaultComparer_t1688075573;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRButtonId66145412.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Valve.VR.EVRButtonId>::.ctor()
extern "C"  void DefaultComparer__ctor_m3611469924_gshared (DefaultComparer_t1688075573 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3611469924(__this, method) ((  void (*) (DefaultComparer_t1688075573 *, const MethodInfo*))DefaultComparer__ctor_m3611469924_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Valve.VR.EVRButtonId>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1478697909_gshared (DefaultComparer_t1688075573 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1478697909(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1688075573 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1478697909_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Valve.VR.EVRButtonId>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m36679785_gshared (DefaultComparer_t1688075573 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m36679785(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1688075573 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m36679785_gshared)(__this, ___x0, ___y1, method)
