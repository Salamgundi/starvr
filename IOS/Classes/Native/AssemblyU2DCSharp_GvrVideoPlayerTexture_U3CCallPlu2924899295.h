﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1785723201;
// GvrVideoPlayerTexture
struct GvrVideoPlayerTexture_t673526704;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1
struct  U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295  : public Il2CppObject
{
public:
	// System.Boolean GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::<running>__0
	bool ___U3CrunningU3E__0_0;
	// UnityEngine.WaitForEndOfFrame GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::<wfeof>__1
	WaitForEndOfFrame_t1785723201 * ___U3CwfeofU3E__1_1;
	// System.IntPtr GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::<tex>__2
	IntPtr_t ___U3CtexU3E__2_2;
	// System.Int32 GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::<w>__3
	int32_t ___U3CwU3E__3_3;
	// System.Int32 GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::<h>__4
	int32_t ___U3ChU3E__4_4;
	// System.Int64 GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::<bp>__5
	int64_t ___U3CbpU3E__5_5;
	// GvrVideoPlayerTexture GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::$this
	GvrVideoPlayerTexture_t673526704 * ___U24this_6;
	// System.Object GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::$disposing
	bool ___U24disposing_8;
	// System.Int32 GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CrunningU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___U3CrunningU3E__0_0)); }
	inline bool get_U3CrunningU3E__0_0() const { return ___U3CrunningU3E__0_0; }
	inline bool* get_address_of_U3CrunningU3E__0_0() { return &___U3CrunningU3E__0_0; }
	inline void set_U3CrunningU3E__0_0(bool value)
	{
		___U3CrunningU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CwfeofU3E__1_1() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___U3CwfeofU3E__1_1)); }
	inline WaitForEndOfFrame_t1785723201 * get_U3CwfeofU3E__1_1() const { return ___U3CwfeofU3E__1_1; }
	inline WaitForEndOfFrame_t1785723201 ** get_address_of_U3CwfeofU3E__1_1() { return &___U3CwfeofU3E__1_1; }
	inline void set_U3CwfeofU3E__1_1(WaitForEndOfFrame_t1785723201 * value)
	{
		___U3CwfeofU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwfeofU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CtexU3E__2_2() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___U3CtexU3E__2_2)); }
	inline IntPtr_t get_U3CtexU3E__2_2() const { return ___U3CtexU3E__2_2; }
	inline IntPtr_t* get_address_of_U3CtexU3E__2_2() { return &___U3CtexU3E__2_2; }
	inline void set_U3CtexU3E__2_2(IntPtr_t value)
	{
		___U3CtexU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CwU3E__3_3() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___U3CwU3E__3_3)); }
	inline int32_t get_U3CwU3E__3_3() const { return ___U3CwU3E__3_3; }
	inline int32_t* get_address_of_U3CwU3E__3_3() { return &___U3CwU3E__3_3; }
	inline void set_U3CwU3E__3_3(int32_t value)
	{
		___U3CwU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3ChU3E__4_4() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___U3ChU3E__4_4)); }
	inline int32_t get_U3ChU3E__4_4() const { return ___U3ChU3E__4_4; }
	inline int32_t* get_address_of_U3ChU3E__4_4() { return &___U3ChU3E__4_4; }
	inline void set_U3ChU3E__4_4(int32_t value)
	{
		___U3ChU3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CbpU3E__5_5() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___U3CbpU3E__5_5)); }
	inline int64_t get_U3CbpU3E__5_5() const { return ___U3CbpU3E__5_5; }
	inline int64_t* get_address_of_U3CbpU3E__5_5() { return &___U3CbpU3E__5_5; }
	inline void set_U3CbpU3E__5_5(int64_t value)
	{
		___U3CbpU3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___U24this_6)); }
	inline GvrVideoPlayerTexture_t673526704 * get_U24this_6() const { return ___U24this_6; }
	inline GvrVideoPlayerTexture_t673526704 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(GvrVideoPlayerTexture_t673526704 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
