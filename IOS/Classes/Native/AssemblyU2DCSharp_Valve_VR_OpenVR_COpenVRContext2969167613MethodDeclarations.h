﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.OpenVR/COpenVRContext
struct COpenVRContext_t2969167613;
// Valve.VR.CVRSystem
struct CVRSystem_t1953699154;
// Valve.VR.CVRChaperone
struct CVRChaperone_t441701222;
// Valve.VR.CVRChaperoneSetup
struct CVRChaperoneSetup_t1611144107;
// Valve.VR.CVRCompositor
struct CVRCompositor_t197946050;
// Valve.VR.CVROverlay
struct CVROverlay_t3377499315;
// Valve.VR.CVRRenderModels
struct CVRRenderModels_t2019937239;
// Valve.VR.CVRExtendedDisplay
struct CVRExtendedDisplay_t1925229748;
// Valve.VR.CVRSettings
struct CVRSettings_t3592067458;
// Valve.VR.CVRApplications
struct CVRApplications_t1900926488;
// Valve.VR.CVRScreenshots
struct CVRScreenshots_t3241040508;
// Valve.VR.CVRTrackedCamera
struct CVRTrackedCamera_t2050215972;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.OpenVR/COpenVRContext::.ctor()
extern "C"  void COpenVRContext__ctor_m1818826262 (COpenVRContext_t2969167613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.OpenVR/COpenVRContext::Clear()
extern "C"  void COpenVRContext_Clear_m268569461 (COpenVRContext_t2969167613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.OpenVR/COpenVRContext::CheckClear()
extern "C"  void COpenVRContext_CheckClear_m1647284583 (COpenVRContext_t2969167613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRSystem Valve.VR.OpenVR/COpenVRContext::VRSystem()
extern "C"  CVRSystem_t1953699154 * COpenVRContext_VRSystem_m1575893516 (COpenVRContext_t2969167613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRChaperone Valve.VR.OpenVR/COpenVRContext::VRChaperone()
extern "C"  CVRChaperone_t441701222 * COpenVRContext_VRChaperone_m4058438856 (COpenVRContext_t2969167613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRChaperoneSetup Valve.VR.OpenVR/COpenVRContext::VRChaperoneSetup()
extern "C"  CVRChaperoneSetup_t1611144107 * COpenVRContext_VRChaperoneSetup_m2191672286 (COpenVRContext_t2969167613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRCompositor Valve.VR.OpenVR/COpenVRContext::VRCompositor()
extern "C"  CVRCompositor_t197946050 * COpenVRContext_VRCompositor_m618695020 (COpenVRContext_t2969167613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVROverlay Valve.VR.OpenVR/COpenVRContext::VROverlay()
extern "C"  CVROverlay_t3377499315 * COpenVRContext_VROverlay_m2864177768 (COpenVRContext_t2969167613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRRenderModels Valve.VR.OpenVR/COpenVRContext::VRRenderModels()
extern "C"  CVRRenderModels_t2019937239 * COpenVRContext_VRRenderModels_m3521916894 (COpenVRContext_t2969167613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRExtendedDisplay Valve.VR.OpenVR/COpenVRContext::VRExtendedDisplay()
extern "C"  CVRExtendedDisplay_t1925229748 * COpenVRContext_VRExtendedDisplay_m4102313992 (COpenVRContext_t2969167613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRSettings Valve.VR.OpenVR/COpenVRContext::VRSettings()
extern "C"  CVRSettings_t3592067458 * COpenVRContext_VRSettings_m66870476 (COpenVRContext_t2969167613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRApplications Valve.VR.OpenVR/COpenVRContext::VRApplications()
extern "C"  CVRApplications_t1900926488 * COpenVRContext_VRApplications_m3223396700 (COpenVRContext_t2969167613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRScreenshots Valve.VR.OpenVR/COpenVRContext::VRScreenshots()
extern "C"  CVRScreenshots_t3241040508 * COpenVRContext_VRScreenshots_m76994888 (COpenVRContext_t2969167613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.CVRTrackedCamera Valve.VR.OpenVR/COpenVRContext::VRTrackedCamera()
extern "C"  CVRTrackedCamera_t2050215972 * COpenVRContext_VRTrackedCamera_m171198472 (COpenVRContext_t2969167613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
