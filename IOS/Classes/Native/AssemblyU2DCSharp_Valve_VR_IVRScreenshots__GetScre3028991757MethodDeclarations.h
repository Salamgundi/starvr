﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRScreenshots/_GetScreenshotPropertyType
struct _GetScreenshotPropertyType_t3028991757;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotType611740195.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRScreenshotError1400268927.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRScreenshots/_GetScreenshotPropertyType::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetScreenshotPropertyType__ctor_m1910313476 (_GetScreenshotPropertyType_t3028991757 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotType Valve.VR.IVRScreenshots/_GetScreenshotPropertyType::Invoke(System.UInt32,Valve.VR.EVRScreenshotError&)
extern "C"  int32_t _GetScreenshotPropertyType_Invoke_m1382846401 (_GetScreenshotPropertyType_t3028991757 * __this, uint32_t ___screenshotHandle0, int32_t* ___pError1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRScreenshots/_GetScreenshotPropertyType::BeginInvoke(System.UInt32,Valve.VR.EVRScreenshotError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetScreenshotPropertyType_BeginInvoke_m4002242868 (_GetScreenshotPropertyType_t3028991757 * __this, uint32_t ___screenshotHandle0, int32_t* ___pError1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRScreenshotType Valve.VR.IVRScreenshots/_GetScreenshotPropertyType::EndInvoke(Valve.VR.EVRScreenshotError&,System.IAsyncResult)
extern "C"  int32_t _GetScreenshotPropertyType_EndInvoke_m1385256291 (_GetScreenshotPropertyType_t3028991757 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
