﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_Submit
struct _Submit_t938257482;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRCompositorError3948578210.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREye3088716538.h"
#include "AssemblyU2DCSharp_Valve_VR_Texture_t3277130850.h"
#include "AssemblyU2DCSharp_Valve_VR_VRTextureBounds_t1897807375.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRSubmitFlags2736259668.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_Submit::.ctor(System.Object,System.IntPtr)
extern "C"  void _Submit__ctor_m2116451555 (_Submit_t938257482 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.IVRCompositor/_Submit::Invoke(Valve.VR.EVREye,Valve.VR.Texture_t&,Valve.VR.VRTextureBounds_t&,Valve.VR.EVRSubmitFlags)
extern "C"  int32_t _Submit_Invoke_m480649067 (_Submit_t938257482 * __this, int32_t ___eEye0, Texture_t_t3277130850 * ___pTexture1, VRTextureBounds_t_t1897807375 * ___pBounds2, int32_t ___nSubmitFlags3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_Submit::BeginInvoke(Valve.VR.EVREye,Valve.VR.Texture_t&,Valve.VR.VRTextureBounds_t&,Valve.VR.EVRSubmitFlags,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _Submit_BeginInvoke_m1954941359 (_Submit_t938257482 * __this, int32_t ___eEye0, Texture_t_t3277130850 * ___pTexture1, VRTextureBounds_t_t1897807375 * ___pBounds2, int32_t ___nSubmitFlags3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRCompositorError Valve.VR.IVRCompositor/_Submit::EndInvoke(Valve.VR.Texture_t&,Valve.VR.VRTextureBounds_t&,System.IAsyncResult)
extern "C"  int32_t _Submit_EndInvoke_m3298641251 (_Submit_t938257482 * __this, Texture_t_t3277130850 * ___pTexture0, VRTextureBounds_t_t1897807375 * ___pBounds1, Il2CppObject * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
