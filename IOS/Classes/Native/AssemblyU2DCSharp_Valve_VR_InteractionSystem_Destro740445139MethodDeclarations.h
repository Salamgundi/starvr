﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.DestroyOnTriggerEnter
struct DestroyOnTriggerEnter_t740445139;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void Valve.VR.InteractionSystem.DestroyOnTriggerEnter::.ctor()
extern "C"  void DestroyOnTriggerEnter__ctor_m3794492033 (DestroyOnTriggerEnter_t740445139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.DestroyOnTriggerEnter::Start()
extern "C"  void DestroyOnTriggerEnter_Start_m998765897 (DestroyOnTriggerEnter_t740445139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.DestroyOnTriggerEnter::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void DestroyOnTriggerEnter_OnTriggerEnter_m3225494661 (DestroyOnTriggerEnter_t740445139 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
