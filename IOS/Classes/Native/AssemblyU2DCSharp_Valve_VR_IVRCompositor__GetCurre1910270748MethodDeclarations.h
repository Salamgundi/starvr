﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRCompositor/_GetCurrentSceneFocusProcess
struct _GetCurrentSceneFocusProcess_t1910270748;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRCompositor/_GetCurrentSceneFocusProcess::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetCurrentSceneFocusProcess__ctor_m2576044297 (_GetCurrentSceneFocusProcess_t1910270748 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRCompositor/_GetCurrentSceneFocusProcess::Invoke()
extern "C"  uint32_t _GetCurrentSceneFocusProcess_Invoke_m594063176 (_GetCurrentSceneFocusProcess_t1910270748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRCompositor/_GetCurrentSceneFocusProcess::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetCurrentSceneFocusProcess_BeginInvoke_m3919076146 (_GetCurrentSceneFocusProcess_t1910270748 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRCompositor/_GetCurrentSceneFocusProcess::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetCurrentSceneFocusProcess_EndInvoke_m2795540444 (_GetCurrentSceneFocusProcess_t1910270748 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
