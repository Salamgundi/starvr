﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_GetRenderModelErrorNameFromEnum
struct _GetRenderModelErrorNameFromEnum_t298277168;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRRenderModelError21703732.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_GetRenderModelErrorNameFromEnum::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetRenderModelErrorNameFromEnum__ctor_m3211935049 (_GetRenderModelErrorNameFromEnum_t298277168 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRRenderModels/_GetRenderModelErrorNameFromEnum::Invoke(Valve.VR.EVRRenderModelError)
extern "C"  IntPtr_t _GetRenderModelErrorNameFromEnum_Invoke_m3481395996 (_GetRenderModelErrorNameFromEnum_t298277168 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetRenderModelErrorNameFromEnum::BeginInvoke(Valve.VR.EVRRenderModelError,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetRenderModelErrorNameFromEnum_BeginInvoke_m3692303504 (_GetRenderModelErrorNameFromEnum_t298277168 * __this, int32_t ___error0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.IVRRenderModels/_GetRenderModelErrorNameFromEnum::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t _GetRenderModelErrorNameFromEnum_EndInvoke_m1717712356 (_GetRenderModelErrorNameFromEnum_t298277168 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
