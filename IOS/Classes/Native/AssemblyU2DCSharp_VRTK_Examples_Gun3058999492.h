﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.Gun
struct  Gun_t3058999492  : public VRTK_InteractableObject_t2604188111
{
public:
	// UnityEngine.GameObject VRTK.Examples.Gun::bullet
	GameObject_t1756533147 * ___bullet_45;
	// System.Single VRTK.Examples.Gun::bulletSpeed
	float ___bulletSpeed_46;
	// System.Single VRTK.Examples.Gun::bulletLife
	float ___bulletLife_47;

public:
	inline static int32_t get_offset_of_bullet_45() { return static_cast<int32_t>(offsetof(Gun_t3058999492, ___bullet_45)); }
	inline GameObject_t1756533147 * get_bullet_45() const { return ___bullet_45; }
	inline GameObject_t1756533147 ** get_address_of_bullet_45() { return &___bullet_45; }
	inline void set_bullet_45(GameObject_t1756533147 * value)
	{
		___bullet_45 = value;
		Il2CppCodeGenWriteBarrier(&___bullet_45, value);
	}

	inline static int32_t get_offset_of_bulletSpeed_46() { return static_cast<int32_t>(offsetof(Gun_t3058999492, ___bulletSpeed_46)); }
	inline float get_bulletSpeed_46() const { return ___bulletSpeed_46; }
	inline float* get_address_of_bulletSpeed_46() { return &___bulletSpeed_46; }
	inline void set_bulletSpeed_46(float value)
	{
		___bulletSpeed_46 = value;
	}

	inline static int32_t get_offset_of_bulletLife_47() { return static_cast<int32_t>(offsetof(Gun_t3058999492, ___bulletLife_47)); }
	inline float get_bulletLife_47() const { return ___bulletLife_47; }
	inline float* get_address_of_bulletLife_47() { return &___bulletLife_47; }
	inline void set_bulletLife_47(float value)
	{
		___bulletLife_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
