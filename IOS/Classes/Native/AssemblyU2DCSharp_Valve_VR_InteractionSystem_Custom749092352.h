﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen418066368.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.CustomEvents/UnityEventHand
struct  UnityEventHand_t749092352  : public UnityEvent_1_t418066368
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
