﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.LinearMapping
struct LinearMapping_t810676855;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.LinearAudioPitch
struct  LinearAudioPitch_t1253673161  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.LinearMapping Valve.VR.InteractionSystem.LinearAudioPitch::linearMapping
	LinearMapping_t810676855 * ___linearMapping_2;
	// UnityEngine.AnimationCurve Valve.VR.InteractionSystem.LinearAudioPitch::pitchCurve
	AnimationCurve_t3306541151 * ___pitchCurve_3;
	// System.Single Valve.VR.InteractionSystem.LinearAudioPitch::minPitch
	float ___minPitch_4;
	// System.Single Valve.VR.InteractionSystem.LinearAudioPitch::maxPitch
	float ___maxPitch_5;
	// System.Boolean Valve.VR.InteractionSystem.LinearAudioPitch::applyContinuously
	bool ___applyContinuously_6;
	// UnityEngine.AudioSource Valve.VR.InteractionSystem.LinearAudioPitch::audioSource
	AudioSource_t1135106623 * ___audioSource_7;

public:
	inline static int32_t get_offset_of_linearMapping_2() { return static_cast<int32_t>(offsetof(LinearAudioPitch_t1253673161, ___linearMapping_2)); }
	inline LinearMapping_t810676855 * get_linearMapping_2() const { return ___linearMapping_2; }
	inline LinearMapping_t810676855 ** get_address_of_linearMapping_2() { return &___linearMapping_2; }
	inline void set_linearMapping_2(LinearMapping_t810676855 * value)
	{
		___linearMapping_2 = value;
		Il2CppCodeGenWriteBarrier(&___linearMapping_2, value);
	}

	inline static int32_t get_offset_of_pitchCurve_3() { return static_cast<int32_t>(offsetof(LinearAudioPitch_t1253673161, ___pitchCurve_3)); }
	inline AnimationCurve_t3306541151 * get_pitchCurve_3() const { return ___pitchCurve_3; }
	inline AnimationCurve_t3306541151 ** get_address_of_pitchCurve_3() { return &___pitchCurve_3; }
	inline void set_pitchCurve_3(AnimationCurve_t3306541151 * value)
	{
		___pitchCurve_3 = value;
		Il2CppCodeGenWriteBarrier(&___pitchCurve_3, value);
	}

	inline static int32_t get_offset_of_minPitch_4() { return static_cast<int32_t>(offsetof(LinearAudioPitch_t1253673161, ___minPitch_4)); }
	inline float get_minPitch_4() const { return ___minPitch_4; }
	inline float* get_address_of_minPitch_4() { return &___minPitch_4; }
	inline void set_minPitch_4(float value)
	{
		___minPitch_4 = value;
	}

	inline static int32_t get_offset_of_maxPitch_5() { return static_cast<int32_t>(offsetof(LinearAudioPitch_t1253673161, ___maxPitch_5)); }
	inline float get_maxPitch_5() const { return ___maxPitch_5; }
	inline float* get_address_of_maxPitch_5() { return &___maxPitch_5; }
	inline void set_maxPitch_5(float value)
	{
		___maxPitch_5 = value;
	}

	inline static int32_t get_offset_of_applyContinuously_6() { return static_cast<int32_t>(offsetof(LinearAudioPitch_t1253673161, ___applyContinuously_6)); }
	inline bool get_applyContinuously_6() const { return ___applyContinuously_6; }
	inline bool* get_address_of_applyContinuously_6() { return &___applyContinuously_6; }
	inline void set_applyContinuously_6(bool value)
	{
		___applyContinuously_6 = value;
	}

	inline static int32_t get_offset_of_audioSource_7() { return static_cast<int32_t>(offsetof(LinearAudioPitch_t1253673161, ___audioSource_7)); }
	inline AudioSource_t1135106623 * get_audioSource_7() const { return ___audioSource_7; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_7() { return &___audioSource_7; }
	inline void set_audioSource_7(AudioSource_t1135106623 * value)
	{
		___audioSource_7 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
