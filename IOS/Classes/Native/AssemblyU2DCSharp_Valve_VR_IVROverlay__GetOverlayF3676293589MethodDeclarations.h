﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_GetOverlayFlag
struct _GetOverlayFlag_t3676293589;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayFlags2344570851.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_GetOverlayFlag::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetOverlayFlag__ctor_m512331406 (_GetOverlayFlag_t3676293589 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayFlag::Invoke(System.UInt64,Valve.VR.VROverlayFlags,System.Boolean&)
extern "C"  int32_t _GetOverlayFlag_Invoke_m2564400583 (_GetOverlayFlag_t3676293589 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eOverlayFlag1, bool* ___pbEnabled2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_GetOverlayFlag::BeginInvoke(System.UInt64,Valve.VR.VROverlayFlags,System.Boolean&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetOverlayFlag_BeginInvoke_m2847533122 (_GetOverlayFlag_t3676293589 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eOverlayFlag1, bool* ___pbEnabled2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_GetOverlayFlag::EndInvoke(System.Boolean&,System.IAsyncResult)
extern "C"  int32_t _GetOverlayFlag_EndInvoke_m3941304439 (_GetOverlayFlag_t3676293589 * __this, bool* ___pbEnabled0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
