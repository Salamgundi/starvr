﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_HeadsetCollision
struct VRTK_HeadsetCollision_t2015187094;
// VRTK.HeadsetCollisionEventHandler
struct HeadsetCollisionEventHandler_t3119610762;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventHandle3119610762.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventArgs1242373387.h"

// System.Void VRTK.VRTK_HeadsetCollision::.ctor()
extern "C"  void VRTK_HeadsetCollision__ctor_m633934506 (VRTK_HeadsetCollision_t2015187094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollision::add_HeadsetCollisionDetect(VRTK.HeadsetCollisionEventHandler)
extern "C"  void VRTK_HeadsetCollision_add_HeadsetCollisionDetect_m3756029740 (VRTK_HeadsetCollision_t2015187094 * __this, HeadsetCollisionEventHandler_t3119610762 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollision::remove_HeadsetCollisionDetect(VRTK.HeadsetCollisionEventHandler)
extern "C"  void VRTK_HeadsetCollision_remove_HeadsetCollisionDetect_m3187483961 (VRTK_HeadsetCollision_t2015187094 * __this, HeadsetCollisionEventHandler_t3119610762 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollision::add_HeadsetCollisionEnded(VRTK.HeadsetCollisionEventHandler)
extern "C"  void VRTK_HeadsetCollision_add_HeadsetCollisionEnded_m1130653975 (VRTK_HeadsetCollision_t2015187094 * __this, HeadsetCollisionEventHandler_t3119610762 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollision::remove_HeadsetCollisionEnded(VRTK.HeadsetCollisionEventHandler)
extern "C"  void VRTK_HeadsetCollision_remove_HeadsetCollisionEnded_m1704354028 (VRTK_HeadsetCollision_t2015187094 * __this, HeadsetCollisionEventHandler_t3119610762 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollision::OnHeadsetCollisionDetect(VRTK.HeadsetCollisionEventArgs)
extern "C"  void VRTK_HeadsetCollision_OnHeadsetCollisionDetect_m1786789988 (VRTK_HeadsetCollision_t2015187094 * __this, HeadsetCollisionEventArgs_t1242373387  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollision::OnHeadsetCollisionEnded(VRTK.HeadsetCollisionEventArgs)
extern "C"  void VRTK_HeadsetCollision_OnHeadsetCollisionEnded_m1757355483 (VRTK_HeadsetCollision_t2015187094 * __this, HeadsetCollisionEventArgs_t1242373387  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_HeadsetCollision::IsColliding()
extern "C"  bool VRTK_HeadsetCollision_IsColliding_m4169046301 (VRTK_HeadsetCollision_t2015187094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollision::OnEnable()
extern "C"  void VRTK_HeadsetCollision_OnEnable_m3243799582 (VRTK_HeadsetCollision_t2015187094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollision::OnDisable()
extern "C"  void VRTK_HeadsetCollision_OnDisable_m593433163 (VRTK_HeadsetCollision_t2015187094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollision::Update()
extern "C"  void VRTK_HeadsetCollision_Update_m4215563787 (VRTK_HeadsetCollision_t2015187094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollision::CreateHeadsetColliderContainer()
extern "C"  void VRTK_HeadsetCollision_CreateHeadsetColliderContainer_m1980726969 (VRTK_HeadsetCollision_t2015187094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollision::SetupHeadset()
extern "C"  void VRTK_HeadsetCollision_SetupHeadset_m1854427279 (VRTK_HeadsetCollision_t2015187094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_HeadsetCollision::TearDownHeadset()
extern "C"  void VRTK_HeadsetCollision_TearDownHeadset_m3548410440 (VRTK_HeadsetCollision_t2015187094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
