﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t447141393;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.UnityEventHelper.VRTK_SnapDropZone_UnityEvents/UnityObjectEvent::.ctor()
extern "C"  void UnityObjectEvent__ctor_m3994519736 (UnityObjectEvent_t447141393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
