﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Valve.VR.EVRButtonId,System.Object>
struct Dictionary_2_t3947503238;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En972560644.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21704848460.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRButtonId66145412.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4054083607_gshared (Enumerator_t972560644 * __this, Dictionary_2_t3947503238 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m4054083607(__this, ___dictionary0, method) ((  void (*) (Enumerator_t972560644 *, Dictionary_2_t3947503238 *, const MethodInfo*))Enumerator__ctor_m4054083607_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m623467750_gshared (Enumerator_t972560644 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m623467750(__this, method) ((  Il2CppObject * (*) (Enumerator_t972560644 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m623467750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1160042744_gshared (Enumerator_t972560644 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1160042744(__this, method) ((  void (*) (Enumerator_t972560644 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1160042744_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3244551521_gshared (Enumerator_t972560644 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3244551521(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t972560644 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3244551521_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1601136300_gshared (Enumerator_t972560644 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1601136300(__this, method) ((  Il2CppObject * (*) (Enumerator_t972560644 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1601136300_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m452517118_gshared (Enumerator_t972560644 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m452517118(__this, method) ((  Il2CppObject * (*) (Enumerator_t972560644 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m452517118_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2120131144_gshared (Enumerator_t972560644 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2120131144(__this, method) ((  bool (*) (Enumerator_t972560644 *, const MethodInfo*))Enumerator_MoveNext_m2120131144_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1704848460  Enumerator_get_Current_m1663316944_gshared (Enumerator_t972560644 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1663316944(__this, method) ((  KeyValuePair_2_t1704848460  (*) (Enumerator_t972560644 *, const MethodInfo*))Enumerator_get_Current_m1663316944_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m4061335851_gshared (Enumerator_t972560644 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m4061335851(__this, method) ((  int32_t (*) (Enumerator_t972560644 *, const MethodInfo*))Enumerator_get_CurrentKey_m4061335851_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m2921613259_gshared (Enumerator_t972560644 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2921613259(__this, method) ((  Il2CppObject * (*) (Enumerator_t972560644 *, const MethodInfo*))Enumerator_get_CurrentValue_m2921613259_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m2558404929_gshared (Enumerator_t972560644 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2558404929(__this, method) ((  void (*) (Enumerator_t972560644 *, const MethodInfo*))Enumerator_Reset_m2558404929_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m549837356_gshared (Enumerator_t972560644 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m549837356(__this, method) ((  void (*) (Enumerator_t972560644 *, const MethodInfo*))Enumerator_VerifyState_m549837356_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1820742208_gshared (Enumerator_t972560644 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1820742208(__this, method) ((  void (*) (Enumerator_t972560644 *, const MethodInfo*))Enumerator_VerifyCurrent_m1820742208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVRButtonId,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2015859619_gshared (Enumerator_t972560644 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2015859619(__this, method) ((  void (*) (Enumerator_t972560644 *, const MethodInfo*))Enumerator_Dispose_m2015859619_gshared)(__this, method)
