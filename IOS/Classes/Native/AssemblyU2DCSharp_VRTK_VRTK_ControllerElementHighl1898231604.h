﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.Highlighters.VRTK_BaseHighlighter
struct VRTK_BaseHighlighter_t3110203740;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ControllerElementHighlighers
struct  VRTK_ControllerElementHighlighers_t1898231604 
{
public:
	// VRTK.Highlighters.VRTK_BaseHighlighter VRTK.VRTK_ControllerElementHighlighers::body
	VRTK_BaseHighlighter_t3110203740 * ___body_0;
	// VRTK.Highlighters.VRTK_BaseHighlighter VRTK.VRTK_ControllerElementHighlighers::trigger
	VRTK_BaseHighlighter_t3110203740 * ___trigger_1;
	// VRTK.Highlighters.VRTK_BaseHighlighter VRTK.VRTK_ControllerElementHighlighers::gripLeft
	VRTK_BaseHighlighter_t3110203740 * ___gripLeft_2;
	// VRTK.Highlighters.VRTK_BaseHighlighter VRTK.VRTK_ControllerElementHighlighers::gripRight
	VRTK_BaseHighlighter_t3110203740 * ___gripRight_3;
	// VRTK.Highlighters.VRTK_BaseHighlighter VRTK.VRTK_ControllerElementHighlighers::touchpad
	VRTK_BaseHighlighter_t3110203740 * ___touchpad_4;
	// VRTK.Highlighters.VRTK_BaseHighlighter VRTK.VRTK_ControllerElementHighlighers::buttonOne
	VRTK_BaseHighlighter_t3110203740 * ___buttonOne_5;
	// VRTK.Highlighters.VRTK_BaseHighlighter VRTK.VRTK_ControllerElementHighlighers::buttonTwo
	VRTK_BaseHighlighter_t3110203740 * ___buttonTwo_6;
	// VRTK.Highlighters.VRTK_BaseHighlighter VRTK.VRTK_ControllerElementHighlighers::systemMenu
	VRTK_BaseHighlighter_t3110203740 * ___systemMenu_7;
	// VRTK.Highlighters.VRTK_BaseHighlighter VRTK.VRTK_ControllerElementHighlighers::startMenu
	VRTK_BaseHighlighter_t3110203740 * ___startMenu_8;

public:
	inline static int32_t get_offset_of_body_0() { return static_cast<int32_t>(offsetof(VRTK_ControllerElementHighlighers_t1898231604, ___body_0)); }
	inline VRTK_BaseHighlighter_t3110203740 * get_body_0() const { return ___body_0; }
	inline VRTK_BaseHighlighter_t3110203740 ** get_address_of_body_0() { return &___body_0; }
	inline void set_body_0(VRTK_BaseHighlighter_t3110203740 * value)
	{
		___body_0 = value;
		Il2CppCodeGenWriteBarrier(&___body_0, value);
	}

	inline static int32_t get_offset_of_trigger_1() { return static_cast<int32_t>(offsetof(VRTK_ControllerElementHighlighers_t1898231604, ___trigger_1)); }
	inline VRTK_BaseHighlighter_t3110203740 * get_trigger_1() const { return ___trigger_1; }
	inline VRTK_BaseHighlighter_t3110203740 ** get_address_of_trigger_1() { return &___trigger_1; }
	inline void set_trigger_1(VRTK_BaseHighlighter_t3110203740 * value)
	{
		___trigger_1 = value;
		Il2CppCodeGenWriteBarrier(&___trigger_1, value);
	}

	inline static int32_t get_offset_of_gripLeft_2() { return static_cast<int32_t>(offsetof(VRTK_ControllerElementHighlighers_t1898231604, ___gripLeft_2)); }
	inline VRTK_BaseHighlighter_t3110203740 * get_gripLeft_2() const { return ___gripLeft_2; }
	inline VRTK_BaseHighlighter_t3110203740 ** get_address_of_gripLeft_2() { return &___gripLeft_2; }
	inline void set_gripLeft_2(VRTK_BaseHighlighter_t3110203740 * value)
	{
		___gripLeft_2 = value;
		Il2CppCodeGenWriteBarrier(&___gripLeft_2, value);
	}

	inline static int32_t get_offset_of_gripRight_3() { return static_cast<int32_t>(offsetof(VRTK_ControllerElementHighlighers_t1898231604, ___gripRight_3)); }
	inline VRTK_BaseHighlighter_t3110203740 * get_gripRight_3() const { return ___gripRight_3; }
	inline VRTK_BaseHighlighter_t3110203740 ** get_address_of_gripRight_3() { return &___gripRight_3; }
	inline void set_gripRight_3(VRTK_BaseHighlighter_t3110203740 * value)
	{
		___gripRight_3 = value;
		Il2CppCodeGenWriteBarrier(&___gripRight_3, value);
	}

	inline static int32_t get_offset_of_touchpad_4() { return static_cast<int32_t>(offsetof(VRTK_ControllerElementHighlighers_t1898231604, ___touchpad_4)); }
	inline VRTK_BaseHighlighter_t3110203740 * get_touchpad_4() const { return ___touchpad_4; }
	inline VRTK_BaseHighlighter_t3110203740 ** get_address_of_touchpad_4() { return &___touchpad_4; }
	inline void set_touchpad_4(VRTK_BaseHighlighter_t3110203740 * value)
	{
		___touchpad_4 = value;
		Il2CppCodeGenWriteBarrier(&___touchpad_4, value);
	}

	inline static int32_t get_offset_of_buttonOne_5() { return static_cast<int32_t>(offsetof(VRTK_ControllerElementHighlighers_t1898231604, ___buttonOne_5)); }
	inline VRTK_BaseHighlighter_t3110203740 * get_buttonOne_5() const { return ___buttonOne_5; }
	inline VRTK_BaseHighlighter_t3110203740 ** get_address_of_buttonOne_5() { return &___buttonOne_5; }
	inline void set_buttonOne_5(VRTK_BaseHighlighter_t3110203740 * value)
	{
		___buttonOne_5 = value;
		Il2CppCodeGenWriteBarrier(&___buttonOne_5, value);
	}

	inline static int32_t get_offset_of_buttonTwo_6() { return static_cast<int32_t>(offsetof(VRTK_ControllerElementHighlighers_t1898231604, ___buttonTwo_6)); }
	inline VRTK_BaseHighlighter_t3110203740 * get_buttonTwo_6() const { return ___buttonTwo_6; }
	inline VRTK_BaseHighlighter_t3110203740 ** get_address_of_buttonTwo_6() { return &___buttonTwo_6; }
	inline void set_buttonTwo_6(VRTK_BaseHighlighter_t3110203740 * value)
	{
		___buttonTwo_6 = value;
		Il2CppCodeGenWriteBarrier(&___buttonTwo_6, value);
	}

	inline static int32_t get_offset_of_systemMenu_7() { return static_cast<int32_t>(offsetof(VRTK_ControllerElementHighlighers_t1898231604, ___systemMenu_7)); }
	inline VRTK_BaseHighlighter_t3110203740 * get_systemMenu_7() const { return ___systemMenu_7; }
	inline VRTK_BaseHighlighter_t3110203740 ** get_address_of_systemMenu_7() { return &___systemMenu_7; }
	inline void set_systemMenu_7(VRTK_BaseHighlighter_t3110203740 * value)
	{
		___systemMenu_7 = value;
		Il2CppCodeGenWriteBarrier(&___systemMenu_7, value);
	}

	inline static int32_t get_offset_of_startMenu_8() { return static_cast<int32_t>(offsetof(VRTK_ControllerElementHighlighers_t1898231604, ___startMenu_8)); }
	inline VRTK_BaseHighlighter_t3110203740 * get_startMenu_8() const { return ___startMenu_8; }
	inline VRTK_BaseHighlighter_t3110203740 ** get_address_of_startMenu_8() { return &___startMenu_8; }
	inline void set_startMenu_8(VRTK_BaseHighlighter_t3110203740 * value)
	{
		___startMenu_8 = value;
		Il2CppCodeGenWriteBarrier(&___startMenu_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VRTK.VRTK_ControllerElementHighlighers
struct VRTK_ControllerElementHighlighers_t1898231604_marshaled_pinvoke
{
	VRTK_BaseHighlighter_t3110203740 * ___body_0;
	VRTK_BaseHighlighter_t3110203740 * ___trigger_1;
	VRTK_BaseHighlighter_t3110203740 * ___gripLeft_2;
	VRTK_BaseHighlighter_t3110203740 * ___gripRight_3;
	VRTK_BaseHighlighter_t3110203740 * ___touchpad_4;
	VRTK_BaseHighlighter_t3110203740 * ___buttonOne_5;
	VRTK_BaseHighlighter_t3110203740 * ___buttonTwo_6;
	VRTK_BaseHighlighter_t3110203740 * ___systemMenu_7;
	VRTK_BaseHighlighter_t3110203740 * ___startMenu_8;
};
// Native definition for COM marshalling of VRTK.VRTK_ControllerElementHighlighers
struct VRTK_ControllerElementHighlighers_t1898231604_marshaled_com
{
	VRTK_BaseHighlighter_t3110203740 * ___body_0;
	VRTK_BaseHighlighter_t3110203740 * ___trigger_1;
	VRTK_BaseHighlighter_t3110203740 * ___gripLeft_2;
	VRTK_BaseHighlighter_t3110203740 * ___gripRight_3;
	VRTK_BaseHighlighter_t3110203740 * ___touchpad_4;
	VRTK_BaseHighlighter_t3110203740 * ___buttonOne_5;
	VRTK_BaseHighlighter_t3110203740 * ___buttonTwo_6;
	VRTK_BaseHighlighter_t3110203740 * ___systemMenu_7;
	VRTK_BaseHighlighter_t3110203740 * ___startMenu_8;
};
