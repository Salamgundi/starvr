﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct DefaultComparer_t3009648097;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1387717936.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Valve.VR.InteractionSystem.Hand/AttachedObject>::.ctor()
extern "C"  void DefaultComparer__ctor_m2534173052_gshared (DefaultComparer_t3009648097 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2534173052(__this, method) ((  void (*) (DefaultComparer_t3009648097 *, const MethodInfo*))DefaultComparer__ctor_m2534173052_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Valve.VR.InteractionSystem.Hand/AttachedObject>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4163319125_gshared (DefaultComparer_t3009648097 * __this, AttachedObject_t1387717936  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m4163319125(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3009648097 *, AttachedObject_t1387717936 , const MethodInfo*))DefaultComparer_GetHashCode_m4163319125_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Valve.VR.InteractionSystem.Hand/AttachedObject>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4170239753_gshared (DefaultComparer_t3009648097 * __this, AttachedObject_t1387717936  ___x0, AttachedObject_t1387717936  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m4170239753(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3009648097 *, AttachedObject_t1387717936 , AttachedObject_t1387717936 , const MethodInfo*))DefaultComparer_Equals_m4170239753_gshared)(__this, ___x0, ___y1, method)
