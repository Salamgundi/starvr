﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_ShowDashboard
struct _ShowDashboard_t4127025320;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_ShowDashboard::.ctor(System.Object,System.IntPtr)
extern "C"  void _ShowDashboard__ctor_m3814115003 (_ShowDashboard_t4127025320 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVROverlay/_ShowDashboard::Invoke(System.String)
extern "C"  void _ShowDashboard_Invoke_m862074589 (_ShowDashboard_t4127025320 * __this, String_t* ___pchOverlayToShow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_ShowDashboard::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ShowDashboard_BeginInvoke_m1839620774 (_ShowDashboard_t4127025320 * __this, String_t* ___pchOverlayToShow0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVROverlay/_ShowDashboard::EndInvoke(System.IAsyncResult)
extern "C"  void _ShowDashboard_EndInvoke_m2941218449 (_ShowDashboard_t4127025320 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
