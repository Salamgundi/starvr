﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.HapticRack
struct HapticRack_t1520216690;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand379716353.h"

// System.Void Valve.VR.InteractionSystem.HapticRack::.ctor()
extern "C"  void HapticRack__ctor_m2358645974 (HapticRack_t1520216690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.HapticRack::Awake()
extern "C"  void HapticRack_Awake_m1917452247 (HapticRack_t1520216690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.HapticRack::OnHandHoverBegin(Valve.VR.InteractionSystem.Hand)
extern "C"  void HapticRack_OnHandHoverBegin_m11922843 (HapticRack_t1520216690 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.HapticRack::OnHandHoverEnd(Valve.VR.InteractionSystem.Hand)
extern "C"  void HapticRack_OnHandHoverEnd_m2189849611 (HapticRack_t1520216690 * __this, Hand_t379716353 * ___hand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.HapticRack::Update()
extern "C"  void HapticRack_Update_m776603627 (HapticRack_t1520216690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.HapticRack::Pulse()
extern "C"  void HapticRack_Pulse_m2884214529 (HapticRack_t1520216690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
