﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.PlayerClimbEventHandler
struct PlayerClimbEventHandler_t2826600604;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_BodyPhysics
struct VRTK_BodyPhysics_t3414085265;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_PlayerClimb
struct  VRTK_PlayerClimb_t322130288  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.VRTK_PlayerClimb::usePlayerScale
	bool ___usePlayerScale_2;
	// VRTK.PlayerClimbEventHandler VRTK.VRTK_PlayerClimb::PlayerClimbStarted
	PlayerClimbEventHandler_t2826600604 * ___PlayerClimbStarted_3;
	// VRTK.PlayerClimbEventHandler VRTK.VRTK_PlayerClimb::PlayerClimbEnded
	PlayerClimbEventHandler_t2826600604 * ___PlayerClimbEnded_4;
	// UnityEngine.Transform VRTK.VRTK_PlayerClimb::playArea
	Transform_t3275118058 * ___playArea_5;
	// UnityEngine.Vector3 VRTK.VRTK_PlayerClimb::startControllerScaledLocalPosition
	Vector3_t2243707580  ___startControllerScaledLocalPosition_6;
	// UnityEngine.Vector3 VRTK.VRTK_PlayerClimb::startGrabPointLocalPosition
	Vector3_t2243707580  ___startGrabPointLocalPosition_7;
	// UnityEngine.Vector3 VRTK.VRTK_PlayerClimb::startPlayAreaWorldOffset
	Vector3_t2243707580  ___startPlayAreaWorldOffset_8;
	// UnityEngine.GameObject VRTK.VRTK_PlayerClimb::grabbingController
	GameObject_t1756533147 * ___grabbingController_9;
	// UnityEngine.GameObject VRTK.VRTK_PlayerClimb::climbingObject
	GameObject_t1756533147 * ___climbingObject_10;
	// UnityEngine.Quaternion VRTK.VRTK_PlayerClimb::climbingObjectLastRotation
	Quaternion_t4030073918  ___climbingObjectLastRotation_11;
	// VRTK.VRTK_BodyPhysics VRTK.VRTK_PlayerClimb::bodyPhysics
	VRTK_BodyPhysics_t3414085265 * ___bodyPhysics_12;
	// System.Boolean VRTK.VRTK_PlayerClimb::isClimbing
	bool ___isClimbing_13;
	// System.Boolean VRTK.VRTK_PlayerClimb::useGrabbedObjectRotation
	bool ___useGrabbedObjectRotation_14;

public:
	inline static int32_t get_offset_of_usePlayerScale_2() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_t322130288, ___usePlayerScale_2)); }
	inline bool get_usePlayerScale_2() const { return ___usePlayerScale_2; }
	inline bool* get_address_of_usePlayerScale_2() { return &___usePlayerScale_2; }
	inline void set_usePlayerScale_2(bool value)
	{
		___usePlayerScale_2 = value;
	}

	inline static int32_t get_offset_of_PlayerClimbStarted_3() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_t322130288, ___PlayerClimbStarted_3)); }
	inline PlayerClimbEventHandler_t2826600604 * get_PlayerClimbStarted_3() const { return ___PlayerClimbStarted_3; }
	inline PlayerClimbEventHandler_t2826600604 ** get_address_of_PlayerClimbStarted_3() { return &___PlayerClimbStarted_3; }
	inline void set_PlayerClimbStarted_3(PlayerClimbEventHandler_t2826600604 * value)
	{
		___PlayerClimbStarted_3 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerClimbStarted_3, value);
	}

	inline static int32_t get_offset_of_PlayerClimbEnded_4() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_t322130288, ___PlayerClimbEnded_4)); }
	inline PlayerClimbEventHandler_t2826600604 * get_PlayerClimbEnded_4() const { return ___PlayerClimbEnded_4; }
	inline PlayerClimbEventHandler_t2826600604 ** get_address_of_PlayerClimbEnded_4() { return &___PlayerClimbEnded_4; }
	inline void set_PlayerClimbEnded_4(PlayerClimbEventHandler_t2826600604 * value)
	{
		___PlayerClimbEnded_4 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerClimbEnded_4, value);
	}

	inline static int32_t get_offset_of_playArea_5() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_t322130288, ___playArea_5)); }
	inline Transform_t3275118058 * get_playArea_5() const { return ___playArea_5; }
	inline Transform_t3275118058 ** get_address_of_playArea_5() { return &___playArea_5; }
	inline void set_playArea_5(Transform_t3275118058 * value)
	{
		___playArea_5 = value;
		Il2CppCodeGenWriteBarrier(&___playArea_5, value);
	}

	inline static int32_t get_offset_of_startControllerScaledLocalPosition_6() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_t322130288, ___startControllerScaledLocalPosition_6)); }
	inline Vector3_t2243707580  get_startControllerScaledLocalPosition_6() const { return ___startControllerScaledLocalPosition_6; }
	inline Vector3_t2243707580 * get_address_of_startControllerScaledLocalPosition_6() { return &___startControllerScaledLocalPosition_6; }
	inline void set_startControllerScaledLocalPosition_6(Vector3_t2243707580  value)
	{
		___startControllerScaledLocalPosition_6 = value;
	}

	inline static int32_t get_offset_of_startGrabPointLocalPosition_7() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_t322130288, ___startGrabPointLocalPosition_7)); }
	inline Vector3_t2243707580  get_startGrabPointLocalPosition_7() const { return ___startGrabPointLocalPosition_7; }
	inline Vector3_t2243707580 * get_address_of_startGrabPointLocalPosition_7() { return &___startGrabPointLocalPosition_7; }
	inline void set_startGrabPointLocalPosition_7(Vector3_t2243707580  value)
	{
		___startGrabPointLocalPosition_7 = value;
	}

	inline static int32_t get_offset_of_startPlayAreaWorldOffset_8() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_t322130288, ___startPlayAreaWorldOffset_8)); }
	inline Vector3_t2243707580  get_startPlayAreaWorldOffset_8() const { return ___startPlayAreaWorldOffset_8; }
	inline Vector3_t2243707580 * get_address_of_startPlayAreaWorldOffset_8() { return &___startPlayAreaWorldOffset_8; }
	inline void set_startPlayAreaWorldOffset_8(Vector3_t2243707580  value)
	{
		___startPlayAreaWorldOffset_8 = value;
	}

	inline static int32_t get_offset_of_grabbingController_9() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_t322130288, ___grabbingController_9)); }
	inline GameObject_t1756533147 * get_grabbingController_9() const { return ___grabbingController_9; }
	inline GameObject_t1756533147 ** get_address_of_grabbingController_9() { return &___grabbingController_9; }
	inline void set_grabbingController_9(GameObject_t1756533147 * value)
	{
		___grabbingController_9 = value;
		Il2CppCodeGenWriteBarrier(&___grabbingController_9, value);
	}

	inline static int32_t get_offset_of_climbingObject_10() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_t322130288, ___climbingObject_10)); }
	inline GameObject_t1756533147 * get_climbingObject_10() const { return ___climbingObject_10; }
	inline GameObject_t1756533147 ** get_address_of_climbingObject_10() { return &___climbingObject_10; }
	inline void set_climbingObject_10(GameObject_t1756533147 * value)
	{
		___climbingObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___climbingObject_10, value);
	}

	inline static int32_t get_offset_of_climbingObjectLastRotation_11() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_t322130288, ___climbingObjectLastRotation_11)); }
	inline Quaternion_t4030073918  get_climbingObjectLastRotation_11() const { return ___climbingObjectLastRotation_11; }
	inline Quaternion_t4030073918 * get_address_of_climbingObjectLastRotation_11() { return &___climbingObjectLastRotation_11; }
	inline void set_climbingObjectLastRotation_11(Quaternion_t4030073918  value)
	{
		___climbingObjectLastRotation_11 = value;
	}

	inline static int32_t get_offset_of_bodyPhysics_12() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_t322130288, ___bodyPhysics_12)); }
	inline VRTK_BodyPhysics_t3414085265 * get_bodyPhysics_12() const { return ___bodyPhysics_12; }
	inline VRTK_BodyPhysics_t3414085265 ** get_address_of_bodyPhysics_12() { return &___bodyPhysics_12; }
	inline void set_bodyPhysics_12(VRTK_BodyPhysics_t3414085265 * value)
	{
		___bodyPhysics_12 = value;
		Il2CppCodeGenWriteBarrier(&___bodyPhysics_12, value);
	}

	inline static int32_t get_offset_of_isClimbing_13() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_t322130288, ___isClimbing_13)); }
	inline bool get_isClimbing_13() const { return ___isClimbing_13; }
	inline bool* get_address_of_isClimbing_13() { return &___isClimbing_13; }
	inline void set_isClimbing_13(bool value)
	{
		___isClimbing_13 = value;
	}

	inline static int32_t get_offset_of_useGrabbedObjectRotation_14() { return static_cast<int32_t>(offsetof(VRTK_PlayerClimb_t322130288, ___useGrabbedObjectRotation_14)); }
	inline bool get_useGrabbedObjectRotation_14() const { return ___useGrabbedObjectRotation_14; }
	inline bool* get_address_of_useGrabbedObjectRotation_14() { return &___useGrabbedObjectRotation_14; }
	inline void set_useGrabbedObjectRotation_14(bool value)
	{
		___useGrabbedObjectRotation_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
