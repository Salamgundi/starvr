﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_AI_NavMeshHit3805955059.h"

// System.Boolean UnityEngine.AI.NavMesh::SamplePosition(UnityEngine.Vector3,UnityEngine.AI.NavMeshHit&,System.Single,System.Int32)
extern "C"  bool NavMesh_SamplePosition_m1602526535 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___sourcePosition0, NavMeshHit_t3805955059 * ___hit1, float ___maxDistance2, int32_t ___areaMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AI.NavMesh::INTERNAL_CALL_SamplePosition(UnityEngine.Vector3&,UnityEngine.AI.NavMeshHit&,System.Single,System.Int32)
extern "C"  bool NavMesh_INTERNAL_CALL_SamplePosition_m2200854406 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___sourcePosition0, NavMeshHit_t3805955059 * ___hit1, float ___maxDistance2, int32_t ___areaMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
