﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_1_gen569904416MethodDeclarations.h"

// System.Void SteamVR_Events/Event`1<Valve.VR.TrackedDevicePose_t[]>::.ctor()
#define Event_1__ctor_m1434222203(__this, method) ((  void (*) (Event_1_t777727170 *, const MethodInfo*))Event_1__ctor_m28252523_gshared)(__this, method)
// System.Void SteamVR_Events/Event`1<Valve.VR.TrackedDevicePose_t[]>::Listen(UnityEngine.Events.UnityAction`1<T>)
#define Event_1_Listen_m10908998(__this, ___action0, method) ((  void (*) (Event_1_t777727170 *, UnityAction_1_t4263857800 *, const MethodInfo*))Event_1_Listen_m1961284276_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`1<Valve.VR.TrackedDevicePose_t[]>::Remove(UnityEngine.Events.UnityAction`1<T>)
#define Event_1_Remove_m1518213545(__this, ___action0, method) ((  void (*) (Event_1_t777727170 *, UnityAction_1_t4263857800 *, const MethodInfo*))Event_1_Remove_m1960904225_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`1<Valve.VR.TrackedDevicePose_t[]>::Send(T)
#define Event_1_Send_m1761870211(__this, ___arg00, method) ((  void (*) (Event_1_t777727170 *, TrackedDevicePose_tU5BU5D_t2897272049*, const MethodInfo*))Event_1_Send_m2836426603_gshared)(__this, ___arg00, method)
