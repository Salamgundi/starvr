﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1957233784.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Valve_VR_AppOverrideKeys_t1098481522.h"

// System.Void System.Array/InternalEnumerator`1<Valve.VR.AppOverrideKeys_t>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3209077273_gshared (InternalEnumerator_1_t1957233784 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3209077273(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1957233784 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3209077273_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.AppOverrideKeys_t>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1667737789_gshared (InternalEnumerator_1_t1957233784 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1667737789(__this, method) ((  void (*) (InternalEnumerator_1_t1957233784 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1667737789_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Valve.VR.AppOverrideKeys_t>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2551755637_gshared (InternalEnumerator_1_t1957233784 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2551755637(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1957233784 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2551755637_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Valve.VR.AppOverrideKeys_t>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2509238930_gshared (InternalEnumerator_1_t1957233784 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2509238930(__this, method) ((  void (*) (InternalEnumerator_1_t1957233784 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2509238930_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Valve.VR.AppOverrideKeys_t>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1433174629_gshared (InternalEnumerator_1_t1957233784 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1433174629(__this, method) ((  bool (*) (InternalEnumerator_1_t1957233784 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1433174629_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Valve.VR.AppOverrideKeys_t>::get_Current()
extern "C"  AppOverrideKeys_t_t1098481522  InternalEnumerator_1_get_Current_m3751456528_gshared (InternalEnumerator_1_t1957233784 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3751456528(__this, method) ((  AppOverrideKeys_t_t1098481522  (*) (InternalEnumerator_1_t1957233784 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3751456528_gshared)(__this, method)
