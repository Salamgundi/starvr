﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.SDK_BaseBoundaries
struct  SDK_BaseBoundaries_t1766380066  : public ScriptableObject_t1975622470
{
public:
	// UnityEngine.Transform VRTK.SDK_BaseBoundaries::cachedPlayArea
	Transform_t3275118058 * ___cachedPlayArea_2;

public:
	inline static int32_t get_offset_of_cachedPlayArea_2() { return static_cast<int32_t>(offsetof(SDK_BaseBoundaries_t1766380066, ___cachedPlayArea_2)); }
	inline Transform_t3275118058 * get_cachedPlayArea_2() const { return ___cachedPlayArea_2; }
	inline Transform_t3275118058 ** get_address_of_cachedPlayArea_2() { return &___cachedPlayArea_2; }
	inline void set_cachedPlayArea_2(Transform_t3275118058 * value)
	{
		___cachedPlayArea_2 = value;
		Il2CppCodeGenWriteBarrier(&___cachedPlayArea_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
