﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MenuOOn
struct MenuOOn_t418768851;
// SteamVR_Controller/Device
struct Device_t2885069456;

#include "codegen/il2cpp-codegen.h"

// System.Void MenuOOn::.ctor()
extern "C"  void MenuOOn__ctor_m2623225184 (MenuOOn_t418768851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SteamVR_Controller/Device MenuOOn::get_Controller()
extern "C"  Device_t2885069456 * MenuOOn_get_Controller_m1875122776 (MenuOOn_t418768851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuOOn::Awake()
extern "C"  void MenuOOn_Awake_m1330924415 (MenuOOn_t418768851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuOOn::Update()
extern "C"  void MenuOOn_Update_m2459716603 (MenuOOn_t418768851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
