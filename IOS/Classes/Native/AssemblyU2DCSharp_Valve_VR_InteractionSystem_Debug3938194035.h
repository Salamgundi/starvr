﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.Player
struct Player_t4256718089;
// Valve.VR.InteractionSystem.DebugUI
struct DebugUI_t3938194035;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.DebugUI
struct  DebugUI_t3938194035  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.Player Valve.VR.InteractionSystem.DebugUI::player
	Player_t4256718089 * ___player_2;

public:
	inline static int32_t get_offset_of_player_2() { return static_cast<int32_t>(offsetof(DebugUI_t3938194035, ___player_2)); }
	inline Player_t4256718089 * get_player_2() const { return ___player_2; }
	inline Player_t4256718089 ** get_address_of_player_2() { return &___player_2; }
	inline void set_player_2(Player_t4256718089 * value)
	{
		___player_2 = value;
		Il2CppCodeGenWriteBarrier(&___player_2, value);
	}
};

struct DebugUI_t3938194035_StaticFields
{
public:
	// Valve.VR.InteractionSystem.DebugUI Valve.VR.InteractionSystem.DebugUI::_instance
	DebugUI_t3938194035 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(DebugUI_t3938194035_StaticFields, ____instance_3)); }
	inline DebugUI_t3938194035 * get__instance_3() const { return ____instance_3; }
	inline DebugUI_t3938194035 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(DebugUI_t3938194035 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier(&____instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
