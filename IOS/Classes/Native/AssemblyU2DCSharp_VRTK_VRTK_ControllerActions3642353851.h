﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_ControllerModelElementPaths
struct VRTK_ControllerModelElementPaths_t672993657;
// VRTK.ControllerActionsEventHandler
struct ControllerActionsEventHandler_t1521142243;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>
struct Dictionary_2_t894930024;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerElementHighl1898231604.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_ControllerActions
struct  VRTK_ControllerActions_t3642353851  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_ControllerModelElementPaths VRTK.VRTK_ControllerActions::modelElementPaths
	VRTK_ControllerModelElementPaths_t672993657 * ___modelElementPaths_2;
	// VRTK.VRTK_ControllerElementHighlighers VRTK.VRTK_ControllerActions::elementHighlighterOverrides
	VRTK_ControllerElementHighlighers_t1898231604  ___elementHighlighterOverrides_3;
	// VRTK.ControllerActionsEventHandler VRTK.VRTK_ControllerActions::ControllerModelVisible
	ControllerActionsEventHandler_t1521142243 * ___ControllerModelVisible_4;
	// VRTK.ControllerActionsEventHandler VRTK.VRTK_ControllerActions::ControllerModelInvisible
	ControllerActionsEventHandler_t1521142243 * ___ControllerModelInvisible_5;
	// UnityEngine.GameObject VRTK.VRTK_ControllerActions::modelContainer
	GameObject_t1756533147 * ___modelContainer_6;
	// System.Boolean VRTK.VRTK_ControllerActions::controllerVisible
	bool ___controllerVisible_7;
	// System.Boolean VRTK.VRTK_ControllerActions::controllerHighlighted
	bool ___controllerHighlighted_8;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform> VRTK.VRTK_ControllerActions::cachedElements
	Dictionary_2_t894930024 * ___cachedElements_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> VRTK.VRTK_ControllerActions::highlighterOptions
	Dictionary_2_t309261261 * ___highlighterOptions_10;
	// UnityEngine.Coroutine VRTK.VRTK_ControllerActions::hapticLoop
	Coroutine_t2299508840 * ___hapticLoop_11;

public:
	inline static int32_t get_offset_of_modelElementPaths_2() { return static_cast<int32_t>(offsetof(VRTK_ControllerActions_t3642353851, ___modelElementPaths_2)); }
	inline VRTK_ControllerModelElementPaths_t672993657 * get_modelElementPaths_2() const { return ___modelElementPaths_2; }
	inline VRTK_ControllerModelElementPaths_t672993657 ** get_address_of_modelElementPaths_2() { return &___modelElementPaths_2; }
	inline void set_modelElementPaths_2(VRTK_ControllerModelElementPaths_t672993657 * value)
	{
		___modelElementPaths_2 = value;
		Il2CppCodeGenWriteBarrier(&___modelElementPaths_2, value);
	}

	inline static int32_t get_offset_of_elementHighlighterOverrides_3() { return static_cast<int32_t>(offsetof(VRTK_ControllerActions_t3642353851, ___elementHighlighterOverrides_3)); }
	inline VRTK_ControllerElementHighlighers_t1898231604  get_elementHighlighterOverrides_3() const { return ___elementHighlighterOverrides_3; }
	inline VRTK_ControllerElementHighlighers_t1898231604 * get_address_of_elementHighlighterOverrides_3() { return &___elementHighlighterOverrides_3; }
	inline void set_elementHighlighterOverrides_3(VRTK_ControllerElementHighlighers_t1898231604  value)
	{
		___elementHighlighterOverrides_3 = value;
	}

	inline static int32_t get_offset_of_ControllerModelVisible_4() { return static_cast<int32_t>(offsetof(VRTK_ControllerActions_t3642353851, ___ControllerModelVisible_4)); }
	inline ControllerActionsEventHandler_t1521142243 * get_ControllerModelVisible_4() const { return ___ControllerModelVisible_4; }
	inline ControllerActionsEventHandler_t1521142243 ** get_address_of_ControllerModelVisible_4() { return &___ControllerModelVisible_4; }
	inline void set_ControllerModelVisible_4(ControllerActionsEventHandler_t1521142243 * value)
	{
		___ControllerModelVisible_4 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerModelVisible_4, value);
	}

	inline static int32_t get_offset_of_ControllerModelInvisible_5() { return static_cast<int32_t>(offsetof(VRTK_ControllerActions_t3642353851, ___ControllerModelInvisible_5)); }
	inline ControllerActionsEventHandler_t1521142243 * get_ControllerModelInvisible_5() const { return ___ControllerModelInvisible_5; }
	inline ControllerActionsEventHandler_t1521142243 ** get_address_of_ControllerModelInvisible_5() { return &___ControllerModelInvisible_5; }
	inline void set_ControllerModelInvisible_5(ControllerActionsEventHandler_t1521142243 * value)
	{
		___ControllerModelInvisible_5 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerModelInvisible_5, value);
	}

	inline static int32_t get_offset_of_modelContainer_6() { return static_cast<int32_t>(offsetof(VRTK_ControllerActions_t3642353851, ___modelContainer_6)); }
	inline GameObject_t1756533147 * get_modelContainer_6() const { return ___modelContainer_6; }
	inline GameObject_t1756533147 ** get_address_of_modelContainer_6() { return &___modelContainer_6; }
	inline void set_modelContainer_6(GameObject_t1756533147 * value)
	{
		___modelContainer_6 = value;
		Il2CppCodeGenWriteBarrier(&___modelContainer_6, value);
	}

	inline static int32_t get_offset_of_controllerVisible_7() { return static_cast<int32_t>(offsetof(VRTK_ControllerActions_t3642353851, ___controllerVisible_7)); }
	inline bool get_controllerVisible_7() const { return ___controllerVisible_7; }
	inline bool* get_address_of_controllerVisible_7() { return &___controllerVisible_7; }
	inline void set_controllerVisible_7(bool value)
	{
		___controllerVisible_7 = value;
	}

	inline static int32_t get_offset_of_controllerHighlighted_8() { return static_cast<int32_t>(offsetof(VRTK_ControllerActions_t3642353851, ___controllerHighlighted_8)); }
	inline bool get_controllerHighlighted_8() const { return ___controllerHighlighted_8; }
	inline bool* get_address_of_controllerHighlighted_8() { return &___controllerHighlighted_8; }
	inline void set_controllerHighlighted_8(bool value)
	{
		___controllerHighlighted_8 = value;
	}

	inline static int32_t get_offset_of_cachedElements_9() { return static_cast<int32_t>(offsetof(VRTK_ControllerActions_t3642353851, ___cachedElements_9)); }
	inline Dictionary_2_t894930024 * get_cachedElements_9() const { return ___cachedElements_9; }
	inline Dictionary_2_t894930024 ** get_address_of_cachedElements_9() { return &___cachedElements_9; }
	inline void set_cachedElements_9(Dictionary_2_t894930024 * value)
	{
		___cachedElements_9 = value;
		Il2CppCodeGenWriteBarrier(&___cachedElements_9, value);
	}

	inline static int32_t get_offset_of_highlighterOptions_10() { return static_cast<int32_t>(offsetof(VRTK_ControllerActions_t3642353851, ___highlighterOptions_10)); }
	inline Dictionary_2_t309261261 * get_highlighterOptions_10() const { return ___highlighterOptions_10; }
	inline Dictionary_2_t309261261 ** get_address_of_highlighterOptions_10() { return &___highlighterOptions_10; }
	inline void set_highlighterOptions_10(Dictionary_2_t309261261 * value)
	{
		___highlighterOptions_10 = value;
		Il2CppCodeGenWriteBarrier(&___highlighterOptions_10, value);
	}

	inline static int32_t get_offset_of_hapticLoop_11() { return static_cast<int32_t>(offsetof(VRTK_ControllerActions_t3642353851, ___hapticLoop_11)); }
	inline Coroutine_t2299508840 * get_hapticLoop_11() const { return ___hapticLoop_11; }
	inline Coroutine_t2299508840 ** get_address_of_hapticLoop_11() { return &___hapticLoop_11; }
	inline void set_hapticLoop_11(Coroutine_t2299508840 * value)
	{
		___hapticLoop_11 = value;
		Il2CppCodeGenWriteBarrier(&___hapticLoop_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
