﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_ControllerActions_UnityEvents
struct VRTK_ControllerActions_UnityEvents_t3920462976;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ControllerActionsEventArgs344001476.h"

// System.Void VRTK.UnityEventHelper.VRTK_ControllerActions_UnityEvents::.ctor()
extern "C"  void VRTK_ControllerActions_UnityEvents__ctor_m1725067315 (VRTK_ControllerActions_UnityEvents_t3920462976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerActions_UnityEvents::SetControllerAction()
extern "C"  void VRTK_ControllerActions_UnityEvents_SetControllerAction_m2501703939 (VRTK_ControllerActions_UnityEvents_t3920462976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerActions_UnityEvents::OnEnable()
extern "C"  void VRTK_ControllerActions_UnityEvents_OnEnable_m1146974455 (VRTK_ControllerActions_UnityEvents_t3920462976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerActions_UnityEvents::ControllerModelVisible(System.Object,VRTK.ControllerActionsEventArgs)
extern "C"  void VRTK_ControllerActions_UnityEvents_ControllerModelVisible_m3695185923 (VRTK_ControllerActions_UnityEvents_t3920462976 * __this, Il2CppObject * ___o0, ControllerActionsEventArgs_t344001476  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerActions_UnityEvents::ControllerModelInvisible(System.Object,VRTK.ControllerActionsEventArgs)
extern "C"  void VRTK_ControllerActions_UnityEvents_ControllerModelInvisible_m1672568724 (VRTK_ControllerActions_UnityEvents_t3920462976 * __this, Il2CppObject * ___o0, ControllerActionsEventArgs_t344001476  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_ControllerActions_UnityEvents::OnDisable()
extern "C"  void VRTK_ControllerActions_UnityEvents_OnDisable_m3981131970 (VRTK_ControllerActions_UnityEvents_t3920462976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
