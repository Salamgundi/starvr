﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// VRTK.VRTK_TeleportDisableOnHeadsetCollision/<EnableAtEndOfFrame>c__Iterator0
struct U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882;
// System.Object
struct Il2CppObject;
// VRTK.VRTK_TouchpadControl
struct VRTK_TouchpadControl_t3191058221;
// VRTK.VRTK_ObjectControl
struct VRTK_ObjectControl_t724022372;
// VRTK.VRTK_TouchpadMovement
struct VRTK_TouchpadMovement_t1979813355;
// VRTK.TouchpadMovementAxisEventHandler
struct TouchpadMovementAxisEventHandler_t2208405138;
// VRTK.VRTK_BodyPhysics
struct VRTK_BodyPhysics_t3414085265;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t720607407;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// VRTK.VRTK_TouchpadWalking
struct VRTK_TouchpadWalking_t2507848959;
// VRTK.VRTK_TrackedController
struct VRTK_TrackedController_t520756048;
// VRTK.VRTKTrackedControllerEventHandler
struct VRTKTrackedControllerEventHandler_t2437916365;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// VRTK.VRTK_TrackedController/<Enable>c__Iterator0
struct U3CEnableU3Ec__Iterator0_t1493054694;
// VRTK.VRTK_TrackedHeadset
struct VRTK_TrackedHeadset_t3483597430;
// VRTK.VRTK_TransformFollow
struct VRTK_TransformFollow_t3532748285;
// UnityEngine.Camera
struct Camera_t189460977;
// VRTK.VRTK_UICanvas
struct VRTK_UICanvas_t1283311654;
// UnityEngine.Collider
struct Collider_t3497673348;
// VRTK.VRTK_PlayerObject
struct VRTK_PlayerObject_t502441292;
// VRTK.VRTK_UIPointer
struct VRTK_UIPointer_t2714926455;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_t410733016;
// VRTK.VRTK_UIGraphicRaycaster
struct VRTK_UIGraphicRaycaster_t2816944648;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t1967201810;
// VRTK.VRTK_UIPointerAutoActivator
struct VRTK_UIPointerAutoActivator_t1282719345;
// VRTK.VRTK_UIDraggableItem
struct VRTK_UIDraggableItem_t2269178406;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// VRTK.VRTK_UIDropZone
struct VRTK_UIDropZone_t1661569883;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// VRTK.UIPointerEventHandler
struct UIPointerEventHandler_t988663103;
// VRTK.VRTK_VRInputModule
struct VRTK_VRInputModule_t1472500726;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// VRTK.VRTK_EventSystem
struct VRTK_EventSystem_t3222336529;
// VRTK.VRTK_UIPointer/<WaitForPointerId>c__Iterator0
struct U3CWaitForPointerIdU3Ec__Iterator0_t2911062424;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct EventFunction_1_t3253137806;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct EventFunction_1_t1186599945;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler>
struct EventFunction_1_t2985282902;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct EventFunction_1_t2426197568;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_t344915111;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
struct EventFunction_1_t2888287612;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
struct EventFunction_1_t1081143969;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct EventFunction_1_t4141241546;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct EventFunction_1_t887251860;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
struct EventFunction_1_t1847959737;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
struct EventFunction_1_t1632278510;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct EventFunction_1_t2331828160;
// VRTK.VRTK_WarpObjectControlAction
struct VRTK_WarpObjectControlAction_t3039272518;
// VRTK.VRTK_Wheel
struct VRTK_Wheel_t803354859;
// UnityEngine.HingeJoint
struct HingeJoint_t2745110831;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach
struct VRTK_TrackObjectGrabAttach_t1133162717;
// VRTK.GrabAttachMechanics.VRTK_RotatorTrackGrabAttach
struct VRTK_RotatorTrackGrabAttach_t3062106299;
// VRTK.SecondaryControllerGrabActions.VRTK_SwapControllerGrabAction
struct VRTK_SwapControllerGrabAction_t918155359;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TeleportDisableOnHeads3548001882.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TeleportDisableOnHeads3548001882MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventHandle3119610762MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_HeadsetCollision2015187094MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TeleportDisableOnHeadset56384818.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BasicTeleport3532761337.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectCache513154459.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectCache513154459MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_HeadsetCollision2015187094.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TeleportDisableOnHeadset56384818MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventArgs1242373387.h"
#include "AssemblyU2DCSharp_VRTK_HeadsetCollisionEventHandle3119610762.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadControl3191058221.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadControl3191058221MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectControl724022372MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectControl724022372.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_VRTK_ObjectControlEventArgs2459490319.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DeviceFinder2083124544MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventHa343979916MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents3225224819MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents3225224819.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventAr287637539.h"
#include "AssemblyU2DCSharp_VRTK_ControllerInteractionEventHa343979916.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement1979813355.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement1979813355MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_Verti2238062081.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_Horiz3153229385.h"
#include "AssemblyU2DCSharp_VRTK_TouchpadMovementAxisEventHa2208405138.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked1625106012.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PlayerObject502441292MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PlayerObject_ObjectTyp2764484032.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BodyPhysics3414085265.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_CapsuleCollider720607407.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BodyPhysics3414085265MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_AxisM2300405083.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_AxisM1724819570.h"
#include "AssemblyU2DCSharp_VRTK_TouchpadMovementAxisEventHa2208405138MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_TouchpadMovementAxisEventAr1237298133.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_DeviceFinder_Devices2408891389.h"
#include "UnityEngine_UnityEngine_CapsuleCollider720607407MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics634932869MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_SDK_Bridge2841169330MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_AxisM1724819570MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_AxisM2300405083MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_Horiz3153229385MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadMovement_Verti2238062081MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadWalking2507848959.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TouchpadWalking2507848959MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TrackedController520756048.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TrackedController520756048MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTKTrackedControllerEventH2437916365.h"
#include "AssemblyU2DCSharp_VRTK_VRTKTrackedControllerEventA2407378264.h"
#include "AssemblyU2DCSharp_VRTK_VRTKTrackedControllerEventH2437916365MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TrackedController_U3CE1493054694MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TrackedController_U3CE1493054694.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TrackedHeadset3483597430.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TrackedHeadset3483597430MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TransformFollow3532748285.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TransformFollow3532748285MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectFollow3175963762MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TransformFollow_Follow1516682274.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback834278767MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ObjectFollow3175963762.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback834278767.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_TransformFollow_Follow1516682274MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UICanvas1283311654.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UICanvas1283311654MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PlayerObject502441292.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer2714926455.h"
#include "UnityEngine_UnityEngine_Canvas209405766MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster410733016MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider22920061MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider3497673348MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster410733016.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIGraphicRaycaster2816944648.h"
#include "UnityEngine_UnityEngine_RenderMode4280533217.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_Blo2548930813.h"
#include "UnityEngine_UnityEngine_BoxCollider22920061.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1967201810.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointerAutoActivator1282719345.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIDraggableItem2269178406.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIDraggableItem2269178406MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UnityEngine_CanvasGroup3296560743MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasGroup3296560743.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIDropZone1661569883.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer2714926455MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_UIPointerEventArgs1171985978.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransformUtility2941082270MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIDropZone1661569883MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIGraphicRaycaster2816944648MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3685274804.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3685274804MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "UnityEngine_UnityEngine_Ray2469606224MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas2336171397MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas2336171397.h"
#include "mscorlib_System_Nullable_1_gen2579219987.h"
#include "mscorlib_System_Nullable_1_gen2579219987MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics2D2540166467MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRegistry377833367MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen1282925227MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen1282925227.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_UIPointerEventHandler988663103.h"
#include "AssemblyU2DCSharp_VRTK_UIPointerEventHandler988663103MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_VRInputModule1472500726.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_EventSystem3222336529.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer_ActivationMe3611442703.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2084047587MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2084047587.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer_U3CWaitForPo2911062424MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer_U3CWaitForPo2911062424.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer_ActivationMe3611442703MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer_ClickMethods1099240745.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointer_ClickMethods1099240745MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_UIPointerAutoActivator1282719345MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_VRInputModule1472500726MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp1441575871MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM1295781545MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1125654279MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat660383953.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1125654279.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat660383953MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3220004478.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation1571958496.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve3253137806.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3220004478MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve2985282902.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation1571958496MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode1081683921.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve2426197568.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEven344915111.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve2888287612.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1081143969.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve4141241546.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEven887251860.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1847959737.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1632278510.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve2331828160.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_WarpObjectControlActio3039272518.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_WarpObjectControlActio3039272518MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BaseObjectControlActio3375001897MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BaseObjectControlActio3375001897.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Wheel803354859.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Wheel803354859MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control651619021MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Joint454317436MethodDeclarations.h"
#include "UnityEngine_UnityEngine_HingeJoint2745110831.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_ControlValueRa2976216666.h"
#include "UnityEngine_UnityEngine_HingeJoint2745110831MethodDeclarations.h"
#include "UnityEngine_UnityEngine_JointLimits4282861422.h"
#include "UnityEngine_UnityEngine_JointLimits4282861422MethodDeclarations.h"
#include "UnityEngine_UnityEngine_JointSpring1540921605.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventHandl940909295MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"
#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Tr1133162717.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Wheel_GrabTypes504515700.h"
#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Ro3062106299.h"
#include "AssemblyU2DCSharp_VRTK_GrabAttachMechanics_VRTK_Ba3487134318.h"
#include "AssemblyU2DCSharp_VRTK_SecondaryControllerGrabActio918155359.h"
#include "AssemblyU2DCSharp_VRTK_SecondaryControllerGrabActi4095736311.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventHandl940909295.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control651619021.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Wheel_GrabTypes504515700MethodDeclarations.h"
#include "AssemblyU2DCSharp_VRTK_VRTKTrackedControllerEventA2407378264MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2650145732(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<VRTK.VRTK_TouchpadControl>()
#define GameObject_GetComponent_TisVRTK_TouchpadControl_t3191058221_m3229846933(__this, method) ((  VRTK_TouchpadControl_t3191058221 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.Object>(!!0&,!!0,!!0)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m2145889806(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<VRTK.TouchpadMovementAxisEventHandler>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisTouchpadMovementAxisEventHandler_t2208405138_m3627842690(__this /* static, unused */, p0, p1, p2, method) ((  TouchpadMovementAxisEventHandler_t2208405138 * (*) (Il2CppObject * /* static, unused */, TouchpadMovementAxisEventHandler_t2208405138 **, TouchpadMovementAxisEventHandler_t2208405138 *, TouchpadMovementAxisEventHandler_t2208405138 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<VRTK.VRTK_BodyPhysics>()
#define Component_GetComponent_TisVRTK_BodyPhysics_t3414085265_m1861204865(__this, method) ((  VRTK_BodyPhysics_t3414085265 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CapsuleCollider>()
#define Component_GetComponent_TisCapsuleCollider_t720607407_m1210329947(__this, method) ((  CapsuleCollider_t720607407 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<VRTK.VRTK_ControllerEvents>()
#define GameObject_GetComponent_TisVRTK_ControllerEvents_t3225224819_m95931981(__this, method) ((  VRTK_ControllerEvents_t3225224819 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 System.Threading.Interlocked::CompareExchange<VRTK.VRTKTrackedControllerEventHandler>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisVRTKTrackedControllerEventHandler_t2437916365_m2030479713(__this /* static, unused */, p0, p1, p2, method) ((  VRTKTrackedControllerEventHandler_t2437916365 * (*) (Il2CppObject * /* static, unused */, VRTKTrackedControllerEventHandler_t2437916365 **, VRTKTrackedControllerEventHandler_t2437916365 *, VRTKTrackedControllerEventHandler_t2437916365 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Component::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponentInParent_TisIl2CppObject_m2509612665(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInParent<VRTK.VRTK_PlayerObject>()
#define Component_GetComponentInParent_TisVRTK_PlayerObject_t502441292_m2727225383(__this, method) ((  VRTK_PlayerObject_t502441292 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInParent<VRTK.VRTK_UIPointer>()
#define Component_GetComponentInParent_TisVRTK_UIPointer_t2714926455_m56220952(__this, method) ((  VRTK_UIPointer_t2714926455 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Canvas>()
#define Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, method) ((  Canvas_t209405766 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t3349966182_m1310250299(__this, method) ((  RectTransform_t3349966182 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.GraphicRaycaster>()
#define GameObject_GetComponent_TisGraphicRaycaster_t410733016_m3518220980(__this, method) ((  GraphicRaycaster_t410733016 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<VRTK.VRTK_UIGraphicRaycaster>()
#define GameObject_GetComponent_TisVRTK_UIGraphicRaycaster_t2816944648_m3913302738(__this, method) ((  VRTK_UIGraphicRaycaster_t2816944648 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3813873105(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<VRTK.VRTK_UIGraphicRaycaster>()
#define GameObject_AddComponent_TisVRTK_UIGraphicRaycaster_t2816944648_m136299103(__this, method) ((  VRTK_UIGraphicRaycaster_t2816944648 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.BoxCollider>()
#define GameObject_GetComponent_TisBoxCollider_t22920061_m2353115491(__this, method) ((  BoxCollider_t22920061 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.BoxCollider>()
#define GameObject_AddComponent_TisBoxCollider_t22920061_m1676656656(__this, method) ((  BoxCollider_t22920061 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody>()
#define GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193(__this, method) ((  Rigidbody_t4233889191 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Rigidbody>()
#define GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210(__this, method) ((  Rigidbody_t4233889191 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.RectTransform>()
#define GameObject_AddComponent_TisRectTransform_t3349966182_m4066292940(__this, method) ((  RectTransform_t3349966182 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.UI.Image>()
#define GameObject_AddComponent_TisImage_t2042527209_m4249278385(__this, method) ((  Image_t2042527209 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.EventSystems.EventTrigger>()
#define GameObject_AddComponent_TisEventTrigger_t1967201810_m902787999(__this, method) ((  EventTrigger_t1967201810 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(__this, method) ((  RectTransform_t3349966182 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<VRTK.VRTK_UIPointerAutoActivator>()
#define GameObject_AddComponent_TisVRTK_UIPointerAutoActivator_t1282719345_m1646521992(__this, method) ((  VRTK_UIPointerAutoActivator_t1282719345 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInParent<UnityEngine.Canvas>()
#define Component_GetComponentInParent_TisCanvas_t209405766_m2415242607(__this, method) ((  Canvas_t209405766 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInParent<VRTK.VRTK_UIDropZone>()
#define Component_GetComponentInParent_TisVRTK_UIDropZone_t1661569883_m290600918(__this, method) ((  VRTK_UIDropZone_t1661569883 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInParent_TisIl2CppObject_m781417156_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponentInParent_TisIl2CppObject_m781417156(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInParent_TisIl2CppObject_m781417156_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInParent<UnityEngine.Canvas>()
#define GameObject_GetComponentInParent_TisCanvas_t209405766_m4180257979(__this, method) ((  Canvas_t209405766 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInParent_TisIl2CppObject_m781417156_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CanvasGroup>()
#define Component_GetComponent_TisCanvasGroup_t3296560743_m846564447(__this, method) ((  CanvasGroup_t3296560743 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<VRTK.VRTK_UIPointer>()
#define GameObject_GetComponent_TisVRTK_UIPointer_t2714926455_m2163511105(__this, method) ((  VRTK_UIPointer_t2714926455 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<VRTK.VRTK_UIDraggableItem>()
#define GameObject_GetComponent_TisVRTK_UIDraggableItem_t2269178406_m2049988108(__this, method) ((  VRTK_UIDraggableItem_t2269178406 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Canvas>()
#define GameObject_GetComponent_TisCanvas_t209405766_m195193039(__this, method) ((  Canvas_t209405766 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 System.Threading.Interlocked::CompareExchange<VRTK.UIPointerEventHandler>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUIPointerEventHandler_t988663103_m113197095(__this /* static, unused */, p0, p1, p2, method) ((  UIPointerEventHandler_t988663103 * (*) (Il2CppObject * /* static, unused */, UIPointerEventHandler_t988663103 **, UIPointerEventHandler_t988663103 *, UIPointerEventHandler_t988663103 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::AddComponent<VRTK.VRTK_EventSystem>()
#define GameObject_AddComponent_TisVRTK_EventSystem_t3222336529_m3671436594(__this, method) ((  VRTK_EventSystem_t3222336529 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<VRTK.VRTK_VRInputModule>()
#define Component_GetComponent_TisVRTK_VRInputModule_t1472500726_m1511391868(__this, method) ((  VRTK_VRInputModule_t1472500726 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m483057723_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m483057723(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m483057723_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<VRTK.VRTK_EventSystem>()
#define Object_FindObjectOfType_TisVRTK_EventSystem_t3222336529_m909515361(__this /* static, unused */, method) ((  VRTK_EventSystem_t3222336529 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m483057723_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Component::GetComponent<VRTK.VRTK_ControllerEvents>()
#define Component_GetComponent_TisVRTK_ControllerEvents_t3225224819_m526559481(__this, method) ((  VRTK_ControllerEvents_t3225224819 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Object::FindObjectOfType<UnityEngine.EventSystems.EventSystem>()
#define Object_FindObjectOfType_TisEventSystem_t3466835263_m3846439091(__this /* static, unused */, method) ((  EventSystem_t3466835263 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m483057723_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.GameObject::GetComponentInParent<VRTK.VRTK_UICanvas>()
#define GameObject_GetComponentInParent_TisVRTK_UICanvas_t1283311654_m2879938029(__this, method) ((  VRTK_UICanvas_t1283311654 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInParent_TisIl2CppObject_m781417156_gshared)(__this, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
extern "C"  GameObject_t1756533147 * ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * p0, BaseEventData_t2681005625 * p1, EventFunction_1_t1186599945 * p2, const MethodInfo* method);
#define ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t1186599945 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IPointerExitHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIPointerExitHandler_t461019860_m300279158(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t3253137806 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IPointerEnterHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIPointerEnterHandler_t193164956_m1445871235(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t2985282902 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Selectable>()
#define GameObject_GetComponent_TisSelectable_t1490392188_m1495814632(__this, method) ((  Selectable_t1490392188 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IPointerDownHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIPointerDownHandler_t3929046918_m462226506(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t2426197568 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IPointerUpHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIPointerUpHandler_t1847764461_m1441454696(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t344915111 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IPointerClickHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIPointerClickHandler_t96169666_m1165684521(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t2888287612 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IDragHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIDragHandler_t2583993319_m1802910190(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t1081143969 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IEndDragHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIEndDragHandler_t1349123600_m848469007(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t4141241546 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IDropHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIDropHandler_t2390101210_m4291895312(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t887251860 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IInitializePotentialDragHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIInitializePotentialDragHandler_t3350809087_m3328014326(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t1847959737 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IBeginDragHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIBeginDragHandler_t3135127860_m2721925759(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t1632278510 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IScrollHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIScrollHandler_t3834677510_m3398003692(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t2331828160 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, method) ((  Rigidbody_t4233889191 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.HingeJoint>()
#define Component_GetComponent_TisHingeJoint_t2745110831_m4068659703(__this, method) ((  HingeJoint_t2745110831 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.HingeJoint>()
#define GameObject_AddComponent_TisHingeJoint_t2745110831_m45774388(__this, method) ((  HingeJoint_t2745110831 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<VRTK.VRTK_InteractableObject>()
#define Component_GetComponent_TisVRTK_InteractableObject_t2604188111_m1255171419(__this, method) ((  VRTK_InteractableObject_t2604188111 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<VRTK.VRTK_InteractableObject>()
#define GameObject_AddComponent_TisVRTK_InteractableObject_t2604188111_m3085208446(__this, method) ((  VRTK_InteractableObject_t2604188111 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<VRTK.GrabAttachMechanics.VRTK_TrackObjectGrabAttach>()
#define GameObject_AddComponent_TisVRTK_TrackObjectGrabAttach_t1133162717_m4130202784(__this, method) ((  VRTK_TrackObjectGrabAttach_t1133162717 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<VRTK.GrabAttachMechanics.VRTK_RotatorTrackGrabAttach>()
#define GameObject_AddComponent_TisVRTK_RotatorTrackGrabAttach_t3062106299_m2781911400(__this, method) ((  VRTK_RotatorTrackGrabAttach_t3062106299 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<VRTK.SecondaryControllerGrabActions.VRTK_SwapControllerGrabAction>()
#define GameObject_AddComponent_TisVRTK_SwapControllerGrabAction_t918155359_m2127770779(__this, method) ((  VRTK_SwapControllerGrabAction_t918155359 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VRTK.VRTK_TeleportDisableOnHeadsetCollision/<EnableAtEndOfFrame>c__Iterator0::.ctor()
extern "C"  void U3CEnableAtEndOfFrameU3Ec__Iterator0__ctor_m3879803257 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VRTK.VRTK_TeleportDisableOnHeadsetCollision/<EnableAtEndOfFrame>c__Iterator0::MoveNext()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var;
extern Il2CppClass* VRTK_ObjectCache_t513154459_il2cpp_TypeInfo_var;
extern Il2CppClass* HeadsetCollisionEventHandler_t3119610762_il2cpp_TypeInfo_var;
extern const MethodInfo* VRTK_TeleportDisableOnHeadsetCollision_DisableTeleport_m2051254723_MethodInfo_var;
extern const MethodInfo* VRTK_TeleportDisableOnHeadsetCollision_EnableTeleport_m2564996272_MethodInfo_var;
extern const uint32_t U3CEnableAtEndOfFrameU3Ec__Iterator0_MoveNext_m3885006675_MetadataUsageId;
extern "C"  bool U3CEnableAtEndOfFrameU3Ec__Iterator0_MoveNext_m3885006675 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEnableAtEndOfFrameU3Ec__Iterator0_MoveNext_m3885006675_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_005b;
		}
	}
	{
		goto IL_00c9;
	}

IL_0021:
	{
		VRTK_TeleportDisableOnHeadsetCollision_t56384818 * L_2 = __this->get_U24this_0();
		NullCheck(L_2);
		VRTK_BasicTeleport_t3532761337 * L_3 = L_2->get_basicTeleport_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003c;
		}
	}
	{
		goto IL_00c9;
	}

IL_003c:
	{
		WaitForEndOfFrame_t1785723201 * L_5 = (WaitForEndOfFrame_t1785723201 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m3062480170(L_5, /*hidden argument*/NULL);
		__this->set_U24current_1(L_5);
		bool L_6 = __this->get_U24disposing_2();
		if (L_6)
		{
			goto IL_0056;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0056:
	{
		goto IL_00cb;
	}

IL_005b:
	{
		VRTK_TeleportDisableOnHeadsetCollision_t56384818 * L_7 = __this->get_U24this_0();
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_ObjectCache_t513154459_il2cpp_TypeInfo_var);
		VRTK_HeadsetCollision_t2015187094 * L_8 = ((VRTK_ObjectCache_t513154459_StaticFields*)VRTK_ObjectCache_t513154459_il2cpp_TypeInfo_var->static_fields)->get_registeredHeadsetCollider_4();
		NullCheck(L_7);
		L_7->set_headsetCollision_3(L_8);
		VRTK_TeleportDisableOnHeadsetCollision_t56384818 * L_9 = __this->get_U24this_0();
		NullCheck(L_9);
		VRTK_HeadsetCollision_t2015187094 * L_10 = L_9->get_headsetCollision_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00c2;
		}
	}
	{
		VRTK_TeleportDisableOnHeadsetCollision_t56384818 * L_12 = __this->get_U24this_0();
		NullCheck(L_12);
		VRTK_HeadsetCollision_t2015187094 * L_13 = L_12->get_headsetCollision_3();
		VRTK_TeleportDisableOnHeadsetCollision_t56384818 * L_14 = __this->get_U24this_0();
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)VRTK_TeleportDisableOnHeadsetCollision_DisableTeleport_m2051254723_MethodInfo_var);
		HeadsetCollisionEventHandler_t3119610762 * L_16 = (HeadsetCollisionEventHandler_t3119610762 *)il2cpp_codegen_object_new(HeadsetCollisionEventHandler_t3119610762_il2cpp_TypeInfo_var);
		HeadsetCollisionEventHandler__ctor_m3587092406(L_16, L_14, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		VRTK_HeadsetCollision_add_HeadsetCollisionDetect_m3756029740(L_13, L_16, /*hidden argument*/NULL);
		VRTK_TeleportDisableOnHeadsetCollision_t56384818 * L_17 = __this->get_U24this_0();
		NullCheck(L_17);
		VRTK_HeadsetCollision_t2015187094 * L_18 = L_17->get_headsetCollision_3();
		VRTK_TeleportDisableOnHeadsetCollision_t56384818 * L_19 = __this->get_U24this_0();
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)VRTK_TeleportDisableOnHeadsetCollision_EnableTeleport_m2564996272_MethodInfo_var);
		HeadsetCollisionEventHandler_t3119610762 * L_21 = (HeadsetCollisionEventHandler_t3119610762 *)il2cpp_codegen_object_new(HeadsetCollisionEventHandler_t3119610762_il2cpp_TypeInfo_var);
		HeadsetCollisionEventHandler__ctor_m3587092406(L_21, L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		VRTK_HeadsetCollision_add_HeadsetCollisionEnded_m1130653975(L_18, L_21, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		__this->set_U24PC_3((-1));
	}

IL_00c9:
	{
		return (bool)0;
	}

IL_00cb:
	{
		return (bool)1;
	}
}
// System.Object VRTK.VRTK_TeleportDisableOnHeadsetCollision/<EnableAtEndOfFrame>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEnableAtEndOfFrameU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1995470611 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object VRTK.VRTK_TeleportDisableOnHeadsetCollision/<EnableAtEndOfFrame>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEnableAtEndOfFrameU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1214637771 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void VRTK.VRTK_TeleportDisableOnHeadsetCollision/<EnableAtEndOfFrame>c__Iterator0::Dispose()
extern "C"  void U3CEnableAtEndOfFrameU3Ec__Iterator0_Dispose_m1764058052 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void VRTK.VRTK_TeleportDisableOnHeadsetCollision/<EnableAtEndOfFrame>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CEnableAtEndOfFrameU3Ec__Iterator0_Reset_m1030118538_MetadataUsageId;
extern "C"  void U3CEnableAtEndOfFrameU3Ec__Iterator0_Reset_m1030118538 (U3CEnableAtEndOfFrameU3Ec__Iterator0_t3548001882 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEnableAtEndOfFrameU3Ec__Iterator0_Reset_m1030118538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void VRTK.VRTK_TouchpadControl::.ctor()
extern "C"  void VRTK_TouchpadControl__ctor_m3214216801 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method)
{
	{
		__this->set_primaryActivationButton_22(((int32_t)9));
		__this->set_actionModifierButton_23(((int32_t)10));
		Vector2_t2243707579  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3067419446(&L_0, (0.2f), (0.2f), /*hidden argument*/NULL);
		__this->set_axisDeadzone_24(L_0);
		VRTK_ObjectControl__ctor_m682487718(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadControl::OnEnable()
extern "C"  void VRTK_TouchpadControl_OnEnable_m2800640977 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method)
{
	{
		VRTK_ObjectControl_OnEnable_m2865670526(__this, /*hidden argument*/NULL);
		__this->set_touchpadFirstChange_25((bool)1);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadControl::ControlFixedUpdate()
extern "C"  void VRTK_TouchpadControl_ControlFixedUpdate_m251177845 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(22 /* System.Void VRTK.VRTK_TouchpadControl::ModifierButtonActive() */, __this);
		Vector2_t2243707579 * L_0 = ((VRTK_ObjectControl_t724022372 *)__this)->get_address_of_currentAxis_16();
		float L_1 = L_0->get_x_0();
		Vector2_t2243707579 * L_2 = __this->get_address_of_axisDeadzone_24();
		float L_3 = L_2->get_x_0();
		bool L_4 = VirtFuncInvoker2< bool, float, float >::Invoke(20 /* System.Boolean VRTK.VRTK_TouchpadControl::OutsideDeadzone(System.Single,System.Single) */, __this, L_1, L_3);
		if (L_4)
		{
			goto IL_003c;
		}
	}
	{
		Vector2_t2243707579 * L_5 = ((VRTK_ObjectControl_t724022372 *)__this)->get_address_of_currentAxis_16();
		float L_6 = L_5->get_x_0();
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_0069;
		}
	}

IL_003c:
	{
		Transform_t3275118058 * L_7 = ((VRTK_ObjectControl_t724022372 *)__this)->get_directionDevice_14();
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_right_m440863970(L_7, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_9 = ((VRTK_ObjectControl_t724022372 *)__this)->get_address_of_currentAxis_16();
		float L_10 = L_9->get_x_0();
		Vector2_t2243707579 * L_11 = __this->get_address_of_axisDeadzone_24();
		float L_12 = L_11->get_x_0();
		ObjectControlEventArgs_t2459490319  L_13 = VirtFuncInvoker3< ObjectControlEventArgs_t2459490319 , Vector3_t2243707580 , float, float >::Invoke(14 /* VRTK.ObjectControlEventArgs VRTK.VRTK_ObjectControl::SetEventArguements(UnityEngine.Vector3,System.Single,System.Single) */, __this, L_8, L_10, L_12);
		VirtActionInvoker1< ObjectControlEventArgs_t2459490319  >::Invoke(4 /* System.Void VRTK.VRTK_ObjectControl::OnXAxisChanged(VRTK.ObjectControlEventArgs) */, __this, L_13);
	}

IL_0069:
	{
		Vector2_t2243707579 * L_14 = ((VRTK_ObjectControl_t724022372 *)__this)->get_address_of_currentAxis_16();
		float L_15 = L_14->get_y_1();
		Vector2_t2243707579 * L_16 = __this->get_address_of_axisDeadzone_24();
		float L_17 = L_16->get_y_1();
		bool L_18 = VirtFuncInvoker2< bool, float, float >::Invoke(20 /* System.Boolean VRTK.VRTK_TouchpadControl::OutsideDeadzone(System.Single,System.Single) */, __this, L_15, L_17);
		if (L_18)
		{
			goto IL_009f;
		}
	}
	{
		Vector2_t2243707579 * L_19 = ((VRTK_ObjectControl_t724022372 *)__this)->get_address_of_currentAxis_16();
		float L_20 = L_19->get_x_0();
		if ((!(((float)L_20) == ((float)(0.0f)))))
		{
			goto IL_00cc;
		}
	}

IL_009f:
	{
		Transform_t3275118058 * L_21 = ((VRTK_ObjectControl_t724022372 *)__this)->get_directionDevice_14();
		NullCheck(L_21);
		Vector3_t2243707580  L_22 = Transform_get_forward_m1833488937(L_21, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_23 = ((VRTK_ObjectControl_t724022372 *)__this)->get_address_of_currentAxis_16();
		float L_24 = L_23->get_y_1();
		Vector2_t2243707579 * L_25 = __this->get_address_of_axisDeadzone_24();
		float L_26 = L_25->get_y_1();
		ObjectControlEventArgs_t2459490319  L_27 = VirtFuncInvoker3< ObjectControlEventArgs_t2459490319 , Vector3_t2243707580 , float, float >::Invoke(14 /* VRTK.ObjectControlEventArgs VRTK.VRTK_ObjectControl::SetEventArguements(UnityEngine.Vector3,System.Single,System.Single) */, __this, L_22, L_24, L_26);
		VirtActionInvoker1< ObjectControlEventArgs_t2459490319  >::Invoke(5 /* System.Void VRTK.VRTK_ObjectControl::OnYAxisChanged(VRTK.ObjectControlEventArgs) */, __this, L_27);
	}

IL_00cc:
	{
		return;
	}
}
// VRTK.VRTK_ObjectControl VRTK.VRTK_TouchpadControl::GetOtherControl()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisVRTK_TouchpadControl_t3191058221_m3229846933_MethodInfo_var;
extern const uint32_t VRTK_TouchpadControl_GetOtherControl_m3058459202_MetadataUsageId;
extern "C"  VRTK_ObjectControl_t724022372 * VRTK_TouchpadControl_GetOtherControl_m3058459202 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadControl_GetOtherControl_m3058459202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObject_t1756533147 * G_B3_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		bool L_1 = VRTK_DeviceFinder_IsControllerLeftHand_m2483100704(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		GameObject_t1756533147 * L_2 = VRTK_DeviceFinder_GetControllerRightHand_m2938812741(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0021;
	}

IL_001b:
	{
		GameObject_t1756533147 * L_3 = VRTK_DeviceFinder_GetControllerLeftHand_m151411604(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0021:
	{
		V_0 = G_B3_0;
		GameObject_t1756533147 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		GameObject_t1756533147 * L_6 = V_0;
		NullCheck(L_6);
		VRTK_TouchpadControl_t3191058221 * L_7 = GameObject_GetComponent_TisVRTK_TouchpadControl_t3191058221_m3229846933(L_6, /*hidden argument*/GameObject_GetComponent_TisVRTK_TouchpadControl_t3191058221_m3229846933_MethodInfo_var);
		return L_7;
	}

IL_0034:
	{
		return (VRTK_ObjectControl_t724022372 *)NULL;
	}
}
// System.Void VRTK.VRTK_TouchpadControl::SetListeners(System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ControllerInteractionEventHandler_t343979916_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadControl_SetListeners_m3984552921_MetadataUsageId;
extern "C"  void VRTK_TouchpadControl_SetListeners_m3984552921 (VRTK_TouchpadControl_t3191058221 * __this, bool ___state0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadControl_SetListeners_m3984552921_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_0 = ((VRTK_ObjectControl_t724022372 *)__this)->get_controllerEvents_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007b;
		}
	}
	{
		bool L_2 = ___state0;
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_3 = ((VRTK_ObjectControl_t724022372 *)__this)->get_controllerEvents_9();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 24));
		ControllerInteractionEventHandler_t343979916 * L_5 = (ControllerInteractionEventHandler_t343979916 *)il2cpp_codegen_object_new(ControllerInteractionEventHandler_t343979916_il2cpp_TypeInfo_var);
		ControllerInteractionEventHandler__ctor_m4206169134(L_5, __this, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		VRTK_ControllerEvents_add_TouchpadAxisChanged_m3280812681(L_3, L_5, /*hidden argument*/NULL);
		VRTK_ControllerEvents_t3225224819 * L_6 = ((VRTK_ObjectControl_t724022372 *)__this)->get_controllerEvents_9();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 25));
		ControllerInteractionEventHandler_t343979916 * L_8 = (ControllerInteractionEventHandler_t343979916 *)il2cpp_codegen_object_new(ControllerInteractionEventHandler_t343979916_il2cpp_TypeInfo_var);
		ControllerInteractionEventHandler__ctor_m4206169134(L_8, __this, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		VRTK_ControllerEvents_add_TouchpadTouchEnd_m4159394722(L_6, L_8, /*hidden argument*/NULL);
		goto IL_007b;
	}

IL_004b:
	{
		VRTK_ControllerEvents_t3225224819 * L_9 = ((VRTK_ObjectControl_t724022372 *)__this)->get_controllerEvents_9();
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 24));
		ControllerInteractionEventHandler_t343979916 * L_11 = (ControllerInteractionEventHandler_t343979916 *)il2cpp_codegen_object_new(ControllerInteractionEventHandler_t343979916_il2cpp_TypeInfo_var);
		ControllerInteractionEventHandler__ctor_m4206169134(L_11, __this, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		VRTK_ControllerEvents_remove_TouchpadAxisChanged_m2513257630(L_9, L_11, /*hidden argument*/NULL);
		VRTK_ControllerEvents_t3225224819 * L_12 = ((VRTK_ObjectControl_t724022372 *)__this)->get_controllerEvents_9();
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 25));
		ControllerInteractionEventHandler_t343979916 * L_14 = (ControllerInteractionEventHandler_t343979916 *)il2cpp_codegen_object_new(ControllerInteractionEventHandler_t343979916_il2cpp_TypeInfo_var);
		ControllerInteractionEventHandler__ctor_m4206169134(L_14, __this, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		VRTK_ControllerEvents_remove_TouchpadTouchEnd_m1310523989(L_12, L_14, /*hidden argument*/NULL);
	}

IL_007b:
	{
		return;
	}
}
// System.Boolean VRTK.VRTK_TouchpadControl::IsInAction()
extern "C"  bool VRTK_TouchpadControl_IsInAction_m882793342 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean VRTK.VRTK_TouchpadControl::ValidPrimaryButton() */, __this);
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(23 /* System.Boolean VRTK.VRTK_TouchpadControl::TouchpadTouched() */, __this);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_0014;
	}

IL_0013:
	{
		G_B3_0 = 0;
	}

IL_0014:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean VRTK.VRTK_TouchpadControl::OutsideDeadzone(System.Single,System.Single)
extern "C"  bool VRTK_TouchpadControl_OutsideDeadzone_m1532533014 (VRTK_TouchpadControl_t3191058221 * __this, float ___axisValue0, float ___deadzoneThreshold1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		float L_0 = ___axisValue0;
		float L_1 = ___deadzoneThreshold1;
		if ((((float)L_0) > ((float)L_1)))
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___axisValue0;
		float L_3 = ___deadzoneThreshold1;
		G_B3_0 = ((((float)L_2) < ((float)((-L_3))))? 1 : 0);
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = 1;
	}

IL_000f:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean VRTK.VRTK_TouchpadControl::ValidPrimaryButton()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadControl_ValidPrimaryButton_m2469929053_MetadataUsageId;
extern "C"  bool VRTK_TouchpadControl_ValidPrimaryButton_m2469929053 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadControl_ValidPrimaryButton_m2469929053_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		VRTK_ControllerEvents_t3225224819 * L_0 = ((VRTK_ObjectControl_t724022372 *)__this)->get_controllerEvents_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_2 = __this->get_primaryActivationButton_22();
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_3 = ((VRTK_ObjectControl_t724022372 *)__this)->get_controllerEvents_9();
		int32_t L_4 = __this->get_primaryActivationButton_22();
		NullCheck(L_3);
		bool L_5 = VRTK_ControllerEvents_IsButtonPressed_m407246062(L_3, L_4, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_5));
		goto IL_002f;
	}

IL_002e:
	{
		G_B4_0 = 1;
	}

IL_002f:
	{
		G_B6_0 = G_B4_0;
		goto IL_0032;
	}

IL_0031:
	{
		G_B6_0 = 0;
	}

IL_0032:
	{
		return (bool)G_B6_0;
	}
}
// System.Void VRTK.VRTK_TouchpadControl::ModifierButtonActive()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadControl_ModifierButtonActive_m4102088174_MetadataUsageId;
extern "C"  void VRTK_TouchpadControl_ModifierButtonActive_m4102088174 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadControl_ModifierButtonActive_m4102088174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_TouchpadControl_t3191058221 * G_B3_0 = NULL;
	VRTK_TouchpadControl_t3191058221 * G_B1_0 = NULL;
	VRTK_TouchpadControl_t3191058221 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	VRTK_TouchpadControl_t3191058221 * G_B4_1 = NULL;
	{
		VRTK_ControllerEvents_t3225224819 * L_0 = ((VRTK_ObjectControl_t724022372 *)__this)->get_controllerEvents_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_1)
		{
			G_B3_0 = __this;
			goto IL_002f;
		}
	}
	{
		int32_t L_2 = __this->get_actionModifierButton_23();
		G_B2_0 = G_B1_0;
		if (!L_2)
		{
			G_B3_0 = G_B1_0;
			goto IL_002f;
		}
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_3 = ((VRTK_ObjectControl_t724022372 *)__this)->get_controllerEvents_9();
		int32_t L_4 = __this->get_actionModifierButton_23();
		NullCheck(L_3);
		bool L_5 = VRTK_ControllerEvents_IsButtonPressed_m407246062(L_3, L_4, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_5));
		G_B4_1 = G_B2_0;
		goto IL_0030;
	}

IL_002f:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0030:
	{
		NullCheck(G_B4_1);
		((VRTK_ObjectControl_t724022372 *)G_B4_1)->set_modifierActive_19((bool)G_B4_0);
		return;
	}
}
// System.Boolean VRTK.VRTK_TouchpadControl::TouchpadTouched()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadControl_TouchpadTouched_m1583952841_MetadataUsageId;
extern "C"  bool VRTK_TouchpadControl_TouchpadTouched_m1583952841 (VRTK_TouchpadControl_t3191058221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadControl_TouchpadTouched_m1583952841_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		VRTK_ControllerEvents_t3225224819 * L_0 = ((VRTK_ObjectControl_t724022372 *)__this)->get_controllerEvents_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_2 = ((VRTK_ObjectControl_t724022372 *)__this)->get_controllerEvents_9();
		NullCheck(L_2);
		bool L_3 = VRTK_ControllerEvents_IsButtonPressed_m407246062(L_2, ((int32_t)9), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 0;
	}

IL_0020:
	{
		return (bool)G_B3_0;
	}
}
// System.Void VRTK.VRTK_TouchpadControl::TouchpadAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadControl_TouchpadAxisChanged_m528373126_MetadataUsageId;
extern "C"  void VRTK_TouchpadControl_TouchpadAxisChanged_m528373126 (VRTK_TouchpadControl_t3191058221 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadControl_TouchpadAxisChanged_m528373126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_TouchpadControl_t3191058221 * G_B7_0 = NULL;
	VRTK_TouchpadControl_t3191058221 * G_B6_0 = NULL;
	Vector2_t2243707579  G_B8_0;
	memset(&G_B8_0, 0, sizeof(G_B8_0));
	VRTK_TouchpadControl_t3191058221 * G_B8_1 = NULL;
	{
		bool L_0 = __this->get_touchpadFirstChange_25();
		if (!L_0)
		{
			goto IL_0059;
		}
	}
	{
		VRTK_ObjectControl_t724022372 * L_1 = ((VRTK_ObjectControl_t724022372 *)__this)->get_otherObjectControl_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0059;
		}
	}
	{
		bool L_3 = ((VRTK_ObjectControl_t724022372 *)__this)->get_disableOtherControlsOnActive_4();
		if (!L_3)
		{
			goto IL_0059;
		}
	}
	{
		Vector2_t2243707579  L_4 = (&___e1)->get_touchpadAxis_2();
		Vector2_t2243707579  L_5 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_6 = Vector2_op_Inequality_m4283136193(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0059;
		}
	}
	{
		VRTK_ObjectControl_t724022372 * L_7 = ((VRTK_ObjectControl_t724022372 *)__this)->get_otherObjectControl_11();
		NullCheck(L_7);
		bool L_8 = Behaviour_get_enabled_m4079055610(L_7, /*hidden argument*/NULL);
		__this->set_otherTouchpadControlEnabledState_26(L_8);
		VRTK_ObjectControl_t724022372 * L_9 = ((VRTK_ObjectControl_t724022372 *)__this)->get_otherObjectControl_11();
		NullCheck(L_9);
		Behaviour_set_enabled_m1796096907(L_9, (bool)0, /*hidden argument*/NULL);
	}

IL_0059:
	{
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean VRTK.VRTK_TouchpadControl::ValidPrimaryButton() */, __this);
		G_B6_0 = __this;
		if (!L_10)
		{
			G_B7_0 = __this;
			goto IL_0071;
		}
	}
	{
		Vector2_t2243707579  L_11 = (&___e1)->get_touchpadAxis_2();
		G_B8_0 = L_11;
		G_B8_1 = G_B6_0;
		goto IL_0076;
	}

IL_0071:
	{
		Vector2_t2243707579  L_12 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_12;
		G_B8_1 = G_B7_0;
	}

IL_0076:
	{
		NullCheck(G_B8_1);
		((VRTK_ObjectControl_t724022372 *)G_B8_1)->set_currentAxis_16(G_B8_0);
		Vector2_t2243707579  L_13 = ((VRTK_ObjectControl_t724022372 *)__this)->get_currentAxis_16();
		Vector2_t2243707579  L_14 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_15 = Vector2_op_Inequality_m4283136193(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0097;
		}
	}
	{
		__this->set_touchpadFirstChange_25((bool)0);
	}

IL_0097:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadControl::TouchpadTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadControl_TouchpadTouchEnd_m1541439557_MetadataUsageId;
extern "C"  void VRTK_TouchpadControl_TouchpadTouchEnd_m1541439557 (VRTK_TouchpadControl_t3191058221 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadControl_TouchpadTouchEnd_m1541439557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VRTK_ObjectControl_t724022372 * L_0 = ((VRTK_ObjectControl_t724022372 *)__this)->get_otherObjectControl_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		bool L_2 = ((VRTK_ObjectControl_t724022372 *)__this)->get_disableOtherControlsOnActive_4();
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		VRTK_ObjectControl_t724022372 * L_3 = ((VRTK_ObjectControl_t724022372 *)__this)->get_otherObjectControl_11();
		bool L_4 = __this->get_otherTouchpadControlEnabledState_26();
		NullCheck(L_3);
		Behaviour_set_enabled_m1796096907(L_3, L_4, /*hidden argument*/NULL);
	}

IL_002c:
	{
		Vector2_t2243707579  L_5 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		((VRTK_ObjectControl_t724022372 *)__this)->set_currentAxis_16(L_5);
		__this->set_touchpadFirstChange_25((bool)1);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::.ctor()
extern "C"  void VRTK_TouchpadMovement__ctor_m1906722849 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method)
{
	{
		__this->set_leftController_3((bool)1);
		__this->set_rightController_4((bool)1);
		__this->set_verticalAxisMovement_7(1);
		__this->set_verticalDeadzone_8((0.2f));
		__this->set_verticalMultiplier_9((1.5f));
		__this->set_flipDeadzone_12((0.7f));
		__this->set_flipDelay_13((1.0f));
		__this->set_flipBlink_14((bool)1);
		__this->set_horizontalAxisMovement_15(1);
		__this->set_horizontalDeadzone_16((0.2f));
		__this->set_horizontalMultiplier_17((1.25f));
		__this->set_snapRotateDelay_18((0.5f));
		__this->set_snapRotateAngle_19((30.0f));
		__this->set_rotateMaxSpeed_20((3.0f));
		__this->set_blinkDurationMultiplier_21((0.7f));
		__this->set_slideMaxSpeed_22((3.0f));
		__this->set_slideDeceleration_23((0.1f));
		__this->set_warpDelay_24((0.5f));
		__this->set_warpRange_25((1.0f));
		__this->set_warpMaxAltitudeChange_26((1.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::add_AxisMovement(VRTK.TouchpadMovementAxisEventHandler)
extern Il2CppClass* TouchpadMovementAxisEventHandler_t2208405138_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadMovement_add_AxisMovement_m2558945748_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_add_AxisMovement_m2558945748 (VRTK_TouchpadMovement_t1979813355 * __this, TouchpadMovementAxisEventHandler_t2208405138 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_add_AxisMovement_m2558945748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TouchpadMovementAxisEventHandler_t2208405138 * V_0 = NULL;
	TouchpadMovementAxisEventHandler_t2208405138 * V_1 = NULL;
	{
		TouchpadMovementAxisEventHandler_t2208405138 * L_0 = __this->get_AxisMovement_2();
		V_0 = L_0;
	}

IL_0007:
	{
		TouchpadMovementAxisEventHandler_t2208405138 * L_1 = V_0;
		V_1 = L_1;
		TouchpadMovementAxisEventHandler_t2208405138 ** L_2 = __this->get_address_of_AxisMovement_2();
		TouchpadMovementAxisEventHandler_t2208405138 * L_3 = V_1;
		TouchpadMovementAxisEventHandler_t2208405138 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		TouchpadMovementAxisEventHandler_t2208405138 * L_6 = V_0;
		TouchpadMovementAxisEventHandler_t2208405138 * L_7 = InterlockedCompareExchangeImpl<TouchpadMovementAxisEventHandler_t2208405138 *>(L_2, ((TouchpadMovementAxisEventHandler_t2208405138 *)CastclassSealed(L_5, TouchpadMovementAxisEventHandler_t2208405138_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		TouchpadMovementAxisEventHandler_t2208405138 * L_8 = V_0;
		TouchpadMovementAxisEventHandler_t2208405138 * L_9 = V_1;
		if ((!(((Il2CppObject*)(TouchpadMovementAxisEventHandler_t2208405138 *)L_8) == ((Il2CppObject*)(TouchpadMovementAxisEventHandler_t2208405138 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::remove_AxisMovement(VRTK.TouchpadMovementAxisEventHandler)
extern Il2CppClass* TouchpadMovementAxisEventHandler_t2208405138_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadMovement_remove_AxisMovement_m1781378661_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_remove_AxisMovement_m1781378661 (VRTK_TouchpadMovement_t1979813355 * __this, TouchpadMovementAxisEventHandler_t2208405138 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_remove_AxisMovement_m1781378661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TouchpadMovementAxisEventHandler_t2208405138 * V_0 = NULL;
	TouchpadMovementAxisEventHandler_t2208405138 * V_1 = NULL;
	{
		TouchpadMovementAxisEventHandler_t2208405138 * L_0 = __this->get_AxisMovement_2();
		V_0 = L_0;
	}

IL_0007:
	{
		TouchpadMovementAxisEventHandler_t2208405138 * L_1 = V_0;
		V_1 = L_1;
		TouchpadMovementAxisEventHandler_t2208405138 ** L_2 = __this->get_address_of_AxisMovement_2();
		TouchpadMovementAxisEventHandler_t2208405138 * L_3 = V_1;
		TouchpadMovementAxisEventHandler_t2208405138 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		TouchpadMovementAxisEventHandler_t2208405138 * L_6 = V_0;
		TouchpadMovementAxisEventHandler_t2208405138 * L_7 = InterlockedCompareExchangeImpl<TouchpadMovementAxisEventHandler_t2208405138 *>(L_2, ((TouchpadMovementAxisEventHandler_t2208405138 *)CastclassSealed(L_5, TouchpadMovementAxisEventHandler_t2208405138_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		TouchpadMovementAxisEventHandler_t2208405138 * L_8 = V_0;
		TouchpadMovementAxisEventHandler_t2208405138 * L_9 = V_1;
		if ((!(((Il2CppObject*)(TouchpadMovementAxisEventHandler_t2208405138 *)L_8) == ((Il2CppObject*)(TouchpadMovementAxisEventHandler_t2208405138 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::Awake()
extern Il2CppClass* ControllerInteractionEventHandler_t343979916_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* VRTK_TouchpadMovement_DoTouchpadAxisChanged_m314873841_MethodInfo_var;
extern const MethodInfo* VRTK_TouchpadMovement_DoTouchpadTouchEnd_m3376986714_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1619768869;
extern Il2CppCodeGenString* _stringLiteral457491230;
extern const uint32_t VRTK_TouchpadMovement_Awake_m2540975428_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_Awake_m2540975428 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_Awake_m2540975428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)VRTK_TouchpadMovement_DoTouchpadAxisChanged_m314873841_MethodInfo_var);
		ControllerInteractionEventHandler_t343979916 * L_1 = (ControllerInteractionEventHandler_t343979916 *)il2cpp_codegen_object_new(ControllerInteractionEventHandler_t343979916_il2cpp_TypeInfo_var);
		ControllerInteractionEventHandler__ctor_m4206169134(L_1, __this, L_0, /*hidden argument*/NULL);
		__this->set_touchpadAxisChanged_42(L_1);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)VRTK_TouchpadMovement_DoTouchpadTouchEnd_m3376986714_MethodInfo_var);
		ControllerInteractionEventHandler_t343979916 * L_3 = (ControllerInteractionEventHandler_t343979916 *)il2cpp_codegen_object_new(ControllerInteractionEventHandler_t343979916_il2cpp_TypeInfo_var);
		ControllerInteractionEventHandler__ctor_m4206169134(L_3, __this, L_2, /*hidden argument*/NULL);
		__this->set_touchpadUntouched_43(L_3);
		GameObject_t1756533147 * L_4 = VRTK_DeviceFinder_GetControllerLeftHand_m151411604(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_controllerLeftHand_27(L_4);
		GameObject_t1756533147 * L_5 = VRTK_DeviceFinder_GetControllerRightHand_m2938812741(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_controllerRightHand_28(L_5);
		Transform_t3275118058 * L_6 = VRTK_DeviceFinder_PlayAreaTransform_m1884263923(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_playArea_29(L_6);
		Transform_t3275118058 * L_7 = __this->get_playArea_29();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0061;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1619768869, /*hidden argument*/NULL);
	}

IL_0061:
	{
		Transform_t3275118058 * L_9 = VRTK_DeviceFinder_HeadsetTransform_m2991712096(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_headset_39(L_9);
		Transform_t3275118058 * L_10 = __this->get_headset_39();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0086;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral457491230, /*hidden argument*/NULL);
	}

IL_0086:
	{
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		VRTK_PlayerObject_SetPlayerObject_m1121178556(NULL /*static, unused*/, L_12, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::OnEnable()
extern const MethodInfo* Component_GetComponent_TisVRTK_BodyPhysics_t3414085265_m1861204865_MethodInfo_var;
extern const uint32_t VRTK_TouchpadMovement_OnEnable_m1783118689_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_OnEnable_m1783118689 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_OnEnable_m1783118689_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_controllerLeftHand_27();
		bool L_1 = __this->get_leftController_3();
		bool* L_2 = __this->get_address_of_leftSubscribed_40();
		VRTK_TouchpadMovement_SetControllerListeners_m814129575(__this, L_0, L_1, L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_controllerRightHand_28();
		bool L_4 = __this->get_rightController_4();
		bool* L_5 = __this->get_address_of_rightSubscribed_41();
		VRTK_TouchpadMovement_SetControllerListeners_m814129575(__this, L_3, L_4, L_5, (bool)0, /*hidden argument*/NULL);
		VRTK_BodyPhysics_t3414085265 * L_6 = Component_GetComponent_TisVRTK_BodyPhysics_t3414085265_m1861204865(__this, /*hidden argument*/Component_GetComponent_TisVRTK_BodyPhysics_t3414085265_m1861204865_MethodInfo_var);
		__this->set_bodyPhysics_45(L_6);
		__this->set_movementSpeed_31((0.0f));
		__this->set_strafeSpeed_32((0.0f));
		__this->set_blinkFadeInTime_33((0.0f));
		__this->set_lastWarp_34((0.0f));
		__this->set_lastFlip_35((0.0f));
		__this->set_lastSnapRotate_36((0.0f));
		__this->set_multiplyMovement_37((bool)0);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCapsuleCollider_t720607407_m1210329947_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1338124318;
extern const uint32_t VRTK_TouchpadMovement_Start_m3065344833_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_Start_m3065344833 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_Start_m3065344833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = __this->get_playArea_29();
		NullCheck(L_0);
		CapsuleCollider_t720607407 * L_1 = Component_GetComponent_TisCapsuleCollider_t720607407_m1210329947(L_0, /*hidden argument*/Component_GetComponent_TisCapsuleCollider_t720607407_m1210329947_MethodInfo_var);
		__this->set_bodyCollider_38(L_1);
		CapsuleCollider_t720607407 * L_2 = __this->get_bodyCollider_38();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1338124318, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::OnDisable()
extern "C"  void VRTK_TouchpadMovement_OnDisable_m608677076 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_controllerLeftHand_27();
		bool L_1 = __this->get_leftController_3();
		bool* L_2 = __this->get_address_of_leftSubscribed_40();
		VRTK_TouchpadMovement_SetControllerListeners_m814129575(__this, L_0, L_1, L_2, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_controllerRightHand_28();
		bool L_4 = __this->get_rightController_4();
		bool* L_5 = __this->get_address_of_rightSubscribed_41();
		VRTK_TouchpadMovement_SetControllerListeners_m814129575(__this, L_3, L_4, L_5, (bool)1, /*hidden argument*/NULL);
		__this->set_bodyPhysics_45((VRTK_BodyPhysics_t3414085265 *)NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadMovement_Update_m1620871106_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_Update_m1620871106 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_Update_m1620871106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_TouchpadMovement_t1979813355 * G_B3_0 = NULL;
	VRTK_TouchpadMovement_t1979813355 * G_B1_0 = NULL;
	VRTK_TouchpadMovement_t1979813355 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	VRTK_TouchpadMovement_t1979813355 * G_B4_1 = NULL;
	{
		VRTK_ControllerEvents_t3225224819 * L_0 = __this->get_controllerEvents_44();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_1)
		{
			G_B3_0 = __this;
			goto IL_002f;
		}
	}
	{
		int32_t L_2 = __this->get_movementMultiplierButton_6();
		G_B2_0 = G_B1_0;
		if (!L_2)
		{
			G_B3_0 = G_B1_0;
			goto IL_002f;
		}
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_3 = __this->get_controllerEvents_44();
		int32_t L_4 = __this->get_movementMultiplierButton_6();
		NullCheck(L_3);
		bool L_5 = VRTK_ControllerEvents_IsButtonPressed_m407246062(L_3, L_4, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_5));
		G_B4_1 = G_B2_0;
		goto IL_0030;
	}

IL_002f:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0030:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_multiplyMovement_37((bool)G_B4_0);
		GameObject_t1756533147 * L_6 = __this->get_controllerLeftHand_27();
		bool L_7 = __this->get_leftController_3();
		bool* L_8 = __this->get_address_of_leftSubscribed_40();
		bool* L_9 = __this->get_address_of_previousLeftControllerState_47();
		VirtActionInvoker4< GameObject_t1756533147 *, bool, bool*, bool* >::Invoke(11 /* System.Void VRTK.VRTK_TouchpadMovement::CheckControllerState(UnityEngine.GameObject,System.Boolean,System.Boolean&,System.Boolean&) */, __this, L_6, L_7, L_8, L_9);
		GameObject_t1756533147 * L_10 = __this->get_controllerRightHand_28();
		bool L_11 = __this->get_rightController_4();
		bool* L_12 = __this->get_address_of_rightSubscribed_41();
		bool* L_13 = __this->get_address_of_previousRightControllerState_48();
		VirtActionInvoker4< GameObject_t1756533147 *, bool, bool*, bool* >::Invoke(11 /* System.Void VRTK.VRTK_TouchpadMovement::CheckControllerState(UnityEngine.GameObject,System.Boolean,System.Boolean&,System.Boolean&) */, __this, L_10, L_11, L_12, L_13);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::FixedUpdate()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadMovement_FixedUpdate_m694020628_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_FixedUpdate_m694020628 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_FixedUpdate_m694020628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		VirtActionInvoker0::Invoke(10 /* System.Void VRTK.VRTK_TouchpadMovement::HandleFalling() */, __this);
		int32_t L_0 = __this->get_horizontalAxisMovement_15();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0033;
		}
	}
	{
		float* L_1 = __this->get_address_of_strafeSpeed_32();
		Vector2_t2243707579 * L_2 = __this->get_address_of_touchAxis_30();
		float L_3 = L_2->get_x_0();
		VRTK_TouchpadMovement_CalculateSpeed_m746457475(__this, (bool)1, L_1, L_3, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_00f5;
	}

IL_0033:
	{
		int32_t L_4 = __this->get_horizontalAxisMovement_15();
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_004a;
		}
	}
	{
		VRTK_TouchpadMovement_Rotate_m4105360080(__this, /*hidden argument*/NULL);
		goto IL_00f5;
	}

IL_004a:
	{
		int32_t L_5 = __this->get_horizontalAxisMovement_15();
		if ((((int32_t)L_5) == ((int32_t)3)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_6 = __this->get_horizontalAxisMovement_15();
		if ((!(((uint32_t)L_6) == ((uint32_t)4))))
		{
			goto IL_00a2;
		}
	}

IL_0062:
	{
		Vector2_t2243707579 * L_7 = __this->get_address_of_touchAxis_30();
		float L_8 = L_7->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_9 = fabsf(L_8);
		float L_10 = __this->get_horizontalDeadzone_16();
		if ((!(((float)L_9) > ((float)L_10))))
		{
			goto IL_00a2;
		}
	}
	{
		float L_11 = __this->get_lastSnapRotate_36();
		float L_12 = Time_get_timeSinceLevelLoad_m1980066582(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_11) < ((float)L_12))))
		{
			goto IL_00a2;
		}
	}
	{
		int32_t L_13 = __this->get_horizontalAxisMovement_15();
		VRTK_TouchpadMovement_SnapRotate_m1907291896(__this, (bool)((((int32_t)L_13) == ((int32_t)4))? 1 : 0), (bool)0, /*hidden argument*/NULL);
		goto IL_00f5;
	}

IL_00a2:
	{
		int32_t L_14 = __this->get_horizontalAxisMovement_15();
		if ((((int32_t)L_14) == ((int32_t)5)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_15 = __this->get_horizontalAxisMovement_15();
		if ((!(((uint32_t)L_15) == ((uint32_t)6))))
		{
			goto IL_00f5;
		}
	}

IL_00ba:
	{
		Vector2_t2243707579 * L_16 = __this->get_address_of_touchAxis_30();
		float L_17 = L_16->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_18 = fabsf(L_17);
		float L_19 = __this->get_horizontalDeadzone_16();
		if ((!(((float)L_18) > ((float)L_19))))
		{
			goto IL_00f5;
		}
	}
	{
		float L_20 = __this->get_lastWarp_34();
		float L_21 = Time_get_timeSinceLevelLoad_m1980066582(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_20) < ((float)L_21))))
		{
			goto IL_00f5;
		}
	}
	{
		int32_t L_22 = __this->get_horizontalAxisMovement_15();
		VRTK_TouchpadMovement_Warp_m2450696817(__this, (bool)((((int32_t)L_22) == ((int32_t)6))? 1 : 0), (bool)1, /*hidden argument*/NULL);
	}

IL_00f5:
	{
		bool L_23 = __this->get_flipDirectionEnabled_11();
		if (!L_23)
		{
			goto IL_0160;
		}
	}
	{
		Vector2_t2243707579 * L_24 = __this->get_address_of_touchAxis_30();
		float L_25 = L_24->get_y_1();
		if ((!(((float)L_25) < ((float)(0.0f)))))
		{
			goto IL_0160;
		}
	}
	{
		Vector2_t2243707579 * L_26 = __this->get_address_of_touchAxis_30();
		float L_27 = L_26->get_y_1();
		float L_28 = __this->get_flipDeadzone_12();
		if ((!(((float)L_27) < ((float)((-L_28))))))
		{
			goto IL_015b;
		}
	}
	{
		float L_29 = __this->get_lastFlip_35();
		float L_30 = Time_get_timeSinceLevelLoad_m1980066582(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_29) < ((float)L_30))))
		{
			goto IL_015b;
		}
	}
	{
		float L_31 = Time_get_timeSinceLevelLoad_m1980066582(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_32 = __this->get_flipDelay_13();
		__this->set_lastFlip_35(((float)((float)L_31+(float)L_32)));
		bool L_33 = __this->get_flipBlink_14();
		VRTK_TouchpadMovement_SnapRotate_m1907291896(__this, L_33, (bool)1, /*hidden argument*/NULL);
	}

IL_015b:
	{
		goto IL_01de;
	}

IL_0160:
	{
		int32_t L_34 = __this->get_verticalAxisMovement_7();
		if ((!(((uint32_t)L_34) == ((uint32_t)1))))
		{
			goto IL_018b;
		}
	}
	{
		float* L_35 = __this->get_address_of_movementSpeed_31();
		Vector2_t2243707579 * L_36 = __this->get_address_of_touchAxis_30();
		float L_37 = L_36->get_y_1();
		VRTK_TouchpadMovement_CalculateSpeed_m746457475(__this, (bool)0, L_35, L_37, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_01de;
	}

IL_018b:
	{
		int32_t L_38 = __this->get_verticalAxisMovement_7();
		if ((((int32_t)L_38) == ((int32_t)2)))
		{
			goto IL_01a3;
		}
	}
	{
		int32_t L_39 = __this->get_verticalAxisMovement_7();
		if ((!(((uint32_t)L_39) == ((uint32_t)3))))
		{
			goto IL_01de;
		}
	}

IL_01a3:
	{
		Vector2_t2243707579 * L_40 = __this->get_address_of_touchAxis_30();
		float L_41 = L_40->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_42 = fabsf(L_41);
		float L_43 = __this->get_verticalDeadzone_8();
		if ((!(((float)L_42) > ((float)L_43))))
		{
			goto IL_01de;
		}
	}
	{
		float L_44 = __this->get_lastWarp_34();
		float L_45 = Time_get_timeSinceLevelLoad_m1980066582(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_44) < ((float)L_45))))
		{
			goto IL_01de;
		}
	}
	{
		int32_t L_46 = __this->get_verticalAxisMovement_7();
		VRTK_TouchpadMovement_Warp_m2450696817(__this, (bool)((((int32_t)L_46) == ((int32_t)3))? 1 : 0), (bool)0, /*hidden argument*/NULL);
	}

IL_01de:
	{
		bool L_47 = V_0;
		if (!L_47)
		{
			goto IL_01ea;
		}
	}
	{
		VRTK_TouchpadMovement_Move_m3310755870(__this, /*hidden argument*/NULL);
	}

IL_01ea:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::HandleFalling()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadMovement_HandleFalling_m3684340994_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_HandleFalling_m3684340994 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_HandleFalling_m3684340994_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VRTK_BodyPhysics_t3414085265 * L_0 = __this->get_bodyPhysics_45();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		VRTK_BodyPhysics_t3414085265 * L_2 = __this->get_bodyPhysics_45();
		NullCheck(L_2);
		bool L_3 = VRTK_BodyPhysics_IsFalling_m3925295988(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		Vector2_t2243707579  L_4 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_touchAxis_30(L_4);
		__this->set_wasFalling_46((bool)1);
	}

IL_0032:
	{
		VRTK_BodyPhysics_t3414085265 * L_5 = __this->get_bodyPhysics_45();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0085;
		}
	}
	{
		VRTK_BodyPhysics_t3414085265 * L_7 = __this->get_bodyPhysics_45();
		NullCheck(L_7);
		bool L_8 = VRTK_BodyPhysics_IsFalling_m3925295988(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0085;
		}
	}
	{
		bool L_9 = __this->get_wasFalling_46();
		if (!L_9)
		{
			goto IL_0085;
		}
	}
	{
		Vector2_t2243707579  L_10 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_touchAxis_30(L_10);
		__this->set_wasFalling_46((bool)0);
		__this->set_strafeSpeed_32((0.0f));
		__this->set_movementSpeed_31((0.0f));
	}

IL_0085:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::CheckControllerState(UnityEngine.GameObject,System.Boolean,System.Boolean&,System.Boolean&)
extern "C"  void VRTK_TouchpadMovement_CheckControllerState_m2599412199 (VRTK_TouchpadMovement_t1979813355 * __this, GameObject_t1756533147 * ___controller0, bool ___controllerState1, bool* ___subscribedState2, bool* ___previousState3, const MethodInfo* method)
{
	{
		bool L_0 = ___controllerState1;
		bool* L_1 = ___previousState3;
		if ((((int32_t)L_0) == ((int32_t)(*((int8_t*)L_1)))))
		{
			goto IL_0013;
		}
	}
	{
		GameObject_t1756533147 * L_2 = ___controller0;
		bool L_3 = ___controllerState1;
		bool* L_4 = ___subscribedState2;
		VRTK_TouchpadMovement_SetControllerListeners_m814129575(__this, L_2, L_3, L_4, (bool)0, /*hidden argument*/NULL);
	}

IL_0013:
	{
		bool* L_5 = ___previousState3;
		bool L_6 = ___controllerState1;
		*((int8_t*)(L_5)) = (int8_t)L_6;
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::DoTouchpadAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern Il2CppClass* VRTK_ControllerEvents_t3225224819_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadMovement_DoTouchpadAxisChanged_m314873841_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_DoTouchpadAxisChanged_m314873841 (VRTK_TouchpadMovement_t1979813355 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_DoTouchpadAxisChanged_m314873841_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Il2CppObject * L_0 = ___sender0;
		__this->set_controllerEvents_44(((VRTK_ControllerEvents_t3225224819 *)CastclassClass(L_0, VRTK_ControllerEvents_t3225224819_il2cpp_TypeInfo_var)));
		int32_t L_1 = __this->get_moveOnButtonPress_5();
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_2 = __this->get_controllerEvents_44();
		int32_t L_3 = __this->get_moveOnButtonPress_5();
		NullCheck(L_2);
		bool L_4 = VRTK_ControllerEvents_IsButtonPressed_m407246062(L_2, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0040;
		}
	}
	{
		Vector2_t2243707579  L_5 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_touchAxis_30(L_5);
		__this->set_controllerEvents_44((VRTK_ControllerEvents_t3225224819 *)NULL);
		return;
	}

IL_0040:
	{
		Vector2_t2243707579  L_6 = (&___e1)->get_touchpadAxis_2();
		V_0 = L_6;
		Vector2_t2243707579  L_7 = Vector2_get_normalized_m2985402409((&V_0), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = Vector2_get_magnitude_m33802565((&V_0), /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = (&V_0)->get_y_1();
		float L_10 = __this->get_verticalDeadzone_8();
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_008e;
		}
	}
	{
		float L_11 = (&V_0)->get_y_1();
		float L_12 = __this->get_verticalDeadzone_8();
		if ((!(((float)L_11) > ((float)((-L_12))))))
		{
			goto IL_008e;
		}
	}
	{
		(&V_0)->set_y_1((0.0f));
		goto IL_00b8;
	}

IL_008e:
	{
		Vector2_t2243707579  L_13 = V_1;
		float L_14 = V_2;
		float L_15 = __this->get_verticalDeadzone_8();
		float L_16 = __this->get_verticalDeadzone_8();
		Vector2_t2243707579  L_17 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_13, ((float)((float)((float)((float)L_14-(float)L_15))/(float)((float)((float)(1.0f)-(float)L_16)))), /*hidden argument*/NULL);
		V_3 = L_17;
		float L_18 = (&V_3)->get_y_1();
		(&V_0)->set_y_1(L_18);
	}

IL_00b8:
	{
		float L_19 = (&V_0)->get_x_0();
		float L_20 = __this->get_horizontalDeadzone_16();
		if ((!(((float)L_19) < ((float)L_20))))
		{
			goto IL_00ee;
		}
	}
	{
		float L_21 = (&V_0)->get_x_0();
		float L_22 = __this->get_horizontalDeadzone_16();
		if ((!(((float)L_21) > ((float)((-L_22))))))
		{
			goto IL_00ee;
		}
	}
	{
		(&V_0)->set_x_0((0.0f));
		goto IL_0119;
	}

IL_00ee:
	{
		Vector2_t2243707579  L_23 = V_1;
		float L_24 = V_2;
		float L_25 = __this->get_horizontalDeadzone_16();
		float L_26 = __this->get_horizontalDeadzone_16();
		Vector2_t2243707579  L_27 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_23, ((float)((float)((float)((float)L_24-(float)L_25))/(float)((float)((float)(1.0f)-(float)L_26)))), /*hidden argument*/NULL);
		V_4 = L_27;
		float L_28 = (&V_4)->get_x_0();
		(&V_0)->set_x_0(L_28);
	}

IL_0119:
	{
		Vector2_t2243707579  L_29 = V_0;
		__this->set_touchAxis_30(L_29);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::DoTouchpadTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_TouchpadMovement_DoTouchpadTouchEnd_m3376986714 (VRTK_TouchpadMovement_t1979813355 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_touchAxis_30(L_0);
		__this->set_controllerEvents_44((VRTK_ControllerEvents_t3225224819 *)NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::OnAxisMovement(VRTK.VRTK_TouchpadMovement/AxisMovementType,VRTK.VRTK_TouchpadMovement/AxisMovementDirection)
extern Il2CppClass* TouchpadMovementAxisEventArgs_t1237298133_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadMovement_OnAxisMovement_m3391609629_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_OnAxisMovement_m3391609629 (VRTK_TouchpadMovement_t1979813355 * __this, int32_t ___givenMovementType0, int32_t ___givenDirection1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_OnAxisMovement_m3391609629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TouchpadMovementAxisEventArgs_t1237298133  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		TouchpadMovementAxisEventHandler_t2208405138 * L_0 = __this->get_AxisMovement_2();
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		Initobj (TouchpadMovementAxisEventArgs_t1237298133_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_1 = ___givenMovementType0;
		(&V_0)->set_movementType_0(L_1);
		int32_t L_2 = ___givenDirection1;
		(&V_0)->set_direction_1(L_2);
		TouchpadMovementAxisEventHandler_t2208405138 * L_3 = __this->get_AxisMovement_2();
		TouchpadMovementAxisEventArgs_t1237298133  L_4 = V_0;
		NullCheck(L_3);
		TouchpadMovementAxisEventHandler_Invoke_m414806234(L_3, __this, L_4, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::CalculateSpeed(System.Boolean,System.Single&,System.Single)
extern "C"  void VRTK_TouchpadMovement_CalculateSpeed_m746457475 (VRTK_TouchpadMovement_t1979813355 * __this, bool ___horizontal0, float* ___speed1, float ___inputValue2, const MethodInfo* method)
{
	float* G_B6_0 = NULL;
	float* G_B2_0 = NULL;
	float G_B4_0 = 0.0f;
	float* G_B4_1 = NULL;
	float G_B3_0 = 0.0f;
	float* G_B3_1 = NULL;
	float G_B5_0 = 0.0f;
	float G_B5_1 = 0.0f;
	float* G_B5_2 = NULL;
	float G_B7_0 = 0.0f;
	float* G_B7_1 = NULL;
	{
		float L_0 = ___inputValue2;
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0048;
		}
	}
	{
		float* L_1 = ___speed1;
		float L_2 = __this->get_slideMaxSpeed_22();
		float L_3 = ___inputValue2;
		*((float*)(L_1)) = (float)((float)((float)L_2*(float)L_3));
		float* L_4 = ___speed1;
		bool L_5 = __this->get_multiplyMovement_37();
		G_B2_0 = L_4;
		if (!L_5)
		{
			G_B6_0 = L_4;
			goto IL_0040;
		}
	}
	{
		float* L_6 = ___speed1;
		bool L_7 = ___horizontal0;
		G_B3_0 = (*((float*)L_6));
		G_B3_1 = G_B2_0;
		if (!L_7)
		{
			G_B4_0 = (*((float*)L_6));
			G_B4_1 = G_B2_0;
			goto IL_0034;
		}
	}
	{
		float L_8 = __this->get_horizontalMultiplier_17();
		G_B5_0 = L_8;
		G_B5_1 = G_B3_0;
		G_B5_2 = G_B3_1;
		goto IL_003a;
	}

IL_0034:
	{
		float L_9 = __this->get_verticalMultiplier_9();
		G_B5_0 = L_9;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
	}

IL_003a:
	{
		G_B7_0 = ((float)((float)G_B5_1*(float)G_B5_0));
		G_B7_1 = G_B5_2;
		goto IL_0042;
	}

IL_0040:
	{
		float* L_10 = ___speed1;
		G_B7_0 = (*((float*)L_10));
		G_B7_1 = G_B6_0;
	}

IL_0042:
	{
		*((float*)(G_B7_1)) = (float)G_B7_0;
		goto IL_004f;
	}

IL_0048:
	{
		float* L_11 = ___speed1;
		VRTK_TouchpadMovement_Decelerate_m2395966906(__this, L_11, /*hidden argument*/NULL);
	}

IL_004f:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::Decelerate(System.Single&)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadMovement_Decelerate_m2395966906_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_Decelerate_m2395966906 (VRTK_TouchpadMovement_t1979813355 * __this, float* ___speed0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_Decelerate_m2395966906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float* L_0 = ___speed0;
		if ((!(((float)(*((float*)L_0))) > ((float)(0.0f)))))
		{
			goto IL_002c;
		}
	}
	{
		float* L_1 = ___speed0;
		float* L_2 = ___speed0;
		float L_3 = __this->get_slideDeceleration_23();
		float L_4 = __this->get_slideMaxSpeed_22();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_5 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_3, L_4, (0.0f), /*hidden argument*/NULL);
		*((float*)(L_1)) = (float)((float)((float)(*((float*)L_2))-(float)L_5));
		goto IL_0060;
	}

IL_002c:
	{
		float* L_6 = ___speed0;
		if ((!(((float)(*((float*)L_6))) < ((float)(0.0f)))))
		{
			goto IL_0059;
		}
	}
	{
		float* L_7 = ___speed0;
		float* L_8 = ___speed0;
		float L_9 = __this->get_slideDeceleration_23();
		float L_10 = __this->get_slideMaxSpeed_22();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_9, ((-L_10)), (0.0f), /*hidden argument*/NULL);
		*((float*)(L_7)) = (float)((float)((float)(*((float*)L_8))+(float)L_11));
		goto IL_0060;
	}

IL_0059:
	{
		float* L_12 = ___speed0;
		*((float*)(L_12)) = (float)(0.0f);
	}

IL_0060:
	{
		float* L_13 = ___speed0;
		float L_14 = __this->get_verticalDeadzone_8();
		if ((!(((float)(*((float*)L_13))) < ((float)L_14))))
		{
			goto IL_0082;
		}
	}
	{
		float* L_15 = ___speed0;
		float L_16 = __this->get_verticalDeadzone_8();
		if ((!(((float)(*((float*)L_15))) > ((float)((-L_16))))))
		{
			goto IL_0082;
		}
	}
	{
		float* L_17 = ___speed0;
		*((float*)(L_17)) = (float)(0.0f);
	}

IL_0082:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::Move()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadMovement_Move_m3310755870_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_Move_m3310755870 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_Move_m3310755870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		int32_t L_0 = __this->get_deviceForDirection_10();
		Transform_t3275118058 * L_1 = VRTK_DeviceFinder_DeviceTransform_m3095571891(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Transform_t3275118058 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00ba;
		}
	}
	{
		Transform_t3275118058 * L_4 = V_0;
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_forward_m1833488937(L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_movementSpeed_31();
		Vector3_t2243707580  L_7 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		float L_8 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Transform_t3275118058 * L_10 = V_0;
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_right_m440863970(L_10, /*hidden argument*/NULL);
		float L_12 = __this->get_strafeSpeed_32();
		Vector3_t2243707580  L_13 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		float L_14 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		Transform_t3275118058 * L_16 = __this->get_playArea_29();
		NullCheck(L_16);
		Vector3_t2243707580  L_17 = Transform_get_position_m1104419803(L_16, /*hidden argument*/NULL);
		V_4 = L_17;
		float L_18 = (&V_4)->get_y_2();
		V_3 = L_18;
		Transform_t3275118058 * L_19 = __this->get_playArea_29();
		Transform_t3275118058 * L_20 = L_19;
		NullCheck(L_20);
		Vector3_t2243707580  L_21 = Transform_get_position_m1104419803(L_20, /*hidden argument*/NULL);
		Vector3_t2243707580  L_22 = V_1;
		Vector3_t2243707580  L_23 = V_2;
		Vector3_t2243707580  L_24 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_21, L_24, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_position_m2469242620(L_20, L_25, /*hidden argument*/NULL);
		Transform_t3275118058 * L_26 = __this->get_playArea_29();
		Transform_t3275118058 * L_27 = __this->get_playArea_29();
		NullCheck(L_27);
		Vector3_t2243707580  L_28 = Transform_get_position_m1104419803(L_27, /*hidden argument*/NULL);
		V_5 = L_28;
		float L_29 = (&V_5)->get_x_1();
		float L_30 = V_3;
		Transform_t3275118058 * L_31 = __this->get_playArea_29();
		NullCheck(L_31);
		Vector3_t2243707580  L_32 = Transform_get_position_m1104419803(L_31, /*hidden argument*/NULL);
		V_6 = L_32;
		float L_33 = (&V_6)->get_z_3();
		Vector3_t2243707580  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector3__ctor_m2638739322(&L_34, L_29, L_30, L_33, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_position_m2469242620(L_26, L_34, /*hidden argument*/NULL);
	}

IL_00ba:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::Warp(System.Boolean,System.Boolean)
extern Il2CppClass* VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1216450955;
extern const uint32_t VRTK_TouchpadMovement_Warp_m2450696817_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_Warp_m2450696817 (VRTK_TouchpadMovement_t1979813355 * __this, bool ___blink0, bool ___horizontal1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_Warp_m2450696817_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Transform_t3275118058 * V_1 = NULL;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	RaycastHit_t87180320  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float G_B5_0 = 0.0f;
	float G_B1_0 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B2_0 = 0.0f;
	float G_B4_0 = 0.0f;
	float G_B4_1 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B6_1 = 0.0f;
	Vector3_t2243707580  G_B8_0;
	memset(&G_B8_0, 0, sizeof(G_B8_0));
	Vector3_t2243707580  G_B7_0;
	memset(&G_B7_0, 0, sizeof(G_B7_0));
	Vector3_t2243707580  G_B9_0;
	memset(&G_B9_0, 0, sizeof(G_B9_0));
	Vector3_t2243707580  G_B9_1;
	memset(&G_B9_1, 0, sizeof(G_B9_1));
	Vector3_t2243707580  G_B11_0;
	memset(&G_B11_0, 0, sizeof(G_B11_0));
	Vector3_t2243707580  G_B11_1;
	memset(&G_B11_1, 0, sizeof(G_B11_1));
	Vector3_t2243707580  G_B10_0;
	memset(&G_B10_0, 0, sizeof(G_B10_0));
	Vector3_t2243707580  G_B10_1;
	memset(&G_B10_1, 0, sizeof(G_B10_1));
	float G_B12_0 = 0.0f;
	Vector3_t2243707580  G_B12_1;
	memset(&G_B12_1, 0, sizeof(G_B12_1));
	Vector3_t2243707580  G_B12_2;
	memset(&G_B12_2, 0, sizeof(G_B12_2));
	Vector3_t2243707580  G_B14_0;
	memset(&G_B14_0, 0, sizeof(G_B14_0));
	Vector3_t2243707580  G_B14_1;
	memset(&G_B14_1, 0, sizeof(G_B14_1));
	Vector3_t2243707580  G_B13_0;
	memset(&G_B13_0, 0, sizeof(G_B13_0));
	Vector3_t2243707580  G_B13_1;
	memset(&G_B13_1, 0, sizeof(G_B13_1));
	int32_t G_B15_0 = 0;
	Vector3_t2243707580  G_B15_1;
	memset(&G_B15_1, 0, sizeof(G_B15_1));
	Vector3_t2243707580  G_B15_2;
	memset(&G_B15_2, 0, sizeof(G_B15_2));
	Vector3_t2243707580  G_B19_0;
	memset(&G_B19_0, 0, sizeof(G_B19_0));
	Vector3_t2243707580  G_B23_0;
	memset(&G_B23_0, 0, sizeof(G_B23_0));
	int32_t G_B33_0 = 0;
	VRTK_TouchpadMovement_t1979813355 * G_B33_1 = NULL;
	int32_t G_B29_0 = 0;
	VRTK_TouchpadMovement_t1979813355 * G_B29_1 = NULL;
	int32_t G_B31_0 = 0;
	VRTK_TouchpadMovement_t1979813355 * G_B31_1 = NULL;
	int32_t G_B30_0 = 0;
	VRTK_TouchpadMovement_t1979813355 * G_B30_1 = NULL;
	int32_t G_B32_0 = 0;
	int32_t G_B32_1 = 0;
	VRTK_TouchpadMovement_t1979813355 * G_B32_2 = NULL;
	int32_t G_B36_0 = 0;
	int32_t G_B36_1 = 0;
	VRTK_TouchpadMovement_t1979813355 * G_B36_2 = NULL;
	int32_t G_B35_0 = 0;
	VRTK_TouchpadMovement_t1979813355 * G_B35_1 = NULL;
	int32_t G_B34_0 = 0;
	VRTK_TouchpadMovement_t1979813355 * G_B34_1 = NULL;
	{
		float L_0 = __this->get_warpRange_25();
		bool L_1 = __this->get_multiplyMovement_37();
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B5_0 = L_0;
			goto IL_002d;
		}
	}
	{
		bool L_2 = ___horizontal1;
		G_B2_0 = G_B1_0;
		if (!L_2)
		{
			G_B3_0 = G_B1_0;
			goto IL_0022;
		}
	}
	{
		float L_3 = __this->get_horizontalMultiplier_17();
		G_B4_0 = L_3;
		G_B4_1 = G_B2_0;
		goto IL_0028;
	}

IL_0022:
	{
		float L_4 = __this->get_verticalMultiplier_9();
		G_B4_0 = L_4;
		G_B4_1 = G_B3_0;
	}

IL_0028:
	{
		G_B6_0 = G_B4_0;
		G_B6_1 = G_B4_1;
		goto IL_0032;
	}

IL_002d:
	{
		G_B6_0 = (1.0f);
		G_B6_1 = G_B5_0;
	}

IL_0032:
	{
		V_0 = ((float)((float)G_B6_1*(float)G_B6_0));
		int32_t L_5 = __this->get_deviceForDirection_10();
		Transform_t3275118058 * L_6 = VRTK_DeviceFinder_DeviceTransform_m3095571891(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Transform_t3275118058 * L_7 = __this->get_playArea_29();
		CapsuleCollider_t720607407 * L_8 = __this->get_bodyCollider_38();
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = CapsuleCollider_get_center_m189177564(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t2243707580  L_10 = Transform_TransformPoint_m3272254198(L_7, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		Vector3_t2243707580  L_11 = V_2;
		bool L_12 = ___horizontal1;
		G_B7_0 = L_11;
		if (!L_12)
		{
			G_B8_0 = L_11;
			goto IL_0069;
		}
	}
	{
		Transform_t3275118058 * L_13 = V_1;
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = Transform_get_right_m440863970(L_13, /*hidden argument*/NULL);
		G_B9_0 = L_14;
		G_B9_1 = G_B7_0;
		goto IL_006f;
	}

IL_0069:
	{
		Transform_t3275118058 * L_15 = V_1;
		NullCheck(L_15);
		Vector3_t2243707580  L_16 = Transform_get_forward_m1833488937(L_15, /*hidden argument*/NULL);
		G_B9_0 = L_16;
		G_B9_1 = G_B8_0;
	}

IL_006f:
	{
		float L_17 = V_0;
		Vector3_t2243707580  L_18 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, G_B9_0, L_17, /*hidden argument*/NULL);
		bool L_19 = ___horizontal1;
		G_B10_0 = L_18;
		G_B10_1 = G_B9_1;
		if (!L_19)
		{
			G_B11_0 = L_18;
			G_B11_1 = G_B9_1;
			goto IL_008b;
		}
	}
	{
		Vector2_t2243707579 * L_20 = __this->get_address_of_touchAxis_30();
		float L_21 = L_20->get_x_0();
		G_B12_0 = L_21;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		goto IL_0096;
	}

IL_008b:
	{
		Vector2_t2243707579 * L_22 = __this->get_address_of_touchAxis_30();
		float L_23 = L_22->get_y_1();
		G_B12_0 = L_23;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
	}

IL_0096:
	{
		G_B13_0 = G_B12_1;
		G_B13_1 = G_B12_2;
		if ((!(((float)G_B12_0) < ((float)(0.0f)))))
		{
			G_B14_0 = G_B12_1;
			G_B14_1 = G_B12_2;
			goto IL_00a6;
		}
	}
	{
		G_B15_0 = (-1);
		G_B15_1 = G_B13_0;
		G_B15_2 = G_B13_1;
		goto IL_00a7;
	}

IL_00a6:
	{
		G_B15_0 = 1;
		G_B15_1 = G_B14_0;
		G_B15_2 = G_B14_1;
	}

IL_00a7:
	{
		Vector3_t2243707580  L_24 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, G_B15_1, (((float)((float)G_B15_0))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, G_B15_2, L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		V_4 = (0.2f);
		bool L_26 = ___horizontal1;
		if (!L_26)
		{
			goto IL_00ff;
		}
	}
	{
		Vector2_t2243707579 * L_27 = __this->get_address_of_touchAxis_30();
		float L_28 = L_27->get_x_0();
		if ((!(((float)L_28) < ((float)(0.0f)))))
		{
			goto IL_00e5;
		}
	}
	{
		Transform_t3275118058 * L_29 = __this->get_headset_39();
		NullCheck(L_29);
		Vector3_t2243707580  L_30 = Transform_get_right_m440863970(L_29, /*hidden argument*/NULL);
		G_B19_0 = L_30;
		goto IL_00fa;
	}

IL_00e5:
	{
		Transform_t3275118058 * L_31 = __this->get_headset_39();
		NullCheck(L_31);
		Vector3_t2243707580  L_32 = Transform_get_right_m440863970(L_31, /*hidden argument*/NULL);
		Vector3_t2243707580  L_33 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_32, (-1.0f), /*hidden argument*/NULL);
		G_B19_0 = L_33;
	}

IL_00fa:
	{
		G_B23_0 = G_B19_0;
		goto IL_0139;
	}

IL_00ff:
	{
		Vector2_t2243707579 * L_34 = __this->get_address_of_touchAxis_30();
		float L_35 = L_34->get_y_1();
		if ((!(((float)L_35) > ((float)(0.0f)))))
		{
			goto IL_0124;
		}
	}
	{
		Transform_t3275118058 * L_36 = __this->get_headset_39();
		NullCheck(L_36);
		Vector3_t2243707580  L_37 = Transform_get_forward_m1833488937(L_36, /*hidden argument*/NULL);
		G_B23_0 = L_37;
		goto IL_0139;
	}

IL_0124:
	{
		Transform_t3275118058 * L_38 = __this->get_headset_39();
		NullCheck(L_38);
		Vector3_t2243707580  L_39 = Transform_get_forward_m1833488937(L_38, /*hidden argument*/NULL);
		Vector3_t2243707580  L_40 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_39, (-1.0f), /*hidden argument*/NULL);
		G_B23_0 = L_40;
	}

IL_0139:
	{
		V_6 = G_B23_0;
		Transform_t3275118058 * L_41 = __this->get_headset_39();
		NullCheck(L_41);
		Vector3_t2243707580  L_42 = Transform_get_position_m1104419803(L_41, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_44 = V_4;
		Vector3_t2243707580  L_45 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		Vector3_t2243707580  L_46 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_42, L_45, /*hidden argument*/NULL);
		Vector3_t2243707580  L_47 = V_6;
		float L_48 = V_0;
		bool L_49 = Physics_Raycast_m2994111303(NULL /*static, unused*/, L_46, L_47, (&V_5), L_48, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_0185;
		}
	}
	{
		Vector3_t2243707580  L_50 = RaycastHit_get_point_m326143462((&V_5), /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_6;
		CapsuleCollider_t720607407 * L_52 = __this->get_bodyCollider_38();
		NullCheck(L_52);
		float L_53 = CapsuleCollider_get_radius_m842698081(L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_51, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_55 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_50, L_54, /*hidden argument*/NULL);
		V_3 = L_55;
	}

IL_0185:
	{
		Vector3_t2243707580  L_56 = V_3;
		Vector3_t2243707580  L_57 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_58 = __this->get_warpMaxAltitudeChange_26();
		float L_59 = V_4;
		Vector3_t2243707580  L_60 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_57, ((float)((float)L_58+(float)L_59)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_61 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_56, L_60, /*hidden argument*/NULL);
		Vector3_t2243707580  L_62 = Vector3_get_down_m2372302126(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_63 = __this->get_warpMaxAltitudeChange_26();
		float L_64 = V_4;
		bool L_65 = Physics_Raycast_m2994111303(NULL /*static, unused*/, L_61, L_62, (&V_5), ((float)((float)((float)((float)L_63+(float)L_64))*(float)(2.0f))), /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_029e;
		}
	}
	{
		Vector3_t2243707580  L_66 = RaycastHit_get_point_m326143462((&V_5), /*hidden argument*/NULL);
		V_7 = L_66;
		float L_67 = (&V_7)->get_y_2();
		CapsuleCollider_t720607407 * L_68 = __this->get_bodyCollider_38();
		NullCheck(L_68);
		float L_69 = CapsuleCollider_get_height_m2987501502(L_68, /*hidden argument*/NULL);
		(&V_3)->set_y_2(((float)((float)L_67+(float)((float)((float)L_69/(float)(2.0f))))));
		float L_70 = Time_get_timeSinceLevelLoad_m1980066582(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_71 = __this->get_warpDelay_24();
		__this->set_lastWarp_34(((float)((float)L_70+(float)L_71)));
		Transform_t3275118058 * L_72 = __this->get_playArea_29();
		Vector3_t2243707580  L_73 = V_3;
		Vector3_t2243707580  L_74 = V_2;
		Vector3_t2243707580  L_75 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_73, L_74, /*hidden argument*/NULL);
		Transform_t3275118058 * L_76 = __this->get_playArea_29();
		NullCheck(L_76);
		Vector3_t2243707580  L_77 = Transform_get_position_m1104419803(L_76, /*hidden argument*/NULL);
		Vector3_t2243707580  L_78 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_75, L_77, /*hidden argument*/NULL);
		NullCheck(L_72);
		Transform_set_position_m2469242620(L_72, L_78, /*hidden argument*/NULL);
		bool L_79 = ___blink0;
		if (!L_79)
		{
			goto IL_0254;
		}
	}
	{
		float L_80 = __this->get_warpDelay_24();
		float L_81 = __this->get_blinkDurationMultiplier_21();
		__this->set_blinkFadeInTime_33(((float)((float)L_80*(float)L_81)));
		Color_t2020392075  L_82 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var);
		VRTK_SDK_Bridge_HeadsetFade_m28453102(NULL /*static, unused*/, L_82, (0.0f), (bool)0, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral1216450955, (0.01f), /*hidden argument*/NULL);
	}

IL_0254:
	{
		bool L_83 = ___horizontal1;
		G_B29_0 = 0;
		G_B29_1 = __this;
		if (!L_83)
		{
			G_B33_0 = 0;
			G_B33_1 = __this;
			goto IL_027d;
		}
	}
	{
		Vector2_t2243707579 * L_84 = __this->get_address_of_touchAxis_30();
		float L_85 = L_84->get_x_0();
		G_B30_0 = G_B29_0;
		G_B30_1 = G_B29_1;
		if ((!(((float)L_85) < ((float)(0.0f)))))
		{
			G_B31_0 = G_B29_0;
			G_B31_1 = G_B29_1;
			goto IL_0277;
		}
	}
	{
		G_B32_0 = 1;
		G_B32_1 = G_B30_0;
		G_B32_2 = G_B30_1;
		goto IL_0278;
	}

IL_0277:
	{
		G_B32_0 = 2;
		G_B32_1 = G_B31_0;
		G_B32_2 = G_B31_1;
	}

IL_0278:
	{
		G_B36_0 = G_B32_0;
		G_B36_1 = G_B32_1;
		G_B36_2 = G_B32_2;
		goto IL_0299;
	}

IL_027d:
	{
		Vector2_t2243707579 * L_86 = __this->get_address_of_touchAxis_30();
		float L_87 = L_86->get_y_1();
		G_B34_0 = G_B33_0;
		G_B34_1 = G_B33_1;
		if ((!(((float)L_87) < ((float)(0.0f)))))
		{
			G_B35_0 = G_B33_0;
			G_B35_1 = G_B33_1;
			goto IL_0298;
		}
	}
	{
		G_B36_0 = 4;
		G_B36_1 = G_B34_0;
		G_B36_2 = G_B34_1;
		goto IL_0299;
	}

IL_0298:
	{
		G_B36_0 = 3;
		G_B36_1 = G_B35_0;
		G_B36_2 = G_B35_1;
	}

IL_0299:
	{
		NullCheck(G_B36_2);
		VRTK_TouchpadMovement_OnAxisMovement_m3391609629(G_B36_2, G_B36_1, G_B36_0, /*hidden argument*/NULL);
	}

IL_029e:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::RotateAroundPlayer(System.Single)
extern "C"  void VRTK_TouchpadMovement_RotateAroundPlayer_m2493629917 (VRTK_TouchpadMovement_t1979813355 * __this, float ___angle0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3275118058 * L_0 = __this->get_playArea_29();
		CapsuleCollider_t720607407 * L_1 = __this->get_bodyCollider_38();
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = CapsuleCollider_get_center_m189177564(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_3 = Transform_TransformPoint_m3272254198(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Transform_t3275118058 * L_4 = __this->get_playArea_29();
		Vector3_t2243707580  L_5 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ___angle0;
		NullCheck(L_4);
		Transform_Rotate_m882843932(L_4, L_5, L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = V_0;
		Transform_t3275118058 * L_8 = __this->get_playArea_29();
		CapsuleCollider_t720607407 * L_9 = __this->get_bodyCollider_38();
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = CapsuleCollider_get_center_m189177564(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t2243707580  L_11 = Transform_TransformPoint_m3272254198(L_8, L_10, /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_7, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		Transform_t3275118058 * L_13 = __this->get_playArea_29();
		Transform_t3275118058 * L_14 = L_13;
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = Transform_get_position_m1104419803(L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = V_0;
		Vector3_t2243707580  L_17 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_position_m2469242620(L_14, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::Rotate()
extern "C"  void VRTK_TouchpadMovement_Rotate_m4105360080 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B2_0 = 0.0f;
	float G_B1_0 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	{
		Vector2_t2243707579 * L_0 = __this->get_address_of_touchAxis_30();
		float L_1 = L_0->get_x_0();
		float L_2 = __this->get_rotateMaxSpeed_20();
		float L_3 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_4 = __this->get_multiplyMovement_37();
		G_B1_0 = ((float)((float)((float)((float)L_1*(float)L_2))*(float)L_3));
		if (!L_4)
		{
			G_B2_0 = ((float)((float)((float)((float)L_1*(float)L_2))*(float)L_3));
			goto IL_002e;
		}
	}
	{
		float L_5 = __this->get_horizontalMultiplier_17();
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		goto IL_0033;
	}

IL_002e:
	{
		G_B3_0 = (1.0f);
		G_B3_1 = G_B2_0;
	}

IL_0033:
	{
		V_0 = ((float)((float)((float)((float)G_B3_1*(float)G_B3_0))*(float)(10.0f)));
		float L_6 = V_0;
		VRTK_TouchpadMovement_RotateAroundPlayer_m2493629917(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::SnapRotate(System.Boolean,System.Boolean)
extern Il2CppClass* VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1216450955;
extern const uint32_t VRTK_TouchpadMovement_SnapRotate_m1907291896_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_SnapRotate_m1907291896 (VRTK_TouchpadMovement_t1979813355 * __this, bool ___blink0, bool ___flipDirection1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_SnapRotate_m1907291896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B4_0 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B5_0 = 0.0f;
	float G_B5_1 = 0.0f;
	float G_B7_0 = 0.0f;
	float G_B6_0 = 0.0f;
	int32_t G_B8_0 = 0;
	float G_B8_1 = 0.0f;
	VRTK_TouchpadMovement_t1979813355 * G_B13_0 = NULL;
	VRTK_TouchpadMovement_t1979813355 * G_B12_0 = NULL;
	int32_t G_B14_0 = 0;
	VRTK_TouchpadMovement_t1979813355 * G_B14_1 = NULL;
	int32_t G_B16_0 = 0;
	VRTK_TouchpadMovement_t1979813355 * G_B16_1 = NULL;
	int32_t G_B15_0 = 0;
	VRTK_TouchpadMovement_t1979813355 * G_B15_1 = NULL;
	int32_t G_B17_0 = 0;
	int32_t G_B17_1 = 0;
	VRTK_TouchpadMovement_t1979813355 * G_B17_2 = NULL;
	{
		float L_0 = Time_get_timeSinceLevelLoad_m1980066582(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_snapRotateDelay_18();
		__this->set_lastSnapRotate_36(((float)((float)L_0+(float)L_1)));
		bool L_2 = ___flipDirection1;
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		G_B9_0 = (180.0f);
		goto IL_0062;
	}

IL_0022:
	{
		float L_3 = __this->get_snapRotateAngle_19();
		bool L_4 = __this->get_multiplyMovement_37();
		G_B3_0 = L_3;
		if (!L_4)
		{
			G_B4_0 = L_3;
			goto IL_003e;
		}
	}
	{
		float L_5 = __this->get_horizontalMultiplier_17();
		G_B5_0 = L_5;
		G_B5_1 = G_B3_0;
		goto IL_0043;
	}

IL_003e:
	{
		G_B5_0 = (1.0f);
		G_B5_1 = G_B4_0;
	}

IL_0043:
	{
		Vector2_t2243707579 * L_6 = __this->get_address_of_touchAxis_30();
		float L_7 = L_6->get_x_0();
		G_B6_0 = ((float)((float)G_B5_1*(float)G_B5_0));
		if ((!(((float)L_7) < ((float)(0.0f)))))
		{
			G_B7_0 = ((float)((float)G_B5_1*(float)G_B5_0));
			goto IL_005f;
		}
	}
	{
		G_B8_0 = (-1);
		G_B8_1 = G_B6_0;
		goto IL_0060;
	}

IL_005f:
	{
		G_B8_0 = 1;
		G_B8_1 = G_B7_0;
	}

IL_0060:
	{
		G_B9_0 = ((float)((float)G_B8_1*(float)(((float)((float)G_B8_0)))));
	}

IL_0062:
	{
		V_0 = G_B9_0;
		float L_8 = V_0;
		VRTK_TouchpadMovement_RotateAroundPlayer_m2493629917(__this, L_8, /*hidden argument*/NULL);
		bool L_9 = ___blink0;
		if (!L_9)
		{
			goto IL_00a3;
		}
	}
	{
		float L_10 = __this->get_snapRotateDelay_18();
		float L_11 = __this->get_blinkDurationMultiplier_21();
		__this->set_blinkFadeInTime_33(((float)((float)L_10*(float)L_11)));
		Color_t2020392075  L_12 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var);
		VRTK_SDK_Bridge_HeadsetFade_m28453102(NULL /*static, unused*/, L_12, (0.0f), (bool)0, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral1216450955, (0.01f), /*hidden argument*/NULL);
	}

IL_00a3:
	{
		bool L_13 = ___flipDirection1;
		G_B12_0 = __this;
		if (!L_13)
		{
			G_B13_0 = __this;
			goto IL_00b0;
		}
	}
	{
		G_B14_0 = 1;
		G_B14_1 = G_B12_0;
		goto IL_00b1;
	}

IL_00b0:
	{
		G_B14_0 = 2;
		G_B14_1 = G_B13_0;
	}

IL_00b1:
	{
		Vector2_t2243707579 * L_14 = __this->get_address_of_touchAxis_30();
		float L_15 = L_14->get_x_0();
		G_B15_0 = G_B14_0;
		G_B15_1 = G_B14_1;
		if ((!(((float)L_15) < ((float)(0.0f)))))
		{
			G_B16_0 = G_B14_0;
			G_B16_1 = G_B14_1;
			goto IL_00cc;
		}
	}
	{
		G_B17_0 = 1;
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		goto IL_00cd;
	}

IL_00cc:
	{
		G_B17_0 = 2;
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
	}

IL_00cd:
	{
		NullCheck(G_B17_2);
		VRTK_TouchpadMovement_OnAxisMovement_m3391609629(G_B17_2, G_B17_1, G_B17_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::ReleaseBlink()
extern Il2CppClass* VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadMovement_ReleaseBlink_m3016794946_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_ReleaseBlink_m3016794946 (VRTK_TouchpadMovement_t1979813355 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_ReleaseBlink_m3016794946_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2020392075  L_0 = Color_get_clear_m1469108305(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_blinkFadeInTime_33();
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var);
		VRTK_SDK_Bridge_HeadsetFade_m28453102(NULL /*static, unused*/, L_0, L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::SetControllerListeners(UnityEngine.GameObject,System.Boolean,System.Boolean&,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadMovement_SetControllerListeners_m814129575_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_SetControllerListeners_m814129575 (VRTK_TouchpadMovement_t1979813355 * __this, GameObject_t1756533147 * ___controller0, bool ___controllerState1, bool* ___subscribedState2, bool ___forceDisabled3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_SetControllerListeners_m814129575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B4_0 = 0;
	{
		GameObject_t1756533147 * L_0 = ___controller0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		bool L_2 = ___forceDisabled3;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0019;
	}

IL_0018:
	{
		bool L_3 = ___controllerState1;
		G_B4_0 = ((int32_t)(L_3));
	}

IL_0019:
	{
		V_0 = (bool)G_B4_0;
		GameObject_t1756533147 * L_4 = ___controller0;
		bool L_5 = V_0;
		bool* L_6 = ___subscribedState2;
		VRTK_TouchpadMovement_ToggleControllerListeners_m1383735902(__this, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadMovement::ToggleControllerListeners(UnityEngine.GameObject,System.Boolean,System.Boolean&)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisVRTK_ControllerEvents_t3225224819_m95931981_MethodInfo_var;
extern const uint32_t VRTK_TouchpadMovement_ToggleControllerListeners_m1383735902_MetadataUsageId;
extern "C"  void VRTK_TouchpadMovement_ToggleControllerListeners_m1383735902 (VRTK_TouchpadMovement_t1979813355 * __this, GameObject_t1756533147 * ___controller0, bool ___toggle1, bool* ___subscribed2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadMovement_ToggleControllerListeners_m1383735902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_ControllerEvents_t3225224819 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___controller0;
		NullCheck(L_0);
		VRTK_ControllerEvents_t3225224819 * L_1 = GameObject_GetComponent_TisVRTK_ControllerEvents_t3225224819_m95931981(L_0, /*hidden argument*/GameObject_GetComponent_TisVRTK_ControllerEvents_t3225224819_m95931981_MethodInfo_var);
		V_0 = L_1;
		VRTK_ControllerEvents_t3225224819 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		bool L_4 = ___toggle1;
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		bool* L_5 = ___subscribed2;
		if ((*((int8_t*)L_5)))
		{
			goto IL_003f;
		}
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_6 = V_0;
		ControllerInteractionEventHandler_t343979916 * L_7 = __this->get_touchpadAxisChanged_42();
		NullCheck(L_6);
		VRTK_ControllerEvents_add_TouchpadAxisChanged_m3280812681(L_6, L_7, /*hidden argument*/NULL);
		VRTK_ControllerEvents_t3225224819 * L_8 = V_0;
		ControllerInteractionEventHandler_t343979916 * L_9 = __this->get_touchpadUntouched_43();
		NullCheck(L_8);
		VRTK_ControllerEvents_add_TouchpadTouchEnd_m4159394722(L_8, L_9, /*hidden argument*/NULL);
		bool* L_10 = ___subscribed2;
		*((int8_t*)(L_10)) = (int8_t)1;
		goto IL_007d;
	}

IL_003f:
	{
		VRTK_ControllerEvents_t3225224819 * L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		bool L_13 = ___toggle1;
		if (L_13)
		{
			goto IL_007d;
		}
	}
	{
		bool* L_14 = ___subscribed2;
		if (!(*((int8_t*)L_14)))
		{
			goto IL_007d;
		}
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_15 = V_0;
		ControllerInteractionEventHandler_t343979916 * L_16 = __this->get_touchpadAxisChanged_42();
		NullCheck(L_15);
		VRTK_ControllerEvents_remove_TouchpadAxisChanged_m2513257630(L_15, L_16, /*hidden argument*/NULL);
		VRTK_ControllerEvents_t3225224819 * L_17 = V_0;
		ControllerInteractionEventHandler_t343979916 * L_18 = __this->get_touchpadUntouched_43();
		NullCheck(L_17);
		VRTK_ControllerEvents_remove_TouchpadTouchEnd_m1310523989(L_17, L_18, /*hidden argument*/NULL);
		Vector2_t2243707579  L_19 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_touchAxis_30(L_19);
		bool* L_20 = ___subscribed2;
		*((int8_t*)(L_20)) = (int8_t)0;
	}

IL_007d:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::.ctor()
extern "C"  void VRTK_TouchpadWalking__ctor_m2170475895 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method)
{
	{
		__this->set_leftController_2((bool)1);
		__this->set_rightController_3((bool)1);
		__this->set_maxWalkSpeed_4((3.0f));
		__this->set_deceleration_5((0.1f));
		__this->set_speedMultiplier_9((1.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::Awake()
extern Il2CppClass* ControllerInteractionEventHandler_t343979916_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* VRTK_TouchpadWalking_DoTouchpadAxisChanged_m536676883_MethodInfo_var;
extern const MethodInfo* VRTK_TouchpadWalking_DoTouchpadTouchEnd_m847870042_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1619768869;
extern const uint32_t VRTK_TouchpadWalking_Awake_m1649679240_MetadataUsageId;
extern "C"  void VRTK_TouchpadWalking_Awake_m1649679240 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadWalking_Awake_m1649679240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)VRTK_TouchpadWalking_DoTouchpadAxisChanged_m536676883_MethodInfo_var);
		ControllerInteractionEventHandler_t343979916 * L_1 = (ControllerInteractionEventHandler_t343979916 *)il2cpp_codegen_object_new(ControllerInteractionEventHandler_t343979916_il2cpp_TypeInfo_var);
		ControllerInteractionEventHandler__ctor_m4206169134(L_1, __this, L_0, /*hidden argument*/NULL);
		__this->set_touchpadAxisChanged_18(L_1);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)VRTK_TouchpadWalking_DoTouchpadTouchEnd_m847870042_MethodInfo_var);
		ControllerInteractionEventHandler_t343979916 * L_3 = (ControllerInteractionEventHandler_t343979916 *)il2cpp_codegen_object_new(ControllerInteractionEventHandler_t343979916_il2cpp_TypeInfo_var);
		ControllerInteractionEventHandler__ctor_m4206169134(L_3, __this, L_2, /*hidden argument*/NULL);
		__this->set_touchpadUntouched_19(L_3);
		Transform_t3275118058 * L_4 = VRTK_DeviceFinder_PlayAreaTransform_m1884263923(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_playArea_12(L_4);
		GameObject_t1756533147 * L_5 = VRTK_DeviceFinder_GetControllerLeftHand_m151411604(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_controllerLeftHand_10(L_5);
		GameObject_t1756533147 * L_6 = VRTK_DeviceFinder_GetControllerRightHand_m2938812741(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_controllerRightHand_11(L_6);
		Transform_t3275118058 * L_7 = __this->get_playArea_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0061;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1619768869, /*hidden argument*/NULL);
	}

IL_0061:
	{
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		VRTK_PlayerObject_SetPlayerObject_m1121178556(NULL /*static, unused*/, L_9, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::OnEnable()
extern const MethodInfo* Component_GetComponent_TisVRTK_BodyPhysics_t3414085265_m1861204865_MethodInfo_var;
extern const uint32_t VRTK_TouchpadWalking_OnEnable_m1583626659_MetadataUsageId;
extern "C"  void VRTK_TouchpadWalking_OnEnable_m1583626659 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadWalking_OnEnable_m1583626659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_controllerLeftHand_10();
		bool L_1 = __this->get_leftController_2();
		bool* L_2 = __this->get_address_of_leftSubscribed_16();
		VRTK_TouchpadWalking_SetControllerListeners_m1909848785(__this, L_0, L_1, L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_controllerRightHand_11();
		bool L_4 = __this->get_rightController_3();
		bool* L_5 = __this->get_address_of_rightSubscribed_17();
		VRTK_TouchpadWalking_SetControllerListeners_m1909848785(__this, L_3, L_4, L_5, (bool)0, /*hidden argument*/NULL);
		VRTK_BodyPhysics_t3414085265 * L_6 = Component_GetComponent_TisVRTK_BodyPhysics_t3414085265_m1861204865(__this, /*hidden argument*/Component_GetComponent_TisVRTK_BodyPhysics_t3414085265_m1861204865_MethodInfo_var);
		__this->set_bodyPhysics_22(L_6);
		__this->set_movementSpeed_14((0.0f));
		__this->set_strafeSpeed_15((0.0f));
		__this->set_multiplySpeed_20((bool)0);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::OnDisable()
extern "C"  void VRTK_TouchpadWalking_OnDisable_m2473567300 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_controllerLeftHand_10();
		bool L_1 = __this->get_leftController_2();
		bool* L_2 = __this->get_address_of_leftSubscribed_16();
		VRTK_TouchpadWalking_SetControllerListeners_m1909848785(__this, L_0, L_1, L_2, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_controllerRightHand_11();
		bool L_4 = __this->get_rightController_3();
		bool* L_5 = __this->get_address_of_rightSubscribed_17();
		VRTK_TouchpadWalking_SetControllerListeners_m1909848785(__this, L_3, L_4, L_5, (bool)1, /*hidden argument*/NULL);
		__this->set_bodyPhysics_22((VRTK_BodyPhysics_t3414085265 *)NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadWalking_Update_m3857399370_MetadataUsageId;
extern "C"  void VRTK_TouchpadWalking_Update_m3857399370 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadWalking_Update_m3857399370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_TouchpadWalking_t2507848959 * G_B3_0 = NULL;
	VRTK_TouchpadWalking_t2507848959 * G_B1_0 = NULL;
	VRTK_TouchpadWalking_t2507848959 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	VRTK_TouchpadWalking_t2507848959 * G_B4_1 = NULL;
	{
		VRTK_ControllerEvents_t3225224819 * L_0 = __this->get_controllerEvents_21();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_1)
		{
			G_B3_0 = __this;
			goto IL_002f;
		}
	}
	{
		int32_t L_2 = __this->get_speedMultiplierButton_8();
		G_B2_0 = G_B1_0;
		if (!L_2)
		{
			G_B3_0 = G_B1_0;
			goto IL_002f;
		}
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_3 = __this->get_controllerEvents_21();
		int32_t L_4 = __this->get_speedMultiplierButton_8();
		NullCheck(L_3);
		bool L_5 = VRTK_ControllerEvents_IsButtonPressed_m407246062(L_3, L_4, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_5));
		G_B4_1 = G_B2_0;
		goto IL_0030;
	}

IL_002f:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0030:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_multiplySpeed_20((bool)G_B4_0);
		GameObject_t1756533147 * L_6 = __this->get_controllerLeftHand_10();
		bool L_7 = __this->get_leftController_2();
		bool* L_8 = __this->get_address_of_leftSubscribed_16();
		bool* L_9 = __this->get_address_of_previousLeftControllerState_24();
		VirtActionInvoker4< GameObject_t1756533147 *, bool, bool*, bool* >::Invoke(10 /* System.Void VRTK.VRTK_TouchpadWalking::CheckControllerState(UnityEngine.GameObject,System.Boolean,System.Boolean&,System.Boolean&) */, __this, L_6, L_7, L_8, L_9);
		GameObject_t1756533147 * L_10 = __this->get_controllerRightHand_11();
		bool L_11 = __this->get_rightController_3();
		bool* L_12 = __this->get_address_of_rightSubscribed_17();
		bool* L_13 = __this->get_address_of_previousRightControllerState_25();
		VirtActionInvoker4< GameObject_t1756533147 *, bool, bool*, bool* >::Invoke(10 /* System.Void VRTK.VRTK_TouchpadWalking::CheckControllerState(UnityEngine.GameObject,System.Boolean,System.Boolean&,System.Boolean&) */, __this, L_10, L_11, L_12, L_13);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::FixedUpdate()
extern "C"  void VRTK_TouchpadWalking_FixedUpdate_m8493968 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(9 /* System.Void VRTK.VRTK_TouchpadWalking::HandleFalling() */, __this);
		float* L_0 = __this->get_address_of_movementSpeed_14();
		Vector2_t2243707579 * L_1 = __this->get_address_of_touchAxis_13();
		float L_2 = L_1->get_y_1();
		VRTK_TouchpadWalking_CalculateSpeed_m3501149496(__this, L_0, L_2, /*hidden argument*/NULL);
		float* L_3 = __this->get_address_of_strafeSpeed_15();
		Vector2_t2243707579 * L_4 = __this->get_address_of_touchAxis_13();
		float L_5 = L_4->get_x_0();
		VRTK_TouchpadWalking_CalculateSpeed_m3501149496(__this, L_3, L_5, /*hidden argument*/NULL);
		VRTK_TouchpadWalking_Move_m3469646986(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::HandleFalling()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadWalking_HandleFalling_m2567566774_MetadataUsageId;
extern "C"  void VRTK_TouchpadWalking_HandleFalling_m2567566774 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadWalking_HandleFalling_m2567566774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VRTK_BodyPhysics_t3414085265 * L_0 = __this->get_bodyPhysics_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		VRTK_BodyPhysics_t3414085265 * L_2 = __this->get_bodyPhysics_22();
		NullCheck(L_2);
		bool L_3 = VRTK_BodyPhysics_IsFalling_m3925295988(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		Vector2_t2243707579  L_4 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_touchAxis_13(L_4);
		__this->set_wasFalling_23((bool)1);
	}

IL_0032:
	{
		VRTK_BodyPhysics_t3414085265 * L_5 = __this->get_bodyPhysics_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0085;
		}
	}
	{
		VRTK_BodyPhysics_t3414085265 * L_7 = __this->get_bodyPhysics_22();
		NullCheck(L_7);
		bool L_8 = VRTK_BodyPhysics_IsFalling_m3925295988(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0085;
		}
	}
	{
		bool L_9 = __this->get_wasFalling_23();
		if (!L_9)
		{
			goto IL_0085;
		}
	}
	{
		Vector2_t2243707579  L_10 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_touchAxis_13(L_10);
		__this->set_wasFalling_23((bool)0);
		__this->set_strafeSpeed_15((0.0f));
		__this->set_movementSpeed_14((0.0f));
	}

IL_0085:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::CheckControllerState(UnityEngine.GameObject,System.Boolean,System.Boolean&,System.Boolean&)
extern "C"  void VRTK_TouchpadWalking_CheckControllerState_m2508879645 (VRTK_TouchpadWalking_t2507848959 * __this, GameObject_t1756533147 * ___controller0, bool ___controllerState1, bool* ___subscribedState2, bool* ___previousState3, const MethodInfo* method)
{
	{
		bool L_0 = ___controllerState1;
		bool* L_1 = ___previousState3;
		if ((((int32_t)L_0) == ((int32_t)(*((int8_t*)L_1)))))
		{
			goto IL_0013;
		}
	}
	{
		GameObject_t1756533147 * L_2 = ___controller0;
		bool L_3 = ___controllerState1;
		bool* L_4 = ___subscribedState2;
		VRTK_TouchpadWalking_SetControllerListeners_m1909848785(__this, L_2, L_3, L_4, (bool)0, /*hidden argument*/NULL);
	}

IL_0013:
	{
		bool* L_5 = ___previousState3;
		bool L_6 = ___controllerState1;
		*((int8_t*)(L_5)) = (int8_t)L_6;
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::DoTouchpadAxisChanged(System.Object,VRTK.ControllerInteractionEventArgs)
extern Il2CppClass* VRTK_ControllerEvents_t3225224819_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadWalking_DoTouchpadAxisChanged_m536676883_MetadataUsageId;
extern "C"  void VRTK_TouchpadWalking_DoTouchpadAxisChanged_m536676883 (VRTK_TouchpadWalking_t2507848959 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadWalking_DoTouchpadAxisChanged_m536676883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___sender0;
		__this->set_controllerEvents_21(((VRTK_ControllerEvents_t3225224819 *)CastclassClass(L_0, VRTK_ControllerEvents_t3225224819_il2cpp_TypeInfo_var)));
		int32_t L_1 = __this->get_moveOnButtonPress_6();
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_2 = __this->get_controllerEvents_21();
		int32_t L_3 = __this->get_moveOnButtonPress_6();
		NullCheck(L_2);
		bool L_4 = VRTK_ControllerEvents_IsButtonPressed_m407246062(L_2, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0040;
		}
	}
	{
		Vector2_t2243707579  L_5 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_touchAxis_13(L_5);
		__this->set_controllerEvents_21((VRTK_ControllerEvents_t3225224819 *)NULL);
		return;
	}

IL_0040:
	{
		Vector2_t2243707579  L_6 = (&___e1)->get_touchpadAxis_2();
		__this->set_touchAxis_13(L_6);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::DoTouchpadTouchEnd(System.Object,VRTK.ControllerInteractionEventArgs)
extern "C"  void VRTK_TouchpadWalking_DoTouchpadTouchEnd_m847870042 (VRTK_TouchpadWalking_t2507848959 * __this, Il2CppObject * ___sender0, ControllerInteractionEventArgs_t287637539  ___e1, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_touchAxis_13(L_0);
		__this->set_controllerEvents_21((VRTK_ControllerEvents_t3225224819 *)NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::CalculateSpeed(System.Single&,System.Single)
extern "C"  void VRTK_TouchpadWalking_CalculateSpeed_m3501149496 (VRTK_TouchpadWalking_t2507848959 * __this, float* ___speed0, float ___inputValue1, const MethodInfo* method)
{
	float* G_B3_0 = NULL;
	float* G_B2_0 = NULL;
	float G_B4_0 = 0.0f;
	float* G_B4_1 = NULL;
	{
		float L_0 = ___inputValue1;
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0037;
		}
	}
	{
		float* L_1 = ___speed0;
		float L_2 = __this->get_maxWalkSpeed_4();
		float L_3 = ___inputValue1;
		*((float*)(L_1)) = (float)((float)((float)L_2*(float)L_3));
		float* L_4 = ___speed0;
		bool L_5 = __this->get_multiplySpeed_20();
		G_B2_0 = L_4;
		if (!L_5)
		{
			G_B3_0 = L_4;
			goto IL_002f;
		}
	}
	{
		float* L_6 = ___speed0;
		float L_7 = __this->get_speedMultiplier_9();
		G_B4_0 = ((float)((float)(*((float*)L_6))*(float)L_7));
		G_B4_1 = G_B2_0;
		goto IL_0031;
	}

IL_002f:
	{
		float* L_8 = ___speed0;
		G_B4_0 = (*((float*)L_8));
		G_B4_1 = G_B3_0;
	}

IL_0031:
	{
		*((float*)(G_B4_1)) = (float)G_B4_0;
		goto IL_003e;
	}

IL_0037:
	{
		float* L_9 = ___speed0;
		VRTK_TouchpadWalking_Decelerate_m1603135194(__this, L_9, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::Decelerate(System.Single&)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadWalking_Decelerate_m1603135194_MetadataUsageId;
extern "C"  void VRTK_TouchpadWalking_Decelerate_m1603135194 (VRTK_TouchpadWalking_t2507848959 * __this, float* ___speed0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadWalking_Decelerate_m1603135194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float* L_0 = ___speed0;
		if ((!(((float)(*((float*)L_0))) > ((float)(0.0f)))))
		{
			goto IL_002c;
		}
	}
	{
		float* L_1 = ___speed0;
		float* L_2 = ___speed0;
		float L_3 = __this->get_deceleration_5();
		float L_4 = __this->get_maxWalkSpeed_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_5 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_3, L_4, (0.0f), /*hidden argument*/NULL);
		*((float*)(L_1)) = (float)((float)((float)(*((float*)L_2))-(float)L_5));
		goto IL_0060;
	}

IL_002c:
	{
		float* L_6 = ___speed0;
		if ((!(((float)(*((float*)L_6))) < ((float)(0.0f)))))
		{
			goto IL_0059;
		}
	}
	{
		float* L_7 = ___speed0;
		float* L_8 = ___speed0;
		float L_9 = __this->get_deceleration_5();
		float L_10 = __this->get_maxWalkSpeed_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_9, ((-L_10)), (0.0f), /*hidden argument*/NULL);
		*((float*)(L_7)) = (float)((float)((float)(*((float*)L_8))+(float)L_11));
		goto IL_0060;
	}

IL_0059:
	{
		float* L_12 = ___speed0;
		*((float*)(L_12)) = (float)(0.0f);
	}

IL_0060:
	{
		V_0 = (0.1f);
		float* L_13 = ___speed0;
		float L_14 = V_0;
		if ((!(((float)(*((float*)L_13))) < ((float)L_14))))
		{
			goto IL_007e;
		}
	}
	{
		float* L_15 = ___speed0;
		float L_16 = V_0;
		if ((!(((float)(*((float*)L_15))) > ((float)((-L_16))))))
		{
			goto IL_007e;
		}
	}
	{
		float* L_17 = ___speed0;
		*((float*)(L_17)) = (float)(0.0f);
	}

IL_007e:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::Move()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadWalking_Move_m3469646986_MetadataUsageId;
extern "C"  void VRTK_TouchpadWalking_Move_m3469646986 (VRTK_TouchpadWalking_t2507848959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadWalking_Move_m3469646986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		int32_t L_0 = __this->get_deviceForDirection_7();
		Transform_t3275118058 * L_1 = VRTK_DeviceFinder_DeviceTransform_m3095571891(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Transform_t3275118058 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00ba;
		}
	}
	{
		Transform_t3275118058 * L_4 = V_0;
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_forward_m1833488937(L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_movementSpeed_14();
		Vector3_t2243707580  L_7 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		float L_8 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Transform_t3275118058 * L_10 = V_0;
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_right_m440863970(L_10, /*hidden argument*/NULL);
		float L_12 = __this->get_strafeSpeed_15();
		Vector3_t2243707580  L_13 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		float L_14 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		Transform_t3275118058 * L_16 = __this->get_playArea_12();
		NullCheck(L_16);
		Vector3_t2243707580  L_17 = Transform_get_position_m1104419803(L_16, /*hidden argument*/NULL);
		V_4 = L_17;
		float L_18 = (&V_4)->get_y_2();
		V_3 = L_18;
		Transform_t3275118058 * L_19 = __this->get_playArea_12();
		Transform_t3275118058 * L_20 = L_19;
		NullCheck(L_20);
		Vector3_t2243707580  L_21 = Transform_get_position_m1104419803(L_20, /*hidden argument*/NULL);
		Vector3_t2243707580  L_22 = V_1;
		Vector3_t2243707580  L_23 = V_2;
		Vector3_t2243707580  L_24 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_21, L_24, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_position_m2469242620(L_20, L_25, /*hidden argument*/NULL);
		Transform_t3275118058 * L_26 = __this->get_playArea_12();
		Transform_t3275118058 * L_27 = __this->get_playArea_12();
		NullCheck(L_27);
		Vector3_t2243707580  L_28 = Transform_get_position_m1104419803(L_27, /*hidden argument*/NULL);
		V_5 = L_28;
		float L_29 = (&V_5)->get_x_1();
		float L_30 = V_3;
		Transform_t3275118058 * L_31 = __this->get_playArea_12();
		NullCheck(L_31);
		Vector3_t2243707580  L_32 = Transform_get_position_m1104419803(L_31, /*hidden argument*/NULL);
		V_6 = L_32;
		float L_33 = (&V_6)->get_z_3();
		Vector3_t2243707580  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector3__ctor_m2638739322(&L_34, L_29, L_30, L_33, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_position_m2469242620(L_26, L_34, /*hidden argument*/NULL);
	}

IL_00ba:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::SetControllerListeners(UnityEngine.GameObject,System.Boolean,System.Boolean&,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TouchpadWalking_SetControllerListeners_m1909848785_MetadataUsageId;
extern "C"  void VRTK_TouchpadWalking_SetControllerListeners_m1909848785 (VRTK_TouchpadWalking_t2507848959 * __this, GameObject_t1756533147 * ___controller0, bool ___controllerState1, bool* ___subscribedState2, bool ___forceDisabled3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadWalking_SetControllerListeners_m1909848785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B4_0 = 0;
	{
		GameObject_t1756533147 * L_0 = ___controller0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		bool L_2 = ___forceDisabled3;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0019;
	}

IL_0018:
	{
		bool L_3 = ___controllerState1;
		G_B4_0 = ((int32_t)(L_3));
	}

IL_0019:
	{
		V_0 = (bool)G_B4_0;
		GameObject_t1756533147 * L_4 = ___controller0;
		bool L_5 = V_0;
		bool* L_6 = ___subscribedState2;
		VRTK_TouchpadWalking_ToggleControllerListeners_m3493193226(__this, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TouchpadWalking::ToggleControllerListeners(UnityEngine.GameObject,System.Boolean,System.Boolean&)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisVRTK_ControllerEvents_t3225224819_m95931981_MethodInfo_var;
extern const uint32_t VRTK_TouchpadWalking_ToggleControllerListeners_m3493193226_MetadataUsageId;
extern "C"  void VRTK_TouchpadWalking_ToggleControllerListeners_m3493193226 (VRTK_TouchpadWalking_t2507848959 * __this, GameObject_t1756533147 * ___controller0, bool ___toggle1, bool* ___subscribed2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TouchpadWalking_ToggleControllerListeners_m3493193226_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_ControllerEvents_t3225224819 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___controller0;
		NullCheck(L_0);
		VRTK_ControllerEvents_t3225224819 * L_1 = GameObject_GetComponent_TisVRTK_ControllerEvents_t3225224819_m95931981(L_0, /*hidden argument*/GameObject_GetComponent_TisVRTK_ControllerEvents_t3225224819_m95931981_MethodInfo_var);
		V_0 = L_1;
		VRTK_ControllerEvents_t3225224819 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		bool L_4 = ___toggle1;
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		bool* L_5 = ___subscribed2;
		if ((*((int8_t*)L_5)))
		{
			goto IL_003f;
		}
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_6 = V_0;
		ControllerInteractionEventHandler_t343979916 * L_7 = __this->get_touchpadAxisChanged_18();
		NullCheck(L_6);
		VRTK_ControllerEvents_add_TouchpadAxisChanged_m3280812681(L_6, L_7, /*hidden argument*/NULL);
		VRTK_ControllerEvents_t3225224819 * L_8 = V_0;
		ControllerInteractionEventHandler_t343979916 * L_9 = __this->get_touchpadUntouched_19();
		NullCheck(L_8);
		VRTK_ControllerEvents_add_TouchpadTouchEnd_m4159394722(L_8, L_9, /*hidden argument*/NULL);
		bool* L_10 = ___subscribed2;
		*((int8_t*)(L_10)) = (int8_t)1;
		goto IL_007d;
	}

IL_003f:
	{
		VRTK_ControllerEvents_t3225224819 * L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		bool L_13 = ___toggle1;
		if (L_13)
		{
			goto IL_007d;
		}
	}
	{
		bool* L_14 = ___subscribed2;
		if (!(*((int8_t*)L_14)))
		{
			goto IL_007d;
		}
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_15 = V_0;
		ControllerInteractionEventHandler_t343979916 * L_16 = __this->get_touchpadAxisChanged_18();
		NullCheck(L_15);
		VRTK_ControllerEvents_remove_TouchpadAxisChanged_m2513257630(L_15, L_16, /*hidden argument*/NULL);
		VRTK_ControllerEvents_t3225224819 * L_17 = V_0;
		ControllerInteractionEventHandler_t343979916 * L_18 = __this->get_touchpadUntouched_19();
		NullCheck(L_17);
		VRTK_ControllerEvents_remove_TouchpadTouchEnd_m1310523989(L_17, L_18, /*hidden argument*/NULL);
		Vector2_t2243707579  L_19 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_touchAxis_13(L_19);
		bool* L_20 = ___subscribed2;
		*((int8_t*)(L_20)) = (int8_t)0;
	}

IL_007d:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TrackedController::.ctor()
extern "C"  void VRTK_TrackedController__ctor_m2801563792 (VRTK_TrackedController_t520756048 * __this, const MethodInfo* method)
{
	{
		__this->set_currentIndex_6((-1));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TrackedController::add_ControllerEnabled(VRTK.VRTKTrackedControllerEventHandler)
extern Il2CppClass* VRTKTrackedControllerEventHandler_t2437916365_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TrackedController_add_ControllerEnabled_m1420062455_MetadataUsageId;
extern "C"  void VRTK_TrackedController_add_ControllerEnabled_m1420062455 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventHandler_t2437916365 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TrackedController_add_ControllerEnabled_m1420062455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTKTrackedControllerEventHandler_t2437916365 * V_0 = NULL;
	VRTKTrackedControllerEventHandler_t2437916365 * V_1 = NULL;
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_0 = __this->get_ControllerEnabled_3();
		V_0 = L_0;
	}

IL_0007:
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_1 = V_0;
		V_1 = L_1;
		VRTKTrackedControllerEventHandler_t2437916365 ** L_2 = __this->get_address_of_ControllerEnabled_3();
		VRTKTrackedControllerEventHandler_t2437916365 * L_3 = V_1;
		VRTKTrackedControllerEventHandler_t2437916365 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		VRTKTrackedControllerEventHandler_t2437916365 * L_6 = V_0;
		VRTKTrackedControllerEventHandler_t2437916365 * L_7 = InterlockedCompareExchangeImpl<VRTKTrackedControllerEventHandler_t2437916365 *>(L_2, ((VRTKTrackedControllerEventHandler_t2437916365 *)CastclassSealed(L_5, VRTKTrackedControllerEventHandler_t2437916365_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		VRTKTrackedControllerEventHandler_t2437916365 * L_8 = V_0;
		VRTKTrackedControllerEventHandler_t2437916365 * L_9 = V_1;
		if ((!(((Il2CppObject*)(VRTKTrackedControllerEventHandler_t2437916365 *)L_8) == ((Il2CppObject*)(VRTKTrackedControllerEventHandler_t2437916365 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_TrackedController::remove_ControllerEnabled(VRTK.VRTKTrackedControllerEventHandler)
extern Il2CppClass* VRTKTrackedControllerEventHandler_t2437916365_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TrackedController_remove_ControllerEnabled_m3397407694_MetadataUsageId;
extern "C"  void VRTK_TrackedController_remove_ControllerEnabled_m3397407694 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventHandler_t2437916365 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TrackedController_remove_ControllerEnabled_m3397407694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTKTrackedControllerEventHandler_t2437916365 * V_0 = NULL;
	VRTKTrackedControllerEventHandler_t2437916365 * V_1 = NULL;
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_0 = __this->get_ControllerEnabled_3();
		V_0 = L_0;
	}

IL_0007:
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_1 = V_0;
		V_1 = L_1;
		VRTKTrackedControllerEventHandler_t2437916365 ** L_2 = __this->get_address_of_ControllerEnabled_3();
		VRTKTrackedControllerEventHandler_t2437916365 * L_3 = V_1;
		VRTKTrackedControllerEventHandler_t2437916365 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		VRTKTrackedControllerEventHandler_t2437916365 * L_6 = V_0;
		VRTKTrackedControllerEventHandler_t2437916365 * L_7 = InterlockedCompareExchangeImpl<VRTKTrackedControllerEventHandler_t2437916365 *>(L_2, ((VRTKTrackedControllerEventHandler_t2437916365 *)CastclassSealed(L_5, VRTKTrackedControllerEventHandler_t2437916365_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		VRTKTrackedControllerEventHandler_t2437916365 * L_8 = V_0;
		VRTKTrackedControllerEventHandler_t2437916365 * L_9 = V_1;
		if ((!(((Il2CppObject*)(VRTKTrackedControllerEventHandler_t2437916365 *)L_8) == ((Il2CppObject*)(VRTKTrackedControllerEventHandler_t2437916365 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_TrackedController::add_ControllerDisabled(VRTK.VRTKTrackedControllerEventHandler)
extern Il2CppClass* VRTKTrackedControllerEventHandler_t2437916365_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TrackedController_add_ControllerDisabled_m102111392_MetadataUsageId;
extern "C"  void VRTK_TrackedController_add_ControllerDisabled_m102111392 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventHandler_t2437916365 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TrackedController_add_ControllerDisabled_m102111392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTKTrackedControllerEventHandler_t2437916365 * V_0 = NULL;
	VRTKTrackedControllerEventHandler_t2437916365 * V_1 = NULL;
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_0 = __this->get_ControllerDisabled_4();
		V_0 = L_0;
	}

IL_0007:
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_1 = V_0;
		V_1 = L_1;
		VRTKTrackedControllerEventHandler_t2437916365 ** L_2 = __this->get_address_of_ControllerDisabled_4();
		VRTKTrackedControllerEventHandler_t2437916365 * L_3 = V_1;
		VRTKTrackedControllerEventHandler_t2437916365 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		VRTKTrackedControllerEventHandler_t2437916365 * L_6 = V_0;
		VRTKTrackedControllerEventHandler_t2437916365 * L_7 = InterlockedCompareExchangeImpl<VRTKTrackedControllerEventHandler_t2437916365 *>(L_2, ((VRTKTrackedControllerEventHandler_t2437916365 *)CastclassSealed(L_5, VRTKTrackedControllerEventHandler_t2437916365_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		VRTKTrackedControllerEventHandler_t2437916365 * L_8 = V_0;
		VRTKTrackedControllerEventHandler_t2437916365 * L_9 = V_1;
		if ((!(((Il2CppObject*)(VRTKTrackedControllerEventHandler_t2437916365 *)L_8) == ((Il2CppObject*)(VRTKTrackedControllerEventHandler_t2437916365 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_TrackedController::remove_ControllerDisabled(VRTK.VRTKTrackedControllerEventHandler)
extern Il2CppClass* VRTKTrackedControllerEventHandler_t2437916365_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TrackedController_remove_ControllerDisabled_m3736811585_MetadataUsageId;
extern "C"  void VRTK_TrackedController_remove_ControllerDisabled_m3736811585 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventHandler_t2437916365 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TrackedController_remove_ControllerDisabled_m3736811585_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTKTrackedControllerEventHandler_t2437916365 * V_0 = NULL;
	VRTKTrackedControllerEventHandler_t2437916365 * V_1 = NULL;
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_0 = __this->get_ControllerDisabled_4();
		V_0 = L_0;
	}

IL_0007:
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_1 = V_0;
		V_1 = L_1;
		VRTKTrackedControllerEventHandler_t2437916365 ** L_2 = __this->get_address_of_ControllerDisabled_4();
		VRTKTrackedControllerEventHandler_t2437916365 * L_3 = V_1;
		VRTKTrackedControllerEventHandler_t2437916365 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		VRTKTrackedControllerEventHandler_t2437916365 * L_6 = V_0;
		VRTKTrackedControllerEventHandler_t2437916365 * L_7 = InterlockedCompareExchangeImpl<VRTKTrackedControllerEventHandler_t2437916365 *>(L_2, ((VRTKTrackedControllerEventHandler_t2437916365 *)CastclassSealed(L_5, VRTKTrackedControllerEventHandler_t2437916365_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		VRTKTrackedControllerEventHandler_t2437916365 * L_8 = V_0;
		VRTKTrackedControllerEventHandler_t2437916365 * L_9 = V_1;
		if ((!(((Il2CppObject*)(VRTKTrackedControllerEventHandler_t2437916365 *)L_8) == ((Il2CppObject*)(VRTKTrackedControllerEventHandler_t2437916365 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_TrackedController::add_ControllerIndexChanged(VRTK.VRTKTrackedControllerEventHandler)
extern Il2CppClass* VRTKTrackedControllerEventHandler_t2437916365_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TrackedController_add_ControllerIndexChanged_m3987335020_MetadataUsageId;
extern "C"  void VRTK_TrackedController_add_ControllerIndexChanged_m3987335020 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventHandler_t2437916365 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TrackedController_add_ControllerIndexChanged_m3987335020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTKTrackedControllerEventHandler_t2437916365 * V_0 = NULL;
	VRTKTrackedControllerEventHandler_t2437916365 * V_1 = NULL;
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_0 = __this->get_ControllerIndexChanged_5();
		V_0 = L_0;
	}

IL_0007:
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_1 = V_0;
		V_1 = L_1;
		VRTKTrackedControllerEventHandler_t2437916365 ** L_2 = __this->get_address_of_ControllerIndexChanged_5();
		VRTKTrackedControllerEventHandler_t2437916365 * L_3 = V_1;
		VRTKTrackedControllerEventHandler_t2437916365 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		VRTKTrackedControllerEventHandler_t2437916365 * L_6 = V_0;
		VRTKTrackedControllerEventHandler_t2437916365 * L_7 = InterlockedCompareExchangeImpl<VRTKTrackedControllerEventHandler_t2437916365 *>(L_2, ((VRTKTrackedControllerEventHandler_t2437916365 *)CastclassSealed(L_5, VRTKTrackedControllerEventHandler_t2437916365_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		VRTKTrackedControllerEventHandler_t2437916365 * L_8 = V_0;
		VRTKTrackedControllerEventHandler_t2437916365 * L_9 = V_1;
		if ((!(((Il2CppObject*)(VRTKTrackedControllerEventHandler_t2437916365 *)L_8) == ((Il2CppObject*)(VRTKTrackedControllerEventHandler_t2437916365 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_TrackedController::remove_ControllerIndexChanged(VRTK.VRTKTrackedControllerEventHandler)
extern Il2CppClass* VRTKTrackedControllerEventHandler_t2437916365_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TrackedController_remove_ControllerIndexChanged_m779748417_MetadataUsageId;
extern "C"  void VRTK_TrackedController_remove_ControllerIndexChanged_m779748417 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventHandler_t2437916365 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TrackedController_remove_ControllerIndexChanged_m779748417_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTKTrackedControllerEventHandler_t2437916365 * V_0 = NULL;
	VRTKTrackedControllerEventHandler_t2437916365 * V_1 = NULL;
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_0 = __this->get_ControllerIndexChanged_5();
		V_0 = L_0;
	}

IL_0007:
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_1 = V_0;
		V_1 = L_1;
		VRTKTrackedControllerEventHandler_t2437916365 ** L_2 = __this->get_address_of_ControllerIndexChanged_5();
		VRTKTrackedControllerEventHandler_t2437916365 * L_3 = V_1;
		VRTKTrackedControllerEventHandler_t2437916365 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		VRTKTrackedControllerEventHandler_t2437916365 * L_6 = V_0;
		VRTKTrackedControllerEventHandler_t2437916365 * L_7 = InterlockedCompareExchangeImpl<VRTKTrackedControllerEventHandler_t2437916365 *>(L_2, ((VRTKTrackedControllerEventHandler_t2437916365 *)CastclassSealed(L_5, VRTKTrackedControllerEventHandler_t2437916365_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		VRTKTrackedControllerEventHandler_t2437916365 * L_8 = V_0;
		VRTKTrackedControllerEventHandler_t2437916365 * L_9 = V_1;
		if ((!(((Il2CppObject*)(VRTKTrackedControllerEventHandler_t2437916365 *)L_8) == ((Il2CppObject*)(VRTKTrackedControllerEventHandler_t2437916365 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_TrackedController::OnControllerEnabled(VRTK.VRTKTrackedControllerEventArgs)
extern "C"  void VRTK_TrackedController_OnControllerEnabled_m2710234991 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventArgs_t2407378264  ___e0, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_currentIndex_6();
		if ((!(((uint32_t)L_0) < ((uint32_t)(-1)))))
		{
			goto IL_0024;
		}
	}
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_1 = __this->get_ControllerEnabled_3();
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_2 = __this->get_ControllerEnabled_3();
		VRTKTrackedControllerEventArgs_t2407378264  L_3 = ___e0;
		NullCheck(L_2);
		VRTKTrackedControllerEventHandler_Invoke_m4037681358(L_2, __this, L_3, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TrackedController::OnControllerDisabled(VRTK.VRTKTrackedControllerEventArgs)
extern "C"  void VRTK_TrackedController_OnControllerDisabled_m3012508802 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventArgs_t2407378264  ___e0, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_currentIndex_6();
		if ((!(((uint32_t)L_0) < ((uint32_t)(-1)))))
		{
			goto IL_0024;
		}
	}
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_1 = __this->get_ControllerDisabled_4();
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_2 = __this->get_ControllerDisabled_4();
		VRTKTrackedControllerEventArgs_t2407378264  L_3 = ___e0;
		NullCheck(L_2);
		VRTKTrackedControllerEventHandler_Invoke_m4037681358(L_2, __this, L_3, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TrackedController::OnControllerIndexChanged(VRTK.VRTKTrackedControllerEventArgs)
extern "C"  void VRTK_TrackedController_OnControllerIndexChanged_m996213926 (VRTK_TrackedController_t520756048 * __this, VRTKTrackedControllerEventArgs_t2407378264  ___e0, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_currentIndex_6();
		if ((!(((uint32_t)L_0) < ((uint32_t)(-1)))))
		{
			goto IL_0024;
		}
	}
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_1 = __this->get_ControllerIndexChanged_5();
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		VRTKTrackedControllerEventHandler_t2437916365 * L_2 = __this->get_ControllerIndexChanged_5();
		VRTKTrackedControllerEventArgs_t2407378264  L_3 = ___e0;
		NullCheck(L_2);
		VRTKTrackedControllerEventHandler_Invoke_m4037681358(L_2, __this, L_3, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// VRTK.VRTKTrackedControllerEventArgs VRTK.VRTK_TrackedController::SetEventPayload(System.UInt32)
extern "C"  VRTKTrackedControllerEventArgs_t2407378264  VRTK_TrackedController_SetEventPayload_m3983054276 (VRTK_TrackedController_t520756048 * __this, uint32_t ___previousIndex0, const MethodInfo* method)
{
	VRTKTrackedControllerEventArgs_t2407378264  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		uint32_t L_0 = __this->get_currentIndex_6();
		(&V_0)->set_currentIndex_0(L_0);
		uint32_t L_1 = ___previousIndex0;
		(&V_0)->set_previousIndex_1(L_1);
		VRTKTrackedControllerEventArgs_t2407378264  L_2 = V_0;
		return L_2;
	}
}
// System.Void VRTK.VRTK_TrackedController::OnEnable()
extern "C"  void VRTK_TrackedController_OnEnable_m1630703200 (VRTK_TrackedController_t520756048 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = VRTK_DeviceFinder_GetScriptAliasController_m946420826(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_aliasController_8(L_1);
		Coroutine_t2299508840 * L_2 = __this->get_enableControllerCoroutine_7();
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		Coroutine_t2299508840 * L_3 = __this->get_enableControllerCoroutine_7();
		MonoBehaviour_StopCoroutine_m1668572632(__this, L_3, /*hidden argument*/NULL);
	}

IL_0028:
	{
		Il2CppObject * L_4 = VRTK_TrackedController_Enable_m2953694943(__this, /*hidden argument*/NULL);
		Coroutine_t2299508840 * L_5 = MonoBehaviour_StartCoroutine_m2470621050(__this, L_4, /*hidden argument*/NULL);
		__this->set_enableControllerCoroutine_7(L_5);
		return;
	}
}
// System.Void VRTK.VRTK_TrackedController::OnDisable()
extern Il2CppCodeGenString* _stringLiteral405776698;
extern const uint32_t VRTK_TrackedController_OnDisable_m2752210889_MetadataUsageId;
extern "C"  void VRTK_TrackedController_OnDisable_m2752210889 (VRTK_TrackedController_t520756048 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TrackedController_OnDisable_m2752210889_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral405776698, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TrackedController::Disable()
extern "C"  void VRTK_TrackedController_Disable_m3589670064 (VRTK_TrackedController_t520756048 * __this, const MethodInfo* method)
{
	{
		Coroutine_t2299508840 * L_0 = __this->get_enableControllerCoroutine_7();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Coroutine_t2299508840 * L_1 = __this->get_enableControllerCoroutine_7();
		MonoBehaviour_StopCoroutine_m1668572632(__this, L_1, /*hidden argument*/NULL);
	}

IL_0017:
	{
		VRTKTrackedControllerEventArgs_t2407378264  L_2 = VRTK_TrackedController_SetEventPayload_m3983054276(__this, (-1), /*hidden argument*/NULL);
		VirtActionInvoker1< VRTKTrackedControllerEventArgs_t2407378264  >::Invoke(5 /* System.Void VRTK.VRTK_TrackedController::OnControllerDisabled(VRTK.VRTKTrackedControllerEventArgs) */, __this, L_2);
		return;
	}
}
// System.Void VRTK.VRTK_TrackedController::Update()
extern Il2CppClass* VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TrackedController_Update_m1693704765_MetadataUsageId;
extern "C"  void VRTK_TrackedController_Update_m1693704765 (VRTK_TrackedController_t520756048 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TrackedController_Update_m1693704765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		uint32_t L_1 = VRTK_DeviceFinder_GetControllerIndex_m601770971(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		uint32_t L_2 = __this->get_currentIndex_6();
		if ((!(((uint32_t)L_2) < ((uint32_t)(-1)))))
		{
			goto IL_003f;
		}
	}
	{
		uint32_t L_3 = V_0;
		uint32_t L_4 = __this->get_currentIndex_6();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_003f;
		}
	}
	{
		uint32_t L_5 = __this->get_currentIndex_6();
		V_1 = L_5;
		uint32_t L_6 = V_0;
		__this->set_currentIndex_6(L_6);
		uint32_t L_7 = V_1;
		VRTKTrackedControllerEventArgs_t2407378264  L_8 = VRTK_TrackedController_SetEventPayload_m3983054276(__this, L_7, /*hidden argument*/NULL);
		VirtActionInvoker1< VRTKTrackedControllerEventArgs_t2407378264  >::Invoke(6 /* System.Void VRTK.VRTK_TrackedController::OnControllerIndexChanged(VRTK.VRTKTrackedControllerEventArgs) */, __this, L_8);
	}

IL_003f:
	{
		uint32_t L_9 = __this->get_currentIndex_6();
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var);
		VRTK_SDK_Bridge_ControllerProcessUpdate_m116141295(NULL /*static, unused*/, L_9, (Dictionary_2_t309261261 *)NULL, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = __this->get_aliasController_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0087;
		}
	}
	{
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		bool L_13 = GameObject_get_activeInHierarchy_m4242915935(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0087;
		}
	}
	{
		GameObject_t1756533147 * L_14 = __this->get_aliasController_8();
		NullCheck(L_14);
		bool L_15 = GameObject_get_activeSelf_m313590879(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0087;
		}
	}
	{
		GameObject_t1756533147 * L_16 = __this->get_aliasController_8();
		NullCheck(L_16);
		GameObject_SetActive_m2887581199(L_16, (bool)1, /*hidden argument*/NULL);
	}

IL_0087:
	{
		return;
	}
}
// System.Collections.IEnumerator VRTK.VRTK_TrackedController::Enable()
extern Il2CppClass* U3CEnableU3Ec__Iterator0_t1493054694_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TrackedController_Enable_m2953694943_MetadataUsageId;
extern "C"  Il2CppObject * VRTK_TrackedController_Enable_m2953694943 (VRTK_TrackedController_t520756048 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TrackedController_Enable_m2953694943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CEnableU3Ec__Iterator0_t1493054694 * V_0 = NULL;
	{
		U3CEnableU3Ec__Iterator0_t1493054694 * L_0 = (U3CEnableU3Ec__Iterator0_t1493054694 *)il2cpp_codegen_object_new(U3CEnableU3Ec__Iterator0_t1493054694_il2cpp_TypeInfo_var);
		U3CEnableU3Ec__Iterator0__ctor_m1868869793(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEnableU3Ec__Iterator0_t1493054694 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CEnableU3Ec__Iterator0_t1493054694 * L_2 = V_0;
		return L_2;
	}
}
// System.Void VRTK.VRTK_TrackedController/<Enable>c__Iterator0::.ctor()
extern "C"  void U3CEnableU3Ec__Iterator0__ctor_m1868869793 (U3CEnableU3Ec__Iterator0_t1493054694 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VRTK.VRTK_TrackedController/<Enable>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var;
extern const uint32_t U3CEnableU3Ec__Iterator0_MoveNext_m931613375_MetadataUsageId;
extern "C"  bool U3CEnableU3Ec__Iterator0_MoveNext_m931613375 (U3CEnableU3Ec__Iterator0_t1493054694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEnableU3Ec__Iterator0_MoveNext_m931613375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0044;
		}
		if (L_1 == 2)
		{
			goto IL_0064;
		}
	}
	{
		goto IL_00b2;
	}

IL_0025:
	{
		WaitForEndOfFrame_t1785723201 * L_2 = (WaitForEndOfFrame_t1785723201 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m3062480170(L_2, /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_003f:
	{
		goto IL_00b4;
	}

IL_0044:
	{
		goto IL_0064;
	}

IL_0049:
	{
		__this->set_U24current_1(NULL);
		bool L_4 = __this->get_U24disposing_2();
		if (L_4)
		{
			goto IL_005f;
		}
	}
	{
		__this->set_U24PC_3(2);
	}

IL_005f:
	{
		goto IL_00b4;
	}

IL_0064:
	{
		VRTK_TrackedController_t520756048 * L_5 = __this->get_U24this_0();
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = GameObject_get_activeInHierarchy_m4242915935(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		VRTK_TrackedController_t520756048 * L_8 = __this->get_U24this_0();
		VRTK_TrackedController_t520756048 * L_9 = __this->get_U24this_0();
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(L_9, /*hidden argument*/NULL);
		uint32_t L_11 = VRTK_DeviceFinder_GetControllerIndex_m601770971(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_currentIndex_6(L_11);
		VRTK_TrackedController_t520756048 * L_12 = __this->get_U24this_0();
		VRTK_TrackedController_t520756048 * L_13 = __this->get_U24this_0();
		NullCheck(L_13);
		VRTKTrackedControllerEventArgs_t2407378264  L_14 = VRTK_TrackedController_SetEventPayload_m3983054276(L_13, (-1), /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< VRTKTrackedControllerEventArgs_t2407378264  >::Invoke(4 /* System.Void VRTK.VRTK_TrackedController::OnControllerEnabled(VRTK.VRTKTrackedControllerEventArgs) */, L_12, L_14);
		__this->set_U24PC_3((-1));
	}

IL_00b2:
	{
		return (bool)0;
	}

IL_00b4:
	{
		return (bool)1;
	}
}
// System.Object VRTK.VRTK_TrackedController/<Enable>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEnableU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2039408495 (U3CEnableU3Ec__Iterator0_t1493054694 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object VRTK.VRTK_TrackedController/<Enable>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEnableU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m685560423 (U3CEnableU3Ec__Iterator0_t1493054694 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void VRTK.VRTK_TrackedController/<Enable>c__Iterator0::Dispose()
extern "C"  void U3CEnableU3Ec__Iterator0_Dispose_m848169744 (U3CEnableU3Ec__Iterator0_t1493054694 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void VRTK.VRTK_TrackedController/<Enable>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CEnableU3Ec__Iterator0_Reset_m1074814810_MetadataUsageId;
extern "C"  void U3CEnableU3Ec__Iterator0_Reset_m1074814810 (U3CEnableU3Ec__Iterator0_t1493054694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEnableU3Ec__Iterator0_Reset_m1074814810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void VRTK.VRTK_TrackedHeadset::.ctor()
extern "C"  void VRTK_TrackedHeadset__ctor_m762156736 (VRTK_TrackedHeadset_t3483597430 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TrackedHeadset::Update()
extern Il2CppClass* VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TrackedHeadset_Update_m708495359_MetadataUsageId;
extern "C"  void VRTK_TrackedHeadset_Update_m708495359 (VRTK_TrackedHeadset_t3483597430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TrackedHeadset_Update_m708495359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var);
		VRTK_SDK_Bridge_HeadsetProcessUpdate_m638289833(NULL /*static, unused*/, (Dictionary_2_t309261261 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TransformFollow::.ctor()
extern "C"  void VRTK_TransformFollow__ctor_m3353458671 (VRTK_TransformFollow_t3532748285 * __this, const MethodInfo* method)
{
	{
		__this->set_moment_16(2);
		VRTK_ObjectFollow__ctor_m2665693762(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_TransformFollow::OnEnable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Camera_t189460977_il2cpp_TypeInfo_var;
extern Il2CppClass* CameraCallback_t834278767_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TransformFollow_OnEnable_m4197672875_MetadataUsageId;
extern "C"  void VRTK_TransformFollow_OnEnable_m4197672875 (VRTK_TransformFollow_t3532748285 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TransformFollow_OnEnable_m4197672875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VRTK_ObjectFollow_OnEnable_m3692853350(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = ((VRTK_ObjectFollow_t3175963762 *)__this)->get_gameObjectToFollow_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		return;
	}

IL_0018:
	{
		GameObject_t1756533147 * L_2 = ((VRTK_ObjectFollow_t3175963762 *)__this)->get_gameObjectToFollow_2();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		__this->set_transformToFollow_17(L_3);
		GameObject_t1756533147 * L_4 = ((VRTK_ObjectFollow_t3175963762 *)__this)->get_gameObjectToChange_3();
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		__this->set_transformToChange_18(L_5);
		int32_t L_6 = __this->get_moment_16();
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_006e;
		}
	}
	{
		CameraCallback_t834278767 * L_7 = ((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->get_onPreRender_3();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 15));
		CameraCallback_t834278767 * L_9 = (CameraCallback_t834278767 *)il2cpp_codegen_object_new(CameraCallback_t834278767_il2cpp_TypeInfo_var);
		CameraCallback__ctor_m2929748071(L_9, __this, L_8, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_10 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->set_onPreRender_3(((CameraCallback_t834278767 *)CastclassSealed(L_10, CameraCallback_t834278767_il2cpp_TypeInfo_var)));
		__this->set_isListeningToOnPreRender_19((bool)1);
	}

IL_006e:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TransformFollow::OnDisable()
extern Il2CppClass* Camera_t189460977_il2cpp_TypeInfo_var;
extern Il2CppClass* CameraCallback_t834278767_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TransformFollow_OnDisable_m345488282_MetadataUsageId;
extern "C"  void VRTK_TransformFollow_OnDisable_m345488282 (VRTK_TransformFollow_t3532748285 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TransformFollow_OnDisable_m345488282_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_transformToFollow_17((Transform_t3275118058 *)NULL);
		__this->set_transformToChange_18((Transform_t3275118058 *)NULL);
		bool L_0 = __this->get_isListeningToOnPreRender_19();
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		CameraCallback_t834278767 * L_1 = ((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->get_onPreRender_3();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 15));
		CameraCallback_t834278767 * L_3 = (CameraCallback_t834278767 *)il2cpp_codegen_object_new(CameraCallback_t834278767_il2cpp_TypeInfo_var);
		CameraCallback__ctor_m2929748071(L_3, __this, L_2, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->set_onPreRender_3(((CameraCallback_t834278767 *)CastclassSealed(L_4, CameraCallback_t834278767_il2cpp_TypeInfo_var)));
		__this->set_isListeningToOnPreRender_19((bool)0);
	}

IL_0041:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TransformFollow::Update()
extern "C"  void VRTK_TransformFollow_Update_m367189140 (VRTK_TransformFollow_t3532748285 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_moment_16();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		VRTK_ObjectFollow_Follow_m541450691(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TransformFollow::LateUpdate()
extern "C"  void VRTK_TransformFollow_LateUpdate_m546395136 (VRTK_TransformFollow_t3532748285 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_moment_16();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		VRTK_ObjectFollow_Follow_m541450691(__this, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void VRTK.VRTK_TransformFollow::OnCamPreRender(UnityEngine.Camera)
extern Il2CppClass* VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_TransformFollow_OnCamPreRender_m4014787152_MetadataUsageId;
extern "C"  void VRTK_TransformFollow_OnCamPreRender_m4014787152 (VRTK_TransformFollow_t3532748285 * __this, Camera_t189460977 * ___cam0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_TransformFollow_OnCamPreRender_m4014787152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = ___cam0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var);
		Transform_t3275118058 * L_3 = VRTK_SDK_Bridge_GetHeadsetCamera_m3254825739(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0020;
		}
	}
	{
		VRTK_ObjectFollow_Follow_m541450691(__this, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// UnityEngine.Vector3 VRTK.VRTK_TransformFollow::GetPositionToFollow()
extern "C"  Vector3_t2243707580  VRTK_TransformFollow_GetPositionToFollow_m33675576 (VRTK_TransformFollow_t3532748285 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = __this->get_transformToFollow_17();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VRTK.VRTK_TransformFollow::SetPositionOnGameObject(UnityEngine.Vector3)
extern "C"  void VRTK_TransformFollow_SetPositionOnGameObject_m398158879 (VRTK_TransformFollow_t3532748285 * __this, Vector3_t2243707580  ___newPosition0, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = __this->get_transformToChange_18();
		Vector3_t2243707580  L_1 = ___newPosition0;
		NullCheck(L_0);
		Transform_set_position_m2469242620(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Quaternion VRTK.VRTK_TransformFollow::GetRotationToFollow()
extern "C"  Quaternion_t4030073918  VRTK_TransformFollow_GetRotationToFollow_m1588216399 (VRTK_TransformFollow_t3532748285 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = __this->get_transformToFollow_17();
		NullCheck(L_0);
		Quaternion_t4030073918  L_1 = Transform_get_rotation_m1033555130(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VRTK.VRTK_TransformFollow::SetRotationOnGameObject(UnityEngine.Quaternion)
extern "C"  void VRTK_TransformFollow_SetRotationOnGameObject_m606028722 (VRTK_TransformFollow_t3532748285 * __this, Quaternion_t4030073918  ___newRotation0, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = __this->get_transformToChange_18();
		Quaternion_t4030073918  L_1 = ___newRotation0;
		NullCheck(L_0);
		Transform_set_rotation_m3411284563(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UICanvas::.ctor()
extern "C"  void VRTK_UICanvas__ctor_m1974588954 (VRTK_UICanvas_t1283311654 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UICanvas::OnEnable()
extern "C"  void VRTK_UICanvas_OnEnable_m3066735918 (VRTK_UICanvas_t1283311654 * __this, const MethodInfo* method)
{
	{
		VRTK_UICanvas_SetupCanvas_m1915469409(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UICanvas::OnDisable()
extern "C"  void VRTK_UICanvas_OnDisable_m1138892475 (VRTK_UICanvas_t1283311654 * __this, const MethodInfo* method)
{
	{
		VRTK_UICanvas_RemoveCanvas_m3047433396(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UICanvas::OnDestroy()
extern "C"  void VRTK_UICanvas_OnDestroy_m4209568341 (VRTK_UICanvas_t1283311654 * __this, const MethodInfo* method)
{
	{
		VRTK_UICanvas_RemoveCanvas_m3047433396(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UICanvas::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInParent_TisVRTK_PlayerObject_t502441292_m2727225383_MethodInfo_var;
extern const MethodInfo* Component_GetComponentInParent_TisVRTK_UIPointer_t2714926455_m56220952_MethodInfo_var;
extern const uint32_t VRTK_UICanvas_OnTriggerEnter_m2083917790_MetadataUsageId;
extern "C"  void VRTK_UICanvas_OnTriggerEnter_m2083917790 (VRTK_UICanvas_t1283311654 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UICanvas_OnTriggerEnter_m2083917790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_PlayerObject_t502441292 * V_0 = NULL;
	VRTK_UIPointer_t2714926455 * V_1 = NULL;
	VRTK_UIPointer_t2714926455 * G_B5_0 = NULL;
	VRTK_UIPointer_t2714926455 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	VRTK_UIPointer_t2714926455 * G_B6_1 = NULL;
	{
		Collider_t3497673348 * L_0 = ___collider0;
		NullCheck(L_0);
		VRTK_PlayerObject_t502441292 * L_1 = Component_GetComponentInParent_TisVRTK_PlayerObject_t502441292_m2727225383(L_0, /*hidden argument*/Component_GetComponentInParent_TisVRTK_PlayerObject_t502441292_m2727225383_MethodInfo_var);
		V_0 = L_1;
		Collider_t3497673348 * L_2 = ___collider0;
		NullCheck(L_2);
		VRTK_UIPointer_t2714926455 * L_3 = Component_GetComponentInParent_TisVRTK_UIPointer_t2714926455_m56220952(L_2, /*hidden argument*/Component_GetComponentInParent_TisVRTK_UIPointer_t2714926455_m56220952_MethodInfo_var);
		V_1 = L_3;
		VRTK_UIPointer_t2714926455 * L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		VRTK_PlayerObject_t502441292 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		VRTK_PlayerObject_t502441292 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_objectType_2();
		if ((!(((uint32_t)L_9) == ((uint32_t)6))))
		{
			goto IL_0048;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_10 = V_1;
		bool L_11 = __this->get_clickOnPointerCollision_2();
		G_B4_0 = L_10;
		if (!L_11)
		{
			G_B5_0 = L_10;
			goto IL_0042;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_0043;
	}

IL_0042:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0043:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_collisionClick_16((bool)G_B6_0);
	}

IL_0048:
	{
		return;
	}
}
// System.Void VRTK.VRTK_UICanvas::OnTriggerExit(UnityEngine.Collider)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInParent_TisVRTK_UIPointer_t2714926455_m56220952_MethodInfo_var;
extern const uint32_t VRTK_UICanvas_OnTriggerExit_m2747870036_MetadataUsageId;
extern "C"  void VRTK_UICanvas_OnTriggerExit_m2747870036 (VRTK_UICanvas_t1283311654 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UICanvas_OnTriggerExit_m2747870036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_UIPointer_t2714926455 * V_0 = NULL;
	{
		Collider_t3497673348 * L_0 = ___collider0;
		NullCheck(L_0);
		VRTK_UIPointer_t2714926455 * L_1 = Component_GetComponentInParent_TisVRTK_UIPointer_t2714926455_m56220952(L_0, /*hidden argument*/Component_GetComponentInParent_TisVRTK_UIPointer_t2714926455_m56220952_MethodInfo_var);
		V_0 = L_1;
		VRTK_UIPointer_t2714926455 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_4 = V_0;
		NullCheck(L_4);
		L_4->set_collisionClick_16((bool)0);
	}

IL_0019:
	{
		return;
	}
}
// System.Void VRTK.VRTK_UICanvas::SetupCanvas()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRectTransform_t3349966182_m1310250299_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGraphicRaycaster_t410733016_m3518220980_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisVRTK_UIGraphicRaycaster_t2816944648_m3913302738_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisVRTK_UIGraphicRaycaster_t2816944648_m136299103_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider_t22920061_m2353115491_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisBoxCollider_t22920061_m1676656656_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2783416216;
extern const uint32_t VRTK_UICanvas_SetupCanvas_m1915469409_MetadataUsageId;
extern "C"  void VRTK_UICanvas_SetupCanvas_m1915469409 (VRTK_UICanvas_t1283311654 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UICanvas_SetupCanvas_m1915469409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Canvas_t209405766 * V_0 = NULL;
	RectTransform_t3349966182 * V_1 = NULL;
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	GraphicRaycaster_t410733016 * V_3 = NULL;
	VRTK_UIGraphicRaycaster_t2816944648 * V_4 = NULL;
	Vector2_t2243707579  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		Canvas_t209405766 * L_0 = Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var);
		V_0 = L_0;
		Canvas_t209405766 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Canvas_t209405766 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = Canvas_get_renderMode_m1816014618(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_0029;
		}
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral2783416216, /*hidden argument*/NULL);
		return;
	}

IL_0029:
	{
		Canvas_t209405766 * L_5 = V_0;
		NullCheck(L_5);
		RectTransform_t3349966182 * L_6 = Component_GetComponent_TisRectTransform_t3349966182_m1310250299(L_5, /*hidden argument*/Component_GetComponent_TisRectTransform_t3349966182_m1310250299_MethodInfo_var);
		V_1 = L_6;
		RectTransform_t3349966182 * L_7 = V_1;
		NullCheck(L_7);
		Vector2_t2243707579  L_8 = RectTransform_get_sizeDelta_m2157326342(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Canvas_t209405766 * L_9 = V_0;
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		GraphicRaycaster_t410733016 * L_11 = GameObject_GetComponent_TisGraphicRaycaster_t410733016_m3518220980(L_10, /*hidden argument*/GameObject_GetComponent_TisGraphicRaycaster_t410733016_m3518220980_MethodInfo_var);
		V_3 = L_11;
		Canvas_t209405766 * L_12 = V_0;
		NullCheck(L_12);
		GameObject_t1756533147 * L_13 = Component_get_gameObject_m3105766835(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		VRTK_UIGraphicRaycaster_t2816944648 * L_14 = GameObject_GetComponent_TisVRTK_UIGraphicRaycaster_t2816944648_m3913302738(L_13, /*hidden argument*/GameObject_GetComponent_TisVRTK_UIGraphicRaycaster_t2816944648_m3913302738_MethodInfo_var);
		V_4 = L_14;
		VRTK_UIGraphicRaycaster_t2816944648 * L_15 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_0069;
		}
	}
	{
		Canvas_t209405766 * L_17 = V_0;
		NullCheck(L_17);
		GameObject_t1756533147 * L_18 = Component_get_gameObject_m3105766835(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		VRTK_UIGraphicRaycaster_t2816944648 * L_19 = GameObject_AddComponent_TisVRTK_UIGraphicRaycaster_t2816944648_m136299103(L_18, /*hidden argument*/GameObject_AddComponent_TisVRTK_UIGraphicRaycaster_t2816944648_m136299103_MethodInfo_var);
		V_4 = L_19;
	}

IL_0069:
	{
		GraphicRaycaster_t410733016 * L_20 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_21 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00a0;
		}
	}
	{
		GraphicRaycaster_t410733016 * L_22 = V_3;
		NullCheck(L_22);
		bool L_23 = Behaviour_get_enabled_m4079055610(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00a0;
		}
	}
	{
		VRTK_UIGraphicRaycaster_t2816944648 * L_24 = V_4;
		GraphicRaycaster_t410733016 * L_25 = V_3;
		NullCheck(L_25);
		bool L_26 = GraphicRaycaster_get_ignoreReversedGraphics_m2757724367(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		GraphicRaycaster_set_ignoreReversedGraphics_m3329065662(L_24, L_26, /*hidden argument*/NULL);
		VRTK_UIGraphicRaycaster_t2816944648 * L_27 = V_4;
		GraphicRaycaster_t410733016 * L_28 = V_3;
		NullCheck(L_28);
		int32_t L_29 = GraphicRaycaster_get_blockingObjects_m1447656241(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		GraphicRaycaster_set_blockingObjects_m1623398080(L_27, L_29, /*hidden argument*/NULL);
		GraphicRaycaster_t410733016 * L_30 = V_3;
		NullCheck(L_30);
		Behaviour_set_enabled_m1796096907(L_30, (bool)0, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		Canvas_t209405766 * L_31 = V_0;
		NullCheck(L_31);
		GameObject_t1756533147 * L_32 = Component_get_gameObject_m3105766835(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		BoxCollider_t22920061 * L_33 = GameObject_GetComponent_TisBoxCollider_t22920061_m2353115491(L_32, /*hidden argument*/GameObject_GetComponent_TisBoxCollider_t22920061_m2353115491_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_34 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_0167;
		}
	}
	{
		RectTransform_t3349966182 * L_35 = V_1;
		NullCheck(L_35);
		Vector2_t2243707579  L_36 = RectTransform_get_pivot_m759087479(L_35, /*hidden argument*/NULL);
		V_5 = L_36;
		V_6 = (0.1f);
		float L_37 = V_6;
		RectTransform_t3349966182 * L_38 = V_1;
		NullCheck(L_38);
		Vector3_t2243707580  L_39 = Transform_get_localScale_m3074381503(L_38, /*hidden argument*/NULL);
		V_8 = L_39;
		float L_40 = (&V_8)->get_z_3();
		V_7 = ((float)((float)L_37/(float)L_40));
		Canvas_t209405766 * L_41 = V_0;
		NullCheck(L_41);
		GameObject_t1756533147 * L_42 = Component_get_gameObject_m3105766835(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		BoxCollider_t22920061 * L_43 = GameObject_AddComponent_TisBoxCollider_t22920061_m1676656656(L_42, /*hidden argument*/GameObject_AddComponent_TisBoxCollider_t22920061_m1676656656_MethodInfo_var);
		__this->set_canvasBoxCollider_4(L_43);
		BoxCollider_t22920061 * L_44 = __this->get_canvasBoxCollider_4();
		float L_45 = (&V_2)->get_x_0();
		float L_46 = (&V_2)->get_y_1();
		float L_47 = V_7;
		Vector3_t2243707580  L_48;
		memset(&L_48, 0, sizeof(L_48));
		Vector3__ctor_m2638739322(&L_48, L_45, L_46, L_47, /*hidden argument*/NULL);
		NullCheck(L_44);
		BoxCollider_set_size_m4101048759(L_44, L_48, /*hidden argument*/NULL);
		BoxCollider_t22920061 * L_49 = __this->get_canvasBoxCollider_4();
		float L_50 = (&V_2)->get_x_0();
		float L_51 = (&V_2)->get_x_0();
		float L_52 = (&V_5)->get_x_0();
		float L_53 = (&V_2)->get_y_1();
		float L_54 = (&V_2)->get_y_1();
		float L_55 = (&V_5)->get_y_1();
		float L_56 = V_7;
		Vector3_t2243707580  L_57;
		memset(&L_57, 0, sizeof(L_57));
		Vector3__ctor_m2638739322(&L_57, ((float)((float)((float)((float)L_50/(float)(2.0f)))-(float)((float)((float)L_51*(float)L_52)))), ((float)((float)((float)((float)L_53/(float)(2.0f)))-(float)((float)((float)L_54*(float)L_55)))), ((float)((float)L_56/(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_49);
		BoxCollider_set_center_m1913800513(L_49, L_57, /*hidden argument*/NULL);
		BoxCollider_t22920061 * L_58 = __this->get_canvasBoxCollider_4();
		NullCheck(L_58);
		Collider_set_isTrigger_m1298573031(L_58, (bool)1, /*hidden argument*/NULL);
	}

IL_0167:
	{
		Canvas_t209405766 * L_59 = V_0;
		NullCheck(L_59);
		GameObject_t1756533147 * L_60 = Component_get_gameObject_m3105766835(L_59, /*hidden argument*/NULL);
		NullCheck(L_60);
		Rigidbody_t4233889191 * L_61 = GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193(L_60, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_62 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		if (L_62)
		{
			goto IL_0199;
		}
	}
	{
		Canvas_t209405766 * L_63 = V_0;
		NullCheck(L_63);
		GameObject_t1756533147 * L_64 = Component_get_gameObject_m3105766835(L_63, /*hidden argument*/NULL);
		NullCheck(L_64);
		Rigidbody_t4233889191 * L_65 = GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210(L_64, /*hidden argument*/GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210_MethodInfo_var);
		__this->set_canvasRigidBody_5(L_65);
		Rigidbody_t4233889191 * L_66 = __this->get_canvasRigidBody_5();
		NullCheck(L_66);
		Rigidbody_set_isKinematic_m738793415(L_66, (bool)1, /*hidden argument*/NULL);
	}

IL_0199:
	{
		Canvas_t209405766 * L_67 = V_0;
		Vector2_t2243707579  L_68 = V_2;
		VRTK_UICanvas_CreateDraggablePanel_m3027513116(__this, L_67, L_68, /*hidden argument*/NULL);
		Canvas_t209405766 * L_69 = V_0;
		Vector2_t2243707579  L_70 = V_2;
		VRTK_UICanvas_CreateActivator_m820516210(__this, L_69, L_70, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UICanvas::CreateDraggablePanel(UnityEngine.Canvas,UnityEngine.Vector2)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisRectTransform_t3349966182_m4066292940_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisImage_t2042527209_m4249278385_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisEventTrigger_t1967201810_m902787999_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1496028191;
extern const uint32_t VRTK_UICanvas_CreateDraggablePanel_m3027513116_MetadataUsageId;
extern "C"  void VRTK_UICanvas_CreateDraggablePanel_m3027513116 (VRTK_UICanvas_t1283311654 * __this, Canvas_t209405766 * ___canvas0, Vector2_t2243707579  ___canvasSize1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UICanvas_CreateDraggablePanel_m3027513116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		Canvas_t209405766 * L_0 = ___canvas0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a6;
		}
	}
	{
		Canvas_t209405766 * L_2 = ___canvas0;
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Transform_FindChild_m2677714886(L_3, _stringLiteral1496028191, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_00a6;
		}
	}
	{
		GameObject_t1756533147 * L_6 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_6, _stringLiteral1496028191, /*hidden argument*/NULL);
		V_0 = L_6;
		GameObject_t1756533147 * L_7 = V_0;
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		Canvas_t209405766 * L_9 = ___canvas0;
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_SetParent_m4124909910(L_8, L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = V_0;
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localPosition_m1026930133(L_12, L_13, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_14 = V_0;
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_16 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_localRotation_m2055111962(L_15, L_16, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_17 = V_0;
		NullCheck(L_17);
		Transform_t3275118058 * L_18 = GameObject_get_transform_m909382139(L_17, /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_set_localScale_m2325460848(L_18, L_19, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = V_0;
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = GameObject_get_transform_m909382139(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_SetAsFirstSibling_m3606528771(L_21, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_22 = V_0;
		NullCheck(L_22);
		GameObject_AddComponent_TisRectTransform_t3349966182_m4066292940(L_22, /*hidden argument*/GameObject_AddComponent_TisRectTransform_t3349966182_m4066292940_MethodInfo_var);
		GameObject_t1756533147 * L_23 = V_0;
		NullCheck(L_23);
		Image_t2042527209 * L_24 = GameObject_AddComponent_TisImage_t2042527209_m4249278385(L_23, /*hidden argument*/GameObject_AddComponent_TisImage_t2042527209_m4249278385_MethodInfo_var);
		Color_t2020392075  L_25 = Color_get_clear_m1469108305(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_24, L_25);
		GameObject_t1756533147 * L_26 = V_0;
		NullCheck(L_26);
		GameObject_AddComponent_TisEventTrigger_t1967201810_m902787999(L_26, /*hidden argument*/GameObject_AddComponent_TisEventTrigger_t1967201810_m902787999_MethodInfo_var);
		GameObject_t1756533147 * L_27 = V_0;
		NullCheck(L_27);
		RectTransform_t3349966182 * L_28 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_27, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		Vector2_t2243707579  L_29 = ___canvasSize1;
		NullCheck(L_28);
		RectTransform_set_sizeDelta_m2319668137(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00a6:
	{
		return;
	}
}
// System.Void VRTK.VRTK_UICanvas::CreateActivator(UnityEngine.Canvas,UnityEngine.Vector2)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRectTransform_t3349966182_m1310250299_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisBoxCollider_t22920061_m1676656656_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisVRTK_UIPointerAutoActivator_t1282719345_m1646521992_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral67875519;
extern Il2CppCodeGenString* _stringLiteral3860406159;
extern const uint32_t VRTK_UICanvas_CreateActivator_m820516210_MetadataUsageId;
extern "C"  void VRTK_UICanvas_CreateActivator_m820516210 (VRTK_UICanvas_t1283311654 * __this, Canvas_t209405766 * ___canvas0, Vector2_t2243707579  ___canvasSize1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UICanvas_CreateActivator_m820516210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3349966182 * V_0 = NULL;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GameObject_t1756533147 * V_2 = NULL;
	float V_3 = 0.0f;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	BoxCollider_t22920061 * V_5 = NULL;
	{
		float L_0 = __this->get_autoActivateWithinDistance_3();
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0157;
		}
	}
	{
		Canvas_t209405766 * L_1 = ___canvas0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0157;
		}
	}
	{
		Canvas_t209405766 * L_3 = ___canvas0;
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Transform_FindChild_m2677714886(L_4, _stringLiteral67875519, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0157;
		}
	}
	{
		Canvas_t209405766 * L_7 = ___canvas0;
		NullCheck(L_7);
		RectTransform_t3349966182 * L_8 = Component_GetComponent_TisRectTransform_t3349966182_m1310250299(L_7, /*hidden argument*/Component_GetComponent_TisRectTransform_t3349966182_m1310250299_MethodInfo_var);
		V_0 = L_8;
		RectTransform_t3349966182 * L_9 = V_0;
		NullCheck(L_9);
		Vector2_t2243707579  L_10 = RectTransform_get_pivot_m759087479(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		GameObject_t1756533147 * L_11 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_11, _stringLiteral67875519, /*hidden argument*/NULL);
		V_2 = L_11;
		GameObject_t1756533147 * L_12 = V_2;
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = GameObject_get_transform_m909382139(L_12, /*hidden argument*/NULL);
		Canvas_t209405766 * L_14 = ___canvas0;
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_SetParent_m4124909910(L_13, L_15, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_16 = V_2;
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = GameObject_get_transform_m909382139(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_SetAsFirstSibling_m3606528771(L_17, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_18 = V_2;
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = GameObject_get_transform_m909382139(L_18, /*hidden argument*/NULL);
		float L_20 = (&___canvasSize1)->get_x_0();
		float L_21 = (&___canvasSize1)->get_x_0();
		float L_22 = (&V_1)->get_x_0();
		float L_23 = (&___canvasSize1)->get_y_1();
		float L_24 = (&___canvasSize1)->get_y_1();
		float L_25 = (&V_1)->get_y_1();
		Vector3_t2243707580  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Vector3__ctor_m2720820983(&L_26, ((float)((float)((float)((float)L_20/(float)(2.0f)))-(float)((float)((float)L_21*(float)L_22)))), ((float)((float)((float)((float)L_23/(float)(2.0f)))-(float)((float)((float)L_24*(float)L_25)))), /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_localPosition_m1026930133(L_19, L_26, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = V_2;
		NullCheck(L_27);
		Transform_t3275118058 * L_28 = GameObject_get_transform_m909382139(L_27, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_29 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_set_localRotation_m2055111962(L_28, L_29, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_30 = V_2;
		NullCheck(L_30);
		Transform_t3275118058 * L_31 = GameObject_get_transform_m909382139(L_30, /*hidden argument*/NULL);
		Vector3_t2243707580  L_32 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_set_localScale_m2325460848(L_31, L_32, /*hidden argument*/NULL);
		float L_33 = __this->get_autoActivateWithinDistance_3();
		RectTransform_t3349966182 * L_34 = V_0;
		NullCheck(L_34);
		Vector3_t2243707580  L_35 = Transform_get_localScale_m3074381503(L_34, /*hidden argument*/NULL);
		V_4 = L_35;
		float L_36 = (&V_4)->get_z_3();
		V_3 = ((float)((float)L_33/(float)L_36));
		GameObject_t1756533147 * L_37 = V_2;
		NullCheck(L_37);
		BoxCollider_t22920061 * L_38 = GameObject_AddComponent_TisBoxCollider_t22920061_m1676656656(L_37, /*hidden argument*/GameObject_AddComponent_TisBoxCollider_t22920061_m1676656656_MethodInfo_var);
		V_5 = L_38;
		BoxCollider_t22920061 * L_39 = V_5;
		NullCheck(L_39);
		Collider_set_isTrigger_m1298573031(L_39, (bool)1, /*hidden argument*/NULL);
		BoxCollider_t22920061 * L_40 = V_5;
		float L_41 = (&___canvasSize1)->get_x_0();
		float L_42 = (&___canvasSize1)->get_y_1();
		float L_43 = V_3;
		Vector3_t2243707580  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Vector3__ctor_m2638739322(&L_44, L_41, L_42, L_43, /*hidden argument*/NULL);
		NullCheck(L_40);
		BoxCollider_set_size_m4101048759(L_40, L_44, /*hidden argument*/NULL);
		BoxCollider_t22920061 * L_45 = V_5;
		float L_46 = V_3;
		Vector3_t2243707580  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Vector3__ctor_m2638739322(&L_47, (0.0f), (0.0f), ((-((float)((float)L_46/(float)(2.0f))))), /*hidden argument*/NULL);
		NullCheck(L_45);
		BoxCollider_set_center_m1913800513(L_45, L_47, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_48 = V_2;
		NullCheck(L_48);
		Rigidbody_t4233889191 * L_49 = GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210(L_48, /*hidden argument*/GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210_MethodInfo_var);
		NullCheck(L_49);
		Rigidbody_set_isKinematic_m738793415(L_49, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_50 = V_2;
		NullCheck(L_50);
		GameObject_AddComponent_TisVRTK_UIPointerAutoActivator_t1282719345_m1646521992(L_50, /*hidden argument*/GameObject_AddComponent_TisVRTK_UIPointerAutoActivator_t1282719345_m1646521992_MethodInfo_var);
		GameObject_t1756533147 * L_51 = V_2;
		int32_t L_52 = LayerMask_NameToLayer_m1506372053(NULL /*static, unused*/, _stringLiteral3860406159, /*hidden argument*/NULL);
		NullCheck(L_51);
		GameObject_set_layer_m2712461877(L_51, L_52, /*hidden argument*/NULL);
	}

IL_0157:
	{
		return;
	}
}
// System.Void VRTK.VRTK_UICanvas::RemoveCanvas()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGraphicRaycaster_t410733016_m3518220980_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisVRTK_UIGraphicRaycaster_t2816944648_m3913302738_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1496028191;
extern Il2CppCodeGenString* _stringLiteral67875519;
extern const uint32_t VRTK_UICanvas_RemoveCanvas_m3047433396_MetadataUsageId;
extern "C"  void VRTK_UICanvas_RemoveCanvas_m3047433396 (VRTK_UICanvas_t1283311654 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UICanvas_RemoveCanvas_m3047433396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Canvas_t209405766 * V_0 = NULL;
	GraphicRaycaster_t410733016 * V_1 = NULL;
	VRTK_UIGraphicRaycaster_t2816944648 * V_2 = NULL;
	Transform_t3275118058 * V_3 = NULL;
	Transform_t3275118058 * V_4 = NULL;
	{
		Canvas_t209405766 * L_0 = Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var);
		V_0 = L_0;
		Canvas_t209405766 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Canvas_t209405766 * L_3 = V_0;
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GraphicRaycaster_t410733016 * L_5 = GameObject_GetComponent_TisGraphicRaycaster_t410733016_m3518220980(L_4, /*hidden argument*/GameObject_GetComponent_TisGraphicRaycaster_t410733016_m3518220980_MethodInfo_var);
		V_1 = L_5;
		Canvas_t209405766 * L_6 = V_0;
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		VRTK_UIGraphicRaycaster_t2816944648 * L_8 = GameObject_GetComponent_TisVRTK_UIGraphicRaycaster_t2816944648_m3913302738(L_7, /*hidden argument*/GameObject_GetComponent_TisVRTK_UIGraphicRaycaster_t2816944648_m3913302738_MethodInfo_var);
		V_2 = L_8;
		VRTK_UIGraphicRaycaster_t2816944648 * L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_003c;
		}
	}
	{
		VRTK_UIGraphicRaycaster_t2816944648 * L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_003c:
	{
		GraphicRaycaster_t410733016 * L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0059;
		}
	}
	{
		GraphicRaycaster_t410733016 * L_14 = V_1;
		NullCheck(L_14);
		bool L_15 = Behaviour_get_enabled_m4079055610(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0059;
		}
	}
	{
		GraphicRaycaster_t410733016 * L_16 = V_1;
		NullCheck(L_16);
		Behaviour_set_enabled_m1796096907(L_16, (bool)1, /*hidden argument*/NULL);
	}

IL_0059:
	{
		BoxCollider_t22920061 * L_17 = __this->get_canvasBoxCollider_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0074;
		}
	}
	{
		BoxCollider_t22920061 * L_19 = __this->get_canvasBoxCollider_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_0074:
	{
		Rigidbody_t4233889191 * L_20 = __this->get_canvasRigidBody_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_21 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_008f;
		}
	}
	{
		Rigidbody_t4233889191 * L_22 = __this->get_canvasRigidBody_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
	}

IL_008f:
	{
		Canvas_t209405766 * L_23 = V_0;
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = Component_get_transform_m2697483695(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_t3275118058 * L_25 = Transform_FindChild_m2677714886(L_24, _stringLiteral1496028191, /*hidden argument*/NULL);
		V_3 = L_25;
		Transform_t3275118058 * L_26 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_27 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00b6;
		}
	}
	{
		Transform_t3275118058 * L_28 = V_3;
		NullCheck(L_28);
		GameObject_t1756533147 * L_29 = Component_get_gameObject_m3105766835(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
	}

IL_00b6:
	{
		Canvas_t209405766 * L_30 = V_0;
		NullCheck(L_30);
		Transform_t3275118058 * L_31 = Component_get_transform_m2697483695(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_t3275118058 * L_32 = Transform_FindChild_m2677714886(L_31, _stringLiteral67875519, /*hidden argument*/NULL);
		V_4 = L_32;
		Transform_t3275118058 * L_33 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_34 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00e0;
		}
	}
	{
		Transform_t3275118058 * L_35 = V_4;
		NullCheck(L_35);
		GameObject_t1756533147 * L_36 = Component_get_gameObject_m3105766835(L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
	}

IL_00e0:
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIDraggableItem::.ctor()
extern "C"  void VRTK_UIDraggableItem__ctor_m3029610376 (VRTK_UIDraggableItem_t2269178406 * __this, const MethodInfo* method)
{
	{
		__this->set_forwardOffset_4((0.1f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UIDraggableItem::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInParent_TisCanvas_t209405766_m2415242607_MethodInfo_var;
extern const MethodInfo* Component_GetComponentInParent_TisVRTK_UIDropZone_t1661569883_m290600918_MethodInfo_var;
extern const uint32_t VRTK_UIDraggableItem_OnBeginDrag_m407964972_MetadataUsageId;
extern "C"  void VRTK_UIDraggableItem_OnBeginDrag_m407964972 (VRTK_UIDraggableItem_t2269178406 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIDraggableItem_OnBeginDrag_m407964972_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_UIPointer_t2714926455 * V_0 = NULL;
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		__this->set_startPosition_7(L_1);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Quaternion_t4030073918  L_3 = Transform_get_rotation_m1033555130(L_2, /*hidden argument*/NULL);
		__this->set_startRotation_8(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Transform_get_parent_m147407266(L_4, /*hidden argument*/NULL);
		__this->set_startParent_10(L_5);
		Canvas_t209405766 * L_6 = Component_GetComponentInParent_TisCanvas_t209405766_m2415242607(__this, /*hidden argument*/Component_GetComponentInParent_TisCanvas_t209405766_m2415242607_MethodInfo_var);
		__this->set_startCanvas_11(L_6);
		CanvasGroup_t3296560743 * L_7 = __this->get_canvasGroup_12();
		NullCheck(L_7);
		CanvasGroup_set_blocksRaycasts_m3812230476(L_7, (bool)0, /*hidden argument*/NULL);
		bool L_8 = __this->get_restrictToDropZone_2();
		if (!L_8)
		{
			goto IL_0073;
		}
	}
	{
		VRTK_UIDropZone_t1661569883 * L_9 = Component_GetComponentInParent_TisVRTK_UIDropZone_t1661569883_m290600918(__this, /*hidden argument*/Component_GetComponentInParent_TisVRTK_UIDropZone_t1661569883_m290600918_MethodInfo_var);
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(L_9, /*hidden argument*/NULL);
		__this->set_startDropZone_9(L_10);
		GameObject_t1756533147 * L_11 = __this->get_startDropZone_9();
		__this->set_validDropZone_5(L_11);
	}

IL_0073:
	{
		PointerEventData_t1599784723 * L_12 = ___eventData0;
		VRTK_UIDraggableItem_SetDragPosition_m533012469(__this, L_12, /*hidden argument*/NULL);
		PointerEventData_t1599784723 * L_13 = ___eventData0;
		VRTK_UIPointer_t2714926455 * L_14 = VRTK_UIDraggableItem_GetPointer_m1874048532(__this, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		VRTK_UIPointer_t2714926455 * L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00a0;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_17 = V_0;
		VRTK_UIPointer_t2714926455 * L_18 = V_0;
		GameObject_t1756533147 * L_19 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		UIPointerEventArgs_t1171985978  L_20 = VirtFuncInvoker2< UIPointerEventArgs_t1171985978 , GameObject_t1756533147 *, GameObject_t1756533147 * >::Invoke(9 /* VRTK.UIPointerEventArgs VRTK.VRTK_UIPointer::SetUIPointerEvent(UnityEngine.GameObject,UnityEngine.GameObject) */, L_18, L_19, (GameObject_t1756533147 *)NULL);
		NullCheck(L_17);
		VirtActionInvoker1< UIPointerEventArgs_t1171985978  >::Invoke(7 /* System.Void VRTK.VRTK_UIPointer::OnUIPointerElementDragStart(VRTK.UIPointerEventArgs) */, L_17, L_20);
	}

IL_00a0:
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIDraggableItem::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void VRTK_UIDraggableItem_OnDrag_m3631955875 (VRTK_UIDraggableItem_t2269178406 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method)
{
	{
		PointerEventData_t1599784723 * L_0 = ___eventData0;
		VRTK_UIDraggableItem_SetDragPosition_m533012469(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UIDraggableItem::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentInParent_TisCanvas_t209405766_m4180257979_MethodInfo_var;
extern const uint32_t VRTK_UIDraggableItem_OnEndDrag_m2148315312_MetadataUsageId;
extern "C"  void VRTK_UIDraggableItem_OnEndDrag_m2148315312 (VRTK_UIDraggableItem_t2269178406 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIDraggableItem_OnEndDrag_m2148315312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Canvas_t209405766 * V_1 = NULL;
	VRTK_UIPointer_t2714926455 * V_2 = NULL;
	Canvas_t209405766 * G_B8_0 = NULL;
	{
		CanvasGroup_t3296560743 * L_0 = __this->get_canvasGroup_12();
		NullCheck(L_0);
		CanvasGroup_set_blocksRaycasts_m3812230476(L_0, (bool)1, /*hidden argument*/NULL);
		__this->set_dragTransform_6((RectTransform_t3349966182 *)NULL);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = L_1;
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_forward_m1833488937(L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_forwardOffset_4();
		Vector3_t2243707580  L_7 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_3, L_7, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m2469242620(L_2, L_8, /*hidden argument*/NULL);
		V_0 = (bool)1;
		bool L_9 = __this->get_restrictToDropZone_2();
		if (!L_9)
		{
			goto IL_0095;
		}
	}
	{
		GameObject_t1756533147 * L_10 = __this->get_validDropZone_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_008d;
		}
	}
	{
		GameObject_t1756533147 * L_12 = __this->get_validDropZone_5();
		GameObject_t1756533147 * L_13 = __this->get_startDropZone_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_008d;
		}
	}
	{
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_16 = __this->get_validDropZone_5();
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = GameObject_get_transform_m909382139(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_SetParent_m4124909910(L_15, L_17, /*hidden argument*/NULL);
		goto IL_0095;
	}

IL_008d:
	{
		VRTK_UIDraggableItem_ResetElement_m2312080505(__this, /*hidden argument*/NULL);
		V_0 = (bool)0;
	}

IL_0095:
	{
		PointerEventData_t1599784723 * L_18 = ___eventData0;
		NullCheck(L_18);
		GameObject_t1756533147 * L_19 = PointerEventData_get_pointerEnter_m2114522773(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b5;
		}
	}
	{
		PointerEventData_t1599784723 * L_21 = ___eventData0;
		NullCheck(L_21);
		GameObject_t1756533147 * L_22 = PointerEventData_get_pointerEnter_m2114522773(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Canvas_t209405766 * L_23 = GameObject_GetComponentInParent_TisCanvas_t209405766_m4180257979(L_22, /*hidden argument*/GameObject_GetComponentInParent_TisCanvas_t209405766_m4180257979_MethodInfo_var);
		G_B8_0 = L_23;
		goto IL_00b6;
	}

IL_00b5:
	{
		G_B8_0 = ((Canvas_t209405766 *)(NULL));
	}

IL_00b6:
	{
		V_1 = G_B8_0;
		bool L_24 = __this->get_restrictToOriginalCanvas_3();
		if (!L_24)
		{
			goto IL_00e6;
		}
	}
	{
		Canvas_t209405766 * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00e6;
		}
	}
	{
		Canvas_t209405766 * L_27 = V_1;
		Canvas_t209405766 * L_28 = __this->get_startCanvas_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_29 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00e6;
		}
	}
	{
		VRTK_UIDraggableItem_ResetElement_m2312080505(__this, /*hidden argument*/NULL);
		V_0 = (bool)0;
	}

IL_00e6:
	{
		Canvas_t209405766 * L_30 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_31 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_30, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00fa;
		}
	}
	{
		VRTK_UIDraggableItem_ResetElement_m2312080505(__this, /*hidden argument*/NULL);
		V_0 = (bool)0;
	}

IL_00fa:
	{
		bool L_32 = V_0;
		if (!L_32)
		{
			goto IL_0126;
		}
	}
	{
		PointerEventData_t1599784723 * L_33 = ___eventData0;
		VRTK_UIPointer_t2714926455 * L_34 = VRTK_UIDraggableItem_GetPointer_m1874048532(__this, L_33, /*hidden argument*/NULL);
		V_2 = L_34;
		VRTK_UIPointer_t2714926455 * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_36 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0126;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_37 = V_2;
		VRTK_UIPointer_t2714926455 * L_38 = V_2;
		GameObject_t1756533147 * L_39 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_38);
		UIPointerEventArgs_t1171985978  L_40 = VirtFuncInvoker2< UIPointerEventArgs_t1171985978 , GameObject_t1756533147 *, GameObject_t1756533147 * >::Invoke(9 /* VRTK.UIPointerEventArgs VRTK.VRTK_UIPointer::SetUIPointerEvent(UnityEngine.GameObject,UnityEngine.GameObject) */, L_38, L_39, (GameObject_t1756533147 *)NULL);
		NullCheck(L_37);
		VirtActionInvoker1< UIPointerEventArgs_t1171985978  >::Invoke(8 /* System.Void VRTK.VRTK_UIPointer::OnUIPointerElementDragEnd(VRTK.UIPointerEventArgs) */, L_37, L_40);
	}

IL_0126:
	{
		__this->set_validDropZone_5((GameObject_t1756533147 *)NULL);
		__this->set_startParent_10((Transform_t3275118058 *)NULL);
		__this->set_startCanvas_11((Canvas_t209405766 *)NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UIDraggableItem::OnEnable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvasGroup_t3296560743_m846564447_MethodInfo_var;
extern const MethodInfo* Component_GetComponentInParent_TisVRTK_UIDropZone_t1661569883_m290600918_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4035578045;
extern const uint32_t VRTK_UIDraggableItem_OnEnable_m2200266388_MetadataUsageId;
extern "C"  void VRTK_UIDraggableItem_OnEnable_m2200266388 (VRTK_UIDraggableItem_t2269178406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIDraggableItem_OnEnable_m2200266388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CanvasGroup_t3296560743 * L_0 = Component_GetComponent_TisCanvasGroup_t3296560743_m846564447(__this, /*hidden argument*/Component_GetComponent_TisCanvasGroup_t3296560743_m846564447_MethodInfo_var);
		__this->set_canvasGroup_12(L_0);
		bool L_1 = __this->get_restrictToDropZone_2();
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		VRTK_UIDropZone_t1661569883 * L_2 = Component_GetComponentInParent_TisVRTK_UIDropZone_t1661569883_m290600918(__this, /*hidden argument*/Component_GetComponentInParent_TisVRTK_UIDropZone_t1661569883_m290600918_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral4035578045, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// VRTK.VRTK_UIPointer VRTK.VRTK_UIDraggableItem::GetPointer(UnityEngine.EventSystems.PointerEventData)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisVRTK_UIPointer_t2714926455_m2163511105_MethodInfo_var;
extern const uint32_t VRTK_UIDraggableItem_GetPointer_m1874048532_MetadataUsageId;
extern "C"  VRTK_UIPointer_t2714926455 * VRTK_UIDraggableItem_GetPointer_m1874048532 (VRTK_UIDraggableItem_t2269178406 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIDraggableItem_GetPointer_m1874048532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	VRTK_UIPointer_t2714926455 * G_B3_0 = NULL;
	{
		PointerEventData_t1599784723 * L_0 = ___eventData0;
		NullCheck(L_0);
		int32_t L_1 = PointerEventData_get_pointerId_m2835313597(L_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = VRTK_DeviceFinder_GetControllerByIndex_m261285063(NULL /*static, unused*/, L_1, (bool)0, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0023;
		}
	}
	{
		GameObject_t1756533147 * L_5 = V_0;
		NullCheck(L_5);
		VRTK_UIPointer_t2714926455 * L_6 = GameObject_GetComponent_TisVRTK_UIPointer_t2714926455_m2163511105(L_5, /*hidden argument*/GameObject_GetComponent_TisVRTK_UIPointer_t2714926455_m2163511105_MethodInfo_var);
		G_B3_0 = L_6;
		goto IL_0024;
	}

IL_0023:
	{
		G_B3_0 = ((VRTK_UIPointer_t2714926455 *)(NULL));
	}

IL_0024:
	{
		return G_B3_0;
	}
}
// System.Void VRTK.VRTK_UIDraggableItem::SetDragPosition(UnityEngine.EventSystems.PointerEventData)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIDraggableItem_SetDragPosition_m533012469_MetadataUsageId;
extern "C"  void VRTK_UIDraggableItem_SetDragPosition_m533012469 (VRTK_UIDraggableItem_t2269178406 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIDraggableItem_SetDragPosition_m533012469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PointerEventData_t1599784723 * L_0 = ___eventData0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = PointerEventData_get_pointerEnter_m2114522773(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0042;
		}
	}
	{
		PointerEventData_t1599784723 * L_3 = ___eventData0;
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = PointerEventData_get_pointerEnter_m2114522773(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, ((RectTransform_t3349966182 *)IsInstSealed(L_5, RectTransform_t3349966182_il2cpp_TypeInfo_var)), (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		PointerEventData_t1599784723 * L_7 = ___eventData0;
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = PointerEventData_get_pointerEnter_m2114522773(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		__this->set_dragTransform_6(((RectTransform_t3349966182 *)IsInstSealed(L_9, RectTransform_t3349966182_il2cpp_TypeInfo_var)));
	}

IL_0042:
	{
		RectTransform_t3349966182 * L_10 = __this->get_dragTransform_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00ad;
		}
	}
	{
		RectTransform_t3349966182 * L_12 = __this->get_dragTransform_6();
		PointerEventData_t1599784723 * L_13 = ___eventData0;
		NullCheck(L_13);
		Vector2_t2243707579  L_14 = PointerEventData_get_position_m2131765015(L_13, /*hidden argument*/NULL);
		PointerEventData_t1599784723 * L_15 = ___eventData0;
		NullCheck(L_15);
		Camera_t189460977 * L_16 = PointerEventData_get_pressEventCamera_m724559964(L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		bool L_17 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m2304638810(NULL /*static, unused*/, L_12, L_14, L_16, (&V_0), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00ad;
		}
	}
	{
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = V_0;
		Transform_t3275118058 * L_20 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t2243707580  L_21 = Transform_get_forward_m1833488937(L_20, /*hidden argument*/NULL);
		float L_22 = __this->get_forwardOffset_4();
		Vector3_t2243707580  L_23 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		Vector3_t2243707580  L_24 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_19, L_23, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_set_position_m2469242620(L_18, L_24, /*hidden argument*/NULL);
		Transform_t3275118058 * L_25 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_26 = __this->get_dragTransform_6();
		NullCheck(L_26);
		Quaternion_t4030073918  L_27 = Transform_get_rotation_m1033555130(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_rotation_m3411284563(L_25, L_27, /*hidden argument*/NULL);
	}

IL_00ad:
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIDraggableItem::ResetElement()
extern "C"  void VRTK_UIDraggableItem_ResetElement_m2312080505 (VRTK_UIDraggableItem_t2269178406 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = __this->get_startPosition_7();
		NullCheck(L_0);
		Transform_set_position_m2469242620(L_0, L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_3 = __this->get_startRotation_8();
		NullCheck(L_2);
		Transform_set_rotation_m3411284563(L_2, L_3, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = __this->get_startParent_10();
		NullCheck(L_4);
		Transform_SetParent_m4124909910(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UIDropZone::.ctor()
extern "C"  void VRTK_UIDropZone__ctor_m2283027709 (VRTK_UIDropZone_t1661569883 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UIDropZone::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisVRTK_UIDraggableItem_t2269178406_m2049988108_MethodInfo_var;
extern const uint32_t VRTK_UIDropZone_OnPointerEnter_m4214959127_MetadataUsageId;
extern "C"  void VRTK_UIDropZone_OnPointerEnter_m4214959127 (VRTK_UIDropZone_t1661569883 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIDropZone_OnPointerEnter_m4214959127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_UIDraggableItem_t2269178406 * V_0 = NULL;
	{
		PointerEventData_t1599784723 * L_0 = ___eventData0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = PointerEventData_get_pointerDrag_m2740415629(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0045;
		}
	}
	{
		PointerEventData_t1599784723 * L_3 = ___eventData0;
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = PointerEventData_get_pointerDrag_m2740415629(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		VRTK_UIDraggableItem_t2269178406 * L_5 = GameObject_GetComponent_TisVRTK_UIDraggableItem_t2269178406_m2049988108(L_4, /*hidden argument*/GameObject_GetComponent_TisVRTK_UIDraggableItem_t2269178406_m2049988108_MethodInfo_var);
		V_0 = L_5;
		VRTK_UIDraggableItem_t2269178406 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0045;
		}
	}
	{
		VRTK_UIDraggableItem_t2269178406 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = L_8->get_restrictToDropZone_2();
		if (!L_9)
		{
			goto IL_0045;
		}
	}
	{
		VRTK_UIDraggableItem_t2269178406 * L_10 = V_0;
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		L_10->set_validDropZone_5(L_11);
		VRTK_UIDraggableItem_t2269178406 * L_12 = V_0;
		__this->set_droppableItem_2(L_12);
	}

IL_0045:
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIDropZone::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIDropZone_OnPointerExit_m4192455799_MetadataUsageId;
extern "C"  void VRTK_UIDropZone_OnPointerExit_m4192455799 (VRTK_UIDropZone_t1661569883 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIDropZone_OnPointerExit_m4192455799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VRTK_UIDraggableItem_t2269178406 * L_0 = __this->get_droppableItem_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		VRTK_UIDraggableItem_t2269178406 * L_2 = __this->get_droppableItem_2();
		NullCheck(L_2);
		L_2->set_validDropZone_5((GameObject_t1756533147 *)NULL);
	}

IL_001c:
	{
		__this->set_droppableItem_2((VRTK_UIDraggableItem_t2269178406 *)NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UIGraphicRaycaster::.ctor()
extern Il2CppClass* GraphicRaycaster_t410733016_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIGraphicRaycaster__ctor_m2535130454_MetadataUsageId;
extern "C"  void VRTK_UIGraphicRaycaster__ctor_m2535130454 (VRTK_UIGraphicRaycaster_t2816944648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIGraphicRaycaster__ctor_m2535130454_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GraphicRaycaster_t410733016_il2cpp_TypeInfo_var);
		GraphicRaycaster__ctor_m900848387(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UIGraphicRaycaster::Raycast(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* VRTK_UIGraphicRaycaster_t2816944648_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m392100656_MethodInfo_var;
extern const uint32_t VRTK_UIGraphicRaycaster_Raycast_m1162287854_MetadataUsageId;
extern "C"  void VRTK_UIGraphicRaycaster_Raycast_m1162287854 (VRTK_UIGraphicRaycaster_t2816944648 * __this, PointerEventData_t1599784723 * ___eventData0, List_1_t3685274804 * ___resultAppendList1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIGraphicRaycaster_Raycast_m1162287854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastResult_t21186376  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RaycastResult_t21186376  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Canvas_t209405766 * L_0 = VRTK_UIGraphicRaycaster_get_canvas_m1893935089(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		PointerEventData_t1599784723 * L_2 = ___eventData0;
		NullCheck(L_2);
		RaycastResult_t21186376  L_3 = PointerEventData_get_pointerCurrentRaycast_m1374279130(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_t2243707580  L_4 = (&V_1)->get_worldPosition_7();
		PointerEventData_t1599784723 * L_5 = ___eventData0;
		NullCheck(L_5);
		RaycastResult_t21186376  L_6 = PointerEventData_get_pointerCurrentRaycast_m1374279130(L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Vector3_t2243707580  L_7 = (&V_2)->get_worldNormal_8();
		Ray__ctor_m3379034047((&V_0), L_4, L_7, /*hidden argument*/NULL);
		Canvas_t209405766 * L_8 = VRTK_UIGraphicRaycaster_get_canvas_m1893935089(__this, /*hidden argument*/NULL);
		Camera_t189460977 * L_9 = VirtFuncInvoker0< Camera_t189460977 * >::Invoke(18 /* UnityEngine.Camera UnityEngine.EventSystems.BaseRaycaster::get_eventCamera() */, __this);
		Ray_t2469606224  L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_UIGraphicRaycaster_t2816944648_il2cpp_TypeInfo_var);
		VRTK_UIGraphicRaycaster_Raycast_m2870623256(__this, L_8, L_9, L_10, (((VRTK_UIGraphicRaycaster_t2816944648_StaticFields*)VRTK_UIGraphicRaycaster_t2816944648_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_RaycastResults_13()), /*hidden argument*/NULL);
		VRTK_UIGraphicRaycaster_SetNearestRaycast_m4292847853(__this, (&___eventData0), (&___resultAppendList1), (((VRTK_UIGraphicRaycaster_t2816944648_StaticFields*)VRTK_UIGraphicRaycaster_t2816944648_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_RaycastResults_13()), /*hidden argument*/NULL);
		List_1_t3685274804 * L_11 = ((VRTK_UIGraphicRaycaster_t2816944648_StaticFields*)VRTK_UIGraphicRaycaster_t2816944648_il2cpp_TypeInfo_var->static_fields)->get_s_RaycastResults_13();
		NullCheck(L_11);
		List_1_Clear_m392100656(L_11, /*hidden argument*/List_1_Clear_m392100656_MethodInfo_var);
		return;
	}
}
// System.Void VRTK.VRTK_UIGraphicRaycaster::SetNearestRaycast(UnityEngine.EventSystems.PointerEventData&,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>&,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>&)
extern Il2CppClass* Nullable_1_t2579219987_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m3435089276_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3279745867_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m3271298471_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m2830962711_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m3769688082_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2123823603_MethodInfo_var;
extern const uint32_t VRTK_UIGraphicRaycaster_SetNearestRaycast_m4292847853_MetadataUsageId;
extern "C"  void VRTK_UIGraphicRaycaster_SetNearestRaycast_m4292847853 (VRTK_UIGraphicRaycaster_t2816944648 * __this, PointerEventData_t1599784723 ** ___eventData0, List_1_t3685274804 ** ___resultAppendList1, List_1_t3685274804 ** ___raycastResults2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIGraphicRaycaster_SetNearestRaycast_m4292847853_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t2579219987  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Nullable_1_t2579219987  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	RaycastResult_t21186376  V_3;
	memset(&V_3, 0, sizeof(V_3));
	RaycastResult_t21186376  V_4;
	memset(&V_4, 0, sizeof(V_4));
	RaycastResult_t21186376  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Initobj (Nullable_1_t2579219987_il2cpp_TypeInfo_var, (&V_1));
		Nullable_1_t2579219987  L_0 = V_1;
		V_0 = L_0;
		V_2 = 0;
		goto IL_0064;
	}

IL_0011:
	{
		List_1_t3685274804 ** L_1 = ___raycastResults2;
		int32_t L_2 = V_2;
		NullCheck((*((List_1_t3685274804 **)L_1)));
		RaycastResult_t21186376  L_3 = List_1_get_Item_m3435089276((*((List_1_t3685274804 **)L_1)), L_2, /*hidden argument*/List_1_get_Item_m3435089276_MethodInfo_var);
		V_3 = L_3;
		List_1_t3685274804 ** L_4 = ___resultAppendList1;
		NullCheck((*((List_1_t3685274804 **)L_4)));
		int32_t L_5 = List_1_get_Count_m3279745867((*((List_1_t3685274804 **)L_4)), /*hidden argument*/List_1_get_Count_m3279745867_MethodInfo_var);
		(&V_3)->set_index_3((((float)((float)L_5))));
		bool L_6 = Nullable_1_get_HasValue_m3271298471((&V_0), /*hidden argument*/Nullable_1_get_HasValue_m3271298471_MethodInfo_var);
		if (!L_6)
		{
			goto IL_0051;
		}
	}
	{
		float L_7 = (&V_3)->get_distance_2();
		RaycastResult_t21186376  L_8 = Nullable_1_get_Value_m2830962711((&V_0), /*hidden argument*/Nullable_1_get_Value_m2830962711_MethodInfo_var);
		V_4 = L_8;
		float L_9 = (&V_4)->get_distance_2();
		if ((!(((float)L_7) < ((float)L_9))))
		{
			goto IL_0058;
		}
	}

IL_0051:
	{
		RaycastResult_t21186376  L_10 = V_3;
		Nullable_1_t2579219987  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Nullable_1__ctor_m3769688082(&L_11, L_10, /*hidden argument*/Nullable_1__ctor_m3769688082_MethodInfo_var);
		V_0 = L_11;
	}

IL_0058:
	{
		List_1_t3685274804 ** L_12 = ___resultAppendList1;
		RaycastResult_t21186376  L_13 = V_3;
		NullCheck((*((List_1_t3685274804 **)L_12)));
		List_1_Add_m2123823603((*((List_1_t3685274804 **)L_12)), L_13, /*hidden argument*/List_1_Add_m2123823603_MethodInfo_var);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0064:
	{
		int32_t L_15 = V_2;
		List_1_t3685274804 ** L_16 = ___raycastResults2;
		NullCheck((*((List_1_t3685274804 **)L_16)));
		int32_t L_17 = List_1_get_Count_m3279745867((*((List_1_t3685274804 **)L_16)), /*hidden argument*/List_1_get_Count_m3279745867_MethodInfo_var);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0011;
		}
	}
	{
		bool L_18 = Nullable_1_get_HasValue_m3271298471((&V_0), /*hidden argument*/Nullable_1_get_HasValue_m3271298471_MethodInfo_var);
		if (!L_18)
		{
			goto IL_00c8;
		}
	}
	{
		PointerEventData_t1599784723 ** L_19 = ___eventData0;
		RaycastResult_t21186376  L_20 = Nullable_1_get_Value_m2830962711((&V_0), /*hidden argument*/Nullable_1_get_Value_m2830962711_MethodInfo_var);
		V_5 = L_20;
		Vector2_t2243707579  L_21 = (&V_5)->get_screenPosition_9();
		NullCheck((*((PointerEventData_t1599784723 **)L_19)));
		PointerEventData_set_position_m794507622((*((PointerEventData_t1599784723 **)L_19)), L_21, /*hidden argument*/NULL);
		PointerEventData_t1599784723 ** L_22 = ___eventData0;
		PointerEventData_t1599784723 ** L_23 = ___eventData0;
		NullCheck((*((PointerEventData_t1599784723 **)L_23)));
		Vector2_t2243707579  L_24 = PointerEventData_get_position_m2131765015((*((PointerEventData_t1599784723 **)L_23)), /*hidden argument*/NULL);
		Vector2_t2243707579  L_25 = __this->get_lastKnownPosition_11();
		Vector2_t2243707579  L_26 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		NullCheck((*((PointerEventData_t1599784723 **)L_22)));
		PointerEventData_set_delta_m3672873329((*((PointerEventData_t1599784723 **)L_22)), L_26, /*hidden argument*/NULL);
		PointerEventData_t1599784723 ** L_27 = ___eventData0;
		NullCheck((*((PointerEventData_t1599784723 **)L_27)));
		Vector2_t2243707579  L_28 = PointerEventData_get_position_m2131765015((*((PointerEventData_t1599784723 **)L_27)), /*hidden argument*/NULL);
		__this->set_lastKnownPosition_11(L_28);
		PointerEventData_t1599784723 ** L_29 = ___eventData0;
		RaycastResult_t21186376  L_30 = Nullable_1_get_Value_m2830962711((&V_0), /*hidden argument*/Nullable_1_get_Value_m2830962711_MethodInfo_var);
		NullCheck((*((PointerEventData_t1599784723 **)L_29)));
		PointerEventData_set_pointerCurrentRaycast_m2431897513((*((PointerEventData_t1599784723 **)L_29)), L_30, /*hidden argument*/NULL);
	}

IL_00c8:
	{
		return;
	}
}
// System.Single VRTK.VRTK_UIGraphicRaycaster::GetHitDistance(UnityEngine.Ray)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIGraphicRaycaster_GetHitDistance_m874130475_MetadataUsageId;
extern "C"  float VRTK_UIGraphicRaycaster_GetHitDistance_m874130475 (VRTK_UIGraphicRaycaster_t2816944648 * __this, Ray_t2469606224  ___ray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIGraphicRaycaster_GetHitDistance_m874130475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	RaycastHit_t87180320  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RaycastHit2D_t4063908774  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		V_0 = (std::numeric_limits<float>::max());
		Canvas_t209405766 * L_0 = VRTK_UIGraphicRaycaster_get_canvas_m1893935089(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Canvas_get_renderMode_m1816014618(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_2 = GraphicRaycaster_get_blockingObjects_m1447656241(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00fa;
		}
	}
	{
		Vector3_t2243707580  L_3 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		Canvas_t209405766 * L_4 = VRTK_UIGraphicRaycaster_get_canvas_m1893935089(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_position_m1104419803(L_5, /*hidden argument*/NULL);
		float L_7 = Vector3_Distance_m1859670022(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		int32_t L_8 = GraphicRaycaster_get_blockingObjects_m1447656241(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)2)))
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_9 = GraphicRaycaster_get_blockingObjects_m1447656241(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)3))))
		{
			goto IL_0090;
		}
	}

IL_0056:
	{
		Ray_t2469606224  L_10 = ___ray0;
		float L_11 = V_1;
		Physics_Raycast_m2308457076(NULL /*static, unused*/, L_10, (&V_2), L_11, /*hidden argument*/NULL);
		Collider_t3497673348 * L_12 = RaycastHit_get_collider_m301198172((&V_2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0090;
		}
	}
	{
		Collider_t3497673348 * L_14 = RaycastHit_get_collider_m301198172((&V_2), /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_t1756533147 * L_15 = Component_get_gameObject_m3105766835(L_14, /*hidden argument*/NULL);
		bool L_16 = VRTK_PlayerObject_IsPlayerObject_m1369301804(NULL /*static, unused*/, L_15, 0, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_0090;
		}
	}
	{
		float L_17 = RaycastHit_get_distance_m1178709367((&V_2), /*hidden argument*/NULL);
		V_0 = L_17;
	}

IL_0090:
	{
		int32_t L_18 = GraphicRaycaster_get_blockingObjects_m1447656241(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_18) == ((int32_t)1)))
		{
			goto IL_00a8;
		}
	}
	{
		int32_t L_19 = GraphicRaycaster_get_blockingObjects_m1447656241(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_19) == ((uint32_t)3))))
		{
			goto IL_00fa;
		}
	}

IL_00a8:
	{
		Vector3_t2243707580  L_20 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_21 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		Vector3_t2243707580  L_22 = Ray_get_direction_m4059191533((&___ray0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_23 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		float L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_25 = Physics2D_Raycast_m3913913442(NULL /*static, unused*/, L_21, L_23, L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		Collider2D_t646061738 * L_26 = RaycastHit2D_get_collider_m2568504212((&V_3), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_27 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_26, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00fa;
		}
	}
	{
		Collider2D_t646061738 * L_28 = RaycastHit2D_get_collider_m2568504212((&V_3), /*hidden argument*/NULL);
		NullCheck(L_28);
		GameObject_t1756533147 * L_29 = Component_get_gameObject_m3105766835(L_28, /*hidden argument*/NULL);
		bool L_30 = VRTK_PlayerObject_IsPlayerObject_m1369301804(NULL /*static, unused*/, L_29, 0, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_00fa;
		}
	}
	{
		float L_31 = RaycastHit2D_get_fraction_m1296150410((&V_3), /*hidden argument*/NULL);
		float L_32 = V_1;
		V_0 = ((float)((float)L_31*(float)L_32));
	}

IL_00fa:
	{
		float L_33 = V_0;
		return L_33;
	}
}
// System.Void VRTK.VRTK_UIGraphicRaycaster::Raycast(UnityEngine.Canvas,UnityEngine.Camera,UnityEngine.Ray,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>&)
extern Il2CppClass* GraphicRegistry_t377833367_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_1_t2967166177_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern Il2CppClass* RaycastResult_t21186376_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t3378300881_il2cpp_TypeInfo_var;
extern Il2CppClass* VRTK_UIGraphicRaycaster_t2816944648_il2cpp_TypeInfo_var;
extern Il2CppClass* Comparison_1_t1282925227_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m2123823603_MethodInfo_var;
extern const MethodInfo* VRTK_UIGraphicRaycaster_U3CRaycastU3Em__0_m2793431176_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m1414815602_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m107990965_MethodInfo_var;
extern const uint32_t VRTK_UIGraphicRaycaster_Raycast_m2870623256_MetadataUsageId;
extern "C"  void VRTK_UIGraphicRaycaster_Raycast_m2870623256 (VRTK_UIGraphicRaycaster_t2816944648 * __this, Canvas_t209405766 * ___canvas0, Camera_t189460977 * ___eventCamera1, Ray_t2469606224  ___ray2, List_1_t3685274804 ** ___results3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIGraphicRaycaster_Raycast_m2870623256_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Il2CppObject* V_1 = NULL;
	int32_t V_2 = 0;
	Graphic_t2426225576 * V_3 = NULL;
	Transform_t3275118058 * V_4 = NULL;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector2_t2243707579  V_8;
	memset(&V_8, 0, sizeof(V_8));
	RaycastResult_t21186376  V_9;
	memset(&V_9, 0, sizeof(V_9));
	RaycastResult_t21186376  V_10;
	memset(&V_10, 0, sizeof(V_10));
	List_1_t3685274804 * G_B16_0 = NULL;
	List_1_t3685274804 * G_B15_0 = NULL;
	{
		Ray_t2469606224  L_0 = ___ray2;
		float L_1 = VRTK_UIGraphicRaycaster_GetHitDistance_m874130475(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Canvas_t209405766 * L_2 = ___canvas0;
		IL2CPP_RUNTIME_CLASS_INIT(GraphicRegistry_t377833367_il2cpp_TypeInfo_var);
		Il2CppObject* L_3 = GraphicRegistry_GetGraphicsForCanvas_m3873480384(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		V_2 = 0;
		goto IL_014b;
	}

IL_0016:
	{
		Il2CppObject* L_4 = V_1;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		Graphic_t2426225576 * L_6 = InterfaceFuncInvoker1< Graphic_t2426225576 *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<UnityEngine.UI.Graphic>::get_Item(System.Int32) */, IList_1_t2967166177_il2cpp_TypeInfo_var, L_4, L_5);
		V_3 = L_6;
		Graphic_t2426225576 * L_7 = V_3;
		NullCheck(L_7);
		int32_t L_8 = Graphic_get_depth_m3526566553(L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)(-1))))
		{
			goto IL_0035;
		}
	}
	{
		Graphic_t2426225576 * L_9 = V_3;
		NullCheck(L_9);
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean UnityEngine.UI.Graphic::get_raycastTarget() */, L_9);
		if (L_10)
		{
			goto IL_003a;
		}
	}

IL_0035:
	{
		goto IL_0147;
	}

IL_003a:
	{
		Graphic_t2426225576 * L_11 = V_3;
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(L_11, /*hidden argument*/NULL);
		V_4 = L_12;
		Transform_t3275118058 * L_13 = V_4;
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = Transform_get_forward_m1833488937(L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		Vector3_t2243707580  L_15 = V_5;
		Transform_t3275118058 * L_16 = V_4;
		NullCheck(L_16);
		Vector3_t2243707580  L_17 = Transform_get_position_m1104419803(L_16, /*hidden argument*/NULL);
		Vector3_t2243707580  L_18 = Ray_get_origin_m3339262500((&___ray2), /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		float L_20 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_15, L_19, /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = V_5;
		Vector3_t2243707580  L_22 = Ray_get_direction_m4059191533((&___ray2), /*hidden argument*/NULL);
		float L_23 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_6 = ((float)((float)L_20/(float)L_23));
		float L_24 = V_6;
		if ((!(((float)L_24) < ((float)(0.0f)))))
		{
			goto IL_0087;
		}
	}
	{
		goto IL_0147;
	}

IL_0087:
	{
		float L_25 = V_6;
		float L_26 = V_0;
		if ((!(((float)((float)((float)L_25-(float)(1.0E-05f)))) > ((float)L_26))))
		{
			goto IL_009a;
		}
	}
	{
		goto IL_0147;
	}

IL_009a:
	{
		float L_27 = V_6;
		Vector3_t2243707580  L_28 = Ray_GetPoint_m1353702366((&___ray2), L_27, /*hidden argument*/NULL);
		V_7 = L_28;
		Camera_t189460977 * L_29 = ___eventCamera1;
		Vector3_t2243707580  L_30 = V_7;
		NullCheck(L_29);
		Vector3_t2243707580  L_31 = Camera_WorldToScreenPoint_m638747266(L_29, L_30, /*hidden argument*/NULL);
		Vector2_t2243707579  L_32 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		V_8 = L_32;
		Graphic_t2426225576 * L_33 = V_3;
		NullCheck(L_33);
		RectTransform_t3349966182 * L_34 = Graphic_get_rectTransform_m2697395074(L_33, /*hidden argument*/NULL);
		Vector2_t2243707579  L_35 = V_8;
		Camera_t189460977 * L_36 = ___eventCamera1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		bool L_37 = RectTransformUtility_RectangleContainsScreenPoint_m1244853728(NULL /*static, unused*/, L_34, L_35, L_36, /*hidden argument*/NULL);
		if (L_37)
		{
			goto IL_00cc;
		}
	}
	{
		goto IL_0147;
	}

IL_00cc:
	{
		Graphic_t2426225576 * L_38 = V_3;
		Vector2_t2243707579  L_39 = V_8;
		Camera_t189460977 * L_40 = ___eventCamera1;
		NullCheck(L_38);
		bool L_41 = VirtFuncInvoker2< bool, Vector2_t2243707579 , Camera_t189460977 * >::Invoke(45 /* System.Boolean UnityEngine.UI.Graphic::Raycast(UnityEngine.Vector2,UnityEngine.Camera) */, L_38, L_39, L_40);
		if (!L_41)
		{
			goto IL_0147;
		}
	}
	{
		Initobj (RaycastResult_t21186376_il2cpp_TypeInfo_var, (&V_10));
		Graphic_t2426225576 * L_42 = V_3;
		NullCheck(L_42);
		GameObject_t1756533147 * L_43 = Component_get_gameObject_m3105766835(L_42, /*hidden argument*/NULL);
		RaycastResult_set_gameObject_m1138581891((&V_10), L_43, /*hidden argument*/NULL);
		(&V_10)->set_module_1(__this);
		float L_44 = V_6;
		(&V_10)->set_distance_2(L_44);
		Vector2_t2243707579  L_45 = V_8;
		(&V_10)->set_screenPosition_9(L_45);
		Vector3_t2243707580  L_46 = V_7;
		(&V_10)->set_worldPosition_7(L_46);
		Graphic_t2426225576 * L_47 = V_3;
		NullCheck(L_47);
		int32_t L_48 = Graphic_get_depth_m3526566553(L_47, /*hidden argument*/NULL);
		(&V_10)->set_depth_4(L_48);
		Canvas_t209405766 * L_49 = ___canvas0;
		NullCheck(L_49);
		int32_t L_50 = Canvas_get_sortingLayerID_m1396307660(L_49, /*hidden argument*/NULL);
		(&V_10)->set_sortingLayer_5(L_50);
		Canvas_t209405766 * L_51 = ___canvas0;
		NullCheck(L_51);
		int32_t L_52 = Canvas_get_sortingOrder_m3120854436(L_51, /*hidden argument*/NULL);
		(&V_10)->set_sortingOrder_6(L_52);
		RaycastResult_t21186376  L_53 = V_10;
		V_9 = L_53;
		List_1_t3685274804 ** L_54 = ___results3;
		RaycastResult_t21186376  L_55 = V_9;
		NullCheck((*((List_1_t3685274804 **)L_54)));
		List_1_Add_m2123823603((*((List_1_t3685274804 **)L_54)), L_55, /*hidden argument*/List_1_Add_m2123823603_MethodInfo_var);
	}

IL_0147:
	{
		int32_t L_56 = V_2;
		V_2 = ((int32_t)((int32_t)L_56+(int32_t)1));
	}

IL_014b:
	{
		int32_t L_57 = V_2;
		Il2CppObject* L_58 = V_1;
		NullCheck(L_58);
		int32_t L_59 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Graphic>::get_Count() */, ICollection_1_t3378300881_il2cpp_TypeInfo_var, L_58);
		if ((((int32_t)L_57) < ((int32_t)L_59)))
		{
			goto IL_0016;
		}
	}
	{
		List_1_t3685274804 ** L_60 = ___results3;
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_UIGraphicRaycaster_t2816944648_il2cpp_TypeInfo_var);
		Comparison_1_t1282925227 * L_61 = ((VRTK_UIGraphicRaycaster_t2816944648_StaticFields*)VRTK_UIGraphicRaycaster_t2816944648_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_14();
		G_B15_0 = (*((List_1_t3685274804 **)L_60));
		if (L_61)
		{
			G_B16_0 = (*((List_1_t3685274804 **)L_60));
			goto IL_0172;
		}
	}
	{
		IntPtr_t L_62;
		L_62.set_m_value_0((void*)(void*)VRTK_UIGraphicRaycaster_U3CRaycastU3Em__0_m2793431176_MethodInfo_var);
		Comparison_1_t1282925227 * L_63 = (Comparison_1_t1282925227 *)il2cpp_codegen_object_new(Comparison_1_t1282925227_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1414815602(L_63, NULL, L_62, /*hidden argument*/Comparison_1__ctor_m1414815602_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_UIGraphicRaycaster_t2816944648_il2cpp_TypeInfo_var);
		((VRTK_UIGraphicRaycaster_t2816944648_StaticFields*)VRTK_UIGraphicRaycaster_t2816944648_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_14(L_63);
		G_B16_0 = G_B15_0;
	}

IL_0172:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_UIGraphicRaycaster_t2816944648_il2cpp_TypeInfo_var);
		Comparison_1_t1282925227 * L_64 = ((VRTK_UIGraphicRaycaster_t2816944648_StaticFields*)VRTK_UIGraphicRaycaster_t2816944648_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_14();
		NullCheck(G_B16_0);
		List_1_Sort_m107990965(G_B16_0, L_64, /*hidden argument*/List_1_Sort_m107990965_MethodInfo_var);
		return;
	}
}
// UnityEngine.Canvas VRTK.VRTK_UIGraphicRaycaster::get_canvas()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCanvas_t209405766_m195193039_MethodInfo_var;
extern const uint32_t VRTK_UIGraphicRaycaster_get_canvas_m1893935089_MetadataUsageId;
extern "C"  Canvas_t209405766 * VRTK_UIGraphicRaycaster_get_canvas_m1893935089 (VRTK_UIGraphicRaycaster_t2816944648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIGraphicRaycaster_get_canvas_m1893935089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Canvas_t209405766 * L_0 = __this->get_m_Canvas_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Canvas_t209405766 * L_2 = __this->get_m_Canvas_10();
		return L_2;
	}

IL_0018:
	{
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Canvas_t209405766 * L_4 = GameObject_GetComponent_TisCanvas_t209405766_m195193039(L_3, /*hidden argument*/GameObject_GetComponent_TisCanvas_t209405766_m195193039_MethodInfo_var);
		__this->set_m_Canvas_10(L_4);
		Canvas_t209405766 * L_5 = __this->get_m_Canvas_10();
		return L_5;
	}
}
// System.Void VRTK.VRTK_UIGraphicRaycaster::.cctor()
extern Il2CppClass* List_1_t3685274804_il2cpp_TypeInfo_var;
extern Il2CppClass* VRTK_UIGraphicRaycaster_t2816944648_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2989057823_MethodInfo_var;
extern const uint32_t VRTK_UIGraphicRaycaster__cctor_m2721502627_MetadataUsageId;
extern "C"  void VRTK_UIGraphicRaycaster__cctor_m2721502627 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIGraphicRaycaster__cctor_m2721502627_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3685274804 * L_0 = (List_1_t3685274804 *)il2cpp_codegen_object_new(List_1_t3685274804_il2cpp_TypeInfo_var);
		List_1__ctor_m2989057823(L_0, /*hidden argument*/List_1__ctor_m2989057823_MethodInfo_var);
		((VRTK_UIGraphicRaycaster_t2816944648_StaticFields*)VRTK_UIGraphicRaycaster_t2816944648_il2cpp_TypeInfo_var->static_fields)->set_s_RaycastResults_13(L_0);
		return;
	}
}
// System.Int32 VRTK.VRTK_UIGraphicRaycaster::<Raycast>m__0(UnityEngine.EventSystems.RaycastResult,UnityEngine.EventSystems.RaycastResult)
extern "C"  int32_t VRTK_UIGraphicRaycaster_U3CRaycastU3Em__0_m2793431176 (Il2CppObject * __this /* static, unused */, RaycastResult_t21186376  ___g10, RaycastResult_t21186376  ___g21, const MethodInfo* method)
{
	{
		int32_t* L_0 = (&___g21)->get_address_of_depth_4();
		int32_t L_1 = (&___g10)->get_depth_4();
		int32_t L_2 = Int32_CompareTo_m3808534558(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void VRTK.VRTK_UIPointer::.ctor()
extern "C"  void VRTK_UIPointer__ctor_m951207381 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method)
{
	{
		__this->set_activationButton_2(((int32_t)10));
		__this->set_selectionButton_4(3);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::add_UIPointerElementEnter(VRTK.UIPointerEventHandler)
extern Il2CppClass* UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_add_UIPointerElementEnter_m2528476926_MetadataUsageId;
extern "C"  void VRTK_UIPointer_add_UIPointerElementEnter_m2528476926 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_add_UIPointerElementEnter_m2528476926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIPointerEventHandler_t988663103 * V_0 = NULL;
	UIPointerEventHandler_t988663103 * V_1 = NULL;
	{
		UIPointerEventHandler_t988663103 * L_0 = __this->get_UIPointerElementEnter_17();
		V_0 = L_0;
	}

IL_0007:
	{
		UIPointerEventHandler_t988663103 * L_1 = V_0;
		V_1 = L_1;
		UIPointerEventHandler_t988663103 ** L_2 = __this->get_address_of_UIPointerElementEnter_17();
		UIPointerEventHandler_t988663103 * L_3 = V_1;
		UIPointerEventHandler_t988663103 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UIPointerEventHandler_t988663103 * L_6 = V_0;
		UIPointerEventHandler_t988663103 * L_7 = InterlockedCompareExchangeImpl<UIPointerEventHandler_t988663103 *>(L_2, ((UIPointerEventHandler_t988663103 *)CastclassSealed(L_5, UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UIPointerEventHandler_t988663103 * L_8 = V_0;
		UIPointerEventHandler_t988663103 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_8) == ((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::remove_UIPointerElementEnter(VRTK.UIPointerEventHandler)
extern Il2CppClass* UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_remove_UIPointerElementEnter_m4179690723_MetadataUsageId;
extern "C"  void VRTK_UIPointer_remove_UIPointerElementEnter_m4179690723 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_remove_UIPointerElementEnter_m4179690723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIPointerEventHandler_t988663103 * V_0 = NULL;
	UIPointerEventHandler_t988663103 * V_1 = NULL;
	{
		UIPointerEventHandler_t988663103 * L_0 = __this->get_UIPointerElementEnter_17();
		V_0 = L_0;
	}

IL_0007:
	{
		UIPointerEventHandler_t988663103 * L_1 = V_0;
		V_1 = L_1;
		UIPointerEventHandler_t988663103 ** L_2 = __this->get_address_of_UIPointerElementEnter_17();
		UIPointerEventHandler_t988663103 * L_3 = V_1;
		UIPointerEventHandler_t988663103 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UIPointerEventHandler_t988663103 * L_6 = V_0;
		UIPointerEventHandler_t988663103 * L_7 = InterlockedCompareExchangeImpl<UIPointerEventHandler_t988663103 *>(L_2, ((UIPointerEventHandler_t988663103 *)CastclassSealed(L_5, UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UIPointerEventHandler_t988663103 * L_8 = V_0;
		UIPointerEventHandler_t988663103 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_8) == ((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::add_UIPointerElementExit(VRTK.UIPointerEventHandler)
extern Il2CppClass* UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_add_UIPointerElementExit_m3973785302_MetadataUsageId;
extern "C"  void VRTK_UIPointer_add_UIPointerElementExit_m3973785302 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_add_UIPointerElementExit_m3973785302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIPointerEventHandler_t988663103 * V_0 = NULL;
	UIPointerEventHandler_t988663103 * V_1 = NULL;
	{
		UIPointerEventHandler_t988663103 * L_0 = __this->get_UIPointerElementExit_18();
		V_0 = L_0;
	}

IL_0007:
	{
		UIPointerEventHandler_t988663103 * L_1 = V_0;
		V_1 = L_1;
		UIPointerEventHandler_t988663103 ** L_2 = __this->get_address_of_UIPointerElementExit_18();
		UIPointerEventHandler_t988663103 * L_3 = V_1;
		UIPointerEventHandler_t988663103 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UIPointerEventHandler_t988663103 * L_6 = V_0;
		UIPointerEventHandler_t988663103 * L_7 = InterlockedCompareExchangeImpl<UIPointerEventHandler_t988663103 *>(L_2, ((UIPointerEventHandler_t988663103 *)CastclassSealed(L_5, UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UIPointerEventHandler_t988663103 * L_8 = V_0;
		UIPointerEventHandler_t988663103 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_8) == ((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::remove_UIPointerElementExit(VRTK.UIPointerEventHandler)
extern Il2CppClass* UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_remove_UIPointerElementExit_m2698589931_MetadataUsageId;
extern "C"  void VRTK_UIPointer_remove_UIPointerElementExit_m2698589931 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_remove_UIPointerElementExit_m2698589931_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIPointerEventHandler_t988663103 * V_0 = NULL;
	UIPointerEventHandler_t988663103 * V_1 = NULL;
	{
		UIPointerEventHandler_t988663103 * L_0 = __this->get_UIPointerElementExit_18();
		V_0 = L_0;
	}

IL_0007:
	{
		UIPointerEventHandler_t988663103 * L_1 = V_0;
		V_1 = L_1;
		UIPointerEventHandler_t988663103 ** L_2 = __this->get_address_of_UIPointerElementExit_18();
		UIPointerEventHandler_t988663103 * L_3 = V_1;
		UIPointerEventHandler_t988663103 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UIPointerEventHandler_t988663103 * L_6 = V_0;
		UIPointerEventHandler_t988663103 * L_7 = InterlockedCompareExchangeImpl<UIPointerEventHandler_t988663103 *>(L_2, ((UIPointerEventHandler_t988663103 *)CastclassSealed(L_5, UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UIPointerEventHandler_t988663103 * L_8 = V_0;
		UIPointerEventHandler_t988663103 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_8) == ((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::add_UIPointerElementClick(VRTK.UIPointerEventHandler)
extern Il2CppClass* UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_add_UIPointerElementClick_m3261397528_MetadataUsageId;
extern "C"  void VRTK_UIPointer_add_UIPointerElementClick_m3261397528 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_add_UIPointerElementClick_m3261397528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIPointerEventHandler_t988663103 * V_0 = NULL;
	UIPointerEventHandler_t988663103 * V_1 = NULL;
	{
		UIPointerEventHandler_t988663103 * L_0 = __this->get_UIPointerElementClick_19();
		V_0 = L_0;
	}

IL_0007:
	{
		UIPointerEventHandler_t988663103 * L_1 = V_0;
		V_1 = L_1;
		UIPointerEventHandler_t988663103 ** L_2 = __this->get_address_of_UIPointerElementClick_19();
		UIPointerEventHandler_t988663103 * L_3 = V_1;
		UIPointerEventHandler_t988663103 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UIPointerEventHandler_t988663103 * L_6 = V_0;
		UIPointerEventHandler_t988663103 * L_7 = InterlockedCompareExchangeImpl<UIPointerEventHandler_t988663103 *>(L_2, ((UIPointerEventHandler_t988663103 *)CastclassSealed(L_5, UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UIPointerEventHandler_t988663103 * L_8 = V_0;
		UIPointerEventHandler_t988663103 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_8) == ((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::remove_UIPointerElementClick(VRTK.UIPointerEventHandler)
extern Il2CppClass* UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_remove_UIPointerElementClick_m3345897689_MetadataUsageId;
extern "C"  void VRTK_UIPointer_remove_UIPointerElementClick_m3345897689 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_remove_UIPointerElementClick_m3345897689_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIPointerEventHandler_t988663103 * V_0 = NULL;
	UIPointerEventHandler_t988663103 * V_1 = NULL;
	{
		UIPointerEventHandler_t988663103 * L_0 = __this->get_UIPointerElementClick_19();
		V_0 = L_0;
	}

IL_0007:
	{
		UIPointerEventHandler_t988663103 * L_1 = V_0;
		V_1 = L_1;
		UIPointerEventHandler_t988663103 ** L_2 = __this->get_address_of_UIPointerElementClick_19();
		UIPointerEventHandler_t988663103 * L_3 = V_1;
		UIPointerEventHandler_t988663103 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UIPointerEventHandler_t988663103 * L_6 = V_0;
		UIPointerEventHandler_t988663103 * L_7 = InterlockedCompareExchangeImpl<UIPointerEventHandler_t988663103 *>(L_2, ((UIPointerEventHandler_t988663103 *)CastclassSealed(L_5, UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UIPointerEventHandler_t988663103 * L_8 = V_0;
		UIPointerEventHandler_t988663103 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_8) == ((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::add_UIPointerElementDragStart(VRTK.UIPointerEventHandler)
extern Il2CppClass* UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_add_UIPointerElementDragStart_m1439174518_MetadataUsageId;
extern "C"  void VRTK_UIPointer_add_UIPointerElementDragStart_m1439174518 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_add_UIPointerElementDragStart_m1439174518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIPointerEventHandler_t988663103 * V_0 = NULL;
	UIPointerEventHandler_t988663103 * V_1 = NULL;
	{
		UIPointerEventHandler_t988663103 * L_0 = __this->get_UIPointerElementDragStart_20();
		V_0 = L_0;
	}

IL_0007:
	{
		UIPointerEventHandler_t988663103 * L_1 = V_0;
		V_1 = L_1;
		UIPointerEventHandler_t988663103 ** L_2 = __this->get_address_of_UIPointerElementDragStart_20();
		UIPointerEventHandler_t988663103 * L_3 = V_1;
		UIPointerEventHandler_t988663103 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UIPointerEventHandler_t988663103 * L_6 = V_0;
		UIPointerEventHandler_t988663103 * L_7 = InterlockedCompareExchangeImpl<UIPointerEventHandler_t988663103 *>(L_2, ((UIPointerEventHandler_t988663103 *)CastclassSealed(L_5, UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UIPointerEventHandler_t988663103 * L_8 = V_0;
		UIPointerEventHandler_t988663103 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_8) == ((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::remove_UIPointerElementDragStart(VRTK.UIPointerEventHandler)
extern Il2CppClass* UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_remove_UIPointerElementDragStart_m145289803_MetadataUsageId;
extern "C"  void VRTK_UIPointer_remove_UIPointerElementDragStart_m145289803 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_remove_UIPointerElementDragStart_m145289803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIPointerEventHandler_t988663103 * V_0 = NULL;
	UIPointerEventHandler_t988663103 * V_1 = NULL;
	{
		UIPointerEventHandler_t988663103 * L_0 = __this->get_UIPointerElementDragStart_20();
		V_0 = L_0;
	}

IL_0007:
	{
		UIPointerEventHandler_t988663103 * L_1 = V_0;
		V_1 = L_1;
		UIPointerEventHandler_t988663103 ** L_2 = __this->get_address_of_UIPointerElementDragStart_20();
		UIPointerEventHandler_t988663103 * L_3 = V_1;
		UIPointerEventHandler_t988663103 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UIPointerEventHandler_t988663103 * L_6 = V_0;
		UIPointerEventHandler_t988663103 * L_7 = InterlockedCompareExchangeImpl<UIPointerEventHandler_t988663103 *>(L_2, ((UIPointerEventHandler_t988663103 *)CastclassSealed(L_5, UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UIPointerEventHandler_t988663103 * L_8 = V_0;
		UIPointerEventHandler_t988663103 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_8) == ((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::add_UIPointerElementDragEnd(VRTK.UIPointerEventHandler)
extern Il2CppClass* UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_add_UIPointerElementDragEnd_m1805642955_MetadataUsageId;
extern "C"  void VRTK_UIPointer_add_UIPointerElementDragEnd_m1805642955 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_add_UIPointerElementDragEnd_m1805642955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIPointerEventHandler_t988663103 * V_0 = NULL;
	UIPointerEventHandler_t988663103 * V_1 = NULL;
	{
		UIPointerEventHandler_t988663103 * L_0 = __this->get_UIPointerElementDragEnd_21();
		V_0 = L_0;
	}

IL_0007:
	{
		UIPointerEventHandler_t988663103 * L_1 = V_0;
		V_1 = L_1;
		UIPointerEventHandler_t988663103 ** L_2 = __this->get_address_of_UIPointerElementDragEnd_21();
		UIPointerEventHandler_t988663103 * L_3 = V_1;
		UIPointerEventHandler_t988663103 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UIPointerEventHandler_t988663103 * L_6 = V_0;
		UIPointerEventHandler_t988663103 * L_7 = InterlockedCompareExchangeImpl<UIPointerEventHandler_t988663103 *>(L_2, ((UIPointerEventHandler_t988663103 *)CastclassSealed(L_5, UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UIPointerEventHandler_t988663103 * L_8 = V_0;
		UIPointerEventHandler_t988663103 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_8) == ((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::remove_UIPointerElementDragEnd(VRTK.UIPointerEventHandler)
extern Il2CppClass* UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_remove_UIPointerElementDragEnd_m3976071480_MetadataUsageId;
extern "C"  void VRTK_UIPointer_remove_UIPointerElementDragEnd_m3976071480 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventHandler_t988663103 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_remove_UIPointerElementDragEnd_m3976071480_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIPointerEventHandler_t988663103 * V_0 = NULL;
	UIPointerEventHandler_t988663103 * V_1 = NULL;
	{
		UIPointerEventHandler_t988663103 * L_0 = __this->get_UIPointerElementDragEnd_21();
		V_0 = L_0;
	}

IL_0007:
	{
		UIPointerEventHandler_t988663103 * L_1 = V_0;
		V_1 = L_1;
		UIPointerEventHandler_t988663103 ** L_2 = __this->get_address_of_UIPointerElementDragEnd_21();
		UIPointerEventHandler_t988663103 * L_3 = V_1;
		UIPointerEventHandler_t988663103 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UIPointerEventHandler_t988663103 * L_6 = V_0;
		UIPointerEventHandler_t988663103 * L_7 = InterlockedCompareExchangeImpl<UIPointerEventHandler_t988663103 *>(L_2, ((UIPointerEventHandler_t988663103 *)CastclassSealed(L_5, UIPointerEventHandler_t988663103_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UIPointerEventHandler_t988663103 * L_8 = V_0;
		UIPointerEventHandler_t988663103 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_8) == ((Il2CppObject*)(UIPointerEventHandler_t988663103 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::OnUIPointerElementEnter(VRTK.UIPointerEventArgs)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_OnUIPointerElementEnter_m988000146_MetadataUsageId;
extern "C"  void VRTK_UIPointer_OnUIPointerElementEnter_m988000146 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventArgs_t1171985978  ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_OnUIPointerElementEnter_m988000146_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = (&___e0)->get_currentTarget_2();
		GameObject_t1756533147 * L_1 = __this->get_currentTarget_26();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		VRTK_UIPointer_ResetHoverTimer_m1372327469(__this, /*hidden argument*/NULL);
	}

IL_001d:
	{
		float L_3 = __this->get_clickAfterHoverDuration_7();
		if ((!(((float)L_3) > ((float)(0.0f)))))
		{
			goto IL_0050;
		}
	}
	{
		float L_4 = __this->get_hoverDurationTimer_13();
		if ((!(((float)L_4) <= ((float)(0.0f)))))
		{
			goto IL_0050;
		}
	}
	{
		__this->set_canClickOnHover_14((bool)1);
		float L_5 = __this->get_clickAfterHoverDuration_7();
		__this->set_hoverDurationTimer_13(L_5);
	}

IL_0050:
	{
		GameObject_t1756533147 * L_6 = (&___e0)->get_currentTarget_2();
		__this->set_currentTarget_26(L_6);
		UIPointerEventHandler_t988663103 * L_7 = __this->get_UIPointerElementEnter_17();
		if (!L_7)
		{
			goto IL_0075;
		}
	}
	{
		UIPointerEventHandler_t988663103 * L_8 = __this->get_UIPointerElementEnter_17();
		UIPointerEventArgs_t1171985978  L_9 = ___e0;
		NullCheck(L_8);
		UIPointerEventHandler_Invoke_m2700959014(L_8, __this, L_9, /*hidden argument*/NULL);
	}

IL_0075:
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::OnUIPointerElementExit(VRTK.UIPointerEventArgs)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_OnUIPointerElementExit_m2620195152_MetadataUsageId;
extern "C"  void VRTK_UIPointer_OnUIPointerElementExit_m2620195152 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventArgs_t1171985978  ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_OnUIPointerElementExit_m2620195152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = (&___e0)->get_previousTarget_3();
		GameObject_t1756533147 * L_1 = __this->get_currentTarget_26();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		VRTK_UIPointer_ResetHoverTimer_m1372327469(__this, /*hidden argument*/NULL);
	}

IL_001d:
	{
		UIPointerEventHandler_t988663103 * L_3 = __this->get_UIPointerElementExit_18();
		if (!L_3)
		{
			goto IL_006f;
		}
	}
	{
		UIPointerEventHandler_t988663103 * L_4 = __this->get_UIPointerElementExit_18();
		UIPointerEventArgs_t1171985978  L_5 = ___e0;
		NullCheck(L_4);
		UIPointerEventHandler_Invoke_m2700959014(L_4, __this, L_5, /*hidden argument*/NULL);
		bool L_6 = __this->get_attemptClickOnDeactivate_6();
		if (!L_6)
		{
			goto IL_006f;
		}
	}
	{
		bool L_7 = (&___e0)->get_isActive_1();
		if (L_7)
		{
			goto IL_006f;
		}
	}
	{
		GameObject_t1756533147 * L_8 = (&___e0)->get_previousTarget_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_006f;
		}
	}
	{
		PointerEventData_t1599784723 * L_10 = __this->get_pointerEventData_10();
		GameObject_t1756533147 * L_11 = (&___e0)->get_previousTarget_3();
		NullCheck(L_10);
		PointerEventData_set_pointerPress_m1418261989(L_10, L_11, /*hidden argument*/NULL);
	}

IL_006f:
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::OnUIPointerElementClick(VRTK.UIPointerEventArgs)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_OnUIPointerElementClick_m3395580096_MetadataUsageId;
extern "C"  void VRTK_UIPointer_OnUIPointerElementClick_m3395580096 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventArgs_t1171985978  ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_OnUIPointerElementClick_m3395580096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = (&___e0)->get_currentTarget_2();
		GameObject_t1756533147 * L_1 = __this->get_currentTarget_26();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		VRTK_UIPointer_ResetHoverTimer_m1372327469(__this, /*hidden argument*/NULL);
	}

IL_001d:
	{
		UIPointerEventHandler_t988663103 * L_3 = __this->get_UIPointerElementClick_19();
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		UIPointerEventHandler_t988663103 * L_4 = __this->get_UIPointerElementClick_19();
		UIPointerEventArgs_t1171985978  L_5 = ___e0;
		NullCheck(L_4);
		UIPointerEventHandler_Invoke_m2700959014(L_4, __this, L_5, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::OnUIPointerElementDragStart(VRTK.UIPointerEventArgs)
extern "C"  void VRTK_UIPointer_OnUIPointerElementDragStart_m3799511114 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventArgs_t1171985978  ___e0, const MethodInfo* method)
{
	{
		UIPointerEventHandler_t988663103 * L_0 = __this->get_UIPointerElementDragStart_20();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		UIPointerEventHandler_t988663103 * L_1 = __this->get_UIPointerElementDragStart_20();
		UIPointerEventArgs_t1171985978  L_2 = ___e0;
		NullCheck(L_1);
		UIPointerEventHandler_Invoke_m2700959014(L_1, __this, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::OnUIPointerElementDragEnd(VRTK.UIPointerEventArgs)
extern "C"  void VRTK_UIPointer_OnUIPointerElementDragEnd_m248063443 (VRTK_UIPointer_t2714926455 * __this, UIPointerEventArgs_t1171985978  ___e0, const MethodInfo* method)
{
	{
		UIPointerEventHandler_t988663103 * L_0 = __this->get_UIPointerElementDragEnd_21();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		UIPointerEventHandler_t988663103 * L_1 = __this->get_UIPointerElementDragEnd_21();
		UIPointerEventArgs_t1171985978  L_2 = ___e0;
		NullCheck(L_1);
		UIPointerEventHandler_Invoke_m2700959014(L_1, __this, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// VRTK.UIPointerEventArgs VRTK.VRTK_UIPointer::SetUIPointerEvent(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  UIPointerEventArgs_t1171985978  VRTK_UIPointer_SetUIPointerEvent_m4163559974 (VRTK_UIPointer_t2714926455 * __this, GameObject_t1756533147 * ___currentTarget0, GameObject_t1756533147 * ___lastTarget1, const MethodInfo* method)
{
	UIPointerEventArgs_t1171985978  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		VRTK_ControllerEvents_t3225224819 * L_0 = __this->get_controller_8();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		uint32_t L_2 = VRTK_DeviceFinder_GetControllerIndex_m601770971(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(&V_0)->set_controllerIndex_0(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean VRTK.VRTK_UIPointer::PointerActive() */, __this);
		(&V_0)->set_isActive_1(L_3);
		GameObject_t1756533147 * L_4 = ___currentTarget0;
		(&V_0)->set_currentTarget_2(L_4);
		GameObject_t1756533147 * L_5 = ___lastTarget1;
		(&V_0)->set_previousTarget_3(L_5);
		UIPointerEventArgs_t1171985978  L_6 = V_0;
		return L_6;
	}
}
// VRTK.VRTK_VRInputModule VRTK.VRTK_UIPointer::SetEventSystem(UnityEngine.EventSystems.EventSystem)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* VRTK_EventSystem_t3222336529_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisVRTK_EventSystem_t3222336529_m3671436594_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisVRTK_VRInputModule_t1472500726_m1511391868_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1205072744;
extern const uint32_t VRTK_UIPointer_SetEventSystem_m2788055360_MetadataUsageId;
extern "C"  VRTK_VRInputModule_t1472500726 * VRTK_UIPointer_SetEventSystem_m2788055360 (VRTK_UIPointer_t2714926455 * __this, EventSystem_t3466835263 * ___eventSystem0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_SetEventSystem_m2788055360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventSystem_t3466835263 * L_0 = ___eventSystem0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1205072744, /*hidden argument*/NULL);
		return (VRTK_VRInputModule_t1472500726 *)NULL;
	}

IL_0017:
	{
		EventSystem_t3466835263 * L_2 = ___eventSystem0;
		if (((VRTK_EventSystem_t3222336529 *)IsInstClass(L_2, VRTK_EventSystem_t3222336529_il2cpp_TypeInfo_var)))
		{
			goto IL_002f;
		}
	}
	{
		EventSystem_t3466835263 * L_3 = ___eventSystem0;
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		VRTK_EventSystem_t3222336529 * L_5 = GameObject_AddComponent_TisVRTK_EventSystem_t3222336529_m3671436594(L_4, /*hidden argument*/GameObject_AddComponent_TisVRTK_EventSystem_t3222336529_m3671436594_MethodInfo_var);
		___eventSystem0 = L_5;
	}

IL_002f:
	{
		EventSystem_t3466835263 * L_6 = ___eventSystem0;
		NullCheck(L_6);
		VRTK_VRInputModule_t1472500726 * L_7 = Component_GetComponent_TisVRTK_VRInputModule_t1472500726_m1511391868(L_6, /*hidden argument*/Component_GetComponent_TisVRTK_VRInputModule_t1472500726_m1511391868_MethodInfo_var);
		return L_7;
	}
}
// System.Void VRTK.VRTK_UIPointer::RemoveEventSystem()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisVRTK_EventSystem_t3222336529_m909515361_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1205072744;
extern const uint32_t VRTK_UIPointer_RemoveEventSystem_m2648293778_MetadataUsageId;
extern "C"  void VRTK_UIPointer_RemoveEventSystem_m2648293778 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_RemoveEventSystem_m2648293778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_EventSystem_t3222336529 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		VRTK_EventSystem_t3222336529 * L_0 = Object_FindObjectOfType_TisVRTK_EventSystem_t3222336529_m909515361(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVRTK_EventSystem_t3222336529_m909515361_MethodInfo_var);
		V_0 = L_0;
		VRTK_EventSystem_t3222336529 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1205072744, /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		VRTK_EventSystem_t3222336529 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VRTK.VRTK_UIPointer::PointerActive()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_PointerActive_m1525123954_MetadataUsageId;
extern "C"  bool VRTK_UIPointer_PointerActive_m1525123954 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_PointerActive_m1525123954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_activationMode_3();
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_001d;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_autoActivatingCanvas_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}

IL_001d:
	{
		return (bool)1;
	}

IL_001f:
	{
		int32_t L_3 = __this->get_activationMode_3();
		if (L_3)
		{
			goto IL_003c;
		}
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_4 = __this->get_controller_8();
		int32_t L_5 = __this->get_activationButton_2();
		NullCheck(L_4);
		bool L_6 = VRTK_ControllerEvents_IsButtonPressed_m407246062(L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_003c:
	{
		__this->set_pointerClicked_22((bool)0);
		VRTK_ControllerEvents_t3225224819 * L_7 = __this->get_controller_8();
		int32_t L_8 = __this->get_activationButton_2();
		NullCheck(L_7);
		bool L_9 = VRTK_ControllerEvents_IsButtonPressed_m407246062(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_006b;
		}
	}
	{
		bool L_10 = __this->get_lastPointerPressState_24();
		if (L_10)
		{
			goto IL_006b;
		}
	}
	{
		__this->set_pointerClicked_22((bool)1);
	}

IL_006b:
	{
		VRTK_ControllerEvents_t3225224819 * L_11 = __this->get_controller_8();
		int32_t L_12 = __this->get_activationButton_2();
		NullCheck(L_11);
		bool L_13 = VRTK_ControllerEvents_IsButtonPressed_m407246062(L_11, L_12, /*hidden argument*/NULL);
		__this->set_lastPointerPressState_24(L_13);
		bool L_14 = __this->get_pointerClicked_22();
		if (!L_14)
		{
			goto IL_009c;
		}
	}
	{
		bool L_15 = __this->get_beamEnabledState_23();
		__this->set_beamEnabledState_23((bool)((((int32_t)L_15) == ((int32_t)0))? 1 : 0));
	}

IL_009c:
	{
		bool L_16 = __this->get_beamEnabledState_23();
		return L_16;
	}
}
// System.Boolean VRTK.VRTK_UIPointer::SelectionButtonActive()
extern "C"  bool VRTK_UIPointer_SelectionButtonActive_m1429660791 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method)
{
	{
		VRTK_ControllerEvents_t3225224819 * L_0 = __this->get_controller_8();
		int32_t L_1 = __this->get_selectionButton_4();
		NullCheck(L_0);
		bool L_2 = VRTK_ControllerEvents_IsButtonPressed_m407246062(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean VRTK.VRTK_UIPointer::ValidClick(System.Boolean,System.Boolean)
extern "C"  bool VRTK_UIPointer_ValidClick_m4095485069 (VRTK_UIPointer_t2714926455 * __this, bool ___checkLastClick0, bool ___lastClickState1, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	bool G_B3_0 = false;
	int32_t G_B7_0 = 0;
	int32_t G_B9_0 = 0;
	{
		bool L_0 = __this->get_collisionClick_16();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_collisionClick_16();
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(13 /* System.Boolean VRTK.VRTK_UIPointer::SelectionButtonActive() */, __this);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		V_0 = G_B3_0;
		bool L_3 = ___checkLastClick0;
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		bool L_5 = __this->get_lastPointerClickState_25();
		bool L_6 = ___lastClickState1;
		G_B7_0 = ((((int32_t)L_5) == ((int32_t)L_6))? 1 : 0);
		goto IL_0035;
	}

IL_0034:
	{
		G_B7_0 = 0;
	}

IL_0035:
	{
		G_B9_0 = G_B7_0;
		goto IL_003b;
	}

IL_003a:
	{
		bool L_7 = V_0;
		G_B9_0 = ((int32_t)(L_7));
	}

IL_003b:
	{
		V_1 = (bool)G_B9_0;
		bool L_8 = V_0;
		__this->set_lastPointerClickState_25(L_8);
		bool L_9 = V_1;
		return L_9;
	}
}
// UnityEngine.Vector3 VRTK.VRTK_UIPointer::GetOriginPosition()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_GetOriginPosition_m1573724116_MetadataUsageId;
extern "C"  Vector3_t2243707580  VRTK_UIPointer_GetOriginPosition_m1573724116 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_GetOriginPosition_m1573724116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		Transform_t3275118058 * L_0 = __this->get_pointerOriginTransform_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		Transform_t3275118058 * L_2 = __this->get_pointerOriginTransform_9();
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002b;
	}

IL_0020:
	{
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// UnityEngine.Vector3 VRTK.VRTK_UIPointer::GetOriginForward()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_GetOriginForward_m878658030_MetadataUsageId;
extern "C"  Vector3_t2243707580  VRTK_UIPointer_GetOriginForward_m878658030 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_GetOriginForward_m878658030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		Transform_t3275118058 * L_0 = __this->get_pointerOriginTransform_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		Transform_t3275118058 * L_2 = __this->get_pointerOriginTransform_9();
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_forward_m1833488937(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002b;
	}

IL_0020:
	{
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_forward_m1833488937(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Void VRTK.VRTK_UIPointer::OnEnable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisVRTK_ControllerEvents_t3225224819_m526559481_MethodInfo_var;
extern const uint32_t VRTK_UIPointer_OnEnable_m2039416421_MetadataUsageId;
extern "C"  void VRTK_UIPointer_OnEnable_m2039416421 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_OnEnable_m2039416421_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_UIPointer_t2714926455 * G_B2_0 = NULL;
	VRTK_UIPointer_t2714926455 * G_B1_0 = NULL;
	Transform_t3275118058 * G_B3_0 = NULL;
	VRTK_UIPointer_t2714926455 * G_B3_1 = NULL;
	{
		Transform_t3275118058 * L_0 = __this->get_pointerOriginTransform_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_1)
		{
			G_B2_0 = __this;
			goto IL_0022;
		}
	}
	{
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var);
		Transform_t3275118058 * L_3 = VRTK_SDK_Bridge_GenerateControllerPointerOrigin_m3971638110(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_0028;
	}

IL_0022:
	{
		Transform_t3275118058 * L_4 = __this->get_pointerOriginTransform_9();
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_0028:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_pointerOriginTransform_9(G_B3_0);
		VRTK_ControllerEvents_t3225224819 * L_5 = __this->get_controller_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004a;
		}
	}
	{
		VRTK_ControllerEvents_t3225224819 * L_7 = Component_GetComponent_TisVRTK_ControllerEvents_t3225224819_m526559481(__this, /*hidden argument*/Component_GetComponent_TisVRTK_ControllerEvents_t3225224819_m526559481_MethodInfo_var);
		__this->set_controller_8(L_7);
	}

IL_004a:
	{
		VRTK_UIPointer_ConfigureEventSystem_m3931862010(__this, /*hidden argument*/NULL);
		__this->set_pointerClicked_22((bool)0);
		__this->set_lastPointerPressState_24((bool)0);
		__this->set_lastPointerClickState_25((bool)0);
		__this->set_beamEnabledState_23((bool)0);
		VRTK_ControllerEvents_t3225224819 * L_8 = __this->get_controller_8();
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_10 = VRTK_SDK_Bridge_GetControllerRenderModel_m3663027324(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		__this->set_controllerRenderModel_12(L_10);
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::OnDisable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Contains_m2379298033_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m2124300734_MethodInfo_var;
extern const uint32_t VRTK_UIPointer_OnDisable_m1810105220_MetadataUsageId;
extern "C"  void VRTK_UIPointer_OnDisable_m1810105220 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_OnDisable_m1810105220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VRTK_VRInputModule_t1472500726 * L_0 = __this->get_cachedVRInputModule_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		VRTK_VRInputModule_t1472500726 * L_2 = __this->get_cachedVRInputModule_28();
		NullCheck(L_2);
		List_1_t2084047587 * L_3 = L_2->get_pointers_14();
		NullCheck(L_3);
		bool L_4 = List_1_Contains_m2379298033(L_3, __this, /*hidden argument*/List_1_Contains_m2379298033_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		VRTK_VRInputModule_t1472500726 * L_5 = __this->get_cachedVRInputModule_28();
		NullCheck(L_5);
		List_1_t2084047587 * L_6 = L_5->get_pointers_14();
		NullCheck(L_6);
		List_1_Remove_m2124300734(L_6, __this, /*hidden argument*/List_1_Remove_m2124300734_MethodInfo_var);
	}

IL_0038:
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::ResetHoverTimer()
extern "C"  void VRTK_UIPointer_ResetHoverTimer_m1372327469 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method)
{
	{
		__this->set_hoverDurationTimer_13((0.0f));
		__this->set_canClickOnHover_14((bool)0);
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer::ConfigureEventSystem()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* PointerEventData_t1599784723_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisEventSystem_t3466835263_m3846439091_MethodInfo_var;
extern const MethodInfo* List_1_Contains_m2379298033_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1256959287_MethodInfo_var;
extern const uint32_t VRTK_UIPointer_ConfigureEventSystem_m3931862010_MetadataUsageId;
extern "C"  void VRTK_UIPointer_ConfigureEventSystem_m3931862010 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_ConfigureEventSystem_m3931862010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventSystem_t3466835263 * L_0 = __this->get_cachedEventSystem_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		EventSystem_t3466835263 * L_2 = Object_FindObjectOfType_TisEventSystem_t3466835263_m3846439091(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisEventSystem_t3466835263_m3846439091_MethodInfo_var);
		__this->set_cachedEventSystem_27(L_2);
	}

IL_001b:
	{
		VRTK_VRInputModule_t1472500726 * L_3 = __this->get_cachedVRInputModule_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003d;
		}
	}
	{
		EventSystem_t3466835263 * L_5 = __this->get_cachedEventSystem_27();
		VRTK_VRInputModule_t1472500726 * L_6 = VirtFuncInvoker1< VRTK_VRInputModule_t1472500726 *, EventSystem_t3466835263 * >::Invoke(10 /* VRTK.VRTK_VRInputModule VRTK.VRTK_UIPointer::SetEventSystem(UnityEngine.EventSystems.EventSystem) */, __this, L_5);
		__this->set_cachedVRInputModule_28(L_6);
	}

IL_003d:
	{
		EventSystem_t3466835263 * L_7 = __this->get_cachedEventSystem_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00ad;
		}
	}
	{
		VRTK_VRInputModule_t1472500726 * L_9 = __this->get_cachedVRInputModule_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00ad;
		}
	}
	{
		PointerEventData_t1599784723 * L_11 = __this->get_pointerEventData_10();
		if (L_11)
		{
			goto IL_0079;
		}
	}
	{
		EventSystem_t3466835263 * L_12 = __this->get_cachedEventSystem_27();
		PointerEventData_t1599784723 * L_13 = (PointerEventData_t1599784723 *)il2cpp_codegen_object_new(PointerEventData_t1599784723_il2cpp_TypeInfo_var);
		PointerEventData__ctor_m3674067728(L_13, L_12, /*hidden argument*/NULL);
		__this->set_pointerEventData_10(L_13);
	}

IL_0079:
	{
		Il2CppObject * L_14 = VRTK_UIPointer_WaitForPointerId_m1975289061(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_14, /*hidden argument*/NULL);
		VRTK_VRInputModule_t1472500726 * L_15 = __this->get_cachedVRInputModule_28();
		NullCheck(L_15);
		List_1_t2084047587 * L_16 = L_15->get_pointers_14();
		NullCheck(L_16);
		bool L_17 = List_1_Contains_m2379298033(L_16, __this, /*hidden argument*/List_1_Contains_m2379298033_MethodInfo_var);
		if (L_17)
		{
			goto IL_00ad;
		}
	}
	{
		VRTK_VRInputModule_t1472500726 * L_18 = __this->get_cachedVRInputModule_28();
		NullCheck(L_18);
		List_1_t2084047587 * L_19 = L_18->get_pointers_14();
		NullCheck(L_19);
		List_1_Add_m1256959287(L_19, __this, /*hidden argument*/List_1_Add_m1256959287_MethodInfo_var);
	}

IL_00ad:
	{
		return;
	}
}
// System.Collections.IEnumerator VRTK.VRTK_UIPointer::WaitForPointerId()
extern Il2CppClass* U3CWaitForPointerIdU3Ec__Iterator0_t2911062424_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_UIPointer_WaitForPointerId_m1975289061_MetadataUsageId;
extern "C"  Il2CppObject * VRTK_UIPointer_WaitForPointerId_m1975289061 (VRTK_UIPointer_t2714926455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointer_WaitForPointerId_m1975289061_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * V_0 = NULL;
	{
		U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * L_0 = (U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 *)il2cpp_codegen_object_new(U3CWaitForPointerIdU3Ec__Iterator0_t2911062424_il2cpp_TypeInfo_var);
		U3CWaitForPointerIdU3Ec__Iterator0__ctor_m1634011407(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * L_2 = V_0;
		return L_2;
	}
}
// System.Void VRTK.VRTK_UIPointer/<WaitForPointerId>c__Iterator0::.ctor()
extern "C"  void U3CWaitForPointerIdU3Ec__Iterator0__ctor_m1634011407 (U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VRTK.VRTK_UIPointer/<WaitForPointerId>c__Iterator0::MoveNext()
extern Il2CppClass* VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForPointerIdU3Ec__Iterator0_MoveNext_m3214135897_MetadataUsageId;
extern "C"  bool U3CWaitForPointerIdU3Ec__Iterator0_MoveNext_m3214135897 (U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForPointerIdU3Ec__Iterator0_MoveNext_m3214135897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0077;
		}
	}
	{
		goto IL_00b0;
	}

IL_0021:
	{
		VRTK_UIPointer_t2714926455 * L_2 = __this->get_U24this_1();
		NullCheck(L_2);
		VRTK_ControllerEvents_t3225224819 * L_3 = L_2->get_controller_8();
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var);
		uint32_t L_5 = VRTK_SDK_Bridge_GetControllerIndex_m2377145161(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_U3CindexU3E__0_0(L_5);
		goto IL_0077;
	}

IL_0041:
	{
		VRTK_UIPointer_t2714926455 * L_6 = __this->get_U24this_1();
		NullCheck(L_6);
		VRTK_ControllerEvents_t3225224819 * L_7 = L_6->get_controller_8();
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var);
		uint32_t L_9 = VRTK_SDK_Bridge_GetControllerIndex_m2377145161(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->set_U3CindexU3E__0_0(L_9);
		__this->set_U24current_2(NULL);
		bool L_10 = __this->get_U24disposing_3();
		if (L_10)
		{
			goto IL_0072;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0072:
	{
		goto IL_00b2;
	}

IL_0077:
	{
		int32_t L_11 = __this->get_U3CindexU3E__0_0();
		if ((((int32_t)L_11) < ((int32_t)0)))
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_12 = __this->get_U3CindexU3E__0_0();
		if ((((int32_t)L_12) == ((int32_t)((int32_t)2147483647LL))))
		{
			goto IL_0041;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_13 = __this->get_U24this_1();
		NullCheck(L_13);
		PointerEventData_t1599784723 * L_14 = L_13->get_pointerEventData_10();
		int32_t L_15 = __this->get_U3CindexU3E__0_0();
		NullCheck(L_14);
		PointerEventData_set_pointerId_m2349910516(L_14, L_15, /*hidden argument*/NULL);
		__this->set_U24PC_4((-1));
	}

IL_00b0:
	{
		return (bool)0;
	}

IL_00b2:
	{
		return (bool)1;
	}
}
// System.Object VRTK.VRTK_UIPointer/<WaitForPointerId>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForPointerIdU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1264197277 (U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object VRTK.VRTK_UIPointer/<WaitForPointerId>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForPointerIdU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1953379669 (U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void VRTK.VRTK_UIPointer/<WaitForPointerId>c__Iterator0::Dispose()
extern "C"  void U3CWaitForPointerIdU3Ec__Iterator0_Dispose_m1763776926 (U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void VRTK.VRTK_UIPointer/<WaitForPointerId>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForPointerIdU3Ec__Iterator0_Reset_m841313512_MetadataUsageId;
extern "C"  void U3CWaitForPointerIdU3Ec__Iterator0_Reset_m841313512 (U3CWaitForPointerIdU3Ec__Iterator0_t2911062424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForPointerIdU3Ec__Iterator0_Reset_m841313512_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void VRTK.VRTK_UIPointerAutoActivator::.ctor()
extern "C"  void VRTK_UIPointerAutoActivator__ctor_m2947574927 (VRTK_UIPointerAutoActivator_t1282719345 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_UIPointerAutoActivator::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInParent_TisVRTK_PlayerObject_t502441292_m2727225383_MethodInfo_var;
extern const MethodInfo* Component_GetComponentInParent_TisVRTK_UIPointer_t2714926455_m56220952_MethodInfo_var;
extern const uint32_t VRTK_UIPointerAutoActivator_OnTriggerEnter_m2629940323_MetadataUsageId;
extern "C"  void VRTK_UIPointerAutoActivator_OnTriggerEnter_m2629940323 (VRTK_UIPointerAutoActivator_t1282719345 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointerAutoActivator_OnTriggerEnter_m2629940323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_PlayerObject_t502441292 * V_0 = NULL;
	VRTK_UIPointer_t2714926455 * V_1 = NULL;
	{
		Collider_t3497673348 * L_0 = ___collider0;
		NullCheck(L_0);
		VRTK_PlayerObject_t502441292 * L_1 = Component_GetComponentInParent_TisVRTK_PlayerObject_t502441292_m2727225383(L_0, /*hidden argument*/Component_GetComponentInParent_TisVRTK_PlayerObject_t502441292_m2727225383_MethodInfo_var);
		V_0 = L_1;
		Collider_t3497673348 * L_2 = ___collider0;
		NullCheck(L_2);
		VRTK_UIPointer_t2714926455 * L_3 = Component_GetComponentInParent_TisVRTK_UIPointer_t2714926455_m56220952(L_2, /*hidden argument*/Component_GetComponentInParent_TisVRTK_UIPointer_t2714926455_m56220952_MethodInfo_var);
		V_1 = L_3;
		VRTK_UIPointer_t2714926455 * L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		VRTK_PlayerObject_t502441292 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		VRTK_PlayerObject_t502441292 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_objectType_2();
		if ((!(((uint32_t)L_9) == ((uint32_t)6))))
		{
			goto IL_003c;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_10 = V_1;
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		L_10->set_autoActivatingCanvas_15(L_11);
	}

IL_003c:
	{
		return;
	}
}
// System.Void VRTK.VRTK_UIPointerAutoActivator::OnTriggerExit(UnityEngine.Collider)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInParent_TisVRTK_UIPointer_t2714926455_m56220952_MethodInfo_var;
extern const uint32_t VRTK_UIPointerAutoActivator_OnTriggerExit_m3837674671_MetadataUsageId;
extern "C"  void VRTK_UIPointerAutoActivator_OnTriggerExit_m3837674671 (VRTK_UIPointerAutoActivator_t1282719345 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_UIPointerAutoActivator_OnTriggerExit_m3837674671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_UIPointer_t2714926455 * V_0 = NULL;
	{
		Collider_t3497673348 * L_0 = ___collider0;
		NullCheck(L_0);
		VRTK_UIPointer_t2714926455 * L_1 = Component_GetComponentInParent_TisVRTK_UIPointer_t2714926455_m56220952(L_0, /*hidden argument*/Component_GetComponentInParent_TisVRTK_UIPointer_t2714926455_m56220952_MethodInfo_var);
		V_0 = L_1;
		VRTK_UIPointer_t2714926455 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_4 = V_0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = L_4->get_autoActivatingCanvas_15();
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_002f;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_8 = V_0;
		NullCheck(L_8);
		L_8->set_autoActivatingCanvas_15((GameObject_t1756533147 *)NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void VRTK.VRTK_VRInputModule::.ctor()
extern Il2CppClass* List_1_t2084047587_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m585628939_MethodInfo_var;
extern const uint32_t VRTK_VRInputModule__ctor_m2020183358_MetadataUsageId;
extern "C"  void VRTK_VRInputModule__ctor_m2020183358 (VRTK_VRInputModule_t1472500726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_VRInputModule__ctor_m2020183358_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2084047587 * L_0 = (List_1_t2084047587 *)il2cpp_codegen_object_new(List_1_t2084047587_il2cpp_TypeInfo_var);
		List_1__ctor_m585628939(L_0, /*hidden argument*/List_1__ctor_m585628939_MethodInfo_var);
		__this->set_pointers_14(L_0);
		PointerInputModule__ctor_m3738792102(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_VRInputModule::Initialise()
extern const MethodInfo* List_1_Clear_m668253194_MethodInfo_var;
extern const uint32_t VRTK_VRInputModule_Initialise_m2688476575_MetadataUsageId;
extern "C"  void VRTK_VRInputModule_Initialise_m2688476575 (VRTK_VRInputModule_t1472500726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_VRInputModule_Initialise_m2688476575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2084047587 * L_0 = __this->get_pointers_14();
		NullCheck(L_0);
		List_1_Clear_m668253194(L_0, /*hidden argument*/List_1_Clear_m668253194_MethodInfo_var);
		return;
	}
}
// System.Boolean VRTK.VRTK_VRInputModule::IsModuleSupported()
extern "C"  bool VRTK_VRInputModule_IsModuleSupported_m851130246 (VRTK_VRInputModule_t1472500726 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void VRTK.VRTK_VRInputModule::Process()
extern Il2CppClass* List_1_t3685274804_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m4073158208_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2989057823_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m753006419_MethodInfo_var;
extern const uint32_t VRTK_VRInputModule_Process_m2081487893_MetadataUsageId;
extern "C"  void VRTK_VRInputModule_Process_m2081487893 (VRTK_VRInputModule_t1472500726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_VRInputModule_Process_m2081487893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	VRTK_UIPointer_t2714926455 * V_1 = NULL;
	List_1_t3685274804 * V_2 = NULL;
	{
		V_0 = 0;
		goto IL_006c;
	}

IL_0007:
	{
		List_1_t2084047587 * L_0 = __this->get_pointers_14();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		VRTK_UIPointer_t2714926455 * L_2 = List_1_get_Item_m4073158208(L_0, L_1, /*hidden argument*/List_1_get_Item_m4073158208_MethodInfo_var);
		V_1 = L_2;
		VRTK_UIPointer_t2714926455 * L_3 = V_1;
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = GameObject_get_activeInHierarchy_m4242915935(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0068;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_6 = V_1;
		NullCheck(L_6);
		bool L_7 = Behaviour_get_enabled_m4079055610(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0068;
		}
	}
	{
		List_1_t3685274804 * L_8 = (List_1_t3685274804 *)il2cpp_codegen_object_new(List_1_t3685274804_il2cpp_TypeInfo_var);
		List_1__ctor_m2989057823(L_8, /*hidden argument*/List_1__ctor_m2989057823_MethodInfo_var);
		V_2 = L_8;
		VRTK_UIPointer_t2714926455 * L_9 = V_1;
		NullCheck(L_9);
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean VRTK.VRTK_UIPointer::PointerActive() */, L_9);
		if (!L_10)
		{
			goto IL_0048;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_11 = V_1;
		List_1_t3685274804 * L_12 = VRTK_VRInputModule_CheckRaycasts_m926584408(__this, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
	}

IL_0048:
	{
		VRTK_UIPointer_t2714926455 * L_13 = V_1;
		List_1_t3685274804 * L_14 = V_2;
		VRTK_VRInputModule_Hover_m1423269845(__this, L_13, L_14, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_15 = V_1;
		List_1_t3685274804 * L_16 = V_2;
		VRTK_VRInputModule_Click_m1795054129(__this, L_15, L_16, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_17 = V_1;
		List_1_t3685274804 * L_18 = V_2;
		VRTK_VRInputModule_Drag_m3103883503(__this, L_17, L_18, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_19 = V_1;
		List_1_t3685274804 * L_20 = V_2;
		VRTK_VRInputModule_Scroll_m982370564(__this, L_19, L_20, /*hidden argument*/NULL);
	}

IL_0068:
	{
		int32_t L_21 = V_0;
		V_0 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_006c:
	{
		int32_t L_22 = V_0;
		List_1_t2084047587 * L_23 = __this->get_pointers_14();
		NullCheck(L_23);
		int32_t L_24 = List_1_get_Count_m753006419(L_23, /*hidden argument*/List_1_get_Count_m753006419_MethodInfo_var);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> VRTK.VRTK_VRInputModule::CheckRaycasts(VRTK.VRTK_UIPointer)
extern Il2CppClass* RaycastResult_t21186376_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3685274804_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2989057823_MethodInfo_var;
extern const uint32_t VRTK_VRInputModule_CheckRaycasts_m926584408_MetadataUsageId;
extern "C"  List_1_t3685274804 * VRTK_VRInputModule_CheckRaycasts_m926584408 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_VRInputModule_CheckRaycasts_m926584408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastResult_t21186376  V_0;
	memset(&V_0, 0, sizeof(V_0));
	List_1_t3685274804 * V_1 = NULL;
	{
		Initobj (RaycastResult_t21186376_il2cpp_TypeInfo_var, (&V_0));
		VRTK_UIPointer_t2714926455 * L_0 = ___pointer0;
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = VirtFuncInvoker0< Vector3_t2243707580  >::Invoke(15 /* UnityEngine.Vector3 VRTK.VRTK_UIPointer::GetOriginPosition() */, L_0);
		(&V_0)->set_worldPosition_7(L_1);
		VRTK_UIPointer_t2714926455 * L_2 = ___pointer0;
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = VirtFuncInvoker0< Vector3_t2243707580  >::Invoke(16 /* UnityEngine.Vector3 VRTK.VRTK_UIPointer::GetOriginForward() */, L_2);
		(&V_0)->set_worldNormal_8(L_3);
		VRTK_UIPointer_t2714926455 * L_4 = ___pointer0;
		NullCheck(L_4);
		PointerEventData_t1599784723 * L_5 = L_4->get_pointerEventData_10();
		RaycastResult_t21186376  L_6 = V_0;
		NullCheck(L_5);
		PointerEventData_set_pointerCurrentRaycast_m2431897513(L_5, L_6, /*hidden argument*/NULL);
		List_1_t3685274804 * L_7 = (List_1_t3685274804 *)il2cpp_codegen_object_new(List_1_t3685274804_il2cpp_TypeInfo_var);
		List_1__ctor_m2989057823(L_7, /*hidden argument*/List_1__ctor_m2989057823_MethodInfo_var);
		V_1 = L_7;
		EventSystem_t3466835263 * L_8 = BaseInputModule_get_eventSystem_m2822730343(__this, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_9 = ___pointer0;
		NullCheck(L_9);
		PointerEventData_t1599784723 * L_10 = L_9->get_pointerEventData_10();
		List_1_t3685274804 * L_11 = V_1;
		NullCheck(L_8);
		EventSystem_RaycastAll_m4000413739(L_8, L_10, L_11, /*hidden argument*/NULL);
		List_1_t3685274804 * L_12 = V_1;
		return L_12;
	}
}
// System.Boolean VRTK.VRTK_VRInputModule::CheckTransformTree(UnityEngine.Transform,UnityEngine.Transform)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_VRInputModule_CheckTransformTree_m2627734320_MetadataUsageId;
extern "C"  bool VRTK_VRInputModule_CheckTransformTree_m2627734320 (VRTK_VRInputModule_t1472500726 * __this, Transform_t3275118058 * ___target0, Transform_t3275118058 * ___source1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_VRInputModule_CheckTransformTree_m2627734320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		Transform_t3275118058 * L_2 = ___target0;
		Transform_t3275118058 * L_3 = ___source1;
		NullCheck(L_2);
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_2, L_3);
		if (!L_4)
		{
			goto IL_001c;
		}
	}
	{
		return (bool)1;
	}

IL_001c:
	{
		Transform_t3275118058 * L_5 = ___target0;
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = Transform_get_parent_m147407266(L_6, /*hidden argument*/NULL);
		Transform_t3275118058 * L_8 = ___source1;
		bool L_9 = VRTK_VRInputModule_CheckTransformTree_m2627734320(__this, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean VRTK.VRTK_VRInputModule::NoValidCollision(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern const MethodInfo* List_1_get_Count_m3279745867_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3435089276_MethodInfo_var;
extern const uint32_t VRTK_VRInputModule_NoValidCollision_m3464415866_MetadataUsageId;
extern "C"  bool VRTK_VRInputModule_NoValidCollision_m3464415866 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_VRInputModule_NoValidCollision_m3464415866_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastResult_t21186376  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B3_0 = 0;
	{
		List_1_t3685274804 * L_0 = ___results1;
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m3279745867(L_0, /*hidden argument*/List_1_get_Count_m3279745867_MethodInfo_var);
		if (!L_1)
		{
			goto IL_003a;
		}
	}
	{
		List_1_t3685274804 * L_2 = ___results1;
		NullCheck(L_2);
		RaycastResult_t21186376  L_3 = List_1_get_Item_m3435089276(L_2, 0, /*hidden argument*/List_1_get_Item_m3435089276_MethodInfo_var);
		V_0 = L_3;
		GameObject_t1756533147 * L_4 = RaycastResult_get_gameObject_m2999022658((&V_0), /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_6 = ___pointer0;
		NullCheck(L_6);
		PointerEventData_t1599784723 * L_7 = L_6->get_pointerEventData_10();
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = PointerEventData_get_pointerEnter_m2114522773(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		bool L_10 = VRTK_VRInputModule_CheckTransformTree_m2627734320(__this, L_5, L_9, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_10) == ((int32_t)0))? 1 : 0);
		goto IL_003b;
	}

IL_003a:
	{
		G_B3_0 = 1;
	}

IL_003b:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean VRTK.VRTK_VRInputModule::IsHovering(VRTK.VRTK_UIPointer)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m970439620_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2389888842_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3635305532_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1242097970_MethodInfo_var;
extern const uint32_t VRTK_VRInputModule_IsHovering_m2892594038_MetadataUsageId;
extern "C"  bool VRTK_VRInputModule_IsHovering_m2892594038 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_VRInputModule_IsHovering_m2892594038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Enumerator_t660383953  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		VRTK_UIPointer_t2714926455 * L_0 = ___pointer0;
		NullCheck(L_0);
		PointerEventData_t1599784723 * L_1 = L_0->get_pointerEventData_10();
		NullCheck(L_1);
		List_1_t1125654279 * L_2 = L_1->get_hovered_9();
		NullCheck(L_2);
		Enumerator_t660383953  L_3 = List_1_GetEnumerator_m970439620(L_2, /*hidden argument*/List_1_GetEnumerator_m970439620_MethodInfo_var);
		V_1 = L_3;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0066;
		}

IL_0016:
		{
			GameObject_t1756533147 * L_4 = Enumerator_get_Current_m2389888842((&V_1), /*hidden argument*/Enumerator_get_Current_m2389888842_MethodInfo_var);
			V_0 = L_4;
			VRTK_UIPointer_t2714926455 * L_5 = ___pointer0;
			NullCheck(L_5);
			PointerEventData_t1599784723 * L_6 = L_5->get_pointerEventData_10();
			NullCheck(L_6);
			GameObject_t1756533147 * L_7 = PointerEventData_get_pointerEnter_m2114522773(L_6, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_8 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_0066;
			}
		}

IL_0033:
		{
			GameObject_t1756533147 * L_9 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_10 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
			if (!L_10)
			{
				goto IL_0066;
			}
		}

IL_003e:
		{
			GameObject_t1756533147 * L_11 = V_0;
			NullCheck(L_11);
			Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
			VRTK_UIPointer_t2714926455 * L_13 = ___pointer0;
			NullCheck(L_13);
			PointerEventData_t1599784723 * L_14 = L_13->get_pointerEventData_10();
			NullCheck(L_14);
			GameObject_t1756533147 * L_15 = PointerEventData_get_pointerEnter_m2114522773(L_14, /*hidden argument*/NULL);
			NullCheck(L_15);
			Transform_t3275118058 * L_16 = GameObject_get_transform_m909382139(L_15, /*hidden argument*/NULL);
			bool L_17 = VRTK_VRInputModule_CheckTransformTree_m2627734320(__this, L_12, L_16, /*hidden argument*/NULL);
			if (!L_17)
			{
				goto IL_0066;
			}
		}

IL_005f:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x87, FINALLY_0077);
		}

IL_0066:
		{
			bool L_18 = Enumerator_MoveNext_m3635305532((&V_1), /*hidden argument*/Enumerator_MoveNext_m3635305532_MethodInfo_var);
			if (L_18)
			{
				goto IL_0016;
			}
		}

IL_0072:
		{
			IL2CPP_LEAVE(0x85, FINALLY_0077);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0077;
	}

FINALLY_0077:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1242097970((&V_1), /*hidden argument*/Enumerator_Dispose_m1242097970_MethodInfo_var);
		IL2CPP_END_FINALLY(119)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(119)
	{
		IL2CPP_JUMP_TBL(0x87, IL_0087)
		IL2CPP_JUMP_TBL(0x85, IL_0085)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0085:
	{
		return (bool)0;
	}

IL_0087:
	{
		bool L_19 = V_2;
		return L_19;
	}
}
// System.Boolean VRTK.VRTK_VRInputModule::ValidElement(UnityEngine.GameObject)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentInParent_TisVRTK_UICanvas_t1283311654_m2879938029_MethodInfo_var;
extern const uint32_t VRTK_VRInputModule_ValidElement_m1326706494_MetadataUsageId;
extern "C"  bool VRTK_VRInputModule_ValidElement_m1326706494 (VRTK_VRInputModule_t1472500726 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_VRInputModule_ValidElement_m1326706494_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_UICanvas_t1283311654 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	{
		GameObject_t1756533147 * L_0 = ___obj0;
		NullCheck(L_0);
		VRTK_UICanvas_t1283311654 * L_1 = GameObject_GetComponentInParent_TisVRTK_UICanvas_t1283311654_m2879938029(L_0, /*hidden argument*/GameObject_GetComponentInParent_TisVRTK_UICanvas_t1283311654_m2879938029_MethodInfo_var);
		V_0 = L_1;
		VRTK_UICanvas_t1283311654 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		VRTK_UICanvas_t1283311654 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = Behaviour_get_enabled_m4079055610(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0023;
		}
	}
	{
		G_B4_0 = 1;
		goto IL_0024;
	}

IL_0023:
	{
		G_B4_0 = 0;
	}

IL_0024:
	{
		return (bool)G_B4_0;
	}
}
// System.Void VRTK.VRTK_VRInputModule::CheckPointerHoverClick(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  void VRTK_VRInputModule_CheckPointerHoverClick_m1182551694 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method)
{
	{
		VRTK_UIPointer_t2714926455 * L_0 = ___pointer0;
		NullCheck(L_0);
		float L_1 = L_0->get_hoverDurationTimer_13();
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_2 = ___pointer0;
		VRTK_UIPointer_t2714926455 * L_3 = L_2;
		NullCheck(L_3);
		float L_4 = L_3->get_hoverDurationTimer_13();
		float L_5 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_hoverDurationTimer_13(((float)((float)L_4-(float)L_5)));
	}

IL_0022:
	{
		VRTK_UIPointer_t2714926455 * L_6 = ___pointer0;
		NullCheck(L_6);
		bool L_7 = L_6->get_canClickOnHover_14();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_8 = ___pointer0;
		NullCheck(L_8);
		float L_9 = L_8->get_hoverDurationTimer_13();
		if ((!(((float)L_9) <= ((float)(0.0f)))))
		{
			goto IL_004d;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_10 = ___pointer0;
		NullCheck(L_10);
		L_10->set_canClickOnHover_14((bool)0);
		VRTK_UIPointer_t2714926455 * L_11 = ___pointer0;
		List_1_t3685274804 * L_12 = ___results1;
		VRTK_VRInputModule_ClickOnDown_m903340057(__this, L_11, L_12, (bool)1, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return;
	}
}
// System.Void VRTK.VRTK_VRInputModule::Hover(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteEvents_t1693084770_il2cpp_TypeInfo_var;
extern Il2CppClass* Navigation_t1571958496_il2cpp_TypeInfo_var;
extern const MethodInfo* ExecuteEvents_ExecuteHierarchy_TisIPointerExitHandler_t461019860_m300279158_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m2287078133_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2972492689_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4066973001_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_ExecuteHierarchy_TisIPointerEnterHandler_t193164956_m1445871235_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSelectable_t1490392188_m1495814632_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3441471442_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3743968709_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3455280711_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3279745867_MethodInfo_var;
extern const uint32_t VRTK_VRInputModule_Hover_m1423269845_MetadataUsageId;
extern "C"  void VRTK_VRInputModule_Hover_m1423269845 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_VRInputModule_Hover_m1423269845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastResult_t21186376  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3220004478  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GameObject_t1756533147 * V_2 = NULL;
	Selectable_t1490392188 * V_3 = NULL;
	Navigation_t1571958496  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		VRTK_UIPointer_t2714926455 * L_0 = ___pointer0;
		NullCheck(L_0);
		PointerEventData_t1599784723 * L_1 = L_0->get_pointerEventData_10();
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = PointerEventData_get_pointerEnter_m2114522773(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0096;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_4 = ___pointer0;
		List_1_t3685274804 * L_5 = ___results1;
		VRTK_VRInputModule_CheckPointerHoverClick_m1182551694(__this, L_4, L_5, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_6 = ___pointer0;
		NullCheck(L_6);
		PointerEventData_t1599784723 * L_7 = L_6->get_pointerEventData_10();
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = PointerEventData_get_pointerEnter_m2114522773(L_7, /*hidden argument*/NULL);
		bool L_9 = VRTK_VRInputModule_ValidElement_m1326706494(__this, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0040;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_10 = ___pointer0;
		NullCheck(L_10);
		PointerEventData_t1599784723 * L_11 = L_10->get_pointerEventData_10();
		NullCheck(L_11);
		PointerEventData_set_pointerEnter_m1440587006(L_11, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
		return;
	}

IL_0040:
	{
		VRTK_UIPointer_t2714926455 * L_12 = ___pointer0;
		List_1_t3685274804 * L_13 = ___results1;
		bool L_14 = VRTK_VRInputModule_NoValidCollision_m3464415866(__this, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0091;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_15 = ___pointer0;
		NullCheck(L_15);
		PointerEventData_t1599784723 * L_16 = L_15->get_pointerEventData_10();
		NullCheck(L_16);
		GameObject_t1756533147 * L_17 = PointerEventData_get_pointerEnter_m2114522773(L_16, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_18 = ___pointer0;
		NullCheck(L_18);
		PointerEventData_t1599784723 * L_19 = L_18->get_pointerEventData_10();
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t3253137806 * L_20 = ExecuteEvents_get_pointerExitHandler_m3250605428(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_ExecuteHierarchy_TisIPointerExitHandler_t461019860_m300279158(NULL /*static, unused*/, L_17, L_19, L_20, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIPointerExitHandler_t461019860_m300279158_MethodInfo_var);
		VRTK_UIPointer_t2714926455 * L_21 = ___pointer0;
		NullCheck(L_21);
		PointerEventData_t1599784723 * L_22 = L_21->get_pointerEventData_10();
		NullCheck(L_22);
		List_1_t1125654279 * L_23 = L_22->get_hovered_9();
		VRTK_UIPointer_t2714926455 * L_24 = ___pointer0;
		NullCheck(L_24);
		PointerEventData_t1599784723 * L_25 = L_24->get_pointerEventData_10();
		NullCheck(L_25);
		GameObject_t1756533147 * L_26 = PointerEventData_get_pointerEnter_m2114522773(L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		List_1_Remove_m2287078133(L_23, L_26, /*hidden argument*/List_1_Remove_m2287078133_MethodInfo_var);
		VRTK_UIPointer_t2714926455 * L_27 = ___pointer0;
		NullCheck(L_27);
		PointerEventData_t1599784723 * L_28 = L_27->get_pointerEventData_10();
		NullCheck(L_28);
		PointerEventData_set_pointerEnter_m1440587006(L_28, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
	}

IL_0091:
	{
		goto IL_01f2;
	}

IL_0096:
	{
		List_1_t3685274804 * L_29 = ___results1;
		NullCheck(L_29);
		Enumerator_t3220004478  L_30 = List_1_GetEnumerator_m2972492689(L_29, /*hidden argument*/List_1_GetEnumerator_m2972492689_MethodInfo_var);
		V_1 = L_30;
	}

IL_009d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_019e;
		}

IL_00a2:
		{
			RaycastResult_t21186376  L_31 = Enumerator_get_Current_m4066973001((&V_1), /*hidden argument*/Enumerator_get_Current_m4066973001_MethodInfo_var);
			V_0 = L_31;
			GameObject_t1756533147 * L_32 = RaycastResult_get_gameObject_m2999022658((&V_0), /*hidden argument*/NULL);
			bool L_33 = VRTK_VRInputModule_ValidElement_m1326706494(__this, L_32, /*hidden argument*/NULL);
			if (L_33)
			{
				goto IL_00c1;
			}
		}

IL_00bc:
		{
			goto IL_019e;
		}

IL_00c1:
		{
			GameObject_t1756533147 * L_34 = RaycastResult_get_gameObject_m2999022658((&V_0), /*hidden argument*/NULL);
			VRTK_UIPointer_t2714926455 * L_35 = ___pointer0;
			NullCheck(L_35);
			PointerEventData_t1599784723 * L_36 = L_35->get_pointerEventData_10();
			IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
			EventFunction_1_t2985282902 * L_37 = ExecuteEvents_get_pointerEnterHandler_m2874610790(NULL /*static, unused*/, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_38 = ExecuteEvents_ExecuteHierarchy_TisIPointerEnterHandler_t193164956_m1445871235(NULL /*static, unused*/, L_34, L_36, L_37, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIPointerEnterHandler_t193164956_m1445871235_MethodInfo_var);
			V_2 = L_38;
			GameObject_t1756533147 * L_39 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_40 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_39, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (!L_40)
			{
				goto IL_0161;
			}
		}

IL_00e5:
		{
			GameObject_t1756533147 * L_41 = V_2;
			NullCheck(L_41);
			Selectable_t1490392188 * L_42 = GameObject_GetComponent_TisSelectable_t1490392188_m1495814632(L_41, /*hidden argument*/GameObject_GetComponent_TisSelectable_t1490392188_m1495814632_MethodInfo_var);
			V_3 = L_42;
			Selectable_t1490392188 * L_43 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_44 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
			if (!L_44)
			{
				goto IL_010f;
			}
		}

IL_00f7:
		{
			Initobj (Navigation_t1571958496_il2cpp_TypeInfo_var, (&V_4));
			Navigation_set_mode_m2631871514((&V_4), 0, /*hidden argument*/NULL);
			Selectable_t1490392188 * L_45 = V_3;
			Navigation_t1571958496  L_46 = V_4;
			NullCheck(L_45);
			Selectable_set_navigation_m2001068675(L_45, L_46, /*hidden argument*/NULL);
		}

IL_010f:
		{
			VRTK_UIPointer_t2714926455 * L_47 = ___pointer0;
			VRTK_UIPointer_t2714926455 * L_48 = ___pointer0;
			GameObject_t1756533147 * L_49 = V_2;
			VRTK_UIPointer_t2714926455 * L_50 = ___pointer0;
			NullCheck(L_50);
			GameObject_t1756533147 * L_51 = L_50->get_hoveringElement_11();
			NullCheck(L_48);
			UIPointerEventArgs_t1171985978  L_52 = VirtFuncInvoker2< UIPointerEventArgs_t1171985978 , GameObject_t1756533147 *, GameObject_t1756533147 * >::Invoke(9 /* VRTK.UIPointerEventArgs VRTK.VRTK_UIPointer::SetUIPointerEvent(UnityEngine.GameObject,UnityEngine.GameObject) */, L_48, L_49, L_51);
			NullCheck(L_47);
			VirtActionInvoker1< UIPointerEventArgs_t1171985978  >::Invoke(4 /* System.Void VRTK.VRTK_UIPointer::OnUIPointerElementEnter(VRTK.UIPointerEventArgs) */, L_47, L_52);
			VRTK_UIPointer_t2714926455 * L_53 = ___pointer0;
			GameObject_t1756533147 * L_54 = V_2;
			NullCheck(L_53);
			L_53->set_hoveringElement_11(L_54);
			VRTK_UIPointer_t2714926455 * L_55 = ___pointer0;
			NullCheck(L_55);
			PointerEventData_t1599784723 * L_56 = L_55->get_pointerEventData_10();
			RaycastResult_t21186376  L_57 = V_0;
			NullCheck(L_56);
			PointerEventData_set_pointerCurrentRaycast_m2431897513(L_56, L_57, /*hidden argument*/NULL);
			VRTK_UIPointer_t2714926455 * L_58 = ___pointer0;
			NullCheck(L_58);
			PointerEventData_t1599784723 * L_59 = L_58->get_pointerEventData_10();
			GameObject_t1756533147 * L_60 = V_2;
			NullCheck(L_59);
			PointerEventData_set_pointerEnter_m1440587006(L_59, L_60, /*hidden argument*/NULL);
			VRTK_UIPointer_t2714926455 * L_61 = ___pointer0;
			NullCheck(L_61);
			PointerEventData_t1599784723 * L_62 = L_61->get_pointerEventData_10();
			NullCheck(L_62);
			List_1_t1125654279 * L_63 = L_62->get_hovered_9();
			VRTK_UIPointer_t2714926455 * L_64 = ___pointer0;
			NullCheck(L_64);
			PointerEventData_t1599784723 * L_65 = L_64->get_pointerEventData_10();
			NullCheck(L_65);
			GameObject_t1756533147 * L_66 = PointerEventData_get_pointerEnter_m2114522773(L_65, /*hidden argument*/NULL);
			NullCheck(L_63);
			List_1_Add_m3441471442(L_63, L_66, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
			goto IL_01aa;
		}

IL_0161:
		{
			GameObject_t1756533147 * L_67 = RaycastResult_get_gameObject_m2999022658((&V_0), /*hidden argument*/NULL);
			VRTK_UIPointer_t2714926455 * L_68 = ___pointer0;
			NullCheck(L_68);
			GameObject_t1756533147 * L_69 = L_68->get_hoveringElement_11();
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_70 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_67, L_69, /*hidden argument*/NULL);
			if (!L_70)
			{
				goto IL_0191;
			}
		}

IL_0178:
		{
			VRTK_UIPointer_t2714926455 * L_71 = ___pointer0;
			VRTK_UIPointer_t2714926455 * L_72 = ___pointer0;
			GameObject_t1756533147 * L_73 = RaycastResult_get_gameObject_m2999022658((&V_0), /*hidden argument*/NULL);
			VRTK_UIPointer_t2714926455 * L_74 = ___pointer0;
			NullCheck(L_74);
			GameObject_t1756533147 * L_75 = L_74->get_hoveringElement_11();
			NullCheck(L_72);
			UIPointerEventArgs_t1171985978  L_76 = VirtFuncInvoker2< UIPointerEventArgs_t1171985978 , GameObject_t1756533147 *, GameObject_t1756533147 * >::Invoke(9 /* VRTK.UIPointerEventArgs VRTK.VRTK_UIPointer::SetUIPointerEvent(UnityEngine.GameObject,UnityEngine.GameObject) */, L_72, L_73, L_75);
			NullCheck(L_71);
			VirtActionInvoker1< UIPointerEventArgs_t1171985978  >::Invoke(4 /* System.Void VRTK.VRTK_UIPointer::OnUIPointerElementEnter(VRTK.UIPointerEventArgs) */, L_71, L_76);
		}

IL_0191:
		{
			VRTK_UIPointer_t2714926455 * L_77 = ___pointer0;
			GameObject_t1756533147 * L_78 = RaycastResult_get_gameObject_m2999022658((&V_0), /*hidden argument*/NULL);
			NullCheck(L_77);
			L_77->set_hoveringElement_11(L_78);
		}

IL_019e:
		{
			bool L_79 = Enumerator_MoveNext_m3743968709((&V_1), /*hidden argument*/Enumerator_MoveNext_m3743968709_MethodInfo_var);
			if (L_79)
			{
				goto IL_00a2;
			}
		}

IL_01aa:
		{
			IL2CPP_LEAVE(0x1BD, FINALLY_01af);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_01af;
	}

FINALLY_01af:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3455280711((&V_1), /*hidden argument*/Enumerator_Dispose_m3455280711_MethodInfo_var);
		IL2CPP_END_FINALLY(431)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(431)
	{
		IL2CPP_JUMP_TBL(0x1BD, IL_01bd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_01bd:
	{
		VRTK_UIPointer_t2714926455 * L_80 = ___pointer0;
		NullCheck(L_80);
		GameObject_t1756533147 * L_81 = L_80->get_hoveringElement_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_82 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_81, /*hidden argument*/NULL);
		if (!L_82)
		{
			goto IL_01f2;
		}
	}
	{
		List_1_t3685274804 * L_83 = ___results1;
		NullCheck(L_83);
		int32_t L_84 = List_1_get_Count_m3279745867(L_83, /*hidden argument*/List_1_get_Count_m3279745867_MethodInfo_var);
		if (L_84)
		{
			goto IL_01f2;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_85 = ___pointer0;
		VRTK_UIPointer_t2714926455 * L_86 = ___pointer0;
		VRTK_UIPointer_t2714926455 * L_87 = ___pointer0;
		NullCheck(L_87);
		GameObject_t1756533147 * L_88 = L_87->get_hoveringElement_11();
		NullCheck(L_86);
		UIPointerEventArgs_t1171985978  L_89 = VirtFuncInvoker2< UIPointerEventArgs_t1171985978 , GameObject_t1756533147 *, GameObject_t1756533147 * >::Invoke(9 /* VRTK.UIPointerEventArgs VRTK.VRTK_UIPointer::SetUIPointerEvent(UnityEngine.GameObject,UnityEngine.GameObject) */, L_86, (GameObject_t1756533147 *)NULL, L_88);
		NullCheck(L_85);
		VirtActionInvoker1< UIPointerEventArgs_t1171985978  >::Invoke(5 /* System.Void VRTK.VRTK_UIPointer::OnUIPointerElementExit(VRTK.UIPointerEventArgs) */, L_85, L_89);
		VRTK_UIPointer_t2714926455 * L_90 = ___pointer0;
		NullCheck(L_90);
		L_90->set_hoveringElement_11((GameObject_t1756533147 *)NULL);
	}

IL_01f2:
	{
		return;
	}
}
// System.Void VRTK.VRTK_VRInputModule::Click(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  void VRTK_VRInputModule_Click_m1795054129 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		VRTK_UIPointer_t2714926455 * L_0 = ___pointer0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_clickMethod_5();
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0026;
		}
	}
	{
		goto IL_0034;
	}

IL_0019:
	{
		VRTK_UIPointer_t2714926455 * L_4 = ___pointer0;
		List_1_t3685274804 * L_5 = ___results1;
		VRTK_VRInputModule_ClickOnUp_m2325928753(__this, L_4, L_5, /*hidden argument*/NULL);
		goto IL_0034;
	}

IL_0026:
	{
		VRTK_UIPointer_t2714926455 * L_6 = ___pointer0;
		List_1_t3685274804 * L_7 = ___results1;
		VRTK_VRInputModule_ClickOnDown_m903340057(__this, L_6, L_7, (bool)0, /*hidden argument*/NULL);
		goto IL_0034;
	}

IL_0034:
	{
		return;
	}
}
// System.Void VRTK.VRTK_VRInputModule::ClickOnUp(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  void VRTK_VRInputModule_ClickOnUp_m2325928753 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method)
{
	{
		VRTK_UIPointer_t2714926455 * L_0 = ___pointer0;
		NullCheck(L_0);
		PointerEventData_t1599784723 * L_1 = L_0->get_pointerEventData_10();
		VRTK_UIPointer_t2714926455 * L_2 = ___pointer0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker2< bool, bool, bool >::Invoke(14 /* System.Boolean VRTK.VRTK_UIPointer::ValidClick(System.Boolean,System.Boolean) */, L_2, (bool)0, (bool)0);
		NullCheck(L_1);
		PointerEventData_set_eligibleForClick_m2036057844(L_1, L_3, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_4 = ___pointer0;
		bool L_5 = VRTK_VRInputModule_AttemptClick_m2631317559(__this, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0028;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_6 = ___pointer0;
		List_1_t3685274804 * L_7 = ___results1;
		VRTK_VRInputModule_IsEligibleClick_m2949652322(__this, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void VRTK.VRTK_VRInputModule::ClickOnDown(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>,System.Boolean)
extern "C"  void VRTK_VRInputModule_ClickOnDown_m903340057 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, bool ___forceClick2, const MethodInfo* method)
{
	PointerEventData_t1599784723 * G_B2_0 = NULL;
	PointerEventData_t1599784723 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	PointerEventData_t1599784723 * G_B3_1 = NULL;
	{
		VRTK_UIPointer_t2714926455 * L_0 = ___pointer0;
		NullCheck(L_0);
		PointerEventData_t1599784723 * L_1 = L_0->get_pointerEventData_10();
		bool L_2 = ___forceClick2;
		G_B1_0 = L_1;
		if (!L_2)
		{
			G_B2_0 = L_1;
			goto IL_0012;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_001a;
	}

IL_0012:
	{
		VRTK_UIPointer_t2714926455 * L_3 = ___pointer0;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker2< bool, bool, bool >::Invoke(14 /* System.Boolean VRTK.VRTK_UIPointer::ValidClick(System.Boolean,System.Boolean) */, L_3, (bool)1, (bool)0);
		G_B3_0 = ((int32_t)(L_4));
		G_B3_1 = G_B2_0;
	}

IL_001a:
	{
		NullCheck(G_B3_1);
		PointerEventData_set_eligibleForClick_m2036057844(G_B3_1, (bool)G_B3_0, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_5 = ___pointer0;
		List_1_t3685274804 * L_6 = ___results1;
		bool L_7 = VRTK_VRInputModule_IsEligibleClick_m2949652322(__this, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0040;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_8 = ___pointer0;
		NullCheck(L_8);
		PointerEventData_t1599784723 * L_9 = L_8->get_pointerEventData_10();
		NullCheck(L_9);
		PointerEventData_set_eligibleForClick_m2036057844(L_9, (bool)0, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_10 = ___pointer0;
		VRTK_VRInputModule_AttemptClick_m2631317559(__this, L_10, /*hidden argument*/NULL);
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean VRTK.VRTK_VRInputModule::IsEligibleClick(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern Il2CppClass* ExecuteEvents_t1693084770_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2972492689_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4066973001_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_ExecuteHierarchy_TisIPointerDownHandler_t3929046918_m462226506_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3743968709_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3455280711_MethodInfo_var;
extern const uint32_t VRTK_VRInputModule_IsEligibleClick_m2949652322_MetadataUsageId;
extern "C"  bool VRTK_VRInputModule_IsEligibleClick_m2949652322 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_VRInputModule_IsEligibleClick_m2949652322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastResult_t21186376  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3220004478  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GameObject_t1756533147 * V_2 = NULL;
	bool V_3 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		VRTK_UIPointer_t2714926455 * L_0 = ___pointer0;
		NullCheck(L_0);
		PointerEventData_t1599784723 * L_1 = L_0->get_pointerEventData_10();
		NullCheck(L_1);
		bool L_2 = PointerEventData_get_eligibleForClick_m2497780621(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00b3;
		}
	}
	{
		List_1_t3685274804 * L_3 = ___results1;
		NullCheck(L_3);
		Enumerator_t3220004478  L_4 = List_1_GetEnumerator_m2972492689(L_3, /*hidden argument*/List_1_GetEnumerator_m2972492689_MethodInfo_var);
		V_1 = L_4;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0094;
		}

IL_001c:
		{
			RaycastResult_t21186376  L_5 = Enumerator_get_Current_m4066973001((&V_1), /*hidden argument*/Enumerator_get_Current_m4066973001_MethodInfo_var);
			V_0 = L_5;
			GameObject_t1756533147 * L_6 = RaycastResult_get_gameObject_m2999022658((&V_0), /*hidden argument*/NULL);
			bool L_7 = VRTK_VRInputModule_ValidElement_m1326706494(__this, L_6, /*hidden argument*/NULL);
			if (L_7)
			{
				goto IL_003b;
			}
		}

IL_0036:
		{
			goto IL_0094;
		}

IL_003b:
		{
			GameObject_t1756533147 * L_8 = RaycastResult_get_gameObject_m2999022658((&V_0), /*hidden argument*/NULL);
			VRTK_UIPointer_t2714926455 * L_9 = ___pointer0;
			NullCheck(L_9);
			PointerEventData_t1599784723 * L_10 = L_9->get_pointerEventData_10();
			IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
			EventFunction_1_t2426197568 * L_11 = ExecuteEvents_get_pointerDownHandler_m1172742772(NULL /*static, unused*/, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_12 = ExecuteEvents_ExecuteHierarchy_TisIPointerDownHandler_t3929046918_m462226506(NULL /*static, unused*/, L_8, L_10, L_11, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIPointerDownHandler_t3929046918_m462226506_MethodInfo_var);
			V_2 = L_12;
			GameObject_t1756533147 * L_13 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_14 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_13, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (!L_14)
			{
				goto IL_0094;
			}
		}

IL_005f:
		{
			VRTK_UIPointer_t2714926455 * L_15 = ___pointer0;
			NullCheck(L_15);
			PointerEventData_t1599784723 * L_16 = L_15->get_pointerEventData_10();
			VRTK_UIPointer_t2714926455 * L_17 = ___pointer0;
			NullCheck(L_17);
			PointerEventData_t1599784723 * L_18 = L_17->get_pointerEventData_10();
			NullCheck(L_18);
			Vector2_t2243707579  L_19 = PointerEventData_get_position_m2131765015(L_18, /*hidden argument*/NULL);
			NullCheck(L_16);
			PointerEventData_set_pressPosition_m2094137883(L_16, L_19, /*hidden argument*/NULL);
			VRTK_UIPointer_t2714926455 * L_20 = ___pointer0;
			NullCheck(L_20);
			PointerEventData_t1599784723 * L_21 = L_20->get_pointerEventData_10();
			RaycastResult_t21186376  L_22 = V_0;
			NullCheck(L_21);
			PointerEventData_set_pointerPressRaycast_m2551142399(L_21, L_22, /*hidden argument*/NULL);
			VRTK_UIPointer_t2714926455 * L_23 = ___pointer0;
			NullCheck(L_23);
			PointerEventData_t1599784723 * L_24 = L_23->get_pointerEventData_10();
			GameObject_t1756533147 * L_25 = V_2;
			NullCheck(L_24);
			PointerEventData_set_pointerPress_m1418261989(L_24, L_25, /*hidden argument*/NULL);
			V_3 = (bool)1;
			IL2CPP_LEAVE(0xB5, FINALLY_00a5);
		}

IL_0094:
		{
			bool L_26 = Enumerator_MoveNext_m3743968709((&V_1), /*hidden argument*/Enumerator_MoveNext_m3743968709_MethodInfo_var);
			if (L_26)
			{
				goto IL_001c;
			}
		}

IL_00a0:
		{
			IL2CPP_LEAVE(0xB3, FINALLY_00a5);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00a5;
	}

FINALLY_00a5:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3455280711((&V_1), /*hidden argument*/Enumerator_Dispose_m3455280711_MethodInfo_var);
		IL2CPP_END_FINALLY(165)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(165)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_JUMP_TBL(0xB3, IL_00b3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		bool L_27 = V_3;
		return L_27;
	}
}
// System.Boolean VRTK.VRTK_VRInputModule::AttemptClick(VRTK.VRTK_UIPointer)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteEvents_t1693084770_il2cpp_TypeInfo_var;
extern const MethodInfo* ExecuteEvents_ExecuteHierarchy_TisIPointerUpHandler_t1847764461_m1441454696_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_ExecuteHierarchy_TisIPointerClickHandler_t96169666_m1165684521_MethodInfo_var;
extern const uint32_t VRTK_VRInputModule_AttemptClick_m2631317559_MetadataUsageId;
extern "C"  bool VRTK_VRInputModule_AttemptClick_m2631317559 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_VRInputModule_AttemptClick_m2631317559_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VRTK_UIPointer_t2714926455 * L_0 = ___pointer0;
		NullCheck(L_0);
		PointerEventData_t1599784723 * L_1 = L_0->get_pointerEventData_10();
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = PointerEventData_get_pointerPress_m880101744(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00d4;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_4 = ___pointer0;
		NullCheck(L_4);
		PointerEventData_t1599784723 * L_5 = L_4->get_pointerEventData_10();
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = PointerEventData_get_pointerPress_m880101744(L_5, /*hidden argument*/NULL);
		bool L_7 = VRTK_VRInputModule_ValidElement_m1326706494(__this, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_002d;
		}
	}
	{
		return (bool)1;
	}

IL_002d:
	{
		VRTK_UIPointer_t2714926455 * L_8 = ___pointer0;
		NullCheck(L_8);
		PointerEventData_t1599784723 * L_9 = L_8->get_pointerEventData_10();
		NullCheck(L_9);
		bool L_10 = PointerEventData_get_eligibleForClick_m2497780621(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0076;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_11 = ___pointer0;
		bool L_12 = VRTK_VRInputModule_IsHovering_m2892594038(__this, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0071;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_13 = ___pointer0;
		NullCheck(L_13);
		PointerEventData_t1599784723 * L_14 = L_13->get_pointerEventData_10();
		NullCheck(L_14);
		GameObject_t1756533147 * L_15 = PointerEventData_get_pointerPress_m880101744(L_14, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_16 = ___pointer0;
		NullCheck(L_16);
		PointerEventData_t1599784723 * L_17 = L_16->get_pointerEventData_10();
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t344915111 * L_18 = ExecuteEvents_get_pointerUpHandler_m3494368244(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_ExecuteHierarchy_TisIPointerUpHandler_t1847764461_m1441454696(NULL /*static, unused*/, L_15, L_17, L_18, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIPointerUpHandler_t1847764461_m1441454696_MethodInfo_var);
		VRTK_UIPointer_t2714926455 * L_19 = ___pointer0;
		NullCheck(L_19);
		PointerEventData_t1599784723 * L_20 = L_19->get_pointerEventData_10();
		NullCheck(L_20);
		PointerEventData_set_pointerPress_m1418261989(L_20, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
	}

IL_0071:
	{
		goto IL_00d2;
	}

IL_0076:
	{
		VRTK_UIPointer_t2714926455 * L_21 = ___pointer0;
		VRTK_UIPointer_t2714926455 * L_22 = ___pointer0;
		VRTK_UIPointer_t2714926455 * L_23 = ___pointer0;
		NullCheck(L_23);
		PointerEventData_t1599784723 * L_24 = L_23->get_pointerEventData_10();
		NullCheck(L_24);
		GameObject_t1756533147 * L_25 = PointerEventData_get_pointerPress_m880101744(L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		UIPointerEventArgs_t1171985978  L_26 = VirtFuncInvoker2< UIPointerEventArgs_t1171985978 , GameObject_t1756533147 *, GameObject_t1756533147 * >::Invoke(9 /* VRTK.UIPointerEventArgs VRTK.VRTK_UIPointer::SetUIPointerEvent(UnityEngine.GameObject,UnityEngine.GameObject) */, L_22, L_25, (GameObject_t1756533147 *)NULL);
		NullCheck(L_21);
		VirtActionInvoker1< UIPointerEventArgs_t1171985978  >::Invoke(6 /* System.Void VRTK.VRTK_UIPointer::OnUIPointerElementClick(VRTK.UIPointerEventArgs) */, L_21, L_26);
		VRTK_UIPointer_t2714926455 * L_27 = ___pointer0;
		NullCheck(L_27);
		PointerEventData_t1599784723 * L_28 = L_27->get_pointerEventData_10();
		NullCheck(L_28);
		GameObject_t1756533147 * L_29 = PointerEventData_get_pointerPress_m880101744(L_28, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_30 = ___pointer0;
		NullCheck(L_30);
		PointerEventData_t1599784723 * L_31 = L_30->get_pointerEventData_10();
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t2888287612 * L_32 = ExecuteEvents_get_pointerClickHandler_m713983310(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_ExecuteHierarchy_TisIPointerClickHandler_t96169666_m1165684521(NULL /*static, unused*/, L_29, L_31, L_32, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIPointerClickHandler_t96169666_m1165684521_MethodInfo_var);
		VRTK_UIPointer_t2714926455 * L_33 = ___pointer0;
		NullCheck(L_33);
		PointerEventData_t1599784723 * L_34 = L_33->get_pointerEventData_10();
		NullCheck(L_34);
		GameObject_t1756533147 * L_35 = PointerEventData_get_pointerPress_m880101744(L_34, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_36 = ___pointer0;
		NullCheck(L_36);
		PointerEventData_t1599784723 * L_37 = L_36->get_pointerEventData_10();
		EventFunction_1_t344915111 * L_38 = ExecuteEvents_get_pointerUpHandler_m3494368244(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_ExecuteHierarchy_TisIPointerUpHandler_t1847764461_m1441454696(NULL /*static, unused*/, L_35, L_37, L_38, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIPointerUpHandler_t1847764461_m1441454696_MethodInfo_var);
		VRTK_UIPointer_t2714926455 * L_39 = ___pointer0;
		NullCheck(L_39);
		PointerEventData_t1599784723 * L_40 = L_39->get_pointerEventData_10();
		NullCheck(L_40);
		PointerEventData_set_pointerPress_m1418261989(L_40, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
	}

IL_00d2:
	{
		return (bool)1;
	}

IL_00d4:
	{
		return (bool)0;
	}
}
// System.Void VRTK.VRTK_VRInputModule::Drag(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteEvents_t1693084770_il2cpp_TypeInfo_var;
extern const MethodInfo* ExecuteEvents_ExecuteHierarchy_TisIDragHandler_t2583993319_m1802910190_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_ExecuteHierarchy_TisIEndDragHandler_t1349123600_m848469007_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2972492689_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4066973001_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_ExecuteHierarchy_TisIDropHandler_t2390101210_m4291895312_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3743968709_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3455280711_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_ExecuteHierarchy_TisIInitializePotentialDragHandler_t3350809087_m3328014326_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_ExecuteHierarchy_TisIBeginDragHandler_t3135127860_m2721925759_MethodInfo_var;
extern const uint32_t VRTK_VRInputModule_Drag_m3103883503_MetadataUsageId;
extern "C"  void VRTK_VRInputModule_Drag_m3103883503 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_VRInputModule_Drag_m3103883503_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastResult_t21186376  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3220004478  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RaycastResult_t21186376  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t3220004478  V_3;
	memset(&V_3, 0, sizeof(V_3));
	GameObject_t1756533147 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	PointerEventData_t1599784723 * G_B2_0 = NULL;
	PointerEventData_t1599784723 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	PointerEventData_t1599784723 * G_B3_1 = NULL;
	{
		VRTK_UIPointer_t2714926455 * L_0 = ___pointer0;
		NullCheck(L_0);
		PointerEventData_t1599784723 * L_1 = L_0->get_pointerEventData_10();
		VRTK_UIPointer_t2714926455 * L_2 = ___pointer0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(13 /* System.Boolean VRTK.VRTK_UIPointer::SelectionButtonActive() */, L_2);
		G_B1_0 = L_1;
		if (!L_3)
		{
			G_B2_0 = L_1;
			goto IL_0028;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_4 = ___pointer0;
		NullCheck(L_4);
		PointerEventData_t1599784723 * L_5 = L_4->get_pointerEventData_10();
		NullCheck(L_5);
		Vector2_t2243707579  L_6 = PointerEventData_get_delta_m1072163964(L_5, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_8 = Vector2_op_Inequality_m4283136193(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_8));
		G_B3_1 = G_B1_0;
		goto IL_0029;
	}

IL_0028:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0029:
	{
		NullCheck(G_B3_1);
		PointerEventData_set_dragging_m915629341(G_B3_1, (bool)G_B3_0, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_9 = ___pointer0;
		NullCheck(L_9);
		PointerEventData_t1599784723 * L_10 = L_9->get_pointerEventData_10();
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = PointerEventData_get_pointerDrag_m2740415629(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_012b;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_13 = ___pointer0;
		NullCheck(L_13);
		PointerEventData_t1599784723 * L_14 = L_13->get_pointerEventData_10();
		NullCheck(L_14);
		GameObject_t1756533147 * L_15 = PointerEventData_get_pointerDrag_m2740415629(L_14, /*hidden argument*/NULL);
		bool L_16 = VRTK_VRInputModule_ValidElement_m1326706494(__this, L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_005a;
		}
	}
	{
		return;
	}

IL_005a:
	{
		VRTK_UIPointer_t2714926455 * L_17 = ___pointer0;
		NullCheck(L_17);
		PointerEventData_t1599784723 * L_18 = L_17->get_pointerEventData_10();
		NullCheck(L_18);
		bool L_19 = PointerEventData_get_dragging_m220490640(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0097;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_20 = ___pointer0;
		bool L_21 = VRTK_VRInputModule_IsHovering_m2892594038(__this, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0092;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_22 = ___pointer0;
		NullCheck(L_22);
		PointerEventData_t1599784723 * L_23 = L_22->get_pointerEventData_10();
		NullCheck(L_23);
		GameObject_t1756533147 * L_24 = PointerEventData_get_pointerDrag_m2740415629(L_23, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_25 = ___pointer0;
		NullCheck(L_25);
		PointerEventData_t1599784723 * L_26 = L_25->get_pointerEventData_10();
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t1081143969 * L_27 = ExecuteEvents_get_dragHandler_m1515454020(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_ExecuteHierarchy_TisIDragHandler_t2583993319_m1802910190(NULL /*static, unused*/, L_24, L_26, L_27, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIDragHandler_t2583993319_m1802910190_MethodInfo_var);
	}

IL_0092:
	{
		goto IL_0126;
	}

IL_0097:
	{
		VRTK_UIPointer_t2714926455 * L_28 = ___pointer0;
		NullCheck(L_28);
		PointerEventData_t1599784723 * L_29 = L_28->get_pointerEventData_10();
		NullCheck(L_29);
		GameObject_t1756533147 * L_30 = PointerEventData_get_pointerDrag_m2740415629(L_29, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_31 = ___pointer0;
		NullCheck(L_31);
		PointerEventData_t1599784723 * L_32 = L_31->get_pointerEventData_10();
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		EventFunction_1_t1081143969 * L_33 = ExecuteEvents_get_dragHandler_m1515454020(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_ExecuteHierarchy_TisIDragHandler_t2583993319_m1802910190(NULL /*static, unused*/, L_30, L_32, L_33, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIDragHandler_t2583993319_m1802910190_MethodInfo_var);
		VRTK_UIPointer_t2714926455 * L_34 = ___pointer0;
		NullCheck(L_34);
		PointerEventData_t1599784723 * L_35 = L_34->get_pointerEventData_10();
		NullCheck(L_35);
		GameObject_t1756533147 * L_36 = PointerEventData_get_pointerDrag_m2740415629(L_35, /*hidden argument*/NULL);
		VRTK_UIPointer_t2714926455 * L_37 = ___pointer0;
		NullCheck(L_37);
		PointerEventData_t1599784723 * L_38 = L_37->get_pointerEventData_10();
		EventFunction_1_t4141241546 * L_39 = ExecuteEvents_get_endDragHandler_m56074740(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_ExecuteHierarchy_TisIEndDragHandler_t1349123600_m848469007(NULL /*static, unused*/, L_36, L_38, L_39, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIEndDragHandler_t1349123600_m848469007_MethodInfo_var);
		List_1_t3685274804 * L_40 = ___results1;
		NullCheck(L_40);
		Enumerator_t3220004478  L_41 = List_1_GetEnumerator_m2972492689(L_40, /*hidden argument*/List_1_GetEnumerator_m2972492689_MethodInfo_var);
		V_1 = L_41;
	}

IL_00d6:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00fb;
		}

IL_00db:
		{
			RaycastResult_t21186376  L_42 = Enumerator_get_Current_m4066973001((&V_1), /*hidden argument*/Enumerator_get_Current_m4066973001_MethodInfo_var);
			V_0 = L_42;
			GameObject_t1756533147 * L_43 = RaycastResult_get_gameObject_m2999022658((&V_0), /*hidden argument*/NULL);
			VRTK_UIPointer_t2714926455 * L_44 = ___pointer0;
			NullCheck(L_44);
			PointerEventData_t1599784723 * L_45 = L_44->get_pointerEventData_10();
			IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
			EventFunction_1_t887251860 * L_46 = ExecuteEvents_get_dropHandler_m1848078078(NULL /*static, unused*/, /*hidden argument*/NULL);
			ExecuteEvents_ExecuteHierarchy_TisIDropHandler_t2390101210_m4291895312(NULL /*static, unused*/, L_43, L_45, L_46, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIDropHandler_t2390101210_m4291895312_MethodInfo_var);
		}

IL_00fb:
		{
			bool L_47 = Enumerator_MoveNext_m3743968709((&V_1), /*hidden argument*/Enumerator_MoveNext_m3743968709_MethodInfo_var);
			if (L_47)
			{
				goto IL_00db;
			}
		}

IL_0107:
		{
			IL2CPP_LEAVE(0x11A, FINALLY_010c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_010c;
	}

FINALLY_010c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3455280711((&V_1), /*hidden argument*/Enumerator_Dispose_m3455280711_MethodInfo_var);
		IL2CPP_END_FINALLY(268)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(268)
	{
		IL2CPP_JUMP_TBL(0x11A, IL_011a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_011a:
	{
		VRTK_UIPointer_t2714926455 * L_48 = ___pointer0;
		NullCheck(L_48);
		PointerEventData_t1599784723 * L_49 = L_48->get_pointerEventData_10();
		NullCheck(L_49);
		PointerEventData_set_pointerDrag_m3543074708(L_49, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
	}

IL_0126:
	{
		goto IL_01ed;
	}

IL_012b:
	{
		VRTK_UIPointer_t2714926455 * L_50 = ___pointer0;
		NullCheck(L_50);
		PointerEventData_t1599784723 * L_51 = L_50->get_pointerEventData_10();
		NullCheck(L_51);
		bool L_52 = PointerEventData_get_dragging_m220490640(L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_01ed;
		}
	}
	{
		List_1_t3685274804 * L_53 = ___results1;
		NullCheck(L_53);
		Enumerator_t3220004478  L_54 = List_1_GetEnumerator_m2972492689(L_53, /*hidden argument*/List_1_GetEnumerator_m2972492689_MethodInfo_var);
		V_3 = L_54;
	}

IL_0142:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01ce;
		}

IL_0147:
		{
			RaycastResult_t21186376  L_55 = Enumerator_get_Current_m4066973001((&V_3), /*hidden argument*/Enumerator_get_Current_m4066973001_MethodInfo_var);
			V_2 = L_55;
			GameObject_t1756533147 * L_56 = RaycastResult_get_gameObject_m2999022658((&V_2), /*hidden argument*/NULL);
			bool L_57 = VRTK_VRInputModule_ValidElement_m1326706494(__this, L_56, /*hidden argument*/NULL);
			if (L_57)
			{
				goto IL_0166;
			}
		}

IL_0161:
		{
			goto IL_01ce;
		}

IL_0166:
		{
			GameObject_t1756533147 * L_58 = RaycastResult_get_gameObject_m2999022658((&V_2), /*hidden argument*/NULL);
			VRTK_UIPointer_t2714926455 * L_59 = ___pointer0;
			NullCheck(L_59);
			PointerEventData_t1599784723 * L_60 = L_59->get_pointerEventData_10();
			IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
			EventFunction_1_t1847959737 * L_61 = ExecuteEvents_get_initializePotentialDrag_m2227640438(NULL /*static, unused*/, /*hidden argument*/NULL);
			ExecuteEvents_ExecuteHierarchy_TisIInitializePotentialDragHandler_t3350809087_m3328014326(NULL /*static, unused*/, L_58, L_60, L_61, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIInitializePotentialDragHandler_t3350809087_m3328014326_MethodInfo_var);
			GameObject_t1756533147 * L_62 = RaycastResult_get_gameObject_m2999022658((&V_2), /*hidden argument*/NULL);
			VRTK_UIPointer_t2714926455 * L_63 = ___pointer0;
			NullCheck(L_63);
			PointerEventData_t1599784723 * L_64 = L_63->get_pointerEventData_10();
			EventFunction_1_t1632278510 * L_65 = ExecuteEvents_get_beginDragHandler_m2307748884(NULL /*static, unused*/, /*hidden argument*/NULL);
			ExecuteEvents_ExecuteHierarchy_TisIBeginDragHandler_t3135127860_m2721925759(NULL /*static, unused*/, L_62, L_64, L_65, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIBeginDragHandler_t3135127860_m2721925759_MethodInfo_var);
			GameObject_t1756533147 * L_66 = RaycastResult_get_gameObject_m2999022658((&V_2), /*hidden argument*/NULL);
			VRTK_UIPointer_t2714926455 * L_67 = ___pointer0;
			NullCheck(L_67);
			PointerEventData_t1599784723 * L_68 = L_67->get_pointerEventData_10();
			EventFunction_1_t1081143969 * L_69 = ExecuteEvents_get_dragHandler_m1515454020(NULL /*static, unused*/, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_70 = ExecuteEvents_ExecuteHierarchy_TisIDragHandler_t2583993319_m1802910190(NULL /*static, unused*/, L_66, L_68, L_69, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIDragHandler_t2583993319_m1802910190_MethodInfo_var);
			V_4 = L_70;
			GameObject_t1756533147 * L_71 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_72 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_71, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (!L_72)
			{
				goto IL_01ce;
			}
		}

IL_01bc:
		{
			VRTK_UIPointer_t2714926455 * L_73 = ___pointer0;
			NullCheck(L_73);
			PointerEventData_t1599784723 * L_74 = L_73->get_pointerEventData_10();
			GameObject_t1756533147 * L_75 = V_4;
			NullCheck(L_74);
			PointerEventData_set_pointerDrag_m3543074708(L_74, L_75, /*hidden argument*/NULL);
			goto IL_01da;
		}

IL_01ce:
		{
			bool L_76 = Enumerator_MoveNext_m3743968709((&V_3), /*hidden argument*/Enumerator_MoveNext_m3743968709_MethodInfo_var);
			if (L_76)
			{
				goto IL_0147;
			}
		}

IL_01da:
		{
			IL2CPP_LEAVE(0x1ED, FINALLY_01df);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_01df;
	}

FINALLY_01df:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3455280711((&V_3), /*hidden argument*/Enumerator_Dispose_m3455280711_MethodInfo_var);
		IL2CPP_END_FINALLY(479)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(479)
	{
		IL2CPP_JUMP_TBL(0x1ED, IL_01ed)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_01ed:
	{
		return;
	}
}
// System.Void VRTK.VRTK_VRInputModule::Scroll(VRTK.VRTK_UIPointer,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern Il2CppClass* ExecuteEvents_t1693084770_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2972492689_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4066973001_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_ExecuteHierarchy_TisIScrollHandler_t3834677510_m3398003692_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3743968709_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3455280711_MethodInfo_var;
extern const uint32_t VRTK_VRInputModule_Scroll_m982370564_MetadataUsageId;
extern "C"  void VRTK_VRInputModule_Scroll_m982370564 (VRTK_VRInputModule_t1472500726 * __this, VRTK_UIPointer_t2714926455 * ___pointer0, List_1_t3685274804 * ___results1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_VRInputModule_Scroll_m982370564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RaycastResult_t21186376  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t3220004478  V_2;
	memset(&V_2, 0, sizeof(V_2));
	GameObject_t1756533147 * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		VRTK_UIPointer_t2714926455 * L_0 = ___pointer0;
		NullCheck(L_0);
		PointerEventData_t1599784723 * L_1 = L_0->get_pointerEventData_10();
		VRTK_UIPointer_t2714926455 * L_2 = ___pointer0;
		NullCheck(L_2);
		VRTK_ControllerEvents_t3225224819 * L_3 = L_2->get_controller_8();
		NullCheck(L_3);
		Vector2_t2243707579  L_4 = VRTK_ControllerEvents_GetTouchpadAxis_m84603443(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		PointerEventData_set_scrollDelta_m4002219844(L_1, L_4, /*hidden argument*/NULL);
		V_0 = (bool)0;
		List_1_t3685274804 * L_5 = ___results1;
		NullCheck(L_5);
		Enumerator_t3220004478  L_6 = List_1_GetEnumerator_m2972492689(L_5, /*hidden argument*/List_1_GetEnumerator_m2972492689_MethodInfo_var);
		V_2 = L_6;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006b;
		}

IL_0024:
		{
			RaycastResult_t21186376  L_7 = Enumerator_get_Current_m4066973001((&V_2), /*hidden argument*/Enumerator_get_Current_m4066973001_MethodInfo_var);
			V_1 = L_7;
			VRTK_UIPointer_t2714926455 * L_8 = ___pointer0;
			NullCheck(L_8);
			PointerEventData_t1599784723 * L_9 = L_8->get_pointerEventData_10();
			NullCheck(L_9);
			Vector2_t2243707579  L_10 = PointerEventData_get_scrollDelta_m1283145047(L_9, /*hidden argument*/NULL);
			Vector2_t2243707579  L_11 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
			bool L_12 = Vector2_op_Inequality_m4283136193(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
			if (!L_12)
			{
				goto IL_006b;
			}
		}

IL_0046:
		{
			GameObject_t1756533147 * L_13 = RaycastResult_get_gameObject_m2999022658((&V_1), /*hidden argument*/NULL);
			VRTK_UIPointer_t2714926455 * L_14 = ___pointer0;
			NullCheck(L_14);
			PointerEventData_t1599784723 * L_15 = L_14->get_pointerEventData_10();
			IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
			EventFunction_1_t2331828160 * L_16 = ExecuteEvents_get_scrollHandler_m2797719886(NULL /*static, unused*/, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_17 = ExecuteEvents_ExecuteHierarchy_TisIScrollHandler_t3834677510_m3398003692(NULL /*static, unused*/, L_13, L_15, L_16, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIScrollHandler_t3834677510_m3398003692_MethodInfo_var);
			V_3 = L_17;
			GameObject_t1756533147 * L_18 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_19 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_006b;
			}
		}

IL_0069:
		{
			V_0 = (bool)1;
		}

IL_006b:
		{
			bool L_20 = Enumerator_MoveNext_m3743968709((&V_2), /*hidden argument*/Enumerator_MoveNext_m3743968709_MethodInfo_var);
			if (L_20)
			{
				goto IL_0024;
			}
		}

IL_0077:
		{
			IL2CPP_LEAVE(0x8A, FINALLY_007c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_007c;
	}

FINALLY_007c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3455280711((&V_2), /*hidden argument*/Enumerator_Dispose_m3455280711_MethodInfo_var);
		IL2CPP_END_FINALLY(124)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(124)
	{
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008a:
	{
		VRTK_UIPointer_t2714926455 * L_21 = ___pointer0;
		NullCheck(L_21);
		GameObject_t1756533147 * L_22 = L_21->get_controllerRenderModel_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_23 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00a6;
		}
	}
	{
		VRTK_UIPointer_t2714926455 * L_24 = ___pointer0;
		NullCheck(L_24);
		GameObject_t1756533147 * L_25 = L_24->get_controllerRenderModel_12();
		bool L_26 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_SDK_Bridge_t2841169330_il2cpp_TypeInfo_var);
		VRTK_SDK_Bridge_SetControllerRenderModelWheel_m3652369569(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
	}

IL_00a6:
	{
		return;
	}
}
// System.Void VRTK.VRTK_WarpObjectControlAction::.ctor()
extern "C"  void VRTK_WarpObjectControlAction__ctor_m2400935438 (VRTK_WarpObjectControlAction_t3039272518 * __this, const MethodInfo* method)
{
	{
		__this->set_warpDistance_10((1.0f));
		__this->set_warpMultiplier_11((2.0f));
		__this->set_warpDelay_12((0.5f));
		__this->set_floorHeightTolerance_13((1.0f));
		__this->set_blinkTransitionSpeed_14((0.6f));
		VRTK_BaseObjectControlAction__ctor_m2598474563(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_WarpObjectControlAction::Process(UnityEngine.GameObject,UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Single,System.Boolean,System.Boolean)
extern "C"  void VRTK_WarpObjectControlAction_Process_m1965104587 (VRTK_WarpObjectControlAction_t3039272518 * __this, GameObject_t1756533147 * ___controlledGameObject0, Transform_t3275118058 * ___directionDevice1, Vector3_t2243707580  ___axisDirection2, float ___axis3, float ___deadzone4, bool ___currentlyFalling5, bool ___modifierActive6, const MethodInfo* method)
{
	{
		float L_0 = __this->get_warpDelayTimer_15();
		float L_1 = Time_get_timeSinceLevelLoad_m1980066582(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_0029;
		}
	}
	{
		float L_2 = ___axis3;
		if ((((float)L_2) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		GameObject_t1756533147 * L_3 = ___controlledGameObject0;
		Transform_t3275118058 * L_4 = ___directionDevice1;
		Vector3_t2243707580  L_5 = ___axisDirection2;
		float L_6 = ___axis3;
		bool L_7 = ___modifierActive6;
		VirtActionInvoker5< GameObject_t1756533147 *, Transform_t3275118058 *, Vector3_t2243707580 , float, bool >::Invoke(13 /* System.Void VRTK.VRTK_WarpObjectControlAction::Warp(UnityEngine.GameObject,UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean) */, __this, L_3, L_4, L_5, L_6, L_7);
	}

IL_0029:
	{
		return;
	}
}
// System.Void VRTK.VRTK_WarpObjectControlAction::OnEnable()
extern "C"  void VRTK_WarpObjectControlAction_OnEnable_m2196046506 (VRTK_WarpObjectControlAction_t3039272518 * __this, const MethodInfo* method)
{
	{
		VRTK_BaseObjectControlAction_OnEnable_m3563341911(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_0 = VRTK_DeviceFinder_HeadsetTransform_m2991712096(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_headset_16(L_0);
		return;
	}
}
// System.Void VRTK.VRTK_WarpObjectControlAction::Warp(UnityEngine.GameObject,UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_WarpObjectControlAction_Warp_m3281243986_MetadataUsageId;
extern "C"  void VRTK_WarpObjectControlAction_Warp_m3281243986 (VRTK_WarpObjectControlAction_t3039272518 * __this, GameObject_t1756533147 * ___controlledGameObject0, Transform_t3275118058 * ___directionDevice1, Vector3_t2243707580  ___axisDirection2, float ___axis3, bool ___modifierActive4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_WarpObjectControlAction_Warp_m3281243986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	RaycastHit_t87180320  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float G_B2_0 = 0.0f;
	float G_B1_0 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	Vector3_t2243707580  G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	{
		GameObject_t1756533147 * L_0 = ___controlledGameObject0;
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = VirtFuncInvoker1< Vector3_t2243707580 , Transform_t3275118058 * >::Invoke(11 /* UnityEngine.Vector3 VRTK.VRTK_BaseObjectControlAction::GetObjectCenter(UnityEngine.Transform) */, __this, L_1);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = ___controlledGameObject0;
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = V_0;
		NullCheck(L_4);
		Vector3_t2243707580  L_6 = Transform_TransformPoint_m3272254198(L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = __this->get_warpDistance_10();
		bool L_8 = ___modifierActive4;
		G_B1_0 = L_7;
		if (!L_8)
		{
			G_B2_0 = L_7;
			goto IL_0032;
		}
	}
	{
		float L_9 = __this->get_warpMultiplier_11();
		G_B3_0 = L_9;
		G_B3_1 = G_B1_0;
		goto IL_0037;
	}

IL_0032:
	{
		G_B3_0 = (1.0f);
		G_B3_1 = G_B2_0;
	}

IL_0037:
	{
		V_2 = ((float)((float)G_B3_1*(float)G_B3_0));
		float L_10 = ___axis3;
		int32_t L_11 = VirtFuncInvoker1< int32_t, float >::Invoke(12 /* System.Int32 VRTK.VRTK_BaseObjectControlAction::GetAxisDirection(System.Single) */, __this, L_10);
		V_3 = L_11;
		Vector3_t2243707580  L_12 = V_1;
		Vector3_t2243707580  L_13 = ___axisDirection2;
		float L_14 = V_2;
		Vector3_t2243707580  L_15 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_3;
		Vector3_t2243707580  L_17 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_15, (((float)((float)L_16))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_18 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_12, L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		V_5 = (0.2f);
		int32_t L_19 = V_3;
		Vector3_t2243707580  L_20 = ___axisDirection2;
		Vector3_t2243707580  L_21 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, (((float)((float)L_19))), L_20, /*hidden argument*/NULL);
		V_7 = L_21;
		GameObject_t1756533147 * L_22 = ___controlledGameObject0;
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = GameObject_get_transform_m909382139(L_22, /*hidden argument*/NULL);
		Transform_t3275118058 * L_24 = ((VRTK_BaseObjectControlAction_t3375001897 *)__this)->get_playArea_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_008f;
		}
	}
	{
		Transform_t3275118058 * L_26 = __this->get_headset_16();
		NullCheck(L_26);
		Vector3_t2243707580  L_27 = Transform_get_position_m1104419803(L_26, /*hidden argument*/NULL);
		G_B6_0 = L_27;
		goto IL_009a;
	}

IL_008f:
	{
		GameObject_t1756533147 * L_28 = ___controlledGameObject0;
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = GameObject_get_transform_m909382139(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t2243707580  L_30 = Transform_get_position_m1104419803(L_29, /*hidden argument*/NULL);
		G_B6_0 = L_30;
	}

IL_009a:
	{
		V_8 = G_B6_0;
		Vector3_t2243707580  L_31 = V_8;
		Vector3_t2243707580  L_32 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_33 = V_5;
		Vector3_t2243707580  L_34 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		Vector3_t2243707580  L_35 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_31, L_34, /*hidden argument*/NULL);
		Vector3_t2243707580  L_36 = V_7;
		float L_37 = V_2;
		float L_38 = ((VRTK_BaseObjectControlAction_t3375001897 *)__this)->get_colliderRadius_6();
		bool L_39 = Physics_Raycast_m2994111303(NULL /*static, unused*/, L_35, L_36, (&V_6), ((float)((float)L_37-(float)L_38)), /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_00e0;
		}
	}
	{
		Vector3_t2243707580  L_40 = RaycastHit_get_point_m326143462((&V_6), /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_7;
		float L_42 = ((VRTK_BaseObjectControlAction_t3375001897 *)__this)->get_colliderRadius_6();
		Vector3_t2243707580  L_43 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_44 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_40, L_43, /*hidden argument*/NULL);
		V_4 = L_44;
	}

IL_00e0:
	{
		Vector3_t2243707580  L_45 = V_4;
		Vector3_t2243707580  L_46 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_47 = __this->get_floorHeightTolerance_13();
		float L_48 = V_5;
		Vector3_t2243707580  L_49 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_46, ((float)((float)L_47+(float)L_48)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_45, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = Vector3_get_down_m2372302126(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_52 = __this->get_floorHeightTolerance_13();
		float L_53 = V_5;
		bool L_54 = Physics_Raycast_m2994111303(NULL /*static, unused*/, L_50, L_51, (&V_6), ((float)((float)((float)((float)L_52+(float)L_53))*(float)(2.0f))), /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_0183;
		}
	}
	{
		Vector3_t2243707580  L_55 = RaycastHit_get_point_m326143462((&V_6), /*hidden argument*/NULL);
		V_9 = L_55;
		float L_56 = (&V_9)->get_y_2();
		float L_57 = ((VRTK_BaseObjectControlAction_t3375001897 *)__this)->get_colliderHeight_7();
		(&V_4)->set_y_2(((float)((float)L_56+(float)((float)((float)L_57/(float)(2.0f))))));
		Vector3_t2243707580  L_58 = V_4;
		Vector3_t2243707580  L_59 = V_1;
		Vector3_t2243707580  L_60 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_58, L_59, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_61 = ___controlledGameObject0;
		NullCheck(L_61);
		Transform_t3275118058 * L_62 = GameObject_get_transform_m909382139(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		Vector3_t2243707580  L_63 = Transform_get_position_m1104419803(L_62, /*hidden argument*/NULL);
		Vector3_t2243707580  L_64 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_60, L_63, /*hidden argument*/NULL);
		V_10 = L_64;
		float L_65 = Time_get_timeSinceLevelLoad_m1980066582(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_66 = __this->get_warpDelay_12();
		__this->set_warpDelayTimer_15(((float)((float)L_65+(float)L_66)));
		GameObject_t1756533147 * L_67 = ___controlledGameObject0;
		NullCheck(L_67);
		Transform_t3275118058 * L_68 = GameObject_get_transform_m909382139(L_67, /*hidden argument*/NULL);
		Vector3_t2243707580  L_69 = V_10;
		NullCheck(L_68);
		Transform_set_position_m2469242620(L_68, L_69, /*hidden argument*/NULL);
		float L_70 = __this->get_blinkTransitionSpeed_14();
		VirtActionInvoker1< float >::Invoke(9 /* System.Void VRTK.VRTK_BaseObjectControlAction::Blink(System.Single) */, __this, L_70);
	}

IL_0183:
	{
		return;
	}
}
// System.Void VRTK.VRTK_Wheel::.ctor()
extern Il2CppClass* VRTK_Control_t651619021_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_Wheel__ctor_m4178971337_MetadataUsageId;
extern "C"  void VRTK_Wheel__ctor_m4178971337 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_Wheel__ctor_m4178971337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_detatchDistance_17((0.5f));
		__this->set_maximumValue_19((10.0f));
		__this->set_stepSize_20((1.0f));
		__this->set_grabbedFriction_22((25.0f));
		__this->set_releasedFriction_23((10.0f));
		__this->set_maxAngle_24((359.0f));
		__this->set_angularVelocityLimit_26((150.0f));
		__this->set_springStrengthValue_27((150.0f));
		__this->set_springDamperValue_28((5.0f));
		IL2CPP_RUNTIME_CLASS_INIT(VRTK_Control_t651619021_il2cpp_TypeInfo_var);
		VRTK_Control__ctor_m394111009(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_Wheel::InitRequiredComponents()
extern "C"  void VRTK_Wheel_InitRequiredComponents_m2622498188 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t4030073918  L_1 = Transform_get_localRotation_m4001487205(L_0, /*hidden argument*/NULL);
		__this->set_initialLocalRotation_29(L_1);
		VRTK_Wheel_InitWheel_m1410538514(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VRTK.VRTK_Wheel::DetectSetup()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193_MethodInfo_var;
extern const uint32_t VRTK_Wheel_DetectSetup_m2996764025_MetadataUsageId;
extern "C"  bool VRTK_Wheel_DetectSetup_m2996764025 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_Wheel_DetectSetup_m2996764025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_wheelHingeCreated_32();
		if (!L_0)
		{
			goto IL_0051;
		}
	}
	{
		HingeJoint_t2745110831 * L_1 = __this->get_wheelHinge_31();
		Vector3_t2243707580  L_2 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Joint_set_anchor_m4209051050(L_1, L_2, /*hidden argument*/NULL);
		HingeJoint_t2745110831 * L_3 = __this->get_wheelHinge_31();
		Vector3_t2243707580  L_4 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Joint_set_axis_m2488398082(L_3, L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = __this->get_connectedTo_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0051;
		}
	}
	{
		HingeJoint_t2745110831 * L_7 = __this->get_wheelHinge_31();
		GameObject_t1756533147 * L_8 = __this->get_connectedTo_15();
		NullCheck(L_8);
		Rigidbody_t4233889191 * L_9 = GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193(L_8, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193_MethodInfo_var);
		NullCheck(L_7);
		Joint_set_connectedBody_m2242534827(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return (bool)1;
	}
}
// VRTK.VRTK_Control/ControlValueRange VRTK.VRTK_Wheel::RegisterValueRange()
extern Il2CppClass* ControlValueRange_t2976216666_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_Wheel_RegisterValueRange_m860455153_MetadataUsageId;
extern "C"  ControlValueRange_t2976216666  VRTK_Wheel_RegisterValueRange_m860455153 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_Wheel_RegisterValueRange_m860455153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ControlValueRange_t2976216666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (ControlValueRange_t2976216666_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = __this->get_minimumValue_18();
		(&V_0)->set_controlMin_0(L_0);
		float L_1 = __this->get_maximumValue_19();
		(&V_0)->set_controlMax_1(L_1);
		ControlValueRange_t2976216666  L_2 = V_0;
		return L_2;
	}
}
// System.Void VRTK.VRTK_Wheel::HandleUpdate()
extern "C"  void VRTK_Wheel_HandleUpdate_m1696673920 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method)
{
	{
		VRTK_Wheel_CalculateValue_m79252598(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_lockAtLimits_25();
		if (!L_0)
		{
			goto IL_0034;
		}
	}
	{
		bool L_1 = __this->get_initialValueCalculated_33();
		if (L_1)
		{
			goto IL_0034;
		}
	}
	{
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_3 = __this->get_initialLocalRotation_29();
		NullCheck(L_2);
		Transform_set_localRotation_m2055111962(L_2, L_3, /*hidden argument*/NULL);
		__this->set_initialValueCalculated_33((bool)1);
	}

IL_0034:
	{
		return;
	}
}
// System.Void VRTK.VRTK_Wheel::InitWheel()
extern "C"  void VRTK_Wheel_InitWheel_m1410538514 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method)
{
	{
		VRTK_Wheel_SetupRigidbody_m3453275419(__this, /*hidden argument*/NULL);
		VRTK_Wheel_SetupHinge_m2643596911(__this, /*hidden argument*/NULL);
		VRTK_Wheel_SetupInteractableObject_m3175052259(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_Wheel::SetupRigidbody()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t4233889191_m520013213_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193_MethodInfo_var;
extern const uint32_t VRTK_Wheel_SetupRigidbody_m3453275419_MetadataUsageId;
extern "C"  void VRTK_Wheel_SetupRigidbody_m3453275419 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_Wheel_SetupRigidbody_m3453275419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody_t4233889191 * V_0 = NULL;
	{
		Rigidbody_t4233889191 * L_0 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_MethodInfo_var);
		__this->set_wheelRigidbody_30(L_0);
		Rigidbody_t4233889191 * L_1 = __this->get_wheelRigidbody_30();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Rigidbody_t4233889191 * L_4 = GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210(L_3, /*hidden argument*/GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210_MethodInfo_var);
		__this->set_wheelRigidbody_30(L_4);
		Rigidbody_t4233889191 * L_5 = __this->get_wheelRigidbody_30();
		float L_6 = __this->get_releasedFriction_23();
		NullCheck(L_5);
		Rigidbody_set_angularDrag_m1042416512(L_5, L_6, /*hidden argument*/NULL);
	}

IL_003f:
	{
		Rigidbody_t4233889191 * L_7 = __this->get_wheelRigidbody_30();
		NullCheck(L_7);
		Rigidbody_set_isKinematic_m738793415(L_7, (bool)0, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_8 = __this->get_wheelRigidbody_30();
		NullCheck(L_8);
		Rigidbody_set_useGravity_m2606656539(L_8, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = __this->get_connectedTo_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0099;
		}
	}
	{
		GameObject_t1756533147 * L_11 = __this->get_connectedTo_15();
		NullCheck(L_11);
		Rigidbody_t4233889191 * L_12 = GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193(L_11, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193_MethodInfo_var);
		V_0 = L_12;
		Rigidbody_t4233889191 * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_13, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0099;
		}
	}
	{
		GameObject_t1756533147 * L_15 = __this->get_connectedTo_15();
		NullCheck(L_15);
		Rigidbody_t4233889191 * L_16 = GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210(L_15, /*hidden argument*/GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210_MethodInfo_var);
		V_0 = L_16;
		Rigidbody_t4233889191 * L_17 = V_0;
		NullCheck(L_17);
		Rigidbody_set_useGravity_m2606656539(L_17, (bool)0, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_18 = V_0;
		NullCheck(L_18);
		Rigidbody_set_isKinematic_m738793415(L_18, (bool)1, /*hidden argument*/NULL);
	}

IL_0099:
	{
		return;
	}
}
// System.Void VRTK.VRTK_Wheel::SetupHinge()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisHingeJoint_t2745110831_m4068659703_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisHingeJoint_t2745110831_m45774388_MethodInfo_var;
extern const uint32_t VRTK_Wheel_SetupHinge_m2643596911_MetadataUsageId;
extern "C"  void VRTK_Wheel_SetupHinge_m2643596911 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_Wheel_SetupHinge_m2643596911_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HingeJoint_t2745110831 * L_0 = Component_GetComponent_TisHingeJoint_t2745110831_m4068659703(__this, /*hidden argument*/Component_GetComponent_TisHingeJoint_t2745110831_m4068659703_MethodInfo_var);
		__this->set_wheelHinge_31(L_0);
		HingeJoint_t2745110831 * L_1 = __this->get_wheelHinge_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0035;
		}
	}
	{
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		HingeJoint_t2745110831 * L_4 = GameObject_AddComponent_TisHingeJoint_t2745110831_m45774388(L_3, /*hidden argument*/GameObject_AddComponent_TisHingeJoint_t2745110831_m45774388_MethodInfo_var);
		__this->set_wheelHinge_31(L_4);
		__this->set_wheelHingeCreated_32((bool)1);
	}

IL_0035:
	{
		VRTK_Wheel_SetupHingeRestrictions_m305252104(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_Wheel::SetupHingeRestrictions()
extern Il2CppClass* JointLimits_t4282861422_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_Wheel_SetupHingeRestrictions_m305252104_MetadataUsageId;
extern "C"  void VRTK_Wheel_SetupHingeRestrictions_m305252104 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_Wheel_SetupHingeRestrictions_m305252104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	JointLimits_t4282861422  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_t2243707580  V_15;
	memset(&V_15, 0, sizeof(V_15));
	{
		V_0 = (0.0f);
		float L_0 = __this->get_maxAngle_24();
		V_1 = L_0;
		float L_1 = __this->get_maxAngle_24();
		V_2 = ((float)((float)L_1-(float)(180.0f)));
		float L_2 = V_2;
		if ((!(((float)L_2) > ((float)(0.0f)))))
		{
			goto IL_002f;
		}
	}
	{
		float L_3 = V_0;
		float L_4 = V_2;
		V_0 = ((float)((float)L_3-(float)L_4));
		V_1 = (180.0f);
	}

IL_002f:
	{
		bool L_5 = __this->get_lockAtLimits_25();
		if (!L_5)
		{
			goto IL_01a5;
		}
	}
	{
		HingeJoint_t2745110831 * L_6 = __this->get_wheelHinge_31();
		NullCheck(L_6);
		HingeJoint_set_useLimits_m1140625641(L_6, (bool)1, /*hidden argument*/NULL);
		Initobj (JointLimits_t4282861422_il2cpp_TypeInfo_var, (&V_3));
		float L_7 = V_0;
		JointLimits_set_min_m3403685567((&V_3), L_7, /*hidden argument*/NULL);
		float L_8 = V_1;
		JointLimits_set_max_m2042039841((&V_3), L_8, /*hidden argument*/NULL);
		HingeJoint_t2745110831 * L_9 = __this->get_wheelHinge_31();
		JointLimits_t4282861422  L_10 = V_3;
		NullCheck(L_9);
		HingeJoint_set_limits_m249910410(L_9, L_10, /*hidden argument*/NULL);
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t2243707580  L_12 = Transform_get_localEulerAngles_m4231787854(L_11, /*hidden argument*/NULL);
		V_4 = L_12;
		Quaternion_t4030073918 * L_13 = __this->get_address_of_initialLocalRotation_29();
		Vector3_t2243707580  L_14 = Quaternion_get_eulerAngles_m3302573991(L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		float L_15 = (&V_5)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_16 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		V_6 = L_16;
		int32_t L_17 = V_6;
		if (!L_17)
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_18 = V_6;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)90))))
		{
			goto IL_00fd;
		}
	}
	{
		int32_t L_19 = V_6;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)180))))
		{
			goto IL_0147;
		}
	}
	{
		goto IL_0191;
	}

IL_00b3:
	{
		Transform_t3275118058 * L_20 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t2243707580  L_21 = Transform_get_localEulerAngles_m4231787854(L_20, /*hidden argument*/NULL);
		V_7 = L_21;
		float L_22 = (&V_7)->get_x_1();
		Transform_t3275118058 * L_23 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t2243707580  L_24 = Transform_get_localEulerAngles_m4231787854(L_23, /*hidden argument*/NULL);
		V_8 = L_24;
		float L_25 = (&V_8)->get_y_2();
		float L_26 = V_0;
		Transform_t3275118058 * L_27 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t2243707580  L_28 = Transform_get_localEulerAngles_m4231787854(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		float L_29 = (&V_9)->get_z_3();
		Vector3__ctor_m2638739322((&V_4), L_22, ((float)((float)L_25-(float)L_26)), L_29, /*hidden argument*/NULL);
		goto IL_0191;
	}

IL_00fd:
	{
		Transform_t3275118058 * L_30 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_t2243707580  L_31 = Transform_get_localEulerAngles_m4231787854(L_30, /*hidden argument*/NULL);
		V_10 = L_31;
		float L_32 = (&V_10)->get_x_1();
		float L_33 = V_0;
		Transform_t3275118058 * L_34 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_34);
		Vector3_t2243707580  L_35 = Transform_get_localEulerAngles_m4231787854(L_34, /*hidden argument*/NULL);
		V_11 = L_35;
		float L_36 = (&V_11)->get_y_2();
		Transform_t3275118058 * L_37 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		Vector3_t2243707580  L_38 = Transform_get_localEulerAngles_m4231787854(L_37, /*hidden argument*/NULL);
		V_12 = L_38;
		float L_39 = (&V_12)->get_z_3();
		Vector3__ctor_m2638739322((&V_4), ((float)((float)L_32+(float)L_33)), L_36, L_39, /*hidden argument*/NULL);
		goto IL_0191;
	}

IL_0147:
	{
		Transform_t3275118058 * L_40 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_t2243707580  L_41 = Transform_get_localEulerAngles_m4231787854(L_40, /*hidden argument*/NULL);
		V_13 = L_41;
		float L_42 = (&V_13)->get_x_1();
		Transform_t3275118058 * L_43 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_43);
		Vector3_t2243707580  L_44 = Transform_get_localEulerAngles_m4231787854(L_43, /*hidden argument*/NULL);
		V_14 = L_44;
		float L_45 = (&V_14)->get_y_2();
		float L_46 = V_0;
		Transform_t3275118058 * L_47 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		Vector3_t2243707580  L_48 = Transform_get_localEulerAngles_m4231787854(L_47, /*hidden argument*/NULL);
		V_15 = L_48;
		float L_49 = (&V_15)->get_z_3();
		Vector3__ctor_m2638739322((&V_4), L_42, ((float)((float)L_45+(float)L_46)), L_49, /*hidden argument*/NULL);
		goto IL_0191;
	}

IL_0191:
	{
		Transform_t3275118058 * L_50 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_4;
		NullCheck(L_50);
		Transform_set_localEulerAngles_m2927195985(L_50, L_51, /*hidden argument*/NULL);
		__this->set_initialValueCalculated_33((bool)0);
	}

IL_01a5:
	{
		return;
	}
}
// System.Void VRTK.VRTK_Wheel::ConfigureHingeSpring()
extern Il2CppClass* JointSpring_t1540921605_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_Wheel_ConfigureHingeSpring_m567323129_MetadataUsageId;
extern "C"  void VRTK_Wheel_ConfigureHingeSpring_m567323129 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_Wheel_ConfigureHingeSpring_m567323129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JointSpring_t1540921605  V_0;
	memset(&V_0, 0, sizeof(V_0));
	JointLimits_t4282861422  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (JointSpring_t1540921605_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = __this->get_springStrengthValue_27();
		(&V_0)->set_spring_0(L_0);
		float L_1 = __this->get_springDamperValue_28();
		(&V_0)->set_damper_1(L_1);
		float L_2 = __this->get_springAngle_34();
		HingeJoint_t2745110831 * L_3 = __this->get_wheelHinge_31();
		NullCheck(L_3);
		JointLimits_t4282861422  L_4 = HingeJoint_get_limits_m4283751733(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = JointLimits_get_min_m2921097272((&V_1), /*hidden argument*/NULL);
		(&V_0)->set_targetPosition_2(((float)((float)L_2+(float)L_5)));
		HingeJoint_t2745110831 * L_6 = __this->get_wheelHinge_31();
		JointSpring_t1540921605  L_7 = V_0;
		NullCheck(L_6);
		HingeJoint_set_spring_m3669466314(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_Wheel::SetupInteractableObject()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* InteractableObjectEventHandler_t940909295_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisVRTK_InteractableObject_t2604188111_m1255171419_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisVRTK_InteractableObject_t2604188111_m3085208446_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisVRTK_TrackObjectGrabAttach_t1133162717_m4130202784_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisVRTK_RotatorTrackGrabAttach_t3062106299_m2781911400_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisVRTK_SwapControllerGrabAction_t918155359_m2127770779_MethodInfo_var;
extern const MethodInfo* VRTK_Wheel_WheelInteractableObjectGrabbed_m2047642123_MethodInfo_var;
extern const MethodInfo* VRTK_Wheel_WheelInteractableObjectUngrabbed_m162986388_MethodInfo_var;
extern const uint32_t VRTK_Wheel_SetupInteractableObject_m3175052259_MetadataUsageId;
extern "C"  void VRTK_Wheel_SetupInteractableObject_m3175052259 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_Wheel_SetupInteractableObject_m3175052259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VRTK_InteractableObject_t2604188111 * V_0 = NULL;
	VRTK_TrackObjectGrabAttach_t1133162717 * V_1 = NULL;
	{
		VRTK_InteractableObject_t2604188111 * L_0 = Component_GetComponent_TisVRTK_InteractableObject_t2604188111_m1255171419(__this, /*hidden argument*/Component_GetComponent_TisVRTK_InteractableObject_t2604188111_m1255171419_MethodInfo_var);
		V_0 = L_0;
		VRTK_InteractableObject_t2604188111 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		VRTK_InteractableObject_t2604188111 * L_4 = GameObject_AddComponent_TisVRTK_InteractableObject_t2604188111_m3085208446(L_3, /*hidden argument*/GameObject_AddComponent_TisVRTK_InteractableObject_t2604188111_m3085208446_MethodInfo_var);
		V_0 = L_4;
	}

IL_001f:
	{
		VRTK_InteractableObject_t2604188111 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_isGrabbable_5((bool)1);
		int32_t L_6 = __this->get_grabType_16();
		if (L_6)
		{
			goto IL_0064;
		}
	}
	{
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		VRTK_TrackObjectGrabAttach_t1133162717 * L_8 = GameObject_AddComponent_TisVRTK_TrackObjectGrabAttach_t1133162717_m4130202784(L_7, /*hidden argument*/GameObject_AddComponent_TisVRTK_TrackObjectGrabAttach_t1133162717_m4130202784_MethodInfo_var);
		V_1 = L_8;
		bool L_9 = __this->get_lockAtLimits_25();
		if (!L_9)
		{
			goto IL_005f;
		}
	}
	{
		VRTK_TrackObjectGrabAttach_t1133162717 * L_10 = V_1;
		NullCheck(L_10);
		L_10->set_velocityLimit_19((0.0f));
		VRTK_TrackObjectGrabAttach_t1133162717 * L_11 = V_1;
		float L_12 = __this->get_angularVelocityLimit_26();
		NullCheck(L_11);
		L_11->set_angularVelocityLimit_20(L_12);
	}

IL_005f:
	{
		goto IL_0070;
	}

IL_0064:
	{
		GameObject_t1756533147 * L_13 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		VRTK_RotatorTrackGrabAttach_t3062106299 * L_14 = GameObject_AddComponent_TisVRTK_RotatorTrackGrabAttach_t3062106299_m2781911400(L_13, /*hidden argument*/GameObject_AddComponent_TisVRTK_RotatorTrackGrabAttach_t3062106299_m2781911400_MethodInfo_var);
		V_1 = L_14;
	}

IL_0070:
	{
		VRTK_TrackObjectGrabAttach_t1133162717 * L_15 = V_1;
		NullCheck(L_15);
		((VRTK_BaseGrabAttach_t3487134318 *)L_15)->set_precisionGrab_2((bool)1);
		VRTK_TrackObjectGrabAttach_t1133162717 * L_16 = V_1;
		float L_17 = __this->get_detatchDistance_17();
		NullCheck(L_16);
		L_16->set_detachDistance_18(L_17);
		VRTK_InteractableObject_t2604188111 * L_18 = V_0;
		VRTK_TrackObjectGrabAttach_t1133162717 * L_19 = V_1;
		NullCheck(L_18);
		L_18->set_grabAttachMechanicScript_11(L_19);
		VRTK_InteractableObject_t2604188111 * L_20 = V_0;
		GameObject_t1756533147 * L_21 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		VRTK_SwapControllerGrabAction_t918155359 * L_22 = GameObject_AddComponent_TisVRTK_SwapControllerGrabAction_t918155359_m2127770779(L_21, /*hidden argument*/GameObject_AddComponent_TisVRTK_SwapControllerGrabAction_t918155359_m2127770779_MethodInfo_var);
		NullCheck(L_20);
		L_20->set_secondaryGrabActionScript_12(L_22);
		VRTK_InteractableObject_t2604188111 * L_23 = V_0;
		NullCheck(L_23);
		L_23->set_stayGrabbedOnTeleport_7((bool)0);
		VRTK_InteractableObject_t2604188111 * L_24 = V_0;
		IntPtr_t L_25;
		L_25.set_m_value_0((void*)(void*)VRTK_Wheel_WheelInteractableObjectGrabbed_m2047642123_MethodInfo_var);
		InteractableObjectEventHandler_t940909295 * L_26 = (InteractableObjectEventHandler_t940909295 *)il2cpp_codegen_object_new(InteractableObjectEventHandler_t940909295_il2cpp_TypeInfo_var);
		InteractableObjectEventHandler__ctor_m1866744359(L_26, __this, L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		VRTK_InteractableObject_add_InteractableObjectGrabbed_m2695870213(L_24, L_26, /*hidden argument*/NULL);
		VRTK_InteractableObject_t2604188111 * L_27 = V_0;
		IntPtr_t L_28;
		L_28.set_m_value_0((void*)(void*)VRTK_Wheel_WheelInteractableObjectUngrabbed_m162986388_MethodInfo_var);
		InteractableObjectEventHandler_t940909295 * L_29 = (InteractableObjectEventHandler_t940909295 *)il2cpp_codegen_object_new(InteractableObjectEventHandler_t940909295_il2cpp_TypeInfo_var);
		InteractableObjectEventHandler__ctor_m1866744359(L_29, __this, L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		VRTK_InteractableObject_add_InteractableObjectUngrabbed_m569745414(L_27, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_Wheel::WheelInteractableObjectGrabbed(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_Wheel_WheelInteractableObjectGrabbed_m2047642123 (VRTK_Wheel_t803354859 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method)
{
	{
		Rigidbody_t4233889191 * L_0 = __this->get_wheelRigidbody_30();
		float L_1 = __this->get_grabbedFriction_22();
		NullCheck(L_0);
		Rigidbody_set_angularDrag_m1042416512(L_0, L_1, /*hidden argument*/NULL);
		HingeJoint_t2745110831 * L_2 = __this->get_wheelHinge_31();
		NullCheck(L_2);
		HingeJoint_set_useSpring_m3450562342(L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRTK.VRTK_Wheel::WheelInteractableObjectUngrabbed(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_Wheel_WheelInteractableObjectUngrabbed_m162986388 (VRTK_Wheel_t803354859 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method)
{
	{
		Rigidbody_t4233889191 * L_0 = __this->get_wheelRigidbody_30();
		float L_1 = __this->get_releasedFriction_23();
		NullCheck(L_0);
		Rigidbody_set_angularDrag_m1042416512(L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = __this->get_snapToStep_21();
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		HingeJoint_t2745110831 * L_3 = __this->get_wheelHinge_31();
		NullCheck(L_3);
		HingeJoint_set_useSpring_m3450562342(L_3, (bool)1, /*hidden argument*/NULL);
		VRTK_Wheel_ConfigureHingeSpring_m567323129(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void VRTK.VRTK_Wheel::CalculateValue()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t VRTK_Wheel_CalculateValue_m79252598_MetadataUsageId;
extern "C"  void VRTK_Wheel_CalculateValue_m79252598 (VRTK_Wheel_t803354859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTK_Wheel_CalculateValue_m79252598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ControlValueRange_t2976216666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Quaternion_t4030073918  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	{
		ControlValueRange_t2976216666  L_0 = VirtFuncInvoker0< ControlValueRange_t2976216666  >::Invoke(6 /* VRTK.VRTK_Control/ControlValueRange VRTK.VRTK_Control::RegisterValueRange() */, __this);
		V_0 = L_0;
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Quaternion_t4030073918  L_2 = Transform_get_localRotation_m4001487205(L_1, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_3 = __this->get_initialLocalRotation_29();
		Quaternion_t4030073918  L_4 = Quaternion_Inverse_m3931399088(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		Quaternion_ToAngleAxis_m2980929840((&V_3), (&V_1), (&V_2), /*hidden argument*/NULL);
		float L_6 = (&V_0)->get_controlMin_0();
		float L_7 = V_1;
		float L_8 = __this->get_maxAngle_24();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_9 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, ((float)((float)L_7/(float)L_8)), /*hidden argument*/NULL);
		float L_10 = (&V_0)->get_controlMax_1();
		float L_11 = (&V_0)->get_controlMin_0();
		float L_12 = __this->get_stepSize_20();
		float L_13 = bankers_roundf(((float)((float)((float)((float)L_6+(float)((float)((float)L_9*(float)((float)((float)L_10-(float)L_11))))))/(float)L_12)));
		float L_14 = __this->get_stepSize_20();
		V_4 = ((float)((float)L_13*(float)L_14));
		float L_15 = V_4;
		float L_16 = (&V_0)->get_controlMin_0();
		V_5 = ((float)((float)L_15-(float)L_16));
		float L_17 = (&V_0)->get_controlMax_1();
		float L_18 = (&V_0)->get_controlMin_0();
		V_6 = ((float)((float)L_17-(float)L_18));
		float L_19 = V_5;
		float L_20 = V_6;
		float L_21 = __this->get_maxAngle_24();
		__this->set_springAngle_34(((float)((float)((float)((float)L_19/(float)L_20))*(float)L_21)));
		float L_22 = V_4;
		((VRTK_Control_t651619021 *)__this)->set_value_8(L_22);
		return;
	}
}
// System.Void VRTK.VRTKTrackedControllerEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void VRTKTrackedControllerEventHandler__ctor_m4210029403 (VRTKTrackedControllerEventHandler_t2437916365 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void VRTK.VRTKTrackedControllerEventHandler::Invoke(System.Object,VRTK.VRTKTrackedControllerEventArgs)
extern "C"  void VRTKTrackedControllerEventHandler_Invoke_m4037681358 (VRTKTrackedControllerEventHandler_t2437916365 * __this, Il2CppObject * ___sender0, VRTKTrackedControllerEventArgs_t2407378264  ___e1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		VRTKTrackedControllerEventHandler_Invoke_m4037681358((VRTKTrackedControllerEventHandler_t2437916365 *)__this->get_prev_9(),___sender0, ___e1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___sender0, VRTKTrackedControllerEventArgs_t2407378264  ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___sender0, VRTKTrackedControllerEventArgs_t2407378264  ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, VRTKTrackedControllerEventArgs_t2407378264  ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult VRTK.VRTKTrackedControllerEventHandler::BeginInvoke(System.Object,VRTK.VRTKTrackedControllerEventArgs,System.AsyncCallback,System.Object)
extern Il2CppClass* VRTKTrackedControllerEventArgs_t2407378264_il2cpp_TypeInfo_var;
extern const uint32_t VRTKTrackedControllerEventHandler_BeginInvoke_m3154246081_MetadataUsageId;
extern "C"  Il2CppObject * VRTKTrackedControllerEventHandler_BeginInvoke_m3154246081 (VRTKTrackedControllerEventHandler_t2437916365 * __this, Il2CppObject * ___sender0, VRTKTrackedControllerEventArgs_t2407378264  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRTKTrackedControllerEventHandler_BeginInvoke_m3154246081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___sender0;
	__d_args[1] = Box(VRTKTrackedControllerEventArgs_t2407378264_il2cpp_TypeInfo_var, &___e1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void VRTK.VRTKTrackedControllerEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void VRTKTrackedControllerEventHandler_EndInvoke_m3508965689 (VRTKTrackedControllerEventHandler_t2437916365 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
