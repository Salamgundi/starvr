﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// Valve.VR.InteractionSystem.LinearMapping
struct LinearMapping_t810676855;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.LinearDrive
struct  LinearDrive_t1068763581  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Valve.VR.InteractionSystem.LinearDrive::startPosition
	Transform_t3275118058 * ___startPosition_2;
	// UnityEngine.Transform Valve.VR.InteractionSystem.LinearDrive::endPosition
	Transform_t3275118058 * ___endPosition_3;
	// Valve.VR.InteractionSystem.LinearMapping Valve.VR.InteractionSystem.LinearDrive::linearMapping
	LinearMapping_t810676855 * ___linearMapping_4;
	// System.Boolean Valve.VR.InteractionSystem.LinearDrive::repositionGameObject
	bool ___repositionGameObject_5;
	// System.Boolean Valve.VR.InteractionSystem.LinearDrive::maintainMomemntum
	bool ___maintainMomemntum_6;
	// System.Single Valve.VR.InteractionSystem.LinearDrive::momemtumDampenRate
	float ___momemtumDampenRate_7;
	// System.Single Valve.VR.InteractionSystem.LinearDrive::initialMappingOffset
	float ___initialMappingOffset_8;
	// System.Int32 Valve.VR.InteractionSystem.LinearDrive::numMappingChangeSamples
	int32_t ___numMappingChangeSamples_9;
	// System.Single[] Valve.VR.InteractionSystem.LinearDrive::mappingChangeSamples
	SingleU5BU5D_t577127397* ___mappingChangeSamples_10;
	// System.Single Valve.VR.InteractionSystem.LinearDrive::prevMapping
	float ___prevMapping_11;
	// System.Single Valve.VR.InteractionSystem.LinearDrive::mappingChangeRate
	float ___mappingChangeRate_12;
	// System.Int32 Valve.VR.InteractionSystem.LinearDrive::sampleCount
	int32_t ___sampleCount_13;

public:
	inline static int32_t get_offset_of_startPosition_2() { return static_cast<int32_t>(offsetof(LinearDrive_t1068763581, ___startPosition_2)); }
	inline Transform_t3275118058 * get_startPosition_2() const { return ___startPosition_2; }
	inline Transform_t3275118058 ** get_address_of_startPosition_2() { return &___startPosition_2; }
	inline void set_startPosition_2(Transform_t3275118058 * value)
	{
		___startPosition_2 = value;
		Il2CppCodeGenWriteBarrier(&___startPosition_2, value);
	}

	inline static int32_t get_offset_of_endPosition_3() { return static_cast<int32_t>(offsetof(LinearDrive_t1068763581, ___endPosition_3)); }
	inline Transform_t3275118058 * get_endPosition_3() const { return ___endPosition_3; }
	inline Transform_t3275118058 ** get_address_of_endPosition_3() { return &___endPosition_3; }
	inline void set_endPosition_3(Transform_t3275118058 * value)
	{
		___endPosition_3 = value;
		Il2CppCodeGenWriteBarrier(&___endPosition_3, value);
	}

	inline static int32_t get_offset_of_linearMapping_4() { return static_cast<int32_t>(offsetof(LinearDrive_t1068763581, ___linearMapping_4)); }
	inline LinearMapping_t810676855 * get_linearMapping_4() const { return ___linearMapping_4; }
	inline LinearMapping_t810676855 ** get_address_of_linearMapping_4() { return &___linearMapping_4; }
	inline void set_linearMapping_4(LinearMapping_t810676855 * value)
	{
		___linearMapping_4 = value;
		Il2CppCodeGenWriteBarrier(&___linearMapping_4, value);
	}

	inline static int32_t get_offset_of_repositionGameObject_5() { return static_cast<int32_t>(offsetof(LinearDrive_t1068763581, ___repositionGameObject_5)); }
	inline bool get_repositionGameObject_5() const { return ___repositionGameObject_5; }
	inline bool* get_address_of_repositionGameObject_5() { return &___repositionGameObject_5; }
	inline void set_repositionGameObject_5(bool value)
	{
		___repositionGameObject_5 = value;
	}

	inline static int32_t get_offset_of_maintainMomemntum_6() { return static_cast<int32_t>(offsetof(LinearDrive_t1068763581, ___maintainMomemntum_6)); }
	inline bool get_maintainMomemntum_6() const { return ___maintainMomemntum_6; }
	inline bool* get_address_of_maintainMomemntum_6() { return &___maintainMomemntum_6; }
	inline void set_maintainMomemntum_6(bool value)
	{
		___maintainMomemntum_6 = value;
	}

	inline static int32_t get_offset_of_momemtumDampenRate_7() { return static_cast<int32_t>(offsetof(LinearDrive_t1068763581, ___momemtumDampenRate_7)); }
	inline float get_momemtumDampenRate_7() const { return ___momemtumDampenRate_7; }
	inline float* get_address_of_momemtumDampenRate_7() { return &___momemtumDampenRate_7; }
	inline void set_momemtumDampenRate_7(float value)
	{
		___momemtumDampenRate_7 = value;
	}

	inline static int32_t get_offset_of_initialMappingOffset_8() { return static_cast<int32_t>(offsetof(LinearDrive_t1068763581, ___initialMappingOffset_8)); }
	inline float get_initialMappingOffset_8() const { return ___initialMappingOffset_8; }
	inline float* get_address_of_initialMappingOffset_8() { return &___initialMappingOffset_8; }
	inline void set_initialMappingOffset_8(float value)
	{
		___initialMappingOffset_8 = value;
	}

	inline static int32_t get_offset_of_numMappingChangeSamples_9() { return static_cast<int32_t>(offsetof(LinearDrive_t1068763581, ___numMappingChangeSamples_9)); }
	inline int32_t get_numMappingChangeSamples_9() const { return ___numMappingChangeSamples_9; }
	inline int32_t* get_address_of_numMappingChangeSamples_9() { return &___numMappingChangeSamples_9; }
	inline void set_numMappingChangeSamples_9(int32_t value)
	{
		___numMappingChangeSamples_9 = value;
	}

	inline static int32_t get_offset_of_mappingChangeSamples_10() { return static_cast<int32_t>(offsetof(LinearDrive_t1068763581, ___mappingChangeSamples_10)); }
	inline SingleU5BU5D_t577127397* get_mappingChangeSamples_10() const { return ___mappingChangeSamples_10; }
	inline SingleU5BU5D_t577127397** get_address_of_mappingChangeSamples_10() { return &___mappingChangeSamples_10; }
	inline void set_mappingChangeSamples_10(SingleU5BU5D_t577127397* value)
	{
		___mappingChangeSamples_10 = value;
		Il2CppCodeGenWriteBarrier(&___mappingChangeSamples_10, value);
	}

	inline static int32_t get_offset_of_prevMapping_11() { return static_cast<int32_t>(offsetof(LinearDrive_t1068763581, ___prevMapping_11)); }
	inline float get_prevMapping_11() const { return ___prevMapping_11; }
	inline float* get_address_of_prevMapping_11() { return &___prevMapping_11; }
	inline void set_prevMapping_11(float value)
	{
		___prevMapping_11 = value;
	}

	inline static int32_t get_offset_of_mappingChangeRate_12() { return static_cast<int32_t>(offsetof(LinearDrive_t1068763581, ___mappingChangeRate_12)); }
	inline float get_mappingChangeRate_12() const { return ___mappingChangeRate_12; }
	inline float* get_address_of_mappingChangeRate_12() { return &___mappingChangeRate_12; }
	inline void set_mappingChangeRate_12(float value)
	{
		___mappingChangeRate_12 = value;
	}

	inline static int32_t get_offset_of_sampleCount_13() { return static_cast<int32_t>(offsetof(LinearDrive_t1068763581, ___sampleCount_13)); }
	inline int32_t get_sampleCount_13() const { return ___sampleCount_13; }
	inline int32_t* get_address_of_sampleCount_13() { return &___sampleCount_13; }
	inline void set_sampleCount_13(int32_t value)
	{
		___sampleCount_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
