﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.OpenVRInterop
struct OpenVRInterop_t3889140593;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRInitError1685532365.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRApplicationType1952981913.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Valve.VR.OpenVRInterop::.ctor()
extern "C"  void OpenVRInterop__ctor_m1152241406 (OpenVRInterop_t3889140593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.OpenVRInterop::InitInternal(Valve.VR.EVRInitError&,Valve.VR.EVRApplicationType)
extern "C"  uint32_t OpenVRInterop_InitInternal_m4284973328 (Il2CppObject * __this /* static, unused */, int32_t* ___peError0, int32_t ___eApplicationType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.OpenVRInterop::ShutdownInternal()
extern "C"  void OpenVRInterop_ShutdownInternal_m3304386339 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.OpenVRInterop::IsHmdPresent()
extern "C"  bool OpenVRInterop_IsHmdPresent_m2130918122 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.OpenVRInterop::IsRuntimeInstalled()
extern "C"  bool OpenVRInterop_IsRuntimeInstalled_m214010688 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.OpenVRInterop::GetStringForHmdError(Valve.VR.EVRInitError)
extern "C"  IntPtr_t OpenVRInterop_GetStringForHmdError_m1380919063 (Il2CppObject * __this /* static, unused */, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Valve.VR.OpenVRInterop::GetGenericInterface(System.String,Valve.VR.EVRInitError&)
extern "C"  IntPtr_t OpenVRInterop_GetGenericInterface_m2103787160 (Il2CppObject * __this /* static, unused */, String_t* ___pchInterfaceVersion0, int32_t* ___peError1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.OpenVRInterop::IsInterfaceVersionValid(System.String)
extern "C"  bool OpenVRInterop_IsInterfaceVersionValid_m2079888169 (Il2CppObject * __this /* static, unused */, String_t* ___pchInterfaceVersion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.OpenVRInterop::GetInitToken()
extern "C"  uint32_t OpenVRInterop_GetInitToken_m505462722 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
