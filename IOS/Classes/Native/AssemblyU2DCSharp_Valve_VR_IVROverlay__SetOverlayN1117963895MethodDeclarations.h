﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetOverlayNeighbor
struct _SetOverlayNeighbor_t1117963895;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_EOverlayDirection2759670492.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetOverlayNeighbor::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayNeighbor__ctor_m91799642 (_SetOverlayNeighbor_t1117963895 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayNeighbor::Invoke(Valve.VR.EOverlayDirection,System.UInt64,System.UInt64)
extern "C"  int32_t _SetOverlayNeighbor_Invoke_m201989114 (_SetOverlayNeighbor_t1117963895 * __this, int32_t ___eDirection0, uint64_t ___ulFrom1, uint64_t ___ulTo2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayNeighbor::BeginInvoke(Valve.VR.EOverlayDirection,System.UInt64,System.UInt64,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetOverlayNeighbor_BeginInvoke_m2559932015 (_SetOverlayNeighbor_t1117963895 * __this, int32_t ___eDirection0, uint64_t ___ulFrom1, uint64_t ___ulTo2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayNeighbor::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayNeighbor_EndInvoke_m1118765798 (_SetOverlayNeighbor_t1117963895 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
