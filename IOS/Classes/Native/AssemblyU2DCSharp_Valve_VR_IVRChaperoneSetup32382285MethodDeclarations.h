﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperoneSetup
struct IVRChaperoneSetup_t32382285;
struct IVRChaperoneSetup_t32382285_marshaled_pinvoke;
struct IVRChaperoneSetup_t32382285_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IVRChaperoneSetup_t32382285;
struct IVRChaperoneSetup_t32382285_marshaled_pinvoke;

extern "C" void IVRChaperoneSetup_t32382285_marshal_pinvoke(const IVRChaperoneSetup_t32382285& unmarshaled, IVRChaperoneSetup_t32382285_marshaled_pinvoke& marshaled);
extern "C" void IVRChaperoneSetup_t32382285_marshal_pinvoke_back(const IVRChaperoneSetup_t32382285_marshaled_pinvoke& marshaled, IVRChaperoneSetup_t32382285& unmarshaled);
extern "C" void IVRChaperoneSetup_t32382285_marshal_pinvoke_cleanup(IVRChaperoneSetup_t32382285_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IVRChaperoneSetup_t32382285;
struct IVRChaperoneSetup_t32382285_marshaled_com;

extern "C" void IVRChaperoneSetup_t32382285_marshal_com(const IVRChaperoneSetup_t32382285& unmarshaled, IVRChaperoneSetup_t32382285_marshaled_com& marshaled);
extern "C" void IVRChaperoneSetup_t32382285_marshal_com_back(const IVRChaperoneSetup_t32382285_marshaled_com& marshaled, IVRChaperoneSetup_t32382285& unmarshaled);
extern "C" void IVRChaperoneSetup_t32382285_marshal_com_cleanup(IVRChaperoneSetup_t32382285_marshaled_com& marshaled);
