﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1
struct U3CHapticPulseU3Ec__Iterator1_t288649631;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1::.ctor()
extern "C"  void U3CHapticPulseU3Ec__Iterator1__ctor_m510469464 (U3CHapticPulseU3Ec__Iterator1_t288649631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1::MoveNext()
extern "C"  bool U3CHapticPulseU3Ec__Iterator1_MoveNext_m29992292 (U3CHapticPulseU3Ec__Iterator1_t288649631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CHapticPulseU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1586795706 (U3CHapticPulseU3Ec__Iterator1_t288649631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CHapticPulseU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1387303970 (U3CHapticPulseU3Ec__Iterator1_t288649631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1::Dispose()
extern "C"  void U3CHapticPulseU3Ec__Iterator1_Dispose_m3306377753 (U3CHapticPulseU3Ec__Iterator1_t288649631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_ControllerActions/<HapticPulse>c__Iterator1::Reset()
extern "C"  void U3CHapticPulseU3Ec__Iterator1_Reset_m1196022443 (U3CHapticPulseU3Ec__Iterator1_t288649631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
