﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_Lever
struct VRTK_Lever_t736126558;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_ControlValueRa2976216666.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_InteractableObjectEventArgs473175556.h"

// System.Void VRTK.VRTK_Lever::.ctor()
extern "C"  void VRTK_Lever__ctor_m2232289166 (VRTK_Lever_t736126558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Lever::InitRequiredComponents()
extern "C"  void VRTK_Lever_InitRequiredComponents_m2331769041 (VRTK_Lever_t736126558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Lever::DetectSetup()
extern "C"  bool VRTK_Lever_DetectSetup_m4085338128 (VRTK_Lever_t736126558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_Control/ControlValueRange VRTK.VRTK_Lever::RegisterValueRange()
extern "C"  ControlValueRange_t2976216666  VRTK_Lever_RegisterValueRange_m2926679194 (VRTK_Lever_t736126558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Lever::HandleUpdate()
extern "C"  void VRTK_Lever_HandleUpdate_m1927740893 (VRTK_Lever_t736126558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Lever::InitRigidbody()
extern "C"  void VRTK_Lever_InitRigidbody_m1713659953 (VRTK_Lever_t736126558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Lever::InitInteractableObject()
extern "C"  void VRTK_Lever_InitInteractableObject_m3105216313 (VRTK_Lever_t736126558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Lever::InteractableObjectGrabbed(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_Lever_InteractableObjectGrabbed_m1440394255 (VRTK_Lever_t736126558 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Lever::InteractableObjectUngrabbed(System.Object,VRTK.InteractableObjectEventArgs)
extern "C"  void VRTK_Lever_InteractableObjectUngrabbed_m2957812700 (VRTK_Lever_t736126558 * __this, Il2CppObject * ___sender0, InteractableObjectEventArgs_t473175556  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Lever::InitHingeJoint()
extern "C"  void VRTK_Lever_InitHingeJoint_m525376735 (VRTK_Lever_t736126558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_Lever::CalculateValue()
extern "C"  float VRTK_Lever_CalculateValue_m1138589503 (VRTK_Lever_t736126558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Lever::SnapToValue(System.Single)
extern "C"  void VRTK_Lever_SnapToValue_m479583655 (VRTK_Lever_t736126558 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
