﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`1<Valve.VR.VREvent_t>
struct InvokableCall_1_t1599593246;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<Valve.VR.VREvent_t>
struct UnityAction_1_t476884844;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`1<Valve.VR.VREvent_t>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m463912998_gshared (InvokableCall_1_t1599593246 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_1__ctor_m463912998(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_1_t1599593246 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m463912998_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`1<Valve.VR.VREvent_t>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m2684446796_gshared (InvokableCall_1_t1599593246 * __this, UnityAction_1_t476884844 * ___action0, const MethodInfo* method);
#define InvokableCall_1__ctor_m2684446796(__this, ___action0, method) ((  void (*) (InvokableCall_1_t1599593246 *, UnityAction_1_t476884844 *, const MethodInfo*))InvokableCall_1__ctor_m2684446796_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`1<Valve.VR.VREvent_t>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m2367044609_gshared (InvokableCall_1_t1599593246 * __this, UnityAction_1_t476884844 * ___value0, const MethodInfo* method);
#define InvokableCall_1_add_Delegate_m2367044609(__this, ___value0, method) ((  void (*) (InvokableCall_1_t1599593246 *, UnityAction_1_t476884844 *, const MethodInfo*))InvokableCall_1_add_Delegate_m2367044609_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`1<Valve.VR.VREvent_t>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m3030024234_gshared (InvokableCall_1_t1599593246 * __this, UnityAction_1_t476884844 * ___value0, const MethodInfo* method);
#define InvokableCall_1_remove_Delegate_m3030024234(__this, ___value0, method) ((  void (*) (InvokableCall_1_t1599593246 *, UnityAction_1_t476884844 *, const MethodInfo*))InvokableCall_1_remove_Delegate_m3030024234_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`1<Valve.VR.VREvent_t>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m4211226441_gshared (InvokableCall_1_t1599593246 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_1_Invoke_m4211226441(__this, ___args0, method) ((  void (*) (InvokableCall_1_t1599593246 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_1_Invoke_m4211226441_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<Valve.VR.VREvent_t>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m4080829761_gshared (InvokableCall_1_t1599593246 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_1_Find_m4080829761(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_1_t1599593246 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m4080829761_gshared)(__this, ___targetObj0, ___method1, method)
