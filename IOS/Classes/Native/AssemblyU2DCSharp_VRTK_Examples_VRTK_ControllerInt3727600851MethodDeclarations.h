﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.VRTK_ControllerInteract_ListenerExample
struct VRTK_ControllerInteract_ListenerExample_t3727600851;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventArgs771291242.h"

// System.Void VRTK.Examples.VRTK_ControllerInteract_ListenerExample::.ctor()
extern "C"  void VRTK_ControllerInteract_ListenerExample__ctor_m3766972192 (VRTK_ControllerInteract_ListenerExample_t3727600851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerInteract_ListenerExample::Start()
extern "C"  void VRTK_ControllerInteract_ListenerExample_Start_m795102040 (VRTK_ControllerInteract_ListenerExample_t3727600851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerInteract_ListenerExample::DebugLogger(System.UInt32,System.String,UnityEngine.GameObject)
extern "C"  void VRTK_ControllerInteract_ListenerExample_DebugLogger_m2183643895 (VRTK_ControllerInteract_ListenerExample_t3727600851 * __this, uint32_t ___index0, String_t* ___action1, GameObject_t1756533147 * ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerInteract_ListenerExample::DoInteractTouch(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_ControllerInteract_ListenerExample_DoInteractTouch_m781813511 (VRTK_ControllerInteract_ListenerExample_t3727600851 * __this, Il2CppObject * ___sender0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerInteract_ListenerExample::DoInteractUntouch(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_ControllerInteract_ListenerExample_DoInteractUntouch_m465846610 (VRTK_ControllerInteract_ListenerExample_t3727600851 * __this, Il2CppObject * ___sender0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerInteract_ListenerExample::DoInteractGrab(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_ControllerInteract_ListenerExample_DoInteractGrab_m2402740010 (VRTK_ControllerInteract_ListenerExample_t3727600851 * __this, Il2CppObject * ___sender0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.VRTK_ControllerInteract_ListenerExample::DoInteractUngrab(System.Object,VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_ControllerInteract_ListenerExample_DoInteractUngrab_m3592646383 (VRTK_ControllerInteract_ListenerExample_t3727600851 * __this, Il2CppObject * ___sender0, ObjectInteractEventArgs_t771291242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
