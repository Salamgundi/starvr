﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach
struct VRTK_BaseGrabAttach_t3487134318;
// VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction
struct VRTK_BaseGrabAction_t4095736311;
// VRTK.InteractableObjectEventHandler
struct InteractableObjectEventHandler_t940909295;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.Highlighters.VRTK_BaseHighlighter
struct VRTK_BaseHighlighter_t3110203740;
// VRTK.VRTK_SnapDropZone
struct VRTK_SnapDropZone_t1948041105;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject_All2794712781.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject_Vali960740973.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_ControllerEvents_Butto1389393715.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_InteractableObject
struct  VRTK_InteractableObject_t2604188111  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRTK.VRTK_InteractableObject::disableWhenIdle
	bool ___disableWhenIdle_2;
	// UnityEngine.Color VRTK.VRTK_InteractableObject::touchHighlightColor
	Color_t2020392075  ___touchHighlightColor_3;
	// VRTK.VRTK_InteractableObject/AllowedController VRTK.VRTK_InteractableObject::allowedTouchControllers
	int32_t ___allowedTouchControllers_4;
	// System.Boolean VRTK.VRTK_InteractableObject::isGrabbable
	bool ___isGrabbable_5;
	// System.Boolean VRTK.VRTK_InteractableObject::holdButtonToGrab
	bool ___holdButtonToGrab_6;
	// System.Boolean VRTK.VRTK_InteractableObject::stayGrabbedOnTeleport
	bool ___stayGrabbedOnTeleport_7;
	// VRTK.VRTK_InteractableObject/ValidDropTypes VRTK.VRTK_InteractableObject::validDrop
	int32_t ___validDrop_8;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_InteractableObject::grabOverrideButton
	int32_t ___grabOverrideButton_9;
	// VRTK.VRTK_InteractableObject/AllowedController VRTK.VRTK_InteractableObject::allowedGrabControllers
	int32_t ___allowedGrabControllers_10;
	// VRTK.GrabAttachMechanics.VRTK_BaseGrabAttach VRTK.VRTK_InteractableObject::grabAttachMechanicScript
	VRTK_BaseGrabAttach_t3487134318 * ___grabAttachMechanicScript_11;
	// VRTK.SecondaryControllerGrabActions.VRTK_BaseGrabAction VRTK.VRTK_InteractableObject::secondaryGrabActionScript
	VRTK_BaseGrabAction_t4095736311 * ___secondaryGrabActionScript_12;
	// System.Boolean VRTK.VRTK_InteractableObject::isUsable
	bool ___isUsable_13;
	// System.Boolean VRTK.VRTK_InteractableObject::holdButtonToUse
	bool ___holdButtonToUse_14;
	// System.Boolean VRTK.VRTK_InteractableObject::useOnlyIfGrabbed
	bool ___useOnlyIfGrabbed_15;
	// System.Boolean VRTK.VRTK_InteractableObject::pointerActivatesUseAction
	bool ___pointerActivatesUseAction_16;
	// VRTK.VRTK_ControllerEvents/ButtonAlias VRTK.VRTK_InteractableObject::useOverrideButton
	int32_t ___useOverrideButton_17;
	// VRTK.VRTK_InteractableObject/AllowedController VRTK.VRTK_InteractableObject::allowedUseControllers
	int32_t ___allowedUseControllers_18;
	// VRTK.InteractableObjectEventHandler VRTK.VRTK_InteractableObject::InteractableObjectTouched
	InteractableObjectEventHandler_t940909295 * ___InteractableObjectTouched_19;
	// VRTK.InteractableObjectEventHandler VRTK.VRTK_InteractableObject::InteractableObjectUntouched
	InteractableObjectEventHandler_t940909295 * ___InteractableObjectUntouched_20;
	// VRTK.InteractableObjectEventHandler VRTK.VRTK_InteractableObject::InteractableObjectGrabbed
	InteractableObjectEventHandler_t940909295 * ___InteractableObjectGrabbed_21;
	// VRTK.InteractableObjectEventHandler VRTK.VRTK_InteractableObject::InteractableObjectUngrabbed
	InteractableObjectEventHandler_t940909295 * ___InteractableObjectUngrabbed_22;
	// VRTK.InteractableObjectEventHandler VRTK.VRTK_InteractableObject::InteractableObjectUsed
	InteractableObjectEventHandler_t940909295 * ___InteractableObjectUsed_23;
	// VRTK.InteractableObjectEventHandler VRTK.VRTK_InteractableObject::InteractableObjectUnused
	InteractableObjectEventHandler_t940909295 * ___InteractableObjectUnused_24;
	// System.Int32 VRTK.VRTK_InteractableObject::usingState
	int32_t ___usingState_25;
	// UnityEngine.Rigidbody VRTK.VRTK_InteractableObject::interactableRigidbody
	Rigidbody_t4233889191 * ___interactableRigidbody_26;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> VRTK.VRTK_InteractableObject::touchingObjects
	List_1_t1125654279 * ___touchingObjects_27;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> VRTK.VRTK_InteractableObject::grabbingObjects
	List_1_t1125654279 * ___grabbingObjects_28;
	// UnityEngine.GameObject VRTK.VRTK_InteractableObject::usingObject
	GameObject_t1756533147 * ___usingObject_29;
	// UnityEngine.Transform VRTK.VRTK_InteractableObject::trackPoint
	Transform_t3275118058 * ___trackPoint_30;
	// System.Boolean VRTK.VRTK_InteractableObject::customTrackPoint
	bool ___customTrackPoint_31;
	// UnityEngine.Transform VRTK.VRTK_InteractableObject::primaryControllerAttachPoint
	Transform_t3275118058 * ___primaryControllerAttachPoint_32;
	// UnityEngine.Transform VRTK.VRTK_InteractableObject::secondaryControllerAttachPoint
	Transform_t3275118058 * ___secondaryControllerAttachPoint_33;
	// UnityEngine.Transform VRTK.VRTK_InteractableObject::previousParent
	Transform_t3275118058 * ___previousParent_34;
	// System.Boolean VRTK.VRTK_InteractableObject::previousKinematicState
	bool ___previousKinematicState_35;
	// System.Boolean VRTK.VRTK_InteractableObject::previousIsGrabbable
	bool ___previousIsGrabbable_36;
	// System.Boolean VRTK.VRTK_InteractableObject::forcedDropped
	bool ___forcedDropped_37;
	// System.Boolean VRTK.VRTK_InteractableObject::forceDisabled
	bool ___forceDisabled_38;
	// VRTK.Highlighters.VRTK_BaseHighlighter VRTK.VRTK_InteractableObject::objectHighlighter
	VRTK_BaseHighlighter_t3110203740 * ___objectHighlighter_39;
	// System.Boolean VRTK.VRTK_InteractableObject::autoHighlighter
	bool ___autoHighlighter_40;
	// System.Boolean VRTK.VRTK_InteractableObject::hoveredOverSnapDropZone
	bool ___hoveredOverSnapDropZone_41;
	// System.Boolean VRTK.VRTK_InteractableObject::snappedInSnapDropZone
	bool ___snappedInSnapDropZone_42;
	// VRTK.VRTK_SnapDropZone VRTK.VRTK_InteractableObject::storedSnapDropZone
	VRTK_SnapDropZone_t1948041105 * ___storedSnapDropZone_43;
	// UnityEngine.Vector3 VRTK.VRTK_InteractableObject::previousLocalScale
	Vector3_t2243707580  ___previousLocalScale_44;

public:
	inline static int32_t get_offset_of_disableWhenIdle_2() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___disableWhenIdle_2)); }
	inline bool get_disableWhenIdle_2() const { return ___disableWhenIdle_2; }
	inline bool* get_address_of_disableWhenIdle_2() { return &___disableWhenIdle_2; }
	inline void set_disableWhenIdle_2(bool value)
	{
		___disableWhenIdle_2 = value;
	}

	inline static int32_t get_offset_of_touchHighlightColor_3() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___touchHighlightColor_3)); }
	inline Color_t2020392075  get_touchHighlightColor_3() const { return ___touchHighlightColor_3; }
	inline Color_t2020392075 * get_address_of_touchHighlightColor_3() { return &___touchHighlightColor_3; }
	inline void set_touchHighlightColor_3(Color_t2020392075  value)
	{
		___touchHighlightColor_3 = value;
	}

	inline static int32_t get_offset_of_allowedTouchControllers_4() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___allowedTouchControllers_4)); }
	inline int32_t get_allowedTouchControllers_4() const { return ___allowedTouchControllers_4; }
	inline int32_t* get_address_of_allowedTouchControllers_4() { return &___allowedTouchControllers_4; }
	inline void set_allowedTouchControllers_4(int32_t value)
	{
		___allowedTouchControllers_4 = value;
	}

	inline static int32_t get_offset_of_isGrabbable_5() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___isGrabbable_5)); }
	inline bool get_isGrabbable_5() const { return ___isGrabbable_5; }
	inline bool* get_address_of_isGrabbable_5() { return &___isGrabbable_5; }
	inline void set_isGrabbable_5(bool value)
	{
		___isGrabbable_5 = value;
	}

	inline static int32_t get_offset_of_holdButtonToGrab_6() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___holdButtonToGrab_6)); }
	inline bool get_holdButtonToGrab_6() const { return ___holdButtonToGrab_6; }
	inline bool* get_address_of_holdButtonToGrab_6() { return &___holdButtonToGrab_6; }
	inline void set_holdButtonToGrab_6(bool value)
	{
		___holdButtonToGrab_6 = value;
	}

	inline static int32_t get_offset_of_stayGrabbedOnTeleport_7() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___stayGrabbedOnTeleport_7)); }
	inline bool get_stayGrabbedOnTeleport_7() const { return ___stayGrabbedOnTeleport_7; }
	inline bool* get_address_of_stayGrabbedOnTeleport_7() { return &___stayGrabbedOnTeleport_7; }
	inline void set_stayGrabbedOnTeleport_7(bool value)
	{
		___stayGrabbedOnTeleport_7 = value;
	}

	inline static int32_t get_offset_of_validDrop_8() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___validDrop_8)); }
	inline int32_t get_validDrop_8() const { return ___validDrop_8; }
	inline int32_t* get_address_of_validDrop_8() { return &___validDrop_8; }
	inline void set_validDrop_8(int32_t value)
	{
		___validDrop_8 = value;
	}

	inline static int32_t get_offset_of_grabOverrideButton_9() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___grabOverrideButton_9)); }
	inline int32_t get_grabOverrideButton_9() const { return ___grabOverrideButton_9; }
	inline int32_t* get_address_of_grabOverrideButton_9() { return &___grabOverrideButton_9; }
	inline void set_grabOverrideButton_9(int32_t value)
	{
		___grabOverrideButton_9 = value;
	}

	inline static int32_t get_offset_of_allowedGrabControllers_10() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___allowedGrabControllers_10)); }
	inline int32_t get_allowedGrabControllers_10() const { return ___allowedGrabControllers_10; }
	inline int32_t* get_address_of_allowedGrabControllers_10() { return &___allowedGrabControllers_10; }
	inline void set_allowedGrabControllers_10(int32_t value)
	{
		___allowedGrabControllers_10 = value;
	}

	inline static int32_t get_offset_of_grabAttachMechanicScript_11() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___grabAttachMechanicScript_11)); }
	inline VRTK_BaseGrabAttach_t3487134318 * get_grabAttachMechanicScript_11() const { return ___grabAttachMechanicScript_11; }
	inline VRTK_BaseGrabAttach_t3487134318 ** get_address_of_grabAttachMechanicScript_11() { return &___grabAttachMechanicScript_11; }
	inline void set_grabAttachMechanicScript_11(VRTK_BaseGrabAttach_t3487134318 * value)
	{
		___grabAttachMechanicScript_11 = value;
		Il2CppCodeGenWriteBarrier(&___grabAttachMechanicScript_11, value);
	}

	inline static int32_t get_offset_of_secondaryGrabActionScript_12() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___secondaryGrabActionScript_12)); }
	inline VRTK_BaseGrabAction_t4095736311 * get_secondaryGrabActionScript_12() const { return ___secondaryGrabActionScript_12; }
	inline VRTK_BaseGrabAction_t4095736311 ** get_address_of_secondaryGrabActionScript_12() { return &___secondaryGrabActionScript_12; }
	inline void set_secondaryGrabActionScript_12(VRTK_BaseGrabAction_t4095736311 * value)
	{
		___secondaryGrabActionScript_12 = value;
		Il2CppCodeGenWriteBarrier(&___secondaryGrabActionScript_12, value);
	}

	inline static int32_t get_offset_of_isUsable_13() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___isUsable_13)); }
	inline bool get_isUsable_13() const { return ___isUsable_13; }
	inline bool* get_address_of_isUsable_13() { return &___isUsable_13; }
	inline void set_isUsable_13(bool value)
	{
		___isUsable_13 = value;
	}

	inline static int32_t get_offset_of_holdButtonToUse_14() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___holdButtonToUse_14)); }
	inline bool get_holdButtonToUse_14() const { return ___holdButtonToUse_14; }
	inline bool* get_address_of_holdButtonToUse_14() { return &___holdButtonToUse_14; }
	inline void set_holdButtonToUse_14(bool value)
	{
		___holdButtonToUse_14 = value;
	}

	inline static int32_t get_offset_of_useOnlyIfGrabbed_15() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___useOnlyIfGrabbed_15)); }
	inline bool get_useOnlyIfGrabbed_15() const { return ___useOnlyIfGrabbed_15; }
	inline bool* get_address_of_useOnlyIfGrabbed_15() { return &___useOnlyIfGrabbed_15; }
	inline void set_useOnlyIfGrabbed_15(bool value)
	{
		___useOnlyIfGrabbed_15 = value;
	}

	inline static int32_t get_offset_of_pointerActivatesUseAction_16() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___pointerActivatesUseAction_16)); }
	inline bool get_pointerActivatesUseAction_16() const { return ___pointerActivatesUseAction_16; }
	inline bool* get_address_of_pointerActivatesUseAction_16() { return &___pointerActivatesUseAction_16; }
	inline void set_pointerActivatesUseAction_16(bool value)
	{
		___pointerActivatesUseAction_16 = value;
	}

	inline static int32_t get_offset_of_useOverrideButton_17() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___useOverrideButton_17)); }
	inline int32_t get_useOverrideButton_17() const { return ___useOverrideButton_17; }
	inline int32_t* get_address_of_useOverrideButton_17() { return &___useOverrideButton_17; }
	inline void set_useOverrideButton_17(int32_t value)
	{
		___useOverrideButton_17 = value;
	}

	inline static int32_t get_offset_of_allowedUseControllers_18() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___allowedUseControllers_18)); }
	inline int32_t get_allowedUseControllers_18() const { return ___allowedUseControllers_18; }
	inline int32_t* get_address_of_allowedUseControllers_18() { return &___allowedUseControllers_18; }
	inline void set_allowedUseControllers_18(int32_t value)
	{
		___allowedUseControllers_18 = value;
	}

	inline static int32_t get_offset_of_InteractableObjectTouched_19() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___InteractableObjectTouched_19)); }
	inline InteractableObjectEventHandler_t940909295 * get_InteractableObjectTouched_19() const { return ___InteractableObjectTouched_19; }
	inline InteractableObjectEventHandler_t940909295 ** get_address_of_InteractableObjectTouched_19() { return &___InteractableObjectTouched_19; }
	inline void set_InteractableObjectTouched_19(InteractableObjectEventHandler_t940909295 * value)
	{
		___InteractableObjectTouched_19 = value;
		Il2CppCodeGenWriteBarrier(&___InteractableObjectTouched_19, value);
	}

	inline static int32_t get_offset_of_InteractableObjectUntouched_20() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___InteractableObjectUntouched_20)); }
	inline InteractableObjectEventHandler_t940909295 * get_InteractableObjectUntouched_20() const { return ___InteractableObjectUntouched_20; }
	inline InteractableObjectEventHandler_t940909295 ** get_address_of_InteractableObjectUntouched_20() { return &___InteractableObjectUntouched_20; }
	inline void set_InteractableObjectUntouched_20(InteractableObjectEventHandler_t940909295 * value)
	{
		___InteractableObjectUntouched_20 = value;
		Il2CppCodeGenWriteBarrier(&___InteractableObjectUntouched_20, value);
	}

	inline static int32_t get_offset_of_InteractableObjectGrabbed_21() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___InteractableObjectGrabbed_21)); }
	inline InteractableObjectEventHandler_t940909295 * get_InteractableObjectGrabbed_21() const { return ___InteractableObjectGrabbed_21; }
	inline InteractableObjectEventHandler_t940909295 ** get_address_of_InteractableObjectGrabbed_21() { return &___InteractableObjectGrabbed_21; }
	inline void set_InteractableObjectGrabbed_21(InteractableObjectEventHandler_t940909295 * value)
	{
		___InteractableObjectGrabbed_21 = value;
		Il2CppCodeGenWriteBarrier(&___InteractableObjectGrabbed_21, value);
	}

	inline static int32_t get_offset_of_InteractableObjectUngrabbed_22() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___InteractableObjectUngrabbed_22)); }
	inline InteractableObjectEventHandler_t940909295 * get_InteractableObjectUngrabbed_22() const { return ___InteractableObjectUngrabbed_22; }
	inline InteractableObjectEventHandler_t940909295 ** get_address_of_InteractableObjectUngrabbed_22() { return &___InteractableObjectUngrabbed_22; }
	inline void set_InteractableObjectUngrabbed_22(InteractableObjectEventHandler_t940909295 * value)
	{
		___InteractableObjectUngrabbed_22 = value;
		Il2CppCodeGenWriteBarrier(&___InteractableObjectUngrabbed_22, value);
	}

	inline static int32_t get_offset_of_InteractableObjectUsed_23() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___InteractableObjectUsed_23)); }
	inline InteractableObjectEventHandler_t940909295 * get_InteractableObjectUsed_23() const { return ___InteractableObjectUsed_23; }
	inline InteractableObjectEventHandler_t940909295 ** get_address_of_InteractableObjectUsed_23() { return &___InteractableObjectUsed_23; }
	inline void set_InteractableObjectUsed_23(InteractableObjectEventHandler_t940909295 * value)
	{
		___InteractableObjectUsed_23 = value;
		Il2CppCodeGenWriteBarrier(&___InteractableObjectUsed_23, value);
	}

	inline static int32_t get_offset_of_InteractableObjectUnused_24() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___InteractableObjectUnused_24)); }
	inline InteractableObjectEventHandler_t940909295 * get_InteractableObjectUnused_24() const { return ___InteractableObjectUnused_24; }
	inline InteractableObjectEventHandler_t940909295 ** get_address_of_InteractableObjectUnused_24() { return &___InteractableObjectUnused_24; }
	inline void set_InteractableObjectUnused_24(InteractableObjectEventHandler_t940909295 * value)
	{
		___InteractableObjectUnused_24 = value;
		Il2CppCodeGenWriteBarrier(&___InteractableObjectUnused_24, value);
	}

	inline static int32_t get_offset_of_usingState_25() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___usingState_25)); }
	inline int32_t get_usingState_25() const { return ___usingState_25; }
	inline int32_t* get_address_of_usingState_25() { return &___usingState_25; }
	inline void set_usingState_25(int32_t value)
	{
		___usingState_25 = value;
	}

	inline static int32_t get_offset_of_interactableRigidbody_26() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___interactableRigidbody_26)); }
	inline Rigidbody_t4233889191 * get_interactableRigidbody_26() const { return ___interactableRigidbody_26; }
	inline Rigidbody_t4233889191 ** get_address_of_interactableRigidbody_26() { return &___interactableRigidbody_26; }
	inline void set_interactableRigidbody_26(Rigidbody_t4233889191 * value)
	{
		___interactableRigidbody_26 = value;
		Il2CppCodeGenWriteBarrier(&___interactableRigidbody_26, value);
	}

	inline static int32_t get_offset_of_touchingObjects_27() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___touchingObjects_27)); }
	inline List_1_t1125654279 * get_touchingObjects_27() const { return ___touchingObjects_27; }
	inline List_1_t1125654279 ** get_address_of_touchingObjects_27() { return &___touchingObjects_27; }
	inline void set_touchingObjects_27(List_1_t1125654279 * value)
	{
		___touchingObjects_27 = value;
		Il2CppCodeGenWriteBarrier(&___touchingObjects_27, value);
	}

	inline static int32_t get_offset_of_grabbingObjects_28() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___grabbingObjects_28)); }
	inline List_1_t1125654279 * get_grabbingObjects_28() const { return ___grabbingObjects_28; }
	inline List_1_t1125654279 ** get_address_of_grabbingObjects_28() { return &___grabbingObjects_28; }
	inline void set_grabbingObjects_28(List_1_t1125654279 * value)
	{
		___grabbingObjects_28 = value;
		Il2CppCodeGenWriteBarrier(&___grabbingObjects_28, value);
	}

	inline static int32_t get_offset_of_usingObject_29() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___usingObject_29)); }
	inline GameObject_t1756533147 * get_usingObject_29() const { return ___usingObject_29; }
	inline GameObject_t1756533147 ** get_address_of_usingObject_29() { return &___usingObject_29; }
	inline void set_usingObject_29(GameObject_t1756533147 * value)
	{
		___usingObject_29 = value;
		Il2CppCodeGenWriteBarrier(&___usingObject_29, value);
	}

	inline static int32_t get_offset_of_trackPoint_30() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___trackPoint_30)); }
	inline Transform_t3275118058 * get_trackPoint_30() const { return ___trackPoint_30; }
	inline Transform_t3275118058 ** get_address_of_trackPoint_30() { return &___trackPoint_30; }
	inline void set_trackPoint_30(Transform_t3275118058 * value)
	{
		___trackPoint_30 = value;
		Il2CppCodeGenWriteBarrier(&___trackPoint_30, value);
	}

	inline static int32_t get_offset_of_customTrackPoint_31() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___customTrackPoint_31)); }
	inline bool get_customTrackPoint_31() const { return ___customTrackPoint_31; }
	inline bool* get_address_of_customTrackPoint_31() { return &___customTrackPoint_31; }
	inline void set_customTrackPoint_31(bool value)
	{
		___customTrackPoint_31 = value;
	}

	inline static int32_t get_offset_of_primaryControllerAttachPoint_32() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___primaryControllerAttachPoint_32)); }
	inline Transform_t3275118058 * get_primaryControllerAttachPoint_32() const { return ___primaryControllerAttachPoint_32; }
	inline Transform_t3275118058 ** get_address_of_primaryControllerAttachPoint_32() { return &___primaryControllerAttachPoint_32; }
	inline void set_primaryControllerAttachPoint_32(Transform_t3275118058 * value)
	{
		___primaryControllerAttachPoint_32 = value;
		Il2CppCodeGenWriteBarrier(&___primaryControllerAttachPoint_32, value);
	}

	inline static int32_t get_offset_of_secondaryControllerAttachPoint_33() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___secondaryControllerAttachPoint_33)); }
	inline Transform_t3275118058 * get_secondaryControllerAttachPoint_33() const { return ___secondaryControllerAttachPoint_33; }
	inline Transform_t3275118058 ** get_address_of_secondaryControllerAttachPoint_33() { return &___secondaryControllerAttachPoint_33; }
	inline void set_secondaryControllerAttachPoint_33(Transform_t3275118058 * value)
	{
		___secondaryControllerAttachPoint_33 = value;
		Il2CppCodeGenWriteBarrier(&___secondaryControllerAttachPoint_33, value);
	}

	inline static int32_t get_offset_of_previousParent_34() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___previousParent_34)); }
	inline Transform_t3275118058 * get_previousParent_34() const { return ___previousParent_34; }
	inline Transform_t3275118058 ** get_address_of_previousParent_34() { return &___previousParent_34; }
	inline void set_previousParent_34(Transform_t3275118058 * value)
	{
		___previousParent_34 = value;
		Il2CppCodeGenWriteBarrier(&___previousParent_34, value);
	}

	inline static int32_t get_offset_of_previousKinematicState_35() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___previousKinematicState_35)); }
	inline bool get_previousKinematicState_35() const { return ___previousKinematicState_35; }
	inline bool* get_address_of_previousKinematicState_35() { return &___previousKinematicState_35; }
	inline void set_previousKinematicState_35(bool value)
	{
		___previousKinematicState_35 = value;
	}

	inline static int32_t get_offset_of_previousIsGrabbable_36() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___previousIsGrabbable_36)); }
	inline bool get_previousIsGrabbable_36() const { return ___previousIsGrabbable_36; }
	inline bool* get_address_of_previousIsGrabbable_36() { return &___previousIsGrabbable_36; }
	inline void set_previousIsGrabbable_36(bool value)
	{
		___previousIsGrabbable_36 = value;
	}

	inline static int32_t get_offset_of_forcedDropped_37() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___forcedDropped_37)); }
	inline bool get_forcedDropped_37() const { return ___forcedDropped_37; }
	inline bool* get_address_of_forcedDropped_37() { return &___forcedDropped_37; }
	inline void set_forcedDropped_37(bool value)
	{
		___forcedDropped_37 = value;
	}

	inline static int32_t get_offset_of_forceDisabled_38() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___forceDisabled_38)); }
	inline bool get_forceDisabled_38() const { return ___forceDisabled_38; }
	inline bool* get_address_of_forceDisabled_38() { return &___forceDisabled_38; }
	inline void set_forceDisabled_38(bool value)
	{
		___forceDisabled_38 = value;
	}

	inline static int32_t get_offset_of_objectHighlighter_39() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___objectHighlighter_39)); }
	inline VRTK_BaseHighlighter_t3110203740 * get_objectHighlighter_39() const { return ___objectHighlighter_39; }
	inline VRTK_BaseHighlighter_t3110203740 ** get_address_of_objectHighlighter_39() { return &___objectHighlighter_39; }
	inline void set_objectHighlighter_39(VRTK_BaseHighlighter_t3110203740 * value)
	{
		___objectHighlighter_39 = value;
		Il2CppCodeGenWriteBarrier(&___objectHighlighter_39, value);
	}

	inline static int32_t get_offset_of_autoHighlighter_40() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___autoHighlighter_40)); }
	inline bool get_autoHighlighter_40() const { return ___autoHighlighter_40; }
	inline bool* get_address_of_autoHighlighter_40() { return &___autoHighlighter_40; }
	inline void set_autoHighlighter_40(bool value)
	{
		___autoHighlighter_40 = value;
	}

	inline static int32_t get_offset_of_hoveredOverSnapDropZone_41() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___hoveredOverSnapDropZone_41)); }
	inline bool get_hoveredOverSnapDropZone_41() const { return ___hoveredOverSnapDropZone_41; }
	inline bool* get_address_of_hoveredOverSnapDropZone_41() { return &___hoveredOverSnapDropZone_41; }
	inline void set_hoveredOverSnapDropZone_41(bool value)
	{
		___hoveredOverSnapDropZone_41 = value;
	}

	inline static int32_t get_offset_of_snappedInSnapDropZone_42() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___snappedInSnapDropZone_42)); }
	inline bool get_snappedInSnapDropZone_42() const { return ___snappedInSnapDropZone_42; }
	inline bool* get_address_of_snappedInSnapDropZone_42() { return &___snappedInSnapDropZone_42; }
	inline void set_snappedInSnapDropZone_42(bool value)
	{
		___snappedInSnapDropZone_42 = value;
	}

	inline static int32_t get_offset_of_storedSnapDropZone_43() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___storedSnapDropZone_43)); }
	inline VRTK_SnapDropZone_t1948041105 * get_storedSnapDropZone_43() const { return ___storedSnapDropZone_43; }
	inline VRTK_SnapDropZone_t1948041105 ** get_address_of_storedSnapDropZone_43() { return &___storedSnapDropZone_43; }
	inline void set_storedSnapDropZone_43(VRTK_SnapDropZone_t1948041105 * value)
	{
		___storedSnapDropZone_43 = value;
		Il2CppCodeGenWriteBarrier(&___storedSnapDropZone_43, value);
	}

	inline static int32_t get_offset_of_previousLocalScale_44() { return static_cast<int32_t>(offsetof(VRTK_InteractableObject_t2604188111, ___previousLocalScale_44)); }
	inline Vector3_t2243707580  get_previousLocalScale_44() const { return ___previousLocalScale_44; }
	inline Vector3_t2243707580 * get_address_of_previousLocalScale_44() { return &___previousLocalScale_44; }
	inline void set_previousLocalScale_44(Vector3_t2243707580  value)
	{
		___previousLocalScale_44 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
