﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Collider
struct Collider_t3497673348;
// Valve.VR.InteractionSystem.LinearMapping
struct LinearMapping_t810676855;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Valve.VR.InteractionSystem.Hand
struct Hand_t379716353;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Circu2696941949.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.CircularDrive
struct  CircularDrive_t2658775813  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.CircularDrive/Axis_t Valve.VR.InteractionSystem.CircularDrive::axisOfRotation
	int32_t ___axisOfRotation_2;
	// UnityEngine.Collider Valve.VR.InteractionSystem.CircularDrive::childCollider
	Collider_t3497673348 * ___childCollider_3;
	// Valve.VR.InteractionSystem.LinearMapping Valve.VR.InteractionSystem.CircularDrive::linearMapping
	LinearMapping_t810676855 * ___linearMapping_4;
	// System.Boolean Valve.VR.InteractionSystem.CircularDrive::hoverLock
	bool ___hoverLock_5;
	// System.Boolean Valve.VR.InteractionSystem.CircularDrive::limited
	bool ___limited_6;
	// UnityEngine.Vector2 Valve.VR.InteractionSystem.CircularDrive::frozenDistanceMinMaxThreshold
	Vector2_t2243707579  ___frozenDistanceMinMaxThreshold_7;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.CircularDrive::onFrozenDistanceThreshold
	UnityEvent_t408735097 * ___onFrozenDistanceThreshold_8;
	// System.Single Valve.VR.InteractionSystem.CircularDrive::minAngle
	float ___minAngle_9;
	// System.Boolean Valve.VR.InteractionSystem.CircularDrive::freezeOnMin
	bool ___freezeOnMin_10;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.CircularDrive::onMinAngle
	UnityEvent_t408735097 * ___onMinAngle_11;
	// System.Single Valve.VR.InteractionSystem.CircularDrive::maxAngle
	float ___maxAngle_12;
	// System.Boolean Valve.VR.InteractionSystem.CircularDrive::freezeOnMax
	bool ___freezeOnMax_13;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.CircularDrive::onMaxAngle
	UnityEvent_t408735097 * ___onMaxAngle_14;
	// System.Boolean Valve.VR.InteractionSystem.CircularDrive::forceStart
	bool ___forceStart_15;
	// System.Single Valve.VR.InteractionSystem.CircularDrive::startAngle
	float ___startAngle_16;
	// System.Boolean Valve.VR.InteractionSystem.CircularDrive::rotateGameObject
	bool ___rotateGameObject_17;
	// System.Boolean Valve.VR.InteractionSystem.CircularDrive::debugPath
	bool ___debugPath_18;
	// System.Int32 Valve.VR.InteractionSystem.CircularDrive::dbgPathLimit
	int32_t ___dbgPathLimit_19;
	// UnityEngine.TextMesh Valve.VR.InteractionSystem.CircularDrive::debugText
	TextMesh_t1641806576 * ___debugText_20;
	// System.Single Valve.VR.InteractionSystem.CircularDrive::outAngle
	float ___outAngle_21;
	// UnityEngine.Quaternion Valve.VR.InteractionSystem.CircularDrive::start
	Quaternion_t4030073918  ___start_22;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.CircularDrive::worldPlaneNormal
	Vector3_t2243707580  ___worldPlaneNormal_23;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.CircularDrive::localPlaneNormal
	Vector3_t2243707580  ___localPlaneNormal_24;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.CircularDrive::lastHandProjected
	Vector3_t2243707580  ___lastHandProjected_25;
	// UnityEngine.Color Valve.VR.InteractionSystem.CircularDrive::red
	Color_t2020392075  ___red_26;
	// UnityEngine.Color Valve.VR.InteractionSystem.CircularDrive::green
	Color_t2020392075  ___green_27;
	// UnityEngine.GameObject[] Valve.VR.InteractionSystem.CircularDrive::dbgHandObjects
	GameObjectU5BU5D_t3057952154* ___dbgHandObjects_28;
	// UnityEngine.GameObject[] Valve.VR.InteractionSystem.CircularDrive::dbgProjObjects
	GameObjectU5BU5D_t3057952154* ___dbgProjObjects_29;
	// UnityEngine.GameObject Valve.VR.InteractionSystem.CircularDrive::dbgObjectsParent
	GameObject_t1756533147 * ___dbgObjectsParent_30;
	// System.Int32 Valve.VR.InteractionSystem.CircularDrive::dbgObjectCount
	int32_t ___dbgObjectCount_31;
	// System.Int32 Valve.VR.InteractionSystem.CircularDrive::dbgObjectIndex
	int32_t ___dbgObjectIndex_32;
	// System.Boolean Valve.VR.InteractionSystem.CircularDrive::driving
	bool ___driving_33;
	// System.Single Valve.VR.InteractionSystem.CircularDrive::minMaxAngularThreshold
	float ___minMaxAngularThreshold_34;
	// System.Boolean Valve.VR.InteractionSystem.CircularDrive::frozen
	bool ___frozen_35;
	// System.Single Valve.VR.InteractionSystem.CircularDrive::frozenAngle
	float ___frozenAngle_36;
	// UnityEngine.Vector3 Valve.VR.InteractionSystem.CircularDrive::frozenHandWorldPos
	Vector3_t2243707580  ___frozenHandWorldPos_37;
	// UnityEngine.Vector2 Valve.VR.InteractionSystem.CircularDrive::frozenSqDistanceMinMaxThreshold
	Vector2_t2243707579  ___frozenSqDistanceMinMaxThreshold_38;
	// Valve.VR.InteractionSystem.Hand Valve.VR.InteractionSystem.CircularDrive::handHoverLocked
	Hand_t379716353 * ___handHoverLocked_39;

public:
	inline static int32_t get_offset_of_axisOfRotation_2() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___axisOfRotation_2)); }
	inline int32_t get_axisOfRotation_2() const { return ___axisOfRotation_2; }
	inline int32_t* get_address_of_axisOfRotation_2() { return &___axisOfRotation_2; }
	inline void set_axisOfRotation_2(int32_t value)
	{
		___axisOfRotation_2 = value;
	}

	inline static int32_t get_offset_of_childCollider_3() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___childCollider_3)); }
	inline Collider_t3497673348 * get_childCollider_3() const { return ___childCollider_3; }
	inline Collider_t3497673348 ** get_address_of_childCollider_3() { return &___childCollider_3; }
	inline void set_childCollider_3(Collider_t3497673348 * value)
	{
		___childCollider_3 = value;
		Il2CppCodeGenWriteBarrier(&___childCollider_3, value);
	}

	inline static int32_t get_offset_of_linearMapping_4() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___linearMapping_4)); }
	inline LinearMapping_t810676855 * get_linearMapping_4() const { return ___linearMapping_4; }
	inline LinearMapping_t810676855 ** get_address_of_linearMapping_4() { return &___linearMapping_4; }
	inline void set_linearMapping_4(LinearMapping_t810676855 * value)
	{
		___linearMapping_4 = value;
		Il2CppCodeGenWriteBarrier(&___linearMapping_4, value);
	}

	inline static int32_t get_offset_of_hoverLock_5() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___hoverLock_5)); }
	inline bool get_hoverLock_5() const { return ___hoverLock_5; }
	inline bool* get_address_of_hoverLock_5() { return &___hoverLock_5; }
	inline void set_hoverLock_5(bool value)
	{
		___hoverLock_5 = value;
	}

	inline static int32_t get_offset_of_limited_6() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___limited_6)); }
	inline bool get_limited_6() const { return ___limited_6; }
	inline bool* get_address_of_limited_6() { return &___limited_6; }
	inline void set_limited_6(bool value)
	{
		___limited_6 = value;
	}

	inline static int32_t get_offset_of_frozenDistanceMinMaxThreshold_7() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___frozenDistanceMinMaxThreshold_7)); }
	inline Vector2_t2243707579  get_frozenDistanceMinMaxThreshold_7() const { return ___frozenDistanceMinMaxThreshold_7; }
	inline Vector2_t2243707579 * get_address_of_frozenDistanceMinMaxThreshold_7() { return &___frozenDistanceMinMaxThreshold_7; }
	inline void set_frozenDistanceMinMaxThreshold_7(Vector2_t2243707579  value)
	{
		___frozenDistanceMinMaxThreshold_7 = value;
	}

	inline static int32_t get_offset_of_onFrozenDistanceThreshold_8() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___onFrozenDistanceThreshold_8)); }
	inline UnityEvent_t408735097 * get_onFrozenDistanceThreshold_8() const { return ___onFrozenDistanceThreshold_8; }
	inline UnityEvent_t408735097 ** get_address_of_onFrozenDistanceThreshold_8() { return &___onFrozenDistanceThreshold_8; }
	inline void set_onFrozenDistanceThreshold_8(UnityEvent_t408735097 * value)
	{
		___onFrozenDistanceThreshold_8 = value;
		Il2CppCodeGenWriteBarrier(&___onFrozenDistanceThreshold_8, value);
	}

	inline static int32_t get_offset_of_minAngle_9() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___minAngle_9)); }
	inline float get_minAngle_9() const { return ___minAngle_9; }
	inline float* get_address_of_minAngle_9() { return &___minAngle_9; }
	inline void set_minAngle_9(float value)
	{
		___minAngle_9 = value;
	}

	inline static int32_t get_offset_of_freezeOnMin_10() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___freezeOnMin_10)); }
	inline bool get_freezeOnMin_10() const { return ___freezeOnMin_10; }
	inline bool* get_address_of_freezeOnMin_10() { return &___freezeOnMin_10; }
	inline void set_freezeOnMin_10(bool value)
	{
		___freezeOnMin_10 = value;
	}

	inline static int32_t get_offset_of_onMinAngle_11() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___onMinAngle_11)); }
	inline UnityEvent_t408735097 * get_onMinAngle_11() const { return ___onMinAngle_11; }
	inline UnityEvent_t408735097 ** get_address_of_onMinAngle_11() { return &___onMinAngle_11; }
	inline void set_onMinAngle_11(UnityEvent_t408735097 * value)
	{
		___onMinAngle_11 = value;
		Il2CppCodeGenWriteBarrier(&___onMinAngle_11, value);
	}

	inline static int32_t get_offset_of_maxAngle_12() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___maxAngle_12)); }
	inline float get_maxAngle_12() const { return ___maxAngle_12; }
	inline float* get_address_of_maxAngle_12() { return &___maxAngle_12; }
	inline void set_maxAngle_12(float value)
	{
		___maxAngle_12 = value;
	}

	inline static int32_t get_offset_of_freezeOnMax_13() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___freezeOnMax_13)); }
	inline bool get_freezeOnMax_13() const { return ___freezeOnMax_13; }
	inline bool* get_address_of_freezeOnMax_13() { return &___freezeOnMax_13; }
	inline void set_freezeOnMax_13(bool value)
	{
		___freezeOnMax_13 = value;
	}

	inline static int32_t get_offset_of_onMaxAngle_14() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___onMaxAngle_14)); }
	inline UnityEvent_t408735097 * get_onMaxAngle_14() const { return ___onMaxAngle_14; }
	inline UnityEvent_t408735097 ** get_address_of_onMaxAngle_14() { return &___onMaxAngle_14; }
	inline void set_onMaxAngle_14(UnityEvent_t408735097 * value)
	{
		___onMaxAngle_14 = value;
		Il2CppCodeGenWriteBarrier(&___onMaxAngle_14, value);
	}

	inline static int32_t get_offset_of_forceStart_15() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___forceStart_15)); }
	inline bool get_forceStart_15() const { return ___forceStart_15; }
	inline bool* get_address_of_forceStart_15() { return &___forceStart_15; }
	inline void set_forceStart_15(bool value)
	{
		___forceStart_15 = value;
	}

	inline static int32_t get_offset_of_startAngle_16() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___startAngle_16)); }
	inline float get_startAngle_16() const { return ___startAngle_16; }
	inline float* get_address_of_startAngle_16() { return &___startAngle_16; }
	inline void set_startAngle_16(float value)
	{
		___startAngle_16 = value;
	}

	inline static int32_t get_offset_of_rotateGameObject_17() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___rotateGameObject_17)); }
	inline bool get_rotateGameObject_17() const { return ___rotateGameObject_17; }
	inline bool* get_address_of_rotateGameObject_17() { return &___rotateGameObject_17; }
	inline void set_rotateGameObject_17(bool value)
	{
		___rotateGameObject_17 = value;
	}

	inline static int32_t get_offset_of_debugPath_18() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___debugPath_18)); }
	inline bool get_debugPath_18() const { return ___debugPath_18; }
	inline bool* get_address_of_debugPath_18() { return &___debugPath_18; }
	inline void set_debugPath_18(bool value)
	{
		___debugPath_18 = value;
	}

	inline static int32_t get_offset_of_dbgPathLimit_19() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___dbgPathLimit_19)); }
	inline int32_t get_dbgPathLimit_19() const { return ___dbgPathLimit_19; }
	inline int32_t* get_address_of_dbgPathLimit_19() { return &___dbgPathLimit_19; }
	inline void set_dbgPathLimit_19(int32_t value)
	{
		___dbgPathLimit_19 = value;
	}

	inline static int32_t get_offset_of_debugText_20() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___debugText_20)); }
	inline TextMesh_t1641806576 * get_debugText_20() const { return ___debugText_20; }
	inline TextMesh_t1641806576 ** get_address_of_debugText_20() { return &___debugText_20; }
	inline void set_debugText_20(TextMesh_t1641806576 * value)
	{
		___debugText_20 = value;
		Il2CppCodeGenWriteBarrier(&___debugText_20, value);
	}

	inline static int32_t get_offset_of_outAngle_21() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___outAngle_21)); }
	inline float get_outAngle_21() const { return ___outAngle_21; }
	inline float* get_address_of_outAngle_21() { return &___outAngle_21; }
	inline void set_outAngle_21(float value)
	{
		___outAngle_21 = value;
	}

	inline static int32_t get_offset_of_start_22() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___start_22)); }
	inline Quaternion_t4030073918  get_start_22() const { return ___start_22; }
	inline Quaternion_t4030073918 * get_address_of_start_22() { return &___start_22; }
	inline void set_start_22(Quaternion_t4030073918  value)
	{
		___start_22 = value;
	}

	inline static int32_t get_offset_of_worldPlaneNormal_23() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___worldPlaneNormal_23)); }
	inline Vector3_t2243707580  get_worldPlaneNormal_23() const { return ___worldPlaneNormal_23; }
	inline Vector3_t2243707580 * get_address_of_worldPlaneNormal_23() { return &___worldPlaneNormal_23; }
	inline void set_worldPlaneNormal_23(Vector3_t2243707580  value)
	{
		___worldPlaneNormal_23 = value;
	}

	inline static int32_t get_offset_of_localPlaneNormal_24() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___localPlaneNormal_24)); }
	inline Vector3_t2243707580  get_localPlaneNormal_24() const { return ___localPlaneNormal_24; }
	inline Vector3_t2243707580 * get_address_of_localPlaneNormal_24() { return &___localPlaneNormal_24; }
	inline void set_localPlaneNormal_24(Vector3_t2243707580  value)
	{
		___localPlaneNormal_24 = value;
	}

	inline static int32_t get_offset_of_lastHandProjected_25() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___lastHandProjected_25)); }
	inline Vector3_t2243707580  get_lastHandProjected_25() const { return ___lastHandProjected_25; }
	inline Vector3_t2243707580 * get_address_of_lastHandProjected_25() { return &___lastHandProjected_25; }
	inline void set_lastHandProjected_25(Vector3_t2243707580  value)
	{
		___lastHandProjected_25 = value;
	}

	inline static int32_t get_offset_of_red_26() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___red_26)); }
	inline Color_t2020392075  get_red_26() const { return ___red_26; }
	inline Color_t2020392075 * get_address_of_red_26() { return &___red_26; }
	inline void set_red_26(Color_t2020392075  value)
	{
		___red_26 = value;
	}

	inline static int32_t get_offset_of_green_27() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___green_27)); }
	inline Color_t2020392075  get_green_27() const { return ___green_27; }
	inline Color_t2020392075 * get_address_of_green_27() { return &___green_27; }
	inline void set_green_27(Color_t2020392075  value)
	{
		___green_27 = value;
	}

	inline static int32_t get_offset_of_dbgHandObjects_28() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___dbgHandObjects_28)); }
	inline GameObjectU5BU5D_t3057952154* get_dbgHandObjects_28() const { return ___dbgHandObjects_28; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_dbgHandObjects_28() { return &___dbgHandObjects_28; }
	inline void set_dbgHandObjects_28(GameObjectU5BU5D_t3057952154* value)
	{
		___dbgHandObjects_28 = value;
		Il2CppCodeGenWriteBarrier(&___dbgHandObjects_28, value);
	}

	inline static int32_t get_offset_of_dbgProjObjects_29() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___dbgProjObjects_29)); }
	inline GameObjectU5BU5D_t3057952154* get_dbgProjObjects_29() const { return ___dbgProjObjects_29; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_dbgProjObjects_29() { return &___dbgProjObjects_29; }
	inline void set_dbgProjObjects_29(GameObjectU5BU5D_t3057952154* value)
	{
		___dbgProjObjects_29 = value;
		Il2CppCodeGenWriteBarrier(&___dbgProjObjects_29, value);
	}

	inline static int32_t get_offset_of_dbgObjectsParent_30() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___dbgObjectsParent_30)); }
	inline GameObject_t1756533147 * get_dbgObjectsParent_30() const { return ___dbgObjectsParent_30; }
	inline GameObject_t1756533147 ** get_address_of_dbgObjectsParent_30() { return &___dbgObjectsParent_30; }
	inline void set_dbgObjectsParent_30(GameObject_t1756533147 * value)
	{
		___dbgObjectsParent_30 = value;
		Il2CppCodeGenWriteBarrier(&___dbgObjectsParent_30, value);
	}

	inline static int32_t get_offset_of_dbgObjectCount_31() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___dbgObjectCount_31)); }
	inline int32_t get_dbgObjectCount_31() const { return ___dbgObjectCount_31; }
	inline int32_t* get_address_of_dbgObjectCount_31() { return &___dbgObjectCount_31; }
	inline void set_dbgObjectCount_31(int32_t value)
	{
		___dbgObjectCount_31 = value;
	}

	inline static int32_t get_offset_of_dbgObjectIndex_32() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___dbgObjectIndex_32)); }
	inline int32_t get_dbgObjectIndex_32() const { return ___dbgObjectIndex_32; }
	inline int32_t* get_address_of_dbgObjectIndex_32() { return &___dbgObjectIndex_32; }
	inline void set_dbgObjectIndex_32(int32_t value)
	{
		___dbgObjectIndex_32 = value;
	}

	inline static int32_t get_offset_of_driving_33() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___driving_33)); }
	inline bool get_driving_33() const { return ___driving_33; }
	inline bool* get_address_of_driving_33() { return &___driving_33; }
	inline void set_driving_33(bool value)
	{
		___driving_33 = value;
	}

	inline static int32_t get_offset_of_minMaxAngularThreshold_34() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___minMaxAngularThreshold_34)); }
	inline float get_minMaxAngularThreshold_34() const { return ___minMaxAngularThreshold_34; }
	inline float* get_address_of_minMaxAngularThreshold_34() { return &___minMaxAngularThreshold_34; }
	inline void set_minMaxAngularThreshold_34(float value)
	{
		___minMaxAngularThreshold_34 = value;
	}

	inline static int32_t get_offset_of_frozen_35() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___frozen_35)); }
	inline bool get_frozen_35() const { return ___frozen_35; }
	inline bool* get_address_of_frozen_35() { return &___frozen_35; }
	inline void set_frozen_35(bool value)
	{
		___frozen_35 = value;
	}

	inline static int32_t get_offset_of_frozenAngle_36() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___frozenAngle_36)); }
	inline float get_frozenAngle_36() const { return ___frozenAngle_36; }
	inline float* get_address_of_frozenAngle_36() { return &___frozenAngle_36; }
	inline void set_frozenAngle_36(float value)
	{
		___frozenAngle_36 = value;
	}

	inline static int32_t get_offset_of_frozenHandWorldPos_37() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___frozenHandWorldPos_37)); }
	inline Vector3_t2243707580  get_frozenHandWorldPos_37() const { return ___frozenHandWorldPos_37; }
	inline Vector3_t2243707580 * get_address_of_frozenHandWorldPos_37() { return &___frozenHandWorldPos_37; }
	inline void set_frozenHandWorldPos_37(Vector3_t2243707580  value)
	{
		___frozenHandWorldPos_37 = value;
	}

	inline static int32_t get_offset_of_frozenSqDistanceMinMaxThreshold_38() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___frozenSqDistanceMinMaxThreshold_38)); }
	inline Vector2_t2243707579  get_frozenSqDistanceMinMaxThreshold_38() const { return ___frozenSqDistanceMinMaxThreshold_38; }
	inline Vector2_t2243707579 * get_address_of_frozenSqDistanceMinMaxThreshold_38() { return &___frozenSqDistanceMinMaxThreshold_38; }
	inline void set_frozenSqDistanceMinMaxThreshold_38(Vector2_t2243707579  value)
	{
		___frozenSqDistanceMinMaxThreshold_38 = value;
	}

	inline static int32_t get_offset_of_handHoverLocked_39() { return static_cast<int32_t>(offsetof(CircularDrive_t2658775813, ___handHoverLocked_39)); }
	inline Hand_t379716353 * get_handHoverLocked_39() const { return ___handHoverLocked_39; }
	inline Hand_t379716353 ** get_address_of_handHoverLocked_39() { return &___handHoverLocked_39; }
	inline void set_handHoverLocked_39(Hand_t379716353 * value)
	{
		___handHoverLocked_39 = value;
		Il2CppCodeGenWriteBarrier(&___handHoverLocked_39, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
