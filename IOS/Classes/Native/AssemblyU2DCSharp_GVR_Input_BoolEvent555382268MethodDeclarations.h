﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GVR.Input.BoolEvent
struct BoolEvent_t555382268;

#include "codegen/il2cpp-codegen.h"

// System.Void GVR.Input.BoolEvent::.ctor()
extern "C"  void BoolEvent__ctor_m3444785988 (BoolEvent_t555382268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
