﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_InteractTouch
struct VRTK_InteractTouch_t4022091061;
// VRTK.ObjectInteractEventHandler
struct ObjectInteractEventHandler_t1701902511;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629;
// UnityEngine.Collider
struct Collider_t3497673348;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventHandler1701902511.h"
#include "AssemblyU2DCSharp_VRTK_ObjectInteractEventArgs771291242.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"

// System.Void VRTK.VRTK_InteractTouch::.ctor()
extern "C"  void VRTK_InteractTouch__ctor_m1216724021 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::add_ControllerTouchInteractableObject(VRTK.ObjectInteractEventHandler)
extern "C"  void VRTK_InteractTouch_add_ControllerTouchInteractableObject_m2714111789 (VRTK_InteractTouch_t4022091061 * __this, ObjectInteractEventHandler_t1701902511 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::remove_ControllerTouchInteractableObject(VRTK.ObjectInteractEventHandler)
extern "C"  void VRTK_InteractTouch_remove_ControllerTouchInteractableObject_m3991208518 (VRTK_InteractTouch_t4022091061 * __this, ObjectInteractEventHandler_t1701902511 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::add_ControllerUntouchInteractableObject(VRTK.ObjectInteractEventHandler)
extern "C"  void VRTK_InteractTouch_add_ControllerUntouchInteractableObject_m3566965328 (VRTK_InteractTouch_t4022091061 * __this, ObjectInteractEventHandler_t1701902511 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::remove_ControllerUntouchInteractableObject(VRTK.ObjectInteractEventHandler)
extern "C"  void VRTK_InteractTouch_remove_ControllerUntouchInteractableObject_m1637300597 (VRTK_InteractTouch_t4022091061 * __this, ObjectInteractEventHandler_t1701902511 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::OnControllerTouchInteractableObject(VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_InteractTouch_OnControllerTouchInteractableObject_m36648889 (VRTK_InteractTouch_t4022091061 * __this, ObjectInteractEventArgs_t771291242  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::OnControllerUntouchInteractableObject(VRTK.ObjectInteractEventArgs)
extern "C"  void VRTK_InteractTouch_OnControllerUntouchInteractableObject_m4175843026 (VRTK_InteractTouch_t4022091061 * __this, ObjectInteractEventArgs_t771291242  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.ObjectInteractEventArgs VRTK.VRTK_InteractTouch::SetControllerInteractEvent(UnityEngine.GameObject)
extern "C"  ObjectInteractEventArgs_t771291242  VRTK_InteractTouch_SetControllerInteractEvent_m2914477211 (VRTK_InteractTouch_t4022091061 * __this, GameObject_t1756533147 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::ForceTouch(UnityEngine.GameObject)
extern "C"  void VRTK_InteractTouch_ForceTouch_m2220132341 (VRTK_InteractTouch_t4022091061 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_InteractTouch::GetTouchedObject()
extern "C"  GameObject_t1756533147 * VRTK_InteractTouch_GetTouchedObject_m1709922729 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractTouch::IsObjectInteractable(UnityEngine.GameObject)
extern "C"  bool VRTK_InteractTouch_IsObjectInteractable_m2731153668 (VRTK_InteractTouch_t4022091061 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::ToggleControllerRigidBody(System.Boolean,System.Boolean)
extern "C"  void VRTK_InteractTouch_ToggleControllerRigidBody_m1087513198 (VRTK_InteractTouch_t4022091061 * __this, bool ___state0, bool ___forceToggle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractTouch::IsRigidBodyActive()
extern "C"  bool VRTK_InteractTouch_IsRigidBodyActive_m4243075314 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractTouch::IsRigidBodyForcedActive()
extern "C"  bool VRTK_InteractTouch_IsRigidBodyForcedActive_m2002145119 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::ForceStopTouching()
extern "C"  void VRTK_InteractTouch_ForceStopTouching_m189838121 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] VRTK.VRTK_InteractTouch::ControllerColliders()
extern "C"  ColliderU5BU5D_t462843629* VRTK_InteractTouch_ControllerColliders_m3120100050 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::Awake()
extern "C"  void VRTK_InteractTouch_Awake_m1745470254 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::OnEnable()
extern "C"  void VRTK_InteractTouch_OnEnable_m884523397 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::OnDisable()
extern "C"  void VRTK_InteractTouch_OnDisable_m3006076614 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void VRTK_InteractTouch_OnTriggerEnter_m3494716889 (VRTK_InteractTouch_t4022091061 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::OnTriggerExit(UnityEngine.Collider)
extern "C"  void VRTK_InteractTouch_OnTriggerExit_m2148537449 (VRTK_InteractTouch_t4022091061 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::OnTriggerStay(UnityEngine.Collider)
extern "C"  void VRTK_InteractTouch_OnTriggerStay_m1547523498 (VRTK_InteractTouch_t4022091061 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::FixedUpdate()
extern "C"  void VRTK_InteractTouch_FixedUpdate_m1263634770 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::LateUpdate()
extern "C"  void VRTK_InteractTouch_LateUpdate_m3921884980 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_InteractTouch::GetColliderInteractableObject(UnityEngine.Collider)
extern "C"  GameObject_t1756533147 * VRTK_InteractTouch_GetColliderInteractableObject_m3264948344 (VRTK_InteractTouch_t4022091061 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::AddActiveCollider(UnityEngine.Collider)
extern "C"  void VRTK_InteractTouch_AddActiveCollider_m672773083 (VRTK_InteractTouch_t4022091061 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::StoreTouchedObjectColliders(UnityEngine.Collider)
extern "C"  void VRTK_InteractTouch_StoreTouchedObjectColliders_m2021021655 (VRTK_InteractTouch_t4022091061 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::ToggleControllerVisibility(System.Boolean)
extern "C"  void VRTK_InteractTouch_ToggleControllerVisibility_m795276296 (VRTK_InteractTouch_t4022091061 * __this, bool ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::CheckRumbleController(VRTK.VRTK_InteractableObject)
extern "C"  void VRTK_InteractTouch_CheckRumbleController_m651933864 (VRTK_InteractTouch_t4022091061 * __this, VRTK_InteractableObject_t2604188111 * ___touchedObjectScript0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::CheckStopTouching()
extern "C"  void VRTK_InteractTouch_CheckStopTouching_m1842731092 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VRTK.VRTK_InteractTouch::TriggerStart(UnityEngine.Collider)
extern "C"  GameObject_t1756533147 * VRTK_InteractTouch_TriggerStart_m3542322903 (VRTK_InteractTouch_t4022091061 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractTouch::IsSnapDropZone(UnityEngine.Collider)
extern "C"  bool VRTK_InteractTouch_IsSnapDropZone_m2674113949 (VRTK_InteractTouch_t4022091061 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::CheckButtonOverrides(VRTK.VRTK_InteractableObject)
extern "C"  void VRTK_InteractTouch_CheckButtonOverrides_m3952584764 (VRTK_InteractTouch_t4022091061 * __this, VRTK_InteractableObject_t2604188111 * ___touchedObjectScript0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::ResetButtonOverrides(System.Boolean,System.Boolean)
extern "C"  void VRTK_InteractTouch_ResetButtonOverrides_m3817593309 (VRTK_InteractTouch_t4022091061 * __this, bool ___isGrabbed0, bool ___isUsing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::ResetTriggerRumble()
extern "C"  void VRTK_InteractTouch_ResetTriggerRumble_m1032925517 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::StopTouching(UnityEngine.GameObject)
extern "C"  void VRTK_InteractTouch_StopTouching_m3123480174 (VRTK_InteractTouch_t4022091061 * __this, GameObject_t1756533147 * ___untouched0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::CleanupEndTouch()
extern "C"  void VRTK_InteractTouch_CleanupEndTouch_m104540787 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::DestroyTouchCollider()
extern "C"  void VRTK_InteractTouch_DestroyTouchCollider_m876542884 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_InteractTouch::CustomRigidBodyIsChild()
extern "C"  bool VRTK_InteractTouch_CustomRigidBodyIsChild_m1440716775 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::CreateTouchCollider()
extern "C"  void VRTK_InteractTouch_CreateTouchCollider_m2936039042 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_InteractTouch::CreateTouchRigidBody()
extern "C"  void VRTK_InteractTouch_CreateTouchRigidBody_m2238679425 (VRTK_InteractTouch_t4022091061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
