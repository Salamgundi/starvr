﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetOverlayIntersectionMask
struct _SetOverlayIntersectionMask_t3952572284;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_VROverlayIntersectionMa4278514679.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetOverlayIntersectionMask::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayIntersectionMask__ctor_m2962422373 (_SetOverlayIntersectionMask_t3952572284 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayIntersectionMask::Invoke(System.UInt64,Valve.VR.VROverlayIntersectionMaskPrimitive_t&,System.UInt32,System.UInt32)
extern "C"  int32_t _SetOverlayIntersectionMask_Invoke_m1851553101 (_SetOverlayIntersectionMask_t3952572284 * __this, uint64_t ___ulOverlayHandle0, VROverlayIntersectionMaskPrimitive_t_t4278514679 * ___pMaskPrimitives1, uint32_t ___unNumMaskPrimitives2, uint32_t ___unPrimitiveSize3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayIntersectionMask::BeginInvoke(System.UInt64,Valve.VR.VROverlayIntersectionMaskPrimitive_t&,System.UInt32,System.UInt32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetOverlayIntersectionMask_BeginInvoke_m350419626 (_SetOverlayIntersectionMask_t3952572284 * __this, uint64_t ___ulOverlayHandle0, VROverlayIntersectionMaskPrimitive_t_t4278514679 * ___pMaskPrimitives1, uint32_t ___unNumMaskPrimitives2, uint32_t ___unPrimitiveSize3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayIntersectionMask::EndInvoke(Valve.VR.VROverlayIntersectionMaskPrimitive_t&,System.IAsyncResult)
extern "C"  int32_t _SetOverlayIntersectionMask_EndInvoke_m2642320422 (_SetOverlayIntersectionMask_t3952572284 * __this, VROverlayIntersectionMaskPrimitive_t_t4278514679 * ___pMaskPrimitives0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
