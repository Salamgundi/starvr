﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperone/_SetSceneColor
struct _SetSceneColor_t792692487;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdColor_t1780554589.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperone/_SetSceneColor::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetSceneColor__ctor_m1073010496 (_SetSceneColor_t792692487 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperone/_SetSceneColor::Invoke(Valve.VR.HmdColor_t)
extern "C"  void _SetSceneColor_Invoke_m2798379069 (_SetSceneColor_t792692487 * __this, HmdColor_t_t1780554589  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperone/_SetSceneColor::BeginInvoke(Valve.VR.HmdColor_t,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetSceneColor_BeginInvoke_m680158764 (_SetSceneColor_t792692487 * __this, HmdColor_t_t1780554589  ___color0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperone/_SetSceneColor::EndInvoke(System.IAsyncResult)
extern "C"  void _SetSceneColor_EndInvoke_m1921499986 (_SetSceneColor_t792692487 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
