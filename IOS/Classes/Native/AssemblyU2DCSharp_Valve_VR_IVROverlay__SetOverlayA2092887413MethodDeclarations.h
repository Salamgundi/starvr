﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetOverlayAlpha
struct _SetOverlayAlpha_t2092887413;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetOverlayAlpha::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayAlpha__ctor_m3567544318 (_SetOverlayAlpha_t2092887413 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayAlpha::Invoke(System.UInt64,System.Single)
extern "C"  int32_t _SetOverlayAlpha_Invoke_m2552656502 (_SetOverlayAlpha_t2092887413 * __this, uint64_t ___ulOverlayHandle0, float ___fAlpha1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayAlpha::BeginInvoke(System.UInt64,System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetOverlayAlpha_BeginInvoke_m393829297 (_SetOverlayAlpha_t2092887413 * __this, uint64_t ___ulOverlayHandle0, float ___fAlpha1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayAlpha::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayAlpha_EndInvoke_m1775744854 (_SetOverlayAlpha_t2092887413 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
