﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VideoControlsManager/<DoFade>c__Iterator1
struct U3CDoFadeU3Ec__Iterator1_t4193299950;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VideoControlsManager/<DoFade>c__Iterator1::.ctor()
extern "C"  void U3CDoFadeU3Ec__Iterator1__ctor_m20024509 (U3CDoFadeU3Ec__Iterator1_t4193299950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VideoControlsManager/<DoFade>c__Iterator1::MoveNext()
extern "C"  bool U3CDoFadeU3Ec__Iterator1_MoveNext_m2806712515 (U3CDoFadeU3Ec__Iterator1_t4193299950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VideoControlsManager/<DoFade>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoFadeU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2384802911 (U3CDoFadeU3Ec__Iterator1_t4193299950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VideoControlsManager/<DoFade>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoFadeU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m592968455 (U3CDoFadeU3Ec__Iterator1_t4193299950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoControlsManager/<DoFade>c__Iterator1::Dispose()
extern "C"  void U3CDoFadeU3Ec__Iterator1_Dispose_m2743829128 (U3CDoFadeU3Ec__Iterator1_t4193299950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoControlsManager/<DoFade>c__Iterator1::Reset()
extern "C"  void U3CDoFadeU3Ec__Iterator1_Reset_m3352664634 (U3CDoFadeU3Ec__Iterator1_t4193299950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
