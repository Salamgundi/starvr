﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_TrackedController
struct SteamVR_TrackedController_t3050739949;
// ClickedEventHandler
struct ClickedEventHandler_t1112331409;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ClickedEventHandler1112331409.h"
#include "AssemblyU2DCSharp_ClickedEventArgs2917034410.h"

// System.Void SteamVR_TrackedController::.ctor()
extern "C"  void SteamVR_TrackedController__ctor_m3576584964 (SteamVR_TrackedController_t3050739949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::add_MenuButtonClicked(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_add_MenuButtonClicked_m31274123 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::remove_MenuButtonClicked(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_remove_MenuButtonClicked_m1540779454 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::add_MenuButtonUnclicked(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_add_MenuButtonUnclicked_m1881536350 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::remove_MenuButtonUnclicked(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_remove_MenuButtonUnclicked_m3576321419 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::add_TriggerClicked(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_add_TriggerClicked_m3222724612 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::remove_TriggerClicked(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_remove_TriggerClicked_m1033624533 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::add_TriggerUnclicked(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_add_TriggerUnclicked_m521184913 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::remove_TriggerUnclicked(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_remove_TriggerUnclicked_m1724017026 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::add_SteamClicked(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_add_SteamClicked_m270816040 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::remove_SteamClicked(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_remove_SteamClicked_m3964866743 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::add_PadClicked(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_add_PadClicked_m2398359559 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::remove_PadClicked(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_remove_PadClicked_m536720892 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::add_PadUnclicked(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_add_PadUnclicked_m3014895694 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::remove_PadUnclicked(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_remove_PadUnclicked_m2185130907 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::add_PadTouched(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_add_PadTouched_m3126250544 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::remove_PadTouched(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_remove_PadTouched_m2667611079 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::add_PadUntouched(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_add_PadUntouched_m1268398219 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::remove_PadUntouched(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_remove_PadUntouched_m2330963810 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::add_Gripped(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_add_Gripped_m868788018 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::remove_Gripped(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_remove_Gripped_m1150416895 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::add_Ungripped(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_add_Ungripped_m2790596471 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::remove_Ungripped(ClickedEventHandler)
extern "C"  void SteamVR_TrackedController_remove_Ungripped_m1093910666 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventHandler_t1112331409 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::Start()
extern "C"  void SteamVR_TrackedController_Start_m1525366324 (SteamVR_TrackedController_t3050739949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::SetDeviceIndex(System.Int32)
extern "C"  void SteamVR_TrackedController_SetDeviceIndex_m2565689141 (SteamVR_TrackedController_t3050739949 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::OnTriggerClicked(ClickedEventArgs)
extern "C"  void SteamVR_TrackedController_OnTriggerClicked_m3780692892 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventArgs_t2917034410  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::OnTriggerUnclicked(ClickedEventArgs)
extern "C"  void SteamVR_TrackedController_OnTriggerUnclicked_m2155088573 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventArgs_t2917034410  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::OnMenuClicked(ClickedEventArgs)
extern "C"  void SteamVR_TrackedController_OnMenuClicked_m1715251373 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventArgs_t2917034410  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::OnMenuUnclicked(ClickedEventArgs)
extern "C"  void SteamVR_TrackedController_OnMenuUnclicked_m500733404 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventArgs_t2917034410  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::OnSteamClicked(ClickedEventArgs)
extern "C"  void SteamVR_TrackedController_OnSteamClicked_m2570391956 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventArgs_t2917034410  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::OnPadClicked(ClickedEventArgs)
extern "C"  void SteamVR_TrackedController_OnPadClicked_m2733666495 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventArgs_t2917034410  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::OnPadUnclicked(ClickedEventArgs)
extern "C"  void SteamVR_TrackedController_OnPadUnclicked_m1078928474 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventArgs_t2917034410  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::OnPadTouched(ClickedEventArgs)
extern "C"  void SteamVR_TrackedController_OnPadTouched_m1820601084 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventArgs_t2917034410  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::OnPadUntouched(ClickedEventArgs)
extern "C"  void SteamVR_TrackedController_OnPadUntouched_m2899943683 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventArgs_t2917034410  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::OnGripped(ClickedEventArgs)
extern "C"  void SteamVR_TrackedController_OnGripped_m2063197756 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventArgs_t2917034410  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::OnUngripped(ClickedEventArgs)
extern "C"  void SteamVR_TrackedController_OnUngripped_m1930275579 (SteamVR_TrackedController_t3050739949 * __this, ClickedEventArgs_t2917034410  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_TrackedController::Update()
extern "C"  void SteamVR_TrackedController_Update_m4279952113 (SteamVR_TrackedController_t3050739949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
