﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperone/_GetPlayAreaRect
struct _GetPlayAreaRect_t2644523155;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdQuad_t2172573705.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperone/_GetPlayAreaRect::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetPlayAreaRect__ctor_m2898003030 (_GetPlayAreaRect_t2644523155 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperone/_GetPlayAreaRect::Invoke(Valve.VR.HmdQuad_t&)
extern "C"  bool _GetPlayAreaRect_Invoke_m2606931859 (_GetPlayAreaRect_t2644523155 * __this, HmdQuad_t_t2172573705 * ___rect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperone/_GetPlayAreaRect::BeginInvoke(Valve.VR.HmdQuad_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetPlayAreaRect_BeginInvoke_m2283595292 (_GetPlayAreaRect_t2644523155 * __this, HmdQuad_t_t2172573705 * ___rect0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRChaperone/_GetPlayAreaRect::EndInvoke(Valve.VR.HmdQuad_t&,System.IAsyncResult)
extern "C"  bool _GetPlayAreaRect_EndInvoke_m3529747857 (_GetPlayAreaRect_t2644523155 * __this, HmdQuad_t_t2172573705 * ___rect0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
