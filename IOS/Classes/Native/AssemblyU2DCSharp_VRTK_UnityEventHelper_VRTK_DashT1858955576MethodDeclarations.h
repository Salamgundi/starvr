﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.UnityEventHelper.VRTK_DashTeleport_UnityEvents
struct VRTK_DashTeleport_UnityEvents_t1858955576;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_VRTK_DashTeleportEventArgs2197253242.h"

// System.Void VRTK.UnityEventHelper.VRTK_DashTeleport_UnityEvents::.ctor()
extern "C"  void VRTK_DashTeleport_UnityEvents__ctor_m3998782441 (VRTK_DashTeleport_UnityEvents_t1858955576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_DashTeleport_UnityEvents::SetDashTeleport()
extern "C"  void VRTK_DashTeleport_UnityEvents_SetDashTeleport_m4113735446 (VRTK_DashTeleport_UnityEvents_t1858955576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_DashTeleport_UnityEvents::OnEnable()
extern "C"  void VRTK_DashTeleport_UnityEvents_OnEnable_m1672237001 (VRTK_DashTeleport_UnityEvents_t1858955576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_DashTeleport_UnityEvents::WillDashThruObjects(System.Object,VRTK.DashTeleportEventArgs)
extern "C"  void VRTK_DashTeleport_UnityEvents_WillDashThruObjects_m630050971 (VRTK_DashTeleport_UnityEvents_t1858955576 * __this, Il2CppObject * ___o0, DashTeleportEventArgs_t2197253242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_DashTeleport_UnityEvents::DashedThruObjects(System.Object,VRTK.DashTeleportEventArgs)
extern "C"  void VRTK_DashTeleport_UnityEvents_DashedThruObjects_m992061250 (VRTK_DashTeleport_UnityEvents_t1858955576 * __this, Il2CppObject * ___o0, DashTeleportEventArgs_t2197253242  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.UnityEventHelper.VRTK_DashTeleport_UnityEvents::OnDisable()
extern "C"  void VRTK_DashTeleport_UnityEvents_OnDisable_m2022263202 (VRTK_DashTeleport_UnityEvents_t1858955576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
