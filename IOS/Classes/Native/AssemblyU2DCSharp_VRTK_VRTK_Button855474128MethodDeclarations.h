﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_Button
struct VRTK_Button_t855474128;
// VRTK.Button3DEventHandler
struct Button3DEventHandler_t1715584557;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_Button3DEventHandler1715584557.h"
#include "AssemblyU2DCSharp_VRTK_Control3DEventArgs4095025701.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Control_ControlValueRa2976216666.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Button_ButtonDirection1346461537.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_Button::.ctor()
extern "C"  void VRTK_Button__ctor_m2845104580 (VRTK_Button_t855474128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Button::add_Pushed(VRTK.Button3DEventHandler)
extern "C"  void VRTK_Button_add_Pushed_m3307971813 (VRTK_Button_t855474128 * __this, Button3DEventHandler_t1715584557 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Button::remove_Pushed(VRTK.Button3DEventHandler)
extern "C"  void VRTK_Button_remove_Pushed_m3510980944 (VRTK_Button_t855474128 * __this, Button3DEventHandler_t1715584557 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Button::OnPushed(VRTK.Control3DEventArgs)
extern "C"  void VRTK_Button_OnPushed_m3398304502 (VRTK_Button_t855474128 * __this, Control3DEventArgs_t4095025701  ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Button::OnDrawGizmos()
extern "C"  void VRTK_Button_OnDrawGizmos_m938528650 (VRTK_Button_t855474128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Button::InitRequiredComponents()
extern "C"  void VRTK_Button_InitRequiredComponents_m3863051287 (VRTK_Button_t855474128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Button::DetectSetup()
extern "C"  bool VRTK_Button_DetectSetup_m673914778 (VRTK_Button_t855474128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_Control/ControlValueRange VRTK.VRTK_Button::RegisterValueRange()
extern "C"  ControlValueRange_t2976216666  VRTK_Button_RegisterValueRange_m670464370 (VRTK_Button_t855474128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Button::HandleUpdate()
extern "C"  void VRTK_Button_HandleUpdate_m3725961659 (VRTK_Button_t855474128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Button::FixedUpdate()
extern "C"  void VRTK_Button_FixedUpdate_m3020691695 (VRTK_Button_t855474128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Button::OnCollisionExit(UnityEngine.Collision)
extern "C"  void VRTK_Button_OnCollisionExit_m2812634336 (VRTK_Button_t855474128 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_Button::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void VRTK_Button_OnCollisionEnter_m1285002802 (VRTK_Button_t855474128 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VRTK.VRTK_Button/ButtonDirection VRTK.VRTK_Button::DetectDirection()
extern "C"  int32_t VRTK_Button_DetectDirection_m982156426 (VRTK_Button_t855474128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_Button::CalculateActivationDir()
extern "C"  Vector3_t2243707580  VRTK_Button_CalculateActivationDir_m4019336065 (VRTK_Button_t855474128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_Button::ReachedActivationDistance()
extern "C"  bool VRTK_Button_ReachedActivationDistance_m1472629663 (VRTK_Button_t855474128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VRTK.VRTK_Button::GetForceVector()
extern "C"  Vector3_t2243707580  VRTK_Button_GetForceVector_m1035285692 (VRTK_Button_t855474128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
