﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SteamVR_LoadLevel/<LoadLevel>c__Iterator0
struct U3CLoadLevelU3Ec__Iterator0_t3488978783;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SteamVR_LoadLevel/<LoadLevel>c__Iterator0::.ctor()
extern "C"  void U3CLoadLevelU3Ec__Iterator0__ctor_m3595996716 (U3CLoadLevelU3Ec__Iterator0_t3488978783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SteamVR_LoadLevel/<LoadLevel>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadLevelU3Ec__Iterator0_MoveNext_m1890010108 (U3CLoadLevelU3Ec__Iterator0_t3488978783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SteamVR_LoadLevel/<LoadLevel>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadLevelU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3195062586 (U3CLoadLevelU3Ec__Iterator0_t3488978783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SteamVR_LoadLevel/<LoadLevel>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadLevelU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3313055250 (U3CLoadLevelU3Ec__Iterator0_t3488978783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LoadLevel/<LoadLevel>c__Iterator0::Dispose()
extern "C"  void U3CLoadLevelU3Ec__Iterator0_Dispose_m2347799689 (U3CLoadLevelU3Ec__Iterator0_t3488978783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SteamVR_LoadLevel/<LoadLevel>c__Iterator0::Reset()
extern "C"  void U3CLoadLevelU3Ec__Iterator0_Reset_m4017089703 (U3CLoadLevelU3Ec__Iterator0_t3488978783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
