﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRTrackedCamera/_GetCameraProjection
struct _GetCameraProjection_t2867851566;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraError3529690400.h"
#include "AssemblyU2DCSharp_Valve_VR_EVRTrackedCameraFrameTy2003723073.h"
#include "AssemblyU2DCSharp_Valve_VR_HmdMatrix44_t664273159.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRTrackedCamera/_GetCameraProjection::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetCameraProjection__ctor_m492031927 (_GetCameraProjection_t2867851566 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_GetCameraProjection::Invoke(System.UInt32,Valve.VR.EVRTrackedCameraFrameType,System.Single,System.Single,Valve.VR.HmdMatrix44_t&)
extern "C"  int32_t _GetCameraProjection_Invoke_m350751408 (_GetCameraProjection_t2867851566 * __this, uint32_t ___nDeviceIndex0, int32_t ___eFrameType1, float ___flZNear2, float ___flZFar3, HmdMatrix44_t_t664273159 * ___pProjection4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRTrackedCamera/_GetCameraProjection::BeginInvoke(System.UInt32,Valve.VR.EVRTrackedCameraFrameType,System.Single,System.Single,Valve.VR.HmdMatrix44_t&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetCameraProjection_BeginInvoke_m538182144 (_GetCameraProjection_t2867851566 * __this, uint32_t ___nDeviceIndex0, int32_t ___eFrameType1, float ___flZNear2, float ___flZFar3, HmdMatrix44_t_t664273159 * ___pProjection4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVRTrackedCameraError Valve.VR.IVRTrackedCamera/_GetCameraProjection::EndInvoke(Valve.VR.HmdMatrix44_t&,System.IAsyncResult)
extern "C"  int32_t _GetCameraProjection_EndInvoke_m2894727983 (_GetCameraProjection_t2867851566 * __this, HmdMatrix44_t_t664273159 * ___pProjection0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
