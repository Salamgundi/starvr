﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.InteractionSystem.ControllerHintsExample/<TestTextHints>c__Iterator1
struct U3CTestTextHintsU3Ec__Iterator1_t1955400369;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Valve.VR.InteractionSystem.ControllerHintsExample/<TestTextHints>c__Iterator1::.ctor()
extern "C"  void U3CTestTextHintsU3Ec__Iterator1__ctor_m1900835266 (U3CTestTextHintsU3Ec__Iterator1_t1955400369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.InteractionSystem.ControllerHintsExample/<TestTextHints>c__Iterator1::MoveNext()
extern "C"  bool U3CTestTextHintsU3Ec__Iterator1_MoveNext_m641062530 (U3CTestTextHintsU3Ec__Iterator1_t1955400369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.ControllerHintsExample/<TestTextHints>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTestTextHintsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3425770348 (U3CTestTextHintsU3Ec__Iterator1_t1955400369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Valve.VR.InteractionSystem.ControllerHintsExample/<TestTextHints>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTestTextHintsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m648527252 (U3CTestTextHintsU3Ec__Iterator1_t1955400369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHintsExample/<TestTextHints>c__Iterator1::Dispose()
extern "C"  void U3CTestTextHintsU3Ec__Iterator1_Dispose_m3064413139 (U3CTestTextHintsU3Ec__Iterator1_t1955400369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.InteractionSystem.ControllerHintsExample/<TestTextHints>c__Iterator1::Reset()
extern "C"  void U3CTestTextHintsU3Ec__Iterator1_Reset_m2346026869 (U3CTestTextHintsU3Ec__Iterator1_t1955400369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
