﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.CombineInstance>
struct List_1_t3728683638;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3263413312.h"
#include "UnityEngine_UnityEngine_CombineInstance64595210.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CombineInstance>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m263044998_gshared (Enumerator_t3263413312 * __this, List_1_t3728683638 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m263044998(__this, ___l0, method) ((  void (*) (Enumerator_t3263413312 *, List_1_t3728683638 *, const MethodInfo*))Enumerator__ctor_m263044998_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CombineInstance>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1028529932_gshared (Enumerator_t3263413312 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1028529932(__this, method) ((  void (*) (Enumerator_t3263413312 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1028529932_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.CombineInstance>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3936738908_gshared (Enumerator_t3263413312 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3936738908(__this, method) ((  Il2CppObject * (*) (Enumerator_t3263413312 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3936738908_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CombineInstance>::Dispose()
extern "C"  void Enumerator_Dispose_m2206009625_gshared (Enumerator_t3263413312 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2206009625(__this, method) ((  void (*) (Enumerator_t3263413312 *, const MethodInfo*))Enumerator_Dispose_m2206009625_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CombineInstance>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1263498484_gshared (Enumerator_t3263413312 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1263498484(__this, method) ((  void (*) (Enumerator_t3263413312 *, const MethodInfo*))Enumerator_VerifyState_m1263498484_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.CombineInstance>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4068630256_gshared (Enumerator_t3263413312 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4068630256(__this, method) ((  bool (*) (Enumerator_t3263413312 *, const MethodInfo*))Enumerator_MoveNext_m4068630256_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.CombineInstance>::get_Current()
extern "C"  CombineInstance_t64595210  Enumerator_get_Current_m4292106305_gshared (Enumerator_t3263413312 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4292106305(__this, method) ((  CombineInstance_t64595210  (*) (Enumerator_t3263413312 *, const MethodInfo*))Enumerator_get_Current_m4292106305_gshared)(__this, method)
