﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.SoundDeparent
struct  SoundDeparent_t50869408  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Valve.VR.InteractionSystem.SoundDeparent::destroyAfterPlayOnce
	bool ___destroyAfterPlayOnce_2;
	// UnityEngine.AudioSource Valve.VR.InteractionSystem.SoundDeparent::thisAudioSource
	AudioSource_t1135106623 * ___thisAudioSource_3;

public:
	inline static int32_t get_offset_of_destroyAfterPlayOnce_2() { return static_cast<int32_t>(offsetof(SoundDeparent_t50869408, ___destroyAfterPlayOnce_2)); }
	inline bool get_destroyAfterPlayOnce_2() const { return ___destroyAfterPlayOnce_2; }
	inline bool* get_address_of_destroyAfterPlayOnce_2() { return &___destroyAfterPlayOnce_2; }
	inline void set_destroyAfterPlayOnce_2(bool value)
	{
		___destroyAfterPlayOnce_2 = value;
	}

	inline static int32_t get_offset_of_thisAudioSource_3() { return static_cast<int32_t>(offsetof(SoundDeparent_t50869408, ___thisAudioSource_3)); }
	inline AudioSource_t1135106623 * get_thisAudioSource_3() const { return ___thisAudioSource_3; }
	inline AudioSource_t1135106623 ** get_address_of_thisAudioSource_3() { return &___thisAudioSource_3; }
	inline void set_thisAudioSource_3(AudioSource_t1135106623 * value)
	{
		___thisAudioSource_3 = value;
		Il2CppCodeGenWriteBarrier(&___thisAudioSource_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
