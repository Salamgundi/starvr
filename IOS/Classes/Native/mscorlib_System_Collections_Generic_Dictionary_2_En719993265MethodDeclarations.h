﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Valve.VR.EVREventType,System.Object>
struct Dictionary_2_t3694935859;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En719993265.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21452281081.h"
#include "AssemblyU2DCSharp_Valve_VR_EVREventType6846875.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2698405850_gshared (Enumerator_t719993265 * __this, Dictionary_2_t3694935859 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2698405850(__this, ___dictionary0, method) ((  void (*) (Enumerator_t719993265 *, Dictionary_2_t3694935859 *, const MethodInfo*))Enumerator__ctor_m2698405850_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2475576853_gshared (Enumerator_t719993265 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2475576853(__this, method) ((  Il2CppObject * (*) (Enumerator_t719993265 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2475576853_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4244914941_gshared (Enumerator_t719993265 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4244914941(__this, method) ((  void (*) (Enumerator_t719993265 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4244914941_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2239374492_gshared (Enumerator_t719993265 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2239374492(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t719993265 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2239374492_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3769630515_gshared (Enumerator_t719993265 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3769630515(__this, method) ((  Il2CppObject * (*) (Enumerator_t719993265 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3769630515_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2276293475_gshared (Enumerator_t719993265 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2276293475(__this, method) ((  Il2CppObject * (*) (Enumerator_t719993265 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2276293475_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3174323269_gshared (Enumerator_t719993265 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3174323269(__this, method) ((  bool (*) (Enumerator_t719993265 *, const MethodInfo*))Enumerator_MoveNext_m3174323269_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1452281081  Enumerator_get_Current_m4293436273_gshared (Enumerator_t719993265 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4293436273(__this, method) ((  KeyValuePair_2_t1452281081  (*) (Enumerator_t719993265 *, const MethodInfo*))Enumerator_get_Current_m4293436273_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m1155204714_gshared (Enumerator_t719993265 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1155204714(__this, method) ((  int32_t (*) (Enumerator_t719993265 *, const MethodInfo*))Enumerator_get_CurrentKey_m1155204714_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m2453297386_gshared (Enumerator_t719993265 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2453297386(__this, method) ((  Il2CppObject * (*) (Enumerator_t719993265 *, const MethodInfo*))Enumerator_get_CurrentValue_m2453297386_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m2804750196_gshared (Enumerator_t719993265 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2804750196(__this, method) ((  void (*) (Enumerator_t719993265 *, const MethodInfo*))Enumerator_Reset_m2804750196_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m879070195_gshared (Enumerator_t719993265 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m879070195(__this, method) ((  void (*) (Enumerator_t719993265 *, const MethodInfo*))Enumerator_VerifyState_m879070195_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m4168362253_gshared (Enumerator_t719993265 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m4168362253(__this, method) ((  void (*) (Enumerator_t719993265 *, const MethodInfo*))Enumerator_VerifyCurrent_m4168362253_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1726429690_gshared (Enumerator_t719993265 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1726429690(__this, method) ((  void (*) (Enumerator_t719993265 *, const MethodInfo*))Enumerator_Dispose_m1726429690_gshared)(__this, method)
