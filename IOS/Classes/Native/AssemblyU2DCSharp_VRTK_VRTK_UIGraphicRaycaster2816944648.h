﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Canvas
struct Canvas_t209405766;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t1282925227;

#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster410733016.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_UIGraphicRaycaster
struct  VRTK_UIGraphicRaycaster_t2816944648  : public GraphicRaycaster_t410733016
{
public:
	// UnityEngine.Canvas VRTK.VRTK_UIGraphicRaycaster::m_Canvas
	Canvas_t209405766 * ___m_Canvas_10;
	// UnityEngine.Vector2 VRTK.VRTK_UIGraphicRaycaster::lastKnownPosition
	Vector2_t2243707579  ___lastKnownPosition_11;

public:
	inline static int32_t get_offset_of_m_Canvas_10() { return static_cast<int32_t>(offsetof(VRTK_UIGraphicRaycaster_t2816944648, ___m_Canvas_10)); }
	inline Canvas_t209405766 * get_m_Canvas_10() const { return ___m_Canvas_10; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_10() { return &___m_Canvas_10; }
	inline void set_m_Canvas_10(Canvas_t209405766 * value)
	{
		___m_Canvas_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_Canvas_10, value);
	}

	inline static int32_t get_offset_of_lastKnownPosition_11() { return static_cast<int32_t>(offsetof(VRTK_UIGraphicRaycaster_t2816944648, ___lastKnownPosition_11)); }
	inline Vector2_t2243707579  get_lastKnownPosition_11() const { return ___lastKnownPosition_11; }
	inline Vector2_t2243707579 * get_address_of_lastKnownPosition_11() { return &___lastKnownPosition_11; }
	inline void set_lastKnownPosition_11(Vector2_t2243707579  value)
	{
		___lastKnownPosition_11 = value;
	}
};

struct VRTK_UIGraphicRaycaster_t2816944648_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> VRTK.VRTK_UIGraphicRaycaster::s_RaycastResults
	List_1_t3685274804 * ___s_RaycastResults_13;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> VRTK.VRTK_UIGraphicRaycaster::<>f__am$cache0
	Comparison_1_t1282925227 * ___U3CU3Ef__amU24cache0_14;

public:
	inline static int32_t get_offset_of_s_RaycastResults_13() { return static_cast<int32_t>(offsetof(VRTK_UIGraphicRaycaster_t2816944648_StaticFields, ___s_RaycastResults_13)); }
	inline List_1_t3685274804 * get_s_RaycastResults_13() const { return ___s_RaycastResults_13; }
	inline List_1_t3685274804 ** get_address_of_s_RaycastResults_13() { return &___s_RaycastResults_13; }
	inline void set_s_RaycastResults_13(List_1_t3685274804 * value)
	{
		___s_RaycastResults_13 = value;
		Il2CppCodeGenWriteBarrier(&___s_RaycastResults_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(VRTK_UIGraphicRaycaster_t2816944648_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline Comparison_1_t1282925227 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline Comparison_1_t1282925227 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(Comparison_1_t1282925227 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
