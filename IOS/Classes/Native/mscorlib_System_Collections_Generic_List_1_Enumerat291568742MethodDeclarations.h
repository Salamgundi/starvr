﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Valve.VR.InteractionSystem.Hand/AttachedObject>
struct List_1_t756839068;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat291568742.h"
#include "AssemblyU2DCSharp_Valve_VR_InteractionSystem_Hand_1387717936.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Valve.VR.InteractionSystem.Hand/AttachedObject>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3863598825_gshared (Enumerator_t291568742 * __this, List_1_t756839068 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3863598825(__this, ___l0, method) ((  void (*) (Enumerator_t291568742 *, List_1_t756839068 *, const MethodInfo*))Enumerator__ctor_m3863598825_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4025557977_gshared (Enumerator_t291568742 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4025557977(__this, method) ((  void (*) (Enumerator_t291568742 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4025557977_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Valve.VR.InteractionSystem.Hand/AttachedObject>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m9606309_gshared (Enumerator_t291568742 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m9606309(__this, method) ((  Il2CppObject * (*) (Enumerator_t291568742 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m9606309_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Valve.VR.InteractionSystem.Hand/AttachedObject>::Dispose()
extern "C"  void Enumerator_Dispose_m1470980080_gshared (Enumerator_t291568742 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1470980080(__this, method) ((  void (*) (Enumerator_t291568742 *, const MethodInfo*))Enumerator_Dispose_m1470980080_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Valve.VR.InteractionSystem.Hand/AttachedObject>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2088096767_gshared (Enumerator_t291568742 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2088096767(__this, method) ((  void (*) (Enumerator_t291568742 *, const MethodInfo*))Enumerator_VerifyState_m2088096767_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Valve.VR.InteractionSystem.Hand/AttachedObject>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m570462409_gshared (Enumerator_t291568742 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m570462409(__this, method) ((  bool (*) (Enumerator_t291568742 *, const MethodInfo*))Enumerator_MoveNext_m570462409_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Valve.VR.InteractionSystem.Hand/AttachedObject>::get_Current()
extern "C"  AttachedObject_t1387717936  Enumerator_get_Current_m145128352_gshared (Enumerator_t291568742 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m145128352(__this, method) ((  AttachedObject_t1387717936  (*) (Enumerator_t291568742 *, const MethodInfo*))Enumerator_get_Current_m145128352_gshared)(__this, method)
