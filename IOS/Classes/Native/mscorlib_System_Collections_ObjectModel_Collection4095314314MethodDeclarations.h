﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct Collection_1_t4095314314;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>[]
struct KeyValuePair_2U5BU5D_t908974985;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct IEnumerator_1_t2029093387;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>
struct IList_1_t799542865;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_258602264.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::.ctor()
extern "C"  void Collection_1__ctor_m2754777853_gshared (Collection_1_t4095314314 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2754777853(__this, method) ((  void (*) (Collection_1_t4095314314 *, const MethodInfo*))Collection_1__ctor_m2754777853_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m987700660_gshared (Collection_1_t4095314314 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m987700660(__this, method) ((  bool (*) (Collection_1_t4095314314 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m987700660_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1130918605_gshared (Collection_1_t4095314314 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1130918605(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t4095314314 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1130918605_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1085132988_gshared (Collection_1_t4095314314 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1085132988(__this, method) ((  Il2CppObject * (*) (Collection_1_t4095314314 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1085132988_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3376330101_gshared (Collection_1_t4095314314 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3376330101(__this, ___value0, method) ((  int32_t (*) (Collection_1_t4095314314 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3376330101_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2346759853_gshared (Collection_1_t4095314314 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2346759853(__this, ___value0, method) ((  bool (*) (Collection_1_t4095314314 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2346759853_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2578036935_gshared (Collection_1_t4095314314 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2578036935(__this, ___value0, method) ((  int32_t (*) (Collection_1_t4095314314 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2578036935_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m657200686_gshared (Collection_1_t4095314314 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m657200686(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4095314314 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m657200686_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3661188852_gshared (Collection_1_t4095314314 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3661188852(__this, ___value0, method) ((  void (*) (Collection_1_t4095314314 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3661188852_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m4012761657_gshared (Collection_1_t4095314314 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m4012761657(__this, method) ((  bool (*) (Collection_1_t4095314314 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m4012761657_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m353037553_gshared (Collection_1_t4095314314 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m353037553(__this, method) ((  Il2CppObject * (*) (Collection_1_t4095314314 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m353037553_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m3849871662_gshared (Collection_1_t4095314314 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3849871662(__this, method) ((  bool (*) (Collection_1_t4095314314 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m3849871662_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1726532725_gshared (Collection_1_t4095314314 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1726532725(__this, method) ((  bool (*) (Collection_1_t4095314314 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1726532725_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3757340036_gshared (Collection_1_t4095314314 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3757340036(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t4095314314 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3757340036_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2040617815_gshared (Collection_1_t4095314314 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2040617815(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4095314314 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2040617815_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Add(T)
extern "C"  void Collection_1_Add_m4133483058_gshared (Collection_1_t4095314314 * __this, KeyValuePair_2_t258602264  ___item0, const MethodInfo* method);
#define Collection_1_Add_m4133483058(__this, ___item0, method) ((  void (*) (Collection_1_t4095314314 *, KeyValuePair_2_t258602264 , const MethodInfo*))Collection_1_Add_m4133483058_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Clear()
extern "C"  void Collection_1_Clear_m3409223606_gshared (Collection_1_t4095314314 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3409223606(__this, method) ((  void (*) (Collection_1_t4095314314 *, const MethodInfo*))Collection_1_Clear_m3409223606_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::ClearItems()
extern "C"  void Collection_1_ClearItems_m326361348_gshared (Collection_1_t4095314314 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m326361348(__this, method) ((  void (*) (Collection_1_t4095314314 *, const MethodInfo*))Collection_1_ClearItems_m326361348_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Contains(T)
extern "C"  bool Collection_1_Contains_m2101185644_gshared (Collection_1_t4095314314 * __this, KeyValuePair_2_t258602264  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2101185644(__this, ___item0, method) ((  bool (*) (Collection_1_t4095314314 *, KeyValuePair_2_t258602264 , const MethodInfo*))Collection_1_Contains_m2101185644_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m301386386_gshared (Collection_1_t4095314314 * __this, KeyValuePair_2U5BU5D_t908974985* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m301386386(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t4095314314 *, KeyValuePair_2U5BU5D_t908974985*, int32_t, const MethodInfo*))Collection_1_CopyTo_m301386386_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m124272837_gshared (Collection_1_t4095314314 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m124272837(__this, method) ((  Il2CppObject* (*) (Collection_1_t4095314314 *, const MethodInfo*))Collection_1_GetEnumerator_m124272837_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3496578140_gshared (Collection_1_t4095314314 * __this, KeyValuePair_2_t258602264  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3496578140(__this, ___item0, method) ((  int32_t (*) (Collection_1_t4095314314 *, KeyValuePair_2_t258602264 , const MethodInfo*))Collection_1_IndexOf_m3496578140_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1790763471_gshared (Collection_1_t4095314314 * __this, int32_t ___index0, KeyValuePair_2_t258602264  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1790763471(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4095314314 *, int32_t, KeyValuePair_2_t258602264 , const MethodInfo*))Collection_1_Insert_m1790763471_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2229865600_gshared (Collection_1_t4095314314 * __this, int32_t ___index0, KeyValuePair_2_t258602264  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2229865600(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4095314314 *, int32_t, KeyValuePair_2_t258602264 , const MethodInfo*))Collection_1_InsertItem_m2229865600_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::Remove(T)
extern "C"  bool Collection_1_Remove_m3021774363_gshared (Collection_1_t4095314314 * __this, KeyValuePair_2_t258602264  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3021774363(__this, ___item0, method) ((  bool (*) (Collection_1_t4095314314 *, KeyValuePair_2_t258602264 , const MethodInfo*))Collection_1_Remove_m3021774363_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2797047267_gshared (Collection_1_t4095314314 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2797047267(__this, ___index0, method) ((  void (*) (Collection_1_t4095314314 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2797047267_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m302392207_gshared (Collection_1_t4095314314 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m302392207(__this, ___index0, method) ((  void (*) (Collection_1_t4095314314 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m302392207_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m687966037_gshared (Collection_1_t4095314314 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m687966037(__this, method) ((  int32_t (*) (Collection_1_t4095314314 *, const MethodInfo*))Collection_1_get_Count_m687966037_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t258602264  Collection_1_get_Item_m2117398983_gshared (Collection_1_t4095314314 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2117398983(__this, ___index0, method) ((  KeyValuePair_2_t258602264  (*) (Collection_1_t4095314314 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2117398983_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3309439480_gshared (Collection_1_t4095314314 * __this, int32_t ___index0, KeyValuePair_2_t258602264  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3309439480(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4095314314 *, int32_t, KeyValuePair_2_t258602264 , const MethodInfo*))Collection_1_set_Item_m3309439480_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1629491719_gshared (Collection_1_t4095314314 * __this, int32_t ___index0, KeyValuePair_2_t258602264  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1629491719(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4095314314 *, int32_t, KeyValuePair_2_t258602264 , const MethodInfo*))Collection_1_SetItem_m1629491719_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m942675110_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m942675110(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m942675110_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::ConvertItem(System.Object)
extern "C"  KeyValuePair_2_t258602264  Collection_1_ConvertItem_m3849702830_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3849702830(__this /* static, unused */, ___item0, method) ((  KeyValuePair_2_t258602264  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3849702830_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m4147639766_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m4147639766(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m4147639766_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2173500740_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2173500740(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2173500740_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt64>>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1408779223_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1408779223(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1408779223_gshared)(__this /* static, unused */, ___list0, method)
