﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SteamVR_Events_Event_2_gen175250481MethodDeclarations.h"

// System.Void SteamVR_Events/Event`2<SteamVR_RenderModel,System.Boolean>::.ctor()
#define Event_2__ctor_m2789222409(__this, method) ((  void (*) (Event_2_t3622693210 *, const MethodInfo*))Event_2__ctor_m1718189359_gshared)(__this, method)
// System.Void SteamVR_Events/Event`2<SteamVR_RenderModel,System.Boolean>::Listen(UnityEngine.Events.UnityAction`2<T0,T1>)
#define Event_2_Listen_m1020284634(__this, ___action0, method) ((  void (*) (Event_2_t3622693210 *, UnityAction_2_t4073506138 *, const MethodInfo*))Event_2_Listen_m1492392574_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`2<SteamVR_RenderModel,System.Boolean>::Remove(UnityEngine.Events.UnityAction`2<T0,T1>)
#define Event_2_Remove_m3461396121(__this, ___action0, method) ((  void (*) (Event_2_t3622693210 *, UnityAction_2_t4073506138 *, const MethodInfo*))Event_2_Remove_m760931903_gshared)(__this, ___action0, method)
// System.Void SteamVR_Events/Event`2<SteamVR_RenderModel,System.Boolean>::Send(T0,T1)
#define Event_2_Send_m2068752292(__this, ___arg00, ___arg11, method) ((  void (*) (Event_2_t3622693210 *, SteamVR_RenderModel_t2905485978 *, bool, const MethodInfo*))Event_2_Send_m2170767364_gshared)(__this, ___arg00, ___arg11, method)
