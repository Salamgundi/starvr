﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Valve.VR.InteractionSystem.LinearMapping
struct LinearMapping_t810676855;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t4220419316;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.LinearBlendshape
struct  LinearBlendshape_t393683001  : public MonoBehaviour_t1158329972
{
public:
	// Valve.VR.InteractionSystem.LinearMapping Valve.VR.InteractionSystem.LinearBlendshape::linearMapping
	LinearMapping_t810676855 * ___linearMapping_2;
	// UnityEngine.SkinnedMeshRenderer Valve.VR.InteractionSystem.LinearBlendshape::skinnedMesh
	SkinnedMeshRenderer_t4220419316 * ___skinnedMesh_3;
	// System.Single Valve.VR.InteractionSystem.LinearBlendshape::lastValue
	float ___lastValue_4;

public:
	inline static int32_t get_offset_of_linearMapping_2() { return static_cast<int32_t>(offsetof(LinearBlendshape_t393683001, ___linearMapping_2)); }
	inline LinearMapping_t810676855 * get_linearMapping_2() const { return ___linearMapping_2; }
	inline LinearMapping_t810676855 ** get_address_of_linearMapping_2() { return &___linearMapping_2; }
	inline void set_linearMapping_2(LinearMapping_t810676855 * value)
	{
		___linearMapping_2 = value;
		Il2CppCodeGenWriteBarrier(&___linearMapping_2, value);
	}

	inline static int32_t get_offset_of_skinnedMesh_3() { return static_cast<int32_t>(offsetof(LinearBlendshape_t393683001, ___skinnedMesh_3)); }
	inline SkinnedMeshRenderer_t4220419316 * get_skinnedMesh_3() const { return ___skinnedMesh_3; }
	inline SkinnedMeshRenderer_t4220419316 ** get_address_of_skinnedMesh_3() { return &___skinnedMesh_3; }
	inline void set_skinnedMesh_3(SkinnedMeshRenderer_t4220419316 * value)
	{
		___skinnedMesh_3 = value;
		Il2CppCodeGenWriteBarrier(&___skinnedMesh_3, value);
	}

	inline static int32_t get_offset_of_lastValue_4() { return static_cast<int32_t>(offsetof(LinearBlendshape_t393683001, ___lastValue_4)); }
	inline float get_lastValue_4() const { return ___lastValue_4; }
	inline float* get_address_of_lastValue_4() { return &___lastValue_4; }
	inline void set_lastValue_4(float value)
	{
		___lastValue_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
