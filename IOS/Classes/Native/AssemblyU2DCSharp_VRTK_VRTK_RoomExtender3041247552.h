﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_RoomExtender_MovementF2403078603.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_RoomExtender
struct  VRTK_RoomExtender_t3041247552  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_RoomExtender/MovementFunction VRTK.VRTK_RoomExtender::movementFunction
	int32_t ___movementFunction_2;
	// System.Boolean VRTK.VRTK_RoomExtender::additionalMovementEnabled
	bool ___additionalMovementEnabled_3;
	// System.Boolean VRTK.VRTK_RoomExtender::additionalMovementEnabledOnButtonPress
	bool ___additionalMovementEnabledOnButtonPress_4;
	// System.Single VRTK.VRTK_RoomExtender::additionalMovementMultiplier
	float ___additionalMovementMultiplier_5;
	// System.Single VRTK.VRTK_RoomExtender::headZoneRadius
	float ___headZoneRadius_6;
	// UnityEngine.Transform VRTK.VRTK_RoomExtender::debugTransform
	Transform_t3275118058 * ___debugTransform_7;
	// UnityEngine.Vector3 VRTK.VRTK_RoomExtender::relativeMovementOfCameraRig
	Vector3_t2243707580  ___relativeMovementOfCameraRig_8;
	// UnityEngine.Transform VRTK.VRTK_RoomExtender::movementTransform
	Transform_t3275118058 * ___movementTransform_9;
	// UnityEngine.Transform VRTK.VRTK_RoomExtender::playArea
	Transform_t3275118058 * ___playArea_10;
	// UnityEngine.Vector3 VRTK.VRTK_RoomExtender::headCirclePosition
	Vector3_t2243707580  ___headCirclePosition_11;
	// UnityEngine.Vector3 VRTK.VRTK_RoomExtender::lastPosition
	Vector3_t2243707580  ___lastPosition_12;
	// UnityEngine.Vector3 VRTK.VRTK_RoomExtender::lastMovement
	Vector3_t2243707580  ___lastMovement_13;

public:
	inline static int32_t get_offset_of_movementFunction_2() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_t3041247552, ___movementFunction_2)); }
	inline int32_t get_movementFunction_2() const { return ___movementFunction_2; }
	inline int32_t* get_address_of_movementFunction_2() { return &___movementFunction_2; }
	inline void set_movementFunction_2(int32_t value)
	{
		___movementFunction_2 = value;
	}

	inline static int32_t get_offset_of_additionalMovementEnabled_3() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_t3041247552, ___additionalMovementEnabled_3)); }
	inline bool get_additionalMovementEnabled_3() const { return ___additionalMovementEnabled_3; }
	inline bool* get_address_of_additionalMovementEnabled_3() { return &___additionalMovementEnabled_3; }
	inline void set_additionalMovementEnabled_3(bool value)
	{
		___additionalMovementEnabled_3 = value;
	}

	inline static int32_t get_offset_of_additionalMovementEnabledOnButtonPress_4() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_t3041247552, ___additionalMovementEnabledOnButtonPress_4)); }
	inline bool get_additionalMovementEnabledOnButtonPress_4() const { return ___additionalMovementEnabledOnButtonPress_4; }
	inline bool* get_address_of_additionalMovementEnabledOnButtonPress_4() { return &___additionalMovementEnabledOnButtonPress_4; }
	inline void set_additionalMovementEnabledOnButtonPress_4(bool value)
	{
		___additionalMovementEnabledOnButtonPress_4 = value;
	}

	inline static int32_t get_offset_of_additionalMovementMultiplier_5() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_t3041247552, ___additionalMovementMultiplier_5)); }
	inline float get_additionalMovementMultiplier_5() const { return ___additionalMovementMultiplier_5; }
	inline float* get_address_of_additionalMovementMultiplier_5() { return &___additionalMovementMultiplier_5; }
	inline void set_additionalMovementMultiplier_5(float value)
	{
		___additionalMovementMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_headZoneRadius_6() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_t3041247552, ___headZoneRadius_6)); }
	inline float get_headZoneRadius_6() const { return ___headZoneRadius_6; }
	inline float* get_address_of_headZoneRadius_6() { return &___headZoneRadius_6; }
	inline void set_headZoneRadius_6(float value)
	{
		___headZoneRadius_6 = value;
	}

	inline static int32_t get_offset_of_debugTransform_7() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_t3041247552, ___debugTransform_7)); }
	inline Transform_t3275118058 * get_debugTransform_7() const { return ___debugTransform_7; }
	inline Transform_t3275118058 ** get_address_of_debugTransform_7() { return &___debugTransform_7; }
	inline void set_debugTransform_7(Transform_t3275118058 * value)
	{
		___debugTransform_7 = value;
		Il2CppCodeGenWriteBarrier(&___debugTransform_7, value);
	}

	inline static int32_t get_offset_of_relativeMovementOfCameraRig_8() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_t3041247552, ___relativeMovementOfCameraRig_8)); }
	inline Vector3_t2243707580  get_relativeMovementOfCameraRig_8() const { return ___relativeMovementOfCameraRig_8; }
	inline Vector3_t2243707580 * get_address_of_relativeMovementOfCameraRig_8() { return &___relativeMovementOfCameraRig_8; }
	inline void set_relativeMovementOfCameraRig_8(Vector3_t2243707580  value)
	{
		___relativeMovementOfCameraRig_8 = value;
	}

	inline static int32_t get_offset_of_movementTransform_9() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_t3041247552, ___movementTransform_9)); }
	inline Transform_t3275118058 * get_movementTransform_9() const { return ___movementTransform_9; }
	inline Transform_t3275118058 ** get_address_of_movementTransform_9() { return &___movementTransform_9; }
	inline void set_movementTransform_9(Transform_t3275118058 * value)
	{
		___movementTransform_9 = value;
		Il2CppCodeGenWriteBarrier(&___movementTransform_9, value);
	}

	inline static int32_t get_offset_of_playArea_10() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_t3041247552, ___playArea_10)); }
	inline Transform_t3275118058 * get_playArea_10() const { return ___playArea_10; }
	inline Transform_t3275118058 ** get_address_of_playArea_10() { return &___playArea_10; }
	inline void set_playArea_10(Transform_t3275118058 * value)
	{
		___playArea_10 = value;
		Il2CppCodeGenWriteBarrier(&___playArea_10, value);
	}

	inline static int32_t get_offset_of_headCirclePosition_11() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_t3041247552, ___headCirclePosition_11)); }
	inline Vector3_t2243707580  get_headCirclePosition_11() const { return ___headCirclePosition_11; }
	inline Vector3_t2243707580 * get_address_of_headCirclePosition_11() { return &___headCirclePosition_11; }
	inline void set_headCirclePosition_11(Vector3_t2243707580  value)
	{
		___headCirclePosition_11 = value;
	}

	inline static int32_t get_offset_of_lastPosition_12() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_t3041247552, ___lastPosition_12)); }
	inline Vector3_t2243707580  get_lastPosition_12() const { return ___lastPosition_12; }
	inline Vector3_t2243707580 * get_address_of_lastPosition_12() { return &___lastPosition_12; }
	inline void set_lastPosition_12(Vector3_t2243707580  value)
	{
		___lastPosition_12 = value;
	}

	inline static int32_t get_offset_of_lastMovement_13() { return static_cast<int32_t>(offsetof(VRTK_RoomExtender_t3041247552, ___lastMovement_13)); }
	inline Vector3_t2243707580  get_lastMovement_13() const { return ___lastMovement_13; }
	inline Vector3_t2243707580 * get_address_of_lastMovement_13() { return &___lastMovement_13; }
	inline void set_lastMovement_13(Vector3_t2243707580  value)
	{
		___lastMovement_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
