﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.BodyPhysicsEventHandler
struct BodyPhysicsEventHandler_t3963963105;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_VRTK_BodyPhysicsEventArgs2230131654.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void VRTK.BodyPhysicsEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void BodyPhysicsEventHandler__ctor_m2950295457 (BodyPhysicsEventHandler_t3963963105 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.BodyPhysicsEventHandler::Invoke(System.Object,VRTK.BodyPhysicsEventArgs)
extern "C"  void BodyPhysicsEventHandler_Invoke_m4210610102 (BodyPhysicsEventHandler_t3963963105 * __this, Il2CppObject * ___sender0, BodyPhysicsEventArgs_t2230131654  ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult VRTK.BodyPhysicsEventHandler::BeginInvoke(System.Object,VRTK.BodyPhysicsEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * BodyPhysicsEventHandler_BeginInvoke_m3339813153 (BodyPhysicsEventHandler_t3963963105 * __this, Il2CppObject * ___sender0, BodyPhysicsEventArgs_t2230131654  ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.BodyPhysicsEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void BodyPhysicsEventHandler_EndInvoke_m4210103407 (BodyPhysicsEventHandler_t3963963105 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
