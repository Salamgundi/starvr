﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen2508261327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SteamVR_Events/Event`2<System.Object,System.Boolean>
struct  Event_2_t175250481  : public UnityEvent_2_t2508261327
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
