﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.PanelMenuItemController
struct PanelMenuItemController_t3837844790;
// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.PanelMenuController
struct  PanelMenuController_t2234710621  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject VRTK.PanelMenuController::rotateTowards
	GameObject_t1756533147 * ___rotateTowards_2;
	// System.Single VRTK.PanelMenuController::zoomScaleMultiplier
	float ___zoomScaleMultiplier_3;
	// VRTK.PanelMenuItemController VRTK.PanelMenuController::topPanelMenuItemController
	PanelMenuItemController_t3837844790 * ___topPanelMenuItemController_4;
	// VRTK.PanelMenuItemController VRTK.PanelMenuController::bottomPanelMenuItemController
	PanelMenuItemController_t3837844790 * ___bottomPanelMenuItemController_5;
	// VRTK.PanelMenuItemController VRTK.PanelMenuController::leftPanelMenuItemController
	PanelMenuItemController_t3837844790 * ___leftPanelMenuItemController_6;
	// VRTK.PanelMenuItemController VRTK.PanelMenuController::rightPanelMenuItemController
	PanelMenuItemController_t3837844790 * ___rightPanelMenuItemController_7;
	// VRTK.VRTK_ControllerEvents VRTK.PanelMenuController::controllerEvents
	VRTK_ControllerEvents_t3225224819 * ___controllerEvents_12;
	// VRTK.PanelMenuItemController VRTK.PanelMenuController::currentPanelMenuItemController
	PanelMenuItemController_t3837844790 * ___currentPanelMenuItemController_13;
	// UnityEngine.GameObject VRTK.PanelMenuController::interactableObject
	GameObject_t1756533147 * ___interactableObject_14;
	// UnityEngine.GameObject VRTK.PanelMenuController::canvasObject
	GameObject_t1756533147 * ___canvasObject_15;
	// UnityEngine.Vector2 VRTK.PanelMenuController::xAxis
	Vector2_t2243707579  ___xAxis_16;
	// UnityEngine.Vector2 VRTK.PanelMenuController::yAxis
	Vector2_t2243707579  ___yAxis_17;
	// UnityEngine.Vector2 VRTK.PanelMenuController::touchStartPosition
	Vector2_t2243707579  ___touchStartPosition_18;
	// UnityEngine.Vector2 VRTK.PanelMenuController::touchEndPosition
	Vector2_t2243707579  ___touchEndPosition_19;
	// System.Single VRTK.PanelMenuController::touchStartTime
	float ___touchStartTime_20;
	// System.Single VRTK.PanelMenuController::currentAngle
	float ___currentAngle_21;
	// System.Boolean VRTK.PanelMenuController::isTrackingSwipe
	bool ___isTrackingSwipe_22;
	// System.Boolean VRTK.PanelMenuController::isPendingSwipeCheck
	bool ___isPendingSwipeCheck_23;
	// System.Boolean VRTK.PanelMenuController::isGrabbed
	bool ___isGrabbed_24;
	// System.Boolean VRTK.PanelMenuController::isShown
	bool ___isShown_25;

public:
	inline static int32_t get_offset_of_rotateTowards_2() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___rotateTowards_2)); }
	inline GameObject_t1756533147 * get_rotateTowards_2() const { return ___rotateTowards_2; }
	inline GameObject_t1756533147 ** get_address_of_rotateTowards_2() { return &___rotateTowards_2; }
	inline void set_rotateTowards_2(GameObject_t1756533147 * value)
	{
		___rotateTowards_2 = value;
		Il2CppCodeGenWriteBarrier(&___rotateTowards_2, value);
	}

	inline static int32_t get_offset_of_zoomScaleMultiplier_3() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___zoomScaleMultiplier_3)); }
	inline float get_zoomScaleMultiplier_3() const { return ___zoomScaleMultiplier_3; }
	inline float* get_address_of_zoomScaleMultiplier_3() { return &___zoomScaleMultiplier_3; }
	inline void set_zoomScaleMultiplier_3(float value)
	{
		___zoomScaleMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_topPanelMenuItemController_4() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___topPanelMenuItemController_4)); }
	inline PanelMenuItemController_t3837844790 * get_topPanelMenuItemController_4() const { return ___topPanelMenuItemController_4; }
	inline PanelMenuItemController_t3837844790 ** get_address_of_topPanelMenuItemController_4() { return &___topPanelMenuItemController_4; }
	inline void set_topPanelMenuItemController_4(PanelMenuItemController_t3837844790 * value)
	{
		___topPanelMenuItemController_4 = value;
		Il2CppCodeGenWriteBarrier(&___topPanelMenuItemController_4, value);
	}

	inline static int32_t get_offset_of_bottomPanelMenuItemController_5() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___bottomPanelMenuItemController_5)); }
	inline PanelMenuItemController_t3837844790 * get_bottomPanelMenuItemController_5() const { return ___bottomPanelMenuItemController_5; }
	inline PanelMenuItemController_t3837844790 ** get_address_of_bottomPanelMenuItemController_5() { return &___bottomPanelMenuItemController_5; }
	inline void set_bottomPanelMenuItemController_5(PanelMenuItemController_t3837844790 * value)
	{
		___bottomPanelMenuItemController_5 = value;
		Il2CppCodeGenWriteBarrier(&___bottomPanelMenuItemController_5, value);
	}

	inline static int32_t get_offset_of_leftPanelMenuItemController_6() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___leftPanelMenuItemController_6)); }
	inline PanelMenuItemController_t3837844790 * get_leftPanelMenuItemController_6() const { return ___leftPanelMenuItemController_6; }
	inline PanelMenuItemController_t3837844790 ** get_address_of_leftPanelMenuItemController_6() { return &___leftPanelMenuItemController_6; }
	inline void set_leftPanelMenuItemController_6(PanelMenuItemController_t3837844790 * value)
	{
		___leftPanelMenuItemController_6 = value;
		Il2CppCodeGenWriteBarrier(&___leftPanelMenuItemController_6, value);
	}

	inline static int32_t get_offset_of_rightPanelMenuItemController_7() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___rightPanelMenuItemController_7)); }
	inline PanelMenuItemController_t3837844790 * get_rightPanelMenuItemController_7() const { return ___rightPanelMenuItemController_7; }
	inline PanelMenuItemController_t3837844790 ** get_address_of_rightPanelMenuItemController_7() { return &___rightPanelMenuItemController_7; }
	inline void set_rightPanelMenuItemController_7(PanelMenuItemController_t3837844790 * value)
	{
		___rightPanelMenuItemController_7 = value;
		Il2CppCodeGenWriteBarrier(&___rightPanelMenuItemController_7, value);
	}

	inline static int32_t get_offset_of_controllerEvents_12() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___controllerEvents_12)); }
	inline VRTK_ControllerEvents_t3225224819 * get_controllerEvents_12() const { return ___controllerEvents_12; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_controllerEvents_12() { return &___controllerEvents_12; }
	inline void set_controllerEvents_12(VRTK_ControllerEvents_t3225224819 * value)
	{
		___controllerEvents_12 = value;
		Il2CppCodeGenWriteBarrier(&___controllerEvents_12, value);
	}

	inline static int32_t get_offset_of_currentPanelMenuItemController_13() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___currentPanelMenuItemController_13)); }
	inline PanelMenuItemController_t3837844790 * get_currentPanelMenuItemController_13() const { return ___currentPanelMenuItemController_13; }
	inline PanelMenuItemController_t3837844790 ** get_address_of_currentPanelMenuItemController_13() { return &___currentPanelMenuItemController_13; }
	inline void set_currentPanelMenuItemController_13(PanelMenuItemController_t3837844790 * value)
	{
		___currentPanelMenuItemController_13 = value;
		Il2CppCodeGenWriteBarrier(&___currentPanelMenuItemController_13, value);
	}

	inline static int32_t get_offset_of_interactableObject_14() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___interactableObject_14)); }
	inline GameObject_t1756533147 * get_interactableObject_14() const { return ___interactableObject_14; }
	inline GameObject_t1756533147 ** get_address_of_interactableObject_14() { return &___interactableObject_14; }
	inline void set_interactableObject_14(GameObject_t1756533147 * value)
	{
		___interactableObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___interactableObject_14, value);
	}

	inline static int32_t get_offset_of_canvasObject_15() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___canvasObject_15)); }
	inline GameObject_t1756533147 * get_canvasObject_15() const { return ___canvasObject_15; }
	inline GameObject_t1756533147 ** get_address_of_canvasObject_15() { return &___canvasObject_15; }
	inline void set_canvasObject_15(GameObject_t1756533147 * value)
	{
		___canvasObject_15 = value;
		Il2CppCodeGenWriteBarrier(&___canvasObject_15, value);
	}

	inline static int32_t get_offset_of_xAxis_16() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___xAxis_16)); }
	inline Vector2_t2243707579  get_xAxis_16() const { return ___xAxis_16; }
	inline Vector2_t2243707579 * get_address_of_xAxis_16() { return &___xAxis_16; }
	inline void set_xAxis_16(Vector2_t2243707579  value)
	{
		___xAxis_16 = value;
	}

	inline static int32_t get_offset_of_yAxis_17() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___yAxis_17)); }
	inline Vector2_t2243707579  get_yAxis_17() const { return ___yAxis_17; }
	inline Vector2_t2243707579 * get_address_of_yAxis_17() { return &___yAxis_17; }
	inline void set_yAxis_17(Vector2_t2243707579  value)
	{
		___yAxis_17 = value;
	}

	inline static int32_t get_offset_of_touchStartPosition_18() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___touchStartPosition_18)); }
	inline Vector2_t2243707579  get_touchStartPosition_18() const { return ___touchStartPosition_18; }
	inline Vector2_t2243707579 * get_address_of_touchStartPosition_18() { return &___touchStartPosition_18; }
	inline void set_touchStartPosition_18(Vector2_t2243707579  value)
	{
		___touchStartPosition_18 = value;
	}

	inline static int32_t get_offset_of_touchEndPosition_19() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___touchEndPosition_19)); }
	inline Vector2_t2243707579  get_touchEndPosition_19() const { return ___touchEndPosition_19; }
	inline Vector2_t2243707579 * get_address_of_touchEndPosition_19() { return &___touchEndPosition_19; }
	inline void set_touchEndPosition_19(Vector2_t2243707579  value)
	{
		___touchEndPosition_19 = value;
	}

	inline static int32_t get_offset_of_touchStartTime_20() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___touchStartTime_20)); }
	inline float get_touchStartTime_20() const { return ___touchStartTime_20; }
	inline float* get_address_of_touchStartTime_20() { return &___touchStartTime_20; }
	inline void set_touchStartTime_20(float value)
	{
		___touchStartTime_20 = value;
	}

	inline static int32_t get_offset_of_currentAngle_21() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___currentAngle_21)); }
	inline float get_currentAngle_21() const { return ___currentAngle_21; }
	inline float* get_address_of_currentAngle_21() { return &___currentAngle_21; }
	inline void set_currentAngle_21(float value)
	{
		___currentAngle_21 = value;
	}

	inline static int32_t get_offset_of_isTrackingSwipe_22() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___isTrackingSwipe_22)); }
	inline bool get_isTrackingSwipe_22() const { return ___isTrackingSwipe_22; }
	inline bool* get_address_of_isTrackingSwipe_22() { return &___isTrackingSwipe_22; }
	inline void set_isTrackingSwipe_22(bool value)
	{
		___isTrackingSwipe_22 = value;
	}

	inline static int32_t get_offset_of_isPendingSwipeCheck_23() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___isPendingSwipeCheck_23)); }
	inline bool get_isPendingSwipeCheck_23() const { return ___isPendingSwipeCheck_23; }
	inline bool* get_address_of_isPendingSwipeCheck_23() { return &___isPendingSwipeCheck_23; }
	inline void set_isPendingSwipeCheck_23(bool value)
	{
		___isPendingSwipeCheck_23 = value;
	}

	inline static int32_t get_offset_of_isGrabbed_24() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___isGrabbed_24)); }
	inline bool get_isGrabbed_24() const { return ___isGrabbed_24; }
	inline bool* get_address_of_isGrabbed_24() { return &___isGrabbed_24; }
	inline void set_isGrabbed_24(bool value)
	{
		___isGrabbed_24 = value;
	}

	inline static int32_t get_offset_of_isShown_25() { return static_cast<int32_t>(offsetof(PanelMenuController_t2234710621, ___isShown_25)); }
	inline bool get_isShown_25() const { return ___isShown_25; }
	inline bool* get_address_of_isShown_25() { return &___isShown_25; }
	inline void set_isShown_25(bool value)
	{
		___isShown_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
