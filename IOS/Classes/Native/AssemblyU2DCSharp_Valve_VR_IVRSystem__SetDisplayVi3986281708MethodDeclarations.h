﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_SetDisplayVisibility
struct _SetDisplayVisibility_t3986281708;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_SetDisplayVisibility::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetDisplayVisibility__ctor_m1856170069 (_SetDisplayVisibility_t3986281708 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_SetDisplayVisibility::Invoke(System.Boolean)
extern "C"  bool _SetDisplayVisibility_Invoke_m1204514572 (_SetDisplayVisibility_t3986281708 * __this, bool ___bIsVisibleOnDesktop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_SetDisplayVisibility::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetDisplayVisibility_BeginInvoke_m1727513097 (_SetDisplayVisibility_t3986281708 * __this, bool ___bIsVisibleOnDesktop0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Valve.VR.IVRSystem/_SetDisplayVisibility::EndInvoke(System.IAsyncResult)
extern "C"  bool _SetDisplayVisibility_EndInvoke_m3396694227 (_SetDisplayVisibility_t3986281708 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
