﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VRTK_VRTK_InteractableObject2604188111.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.Examples.RealGun_Slide
struct  RealGun_Slide_t2998553338  : public VRTK_InteractableObject_t2604188111
{
public:
	// System.Single VRTK.Examples.RealGun_Slide::restPosition
	float ___restPosition_45;
	// System.Single VRTK.Examples.RealGun_Slide::fireTimer
	float ___fireTimer_46;
	// System.Single VRTK.Examples.RealGun_Slide::fireDistance
	float ___fireDistance_47;
	// System.Single VRTK.Examples.RealGun_Slide::boltSpeed
	float ___boltSpeed_48;

public:
	inline static int32_t get_offset_of_restPosition_45() { return static_cast<int32_t>(offsetof(RealGun_Slide_t2998553338, ___restPosition_45)); }
	inline float get_restPosition_45() const { return ___restPosition_45; }
	inline float* get_address_of_restPosition_45() { return &___restPosition_45; }
	inline void set_restPosition_45(float value)
	{
		___restPosition_45 = value;
	}

	inline static int32_t get_offset_of_fireTimer_46() { return static_cast<int32_t>(offsetof(RealGun_Slide_t2998553338, ___fireTimer_46)); }
	inline float get_fireTimer_46() const { return ___fireTimer_46; }
	inline float* get_address_of_fireTimer_46() { return &___fireTimer_46; }
	inline void set_fireTimer_46(float value)
	{
		___fireTimer_46 = value;
	}

	inline static int32_t get_offset_of_fireDistance_47() { return static_cast<int32_t>(offsetof(RealGun_Slide_t2998553338, ___fireDistance_47)); }
	inline float get_fireDistance_47() const { return ___fireDistance_47; }
	inline float* get_address_of_fireDistance_47() { return &___fireDistance_47; }
	inline void set_fireDistance_47(float value)
	{
		___fireDistance_47 = value;
	}

	inline static int32_t get_offset_of_boltSpeed_48() { return static_cast<int32_t>(offsetof(RealGun_Slide_t2998553338, ___boltSpeed_48)); }
	inline float get_boltSpeed_48() const { return ___boltSpeed_48; }
	inline float* get_address_of_boltSpeed_48() { return &___boltSpeed_48; }
	inline void set_boltSpeed_48(float value)
	{
		___boltSpeed_48 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
