﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRChaperone/_ReloadInfo
struct _ReloadInfo_t2817167257;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRChaperone/_ReloadInfo::.ctor(System.Object,System.IntPtr)
extern "C"  void _ReloadInfo__ctor_m1431758802 (_ReloadInfo_t2817167257 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperone/_ReloadInfo::Invoke()
extern "C"  void _ReloadInfo_Invoke_m1251374592 (_ReloadInfo_t2817167257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRChaperone/_ReloadInfo::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _ReloadInfo_BeginInvoke_m408118263 (_ReloadInfo_t2817167257 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Valve.VR.IVRChaperone/_ReloadInfo::EndInvoke(System.IAsyncResult)
extern "C"  void _ReloadInfo_EndInvoke_m3529575012 (_ReloadInfo_t2817167257 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
