﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.HeadsetControllerAwareEventArgs
struct  HeadsetControllerAwareEventArgs_t2653531721 
{
public:
	// UnityEngine.RaycastHit VRTK.HeadsetControllerAwareEventArgs::raycastHit
	RaycastHit_t87180320  ___raycastHit_0;
	// System.UInt32 VRTK.HeadsetControllerAwareEventArgs::controllerIndex
	uint32_t ___controllerIndex_1;

public:
	inline static int32_t get_offset_of_raycastHit_0() { return static_cast<int32_t>(offsetof(HeadsetControllerAwareEventArgs_t2653531721, ___raycastHit_0)); }
	inline RaycastHit_t87180320  get_raycastHit_0() const { return ___raycastHit_0; }
	inline RaycastHit_t87180320 * get_address_of_raycastHit_0() { return &___raycastHit_0; }
	inline void set_raycastHit_0(RaycastHit_t87180320  value)
	{
		___raycastHit_0 = value;
	}

	inline static int32_t get_offset_of_controllerIndex_1() { return static_cast<int32_t>(offsetof(HeadsetControllerAwareEventArgs_t2653531721, ___controllerIndex_1)); }
	inline uint32_t get_controllerIndex_1() const { return ___controllerIndex_1; }
	inline uint32_t* get_address_of_controllerIndex_1() { return &___controllerIndex_1; }
	inline void set_controllerIndex_1(uint32_t value)
	{
		___controllerIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VRTK.HeadsetControllerAwareEventArgs
struct HeadsetControllerAwareEventArgs_t2653531721_marshaled_pinvoke
{
	RaycastHit_t87180320_marshaled_pinvoke ___raycastHit_0;
	uint32_t ___controllerIndex_1;
};
// Native definition for COM marshalling of VRTK.HeadsetControllerAwareEventArgs
struct HeadsetControllerAwareEventArgs_t2653531721_marshaled_com
{
	RaycastHit_t87180320_marshaled_com ___raycastHit_0;
	uint32_t ___controllerIndex_1;
};
