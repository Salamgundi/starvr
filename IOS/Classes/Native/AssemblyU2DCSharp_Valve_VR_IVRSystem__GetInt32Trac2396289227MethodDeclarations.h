﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRSystem/_GetInt32TrackedDeviceProperty
struct _GetInt32TrackedDeviceProperty_t2396289227;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedDeviceProperty3226377054.h"
#include "AssemblyU2DCSharp_Valve_VR_ETrackedPropertyError3340022390.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRSystem/_GetInt32TrackedDeviceProperty::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetInt32TrackedDeviceProperty__ctor_m3778535034 (_GetInt32TrackedDeviceProperty_t2396289227 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Valve.VR.IVRSystem/_GetInt32TrackedDeviceProperty::Invoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&)
extern "C"  int32_t _GetInt32TrackedDeviceProperty_Invoke_m1360536014 (_GetInt32TrackedDeviceProperty_t2396289227 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRSystem/_GetInt32TrackedDeviceProperty::BeginInvoke(System.UInt32,Valve.VR.ETrackedDeviceProperty,Valve.VR.ETrackedPropertyError&,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetInt32TrackedDeviceProperty_BeginInvoke_m2934564355 (_GetInt32TrackedDeviceProperty_t2396289227 * __this, uint32_t ___unDeviceIndex0, int32_t ___prop1, int32_t* ___pError2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Valve.VR.IVRSystem/_GetInt32TrackedDeviceProperty::EndInvoke(Valve.VR.ETrackedPropertyError&,System.IAsyncResult)
extern "C"  int32_t _GetInt32TrackedDeviceProperty_EndInvoke_m213712922 (_GetInt32TrackedDeviceProperty_t2396289227 * __this, int32_t* ___pError0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
