﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRRenderModels/_GetComponentButtonMask
struct _GetComponentButtonMask_t1474657094;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRRenderModels/_GetComponentButtonMask::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetComponentButtonMask__ctor_m1294274575 (_GetComponentButtonMask_t1474657094 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.IVRRenderModels/_GetComponentButtonMask::Invoke(System.String,System.String)
extern "C"  uint64_t _GetComponentButtonMask_Invoke_m2851395467 (_GetComponentButtonMask_t1474657094 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRRenderModels/_GetComponentButtonMask::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetComponentButtonMask_BeginInvoke_m4122093192 (_GetComponentButtonMask_t1474657094 * __this, String_t* ___pchRenderModelName0, String_t* ___pchComponentName1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Valve.VR.IVRRenderModels/_GetComponentButtonMask::EndInvoke(System.IAsyncResult)
extern "C"  uint64_t _GetComponentButtonMask_EndInvoke_m313207949 (_GetComponentButtonMask_t1474657094 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
