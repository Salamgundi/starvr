﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.VRTK_BasePointerRenderer
struct VRTK_BasePointerRenderer_t1270536273;
// VRTK.VRTK_Pointer
struct VRTK_Pointer_t2647108841;
// VRTK.VRTK_PolicyList
struct VRTK_PolicyList_t2965133344;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_Pointer2647108841.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_PolicyList2965133344.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BasePointerRenderer_Vis517960985.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void VRTK.VRTK_BasePointerRenderer::.ctor()
extern "C"  void VRTK_BasePointerRenderer__ctor_m3293965829 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::InitalizePointer(VRTK.VRTK_Pointer,VRTK.VRTK_PolicyList,System.Single,System.Boolean)
extern "C"  void VRTK_BasePointerRenderer_InitalizePointer_m2977208268 (VRTK_BasePointerRenderer_t1270536273 * __this, VRTK_Pointer_t2647108841 * ___givenPointer0, VRTK_PolicyList_t2965133344 * ___givenInvalidListPolicy1, float ___givenNavMeshCheckDistance2, bool ___givenHeadsetPositionCompensation3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::Toggle(System.Boolean,System.Boolean)
extern "C"  void VRTK_BasePointerRenderer_Toggle_m1782510463 (VRTK_BasePointerRenderer_t1270536273 * __this, bool ___pointerState0, bool ___actualState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::ToggleInteraction(System.Boolean)
extern "C"  void VRTK_BasePointerRenderer_ToggleInteraction_m3489880988 (VRTK_BasePointerRenderer_t1270536273 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::UpdateRenderer()
extern "C"  void VRTK_BasePointerRenderer_UpdateRenderer_m1815971309 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit VRTK.VRTK_BasePointerRenderer::GetDestinationHit()
extern "C"  RaycastHit_t87180320  VRTK_BasePointerRenderer_GetDestinationHit_m1637876890 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BasePointerRenderer::ValidPlayArea()
extern "C"  bool VRTK_BasePointerRenderer_ValidPlayArea_m1551046982 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BasePointerRenderer::IsVisible()
extern "C"  bool VRTK_BasePointerRenderer_IsVisible_m467185609 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BasePointerRenderer::IsTracerVisible()
extern "C"  bool VRTK_BasePointerRenderer_IsTracerVisible_m1444756862 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BasePointerRenderer::IsCursorVisible()
extern "C"  bool VRTK_BasePointerRenderer_IsCursorVisible_m1830436115 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::OnEnable()
extern "C"  void VRTK_BasePointerRenderer_OnEnable_m683037813 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::OnDisable()
extern "C"  void VRTK_BasePointerRenderer_OnDisable_m4203202134 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::OnValidate()
extern "C"  void VRTK_BasePointerRenderer_OnValidate_m3227075398 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::FixedUpdate()
extern "C"  void VRTK_BasePointerRenderer_FixedUpdate_m3434648926 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::ToggleObjectInteraction(System.Boolean)
extern "C"  void VRTK_BasePointerRenderer_ToggleObjectInteraction_m841433215 (VRTK_BasePointerRenderer_t1270536273 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::UpdateObjectInteractor()
extern "C"  void VRTK_BasePointerRenderer_UpdateObjectInteractor_m214500496 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::UpdatePointerOriginTransformFollow()
extern "C"  void VRTK_BasePointerRenderer_UpdatePointerOriginTransformFollow_m1048093088 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform VRTK.VRTK_BasePointerRenderer::GetOrigin(System.Boolean)
extern "C"  Transform_t3275118058 * VRTK_BasePointerRenderer_GetOrigin_m1642980324 (VRTK_BasePointerRenderer_t1270536273 * __this, bool ___smoothed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::PointerEnter(UnityEngine.RaycastHit)
extern "C"  void VRTK_BasePointerRenderer_PointerEnter_m2562569383 (VRTK_BasePointerRenderer_t1270536273 * __this, RaycastHit_t87180320  ___givenHit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::PointerExit(UnityEngine.RaycastHit)
extern "C"  void VRTK_BasePointerRenderer_PointerExit_m1068678891 (VRTK_BasePointerRenderer_t1270536273 * __this, RaycastHit_t87180320  ___givenHit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.VRTK_BasePointerRenderer::ValidDestination()
extern "C"  bool VRTK_BasePointerRenderer_ValidDestination_m1917725767 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::ToggleElement(UnityEngine.GameObject,System.Boolean,System.Boolean,VRTK.VRTK_BasePointerRenderer/VisibilityStates,System.Boolean&)
extern "C"  void VRTK_BasePointerRenderer_ToggleElement_m2237561779 (VRTK_BasePointerRenderer_t1270536273 * __this, GameObject_t1756533147 * ___givenObject0, bool ___pointerState1, bool ___actualState2, int32_t ___givenVisibility3, bool* ___currentVisible4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::AddVisibleRenderer(UnityEngine.GameObject)
extern "C"  void VRTK_BasePointerRenderer_AddVisibleRenderer_m217498787 (VRTK_BasePointerRenderer_t1270536273 * __this, GameObject_t1756533147 * ___givenObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::MakeRenderersVisible()
extern "C"  void VRTK_BasePointerRenderer_MakeRenderersVisible_m2686490571 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::ToggleRendererVisibility(UnityEngine.GameObject,System.Boolean)
extern "C"  void VRTK_BasePointerRenderer_ToggleRendererVisibility_m2440736407 (VRTK_BasePointerRenderer_t1270536273 * __this, GameObject_t1756533147 * ___givenObject0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::SetupMaterialRenderer(UnityEngine.GameObject)
extern "C"  void VRTK_BasePointerRenderer_SetupMaterialRenderer_m1818773028 (VRTK_BasePointerRenderer_t1270536273 * __this, GameObject_t1756533147 * ___givenObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::ChangeColor(UnityEngine.Color)
extern "C"  void VRTK_BasePointerRenderer_ChangeColor_m1647275594 (VRTK_BasePointerRenderer_t1270536273 * __this, Color_t2020392075  ___givenColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::ChangeMaterial(UnityEngine.Color)
extern "C"  void VRTK_BasePointerRenderer_ChangeMaterial_m1022403820 (VRTK_BasePointerRenderer_t1270536273 * __this, Color_t2020392075  ___givenColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::ChangeMaterialColor(UnityEngine.GameObject,UnityEngine.Color)
extern "C"  void VRTK_BasePointerRenderer_ChangeMaterialColor_m2862812279 (VRTK_BasePointerRenderer_t1270536273 * __this, GameObject_t1756533147 * ___givenObject0, Color_t2020392075  ___givenColor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::CreateObjectInteractor()
extern "C"  void VRTK_BasePointerRenderer_CreateObjectInteractor_m1663839607 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::ScaleObjectInteractor(UnityEngine.Vector3)
extern "C"  void VRTK_BasePointerRenderer_ScaleObjectInteractor_m1284718164 (VRTK_BasePointerRenderer_t1270536273 * __this, Vector3_t2243707580  ___scaleAmount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::CreatePointerOriginTransformFollow()
extern "C"  void VRTK_BasePointerRenderer_CreatePointerOriginTransformFollow_m573749919 (VRTK_BasePointerRenderer_t1270536273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VRTK.VRTK_BasePointerRenderer::OverrideBeamLength(System.Single)
extern "C"  float VRTK_BasePointerRenderer_OverrideBeamLength_m1115804121 (VRTK_BasePointerRenderer_t1270536273 * __this, float ___currentLength0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.VRTK_BasePointerRenderer::UpdateDependencies(UnityEngine.Vector3)
extern "C"  void VRTK_BasePointerRenderer_UpdateDependencies_m4224793678 (VRTK_BasePointerRenderer_t1270536273 * __this, Vector3_t2243707580  ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
