﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScaleScrollEffect
struct ScaleScrollEffect_t3430758866;

#include "codegen/il2cpp-codegen.h"

// System.Void ScaleScrollEffect::.ctor()
extern "C"  void ScaleScrollEffect__ctor_m84586081 (ScaleScrollEffect_t3430758866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
