﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En719993265MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3241719271(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3611232776 *, Dictionary_2_t2291208074 *, const MethodInfo*))Enumerator__ctor_m2698405850_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m766721288(__this, method) ((  Il2CppObject * (*) (Enumerator_t3611232776 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2475576853_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1962262254(__this, method) ((  void (*) (Enumerator_t3611232776 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4244914941_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1170069817(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3611232776 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2239374492_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3182585046(__this, method) ((  Il2CppObject * (*) (Enumerator_t3611232776 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3769630515_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2791747980(__this, method) ((  Il2CppObject * (*) (Enumerator_t3611232776 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2276293475_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::MoveNext()
#define Enumerator_MoveNext_m2132536838(__this, method) ((  bool (*) (Enumerator_t3611232776 *, const MethodInfo*))Enumerator_MoveNext_m3174323269_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::get_Current()
#define Enumerator_get_Current_m344922886(__this, method) ((  KeyValuePair_2_t48553296  (*) (Enumerator_t3611232776 *, const MethodInfo*))Enumerator_get_Current_m4293436273_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2256596627(__this, method) ((  int32_t (*) (Enumerator_t3611232776 *, const MethodInfo*))Enumerator_get_CurrentKey_m1155204714_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4012383067(__this, method) ((  Event_1_t1285721510 * (*) (Enumerator_t3611232776 *, const MethodInfo*))Enumerator_get_CurrentValue_m2453297386_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::Reset()
#define Enumerator_Reset_m1114168945(__this, method) ((  void (*) (Enumerator_t3611232776 *, const MethodInfo*))Enumerator_Reset_m2804750196_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::VerifyState()
#define Enumerator_VerifyState_m1020809726(__this, method) ((  void (*) (Enumerator_t3611232776 *, const MethodInfo*))Enumerator_VerifyState_m879070195_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m654651646(__this, method) ((  void (*) (Enumerator_t3611232776 *, const MethodInfo*))Enumerator_VerifyCurrent_m4168362253_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Valve.VR.EVREventType,SteamVR_Events/Event`1<Valve.VR.VREvent_t>>::Dispose()
#define Enumerator_Dispose_m3607144723(__this, method) ((  void (*) (Enumerator_t3611232776 *, const MethodInfo*))Enumerator_Dispose_m1726429690_gshared)(__this, method)
