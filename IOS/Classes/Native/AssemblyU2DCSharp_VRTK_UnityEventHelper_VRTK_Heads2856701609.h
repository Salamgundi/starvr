﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_HeadsetCollision
struct VRTK_HeadsetCollision_t2015187094;
// VRTK.UnityEventHelper.VRTK_HeadsetCollision_UnityEvents/UnityObjectEvent
struct UnityObjectEvent_t3010802322;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.UnityEventHelper.VRTK_HeadsetCollision_UnityEvents
struct  VRTK_HeadsetCollision_UnityEvents_t2856701609  : public MonoBehaviour_t1158329972
{
public:
	// VRTK.VRTK_HeadsetCollision VRTK.UnityEventHelper.VRTK_HeadsetCollision_UnityEvents::hc
	VRTK_HeadsetCollision_t2015187094 * ___hc_2;
	// VRTK.UnityEventHelper.VRTK_HeadsetCollision_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_HeadsetCollision_UnityEvents::OnHeadsetCollisionDetect
	UnityObjectEvent_t3010802322 * ___OnHeadsetCollisionDetect_3;
	// VRTK.UnityEventHelper.VRTK_HeadsetCollision_UnityEvents/UnityObjectEvent VRTK.UnityEventHelper.VRTK_HeadsetCollision_UnityEvents::OnHeadsetCollisionEnded
	UnityObjectEvent_t3010802322 * ___OnHeadsetCollisionEnded_4;

public:
	inline static int32_t get_offset_of_hc_2() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollision_UnityEvents_t2856701609, ___hc_2)); }
	inline VRTK_HeadsetCollision_t2015187094 * get_hc_2() const { return ___hc_2; }
	inline VRTK_HeadsetCollision_t2015187094 ** get_address_of_hc_2() { return &___hc_2; }
	inline void set_hc_2(VRTK_HeadsetCollision_t2015187094 * value)
	{
		___hc_2 = value;
		Il2CppCodeGenWriteBarrier(&___hc_2, value);
	}

	inline static int32_t get_offset_of_OnHeadsetCollisionDetect_3() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollision_UnityEvents_t2856701609, ___OnHeadsetCollisionDetect_3)); }
	inline UnityObjectEvent_t3010802322 * get_OnHeadsetCollisionDetect_3() const { return ___OnHeadsetCollisionDetect_3; }
	inline UnityObjectEvent_t3010802322 ** get_address_of_OnHeadsetCollisionDetect_3() { return &___OnHeadsetCollisionDetect_3; }
	inline void set_OnHeadsetCollisionDetect_3(UnityObjectEvent_t3010802322 * value)
	{
		___OnHeadsetCollisionDetect_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnHeadsetCollisionDetect_3, value);
	}

	inline static int32_t get_offset_of_OnHeadsetCollisionEnded_4() { return static_cast<int32_t>(offsetof(VRTK_HeadsetCollision_UnityEvents_t2856701609, ___OnHeadsetCollisionEnded_4)); }
	inline UnityObjectEvent_t3010802322 * get_OnHeadsetCollisionEnded_4() const { return ___OnHeadsetCollisionEnded_4; }
	inline UnityObjectEvent_t3010802322 ** get_address_of_OnHeadsetCollisionEnded_4() { return &___OnHeadsetCollisionEnded_4; }
	inline void set_OnHeadsetCollisionEnded_4(UnityObjectEvent_t3010802322 * value)
	{
		___OnHeadsetCollisionEnded_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnHeadsetCollisionEnded_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
