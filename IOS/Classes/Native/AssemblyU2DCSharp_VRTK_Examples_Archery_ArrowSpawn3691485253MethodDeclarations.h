﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Archery.ArrowSpawner
struct ArrowSpawner_t3691485253;
// UnityEngine.Collider
struct Collider_t3497673348;
// VRTK.VRTK_InteractGrab
struct VRTK_InteractGrab_t124353446;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_InteractGrab124353446.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VRTK.Examples.Archery.ArrowSpawner::.ctor()
extern "C"  void ArrowSpawner__ctor_m2318589226 (ArrowSpawner_t3691485253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.ArrowSpawner::Start()
extern "C"  void ArrowSpawner_Start_m3834651742 (ArrowSpawner_t3691485253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.ArrowSpawner::OnTriggerStay(UnityEngine.Collider)
extern "C"  void ArrowSpawner_OnTriggerStay_m3304335665 (ArrowSpawner_t3691485253 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.Examples.Archery.ArrowSpawner::CanGrab(VRTK.VRTK_InteractGrab)
extern "C"  bool ArrowSpawner_CanGrab_m1784937869 (ArrowSpawner_t3691485253 * __this, VRTK_InteractGrab_t124353446 * ___grabbingController0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VRTK.Examples.Archery.ArrowSpawner::NoArrowNotched(UnityEngine.GameObject)
extern "C"  bool ArrowSpawner_NoArrowNotched_m2785184009 (ArrowSpawner_t3691485253 * __this, GameObject_t1756533147 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
