﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VRTK.VRTK_ControllerEvents
struct VRTK_ControllerEvents_t3225224819;
// UnityEngine.Transform
struct Transform_t3275118058;
// VRTK.VRTK_BasePointer/PointerOriginSmoothingSettings
struct PointerOriginSmoothingSettings_t2805273516;
// UnityEngine.Material
struct Material_t193706927;
// VRTK.VRTK_PlayAreaCursor
struct VRTK_PlayAreaCursor_t3566057915;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRTK.VRTK_InteractableObject
struct VRTK_InteractableObject_t2604188111;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// VRTK.VRTK_InteractGrab
struct VRTK_InteractGrab_t124353446;
// VRTK.VRTK_TransformFollow
struct VRTK_TransformFollow_t3532748285;

#include "AssemblyU2DCSharp_VRTK_VRTK_DestinationMarker667613644.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_VRTK_VRTK_BasePointer_pointerVisi986354463.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTK.VRTK_BasePointer
struct  VRTK_BasePointer_t3466805540  : public VRTK_DestinationMarker_t667613644
{
public:
	// VRTK.VRTK_ControllerEvents VRTK.VRTK_BasePointer::controller
	VRTK_ControllerEvents_t3225224819 * ___controller_9;
	// UnityEngine.Transform VRTK.VRTK_BasePointer::pointerOriginTransform
	Transform_t3275118058 * ___pointerOriginTransform_10;
	// VRTK.VRTK_BasePointer/PointerOriginSmoothingSettings VRTK.VRTK_BasePointer::pointerOriginSmoothingSettings
	PointerOriginSmoothingSettings_t2805273516 * ___pointerOriginSmoothingSettings_11;
	// UnityEngine.Material VRTK.VRTK_BasePointer::pointerMaterial
	Material_t193706927 * ___pointerMaterial_12;
	// UnityEngine.Color VRTK.VRTK_BasePointer::pointerHitColor
	Color_t2020392075  ___pointerHitColor_13;
	// UnityEngine.Color VRTK.VRTK_BasePointer::pointerMissColor
	Color_t2020392075  ___pointerMissColor_14;
	// System.Boolean VRTK.VRTK_BasePointer::holdButtonToActivate
	bool ___holdButtonToActivate_15;
	// System.Boolean VRTK.VRTK_BasePointer::interactWithObjects
	bool ___interactWithObjects_16;
	// System.Boolean VRTK.VRTK_BasePointer::grabToPointerTip
	bool ___grabToPointerTip_17;
	// System.Single VRTK.VRTK_BasePointer::activateDelay
	float ___activateDelay_18;
	// VRTK.VRTK_BasePointer/pointerVisibilityStates VRTK.VRTK_BasePointer::pointerVisibility
	int32_t ___pointerVisibility_19;
	// UnityEngine.LayerMask VRTK.VRTK_BasePointer::layersToIgnore
	LayerMask_t3188175821  ___layersToIgnore_20;
	// UnityEngine.Vector3 VRTK.VRTK_BasePointer::destinationPosition
	Vector3_t2243707580  ___destinationPosition_21;
	// System.Single VRTK.VRTK_BasePointer::pointerContactDistance
	float ___pointerContactDistance_22;
	// UnityEngine.Transform VRTK.VRTK_BasePointer::pointerContactTarget
	Transform_t3275118058 * ___pointerContactTarget_23;
	// UnityEngine.RaycastHit VRTK.VRTK_BasePointer::pointerContactRaycastHit
	RaycastHit_t87180320  ___pointerContactRaycastHit_24;
	// System.UInt32 VRTK.VRTK_BasePointer::controllerIndex
	uint32_t ___controllerIndex_25;
	// VRTK.VRTK_PlayAreaCursor VRTK.VRTK_BasePointer::playAreaCursor
	VRTK_PlayAreaCursor_t3566057915 * ___playAreaCursor_26;
	// UnityEngine.Color VRTK.VRTK_BasePointer::currentPointerColor
	Color_t2020392075  ___currentPointerColor_27;
	// UnityEngine.GameObject VRTK.VRTK_BasePointer::objectInteractor
	GameObject_t1756533147 * ___objectInteractor_28;
	// UnityEngine.GameObject VRTK.VRTK_BasePointer::objectInteractorAttachPoint
	GameObject_t1756533147 * ___objectInteractorAttachPoint_29;
	// System.Boolean VRTK.VRTK_BasePointer::isActive
	bool ___isActive_30;
	// System.Boolean VRTK.VRTK_BasePointer::destinationSetActive
	bool ___destinationSetActive_31;
	// System.Single VRTK.VRTK_BasePointer::activateDelayTimer
	float ___activateDelayTimer_32;
	// System.Int32 VRTK.VRTK_BasePointer::beamEnabledState
	int32_t ___beamEnabledState_33;
	// VRTK.VRTK_InteractableObject VRTK.VRTK_BasePointer::interactableObject
	VRTK_InteractableObject_t2604188111 * ___interactableObject_34;
	// UnityEngine.Rigidbody VRTK.VRTK_BasePointer::savedAttachPoint
	Rigidbody_t4233889191 * ___savedAttachPoint_35;
	// System.Boolean VRTK.VRTK_BasePointer::attachedToInteractorAttachPoint
	bool ___attachedToInteractorAttachPoint_36;
	// System.Single VRTK.VRTK_BasePointer::savedBeamLength
	float ___savedBeamLength_37;
	// VRTK.VRTK_InteractGrab VRTK.VRTK_BasePointer::controllerGrabScript
	VRTK_InteractGrab_t124353446 * ___controllerGrabScript_38;
	// UnityEngine.GameObject VRTK.VRTK_BasePointer::pointerOriginTransformFollowGameObject
	GameObject_t1756533147 * ___pointerOriginTransformFollowGameObject_39;
	// VRTK.VRTK_TransformFollow VRTK.VRTK_BasePointer::pointerOriginTransformFollow
	VRTK_TransformFollow_t3532748285 * ___pointerOriginTransformFollow_40;

public:
	inline static int32_t get_offset_of_controller_9() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___controller_9)); }
	inline VRTK_ControllerEvents_t3225224819 * get_controller_9() const { return ___controller_9; }
	inline VRTK_ControllerEvents_t3225224819 ** get_address_of_controller_9() { return &___controller_9; }
	inline void set_controller_9(VRTK_ControllerEvents_t3225224819 * value)
	{
		___controller_9 = value;
		Il2CppCodeGenWriteBarrier(&___controller_9, value);
	}

	inline static int32_t get_offset_of_pointerOriginTransform_10() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___pointerOriginTransform_10)); }
	inline Transform_t3275118058 * get_pointerOriginTransform_10() const { return ___pointerOriginTransform_10; }
	inline Transform_t3275118058 ** get_address_of_pointerOriginTransform_10() { return &___pointerOriginTransform_10; }
	inline void set_pointerOriginTransform_10(Transform_t3275118058 * value)
	{
		___pointerOriginTransform_10 = value;
		Il2CppCodeGenWriteBarrier(&___pointerOriginTransform_10, value);
	}

	inline static int32_t get_offset_of_pointerOriginSmoothingSettings_11() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___pointerOriginSmoothingSettings_11)); }
	inline PointerOriginSmoothingSettings_t2805273516 * get_pointerOriginSmoothingSettings_11() const { return ___pointerOriginSmoothingSettings_11; }
	inline PointerOriginSmoothingSettings_t2805273516 ** get_address_of_pointerOriginSmoothingSettings_11() { return &___pointerOriginSmoothingSettings_11; }
	inline void set_pointerOriginSmoothingSettings_11(PointerOriginSmoothingSettings_t2805273516 * value)
	{
		___pointerOriginSmoothingSettings_11 = value;
		Il2CppCodeGenWriteBarrier(&___pointerOriginSmoothingSettings_11, value);
	}

	inline static int32_t get_offset_of_pointerMaterial_12() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___pointerMaterial_12)); }
	inline Material_t193706927 * get_pointerMaterial_12() const { return ___pointerMaterial_12; }
	inline Material_t193706927 ** get_address_of_pointerMaterial_12() { return &___pointerMaterial_12; }
	inline void set_pointerMaterial_12(Material_t193706927 * value)
	{
		___pointerMaterial_12 = value;
		Il2CppCodeGenWriteBarrier(&___pointerMaterial_12, value);
	}

	inline static int32_t get_offset_of_pointerHitColor_13() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___pointerHitColor_13)); }
	inline Color_t2020392075  get_pointerHitColor_13() const { return ___pointerHitColor_13; }
	inline Color_t2020392075 * get_address_of_pointerHitColor_13() { return &___pointerHitColor_13; }
	inline void set_pointerHitColor_13(Color_t2020392075  value)
	{
		___pointerHitColor_13 = value;
	}

	inline static int32_t get_offset_of_pointerMissColor_14() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___pointerMissColor_14)); }
	inline Color_t2020392075  get_pointerMissColor_14() const { return ___pointerMissColor_14; }
	inline Color_t2020392075 * get_address_of_pointerMissColor_14() { return &___pointerMissColor_14; }
	inline void set_pointerMissColor_14(Color_t2020392075  value)
	{
		___pointerMissColor_14 = value;
	}

	inline static int32_t get_offset_of_holdButtonToActivate_15() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___holdButtonToActivate_15)); }
	inline bool get_holdButtonToActivate_15() const { return ___holdButtonToActivate_15; }
	inline bool* get_address_of_holdButtonToActivate_15() { return &___holdButtonToActivate_15; }
	inline void set_holdButtonToActivate_15(bool value)
	{
		___holdButtonToActivate_15 = value;
	}

	inline static int32_t get_offset_of_interactWithObjects_16() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___interactWithObjects_16)); }
	inline bool get_interactWithObjects_16() const { return ___interactWithObjects_16; }
	inline bool* get_address_of_interactWithObjects_16() { return &___interactWithObjects_16; }
	inline void set_interactWithObjects_16(bool value)
	{
		___interactWithObjects_16 = value;
	}

	inline static int32_t get_offset_of_grabToPointerTip_17() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___grabToPointerTip_17)); }
	inline bool get_grabToPointerTip_17() const { return ___grabToPointerTip_17; }
	inline bool* get_address_of_grabToPointerTip_17() { return &___grabToPointerTip_17; }
	inline void set_grabToPointerTip_17(bool value)
	{
		___grabToPointerTip_17 = value;
	}

	inline static int32_t get_offset_of_activateDelay_18() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___activateDelay_18)); }
	inline float get_activateDelay_18() const { return ___activateDelay_18; }
	inline float* get_address_of_activateDelay_18() { return &___activateDelay_18; }
	inline void set_activateDelay_18(float value)
	{
		___activateDelay_18 = value;
	}

	inline static int32_t get_offset_of_pointerVisibility_19() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___pointerVisibility_19)); }
	inline int32_t get_pointerVisibility_19() const { return ___pointerVisibility_19; }
	inline int32_t* get_address_of_pointerVisibility_19() { return &___pointerVisibility_19; }
	inline void set_pointerVisibility_19(int32_t value)
	{
		___pointerVisibility_19 = value;
	}

	inline static int32_t get_offset_of_layersToIgnore_20() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___layersToIgnore_20)); }
	inline LayerMask_t3188175821  get_layersToIgnore_20() const { return ___layersToIgnore_20; }
	inline LayerMask_t3188175821 * get_address_of_layersToIgnore_20() { return &___layersToIgnore_20; }
	inline void set_layersToIgnore_20(LayerMask_t3188175821  value)
	{
		___layersToIgnore_20 = value;
	}

	inline static int32_t get_offset_of_destinationPosition_21() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___destinationPosition_21)); }
	inline Vector3_t2243707580  get_destinationPosition_21() const { return ___destinationPosition_21; }
	inline Vector3_t2243707580 * get_address_of_destinationPosition_21() { return &___destinationPosition_21; }
	inline void set_destinationPosition_21(Vector3_t2243707580  value)
	{
		___destinationPosition_21 = value;
	}

	inline static int32_t get_offset_of_pointerContactDistance_22() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___pointerContactDistance_22)); }
	inline float get_pointerContactDistance_22() const { return ___pointerContactDistance_22; }
	inline float* get_address_of_pointerContactDistance_22() { return &___pointerContactDistance_22; }
	inline void set_pointerContactDistance_22(float value)
	{
		___pointerContactDistance_22 = value;
	}

	inline static int32_t get_offset_of_pointerContactTarget_23() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___pointerContactTarget_23)); }
	inline Transform_t3275118058 * get_pointerContactTarget_23() const { return ___pointerContactTarget_23; }
	inline Transform_t3275118058 ** get_address_of_pointerContactTarget_23() { return &___pointerContactTarget_23; }
	inline void set_pointerContactTarget_23(Transform_t3275118058 * value)
	{
		___pointerContactTarget_23 = value;
		Il2CppCodeGenWriteBarrier(&___pointerContactTarget_23, value);
	}

	inline static int32_t get_offset_of_pointerContactRaycastHit_24() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___pointerContactRaycastHit_24)); }
	inline RaycastHit_t87180320  get_pointerContactRaycastHit_24() const { return ___pointerContactRaycastHit_24; }
	inline RaycastHit_t87180320 * get_address_of_pointerContactRaycastHit_24() { return &___pointerContactRaycastHit_24; }
	inline void set_pointerContactRaycastHit_24(RaycastHit_t87180320  value)
	{
		___pointerContactRaycastHit_24 = value;
	}

	inline static int32_t get_offset_of_controllerIndex_25() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___controllerIndex_25)); }
	inline uint32_t get_controllerIndex_25() const { return ___controllerIndex_25; }
	inline uint32_t* get_address_of_controllerIndex_25() { return &___controllerIndex_25; }
	inline void set_controllerIndex_25(uint32_t value)
	{
		___controllerIndex_25 = value;
	}

	inline static int32_t get_offset_of_playAreaCursor_26() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___playAreaCursor_26)); }
	inline VRTK_PlayAreaCursor_t3566057915 * get_playAreaCursor_26() const { return ___playAreaCursor_26; }
	inline VRTK_PlayAreaCursor_t3566057915 ** get_address_of_playAreaCursor_26() { return &___playAreaCursor_26; }
	inline void set_playAreaCursor_26(VRTK_PlayAreaCursor_t3566057915 * value)
	{
		___playAreaCursor_26 = value;
		Il2CppCodeGenWriteBarrier(&___playAreaCursor_26, value);
	}

	inline static int32_t get_offset_of_currentPointerColor_27() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___currentPointerColor_27)); }
	inline Color_t2020392075  get_currentPointerColor_27() const { return ___currentPointerColor_27; }
	inline Color_t2020392075 * get_address_of_currentPointerColor_27() { return &___currentPointerColor_27; }
	inline void set_currentPointerColor_27(Color_t2020392075  value)
	{
		___currentPointerColor_27 = value;
	}

	inline static int32_t get_offset_of_objectInteractor_28() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___objectInteractor_28)); }
	inline GameObject_t1756533147 * get_objectInteractor_28() const { return ___objectInteractor_28; }
	inline GameObject_t1756533147 ** get_address_of_objectInteractor_28() { return &___objectInteractor_28; }
	inline void set_objectInteractor_28(GameObject_t1756533147 * value)
	{
		___objectInteractor_28 = value;
		Il2CppCodeGenWriteBarrier(&___objectInteractor_28, value);
	}

	inline static int32_t get_offset_of_objectInteractorAttachPoint_29() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___objectInteractorAttachPoint_29)); }
	inline GameObject_t1756533147 * get_objectInteractorAttachPoint_29() const { return ___objectInteractorAttachPoint_29; }
	inline GameObject_t1756533147 ** get_address_of_objectInteractorAttachPoint_29() { return &___objectInteractorAttachPoint_29; }
	inline void set_objectInteractorAttachPoint_29(GameObject_t1756533147 * value)
	{
		___objectInteractorAttachPoint_29 = value;
		Il2CppCodeGenWriteBarrier(&___objectInteractorAttachPoint_29, value);
	}

	inline static int32_t get_offset_of_isActive_30() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___isActive_30)); }
	inline bool get_isActive_30() const { return ___isActive_30; }
	inline bool* get_address_of_isActive_30() { return &___isActive_30; }
	inline void set_isActive_30(bool value)
	{
		___isActive_30 = value;
	}

	inline static int32_t get_offset_of_destinationSetActive_31() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___destinationSetActive_31)); }
	inline bool get_destinationSetActive_31() const { return ___destinationSetActive_31; }
	inline bool* get_address_of_destinationSetActive_31() { return &___destinationSetActive_31; }
	inline void set_destinationSetActive_31(bool value)
	{
		___destinationSetActive_31 = value;
	}

	inline static int32_t get_offset_of_activateDelayTimer_32() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___activateDelayTimer_32)); }
	inline float get_activateDelayTimer_32() const { return ___activateDelayTimer_32; }
	inline float* get_address_of_activateDelayTimer_32() { return &___activateDelayTimer_32; }
	inline void set_activateDelayTimer_32(float value)
	{
		___activateDelayTimer_32 = value;
	}

	inline static int32_t get_offset_of_beamEnabledState_33() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___beamEnabledState_33)); }
	inline int32_t get_beamEnabledState_33() const { return ___beamEnabledState_33; }
	inline int32_t* get_address_of_beamEnabledState_33() { return &___beamEnabledState_33; }
	inline void set_beamEnabledState_33(int32_t value)
	{
		___beamEnabledState_33 = value;
	}

	inline static int32_t get_offset_of_interactableObject_34() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___interactableObject_34)); }
	inline VRTK_InteractableObject_t2604188111 * get_interactableObject_34() const { return ___interactableObject_34; }
	inline VRTK_InteractableObject_t2604188111 ** get_address_of_interactableObject_34() { return &___interactableObject_34; }
	inline void set_interactableObject_34(VRTK_InteractableObject_t2604188111 * value)
	{
		___interactableObject_34 = value;
		Il2CppCodeGenWriteBarrier(&___interactableObject_34, value);
	}

	inline static int32_t get_offset_of_savedAttachPoint_35() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___savedAttachPoint_35)); }
	inline Rigidbody_t4233889191 * get_savedAttachPoint_35() const { return ___savedAttachPoint_35; }
	inline Rigidbody_t4233889191 ** get_address_of_savedAttachPoint_35() { return &___savedAttachPoint_35; }
	inline void set_savedAttachPoint_35(Rigidbody_t4233889191 * value)
	{
		___savedAttachPoint_35 = value;
		Il2CppCodeGenWriteBarrier(&___savedAttachPoint_35, value);
	}

	inline static int32_t get_offset_of_attachedToInteractorAttachPoint_36() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___attachedToInteractorAttachPoint_36)); }
	inline bool get_attachedToInteractorAttachPoint_36() const { return ___attachedToInteractorAttachPoint_36; }
	inline bool* get_address_of_attachedToInteractorAttachPoint_36() { return &___attachedToInteractorAttachPoint_36; }
	inline void set_attachedToInteractorAttachPoint_36(bool value)
	{
		___attachedToInteractorAttachPoint_36 = value;
	}

	inline static int32_t get_offset_of_savedBeamLength_37() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___savedBeamLength_37)); }
	inline float get_savedBeamLength_37() const { return ___savedBeamLength_37; }
	inline float* get_address_of_savedBeamLength_37() { return &___savedBeamLength_37; }
	inline void set_savedBeamLength_37(float value)
	{
		___savedBeamLength_37 = value;
	}

	inline static int32_t get_offset_of_controllerGrabScript_38() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___controllerGrabScript_38)); }
	inline VRTK_InteractGrab_t124353446 * get_controllerGrabScript_38() const { return ___controllerGrabScript_38; }
	inline VRTK_InteractGrab_t124353446 ** get_address_of_controllerGrabScript_38() { return &___controllerGrabScript_38; }
	inline void set_controllerGrabScript_38(VRTK_InteractGrab_t124353446 * value)
	{
		___controllerGrabScript_38 = value;
		Il2CppCodeGenWriteBarrier(&___controllerGrabScript_38, value);
	}

	inline static int32_t get_offset_of_pointerOriginTransformFollowGameObject_39() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___pointerOriginTransformFollowGameObject_39)); }
	inline GameObject_t1756533147 * get_pointerOriginTransformFollowGameObject_39() const { return ___pointerOriginTransformFollowGameObject_39; }
	inline GameObject_t1756533147 ** get_address_of_pointerOriginTransformFollowGameObject_39() { return &___pointerOriginTransformFollowGameObject_39; }
	inline void set_pointerOriginTransformFollowGameObject_39(GameObject_t1756533147 * value)
	{
		___pointerOriginTransformFollowGameObject_39 = value;
		Il2CppCodeGenWriteBarrier(&___pointerOriginTransformFollowGameObject_39, value);
	}

	inline static int32_t get_offset_of_pointerOriginTransformFollow_40() { return static_cast<int32_t>(offsetof(VRTK_BasePointer_t3466805540, ___pointerOriginTransformFollow_40)); }
	inline VRTK_TransformFollow_t3532748285 * get_pointerOriginTransformFollow_40() const { return ___pointerOriginTransformFollow_40; }
	inline VRTK_TransformFollow_t3532748285 ** get_address_of_pointerOriginTransformFollow_40() { return &___pointerOriginTransformFollow_40; }
	inline void set_pointerOriginTransformFollow_40(VRTK_TransformFollow_t3532748285 * value)
	{
		___pointerOriginTransformFollow_40 = value;
		Il2CppCodeGenWriteBarrier(&___pointerOriginTransformFollow_40, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
