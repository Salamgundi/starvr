﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.SDK_BaseSystem
struct SDK_BaseSystem_t244469351;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTK.SDK_BaseSystem::.ctor()
extern "C"  void SDK_BaseSystem__ctor_m1062015565 (SDK_BaseSystem_t244469351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
