﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Valve.VR.InteractionSystem.InteractableHoverEvents
struct  InteractableHoverEvents_t1346270787  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.InteractableHoverEvents::onHandHoverBegin
	UnityEvent_t408735097 * ___onHandHoverBegin_2;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.InteractableHoverEvents::onHandHoverEnd
	UnityEvent_t408735097 * ___onHandHoverEnd_3;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.InteractableHoverEvents::onAttachedToHand
	UnityEvent_t408735097 * ___onAttachedToHand_4;
	// UnityEngine.Events.UnityEvent Valve.VR.InteractionSystem.InteractableHoverEvents::onDetachedFromHand
	UnityEvent_t408735097 * ___onDetachedFromHand_5;

public:
	inline static int32_t get_offset_of_onHandHoverBegin_2() { return static_cast<int32_t>(offsetof(InteractableHoverEvents_t1346270787, ___onHandHoverBegin_2)); }
	inline UnityEvent_t408735097 * get_onHandHoverBegin_2() const { return ___onHandHoverBegin_2; }
	inline UnityEvent_t408735097 ** get_address_of_onHandHoverBegin_2() { return &___onHandHoverBegin_2; }
	inline void set_onHandHoverBegin_2(UnityEvent_t408735097 * value)
	{
		___onHandHoverBegin_2 = value;
		Il2CppCodeGenWriteBarrier(&___onHandHoverBegin_2, value);
	}

	inline static int32_t get_offset_of_onHandHoverEnd_3() { return static_cast<int32_t>(offsetof(InteractableHoverEvents_t1346270787, ___onHandHoverEnd_3)); }
	inline UnityEvent_t408735097 * get_onHandHoverEnd_3() const { return ___onHandHoverEnd_3; }
	inline UnityEvent_t408735097 ** get_address_of_onHandHoverEnd_3() { return &___onHandHoverEnd_3; }
	inline void set_onHandHoverEnd_3(UnityEvent_t408735097 * value)
	{
		___onHandHoverEnd_3 = value;
		Il2CppCodeGenWriteBarrier(&___onHandHoverEnd_3, value);
	}

	inline static int32_t get_offset_of_onAttachedToHand_4() { return static_cast<int32_t>(offsetof(InteractableHoverEvents_t1346270787, ___onAttachedToHand_4)); }
	inline UnityEvent_t408735097 * get_onAttachedToHand_4() const { return ___onAttachedToHand_4; }
	inline UnityEvent_t408735097 ** get_address_of_onAttachedToHand_4() { return &___onAttachedToHand_4; }
	inline void set_onAttachedToHand_4(UnityEvent_t408735097 * value)
	{
		___onAttachedToHand_4 = value;
		Il2CppCodeGenWriteBarrier(&___onAttachedToHand_4, value);
	}

	inline static int32_t get_offset_of_onDetachedFromHand_5() { return static_cast<int32_t>(offsetof(InteractableHoverEvents_t1346270787, ___onDetachedFromHand_5)); }
	inline UnityEvent_t408735097 * get_onDetachedFromHand_5() const { return ___onDetachedFromHand_5; }
	inline UnityEvent_t408735097 ** get_address_of_onDetachedFromHand_5() { return &___onDetachedFromHand_5; }
	inline void set_onDetachedFromHand_5(UnityEvent_t408735097 * value)
	{
		___onDetachedFromHand_5 = value;
		Il2CppCodeGenWriteBarrier(&___onDetachedFromHand_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
