﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVROverlay/_SetOverlayTextureColorSpace
struct _SetOverlayTextureColorSpace_t1399555963;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Valve_VR_EVROverlayError3464864153.h"
#include "AssemblyU2DCSharp_Valve_VR_EColorSpace2848861630.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVROverlay/_SetOverlayTextureColorSpace::.ctor(System.Object,System.IntPtr)
extern "C"  void _SetOverlayTextureColorSpace__ctor_m1758240510 (_SetOverlayTextureColorSpace_t1399555963 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTextureColorSpace::Invoke(System.UInt64,Valve.VR.EColorSpace)
extern "C"  int32_t _SetOverlayTextureColorSpace_Invoke_m3985814997 (_SetOverlayTextureColorSpace_t1399555963 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTextureColorSpace1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVROverlay/_SetOverlayTextureColorSpace::BeginInvoke(System.UInt64,Valve.VR.EColorSpace,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _SetOverlayTextureColorSpace_BeginInvoke_m424434592 (_SetOverlayTextureColorSpace_t1399555963 * __this, uint64_t ___ulOverlayHandle0, int32_t ___eTextureColorSpace1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Valve.VR.EVROverlayError Valve.VR.IVROverlay/_SetOverlayTextureColorSpace::EndInvoke(System.IAsyncResult)
extern "C"  int32_t _SetOverlayTextureColorSpace_EndInvoke_m4018109338 (_SetOverlayTextureColorSpace_t1399555963 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
