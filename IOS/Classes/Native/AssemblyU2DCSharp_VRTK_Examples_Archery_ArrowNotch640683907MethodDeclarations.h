﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTK.Examples.Archery.ArrowNotch
struct ArrowNotch_t640683907;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void VRTK.Examples.Archery.ArrowNotch::.ctor()
extern "C"  void ArrowNotch__ctor_m454774180 (ArrowNotch_t640683907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.ArrowNotch::Start()
extern "C"  void ArrowNotch_Start_m3625797148 (ArrowNotch_t640683907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.ArrowNotch::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void ArrowNotch_OnTriggerEnter_m2851835328 (ArrowNotch_t640683907 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTK.Examples.Archery.ArrowNotch::CopyNotchToArrow()
extern "C"  void ArrowNotch_CopyNotchToArrow_m1483584793 (ArrowNotch_t640683907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
