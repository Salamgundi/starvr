﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Valve.VR.IVRApplications/_GetApplicationCount
struct _GetApplicationCount_t2482336573;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Valve.VR.IVRApplications/_GetApplicationCount::.ctor(System.Object,System.IntPtr)
extern "C"  void _GetApplicationCount__ctor_m2546995772 (_GetApplicationCount_t2482336573 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRApplications/_GetApplicationCount::Invoke()
extern "C"  uint32_t _GetApplicationCount_Invoke_m3469067159 (_GetApplicationCount_t2482336573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Valve.VR.IVRApplications/_GetApplicationCount::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * _GetApplicationCount_BeginInvoke_m1347942107 (_GetApplicationCount_t2482336573 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Valve.VR.IVRApplications/_GetApplicationCount::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t _GetApplicationCount_EndInvoke_m622090841 (_GetApplicationCount_t2482336573 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
